.class public Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;
.super Lcom/itau/empresas/ui/fragment/BaseFragment;
.source "ITokenInstaladoFragment.java"


# instance fields
.field controller:Lcom/itau/empresas/controller/ITokenController;

.field private mHandler:Landroid/os/Handler;

.field private otpModel:Lbr/com/itau/security/token/model/OTPModel;

.field progressTempoToken:Landroid/widget/ProgressBar;

.field textoNumero:Landroid/widget/TextView;

.field textoNumeroSerie:Landroid/widget/TextView;

.field textoNumeroToken:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 29
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;-><init>()V

    .line 38
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;

    .line 29
    invoke-direct {p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->exibeToken()V

    return-void
.end method

.method private animeOProgresso(III)V
    .registers 8
    .param p1, "progressStatus"    # I
    .param p2, "max"    # I
    .param p3, "duracao"    # I

    .line 76
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 v1, 0x1

    aput p2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    int-to-long v1, p3

    .line 77
    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-result-object v3

    .line 78
    .local v3, "valueAnimator":Landroid/animation/ValueAnimator;
    new-instance v0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment$2;-><init>(Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;)V

    invoke-virtual {v3, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 85
    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->start()V

    .line 86
    return-void
.end method

.method private exibeToken()V
    .registers 7

    .line 53
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getOfflineOTP()Lbr/com/itau/security/token/model/OTPModel;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->progressTempoToken:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->textoNumeroToken:Landroid/widget/TextView;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->textoNumero:Landroid/widget/TextView;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    if-nez v0, :cond_17

    .line 56
    :cond_16
    return-void

    .line 58
    :cond_17
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    invoke-virtual {v0}, Lbr/com/itau/security/token/model/OTPModel;->timeRemaining()J

    move-result-wide v0

    long-to-int v4, v0

    .line 59
    .local v4, "progressStatus":I
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    invoke-virtual {v0}, Lbr/com/itau/security/token/model/OTPModel;->timeInterval()J

    move-result-wide v0

    long-to-int v5, v0

    .line 61
    .local v5, "max":I
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->progressTempoToken:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v5}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->progressTempoToken:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 64
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->textoNumeroToken:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    invoke-virtual {v1}, Lbr/com/itau/security/token/model/OTPModel;->token()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->textoNumero:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    invoke-virtual {v1}, Lbr/com/itau/security/token/model/OTPModel;->serialNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    invoke-direct {p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->getTempoRestante()J

    move-result-wide v0

    long-to-int v0, v0

    const/4 v1, 0x0

    invoke-direct {p0, v1, v5, v0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->animeOProgresso(III)V

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment$1;-><init>(Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;)V

    .line 72
    invoke-direct {p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->getTempoRestante()J

    move-result-wide v2

    .line 68
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 73
    return-void
.end method

.method private getTempoRestante()J
    .registers 5

    .line 89
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    invoke-virtual {v0}, Lbr/com/itau/security/token/model/OTPModel;->timeRemaining()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method aoCarregar()V
    .registers 2

    .line 44
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->escondeDialogoDeProgresso()V

    .line 45
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/SnackbarUtils;->tentaDispensarSnackbarNo(Ljava/lang/Object;)V

    .line 47
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getOfflineOTP()Lbr/com/itau/security/token/model/OTPModel;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    if-eqz v0, :cond_17

    .line 49
    invoke-direct {p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->exibeToken()V

    .line 50
    :cond_17
    return-void
.end method

.method aoClicarEmDesinstalarIToken()V
    .registers 6

    .line 113
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 114
    const v2, 0x7f0702df

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 115
    const v3, 0x7f0702a0

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 116
    const v4, 0x7f070304

    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 118
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->mostraDialogoDeProgresso()V

    .line 119
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->ocultarMensagemErro()V

    .line 120
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->controller:Lcom/itau/empresas/controller/ITokenController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/ITokenController;->cancelarTokenApp()V

    .line 121
    return-void
.end method

.method aoClicarEmInicio()V
    .registers 6

    .line 104
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 105
    const v2, 0x7f0702df

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 106
    const v3, 0x7f0702a0

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 107
    const v4, 0x7f070309

    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 108
    invoke-static {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 109
    return-void
.end method

.method aoClicarEmOrientacoesSeguranca()V
    .registers 6

    .line 94
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 95
    const v2, 0x7f0702df

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 96
    const v3, 0x7f0702a0

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 97
    const v4, 0x7f07031c

    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/itoken/fragment/ITokenInstaladoFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 99
    invoke-static {p0}, Lcom/itau/empresas/feature/itoken/ITokenOrientacoesSegurancaActivity;->intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/feature/itoken/ITokenOrientacoesSegurancaActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/itoken/ITokenOrientacoesSegurancaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 100
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 125
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.class Landroid/support/v4/widget/NestedScrollView$AccessibilityDelegate;
.super Landroid/support/v4/view/AccessibilityDelegateCompat;
.source "NestedScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v4/widget/NestedScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "AccessibilityDelegate"
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .line 1889
    invoke-direct {p0}, Landroid/support/v4/view/AccessibilityDelegateCompat;-><init>()V

    return-void
.end method


# virtual methods
.method public onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V
    .registers 7
    .param p1, "host"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .line 1946
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/AccessibilityDelegateCompat;->onInitializeAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1947
    move-object v1, p1

    check-cast v1, Landroid/support/v4/widget/NestedScrollView;

    .line 1948
    .local v1, "nsvHost":Landroid/support/v4/widget/NestedScrollView;
    const-class v0, Landroid/widget/ScrollView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 1949
    invoke-static {p2}, Landroid/support/v4/view/accessibility/AccessibilityEventCompat;->asRecord(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    move-result-object v2

    .line 1950
    .local v2, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    invoke-virtual {v1}, Landroid/support/v4/widget/NestedScrollView;->getScrollRange()I

    move-result v0

    if-lez v0, :cond_1b

    const/4 v3, 0x1

    goto :goto_1c

    :cond_1b
    const/4 v3, 0x0

    .line 1951
    .local v3, "scrollable":Z
    :goto_1c
    invoke-virtual {v2, v3}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->setScrollable(Z)V

    .line 1952
    invoke-virtual {v1}, Landroid/support/v4/widget/NestedScrollView;->getScrollX()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->setScrollX(I)V

    .line 1953
    invoke-virtual {v1}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->setScrollY(I)V

    .line 1954
    invoke-virtual {v1}, Landroid/support/v4/widget/NestedScrollView;->getScrollX()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->setMaxScrollX(I)V

    .line 1955
    invoke-virtual {v1}, Landroid/support/v4/widget/NestedScrollView;->getScrollRange()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->setMaxScrollY(I)V

    .line 1956
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .registers 6
    .param p1, "host"    # Landroid/view/View;
    .param p2, "info"    # Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    .line 1927
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/AccessibilityDelegateCompat;->onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    .line 1928
    move-object v1, p1

    check-cast v1, Landroid/support/v4/widget/NestedScrollView;

    .line 1929
    .local v1, "nsvHost":Landroid/support/v4/widget/NestedScrollView;
    const-class v0, Landroid/widget/ScrollView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setClassName(Ljava/lang/CharSequence;)V

    .line 1930
    invoke-virtual {v1}, Landroid/support/v4/widget/NestedScrollView;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_35

    .line 1931
    invoke-virtual {v1}, Landroid/support/v4/widget/NestedScrollView;->getScrollRange()I

    move-result v2

    .line 1932
    .local v2, "scrollRange":I
    if-lez v2, :cond_35

    .line 1933
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setScrollable(Z)V

    .line 1934
    invoke-virtual {v1}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v0

    if-lez v0, :cond_2a

    .line 1935
    const/16 v0, 0x2000

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    .line 1937
    :cond_2a
    invoke-virtual {v1}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v0

    if-ge v0, v2, :cond_35

    .line 1938
    const/16 v0, 0x1000

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    .line 1942
    .end local v2    # "scrollRange":I
    :cond_35
    return-void
.end method

.method public performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z
    .registers 9
    .param p1, "host"    # Landroid/view/View;
    .param p2, "action"    # I
    .param p3, "arguments"    # Landroid/os/Bundle;

    .line 1892
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/view/AccessibilityDelegateCompat;->performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1893
    const/4 v0, 0x1

    return v0

    .line 1895
    :cond_8
    move-object v2, p1

    check-cast v2, Landroid/support/v4/widget/NestedScrollView;

    .line 1896
    .local v2, "nsvHost":Landroid/support/v4/widget/NestedScrollView;
    invoke-virtual {v2}, Landroid/support/v4/widget/NestedScrollView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_13

    .line 1897
    const/4 v0, 0x0

    return v0

    .line 1899
    :cond_13
    sparse-switch p2, :sswitch_data_6c

    goto/16 :goto_69

    .line 1901
    :sswitch_18
    invoke-virtual {v2}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v0

    invoke-virtual {v2}, Landroid/support/v4/widget/NestedScrollView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1902
    invoke-virtual {v2}, Landroid/support/v4/widget/NestedScrollView;->getPaddingTop()I

    move-result v1

    sub-int v3, v0, v1

    .line 1903
    .local v3, "viewportHeight":I
    invoke-virtual {v2}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v0

    add-int/2addr v0, v3

    .line 1904
    invoke-virtual {v2}, Landroid/support/v4/widget/NestedScrollView;->getScrollRange()I

    move-result v1

    .line 1903
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 1905
    .local v4, "targetScrollY":I
    invoke-virtual {v2}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v0

    if-eq v4, v0, :cond_40

    .line 1906
    const/4 v0, 0x0

    invoke-virtual {v2, v0, v4}, Landroid/support/v4/widget/NestedScrollView;->smoothScrollTo(II)V

    .line 1907
    const/4 v0, 0x1

    return v0

    .line 1910
    .end local v3    # "viewportHeight":I
    .end local v4    # "targetScrollY":I
    :cond_40
    const/4 v0, 0x0

    return v0

    .line 1912
    :sswitch_42
    invoke-virtual {v2}, Landroid/support/v4/widget/NestedScrollView;->getHeight()I

    move-result v0

    invoke-virtual {v2}, Landroid/support/v4/widget/NestedScrollView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1913
    invoke-virtual {v2}, Landroid/support/v4/widget/NestedScrollView;->getPaddingTop()I

    move-result v1

    sub-int v3, v0, v1

    .line 1914
    .local v3, "viewportHeight":I
    invoke-virtual {v2}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v0

    sub-int/2addr v0, v3

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 1915
    .local v4, "targetScrollY":I
    invoke-virtual {v2}, Landroid/support/v4/widget/NestedScrollView;->getScrollY()I

    move-result v0

    if-eq v4, v0, :cond_67

    .line 1916
    const/4 v0, 0x0

    invoke-virtual {v2, v0, v4}, Landroid/support/v4/widget/NestedScrollView;->smoothScrollTo(II)V

    .line 1917
    const/4 v0, 0x1

    return v0

    .line 1920
    .end local v3    # "viewportHeight":I
    .end local v4    # "targetScrollY":I
    :cond_67
    const/4 v0, 0x0

    return v0

    .line 1922
    :goto_69
    const/4 v0, 0x0

    return v0

    nop

    :sswitch_data_6c
    .sparse-switch
        0x1000 -> :sswitch_18
        0x2000 -> :sswitch_42
    .end sparse-switch
.end method

.class public final Lbr/com/itau/textovoz/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f0c0169

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f0c016a

.field public static final abc_btn_colored_borderless_text_material:I = 0x7f0c016b

.field public static final abc_color_highlight_material:I = 0x7f0c016c

.field public static final abc_input_method_navigation_guard:I = 0x7f0c0001

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f0c016f

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f0c0170

.field public static final abc_primary_text_material_dark:I = 0x7f0c0171

.field public static final abc_primary_text_material_light:I = 0x7f0c0172

.field public static final abc_search_url_text:I = 0x7f0c0173

.field public static final abc_search_url_text_normal:I = 0x7f0c0002

.field public static final abc_search_url_text_pressed:I = 0x7f0c0003

.field public static final abc_search_url_text_selected:I = 0x7f0c0004

.field public static final abc_secondary_text_material_dark:I = 0x7f0c0174

.field public static final abc_secondary_text_material_light:I = 0x7f0c0175

.field public static final abc_tint_btn_checkable:I = 0x7f0c0176

.field public static final abc_tint_default:I = 0x7f0c0177

.field public static final abc_tint_edittext:I = 0x7f0c0178

.field public static final abc_tint_seek_thumb:I = 0x7f0c0179

.field public static final abc_tint_spinner:I = 0x7f0c017a

.field public static final abc_tint_switch_thumb:I = 0x7f0c017b

.field public static final abc_tint_switch_track:I = 0x7f0c017c

.field public static final accent_material_dark:I = 0x7f0c0005

.field public static final accent_material_light:I = 0x7f0c0006

.field public static final background_floating_material_dark:I = 0x7f0c0012

.field public static final background_floating_material_light:I = 0x7f0c0013

.field public static final background_material_dark:I = 0x7f0c0014

.field public static final background_material_light:I = 0x7f0c0015

.field public static final black:I = 0x7f0c0019

.field public static final black_000000:I = 0x7f0c001a

.field public static final black_464646:I = 0x7f0c001b

.field public static final blue_174F78:I = 0x7f0c001c

.field public static final blue_2581C4:I = 0x7f0c001d

.field public static final blue_FF2581C4:I = 0x7f0c001e

.field public static final blue_clear:I = 0x7f0c001f

.field public static final bright_foreground_disabled_material_dark:I = 0x7f0c0027

.field public static final bright_foreground_disabled_material_light:I = 0x7f0c0028

.field public static final bright_foreground_inverse_material_dark:I = 0x7f0c0029

.field public static final bright_foreground_inverse_material_light:I = 0x7f0c002a

.field public static final bright_foreground_material_dark:I = 0x7f0c002b

.field public static final bright_foreground_material_light:I = 0x7f0c002c

.field public static final button_material_dark:I = 0x7f0c002d

.field public static final button_material_light:I = 0x7f0c002e

.field public static final cardview_dark_background:I = 0x7f0c002f

.field public static final cardview_light_background:I = 0x7f0c0030

.field public static final cardview_shadow_end_color:I = 0x7f0c0031

.field public static final cardview_shadow_start_color:I = 0x7f0c0032

.field public static final colorAccent:I = 0x7f0c0054

.field public static final colorPrimary:I = 0x7f0c0055

.field public static final colorPrimaryDark:I = 0x7f0c0056

.field public static final color_transparent:I = 0x7f0c0057

.field public static final contact_info:I = 0x7f0c007b

.field public static final design_error:I = 0x7f0c0183

.field public static final design_fab_shadow_end_color:I = 0x7f0c008a

.field public static final design_fab_shadow_mid_color:I = 0x7f0c008b

.field public static final design_fab_shadow_start_color:I = 0x7f0c008c

.field public static final design_fab_stroke_end_inner_color:I = 0x7f0c008d

.field public static final design_fab_stroke_end_outer_color:I = 0x7f0c008e

.field public static final design_fab_stroke_top_inner_color:I = 0x7f0c008f

.field public static final design_fab_stroke_top_outer_color:I = 0x7f0c0090

.field public static final design_snackbar_background_color:I = 0x7f0c0091

.field public static final design_textinput_error_color_dark:I = 0x7f0c0092

.field public static final design_textinput_error_color_light:I = 0x7f0c0093

.field public static final design_tint_password_toggle:I = 0x7f0c0184

.field public static final dim_foreground_disabled_material_dark:I = 0x7f0c0094

.field public static final dim_foreground_disabled_material_light:I = 0x7f0c0095

.field public static final dim_foreground_material_dark:I = 0x7f0c0096

.field public static final dim_foreground_material_light:I = 0x7f0c0097

.field public static final disabled_color:I = 0x7f0c0098

.field public static final error_color:I = 0x7f0c0099

.field public static final foreground_material_dark:I = 0x7f0c009a

.field public static final foreground_material_light:I = 0x7f0c009b

.field public static final gray_33464646:I = 0x7f0c009c

.field public static final gray_40464646:I = 0x7f0c009d

.field public static final gray_424242:I = 0x7f0c009e

.field public static final gray_464646:I = 0x7f0c009f

.field public static final gray_575757:I = 0x7f0c00a0

.field public static final gray_757575:I = 0x7f0c00a1

.field public static final gray_847E79:I = 0x7f0c00a2

.field public static final gray_8C8C8C:I = 0x7f0c00a3

.field public static final gray_969696:I = 0x7f0c00a4

.field public static final gray_99F2F2F2:I = 0x7f0c00a5

.field public static final gray_C8C8C8:I = 0x7f0c00a6

.field public static final gray_D2D2D2:I = 0x7f0c00a7

.field public static final gray_DCD7D2:I = 0x7f0c00a8

.field public static final gray_E6E6E6:I = 0x7f0c00a9

.field public static final gray_F2F2F2:I = 0x7f0c00aa

.field public static final gray_FAFAFA:I = 0x7f0c00ab

.field public static final gray_FCF2F2F2:I = 0x7f0c00ac

.field public static final gray_aa000000:I = 0x7f0c00ad

.field public static final gray_ff575757:I = 0x7f0c00ae

.field public static final green_45A9BB:I = 0x7f0c00af

.field public static final help_search_personnalite_background_color:I = 0x7f0c00b0

.field public static final help_search_personnalite_text_color:I = 0x7f0c00b1

.field public static final help_search_personnalite_title_color:I = 0x7f0c00b2

.field public static final highlighted_text_material_dark:I = 0x7f0c00b3

.field public static final highlighted_text_material_light:I = 0x7f0c00b4

.field public static final investment_cdb:I = 0x7f0c00b6

.field public static final investment_fund:I = 0x7f0c00b7

.field public static final investment_private_pension:I = 0x7f0c00b8

.field public static final investment_real_state:I = 0x7f0c00b9

.field public static final investment_savings:I = 0x7f0c00ba

.field public static final investment_stock:I = 0x7f0c00bb

.field public static final investment_super_savings:I = 0x7f0c00bc

.field public static final itau_background_above_image:I = 0x7f0c00bd

.field public static final itau_primary:I = 0x7f0c00be

.field public static final itau_primary_dark:I = 0x7f0c00bf

.field public static final itau_theme_color:I = 0x7f0c00c0

.field public static final itau_theme_color_dark:I = 0x7f0c00c1

.field public static final itau_window_background:I = 0x7f0c00c2

.field public static final itausdkcore_color_password_button_text:I = 0x7f0c0185

.field public static final itausdkcore_ok_background_color:I = 0x7f0c00c7

.field public static final itausdkcore_reenviar_background_color:I = 0x7f0c00c8

.field public static final itausdkcore_reenviar_disabled_background_color:I = 0x7f0c00c9

.field public static final itausdkcore_token_divisor:I = 0x7f0c00ca

.field public static final material_blue_grey_800:I = 0x7f0c00d5

.field public static final material_blue_grey_900:I = 0x7f0c00d6

.field public static final material_blue_grey_950:I = 0x7f0c00d7

.field public static final material_deep_teal_200:I = 0x7f0c00d8

.field public static final material_deep_teal_500:I = 0x7f0c00d9

.field public static final material_grey_100:I = 0x7f0c00da

.field public static final material_grey_300:I = 0x7f0c00db

.field public static final material_grey_50:I = 0x7f0c00dc

.field public static final material_grey_600:I = 0x7f0c00dd

.field public static final material_grey_800:I = 0x7f0c00de

.field public static final material_grey_850:I = 0x7f0c00df

.field public static final material_grey_900:I = 0x7f0c00e0

.field public static final mdtp_accent_color:I = 0x7f0c00e1

.field public static final mdtp_accent_color_dark:I = 0x7f0c00e2

.field public static final menu_bg_back_itau:I = 0x7f0c0106

.field public static final menu_bg_back_itau_dark:I = 0x7f0c0107

.field public static final menu_bg_back_personnnalite:I = 0x7f0c0108

.field public static final menu_bg_back_personnnalite_dark:I = 0x7f0c0109

.field public static final menu_bg_back_private:I = 0x7f0c010a

.field public static final menu_bg_back_private_dark:I = 0x7f0c010b

.field public static final menu_bg_back_uniclass:I = 0x7f0c010c

.field public static final menu_bg_back_uniclass_dark:I = 0x7f0c010d

.field public static final menu_bg_header_itau:I = 0x7f0c010e

.field public static final menu_bg_header_personnalite:I = 0x7f0c010f

.field public static final menu_bg_header_uniclass:I = 0x7f0c0110

.field public static final menu_text_back:I = 0x7f0c0111

.field public static final menu_text_back_personnalite:I = 0x7f0c0112

.field public static final menu_text_header_itau:I = 0x7f0c0113

.field public static final menu_text_header_personnnalite:I = 0x7f0c0114

.field public static final menu_text_header_private:I = 0x7f0c0115

.field public static final menu_text_header_uniclass:I = 0x7f0c0116

.field public static final notification_central_itau_color:I = 0x7f0c011a

.field public static final notification_central_personnalite_color:I = 0x7f0c011b

.field public static final personnalite_theme_color:I = 0x7f0c011e

.field public static final personnalite_theme_color_dark:I = 0x7f0c011f

.field public static final primary_dark_material_dark:I = 0x7f0c0120

.field public static final primary_dark_material_light:I = 0x7f0c0121

.field public static final primary_material_dark:I = 0x7f0c0122

.field public static final primary_material_light:I = 0x7f0c0123

.field public static final primary_text_default_material_dark:I = 0x7f0c0124

.field public static final primary_text_default_material_light:I = 0x7f0c0125

.field public static final primary_text_disabled_material_dark:I = 0x7f0c0126

.field public static final primary_text_disabled_material_light:I = 0x7f0c0127

.field public static final red_F34235:I = 0x7f0c0128

.field public static final ripple_material_dark:I = 0x7f0c0129

.field public static final ripple_material_light:I = 0x7f0c012a

.field public static final search_background_head_color:I = 0x7f0c012f

.field public static final search_background_item_color:I = 0x7f0c0130

.field public static final search_background_item_text_color:I = 0x7f0c0131

.field public static final search_background_item_text_color_description:I = 0x7f0c0132

.field public static final search_background_item_title:I = 0x7f0c0133

.field public static final secondary_text_default_material_dark:I = 0x7f0c0134

.field public static final secondary_text_default_material_light:I = 0x7f0c0135

.field public static final secondary_text_disabled_material_dark:I = 0x7f0c0136

.field public static final secondary_text_disabled_material_light:I = 0x7f0c0137

.field public static final send_background_item_blue:I = 0x7f0c0138

.field public static final send_background_item_title:I = 0x7f0c0139

.field public static final signal_negative_color:I = 0x7f0c013a

.field public static final switch_thumb_disabled_material_dark:I = 0x7f0c0141

.field public static final switch_thumb_disabled_material_light:I = 0x7f0c0142

.field public static final switch_thumb_material_dark:I = 0x7f0c018a

.field public static final switch_thumb_material_light:I = 0x7f0c018b

.field public static final switch_thumb_normal_material_dark:I = 0x7f0c0143

.field public static final switch_thumb_normal_material_light:I = 0x7f0c0144

.field public static final text_color_theme_itau:I = 0x7f0c0145

.field public static final text_color_theme_personnalite:I = 0x7f0c0146

.field public static final transparent:I = 0x7f0c0147

.field public static final white_55ffffff:I = 0x7f0c015b

.field public static final white_fffafa:I = 0x7f0c015c

.field public static final white_ffffff:I = 0x7f0c015d


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 414
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

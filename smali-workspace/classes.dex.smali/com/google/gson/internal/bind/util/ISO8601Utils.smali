.class public Lcom/google/gson/internal/bind/util/ISO8601Utils;
.super Ljava/lang/Object;
.source "ISO8601Utils.java"


# static fields
.field private static final TIMEZONE_UTC:Ljava/util/TimeZone;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 30
    const-string v0, "UTC"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, Lcom/google/gson/internal/bind/util/ISO8601Utils;->TIMEZONE_UTC:Ljava/util/TimeZone;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static checkOffset(Ljava/lang/String;IC)Z
    .registers 4
    .param p0, "value"    # Ljava/lang/String;
    .param p1, "offset"    # I
    .param p2, "expected"    # C

    .line 288
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge p1, v0, :cond_e

    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, p2, :cond_e

    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

.method private static indexOfNonDigit(Ljava/lang/String;I)I
    .registers 5
    .param p0, "string"    # Ljava/lang/String;
    .param p1, "offset"    # I

    .line 345
    move v1, p1

    .local v1, "i":I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v1, v0, :cond_17

    .line 346
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 347
    .local v2, "c":C
    const/16 v0, 0x30

    if-lt v2, v0, :cond_13

    const/16 v0, 0x39

    if-le v2, v0, :cond_14

    :cond_13
    return v1

    .line 345
    .end local v2    # "c":C
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 349
    .end local v1    # "i":I
    :cond_17
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    return v0
.end method

.method public static parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Date;
    .registers 21
    .param p0, "date"    # Ljava/lang/String;
    .param p1, "pos"    # Ljava/text/ParsePosition;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 124
    const/4 v3, 0x0

    .line 126
    .local v3, "fail":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Ljava/text/ParsePosition;->getIndex()I

    move-result v4

    .line 129
    .local v4, "offset":I
    move v0, v4

    add-int/lit8 v4, v4, 0x4

    move-object/from16 v1, p0

    invoke-static {v1, v0, v4}, Lcom/google/gson/internal/bind/util/ISO8601Utils;->parseInt(Ljava/lang/String;II)I

    move-result v5

    .line 130
    .local v5, "year":I
    move-object/from16 v0, p0

    const/16 v1, 0x2d

    invoke-static {v0, v4, v1}, Lcom/google/gson/internal/bind/util/ISO8601Utils;->checkOffset(Ljava/lang/String;IC)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 131
    add-int/lit8 v4, v4, 0x1

    .line 135
    :cond_1a
    move v0, v4

    add-int/lit8 v4, v4, 0x2

    move-object/from16 v1, p0

    invoke-static {v1, v0, v4}, Lcom/google/gson/internal/bind/util/ISO8601Utils;->parseInt(Ljava/lang/String;II)I

    move-result v6

    .line 136
    .local v6, "month":I
    move-object/from16 v0, p0

    const/16 v1, 0x2d

    invoke-static {v0, v4, v1}, Lcom/google/gson/internal/bind/util/ISO8601Utils;->checkOffset(Ljava/lang/String;IC)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 137
    add-int/lit8 v4, v4, 0x1

    .line 141
    :cond_2f
    move v0, v4

    add-int/lit8 v4, v4, 0x2

    move-object/from16 v1, p0

    invoke-static {v1, v0, v4}, Lcom/google/gson/internal/bind/util/ISO8601Utils;->parseInt(Ljava/lang/String;II)I

    move-result v7

    .line 143
    .local v7, "day":I
    const/4 v8, 0x0

    .line 144
    .local v8, "hour":I
    const/4 v9, 0x0

    .line 145
    .local v9, "minutes":I
    const/4 v10, 0x0

    .line 146
    .local v10, "seconds":I
    const/4 v11, 0x0

    .line 149
    .local v11, "milliseconds":I
    move-object/from16 v0, p0

    const/16 v1, 0x54

    invoke-static {v0, v4, v1}, Lcom/google/gson/internal/bind/util/ISO8601Utils;->checkOffset(Ljava/lang/String;IC)Z

    move-result v12

    .line 151
    .local v12, "hasT":Z
    if-nez v12, :cond_5d

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v0

    if-gt v0, v4, :cond_5d

    .line 152
    new-instance v13, Ljava/util/GregorianCalendar;

    add-int/lit8 v0, v6, -0x1

    invoke-direct {v13, v5, v0, v7}, Ljava/util/GregorianCalendar;-><init>(III)V

    .line 154
    .local v13, "calendar":Ljava/util/Calendar;
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 155
    invoke-virtual {v13}, Ljava/util/Calendar;->getTime()Ljava/util/Date;
    :try_end_5b
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_5b} :catch_1fc
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_5b} :catch_1ff
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_5b} :catch_202

    move-result-object v0

    return-object v0

    .line 158
    .end local v13    # "calendar":Ljava/util/Calendar;
    :cond_5d
    if-eqz v12, :cond_e5

    .line 161
    add-int/lit8 v4, v4, 0x1

    move v0, v4

    add-int/lit8 v4, v4, 0x2

    move-object/from16 v1, p0

    :try_start_66
    invoke-static {v1, v0, v4}, Lcom/google/gson/internal/bind/util/ISO8601Utils;->parseInt(Ljava/lang/String;II)I

    move-result v8

    .line 162
    move-object/from16 v0, p0

    const/16 v1, 0x3a

    invoke-static {v0, v4, v1}, Lcom/google/gson/internal/bind/util/ISO8601Utils;->checkOffset(Ljava/lang/String;IC)Z

    move-result v0

    if-eqz v0, :cond_76

    .line 163
    add-int/lit8 v4, v4, 0x1

    .line 166
    :cond_76
    move v0, v4

    add-int/lit8 v4, v4, 0x2

    move-object/from16 v1, p0

    invoke-static {v1, v0, v4}, Lcom/google/gson/internal/bind/util/ISO8601Utils;->parseInt(Ljava/lang/String;II)I

    move-result v9

    .line 167
    move-object/from16 v0, p0

    const/16 v1, 0x3a

    invoke-static {v0, v4, v1}, Lcom/google/gson/internal/bind/util/ISO8601Utils;->checkOffset(Ljava/lang/String;IC)Z

    move-result v0

    if-eqz v0, :cond_8b

    .line 168
    add-int/lit8 v4, v4, 0x1

    .line 171
    :cond_8b
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v4, :cond_e5

    .line 172
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v13

    .line 173
    .local v13, "c":C
    const/16 v0, 0x5a

    if-eq v13, v0, :cond_e5

    const/16 v0, 0x2b

    if-eq v13, v0, :cond_e5

    const/16 v0, 0x2d

    if-eq v13, v0, :cond_e5

    .line 174
    move v0, v4

    add-int/lit8 v4, v4, 0x2

    move-object/from16 v1, p0

    invoke-static {v1, v0, v4}, Lcom/google/gson/internal/bind/util/ISO8601Utils;->parseInt(Ljava/lang/String;II)I

    move-result v10

    .line 175
    const/16 v0, 0x3b

    if-le v10, v0, :cond_b6

    const/16 v0, 0x3f

    if-ge v10, v0, :cond_b6

    const/16 v10, 0x3b

    .line 177
    :cond_b6
    move-object/from16 v0, p0

    const/16 v1, 0x2e

    invoke-static {v0, v4, v1}, Lcom/google/gson/internal/bind/util/ISO8601Utils;->checkOffset(Ljava/lang/String;IC)Z

    move-result v0

    if-eqz v0, :cond_e5

    .line 178
    add-int/lit8 v4, v4, 0x1

    .line 179
    add-int/lit8 v0, v4, 0x1

    move-object/from16 v1, p0

    invoke-static {v1, v0}, Lcom/google/gson/internal/bind/util/ISO8601Utils;->indexOfNonDigit(Ljava/lang/String;I)I

    move-result v14

    .line 180
    .local v14, "endOffset":I
    add-int/lit8 v0, v4, 0x3

    invoke-static {v14, v0}, Ljava/lang/Math;->min(II)I

    move-result v15

    .line 181
    .local v15, "parseEndOffset":I
    move-object/from16 v0, p0

    invoke-static {v0, v4, v15}, Lcom/google/gson/internal/bind/util/ISO8601Utils;->parseInt(Ljava/lang/String;II)I

    move-result v16

    .line 183
    .local v16, "fraction":I
    sub-int v0, v15, v4

    sparse-switch v0, :sswitch_data_27a

    goto :goto_e2

    .line 185
    :sswitch_dc
    mul-int/lit8 v11, v16, 0xa

    .line 186
    goto :goto_e4

    .line 188
    :sswitch_df
    mul-int/lit8 v11, v16, 0x64

    .line 189
    goto :goto_e4

    .line 191
    :goto_e2
    move/from16 v11, v16

    .line 193
    :goto_e4
    move v4, v14

    .line 200
    .end local v13    # "c":C
    .end local v14    # "endOffset":I
    .end local v15    # "parseEndOffset":I
    .end local v16    # "fraction":I
    :cond_e5
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v0

    if-gt v0, v4, :cond_f3

    .line 201
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No time zone indicator"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 204
    :cond_f3
    const/4 v13, 0x0

    .line 205
    .local v13, "timezone":Ljava/util/TimeZone;
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v14

    .line 207
    .local v14, "timezoneIndicator":C
    const/16 v0, 0x5a

    if-ne v14, v0, :cond_104

    .line 208
    sget-object v13, Lcom/google/gson/internal/bind/util/ISO8601Utils;->TIMEZONE_UTC:Ljava/util/TimeZone;

    .line 209
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1c7

    .line 210
    :cond_104
    const/16 v0, 0x2b

    if-eq v14, v0, :cond_10c

    const/16 v0, 0x2d

    if-ne v14, v0, :cond_1a8

    .line 211
    :cond_10c
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v15

    .line 214
    .local v15, "timezoneOffset":Ljava/lang/String;
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x5

    if-lt v0, v1, :cond_11a

    goto :goto_12d

    :cond_11a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "00"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 216
    :goto_12d
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v4, v0

    .line 218
    const-string v0, "+0000"

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_142

    const-string v0, "+00:00"

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_145

    .line 219
    :cond_142
    sget-object v13, Lcom/google/gson/internal/bind/util/ISO8601Utils;->TIMEZONE_UTC:Ljava/util/TimeZone;

    goto :goto_1a7

    .line 225
    :cond_145
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GMT"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 228
    .local v16, "timezoneId":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v13

    .line 230
    invoke-virtual {v13}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v17

    .line 231
    .local v17, "act":Ljava/lang/String;
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a7

    .line 237
    const-string v0, ":"

    const-string v1, ""

    move-object/from16 v2, v17

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v18

    .line 238
    .local v18, "cleaned":Ljava/lang/String;
    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a7

    .line 239
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Mismatching time zone indicator: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v2, v16

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " given, resolves to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 240
    invoke-virtual {v13}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 244
    .end local v15    # "timezoneOffset":Ljava/lang/String;
    .end local v16    # "timezoneId":Ljava/lang/String;
    .end local v17    # "act":Ljava/lang/String;
    .end local v18    # "cleaned":Ljava/lang/String;
    :cond_1a7
    :goto_1a7
    goto :goto_1c7

    .line 245
    :cond_1a8
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid time zone indicator \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 248
    :goto_1c7
    new-instance v15, Ljava/util/GregorianCalendar;

    invoke-direct {v15, v13}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 249
    .local v15, "calendar":Ljava/util/Calendar;
    const/4 v0, 0x0

    invoke-virtual {v15, v0}, Ljava/util/Calendar;->setLenient(Z)V

    .line 250
    const/4 v0, 0x1

    invoke-virtual {v15, v0, v5}, Ljava/util/Calendar;->set(II)V

    .line 251
    add-int/lit8 v0, v6, -0x1

    const/4 v1, 0x2

    invoke-virtual {v15, v1, v0}, Ljava/util/Calendar;->set(II)V

    .line 252
    const/4 v0, 0x5

    invoke-virtual {v15, v0, v7}, Ljava/util/Calendar;->set(II)V

    .line 253
    const/16 v0, 0xb

    invoke-virtual {v15, v0, v8}, Ljava/util/Calendar;->set(II)V

    .line 254
    const/16 v0, 0xc

    invoke-virtual {v15, v0, v9}, Ljava/util/Calendar;->set(II)V

    .line 255
    const/16 v0, 0xd

    invoke-virtual {v15, v0, v10}, Ljava/util/Calendar;->set(II)V

    .line 256
    const/16 v0, 0xe

    invoke-virtual {v15, v0, v11}, Ljava/util/Calendar;->set(II)V

    .line 258
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 259
    invoke-virtual {v15}, Ljava/util/Calendar;->getTime()Ljava/util/Date;
    :try_end_1fa
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_66 .. :try_end_1fa} :catch_1fc
    .catch Ljava/lang/NumberFormatException; {:try_start_66 .. :try_end_1fa} :catch_1ff
    .catch Ljava/lang/IllegalArgumentException; {:try_start_66 .. :try_end_1fa} :catch_202

    move-result-object v0

    return-object v0

    .line 262
    .end local v4    # "offset":I
    .end local v5    # "year":I
    .end local v6    # "month":I
    .end local v7    # "day":I
    .end local v8    # "hour":I
    .end local v9    # "minutes":I
    .end local v10    # "seconds":I
    .end local v11    # "milliseconds":I
    .end local v12    # "hasT":Z
    .end local v13    # "timezone":Ljava/util/TimeZone;
    .end local v14    # "timezoneIndicator":C
    .end local v15    # "calendar":Ljava/util/Calendar;
    :catch_1fc
    move-exception v4

    .line 263
    .local v4, "e":Ljava/lang/IndexOutOfBoundsException;
    move-object v3, v4

    .line 268
    .end local v4    # "e":Ljava/lang/IndexOutOfBoundsException;
    goto :goto_204

    .line 264
    :catch_1ff
    move-exception v4

    .line 265
    .local v4, "e":Ljava/lang/NumberFormatException;
    move-object v3, v4

    .line 268
    .end local v4    # "e":Ljava/lang/NumberFormatException;
    goto :goto_204

    .line 266
    :catch_202
    move-exception v4

    .line 267
    .local v4, "e":Ljava/lang/IllegalArgumentException;
    move-object v3, v4

    .line 269
    .end local v4    # "e":Ljava/lang/IllegalArgumentException;
    :goto_204
    if-nez p0, :cond_208

    const/4 v4, 0x0

    goto :goto_223

    :cond_208
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 270
    .local v4, "input":Ljava/lang/String;
    :goto_223
    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    .line 271
    .local v5, "msg":Ljava/lang/String;
    if-eqz v5, :cond_22f

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_250

    .line 272
    :cond_22f
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 274
    :cond_250
    new-instance v6, Ljava/text/ParseException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Failed to parse date ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual/range {p1 .. p1}, Ljava/text/ParsePosition;->getIndex()I

    move-result v1

    invoke-direct {v6, v0, v1}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    .line 275
    .local v6, "ex":Ljava/text/ParseException;
    invoke-virtual {v6, v3}, Ljava/text/ParseException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 276
    throw v6

    :sswitch_data_27a
    .sparse-switch
        0x1 -> :sswitch_df
        0x2 -> :sswitch_dc
    .end sparse-switch
.end method

.method private static parseInt(Ljava/lang/String;II)I
    .registers 9
    .param p0, "value"    # Ljava/lang/String;
    .param p1, "beginIndex"    # I
    .param p2, "endIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    .line 301
    if-ltz p1, :cond_a

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-gt p2, v0, :cond_a

    if-le p1, p2, :cond_10

    .line 302
    :cond_a
    new-instance v0, Ljava/lang/NumberFormatException;

    invoke-direct {v0, p0}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 305
    :cond_10
    move v3, p1

    .line 306
    .local v3, "i":I
    const/4 v4, 0x0

    .line 308
    .local v4, "result":I
    if-ge v3, p2, :cond_41

    .line 309
    move v0, v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Ljava/lang/Character;->digit(CI)I

    move-result v5

    .line 310
    .local v5, "digit":I
    if-gez v5, :cond_40

    .line 311
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid number: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 313
    :cond_40
    neg-int v4, v5

    .line 315
    .end local v5    # "digit":I
    :cond_41
    :goto_41
    if-ge v3, p2, :cond_73

    .line 316
    move v0, v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0xa

    invoke-static {v0, v1}, Ljava/lang/Character;->digit(CI)I

    move-result v5

    .line 317
    .local v5, "digit":I
    if-gez v5, :cond_6f

    .line 318
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid number: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, p1, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 320
    :cond_6f
    mul-int/lit8 v4, v4, 0xa

    .line 321
    sub-int/2addr v4, v5

    goto :goto_41

    .line 323
    .end local v5    # "digit":I
    :cond_73
    neg-int v0, v4

    return v0
.end method

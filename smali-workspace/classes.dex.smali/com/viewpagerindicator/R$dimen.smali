.class public final Lcom/viewpagerindicator/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/viewpagerindicator/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final default_circle_indicator_radius:I = 0x7f0800dc

.field public static final default_circle_indicator_stroke_width:I = 0x7f0800dd

.field public static final default_line_indicator_gap_width:I = 0x7f0800e6

.field public static final default_line_indicator_line_width:I = 0x7f0800e7

.field public static final default_line_indicator_stroke_width:I = 0x7f0800e8

.field public static final default_title_indicator_clip_padding:I = 0x7f080124

.field public static final default_title_indicator_footer_indicator_height:I = 0x7f080125

.field public static final default_title_indicator_footer_indicator_underline_padding:I = 0x7f080126

.field public static final default_title_indicator_footer_line_height:I = 0x7f080127

.field public static final default_title_indicator_footer_padding:I = 0x7f080128

.field public static final default_title_indicator_text_size:I = 0x7f080129

.field public static final default_title_indicator_title_padding:I = 0x7f08012a

.field public static final default_title_indicator_top_padding:I = 0x7f08012b


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public final Lbr/com/itau/security/routercryptor/Cryptor;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[B

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .registers 9

    .line 6
    const/16 v0, 0xf

    new-array v0, v0, [B

    fill-array-data v0, :array_4e

    sput-object v0, Lbr/com/itau/security/routercryptor/Cryptor;->a:[B

    const/16 v0, 0x8e

    sput v0, Lbr/com/itau/security/routercryptor/Cryptor;->b:I

    sget-object v0, Lbr/com/itau/security/routercryptor/Cryptor;->a:[B

    const/4 v1, 0x5

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    int-to-byte v4, v0

    int-to-byte v5, v4

    int-to-byte v6, v5

    .line 1000
    sget-object v7, Lbr/com/itau/security/routercryptor/Cryptor;->a:[B

    const/4 v8, 0x0

    mul-int/lit8 v6, v6, 0x3

    add-int/lit8 v6, v6, 0xc

    new-instance v0, Ljava/lang/String;

    mul-int/lit8 v5, v5, 0x3

    rsub-int/lit8 v5, v5, 0x4

    mul-int/lit8 v4, v4, 0x2

    rsub-int/lit8 v4, v4, 0x43

    new-array v1, v6, [B

    add-int/lit8 v6, v6, -0x1

    if-nez v7, :cond_35

    move v2, v4

    move v3, v6

    :goto_30
    add-int/lit8 v5, v5, 0x1

    add-int/2addr v2, v3

    add-int/lit8 v4, v2, -0x2

    :cond_35
    int-to-byte v2, v4

    aput-byte v2, v1, v8

    move v2, v8

    add-int/lit8 v8, v8, 0x1

    if-ne v2, v6, :cond_42

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    goto :goto_46

    :cond_42
    move v2, v4

    aget-byte v3, v7, v5

    goto :goto_30

    .line 6
    :goto_46
    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 7
    return-void

    :array_4e
    .array-data 1
        0x4dt
        -0xft
        -0x70t
        0x62t
        0x2et
        0x1t
        0x8t
        -0xdt
        0xbt
        0x8t
        -0x2ft
        0x20t
        0x4t
        0x7t
        -0x1t
    .end array-data
.end method

.method constructor <init>()V
    .registers 1

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native Get(Ljava/lang/String;)Ljava/lang/String;
.end method

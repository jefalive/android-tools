.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
.super Ljava/lang/Object;
.source "Entry.java"


# instance fields
.field private mData:Ljava/lang/Object;

.field private mVal:F

.field private mXIndex:I


# direct methods
.method public constructor <init>(FI)V
    .registers 4
    .param p1, "val"    # F
    .param p2, "xIndex"    # I

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->mVal:F

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->mXIndex:I

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->mData:Ljava/lang/Object;

    .line 33
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->mVal:F

    .line 34
    iput p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->mXIndex:I

    .line 35
    return-void
.end method

.method public constructor <init>(FILjava/lang/Object;)V
    .registers 4
    .param p1, "val"    # F
    .param p2, "xIndex"    # I
    .param p3, "data"    # Ljava/lang/Object;

    .line 48
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;-><init>(FI)V

    .line 51
    iput-object p3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->mData:Ljava/lang/Object;

    .line 52
    return-void
.end method


# virtual methods
.method public copy()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    .registers 5

    .line 122
    new-instance v3, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->mVal:F

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->mXIndex:I

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->mData:Ljava/lang/Object;

    invoke-direct {v3, v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;-><init>(FILjava/lang/Object;)V

    .line 123
    .local v3, "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    return-object v3
.end method

.method public equalTo(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;)Z
    .registers 4
    .param p1, "e"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    .line 138
    if-nez p1, :cond_4

    .line 139
    const/4 v0, 0x0

    return v0

    .line 142
    :cond_4
    iget-object v0, p1, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->mData:Ljava/lang/Object;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->mData:Ljava/lang/Object;

    if-eq v0, v1, :cond_c

    .line 143
    const/4 v0, 0x0

    return v0

    .line 144
    :cond_c
    iget v0, p1, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->mXIndex:I

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->mXIndex:I

    if-eq v0, v1, :cond_14

    .line 145
    const/4 v0, 0x0

    return v0

    .line 148
    :cond_14
    iget v0, p1, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->mVal:F

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->mVal:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3727c5ac    # 1.0E-5f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_26

    .line 149
    const/4 v0, 0x0

    return v0

    .line 152
    :cond_26
    const/4 v0, 0x1

    return v0
.end method

.method public getData()Ljava/lang/Object;
    .registers 2

    .line 102
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->mData:Ljava/lang/Object;

    return-object v0
.end method

.method public getVal()F
    .registers 2

    .line 81
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->mVal:F

    return v0
.end method

.method public getXIndex()I
    .registers 2

    .line 61
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->mXIndex:I

    return v0
.end method

.method public setData(Ljava/lang/Object;)V
    .registers 2
    .param p1, "data"    # Ljava/lang/Object;

    .line 112
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->mData:Ljava/lang/Object;

    .line 113
    return-void
.end method

.method public setVal(F)V
    .registers 2
    .param p1, "val"    # F

    .line 91
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->mVal:F

    .line 92
    return-void
.end method

.method public setXIndex(I)V
    .registers 2
    .param p1, "x"    # I

    .line 71
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->mXIndex:I

    .line 72
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Entry, xIndex: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->mXIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " val (sum): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

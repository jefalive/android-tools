.class public Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;
.super Lcom/itau/empresas/ui/dialog/BaseDialog;
.source "FiltroExtratoDialog.java"


# instance fields
.field botaoFiltrarExtrato:Landroid/widget/Button;

.field private filtroClickListener:Landroid/view/View$OnClickListener;

.field flBotaoFecharFiltro:Landroid/widget/FrameLayout;

.field private ordem:I

.field quantidadeDias:I

.field rbOrdemAntigo:Landroid/widget/RadioButton;

.field rbOrdemRecente:Landroid/widget/RadioButton;

.field rgFiltroExtratoSecao1:Landroid/widget/RadioGroup;

.field rgFiltroExtratoSecao2:Landroid/widget/RadioGroup;

.field private rgListenerSecao1:Landroid/widget/RadioGroup$OnCheckedChangeListener;

.field private rgListenerSecao2:Landroid/widget/RadioGroup$OnCheckedChangeListener;

.field rgOrdemExtrato:Landroid/widget/RadioGroup;


# direct methods
.method public constructor <init>()V
    .registers 3

    .line 75
    invoke-direct {p0}, Lcom/itau/empresas/ui/dialog/BaseDialog;-><init>()V

    .line 53
    new-instance v0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog$1;-><init>(Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->rgListenerSecao1:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    .line 64
    new-instance v0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog$2;-><init>(Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->rgListenerSecao2:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    .line 76
    const v0, 0x7f090146

    const v1, 0x7f090146

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->setStyle(II)V

    .line 77
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;)Landroid/widget/RadioGroup$OnCheckedChangeListener;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;

    .line 24
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->rgListenerSecao2:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;I)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;
    .param p1, "x1"    # I

    .line 24
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->setQuantidadeDias(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;)Landroid/widget/RadioGroup$OnCheckedChangeListener;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;

    .line 24
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->rgListenerSecao1:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;)I
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;

    .line 24
    iget v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->ordem:I

    return v0
.end method

.method static synthetic access$302(Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;I)I
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;
    .param p1, "x1"    # I

    .line 24
    iput p1, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->ordem:I

    return p1
.end method

.method static synthetic access$400(Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;)Lcom/itau/empresas/CustomApplication;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;

    .line 24
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->application:Lcom/itau/empresas/CustomApplication;

    return-object v0
.end method

.method static synthetic access$500(Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;)Lcom/itau/empresas/CustomApplication;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;

    .line 24
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->application:Lcom/itau/empresas/CustomApplication;

    return-object v0
.end method

.method private configurarListenerOrdemExtrato()V
    .registers 3

    .line 115
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->rgOrdemExtrato:Landroid/widget/RadioGroup;

    new-instance v1, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog$3;-><init>(Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 133
    return-void
.end method

.method private disparaEventoAnalytics()V
    .registers 5

    .line 190
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v3

    .line 191
    .line 192
    .local v3, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    const v0, 0x7f0700e0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 193
    const v1, 0x7f0700c8

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 194
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 191
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    return-void
.end method

.method private inicializarRadioDias()V
    .registers 3

    .line 136
    iget v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->quantidadeDias:I

    sparse-switch v0, :sswitch_data_4e

    goto/16 :goto_4c

    .line 138
    :sswitch_7
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->rgFiltroExtratoSecao1:Landroid/widget/RadioGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 139
    goto :goto_4c

    .line 141
    :sswitch_15
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->rgFiltroExtratoSecao1:Landroid/widget/RadioGroup;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 142
    goto :goto_4c

    .line 144
    :sswitch_23
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->rgFiltroExtratoSecao2:Landroid/widget/RadioGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 145
    goto :goto_4c

    .line 147
    :sswitch_31
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->rgFiltroExtratoSecao2:Landroid/widget/RadioGroup;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 148
    goto :goto_4c

    .line 150
    :sswitch_3f
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->rgFiltroExtratoSecao2:Landroid/widget/RadioGroup;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 151
    .line 155
    :goto_4c
    return-void

    nop

    :sswitch_data_4e
    .sparse-switch
        0x7 -> :sswitch_7
        0xf -> :sswitch_15
        0x1e -> :sswitch_23
        0x3c -> :sswitch_31
        0x5a -> :sswitch_3f
    .end sparse-switch
.end method

.method private inicializarRadioOrdenacao()V
    .registers 5

    .line 158
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->application:Lcom/itau/empresas/CustomApplication;

    .line 159
    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_ORDEMEXTRATO"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 161
    .local v3, "ordemRegistrada":I
    const/4 v0, 0x2

    if-ne v3, v0, :cond_2d

    .line 162
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->rbOrdemAntigo:Landroid/widget/RadioButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 164
    :cond_2d
    return-void
.end method

.method private setQuantidadeDias(I)V
    .registers 3
    .param p1, "checkedId"    # I

    .line 80
    packed-switch p1, :pswitch_data_1c

    goto :goto_1b

    .line 82
    :pswitch_4
    const/4 v0, 0x7

    iput v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->quantidadeDias:I

    .line 83
    goto :goto_1b

    .line 85
    :pswitch_8
    const/16 v0, 0xf

    iput v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->quantidadeDias:I

    .line 86
    goto :goto_1b

    .line 88
    :pswitch_d
    const/16 v0, 0x1e

    iput v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->quantidadeDias:I

    .line 89
    goto :goto_1b

    .line 91
    :pswitch_12
    const/16 v0, 0x3c

    iput v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->quantidadeDias:I

    .line 92
    goto :goto_1b

    .line 94
    :pswitch_17
    const/16 v0, 0x5a

    iput v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->quantidadeDias:I

    .line 95
    .line 99
    :goto_1b
    :pswitch_1b
    return-void

    :pswitch_data_1c
    .packed-switch 0x7f0e0416
        :pswitch_4
        :pswitch_8
        :pswitch_1b
        :pswitch_d
        :pswitch_12
        :pswitch_17
    .end packed-switch
.end method


# virtual methods
.method afterViews()V
    .registers 3

    .line 103
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->rgFiltroExtratoSecao1:Landroid/widget/RadioGroup;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->rgListenerSecao1:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 104
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->rgFiltroExtratoSecao2:Landroid/widget/RadioGroup;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->rgListenerSecao2:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 106
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->inicializarRadioDias()V

    .line 107
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->inicializarRadioOrdenacao()V

    .line 109
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->configurarListenerOrdemExtrato()V

    .line 111
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->disparaEventoAnalytics()V

    .line 112
    return-void
.end method

.method botaoFecharFiltro()V
    .registers 1

    .line 168
    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->dismiss()V

    .line 169
    return-void
.end method

.method botaoFiltrar(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 173
    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->dismiss()V

    .line 174
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->filtroClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_c

    .line 175
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->filtroClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 177
    :cond_c
    return-void
.end method

.method public getQuantidadeDias()I
    .registers 2

    .line 186
    iget v0, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->quantidadeDias:I

    return v0
.end method

.method public setFiltroClickListener(Landroid/view/View$OnClickListener;)Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;
    .registers 2
    .param p1, "filtroClickListener"    # Landroid/view/View$OnClickListener;

    .line 181
    iput-object p1, p0, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->filtroClickListener:Landroid/view/View$OnClickListener;

    .line 182
    return-object p0
.end method

.class public Lcom/itau/empresas/api/model/DadosPagamentoVO;
.super Ljava/lang/Object;
.source "DadosPagamentoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private formaPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "forma_pagamento"
    .end annotation
.end field

.field private indicadorAgendamento:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_antecipacao_agendamento"
    .end annotation
.end field

.field private opcaoPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "opcao_pagamento"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const-string v0, "DebitoConta"

    iput-object v0, p0, Lcom/itau/empresas/api/model/DadosPagamentoVO;->opcaoPagamento:Ljava/lang/String;

    .line 13
    const-string v0, "BoletoSimples"

    iput-object v0, p0, Lcom/itau/empresas/api/model/DadosPagamentoVO;->formaPagamento:Ljava/lang/String;

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/api/model/DadosPagamentoVO;->indicadorAgendamento:Z

    return-void
.end method

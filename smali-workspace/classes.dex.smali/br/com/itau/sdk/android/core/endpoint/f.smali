.class public final Lbr/com/itau/sdk/android/core/endpoint/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareCaller;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 10

    .line 13
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    const/4 v4, -0x1

    const/4 v5, -0x1

    invoke-virtual/range {v0 .. v5}, Lbr/com/itau/sdk/android/core/endpoint/f;->execute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public execute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Ljava/lang/String;
    .registers 18

    .line 20
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getUserAgent()Ljava/lang/String;

    move-result-object v9

    .line 21
    invoke-static {}, Lbr/com/itau/sdk/android/core/o;->a()Lbr/com/itau/sdk/android/core/o;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/o;->g()Ljava/lang/String;

    move-result-object v10

    .line 23
    if-ltz p4, :cond_10

    if-gez p5, :cond_1c

    .line 24
    :cond_10
    new-instance v0, Lbr/com/itau/security/middlewarechannel/RequestProperties;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, v9

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/security/middlewarechannel/RequestProperties;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v8, v0

    goto :goto_2b

    .line 30
    :cond_1c
    new-instance v0, Lbr/com/itau/security/middlewarechannel/RequestProperties;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, v9

    move-object v5, v10

    move/from16 v6, p4

    move/from16 v7, p5

    invoke-direct/range {v0 .. v7}, Lbr/com/itau/security/middlewarechannel/RequestProperties;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    move-object v8, v0

    .line 37
    :goto_2b
    new-instance v11, Lbr/com/itau/security/middlewarechannel/CryptoHandler;

    invoke-direct {v11, v8}, Lbr/com/itau/security/middlewarechannel/CryptoHandler;-><init>(Lbr/com/itau/security/middlewarechannel/RequestProperties;)V

    .line 38
    invoke-virtual {v11}, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->getData()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public final Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;
.super Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity;
.source "AtributosProdutoMultilimiteActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 37
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity;-><init>()V

    .line 41
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;

    .line 37
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;

    .line 37
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 55
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 56
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 57
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 58
    invoke-static {p0}, Lcom/itau/empresas/adapter/DetalhesMultilimiteAdapter_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/adapter/DetalhesMultilimiteAdapter_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;->adapter:Lcom/itau/empresas/adapter/DetalhesMultilimiteAdapter;

    .line 59
    invoke-static {p0}, Lcom/itau/empresas/controller/ProdutosController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/controller/ProdutosController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;->produtosController:Lcom/itau/empresas/controller/ProdutosController;

    .line 60
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;->injectExtras_()V

    .line 61
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;->afterInject()V

    .line 62
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 103
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 104
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_2c

    .line 105
    const-string v0, "titulo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 106
    const-string v0, "titulo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;->titulo:Ljava/lang/String;

    .line 108
    :cond_1a
    const-string v0, "atributoVOs"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 109
    const-string v0, "atributoVOs"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;->atributoVOs:Ljava/util/ArrayList;

    .line 112
    :cond_2c
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 137
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_$2;-><init>(Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 145
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 125
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_$1;-><init>(Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 133
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 48
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;->init_(Landroid/os/Bundle;)V

    .line 49
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 51
    const v0, 0x7f03004e

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;->setContentView(I)V

    .line 52
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 96
    const v0, 0x7f0e0258

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;->lista:Landroid/widget/ListView;

    .line 97
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 98
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;->aoIniciarActivity()V

    .line 99
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 66
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity;->setContentView(I)V

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 68
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 78
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity;->setContentView(Landroid/view/View;)V

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 80
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 72
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 74
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 116
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity;->setIntent(Landroid/content/Intent;)V

    .line 117
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_;->injectExtras_()V

    .line 118
    return-void
.end method

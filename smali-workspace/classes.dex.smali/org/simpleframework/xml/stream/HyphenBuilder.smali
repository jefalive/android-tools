.class Lorg/simpleframework/xml/stream/HyphenBuilder;
.super Ljava/lang/Object;
.source "HyphenBuilder.java"

# interfaces
.implements Lorg/simpleframework/xml/stream/Style;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/simpleframework/xml/stream/HyphenBuilder$1;,
        Lorg/simpleframework/xml/stream/HyphenBuilder$Parser;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    return-void
.end method


# virtual methods
.method public getAttribute(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .param p1, "name"    # Ljava/lang/String;

    .line 55
    if-eqz p1, :cond_d

    .line 56
    new-instance v0, Lorg/simpleframework/xml/stream/HyphenBuilder$Parser;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lorg/simpleframework/xml/stream/HyphenBuilder$Parser;-><init>(Lorg/simpleframework/xml/stream/HyphenBuilder;Ljava/lang/String;Lorg/simpleframework/xml/stream/HyphenBuilder$1;)V

    invoke-virtual {v0}, Lorg/simpleframework/xml/stream/HyphenBuilder$Parser;->process()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 58
    :cond_d
    const/4 v0, 0x0

    return-object v0
.end method

.method public getElement(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .param p1, "name"    # Ljava/lang/String;

    .line 72
    if-eqz p1, :cond_d

    .line 73
    new-instance v0, Lorg/simpleframework/xml/stream/HyphenBuilder$Parser;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lorg/simpleframework/xml/stream/HyphenBuilder$Parser;-><init>(Lorg/simpleframework/xml/stream/HyphenBuilder;Ljava/lang/String;Lorg/simpleframework/xml/stream/HyphenBuilder$1;)V

    invoke-virtual {v0}, Lorg/simpleframework/xml/stream/HyphenBuilder$Parser;->process()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 75
    :cond_d
    const/4 v0, 0x0

    return-object v0
.end method

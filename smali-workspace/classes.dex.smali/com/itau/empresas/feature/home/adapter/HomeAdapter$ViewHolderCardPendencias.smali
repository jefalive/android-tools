.class Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardPendencias;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "HomeAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/home/adapter/HomeAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ViewHolderCardPendencias"
.end annotation


# instance fields
.field final parentView:Lcom/itau/empresas/ui/view/CardAlertaView;


# direct methods
.method private constructor <init>(Lcom/itau/empresas/ui/view/CardAlertaView;)V
    .registers 3
    .param p1, "itemView"    # Lcom/itau/empresas/ui/view/CardAlertaView;

    .line 66
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 67
    iput-object p1, p0, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardPendencias;->parentView:Lcom/itau/empresas/ui/view/CardAlertaView;

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardPendencias;->parentView:Lcom/itau/empresas/ui/view/CardAlertaView;

    invoke-virtual {v0, p0}, Lcom/itau/empresas/ui/view/CardAlertaView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    return-void
.end method

.method synthetic constructor <init>(Lcom/itau/empresas/ui/view/CardAlertaView;Lcom/itau/empresas/feature/home/adapter/HomeAdapter$1;)V
    .registers 3
    .param p1, "x0"    # Lcom/itau/empresas/ui/view/CardAlertaView;
    .param p2, "x1"    # Lcom/itau/empresas/feature/home/adapter/HomeAdapter$1;

    .line 61
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardPendencias;-><init>(Lcom/itau/empresas/ui/view/CardAlertaView;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 73
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 74
    return-void
.end method

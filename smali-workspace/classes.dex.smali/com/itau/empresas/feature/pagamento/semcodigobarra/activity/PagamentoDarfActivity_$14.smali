.class Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$14;
.super Ljava/lang/Object;
.source "PagamentoDarfActivity_.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;

    .line 276
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$14;->this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 3
    .param p1, "s"    # Landroid/text/Editable;

    .line 288
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$14;->this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->atualizaValorTotalComJuros(Landroid/text/Editable;)V

    .line 289
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$14;->this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->fechaTooltip()V

    .line 290
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .line 280
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .line 284
    return-void
.end method

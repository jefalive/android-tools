.class public final Lcom/google/zxing/client/android/i;
.super Ljava/lang/Object;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/app/Activity;

.field private final c:Landroid/content/BroadcastReceiver;

.field private d:Z

.field private e:Landroid/os/AsyncTask;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-class v0, Lcom/google/zxing/client/android/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/zxing/client/android/i;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .registers 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/zxing/client/android/i;->b:Landroid/app/Activity;

    new-instance v0, Lcom/google/zxing/client/android/l;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/zxing/client/android/l;-><init>(Lcom/google/zxing/client/android/i;Lcom/google/zxing/client/android/j;)V

    iput-object v0, p0, Lcom/google/zxing/client/android/i;->c:Landroid/content/BroadcastReceiver;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/zxing/client/android/i;->d:Z

    invoke-virtual {p0}, Lcom/google/zxing/client/android/i;->a()V

    return-void
.end method

.method static synthetic a(Lcom/google/zxing/client/android/i;)V
    .registers 1

    invoke-direct {p0}, Lcom/google/zxing/client/android/i;->f()V

    return-void
.end method

.method static synthetic b(Lcom/google/zxing/client/android/i;)Landroid/app/Activity;
    .registers 2

    iget-object v0, p0, Lcom/google/zxing/client/android/i;->b:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic e()Ljava/lang/String;
    .registers 1

    sget-object v0, Lcom/google/zxing/client/android/i;->a:Ljava/lang/String;

    return-object v0
.end method

.method private declared-synchronized f()V
    .registers 3

    monitor-enter p0

    :try_start_1
    iget-object v1, p0, Lcom/google/zxing/client/android/i;->e:Landroid/os/AsyncTask;

    if-eqz v1, :cond_c

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/os/AsyncTask;->cancel(Z)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/zxing/client/android/i;->e:Landroid/os/AsyncTask;
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_e

    :cond_c
    monitor-exit p0

    return-void

    :catchall_e
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public declared-synchronized a()V
    .registers 5

    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, Lcom/google/zxing/client/android/i;->f()V

    new-instance v0, Lcom/google/zxing/client/android/k;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/zxing/client/android/k;-><init>(Lcom/google/zxing/client/android/i;Lcom/google/zxing/client/android/j;)V

    iput-object v0, p0, Lcom/google/zxing/client/android/i;->e:Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/google/zxing/client/android/i;->e:Landroid/os/AsyncTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_18

    monitor-exit p0

    return-void

    :catchall_18
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized b()V
    .registers 4

    monitor-enter p0

    :try_start_1
    invoke-direct {p0}, Lcom/google/zxing/client/android/i;->f()V

    iget-boolean v0, p0, Lcom/google/zxing/client/android/i;->d:Z

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/zxing/client/android/i;->b:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/zxing/client/android/i;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/zxing/client/android/i;->d:Z

    goto :goto_1a

    :cond_13
    sget-object v0, Lcom/google/zxing/client/android/i;->a:Ljava/lang/String;

    const-string v1, "PowerStatusReceiver was never registered?"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1a
    .catchall {:try_start_1 .. :try_end_1a} :catchall_1c

    :goto_1a
    monitor-exit p0

    return-void

    :catchall_1c
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized c()V
    .registers 6

    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/zxing/client/android/i;->d:Z

    if-eqz v0, :cond_d

    sget-object v0, Lcom/google/zxing/client/android/i;->a:Ljava/lang/String;

    const-string v1, "PowerStatusReceiver was already registered?"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1e

    :cond_d
    iget-object v0, p0, Lcom/google/zxing/client/android/i;->b:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/zxing/client/android/i;->c:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/zxing/client/android/i;->d:Z

    :goto_1e
    invoke-virtual {p0}, Lcom/google/zxing/client/android/i;->a()V
    :try_end_21
    .catchall {:try_start_1 .. :try_end_21} :catchall_23

    monitor-exit p0

    return-void

    :catchall_23
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public d()V
    .registers 1

    invoke-direct {p0}, Lcom/google/zxing/client/android/i;->f()V

    return-void
.end method

.class public Lcom/viewpagerindicator/CirclePageIndicator;
.super Landroid/view/View;
.source "CirclePageIndicator.java"

# interfaces
.implements Lcom/viewpagerindicator/PageIndicator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/viewpagerindicator/CirclePageIndicator$1;,
        Lcom/viewpagerindicator/CirclePageIndicator$SavedState;
    }
.end annotation


# instance fields
.field private mActivePointerId:I

.field private mCentered:Z

.field private mCurrentPage:I

.field private mIsDragging:Z

.field private mLastMotionX:F

.field private mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

.field private mOrientation:I

.field private mPageOffset:F

.field private final mPaintFill:Landroid/graphics/Paint;

.field private final mPaintPageFill:Landroid/graphics/Paint;

.field private final mPaintStroke:Landroid/graphics/Paint;

.field private mRadius:F

.field private mScrollState:I

.field private mSnap:Z

.field private mSnapPage:I

.field private mTouchSlop:I

.field private mViewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 68
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/viewpagerindicator/CirclePageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 72
    sget v0, Lcom/viewpagerindicator/R$attr;->vpiCirclePageIndicatorStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/viewpagerindicator/CirclePageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 21
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .line 76
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    invoke-direct {v0, v1, v2, v3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/viewpagerindicator/CirclePageIndicator;->mPaintPageFill:Landroid/graphics/Paint;

    .line 49
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/viewpagerindicator/CirclePageIndicator;->mPaintStroke:Landroid/graphics/Paint;

    .line 50
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/viewpagerindicator/CirclePageIndicator;->mPaintFill:Landroid/graphics/Paint;

    .line 62
    const/high16 v0, -0x40800000    # -1.0f

    move-object/from16 v1, p0

    iput v0, v1, Lcom/viewpagerindicator/CirclePageIndicator;->mLastMotionX:F

    .line 63
    const/4 v0, -0x1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/viewpagerindicator/CirclePageIndicator;->mActivePointerId:I

    .line 77
    invoke-virtual/range {p0 .. p0}, Lcom/viewpagerindicator/CirclePageIndicator;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_3b

    return-void

    .line 80
    :cond_3b
    invoke-virtual/range {p0 .. p0}, Lcom/viewpagerindicator/CirclePageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 81
    .local v5, "res":Landroid/content/res/Resources;
    sget v0, Lcom/viewpagerindicator/R$color;->default_circle_indicator_page_color:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 82
    .local v6, "defaultPageColor":I
    sget v0, Lcom/viewpagerindicator/R$color;->default_circle_indicator_fill_color:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    .line 83
    .local v7, "defaultFillColor":I
    sget v0, Lcom/viewpagerindicator/R$integer;->default_circle_indicator_orientation:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v8

    .line 84
    .local v8, "defaultOrientation":I
    sget v0, Lcom/viewpagerindicator/R$color;->default_circle_indicator_stroke_color:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 85
    .local v9, "defaultStrokeColor":I
    sget v0, Lcom/viewpagerindicator/R$dimen;->default_circle_indicator_stroke_width:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    .line 86
    .local v10, "defaultStrokeWidth":F
    sget v0, Lcom/viewpagerindicator/R$dimen;->default_circle_indicator_radius:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v11

    .line 87
    .local v11, "defaultRadius":F
    sget v0, Lcom/viewpagerindicator/R$bool;->default_circle_indicator_centered:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v12

    .line 88
    .local v12, "defaultCentered":Z
    sget v0, Lcom/viewpagerindicator/R$bool;->default_circle_indicator_snap:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v13

    .line 91
    .local v13, "defaultSnap":Z
    sget-object v0, Lcom/viewpagerindicator/R$styleable;->CirclePageIndicator:[I

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v14

    .line 93
    .local v14, "a":Landroid/content/res/TypedArray;
    const/4 v0, 0x2

    invoke-virtual {v14, v0, v12}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/viewpagerindicator/CirclePageIndicator;->mCentered:Z

    .line 94
    const/4 v0, 0x0

    invoke-virtual {v14, v0, v8}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lcom/viewpagerindicator/CirclePageIndicator;->mOrientation:I

    .line 95
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mPaintPageFill:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 96
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mPaintPageFill:Landroid/graphics/Paint;

    const/4 v1, 0x5

    invoke-virtual {v14, v1, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 97
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mPaintStroke:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 98
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mPaintStroke:Landroid/graphics/Paint;

    const/16 v1, 0x8

    invoke-virtual {v14, v1, v9}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 99
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mPaintStroke:Landroid/graphics/Paint;

    const/4 v1, 0x3

    invoke-virtual {v14, v1, v10}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 100
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mPaintFill:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 101
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mPaintFill:Landroid/graphics/Paint;

    const/4 v1, 0x4

    invoke-virtual {v14, v1, v7}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 102
    const/4 v0, 0x6

    invoke-virtual {v14, v0, v11}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lcom/viewpagerindicator/CirclePageIndicator;->mRadius:F

    .line 103
    const/4 v0, 0x7

    invoke-virtual {v14, v0, v13}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/viewpagerindicator/CirclePageIndicator;->mSnap:Z

    .line 105
    const/4 v0, 0x1

    invoke-virtual {v14, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v15

    .line 106
    .local v15, "background":Landroid/graphics/drawable/Drawable;
    if-eqz v15, :cond_f8

    .line 107
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/viewpagerindicator/CirclePageIndicator;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 110
    :cond_f8
    invoke-virtual {v14}, Landroid/content/res/TypedArray;->recycle()V

    .line 112
    invoke-static/range {p1 .. p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v16

    .line 113
    .local v16, "configuration":Landroid/view/ViewConfiguration;
    invoke-static/range {v16 .. v16}, Landroid/support/v4/view/ViewConfigurationCompat;->getScaledPagingTouchSlop(Landroid/view/ViewConfiguration;)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lcom/viewpagerindicator/CirclePageIndicator;->mTouchSlop:I

    .line 114
    return-void
.end method

.method private measureLong(I)I
    .registers 9
    .param p1, "measureSpec"    # I

    .line 462
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 463
    .local v4, "specMode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 465
    .local v5, "specSize":I
    const/high16 v0, 0x40000000    # 2.0f

    if-eq v4, v0, :cond_10

    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_12

    .line 467
    :cond_10
    move v3, v5

    .local v3, "result":I
    goto :goto_40

    .line 470
    .end local v3    # "result":I
    :cond_12
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v6

    .line 471
    .local v6, "count":I
    invoke-virtual {p0}, Lcom/viewpagerindicator/CirclePageIndicator;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/viewpagerindicator/CirclePageIndicator;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    int-to-float v0, v0

    mul-int/lit8 v1, v6, 0x2

    int-to-float v1, v1

    iget v2, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mRadius:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    add-int/lit8 v1, v6, -0x1

    int-to-float v1, v1

    iget v2, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mRadius:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v0, v1

    float-to-int v3, v0

    .line 474
    .local v3, "result":I
    const/high16 v0, -0x80000000

    if-ne v4, v0, :cond_40

    .line 475
    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 478
    .end local v6    # "count":I
    :cond_40
    :goto_40
    return v3
.end method

.method private measureShort(I)I
    .registers 7
    .param p1, "measureSpec"    # I

    .line 490
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 491
    .local v3, "specMode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 493
    .local v4, "specSize":I
    const/high16 v0, 0x40000000    # 2.0f

    if-ne v3, v0, :cond_e

    .line 495
    move v2, v4

    .local v2, "result":I
    goto :goto_2b

    .line 498
    .end local v2    # "result":I
    :cond_e
    iget v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mRadius:F

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/viewpagerindicator/CirclePageIndicator;->getPaddingTop()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/viewpagerindicator/CirclePageIndicator;->getPaddingBottom()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v0, v1

    float-to-int v2, v0

    .line 500
    .local v2, "result":I
    const/high16 v0, -0x80000000

    if-ne v3, v0, :cond_2b

    .line 501
    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 504
    :cond_2b
    :goto_2b
    return v2
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 18
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 199
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-super {v0, v1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 201
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_e

    .line 202
    return-void

    .line 204
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v3

    .line 205
    .local v3, "count":I
    if-nez v3, :cond_1d

    .line 206
    return-void

    .line 209
    :cond_1d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mCurrentPage:I

    if-lt v0, v3, :cond_2b

    .line 210
    add-int/lit8 v0, v3, -0x1

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lcom/viewpagerindicator/CirclePageIndicator;->setCurrentItem(I)V

    .line 211
    return-void

    .line 218
    :cond_2b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mOrientation:I

    if-nez v0, :cond_42

    .line 219
    invoke-virtual/range {p0 .. p0}, Lcom/viewpagerindicator/CirclePageIndicator;->getWidth()I

    move-result v4

    .line 220
    .local v4, "longSize":I
    invoke-virtual/range {p0 .. p0}, Lcom/viewpagerindicator/CirclePageIndicator;->getPaddingLeft()I

    move-result v5

    .line 221
    .local v5, "longPaddingBefore":I
    invoke-virtual/range {p0 .. p0}, Lcom/viewpagerindicator/CirclePageIndicator;->getPaddingRight()I

    move-result v6

    .line 222
    .local v6, "longPaddingAfter":I
    invoke-virtual/range {p0 .. p0}, Lcom/viewpagerindicator/CirclePageIndicator;->getPaddingTop()I

    move-result v7

    .local v7, "shortPaddingBefore":I
    goto :goto_52

    .line 224
    .end local v4    # "longSize":I
    .end local v5    # "longPaddingBefore":I
    .end local v6    # "longPaddingAfter":I
    .end local v7    # "shortPaddingBefore":I
    :cond_42
    invoke-virtual/range {p0 .. p0}, Lcom/viewpagerindicator/CirclePageIndicator;->getHeight()I

    move-result v4

    .line 225
    .local v4, "longSize":I
    invoke-virtual/range {p0 .. p0}, Lcom/viewpagerindicator/CirclePageIndicator;->getPaddingTop()I

    move-result v5

    .line 226
    .local v5, "longPaddingBefore":I
    invoke-virtual/range {p0 .. p0}, Lcom/viewpagerindicator/CirclePageIndicator;->getPaddingBottom()I

    move-result v6

    .line 227
    .local v6, "longPaddingAfter":I
    invoke-virtual/range {p0 .. p0}, Lcom/viewpagerindicator/CirclePageIndicator;->getPaddingLeft()I

    move-result v7

    .line 230
    .local v7, "shortPaddingBefore":I
    :goto_52
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mRadius:F

    const/high16 v1, 0x40400000    # 3.0f

    mul-float v8, v0, v1

    .line 231
    .local v8, "threeRadius":F
    int-to-float v0, v7

    move-object/from16 v1, p0

    iget v1, v1, Lcom/viewpagerindicator/CirclePageIndicator;->mRadius:F

    add-float v9, v0, v1

    .line 232
    .local v9, "shortOffset":F
    int-to-float v0, v5

    move-object/from16 v1, p0

    iget v1, v1, Lcom/viewpagerindicator/CirclePageIndicator;->mRadius:F

    add-float v10, v0, v1

    .line 233
    .local v10, "longOffset":F
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mCentered:Z

    if-eqz v0, :cond_7c

    .line 234
    sub-int v0, v4, v5

    sub-int/2addr v0, v6

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    int-to-float v1, v3

    mul-float/2addr v1, v8

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    add-float/2addr v10, v0

    .line 240
    :cond_7c
    move-object/from16 v0, p0

    iget v13, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mRadius:F

    .line 241
    .local v13, "pageFillRadius":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mPaintStroke:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_99

    .line 242
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mPaintStroke:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    sub-float/2addr v13, v0

    .line 246
    :cond_99
    const/4 v14, 0x0

    .local v14, "iLoop":I
    :goto_9a
    if-ge v14, v3, :cond_d6

    .line 247
    int-to-float v0, v14

    mul-float/2addr v0, v8

    add-float v15, v10, v0

    .line 248
    .local v15, "drawLong":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mOrientation:I

    if-nez v0, :cond_a9

    .line 249
    move v11, v15

    .line 250
    .local v11, "dX":F
    move v12, v9

    .local v12, "dY":F
    goto :goto_ab

    .line 252
    .end local v11    # "dX":F
    .end local v12    # "dY":F
    :cond_a9
    move v11, v9

    .line 253
    .local v11, "dX":F
    move v12, v15

    .line 256
    .local v12, "dY":F
    :goto_ab
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mPaintPageFill:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    if-lez v0, :cond_be

    .line 257
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mPaintPageFill:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual {v1, v11, v12, v13, v0}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 261
    :cond_be
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mRadius:F

    cmpl-float v0, v13, v0

    if-eqz v0, :cond_d3

    .line 262
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mRadius:F

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/viewpagerindicator/CirclePageIndicator;->mPaintStroke:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual {v2, v11, v12, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 246
    .end local v15    # "drawLong":F
    :cond_d3
    add-int/lit8 v14, v14, 0x1

    .end local v11    # "dX":F
    .end local v12    # "dY":F
    goto :goto_9a

    .line 267
    .end local v14    # "iLoop":I
    :cond_d6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mSnap:Z

    if-eqz v0, :cond_e1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mSnapPage:I

    goto :goto_e5

    :cond_e1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mCurrentPage:I

    :goto_e5
    int-to-float v0, v0

    mul-float v14, v0, v8

    .line 268
    .local v14, "cx":F
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mSnap:Z

    if-nez v0, :cond_f4

    .line 269
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mPageOffset:F

    mul-float/2addr v0, v8

    add-float/2addr v14, v0

    .line 271
    :cond_f4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mOrientation:I

    if-nez v0, :cond_fe

    .line 272
    add-float v11, v10, v14

    .line 273
    .local v11, "dX":F
    move v12, v9

    .local v12, "dY":F
    goto :goto_101

    .line 275
    .end local v11    # "dX":F
    .end local v12    # "dY":F
    :cond_fe
    move v11, v9

    .line 276
    .local v11, "dX":F
    add-float v12, v10, v14

    .line 278
    .local v12, "dY":F
    :goto_101
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/CirclePageIndicator;->mRadius:F

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/viewpagerindicator/CirclePageIndicator;->mPaintFill:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual {v2, v11, v12, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 279
    return-void
.end method

.method protected onMeasure(II)V
    .registers 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .line 446
    iget v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mOrientation:I

    if-nez v0, :cond_10

    .line 447
    invoke-direct {p0, p1}, Lcom/viewpagerindicator/CirclePageIndicator;->measureLong(I)I

    move-result v0

    invoke-direct {p0, p2}, Lcom/viewpagerindicator/CirclePageIndicator;->measureShort(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/viewpagerindicator/CirclePageIndicator;->setMeasuredDimension(II)V

    goto :goto_1b

    .line 449
    :cond_10
    invoke-direct {p0, p1}, Lcom/viewpagerindicator/CirclePageIndicator;->measureShort(I)I

    move-result v0

    invoke-direct {p0, p2}, Lcom/viewpagerindicator/CirclePageIndicator;->measureLong(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/viewpagerindicator/CirclePageIndicator;->setMeasuredDimension(II)V

    .line 451
    :goto_1b
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .registers 3
    .param p1, "state"    # I

    .line 403
    iput p1, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mScrollState:I

    .line 405
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_b

    .line 406
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    .line 408
    :cond_b
    return-void
.end method

.method public onPageScrolled(IFI)V
    .registers 5
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .line 412
    iput p1, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mCurrentPage:I

    .line 413
    iput p2, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mPageOffset:F

    .line 414
    invoke-virtual {p0}, Lcom/viewpagerindicator/CirclePageIndicator;->invalidate()V

    .line 416
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_10

    .line 417
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    .line 419
    :cond_10
    return-void
.end method

.method public onPageSelected(I)V
    .registers 3
    .param p1, "position"    # I

    .line 423
    iget-boolean v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mSnap:Z

    if-nez v0, :cond_8

    iget v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mScrollState:I

    if-nez v0, :cond_f

    .line 424
    :cond_8
    iput p1, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mCurrentPage:I

    .line 425
    iput p1, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mSnapPage:I

    .line 426
    invoke-virtual {p0}, Lcom/viewpagerindicator/CirclePageIndicator;->invalidate()V

    .line 429
    :cond_f
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_18

    .line 430
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageSelected(I)V

    .line 432
    :cond_18
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 4
    .param p1, "state"    # Landroid/os/Parcelable;

    .line 509
    move-object v1, p1

    check-cast v1, Lcom/viewpagerindicator/CirclePageIndicator$SavedState;

    .line 510
    .local v1, "savedState":Lcom/viewpagerindicator/CirclePageIndicator$SavedState;
    invoke-virtual {v1}, Lcom/viewpagerindicator/CirclePageIndicator$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 511
    iget v0, v1, Lcom/viewpagerindicator/CirclePageIndicator$SavedState;->currentPage:I

    iput v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mCurrentPage:I

    .line 512
    iget v0, v1, Lcom/viewpagerindicator/CirclePageIndicator$SavedState;->currentPage:I

    iput v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mSnapPage:I

    .line 513
    invoke-virtual {p0}, Lcom/viewpagerindicator/CirclePageIndicator;->requestLayout()V

    .line 514
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .registers 4

    .line 518
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 519
    .local v1, "superState":Landroid/os/Parcelable;
    new-instance v2, Lcom/viewpagerindicator/CirclePageIndicator$SavedState;

    invoke-direct {v2, v1}, Lcom/viewpagerindicator/CirclePageIndicator$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 520
    .local v2, "savedState":Lcom/viewpagerindicator/CirclePageIndicator$SavedState;
    iget v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mCurrentPage:I

    iput v0, v2, Lcom/viewpagerindicator/CirclePageIndicator$SavedState;->currentPage:I

    .line 521
    return-object v2
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 10
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .line 282
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 283
    const/4 v0, 0x1

    return v0

    .line 285
    :cond_8
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1a

    .line 286
    :cond_18
    const/4 v0, 0x0

    return v0

    .line 289
    :cond_1a
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v3, v0, 0xff

    .line 290
    .local v3, "action":I
    packed-switch v3, :pswitch_data_112

    goto/16 :goto_10f

    .line 292
    :pswitch_25
    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mActivePointerId:I

    .line 293
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mLastMotionX:F

    .line 294
    goto/16 :goto_10f

    .line 297
    :pswitch_34
    iget v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mActivePointerId:I

    invoke-static {p1, v0}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v4

    .line 298
    .local v4, "activePointerIndex":I
    invoke-static {p1, v4}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v5

    .line 299
    .local v5, "x":F
    iget v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mLastMotionX:F

    sub-float v6, v5, v0

    .line 301
    .local v6, "deltaX":F
    iget-boolean v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mIsDragging:Z

    if-nez v0, :cond_54

    .line 302
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mTouchSlop:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_54

    .line 303
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mIsDragging:Z

    .line 307
    :cond_54
    iget-boolean v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mIsDragging:Z

    if-eqz v0, :cond_10f

    .line 308
    iput v5, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mLastMotionX:F

    .line 309
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->isFakeDragging()Z

    move-result v0

    if-nez v0, :cond_6a

    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->beginFakeDrag()Z

    move-result v0

    if-eqz v0, :cond_10f

    .line 310
    :cond_6a
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v6}, Landroid/support/v4/view/ViewPager;->fakeDragBy(F)V

    goto/16 :goto_10f

    .line 319
    .end local v4    # "activePointerIndex":I
    .end local v5    # "x":F
    .end local v6    # "deltaX":F
    :pswitch_71
    iget-boolean v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mIsDragging:Z

    if-nez v0, :cond_c7

    .line 320
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v4

    .line 321
    .local v4, "count":I
    invoke-virtual {p0}, Lcom/viewpagerindicator/CirclePageIndicator;->getWidth()I

    move-result v5

    .line 322
    .local v5, "width":I
    int-to-float v0, v5

    const/high16 v1, 0x40000000    # 2.0f

    div-float v6, v0, v1

    .line 323
    .local v6, "halfWidth":F
    int-to-float v0, v5

    const/high16 v1, 0x40c00000    # 6.0f

    div-float v7, v0, v1

    .line 325
    .local v7, "sixthWidth":F
    iget v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mCurrentPage:I

    if-lez v0, :cond_a9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    sub-float v1, v6, v7

    cmpg-float v0, v0, v1

    if-gez v0, :cond_a9

    .line 326
    const/4 v0, 0x3

    if-eq v3, v0, :cond_a7

    .line 327
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mCurrentPage:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 329
    :cond_a7
    const/4 v0, 0x1

    return v0

    .line 330
    :cond_a9
    iget v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mCurrentPage:I

    add-int/lit8 v1, v4, -0x1

    if-ge v0, v1, :cond_c7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    add-float v1, v6, v7

    cmpl-float v0, v0, v1

    if-lez v0, :cond_c7

    .line 331
    const/4 v0, 0x3

    if-eq v3, v0, :cond_c5

    .line 332
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mCurrentPage:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 334
    :cond_c5
    const/4 v0, 0x1

    return v0

    .line 338
    .end local v4    # "count":I
    .end local v5    # "width":I
    .end local v6    # "halfWidth":F
    .end local v7    # "sixthWidth":F
    :cond_c7
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mIsDragging:Z

    .line 339
    const/4 v0, -0x1

    iput v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mActivePointerId:I

    .line 340
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->isFakeDragging()Z

    move-result v0

    if-eqz v0, :cond_10f

    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->endFakeDrag()V

    goto :goto_10f

    .line 344
    :pswitch_db
    invoke-static {p1}, Landroid/support/v4/view/MotionEventCompat;->getActionIndex(Landroid/view/MotionEvent;)I

    move-result v4

    .line 345
    .local v4, "index":I
    invoke-static {p1, v4}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mLastMotionX:F

    .line 346
    invoke-static {p1, v4}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mActivePointerId:I

    .line 347
    goto :goto_10f

    .line 351
    .end local v4    # "index":I
    :pswitch_ec
    invoke-static {p1}, Landroid/support/v4/view/MotionEventCompat;->getActionIndex(Landroid/view/MotionEvent;)I

    move-result v4

    .line 352
    .local v4, "pointerIndex":I
    invoke-static {p1, v4}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v5

    .line 353
    .local v5, "pointerId":I
    iget v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mActivePointerId:I

    if-ne v5, v0, :cond_103

    .line 354
    if-nez v4, :cond_fc

    const/4 v6, 0x1

    goto :goto_fd

    :cond_fc
    const/4 v6, 0x0

    .line 355
    .local v6, "newPointerIndex":I
    :goto_fd
    invoke-static {p1, v6}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mActivePointerId:I

    .line 357
    .end local v6    # "newPointerIndex":I
    :cond_103
    iget v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mActivePointerId:I

    invoke-static {p1, v0}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v0

    invoke-static {p1, v0}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mLastMotionX:F

    .line 361
    .end local v4    # "pointerIndex":I
    .end local v5    # "pointerId":I
    :cond_10f
    :goto_10f
    :pswitch_10f
    const/4 v0, 0x1

    return v0

    nop

    :pswitch_data_112
    .packed-switch 0x0
        :pswitch_25
        :pswitch_71
        :pswitch_34
        :pswitch_71
        :pswitch_10f
        :pswitch_db
        :pswitch_ec
    .end packed-switch
.end method

.method public setCentered(Z)V
    .registers 2
    .param p1, "centered"    # Z

    .line 118
    iput-boolean p1, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mCentered:Z

    .line 119
    invoke-virtual {p0}, Lcom/viewpagerindicator/CirclePageIndicator;->invalidate()V

    .line 120
    return-void
.end method

.method public setCurrentItem(I)V
    .registers 4
    .param p1, "item"    # I

    .line 388
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_c

    .line 389
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ViewPager has not been bound."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 391
    :cond_c
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 392
    iput p1, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mCurrentPage:I

    .line 393
    invoke-virtual {p0}, Lcom/viewpagerindicator/CirclePageIndicator;->invalidate()V

    .line 394
    return-void
.end method

.method public setFillColor(I)V
    .registers 3
    .param p1, "fillColor"    # I

    .line 136
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mPaintFill:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 137
    invoke-virtual {p0}, Lcom/viewpagerindicator/CirclePageIndicator;->invalidate()V

    .line 138
    return-void
.end method

.method public setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V
    .registers 2
    .param p1, "listener"    # Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 436
    iput-object p1, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 437
    return-void
.end method

.method public setOrientation(I)V
    .registers 4
    .param p1, "orientation"    # I

    .line 145
    sparse-switch p1, :sswitch_data_14

    goto :goto_a

    .line 148
    :sswitch_4
    iput p1, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mOrientation:I

    .line 149
    invoke-virtual {p0}, Lcom/viewpagerindicator/CirclePageIndicator;->requestLayout()V

    .line 150
    goto :goto_12

    .line 153
    :goto_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Orientation must be either HORIZONTAL or VERTICAL."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 155
    :goto_12
    return-void

    nop

    :sswitch_data_14
    .sparse-switch
        0x0 -> :sswitch_4
        0x1 -> :sswitch_4
    .end sparse-switch
.end method

.method public setPageColor(I)V
    .registers 3
    .param p1, "pageColor"    # I

    .line 127
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mPaintPageFill:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 128
    invoke-virtual {p0}, Lcom/viewpagerindicator/CirclePageIndicator;->invalidate()V

    .line 129
    return-void
.end method

.method public setRadius(F)V
    .registers 2
    .param p1, "radius"    # F

    .line 180
    iput p1, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mRadius:F

    .line 181
    invoke-virtual {p0}, Lcom/viewpagerindicator/CirclePageIndicator;->invalidate()V

    .line 182
    return-void
.end method

.method public setSnap(Z)V
    .registers 2
    .param p1, "snap"    # Z

    .line 189
    iput-boolean p1, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mSnap:Z

    .line 190
    invoke-virtual {p0}, Lcom/viewpagerindicator/CirclePageIndicator;->invalidate()V

    .line 191
    return-void
.end method

.method public setStrokeColor(I)V
    .registers 3
    .param p1, "strokeColor"    # I

    .line 162
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mPaintStroke:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 163
    invoke-virtual {p0}, Lcom/viewpagerindicator/CirclePageIndicator;->invalidate()V

    .line 164
    return-void
.end method

.method public setStrokeWidth(F)V
    .registers 3
    .param p1, "strokeWidth"    # F

    .line 171
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mPaintStroke:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 172
    invoke-virtual {p0}, Lcom/viewpagerindicator/CirclePageIndicator;->invalidate()V

    .line 173
    return-void
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;)V
    .registers 4
    .param p1, "view"    # Landroid/support/v4/view/ViewPager;

    .line 366
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-ne v0, p1, :cond_5

    .line 367
    return-void

    .line 369
    :cond_5
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_f

    .line 370
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 372
    :cond_f
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    if-nez v0, :cond_1d

    .line 373
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ViewPager does not have adapter instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 375
    :cond_1d
    iput-object p1, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 376
    iget-object v0, p0, Lcom/viewpagerindicator/CirclePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 377
    invoke-virtual {p0}, Lcom/viewpagerindicator/CirclePageIndicator;->invalidate()V

    .line 378
    return-void
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;I)V
    .registers 3
    .param p1, "view"    # Landroid/support/v4/view/ViewPager;
    .param p2, "initialPosition"    # I

    .line 382
    invoke-virtual {p0, p1}, Lcom/viewpagerindicator/CirclePageIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 383
    invoke-virtual {p0, p2}, Lcom/viewpagerindicator/CirclePageIndicator;->setCurrentItem(I)V

    .line 384
    return-void
.end method

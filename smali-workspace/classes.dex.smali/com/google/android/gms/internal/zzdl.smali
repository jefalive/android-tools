.class public Lcom/google/android/gms/internal/zzdl;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/zzdf;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# static fields
.field static final zzzC:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
        }
    .end annotation
.end field


# instance fields
.field private final zzzA:Lcom/google/android/gms/ads/internal/zze;

.field private final zzzB:Lcom/google/android/gms/internal/zzfn;


# direct methods
.method static constructor <clinit>()V
    .registers 12

    const-string v0, "resize"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "playVideo"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "storePicture"

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, "createCalendarEvent"

    const/4 v7, 0x4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const-string v8, "setOrientationProperties"

    const/4 v9, 0x5

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const-string v10, "closeResizedAd"

    const/4 v11, 0x6

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-static/range {v0 .. v11}, Lcom/google/android/gms/internal/zzmr;->zza(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzdl;->zzzC:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/ads/internal/zze;Lcom/google/android/gms/internal/zzfn;)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/zzdl;->zzzA:Lcom/google/android/gms/ads/internal/zze;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzdl;->zzzB:Lcom/google/android/gms/internal/zzfn;

    return-void
.end method


# virtual methods
.method public zza(Lcom/google/android/gms/internal/zzjp;Ljava/util/Map;)V
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/google/android/gms/internal/zzjp;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;)V"
        }
    .end annotation

    const-string v0, "a"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/internal/zzdl;->zzzC:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v0, 0x5

    if-eq v3, v0, :cond_2b

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdl;->zzzA:Lcom/google/android/gms/ads/internal/zze;

    if-eqz v0, :cond_2b

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdl;->zzzA:Lcom/google/android/gms/ads/internal/zze;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/zze;->zzbh()Z

    move-result v0

    if-nez v0, :cond_2b

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdl;->zzzA:Lcom/google/android/gms/ads/internal/zze;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/zze;->zzq(Ljava/lang/String;)V

    return-void

    :cond_2b
    packed-switch v3, :pswitch_data_5e

    goto :goto_57

    :pswitch_2f
    iget-object v0, p0, Lcom/google/android/gms/internal/zzdl;->zzzB:Lcom/google/android/gms/internal/zzfn;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/internal/zzfn;->zzi(Ljava/util/Map;)V

    goto :goto_5c

    :pswitch_35
    new-instance v0, Lcom/google/android/gms/internal/zzfm;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/internal/zzfm;-><init>(Lcom/google/android/gms/internal/zzjp;Ljava/util/Map;)V

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzfm;->execute()V

    goto :goto_5c

    :pswitch_3e
    new-instance v0, Lcom/google/android/gms/internal/zzfp;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/internal/zzfp;-><init>(Lcom/google/android/gms/internal/zzjp;Ljava/util/Map;)V

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzfp;->execute()V

    goto :goto_5c

    :pswitch_47
    new-instance v0, Lcom/google/android/gms/internal/zzfo;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/internal/zzfo;-><init>(Lcom/google/android/gms/internal/zzjp;Ljava/util/Map;)V

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzfo;->execute()V

    goto :goto_5c

    :pswitch_50
    iget-object v0, p0, Lcom/google/android/gms/internal/zzdl;->zzzB:Lcom/google/android/gms/internal/zzfn;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzfn;->zzp(Z)V

    goto :goto_5c

    :goto_57
    :pswitch_57
    const-string v0, "Unknown MRAID command called."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaJ(Ljava/lang/String;)V

    :goto_5c
    return-void

    nop

    :pswitch_data_5e
    .packed-switch 0x1
        :pswitch_2f
        :pswitch_57
        :pswitch_3e
        :pswitch_35
        :pswitch_47
        :pswitch_50
    .end packed-switch
.end method

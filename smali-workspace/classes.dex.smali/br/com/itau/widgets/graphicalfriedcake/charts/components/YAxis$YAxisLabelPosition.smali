.class public final enum Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;
.super Ljava/lang/Enum;
.source "YAxis.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "YAxisLabelPosition"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

.field public static final enum INSIDE_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

.field public static final enum OUTSIDE_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 77
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    const-string v1, "OUTSIDE_CHART"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;->OUTSIDE_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    const-string v1, "INSIDE_CHART"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;->INSIDE_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    .line 76
    const/4 v0, 0x2

    new-array v0, v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;->OUTSIDE_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;->INSIDE_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;->$VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 76
    const-class v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;
    .registers 1

    .line 76
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;->$VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    invoke-virtual {v0}, [Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    return-object v0
.end method

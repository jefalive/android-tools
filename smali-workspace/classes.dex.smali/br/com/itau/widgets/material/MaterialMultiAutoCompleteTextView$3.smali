.class Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$3;
.super Ljava/lang/Object;
.source "MaterialMultiAutoCompleteTextView.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initFloatingLabel()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;


# direct methods
.method constructor <init>(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)V
    .registers 2

    .line 835
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$3;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .registers 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .line 838
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$3;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    # getter for: Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelEnabled:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->access$200(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)Z

    move-result v0

    if-eqz v0, :cond_25

    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$3;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    # getter for: Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->highlightFloatingLabel:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->access$500(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 839
    if-eqz p2, :cond_1c

    .line 840
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$3;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    # invokes: Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getLabelFocusAnimator()Lcom/nineoldandroids/animation/ObjectAnimator;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->access$600(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->start()V

    goto :goto_25

    .line 842
    :cond_1c
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$3;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    # invokes: Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getLabelFocusAnimator()Lcom/nineoldandroids/animation/ObjectAnimator;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->access$600(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->reverse()V

    .line 845
    :cond_25
    :goto_25
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$3;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    # getter for: Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->validateOnFocusLost:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->access$700(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)Z

    move-result v0

    if-eqz v0, :cond_34

    if-nez p2, :cond_34

    .line 846
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$3;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->validate()Z

    .line 848
    :cond_34
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$3;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->outerFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    if-eqz v0, :cond_41

    .line 849
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$3;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->outerFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-interface {v0, p1, p2}, Landroid/view/View$OnFocusChangeListener;->onFocusChange(Landroid/view/View;Z)V

    .line 851
    :cond_41
    return-void
.end method

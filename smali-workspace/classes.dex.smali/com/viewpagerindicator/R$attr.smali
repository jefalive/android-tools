.class public final Lcom/viewpagerindicator/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/viewpagerindicator/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final centered:I = 0x7f010000

.field public static final clipPadding:I = 0x7f010229

.field public static final fadeDelay:I = 0x7f010253

.field public static final fadeLength:I = 0x7f010254

.field public static final fades:I = 0x7f010252

.field public static final fillColor:I = 0x7f0100ed

.field public static final footerColor:I = 0x7f01022a

.field public static final footerIndicatorHeight:I = 0x7f01022d

.field public static final footerIndicatorStyle:I = 0x7f01022c

.field public static final footerIndicatorUnderlinePadding:I = 0x7f01022e

.field public static final footerLineHeight:I = 0x7f01022b

.field public static final footerPadding:I = 0x7f01022f

.field public static final gapWidth:I = 0x7f010151

.field public static final linePosition:I = 0x7f010230

.field public static final lineWidth:I = 0x7f010150

.field public static final pageColor:I = 0x7f0100ee

.field public static final radius:I = 0x7f0100ef

.field public static final selectedBold:I = 0x7f010231

.field public static final selectedColor:I = 0x7f010031

.field public static final snap:I = 0x7f0100f0

.field public static final strokeColor:I = 0x7f0100f1

.field public static final strokeWidth:I = 0x7f010032

.field public static final titlePadding:I = 0x7f010232

.field public static final topPadding:I = 0x7f010233

.field public static final unselectedColor:I = 0x7f010035

.field public static final vpiCirclePageIndicatorStyle:I = 0x7f01025f

.field public static final vpiIconPageIndicatorStyle:I = 0x7f010260

.field public static final vpiLinePageIndicatorStyle:I = 0x7f010261

.field public static final vpiTabPageIndicatorStyle:I = 0x7f010263

.field public static final vpiTitlePageIndicatorStyle:I = 0x7f010262

.field public static final vpiUnderlinePageIndicatorStyle:I = 0x7f010264


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

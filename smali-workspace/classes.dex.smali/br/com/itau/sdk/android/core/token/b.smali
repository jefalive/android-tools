.class public interface abstract Lbr/com/itau/sdk/android/core/token/b;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract a(Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;)Lbr/com/itau/sdk/android/core/SDKCustomUiController$EnvioSMSResponseDTO;
    .param p1    # Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "_enviaSMS"
    .end annotation
.end method

.method public abstract a(Lbr/com/itau/sdk/android/core/model/PasswordDTO;)Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidatePasswordResponseDTO;
    .param p1    # Lbr/com/itau/sdk/android/core/model/PasswordDTO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "_validaSenha"
    .end annotation
.end method

.method public abstract a(Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;)Lcom/google/gson/JsonElement;
    .param p1    # Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "_consultarTokenApp"
    .end annotation
.end method

.method public abstract a(Ljava/lang/String;Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;)Lcom/google/gson/JsonElement;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Header;
            value = "tipo_dispositivo"
        .end annotation
    .end param
    .param p2    # Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "_simulaComTokenOutroDispositivoComSenha"
    .end annotation
.end method

.method public abstract b(Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;)Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoSMSResponseDTO;
    .param p1    # Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "_validaTokenSMS"
    .end annotation
.end method

.method public abstract b(Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;)Lcom/google/gson/JsonElement;
    .param p1    # Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "_simulaCadastrarTokenApp"
    .end annotation
.end method

.method public abstract b(Ljava/lang/String;Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;)Lcom/google/gson/JsonElement;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Header;
            value = "tipo_dispositivo"
        .end annotation
    .end param
    .param p2    # Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "_simulaComTokenOutroDispositivo"
    .end annotation
.end method

.method public abstract c(Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;)Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoAppResponseDTO;
    .param p1    # Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "_validaTokenAPP"
    .end annotation
.end method

.method public abstract c(Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;)Lcom/google/gson/JsonElement;
    .param p1    # Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "_simulaCancelarTokenApp"
    .end annotation
.end method

.method public abstract c(Ljava/lang/String;Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;)Lcom/google/gson/JsonElement;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Header;
            value = "tipo_dispositivo"
        .end annotation
    .end param
    .param p2    # Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "_cadastrarComTokenOutroDispositivo"
    .end annotation
.end method

.method public abstract d(Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;)Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoFisicoResponseDTO;
    .param p1    # Lbr/com/itau/sdk/android/core/model/TokenRequestDTO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "_validaTokenFisico"
    .end annotation
.end method

.method public abstract d(Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;)Lcom/google/gson/JsonElement;
    .param p1    # Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "_cancelarTokenApp"
    .end annotation
.end method

.method public abstract d(Ljava/lang/String;Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;)Lcom/google/gson/JsonElement;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Header;
            value = "tipo_dispositivo"
        .end annotation
    .end param
    .param p2    # Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "_cadastrarTokenApp"
    .end annotation
.end method

.class public Lcom/itau/animacao/loading/LoadingViewAnimation;
.super Landroid/view/animation/Animation;
.source "LoadingViewAnimation.java"


# instance fields
.field private circle:Lcom/itau/animacao/loading/LoadingView;

.field private newAngle:F

.field private oldAngle:F


# direct methods
.method public constructor <init>(Lcom/itau/animacao/loading/LoadingView;F)V
    .registers 4
    .param p1, "circle"    # Lcom/itau/animacao/loading/LoadingView;
    .param p2, "newAngle"    # F

    .line 13
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 14
    invoke-virtual {p1}, Lcom/itau/animacao/loading/LoadingView;->getAngle()F

    move-result v0

    iput v0, p0, Lcom/itau/animacao/loading/LoadingViewAnimation;->oldAngle:F

    .line 15
    iput p2, p0, Lcom/itau/animacao/loading/LoadingViewAnimation;->newAngle:F

    .line 16
    iput-object p1, p0, Lcom/itau/animacao/loading/LoadingViewAnimation;->circle:Lcom/itau/animacao/loading/LoadingView;

    .line 17
    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .registers 7
    .param p1, "interpolatedTime"    # F
    .param p2, "transformation"    # Landroid/view/animation/Transformation;

    .line 21
    iget v0, p0, Lcom/itau/animacao/loading/LoadingViewAnimation;->oldAngle:F

    iget v1, p0, Lcom/itau/animacao/loading/LoadingViewAnimation;->newAngle:F

    iget v2, p0, Lcom/itau/animacao/loading/LoadingViewAnimation;->oldAngle:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, p1

    add-float v3, v0, v1

    .line 22
    .local v3, "angle":F
    iget-object v0, p0, Lcom/itau/animacao/loading/LoadingViewAnimation;->circle:Lcom/itau/animacao/loading/LoadingView;

    invoke-virtual {v0, v3}, Lcom/itau/animacao/loading/LoadingView;->setAngle(F)V

    .line 23
    iget-object v0, p0, Lcom/itau/animacao/loading/LoadingViewAnimation;->circle:Lcom/itau/animacao/loading/LoadingView;

    invoke-virtual {v0}, Lcom/itau/animacao/loading/LoadingView;->requestLayout()V

    .line 24
    return-void
.end method

.class public Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;
.super Ljava/lang/Object;
.source "PreLoginElegibilidadeBotaoAcessar.java"


# instance fields
.field private botaoComProgresso:Lcom/itau/empresas/ui/view/BotaoComProgresso;

.field private operadorSelecionado:Lcom/itau/empresas/api/model/OperadorVO;

.field private textoCampoDocumento:Landroid/widget/EditText;

.field private textoLogin:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;


# direct methods
.method private constructor <init>()V
    .registers 1

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method

.method public constructor <init>(Lcom/itau/empresas/ui/view/BotaoComProgresso;Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;Landroid/widget/EditText;)V
    .registers 4
    .param p1, "botaoComProgresso"    # Lcom/itau/empresas/ui/view/BotaoComProgresso;
    .param p2, "textoLogin"    # Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;
    .param p3, "textoCampoDocumento"    # Landroid/widget/EditText;

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->botaoComProgresso:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    .line 30
    iput-object p2, p0, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->textoLogin:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    .line 31
    iput-object p3, p0, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->textoCampoDocumento:Landroid/widget/EditText;

    .line 32
    return-void
.end method

.method private deveHabilitarBotaoAcessar(Z)V
    .registers 4
    .param p1, "deveHabilitarBotaoAcessar"    # Z

    .line 62
    if-eqz p1, :cond_e

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->botaoComProgresso:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->setEnabled(Z)V

    goto :goto_19

    .line 65
    :cond_e
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->botaoComProgresso:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->setEnabled(Z)V

    .line 67
    :goto_19
    return-void
.end method

.method private trataBotaoAcessarParaModoAgenciaContaOperador()V
    .registers 2

    .line 91
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->validaBarraUnica()Z

    move-result v0

    if-nez v0, :cond_10

    .line 92
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->deveHabilitarBotaoAcessar(Z)V

    goto :goto_19

    .line 94
    :cond_10
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->deveHabilitarBotaoAcessar(Z)V

    .line 96
    :goto_19
    return-void
.end method

.method private trataBotaoAcessarParaModoMultiDocumentos()V
    .registers 2

    .line 100
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->validaBarraUnica()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->validaCampoDocumento()Z

    move-result v0

    if-nez v0, :cond_16

    .line 101
    :cond_c
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->deveHabilitarBotaoAcessar(Z)V

    goto :goto_1f

    .line 103
    :cond_16
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->deveHabilitarBotaoAcessar(Z)V

    .line 105
    :goto_1f
    return-void
.end method

.method private trataBotaoAcessarParaModoMultioperadores()V
    .registers 2

    .line 108
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->validaBarraUnica()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->validaCampoDocumento()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->validaOperador()Z

    move-result v0

    if-nez v0, :cond_1c

    .line 109
    :cond_12
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->deveHabilitarBotaoAcessar(Z)V

    goto :goto_25

    .line 111
    :cond_1c
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->deveHabilitarBotaoAcessar(Z)V

    .line 113
    :goto_25
    return-void
.end method

.method private validaBarraUnica()Z
    .registers 2

    .line 70
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->textoLogin:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->isValido()Z

    move-result v0

    if-nez v0, :cond_a

    .line 71
    const/4 v0, 0x0

    return v0

    .line 73
    :cond_a
    const/4 v0, 0x1

    return v0
.end method

.method private validaCampoDocumento()Z
    .registers 2

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->textoCampoDocumento:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-nez v0, :cond_a

    .line 78
    const/4 v0, 0x0

    return v0

    .line 80
    :cond_a
    const/4 v0, 0x1

    return v0
.end method

.method private validaOperador()Z
    .registers 2

    .line 84
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->operadorSelecionado:Lcom/itau/empresas/api/model/OperadorVO;

    if-nez v0, :cond_6

    .line 85
    const/4 v0, 0x0

    return v0

    .line 87
    :cond_6
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public setOperadorSelecionado(Lcom/itau/empresas/api/model/OperadorVO;)V
    .registers 2
    .param p1, "operadorVO"    # Lcom/itau/empresas/api/model/OperadorVO;

    .line 35
    iput-object p1, p0, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->operadorSelecionado:Lcom/itau/empresas/api/model/OperadorVO;

    .line 36
    return-void
.end method

.method public trataElegibilidadeBotaoAcessar(I)V
    .registers 2
    .param p1, "modoAtual"    # I

    .line 44
    packed-switch p1, :pswitch_data_10

    goto :goto_f

    .line 47
    :pswitch_4
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->trataBotaoAcessarParaModoAgenciaContaOperador()V

    .line 48
    goto :goto_f

    .line 51
    :pswitch_8
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->trataBotaoAcessarParaModoMultiDocumentos()V

    .line 52
    goto :goto_f

    .line 54
    :pswitch_c
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->trataBotaoAcessarParaModoMultioperadores()V

    .line 55
    .line 59
    :goto_f
    return-void

    :pswitch_data_10
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_8
        :pswitch_8
        :pswitch_c
    .end packed-switch
.end method

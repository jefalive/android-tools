.class public final Ldid/b;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# static fields
.field private static final b:[B

.field private static c:I


# instance fields
.field final synthetic a:Ldid/f;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x30

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Ldid/b;->b:[B

    const/16 v0, 0x1e

    sput v0, Ldid/b;->c:I

    return-void

    :array_e
    .array-data 1
        0x57t
        0x43t
        -0x4bt
        -0x39t
        -0x5t
        -0x7t
        0xbt
        -0xdt
        0x4at
        -0x51t
        -0x1t
        0x1at
        -0x15t
        0x0t
        -0x4t
        -0x6t
        -0xft
        0xft
        0x11t
        -0x7t
        0x3t
        0x22t
        -0x50t
        -0x3t
        -0x7t
        0x52t
        -0x48t
        -0xbt
        -0x8t
        0x5t
        0x8t
        0x34t
        0xct
        -0x29t
        -0x20t
        -0xbt
        0x7t
        -0xft
        0xft
        -0x15t
        0x9t
        -0x7t
        0x5t
        -0x7t
        0x3t
        0x11t
        -0x7t
        0x3t
    .end array-data
.end method

.method public constructor <init>(Ldid/f;)V
    .registers 2

    .line 47
    iput-object p1, p0, Ldid/b;->a:Ldid/f;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private static a(IBS)Ljava/lang/String;
    .registers 9

    rsub-int/lit8 p1, p1, 0x2d

    const/4 v4, 0x0

    rsub-int/lit8 p2, p2, 0x77

    rsub-int/lit8 p0, p0, 0x1a

    new-instance v0, Ljava/lang/String;

    sget-object v5, Ldid/b;->b:[B

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v5, :cond_19

    move v2, p0

    move v3, p1

    :goto_13
    neg-int v3, v3

    add-int/lit8 p1, p1, 0x1

    add-int/2addr v2, v3

    add-int/lit8 p2, v2, -0x2

    :cond_19
    int-to-byte v2, p2

    aput-byte v2, v1, v4

    if-ne v4, p0, :cond_23

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_23
    move v2, p2

    aget-byte v3, v5, p1

    add-int/lit8 v4, v4, 0x1

    goto :goto_13
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8

    .line 50
    invoke-static {}, Lbr/com/itau/security/did/DID;->b()Ljava/lang/String;

    sget-object v0, Ldid/b;->b:[B

    const/4 v1, 0x6

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Ldid/b;->b:[B

    const/16 v2, 0x21

    aget-byte v1, v1, v2

    neg-int v1, v1

    int-to-byte v1, v1

    sget-object v2, Ldid/b;->b:[B

    const/16 v3, 0x12

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Ldid/b;->a(IBS)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 52
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ldid/b;->getResultExtras(Z)Landroid/os/Bundle;

    move-result-object p2

    .line 54
    if-eqz p2, :cond_55

    const/16 v0, 0x17

    const/4 v1, 0x2

    const/16 v2, 0x13

    invoke-static {v0, v1, v2}, Ldid/b;->a(IBS)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_55

    .line 55
    const/16 v0, 0x17

    const/4 v1, 0x2

    const/16 v2, 0x13

    invoke-static {v0, v1, v2}, Ldid/b;->a(IBS)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->to([B)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ldid/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 56
    invoke-static {p1, v4}, Ldid/e;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 59
    :cond_55
    if-eqz p2, :cond_97

    sget v0, Ldid/b;->c:I

    and-int/lit8 v0, v0, 0x77

    int-to-byte v0, v0

    sget-object v1, Ldid/b;->b:[B

    const/16 v2, 0xd

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    int-to-byte v2, v1

    invoke-static {v0, v1, v2}, Ldid/b;->a(IBS)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_97

    .line 60
    invoke-static {}, Lbr/com/itau/security/did/DID;->a()Lbr/com/itau/security/did/DID;

    move-result-object v0

    sget v1, Ldid/b;->c:I

    and-int/lit8 v1, v1, 0x77

    int-to-byte v1, v1

    sget-object v2, Ldid/b;->b:[B

    const/16 v3, 0xd

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    int-to-byte v3, v2

    invoke-static {v1, v2, v3}, Ldid/b;->a(IBS)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Lbr/com/itau/security/commons/binary/ByteUtils;->to([B)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbr/com/itau/security/did/DID;->a(Lbr/com/itau/security/did/DID;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_c6

    .line 62
    :cond_97
    invoke-static {}, Lbr/com/itau/security/did/DID;->b()Ljava/lang/String;

    sget-object v0, Ldid/b;->b:[B

    const/16 v1, 0xd

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    or-int/lit8 v1, v0, 0x1b

    int-to-byte v1, v1

    sget-object v2, Ldid/b;->b:[B

    const/16 v3, 0x22

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Ldid/b;->a(IBS)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    .line 63
    invoke-static {}, Lbr/com/itau/security/did/DID;->a()Lbr/com/itau/security/did/DID;

    move-result-object v0

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ldid/d;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbr/com/itau/security/did/DID;->a(Lbr/com/itau/security/did/DID;Ljava/lang/String;)Ljava/lang/String;

    .line 66
    :goto_c6
    iget-object v0, p0, Ldid/b;->a:Ldid/f;

    invoke-virtual {v0}, Ldid/f;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 67
    sget-object v0, Lbr/com/itau/security/did/DID;->a:Ljava/lang/String;

    invoke-static {}, Lbr/com/itau/security/did/DID;->a()Lbr/com/itau/security/did/DID;

    move-result-object v1

    invoke-static {v1}, Lbr/com/itau/security/did/DID;->a(Lbr/com/itau/security/did/DID;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 68
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 69
    return-void
.end method

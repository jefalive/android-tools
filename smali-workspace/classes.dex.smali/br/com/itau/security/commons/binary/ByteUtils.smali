.class public final Lbr/com/itau/security/commons/binary/ByteUtils;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[B

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x36

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/security/commons/binary/ByteUtils;->a:[B

    const/16 v0, 0xc3

    sput v0, Lbr/com/itau/security/commons/binary/ByteUtils;->b:I

    return-void

    :array_e
    .array-data 1
        0x4dt
        0x44t
        0x39t
        -0x3ft
        0x2bt
        -0xat
        0xet
        -0x53t
        0x4et
        0x1t
        0x5t
        -0x54t
        0x53t
        0x2t
        -0x5t
        0x0t
        -0x1t
        0x3t
        0x2t
        -0x54t
        0x35t
        -0x1t
        -0xet
        -0x19t
        0xbt
        -0x1t
        -0xet
        -0x39t
        0xbt
        0x2ct
        0x6t
        -0x9t
        -0x8t
        -0x44t
        0x4et
        0x1t
        0x5t
        -0x54t
        0x4at
        0x5t
        -0x6t
        0x5t
        -0x4et
        0x42t
        0x17t
        -0x5t
        -0xft
        -0x45t
        0x41t
        0x11t
        0x0t
        -0x11t
        0x18t
        -0x1t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(IIS)Ljava/lang/String;
    .registers 9

    new-instance v0, Ljava/lang/String;

    rsub-int/lit8 p1, p1, 0x19

    add-int/lit8 p2, p2, 0x4

    rsub-int/lit8 p0, p0, 0x75

    const/4 v4, 0x0

    sget-object v5, Lbr/com/itau/security/commons/binary/ByteUtils;->a:[B

    new-array v1, p1, [B

    if-nez v5, :cond_15

    move v2, p1

    move v3, p0

    :goto_11
    add-int/lit8 p2, p2, 0x1

    add-int p0, v2, v3

    :cond_15
    move v2, v4

    int-to-byte v3, p0

    add-int/lit8 v4, v4, 0x1

    aput-byte v3, v1, v2

    if-ne v4, p1, :cond_22

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_22
    move v2, p0

    aget-byte v3, v5, p2

    goto :goto_11
.end method

.method public static b64Decode(Ljava/lang/String;)[B
    .registers 2

    .line 124
    const/4 v0, 0x2

    invoke-static {p0, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    return-object v0
.end method

.method public static b64Decode([BI)[B
    .registers 3

    .line 128
    invoke-static {p0, p1}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v0

    return-object v0
.end method

.method public static b64Encode(Ljava/lang/String;)Ljava/lang/String;
    .registers 3

    .line 136
    invoke-static {p0}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lbr/com/itau/security/commons/binary/ByteUtils;->b64Encode([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b64Encode([B)Ljava/lang/String;
    .registers 2

    .line 140
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->b64Encode([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b64Encode([BI)Ljava/lang/String;
    .registers 3

    .line 144
    invoke-static {p0, p1}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->to([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static bytes2hexStr([B)Ljava/lang/String;
    .registers 7

    .line 28
    if-nez p0, :cond_4

    .line 29
    const/4 v0, 0x0

    return-object v0

    .line 31
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    const/4 v5, 0x0

    :goto_a
    array-length v0, p0

    if-ge v5, v0, :cond_35

    .line 36
    aget-byte v0, p0, v5

    ushr-int/lit8 v0, v0, 0x4

    and-int/lit8 v3, v0, 0xf

    .line 37
    const/4 v4, 0x0

    .line 39
    :cond_14
    if-ltz v3, :cond_21

    const/16 v0, 0x9

    if-gt v3, v0, :cond_21

    .line 40
    add-int/lit8 v0, v3, 0x30

    int-to-char v0, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_29

    .line 42
    :cond_21
    add-int/lit8 v0, v3, -0xa

    add-int/lit8 v0, v0, 0x61

    int-to-char v0, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 43
    :goto_29
    aget-byte v0, p0, v5

    and-int/lit8 v3, v0, 0xf

    .line 44
    move v0, v4

    add-int/lit8 v4, v4, 0x1

    if-lez v0, :cond_14

    .line 35
    add-int/lit8 v5, v5, 0x1

    goto :goto_a

    .line 46
    :cond_35
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static from(Ljava/lang/String;)[B
    .registers 5

    .line 97
    :try_start_0
    sget-object v0, Lbr/com/itau/security/commons/binary/ByteUtils;->a:[B

    const/16 v1, 0xf

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    or-int/lit8 v1, v0, 0x14

    int-to-byte v1, v1

    add-int/lit8 v2, v1, 0x1

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/commons/binary/ByteUtils;->a(IIS)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_18
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_18} :catch_1a

    move-result-object v0

    return-object v0

    .line 98
    .line 99
    :catch_1a
    new-instance v0, Lbr/com/itau/security/commons/exception/UnsupportedEncodingException;

    sget-object v1, Lbr/com/itau/security/commons/binary/ByteUtils;->a:[B

    const/16 v2, 0x11

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/commons/binary/ByteUtils;->a:[B

    const/16 v3, 0xf

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    const/16 v3, 0x31

    invoke-static {v3, v1, v2}, Lbr/com/itau/security/commons/binary/ByteUtils;->a(IIS)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/security/commons/exception/UnsupportedEncodingException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static hexStr2Bytes(Ljava/lang/String;)[B
    .registers 7

    .line 19
    new-instance v0, Ljava/math/BigInteger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lbr/com/itau/security/commons/binary/ByteUtils;->a:[B

    const/4 v3, 0x1

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    sget-object v3, Lbr/com/itau/security/commons/binary/ByteUtils;->a:[B

    const/16 v4, 0x2c

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    const/16 v4, 0x31

    invoke-static {v2, v3, v4}, Lbr/com/itau/security/commons/binary/ByteUtils;->a(IIS)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object p0

    .line 21
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    new-array v5, v0, [B

    .line 22
    array-length v0, v5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0, v1, v5, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 23
    return-object v5
.end method

.method public static join(Ljava/util/List;B)[B
    .registers 8

    .line 80
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 81
    const/4 v5, 0x0

    :goto_6
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v5, v0, :cond_43

    .line 83
    :try_start_c
    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-virtual {v4, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_15} :catch_16

    .line 86
    goto :goto_35

    .line 84
    .line 85
    :catch_16
    new-instance v0, Lbr/com/itau/security/commons/exception/CommonSecurityException;

    sget-object v1, Lbr/com/itau/security/commons/binary/ByteUtils;->a:[B

    const/16 v2, 0xf

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/commons/binary/ByteUtils;->a:[B

    const/16 v3, 0x17

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    const/16 v3, 0x32

    invoke-static {v3, v1, v2}, Lbr/com/itau/security/commons/binary/ByteUtils;->a(IIS)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/security/commons/exception/CommonSecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88
    :goto_35
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v5, v0, :cond_40

    .line 89
    invoke-virtual {v4, p1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 81
    :cond_40
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 92
    :cond_43
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public static join([B[B)[B
    .registers 6

    .line 72
    array-length v0, p0

    array-length v1, p1

    add-int/2addr v0, v1

    new-array v3, v0, [B

    .line 73
    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p0, v1, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 74
    array-length v0, p0

    array-length v1, p1

    const/4 v2, 0x0

    invoke-static {p1, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 75
    return-object v3
.end method

.method public static split([BB)Ljava/util/List;
    .registers 7

    .line 51
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 52
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 54
    const/4 v3, 0x0

    :goto_b
    array-length v0, p0

    if-ge v3, v0, :cond_24

    .line 55
    aget-byte v0, p0, v3

    .line 56
    move v4, v0

    if-ne v0, p1, :cond_1e

    .line 57
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->reset()V

    goto :goto_21

    .line 60
    :cond_1e
    invoke-virtual {v2, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 54
    :goto_21
    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    .line 64
    :cond_24
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v0

    if-lez v0, :cond_31

    .line 65
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    :cond_31
    return-object v1
.end method

.method public static to([B)Ljava/lang/String;
    .registers 4

    .line 104
    sget-object v0, Lbr/com/itau/security/commons/binary/ByteUtils;->a:[B

    const/16 v1, 0xf

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    or-int/lit8 v1, v0, 0x14

    int-to-byte v1, v1

    add-int/lit8 v2, v1, 0x1

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/commons/binary/ByteUtils;->a(IIS)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->to([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static to([BLjava/lang/String;)Ljava/lang/String;
    .registers 6

    .line 109
    if-nez p1, :cond_8

    .line 110
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p0}, Ljava/lang/String;-><init>([B)V

    return-object v0

    .line 113
    :cond_8
    :try_start_8
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p0, p1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_d
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_8 .. :try_end_d} :catch_e

    return-object v0

    .line 114
    .line 115
    :catch_e
    new-instance v0, Lbr/com/itau/security/commons/exception/UnsupportedEncodingException;

    sget-object v1, Lbr/com/itau/security/commons/binary/ByteUtils;->a:[B

    const/16 v2, 0x11

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/commons/binary/ByteUtils;->a:[B

    const/16 v3, 0xf

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    const/16 v3, 0x31

    invoke-static {v3, v1, v2}, Lbr/com/itau/security/commons/binary/ByteUtils;->a(IIS)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/security/commons/exception/UnsupportedEncodingException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

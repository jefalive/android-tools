.class public Lbr/com/itau/textovoz/SearchItau;
.super Ljava/lang/Object;
.source "SearchItau.java"


# static fields
.field private static searchController:Lbr/com/itau/textovoz/controller/SearchController;


# direct methods
.method public static getApi()Lbr/com/itau/textovoz/controller/SearchController;
    .registers 1

    .line 21
    invoke-static {}, Lbr/com/itau/textovoz/SearchItau;->init()V

    .line 22
    sget-object v0, Lbr/com/itau/textovoz/SearchItau;->searchController:Lbr/com/itau/textovoz/controller/SearchController;

    return-object v0
.end method

.method public static getBackendClientApi()Lbr/com/itau/textovoz/api/Api;
    .registers 3

    .line 27
    const-class v0, Lbr/com/itau/textovoz/api/Api;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->api(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/textovoz/api/Api;

    .line 29
    .local v2, "api":Lbr/com/itau/textovoz/api/Api;
    if-nez v2, :cond_13

    .line 30
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "BackendClient est\u00e1 null. Verifique se o BackendClient foi inicializado antes"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 34
    :cond_13
    return-object v2
.end method

.method public static init()V
    .registers 1

    .line 15
    sget-object v0, Lbr/com/itau/textovoz/SearchItau;->searchController:Lbr/com/itau/textovoz/controller/SearchController;

    if-nez v0, :cond_b

    .line 16
    new-instance v0, Lbr/com/itau/textovoz/controller/SearchController;

    invoke-direct {v0}, Lbr/com/itau/textovoz/controller/SearchController;-><init>()V

    sput-object v0, Lbr/com/itau/textovoz/SearchItau;->searchController:Lbr/com/itau/textovoz/controller/SearchController;

    .line 18
    :cond_b
    return-void
.end method

.class Lcom/google/android/gms/internal/zzeg$1$1$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/internal/zzeg$1$1;->zzeo()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic zzBh:Lcom/google/android/gms/internal/zzeg$1$1;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/zzeg$1$1;)V
    .registers 2

    iput-object p1, p0, Lcom/google/android/gms/internal/zzeg$1$1$1;->zzBh:Lcom/google/android/gms/internal/zzeg$1$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    iget-object v0, p0, Lcom/google/android/gms/internal/zzeg$1$1$1;->zzBh:Lcom/google/android/gms/internal/zzeg$1$1;

    iget-object v0, v0, Lcom/google/android/gms/internal/zzeg$1$1;->zzBg:Lcom/google/android/gms/internal/zzeg$1;

    iget-object v0, v0, Lcom/google/android/gms/internal/zzeg$1;->zzBe:Lcom/google/android/gms/internal/zzeg;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzeg;->zzc(Lcom/google/android/gms/internal/zzeg;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    :try_start_b
    iget-object v0, p0, Lcom/google/android/gms/internal/zzeg$1$1$1;->zzBh:Lcom/google/android/gms/internal/zzeg$1$1;

    iget-object v0, v0, Lcom/google/android/gms/internal/zzeg$1$1;->zzBg:Lcom/google/android/gms/internal/zzeg$1;

    iget-object v0, v0, Lcom/google/android/gms/internal/zzeg$1;->zzBd:Lcom/google/android/gms/internal/zzeg$zze;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzeg$zze;->getStatus()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_25

    iget-object v0, p0, Lcom/google/android/gms/internal/zzeg$1$1$1;->zzBh:Lcom/google/android/gms/internal/zzeg$1$1;

    iget-object v0, v0, Lcom/google/android/gms/internal/zzeg$1$1;->zzBg:Lcom/google/android/gms/internal/zzeg$1;

    iget-object v0, v0, Lcom/google/android/gms/internal/zzeg$1;->zzBd:Lcom/google/android/gms/internal/zzeg$zze;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzeg$zze;->getStatus()I
    :try_end_21
    .catchall {:try_start_b .. :try_end_21} :catchall_3f

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_27

    :cond_25
    monitor-exit v2

    return-void

    :cond_27
    :try_start_27
    iget-object v0, p0, Lcom/google/android/gms/internal/zzeg$1$1$1;->zzBh:Lcom/google/android/gms/internal/zzeg$1$1;

    iget-object v0, v0, Lcom/google/android/gms/internal/zzeg$1$1;->zzBg:Lcom/google/android/gms/internal/zzeg$1;

    iget-object v0, v0, Lcom/google/android/gms/internal/zzeg$1;->zzBd:Lcom/google/android/gms/internal/zzeg$zze;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzeg$zze;->reject()V

    new-instance v0, Lcom/google/android/gms/internal/zzeg$1$1$1$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/zzeg$1$1$1$1;-><init>(Lcom/google/android/gms/internal/zzeg$1$1$1;)V

    invoke-static {v0}, Lcom/google/android/gms/internal/zzir;->runOnUiThread(Ljava/lang/Runnable;)V

    const-string v0, "Could not receive loaded message in a timely manner. Rejecting."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V
    :try_end_3d
    .catchall {:try_start_27 .. :try_end_3d} :catchall_3f

    monitor-exit v2

    goto :goto_42

    :catchall_3f
    move-exception v3

    monitor-exit v2

    throw v3

    :goto_42
    return-void
.end method

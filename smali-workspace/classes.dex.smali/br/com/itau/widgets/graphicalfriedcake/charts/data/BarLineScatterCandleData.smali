.class public abstract Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleData;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;
.source "BarLineScatterCandleData.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleDataSet<+Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;>Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 16
    invoke-direct {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;-><init>()V

    .line 17
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .registers 2
    .param p1, "xVals"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/String;>;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;-><init>(Ljava/util/List;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .registers 3
    .param p1, "xVals"    # Ljava/util/List;
    .param p2, "sets"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/String;>;Ljava/util/List<TT;>;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;-><init>(Ljava/util/List;Ljava/util/List;)V

    .line 30
    return-void
.end method

.method public constructor <init>([Ljava/lang/String;)V
    .registers 2
    .param p1, "xVals"    # [Ljava/lang/String;

    .line 24
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;-><init>([Ljava/lang/String;)V

    .line 25
    return-void
.end method

.method public constructor <init>([Ljava/lang/String;Ljava/util/List;)V
    .registers 3
    .param p1, "xVals"    # [Ljava/lang/String;
    .param p2, "sets"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([Ljava/lang/String;Ljava/util/List<TT;>;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;-><init>([Ljava/lang/String;Ljava/util/List;)V

    .line 35
    return-void
.end method

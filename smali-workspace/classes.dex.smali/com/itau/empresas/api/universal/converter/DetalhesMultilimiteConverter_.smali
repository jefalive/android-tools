.class public final Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter_;
.super Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter;
.source "DetalhesMultilimiteConverter_.java"


# static fields
.field private static instance_:Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter_;


# instance fields
.field private context_:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 19
    invoke-direct {p0}, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter_;->context_:Landroid/content/Context;

    .line 21
    return-void
.end method

.method public static getInstance_(Landroid/content/Context;)Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter_;
    .registers 4
    .param p0, "context"    # Landroid/content/Context;

    .line 24
    sget-object v0, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter_;->instance_:Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter_;

    if-nez v0, :cond_1c

    .line 25
    const/4 v0, 0x0

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v2

    .line 26
    .local v2, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    new-instance v0, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter_;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter_;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter_;->instance_:Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter_;

    .line 27
    sget-object v0, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter_;->instance_:Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter_;

    invoke-direct {v0}, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter_;->init_()V

    .line 28
    invoke-static {v2}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 30
    .end local v2    # "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    :cond_1c
    sget-object v0, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter_;->instance_:Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter_;

    return-object v0
.end method

.method private init_()V
    .registers 1

    .line 34
    return-void
.end method

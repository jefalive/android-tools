.class public final Lcom/itau/empresas/ui/util/CollectionUtils;
.super Ljava/lang/Object;
.source "CollectionUtils.java"


# direct methods
.method public static varargs contains(Ljava/lang/Object;[Ljava/lang/Object;)Z
    .registers 7
    .param p0, "value"    # Ljava/lang/Object;
    .param p1, "list"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(TT;[TT;)Z"
        }
    .end annotation

    .line 9
    move-object v1, p1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v2, :cond_12

    aget-object v4, v1, v3

    .line 10
    .local v4, "s":Ljava/lang/Object;, "TT;"
    invoke-virtual {p0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 11
    const/4 v0, 0x1

    return v0

    .line 9
    .end local v4    # "s":Ljava/lang/Object;, "TT;"
    .end local v4
    :cond_f
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 14
    :cond_12
    const/4 v0, 0x0

    return v0
.end method

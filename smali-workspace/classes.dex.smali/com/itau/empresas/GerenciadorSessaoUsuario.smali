.class public Lcom/itau/empresas/GerenciadorSessaoUsuario;
.super Ljava/lang/Object;
.source "GerenciadorSessaoUsuario.java"


# instance fields
.field private gson:Lcom/google/gson/Gson;

.field private sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/GerenciadorSessaoUsuario;->gson:Lcom/google/gson/Gson;

    .line 21
    iget-object v0, p0, Lcom/itau/empresas/GerenciadorSessaoUsuario;->sharedPreferences:Landroid/content/SharedPreferences;

    if-nez v0, :cond_18

    .line 22
    new-instance v0, Lbr/com/itau/security/securestorage/SecureStorage;

    const-string v1, "sessaoUsuario"

    invoke-direct {v0, p1, v1}, Lbr/com/itau/security/securestorage/SecureStorage;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/itau/empresas/GerenciadorSessaoUsuario;->setSharedPreferences(Landroid/content/SharedPreferences;)V

    .line 24
    :cond_18
    return-void
.end method


# virtual methods
.method public buscaPreferencia(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .registers 6
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "clazz"    # Ljava/lang/Class;

    .line 31
    iget-object v0, p0, Lcom/itau/empresas/GerenciadorSessaoUsuario;->sharedPreferences:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 32
    .local v2, "json":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/GerenciadorSessaoUsuario;->gson:Lcom/google/gson/Gson;

    invoke-virtual {v0, v2, p2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public buscaPreferencia(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .param p1, "chave"    # Ljava/lang/String;

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/GerenciadorSessaoUsuario;->sharedPreferences:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public limpaPreferencia()V
    .registers 2

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/GerenciadorSessaoUsuario;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 56
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 57
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 58
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 59
    return-void
.end method

.method public removerPreferencia(Ljava/lang/String;)V
    .registers 3
    .param p1, "chave"    # Ljava/lang/String;

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/GerenciadorSessaoUsuario;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 63
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 64
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 65
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 66
    return-void
.end method

.method public salvaPreferencia(Ljava/lang/String;Ljava/lang/Object;)V
    .registers 5
    .param p1, "chave"    # Ljava/lang/String;
    .param p2, "object"    # Ljava/lang/Object;

    .line 40
    iget-object v0, p0, Lcom/itau/empresas/GerenciadorSessaoUsuario;->gson:Lcom/google/gson/Gson;

    invoke-virtual {v0, p2}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 41
    .local v1, "json":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/GerenciadorSessaoUsuario;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 42
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 43
    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 44
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 45
    return-void
.end method

.method public salvaPreferencia(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "chave"    # Ljava/lang/String;
    .param p2, "valor"    # Ljava/lang/String;

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/GerenciadorSessaoUsuario;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 49
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 50
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 51
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 52
    return-void
.end method

.method public setSharedPreferences(Landroid/content/SharedPreferences;)V
    .registers 2
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;

    .line 27
    iput-object p1, p0, Lcom/itau/empresas/GerenciadorSessaoUsuario;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 28
    return-void
.end method

.class final enum Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;
.super Ljava/lang/Enum;
.source "ITokenActivity.java"

# interfaces
.implements Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/itoken/ITokenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Estados"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;>;Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

.field public static final enum ITOKEN_ASSOCIAR:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

.field public static final enum ITOKEN_BLOQUEADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

.field public static final enum ITOKEN_CANCELAMENTO_OUTRO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

.field public static final enum ITOKEN_CANCELAMENTO_SIMPLES:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

.field public static final enum ITOKEN_INSTALACAO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

.field public static final enum ITOKEN_INSTALACAO_CANCELADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

.field public static final enum ITOKEN_INSTALADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

.field public static final enum ITOKEN_INSTALADO_ASSOCIADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

.field public static final enum ITOKEN_INSTALADO_NOVO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

.field public static final enum ITOKEN_LIBERAR:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

.field public static final enum ITOKEN_LIBERAR_OUTRO_APARELHO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

.field public static final enum ITOKEN_LIBERAR_OUTRO_OPERADOR:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

.field public static final enum ITOKEN_NOVA_INSTALACAO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 406
    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const-string v1, "ITOKEN_INSTALACAO"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALACAO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const-string v1, "ITOKEN_INSTALACAO_CANCELADO"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALACAO_CANCELADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const-string v1, "ITOKEN_INSTALADO"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const-string v1, "ITOKEN_INSTALADO_NOVO"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALADO_NOVO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const-string v1, "ITOKEN_INSTALADO_ASSOCIADO"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALADO_ASSOCIADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const-string v1, "ITOKEN_CANCELAMENTO_SIMPLES"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_CANCELAMENTO_SIMPLES:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const-string v1, "ITOKEN_CANCELAMENTO_OUTRO"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_CANCELAMENTO_OUTRO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const-string v1, "ITOKEN_BLOQUEADO"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_BLOQUEADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const-string v1, "ITOKEN_LIBERAR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_LIBERAR:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const-string v1, "ITOKEN_LIBERAR_OUTRO_APARELHO"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_LIBERAR_OUTRO_APARELHO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const-string v1, "ITOKEN_LIBERAR_OUTRO_OPERADOR"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_LIBERAR_OUTRO_OPERADOR:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const-string v1, "ITOKEN_NOVA_INSTALACAO"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_NOVA_INSTALACAO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const-string v1, "ITOKEN_ASSOCIAR"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_ASSOCIAR:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    .line 405
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALACAO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALACAO_CANCELADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALADO_NOVO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_INSTALADO_ASSOCIADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_CANCELAMENTO_SIMPLES:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_CANCELAMENTO_OUTRO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_BLOQUEADO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_LIBERAR:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_LIBERAR_OUTRO_APARELHO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_LIBERAR_OUTRO_OPERADOR:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_NOVA_INSTALACAO:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->ITOKEN_ASSOCIAR:Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->$VALUES:[Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 405
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 405
    const-class v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    return-object v0
.end method

.method public static values()[Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;
    .registers 1

    .line 405
    sget-object v0, Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->$VALUES:[Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    invoke-virtual {v0}, [Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/itau/empresas/feature/itoken/ITokenActivity$Estados;

    return-object v0
.end method

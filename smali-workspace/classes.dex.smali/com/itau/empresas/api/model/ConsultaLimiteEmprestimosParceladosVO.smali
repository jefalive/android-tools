.class public Lcom/itau/empresas/api/model/ConsultaLimiteEmprestimosParceladosVO;
.super Ljava/lang/Object;
.source "ConsultaLimiteEmprestimosParceladosVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private codigoPlano:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_plano"
    .end annotation
.end field

.field private indicadorSeguro:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_seguro"
    .end annotation
.end field

.field private indicadorTransbordo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_transbordo"
    .end annotation
.end field

.field private numeroPlano:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_plano"
    .end annotation
.end field

.field private qtdOcorrencias:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "qtd_ocorrencias"
    .end annotation
.end field

.field private valorMaximoContratacao:Ljava/lang/Double;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_maximo_contratacao"
    .end annotation
.end field

.field private valorMaximoTransbordo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_maximo_transbordo"
    .end annotation
.end field

.field private valorMinimoContratacao:Ljava/lang/Double;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_minimo_contratacao"
    .end annotation
.end field

.field private valorMinimoTransbordo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_minimo_transbordo"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

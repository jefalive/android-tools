.class Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$2;
.super Ljava/lang/Object;
.source "TransferenciaRecebidasAdapter.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->swipeTouchListener(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

.field final synthetic val$itemVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    .line 137
    iput-object p1, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$2;->this$0:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    iput-object p2, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$2;->val$itemVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .registers 5
    .param p1, "view"    # Landroid/view/View;

    .line 140
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$2;->this$0:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    # getter for: Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->isSwiped:Z
    invoke-static {v0}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->access$100(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;)Z

    move-result v0

    if-nez v0, :cond_27

    .line 141
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$2;->this$0:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    .line 142
    invoke-static {}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog_;->builder()Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog_$FragmentBuilder_;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$2;->val$itemVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    .line 143
    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog_$FragmentBuilder_;->transferenciaRecebidasVO(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;)Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog_$FragmentBuilder_;

    move-result-object v1

    .line 144
    invoke-virtual {v1}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$2;->this$0:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    .line 145
    # getter for: Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->access$200(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasPreviewDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    move-result-object v1

    .line 141
    # setter for: Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->baseDialogPreview:Lcom/itau/empresas/ui/dialog/BaseDialog;
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->access$002(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;Lcom/itau/empresas/ui/dialog/BaseDialog;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 147
    :cond_27
    const/4 v0, 0x0

    return v0
.end method

.class public Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "RecargaConfirmacaoCadastroContatoActivity.java"


# instance fields
.field adicionado:Ljava/lang/Boolean;

.field app:Lcom/itau/empresas/CustomApplication;

.field botaoAutorizarCadastroContato:Landroid/widget/Button;

.field botaoIncluirCadastroContato:Landroid/widget/Button;

.field contatoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

.field escolhePeriodo:Lbr/com/itau/library/LinearValuePickerView;

.field operadora:Ljava/lang/String;

.field operadoraVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;

.field recargaController:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

.field private recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

.field regiao:Ljava/lang/String;

.field regiaoVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;

.field root:Landroid/widget/LinearLayout;

.field textoConfirmacao:Landroid/widget/TextView;

.field textoEscolhaPeriodo:Landroid/widget/TextView;

.field textoNomeContato:Landroid/widget/TextView;

.field textoNumeroContato:Landroid/widget/TextView;

.field textoOperadoraContato:Landroid/widget/TextView;

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field private topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

.field valorRecargaContato:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 52
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private confirmarSimulacao(Z)V
    .registers 5
    .param p1, "autorizante"    # Z

    .line 96
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->isValido()Z

    move-result v0

    if-nez v0, :cond_7

    .line 97
    return-void

    .line 100
    :cond_7
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    if-eqz v0, :cond_10

    .line 101
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismiss()V

    .line 102
    :cond_10
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->mostraDialogoDeProgresso()V

    .line 104
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->criarRecargaInputVO()Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    .line 105
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->getPessoaJuridica()Lcom/itau/empresas/api/model/DadosPJVO;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/itau/empresas/api/model/DadosPJVO;->setAutorizacao(Z)V

    .line 107
    if-eqz p1, :cond_49

    .line 108
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->recargaController:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->contatoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 110
    invoke-virtual {v2}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getDdd()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->contatoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getTelefone()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    .line 109
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->simulaRecargaAutorizar(Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)V

    goto :goto_6d

    .line 113
    :cond_49
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->recargaController:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->contatoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 114
    invoke-virtual {v2}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getDdd()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->contatoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getTelefone()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->simulaRecarga(Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)V

    .line 117
    :goto_6d
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 226
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 228
    const v1, 0x7f0706b8

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 229
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 230
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 231
    return-void
.end method

.method private criarRecargaInputVO()Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;
    .registers 6

    .line 120
    new-instance v1, Lcom/itau/empresas/api/model/DadosClienteVO;

    invoke-direct {v1}, Lcom/itau/empresas/api/model/DadosClienteVO;-><init>()V

    .line 121
    .local v1, "dadosClienteVO":Lcom/itau/empresas/api/model/DadosClienteVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/itau/empresas/api/model/DadosClienteVO;->setAgencia(Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/itau/empresas/api/model/DadosClienteVO;->setConta(Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/itau/empresas/api/model/DadosClienteVO;->setDigitoVerificadorDaConta(Ljava/lang/String;)V

    .line 125
    new-instance v2, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    invoke-direct {v2}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;-><init>()V

    .line 126
    .local v2, "recargaInputVO":Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;
    new-instance v3, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;

    invoke-direct {v3}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;-><init>()V

    .line 127
    .local v3, "recargaVO":Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->operadora:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->setNomeOperadora(Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->regiao:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->setNomeRegiao(Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->valorRecargaContato:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->getValor()Lorg/joda/money/Money;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/money/Money;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->setValorRecarga(Ljava/lang/Integer;)V

    .line 131
    new-instance v4, Lcom/itau/empresas/api/model/DadosPJVO;

    invoke-direct {v4}, Lcom/itau/empresas/api/model/DadosPJVO;-><init>()V

    .line 132
    .local v4, "dadosPJVO":Lcom/itau/empresas/api/model/DadosPJVO;
    const-string v0, "PEX"

    invoke-virtual {v4, v0}, Lcom/itau/empresas/api/model/DadosPJVO;->setProduto(Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/itau/empresas/api/model/DadosPJVO;->setCodigoOperador(Ljava/lang/String;)V

    .line 135
    invoke-virtual {v2, v1}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->setCliente(Lcom/itau/empresas/api/model/DadosClienteVO;)V

    .line 136
    invoke-virtual {v2, v3}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->setRecarga(Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;)V

    .line 137
    invoke-virtual {v2, v4}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->setPessoaJuridica(Lcom/itau/empresas/api/model/DadosPJVO;)V

    .line 139
    return-object v2
.end method

.method private disparaEventoAnalytics()V
    .registers 5

    .line 191
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v3

    .line 192
    .line 193
    .local v3, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    const v0, 0x7f0700f1

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 194
    const v1, 0x7f0700ce

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 195
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 192
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    return-void
.end method

.method private static getWarningMessage(Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;)Ljava/lang/String;
    .registers 5
    .param p0, "resultadoSimulacaoRecargaVO"    # Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse<Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;>;)Ljava/lang/String;"
        }
    .end annotation

    .line 255
    if-nez p0, :cond_4

    .line 256
    const/4 v0, 0x0

    return-object v0

    .line 259
    :cond_4
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;->getHeaders()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/sdk/android/core/model/KeyValue;

    .line 260
    .local v2, "kv":Lbr/com/itau/sdk/android/core/model/KeyValue;
    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/model/KeyValue;->getKey()Ljava/lang/String;

    move-result-object v3

    .line 261
    .local v3, "key":Ljava/lang/String;
    if-eqz v3, :cond_2c

    const-string v0, "Warning"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 262
    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/model/KeyValue;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 263
    .end local v2    # "kv":Lbr/com/itau/sdk/android/core/model/KeyValue;
    .end local v3    # "key":Ljava/lang/String;
    :cond_2c
    goto :goto_c

    .line 264
    :cond_2d
    const/4 v0, 0x0

    return-object v0
.end method

.method private isValido()Z
    .registers 4

    .line 84
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->valorRecargaContato:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->validaValor()Z

    move-result v0

    if-nez v0, :cond_26

    .line 85
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->valorRecargaContato:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->isViewCarregada()Z

    move-result v0

    if-nez v0, :cond_24

    .line 86
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->root:Landroid/widget/LinearLayout;

    const v1, 0x7f070687

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 89
    :cond_24
    const/4 v0, 0x0

    return v0

    .line 92
    :cond_26
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method protected aoClicarAutorizar()V
    .registers 2

    .line 160
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->confirmarSimulacao(Z)V

    .line 161
    return-void
.end method

.method protected aoClicarIncluir()V
    .registers 2

    .line 155
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->confirmarSimulacao(Z)V

    .line 156
    return-void
.end method

.method carregaElementosDaTela()V
    .registers 4

    .line 165
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->constroiToolbar()V

    .line 167
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->operadoraVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->operadora:Ljava/lang/String;

    .line 169
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->regiaoVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;

    if-nez v0, :cond_14

    .line 170
    const-string v0, ""

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->regiao:Ljava/lang/String;

    goto :goto_1c

    .line 172
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->regiaoVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;->getRegiao()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->regiao:Ljava/lang/String;

    .line 175
    :goto_1c
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->textoNomeContato:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->contatoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getApelido()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->textoNumeroContato:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->contatoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 177
    invoke-virtual {v1}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getDdd()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->contatoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 178
    invoke-virtual {v2}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getTelefone()Ljava/lang/String;

    move-result-object v2

    .line 177
    invoke-static {p0, v1, v2}, Lcom/itau/empresas/ui/util/ViewUtils;->formataNumeroTelefonico(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 176
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->textoOperadoraContato:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->contatoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 180
    invoke-virtual {v2}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->contatoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 181
    invoke-virtual {v2}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getEstadoOperadora()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 179
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->textoConfirmacao:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->adicionado:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_72

    const v1, 0x7f0706b9

    goto :goto_75

    :cond_72
    const v1, 0x7f0706ba

    :goto_75
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 185
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->valorRecargaContato:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->operadora:Ljava/lang/String;

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->regiao:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->carregarValores(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->disparaEventoAnalytics()V

    .line 188
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 3
    .param p1, "menu"    # Landroid/view/Menu;

    .line 215
    invoke-static {p0, p1}, Lcom/itau/empresas/ui/util/PoyntUtils;->onCreateOptionsMenu(Landroid/app/Activity;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;)V
    .registers 5
    .param p1, "resultadoSimulacaoRecargaVO"    # Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse<Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;>;)V"
        }
    .end annotation

    .line 238
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->escondeDialogoDeProgresso()V

    .line 240
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->criarRecargaInputVO()Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    move-result-object v0

    if-nez v0, :cond_a

    .line 241
    return-void

    .line 243
    :cond_a
    invoke-static {p1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->getWarningMessage(Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;)Ljava/lang/String;

    move-result-object v2

    .line 245
    .local v2, "warning":Ljava/lang/String;
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->adicionado:Ljava/lang/Boolean;

    .line 246
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->novo(Z)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->textoNomeContato:Landroid/widget/TextView;

    .line 247
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->nome(Ljava/lang/String;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    .line 248
    invoke-virtual {v0, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->aviso(Ljava/lang/String;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    .line 249
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;->getData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->resultadoSimulacaoRecargaVO(Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    .line 250
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->recargaInputVO(Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 251
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 8
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 269
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 270
    return-void

    .line 273
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->escondeDialogoDeProgresso()V

    .line 274
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v3

    .line 275
    .local v3, "error":Lbr/com/itau/sdk/android/core/model/ErrorDTO;
    if-eqz v3, :cond_65

    .line 276
    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getCodigo()Ljava/lang/String;

    move-result-object v4

    const/4 v5, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_66

    goto :goto_34

    :sswitch_21
    const-string v0, "limite_diario"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    const/4 v5, 0x0

    goto :goto_34

    :sswitch_2b
    const-string v0, "saldo_insuficiente"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    const/4 v5, 0x1

    :cond_34
    :goto_34
    sparse-switch v5, :sswitch_data_70

    goto :goto_54

    .line 278
    :sswitch_38
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;->LDP:Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

    .line 279
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->limite(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    .line 280
    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 281
    goto :goto_65

    .line 283
    :sswitch_46
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;->CHEQUE_ESPECIAL:Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

    .line 284
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->limite(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    .line 285
    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 286
    goto :goto_65

    .line 288
    :goto_54
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 289
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 292
    :cond_65
    :goto_65
    return-void

    :sswitch_data_66
    .sparse-switch
        -0x2479db0f -> :sswitch_21
        -0x59e0060 -> :sswitch_2b
    .end sparse-switch

    :sswitch_data_70
    .sparse-switch
        0x0 -> :sswitch_38
        0x1 -> :sswitch_46
    .end sparse-switch
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$ValorSelecionado;)V
    .registers 4
    .param p1, "valorSelecionado"    # Lcom/itau/empresas/Evento$ValorSelecionado;

    .line 143
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->botaoIncluirCadastroContato:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 144
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->application:Lcom/itau/empresas/CustomApplication;

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_RECARGA_AUTORIZAR:Lcom/itau/empresas/CustomApplication$Exibicao;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->verificarPermissao(Lcom/itau/empresas/CustomApplication$Exibicao;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 145
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->botaoAutorizarCadastroContato:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 146
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->botaoAutorizarCadastroContato:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_2a

    .line 148
    :cond_1d
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->botaoAutorizarCadastroContato:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 149
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->botaoAutorizarCadastroContato:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 151
    :goto_2a
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 220
    invoke-static {p0, p1}, Lcom/itau/empresas/ui/util/PoyntUtils;->onOptionsItemSelected(Lcom/itau/empresas/ui/activity/BaseActivity;Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 221
    const/4 v0, 0x1

    return v0

    .line 222
    :cond_8
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 202
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "listaOperadoras"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "contatosRecarga"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "cadastrarContatoRecarga"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "alterarContatoRecarga"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "deletarContatoRecarga"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "valoresOperadora"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "simularRecarga"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "exibicao_recarga_autorizar"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string v1, "efetivarRecarga"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

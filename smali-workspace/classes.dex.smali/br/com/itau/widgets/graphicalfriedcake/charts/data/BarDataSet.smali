.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleDataSet;
.source "BarDataSet.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleDataSet<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;>;"
    }
.end annotation


# instance fields
.field private mBarShadowColor:I

.field private mBarSpace:F

.field private mEntryCountStacks:I

.field private mHighLightAlpha:I

.field private mStackLabels:[Ljava/lang/String;

.field private mStackSize:I


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;)V
    .registers 6
    .param p1, "yVals"    # Ljava/util/List;
    .param p2, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;>;Ljava/lang/String;)V"
        }
    .end annotation

    .line 47
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 14
    const v0, 0x3e19999a    # 0.15f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mBarSpace:F

    .line 21
    const/4 v0, 0x1

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mStackSize:I

    .line 25
    const/16 v0, 0xd7

    const/16 v1, 0xd7

    const/16 v2, 0xd7

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mBarShadowColor:I

    .line 29
    const/16 v0, 0x78

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mHighLightAlpha:I

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mEntryCountStacks:I

    .line 41
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Stack"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mStackLabels:[Ljava/lang/String;

    .line 50
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mHighLightColor:I

    .line 53
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->calcStackSize(Ljava/util/List;)V

    .line 54
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->calcEntryCountIncludingStacks(Ljava/util/List;)V

    .line 55
    return-void
.end method

.method private calcEntryCountIncludingStacks(Ljava/util/List;)V
    .registers 6
    .param p1, "yVals"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;>;)V"
        }
    .end annotation

    .line 91
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mEntryCountStacks:I

    .line 94
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_26

    .line 97
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->getVals()[F

    move-result-object v3

    .line 100
    .local v3, "vals":[F
    if-nez v3, :cond_1d

    .line 101
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mEntryCountStacks:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mEntryCountStacks:I

    goto :goto_23

    .line 103
    :cond_1d
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mEntryCountStacks:I

    array-length v1, v3

    add-int/2addr v0, v1

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mEntryCountStacks:I

    .line 94
    .end local v3    # "vals":[F
    :goto_23
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 105
    .end local v2    # "i":I
    :cond_26
    return-void
.end method

.method private calcStackSize(Ljava/util/List;)V
    .registers 6
    .param p1, "yVals"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;>;)V"
        }
    .end annotation

    .line 115
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1e

    .line 118
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->getVals()[F

    move-result-object v3

    .line 121
    .local v3, "vals":[F
    if-eqz v3, :cond_1b

    array-length v0, v3

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mStackSize:I

    if-le v0, v1, :cond_1b

    .line 122
    array-length v0, v3

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mStackSize:I

    .line 115
    .end local v3    # "vals":[F
    :cond_1b
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 124
    .end local v2    # "i":I
    :cond_1e
    return-void
.end method


# virtual methods
.method protected calcMinMax(II)V
    .registers 9
    .param p1, "start"    # I
    .param p2, "end"    # I

    .line 129
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 132
    .local v2, "yValCount":I
    if-nez v2, :cond_9

    .line 133
    return-void

    .line 139
    :cond_9
    if-eqz p2, :cond_d

    if-lt p2, v2, :cond_10

    .line 140
    :cond_d
    add-int/lit8 v3, v2, -0x1

    .local v3, "endValue":I
    goto :goto_11

    .line 142
    .end local v3    # "endValue":I
    :cond_10
    move v3, p2

    .line 145
    .local v3, "endValue":I
    :goto_11
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mLastStart:I

    .line 146
    iput v3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mLastEnd:I

    .line 149
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mYMin:F

    .line 150
    const v0, -0x800001

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mYMax:F

    .line 153
    move v4, p1

    .local v4, "i":I
    :goto_20
    if-gt v4, v3, :cond_84

    .line 156
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;

    .line 159
    .local v5, "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;
    if-eqz v5, :cond_80

    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->getVal()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_80

    .line 162
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->getVals()[F

    move-result-object v0

    if-nez v0, :cond_5e

    .line 165
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->getVal()F

    move-result v0

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mYMin:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4d

    .line 166
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->getVal()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mYMin:F

    .line 169
    :cond_4d
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->getVal()F

    move-result v0

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mYMax:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_80

    .line 170
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->getVal()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mYMax:F

    goto :goto_80

    .line 174
    :cond_5e
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->getNegativeSum()F

    move-result v0

    neg-float v0, v0

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mYMin:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_70

    .line 175
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->getNegativeSum()F

    move-result v0

    neg-float v0, v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mYMin:F

    .line 178
    :cond_70
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->getPositiveSum()F

    move-result v0

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mYMax:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_80

    .line 179
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->getPositiveSum()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mYMax:F

    .line 153
    .end local v5    # "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;
    :cond_80
    :goto_80
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_20

    .line 185
    .end local v4    # "i":I
    :cond_84
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mYMin:F

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v0, v0, v1

    if-nez v0, :cond_93

    .line 186
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mYMin:F

    .line 187
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mYMax:F

    .line 189
    :cond_93
    return-void
.end method

.method public copy()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;>;"
        }
    .end annotation

    .line 62
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 65
    .local v1, "yVals":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_6
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_20

    .line 66
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->copy()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 70
    .end local v2    # "i":I
    :cond_20
    new-instance v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 71
    .local v2, "copied":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mColors:Ljava/util/List;

    iput-object v0, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mColors:Ljava/util/List;

    .line 72
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mStackSize:I

    iput v0, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mStackSize:I

    .line 73
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mBarSpace:F

    iput v0, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mBarSpace:F

    .line 74
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mBarShadowColor:I

    iput v0, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mBarShadowColor:I

    .line 75
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mStackLabels:[Ljava/lang/String;

    iput-object v0, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mStackLabels:[Ljava/lang/String;

    .line 76
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mHighLightColor:I

    iput v0, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mHighLightColor:I

    .line 77
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mHighLightAlpha:I

    iput v0, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mHighLightAlpha:I

    .line 80
    return-object v2
.end method

.method public getBarShadowColor()I
    .registers 2

    .line 273
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mBarShadowColor:I

    return v0
.end method

.method public getBarSpace()F
    .registers 2

    .line 240
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mBarSpace:F

    return v0
.end method

.method public getBarSpacePercent()F
    .registers 3

    .line 230
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mBarSpace:F

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    return v0
.end method

.method public getEntryCountStacks()I
    .registers 2

    .line 220
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mEntryCountStacks:I

    return v0
.end method

.method public getHighLightAlpha()I
    .registers 2

    .line 295
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mHighLightAlpha:I

    return v0
.end method

.method public getStackLabels()[Ljava/lang/String;
    .registers 2

    .line 315
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mStackLabels:[Ljava/lang/String;

    return-object v0
.end method

.method public getStackSize()I
    .registers 2

    .line 199
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mStackSize:I

    return v0
.end method

.method public isStacked()Z
    .registers 3

    .line 209
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mStackSize:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_7

    const/4 v0, 0x1

    goto :goto_8

    :cond_7
    const/4 v0, 0x0

    :goto_8
    return v0
.end method

.method public setBarShadowColor(I)V
    .registers 2
    .param p1, "color"    # I

    .line 262
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mBarShadowColor:I

    .line 263
    return-void
.end method

.method public setBarSpacePercent(F)V
    .registers 3
    .param p1, "percent"    # F

    .line 250
    const/high16 v0, 0x42c80000    # 100.0f

    div-float v0, p1, v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mBarSpace:F

    .line 251
    return-void
.end method

.method public setHighLightAlpha(I)V
    .registers 2
    .param p1, "alpha"    # I

    .line 284
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mHighLightAlpha:I

    .line 285
    return-void
.end method

.method public setStackLabels([Ljava/lang/String;)V
    .registers 2
    .param p1, "labels"    # [Ljava/lang/String;

    .line 305
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->mStackLabels:[Ljava/lang/String;

    .line 306
    return-void
.end method

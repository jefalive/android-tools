.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;
.source "YAxis.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;,
        Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;
    }
.end annotation


# instance fields
.field private mAxisDependency:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

.field public mAxisMaximum:F

.field public mAxisMinimum:F

.field public mAxisRange:F

.field protected mCustomAxisMax:F

.field protected mCustomAxisMin:F

.field public mDecimals:I

.field private mDrawTopYLabelEntry:Z

.field public mEntries:[F

.field public mEntryCount:I

.field protected mInverted:Z

.field private mLabelCount:I

.field private mPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

.field protected mShowOnlyMinMax:Z

.field protected mSpacePercentBottom:F

.field protected mSpacePercentTop:F

.field protected mStartAtZero:Z

.field protected mValueFormatter:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 94
    invoke-direct {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;-><init>()V

    .line 25
    const/4 v0, 0x0

    new-array v0, v0, [F

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    .line 34
    const/4 v0, 0x6

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mLabelCount:I

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mDrawTopYLabelEntry:Z

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mShowOnlyMinMax:Z

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mInverted:Z

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mStartAtZero:Z

    .line 49
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mCustomAxisMin:F

    .line 52
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mCustomAxisMax:F

    .line 58
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mSpacePercentTop:F

    .line 64
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mSpacePercentBottom:F

    .line 66
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisMaximum:F

    .line 67
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisMinimum:F

    .line 70
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisRange:F

    .line 73
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;->OUTSIDE_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    .line 95
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisDependency:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    .line 96
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mYOffset:F

    .line 97
    return-void
.end method

.method public constructor <init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;)V
    .registers 3
    .param p1, "position"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    .line 100
    invoke-direct {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;-><init>()V

    .line 25
    const/4 v0, 0x0

    new-array v0, v0, [F

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    .line 34
    const/4 v0, 0x6

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mLabelCount:I

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mDrawTopYLabelEntry:Z

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mShowOnlyMinMax:Z

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mInverted:Z

    .line 46
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mStartAtZero:Z

    .line 49
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mCustomAxisMin:F

    .line 52
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mCustomAxisMax:F

    .line 58
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mSpacePercentTop:F

    .line 64
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mSpacePercentBottom:F

    .line 66
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisMaximum:F

    .line 67
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisMinimum:F

    .line 70
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisRange:F

    .line 73
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;->OUTSIDE_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    .line 101
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisDependency:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    .line 102
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mYOffset:F

    .line 103
    return-void
.end method


# virtual methods
.method public getAxisDependency()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;
    .registers 2

    .line 106
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisDependency:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    return-object v0
.end method

.method public getAxisMaxValue()F
    .registers 2

    .line 253
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mCustomAxisMax:F

    return v0
.end method

.method public getAxisMinValue()F
    .registers 2

    .line 228
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mCustomAxisMin:F

    return v0
.end method

.method public getFormattedLabel(I)Ljava/lang/String;
    .registers 4
    .param p1, "index"    # I

    .line 351
    if-ltz p1, :cond_7

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    array-length v0, v0

    if-lt p1, v0, :cond_a

    .line 352
    :cond_7
    const-string v0, ""

    return-object v0

    .line 354
    :cond_a
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getValueFormatter()Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    aget v1, v1, p1

    invoke-interface {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;->getFormattedValue(F)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLabelCount()I
    .registers 2

    .line 168
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mLabelCount:I

    return v0
.end method

.method public getLabelPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;
    .registers 2

    .line 113
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    return-object v0
.end method

.method public getLongestLabel()Ljava/lang/String;
    .registers 6

    .line 330
    const-string v2, ""

    .line 332
    .local v2, "longest":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_3
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    array-length v0, v0

    if-ge v3, v0, :cond_1a

    .line 333
    invoke-virtual {p0, v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getFormattedLabel(I)Ljava/lang/String;

    move-result-object v4

    .line 335
    .local v4, "text":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_17

    .line 336
    move-object v2, v4

    .line 332
    .end local v4    # "text":Ljava/lang/String;
    :cond_17
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 339
    .end local v3    # "i":I
    :cond_1a
    return-object v2
.end method

.method public getRequiredHeightSpace(Landroid/graphics/Paint;)F
    .registers 6
    .param p1, "p"    # Landroid/graphics/Paint;

    .line 321
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mTextSize:F

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 323
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getLongestLabel()Ljava/lang/String;

    move-result-object v3

    .line 324
    .local v3, "label":Ljava/lang/String;
    invoke-static {p1, v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->calcTextHeight(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40200000    # 2.5f

    invoke-static {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getYOffset()F

    move-result v1

    add-float/2addr v0, v1

    return v0
.end method

.method public getRequiredWidthSpace(Landroid/graphics/Paint;)F
    .registers 6
    .param p1, "p"    # Landroid/graphics/Paint;

    .line 313
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mTextSize:F

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 315
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getLongestLabel()Ljava/lang/String;

    move-result-object v3

    .line 316
    .local v3, "label":Ljava/lang/String;
    invoke-static {p1, v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->calcTextWidth(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getXOffset()F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public getSpaceBottom()F
    .registers 2

    .line 308
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mSpacePercentBottom:F

    return v0
.end method

.method public getSpaceTop()F
    .registers 2

    .line 290
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mSpacePercentTop:F

    return v0
.end method

.method public getValueFormatter()Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;
    .registers 2

    .line 380
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mValueFormatter:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;

    return-object v0
.end method

.method public isDrawTopYLabelEntryEnabled()Z
    .registers 2

    .line 131
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mDrawTopYLabelEntry:Z

    return v0
.end method

.method public isInverted()Z
    .registers 2

    .line 206
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mInverted:Z

    return v0
.end method

.method public isShowOnlyMinMaxEnabled()Z
    .registers 2

    .line 187
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mShowOnlyMinMax:Z

    return v0
.end method

.method public isStartAtZeroEnabled()Z
    .registers 2

    .line 224
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mStartAtZero:Z

    return v0
.end method

.method public needsDefaultFormatter()Z
    .registers 2

    .line 390
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mValueFormatter:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;

    if-nez v0, :cond_6

    .line 391
    const/4 v0, 0x1

    return v0

    .line 392
    :cond_6
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mValueFormatter:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;

    instance-of v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/DefaultValueFormatter;

    if-eqz v0, :cond_e

    .line 393
    const/4 v0, 0x1

    return v0

    .line 395
    :cond_e
    const/4 v0, 0x0

    return v0
.end method

.method public needsOffset()Z
    .registers 3

    .line 405
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->isDrawLabelsEnabled()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 406
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getLabelPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;->OUTSIDE_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    if-ne v0, v1, :cond_16

    .line 407
    const/4 v0, 0x1

    return v0

    .line 409
    :cond_16
    const/4 v0, 0x0

    return v0
.end method

.method public resetAxisMaxValue()V
    .registers 2

    .line 272
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mCustomAxisMax:F

    .line 273
    return-void
.end method

.method public resetAxisMinValue()V
    .registers 2

    .line 249
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mCustomAxisMin:F

    .line 250
    return-void
.end method

.method public setAxisMaxValue(F)V
    .registers 2
    .param p1, "max"    # F

    .line 264
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mCustomAxisMax:F

    .line 265
    return-void
.end method

.method public setAxisMinValue(F)V
    .registers 2
    .param p1, "min"    # F

    .line 241
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mCustomAxisMin:F

    .line 242
    return-void
.end method

.method public setDrawTopYLabelEntry(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 142
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mDrawTopYLabelEntry:Z

    .line 143
    return-void
.end method

.method public setInverted(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 197
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mInverted:Z

    .line 198
    return-void
.end method

.method public setLabelCount(I)V
    .registers 3
    .param p1, "yCount"    # I

    .line 154
    const/16 v0, 0x19

    if-le p1, v0, :cond_6

    .line 155
    const/16 p1, 0x19

    .line 156
    :cond_6
    const/4 v0, 0x2

    if-ge p1, v0, :cond_a

    .line 157
    const/4 p1, 0x2

    .line 159
    :cond_a
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mLabelCount:I

    .line 160
    return-void
.end method

.method public setPosition(Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;)V
    .registers 2
    .param p1, "pos"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    .line 122
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    .line 123
    return-void
.end method

.method public setShowOnlyMinMax(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 178
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mShowOnlyMinMax:Z

    .line 179
    return-void
.end method

.method public setSpaceBottom(F)V
    .registers 2
    .param p1, "percent"    # F

    .line 299
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mSpacePercentBottom:F

    .line 300
    return-void
.end method

.method public setSpaceTop(F)V
    .registers 2
    .param p1, "percent"    # F

    .line 281
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mSpacePercentTop:F

    .line 282
    return-void
.end method

.method public setStartAtZero(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 215
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mStartAtZero:Z

    .line 216
    return-void
.end method

.method public setValueFormatter(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;)V
    .registers 2
    .param p1, "f"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;

    .line 368
    if-nez p1, :cond_3

    .line 369
    return-void

    .line 371
    :cond_3
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mValueFormatter:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;

    .line 372
    return-void
.end method

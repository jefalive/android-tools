.class public Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;
.super Ljava/lang/Object;
.source "ComprovanteLabelWrapperImpl.java"

# interfaces
.implements Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;


# instance fields
.field private final label:Ljava/lang/String;

.field private final value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->label:Ljava/lang/String;

    .line 10
    iput-object p2, p0, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->value:Ljava/lang/String;

    .line 11
    return-void
.end method

.method public static load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;
    .registers 3
    .param p0, "label"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .line 25
    new-instance v0, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getLabel()Ljava/lang/String;
    .registers 2

    .line 15
    iget-object v0, p0, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->label:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .registers 2

    .line 20
    iget-object v0, p0, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->value:Ljava/lang/String;

    return-object v0
.end method

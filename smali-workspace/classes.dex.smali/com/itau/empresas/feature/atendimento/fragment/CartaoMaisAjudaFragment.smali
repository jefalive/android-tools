.class public Lcom/itau/empresas/feature/atendimento/fragment/CartaoMaisAjudaFragment;
.super Lcom/itau/empresas/ui/fragment/BaseFragment;
.source "CartaoMaisAjudaFragment.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 16
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;-><init>()V

    return-void
.end method


# virtual methods
.method aoClicarMapas()V
    .registers 6

    .line 29
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/CartaoMaisAjudaFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 30
    const v2, 0x7f0702db

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/atendimento/fragment/CartaoMaisAjudaFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 31
    const v3, 0x7f07029c

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/atendimento/fragment/CartaoMaisAjudaFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 32
    const v4, 0x7f0702d7

    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/atendimento/fragment/CartaoMaisAjudaFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 33
    invoke-static {p0}, Lcom/itau/empresas/feature/mapa/MapaActivity;->intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/feature/mapa/MapaActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/mapa/MapaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 34
    return-void
.end method

.method aoClicarTelefone()V
    .registers 6

    .line 20
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/CartaoMaisAjudaFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 21
    const v2, 0x7f0702db

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/atendimento/fragment/CartaoMaisAjudaFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 22
    const v3, 0x7f07029f

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/atendimento/fragment/CartaoMaisAjudaFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 23
    const v4, 0x7f0702d8

    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/atendimento/fragment/CartaoMaisAjudaFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 24
    invoke-static {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 25
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 38
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

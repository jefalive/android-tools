.class public Lcom/google/android/gms/common/api/internal/zzh;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/api/internal/zzk;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/common/api/internal/zzh$zzd;,
        Lcom/google/android/gms/common/api/internal/zzh$zza;,
        Lcom/google/android/gms/common/api/internal/zzh$zze;,
        Lcom/google/android/gms/common/api/internal/zzh$zzc;,
        Lcom/google/android/gms/common/api/internal/zzh$zzb;,
        Lcom/google/android/gms/common/api/internal/zzh$zzf;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final zzXG:Ljava/util/concurrent/locks/Lock;

.field private final zzags:Lcom/google/android/gms/common/zzc;

.field private final zzagt:Lcom/google/android/gms/common/api/Api$zza;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/Api$zza<+Lcom/google/android/gms/internal/zzrn;Lcom/google/android/gms/internal/zzro;>;"
        }
    .end annotation
.end field

.field private final zzahA:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Lcom/google/android/gms/common/api/Api<*>;Ljava/lang/Integer;>;"
        }
    .end annotation
.end field

.field private zzahB:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Ljava/util/concurrent/Future<*>;>;"
        }
    .end annotation
.end field

.field private final zzahj:Lcom/google/android/gms/common/api/internal/zzl;

.field private zzahm:Lcom/google/android/gms/common/ConnectionResult;

.field private zzahn:I

.field private zzaho:I

.field private zzahp:I

.field private final zzahq:Landroid/os/Bundle;

.field private final zzahr:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<Lcom/google/android/gms/common/api/Api$zzc;>;"
        }
    .end annotation
.end field

.field private zzahs:Lcom/google/android/gms/internal/zzrn;

.field private zzaht:I

.field private zzahu:Z

.field private zzahv:Z

.field private zzahw:Lcom/google/android/gms/common/internal/zzp;

.field private zzahx:Z

.field private zzahy:Z

.field private final zzahz:Lcom/google/android/gms/common/internal/zzf;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/internal/zzl;Lcom/google/android/gms/common/internal/zzf;Ljava/util/Map;Lcom/google/android/gms/common/zzc;Lcom/google/android/gms/common/api/Api$zza;Ljava/util/concurrent/locks/Lock;Landroid/content/Context;)V
    .registers 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/google/android/gms/common/api/internal/zzl;Lcom/google/android/gms/common/internal/zzf;Ljava/util/Map<Lcom/google/android/gms/common/api/Api<*>;Ljava/lang/Integer;>;Lcom/google/android/gms/common/zzc;Lcom/google/android/gms/common/api/Api$zza<+Lcom/google/android/gms/internal/zzrn;Lcom/google/android/gms/internal/zzro;>;Ljava/util/concurrent/locks/Lock;Landroid/content/Context;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzaho:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahq:Landroid/os/Bundle;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahr:Ljava/util/Set;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahB:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iput-object p2, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahz:Lcom/google/android/gms/common/internal/zzf;

    iput-object p3, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahA:Ljava/util/Map;

    iput-object p4, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzags:Lcom/google/android/gms/common/zzc;

    iput-object p5, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzagt:Lcom/google/android/gms/common/api/Api$zza;

    iput-object p6, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzXG:Ljava/util/concurrent/locks/Lock;

    iput-object p7, p0, Lcom/google/android/gms/common/api/internal/zzh;->mContext:Landroid/content/Context;

    return-void
.end method

.method private zzZ(Z)V
    .registers 3

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahs:Lcom/google/android/gms/internal/zzrn;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahs:Lcom/google/android/gms/internal/zzrn;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzrn;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_13

    if-eqz p1, :cond_13

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahs:Lcom/google/android/gms/internal/zzrn;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzrn;->zzFG()V

    :cond_13
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahs:Lcom/google/android/gms/internal/zzrn;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzrn;->disconnect()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahw:Lcom/google/android/gms/common/internal/zzp;

    :cond_1b
    return-void
.end method

.method static synthetic zza(Lcom/google/android/gms/common/api/internal/zzh;)Landroid/content/Context;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic zza(Lcom/google/android/gms/common/api/internal/zzh;Lcom/google/android/gms/common/ConnectionResult;)V
    .registers 2

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/internal/zzh;->zzg(Lcom/google/android/gms/common/ConnectionResult;)V

    return-void
.end method

.method static synthetic zza(Lcom/google/android/gms/common/api/internal/zzh;Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/common/api/Api;I)V
    .registers 4

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/common/api/internal/zzh;->zzb(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/common/api/Api;I)V

    return-void
.end method

.method static synthetic zza(Lcom/google/android/gms/common/api/internal/zzh;Lcom/google/android/gms/signin/internal/SignInResponse;)V
    .registers 2

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/internal/zzh;->zza(Lcom/google/android/gms/signin/internal/SignInResponse;)V

    return-void
.end method

.method private zza(Lcom/google/android/gms/signin/internal/SignInResponse;)V
    .registers 8

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/internal/zzh;->zzbz(I)Z

    move-result v0

    if-nez v0, :cond_8

    return-void

    :cond_8
    invoke-virtual {p1}, Lcom/google/android/gms/signin/internal/SignInResponse;->zzqY()Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_5a

    invoke-virtual {p1}, Lcom/google/android/gms/signin/internal/SignInResponse;->zzFP()Lcom/google/android/gms/common/internal/ResolveAccountResponse;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/common/internal/ResolveAccountResponse;->zzqY()Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v0

    if-nez v0, :cond_41

    const-string v0, "GoogleApiClientConnecting"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Sign-in succeeded with resolve account failure: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-direct {p0, v5}, Lcom/google/android/gms/common/api/internal/zzh;->zzg(Lcom/google/android/gms/common/ConnectionResult;)V

    return-void

    :cond_41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahv:Z

    invoke-virtual {v4}, Lcom/google/android/gms/common/internal/ResolveAccountResponse;->zzqX()Lcom/google/android/gms/common/internal/zzp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahw:Lcom/google/android/gms/common/internal/zzp;

    invoke-virtual {v4}, Lcom/google/android/gms/common/internal/ResolveAccountResponse;->zzqZ()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahx:Z

    invoke-virtual {v4}, Lcom/google/android/gms/common/internal/ResolveAccountResponse;->zzra()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahy:Z

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzh;->zzpv()V

    goto :goto_6a

    :cond_5a
    invoke-direct {p0, v3}, Lcom/google/android/gms/common/api/internal/zzh;->zzf(Lcom/google/android/gms/common/ConnectionResult;)Z

    move-result v0

    if-eqz v0, :cond_67

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzh;->zzpy()V

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzh;->zzpv()V

    goto :goto_6a

    :cond_67
    invoke-direct {p0, v3}, Lcom/google/android/gms/common/api/internal/zzh;->zzg(Lcom/google/android/gms/common/ConnectionResult;)V

    :goto_6a
    return-void
.end method

.method private zza(IILcom/google/android/gms/common/ConnectionResult;)Z
    .registers 5

    const/4 v0, 0x1

    if-ne p2, v0, :cond_b

    invoke-direct {p0, p3}, Lcom/google/android/gms/common/api/internal/zzh;->zze(Lcom/google/android/gms/common/ConnectionResult;)Z

    move-result v0

    if-nez v0, :cond_b

    const/4 v0, 0x0

    return v0

    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahm:Lcom/google/android/gms/common/ConnectionResult;

    if-eqz v0, :cond_13

    iget v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahn:I

    if-ge p1, v0, :cond_15

    :cond_13
    const/4 v0, 0x1

    goto :goto_16

    :cond_15
    const/4 v0, 0x0

    :goto_16
    return v0
.end method

.method static synthetic zza(Lcom/google/android/gms/common/api/internal/zzh;I)Z
    .registers 3

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/internal/zzh;->zzbz(I)Z

    move-result v0

    return v0
.end method

.method static synthetic zzb(Lcom/google/android/gms/common/api/internal/zzh;)Lcom/google/android/gms/common/zzc;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzags:Lcom/google/android/gms/common/zzc;

    return-object v0
.end method

.method private zzb(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/common/api/Api;I)V
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/common/api/Api<*>;I)V"
        }
    .end annotation

    const/4 v0, 0x2

    if-eq p3, v0, :cond_15

    invoke-virtual {p2}, Lcom/google/android/gms/common/api/Api;->zzoP()Lcom/google/android/gms/common/api/Api$zza;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Api$zza;->getPriority()I

    move-result v2

    invoke-direct {p0, v2, p3, p1}, Lcom/google/android/gms/common/api/internal/zzh;->zza(IILcom/google/android/gms/common/ConnectionResult;)Z

    move-result v0

    if-eqz v0, :cond_15

    iput-object p1, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahm:Lcom/google/android/gms/common/ConnectionResult;

    iput v2, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahn:I

    :cond_15
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzl;->zzaio:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/google/android/gms/common/api/Api;->zzoR()Lcom/google/android/gms/common/api/Api$zzc;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method static synthetic zzb(Lcom/google/android/gms/common/api/internal/zzh;Lcom/google/android/gms/common/ConnectionResult;)Z
    .registers 3

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/internal/zzh;->zzf(Lcom/google/android/gms/common/ConnectionResult;)Z

    move-result v0

    return v0
.end method

.method private zzbA(I)Ljava/lang/String;
    .registers 3

    sparse-switch p1, :sswitch_data_e

    goto :goto_a

    :sswitch_4
    const-string v0, "STEP_SERVICE_BINDINGS_AND_SIGN_IN"

    return-object v0

    :sswitch_7
    const-string v0, "STEP_GETTING_REMOTE_SERVICE"

    return-object v0

    :goto_a
    const-string v0, "UNKNOWN"

    return-object v0

    nop

    :sswitch_data_e
    .sparse-switch
        0x0 -> :sswitch_4
        0x1 -> :sswitch_7
    .end sparse-switch
.end method

.method private zzbz(I)Z
    .registers 5

    iget v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzaho:I

    if-eq v0, p1, :cond_4f

    const-string v0, "GoogleApiClientConnecting"

    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iget-object v1, v1, Lcom/google/android/gms/common/api/internal/zzl;->zzagW:Lcom/google/android/gms/common/api/internal/zzj;

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/internal/zzj;->zzpH()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "GoogleApiClientConnecting"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GoogleApiClient connecting is in step "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzaho:I

    invoke-direct {p0, v2}, Lcom/google/android/gms/common/api/internal/zzh;->zzbA(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but received callback for step "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/internal/zzh;->zzbA(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v0, Lcom/google/android/gms/common/ConnectionResult;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/internal/zzh;->zzg(Lcom/google/android/gms/common/ConnectionResult;)V

    const/4 v0, 0x0

    return v0

    :cond_4f
    const/4 v0, 0x1

    return v0
.end method

.method static synthetic zzc(Lcom/google/android/gms/common/api/internal/zzh;)Ljava/util/concurrent/locks/Lock;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzXG:Ljava/util/concurrent/locks/Lock;

    return-object v0
.end method

.method static synthetic zzd(Lcom/google/android/gms/common/api/internal/zzh;)Lcom/google/android/gms/common/api/internal/zzl;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    return-object v0
.end method

.method private zze(Lcom/google/android/gms/common/ConnectionResult;)Z
    .registers 4

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->hasResolution()Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    return v0

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzags:Lcom/google/android/gms/common/zzc;

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/zzc;->zzbu(I)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_16

    const/4 v0, 0x1

    goto :goto_17

    :cond_16
    const/4 v0, 0x0

    :goto_17
    return v0
.end method

.method static synthetic zze(Lcom/google/android/gms/common/api/internal/zzh;)Z
    .registers 2

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahu:Z

    return v0
.end method

.method static synthetic zzf(Lcom/google/android/gms/common/api/internal/zzh;)Lcom/google/android/gms/internal/zzrn;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahs:Lcom/google/android/gms/internal/zzrn;

    return-object v0
.end method

.method private zzf(Lcom/google/android/gms/common/ConnectionResult;)Z
    .registers 4

    iget v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzaht:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_10

    iget v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzaht:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_12

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->hasResolution()Z

    move-result v0

    if-nez v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    goto :goto_13

    :cond_12
    const/4 v0, 0x0

    :goto_13
    return v0
.end method

.method static synthetic zzg(Lcom/google/android/gms/common/api/internal/zzh;)Ljava/util/Set;
    .registers 2

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzh;->zzpA()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private zzg(Lcom/google/android/gms/common/ConnectionResult;)V
    .registers 3

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzh;->zzpz()V

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->hasResolution()Z

    move-result v0

    if-nez v0, :cond_b

    const/4 v0, 0x1

    goto :goto_c

    :cond_b
    const/4 v0, 0x0

    :goto_c
    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/internal/zzh;->zzZ(Z)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/api/internal/zzl;->zzh(Lcom/google/android/gms/common/ConnectionResult;)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzl;->zzais:Lcom/google/android/gms/common/api/internal/zzp$zza;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/api/internal/zzp$zza;->zzd(Lcom/google/android/gms/common/ConnectionResult;)V

    return-void
.end method

.method static synthetic zzh(Lcom/google/android/gms/common/api/internal/zzh;)Lcom/google/android/gms/common/internal/zzp;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahw:Lcom/google/android/gms/common/internal/zzp;

    return-object v0
.end method

.method static synthetic zzi(Lcom/google/android/gms/common/api/internal/zzh;)V
    .registers 1

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzh;->zzpy()V

    return-void
.end method

.method static synthetic zzj(Lcom/google/android/gms/common/api/internal/zzh;)V
    .registers 1

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzh;->zzpv()V

    return-void
.end method

.method static synthetic zzk(Lcom/google/android/gms/common/api/internal/zzh;)Z
    .registers 2

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzh;->zzpu()Z

    move-result v0

    return v0
.end method

.method private zzpA()Ljava/util/Set;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/Set<Lcom/google/android/gms/common/api/Scope;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahz:Lcom/google/android/gms/common/internal/zzf;

    if-nez v0, :cond_9

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    return-object v0

    :cond_9
    new-instance v2, Ljava/util/HashSet;

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahz:Lcom/google/android/gms/common/internal/zzf;

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/zzf;->zzqs()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahz:Lcom/google/android/gms/common/internal/zzf;

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/zzf;->zzqu()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_22
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_49

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/gms/common/api/Api;

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzl;->zzaio:Ljava/util/Map;

    invoke-virtual {v5}, Lcom/google/android/gms/common/api/Api;->zzoR()Lcom/google/android/gms/common/api/Api$zzc;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_48

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/internal/zzf$zza;

    iget-object v0, v0, Lcom/google/android/gms/common/internal/zzf$zza;->zzXf:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    :cond_48
    goto :goto_22

    :cond_49
    return-object v2
.end method

.method private zzpu()Z
    .registers 4

    iget v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahp:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahp:I

    iget v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahp:I

    if-lez v0, :cond_c

    const/4 v0, 0x0

    return v0

    :cond_c
    iget v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahp:I

    if-gez v0, :cond_36

    const-string v0, "GoogleApiClientConnecting"

    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iget-object v1, v1, Lcom/google/android/gms/common/api/internal/zzl;->zzagW:Lcom/google/android/gms/common/api/internal/zzj;

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/internal/zzj;->zzpH()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "GoogleApiClientConnecting"

    const-string v1, "GoogleApiClient received too many callbacks for the given step. Clients may be in an unexpected state; GoogleApiClient will now disconnect."

    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v0, Lcom/google/android/gms/common/ConnectionResult;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/internal/zzh;->zzg(Lcom/google/android/gms/common/ConnectionResult;)V

    const/4 v0, 0x0

    return v0

    :cond_36
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahm:Lcom/google/android/gms/common/ConnectionResult;

    if-eqz v0, :cond_47

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iget v1, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahn:I

    iput v1, v0, Lcom/google/android/gms/common/api/internal/zzl;->zzair:I

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahm:Lcom/google/android/gms/common/ConnectionResult;

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/internal/zzh;->zzg(Lcom/google/android/gms/common/ConnectionResult;)V

    const/4 v0, 0x0

    return v0

    :cond_47
    const/4 v0, 0x1

    return v0
.end method

.method private zzpv()V
    .registers 2

    iget v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahp:I

    if-eqz v0, :cond_5

    return-void

    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahu:Z

    if-eqz v0, :cond_d

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahv:Z

    if-eqz v0, :cond_10

    :cond_d
    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzh;->zzpw()V

    :cond_10
    return-void
.end method

.method private zzpw()V
    .registers 7

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzaho:I

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzl;->zzahT:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahp:I

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzl;->zzahT:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/gms/common/api/Api$zzc;

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzl;->zzaio:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3f

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzh;->zzpu()Z

    move-result v0

    if-eqz v0, :cond_4a

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzh;->zzpx()V

    goto :goto_4a

    :cond_3f
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzl;->zzahT:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4a
    :goto_4a
    goto :goto_1e

    :cond_4b
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_63

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahB:Ljava/util/ArrayList;

    invoke-static {}, Lcom/google/android/gms/common/api/internal/zzm;->zzpN()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/common/api/internal/zzh$zzc;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/common/api/internal/zzh$zzc;-><init>(Lcom/google/android/gms/common/api/internal/zzh;Ljava/util/ArrayList;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_63
    return-void
.end method

.method private zzpx()V
    .registers 7

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/internal/zzl;->zzpL()V

    invoke-static {}, Lcom/google/android/gms/common/api/internal/zzm;->zzpN()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/common/api/internal/zzh$1;

    invoke-direct {v1, p0}, Lcom/google/android/gms/common/api/internal/zzh$1;-><init>(Lcom/google/android/gms/common/api/internal/zzh;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahs:Lcom/google/android/gms/internal/zzrn;

    if-eqz v0, :cond_26

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahx:Z

    if-eqz v0, :cond_22

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahs:Lcom/google/android/gms/internal/zzrn;

    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahw:Lcom/google/android/gms/common/internal/zzp;

    iget-boolean v2, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahy:Z

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/internal/zzrn;->zza(Lcom/google/android/gms/common/internal/zzp;Z)V

    :cond_22
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/internal/zzh;->zzZ(Z)V

    :cond_26
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzl;->zzaio:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_32
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/google/android/gms/common/api/Api$zzc;

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzl;->zzahT:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/gms/common/api/Api$zzb;

    invoke-interface {v5}, Lcom/google/android/gms/common/api/Api$zzb;->disconnect()V

    goto :goto_32

    :cond_4e
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahq:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_58

    const/4 v3, 0x0

    goto :goto_5a

    :cond_58
    iget-object v3, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahq:Landroid/os/Bundle;

    :goto_5a
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzl;->zzais:Lcom/google/android/gms/common/api/internal/zzp$zza;

    invoke-interface {v0, v3}, Lcom/google/android/gms/common/api/internal/zzp$zza;->zzi(Landroid/os/Bundle;)V

    return-void
.end method

.method private zzpy()V
    .registers 7

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahu:Z

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzl;->zzagW:Lcom/google/android/gms/common/api/internal/zzj;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/common/api/internal/zzj;->zzahU:Ljava/util/Set;

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahr:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_13
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/gms/common/api/Api$zzc;

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzl;->zzaio:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_39

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzl;->zzaio:Ljava/util/Map;

    new-instance v1, Lcom/google/android/gms/common/ConnectionResult;

    const/16 v2, 0x11

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    invoke-interface {v0, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_39
    goto :goto_13

    :cond_3a
    return-void
.end method

.method private zzpz()V
    .registers 4

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahB:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/util/concurrent/Future;

    const/4 v0, 0x1

    invoke-interface {v2, v0}, Ljava/util/concurrent/Future;->cancel(Z)Z

    goto :goto_6

    :cond_18
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahB:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method


# virtual methods
.method public begin()V
    .registers 14

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzl;->zzaio:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahu:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahm:Lcom/google/android/gms/common/ConnectionResult;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzaho:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzaht:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahv:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahx:Z

    const/4 v7, 0x0

    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahA:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_29
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_84

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/google/android/gms/common/api/Api;

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzl;->zzahT:Ljava/util/Map;

    invoke-virtual {v10}, Lcom/google/android/gms/common/api/Api;->zzoR()Lcom/google/android/gms/common/api/Api$zzc;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/google/android/gms/common/api/Api$zzb;

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahA:Ljava/util/Map;

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-virtual {v10}, Lcom/google/android/gms/common/api/Api;->zzoP()Lcom/google/android/gms/common/api/Api$zza;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Api$zza;->getPriority()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5e

    const/4 v0, 0x1

    goto :goto_5f

    :cond_5e
    const/4 v0, 0x0

    :goto_5f
    or-int/2addr v7, v0

    invoke-interface {v11}, Lcom/google/android/gms/common/api/Api$zzb;->zzmE()Z

    move-result v0

    if-eqz v0, :cond_7a

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahu:Z

    iget v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzaht:I

    if-ge v12, v0, :cond_6f

    iput v12, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzaht:I

    :cond_6f
    if-eqz v12, :cond_7a

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahr:Ljava/util/Set;

    invoke-virtual {v10}, Lcom/google/android/gms/common/api/Api;->zzoR()Lcom/google/android/gms/common/api/Api$zzc;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_7a
    new-instance v0, Lcom/google/android/gms/common/api/internal/zzh$zza;

    invoke-direct {v0, p0, v10, v12}, Lcom/google/android/gms/common/api/internal/zzh$zza;-><init>(Lcom/google/android/gms/common/api/internal/zzh;Lcom/google/android/gms/common/api/Api;I)V

    invoke-interface {v8, v11, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_29

    :cond_84
    if-eqz v7, :cond_89

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahu:Z

    :cond_89
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahu:Z

    if-eqz v0, :cond_c2

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahz:Lcom/google/android/gms/common/internal/zzf;

    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iget-object v1, v1, Lcom/google/android/gms/common/api/internal/zzl;->zzagW:Lcom/google/android/gms/common/api/internal/zzj;

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/internal/zzj;->getSessionId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/zzf;->zza(Ljava/lang/Integer;)V

    new-instance v9, Lcom/google/android/gms/common/api/internal/zzh$zze;

    const/4 v0, 0x0

    invoke-direct {v9, p0, v0}, Lcom/google/android/gms/common/api/internal/zzh$zze;-><init>(Lcom/google/android/gms/common/api/internal/zzh;Lcom/google/android/gms/common/api/internal/zzh$1;)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzagt:Lcom/google/android/gms/common/api/Api$zza;

    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzh;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iget-object v2, v2, Lcom/google/android/gms/common/api/internal/zzl;->zzagW:Lcom/google/android/gms/common/api/internal/zzj;

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/internal/zzj;->getLooper()Landroid/os/Looper;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahz:Lcom/google/android/gms/common/internal/zzf;

    iget-object v4, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahz:Lcom/google/android/gms/common/internal/zzf;

    invoke-virtual {v4}, Lcom/google/android/gms/common/internal/zzf;->zzqy()Lcom/google/android/gms/internal/zzro;

    move-result-object v4

    move-object v5, v9

    move-object v6, v9

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/common/api/Api$zza;->zza(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/zzf;Ljava/lang/Object;Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)Lcom/google/android/gms/common/api/Api$zzb;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/zzrn;

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahs:Lcom/google/android/gms/internal/zzrn;

    :cond_c2
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzl;->zzahT:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahp:I

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahB:Ljava/util/ArrayList;

    invoke-static {}, Lcom/google/android/gms/common/api/internal/zzm;->zzpN()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/common/api/internal/zzh$zzb;

    invoke-direct {v2, p0, v8}, Lcom/google/android/gms/common/api/internal/zzh$zzb;-><init>(Lcom/google/android/gms/common/api/internal/zzh;Ljava/util/Map;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public connect()V
    .registers 1

    return-void
.end method

.method public disconnect()Z
    .registers 3

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzh;->zzpz()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/internal/zzh;->zzZ(Z)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/internal/zzl;->zzh(Lcom/google/android/gms/common/ConnectionResult;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onConnected(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "connectionHint"    # Landroid/os/Bundle;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/internal/zzh;->zzbz(I)Z

    move-result v0

    if-nez v0, :cond_8

    return-void

    :cond_8
    if-eqz p1, :cond_f

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahq:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    :cond_f
    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzh;->zzpu()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzh;->zzpx()V

    :cond_18
    return-void
.end method

.method public onConnectionSuspended(I)V
    .registers 5
    .param p1, "cause"    # I

    new-instance v0, Lcom/google/android/gms/common/ConnectionResult;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/internal/zzh;->zzg(Lcom/google/android/gms/common/ConnectionResult;)V

    return-void
.end method

.method public zza(Lcom/google/android/gms/common/api/internal/zza$zza;)Lcom/google/android/gms/common/api/internal/zza$zza;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::Lcom/google/android/gms/common/api/Api$zzb;R::Lcom/google/android/gms/common/api/Result;T:Lcom/google/android/gms/common/api/internal/zza$zza<TR;TA;>;>(TT;)TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzh;->zzahj:Lcom/google/android/gms/common/api/internal/zzl;

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzl;->zzagW:Lcom/google/android/gms/common/api/internal/zzj;

    iget-object v0, v0, Lcom/google/android/gms/common/api/internal/zzj;->zzahN:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    return-object p1
.end method

.method public zza(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/common/api/Api;I)V
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/common/api/Api<*>;I)V"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/internal/zzh;->zzbz(I)Z

    move-result v0

    if-nez v0, :cond_8

    return-void

    :cond_8
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/common/api/internal/zzh;->zzb(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/common/api/Api;I)V

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzh;->zzpu()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzh;->zzpx()V

    :cond_14
    return-void
.end method

.method public zzb(Lcom/google/android/gms/common/api/internal/zza$zza;)Lcom/google/android/gms/common/api/internal/zza$zza;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::Lcom/google/android/gms/common/api/Api$zzb;T:Lcom/google/android/gms/common/api/internal/zza$zza<+Lcom/google/android/gms/common/api/Result;TA;>;>(TT;)TT;"
        }
    .end annotation

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GoogleApiClient is not connected yet."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

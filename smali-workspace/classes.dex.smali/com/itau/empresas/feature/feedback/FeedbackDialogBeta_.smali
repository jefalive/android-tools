.class public final Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;
.super Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;
.source "FeedbackDialogBeta_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_$FragmentBuilder_;
    }
.end annotation


# instance fields
.field private contentView_:Landroid/view/View;

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 28
    invoke-direct {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;-><init>()V

    .line 32
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method public static builder()Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_$FragmentBuilder_;
    .registers 1

    .line 92
    new-instance v0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_$FragmentBuilder_;

    invoke-direct {v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_$FragmentBuilder_;-><init>()V

    return-object v0
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 79
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 80
    invoke-direct {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->injectFragmentArguments_()V

    .line 81
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->application:Lcom/itau/empresas/CustomApplication;

    .line 82
    invoke-virtual {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/FeedbackController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/controller/FeedbackController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->controller:Lcom/itau/empresas/controller/FeedbackController;

    .line 83
    return-void
.end method

.method private injectFragmentArguments_()V
    .registers 3

    .line 197
    invoke-virtual {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 198
    .local v1, "args_":Landroid/os/Bundle;
    if-eqz v1, :cond_18

    .line 199
    const-string v0, "feedbackEspontaneo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 200
    const-string v0, "feedbackEspontaneo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->feedbackEspontaneo:Ljava/lang/Boolean;

    .line 203
    :cond_18
    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .registers 3
    .param p1, "id"    # I

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_6

    .line 47
    const/4 v0, 0x0

    return-object v0

    .line 49
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->contentView_:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 39
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->init_(Landroid/os/Bundle;)V

    .line 40
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->onCreate(Landroid/os/Bundle;)V

    .line 41
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 42
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 54
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->contentView_:Landroid/view/View;

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_14

    .line 56
    const v0, 0x7f030086

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->contentView_:Landroid/view/View;

    .line 58
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->contentView_:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .registers 2

    .line 63
    invoke-super {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->onDestroyView()V

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->contentView_:Landroid/view/View;

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->root:Landroid/widget/LinearLayout;

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->llDialogFeedbackAvalie:Landroid/widget/LinearLayout;

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->llDialogFeedbackObrigado:Landroid/widget/LinearLayout;

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->llDialogFeedbackPreenchimentoCompleto:Landroid/widget/LinearLayout;

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->llDialogFeedbackObrigadoBeta:Landroid/widget/LinearLayout;

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->llFeedbackBetaCabecalho:Landroid/widget/LinearLayout;

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->textoFeedbackMensagem:Landroid/widget/TextView;

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->botaoAvaliar:Landroid/widget/Button;

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->campoComentario:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->campoEmail:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->spinnerCategoria:Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 76
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 10
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 97
    const v0, 0x7f0e038c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->root:Landroid/widget/LinearLayout;

    .line 98
    const v0, 0x7f0e038d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->llDialogFeedbackAvalie:Landroid/widget/LinearLayout;

    .line 99
    const v0, 0x7f0e0398

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->llDialogFeedbackObrigado:Landroid/widget/LinearLayout;

    .line 100
    const v0, 0x7f0e0391

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->llDialogFeedbackPreenchimentoCompleto:Landroid/widget/LinearLayout;

    .line 101
    const v0, 0x7f0e039a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->llDialogFeedbackObrigadoBeta:Landroid/widget/LinearLayout;

    .line 102
    const v0, 0x7f0e0392

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->llFeedbackBetaCabecalho:Landroid/widget/LinearLayout;

    .line 103
    const v0, 0x7f0e0393

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->textoFeedbackMensagem:Landroid/widget/TextView;

    .line 104
    const v0, 0x7f0e0397

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->botaoAvaliar:Landroid/widget/Button;

    .line 105
    const v0, 0x7f0e0115

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/material/MaterialEditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->campoComentario:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 106
    const v0, 0x7f0e0114

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/material/MaterialEditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->campoEmail:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 107
    const v0, 0x7f0e0394

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/material/MaterialSpinner;

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->spinnerCategoria:Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 108
    const v0, 0x7f0e038f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 109
    .local v2, "view_botao_fale_sobre":Landroid/view/View;
    const v0, 0x7f0e038e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 110
    .local v3, "view_botao_ir_para_loja":Landroid/view/View;
    const v0, 0x7f0e0390

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 111
    .local v4, "view_botao_cancelar":Landroid/view/View;
    const v0, 0x7f0e0396

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 112
    .local v5, "view_botao_cancelar_avaliacao":Landroid/view/View;
    const v0, 0x7f0e0399

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 113
    .local v6, "view_botao_ok_entendi":Landroid/view/View;
    const v0, 0x7f0e039c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 115
    .local v7, "view_botao_continuar_loja_beta":Landroid/view/View;
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->botaoAvaliar:Landroid/widget/Button;

    if-eqz v0, :cond_bb

    .line 116
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->botaoAvaliar:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_$1;-><init>(Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->botaoAvaliar:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_$2;-><init>(Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    :cond_bb
    if-eqz v2, :cond_c5

    .line 134
    new-instance v0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_$3;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_$3;-><init>(Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    :cond_c5
    if-eqz v3, :cond_cf

    .line 144
    new-instance v0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_$4;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_$4;-><init>(Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 153
    :cond_cf
    if-eqz v4, :cond_d9

    .line 154
    new-instance v0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_$5;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_$5;-><init>(Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;)V

    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    :cond_d9
    if-eqz v5, :cond_e3

    .line 164
    new-instance v0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_$6;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_$6;-><init>(Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;)V

    invoke-virtual {v5, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173
    :cond_e3
    if-eqz v6, :cond_ed

    .line 174
    new-instance v0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_$7;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_$7;-><init>(Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;)V

    invoke-virtual {v6, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    :cond_ed
    if-eqz v7, :cond_f7

    .line 184
    new-instance v0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_$8;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_$8;-><init>(Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;)V

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    :cond_f7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->aoInicializar()V

    .line 194
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 87
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 89
    return-void
.end method

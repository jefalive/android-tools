.class Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias$1;
.super Ljava/lang/Object;
.source "PendenciasAdapter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;-><init>(Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;

.field final synthetic val$this$0:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;)V
    .registers 3
    .param p1, "this$1"    # Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;

    .line 196
    iput-object p1, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias$1;->this$1:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;

    iput-object p2, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias$1;->val$this$0:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 8
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/widget/AdapterView<*>;Landroid/view/View;IJ)V"
        }
    .end annotation

    .line 200
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias$1;->this$1:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;

    # getter for: Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;->podeFazerRequest:Z
    invoke-static {v0}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;->access$000(Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 201
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias$1;->this$1:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;

    iget-object v0, v0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;->this$0:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;

    # getter for: Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->access$100(Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->mostraDialogoDeProgresso()V

    .line 202
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias$1;->this$1:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;

    new-instance v1, Lcom/itau/empresas/Evento$EventoSelecionaDataDaPendencia;

    invoke-direct {v1, p3}, Lcom/itau/empresas/Evento$EventoSelecionaDataDaPendencia;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;->publicaEvento(Ljava/lang/Object;)V

    .line 204
    :cond_1f
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias$1;->this$1:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;

    const/4 v1, 0x1

    # setter for: Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;->podeFazerRequest:Z
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;->access$002(Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderCardSelecaoDias;Z)Z

    .line 205
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .registers 2
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/widget/AdapterView<*>;)V"
        }
    .end annotation

    .line 210
    return-void
.end method

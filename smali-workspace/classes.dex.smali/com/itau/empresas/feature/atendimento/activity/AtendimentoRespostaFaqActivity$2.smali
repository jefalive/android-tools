.class Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity$2;
.super Ljava/lang/Object;
.source "AtendimentoRespostaFaqActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->carregaRelacionadas(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;

.field final synthetic val$relacionadas:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;Ljava/util/ArrayList;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;

    .line 167
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity$2;->this$0:Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;

    iput-object p2, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity$2;->val$relacionadas:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 12
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/widget/AdapterView<*>;Landroid/view/View;IJ)V"
        }
    .end annotation

    .line 170
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity$2;->this$0:Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->mostraDialogoDeProgresso()V

    .line 171
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity$2;->this$0:Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->controller:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

    iget-object v1, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity$2;->val$relacionadas:Ljava/util/ArrayList;

    add-int/lit8 v2, p3, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/atendimento/model/AtendimentoRelacionadas;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoRelacionadas;->getId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->buscaFaqs(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 172
    return-void
.end method

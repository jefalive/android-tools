.class public Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "AreaUsuarioMenuAbertoAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;,
        Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$OnConfiguracoesItemSelected;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter<Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;>;"
    }
.end annotation


# instance fields
.field private mItemConfiguracoes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;>;"
        }
    .end annotation
.end field

.field private mListener:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$OnConfiguracoesItemSelected;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 17
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    return-void
.end method

.method static synthetic access$300(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;)Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$OnConfiguracoesItemSelected;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;

    .line 17
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;->mListener:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$OnConfiguracoesItemSelected;

    return-object v0
.end method


# virtual methods
.method public getItemCount()I
    .registers 2

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;->mItemConfiguracoes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .registers 4

    .line 16
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;

    invoke-virtual {p0, v0, p2}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;->onBindViewHolder(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;I)V
    .registers 6
    .param p1, "holder"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;
    .param p2, "position"    # I

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;->mItemConfiguracoes:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;

    .line 48
    .local v2, "item":Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;
    if-eqz v2, :cond_21

    .line 49
    # getter for: Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;->textoIcone:Lcom/itau/empresas/ui/view/TextViewIcon;
    invoke-static {p1}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;->access$100(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;)Lcom/itau/empresas/ui/view/TextViewIcon;

    move-result-object v0

    invoke-virtual {v2}, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;->getIconeMenu()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/TextViewIcon;->setText(Ljava/lang/CharSequence;)V

    .line 50
    # getter for: Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;->textoDescricao:Landroid/widget/TextView;
    invoke-static {p1}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;->access$200(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v2}, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;->getItemMenu()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    :cond_21
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .registers 4

    .line 16
    invoke-virtual {p0, p1, p2}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;
    .registers 7
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .line 37
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 39
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v0, 0x7f0300f3

    const/4 v1, 0x0

    invoke-virtual {v2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 41
    .local v3, "view":Landroid/view/View;
    new-instance v0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v3, v1}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;-><init>(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;Landroid/view/View;Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$1;)V

    return-object v0
.end method

.method public setListaItensConfiguracoes(Ljava/util/List;)V
    .registers 2
    .param p1, "itemConfiguracoes"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;>;)V"
        }
    .end annotation

    .line 28
    iput-object p1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;->mItemConfiguracoes:Ljava/util/List;

    .line 29
    return-void
.end method

.method public setListener(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$OnConfiguracoesItemSelected;)V
    .registers 2
    .param p1, "listener"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$OnConfiguracoesItemSelected;

    .line 32
    iput-object p1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;->mListener:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$OnConfiguracoesItemSelected;

    .line 33
    return-void
.end method

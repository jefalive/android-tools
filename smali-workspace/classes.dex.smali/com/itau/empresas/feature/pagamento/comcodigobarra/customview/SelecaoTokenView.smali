.class public Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;
.super Lcom/itau/empresas/ui/view/BaseModalView;
.source "SelecaoTokenView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;,
        Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$OnTokenValidoListener;
    }
.end annotation


# instance fields
.field app:Lcom/itau/empresas/CustomApplication;

.field public botaoTokenContinuar:Landroid/widget/Button;

.field public campoToken:Landroid/widget/EditText;

.field public iconeVoltar:Landroid/widget/ImageView;

.field public infoEscolhaDispositivoToken:Landroid/widget/TextView;

.field private onTokenValidoListener:Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$OnTokenValidoListener;

.field public selecaoTokenAplicativo:Landroid/widget/TextView;

.field public selecaoTokenChaveiro:Landroid/widget/TextView;

.field public selecaoTokenSms:Landroid/widget/TextView;

.field private tipoTokenSelecionado:Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

.field public tokenInfo:Ljava/lang/String;

.field public tokenInfoChaveiro:Ljava/lang/String;

.field public tokenInfoSms:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 54
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/BaseModalView;-><init>(Landroid/content/Context;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 58
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/view/BaseModalView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 62
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/ui/view/BaseModalView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    return-void
.end method


# virtual methods
.method clicaBotaoFechar()V
    .registers 1

    .line 79
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->escondePainel()V

    .line 80
    return-void
.end method

.method clicaEmBotaoContinuar()V
    .registers 5

    .line 140
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->campoToken:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 142
    .local v3, "token":Ljava/lang/String;
    if-eqz v3, :cond_13

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_21

    .line 143
    :cond_13
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "iToken precisa ter 6 numeros"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 146
    :cond_21
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->onTokenValidoListener:Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$OnTokenValidoListener;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->tipoTokenSelecionado:Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$OnTokenValidoListener;->onTokenValido(Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;)V

    .line 147
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->campoToken:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 148
    return-void
.end method

.method clicaEmVoltar()V
    .registers 6

    .line 85
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->iconeVoltar:Landroid/widget/ImageView;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->campoToken:Landroid/widget/EditText;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->botaoTokenContinuar:Landroid/widget/Button;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ViewUtils;->gone([Landroid/view/View;)V

    .line 86
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->selecaoTokenSms:Landroid/widget/TextView;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->selecaoTokenAplicativo:Landroid/widget/TextView;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->selecaoTokenChaveiro:Landroid/widget/TextView;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ViewUtils;->visible([Landroid/view/View;)V

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->selecaoTokenSms:Landroid/widget/TextView;

    .line 89
    const v1, 0x7f020173

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 91
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->infoEscolhaDispositivoToken:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->tokenInfo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    return-void
.end method

.method clicaTokenAplicativo()V
    .registers 4

    .line 131
    sget-object v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;->APLICATIVO:Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->tipoTokenSelecionado:Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

    .line 132
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->selecaoTokenSms:Landroid/widget/TextView;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->selecaoTokenAplicativo:Landroid/widget/TextView;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ViewUtils;->gone([Landroid/view/View;)V

    .line 133
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->iconeVoltar:Landroid/widget/ImageView;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->campoToken:Landroid/widget/EditText;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->botaoTokenContinuar:Landroid/widget/Button;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->selecaoTokenAplicativo:Landroid/widget/TextView;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ViewUtils;->visible([Landroid/view/View;)V

    .line 134
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->campoToken:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 135
    return-void
.end method

.method clicaTokenChaveiro()V
    .registers 6

    .line 97
    sget-object v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;->CHAVEIRO:Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->tipoTokenSelecionado:Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

    .line 98
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->selecaoTokenAplicativo:Landroid/widget/TextView;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->selecaoTokenSms:Landroid/widget/TextView;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ViewUtils;->gone([Landroid/view/View;)V

    .line 99
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->iconeVoltar:Landroid/widget/ImageView;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->campoToken:Landroid/widget/EditText;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->botaoTokenContinuar:Landroid/widget/Button;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->selecaoTokenChaveiro:Landroid/widget/TextView;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ViewUtils;->visible([Landroid/view/View;)V

    .line 101
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->tokenInfoChaveiro:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->app:Lcom/itau/empresas/CustomApplication;

    .line 102
    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getPerfilLogado()Lcom/itau/empresas/api/model/PerfilOperadorVO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->getFinalTokenChaveiro()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 101
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 104
    .local v4, "infoChaveiro":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->infoEscolhaDispositivoToken:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    return-void
.end method

.method clicaTokenSms()V
    .registers 7

    .line 110
    sget-object v0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;->SMS:Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->tipoTokenSelecionado:Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView$TokenType;

    .line 111
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->selecaoTokenAplicativo:Landroid/widget/TextView;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->selecaoTokenChaveiro:Landroid/widget/TextView;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ViewUtils;->gone([Landroid/view/View;)V

    .line 112
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/view/View;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->iconeVoltar:Landroid/widget/ImageView;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->campoToken:Landroid/widget/EditText;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->botaoTokenContinuar:Landroid/widget/Button;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->selecaoTokenSms:Landroid/widget/TextView;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ViewUtils;->visible([Landroid/view/View;)V

    .line 114
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->tokenInfoSms:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->app:Lcom/itau/empresas/CustomApplication;

    .line 115
    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getPerfilLogado()Lcom/itau/empresas/api/model/PerfilOperadorVO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->getMascaraTelefoneSms()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 114
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 117
    .local v5, "infoSms":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->selecaoTokenSms:Landroid/widget/TextView;

    .line 118
    const v1, 0x7f020173

    const/4 v2, 0x0

    const v3, 0x7f020174

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 124
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->infoEscolhaDispositivoToken:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/customview/SelecaoTokenView;->campoToken:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 126
    return-void
.end method

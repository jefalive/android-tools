.class public final Lcom/itau/empresas/CustomApplication_;
.super Lcom/itau/empresas/CustomApplication;
.source "CustomApplication_.java"


# static fields
.field private static INSTANCE_:Lcom/itau/empresas/CustomApplication;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 10
    invoke-direct {p0}, Lcom/itau/empresas/CustomApplication;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/itau/empresas/CustomApplication;
    .registers 1

    .line 16
    sget-object v0, Lcom/itau/empresas/CustomApplication_;->INSTANCE_:Lcom/itau/empresas/CustomApplication;

    return-object v0
.end method

.method private init_()V
    .registers 2

    .line 34
    invoke-static {p0}, Lcom/itau/empresas/GerenciadorSessaoUsuario_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/GerenciadorSessaoUsuario_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/CustomApplication_;->gerenciadorSessaoUsuario:Lcom/itau/empresas/GerenciadorSessaoUsuario;

    .line 35
    return-void
.end method


# virtual methods
.method public onCreate()V
    .registers 1

    .line 28
    sput-object p0, Lcom/itau/empresas/CustomApplication_;->INSTANCE_:Lcom/itau/empresas/CustomApplication;

    .line 29
    invoke-direct {p0}, Lcom/itau/empresas/CustomApplication_;->init_()V

    .line 30
    invoke-super {p0}, Lcom/itau/empresas/CustomApplication;->onCreate()V

    .line 31
    return-void
.end method

.class Lorg/simpleframework/xml/stream/CamelCaseBuilder;
.super Ljava/lang/Object;
.source "CamelCaseBuilder.java"

# interfaces
.implements Lorg/simpleframework/xml/stream/Style;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/simpleframework/xml/stream/CamelCaseBuilder$1;,
        Lorg/simpleframework/xml/stream/CamelCaseBuilder$Element;,
        Lorg/simpleframework/xml/stream/CamelCaseBuilder$Attribute;
    }
.end annotation


# instance fields
.field protected final attribute:Z

.field protected final element:Z


# direct methods
.method public constructor <init>(ZZ)V
    .registers 3
    .param p1, "element"    # Z
    .param p2, "attribute"    # Z

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-boolean p2, p0, Lorg/simpleframework/xml/stream/CamelCaseBuilder;->attribute:Z

    .line 65
    iput-boolean p1, p0, Lorg/simpleframework/xml/stream/CamelCaseBuilder;->element:Z

    .line 66
    return-void
.end method


# virtual methods
.method public getAttribute(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .param p1, "name"    # Ljava/lang/String;

    .line 79
    if-eqz p1, :cond_d

    .line 80
    new-instance v0, Lorg/simpleframework/xml/stream/CamelCaseBuilder$Attribute;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lorg/simpleframework/xml/stream/CamelCaseBuilder$Attribute;-><init>(Lorg/simpleframework/xml/stream/CamelCaseBuilder;Ljava/lang/String;Lorg/simpleframework/xml/stream/CamelCaseBuilder$1;)V

    invoke-virtual {v0}, Lorg/simpleframework/xml/stream/CamelCaseBuilder$Attribute;->process()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 82
    :cond_d
    const/4 v0, 0x0

    return-object v0
.end method

.method public getElement(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .param p1, "name"    # Ljava/lang/String;

    .line 96
    if-eqz p1, :cond_d

    .line 97
    new-instance v0, Lorg/simpleframework/xml/stream/CamelCaseBuilder$Element;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lorg/simpleframework/xml/stream/CamelCaseBuilder$Element;-><init>(Lorg/simpleframework/xml/stream/CamelCaseBuilder;Ljava/lang/String;Lorg/simpleframework/xml/stream/CamelCaseBuilder$1;)V

    invoke-virtual {v0}, Lorg/simpleframework/xml/stream/CamelCaseBuilder$Element;->process()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 99
    :cond_d
    const/4 v0, 0x0

    return-object v0
.end method

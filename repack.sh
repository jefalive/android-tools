#!/bin/bash

# clean directory before repacking
rm tmp/classes.recompiled.dex tmp/com.itau.empresas-aligned-debugSigned.apk com.itau.empresas.apk tmp/com.itau.empresas.apk.decompiled/classes.dex

# recompile smali files directory into a new .dex file
/usr/lib/jvm/java-8-oracle/bin/java -jar jesus-freke/smali-2.2.1.jar a tmp/classes.dex.smali -o tmp/classes.recompiled.dex

# put recompiled .dex file into unpacked .apk
cp tmp/classes.recompiled.dex tmp/com.itau.empresas.apk.decompiled/classes.dex

# repack unpacked .apk
/usr/lib/jvm/java-7-oracle/bin/java -jar apktool/apktool_2.2.3.jar b tmp/com.itau.empresas.apk.decompiled -o tmp/com.itau.empresas.apk

# do signing and zip alignment in repacked .apk
/usr/lib/jvm/java-8-oracle/bin/java -jar uber-apk-signer/uber-apk-signer-0.8.0.jar -a tmp
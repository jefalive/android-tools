.class public final Lbr/com/itau/sdk/android/leitorboleto/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/sdk/android/leitorboleto/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f0c0169

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f0c016a

.field public static final abc_color_highlight_material:I = 0x7f0c016c

.field public static final abc_input_method_navigation_guard:I = 0x7f0c0001

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f0c016f

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f0c0170

.field public static final abc_primary_text_material_dark:I = 0x7f0c0171

.field public static final abc_primary_text_material_light:I = 0x7f0c0172

.field public static final abc_search_url_text:I = 0x7f0c0173

.field public static final abc_search_url_text_normal:I = 0x7f0c0002

.field public static final abc_search_url_text_pressed:I = 0x7f0c0003

.field public static final abc_search_url_text_selected:I = 0x7f0c0004

.field public static final abc_secondary_text_material_dark:I = 0x7f0c0174

.field public static final abc_secondary_text_material_light:I = 0x7f0c0175

.field public static final accent_material_dark:I = 0x7f0c0005

.field public static final accent_material_light:I = 0x7f0c0006

.field public static final background_floating_material_dark:I = 0x7f0c0012

.field public static final background_floating_material_light:I = 0x7f0c0013

.field public static final background_material_dark:I = 0x7f0c0014

.field public static final background_material_light:I = 0x7f0c0015

.field public static final bright_foreground_disabled_material_dark:I = 0x7f0c0027

.field public static final bright_foreground_disabled_material_light:I = 0x7f0c0028

.field public static final bright_foreground_inverse_material_dark:I = 0x7f0c0029

.field public static final bright_foreground_inverse_material_light:I = 0x7f0c002a

.field public static final bright_foreground_material_dark:I = 0x7f0c002b

.field public static final bright_foreground_material_light:I = 0x7f0c002c

.field public static final button_material_dark:I = 0x7f0c002d

.field public static final button_material_light:I = 0x7f0c002e

.field public static final dim_foreground_disabled_material_dark:I = 0x7f0c0094

.field public static final dim_foreground_disabled_material_light:I = 0x7f0c0095

.field public static final dim_foreground_material_dark:I = 0x7f0c0096

.field public static final dim_foreground_material_light:I = 0x7f0c0097

.field public static final foreground_material_dark:I = 0x7f0c009a

.field public static final foreground_material_light:I = 0x7f0c009b

.field public static final highlighted_text_material_dark:I = 0x7f0c00b3

.field public static final highlighted_text_material_light:I = 0x7f0c00b4

.field public static final material_blue_grey_800:I = 0x7f0c00d5

.field public static final material_blue_grey_900:I = 0x7f0c00d6

.field public static final material_blue_grey_950:I = 0x7f0c00d7

.field public static final material_deep_teal_200:I = 0x7f0c00d8

.field public static final material_deep_teal_500:I = 0x7f0c00d9

.field public static final material_grey_100:I = 0x7f0c00da

.field public static final material_grey_300:I = 0x7f0c00db

.field public static final material_grey_50:I = 0x7f0c00dc

.field public static final material_grey_600:I = 0x7f0c00dd

.field public static final material_grey_800:I = 0x7f0c00de

.field public static final material_grey_850:I = 0x7f0c00df

.field public static final material_grey_900:I = 0x7f0c00e0

.field public static final primary_dark_material_dark:I = 0x7f0c0120

.field public static final primary_dark_material_light:I = 0x7f0c0121

.field public static final primary_material_dark:I = 0x7f0c0122

.field public static final primary_material_light:I = 0x7f0c0123

.field public static final primary_text_default_material_dark:I = 0x7f0c0124

.field public static final primary_text_default_material_light:I = 0x7f0c0125

.field public static final primary_text_disabled_material_dark:I = 0x7f0c0126

.field public static final primary_text_disabled_material_light:I = 0x7f0c0127

.field public static final ripple_material_dark:I = 0x7f0c0129

.field public static final ripple_material_light:I = 0x7f0c012a

.field public static final secondary_text_default_material_dark:I = 0x7f0c0134

.field public static final secondary_text_default_material_light:I = 0x7f0c0135

.field public static final secondary_text_disabled_material_dark:I = 0x7f0c0136

.field public static final secondary_text_disabled_material_light:I = 0x7f0c0137

.field public static final switch_thumb_disabled_material_dark:I = 0x7f0c0141

.field public static final switch_thumb_disabled_material_light:I = 0x7f0c0142

.field public static final switch_thumb_material_dark:I = 0x7f0c018a

.field public static final switch_thumb_material_light:I = 0x7f0c018b

.field public static final switch_thumb_normal_material_dark:I = 0x7f0c0143

.field public static final switch_thumb_normal_material_light:I = 0x7f0c0144

.field public static final zxing_contents_text:I = 0x7f0c015e

.field public static final zxing_encode_view:I = 0x7f0c015f

.field public static final zxing_possible_result_points:I = 0x7f0c0160

.field public static final zxing_result_minor_text:I = 0x7f0c0161

.field public static final zxing_result_points:I = 0x7f0c0162

.field public static final zxing_result_text:I = 0x7f0c0163

.field public static final zxing_result_view:I = 0x7f0c0164

.field public static final zxing_status_text:I = 0x7f0c0165

.field public static final zxing_transparent:I = 0x7f0c0166

.field public static final zxing_viewfinder_laser:I = 0x7f0c0167

.field public static final zxing_viewfinder_mask:I = 0x7f0c0168


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

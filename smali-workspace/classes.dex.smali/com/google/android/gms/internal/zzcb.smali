.class public Lcom/google/android/gms/internal/zzcb;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# instance fields
.field private final zzpV:Ljava/lang/Object;

.field private final zzxA:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field private zzxB:Ljava/lang/String;

.field private zzxC:Lcom/google/android/gms/internal/zzbz;

.field private zzxD:Lcom/google/android/gms/internal/zzcb;

.field zzxi:Z

.field private final zzxz:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/google/android/gms/internal/zzbz;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;)V
    .registers 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzcb;->zzxz:Ljava/util/List;

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzcb;->zzxA:Ljava/util/Map;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzcb;->zzpV:Ljava/lang/Object;

    iput-boolean p1, p0, Lcom/google/android/gms/internal/zzcb;->zzxi:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/zzcb;->zzxA:Ljava/util/Map;

    const-string v1, "action"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzcb;->zzxA:Ljava/util/Map;

    const-string v1, "ad_format"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public zzN(Ljava/lang/String;)V
    .registers 5

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzcb;->zzxi:Z

    if-nez v0, :cond_5

    return-void

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/internal/zzcb;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_8
    iput-object p1, p0, Lcom/google/android/gms/internal/zzcb;->zzxB:Ljava/lang/String;
    :try_end_a
    .catchall {:try_start_8 .. :try_end_a} :catchall_c

    monitor-exit v1

    goto :goto_f

    :catchall_c
    move-exception v2

    monitor-exit v1

    throw v2

    :goto_f
    return-void
.end method

.method public varargs zza(Lcom/google/android/gms/internal/zzbz;J[Ljava/lang/String;)Z
    .registers 13

    iget-object v1, p0, Lcom/google/android/gms/internal/zzcb;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    move-object v2, p4

    :try_start_4
    array-length v3, v2

    const/4 v4, 0x0

    :goto_6
    if-ge v4, v3, :cond_17

    aget-object v5, v2, v4

    new-instance v6, Lcom/google/android/gms/internal/zzbz;

    invoke-direct {v6, p2, p3, v5, p1}, Lcom/google/android/gms/internal/zzbz;-><init>(JLjava/lang/String;Lcom/google/android/gms/internal/zzbz;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzcb;->zzxz:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_14
    .catchall {:try_start_4 .. :try_end_14} :catchall_1a

    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :cond_17
    monitor-exit v1

    const/4 v0, 0x1

    return v0

    :catchall_1a
    move-exception v7

    monitor-exit v1

    throw v7
.end method

.method public varargs zza(Lcom/google/android/gms/internal/zzbz;[Ljava/lang/String;)Z
    .registers 5

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzcb;->zzxi:Z

    if-eqz v0, :cond_6

    if-nez p1, :cond_8

    :cond_6
    const/4 v0, 0x0

    return v0

    :cond_8
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbG()Lcom/google/android/gms/internal/zzmq;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzmq;->elapsedRealtime()J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1, p2}, Lcom/google/android/gms/internal/zzcb;->zza(Lcom/google/android/gms/internal/zzbz;J[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public zzb(J)Lcom/google/android/gms/internal/zzbz;
    .registers 6

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzcb;->zzxi:Z

    if-nez v0, :cond_6

    const/4 v0, 0x0

    return-object v0

    :cond_6
    new-instance v2, Lcom/google/android/gms/internal/zzbz;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {v2, p1, p2, v0, v1}, Lcom/google/android/gms/internal/zzbz;-><init>(JLjava/lang/String;Lcom/google/android/gms/internal/zzbz;)V

    return-object v2
.end method

.method public zzc(Lcom/google/android/gms/internal/zzcb;)V
    .registers 4

    iget-object v0, p0, Lcom/google/android/gms/internal/zzcb;->zzpV:Ljava/lang/Object;

    monitor-enter v0

    :try_start_3
    iput-object p1, p0, Lcom/google/android/gms/internal/zzcb;->zzxD:Lcom/google/android/gms/internal/zzcb;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v0

    goto :goto_a

    :catchall_7
    move-exception v1

    monitor-exit v0

    throw v1

    :goto_a
    return-void
.end method

.method public zzc(Ljava/lang/String;Ljava/lang/String;)V
    .registers 8

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzcb;->zzxi:Z

    if-eqz v0, :cond_a

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_a
    return-void

    :cond_b
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbF()Lcom/google/android/gms/internal/zzih;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzih;->zzhb()Lcom/google/android/gms/internal/zzbv;

    move-result-object v2

    if-nez v2, :cond_16

    return-void

    :cond_16
    iget-object v3, p0, Lcom/google/android/gms/internal/zzcb;->zzpV:Ljava/lang/Object;

    monitor-enter v3

    :try_start_19
    invoke-virtual {v2, p1}, Lcom/google/android/gms/internal/zzbv;->zzL(Ljava/lang/String;)Lcom/google/android/gms/internal/zzby;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzcb;->zzxA:Ljava/util/Map;

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/gms/internal/zzby;->zza(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_22
    .catchall {:try_start_19 .. :try_end_22} :catchall_24

    monitor-exit v3

    goto :goto_27

    :catchall_24
    move-exception v4

    monitor-exit v3

    throw v4

    :goto_27
    return-void
.end method

.method public zzdB()Lcom/google/android/gms/internal/zzbz;
    .registers 3

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbG()Lcom/google/android/gms/internal/zzmq;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzmq;->elapsedRealtime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/zzcb;->zzb(J)Lcom/google/android/gms/internal/zzbz;

    move-result-object v0

    return-object v0
.end method

.method public zzdC()V
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzcb;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzcb;->zzdB()Lcom/google/android/gms/internal/zzbz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzcb;->zzxC:Lcom/google/android/gms/internal/zzbz;
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_b

    monitor-exit v1

    goto :goto_e

    :catchall_b
    move-exception v2

    monitor-exit v1

    throw v2

    :goto_e
    return-void
.end method

.method public zzdD()Ljava/lang/String;
    .registers 14

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/internal/zzcb;->zzpV:Ljava/lang/Object;

    monitor-enter v3

    :try_start_8
    iget-object v0, p0, Lcom/google/android/gms/internal/zzcb;->zzxz:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_e
    :goto_e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/gms/internal/zzbz;

    invoke-virtual {v5}, Lcom/google/android/gms/internal/zzbz;->getTime()J

    move-result-wide v6

    invoke-virtual {v5}, Lcom/google/android/gms/internal/zzbz;->zzdy()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5}, Lcom/google/android/gms/internal/zzbz;->zzdz()Lcom/google/android/gms/internal/zzbz;

    move-result-object v9

    const-wide/16 v10, 0x0

    if-eqz v9, :cond_e

    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-lez v0, :cond_e

    invoke-virtual {v9}, Lcom/google/android/gms/internal/zzbz;->getTime()J

    move-result-wide v0

    sub-long v10, v6, v0

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_e

    :cond_4b
    iget-object v0, p0, Lcom/google/android/gms/internal/zzcb;->zzxz:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzcb;->zzxB:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5e

    iget-object v0, p0, Lcom/google/android/gms/internal/zzcb;->zzxB:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6d

    :cond_5e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_6d

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    :cond_6d
    :goto_6d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_70
    .catchall {:try_start_8 .. :try_end_70} :catchall_73

    move-result-object v0

    monitor-exit v3

    return-object v0

    :catchall_73
    move-exception v12

    monitor-exit v3

    throw v12
.end method

.method public zzdE()Lcom/google/android/gms/internal/zzbz;
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzcb;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzcb;->zzxC:Lcom/google/android/gms/internal/zzbz;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v1

    return-object v0

    :catchall_7
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method zzn()Ljava/util/Map;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/gms/internal/zzcb;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbF()Lcom/google/android/gms/internal/zzih;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzih;->zzhb()Lcom/google/android/gms/internal/zzbv;

    move-result-object v3

    if-eqz v3, :cond_11

    iget-object v0, p0, Lcom/google/android/gms/internal/zzcb;->zzxD:Lcom/google/android/gms/internal/zzcb;

    if-nez v0, :cond_15

    :cond_11
    iget-object v0, p0, Lcom/google/android/gms/internal/zzcb;->zzxA:Ljava/util/Map;
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_23

    monitor-exit v2

    return-object v0

    :cond_15
    :try_start_15
    iget-object v0, p0, Lcom/google/android/gms/internal/zzcb;->zzxA:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzcb;->zzxD:Lcom/google/android/gms/internal/zzcb;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zzcb;->zzn()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lcom/google/android/gms/internal/zzbv;->zza(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;
    :try_end_20
    .catchall {:try_start_15 .. :try_end_20} :catchall_23

    move-result-object v0

    monitor-exit v2

    return-object v0

    :catchall_23
    move-exception v4

    monitor-exit v2

    throw v4
.end method

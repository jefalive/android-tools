.class Lcom/adobe/mobile/MessageTemplateCallback;
.super Lcom/adobe/mobile/Message;
.source "MessageTemplateCallback.java"


# static fields
.field private static final tokenDataMask:[Z


# instance fields
.field private _combinedVariablesCopy:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation
.end field

.field protected contentType:Ljava/lang/String;

.field private final randomGen:Ljava/security/SecureRandom;

.field protected templateBody:Ljava/lang/String;

.field protected templateUrl:Ljava/lang/String;

.field protected timeout:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 342
    const/16 v0, 0x100

    new-array v0, v0, [Z

    fill-array-data v0, :array_a

    sput-object v0, Lcom/adobe/mobile/MessageTemplateCallback;->tokenDataMask:[Z

    return-void

    :array_a
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x1t
        0x0t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method constructor <init>()V
    .registers 2

    .line 29
    invoke-direct {p0}, Lcom/adobe/mobile/Message;-><init>()V

    .line 38
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    iput-object v0, p0, Lcom/adobe/mobile/MessageTemplateCallback;->randomGen:Ljava/security/SecureRandom;

    return-void
.end method

.method private buildExpansionsForTokens(Ljava/util/ArrayList;Z)Ljava/util/HashMap;
    .registers 12
    .param p1, "tokens"    # Ljava/util/ArrayList;
    .param p2, "urlEncodeExpansions"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Ljava/lang/String;>;Z)Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
        }
    .end annotation

    .line 279
    new-instance v2, Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 281
    .local v2, "expansions":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_44

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/String;

    .line 282
    .local v4, "token":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x1

    invoke-virtual {v4, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    .line 283
    .local v5, "cleanToken":Ljava/lang/String;
    iget-object v0, p0, Lcom/adobe/mobile/MessageTemplateCallback;->_combinedVariablesCopy:Ljava/util/HashMap;

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 284
    .local v6, "tokenObject":Ljava/lang/Object;
    if-nez v6, :cond_34

    const-string v7, ""

    goto :goto_38

    :cond_34
    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    .line 286
    .local v7, "tokenString":Ljava/lang/String;
    :goto_38
    if-eqz p2, :cond_3f

    invoke-static {v7}, Lcom/adobe/mobile/StaticMethods;->URLEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto :goto_40

    :cond_3f
    move-object v8, v7

    .line 287
    .local v8, "tokenValue":Ljava/lang/String;
    :goto_40
    invoke-virtual {v2, v4, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    .end local v4    # "token":Ljava/lang/String;
    .end local v5    # "cleanToken":Ljava/lang/String;
    .end local v6    # "tokenObject":Ljava/lang/Object;
    .end local v7    # "tokenString":Ljava/lang/String;
    .end local v8    # "tokenValue":Ljava/lang/String;
    goto :goto_d

    .line 290
    :cond_44
    return-object v2
.end method

.method private findTokensForExpansion(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 9
    .param p1, "input"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)Ljava/util/ArrayList<Ljava/lang/String;>;"
        }
    .end annotation

    .line 294
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_a

    .line 295
    :cond_8
    const/4 v0, 0x0

    return-object v0

    .line 299
    :cond_a
    new-instance v2, Ljava/util/ArrayList;

    const/16 v0, 0x20

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 301
    .local v2, "foundList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    .line 304
    .local v3, "inputLength":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_16
    if-ge v4, v3, :cond_52

    .line 306
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x7b

    if-ne v0, v1, :cond_4f

    .line 309
    add-int/lit8 v5, v4, 0x1

    .local v5, "j":I
    :goto_22
    if-ge v5, v3, :cond_30

    .line 310
    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x7d

    if-ne v0, v1, :cond_2d

    .line 311
    goto :goto_30

    .line 309
    :cond_2d
    add-int/lit8 v5, v5, 0x1

    goto :goto_22

    .line 316
    :cond_30
    :goto_30
    if-ne v5, v3, :cond_33

    .line 317
    goto :goto_52

    .line 321
    :cond_33
    add-int/lit8 v0, v5, 0x1

    invoke-virtual {p1, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 324
    .local v6, "token":Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x1

    invoke-virtual {v6, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/adobe/mobile/MessageTemplateCallback;->tokenIsValid(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4b

    .line 325
    goto :goto_4f

    .line 329
    :cond_4b
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 332
    move v4, v5

    .line 304
    .end local v5    # "j":I
    .end local v6    # "token":Ljava/lang/String;
    :cond_4f
    :goto_4f
    add-int/lit8 v4, v4, 0x1

    goto :goto_16

    .line 336
    .end local v4    # "i":I
    :cond_52
    :goto_52
    return-object v2
.end method

.method private getExpandedBody()Ljava/lang/String;
    .registers 8

    .line 204
    const/4 v2, 0x0

    .line 205
    .local v2, "expandedBody":Ljava/lang/String;
    iget-object v0, p0, Lcom/adobe/mobile/MessageTemplateCallback;->templateBody:Ljava/lang/String;

    if-eqz v0, :cond_4b

    iget-object v0, p0, Lcom/adobe/mobile/MessageTemplateCallback;->templateBody:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4b

    .line 207
    iget-object v0, p0, Lcom/adobe/mobile/MessageTemplateCallback;->contentType:Ljava/lang/String;

    if-nez v0, :cond_13

    const/4 v3, 0x0

    goto :goto_1f

    :cond_13
    iget-object v0, p0, Lcom/adobe/mobile/MessageTemplateCallback;->contentType:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "application/json"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    .line 208
    .local v3, "contentTypeIsJson":Z
    :goto_1f
    iget-object v0, p0, Lcom/adobe/mobile/MessageTemplateCallback;->templateBody:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/adobe/mobile/MessageTemplateCallback;->findTokensForExpansion(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v3, :cond_29

    const/4 v1, 0x1

    goto :goto_2a

    :cond_29
    const/4 v1, 0x0

    :goto_2a
    invoke-direct {p0, v0, v1}, Lcom/adobe/mobile/MessageTemplateCallback;->buildExpansionsForTokens(Ljava/util/ArrayList;Z)Ljava/util/HashMap;

    move-result-object v4

    .line 210
    .local v4, "bodyExpansions":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 211
    .local v5, "nonEncodedParamsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v0, "{%all_url%}"

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    const-string v0, "{%all_json%}"

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    const/4 v0, 0x0

    invoke-direct {p0, v5, v0}, Lcom/adobe/mobile/MessageTemplateCallback;->buildExpansionsForTokens(Ljava/util/ArrayList;Z)Ljava/util/HashMap;

    move-result-object v6

    .line 215
    .local v6, "nonEncodedURLExpansions":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v4, v6}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 217
    iget-object v0, p0, Lcom/adobe/mobile/MessageTemplateCallback;->templateBody:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/adobe/mobile/StaticMethods;->expandTokens(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    .line 220
    .end local v3    # "contentTypeIsJson":Z
    .end local v4    # "bodyExpansions":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4
    .end local v5    # "nonEncodedParamsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5
    .end local v6    # "nonEncodedURLExpansions":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v6
    :cond_4b
    return-object v2
.end method

.method private getExpansionTokensForVariables(Ljava/util/HashMap;)Ljava/util/HashMap;
    .registers 14
    .param p1, "vars"    # Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;)Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation

    .line 235
    new-instance v4, Ljava/util/HashMap;

    const/4 v0, 0x5

    invoke-direct {v4, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 237
    .local v4, "expansionTokens":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "%sdkver%"

    const-string v1, "4.13.1-AN"

    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    const-string v0, "%cachebust%"

    iget-object v1, p0, Lcom/adobe/mobile/MessageTemplateCallback;->randomGen:Ljava/security/SecureRandom;

    const v2, 0x5f5e100

    invoke-virtual {v1, v2}, Ljava/security/SecureRandom;->nextInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    const-string v0, "%adid%"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getAdvertisingIdentifier()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    const-string v0, "%timestampu%"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getTimeSince1970()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    const-string v0, "%timestampz%"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getIso8601Date()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    const-string v0, "%pushid%"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getPushIdentifier()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    const-string v0, "%mcid%"

    invoke-static {}, Lcom/adobe/mobile/VisitorIDService;->sharedInstance()Lcom/adobe/mobile/VisitorIDService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adobe/mobile/VisitorIDService;->getMarketingCloudID()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5c

    invoke-static {}, Lcom/adobe/mobile/VisitorIDService;->sharedInstance()Lcom/adobe/mobile/VisitorIDService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/adobe/mobile/VisitorIDService;->getMarketingCloudID()Ljava/lang/String;

    move-result-object v1

    goto :goto_5e

    :cond_5c
    const-string v1, ""

    :goto_5e
    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 246
    .local v5, "queryParameters":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 247
    .local v6, "cleanVars":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-virtual {p1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_73
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/lang/String;

    .line 248
    .local v8, "key":Ljava/lang/String;
    if-nez v8, :cond_83

    .line 249
    goto :goto_73

    .line 252
    :cond_83
    invoke-virtual {p1, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    .line 253
    .local v9, "value":Ljava/lang/Object;
    if-nez v9, :cond_8c

    const-string v10, ""

    goto :goto_90

    :cond_8c
    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    .line 255
    .local v10, "cleanValue":Ljava/lang/String;
    :goto_90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v8}, Lcom/adobe/mobile/StaticMethods;->URLEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v10}, Lcom/adobe/mobile/StaticMethods;->URLEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 257
    .local v11, "queryParameterString":Ljava/lang/String;
    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 259
    invoke-virtual {v6, v8, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 260
    .end local v8    # "key":Ljava/lang/String;
    .end local v9    # "value":Ljava/lang/Object;
    .end local v10    # "cleanValue":Ljava/lang/String;
    .end local v11    # "queryParameterString":Ljava/lang/String;
    goto :goto_73

    .line 262
    :cond_b6
    const-string v0, "%all_url%"

    const-string v1, "&"

    invoke-static {v1, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    :try_start_c1
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, v6}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 266
    .local v7, "jsonObject":Lorg/json/JSONObject;
    invoke-virtual {v7}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v8

    .line 268
    .local v8, "jsonString":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_d5

    .line 269
    const-string v0, "%all_json%"

    invoke-virtual {v4, v0, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_d5
    .catch Ljava/lang/NullPointerException; {:try_start_c1 .. :try_end_d5} :catch_d6

    .line 273
    .end local v7    # "jsonObject":Lorg/json/JSONObject;
    .end local v8    # "jsonString":Ljava/lang/String;
    :cond_d5
    goto :goto_e6

    .line 271
    :catch_d6
    move-exception v7

    .line 272
    .local v7, "ex":Ljava/lang/NullPointerException;
    const-string v0, "Data Callback - unable to create json string for vars:  (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/lang/NullPointerException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 275
    .end local v7    # "ex":Ljava/lang/NullPointerException;
    :goto_e6
    return-object v4
.end method

.method private tokenIsValid(Ljava/lang/String;)Z
    .registers 11
    .param p1, "token"    # Ljava/lang/String;

    .line 363
    const-string v0, "UTF-8"

    :try_start_2
    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    .line 366
    .local v4, "utf8Token":[B
    move-object v5, v4

    array-length v6, v5

    const/4 v7, 0x0

    :goto_9
    if-ge v7, v6, :cond_1a

    aget-byte v8, v5, v7

    .line 368
    .local v8, "currentByte":B
    sget-object v0, Lcom/adobe/mobile/MessageTemplateCallback;->tokenDataMask:[Z

    and-int/lit16 v1, v8, 0xff

    aget-boolean v0, v0, v1
    :try_end_13
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_13} :catch_1b

    if-nez v0, :cond_17

    .line 369
    const/4 v0, 0x0

    return v0

    .line 366
    .end local v8    # "currentByte":B
    :cond_17
    add-int/lit8 v7, v7, 0x1

    goto :goto_9

    .line 376
    .end local v4    # "utf8Token":[B
    :cond_1a
    goto :goto_2d

    .line 373
    :catch_1b
    move-exception v4

    .line 374
    .local v4, "ex":Ljava/io/UnsupportedEncodingException;
    const-string v0, "Data Callback - Unable to validate token (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/io/UnsupportedEncodingException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 375
    const/4 v0, 0x0

    return v0

    .line 378
    .end local v4    # "ex":Ljava/io/UnsupportedEncodingException;
    :goto_2d
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method protected getExpandedUrl()Ljava/lang/String;
    .registers 7

    .line 189
    const/4 v2, 0x0

    .line 191
    .local v2, "expandedUrl":Ljava/lang/String;
    iget-object v0, p0, Lcom/adobe/mobile/MessageTemplateCallback;->templateUrl:Ljava/lang/String;

    if-eqz v0, :cond_30

    iget-object v0, p0, Lcom/adobe/mobile/MessageTemplateCallback;->templateUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_30

    .line 192
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 193
    .local v3, "nonEncodedParamsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v0, "{%all_url%}"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    iget-object v0, p0, Lcom/adobe/mobile/MessageTemplateCallback;->templateUrl:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/adobe/mobile/MessageTemplateCallback;->findTokensForExpansion(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/adobe/mobile/MessageTemplateCallback;->buildExpansionsForTokens(Ljava/util/ArrayList;Z)Ljava/util/HashMap;

    move-result-object v4

    .line 195
    .local v4, "urlExpansions":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v0, 0x0

    invoke-direct {p0, v3, v0}, Lcom/adobe/mobile/MessageTemplateCallback;->buildExpansionsForTokens(Ljava/util/ArrayList;Z)Ljava/util/HashMap;

    move-result-object v5

    .line 196
    .local v5, "nonEncodedURLExpansions":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v4, v5}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 197
    iget-object v0, p0, Lcom/adobe/mobile/MessageTemplateCallback;->templateUrl:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/adobe/mobile/StaticMethods;->expandTokens(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    .line 199
    .end local v3    # "nonEncodedParamsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3
    .end local v4    # "urlExpansions":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4
    .end local v5    # "nonEncodedURLExpansions":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v5
    :cond_30
    return-object v2
.end method

.method protected getQueue()Lcom/adobe/mobile/ThirdPartyQueue;
    .registers 2

    .line 226
    invoke-static {}, Lcom/adobe/mobile/ThirdPartyQueue;->sharedInstance()Lcom/adobe/mobile/ThirdPartyQueue;

    move-result-object v0

    return-object v0
.end method

.method protected initWithPayloadObject(Lorg/json/JSONObject;)Z
    .registers 11
    .param p1, "dictionary"    # Lorg/json/JSONObject;

    .line 70
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-gtz v0, :cond_a

    .line 71
    :cond_8
    const/4 v0, 0x0

    return v0

    .line 75
    :cond_a
    invoke-super {p0, p1}, Lcom/adobe/mobile/Message;->initWithPayloadObject(Lorg/json/JSONObject;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 76
    const/4 v0, 0x0

    return v0

    .line 81
    :cond_12
    invoke-virtual {p0}, Lcom/adobe/mobile/MessageTemplateCallback;->logPrefix()Ljava/lang/String;

    move-result-object v5

    .line 84
    .local v5, "logPrefix":Ljava/lang/String;
    const-string v0, "payload"

    :try_start_18
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 85
    .local v4, "jsonPayload":Lorg/json/JSONObject;
    invoke-virtual {v4}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-gtz v0, :cond_34

    .line 86
    const-string v0, "%s - Unable to create data callback %s, \"payload\" is empty"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v5, v1, v2

    iget-object v2, p0, Lcom/adobe/mobile/MessageTemplateCallback;->messageId:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_32
    .catch Lorg/json/JSONException; {:try_start_18 .. :try_end_32} :catch_35

    .line 87
    const/4 v0, 0x0

    return v0

    .line 93
    :cond_34
    goto :goto_48

    .line 90
    .end local v4    # "jsonPayload":Lorg/json/JSONObject;
    :catch_35
    move-exception v6

    .line 91
    .local v6, "ex":Lorg/json/JSONException;
    const-string v0, "%s - Unable to create create data callback %s, \"payload\" is required"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v5, v1, v2

    iget-object v2, p0, Lcom/adobe/mobile/MessageTemplateCallback;->messageId:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    const/4 v0, 0x0

    return v0

    .line 97
    .local v4, "jsonPayload":Lorg/json/JSONObject;
    .end local v6    # "ex":Lorg/json/JSONException;
    :goto_48
    const-string v0, "templateurl"

    :try_start_4a
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MessageTemplateCallback;->templateUrl:Ljava/lang/String;

    .line 98
    iget-object v0, p0, Lcom/adobe/mobile/MessageTemplateCallback;->templateUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_6a

    .line 99
    const-string v0, "%s - Unable to create data callback %s, \"templateurl\" is empty"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v5, v1, v2

    iget-object v2, p0, Lcom/adobe/mobile/MessageTemplateCallback;->messageId:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_68
    .catch Lorg/json/JSONException; {:try_start_4a .. :try_end_68} :catch_6b

    .line 100
    const/4 v0, 0x0

    return v0

    .line 106
    :cond_6a
    goto :goto_7e

    .line 103
    :catch_6b
    move-exception v6

    .line 104
    .local v6, "ex":Lorg/json/JSONException;
    const-string v0, "%s - Unable to create data callback %s, \"templateurl\" is required"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v5, v1, v2

    iget-object v2, p0, Lcom/adobe/mobile/MessageTemplateCallback;->messageId:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    const/4 v0, 0x0

    return v0

    .line 110
    .end local v6    # "ex":Lorg/json/JSONException;
    :goto_7e
    const-string v0, "timeout"

    :try_start_80
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/adobe/mobile/MessageTemplateCallback;->timeout:I
    :try_end_86
    .catch Lorg/json/JSONException; {:try_start_80 .. :try_end_86} :catch_87

    .line 115
    goto :goto_96

    .line 112
    :catch_87
    move-exception v6

    .line 113
    .local v6, "ex":Lorg/json/JSONException;
    const-string v0, "%s - Tried to read \"timeout\" for data callback, but found none.  Using default value of two (2) seconds"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v5, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 114
    const/4 v0, 0x2

    iput v0, p0, Lcom/adobe/mobile/MessageTemplateCallback;->timeout:I

    .line 120
    .end local v6    # "ex":Lorg/json/JSONException;
    :goto_96
    const-string v0, "templatebody"

    :try_start_98
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 121
    .local v6, "tempBody":Ljava/lang/String;
    if-eqz v6, :cond_ba

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_ba

    .line 123
    const/4 v0, 0x0

    invoke-static {v6, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v7

    .line 125
    .local v7, "decodedBytes":[B
    if-eqz v7, :cond_ba

    .line 126
    new-instance v8, Ljava/lang/String;

    const-string v0, "UTF-8"

    invoke-direct {v8, v7, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 127
    .local v8, "decodedBody":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_ba

    .line 128
    iput-object v8, p0, Lcom/adobe/mobile/MessageTemplateCallback;->templateBody:Ljava/lang/String;
    :try_end_ba
    .catch Lorg/json/JSONException; {:try_start_98 .. :try_end_ba} :catch_bb
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_98 .. :try_end_ba} :catch_c8
    .catch Ljava/lang/IllegalArgumentException; {:try_start_98 .. :try_end_ba} :catch_dc

    .line 141
    .end local v6    # "tempBody":Ljava/lang/String;
    .end local v7    # "decodedBytes":[B
    .end local v8    # "decodedBody":Ljava/lang/String;
    :cond_ba
    goto :goto_ef

    .line 133
    :catch_bb
    move-exception v6

    .line 134
    .local v6, "ex":Lorg/json/JSONException;
    const-string v0, "%s - Tried to read \"templatebody\" for data callback, but found none.  This is not a required field"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v5, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 141
    .end local v6    # "ex":Lorg/json/JSONException;
    goto :goto_ef

    .line 136
    :catch_c8
    move-exception v6

    .line 137
    .local v6, "ex":Ljava/io/UnsupportedEncodingException;
    const-string v0, "%s - Failed to decode \"templatebody\" for data callback (%s).  This is not a required field"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v5, v1, v2

    invoke-virtual {v6}, Ljava/io/UnsupportedEncodingException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 141
    .end local v6    # "ex":Ljava/io/UnsupportedEncodingException;
    goto :goto_ef

    .line 139
    :catch_dc
    move-exception v6

    .line 140
    .local v6, "ex":Ljava/lang/IllegalArgumentException;
    const-string v0, "%s - Failed to decode \"templatebody\" for data callback (%s).  This is not a required field"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v5, v1, v2

    invoke-virtual {v6}, Ljava/lang/IllegalArgumentException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 144
    .end local v6    # "ex":Ljava/lang/IllegalArgumentException;
    :goto_ef
    iget-object v0, p0, Lcom/adobe/mobile/MessageTemplateCallback;->templateBody:Ljava/lang/String;

    if-eqz v0, :cond_110

    iget-object v0, p0, Lcom/adobe/mobile/MessageTemplateCallback;->templateBody:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_110

    .line 146
    const-string v0, "contenttype"

    :try_start_fd
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MessageTemplateCallback;->contentType:Ljava/lang/String;
    :try_end_103
    .catch Lorg/json/JSONException; {:try_start_fd .. :try_end_103} :catch_104

    .line 150
    goto :goto_110

    .line 148
    :catch_104
    move-exception v6

    .line 149
    .local v6, "ex":Lorg/json/JSONException;
    const-string v0, "%s - Tried to read \"contenttype\" for data callback, but found none.  This is not a required field"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v5, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 153
    .end local v6    # "ex":Lorg/json/JSONException;
    :cond_110
    :goto_110
    const/4 v0, 0x1

    return v0
.end method

.method protected logPrefix()Ljava/lang/String;
    .registers 2

    .line 231
    const-string v0, "Postbacks"

    return-object v0
.end method

.method protected shouldShowForVariables(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Z
    .registers 6
    .param p1, "vars"    # Ljava/util/Map;
    .param p2, "cdata"    # Ljava/util/Map;
    .param p3, "lifecycleData"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;)Z"
        }
    .end annotation

    .line 158
    if-eqz p2, :cond_8

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, p2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    goto :goto_d

    :cond_8
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 160
    .local v1, "combinedVars":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :goto_d
    if-eqz p1, :cond_12

    .line 161
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 164
    :cond_12
    invoke-direct {p0, v1}, Lcom/adobe/mobile/MessageTemplateCallback;->getExpansionTokensForVariables(Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 166
    if-eqz p3, :cond_1e

    .line 167
    invoke-virtual {v1, p3}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 170
    :cond_1e
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/adobe/mobile/MessageTemplateCallback;->_combinedVariablesCopy:Ljava/util/HashMap;

    .line 172
    invoke-super {p0, p1, p2, p3}, Lcom/adobe/mobile/Message;->shouldShowForVariables(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Z

    move-result v0

    return v0
.end method

.method protected show()V
    .registers 13

    .line 178
    invoke-virtual {p0}, Lcom/adobe/mobile/MessageTemplateCallback;->getExpandedUrl()Ljava/lang/String;

    move-result-object v8

    .line 179
    .local v8, "expandedUrl":Ljava/lang/String;
    invoke-direct {p0}, Lcom/adobe/mobile/MessageTemplateCallback;->getExpandedBody()Ljava/lang/String;

    move-result-object v9

    .line 180
    .local v9, "expandedBody":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/adobe/mobile/MessageTemplateCallback;->logPrefix()Ljava/lang/String;

    move-result-object v10

    .line 182
    .local v10, "logPrefix":Ljava/lang/String;
    const-string v0, "%s - Request Queued (url:%s body:%s contentType:%s)"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v10, v1, v2

    const/4 v2, 0x1

    aput-object v8, v1, v2

    const/4 v2, 0x2

    aput-object v9, v1, v2

    iget-object v2, p0, Lcom/adobe/mobile/MessageTemplateCallback;->contentType:Ljava/lang/String;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 183
    invoke-virtual {p0}, Lcom/adobe/mobile/MessageTemplateCallback;->getQueue()Lcom/adobe/mobile/ThirdPartyQueue;

    move-result-object v11

    .line 184
    .local v11, "currQueue":Lcom/adobe/mobile/ThirdPartyQueue;
    move-object v0, v11

    move-object v1, v8

    move-object v2, v9

    iget-object v3, p0, Lcom/adobe/mobile/MessageTemplateCallback;->contentType:Ljava/lang/String;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getTimeSince1970()J

    move-result-wide v4

    iget v6, p0, Lcom/adobe/mobile/MessageTemplateCallback;->timeout:I

    int-to-long v6, v6

    invoke-virtual/range {v0 .. v7}, Lcom/adobe/mobile/ThirdPartyQueue;->queue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 185
    return-void
.end method

.class public final Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;
.super Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;
.source "LisEscolhaValorActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 40
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;-><init>()V

    .line 44
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;

    .line 40
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;

    .line 40
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 59
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 60
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 61
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 62
    .local v1, "resources_":Landroid/content/res/Resources;
    const v0, 0x7f0801a1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->marginLisEscolhaValorSeekbar:F

    .line 63
    const v0, 0x7f0c00cd

    invoke-static {p0, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->corLaranja:I

    .line 64
    const v0, 0x7f0c0040

    invoke-static {p0, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->corCinza:I

    .line 65
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 66
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->injectExtras_()V

    .line 68
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->afterInject()V

    .line 69
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 159
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 160
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_3e

    .line 161
    const-string v0, "ofertaContratacaoLisVo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 162
    const-string v0, "ofertaContratacaoLisVo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    .line 164
    :cond_1c
    const-string v0, "possuiProdutoLisAdiciona"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 165
    const-string v0, "possuiProdutoLisAdiciona"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->possuiProdutoLisAdiciona:Z

    .line 167
    :cond_2c
    const-string v0, "valorDefault"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 168
    const-string v0, "valorDefault"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->valorDefault:Ljava/lang/Float;

    .line 171
    :cond_3e
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 196
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$5;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$5;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 204
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 184
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$4;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 192
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 52
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->init_(Landroid/os/Bundle;)V

    .line 53
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 55
    const v0, 0x7f03004a

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->setContentView(I)V

    .line 56
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 6
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 103
    const v0, 0x7f0e0225

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->seekBar:Landroid/widget/SeekBar;

    .line 104
    const v0, 0x7f0e0222

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/EditTextMonetario;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->selecionaValor:Lcom/itau/empresas/ui/view/EditTextMonetario;

    .line 105
    const v0, 0x7f0e0228

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->valorMinimoSelecionar:Landroid/widget/TextView;

    .line 106
    const v0, 0x7f0e022a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->valorMaximoSelecionar:Landroid/widget/TextView;

    .line 107
    const v0, 0x7f0e0224

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->valorTaxaJuros:Landroid/widget/TextView;

    .line 108
    const v0, 0x7f0e0221

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->cifrao:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f0e0223

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->separadorEditTex:Landroid/view/View;

    .line 110
    const v0, 0x7f0e0226

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->rlLisEscolhaValorMinMax:Landroid/view/View;

    .line 111
    const v0, 0x7f0e022b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 112
    .local v1, "view_botao_simular":Landroid/view/View;
    const v0, 0x7f0e021e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 114
    .local v2, "view_botao_fechar":Landroid/view/View;
    if-eqz v1, :cond_6c

    .line 115
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$1;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    :cond_6c
    if-eqz v2, :cond_76

    .line 125
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$2;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    :cond_76
    const v0, 0x7f0e0222

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/TextView;

    .line 136
    .local v3, "view":Landroid/widget/TextView;
    if-eqz v3, :cond_8a

    .line 137
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$3;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$3;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;)V

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 155
    .end local v3    # "view":Landroid/widget/TextView;
    :cond_8a
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->aoCarregarTela()V

    .line 156
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 73
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->setContentView(I)V

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 75
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 85
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->setContentView(Landroid/view/View;)V

    .line 86
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 87
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 79
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 81
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 175
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->setIntent(Landroid/content/Intent;)V

    .line 176
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_;->injectExtras_()V

    .line 177
    return-void
.end method

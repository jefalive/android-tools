.class final Lorg/threeten/bp/format/DateTimePrintContext;
.super Ljava/lang/Object;
.source "DateTimePrintContext.java"


# instance fields
.field private locale:Ljava/util/Locale;

.field private optional:I

.field private symbols:Lorg/threeten/bp/format/DecimalStyle;

.field private temporal:Lorg/threeten/bp/temporal/TemporalAccessor;


# direct methods
.method constructor <init>(Lorg/threeten/bp/temporal/TemporalAccessor;Lorg/threeten/bp/format/DateTimeFormatter;)V
    .registers 4
    .param p1, "temporal"    # Lorg/threeten/bp/temporal/TemporalAccessor;
    .param p2, "formatter"    # Lorg/threeten/bp/format/DateTimeFormatter;

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    invoke-static {p1, p2}, Lorg/threeten/bp/format/DateTimePrintContext;->adjust(Lorg/threeten/bp/temporal/TemporalAccessor;Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/temporal/TemporalAccessor;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->temporal:Lorg/threeten/bp/temporal/TemporalAccessor;

    .line 93
    invoke-virtual {p2}, Lorg/threeten/bp/format/DateTimeFormatter;->getLocale()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->locale:Ljava/util/Locale;

    .line 94
    invoke-virtual {p2}, Lorg/threeten/bp/format/DateTimeFormatter;->getDecimalStyle()Lorg/threeten/bp/format/DecimalStyle;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->symbols:Lorg/threeten/bp/format/DecimalStyle;

    .line 95
    return-void
.end method

.method private static adjust(Lorg/threeten/bp/temporal/TemporalAccessor;Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/temporal/TemporalAccessor;
    .registers 16
    .param p0, "temporal"    # Lorg/threeten/bp/temporal/TemporalAccessor;
    .param p1, "formatter"    # Lorg/threeten/bp/format/DateTimeFormatter;

    .line 106
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeFormatter;->getChronology()Lorg/threeten/bp/chrono/Chronology;

    move-result-object v3

    .line 107
    .local v3, "overrideChrono":Lorg/threeten/bp/chrono/Chronology;
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeFormatter;->getZone()Lorg/threeten/bp/ZoneId;

    move-result-object v4

    .line 108
    .local v4, "overrideZone":Lorg/threeten/bp/ZoneId;
    if-nez v3, :cond_d

    if-nez v4, :cond_d

    .line 109
    return-object p0

    .line 113
    :cond_d
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->chronology()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    invoke-interface {p0, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->query(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lorg/threeten/bp/chrono/Chronology;

    .line 114
    .local v5, "temporalChrono":Lorg/threeten/bp/chrono/Chronology;
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->zoneId()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    invoke-interface {p0, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->query(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lorg/threeten/bp/ZoneId;

    .line 115
    .local v6, "temporalZone":Lorg/threeten/bp/ZoneId;
    invoke-static {v5, v3}, Lorg/threeten/bp/jdk8/Jdk8Methods;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 116
    const/4 v3, 0x0

    .line 118
    :cond_2a
    invoke-static {v6, v4}, Lorg/threeten/bp/jdk8/Jdk8Methods;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 119
    const/4 v4, 0x0

    .line 121
    :cond_31
    if-nez v3, :cond_36

    if-nez v4, :cond_36

    .line 122
    return-object p0

    .line 124
    :cond_36
    if-eqz v3, :cond_3a

    move-object v7, v3

    goto :goto_3b

    :cond_3a
    move-object v7, v5

    .line 125
    .local v7, "effectiveChrono":Lorg/threeten/bp/chrono/Chronology;
    :goto_3b
    if-eqz v4, :cond_3f

    move-object v8, v4

    goto :goto_40

    :cond_3f
    move-object v8, v6

    .line 128
    .local v8, "effectiveZone":Lorg/threeten/bp/ZoneId;
    :goto_40
    if-eqz v4, :cond_97

    .line 130
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->INSTANT_SECONDS:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p0, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->isSupported(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v0

    if-eqz v0, :cond_59

    .line 131
    if-eqz v7, :cond_4e

    move-object v9, v7

    goto :goto_50

    :cond_4e
    sget-object v9, Lorg/threeten/bp/chrono/IsoChronology;->INSTANCE:Lorg/threeten/bp/chrono/IsoChronology;

    .line 132
    .local v9, "chrono":Lorg/threeten/bp/chrono/Chronology;
    :goto_50
    invoke-static {p0}, Lorg/threeten/bp/Instant;->from(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/Instant;

    move-result-object v0

    invoke-virtual {v9, v0, v4}, Lorg/threeten/bp/chrono/Chronology;->zonedDateTime(Lorg/threeten/bp/Instant;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/chrono/ChronoZonedDateTime;

    move-result-object v0

    return-object v0

    .line 135
    .end local v9    # "chrono":Lorg/threeten/bp/chrono/Chronology;
    :cond_59
    invoke-virtual {v4}, Lorg/threeten/bp/ZoneId;->normalized()Lorg/threeten/bp/ZoneId;

    move-result-object v9

    .line 136
    .local v9, "normalizedOffset":Lorg/threeten/bp/ZoneId;
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->offset()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    invoke-interface {p0, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->query(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lorg/threeten/bp/ZoneOffset;

    .line 137
    .local v10, "temporalOffset":Lorg/threeten/bp/ZoneOffset;
    instance-of v0, v9, Lorg/threeten/bp/ZoneOffset;

    if-eqz v0, :cond_97

    if-eqz v10, :cond_97

    invoke-virtual {v9, v10}, Lorg/threeten/bp/ZoneId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_97

    .line 138
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid override zone for temporal: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 142
    .end local v9    # "normalizedOffset":Lorg/threeten/bp/ZoneId;
    .end local v10    # "temporalOffset":Lorg/threeten/bp/ZoneOffset;
    :cond_97
    if-eqz v3, :cond_ea

    .line 143
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->EPOCH_DAY:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p0, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->isSupported(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v0

    if-eqz v0, :cond_a6

    .line 144
    invoke-virtual {v7, p0}, Lorg/threeten/bp/chrono/Chronology;->date(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/chrono/ChronoLocalDate;

    move-result-object v9

    .local v9, "effectiveDate":Lorg/threeten/bp/chrono/ChronoLocalDate;
    goto :goto_eb

    .line 147
    .end local v9    # "effectiveDate":Lorg/threeten/bp/chrono/ChronoLocalDate;
    :cond_a6
    sget-object v0, Lorg/threeten/bp/chrono/IsoChronology;->INSTANCE:Lorg/threeten/bp/chrono/IsoChronology;

    if-ne v3, v0, :cond_ac

    if-eqz v5, :cond_e8

    .line 148
    :cond_ac
    invoke-static {}, Lorg/threeten/bp/temporal/ChronoField;->values()[Lorg/threeten/bp/temporal/ChronoField;

    move-result-object v10

    .local v10, "arr$":[Lorg/threeten/bp/temporal/ChronoField;
    array-length v11, v10

    .local v11, "len$":I
    const/4 v12, 0x0

    .local v12, "i$":I
    :goto_b2
    if-ge v12, v11, :cond_e8

    aget-object v13, v10, v12

    .line 149
    .local v13, "f":Lorg/threeten/bp/temporal/ChronoField;
    invoke-virtual {v13}, Lorg/threeten/bp/temporal/ChronoField;->isDateBased()Z

    move-result v0

    if-eqz v0, :cond_e5

    invoke-interface {p0, v13}, Lorg/threeten/bp/temporal/TemporalAccessor;->isSupported(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v0

    if-eqz v0, :cond_e5

    .line 150
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid override chronology for temporal: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    .end local v13    # "f":Lorg/threeten/bp/temporal/ChronoField;
    :cond_e5
    add-int/lit8 v12, v12, 0x1

    goto :goto_b2

    .line 154
    .end local v10    # "arr$":[Lorg/threeten/bp/temporal/ChronoField;
    .end local v11    # "len$":I
    .end local v12    # "i$":I
    :cond_e8
    const/4 v9, 0x0

    .local v9, "effectiveDate":Lorg/threeten/bp/chrono/ChronoLocalDate;
    goto :goto_eb

    .line 157
    .end local v9    # "effectiveDate":Lorg/threeten/bp/chrono/ChronoLocalDate;
    :cond_ea
    const/4 v9, 0x0

    .line 161
    .local v9, "effectiveDate":Lorg/threeten/bp/chrono/ChronoLocalDate;
    :goto_eb
    new-instance v0, Lorg/threeten/bp/format/DateTimePrintContext$1;

    invoke-direct {v0, v9, p0, v7, v8}, Lorg/threeten/bp/format/DateTimePrintContext$1;-><init>(Lorg/threeten/bp/chrono/ChronoLocalDate;Lorg/threeten/bp/temporal/TemporalAccessor;Lorg/threeten/bp/chrono/Chronology;Lorg/threeten/bp/ZoneId;)V

    return-object v0
.end method


# virtual methods
.method endOptional()V
    .registers 3

    .line 245
    iget v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->optional:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->optional:I

    .line 246
    return-void
.end method

.method getLocale()Ljava/util/Locale;
    .registers 2

    .line 219
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method getSymbols()Lorg/threeten/bp/format/DecimalStyle;
    .registers 2

    .line 230
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->symbols:Lorg/threeten/bp/format/DecimalStyle;

    return-object v0
.end method

.method getTemporal()Lorg/threeten/bp/temporal/TemporalAccessor;
    .registers 2

    .line 207
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->temporal:Lorg/threeten/bp/temporal/TemporalAccessor;

    return-object v0
.end method

.method getValue(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;
    .registers 5
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;

    .line 274
    :try_start_0
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->temporal:Lorg/threeten/bp/temporal/TemporalAccessor;

    invoke-interface {v0, p1}, Lorg/threeten/bp/temporal/TemporalAccessor;->getLong(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_9
    .catch Lorg/threeten/bp/DateTimeException; {:try_start_0 .. :try_end_9} :catch_b

    move-result-object v0

    return-object v0

    .line 275
    :catch_b
    move-exception v2

    .line 276
    .local v2, "ex":Lorg/threeten/bp/DateTimeException;
    iget v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->optional:I

    if-lez v0, :cond_12

    .line 277
    const/4 v0, 0x0

    return-object v0

    .line 279
    :cond_12
    throw v2
.end method

.method getValue(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;
    .registers 6
    .param p1, "query"    # Lorg/threeten/bp/temporal/TemporalQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:Ljava/lang/Object;>(Lorg/threeten/bp/temporal/TemporalQuery<TR;>;)TR;"
        }
    .end annotation

    .line 256
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->temporal:Lorg/threeten/bp/temporal/TemporalAccessor;

    invoke-interface {v0, p1}, Lorg/threeten/bp/temporal/TemporalAccessor;->query(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;

    move-result-object v3

    .line 257
    .local v3, "result":Ljava/lang/Object;, "TR;"
    if-nez v3, :cond_2b

    iget v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->optional:I

    if-nez v0, :cond_2b

    .line 258
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to extract value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/threeten/bp/format/DateTimePrintContext;->temporal:Lorg/threeten/bp/temporal/TemporalAccessor;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 260
    :cond_2b
    return-object v3
.end method

.method startOptional()V
    .registers 3

    .line 238
    iget v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->optional:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->optional:I

    .line 239
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .line 291
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimePrintContext;->temporal:Lorg/threeten/bp/temporal/TemporalAccessor;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class final Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "AreaUsuarioMenuAbertoAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "ConfiguracoesViewHolder"
.end annotation


# instance fields
.field private textoDescricao:Landroid/widget/TextView;

.field private textoIcone:Lcom/itau/empresas/ui/view/TextViewIcon;

.field final synthetic this$0:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;


# direct methods
.method private constructor <init>(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;Landroid/view/View;)V
    .registers 4
    .param p1, "this$0"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .line 63
    iput-object p1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;->this$0:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;

    .line 64
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 65
    const v0, 0x7f0e0525

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/TextViewIcon;

    iput-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;->textoIcone:Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 66
    const v0, 0x7f0e0526

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;->textoDescricao:Landroid/widget/TextView;

    .line 68
    new-instance v0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder$1;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder$1;-><init>(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    return-void
.end method

.method synthetic constructor <init>(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;Landroid/view/View;Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$1;)V
    .registers 4
    .param p1, "x0"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;
    .param p2, "x1"    # Landroid/view/View;
    .param p3, "x2"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$1;

    .line 59
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;-><init>(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$100(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;)Lcom/itau/empresas/ui/view/TextViewIcon;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;

    .line 59
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;->textoIcone:Lcom/itau/empresas/ui/view/TextViewIcon;

    return-object v0
.end method

.method static synthetic access$200(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;)Landroid/widget/TextView;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;

    .line 59
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter$ConfiguracoesViewHolder;->textoDescricao:Landroid/widget/TextView;

    return-object v0
.end method

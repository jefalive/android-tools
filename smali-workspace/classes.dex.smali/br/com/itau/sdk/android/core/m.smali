.class public final enum Lbr/com/itau/sdk/android/core/m;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/sdk/android/core/m;>;"
    }
.end annotation


# static fields
.field public static final enum a:Lbr/com/itau/sdk/android/core/m;

.field public static final enum b:Lbr/com/itau/sdk/android/core/m;

.field private static final synthetic d:[Lbr/com/itau/sdk/android/core/m;

.field private static final e:[B

.field private static f:I


# instance fields
.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .line 10
    const/16 v0, 0x88

    new-array v0, v0, [B

    fill-array-data v0, :array_78

    sput-object v0, Lbr/com/itau/sdk/android/core/m;->e:[B

    const/16 v0, 0x6d

    sput v0, Lbr/com/itau/sdk/android/core/m;->f:I

    new-instance v0, Lbr/com/itau/sdk/android/core/m;

    sget-object v1, Lbr/com/itau/sdk/android/core/m;->e:[B

    const/16 v2, 0x2c

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/m;->e:[B

    const/16 v3, 0x3f

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/m;->e:[B

    const/16 v4, 0x19

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/m;->a(III)Ljava/lang/String;

    move-result-object v1

    sget v2, Lbr/com/itau/sdk/android/core/m;->f:I

    and-int/lit16 v2, v2, 0xbf

    sget-object v3, Lbr/com/itau/sdk/android/core/m;->e:[B

    const/16 v4, 0x1c

    aget-byte v3, v3, v4

    add-int/lit8 v4, v3, 0x5

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core/m;->a(III)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lbr/com/itau/sdk/android/core/m;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbr/com/itau/sdk/android/core/m;->a:Lbr/com/itau/sdk/android/core/m;

    .line 15
    new-instance v0, Lbr/com/itau/sdk/android/core/m;

    sget-object v1, Lbr/com/itau/sdk/android/core/m;->e:[B

    const/16 v2, 0x15

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/m;->e:[B

    const/16 v3, 0x3f

    aget-byte v2, v2, v3

    const/16 v3, 0x79

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/m;->a(III)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lbr/com/itau/sdk/android/core/m;->e:[B

    const/16 v3, 0x28

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/m;->e:[B

    const/16 v4, 0x1c

    aget-byte v3, v3, v4

    const/16 v4, 0x42

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core/m;->a(III)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3, v2}, Lbr/com/itau/sdk/android/core/m;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbr/com/itau/sdk/android/core/m;->b:Lbr/com/itau/sdk/android/core/m;

    .line 6
    const/4 v0, 0x2

    new-array v0, v0, [Lbr/com/itau/sdk/android/core/m;

    sget-object v1, Lbr/com/itau/sdk/android/core/m;->a:Lbr/com/itau/sdk/android/core/m;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/m;->b:Lbr/com/itau/sdk/android/core/m;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/sdk/android/core/m;->d:[Lbr/com/itau/sdk/android/core/m;

    return-void

    nop

    :array_78
    .array-data 1
        0x3t
        0x5ft
        0xet
        -0x1ct
        -0xct
        0xat
        -0x5t
        0x35t
        0x1t
        0x4t
        -0x2t
        0x1t
        -0x15t
        0x17t
        -0x46t
        0x51t
        0x4t
        0x8t
        -0x51t
        0x49t
        0xct
        0x9t
        -0x4t
        -0x7t
        -0x9t
        0x13t
        -0x41t
        0x38t
        0xft
        0x1t
        -0x3ct
        0x3et
        0xet
        -0x10t
        0x17t
        -0x44t
        0x48t
        -0xct
        0xat
        -0x3at
        0x36t
        0x10t
        -0x7t
        0x11t
        0x0t
        -0x3t
        -0x2t
        -0x33t
        0x38t
        0xft
        0x6t
        -0xat
        -0x3t
        0x19t
        -0x9t
        -0x38t
        0x28t
        -0xct
        0xat
        0xdt
        0x17t
        -0x23t
        0x2ft
        0x2t
        0x9t
        0x1t
        0x0t
        0x0t
        0x3t
        -0x4t
        0x10t
        0x13t
        -0x41t
        0x38t
        0xft
        0x1t
        -0x3ct
        0x3et
        0xet
        -0x10t
        0x17t
        -0x44t
        0x48t
        -0xct
        0xat
        -0x3at
        0x36t
        0x10t
        -0x7t
        0x11t
        0x0t
        -0x3t
        -0x2t
        -0x33t
        0x38t
        0xft
        0x6t
        -0xat
        -0x3t
        0x19t
        -0x9t
        -0x7t
        0x16t
        -0xat
        0x2t
        0x4t
        0x11t
        -0x7t
        0x1t
        0xat
        -0x3dt
        0x28t
        -0xct
        0xat
        0xdt
        0x17t
        -0x23t
        0x2ft
        0x2t
        0x9t
        0x1t
        0x0t
        0x0t
        0x3t
        -0x4t
        0x10t
        -0x9t
        0x19t
        -0xat
        -0xat
        0x2t
        0x4t
        0x11t
        -0x7t
        0x1t
        0xat
    .end array-data
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 20
    iput-object p3, p0, Lbr/com/itau/sdk/android/core/m;->c:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public static a()Ljava/lang/Class;
    .registers 11

    .line 29
    const/4 v5, 0x0

    .line 31
    invoke-static {}, Lbr/com/itau/sdk/android/core/m;->values()[Lbr/com/itau/sdk/android/core/m;

    move-result-object v6

    array-length v7, v6

    const/4 v8, 0x0

    :goto_7
    if-ge v8, v7, :cond_1c

    aget-object v9, v6, v8

    .line 33
    :try_start_b
    invoke-virtual {v9}, Lbr/com/itau/sdk/android/core/m;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_12
    .catch Ljava/lang/ClassNotFoundException; {:try_start_b .. :try_end_12} :catch_15

    move-result-object v0

    move-object v5, v0

    .line 35
    goto :goto_16

    .line 34
    :catch_15
    move-exception v10

    .line 37
    :goto_16
    if-eqz v5, :cond_19

    .line 38
    goto :goto_1c

    .line 31
    :cond_19
    add-int/lit8 v8, v8, 0x1

    goto :goto_7

    .line 41
    :cond_1c
    :goto_1c
    if-nez v5, :cond_3b

    .line 42
    new-instance v0, Ljava/lang/RuntimeException;

    sget-object v1, Lbr/com/itau/sdk/android/core/m;->e:[B

    const/16 v2, 0x19

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/m;->e:[B

    const/16 v3, 0x2c

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/m;->e:[B

    const/16 v4, 0x8

    aget-byte v3, v3, v4

    neg-int v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/m;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_3b
    return-object v5
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    add-int/lit8 p2, p2, 0x4

    new-instance v0, Ljava/lang/String;

    const/4 v4, 0x0

    add-int/lit8 p0, p0, 0x2

    sget-object v5, Lbr/com/itau/sdk/android/core/m;->e:[B

    add-int/lit8 p1, p1, 0x53

    new-array v1, p0, [B

    if-nez v5, :cond_14

    move v2, p0

    move v3, p2

    :goto_11
    add-int/2addr v2, v3

    add-int/lit8 p1, v2, -0x3

    :cond_14
    move v2, v4

    add-int/lit8 v4, v4, 0x1

    int-to-byte v3, p1

    aput-byte v3, v1, v2

    add-int/lit8 p2, p2, 0x1

    if-ne v4, p0, :cond_23

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_23
    move v2, p1

    aget-byte v3, v5, p2

    goto :goto_11
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/m;
    .registers 2

    .line 6
    const-class v0, Lbr/com/itau/sdk/android/core/m;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/m;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/sdk/android/core/m;
    .registers 1

    .line 6
    sget-object v0, Lbr/com/itau/sdk/android/core/m;->d:[Lbr/com/itau/sdk/android/core/m;

    invoke-virtual {v0}, [Lbr/com/itau/sdk/android/core/m;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/sdk/android/core/m;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 2

    .line 25
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/m;->c:Ljava/lang/String;

    return-object v0
.end method

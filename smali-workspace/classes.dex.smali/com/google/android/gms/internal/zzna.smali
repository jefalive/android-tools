.class public final Lcom/google/android/gms/internal/zzna;
.super Ljava/lang/Object;


# direct methods
.method public static zza(Ljava/io/InputStream;Ljava/io/OutputStream;Z)J
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v0, 0x400

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/gms/internal/zzna;->zza(Ljava/io/InputStream;Ljava/io/OutputStream;ZI)J

    move-result-wide v0

    return-wide v0
.end method

.method public static zza(Ljava/io/InputStream;Ljava/io/OutputStream;ZI)J
    .registers 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-array v2, p3, [B

    const-wide/16 v3, 0x0

    :goto_4
    :try_start_4
    array-length v0, v2

    const/4 v1, 0x0

    invoke-virtual {p0, v2, v1, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    move v5, v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_15

    int-to-long v0, v5

    add-long/2addr v3, v0

    const/4 v0, 0x0

    invoke-virtual {p1, v2, v0, v5}, Ljava/io/OutputStream;->write([BII)V
    :try_end_14
    .catchall {:try_start_4 .. :try_end_14} :catchall_1e

    goto :goto_4

    :cond_15
    if-eqz p2, :cond_28

    invoke-static {p0}, Lcom/google/android/gms/internal/zzna;->zzb(Ljava/io/Closeable;)V

    invoke-static {p1}, Lcom/google/android/gms/internal/zzna;->zzb(Ljava/io/Closeable;)V

    goto :goto_28

    :catchall_1e
    move-exception v6

    if-eqz p2, :cond_27

    invoke-static {p0}, Lcom/google/android/gms/internal/zzna;->zzb(Ljava/io/Closeable;)V

    invoke-static {p1}, Lcom/google/android/gms/internal/zzna;->zzb(Ljava/io/Closeable;)V

    :cond_27
    throw v6

    :cond_28
    :goto_28
    return-wide v3
.end method

.method public static zza(Ljava/io/InputStream;Z)[B
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p0, v2, p1}, Lcom/google/android/gms/internal/zzna;->zza(Ljava/io/InputStream;Ljava/io/OutputStream;Z)J

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public static zzb(Ljava/io/Closeable;)V
    .registers 2

    if-eqz p0, :cond_7

    :try_start_2
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_5} :catch_6

    goto :goto_7

    :catch_6
    move-exception v0

    :cond_7
    :goto_7
    return-void
.end method

.method public static zzk(Ljava/io/InputStream;)[B
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/gms/internal/zzna;->zza(Ljava/io/InputStream;Z)[B

    move-result-object v0

    return-object v0
.end method

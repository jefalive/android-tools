.class public Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;
.super Lcom/itau/empresas/controller/BaseController;
.source "ContatoRecargaController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/itau/empresas/controller/BaseController<Lcom/itau/empresas/api/Api;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 15
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    .line 15
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 15
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    .line 15
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 15
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$400(Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    .line 15
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 15
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$600(Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    .line 15
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 15
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->post(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public alteraApelidoContatoRecarga(Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;)V
    .registers 4
    .param p1, "idContato"    # Ljava/lang/String;
    .param p2, "contato"    # Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;

    .line 43
    new-instance v0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$3;-><init>(Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 52
    return-void
.end method

.method public buscaContatos(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "agencia"    # Ljava/lang/String;
    .param p2, "conta"    # Ljava/lang/String;
    .param p3, "dac"    # Ljava/lang/String;

    .line 18
    new-instance v0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$1;-><init>(Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 27
    return-void
.end method

.method public excluiContatoRecarga(Ljava/lang/String;)V
    .registers 3
    .param p1, "idContato"    # Ljava/lang/String;

    .line 55
    new-instance v0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$4;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$4;-><init>(Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 63
    return-void
.end method

.method public insereContatoRecarga(Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;)V
    .registers 3
    .param p1, "contato"    # Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;

    .line 30
    new-instance v0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$2;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$2;-><init>(Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 39
    return-void
.end method

.class public final Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;
.super Ljava/lang/Object;
.source "TecladoVirtualVO.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/login/model/TecladoVirtualVO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Tecla"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private codigo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo"
    .end annotation
.end field

.field private duplaDeDigitos:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dupla_de_digitos"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 52
    new-instance v0, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla$1;

    invoke-direct {v0}, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla$1;-><init>()V

    sput-object v0, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .param p1, "in"    # Landroid/os/Parcel;

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;->duplaDeDigitos:Ljava/lang/String;

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;->codigo:Ljava/lang/String;

    .line 65
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$1;)V
    .registers 3
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$1;

    .line 45
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    .line 77
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public getCodigo()Ljava/lang/String;
    .registers 2

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;->codigo:Ljava/lang/String;

    return-object v0
.end method

.method public getDuplaDeDigitos()Ljava/lang/String;
    .registers 2

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;->duplaDeDigitos:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .line 82
    iget-object v0, p0, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;->duplaDeDigitos:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;->codigo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 84
    return-void
.end method

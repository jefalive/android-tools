.class public abstract Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;
.super Landroid/app/Activity;

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/Runnable;

.field private final c:Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

.field private d:Lcom/google/zxing/client/android/ViewfinderView;

.field private e:Lcom/google/zxing/client/android/a/g;

.field private f:Lcom/google/zxing/client/android/b;

.field private g:Lcom/google/zxing/client/android/a;

.field private h:Lcom/google/zxing/client/android/c;

.field private i:Landroid/os/Handler;

.field private j:Lcom/google/zxing/k;

.field private k:Z

.field private l:Z

.field private m:Ljava/util/Collection;

.field private n:Ljava/util/Map;

.field private o:Ljava/lang/String;

.field private p:Lcom/google/zxing/client/android/i;

.field private q:Landroid/widget/Button;

.field private r:Landroid/content/Intent;

.field private s:Landroid/support/v7/app/AlertDialog;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-class v0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    invoke-direct {p0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->d()Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->b:Ljava/lang/Runnable;

    new-instance v0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    invoke-direct {v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->c:Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    return-void
.end method

.method private a(ILandroid/content/Intent;J)V
    .registers 8

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->h:Lcom/google/zxing/client/android/c;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->h:Lcom/google/zxing/client/android/c;

    invoke-static {v0, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-lez v0, :cond_16

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->h:Lcom/google/zxing/client/android/c;

    invoke-virtual {v0, v2, p3, p4}, Lcom/google/zxing/client/android/c;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1b

    :cond_16
    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->h:Lcom/google/zxing/client/android/c;

    invoke-virtual {v0, v2}, Lcom/google/zxing/client/android/c;->sendMessage(Landroid/os/Message;)Z

    :cond_1b
    :goto_1b
    return-void
.end method

.method private a(Landroid/graphics/Bitmap;FLcom/google/zxing/k;)V
    .registers 13

    invoke-virtual {p3}, Lcom/google/zxing/k;->b()[Lcom/google/zxing/m;

    move-result-object v2

    if-eqz v2, :cond_4d

    array-length v0, v2

    if-lez v0, :cond_4d

    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    sget v0, Lbr/com/itau/sdk/android/leitorboleto/R$color;->zxing_result_points:I

    invoke-static {p0, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setColor(I)V

    array-length v0, v2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2f

    const/high16 v0, 0x40800000    # 4.0f

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/4 v0, 0x0

    aget-object v0, v2, v0

    const/4 v1, 0x1

    aget-object v1, v2, v1

    invoke-static {v3, v4, v0, v1, p2}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/google/zxing/m;Lcom/google/zxing/m;F)V

    goto :goto_4d

    :cond_2f
    const/high16 v0, 0x41200000    # 10.0f

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    move-object v5, v2

    array-length v6, v5

    const/4 v7, 0x0

    :goto_37
    if-ge v7, v6, :cond_4d

    aget-object v8, v5, v7

    if-eqz v8, :cond_4a

    invoke-virtual {v8}, Lcom/google/zxing/m;->a()F

    move-result v0

    mul-float/2addr v0, p2

    invoke-virtual {v8}, Lcom/google/zxing/m;->b()F

    move-result v1

    mul-float/2addr v1, p2

    invoke-virtual {v3, v0, v1, v4}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    :cond_4a
    add-int/lit8 v7, v7, 0x1

    goto :goto_37

    :cond_4d
    :goto_4d
    return-void
.end method

.method private static a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/google/zxing/m;Lcom/google/zxing/m;F)V
    .registers 11

    if-eqz p2, :cond_1d

    if-eqz p3, :cond_1d

    move-object v0, p0

    invoke-virtual {p2}, Lcom/google/zxing/m;->a()F

    move-result v1

    mul-float/2addr v1, p4

    invoke-virtual {p2}, Lcom/google/zxing/m;->b()F

    move-result v2

    mul-float/2addr v2, p4

    invoke-virtual {p3}, Lcom/google/zxing/m;->a()F

    move-result v3

    mul-float/2addr v3, p4

    invoke-virtual {p3}, Lcom/google/zxing/m;->b()F

    move-result v4

    mul-float/2addr v4, p4

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_1d
    return-void
.end method

.method private a(Landroid/support/v7/app/AlertDialog;)V
    .registers 3

    if-nez p1, :cond_3

    return-void

    :cond_3
    const/4 v0, -0x2

    invoke-direct {p0, p1, v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->a(Landroid/support/v7/app/AlertDialog;I)V

    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->a(Landroid/support/v7/app/AlertDialog;I)V

    const/4 v0, -0x3

    invoke-direct {p0, p1, v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->a(Landroid/support/v7/app/AlertDialog;I)V

    return-void
.end method

.method private a(Landroid/support/v7/app/AlertDialog;I)V
    .registers 5

    if-nez p1, :cond_3

    return-void

    :cond_3
    invoke-virtual {p1, p2}, Landroid/support/v7/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    if-eqz v1, :cond_18

    invoke-virtual {p1}, Landroid/support/v7/app/AlertDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/b;->b(Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    :cond_18
    return-void
.end method

.method private a(Landroid/view/SurfaceHolder;)V
    .registers 9

    if-nez p1, :cond_a

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No SurfaceHolder provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->e:Lcom/google/zxing/client/android/a/g;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/a/g;->b()Z

    move-result v0

    if-eqz v0, :cond_1a

    sget-object v0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->a:Ljava/lang/String;

    const-string v1, "initCamera() while already open -- late SurfaceView callback?"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1a
    :try_start_1a
    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->e:Lcom/google/zxing/client/android/a/g;

    invoke-virtual {v0, p1}, Lcom/google/zxing/client/android/a/g;->a(Landroid/view/SurfaceHolder;)V

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->q:Landroid/widget/Button;

    if-eqz v0, :cond_34

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->q:Landroid/widget/Button;

    iget-object v1, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->e:Lcom/google/zxing/client/android/a/g;

    invoke-virtual {v1}, Lcom/google/zxing/client/android/a/g;->a()Z

    move-result v1

    if-eqz v1, :cond_2f

    const/4 v1, 0x0

    goto :goto_31

    :cond_2f
    const/16 v1, 0x8

    :goto_31
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    :cond_34
    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->e:Lcom/google/zxing/client/android/a/g;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/a/g;->a()Z

    move-result v0

    if-eqz v0, :cond_49

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->q:Landroid/widget/Button;

    if-eqz v0, :cond_49

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->q:Landroid/widget/Button;

    invoke-static {p0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/d;->a(Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_49
    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->h:Lcom/google/zxing/client/android/c;

    if-nez v0, :cond_5d

    new-instance v0, Lcom/google/zxing/client/android/c;

    iget-object v2, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->m:Ljava/util/Collection;

    iget-object v3, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->n:Ljava/util/Map;

    iget-object v4, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->o:Ljava/lang/String;

    iget-object v5, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->e:Lcom/google/zxing/client/android/a/g;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/zxing/client/android/c;-><init>(Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;Ljava/util/Collection;Ljava/util/Map;Ljava/lang/String;Lcom/google/zxing/client/android/a/g;)V

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->h:Lcom/google/zxing/client/android/c;

    :cond_5d
    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->e:Lcom/google/zxing/client/android/a/g;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/a/g;->i()Z

    move-result v0

    if-nez v0, :cond_68

    invoke-direct {p0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->b()V

    :cond_68
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->a(Lcom/google/zxing/k;)V
    :try_end_6c
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_6c} :catch_6d
    .catch Ljava/lang/RuntimeException; {:try_start_1a .. :try_end_6c} :catch_77

    goto :goto_82

    :catch_6d
    move-exception v6

    sget-object v0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->a:Ljava/lang/String;

    invoke-static {v0, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->displayFrameworkBugMessageAndExit()V

    goto :goto_82

    :catch_77
    move-exception v6

    sget-object v0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->a:Ljava/lang/String;

    const-string v1, "Unexpected error initializing camera"

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->displayFrameworkBugMessageAndExit()V

    :goto_82
    return-void
.end method

.method private a(Lcom/google/zxing/k;)V
    .registers 6

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->h:Lcom/google/zxing/client/android/c;

    if-nez v0, :cond_7

    iput-object p1, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->j:Lcom/google/zxing/k;

    goto :goto_21

    :cond_7
    if-eqz p1, :cond_b

    iput-object p1, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->j:Lcom/google/zxing/k;

    :cond_b
    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->j:Lcom/google/zxing/k;

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->h:Lcom/google/zxing/client/android/c;

    sget v1, Lbr/com/itau/sdk/android/leitorboleto/R$id;->zxing_decode_succeeded:I

    iget-object v2, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->j:Lcom/google/zxing/k;

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->h:Lcom/google/zxing/client/android/c;

    invoke-virtual {v0, v3}, Lcom/google/zxing/client/android/c;->sendMessage(Landroid/os/Message;)Z

    :cond_1e
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->j:Lcom/google/zxing/k;

    :goto_21
    return-void
.end method

.method private a(Lcom/google/zxing/k;Landroid/graphics/Bitmap;)V
    .registers 15

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->c:Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->isDrawResultBitmap()Z

    move-result v0

    if-eqz v0, :cond_f

    if-eqz p2, :cond_f

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->d:Lcom/google/zxing/client/android/ViewfinderView;

    invoke-virtual {v0, p2}, Lcom/google/zxing/client/android/ViewfinderView;->a(Landroid/graphics/Bitmap;)V

    :cond_f
    new-instance v3, Landroid/content/Intent;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_28

    const/high16 v0, 0x80000

    invoke-virtual {v3, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_2d

    :cond_28
    const/high16 v0, 0x80000

    invoke-virtual {v3, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :goto_2d
    const-string v0, "SCAN_RESULT"

    invoke-virtual {p1}, Lcom/google/zxing/k;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/google/zxing/k;->a()[B

    move-result-object v4

    if-eqz v4, :cond_44

    array-length v0, v4

    if-lez v0, :cond_44

    const-string v0, "SCAN_RESULT_BYTES"

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    :cond_44
    invoke-virtual {p1}, Lcom/google/zxing/k;->c()Ljava/util/Map;

    move-result-object v5

    if-eqz v5, :cond_a4

    sget-object v0, Lcom/google/zxing/l;->b:Lcom/google/zxing/l;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/Number;

    if-eqz v6, :cond_5e

    const-string v0, "SCAN_RESULT_ORIENTATION"

    invoke-virtual {v6}, Ljava/lang/Number;->intValue()I

    move-result v1

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_5e
    sget-object v0, Lcom/google/zxing/l;->d:Lcom/google/zxing/l;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/lang/String;

    if-eqz v7, :cond_6e

    const-string v0, "SCAN_RESULT_ERROR_CORRECTION_LEVEL"

    invoke-virtual {v3, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_6e
    sget-object v0, Lcom/google/zxing/l;->c:Lcom/google/zxing/l;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/lang/Iterable;

    if-eqz v8, :cond_a4

    const/4 v9, 0x0

    invoke-interface {v8}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_7e
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, [B

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SCAN_RESULT_BYTE_SEGMENTS_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    add-int/lit8 v9, v9, 0x1

    goto :goto_7e

    :cond_a4
    sget v0, Lbr/com/itau/sdk/android/leitorboleto/R$id;->zxing_return_scan_result:I

    const-wide/16 v1, 0x2bc

    invoke-direct {p0, v0, v3, v1, v2}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->a(ILandroid/content/Intent;J)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V
    .registers 10

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->s:Landroid/support/v7/app/AlertDialog;

    if-nez v0, :cond_2f

    invoke-direct {p0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->e()Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    invoke-static {p0, p1}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/b;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    invoke-static {p0, p2}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/b;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p5, p6}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p7}, Landroid/support/v7/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->s:Landroid/support/v7/app/AlertDialog;

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->s:Landroid/support/v7/app/AlertDialog;

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->a(Landroid/support/v7/app/AlertDialog;)V

    :cond_2f
    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->s:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_40

    iget-boolean v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->l:Z

    if-nez v0, :cond_40

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->s:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->show()V

    :cond_40
    return-void
.end method

.method private b()V
    .registers 9

    move-object v0, p0

    iget-object v1, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->c:Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->getBadCameraTitle()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->c:Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->getBadCameraMessage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "tentar novamente"

    invoke-static {}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/b;->a()Landroid/content/DialogInterface$OnClickListener;

    move-result-object v4

    const-string v5, "digitar c\u00f3digo"

    invoke-static {p0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/c;->a(Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    return-void
.end method

.method static synthetic b(Landroid/content/DialogInterface;I)V
    .registers 2

    return-void
.end method

.method private c()V
    .registers 6

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->i:Landroid/os/Handler;

    if-nez v0, :cond_c

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->i:Landroid/os/Handler;

    goto :goto_13

    :cond_c
    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->i:Landroid/os/Handler;

    iget-object v1, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :goto_13
    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->i:Landroid/os/Handler;

    iget-object v1, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->b:Ljava/lang/Runnable;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x14

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private d()Ljava/lang/Runnable;
    .registers 2

    invoke-static {p0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/e;->a(Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;)Ljava/lang/Runnable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Landroid/content/DialogInterface;I)V
    .registers 2

    return-void
.end method

.method private e()Landroid/support/v7/app/AlertDialog$Builder;
    .registers 3

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->c:Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->getDialogStyle()I

    move-result v0

    if-nez v0, :cond_e

    new-instance v1, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    goto :goto_19

    :cond_e
    new-instance v1, Landroid/support/v7/app/AlertDialog$Builder;

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->c:Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->getDialogStyle()I

    move-result v0

    invoke-direct {v1, p0, v0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    :goto_19
    return-object v1
.end method


# virtual methods
.method synthetic a()V
    .registers 9

    move-object v0, p0

    iget-object v1, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->c:Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->getHardToReadTitle()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->c:Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->getHardToReadMessage()Ljava/lang/String;

    move-result-object v2

    const-string v3, "tentar novamente"

    invoke-static {}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/f;->a()Landroid/content/DialogInterface$OnClickListener;

    move-result-object v4

    const-string v5, "digitar c\u00f3digo"

    invoke-static {p0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/g;->a(Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    return-void
.end method

.method synthetic a(Landroid/content/DialogInterface;I)V
    .registers 4

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->getManualTypingResultCode()I

    move-result v0

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->setResult(I)V

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->finish()V

    return-void
.end method

.method synthetic a(Landroid/view/View;)V
    .registers 3

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->e:Lcom/google/zxing/client/android/a/g;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/a/g;->f()V

    return-void
.end method

.method public abstract adjustReaderState(Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;)V
.end method

.method synthetic b(Landroid/view/View;)V
    .registers 3

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->setResult(I)V

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->finish()V

    return-void
.end method

.method synthetic c(Landroid/content/DialogInterface;I)V
    .registers 4

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->getManualTypingResultCode()I

    move-result v0

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->setResult(I)V

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->finish()V

    return-void
.end method

.method public final displayFrameworkBugMessageAndExit()V
    .registers 9

    move-object v0, p0

    iget-object v1, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->c:Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->getDialogTitle()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->c:Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->getDialogError()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Ok"

    new-instance v4, Lcom/google/zxing/client/android/h;

    invoke-direct {v4, p0}, Lcom/google/zxing/client/android/h;-><init>(Landroid/app/Activity;)V

    new-instance v7, Lcom/google/zxing/client/android/h;

    invoke-direct {v7, p0}, Lcom/google/zxing/client/android/h;-><init>(Landroid/app/Activity;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v7}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    return-void
.end method

.method public final drawViewfinder()V
    .registers 2

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->d:Lcom/google/zxing/client/android/ViewfinderView;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/ViewfinderView;->a()V

    return-void
.end method

.method public final getCameraManager()Lcom/google/zxing/client/android/a/g;
    .registers 2

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->e:Lcom/google/zxing/client/android/a/g;

    return-object v0
.end method

.method public final getHandler()Landroid/os/Handler;
    .registers 2

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->h:Lcom/google/zxing/client/android/c;

    return-object v0
.end method

.method public getManualTypingResultCode()I
    .registers 2

    const/4 v0, 0x0

    return v0
.end method

.method public final getViewfinderView()Lcom/google/zxing/client/android/ViewfinderView;
    .registers 2

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->d:Lcom/google/zxing/client/android/ViewfinderView;

    return-object v0
.end method

.method public final handleDecode(Lcom/google/zxing/k;Landroid/graphics/Bitmap;F)V
    .registers 6

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->p:Lcom/google/zxing/client/android/i;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/i;->a()V

    if-eqz p2, :cond_9

    const/4 v1, 0x1

    goto :goto_a

    :cond_9
    const/4 v1, 0x0

    :goto_a
    if-eqz v1, :cond_14

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->f:Lcom/google/zxing/client/android/b;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/b;->b()V

    invoke-direct {p0, p2, p3, p1}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->a(Landroid/graphics/Bitmap;FLcom/google/zxing/k;)V

    :cond_14
    invoke-direct {p0, p1, p2}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->a(Lcom/google/zxing/k;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 8

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/16 v0, 0x80

    invoke-virtual {v3, v0}, Landroid/view/Window;->addFlags(I)V

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->c:Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->adjustReaderState(Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;)V

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->c:Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->getLayoutId()I

    move-result v0

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->setContentView(I)V

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->c:Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->getFlashButtonId()I

    move-result v0

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_2b

    move-object v0, v4

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->q:Landroid/widget/Button;

    :cond_2b
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->k:Z

    new-instance v0, Lcom/google/zxing/client/android/i;

    invoke-direct {v0, p0}, Lcom/google/zxing/client/android/i;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->p:Lcom/google/zxing/client/android/i;

    new-instance v0, Lcom/google/zxing/client/android/b;

    invoke-direct {v0, p0}, Lcom/google/zxing/client/android/b;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->f:Lcom/google/zxing/client/android/b;

    new-instance v0, Lcom/google/zxing/client/android/a;

    invoke-direct {v0, p0}, Lcom/google/zxing/client/android/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->g:Lcom/google/zxing/client/android/a;

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->c:Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->getBackButtonId()I

    move-result v0

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/Button;

    if-eqz v5, :cond_59

    invoke-static {p0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/a;->a(Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_59
    new-instance v0, Lcom/google/zxing/integration/android/IntentIntegrator;

    invoke-direct {v0, p0}, Lcom/google/zxing/integration/android/IntentIntegrator;-><init>(Landroid/app/Activity;)V

    sget-object v1, Lcom/google/zxing/a;->a:Lcom/google/zxing/a;

    invoke-virtual {v1}, Lcom/google/zxing/a;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/zxing/integration/android/IntentIntegrator;->setDesiredBarcodeFormats(Ljava/util/Collection;)Lcom/google/zxing/integration/android/IntentIntegrator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/zxing/integration/android/IntentIntegrator;->autoWide()Lcom/google/zxing/integration/android/IntentIntegrator;

    move-result-object v0

    const-string v1, "TRY_HARDER"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/zxing/integration/android/IntentIntegrator;->addExtra(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/zxing/integration/android/IntentIntegrator;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/zxing/integration/android/IntentIntegrator;->createScanIntent(Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->r:Landroid/content/Intent;

    return-void
.end method

.method protected final onDestroy()V
    .registers 2

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->p:Lcom/google/zxing/client/android/i;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/i;->d()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 5

    sparse-switch p1, :sswitch_data_24

    goto :goto_1f

    :sswitch_4
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->setResult(I)V

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->finish()V

    const/4 v0, 0x1

    return v0

    :sswitch_d
    const/4 v0, 0x1

    return v0

    :sswitch_f
    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->e:Lcom/google/zxing/client/android/a/g;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/zxing/client/android/a/g;->a(Z)V

    const/4 v0, 0x1

    return v0

    :sswitch_17
    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->e:Lcom/google/zxing/client/android/a/g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/zxing/client/android/a/g;->a(Z)V

    const/4 v0, 0x1

    return v0

    :goto_1f
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    :sswitch_data_24
    .sparse-switch
        0x4 -> :sswitch_4
        0x18 -> :sswitch_17
        0x19 -> :sswitch_f
        0x1b -> :sswitch_d
        0x50 -> :sswitch_d
    .end sparse-switch
.end method

.method protected final onPause()V
    .registers 4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->l:Z

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->h:Lcom/google/zxing/client/android/c;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->h:Lcom/google/zxing/client/android/c;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/c;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->h:Lcom/google/zxing/client/android/c;

    :cond_f
    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->p:Lcom/google/zxing/client/android/i;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/i;->b()V

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->g:Lcom/google/zxing/client/android/a;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/a;->a()V

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->f:Lcom/google/zxing/client/android/b;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/b;->close()V

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->e:Lcom/google/zxing/client/android/a/g;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/a/g;->c()V

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->s:Landroid/support/v7/app/AlertDialog;

    if-eqz v0, :cond_34

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->s:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_34

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->s:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->dismiss()V

    :cond_34
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->k:Z

    if-nez v0, :cond_4a

    sget v0, Lbr/com/itau/sdk/android/leitorboleto/R$id;->zxing_preview_view:I

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/view/SurfaceView;

    if-eqz v1, :cond_4a

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-interface {v2, p0}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    :cond_4a
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected final onResume()V
    .registers 8

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->l:Z

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    new-instance v0, Lcom/google/zxing/client/android/a/g;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/zxing/client/android/a/g;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->e:Lcom/google/zxing/client/android/a/g;

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->c:Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->getViewFinderId()I

    move-result v0

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/client/android/ViewfinderView;

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->d:Lcom/google/zxing/client/android/ViewfinderView;

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->d:Lcom/google/zxing/client/android/ViewfinderView;

    iget-object v1, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->e:Lcom/google/zxing/client/android/a/g;

    invoke-virtual {v0, v1}, Lcom/google/zxing/client/android/ViewfinderView;->setCameraManager(Lcom/google/zxing/client/android/a/g;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->h:Lcom/google/zxing/client/android/c;

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->d:Lcom/google/zxing/client/android/ViewfinderView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/zxing/client/android/ViewfinderView;->setVisibility(I)V

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->c:Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->getSurfaceViewId()I

    move-result v0

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/view/SurfaceView;

    invoke-virtual {v3}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v4

    iget-boolean v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->k:Z

    if-eqz v0, :cond_48

    invoke-direct {p0, v4}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->a(Landroid/view/SurfaceHolder;)V

    goto :goto_4b

    :cond_48
    invoke-interface {v4, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    :goto_4b
    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->f:Lcom/google/zxing/client/android/b;

    iget-object v1, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->c:Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->isBeepEnabled()Z

    move-result v1

    iget-object v2, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->c:Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->isVibrateEnabled()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/zxing/client/android/b;->a(ZZ)V

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->g:Lcom/google/zxing/client/android/a;

    iget-object v1, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->e:Lcom/google/zxing/client/android/a/g;

    invoke-virtual {v0, v1}, Lcom/google/zxing/client/android/a;->a(Lcom/google/zxing/client/android/a/g;)V

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->p:Lcom/google/zxing/client/android/i;

    invoke-virtual {v0}, Lcom/google/zxing/client/android/i;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->m:Ljava/util/Collection;

    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->o:Ljava/lang/String;

    invoke-direct {p0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->c()V

    sget-object v0, Lcom/google/zxing/a;->a:Lcom/google/zxing/a;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->m:Ljava/util/Collection;

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->r:Landroid/content/Intent;

    invoke-static {v0}, Lcom/google/zxing/client/android/f;->a(Landroid/content/Intent;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->n:Ljava/util/Map;

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->r:Landroid/content/Intent;

    const-string v1, "SCAN_WIDTH"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b0

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->r:Landroid/content/Intent;

    const-string v1, "SCAN_HEIGHT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b0

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->r:Landroid/content/Intent;

    const-string v1, "SCAN_WIDTH"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->r:Landroid/content/Intent;

    const-string v1, "SCAN_HEIGHT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    if-lez v5, :cond_b0

    if-lez v6, :cond_b0

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->e:Lcom/google/zxing/client/android/a/g;

    invoke-virtual {v0, v5, v6}, Lcom/google/zxing/client/android/a/g;->a(II)V

    :cond_b0
    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->r:Landroid/content/Intent;

    const-string v1, "SCAN_CAMERA_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_ca

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->r:Landroid/content/Intent;

    const-string v1, "SCAN_CAMERA_ID"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    if-ltz v5, :cond_ca

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->e:Lcom/google/zxing/client/android/a/g;

    invoke-virtual {v0, v5}, Lcom/google/zxing/client/android/a/g;->a(I)V

    :cond_ca
    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->r:Landroid/content/Intent;

    const-string v1, "CHARACTER_SET"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->o:Ljava/lang/String;

    return-void
.end method

.method public final surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .registers 5

    return-void
.end method

.method public final surfaceCreated(Landroid/view/SurfaceHolder;)V
    .registers 4

    if-nez p1, :cond_9

    sget-object v0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->a:Ljava/lang/String;

    const-string v1, "*** WARNING *** surfaceCreated() gave us a null surface!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->k:Z

    if-nez v0, :cond_13

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->k:Z

    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->a(Landroid/view/SurfaceHolder;)V

    :cond_13
    return-void
.end method

.method public final surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .registers 3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->k:Z

    return-void
.end method

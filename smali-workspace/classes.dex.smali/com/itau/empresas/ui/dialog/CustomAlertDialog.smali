.class public Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
.super Lcom/itau/empresas/ui/dialog/BaseDialog;
.source "CustomAlertDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;,
        Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;
    }
.end annotation


# instance fields
.field private boldBotaoPositivo:Z

.field botaoNegativo:Landroid/widget/Button;

.field botaoNeutro:Landroid/widget/Button;

.field botaoPositivo:Landroid/widget/Button;

.field protected estadoArg:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

.field private gravity:I

.field private gravityCustom:Z

.field private maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

.field protected mensagem:Ljava/lang/String;

.field private negativoListener:Landroid/view/View$OnClickListener;

.field private neutroListener:Landroid/view/View$OnClickListener;

.field private onShowListener:Landroid/content/DialogInterface$OnShowListener;

.field private positivoListener:Landroid/view/View$OnClickListener;

.field rlDialogBotoesSimNao:Landroid/widget/RelativeLayout;

.field rlDialogUmBotao:Landroid/widget/RelativeLayout;

.field protected textoBotaoNegativo:Ljava/lang/String;

.field protected textoBotaoNeutro:Ljava/lang/String;

.field protected textoBotaoPositivo:Ljava/lang/String;

.field textoMensagem:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 27
    invoke-direct {p0}, Lcom/itau/empresas/ui/dialog/BaseDialog;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)Landroid/content/DialogInterface$OnShowListener;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    .line 27
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->onShowListener:Landroid/content/DialogInterface$OnShowListener;

    return-object v0
.end method

.method private inicializaMaquinaDeEstado()Lcom/itau/empresas/ui/util/MaquinaDeEstado;
    .registers 7

    .line 155
    new-instance v0, Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    invoke-direct {v0}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;-><init>()V

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->UM_BOTAO:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->rlDialogUmBotao:Landroid/widget/RelativeLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 158
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visible([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->rlDialogBotoesSimNao:Landroid/widget/RelativeLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 159
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 160
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 156
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->DOIS_BOTOES:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->rlDialogBotoesSimNao:Landroid/widget/RelativeLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 163
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visible([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->rlDialogUmBotao:Landroid/widget/RelativeLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 164
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 165
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 161
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    .line 155
    return-object v0
.end method


# virtual methods
.method public aoInicializar()V
    .registers 3

    .line 73
    invoke-direct {p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->inicializaMaquinaDeEstado()Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->estadoArg:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 76
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->mensagem:Ljava/lang/String;

    if-eqz v0, :cond_18

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->textoMensagem:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->mensagem:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    :cond_18
    iget-boolean v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->gravityCustom:Z

    if-eqz v0, :cond_23

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->textoMensagem:Landroid/widget/TextView;

    iget v1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->gravity:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 82
    :cond_23
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->textoBotaoPositivo:Ljava/lang/String;

    if-eqz v0, :cond_2e

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->botaoPositivo:Landroid/widget/Button;

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->textoBotaoPositivo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 85
    :cond_2e
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->textoBotaoNegativo:Ljava/lang/String;

    if-eqz v0, :cond_39

    .line 86
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->botaoNegativo:Landroid/widget/Button;

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->textoBotaoNegativo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 88
    :cond_39
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->textoBotaoNeutro:Ljava/lang/String;

    if-eqz v0, :cond_44

    .line 89
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->botaoNeutro:Landroid/widget/Button;

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->textoBotaoNeutro:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 91
    :cond_44
    iget-boolean v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->boldBotaoPositivo:Z

    if-eqz v0, :cond_5c

    .line 92
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->botaoNegativo:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-static {v1}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 93
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->botaoPositivo:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-static {v1}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 97
    :cond_5c
    return-void
.end method

.method protected botaoNegativo()V
    .registers 3

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->negativoListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_b

    .line 113
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->negativoListener:Landroid/view/View$OnClickListener;

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->botaoNegativo:Landroid/widget/Button;

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 115
    :cond_b
    return-void
.end method

.method protected botaoNeutro()V
    .registers 3

    .line 119
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->neutroListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_b

    .line 120
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->neutroListener:Landroid/view/View$OnClickListener;

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->botaoNeutro:Landroid/widget/Button;

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 122
    :cond_b
    return-void
.end method

.method protected botaoPositivo()V
    .registers 3

    .line 105
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->positivoListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_b

    .line 106
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->positivoListener:Landroid/view/View$OnClickListener;

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->botaoPositivo:Landroid/widget/Button;

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 108
    :cond_b
    return-void
.end method

.method public getTextoMensagem()Landroid/widget/TextView;
    .registers 2

    .line 100
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->textoMensagem:Landroid/widget/TextView;

    return-object v0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 53
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/dialog/BaseDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v3

    .line 55
    .local v3, "dialog":Landroid/app/Dialog;
    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 56
    invoke-virtual {v3}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 57
    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 59
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$1;-><init>(Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v3, v0}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 68
    return-object v3
.end method

.method public setBoldBotaoPositivo(Z)V
    .registers 2
    .param p1, "boldBotaoPositivo"    # Z

    .line 149
    iput-boolean p1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->boldBotaoPositivo:Z

    .line 150
    return-void
.end method

.method public setGravityCustom(Z)V
    .registers 2
    .param p1, "gravityCustom"    # Z

    .line 145
    iput-boolean p1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->gravityCustom:Z

    .line 146
    return-void
.end method

.method public setGravityTexto(I)V
    .registers 2
    .param p1, "gravity"    # I

    .line 141
    iput p1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->gravity:I

    .line 142
    return-void
.end method

.method public setNegativoListener(Landroid/view/View$OnClickListener;)V
    .registers 2
    .param p1, "negativoListener"    # Landroid/view/View$OnClickListener;

    .line 133
    iput-object p1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->negativoListener:Landroid/view/View$OnClickListener;

    .line 134
    return-void
.end method

.method public setNeutroListener(Landroid/view/View$OnClickListener;)V
    .registers 2
    .param p1, "neutroListener"    # Landroid/view/View$OnClickListener;

    .line 137
    iput-object p1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->neutroListener:Landroid/view/View$OnClickListener;

    .line 138
    return-void
.end method

.method public setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V
    .registers 2
    .param p1, "onShowListener"    # Landroid/content/DialogInterface$OnShowListener;

    .line 125
    iput-object p1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->onShowListener:Landroid/content/DialogInterface$OnShowListener;

    .line 126
    return-void
.end method

.method public setPositivoListener(Landroid/view/View$OnClickListener;)V
    .registers 2
    .param p1, "positivoListener"    # Landroid/view/View$OnClickListener;

    .line 129
    iput-object p1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->positivoListener:Landroid/view/View$OnClickListener;

    .line 130
    return-void
.end method

.class public final Lorg/threeten/bp/temporal/ValueRange;
.super Ljava/lang/Object;
.source "ValueRange.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x658e56a90d32a548L


# instance fields
.field private final maxLargest:J

.field private final maxSmallest:J

.field private final minLargest:J

.field private final minSmallest:J


# direct methods
.method private constructor <init>(JJJJ)V
    .registers 9
    .param p1, "minSmallest"    # J
    .param p3, "minLargest"    # J
    .param p5, "maxSmallest"    # J
    .param p7, "maxLargest"    # J

    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    iput-wide p1, p0, Lorg/threeten/bp/temporal/ValueRange;->minSmallest:J

    .line 154
    iput-wide p3, p0, Lorg/threeten/bp/temporal/ValueRange;->minLargest:J

    .line 155
    iput-wide p5, p0, Lorg/threeten/bp/temporal/ValueRange;->maxSmallest:J

    .line 156
    iput-wide p7, p0, Lorg/threeten/bp/temporal/ValueRange;->maxLargest:J

    .line 157
    return-void
.end method

.method public static of(JJ)Lorg/threeten/bp/temporal/ValueRange;
    .registers 13
    .param p0, "min"    # J
    .param p2, "max"    # J

    .line 92
    cmp-long v0, p0, p2

    if-lez v0, :cond_c

    .line 93
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Minimum value must be less than maximum value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_c
    new-instance v0, Lorg/threeten/bp/temporal/ValueRange;

    move-wide v1, p0

    move-wide v3, p0

    move-wide v5, p2

    move-wide v7, p2

    invoke-direct/range {v0 .. v8}, Lorg/threeten/bp/temporal/ValueRange;-><init>(JJJJ)V

    return-object v0
.end method

.method public static of(JJJ)Lorg/threeten/bp/temporal/ValueRange;
    .registers 14
    .param p0, "min"    # J
    .param p2, "maxSmallest"    # J
    .param p4, "maxLargest"    # J

    .line 113
    move-wide v0, p0

    move-wide v2, p0

    move-wide v4, p2

    move-wide v6, p4

    invoke-static/range {v0 .. v7}, Lorg/threeten/bp/temporal/ValueRange;->of(JJJJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    return-object v0
.end method

.method public static of(JJJJ)Lorg/threeten/bp/temporal/ValueRange;
    .registers 17
    .param p0, "minSmallest"    # J
    .param p2, "minLargest"    # J
    .param p4, "maxSmallest"    # J
    .param p6, "maxLargest"    # J

    .line 132
    cmp-long v0, p0, p2

    if-lez v0, :cond_c

    .line 133
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Smallest minimum value must be less than largest minimum value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :cond_c
    cmp-long v0, p4, p6

    if-lez v0, :cond_18

    .line 136
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Smallest maximum value must be less than largest maximum value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 138
    :cond_18
    cmp-long v0, p2, p6

    if-lez v0, :cond_24

    .line 139
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Minimum value must be less than maximum value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 141
    :cond_24
    new-instance v0, Lorg/threeten/bp/temporal/ValueRange;

    move-wide v1, p0

    move-wide v3, p2

    move-wide v5, p4

    move-wide v7, p6

    invoke-direct/range {v0 .. v8}, Lorg/threeten/bp/temporal/ValueRange;-><init>(JJJJ)V

    return-object v0
.end method


# virtual methods
.method public checkValidIntValue(JLorg/threeten/bp/temporal/TemporalField;)I
    .registers 7
    .param p1, "value"    # J
    .param p3, "field"    # Lorg/threeten/bp/temporal/TemporalField;

    .line 299
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/temporal/ValueRange;->isValidIntValue(J)Z

    move-result v0

    if-nez v0, :cond_29

    .line 300
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid int value for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 302
    :cond_29
    long-to-int v0, p1

    return v0
.end method

.method public checkValidValue(JLorg/threeten/bp/temporal/TemporalField;)J
    .registers 7
    .param p1, "value"    # J
    .param p3, "field"    # Lorg/threeten/bp/temporal/TemporalField;

    .line 276
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/temporal/ValueRange;->isValidValue(J)Z

    move-result v0

    if-nez v0, :cond_58

    .line 277
    if-eqz p3, :cond_35

    .line 278
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid value for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (valid values "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 280
    :cond_35
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid value (valid values "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283
    :cond_58
    return-wide p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .param p1, "obj"    # Ljava/lang/Object;

    .line 318
    if-ne p1, p0, :cond_4

    .line 319
    const/4 v0, 0x1

    return v0

    .line 321
    :cond_4
    instance-of v0, p1, Lorg/threeten/bp/temporal/ValueRange;

    if-eqz v0, :cond_2f

    .line 322
    move-object v4, p1

    check-cast v4, Lorg/threeten/bp/temporal/ValueRange;

    .line 323
    .local v4, "other":Lorg/threeten/bp/temporal/ValueRange;
    iget-wide v0, p0, Lorg/threeten/bp/temporal/ValueRange;->minSmallest:J

    iget-wide v2, v4, Lorg/threeten/bp/temporal/ValueRange;->minSmallest:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_2d

    iget-wide v0, p0, Lorg/threeten/bp/temporal/ValueRange;->minLargest:J

    iget-wide v2, v4, Lorg/threeten/bp/temporal/ValueRange;->minLargest:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_2d

    iget-wide v0, p0, Lorg/threeten/bp/temporal/ValueRange;->maxSmallest:J

    iget-wide v2, v4, Lorg/threeten/bp/temporal/ValueRange;->maxSmallest:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_2d

    iget-wide v0, p0, Lorg/threeten/bp/temporal/ValueRange;->maxLargest:J

    iget-wide v2, v4, Lorg/threeten/bp/temporal/ValueRange;->maxLargest:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_2d

    const/4 v0, 0x1

    goto :goto_2e

    :cond_2d
    const/4 v0, 0x0

    :goto_2e
    return v0

    .line 326
    .end local v4    # "other":Lorg/threeten/bp/temporal/ValueRange;
    :cond_2f
    const/4 v0, 0x0

    return v0
.end method

.method public getMaximum()J
    .registers 3

    .line 219
    iget-wide v0, p0, Lorg/threeten/bp/temporal/ValueRange;->maxLargest:J

    return-wide v0
.end method

.method public getMinimum()J
    .registers 3

    .line 183
    iget-wide v0, p0, Lorg/threeten/bp/temporal/ValueRange;->minSmallest:J

    return-wide v0
.end method

.method public hashCode()I
    .registers 9

    .line 336
    iget-wide v0, p0, Lorg/threeten/bp/temporal/ValueRange;->minSmallest:J

    iget-wide v2, p0, Lorg/threeten/bp/temporal/ValueRange;->minLargest:J

    add-long/2addr v0, v2

    iget-wide v2, p0, Lorg/threeten/bp/temporal/ValueRange;->minLargest:J

    const-wide/16 v4, 0x10

    add-long/2addr v2, v4

    long-to-int v2, v2

    shl-long/2addr v0, v2

    iget-wide v2, p0, Lorg/threeten/bp/temporal/ValueRange;->maxSmallest:J

    const-wide/16 v4, 0x30

    add-long/2addr v2, v4

    long-to-int v2, v2

    shr-long/2addr v0, v2

    iget-wide v2, p0, Lorg/threeten/bp/temporal/ValueRange;->maxSmallest:J

    const-wide/16 v4, 0x20

    add-long/2addr v2, v4

    long-to-int v2, v2

    shl-long/2addr v0, v2

    iget-wide v2, p0, Lorg/threeten/bp/temporal/ValueRange;->maxLargest:J

    const-wide/16 v4, 0x20

    add-long/2addr v2, v4

    long-to-int v2, v2

    shr-long/2addr v0, v2

    iget-wide v2, p0, Lorg/threeten/bp/temporal/ValueRange;->maxLargest:J

    const-wide/16 v4, 0x30

    add-long/2addr v2, v4

    long-to-int v2, v2

    shl-long/2addr v0, v2

    const/16 v2, 0x10

    shr-long v6, v0, v2

    .line 338
    .local v6, "hash":J
    const/16 v0, 0x20

    ushr-long v0, v6, v0

    xor-long/2addr v0, v6

    long-to-int v0, v0

    return v0
.end method

.method public isFixed()Z
    .registers 5

    .line 170
    iget-wide v0, p0, Lorg/threeten/bp/temporal/ValueRange;->minSmallest:J

    iget-wide v2, p0, Lorg/threeten/bp/temporal/ValueRange;->minLargest:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_12

    iget-wide v0, p0, Lorg/threeten/bp/temporal/ValueRange;->maxSmallest:J

    iget-wide v2, p0, Lorg/threeten/bp/temporal/ValueRange;->maxLargest:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_12

    const/4 v0, 0x1

    goto :goto_13

    :cond_12
    const/4 v0, 0x0

    :goto_13
    return v0
.end method

.method public isIntValue()Z
    .registers 5

    .line 236
    invoke-virtual {p0}, Lorg/threeten/bp/temporal/ValueRange;->getMinimum()J

    move-result-wide v0

    const-wide/32 v2, -0x80000000

    cmp-long v0, v0, v2

    if-ltz v0, :cond_18

    invoke-virtual {p0}, Lorg/threeten/bp/temporal/ValueRange;->getMaximum()J

    move-result-wide v0

    const-wide/32 v2, 0x7fffffff

    cmp-long v0, v0, v2

    if-gtz v0, :cond_18

    const/4 v0, 0x1

    goto :goto_19

    :cond_18
    const/4 v0, 0x0

    :goto_19
    return v0
.end method

.method public isValidIntValue(J)Z
    .registers 4
    .param p1, "value"    # J

    .line 261
    invoke-virtual {p0}, Lorg/threeten/bp/temporal/ValueRange;->isIntValue()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/temporal/ValueRange;->isValidValue(J)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

.method public isValidValue(J)Z
    .registers 5
    .param p1, "value"    # J

    .line 248
    invoke-virtual {p0}, Lorg/threeten/bp/temporal/ValueRange;->getMinimum()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_12

    invoke-virtual {p0}, Lorg/threeten/bp/temporal/ValueRange;->getMaximum()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_12

    const/4 v0, 0x1

    goto :goto_13

    :cond_12
    const/4 v0, 0x0

    :goto_13
    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    .line 353
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 354
    .local v4, "buf":Ljava/lang/StringBuilder;
    iget-wide v0, p0, Lorg/threeten/bp/temporal/ValueRange;->minSmallest:J

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 355
    iget-wide v0, p0, Lorg/threeten/bp/temporal/ValueRange;->minSmallest:J

    iget-wide v2, p0, Lorg/threeten/bp/temporal/ValueRange;->minLargest:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1d

    .line 356
    const/16 v0, 0x2f

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lorg/threeten/bp/temporal/ValueRange;->minLargest:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 358
    :cond_1d
    const-string v0, " - "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lorg/threeten/bp/temporal/ValueRange;->maxSmallest:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 359
    iget-wide v0, p0, Lorg/threeten/bp/temporal/ValueRange;->maxSmallest:J

    iget-wide v2, p0, Lorg/threeten/bp/temporal/ValueRange;->maxLargest:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3b

    .line 360
    const/16 v0, 0x2f

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lorg/threeten/bp/temporal/ValueRange;->maxLargest:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 362
    :cond_3b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

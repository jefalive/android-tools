.class public Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;
.super Ljava/lang/Object;
.source "DialogListaOperadoresUtil.java"


# instance fields
.field private adapter:Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;

.field private context:Landroid/content/Context;

.field private dialog:Landroid/app/Dialog;

.field private final listener:Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ListenerItemSelecionado;

.field private final operadores:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/OperadorVO;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ListenerItemSelecionado;)V
    .registers 3
    .param p1, "operadores"    # Ljava/util/List;
    .param p2, "listener"    # Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ListenerItemSelecionado;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/OperadorVO;>;Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ListenerItemSelecionado;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;->operadores:Ljava/util/List;

    .line 26
    iput-object p2, p0, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;->listener:Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ListenerItemSelecionado;

    .line 27
    return-void
.end method

.method private constroiAdapter()V
    .registers 6

    .line 36
    new-instance v0, Landroid/app/Dialog;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;->dialog:Landroid/app/Dialog;

    .line 37
    iget-object v0, p0, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;->context:Landroid/content/Context;

    const v1, 0x7f030088

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 38
    .local v3, "view":Landroid/view/View;
    iget-object v0, p0, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;->dialog:Landroid/app/Dialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 39
    iget-object v0, p0, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;->dialog:Landroid/app/Dialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 40
    iget-object v0, p0, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 42
    const v0, 0x7f0e03a2

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/support/v7/widget/RecyclerView;

    .line 43
    .local v4, "recyclerView":Landroid/support/v7/widget/RecyclerView;
    new-instance v0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;->operadores:Ljava/util/List;

    iget-object v2, p0, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;->listener:Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ListenerItemSelecionado;

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;-><init>(Ljava/util/List;Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ListenerItemSelecionado;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;->adapter:Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;

    .line 46
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;->adapter:Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;

    invoke-virtual {v4, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;->adapter:Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->notifyDataSetChanged()V

    .line 49
    return-void
.end method

.method private showDialog()V
    .registers 2

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;->adapter:Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;

    if-eqz v0, :cond_9

    .line 53
    iget-object v0, p0, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 55
    :cond_9
    return-void
.end method


# virtual methods
.method public abreDialog(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 30
    iput-object p1, p0, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;->context:Landroid/content/Context;

    .line 31
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;->constroiAdapter()V

    .line 32
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;->showDialog()V

    .line 33
    return-void
.end method

.method public fecharDialog()V
    .registers 2

    .line 58
    iget-object v0, p0, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->hide()V

    .line 59
    return-void
.end method

.class public final Lcom/itau/empresas/feature/chaveacesso/FingerPrintManagerFactory;
.super Ljava/lang/Object;
.source "FingerPrintManagerFactory.java"


# static fields
.field static manager:Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;


# direct methods
.method public static criaFingerPrintManager(Landroid/content/Context;)Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 18
    sget-object v0, Lcom/itau/empresas/feature/chaveacesso/FingerPrintManagerFactory;->manager:Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;

    if-nez v0, :cond_9

    invoke-static {p0}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprint;->getInstance(Landroid/content/Context;)Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;

    move-result-object v0

    goto :goto_b

    :cond_9
    sget-object v0, Lcom/itau/empresas/feature/chaveacesso/FingerPrintManagerFactory;->manager:Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;

    :goto_b
    return-object v0
.end method

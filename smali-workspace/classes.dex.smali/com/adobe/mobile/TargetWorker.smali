.class final Lcom/adobe/mobile/TargetWorker;
.super Ljava/lang/Object;
.source "TargetWorker.java"


# static fields
.field private static final _checkOldCookiesMutex:Ljava/lang/Object;

.field private static _edgeHost:Ljava/lang/String;

.field private static final _edgeHostMutex:Ljava/lang/Object;

.field private static _oldCookiesHaveBeenChecked:Z

.field private static _persistentParameters:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation
.end field

.field private static final _persistentParametersMutex:Ljava/lang/Object;

.field private static _sessionId:Ljava/lang/String;

.field private static final _sessionIdMutex:Ljava/lang/Object;

.field private static _thirdPartyId:Ljava/lang/String;

.field private static final _thirdPartyIdMutex:Ljava/lang/Object;

.field private static _tntId:Ljava/lang/String;

.field private static final _tntIdMutex:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 102
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/TargetWorker;->_sessionId:Ljava/lang/String;

    .line 103
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/TargetWorker;->_tntId:Ljava/lang/String;

    .line 104
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/TargetWorker;->_thirdPartyId:Ljava/lang/String;

    .line 105
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/TargetWorker;->_edgeHost:Ljava/lang/String;

    .line 106
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/TargetWorker;->_persistentParameters:Ljava/util/HashMap;

    .line 111
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/TargetWorker;->_sessionIdMutex:Ljava/lang/Object;

    .line 112
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/TargetWorker;->_tntIdMutex:Ljava/lang/Object;

    .line 113
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/TargetWorker;->_thirdPartyIdMutex:Ljava/lang/Object;

    .line 114
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/TargetWorker;->_edgeHostMutex:Ljava/lang/Object;

    .line 115
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/TargetWorker;->_persistentParametersMutex:Ljava/lang/Object;

    .line 599
    const/4 v0, 0x0

    sput-boolean v0, Lcom/adobe/mobile/TargetWorker;->_oldCookiesHaveBeenChecked:Z

    .line 600
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/TargetWorker;->_checkOldCookiesMutex:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static checkForOldCookieValue(Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .param p0, "cookieName"    # Ljava/lang/String;

    .line 624
    if-eqz p0, :cond_8

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_a

    .line 625
    :cond_8
    const/4 v0, 0x0

    return-object v0

    .line 628
    :cond_a
    const/4 v3, 0x0

    .line 631
    .local v3, "returnCookieValue":Ljava/lang/String;
    :try_start_b
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    .line 632
    .local v4, "preferences":Landroid/content/SharedPreferences;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_Expires"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9c

    .line 634
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_Expires"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-interface {v4, v0, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    .line 635
    .local v5, "cookieExpiryTime":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    cmp-long v0, v5, v0

    if-lez v0, :cond_69

    .line 636
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_Value"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v4, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 637
    .local v7, "prefsCookieValue":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_69

    .line 638
    move-object v3, v7

    .line 643
    .end local v7    # "prefsCookieValue":Ljava/lang/String;
    :cond_69
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    .line 644
    .local v7, "editor":Landroid/content/SharedPreferences$Editor;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_Value"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 645
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_Expires"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 646
    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_9c
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_b .. :try_end_9c} :catch_9d

    .line 651
    .end local v4    # "preferences":Landroid/content/SharedPreferences;
    .end local v5    # "cookieExpiryTime":J
    .end local v7    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_9c
    goto :goto_a6

    .line 649
    :catch_9d
    move-exception v4

    .line 650
    .local v4, "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Target - Error retrieving shared preferences - application context is null"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 653
    .end local v4    # "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_a6
    return-object v3
.end method

.method protected static setEdgeHost(Ljava/lang/String;)V
    .registers 6
    .param p0, "newEdgeHost"    # Ljava/lang/String;

    .line 1014
    sget-object v2, Lcom/adobe/mobile/TargetWorker;->_edgeHostMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 1015
    :try_start_3
    sput-object p0, Lcom/adobe/mobile/TargetWorker;->_edgeHost:Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_31

    .line 1018
    :try_start_5
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 1019
    .local v3, "editor":Landroid/content/SharedPreferences$Editor;
    sget-object v0, Lcom/adobe/mobile/TargetWorker;->_edgeHost:Ljava/lang/String;

    if-eqz v0, :cond_15

    sget-object v0, Lcom/adobe/mobile/TargetWorker;->_edgeHost:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 1020
    :cond_15
    const-string v0, "ADBMOBILE_TARGET_EDGE_HOST"

    invoke-interface {v3, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_22

    .line 1022
    :cond_1b
    const-string v0, "ADBMOBILE_TARGET_EDGE_HOST"

    sget-object v1, Lcom/adobe/mobile/TargetWorker;->_edgeHost:Ljava/lang/String;

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1024
    :goto_22
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_25
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_5 .. :try_end_25} :catch_26
    .catchall {:try_start_5 .. :try_end_25} :catchall_31

    .line 1028
    .end local v3    # "editor":Landroid/content/SharedPreferences$Editor;
    goto :goto_2f

    .line 1026
    :catch_26
    move-exception v3

    .line 1027
    .local v3, "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Target - Error retrieving shared preferences - application context is null"

    const/4 v1, 0x0

    :try_start_2a
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2f
    .catchall {:try_start_2a .. :try_end_2f} :catchall_31

    .line 1029
    .end local v3    # "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_2f
    monitor-exit v2

    goto :goto_34

    :catchall_31
    move-exception v4

    monitor-exit v2

    throw v4

    .line 1030
    :goto_34
    return-void
.end method

.method protected static setSessionId(Ljava/lang/String;)V
    .registers 2
    .param p0, "newSessionId"    # Ljava/lang/String;

    .line 937
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/adobe/mobile/TargetWorker;->setSessionId(Ljava/lang/String;Z)V

    .line 938
    return-void
.end method

.method private static setSessionId(Ljava/lang/String;Z)V
    .registers 4
    .param p0, "newSessionId"    # Ljava/lang/String;
    .param p1, "useMutex"    # Z

    .line 942
    if-eqz p1, :cond_e

    .line 943
    sget-object v0, Lcom/adobe/mobile/TargetWorker;->_sessionIdMutex:Ljava/lang/Object;

    monitor-enter v0

    .line 944
    :try_start_5
    invoke-static {p0}, Lcom/adobe/mobile/TargetWorker;->setSessionIdInternal(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_5 .. :try_end_8} :catchall_a

    .line 945
    monitor-exit v0

    goto :goto_d

    :catchall_a
    move-exception v1

    monitor-exit v0

    throw v1

    :goto_d
    goto :goto_11

    .line 948
    :cond_e
    invoke-static {p0}, Lcom/adobe/mobile/TargetWorker;->setSessionIdInternal(Ljava/lang/String;)V

    .line 950
    :goto_11
    return-void
.end method

.method private static setSessionIdInternal(Ljava/lang/String;)V
    .registers 7
    .param p0, "newSessionId"    # Ljava/lang/String;

    .line 953
    sput-object p0, Lcom/adobe/mobile/TargetWorker;->_sessionId:Ljava/lang/String;

    .line 956
    :try_start_2
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v3

    .line 957
    .local v3, "preferences":Landroid/content/SharedPreferences;
    const-string v0, "ADBMOBILE_TARGET_SESSION_ID"

    const/4 v1, 0x0

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 961
    .local v4, "existingSessionId":Ljava/lang/String;
    if-eqz v4, :cond_17

    sget-object v0, Lcom/adobe/mobile/TargetWorker;->_sessionId:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_45

    .line 963
    :cond_17
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 964
    .local v5, "editor":Landroid/content/SharedPreferences$Editor;
    sget-object v0, Lcom/adobe/mobile/TargetWorker;->_sessionId:Ljava/lang/String;

    if-eqz v0, :cond_38

    sget-object v0, Lcom/adobe/mobile/TargetWorker;->_sessionId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_38

    .line 965
    const-string v0, "ADBMOBILE_TARGET_SESSION_ID"

    sget-object v1, Lcom/adobe/mobile/TargetWorker;->_sessionId:Ljava/lang/String;

    invoke-interface {v5, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 966
    const-string v0, "ADBMOBILE_TARGET_LAST_TIMESTAMP"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getTimeSince1970()J

    move-result-wide v1

    invoke-interface {v5, v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_42

    .line 969
    :cond_38
    const-string v0, "ADBMOBILE_TARGET_SESSION_ID"

    invoke-interface {v5, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 970
    const-string v0, "ADBMOBILE_TARGET_LAST_TIMESTAMP"

    invoke-interface {v5, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 973
    :goto_42
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_45
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_2 .. :try_end_45} :catch_46

    .line 978
    .end local v3    # "preferences":Landroid/content/SharedPreferences;
    .end local v4    # "existingSessionId":Ljava/lang/String;
    .end local v5    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_45
    goto :goto_4f

    .line 976
    :catch_46
    move-exception v3

    .line 977
    .local v3, "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Target - Error retrieving shared preferences - application context is null"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 979
    .end local v3    # "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_4f
    return-void
.end method

.method protected static setThirdPartyId(Ljava/lang/String;)V
    .registers 6
    .param p0, "newThirdPartyId"    # Ljava/lang/String;

    .line 205
    sget-object v2, Lcom/adobe/mobile/TargetWorker;->_thirdPartyIdMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 206
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/TargetWorker;->_thirdPartyId:Ljava/lang/String;

    if-eqz v0, :cond_13

    if-eqz p0, :cond_13

    sget-object v0, Lcom/adobe/mobile/TargetWorker;->_thirdPartyId:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_49

    move-result v0

    if-eqz v0, :cond_13

    .line 207
    monitor-exit v2

    return-void

    .line 212
    :cond_13
    :try_start_13
    sget-object v0, Lcom/adobe/mobile/TargetWorker;->_thirdPartyId:Ljava/lang/String;

    if-eqz v0, :cond_1b

    .line 213
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/adobe/mobile/TargetWorker;->setSessionId(Ljava/lang/String;)V

    .line 216
    :cond_1b
    sput-object p0, Lcom/adobe/mobile/TargetWorker;->_thirdPartyId:Ljava/lang/String;
    :try_end_1d
    .catchall {:try_start_13 .. :try_end_1d} :catchall_49

    .line 219
    :try_start_1d
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 220
    .local v3, "editor":Landroid/content/SharedPreferences$Editor;
    sget-object v0, Lcom/adobe/mobile/TargetWorker;->_thirdPartyId:Ljava/lang/String;

    if-eqz v0, :cond_2d

    sget-object v0, Lcom/adobe/mobile/TargetWorker;->_thirdPartyId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_33

    .line 221
    :cond_2d
    const-string v0, "ADBMOBILE_TARGET_3RD_PARTY_ID"

    invoke-interface {v3, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_3a

    .line 224
    :cond_33
    const-string v0, "ADBMOBILE_TARGET_3RD_PARTY_ID"

    sget-object v1, Lcom/adobe/mobile/TargetWorker;->_thirdPartyId:Ljava/lang/String;

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 226
    :goto_3a
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_3d
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1d .. :try_end_3d} :catch_3e
    .catchall {:try_start_1d .. :try_end_3d} :catchall_49

    .line 230
    .end local v3    # "editor":Landroid/content/SharedPreferences$Editor;
    goto :goto_47

    .line 228
    :catch_3e
    move-exception v3

    .line 229
    .local v3, "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Target - Error retrieving shared preferences - application context is null"

    const/4 v1, 0x0

    :try_start_42
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_47
    .catchall {:try_start_42 .. :try_end_47} :catchall_49

    .line 231
    .end local v3    # "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_47
    monitor-exit v2

    goto :goto_4c

    :catchall_49
    move-exception v4

    monitor-exit v2

    throw v4

    .line 232
    :goto_4c
    return-void
.end method

.method protected static setTntId(Ljava/lang/String;)V
    .registers 6
    .param p0, "newTntId"    # Ljava/lang/String;

    .line 278
    sget-object v2, Lcom/adobe/mobile/TargetWorker;->_tntIdMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 280
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/TargetWorker;->_tntId:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/adobe/mobile/TargetWorker;->tntIdValuesAreEqual(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_4b

    move-result v0

    if-eqz v0, :cond_d

    .line 281
    monitor-exit v2

    return-void

    .line 286
    :cond_d
    :try_start_d
    sget-object v0, Lcom/adobe/mobile/TargetWorker;->_tntId:Ljava/lang/String;

    if-eqz v0, :cond_1d

    sget-object v0, Lcom/adobe/mobile/TargetWorker;->_tntId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1d

    .line 287
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/adobe/mobile/TargetWorker;->setSessionId(Ljava/lang/String;)V

    .line 291
    :cond_1d
    sput-object p0, Lcom/adobe/mobile/TargetWorker;->_tntId:Ljava/lang/String;
    :try_end_1f
    .catchall {:try_start_d .. :try_end_1f} :catchall_4b

    .line 294
    :try_start_1f
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 295
    .local v3, "editor":Landroid/content/SharedPreferences$Editor;
    sget-object v0, Lcom/adobe/mobile/TargetWorker;->_tntId:Ljava/lang/String;

    if-eqz v0, :cond_2f

    sget-object v0, Lcom/adobe/mobile/TargetWorker;->_tntId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_35

    .line 296
    :cond_2f
    const-string v0, "ADBMOBILE_TARGET_TNT_ID"

    invoke-interface {v3, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_3c

    .line 299
    :cond_35
    const-string v0, "ADBMOBILE_TARGET_TNT_ID"

    sget-object v1, Lcom/adobe/mobile/TargetWorker;->_tntId:Ljava/lang/String;

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 301
    :goto_3c
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_3f
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1f .. :try_end_3f} :catch_40
    .catchall {:try_start_1f .. :try_end_3f} :catchall_4b

    .line 305
    .end local v3    # "editor":Landroid/content/SharedPreferences$Editor;
    goto :goto_49

    .line 303
    :catch_40
    move-exception v3

    .line 304
    .local v3, "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Target - Error retrieving shared preferences - application context is null"

    const/4 v1, 0x0

    :try_start_44
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_49
    .catchall {:try_start_44 .. :try_end_49} :catchall_4b

    .line 306
    .end local v3    # "ex":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_49
    monitor-exit v2

    goto :goto_4e

    :catchall_4b
    move-exception v4

    monitor-exit v2

    throw v4

    .line 307
    :goto_4e
    return-void
.end method

.method protected static setTntIdAndSessionFromOldCookieValues()V
    .registers 5

    .line 602
    sget-object v1, Lcom/adobe/mobile/TargetWorker;->_checkOldCookiesMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 603
    :try_start_3
    sget-boolean v0, Lcom/adobe/mobile/TargetWorker;->_oldCookiesHaveBeenChecked:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_24

    if-eqz v0, :cond_9

    .line 604
    monitor-exit v1

    return-void

    .line 608
    :cond_9
    const-string v0, "mboxPC"

    :try_start_b
    invoke-static {v0}, Lcom/adobe/mobile/TargetWorker;->checkForOldCookieValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 609
    .local v2, "oldPcid":Ljava/lang/String;
    if-eqz v2, :cond_14

    .line 610
    invoke-static {v2}, Lcom/adobe/mobile/TargetWorker;->setTntId(Ljava/lang/String;)V

    .line 613
    :cond_14
    const-string v0, "mboxSession"

    invoke-static {v0}, Lcom/adobe/mobile/TargetWorker;->checkForOldCookieValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 614
    .local v3, "oldSession":Ljava/lang/String;
    if-eqz v3, :cond_1f

    .line 615
    invoke-static {v3}, Lcom/adobe/mobile/TargetWorker;->setSessionId(Ljava/lang/String;)V

    .line 618
    :cond_1f
    const/4 v0, 0x1

    sput-boolean v0, Lcom/adobe/mobile/TargetWorker;->_oldCookiesHaveBeenChecked:Z
    :try_end_22
    .catchall {:try_start_b .. :try_end_22} :catchall_24

    .line 619
    .end local v2    # "oldPcid":Ljava/lang/String;
    .end local v3    # "oldSession":Ljava/lang/String;
    monitor-exit v1

    goto :goto_27

    :catchall_24
    move-exception v4

    monitor-exit v1

    throw v4

    .line 620
    :goto_27
    return-void
.end method

.method private static tntIdValuesAreEqual(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 7
    .param p0, "oldId"    # Ljava/lang/String;
    .param p1, "newId"    # Ljava/lang/String;

    .line 258
    if-nez p0, :cond_6

    if-nez p1, :cond_6

    .line 259
    const/4 v0, 0x1

    return v0

    .line 261
    :cond_6
    if-eqz p0, :cond_a

    if-nez p1, :cond_c

    .line 262
    :cond_a
    const/4 v0, 0x0

    return v0

    .line 264
    :cond_c
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 265
    const/4 v0, 0x1

    return v0

    .line 268
    :cond_14
    const/16 v0, 0x2e

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 269
    .local v1, "dotIndexOfOldId":I
    const/4 v0, -0x1

    if-ne v1, v0, :cond_1f

    move-object v2, p0

    goto :goto_24

    :cond_1f
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 271
    .local v2, "oldIdSubstring":Ljava/lang/String;
    :goto_24
    const/16 v0, 0x2e

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 272
    .local v3, "dotIndexOfNewId":I
    const/4 v0, -0x1

    if-ne v3, v0, :cond_2f

    move-object v4, p1

    goto :goto_34

    :cond_2f
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 274
    .local v4, "newIdSubstring":Ljava/lang/String;
    :goto_34
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

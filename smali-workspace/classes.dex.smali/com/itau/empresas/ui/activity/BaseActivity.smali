.class public abstract Lcom/itau/empresas/ui/activity/BaseActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "BaseActivity.java"

# interfaces
.implements Lcom/itau/empresas/api/ApiConsumidor;


# instance fields
.field protected application:Lcom/itau/empresas/CustomApplication;

.field private loadingFragment:Lcom/itau/empresas/ui/fragment/LoadingFragment;

.field protected prefs:Lcom/itau/empresas/Preferences_;

.field private tag:Ljava/lang/String;

.field private final views:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Landroid/view/View;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 51
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/BaseActivity;->views:Ljava/util/List;

    .line 65
    const-string v0, "loading"

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/BaseActivity;->tag:Ljava/lang/String;

    return-void
.end method

.method private checkForCrashes()V
    .registers 4

    .line 309
    .line 310
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "2619ff2e8584cbcdf3ec37d6776c1f63"

    new-instance v2, Lcom/itau/empresas/ui/activity/BaseActivity$2;

    invoke-direct {v2, p0}, Lcom/itau/empresas/ui/activity/BaseActivity$2;-><init>(Lcom/itau/empresas/ui/activity/BaseActivity;)V

    invoke-static {v0, v1, v2}, Lnet/hockeyapp/android/CrashManager;->register(Landroid/content/Context;Ljava/lang/String;Lnet/hockeyapp/android/CrashManagerListener;)V

    .line 317
    return-void
.end method

.method private desregistraEventBus()V
    .registers 2

    .line 328
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->isRegistered(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 329
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->unregister(Ljava/lang/Object;)V

    .line 331
    :cond_11
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/BaseActivity;->views:Ljava/util/List;

    invoke-static {v0}, Lcom/itau/empresas/ui/activity/BaseActivityHelper;->desregistraViews(Ljava/util/List;)V

    .line 332
    return-void
.end method

.method private logaEscondeProgresEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 375
    const-string v0, "ItauEmpresas"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Erro na Chamanda: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 376
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 377
    return-void
.end method

.method private registraEventBus()V
    .registers 2

    .line 320
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->isRegistered(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 321
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->register(Ljava/lang/Object;)V

    .line 323
    :cond_11
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/BaseActivity;->views:Ljava/util/List;

    invoke-static {v0}, Lcom/itau/empresas/ui/activity/BaseActivityHelper;->registraViews(Ljava/util/List;)V

    .line 325
    return-void
.end method

.method private showDefaultErrorDialog()V
    .registers 4

    .line 291
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 292
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 293
    const v1, 0x7f0701ef

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/activity/BaseActivity$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/BaseActivity$1;-><init>(Lcom/itau/empresas/ui/activity/BaseActivity;)V

    .line 294
    const v2, 0x7f0704ae

    invoke-virtual {v0, v2, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 302
    const v1, 0x7f070175

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 303
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 304
    return-void
.end method

.method private trataEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 2
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 370
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->logaEscondeProgresEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V

    .line 371
    invoke-virtual {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->mostraEstadoDeErro(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V

    .line 372
    return-void
.end method


# virtual methods
.method public afterInject()V
    .registers 1

    .line 286
    return-void
.end method

.method public analyticsHit(Ljava/lang/String;)V
    .registers 3
    .param p1, "acao"    # Ljava/lang/String;

    .line 183
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    return-void
.end method

.method public analyticsHit(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "acao"    # Ljava/lang/String;
    .param p2, "label"    # Ljava/lang/String;

    .line 187
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->analyticsHit(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 188
    return-void
.end method

.method public analyticsHit(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .registers 7
    .param p1, "acao"    # Ljava/lang/String;
    .param p2, "label"    # Ljava/lang/String;
    .param p3, "valor"    # Ljava/lang/Long;

    .line 191
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/BaseActivity;->application:Lcom/itau/empresas/CustomApplication;

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/itau/empresas/ui/activity/BaseActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    if-eqz v0, :cond_1e

    .line 192
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/BaseActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 193
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->hitAnalytics()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p1, p2, p3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 195
    :cond_1e
    return-void
.end method

.method public escondeDialogoDeProgresso()V
    .registers 7

    .line 141
    move-object v3, p0

    monitor-enter v3

    .line 143
    :try_start_2
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->popBackStack()V

    .line 144
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/BaseActivity;->tag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_2e

    .line 145
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 146
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/activity/BaseActivity;->tag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 147
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 150
    :cond_2e
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/BaseActivity;->loadingFragment:Lcom/itau/empresas/ui/fragment/LoadingFragment;
    :try_end_31
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_31} :catch_32
    .catchall {:try_start_2 .. :try_end_31} :catchall_35

    .line 153
    goto :goto_33

    .line 151
    :catch_32
    move-exception v4

    .line 154
    :goto_33
    monitor-exit v3

    goto :goto_38

    :catchall_35
    move-exception v5

    monitor-exit v3

    throw v5

    .line 155
    :goto_38
    return-void
.end method

.method public hitAnalytics()Ljava/lang/String;
    .registers 2

    .line 199
    const v0, 0x7f0702dd

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public mostraDialogoDeProgresso()V
    .registers 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 116
    move-object v4, p0

    monitor-enter v4

    .line 118
    :try_start_2
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/BaseActivity;->loadingFragment:Lcom/itau/empresas/ui/fragment/LoadingFragment;

    if-nez v0, :cond_26

    .line 119
    invoke-static {}, Lcom/itau/empresas/ui/fragment/LoadingFragment_;->builder()Lcom/itau/empresas/ui/fragment/LoadingFragment_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/fragment/LoadingFragment_$FragmentBuilder_;->build()Lcom/itau/empresas/ui/fragment/LoadingFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/BaseActivity;->loadingFragment:Lcom/itau/empresas/ui/fragment/LoadingFragment;

    .line 120
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/BaseActivity;->loadingFragment:Lcom/itau/empresas/ui/fragment/LoadingFragment;

    if-eqz v0, :cond_26

    .line 121
    .line 122
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/BaseActivity;->loadingFragment:Lcom/itau/empresas/ui/fragment/LoadingFragment;

    iget-object v2, p0, Lcom/itau/empresas/ui/activity/BaseActivity;->tag:Ljava/lang/String;

    .line 121
    const v3, 0x1020002

    invoke-static {v0, v3, v1, v2}, Lcom/itau/empresas/ui/util/FragmentUtils;->attachFragment(Landroid/support/v4/app/FragmentTransaction;ILcom/itau/empresas/ui/fragment/BaseFragment;Ljava/lang/String;)V
    :try_end_26
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_26} :catch_27
    .catchall {:try_start_2 .. :try_end_26} :catchall_2a

    .line 131
    :cond_26
    goto :goto_28

    .line 129
    :catch_27
    move-exception v5

    .line 132
    :goto_28
    monitor-exit v4

    goto :goto_2d

    :catchall_2a
    move-exception v6

    monitor-exit v4

    throw v6

    .line 134
    :goto_2d
    return-void
.end method

.method protected mostraEstadoDeErro(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 8
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 340
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v3

    .line 341
    .local v3, "exception":Lbr/com/itau/sdk/android/core/exception/BackendException;
    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    .line 342
    .local v4, "opKey":Ljava/lang/String;
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "saldo"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "multiLimites"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "lis"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "francesinha"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "consultaCarrinho"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "/recebiveis/antecipacao/limites"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "consultaLimitesGiro"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "_atualizalimitelegado"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    invoke-static {v4, v0}, Lcom/itau/empresas/ui/util/CollectionUtils;->contains(Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5c

    .line 346
    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    if-eqz v0, :cond_51

    .line 347
    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v5

    .line 348
    .local v5, "error":Lbr/com/itau/sdk/android/core/model/ErrorDTO;
    invoke-virtual {v5}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/itau/empresas/ui/util/SnackbarUtils;->tentaMostrarSnackbarNo(Ljava/lang/Object;Ljava/lang/CharSequence;I)V

    .line 350
    .end local v5    # "error":Lbr/com/itau/sdk/android/core/model/ErrorDTO;
    goto :goto_5c

    .line 351
    .line 352
    :cond_51
    const v0, 0x7f07046e

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 351
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/itau/empresas/ui/util/SnackbarUtils;->tentaMostrarSnackbarNo(Ljava/lang/Object;Ljava/lang/CharSequence;I)V

    .line 356
    :cond_5c
    :goto_5c
    return-void
.end method

.method protected mostraEstadoDeErro(Ljava/lang/String;)V
    .registers 3
    .param p1, "mensagem"    # Ljava/lang/String;

    .line 359
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/itau/empresas/ui/util/SnackbarUtils;->tentaMostrarSnackbarNo(Ljava/lang/Object;Ljava/lang/CharSequence;I)V

    .line 362
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 273
    :try_start_0
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->registraEventBus()V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_7

    .line 275
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 276
    goto :goto_c

    .line 275
    :catchall_7
    move-exception v0

    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    throw v0

    .line 277
    :goto_c
    return-void
.end method

.method protected onDestroy()V
    .registers 2

    .line 264
    :try_start_0
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->desregistraEventBus()V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_7

    .line 266
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onDestroy()V

    .line 267
    goto :goto_c

    .line 266
    :catchall_7
    move-exception v0

    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onDestroy()V

    throw v0

    .line 268
    :goto_c
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Network;)V
    .registers 5
    .param p1, "event"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Network;

    .line 174
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Network;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    .line 175
    .local v2, "exception":Lbr/com/itau/sdk/android/core/exception/BackendException;
    if-eqz v2, :cond_13

    const-string v0, "menuUniversal"

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 176
    return-void

    .line 179
    :cond_13
    invoke-static {p0}, Lcom/itau/empresas/ui/activity/BaseActivityHelper;->trataErroSemConexao(Lcom/itau/empresas/ui/activity/BaseActivity;)V

    .line 180
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 2
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 366
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->trataEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V

    .line 367
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$ErroInesperadoApi;)V
    .registers 2
    .param p1, "evento"    # Lcom/itau/empresas/Evento$ErroInesperadoApi;

    .line 160
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->showDefaultErrorDialog()V

    .line 161
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$MostraDialogoDeErro;)V
    .registers 2
    .param p1, "mostraDialogoDeErro"    # Lcom/itau/empresas/Evento$MostraDialogoDeErro;

    .line 165
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->showDefaultErrorDialog()V

    .line 166
    return-void
.end method

.method public onEventMainThread(Ljava/lang/NullPointerException;)V
    .registers 3
    .param p1, "ex"    # Ljava/lang/NullPointerException;

    .line 169
    new-instance v0, Lcom/itau/empresas/Evento$EventoSair;

    invoke-direct {v0}, Lcom/itau/empresas/Evento$EventoSair;-><init>()V

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->publicaEvento(Ljava/lang/Object;)V

    .line 170
    return-void
.end method

.method protected onPause()V
    .registers 2

    .line 253
    :try_start_0
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->desregistraEventBus()V

    .line 254
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 255
    invoke-static {}, Lcom/adobe/mobile/Config;->pauseCollectingLifecycleData()V
    :try_end_9
    .catchall {:try_start_0 .. :try_end_9} :catchall_d

    .line 257
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onPause()V

    .line 258
    goto :goto_12

    .line 257
    :catchall_d
    move-exception v0

    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onPause()V

    throw v0

    .line 259
    :goto_12
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .registers 5
    .param p1, "requestCode"    # I
    .param p2, "permissions"    # [Ljava/lang/String;
    .param p3, "grantResults"    # [I

    .line 206
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/AppCompatActivity;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    .line 207
    if-nez p1, :cond_e

    .line 208
    new-instance v0, Lcom/itau/empresas/Evento$PermissaoCamera;

    invoke-direct {v0}, Lcom/itau/empresas/Evento$PermissaoCamera;-><init>()V

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->publicaEvento(Ljava/lang/Object;)V

    goto :goto_25

    .line 209
    :cond_e
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1a

    .line 210
    new-instance v0, Lcom/itau/empresas/Evento$PermissaoLocalizacao;

    invoke-direct {v0}, Lcom/itau/empresas/Evento$PermissaoLocalizacao;-><init>()V

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->publicaEvento(Ljava/lang/Object;)V

    goto :goto_25

    .line 211
    :cond_1a
    const/4 v0, 0x1

    if-ne p1, v0, :cond_25

    .line 212
    new-instance v0, Lcom/itau/empresas/Evento$PermissaoContatos;

    invoke-direct {v0}, Lcom/itau/empresas/Evento$PermissaoContatos;-><init>()V

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->publicaEvento(Ljava/lang/Object;)V

    .line 214
    :cond_25
    :goto_25
    const/4 v0, 0x3

    if-ne p1, v0, :cond_30

    .line 215
    new-instance v0, Lcom/itau/empresas/Evento$PermissaoLigacao;

    invoke-direct {v0}, Lcom/itau/empresas/Evento$PermissaoLigacao;-><init>()V

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->publicaEvento(Ljava/lang/Object;)V

    .line 217
    :cond_30
    return-void
.end method

.method protected onResume()V
    .registers 2

    .line 242
    :try_start_0
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->registraEventBus()V

    .line 243
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->checkForCrashes()V
    :try_end_6
    .catchall {:try_start_0 .. :try_end_6} :catchall_d

    .line 245
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onResume()V

    .line 246
    invoke-static {p0}, Lcom/adobe/mobile/Config;->collectLifecycleData(Landroid/app/Activity;)V

    .line 247
    goto :goto_15

    .line 245
    :catchall_d
    move-exception v0

    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onResume()V

    .line 246
    invoke-static {p0}, Lcom/adobe/mobile/Config;->collectLifecycleData(Landroid/app/Activity;)V

    throw v0

    .line 248
    :goto_15
    return-void
.end method

.method protected onStart()V
    .registers 2

    .line 223
    :try_start_0
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->registraEventBus()V
    :try_end_3
    .catchall {:try_start_0 .. :try_end_3} :catchall_7

    .line 225
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onStart()V

    .line 226
    goto :goto_c

    .line 225
    :catchall_7
    move-exception v0

    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onStart()V

    throw v0

    .line 227
    :goto_c
    return-void
.end method

.method protected onStop()V
    .registers 2

    .line 232
    :try_start_0
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->desregistraEventBus()V

    .line 233
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V
    :try_end_6
    .catchall {:try_start_0 .. :try_end_6} :catchall_a

    .line 235
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onStop()V

    .line 236
    goto :goto_f

    .line 235
    :catchall_a
    move-exception v0

    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onStop()V

    throw v0

    .line 237
    :goto_f
    return-void
.end method

.method public publicaEvento(Ljava/lang/Object;)V
    .registers 3
    .param p1, "evento"    # Ljava/lang/Object;

    .line 105
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 106
    return-void
.end method

.method public varargs registraView([Landroid/view/View;)V
    .registers 3
    .param p1, "views"    # [Landroid/view/View;

    .line 91
    if-eqz p1, :cond_5

    array-length v0, p1

    if-nez v0, :cond_6

    .line 92
    :cond_5
    return-void

    .line 95
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/BaseActivity;->views:Ljava/util/List;

    invoke-static {v0, p1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 96
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/BaseActivity;->views:Ljava/util/List;

    invoke-static {v0}, Lcom/itau/empresas/ui/activity/BaseActivityHelper;->registraViews(Ljava/util/List;)V

    .line 97
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .registers 5
    .param p1, "intent"    # Landroid/content/Intent;

    .line 72
    const-string v0, ""

    const/4 v1, 0x1

    :try_start_3
    invoke-static {v0, v1}, Lorg/androidannotations/api/BackgroundExecutor;->cancelAll(Ljava/lang/String;Z)V

    .line 74
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->isRegistered(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 75
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->unregister(Ljava/lang/Object;)V
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_1b

    .line 79
    :cond_17
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->startActivity(Landroid/content/Intent;)V

    .line 80
    goto :goto_20

    .line 79
    :catchall_1b
    move-exception v2

    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->startActivity(Landroid/content/Intent;)V

    throw v2

    .line 81
    :goto_20
    return-void
.end method

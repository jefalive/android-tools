.class public Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;
.super Ljava/lang/Object;
.source "RecargaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private codigoPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_pagamento"
    .end annotation
.end field

.field private identificacaoComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "identificacao_comprovante"
    .end annotation
.end field

.field private nomeOperadora:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_operadora"
    .end annotation
.end field

.field private nomeRegiao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_regiao"
    .end annotation
.end field

.field private valorRecarga:Ljava/lang/Integer;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_recarga"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getNomeOperadora()Ljava/lang/String;
    .registers 2

    .line 29
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->nomeOperadora:Ljava/lang/String;

    return-object v0
.end method

.method public getValorRecarga()Ljava/lang/Integer;
    .registers 2

    .line 45
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->valorRecarga:Ljava/lang/Integer;

    return-object v0
.end method

.method public setCodigoPagamento(Ljava/lang/String;)V
    .registers 2
    .param p1, "codigoPagamento"    # Ljava/lang/String;

    .line 21
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->codigoPagamento:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public setIdentificacaoComprovante(Ljava/lang/String;)V
    .registers 2
    .param p1, "identificacaoComprovante"    # Ljava/lang/String;

    .line 25
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->identificacaoComprovante:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public setNomeOperadora(Ljava/lang/String;)V
    .registers 2
    .param p1, "nomeOperadora"    # Ljava/lang/String;

    .line 33
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->nomeOperadora:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public setNomeRegiao(Ljava/lang/String;)V
    .registers 2
    .param p1, "nomeRegiao"    # Ljava/lang/String;

    .line 41
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->nomeRegiao:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public setValorRecarga(Ljava/lang/Integer;)V
    .registers 2
    .param p1, "valorRecarga"    # Ljava/lang/Integer;

    .line 49
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->valorRecarga:Ljava/lang/Integer;

    .line 50
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RecargaVO{nomeOperadora=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->nomeOperadora:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", nomeRegiao=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->nomeRegiao:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", valorRecarga=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->valorRecarga:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", identificacaoComprovante=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->identificacaoComprovante:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", codigoPagamento=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->codigoPagamento:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

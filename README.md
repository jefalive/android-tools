# android-tools #

Conjunto de ferramentas e automatização para descompilar, editar e recompilar aplicativos android (extensão .apk)


### Dependências

As ferramentas foram clonadas dentro do meu repo, agradecimentos aos donos desses repos:

* https://github.com/JesusFreke/smali

* https://github.com/iBotPeaches/Apktool

* https://github.com/java-decompiler/jd-gui

* https://github.com/Konloch/bytecode-viewer

* https://github.com/pxb1988/dex2jar

* https://github.com/patrickfav/uber-apk-signer

* https://github.com/skylot/jadx


### Como utilizo ###

* Para descompactar o .apk em um diretório (com resources, assets e arquivo classes.dex) e depois recompilar uso o **iBotPeaches/Apktool**

* Para descompilar o classes.dex em arquivos editáveis .smali utilizo o **JesusFreke/smali**

* Para visualizar o código descompilado com desobfuscação utilizo os seguintes:
  - **java-decompiler/jd-gui** → ele é o clássico pra isso
  - **Konloch/bytecode-viewer** → nesse você consegue colocar lado a lado o código desobfuscado através de vários descompiladores (é bom pra comparar)
  - **skylot/jadx** → achei que esse me proporcionou a melhor desobfuscação, ele possui vários parâmetros de desobfuscação na linha de comando, recomendo testar vários deles e ver qual se encaixa melhor no apk que você está trabalhando

* Para fazer o sign do apk gerado utilizo o **patrickfav/uber-apk-signer**

* (Bônus)
Uso o **pxb1988/dex2jar** quando quero transformar o .dex em .jar, ele também pode transformar o .dex em .smali e ainda possui outros recursos, mas para fazer .dex em .smali prefiro o **JesusFreke/smali**, no meu projeto era esse que não dava erro

### Scripts

Fiz 2 scripts para automatizar minhas tarefas, o **unpack.sh** e o **repack.sh**

O **unpack** cria o diretório tmp (**ou limpa ele todo caso ele já exista, cuidado**), pega o apk do diretório apks e gera um diretório dentro de tmp/ com arquivos .smali

O **repack** pega o diretório com arquivos .smali (que provavelmente você editou) e recria o .apk

### Observações

Como alguns .jar utilizavam Java 7 e outros Java 8, fiz as chamadas dele no script assim:

* /usr/lib/jvm/java-7-oracle/bin/java -jar blah.jar

* /usr/lib/jvm/java-8-oracle/bin/java -jar blah.jar

Então lembre de ajustar seu script de acordo com seus binários do Java
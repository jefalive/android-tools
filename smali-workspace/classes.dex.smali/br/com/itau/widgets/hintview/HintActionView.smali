.class public Lbr/com/itau/widgets/hintview/HintActionView;
.super Landroid/widget/FrameLayout;
.source "HintActionView.java"


# instance fields
.field private mImageView:Landroid/widget/ImageView;

.field private mMenuItem:Landroid/view/MenuItem;

.field private mOnMenuItemClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lbr/com/itau/widgets/hintview/HintActionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 58
    const v0, 0x10102d8

    invoke-direct {p0, p1, p2, v0}, Lbr/com/itau/widgets/hintview/HintActionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 62
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/hintview/HintActionView;->setClickable(Z)V

    .line 64
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/hintview/HintActionView;->setLongClickable(Z)V

    .line 66
    invoke-virtual {p0}, Lbr/com/itau/widgets/hintview/HintActionView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/widgets/hintview/R$dimen;->action_button_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 67
    .local v3, "itemWidth":I
    invoke-virtual {p0}, Lbr/com/itau/widgets/hintview/HintActionView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/widgets/hintview/R$dimen;->action_button_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 68
    .local v4, "itemPadding":I
    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    sub-int v0, v3, v4

    const/4 v1, -0x2

    const/16 v2, 0x11

    invoke-direct {v5, v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 70
    .local v5, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/HintActionView;->mTextView:Landroid/widget/TextView;

    .line 71
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/HintActionView;->mImageView:Landroid/widget/ImageView;

    .line 73
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/HintActionView;->mTextView:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setDuplicateParentStateEnabled(Z)V

    .line 74
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/HintActionView;->mImageView:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setDuplicateParentStateEnabled(Z)V

    .line 76
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/HintActionView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v5}, Lbr/com/itau/widgets/hintview/HintActionView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 77
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/HintActionView;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, v5}, Lbr/com/itau/widgets/hintview/HintActionView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 78
    return-void
.end method


# virtual methods
.method public getMenuItem()Landroid/view/MenuItem;
    .registers 2

    .line 113
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/HintActionView;->mMenuItem:Landroid/view/MenuItem;

    return-object v0
.end method

.method public performClick()Z
    .registers 3

    .line 82
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/HintActionView;->mOnMenuItemClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    if-eqz v0, :cond_b

    .line 83
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/HintActionView;->mOnMenuItemClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    iget-object v1, p0, Lbr/com/itau/widgets/hintview/HintActionView;->mMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem$OnMenuItemClickListener;->onMenuItemClick(Landroid/view/MenuItem;)Z

    .line 85
    :cond_b
    invoke-super {p0}, Landroid/widget/FrameLayout;->performClick()Z

    move-result v0

    return v0
.end method

.method public performLongClick()Z
    .registers 11

    .line 90
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/HintActionView;->mMenuItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_64

    iget-object v0, p0, Lbr/com/itau/widgets/hintview/HintActionView;->mMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_64

    .line 91
    const/4 v0, 0x2

    new-array v2, v0, [I

    .line 92
    .local v2, "screenPos":[I
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 93
    .local v3, "displayFrame":Landroid/graphics/Rect;
    invoke-virtual {p0, v2}, Lbr/com/itau/widgets/hintview/HintActionView;->getLocationOnScreen([I)V

    .line 94
    invoke-virtual {p0, v3}, Lbr/com/itau/widgets/hintview/HintActionView;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 95
    invoke-virtual {p0}, Lbr/com/itau/widgets/hintview/HintActionView;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 96
    .local v4, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lbr/com/itau/widgets/hintview/HintActionView;->getWidth()I

    move-result v5

    .line 97
    .local v5, "width":I
    invoke-virtual {p0}, Lbr/com/itau/widgets/hintview/HintActionView;->getHeight()I

    move-result v6

    .line 98
    .local v6, "height":I
    const/4 v0, 0x1

    aget v0, v2, v0

    div-int/lit8 v1, v6, 0x2

    add-int v7, v0, v1

    .line 99
    .local v7, "middleY":I
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v8, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 100
    .local v8, "screenWidth":I
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/HintActionView;->mMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v4, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v9

    .line 101
    .local v9, "cheatSheet":Landroid/widget/Toast;
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-ge v7, v0, :cond_5b

    .line 102
    const/4 v0, 0x0

    aget v0, v2, v0

    sub-int v0, v8, v0

    div-int/lit8 v1, v5, 0x2

    sub-int/2addr v0, v1

    const v1, 0x800035

    invoke-virtual {v9, v1, v0, v6}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_61

    .line 104
    :cond_5b
    const/16 v0, 0x51

    const/4 v1, 0x0

    invoke-virtual {v9, v0, v1, v6}, Landroid/widget/Toast;->setGravity(III)V

    .line 106
    :goto_61
    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    .line 108
    .end local v2    # "screenPos":[I
    .end local v3    # "displayFrame":Landroid/graphics/Rect;
    .end local v4    # "context":Landroid/content/Context;
    .end local v5    # "width":I
    .end local v6    # "height":I
    .end local v7    # "middleY":I
    .end local v8    # "screenWidth":I
    .end local v9    # "cheatSheet":Landroid/widget/Toast;
    :cond_64
    invoke-super {p0}, Landroid/widget/FrameLayout;->performLongClick()Z

    move-result v0

    return v0
.end method

.method public setMenuItem(Landroid/view/MenuItem;)V
    .registers 5
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .line 117
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/HintActionView;->mMenuItem:Landroid/view/MenuItem;

    if-eq v0, p1, :cond_31

    .line 118
    iput-object p1, p0, Lbr/com/itau/widgets/hintview/HintActionView;->mMenuItem:Landroid/view/MenuItem;

    .line 120
    invoke-static {p1}, Landroid/support/v4/view/MenuItemCompat;->getActionView(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v2

    .line 121
    .local v2, "actionView":Landroid/view/View;
    if-eqz v2, :cond_31

    invoke-virtual {v2, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 122
    invoke-interface {p1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 123
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/HintActionView;->mImageView:Landroid/widget/ImageView;

    invoke-interface {p1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_31

    .line 124
    :cond_22
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_31

    .line 125
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/HintActionView;->mTextView:Landroid/widget/TextView;

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    .end local v2    # "actionView":Landroid/view/View;
    :cond_31
    :goto_31
    return-void
.end method

.method public setOnMenuItemClick(Landroid/view/MenuItem$OnMenuItemClickListener;)V
    .registers 2
    .param p1, "l"    # Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 132
    iput-object p1, p0, Lbr/com/itau/widgets/hintview/HintActionView;->mOnMenuItemClickListener:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 133
    return-void
.end method

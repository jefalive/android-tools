.class public interface abstract Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/ChartInterface;
.super Ljava/lang/Object;
.source "ChartInterface.java"


# virtual methods
.method public abstract getCenterOfView()Landroid/graphics/PointF;
.end method

.method public abstract getCenterOffsets()Landroid/graphics/PointF;
.end method

.method public abstract getContentRect()Landroid/graphics/RectF;
.end method

.method public abstract getData()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;
.end method

.method public abstract getDefaultValueFormatter()Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;
.end method

.method public abstract getHeight()I
.end method

.method public abstract getWidth()I
.end method

.method public abstract getXChartMax()F
.end method

.method public abstract getXChartMin()F
.end method

.method public abstract getXValCount()I
.end method

.method public abstract getYChartMax()F
.end method

.method public abstract getYChartMin()F
.end method

.class public Lcom/google/android/gms/internal/zzg;
.super Ljava/lang/Thread;


# instance fields
.field private final zzj:Lcom/google/android/gms/internal/zzb;

.field private final zzk:Lcom/google/android/gms/internal/zzn;

.field private volatile zzl:Z

.field private final zzy:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<Lcom/google/android/gms/internal/zzk<*>;>;"
        }
    .end annotation
.end field

.field private final zzz:Lcom/google/android/gms/internal/zzf;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/BlockingQueue;Lcom/google/android/gms/internal/zzf;Lcom/google/android/gms/internal/zzb;Lcom/google/android/gms/internal/zzn;)V
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/concurrent/BlockingQueue<Lcom/google/android/gms/internal/zzk<*>;>;Lcom/google/android/gms/internal/zzf;Lcom/google/android/gms/internal/zzb;Lcom/google/android/gms/internal/zzn;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzg;->zzl:Z

    iput-object p1, p0, Lcom/google/android/gms/internal/zzg;->zzy:Ljava/util/concurrent/BlockingQueue;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzg;->zzz:Lcom/google/android/gms/internal/zzf;

    iput-object p3, p0, Lcom/google/android/gms/internal/zzg;->zzj:Lcom/google/android/gms/internal/zzb;

    iput-object p4, p0, Lcom/google/android/gms/internal/zzg;->zzk:Lcom/google/android/gms/internal/zzn;

    return-void
.end method

.method private zzb(Lcom/google/android/gms/internal/zzk;)V
    .registers 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/google/android/gms/internal/zzk<*>;)V"
        }
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_d

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzk;->zzg()I

    move-result v0

    invoke-static {v0}, Landroid/net/TrafficStats;->setThreadStatsTag(I)V

    :cond_d
    return-void
.end method

.method private zzb(Lcom/google/android/gms/internal/zzk;Lcom/google/android/gms/internal/zzr;)V
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/google/android/gms/internal/zzk<*>;Lcom/google/android/gms/internal/zzr;)V"
        }
    .end annotation

    invoke-virtual {p1, p2}, Lcom/google/android/gms/internal/zzk;->zzb(Lcom/google/android/gms/internal/zzr;)Lcom/google/android/gms/internal/zzr;

    move-result-object p2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzg;->zzk:Lcom/google/android/gms/internal/zzn;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/internal/zzn;->zza(Lcom/google/android/gms/internal/zzk;Lcom/google/android/gms/internal/zzr;)V

    return-void
.end method


# virtual methods
.method public quit()V
    .registers 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzg;->zzl:Z

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzg;->interrupt()V

    return-void
.end method

.method public run()V
    .registers 10

    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    :goto_5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    :try_start_9
    iget-object v0, p0, Lcom/google/android/gms/internal/zzg;->zzy:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/gms/internal/zzk;
    :try_end_12
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_12} :catch_13

    goto :goto_1a

    :catch_13
    move-exception v7

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzg;->zzl:Z

    if-eqz v0, :cond_19

    return-void

    :cond_19
    goto :goto_5

    :goto_1a
    const-string v0, "network-queue-take"

    :try_start_1c
    invoke-virtual {v6, v0}, Lcom/google/android/gms/internal/zzk;->zzc(Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/google/android/gms/internal/zzk;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_2b

    const-string v0, "network-discard-cancelled"

    invoke-virtual {v6, v0}, Lcom/google/android/gms/internal/zzk;->zzd(Ljava/lang/String;)V
    :try_end_2a
    .catch Lcom/google/android/gms/internal/zzr; {:try_start_1c .. :try_end_2a} :catch_75
    .catch Ljava/lang/Exception; {:try_start_1c .. :try_end_2a} :catch_82

    goto :goto_5

    :cond_2b
    :try_start_2b
    invoke-direct {p0, v6}, Lcom/google/android/gms/internal/zzg;->zzb(Lcom/google/android/gms/internal/zzk;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzg;->zzz:Lcom/google/android/gms/internal/zzf;

    invoke-interface {v0, v6}, Lcom/google/android/gms/internal/zzf;->zza(Lcom/google/android/gms/internal/zzk;)Lcom/google/android/gms/internal/zzi;

    move-result-object v7

    const-string v0, "network-http-complete"

    invoke-virtual {v6, v0}, Lcom/google/android/gms/internal/zzk;->zzc(Ljava/lang/String;)V

    iget-boolean v0, v7, Lcom/google/android/gms/internal/zzi;->zzB:Z

    if-eqz v0, :cond_49

    invoke-virtual {v6}, Lcom/google/android/gms/internal/zzk;->zzw()Z

    move-result v0

    if-eqz v0, :cond_49

    const-string v0, "not-modified"

    invoke-virtual {v6, v0}, Lcom/google/android/gms/internal/zzk;->zzd(Ljava/lang/String;)V
    :try_end_48
    .catch Lcom/google/android/gms/internal/zzr; {:try_start_2b .. :try_end_48} :catch_75
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_48} :catch_82

    goto :goto_5

    :cond_49
    :try_start_49
    invoke-virtual {v6, v7}, Lcom/google/android/gms/internal/zzk;->zza(Lcom/google/android/gms/internal/zzi;)Lcom/google/android/gms/internal/zzm;

    move-result-object v8

    const-string v0, "network-parse-complete"

    invoke-virtual {v6, v0}, Lcom/google/android/gms/internal/zzk;->zzc(Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/google/android/gms/internal/zzk;->zzr()Z

    move-result v0

    if-eqz v0, :cond_6c

    iget-object v0, v8, Lcom/google/android/gms/internal/zzm;->zzag:Lcom/google/android/gms/internal/zzb$zza;

    if-eqz v0, :cond_6c

    iget-object v0, p0, Lcom/google/android/gms/internal/zzg;->zzj:Lcom/google/android/gms/internal/zzb;

    invoke-virtual {v6}, Lcom/google/android/gms/internal/zzk;->zzh()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v8, Lcom/google/android/gms/internal/zzm;->zzag:Lcom/google/android/gms/internal/zzb$zza;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/internal/zzb;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzb$zza;)V

    const-string v0, "network-cache-written"

    invoke-virtual {v6, v0}, Lcom/google/android/gms/internal/zzk;->zzc(Ljava/lang/String;)V

    :cond_6c
    invoke-virtual {v6}, Lcom/google/android/gms/internal/zzk;->zzv()V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzg;->zzk:Lcom/google/android/gms/internal/zzn;

    invoke-interface {v0, v6, v8}, Lcom/google/android/gms/internal/zzn;->zza(Lcom/google/android/gms/internal/zzk;Lcom/google/android/gms/internal/zzm;)V
    :try_end_74
    .catch Lcom/google/android/gms/internal/zzr; {:try_start_49 .. :try_end_74} :catch_75
    .catch Ljava/lang/Exception; {:try_start_49 .. :try_end_74} :catch_82

    goto :goto_a4

    :catch_75
    move-exception v7

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, v4

    invoke-virtual {v7, v0, v1}, Lcom/google/android/gms/internal/zzr;->zza(J)V

    invoke-direct {p0, v6, v7}, Lcom/google/android/gms/internal/zzg;->zzb(Lcom/google/android/gms/internal/zzk;Lcom/google/android/gms/internal/zzr;)V

    goto :goto_a4

    :catch_82
    move-exception v7

    const-string v0, "Unhandled exception %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v7, v0, v1}, Lcom/google/android/gms/internal/zzs;->zza(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v8, Lcom/google/android/gms/internal/zzr;

    invoke-direct {v8, v7}, Lcom/google/android/gms/internal/zzr;-><init>(Ljava/lang/Throwable;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, v4

    invoke-virtual {v8, v0, v1}, Lcom/google/android/gms/internal/zzr;->zza(J)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzg;->zzk:Lcom/google/android/gms/internal/zzn;

    invoke-interface {v0, v6, v8}, Lcom/google/android/gms/internal/zzn;->zza(Lcom/google/android/gms/internal/zzk;Lcom/google/android/gms/internal/zzr;)V

    :goto_a4
    goto/16 :goto_5
.end method

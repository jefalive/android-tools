.class public Lcom/itau/empresas/ui/util/menu/PedagioAutorizarCommand;
.super Ljava/lang/Object;
.source "PedagioAutorizarCommand.java"

# interfaces
.implements Lcom/itau/empresas/ui/util/menu/Command;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public executar(Landroid/support/v7/app/AppCompatActivity;Lcom/itau/empresas/api/model/MenuVO;)V
    .registers 10
    .param p1, "activity"    # Landroid/support/v7/app/AppCompatActivity;
    .param p2, "menuVO"    # Lcom/itau/empresas/api/model/MenuVO;

    .line 17
    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/CustomApplication;

    .line 18
    .local v2, "app":Lcom/itau/empresas/CustomApplication;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 19
    .local v3, "labels":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 21
    .local v4, "chaves":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v2, :cond_94

    .line 22
    sget-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_PEDAGIO_AUTORIZAR_PEX:Lcom/itau/empresas/CustomApplication$Exibicao;

    invoke-virtual {v2, v0}, Lcom/itau/empresas/CustomApplication;->getExibicao(Lcom/itau/empresas/CustomApplication$Exibicao;)Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v5

    .line 23
    .local v5, "pex":Lcom/itau/empresas/api/model/MenuVO;
    if-eqz v5, :cond_2c

    .line 24
    const v0, 0x7f07012f

    invoke-virtual {p1, v0}, Landroid/support/v7/app/AppCompatActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 26
    invoke-virtual {v5}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    :cond_2c
    sget-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_PEDAGIO_AUTORIZAR_SISPAG:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 30
    invoke-virtual {v2, v0}, Lcom/itau/empresas/CustomApplication;->getExibicao(Lcom/itau/empresas/CustomApplication$Exibicao;)Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v6

    .line 31
    .local v6, "sispag":Lcom/itau/empresas/api/model/MenuVO;
    if-eqz v6, :cond_45

    .line 32
    const v0, 0x7f07012e

    invoke-virtual {p1, v0}, Landroid/support/v7/app/AppCompatActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 33
    invoke-virtual {v6}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    :cond_45
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_55

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_56

    .line 37
    :cond_55
    return-void

    .line 39
    :cond_56
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_76

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_76

    .line 40
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcom/itau/empresas/ui/activity/WebViewActivity;->iniciarWebViewActivity(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    return-void

    .line 43
    :cond_76
    invoke-static {}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_;->builder()Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;

    move-result-object v0

    .line 45
    const v1, 0x7f0702e2

    invoke-virtual {p1, v1}, Landroid/support/v7/app/AppCompatActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 44
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;->categoriaAnalytics(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;

    move-result-object v0

    .line 46
    invoke-virtual {v0, v3}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;->labels(Ljava/util/ArrayList;)Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;

    move-result-object v0

    .line 47
    invoke-virtual {v0, v4}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;->chaves(Ljava/util/ArrayList;)Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog_$FragmentBuilder_;->build()Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;

    move-result-object v0

    .line 49
    invoke-virtual {v0, p1}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 52
    .end local v5    # "pex":Lcom/itau/empresas/api/model/MenuVO;
    .end local v6    # "sispag":Lcom/itau/empresas/api/model/MenuVO;
    :cond_94
    return-void
.end method

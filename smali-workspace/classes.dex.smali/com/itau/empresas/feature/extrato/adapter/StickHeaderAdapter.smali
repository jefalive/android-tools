.class interface abstract Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;
.super Ljava/lang/Object;
.source "StickHeaderAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:Landroid/support/v7/widget/RecyclerView$ViewHolder;>Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract getHeaderId(I)J
.end method

.method public abstract onBindHeaderViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)V"
        }
    .end annotation
.end method

.method public abstract onCreateHeaderViewHolder(Landroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/view/ViewGroup;)TT;"
        }
    .end annotation
.end method

.class public Lbr/com/itau/sdk/android/core/webview/MemoryStorageWebView;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/webview/MemoryStorageWebView$SingletonHolder;
    }
.end annotation


# static fields
.field private static mapKeyValues:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 7
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lbr/com/itau/sdk/android/core/webview/MemoryStorageWebView;->mapKeyValues:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/itau/sdk/android/core/webview/MemoryStorageWebView$1;)V
    .registers 2

    .line 6
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/webview/MemoryStorageWebView;-><init>()V

    return-void
.end method

.method public static getInstance()Lbr/com/itau/sdk/android/core/webview/MemoryStorageWebView;
    .registers 1

    .line 13
    # getter for: Lbr/com/itau/sdk/android/core/webview/MemoryStorageWebView$SingletonHolder;->INSTANCE:Lbr/com/itau/sdk/android/core/webview/MemoryStorageWebView;
    invoke-static {}, Lbr/com/itau/sdk/android/core/webview/MemoryStorageWebView$SingletonHolder;->access$000()Lbr/com/itau/sdk/android/core/webview/MemoryStorageWebView;

    move-result-object v0

    .line 14
    return-object v0
.end method


# virtual methods
.method public clearAll()V
    .registers 2

    .line 26
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/MemoryStorageWebView;->mapKeyValues:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 27
    return-void
.end method

.method public findItem(Ljava/lang/String;)Ljava/lang/Object;
    .registers 3

    .line 22
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/MemoryStorageWebView;->mapKeyValues:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_f

    sget-object v0, Lbr/com/itau/sdk/android/core/webview/MemoryStorageWebView;->mapKeyValues:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_11

    :cond_f
    const-string v0, ""

    :goto_11
    return-object v0
.end method

.method public save(Ljava/lang/String;Ljava/lang/Object;)V
    .registers 4

    .line 18
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/MemoryStorageWebView;->mapKeyValues:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    return-void
.end method

.method public saveAll(Ljava/util/HashMap;)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;)V"
        }
    .end annotation

    .line 30
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/MemoryStorageWebView;->mapKeyValues:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 31
    return-void
.end method

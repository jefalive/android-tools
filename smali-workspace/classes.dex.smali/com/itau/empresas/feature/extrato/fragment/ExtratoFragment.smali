.class public Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;
.super Landroid/support/v4/app/Fragment;
.source "ExtratoFragment.java"

# interfaces
.implements Lcom/itau/empresas/feature/extrato/fragment/OnClickMaisLancamentos;


# instance fields
.field private abaFuturo:Z

.field private adapter:Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;

.field private filtroListener:Lcom/itau/empresas/feature/extrato/FiltroListener;

.field private listenerTextoBusca:Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;

.field rvExtrato:Landroid/support/v7/widget/RecyclerView;

.field private textoBuscado:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 18
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private inicializaAdapter(Ljava/util/List;I)V
    .registers 6
    .param p1, "lancamentos"    # Ljava/util/List;
    .param p2, "ordem"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/LancamentoVO;>;I)V"
        }
    .end annotation

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->adapter:Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->setDados(Ljava/util/List;I)V

    .line 70
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->rvExtrato:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->adapter:Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->rvExtrato:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->adapter:Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->notifyDataSetChanged()V

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->adapter:Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->textoBuscado:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->setTextoBuscado(Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method private montarAdapter()V
    .registers 5

    .line 62
    new-instance v0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-boolean v2, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->abaFuturo:Z

    invoke-direct {v0, v1, p0, v2}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;-><init>(Landroid/content/Context;Lcom/itau/empresas/feature/extrato/fragment/OnClickMaisLancamentos;Z)V

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->adapter:Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->adapter:Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->listenerTextoBusca:Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->setListenerTextoBusca(Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;)V

    .line 64
    new-instance v3, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;

    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->adapter:Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;

    invoke-direct {v3, v0}, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;-><init>(Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;)V

    .line 65
    .local v3, "decor":Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->rvExtrato:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;I)V

    .line 66
    return-void
.end method

.method public static novaInstancia(ZLcom/itau/empresas/feature/extrato/FiltroListener;Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;)Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;
    .registers 5
    .param p0, "isFuturo"    # Z
    .param p1, "listener"    # Lcom/itau/empresas/feature/extrato/FiltroListener;
    .param p2, "listenerExtrato"    # Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;

    .line 30
    invoke-static {}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment_;->builder()Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;

    move-result-object v1

    .line 31
    .local v1, "fragment":Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;
    iput-boolean p0, v1, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->abaFuturo:Z

    .line 32
    iput-object p1, v1, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->filtroListener:Lcom/itau/empresas/feature/extrato/FiltroListener;

    .line 33
    iput-object p2, v1, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->listenerTextoBusca:Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;

    .line 34
    return-object v1
.end method


# virtual methods
.method public afterViews()V
    .registers 1

    .line 39
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->montarAdapter()V

    .line 40
    return-void
.end method

.method public filtrar(Ljava/lang/String;)V
    .registers 3
    .param p1, "texto"    # Ljava/lang/String;

    .line 52
    if-eqz p1, :cond_b

    .line 53
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->adapter:Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 54
    :cond_b
    return-void
.end method

.method public onClick()V
    .registers 2

    .line 58
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->filtroListener:Lcom/itau/empresas/feature/extrato/FiltroListener;

    invoke-interface {v0}, Lcom/itau/empresas/feature/extrato/FiltroListener;->aoClicaNoFiltro()V

    .line 59
    return-void
.end method

.method public setDados(Ljava/util/List;I)V
    .registers 3
    .param p1, "lancamentos"    # Ljava/util/List;
    .param p2, "ordem"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/LancamentoVO;>;I)V"
        }
    .end annotation

    .line 48
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->inicializaAdapter(Ljava/util/List;I)V

    .line 49
    return-void
.end method

.method public setTextoBuscado(Ljava/lang/String;)V
    .registers 3
    .param p1, "textoBuscado"    # Ljava/lang/String;

    .line 43
    iput-object p1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->textoBuscado:Ljava/lang/String;

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragment;->adapter:Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->setTextoBuscado(Ljava/lang/String;)V

    .line 45
    return-void
.end method

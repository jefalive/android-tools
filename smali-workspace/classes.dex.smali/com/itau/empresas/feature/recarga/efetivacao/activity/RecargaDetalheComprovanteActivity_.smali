.class public final Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;
.super Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;
.source "RecargaDetalheComprovanteActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 39
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;-><init>()V

    .line 43
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;

    .line 39
    invoke-super {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;

    .line 39
    invoke-super {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 57
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 58
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 59
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 60
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->injectExtras_()V

    .line 61
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->afterInject()V

    .line 62
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 133
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 134
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_2c

    .line 135
    const-string v0, "nome"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 136
    const-string v0, "nome"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->nome:Ljava/lang/String;

    .line 138
    :cond_1a
    const-string v0, "resultadoEfetivacaoRecargaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 139
    const-string v0, "resultadoEfetivacaoRecargaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->resultadoEfetivacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    .line 142
    :cond_2c
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 167
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_$4;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 175
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 155
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_$3;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 163
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 50
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->init_(Landroid/os/Bundle;)V

    .line 51
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 53
    const v0, 0x7f03005e

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->setContentView(I)V

    .line 54
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 5
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 96
    const v0, 0x7f0e02b0

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/WarningView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->warning:Lcom/itau/empresas/ui/view/WarningView;

    .line 97
    const v0, 0x7f0e02c2

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->llRoot:Landroid/widget/LinearLayout;

    .line 98
    const v0, 0x7f0e02c5

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->llCampoComprovante:Landroid/widget/LinearLayout;

    .line 99
    const v0, 0x7f0e02c6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->llCampoAutenticacao:Landroid/widget/LinearLayout;

    .line 100
    const v0, 0x7f0e02c4

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->textoDebitado:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0e0110

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->textoComprovante:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f0e02c7

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->textoAutenticacao:Landroid/widget/TextView;

    .line 103
    const v0, 0x7f0e02c3

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/efetivacao/DadosRecargaView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->dadosRecarga:Lcom/itau/empresas/feature/recarga/efetivacao/DadosRecargaView;

    .line 104
    const v0, 0x7f0e0096

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->scrollView:Landroid/widget/ScrollView;

    .line 105
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 106
    const v0, 0x7f0e02ae

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 107
    .local v1, "view_botao_salvar":Landroid/view/View;
    const v0, 0x7f0e01f0

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 109
    .local v2, "view_botao_compartilhar":Landroid/view/View;
    if-eqz v1, :cond_86

    .line 110
    new-instance v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_$1;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    :cond_86
    if-eqz v2, :cond_90

    .line 120
    new-instance v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_$2;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    :cond_90
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->carregaElementosDaTela()V

    .line 130
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 66
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->setContentView(I)V

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 68
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 78
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->setContentView(Landroid/view/View;)V

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 80
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 72
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 74
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 146
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->setIntent(Landroid/content/Intent;)V

    .line 147
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_;->injectExtras_()V

    .line 148
    return-void
.end method

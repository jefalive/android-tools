.class public Lcom/itau/empresas/feature/home/menu/SubMenuActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "SubMenuActivity.java"


# instance fields
.field private root:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 13
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    return-void
.end method

.method public static intent(Landroid/content/Context;Lcom/itau/empresas/api/model/MenuVO;)Landroid/content/Intent;
    .registers 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "menu"    # Lcom/itau/empresas/api/model/MenuVO;

    .line 38
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/itau/empresas/feature/home/menu/SubMenuActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 39
    .local v1, "intent":Landroid/content/Intent;
    const-string v0, "menuItem"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 40
    return-object v1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 22
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/home/menu/SubMenuActivity;->root:Landroid/widget/FrameLayout;

    .line 24
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/SubMenuActivity;->root:Landroid/widget/FrameLayout;

    const v1, 0x7f0e0191

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setId(I)V

    .line 25
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/SubMenuActivity;->root:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/menu/SubMenuActivity;->setContentView(Landroid/view/View;)V

    .line 27
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/menu/SubMenuActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 28
    .local v2, "extras":Landroid/os/Bundle;
    if-eqz v2, :cond_48

    const-string v0, "menuItem"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_48

    .line 29
    const-string v0, "menuItem"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/itau/empresas/api/model/MenuVO;

    .line 30
    .local v3, "menu":Lcom/itau/empresas/api/model/MenuVO;
    invoke-static {v3}, Lcom/itau/empresas/feature/home/menu/SubMenuFragment;->newInstance(Lcom/itau/empresas/api/model/MenuVO;)Lcom/itau/empresas/feature/home/menu/SubMenuFragment;

    move-result-object v4

    .line 31
    .local v4, "subMenuFragment":Lcom/itau/empresas/feature/home/menu/SubMenuFragment;
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/menu/SubMenuActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 32
    const v1, 0x7f0e0191

    invoke-virtual {v0, v1, v4}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 35
    .end local v3    # "menu":Lcom/itau/empresas/api/model/MenuVO;
    .end local v4    # "subMenuFragment":Lcom/itau/empresas/feature/home/menu/SubMenuFragment;
    :cond_48
    return-void
.end method

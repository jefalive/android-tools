.class public Lnet/hockeyapp/android/utils/ViewHelper;
.super Ljava/lang/Object;
.source "ViewHelper.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getGradient()Landroid/graphics/drawable/Drawable;
    .registers 4

    .line 37
    const/4 v0, 0x2

    new-array v2, v0, [I

    fill-array-data v2, :array_e

    .line 38
    .local v2, "colors":[I
    new-instance v3, Landroid/graphics/drawable/GradientDrawable;

    sget-object v0, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    invoke-direct {v3, v0, v2}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    .line 39
    .local v3, "gradient":Landroid/graphics/drawable/GradientDrawable;
    return-object v3

    :array_e
    .array-data 4
        -0x1000000
        0x0
    .end array-data
.end method

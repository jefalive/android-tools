.class public Lcom/itau/empresas/ui/view/TextoEditavel;
.super Lbr/com/itau/widgets/material/MaterialEditText;
.source "TextoEditavel.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;

    .line 13
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/MaterialEditText;-><init>(Landroid/content/Context;)V

    .line 25
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/TextoEditavel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0035

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/TextoEditavel;->setTextColor(I)V

    .line 14
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 17
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/material/MaterialEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/TextoEditavel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0035

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/TextoEditavel;->setTextColor(I)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 21
    invoke-direct {p0, p1, p2, p3}, Lbr/com/itau/widgets/material/MaterialEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/TextoEditavel;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0035

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/TextoEditavel;->setTextColor(I)V

    .line 22
    return-void
.end method

.class public Lcom/google/android/gms/ads/internal/purchase/zzh;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/ads/internal/purchase/zzh$zza;
    }
.end annotation


# static fields
.field private static final zzFV:Ljava/lang/String;

.field private static zzFX:Lcom/google/android/gms/ads/internal/purchase/zzh;

.field private static final zzpV:Ljava/lang/Object;


# instance fields
.field private final zzFW:Lcom/google/android/gms/ads/internal/purchase/zzh$zza;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "CREATE TABLE IF NOT EXISTS %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, %s TEXT NOT NULL, %s TEXT NOT NULL, %s INTEGER)"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "InAppPurchase"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "purchase_id"

    const/4 v4, 0x1

    aput-object v3, v2, v4

    const-string v3, "product_id"

    const/4 v4, 0x2

    aput-object v3, v2, v4

    const-string v3, "developer_payload"

    const/4 v4, 0x3

    aput-object v3, v2, v4

    const-string v3, "record_time"

    const/4 v4, 0x4

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/internal/purchase/zzh;->zzFV:Ljava/lang/String;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/ads/internal/purchase/zzh;->zzpV:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .registers 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/ads/internal/purchase/zzh$zza;

    const-string v1, "google_inapp_purchase.db"

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/gms/ads/internal/purchase/zzh$zza;-><init>(Lcom/google/android/gms/ads/internal/purchase/zzh;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zzh;->zzFW:Lcom/google/android/gms/ads/internal/purchase/zzh$zza;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .registers 1

    sget-object v0, Lcom/google/android/gms/ads/internal/purchase/zzh;->zzFV:Ljava/lang/String;

    return-object v0
.end method

.method public static zzy(Landroid/content/Context;)Lcom/google/android/gms/ads/internal/purchase/zzh;
    .registers 4

    sget-object v1, Lcom/google/android/gms/ads/internal/purchase/zzh;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/google/android/gms/ads/internal/purchase/zzh;->zzFX:Lcom/google/android/gms/ads/internal/purchase/zzh;

    if-nez v0, :cond_e

    new-instance v0, Lcom/google/android/gms/ads/internal/purchase/zzh;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/internal/purchase/zzh;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/ads/internal/purchase/zzh;->zzFX:Lcom/google/android/gms/ads/internal/purchase/zzh;

    :cond_e
    sget-object v0, Lcom/google/android/gms/ads/internal/purchase/zzh;->zzFX:Lcom/google/android/gms/ads/internal/purchase/zzh;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    monitor-exit v1

    return-object v0

    :catchall_12
    move-exception v2

    monitor-exit v1

    throw v2
.end method


# virtual methods
.method public getRecordCount()I
    .registers 9

    sget-object v2, Lcom/google/android/gms/ads/internal/purchase/zzh;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/purchase/zzh;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_58

    move-result-object v3

    if-nez v3, :cond_c

    monitor-exit v2

    const/4 v0, 0x0

    return v0

    :cond_c
    const/4 v4, 0x0

    const-string v0, "select count(*) from InAppPurchase"

    const/4 v1, 0x0

    :try_start_10
    invoke-virtual {v3, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    move-object v4, v0

    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_27

    const/4 v0, 0x0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_10 .. :try_end_1f} :catch_2d
    .catchall {:try_start_10 .. :try_end_1f} :catchall_4e

    move-result v5

    if-eqz v4, :cond_25

    :try_start_22
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_25
    .catchall {:try_start_22 .. :try_end_25} :catchall_58

    :cond_25
    monitor-exit v2

    return v5

    :cond_27
    if-eqz v4, :cond_55

    :try_start_29
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_2c
    .catchall {:try_start_29 .. :try_end_2c} :catchall_58

    goto :goto_55

    :catch_2d
    move-exception v5

    :try_start_2e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error getting record count"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V
    :try_end_48
    .catchall {:try_start_2e .. :try_end_48} :catchall_4e

    if-eqz v4, :cond_55

    :try_start_4a
    invoke-interface {v4}, Landroid/database/Cursor;->close()V
    :try_end_4d
    .catchall {:try_start_4a .. :try_end_4d} :catchall_58

    goto :goto_55

    :catchall_4e
    move-exception v6

    if-eqz v4, :cond_54

    :try_start_51
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_54
    throw v6
    :try_end_55
    .catchall {:try_start_51 .. :try_end_55} :catchall_58

    :cond_55
    :goto_55
    monitor-exit v2

    const/4 v0, 0x0

    return v0

    :catchall_58
    move-exception v7

    monitor-exit v2

    throw v7
.end method

.method public getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .registers 4

    const/4 v1, 0x0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/zzh;->zzFW:Lcom/google/android/gms/ads/internal/purchase/zzh$zza;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/purchase/zzh$zza;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_6
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_6} :catch_8

    move-result-object v1

    goto :goto_10

    :catch_8
    move-exception v2

    const-string v0, "Error opening writable conversion tracking database"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0

    :goto_10
    return-object v1
.end method

.method public zza(Landroid/database/Cursor;)Lcom/google/android/gms/ads/internal/purchase/zzf;
    .registers 7

    if-nez p1, :cond_4

    const/4 v0, 0x0

    return-object v0

    :cond_4
    new-instance v0, Lcom/google/android/gms/ads/internal/purchase/zzf;

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    const/4 v3, 0x1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/ads/internal/purchase/zzf;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public zza(Lcom/google/android/gms/ads/internal/purchase/zzf;)V
    .registers 11

    if-nez p1, :cond_3

    return-void

    :cond_3
    sget-object v5, Lcom/google/android/gms/ads/internal/purchase/zzh;->zzpV:Ljava/lang/Object;

    monitor-enter v5

    :try_start_6
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/purchase/zzh;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_9
    .catchall {:try_start_6 .. :try_end_9} :catchall_2f

    move-result-object v6

    if-nez v6, :cond_e

    monitor-exit v5

    return-void

    :cond_e
    :try_start_e
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s = %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "purchase_id"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-wide v3, p1, Lcom/google/android/gms/ads/internal/purchase/zzf;->zzFP:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const-string v0, "InAppPurchase"

    const/4 v1, 0x0

    invoke-virtual {v6, v0, v7, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2d
    .catchall {:try_start_e .. :try_end_2d} :catchall_2f

    monitor-exit v5

    goto :goto_32

    :catchall_2f
    move-exception v8

    monitor-exit v5

    throw v8

    :goto_32
    return-void
.end method

.method public zzb(Lcom/google/android/gms/ads/internal/purchase/zzf;)V
    .registers 10

    if-nez p1, :cond_3

    return-void

    :cond_3
    sget-object v4, Lcom/google/android/gms/ads/internal/purchase/zzh;->zzpV:Ljava/lang/Object;

    monitor-enter v4

    :try_start_6
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/purchase/zzh;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_9
    .catchall {:try_start_6 .. :try_end_9} :catchall_47

    move-result-object v5

    if-nez v5, :cond_e

    monitor-exit v4

    return-void

    :cond_e
    :try_start_e
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    const-string v0, "product_id"

    iget-object v1, p1, Lcom/google/android/gms/ads/internal/purchase/zzf;->zzFR:Ljava/lang/String;

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "developer_payload"

    iget-object v1, p1, Lcom/google/android/gms/ads/internal/purchase/zzf;->zzFQ:Ljava/lang/String;

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "record_time"

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "InAppPurchase"

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1, v6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    iput-wide v0, p1, Lcom/google/android/gms/ads/internal/purchase/zzf;->zzFP:J

    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/purchase/zzh;->getRecordCount()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x4e20

    cmp-long v0, v0, v2

    if-lez v0, :cond_45

    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/purchase/zzh;->zzfY()V
    :try_end_45
    .catchall {:try_start_e .. :try_end_45} :catchall_47

    :cond_45
    monitor-exit v4

    goto :goto_4a

    :catchall_47
    move-exception v7

    monitor-exit v4

    throw v7

    :goto_4a
    return-void
.end method

.method public zzfY()V
    .registers 16

    sget-object v9, Lcom/google/android/gms/ads/internal/purchase/zzh;->zzpV:Ljava/lang/Object;

    monitor-enter v9

    :try_start_3
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/purchase/zzh;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_5d

    move-result-object v10

    if-nez v10, :cond_b

    monitor-exit v9

    return-void

    :cond_b
    const/4 v11, 0x0

    const-string v12, "record_time ASC"

    move-object v0, v10

    const-string v1, "InAppPurchase"

    move-object v7, v12

    const-string v8, "1"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    :try_start_19
    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    move-object v11, v0

    if-eqz v11, :cond_2d

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2d

    invoke-virtual {p0, v11}, Lcom/google/android/gms/ads/internal/purchase/zzh;->zza(Landroid/database/Cursor;)Lcom/google/android/gms/ads/internal/purchase/zzf;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/purchase/zzh;->zza(Lcom/google/android/gms/ads/internal/purchase/zzf;)V
    :try_end_2d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_19 .. :try_end_2d} :catch_33
    .catchall {:try_start_19 .. :try_end_2d} :catchall_54

    :cond_2d
    if-eqz v11, :cond_5b

    :try_start_2f
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_32
    .catchall {:try_start_2f .. :try_end_32} :catchall_5d

    goto :goto_5b

    :catch_33
    move-exception v12

    :try_start_34
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error remove oldest record"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V
    :try_end_4e
    .catchall {:try_start_34 .. :try_end_4e} :catchall_54

    if-eqz v11, :cond_5b

    :try_start_50
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_53
    .catchall {:try_start_50 .. :try_end_53} :catchall_5d

    goto :goto_5b

    :catchall_54
    move-exception v13

    if-eqz v11, :cond_5a

    :try_start_57
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_5a
    throw v13
    :try_end_5b
    .catchall {:try_start_57 .. :try_end_5b} :catchall_5d

    :cond_5b
    :goto_5b
    monitor-exit v9

    goto :goto_60

    :catchall_5d
    move-exception v14

    monitor-exit v9

    throw v14

    :goto_60
    return-void
.end method

.method public zzg(J)Ljava/util/List;
    .registers 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)Ljava/util/List<Lcom/google/android/gms/ads/internal/purchase/zzf;>;"
        }
    .end annotation

    sget-object v9, Lcom/google/android/gms/ads/internal/purchase/zzh;->zzpV:Ljava/lang/Object;

    monitor-enter v9

    :try_start_3
    new-instance v10, Ljava/util/LinkedList;

    invoke-direct {v10}, Ljava/util/LinkedList;-><init>()V
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_72

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_10

    monitor-exit v9

    return-object v10

    :cond_10
    :try_start_10
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/ads/internal/purchase/zzh;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_13
    .catchall {:try_start_10 .. :try_end_13} :catchall_72

    move-result-object v11

    if-nez v11, :cond_18

    monitor-exit v9

    return-object v10

    :cond_18
    const/4 v12, 0x0

    const-string v13, "record_time ASC"

    move-object v0, v11

    const-string v1, "InAppPurchase"

    move-object v7, v13

    :try_start_1f
    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    move-object v12, v0

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_42

    :cond_33
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/google/android/gms/ads/internal/purchase/zzh;->zza(Landroid/database/Cursor;)Lcom/google/android/gms/ads/internal/purchase/zzf;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_3f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1f .. :try_end_3f} :catch_48
    .catchall {:try_start_1f .. :try_end_3f} :catchall_69

    move-result v0

    if-nez v0, :cond_33

    :cond_42
    if-eqz v12, :cond_70

    :try_start_44
    invoke-interface {v12}, Landroid/database/Cursor;->close()V
    :try_end_47
    .catchall {:try_start_44 .. :try_end_47} :catchall_72

    goto :goto_70

    :catch_48
    move-exception v13

    :try_start_49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error extracing purchase info: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V
    :try_end_63
    .catchall {:try_start_49 .. :try_end_63} :catchall_69

    if-eqz v12, :cond_70

    :try_start_65
    invoke-interface {v12}, Landroid/database/Cursor;->close()V
    :try_end_68
    .catchall {:try_start_65 .. :try_end_68} :catchall_72

    goto :goto_70

    :catchall_69
    move-exception v14

    if-eqz v12, :cond_6f

    :try_start_6c
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_6f
    throw v14
    :try_end_70
    .catchall {:try_start_6c .. :try_end_70} :catchall_72

    :cond_70
    :goto_70
    monitor-exit v9

    return-object v10

    :catchall_72
    move-exception v15

    monitor-exit v9

    throw v15
.end method

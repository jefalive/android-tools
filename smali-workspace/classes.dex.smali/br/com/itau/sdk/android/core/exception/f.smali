.class public Lbr/com/itau/sdk/android/core/exception/f;
.super Ljava/lang/RuntimeException;
.source "SourceFile"


# static fields
.field private static final a:[B

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x11

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/exception/f;->a:[B

    const/16 v0, 0x63

    sput v0, Lbr/com/itau/sdk/android/core/exception/f;->b:I

    return-void

    :array_e
    .array-data 1
        0x17t
        0x7et
        -0x64t
        0x6bt
        -0x9t
        0x8t
        0x4t
        -0x13t
        0x5t
        -0x3t
        0x7t
        0xct
        -0xct
        0x14t
        -0x3t
        0x4t
        -0xbt
    .end array-data
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    const/4 v4, -0x1

    mul-int/lit8 p0, p0, 0x4

    add-int/lit8 p0, p0, 0x4

    add-int/lit8 p1, p1, 0x4

    sget-object v5, Lbr/com/itau/sdk/android/core/exception/f;->a:[B

    new-instance v0, Ljava/lang/String;

    add-int/lit8 p2, p2, 0x62

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v5, :cond_1a

    move v2, p2

    move v3, p0

    :goto_15
    add-int/2addr v2, v3

    add-int/lit8 p1, p1, 0x1

    add-int/lit8 p2, v2, 0x1

    :cond_1a
    add-int/lit8 v4, v4, 0x1

    int-to-byte v2, p2

    aput-byte v2, v1, v4

    if-ne v4, p0, :cond_26

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_26
    move v2, p2

    aget-byte v3, v5, p1

    goto :goto_15
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .registers 7

    .line 16
    new-instance v4, Lcom/google/gson/JsonObject;

    invoke-direct {v4}, Lcom/google/gson/JsonObject;-><init>()V

    .line 17
    sget v0, Lbr/com/itau/sdk/android/core/exception/f;->b:I

    and-int/lit8 v0, v0, 0x5

    add-int/lit8 v1, v0, -0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/exception/f;->a:[B

    const/16 v3, 0x10

    aget-byte v2, v2, v3

    neg-int v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/exception/f;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0, p0}, Lcom/google/gson/JsonObject;->addProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    new-instance v5, Lcom/google/gson/JsonObject;

    invoke-direct {v5}, Lcom/google/gson/JsonObject;-><init>()V

    .line 19
    sget-object v0, Lbr/com/itau/sdk/android/core/exception/f;->a:[B

    const/16 v1, 0xa

    aget-byte v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lbr/com/itau/sdk/android/core/exception/f;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0, v4}, Lcom/google/gson/JsonObject;->add(Ljava/lang/String;Lcom/google/gson/JsonElement;)V

    .line 20
    sget-object v0, Lbr/com/itau/sdk/android/core/exception/f;->a:[B

    const/4 v1, 0x4

    aget-byte v0, v0, v1

    neg-int v0, v0

    const/4 v1, 0x0

    const/16 v2, 0xa

    invoke-static {v1, v2, v0}, Lbr/com/itau/sdk/android/core/exception/f;->a(III)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->UNEXPECTED:Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException$Kind;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lcom/google/gson/JsonObject;->addProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    invoke-virtual {v5}, Lcom/google/gson/JsonObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

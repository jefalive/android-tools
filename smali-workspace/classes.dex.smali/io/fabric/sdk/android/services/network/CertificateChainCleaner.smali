.class final Lio/fabric/sdk/android/services/network/CertificateChainCleaner;
.super Ljava/lang/Object;
.source "CertificateChainCleaner.java"


# direct methods
.method public static getCleanChain([Ljava/security/cert/X509Certificate;Lio/fabric/sdk/android/services/network/SystemKeyStore;)[Ljava/security/cert/X509Certificate;
    .registers 8
    .param p0, "chain"    # [Ljava/security/cert/X509Certificate;
    .param p1, "systemKeyStore"    # Lio/fabric/sdk/android/services/network/SystemKeyStore;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .line 40
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 41
    .local v2, "cleanChain":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/security/cert/X509Certificate;>;"
    const/4 v3, 0x0

    .line 44
    .local v3, "trustedChain":Z
    const/4 v0, 0x0

    aget-object v0, p0, v0

    invoke-virtual {p1, v0}, Lio/fabric/sdk/android/services/network/SystemKeyStore;->isTrustRoot(Ljava/security/cert/X509Certificate;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 45
    const/4 v3, 0x1

    .line 48
    :cond_10
    const/4 v0, 0x0

    aget-object v0, p0, v0

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 50
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_17
    array-length v0, p0

    if-ge v4, v0, :cond_37

    .line 51
    aget-object v0, p0, v4

    invoke-virtual {p1, v0}, Lio/fabric/sdk/android/services/network/SystemKeyStore;->isTrustRoot(Ljava/security/cert/X509Certificate;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 52
    const/4 v3, 0x1

    .line 55
    :cond_23
    aget-object v0, p0, v4

    add-int/lit8 v1, v4, -0x1

    aget-object v1, p0, v1

    invoke-static {v0, v1}, Lio/fabric/sdk/android/services/network/CertificateChainCleaner;->isValidLink(Ljava/security/cert/X509Certificate;Ljava/security/cert/X509Certificate;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 56
    aget-object v0, p0, v4

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 50
    add-int/lit8 v4, v4, 0x1

    goto :goto_17

    .line 62
    :cond_37
    add-int/lit8 v0, v4, -0x1

    aget-object v0, p0, v0

    invoke-virtual {p1, v0}, Lio/fabric/sdk/android/services/network/SystemKeyStore;->getTrustRootFor(Ljava/security/cert/X509Certificate;)Ljava/security/cert/X509Certificate;

    move-result-object v5

    .line 64
    .local v5, "trustRoot":Ljava/security/cert/X509Certificate;
    if-eqz v5, :cond_45

    .line 65
    invoke-virtual {v2, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 66
    const/4 v3, 0x1

    .line 69
    :cond_45
    if-eqz v3, :cond_54

    .line 70
    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/security/cert/X509Certificate;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/security/cert/X509Certificate;

    return-object v0

    .line 72
    :cond_54
    new-instance v0, Ljava/security/cert/CertificateException;

    const-string v1, "Didn\'t find a trust anchor in chain cleanup!"

    invoke-direct {v0, v1}, Ljava/security/cert/CertificateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static isValidLink(Ljava/security/cert/X509Certificate;Ljava/security/cert/X509Certificate;)Z
    .registers 5
    .param p0, "parent"    # Ljava/security/cert/X509Certificate;
    .param p1, "child"    # Ljava/security/cert/X509Certificate;

    .line 77
    invoke-virtual {p0}, Ljava/security/cert/X509Certificate;->getSubjectX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v0

    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getIssuerX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljavax/security/auth/x500/X500Principal;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 78
    const/4 v0, 0x0

    return v0

    .line 82
    :cond_10
    :try_start_10
    invoke-virtual {p0}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/security/cert/X509Certificate;->verify(Ljava/security/PublicKey;)V
    :try_end_17
    .catch Ljava/security/GeneralSecurityException; {:try_start_10 .. :try_end_17} :catch_18

    .line 85
    goto :goto_1b

    .line 83
    :catch_18
    move-exception v2

    .line 84
    .local v2, "gse":Ljava/security/GeneralSecurityException;
    const/4 v0, 0x0

    return v0

    .line 87
    .end local v2    # "gse":Ljava/security/GeneralSecurityException;
    :goto_1b
    const/4 v0, 0x1

    return v0
.end method

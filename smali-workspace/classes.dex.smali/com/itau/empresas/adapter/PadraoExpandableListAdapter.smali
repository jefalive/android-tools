.class public Lcom/itau/empresas/adapter/PadraoExpandableListAdapter;
.super Landroid/widget/BaseExpandableListAdapter;
.source "PadraoExpandableListAdapter.java"


# instance fields
.field private context:Landroid/content/Context;

.field private listaDetalhe:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
        }
    .end annotation
.end field

.field private listaTitulo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Ljava/util/HashMap;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listaTitulo"    # Ljava/util/List;
    .param p3, "listaDetalhe"    # Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;Ljava/util/List<Ljava/lang/String;>;Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/itau/empresas/adapter/PadraoExpandableListAdapter;->context:Landroid/content/Context;

    .line 25
    iput-object p2, p0, Lcom/itau/empresas/adapter/PadraoExpandableListAdapter;->listaTitulo:Ljava/util/List;

    .line 26
    iput-object p3, p0, Lcom/itau/empresas/adapter/PadraoExpandableListAdapter;->listaDetalhe:Ljava/util/HashMap;

    .line 27
    return-void
.end method


# virtual methods
.method public bridge synthetic getChild(II)Ljava/lang/Object;
    .registers 4

    .line 16
    invoke-virtual {p0, p1, p2}, Lcom/itau/empresas/adapter/PadraoExpandableListAdapter;->getChild(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getChild(II)Ljava/lang/String;
    .registers 5
    .param p1, "groupPosition"    # I
    .param p2, "childPosititon"    # I

    .line 31
    iget-object v0, p0, Lcom/itau/empresas/adapter/PadraoExpandableListAdapter;->listaDetalhe:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/itau/empresas/adapter/PadraoExpandableListAdapter;->listaTitulo:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getChildId(II)J
    .registers 5
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .line 36
    int-to-long v0, p2

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 11
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I
    .param p3, "isLastChild"    # Z
    .param p4, "convertView"    # Landroid/view/View;
    .param p5, "parent"    # Landroid/view/ViewGroup;

    .line 43
    move-object v2, p4

    .line 45
    .local v2, "cv":Landroid/view/View;
    invoke-virtual {p0, p1, p2}, Lcom/itau/empresas/adapter/PadraoExpandableListAdapter;->getChild(II)Ljava/lang/String;

    move-result-object v3

    .line 46
    .local v3, "expandedListText":Ljava/lang/String;
    if-nez v2, :cond_1a

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/adapter/PadraoExpandableListAdapter;->context:Landroid/content/Context;

    const-string v1, "layout_inflater"

    .line 48
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/view/LayoutInflater;

    .line 49
    .local v4, "layoutInflater":Landroid/view/LayoutInflater;
    const v0, 0x7f0300e1

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 51
    .line 52
    .end local v4    # "layoutInflater":Landroid/view/LayoutInflater;
    :cond_1a
    const v0, 0x7f0e04f5

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/TextView;

    .line 53
    .local v4, "expandedListTextView":Landroid/widget/TextView;
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    return-object v2
.end method

.method public getChildrenCount(I)I
    .registers 4
    .param p1, "groupPosition"    # I

    .line 59
    iget-object v0, p0, Lcom/itau/empresas/adapter/PadraoExpandableListAdapter;->listaDetalhe:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/itau/empresas/adapter/PadraoExpandableListAdapter;->listaTitulo:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getGroup(I)Ljava/lang/Object;
    .registers 3
    .param p1, "groupPosition"    # I

    .line 64
    iget-object v0, p0, Lcom/itau/empresas/adapter/PadraoExpandableListAdapter;->listaTitulo:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getGroupCount()I
    .registers 2

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/adapter/PadraoExpandableListAdapter;->listaTitulo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getGroupId(I)J
    .registers 4
    .param p1, "groupPosition"    # I

    .line 74
    int-to-long v0, p1

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 11
    .param p1, "groupPosition"    # I
    .param p2, "isExpanded"    # Z
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .line 81
    move-object v2, p3

    .line 83
    .local v2, "cv":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/itau/empresas/adapter/PadraoExpandableListAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/String;

    .line 84
    .local v3, "listTitle":Ljava/lang/String;
    if-nez v2, :cond_1d

    .line 85
    iget-object v0, p0, Lcom/itau/empresas/adapter/PadraoExpandableListAdapter;->context:Landroid/content/Context;

    const-string v1, "layout_inflater"

    .line 86
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/view/LayoutInflater;

    .line 87
    .local v4, "layoutInflater":Landroid/view/LayoutInflater;
    const v0, 0x7f0300e2

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 89
    .line 90
    .end local v4    # "layoutInflater":Landroid/view/LayoutInflater;
    :cond_1d
    const v0, 0x7f0e04f6

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/TextView;

    .line 91
    .local v4, "listTitleTextView":Landroid/widget/TextView;
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    .line 94
    const v0, 0x7f0e04ef

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 95
    .local v5, "imgSeta":Lcom/itau/empresas/ui/view/TextViewIcon;
    if-nez p2, :cond_44

    iget-object v0, p0, Lcom/itau/empresas/adapter/PadraoExpandableListAdapter;->context:Landroid/content/Context;

    .line 96
    const v1, 0x7f070286

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_51

    :cond_44
    iget-object v0, p0, Lcom/itau/empresas/adapter/PadraoExpandableListAdapter;->context:Landroid/content/Context;

    .line 97
    const v1, 0x7f070288

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 95
    :goto_51
    invoke-virtual {v5, v0}, Lcom/itau/empresas/ui/view/TextViewIcon;->setText(Ljava/lang/CharSequence;)V

    .line 99
    return-object v2
.end method

.method public hasStableIds()Z
    .registers 2

    .line 104
    const/4 v0, 0x0

    return v0
.end method

.method public isChildSelectable(II)Z
    .registers 4
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .line 109
    const/4 v0, 0x1

    return v0
.end method

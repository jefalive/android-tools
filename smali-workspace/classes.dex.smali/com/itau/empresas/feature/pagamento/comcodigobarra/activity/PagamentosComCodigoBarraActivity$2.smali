.class Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$2;
.super Ljava/lang/Object;
.source "PagamentosComCodigoBarraActivity.java"

# interfaces
.implements Lbr/com/itau/widgets/hintview/OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->adicionaHint(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;

    .line 211
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$2;->this$0:Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss()V
    .registers 5

    .line 215
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$2;->this$0:Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;

    # getter for: Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->fechouHintComClique:Z
    invoke-static {v0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->access$000(Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 216
    return-void

    .line 219
    :cond_9
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$2;->this$0:Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;

    # getter for: Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->hintUtils:Lcom/itau/empresas/ui/util/HintUtils;
    invoke-static {v0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->access$100(Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;)Lcom/itau/empresas/ui/util/HintUtils;

    move-result-object v0

    const-string v1, "CHVPAG-QTD"

    .line 220
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/HintUtils;->consultarPreferenciasQuantidade(Ljava/lang/String;)I

    move-result v3

    .line 222
    .local v3, "quantidade":I
    const/4 v0, 0x1

    if-le v3, v0, :cond_25

    .line 223
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$2;->this$0:Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;

    # getter for: Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->hintUtils:Lcom/itau/empresas/ui/util/HintUtils;
    invoke-static {v0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->access$100(Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;)Lcom/itau/empresas/ui/util/HintUtils;

    move-result-object v0

    const-string v1, "CHVPAG-ID"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/HintUtils;->desativarHint(Ljava/lang/String;Z)V

    .line 224
    return-void

    .line 226
    :cond_25
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity$2;->this$0:Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;

    # getter for: Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->hintUtils:Lcom/itau/empresas/ui/util/HintUtils;
    invoke-static {v0}, Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;->access$100(Lcom/itau/empresas/feature/pagamento/comcodigobarra/activity/PagamentosComCodigoBarraActivity;)Lcom/itau/empresas/ui/util/HintUtils;

    move-result-object v0

    const-string v1, "CHVPAG-QTD"

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v1, v3}, Lcom/itau/empresas/ui/util/HintUtils;->registraAcesso(Ljava/lang/String;I)V

    .line 227
    return-void
.end method

.class public final Lcom/itau/empresas/Evento$EventoBuscaContatosRecarga;
.super Ljava/lang/Object;
.source "Evento.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/Evento;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EventoBuscaContatosRecarga"
.end annotation


# instance fields
.field private contatosRecarga:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getContatosRecarga()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;>;"
        }
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/Evento$EventoBuscaContatosRecarga;->contatosRecarga:Ljava/util/List;

    return-object v0
.end method

.method public setContatosRecarga(Ljava/util/List;)V
    .registers 2
    .param p1, "contatosRecarga"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;>;)V"
        }
    .end annotation

    .line 84
    iput-object p1, p0, Lcom/itau/empresas/Evento$EventoBuscaContatosRecarga;->contatosRecarga:Ljava/util/List;

    .line 85
    return-void
.end method

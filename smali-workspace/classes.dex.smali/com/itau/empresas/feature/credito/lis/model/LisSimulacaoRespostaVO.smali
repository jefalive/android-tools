.class public Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;
.super Ljava/lang/Object;
.source "LisSimulacaoRespostaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private agencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia"
    .end annotation
.end field

.field private codigoProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_produto"
    .end annotation
.end field

.field private conta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta"
    .end annotation
.end field

.field private dataVencimentoOperacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_vencimento_operacao"
    .end annotation
.end field

.field private descricaoProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_produto"
    .end annotation
.end field

.field private diaPagamento:Ljava/lang/Integer;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dia_pagamento"
    .end annotation
.end field

.field private digitoConta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_conta"
    .end annotation
.end field

.field private moeda:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "moeda"
    .end annotation
.end field

.field private percentualCetIof:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "percentual_cet_iof"
    .end annotation
.end field

.field private percentualCetTarifaContratacao:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "percentual_cet_tarifa_contratacao"
    .end annotation
.end field

.field private percentualCetTotalOperacao:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "percentual_cet_total_operacao"
    .end annotation
.end field

.field private percentualCetValorLimite:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "percentual_cet_valor_limite"
    .end annotation
.end field

.field private periodicidadeCapitalizacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "periodicidade_capitalizacao"
    .end annotation
.end field

.field private taxaCetAno:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_cet_ano"
    .end annotation
.end field

.field private taxaCetMes:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_cet_mes"
    .end annotation
.end field

.field private taxaJurosAno:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros_ano"
    .end annotation
.end field

.field private taxaJurosMes:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros_mes"
    .end annotation
.end field

.field private textoLimiteAdicional:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "texto_limite_adicional"
    .end annotation
.end field

.field private valorIof:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_iof"
    .end annotation
.end field

.field private valorLimiteAdicional:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_limite_adicional"
    .end annotation
.end field

.field private valorLimiteContratado:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_limite_contratado"
    .end annotation
.end field

.field private valorTarifaContratacao:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_tarifa_contratacao"
    .end annotation
.end field

.field private valorTotalOperacao:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_total_operacao"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDataVencimentoOperacao()Ljava/lang/String;
    .registers 2

    .line 160
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->dataVencimentoOperacao:Ljava/lang/String;

    return-object v0
.end method

.method public getDiaPagamento()Ljava/lang/Integer;
    .registers 2

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->diaPagamento:Ljava/lang/Integer;

    return-object v0
.end method

.method public getPercentualCetIof()F
    .registers 2

    .line 128
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->percentualCetIof:F

    return v0
.end method

.method public getPercentualCetTarifaContratacao()F
    .registers 2

    .line 120
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->percentualCetTarifaContratacao:F

    return v0
.end method

.method public getPercentualCetTotalOperacao()F
    .registers 2

    .line 136
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->percentualCetTotalOperacao:F

    return v0
.end method

.method public getPeriodicidadeCapitalizacao()Ljava/lang/String;
    .registers 2

    .line 156
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->periodicidadeCapitalizacao:Ljava/lang/String;

    return-object v0
.end method

.method public getTaxaCetAno()F
    .registers 2

    .line 152
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->taxaCetAno:F

    return v0
.end method

.method public getTaxaCetMes()F
    .registers 2

    .line 148
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->taxaCetMes:F

    return v0
.end method

.method public getTaxaJurosAno()F
    .registers 2

    .line 144
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->taxaJurosAno:F

    return v0
.end method

.method public getTaxaJurosMes()F
    .registers 2

    .line 140
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->taxaJurosMes:F

    return v0
.end method

.method public getTextoLimiteAdicional()Ljava/lang/String;
    .registers 2

    .line 164
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->textoLimiteAdicional:Ljava/lang/String;

    return-object v0
.end method

.method public getValorIof()F
    .registers 2

    .line 124
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->valorIof:F

    return v0
.end method

.method public getValorLimiteAdicional()F
    .registers 2

    .line 80
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->valorLimiteAdicional:F

    return v0
.end method

.method public getValorLimiteContratado()Ljava/math/BigDecimal;
    .registers 2

    .line 104
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->valorLimiteContratado:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getValorTarifaContratacao()F
    .registers 2

    .line 116
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->valorTarifaContratacao:F

    return v0
.end method

.method public getValorTotalOperacao()F
    .registers 2

    .line 132
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;->valorTotalOperacao:F

    return v0
.end method

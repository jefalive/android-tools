.class public Lcom/itau/empresas/ui/activity/BetaActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "BetaActivity.java"


# instance fields
.field application:Lcom/itau/empresas/CustomApplication;

.field botaoFaleSobreApp:Landroid/widget/Button;

.field isLogado:Z


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 28
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private abrirFeedbackDialog()Lcom/itau/empresas/ui/dialog/BaseDialog;
    .registers 2

    .line 77
    invoke-static {}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_;->builder()Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialogBeta;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    move-result-object v0

    return-object v0
.end method

.method public static chamaParticiparBetaActivity(Landroid/content/Context;)V
    .registers 3
    .param p0, "context"    # Landroid/content/Context;

    .line 71
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 72
    .local v1, "i":Landroid/content/Intent;
    const-string v0, "http://play.google.com/apps/testing/com.itau.empresas/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 73
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 74
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/BetaActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 81
    new-instance v0, Lcom/itau/empresas/ui/activity/BetaActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/BetaActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/ui/activity/BetaActivity_$IntentBuilder_;
    .registers 2
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;

    .line 85
    new-instance v0, Lcom/itau/empresas/ui/activity/BetaActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/BetaActivity_$IntentBuilder_;-><init>(Landroid/support/v4/app/Fragment;)V

    return-object v0
.end method


# virtual methods
.method faleSobreApp()V
    .registers 3

    .line 67
    invoke-static {}, Lcom/itau/empresas/feature/feedback/FeedbackDialog_;->builder()Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;->feedbackEspontaneo(Ljava/lang/Boolean;)Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/feedback/FeedbackDialog;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 68
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 48
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method participarBeta()V
    .registers 3

    .line 53
    invoke-static {p0}, Lcom/itau/empresas/ui/activity/BetaActivity;->chamaParticiparBetaActivity(Landroid/content/Context;)V

    .line 54
    const v0, 0x7f07032e

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/BetaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 55
    const v1, 0x7f07032f

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/BetaActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 54
    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/ui/activity/BetaActivity;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    return-void
.end method

.method sairBeta()V
    .registers 3

    .line 60
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BetaActivity;->abrirFeedbackDialog()Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 61
    const v0, 0x7f07032e

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/BetaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 62
    const v1, 0x7f070330

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/BetaActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 61
    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/ui/activity/BetaActivity;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method validarElementos()V
    .registers 3

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/BetaActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getOperador()Lcom/itau/empresas/api/model/OperadorVO;

    move-result-object v0

    if-nez v0, :cond_f

    .line 42
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/BetaActivity;->botaoFaleSobreApp:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 44
    :cond_f
    return-void
.end method

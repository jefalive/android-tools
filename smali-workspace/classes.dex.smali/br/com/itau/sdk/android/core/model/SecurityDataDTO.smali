.class public final Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private dispositivoNecessario:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dispositivoNecessario"
    .end annotation
.end field

.field private exibeMensagemTokeNaoNecessario:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "exibeMensagemTokeNaoNecessario"
    .end annotation
.end field

.field private senhaNecessaria:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "senhaNecessaria"
    .end annotation
.end field

.field private tokenAppData:Lbr/com/itau/sdk/android/core/model/TokenAppDataDTO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tokenAppData"
    .end annotation
.end field

.field private uiModel:Lbr/com/itau/sdk/android/core/model/UiModelDTO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "uiModel"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getTokenAppData()Lbr/com/itau/sdk/android/core/model/TokenAppDataDTO;
    .registers 2

    .line 37
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->tokenAppData:Lbr/com/itau/sdk/android/core/model/TokenAppDataDTO;

    return-object v0
.end method

.method public getUiModel()Lbr/com/itau/sdk/android/core/model/UiModelDTO;
    .registers 2

    .line 33
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->uiModel:Lbr/com/itau/sdk/android/core/model/UiModelDTO;

    return-object v0
.end method

.method public isDispositivoNecessario()Z
    .registers 2

    .line 29
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->dispositivoNecessario:Z

    return v0
.end method

.method public isExibeMensagemTokeNaoNecessario()Z
    .registers 2

    .line 21
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->exibeMensagemTokeNaoNecessario:Z

    return v0
.end method

.method public isSenhaNecessaria()Z
    .registers 2

    .line 25
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;->senhaNecessaria:Z

    return v0
.end method

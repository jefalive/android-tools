.class public final Lcom/itau/empresas/ui/view/MensagemAlertaView_;
.super Lcom/itau/empresas/ui/view/MensagemAlertaView;
.source "MensagemAlertaView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 37
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/MensagemAlertaView;-><init>(Landroid/content/Context;)V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/MensagemAlertaView_;->alreadyInflated_:Z

    .line 34
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAlertaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 38
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/MensagemAlertaView_;->init_()V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 42
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/view/MensagemAlertaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/MensagemAlertaView_;->alreadyInflated_:Z

    .line 34
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAlertaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 43
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/MensagemAlertaView_;->init_()V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/ui/view/MensagemAlertaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/MensagemAlertaView_;->alreadyInflated_:Z

    .line 34
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAlertaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 48
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/MensagemAlertaView_;->init_()V

    .line 49
    return-void
.end method

.method private init_()V
    .registers 4

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAlertaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v2

    .line 75
    .local v2, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 76
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/MensagemAlertaView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0c00cf

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/MensagemAlertaView_;->corLaranja:I

    .line 77
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAlertaView_;->application:Lcom/itau/empresas/CustomApplication;

    .line 78
    invoke-static {v2}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 79
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 65
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/MensagemAlertaView_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/MensagemAlertaView_;->alreadyInflated_:Z

    .line 67
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/MensagemAlertaView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03014d

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/ui/view/MensagemAlertaView_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAlertaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 70
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/ui/view/MensagemAlertaView;->onFinishInflate()V

    .line 71
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 95
    const v0, 0x7f0e05c6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAlertaView_;->linearCartaoAlerta:Landroid/widget/LinearLayout;

    .line 96
    const v0, 0x7f0e05c7

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/TextViewIcon;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAlertaView_;->imgWarning:Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 97
    const v0, 0x7f0e05c8

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAlertaView_;->textoWarning:Landroid/widget/TextView;

    .line 98
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/MensagemAlertaView_;->afterViews()V

    .line 99
    return-void
.end method

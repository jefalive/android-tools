.class public Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;
.super Lcom/daimajia/swipe/adapters/BaseSwipeAdapter;
.source "TransferenciaRecebidasAdapter.java"

# interfaces
.implements Landroid/widget/Filterable;
.implements Lcom/daimajia/swipe/SwipeLayout$SwipeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$TransferenciaFiltro;
    }
.end annotation


# instance fields
.field private baseDialogPreview:Lcom/itau/empresas/ui/dialog/BaseDialog;

.field private isSwiped:Z

.field private listaOriginal:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;>;"
        }
    .end annotation
.end field

.field private listaTransferenciaRecebidas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;>;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private qtdDias:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<Ljava/lang/Object;>;"
        }
    .end annotation
.end field

.field swipeLayout:Lcom/daimajia/swipe/SwipeLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .registers 4
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "listaTransferenciaRecebidas"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;Ljava/util/List<Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;>;)V"
        }
    .end annotation

    .line 45
    invoke-direct {p0}, Lcom/daimajia/swipe/adapters/BaseSwipeAdapter;-><init>()V

    .line 39
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->qtdDias:Landroid/util/SparseArray;

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->isSwiped:Z

    .line 46
    iput-object p1, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->mContext:Landroid/content/Context;

    .line 47
    iput-object p2, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->listaTransferenciaRecebidas:Ljava/util/List;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->listaOriginal:Ljava/util/List;

    .line 49
    invoke-direct {p0}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->ordenarAdapter()V

    .line 50
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;)Lcom/itau/empresas/ui/dialog/BaseDialog;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    .line 33
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->baseDialogPreview:Lcom/itau/empresas/ui/dialog/BaseDialog;

    return-object v0
.end method

.method static synthetic access$002(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;Lcom/itau/empresas/ui/dialog/BaseDialog;)Lcom/itau/empresas/ui/dialog/BaseDialog;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;
    .param p1, "x1"    # Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 33
    iput-object p1, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->baseDialogPreview:Lcom/itau/empresas/ui/dialog/BaseDialog;

    return-object p1
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;)Z
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    .line 33
    iget-boolean v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->isSwiped:Z

    return v0
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;)Landroid/content/Context;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    .line 33
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;)Ljava/util/List;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    .line 33
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->listaOriginal:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$502(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;
    .param p1, "x1"    # Ljava/util/List;

    .line 33
    iput-object p1, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->listaTransferenciaRecebidas:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$600(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    .line 33
    invoke-direct {p0}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->ordenarAdapter()V

    return-void
.end method

.method private adicionaEventos(Landroid/view/View;Landroid/widget/TextView;Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;)V
    .registers 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "textoNome"    # Landroid/widget/TextView;
    .param p3, "itemVO"    # Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    .line 116
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->eventosCliqueListener(Landroid/view/View;Landroid/widget/TextView;Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;)V

    .line 118
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->swipeLayout:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0, p0}, Lcom/daimajia/swipe/SwipeLayout;->addSwipeListener(Lcom/daimajia/swipe/SwipeLayout$SwipeListener;)V

    .line 120
    invoke-direct {p0, p3}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->swipeTouchListener(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;)V

    .line 121
    return-void
.end method

.method private eventosCliqueListener(Landroid/view/View;Landroid/widget/TextView;Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;)V
    .registers 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "textoNome"    # Landroid/widget/TextView;
    .param p3, "itemVO"    # Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    .line 154
    new-instance v0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$3;

    invoke-direct {v0, p0, p3}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$3;-><init>(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;)V

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    const v0, 0x7f0e02ae

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$4;

    invoke-direct {v1, p0, p3}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$4;-><init>(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    const v0, 0x7f0e01f0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$5;

    invoke-direct {v1, p0, p3}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$5;-><init>(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 180
    return-void
.end method

.method private static formataTituloCabecalho(I)Ljava/lang/String;
    .registers 3
    .param p0, "daysDiff"    # I

    .line 238
    const-string v1, "\u00faltimos 7 dias"

    .line 240
    .local v1, "titulo":Ljava/lang/String;
    const/4 v0, 0x7

    if-le p0, v0, :cond_c

    const/16 v0, 0xf

    if-gt p0, v0, :cond_c

    .line 241
    const-string v1, "\u00faltimos 15 dias"

    goto :goto_2c

    .line 242
    :cond_c
    const/16 v0, 0xf

    if-le p0, v0, :cond_17

    const/16 v0, 0x1e

    if-gt p0, v0, :cond_17

    .line 243
    const-string v1, "\u00faltimos 30 dias"

    goto :goto_2c

    .line 244
    :cond_17
    const/16 v0, 0x1e

    if-le p0, v0, :cond_22

    const/16 v0, 0x3c

    if-gt p0, v0, :cond_22

    .line 245
    const-string v1, "\u00faltimos 60 dias"

    goto :goto_2c

    .line 246
    :cond_22
    const/16 v0, 0x3c

    if-le p0, v0, :cond_2c

    const/16 v0, 0x5a

    if-gt p0, v0, :cond_2c

    .line 247
    const-string v1, "\u00faltimos 90 dias"

    .line 250
    :cond_2c
    :goto_2c
    return-object v1
.end method

.method private formataValorPagamento(Landroid/widget/TextView;Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;)V
    .registers 7
    .param p1, "textoValor"    # Landroid/widget/TextView;
    .param p2, "itemVO"    # Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    .line 105
    .line 106
    invoke-virtual {p2}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getValorPagamento()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 107
    .line 108
    .local v2, "valorFormatado":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getValorPagamento()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 109
    .local v3, "valorFormatadoAcessibilidade":Ljava/lang/String;
    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 111
    return-void
.end method

.method private ordenar()V
    .registers 3

    .line 255
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->listaTransferenciaRecebidas:Ljava/util/List;

    new-instance v1, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$6;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$6;-><init>(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;)V

    .line 256
    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 263
    return-void
.end method

.method private ordenarAdapter()V
    .registers 10

    .line 206
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->qtdDias:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 207
    invoke-direct {p0}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->ordenar()V

    .line 209
    const/4 v2, 0x0

    .line 211
    .local v2, "i":I
    invoke-static {}, Lorg/threeten/bp/LocalDate;->now()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    .line 213
    .local v3, "dataHoje":Lorg/threeten/bp/LocalDate;
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->listaTransferenciaRecebidas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_13
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4d

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    .line 215
    .local v5, "comprovante":Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;
    invoke-virtual {v5}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getDataRecebimento()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->parse(Ljava/lang/String;)Lorg/threeten/bp/LocalDate;

    move-result-object v6

    .line 216
    .local v6, "dataLancamento":Lorg/threeten/bp/LocalDate;
    invoke-static {v6, v3}, Lorg/threeten/bp/Period;->between(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/Period;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/Period;->getDays()I

    move-result v7

    .line 218
    .local v7, "daysDiff":I
    const-string v8, ""

    .line 219
    .local v8, "titulo":Ljava/lang/String;
    if-ltz v7, :cond_3c

    const/16 v0, 0x5a

    if-gt v7, v0, :cond_3c

    .line 220
    invoke-static {v7}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->formataTituloCabecalho(I)Ljava/lang/String;

    move-result-object v8

    .line 223
    :cond_3c
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->qtdDias:Landroid/util/SparseArray;

    invoke-virtual {v0, v8}, Landroid/util/SparseArray;->indexOfValue(Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_4c

    .line 224
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->qtdDias:Landroid/util/SparseArray;

    move v1, v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v1, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 226
    .end local v5    # "comprovante":Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;
    .end local v6    # "dataLancamento":Lorg/threeten/bp/LocalDate;
    .end local v7    # "daysDiff":I
    .end local v8    # "titulo":Ljava/lang/String;
    :cond_4c
    goto :goto_13

    .line 228
    :cond_4d
    invoke-virtual {p0}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->notifyDataSetChanged()V

    .line 229
    return-void
.end method

.method private setaAcessibilidadeHeader(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .param p1, "data"    # Ljava/lang/String;

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 184
    const v2, 0x7f070347

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 183
    return-object v0
.end method

.method private swipeTouchListener(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;)V
    .registers 4
    .param p1, "itemVO"    # Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    .line 124
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->swipeLayout:Lcom/daimajia/swipe/SwipeLayout;

    new-instance v1, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$1;-><init>(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;)V

    .line 125
    invoke-virtual {v0, v1}, Lcom/daimajia/swipe/SwipeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 136
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->swipeLayout:Lcom/daimajia/swipe/SwipeLayout;

    new-instance v1, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$2;

    invoke-direct {v1, p0, p1}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$2;-><init>(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;)V

    .line 137
    invoke-virtual {v0, v1}, Lcom/daimajia/swipe/SwipeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 150
    return-void
.end method


# virtual methods
.method public fillValues(ILandroid/view/View;)V
    .registers 12
    .param p1, "i"    # I
    .param p2, "view"    # Landroid/view/View;

    .line 65
    .line 66
    const v0, 0x7f0e0345

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/TextView;

    .line 67
    .line 68
    .local v2, "textoQuantidadeDiasHeader":Landroid/widget/TextView;
    const v0, 0x7f0e0344

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 69
    .local v3, "headerLista":Landroid/widget/RelativeLayout;
    const v0, 0x7f0e056b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/TextView;

    .line 70
    .line 71
    .local v4, "textoNome":Landroid/widget/TextView;
    const v0, 0x7f0e056c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/TextView;

    .line 72
    .local v5, "textoLiteralIdentificacao":Landroid/widget/TextView;
    const v0, 0x7f0e02d5

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/TextView;

    .line 73
    .local v6, "textoValor":Landroid/widget/TextView;
    const v0, 0x7f0e056a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/daimajia/swipe/SwipeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->swipeLayout:Lcom/daimajia/swipe/SwipeLayout;

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->listaTransferenciaRecebidas:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    .line 76
    .local v7, "itemVO":Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;
    if-nez p1, :cond_5f

    .line 77
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 78
    invoke-virtual {v7}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getDataRecebimento()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 79
    .local v8, "data":Ljava/lang/String;
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    invoke-direct {p0, v8}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->setaAcessibilidadeHeader(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 81
    .end local v8    # "data":Ljava/lang/String;
    goto :goto_a3

    :cond_5f
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->listaTransferenciaRecebidas:Ljava/util/List;

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getDataRecebimento()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->listaTransferenciaRecebidas:Ljava/util/List;

    .line 82
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getDataRecebimento()Ljava/lang/String;

    move-result-object v1

    .line 81
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_85

    .line 83
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_a3

    .line 85
    :cond_85
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 86
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->listaTransferenciaRecebidas:Ljava/util/List;

    .line 87
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getDataRecebimento()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 88
    .local v8, "data":Ljava/lang/String;
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    invoke-direct {p0, v8}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->setaAcessibilidadeHeader(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 91
    .end local v8    # "data":Ljava/lang/String;
    :goto_a3
    invoke-virtual {v7}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getRemetente()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    invoke-virtual {v7}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getRemetente()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 94
    invoke-virtual {v7}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getDataRecebimento()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_d1

    .line 95
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "cnpj/cpf "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getCpfCnpj()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    :cond_d1
    invoke-virtual {v7}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getValorPagamento()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_e6

    const-string v0, ""

    invoke-virtual {v7}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getValorPagamento()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e6

    .line 98
    invoke-direct {p0, v6, v7}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->formataValorPagamento(Landroid/widget/TextView;Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;)V

    .line 101
    :cond_e6
    invoke-direct {p0, p2, v4, v7}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->adicionaEventos(Landroid/view/View;Landroid/widget/TextView;Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;)V

    .line 102
    return-void
.end method

.method public generateView(ILandroid/view/ViewGroup;)Landroid/view/View;
    .registers 6
    .param p1, "i"    # I
    .param p2, "viewGroup"    # Landroid/view/ViewGroup;

    .line 59
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 60
    const v1, 0x7f030113

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 59
    return-object v0
.end method

.method public getCount()I
    .registers 2

    .line 191
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->listaTransferenciaRecebidas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .registers 3

    .line 233
    new-instance v0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$TransferenciaFiltro;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$TransferenciaFiltro;-><init>(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$1;)V

    return-object v0
.end method

.method public getItem(I)Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;
    .registers 3
    .param p1, "position"    # I

    .line 196
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->listaTransferenciaRecebidas:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .registers 3

    .line 33
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->getItem(I)Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .param p1, "position"    # I

    .line 201
    int-to-long v0, p1

    return-wide v0
.end method

.method public getSwipeLayoutResourceId(I)I
    .registers 3
    .param p1, "i"    # I

    .line 54
    const v0, 0x7f0e056a

    return v0
.end method

.method public onClose(Lcom/daimajia/swipe/SwipeLayout;)V
    .registers 3
    .param p1, "layout"    # Lcom/daimajia/swipe/SwipeLayout;

    .line 277
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->isSwiped:Z

    .line 278
    return-void
.end method

.method public onHandRelease(Lcom/daimajia/swipe/SwipeLayout;FF)V
    .registers 4
    .param p1, "layout"    # Lcom/daimajia/swipe/SwipeLayout;
    .param p2, "xvel"    # F
    .param p3, "yvel"    # F

    .line 293
    return-void
.end method

.method public onOpen(Lcom/daimajia/swipe/SwipeLayout;)V
    .registers 3
    .param p1, "layout"    # Lcom/daimajia/swipe/SwipeLayout;

    .line 272
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->isSwiped:Z

    .line 273
    return-void
.end method

.method public onStartClose(Lcom/daimajia/swipe/SwipeLayout;)V
    .registers 2
    .param p1, "layout"    # Lcom/daimajia/swipe/SwipeLayout;

    .line 283
    return-void
.end method

.method public onStartOpen(Lcom/daimajia/swipe/SwipeLayout;)V
    .registers 2
    .param p1, "layout"    # Lcom/daimajia/swipe/SwipeLayout;

    .line 268
    return-void
.end method

.method public onUpdate(Lcom/daimajia/swipe/SwipeLayout;II)V
    .registers 4
    .param p1, "layout"    # Lcom/daimajia/swipe/SwipeLayout;
    .param p2, "leftOffset"    # I
    .param p3, "topOffset"    # I

    .line 288
    return-void
.end method

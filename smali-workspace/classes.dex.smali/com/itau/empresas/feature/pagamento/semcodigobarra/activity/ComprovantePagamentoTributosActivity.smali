.class public Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "ComprovantePagamentoTributosActivity.java"


# instance fields
.field botaoShareComprovante:Landroid/widget/Button;

.field campoAlteracao:Landroid/widget/TextView;

.field campoAutenticacao:Landroid/widget/TextView;

.field campoValorPagamento:Landroid/widget/TextView;

.field dadosComprovanteList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;>;"
        }
    .end annotation
.end field

.field detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

.field labelTipoPagamento:Landroid/widget/TextView;

.field llComprovante:Landroid/widget/LinearLayout;

.field llDadosComprovante:Landroid/widget/LinearLayout;

.field shareComprovante:Z

.field toolbar:Landroid/support/v7/widget/Toolbar;

.field toolbarTitle:Landroid/widget/TextView;

.field private wrapper:Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 36
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;

    .line 36
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->compartilhaComprovante()V

    return-void
.end method

.method private bindStatusPagamento()V
    .registers 4

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->labelTipoPagamento:Landroid/widget/TextView;

    .line 79
    const v1, 0x7f0704a4

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getDescricaoPagamento()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 78
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->campoValorPagamento:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getValorPagamento()Ljava/math/BigDecimal;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/formatacao/FormatadorNumerico;->decimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    return-void
.end method

.method private buildAdpaterComprovantes()V
    .registers 4

    .line 105
    new-instance v2, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/ComprovantesTributosAdapter;

    invoke-direct {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/ComprovantesTributosAdapter;-><init>()V

    .line 106
    .local v2, "adapter":Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/ComprovantesTributosAdapter;
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->dadosComprovanteList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/ComprovantesTributosAdapter;->setList(Ljava/util/List;)V

    .line 107
    new-instance v0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->llDadosComprovante:Landroid/widget/LinearLayout;

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;-><init>(Landroid/widget/LinearLayout;Landroid/widget/Adapter;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->wrapper:Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;

    .line 108
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->wrapper:Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->init()V

    .line 109
    return-void
.end method

.method private compartilhaComprovante()V
    .registers 5

    .line 134
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->botaoShareComprovante:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 135
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->llComprovante:Landroid/widget/LinearLayout;

    invoke-static {v0}, Lcom/itau/empresas/ui/util/BitmapUtils;->getBitmapFromView(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 136
    .local v2, "comprovanteBitmap":Landroid/graphics/Bitmap;
    invoke-static {p0, v2}, Lcom/itau/empresas/ui/util/BitmapUtils;->getUriBitmapTemporario(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/net/Uri;

    move-result-object v3

    .line 137
    .line 138
    .local v3, "bitmapUri":Landroid/net/Uri;
    const v0, 0x7f070192

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v3, v0}, Lcom/itau/empresas/ui/util/CompartilhamentoUtils;->compartilharArquivoImagemPorUri(Landroid/app/Activity;Landroid/net/Uri;Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->botaoShareComprovante:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 140
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->shareComprovante:Z

    .line 141
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f020136

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(I)V

    .line 113
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 114
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 115
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 117
    :cond_1b
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->toolbarTitle:Landroid/widget/TextView;

    const v1, 0x7f070195

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity$2;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 144
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private openDialogSucesso()V
    .registers 3

    .line 73
    new-instance v1, Lcom/itau/empresas/ui/dialog/DialogSucesso;

    invoke-direct {v1}, Lcom/itau/empresas/ui/dialog/DialogSucesso;-><init>()V

    .line 74
    .local v1, "dialogSucesso":Lcom/itau/empresas/ui/dialog/DialogSucesso;
    invoke-virtual {v1, p0}, Lcom/itau/empresas/ui/dialog/DialogSucesso;->showDialog(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/DialogSucesso;

    .line 75
    return-void
.end method


# virtual methods
.method protected onPause()V
    .registers 2

    .line 98
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->onPause()V

    .line 99
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->wrapper:Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;

    if-eqz v0, :cond_c

    .line 100
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->wrapper:Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->unregister()V

    .line 102
    :cond_c
    return-void
.end method

.method protected onResume()V
    .registers 5

    .line 85
    iget-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->shareComprovante:Z

    if-eqz v0, :cond_13

    .line 86
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity$1;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;)V

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 93
    :cond_13
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->onResume()V

    .line 94
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 57
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public shareComprovante()V
    .registers 1

    .line 129
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->compartilhaComprovante()V

    .line 130
    return-void
.end method

.method public updateView()V
    .registers 7

    .line 62
    const-string v0, "%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, " "

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 63
    .local v4, "espaco":Ljava/lang/String;
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->constroiToolbar()V

    .line 64
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->bindStatusPagamento()V

    .line 65
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->buildAdpaterComprovantes()V

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->campoAutenticacao:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getAutenticacaoComprovante()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getDataVencimento()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/formatacao/DateUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 68
    .local v5, "data":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->campoAlteracao:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "as"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getListaEtapas()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;->getHoraEtapa()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ComprovantePagamentoTributosActivity;->openDialogSucesso()V

    .line 70
    return-void
.end method

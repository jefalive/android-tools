.class public Lcom/itau/empresas/ui/util/BuildDadosComprovante;
.super Ljava/lang/Object;
.source "BuildDadosComprovante.java"


# instance fields
.field private context:Landroid/content/Context;

.field private detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;


# direct methods
.method public constructor <init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;Landroid/content/Context;)V
    .registers 3
    .param p1, "detalhesAutorizacaoVO"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;
    .param p2, "context"    # Landroid/content/Context;

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    .line 23
    iput-object p2, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->context:Landroid/content/Context;

    .line 25
    return-void
.end method

.method private buildDadosComprovante()Ljava/util/ArrayList;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;>;"
        }
    .end annotation

    .line 32
    const-string v0, "DARF normal"

    iget-object v1, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getDescricaoPagamento()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 33
    invoke-direct {p0}, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->getListDadosComprovanteDarf()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0

    .line 35
    :cond_13
    invoke-direct {p0}, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->getListDadosComprovanteGPS()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private getListDadosComprovanteDarf()Ljava/util/ArrayList;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;>;"
        }
    .end annotation

    .line 60
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 61
    .local v4, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;>;"
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->context:Landroid/content/Context;

    const v2, 0x7f070480

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getNomeContribuinte()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 62
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->context:Landroid/content/Context;

    const v2, 0x7f0701b9

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;->CPF_CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    iget-object v3, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getCnpj()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->context:Landroid/content/Context;

    const v2, 0x7f07018f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getCodigoReceita()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->context:Landroid/content/Context;

    const v2, 0x7f070486

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getNumeroReferencia()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->context:Landroid/content/Context;

    const v2, 0x7f07049e

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getValorPrincipal()Ljava/math/BigDecimal;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/formatacao/FormatadorNumerico;->decimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 66
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->context:Landroid/content/Context;

    const v2, 0x7f0703f3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    .line 67
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getValorMulta()Ljava/math/BigDecimal;

    move-result-object v2

    iget-object v3, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getValorJuros()Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/formatacao/FormatadorNumerico;->decimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 66
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 68
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->context:Landroid/content/Context;

    const v2, 0x7f070375

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getValorPagamento()Ljava/math/BigDecimal;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/formatacao/FormatadorNumerico;->decimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->context:Landroid/content/Context;

    const v2, 0x7f07068c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getDataVencimento()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/itau/empresas/ui/util/formatacao/DateUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 70
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->context:Landroid/content/Context;

    const v2, 0x7f070495

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getDataPagamento()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/itau/empresas/ui/util/formatacao/DateUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getSeuNumero()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_128

    .line 72
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->context:Landroid/content/Context;

    const v2, 0x7f070498

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getSeuNumero()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    :cond_128
    iget-object v0, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getIdentificacaoComprovante()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_14c

    .line 75
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->context:Landroid/content/Context;

    const v2, 0x7f070370

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getIdentificacaoComprovante()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    :cond_14c
    return-object v4
.end method

.method private getListDadosComprovanteGPS()Ljava/util/ArrayList;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;>;"
        }
    .end annotation

    .line 42
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 43
    .local v4, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;>;"
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->context:Landroid/content/Context;

    const v2, 0x7f070480

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getNomeContribuinte()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->context:Landroid/content/Context;

    const v2, 0x7f07018d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getCodigoPagamento()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->context:Landroid/content/Context;

    const v2, 0x7f070194

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getCompetencia()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->context:Landroid/content/Context;

    const v2, 0x7f070372

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getIdentificacao()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 47
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->context:Landroid/content/Context;

    const v2, 0x7f0706d1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getValorInss()Ljava/math/BigDecimal;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/formatacao/FormatadorNumerico;->decimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 48
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->context:Landroid/content/Context;

    const v2, 0x7f0706ce

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getValorOutrasEntidades()Ljava/math/BigDecimal;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/formatacao/FormatadorNumerico;->decimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->context:Landroid/content/Context;

    const v2, 0x7f070128

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    .line 50
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getValorJuros()Ljava/math/BigDecimal;

    move-result-object v2

    iget-object v3, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getValorJuros()Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/formatacao/FormatadorNumerico;->decimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 49
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->context:Landroid/content/Context;

    const v2, 0x7f070495

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getDataPagamento()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/itau/empresas/ui/util/formatacao/DateUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getSeuNumero()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_106

    .line 53
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->context:Landroid/content/Context;

    const v2, 0x7f070498

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getSeuNumero()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    :cond_106
    return-object v4
.end method


# virtual methods
.method public getDadosComprovante()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosComprovante;>;"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->buildDadosComprovante()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

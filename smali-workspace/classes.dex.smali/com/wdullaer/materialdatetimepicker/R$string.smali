.class public final Lcom/wdullaer/materialdatetimepicker/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wdullaer/materialdatetimepicker/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final mdtp_ampm_circle_radius_multiplier:I = 0x7f07045b

.field public static final mdtp_cancel:I = 0x7f07045c

.field public static final mdtp_circle_radius_multiplier:I = 0x7f07045d

.field public static final mdtp_circle_radius_multiplier_24HourMode:I = 0x7f07045e

.field public static final mdtp_day_of_week_label_typeface:I = 0x7f07006b

.field public static final mdtp_day_picker_description:I = 0x7f070041

.field public static final mdtp_deleted_key:I = 0x7f070042

.field public static final mdtp_done_label:I = 0x7f070043

.field public static final mdtp_hour_picker_description:I = 0x7f070044

.field public static final mdtp_item_is_selected:I = 0x7f070045

.field public static final mdtp_minute_picker_description:I = 0x7f070046

.field public static final mdtp_numbers_radius_multiplier_inner:I = 0x7f07045f

.field public static final mdtp_numbers_radius_multiplier_normal:I = 0x7f070460

.field public static final mdtp_numbers_radius_multiplier_outer:I = 0x7f070461

.field public static final mdtp_ok:I = 0x7f070462

.field public static final mdtp_radial_numbers_typeface:I = 0x7f070463

.field public static final mdtp_sans_serif:I = 0x7f070464

.field public static final mdtp_select_day:I = 0x7f070047

.field public static final mdtp_select_hours:I = 0x7f070048

.field public static final mdtp_select_minutes:I = 0x7f070049

.field public static final mdtp_select_year:I = 0x7f07004a

.field public static final mdtp_selection_radius_multiplier:I = 0x7f070465

.field public static final mdtp_text_size_multiplier_inner:I = 0x7f070466

.field public static final mdtp_text_size_multiplier_normal:I = 0x7f070467

.field public static final mdtp_text_size_multiplier_outer:I = 0x7f070468

.field public static final mdtp_time_placeholder:I = 0x7f070469

.field public static final mdtp_time_separator:I = 0x7f07046a

.field public static final mdtp_year_picker_description:I = 0x7f07004b


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

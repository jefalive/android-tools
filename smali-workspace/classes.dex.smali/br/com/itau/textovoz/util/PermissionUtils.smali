.class public Lbr/com/itau/textovoz/util/PermissionUtils;
.super Ljava/lang/Object;
.source "PermissionUtils.java"


# direct methods
.method public static getPermissionRecordAudio()[Ljava/lang/String;
    .registers 3

    .line 20
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.permission.RECORD_AUDIO"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    return-object v0
.end method

.method public static varargs hasSelfPermissions(Landroid/content/Context;[Ljava/lang/String;)Z
    .registers 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "permissions"    # [Ljava/lang/String;

    .line 33
    move-object v1, p1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v2, :cond_12

    aget-object v4, v1, v3

    .line 34
    .local v4, "permission":Ljava/lang/String;
    invoke-static {p0, v4}, Landroid/support/v4/content/PermissionChecker;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_f

    .line 35
    const/4 v0, 0x0

    return v0

    .line 33
    .end local v4    # "permission":Ljava/lang/String;
    :cond_f
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 38
    :cond_12
    const/4 v0, 0x1

    return v0
.end method

.method public static varargs shouldShowRequestPermissionRationale(Landroid/app/Activity;[Ljava/lang/String;)Z
    .registers 7
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "permissions"    # [Ljava/lang/String;

    .line 43
    move-object v1, p1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v2, :cond_12

    aget-object v4, v1, v3

    .line 44
    .local v4, "permission":Ljava/lang/String;
    invoke-static {p0, v4}, Landroid/support/v4/app/ActivityCompat;->shouldShowRequestPermissionRationale(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 45
    const/4 v0, 0x1

    return v0

    .line 43
    .end local v4    # "permission":Ljava/lang/String;
    :cond_f
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 48
    :cond_12
    const/4 v0, 0x0

    return v0
.end method

.method public static varargs verifyPermissions([I)Z
    .registers 6
    .param p0, "grantResults"    # [I

    .line 24
    move-object v1, p0

    array-length v2, v1

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v2, :cond_e

    aget v4, v1, v3

    .line 25
    .local v4, "result":I
    if-eqz v4, :cond_b

    .line 26
    const/4 v0, 0x0

    return v0

    .line 24
    .end local v4    # "result":I
    :cond_b
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 29
    :cond_e
    const/4 v0, 0x1

    return v0
.end method

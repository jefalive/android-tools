.class public Lcom/itau/empresas/feature/home/CobrancaController;
.super Lcom/itau/empresas/controller/BaseController;
.source "CobrancaController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/itau/empresas/controller/BaseController<Lcom/itau/empresas/api/Api;>;"
    }
.end annotation


# instance fields
.field app:Lcom/itau/empresas/CustomApplication;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 23
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method

.method private TrataRetornoWidget(Lcom/itau/empresas/api/model/FrancesinhaVO;)V
    .registers 18
    .param p1, "francesinha"    # Lcom/itau/empresas/api/model/FrancesinhaVO;

    .line 46
    new-instance v7, Lcom/itau/empresas/Evento$EventoWidgetCobranca;

    invoke-direct {v7}, Lcom/itau/empresas/Evento$EventoWidgetCobranca;-><init>()V

    .line 47
    .local v7, "eventoWidgetCobranca":Lcom/itau/empresas/Evento$EventoWidgetCobranca;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 48
    .local v8, "listaCobrancas":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;>;"
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    .line 49
    .local v9, "valorTotal":Ljava/lang/Float;
    if-eqz p1, :cond_13d

    .line 50
    invoke-virtual/range {p1 .. p1}, Lcom/itau/empresas/api/model/FrancesinhaVO;->getExtratosResumidos()[Lcom/itau/empresas/api/model/FrancesinhaVO$ExtratoResumido;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 51
    invoke-virtual {v0}, Lcom/itau/empresas/api/model/FrancesinhaVO$ExtratoResumido;->getCobrancaComRegistro()Lcom/itau/empresas/api/model/FrancesinhaVO$CobrancComRegistro;

    move-result-object v10

    .line 52
    .local v10, "cobrancas":Lcom/itau/empresas/api/model/FrancesinhaVO$CobrancComRegistro;
    invoke-static {v10}, Lcom/itau/empresas/feature/home/CobrancaController;->somaTotal(Lcom/itau/empresas/api/model/FrancesinhaVO$CobrancComRegistro;)Ljava/lang/Float;

    move-result-object v9

    .line 54
    invoke-virtual {v10}, Lcom/itau/empresas/api/model/FrancesinhaVO$CobrancComRegistro;->getVencidos()Lcom/itau/empresas/api/model/FrancesinhaVO$ValorEQuantidade;

    move-result-object v11

    .line 56
    .local v11, "vencidos":Lcom/itau/empresas/api/model/FrancesinhaVO$ValorEQuantidade;
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;

    const-string v1, "#F1AE2F"

    const-string v2, "vencidos"

    .line 57
    invoke-virtual {v11}, Lcom/itau/empresas/api/model/FrancesinhaVO$ValorEQuantidade;->getValorTitulos()Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v3

    .line 58
    invoke-virtual {v11}, Lcom/itau/empresas/api/model/FrancesinhaVO$ValorEQuantidade;->getQuantidadeTitulos()I

    move-result v4

    invoke-virtual {v11}, Lcom/itau/empresas/api/model/FrancesinhaVO$ValorEQuantidade;->getValorTitulos()Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    const/high16 v6, 0x42c80000    # 100.0f

    mul-float/2addr v5, v6

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v6

    div-float/2addr v5, v6

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IF)V

    .line 56
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    invoke-virtual {v10}, Lcom/itau/empresas/api/model/FrancesinhaVO$CobrancComRegistro;->getaVencer()Lcom/itau/empresas/api/model/FrancesinhaVO$AVencer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/FrancesinhaVO$AVencer;->getPeriodo()Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;->getPrimeiroPeriodo()Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;

    move-result-object v12

    .line 61
    .local v12, "primeiro":Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;

    const-string v1, "#45A9BB"

    invoke-virtual {v12}, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->getLiteralTitulos()Ljava/lang/String;

    move-result-object v2

    .line 62
    invoke-virtual {v12}, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->getValorTitulos()Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v3

    .line 63
    invoke-virtual {v12}, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->getQuantidadeTitulos()I

    move-result v4

    invoke-virtual {v12}, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->getValorTitulos()Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    const/high16 v6, 0x42c80000    # 100.0f

    mul-float/2addr v5, v6

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v6

    div-float/2addr v5, v6

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IF)V

    .line 61
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    invoke-virtual {v10}, Lcom/itau/empresas/api/model/FrancesinhaVO$CobrancComRegistro;->getaVencer()Lcom/itau/empresas/api/model/FrancesinhaVO$AVencer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/FrancesinhaVO$AVencer;->getPeriodo()Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;->getSegundoPeriodo()Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;

    move-result-object v13

    .line 66
    .local v13, "segundo":Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;

    const-string v1, "#00838F"

    invoke-virtual {v13}, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->getLiteralTitulos()Ljava/lang/String;

    move-result-object v2

    .line 67
    invoke-virtual {v13}, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->getValorTitulos()Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v3

    .line 68
    invoke-virtual {v13}, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->getQuantidadeTitulos()I

    move-result v4

    invoke-virtual {v13}, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->getValorTitulos()Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    const/high16 v6, 0x42c80000    # 100.0f

    mul-float/2addr v5, v6

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v6

    div-float/2addr v5, v6

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IF)V

    .line 66
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    invoke-virtual {v10}, Lcom/itau/empresas/api/model/FrancesinhaVO$CobrancComRegistro;->getaVencer()Lcom/itau/empresas/api/model/FrancesinhaVO$AVencer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/FrancesinhaVO$AVencer;->getPeriodo()Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;->getTerceiroPeriodo()Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;

    move-result-object v14

    .line 71
    .local v14, "terceiro":Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;

    const-string v1, "#01579B"

    invoke-virtual {v14}, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->getLiteralTitulos()Ljava/lang/String;

    move-result-object v2

    .line 72
    invoke-virtual {v14}, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->getValorTitulos()Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v3

    .line 73
    invoke-virtual {v14}, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->getQuantidadeTitulos()I

    move-result v4

    invoke-virtual {v14}, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->getValorTitulos()Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    const/high16 v6, 0x42c80000    # 100.0f

    mul-float/2addr v5, v6

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v6

    div-float/2addr v5, v6

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IF)V

    .line 71
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    invoke-virtual {v10}, Lcom/itau/empresas/api/model/FrancesinhaVO$CobrancComRegistro;->getaVencer()Lcom/itau/empresas/api/model/FrancesinhaVO$AVencer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/FrancesinhaVO$AVencer;->getPeriodo()Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;->getQuartoPeriodo()Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;

    move-result-object v15

    .line 76
    .local v15, "quarto":Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;

    const-string v1, "#1A237E"

    invoke-virtual {v15}, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->getLiteralTitulos()Ljava/lang/String;

    move-result-object v2

    .line 77
    invoke-virtual {v15}, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->getValorTitulos()Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v3

    .line 78
    invoke-virtual {v15}, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->getQuantidadeTitulos()I

    move-result v4

    invoke-virtual {v15}, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->getValorTitulos()Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    const/high16 v6, 0x42c80000    # 100.0f

    mul-float/2addr v5, v6

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v6

    div-float/2addr v5, v6

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IF)V

    .line 76
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    .end local v10    # "cobrancas":Lcom/itau/empresas/api/model/FrancesinhaVO$CobrancComRegistro;
    .end local v11    # "vencidos":Lcom/itau/empresas/api/model/FrancesinhaVO$ValorEQuantidade;
    .end local v12    # "primeiro":Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;
    .end local v13    # "segundo":Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;
    .end local v14    # "terceiro":Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;
    .end local v15    # "quarto":Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;
    :cond_13d
    invoke-static {v8}, Lcom/itau/empresas/Evento$EventoWidgetCobranca;->setCobrancas(Ljava/util/List;)V

    .line 82
    invoke-static {v7}, Lcom/itau/empresas/feature/home/CobrancaController;->post(Ljava/lang/Object;)V

    .line 83
    invoke-static {v9}, Lcom/itau/empresas/feature/home/CobrancaController;->post(Ljava/lang/Object;)V

    .line 84
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/home/CobrancaController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/home/CobrancaController;

    .line 23
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/CobrancaController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/home/CobrancaController;Lcom/itau/empresas/api/model/FrancesinhaVO;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/home/CobrancaController;
    .param p1, "x1"    # Lcom/itau/empresas/api/model/FrancesinhaVO;

    .line 23
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/CobrancaController;->TrataRetornoWidget(Lcom/itau/empresas/api/model/FrancesinhaVO;)V

    return-void
.end method

.method private static somaTotal(Lcom/itau/empresas/api/model/FrancesinhaVO$CobrancComRegistro;)Ljava/lang/Float;
    .registers 3
    .param p0, "cobrancas"    # Lcom/itau/empresas/api/model/FrancesinhaVO$CobrancComRegistro;

    .line 87
    invoke-virtual {p0}, Lcom/itau/empresas/api/model/FrancesinhaVO$CobrancComRegistro;->getVencidos()Lcom/itau/empresas/api/model/FrancesinhaVO$ValorEQuantidade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/FrancesinhaVO$ValorEQuantidade;->getValorTitulos()Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 88
    invoke-virtual {p0}, Lcom/itau/empresas/api/model/FrancesinhaVO$CobrancComRegistro;->getaVencer()Lcom/itau/empresas/api/model/FrancesinhaVO$AVencer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/FrancesinhaVO$AVencer;->getPeriodo()Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;->getPrimeiroPeriodo()Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->getValorTitulos()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    add-float/2addr v0, v1

    .line 89
    invoke-virtual {p0}, Lcom/itau/empresas/api/model/FrancesinhaVO$CobrancComRegistro;->getaVencer()Lcom/itau/empresas/api/model/FrancesinhaVO$AVencer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/FrancesinhaVO$AVencer;->getPeriodo()Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;->getSegundoPeriodo()Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->getValorTitulos()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    add-float/2addr v0, v1

    .line 90
    invoke-virtual {p0}, Lcom/itau/empresas/api/model/FrancesinhaVO$CobrancComRegistro;->getaVencer()Lcom/itau/empresas/api/model/FrancesinhaVO$AVencer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/FrancesinhaVO$AVencer;->getPeriodo()Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;->getTerceiroPeriodo()Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->getValorTitulos()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    add-float/2addr v0, v1

    .line 91
    invoke-virtual {p0}, Lcom/itau/empresas/api/model/FrancesinhaVO$CobrancComRegistro;->getaVencer()Lcom/itau/empresas/api/model/FrancesinhaVO$AVencer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/FrancesinhaVO$AVencer;->getPeriodo()Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;->getQuartoPeriodo()Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;->getValorTitulos()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    add-float/2addr v0, v1

    .line 87
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public consultaWidget(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 13
    .param p1, "reduzido"    # Z
    .param p2, "tipoVisao"    # Ljava/lang/String;
    .param p3, "tipoCobranca"    # Ljava/lang/String;
    .param p4, "dataConsulta"    # Ljava/lang/String;
    .param p5, "codigoCarreira"    # Ljava/lang/String;

    .line 30
    new-instance v0, Lcom/itau/empresas/feature/home/CobrancaController$1;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/itau/empresas/feature/home/CobrancaController$1;-><init>(Lcom/itau/empresas/feature/home/CobrancaController;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 43
    return-void
.end method

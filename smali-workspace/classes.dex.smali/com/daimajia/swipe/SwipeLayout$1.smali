.class Lcom/daimajia/swipe/SwipeLayout$1;
.super Landroid/support/v4/widget/ViewDragHelper$Callback;
.source "SwipeLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/daimajia/swipe/SwipeLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field isCloseBeforeDrag:Z

.field final synthetic this$0:Lcom/daimajia/swipe/SwipeLayout;


# direct methods
.method constructor <init>(Lcom/daimajia/swipe/SwipeLayout;)V
    .registers 3

    .line 214
    iput-object p1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-direct {p0}, Landroid/support/v4/widget/ViewDragHelper$Callback;-><init>()V

    .line 328
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->isCloseBeforeDrag:Z

    return-void
.end method


# virtual methods
.method public clampViewPositionHorizontal(Landroid/view/View;II)I
    .registers 6
    .param p1, "child"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "dx"    # I

    .line 218
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_7c

    .line 219
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$4;->$SwitchMap$com$daimajia$swipe$SwipeLayout$DragEdge:[I

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    invoke-static {v1}, Lcom/daimajia/swipe/SwipeLayout;->access$000(Lcom/daimajia/swipe/SwipeLayout;)Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    move-result-object v1

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_de

    goto/16 :goto_7a

    .line 222
    :pswitch_1b
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    return v0

    .line 224
    :pswitch_22
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    if-ge p2, v0, :cond_31

    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    return v0

    .line 225
    :cond_31
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I
    invoke-static {v1}, Lcom/daimajia/swipe/SwipeLayout;->access$100(Lcom/daimajia/swipe/SwipeLayout;)I

    move-result v1

    add-int/2addr v0, v1

    if-le p2, v0, :cond_7a

    .line 226
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I
    invoke-static {v1}, Lcom/daimajia/swipe/SwipeLayout;->access$100(Lcom/daimajia/swipe/SwipeLayout;)I

    move-result v1

    add-int/2addr v0, v1

    return v0

    .line 229
    :pswitch_4e
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    if-le p2, v0, :cond_5d

    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    return v0

    .line 230
    :cond_5d
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I
    invoke-static {v1}, Lcom/daimajia/swipe/SwipeLayout;->access$100(Lcom/daimajia/swipe/SwipeLayout;)I

    move-result v1

    sub-int/2addr v0, v1

    if-ge p2, v0, :cond_7a

    .line 231
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I
    invoke-static {v1}, Lcom/daimajia/swipe/SwipeLayout;->access$100(Lcom/daimajia/swipe/SwipeLayout;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0

    .line 232
    :cond_7a
    :goto_7a
    goto/16 :goto_dd

    .line 234
    :cond_7c
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getCurrentBottomView()Landroid/view/View;

    move-result-object v0

    if-ne v0, p1, :cond_dd

    .line 236
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$4;->$SwitchMap$com$daimajia$swipe$SwipeLayout$DragEdge:[I

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    invoke-static {v1}, Lcom/daimajia/swipe/SwipeLayout;->access$000(Lcom/daimajia/swipe/SwipeLayout;)Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    move-result-object v1

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_ea

    goto :goto_dd

    .line 239
    :pswitch_96
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    return v0

    .line 241
    :pswitch_9d
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mShowMode:Lcom/daimajia/swipe/SwipeLayout$ShowMode;
    invoke-static {v0}, Lcom/daimajia/swipe/SwipeLayout;->access$200(Lcom/daimajia/swipe/SwipeLayout;)Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->PullOut:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    if-ne v0, v1, :cond_dd

    .line 242
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    if-le p2, v0, :cond_dd

    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    return v0

    .line 246
    :pswitch_b6
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mShowMode:Lcom/daimajia/swipe/SwipeLayout$ShowMode;
    invoke-static {v0}, Lcom/daimajia/swipe/SwipeLayout;->access$200(Lcom/daimajia/swipe/SwipeLayout;)Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->PullOut:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    if-ne v0, v1, :cond_dd

    .line 247
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I
    invoke-static {v1}, Lcom/daimajia/swipe/SwipeLayout;->access$100(Lcom/daimajia/swipe/SwipeLayout;)I

    move-result v1

    sub-int/2addr v0, v1

    if-ge p2, v0, :cond_dd

    .line 248
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I
    invoke-static {v1}, Lcom/daimajia/swipe/SwipeLayout;->access$100(Lcom/daimajia/swipe/SwipeLayout;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0

    .line 254
    :cond_dd
    :goto_dd
    return p2

    :pswitch_data_de
    .packed-switch 0x1
        :pswitch_1b
        :pswitch_1b
        :pswitch_22
        :pswitch_4e
    .end packed-switch

    :pswitch_data_ea
    .packed-switch 0x1
        :pswitch_96
        :pswitch_96
        :pswitch_9d
        :pswitch_b6
    .end packed-switch
.end method

.method public clampViewPositionVertical(Landroid/view/View;II)I
    .registers 9
    .param p1, "child"    # Landroid/view/View;
    .param p2, "top"    # I
    .param p3, "dy"    # I

    .line 259
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_7c

    .line 260
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$4;->$SwitchMap$com$daimajia$swipe$SwipeLayout$DragEdge:[I

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    invoke-static {v1}, Lcom/daimajia/swipe/SwipeLayout;->access$000(Lcom/daimajia/swipe/SwipeLayout;)Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    move-result-object v1

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_146

    goto/16 :goto_7a

    .line 263
    :pswitch_1b
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    return v0

    .line 265
    :pswitch_22
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    if-ge p2, v0, :cond_31

    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    return v0

    .line 266
    :cond_31
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I
    invoke-static {v1}, Lcom/daimajia/swipe/SwipeLayout;->access$100(Lcom/daimajia/swipe/SwipeLayout;)I

    move-result v1

    add-int/2addr v0, v1

    if-le p2, v0, :cond_7a

    .line 267
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I
    invoke-static {v1}, Lcom/daimajia/swipe/SwipeLayout;->access$100(Lcom/daimajia/swipe/SwipeLayout;)I

    move-result v1

    add-int/2addr v0, v1

    return v0

    .line 270
    :pswitch_4e
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I
    invoke-static {v1}, Lcom/daimajia/swipe/SwipeLayout;->access$100(Lcom/daimajia/swipe/SwipeLayout;)I

    move-result v1

    sub-int/2addr v0, v1

    if-ge p2, v0, :cond_6b

    .line 271
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I
    invoke-static {v1}, Lcom/daimajia/swipe/SwipeLayout;->access$100(Lcom/daimajia/swipe/SwipeLayout;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0

    .line 273
    :cond_6b
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    if-le p2, v0, :cond_7a

    .line 274
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    return v0

    :cond_7a
    :goto_7a
    goto/16 :goto_144

    .line 278
    :cond_7c
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v3

    .line 279
    .local v3, "surfaceView":Landroid/view/View;
    if-nez v3, :cond_86

    const/4 v4, 0x0

    goto :goto_8a

    :cond_86
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v4

    .line 280
    .local v4, "surfaceViewTop":I
    :goto_8a
    sget-object v0, Lcom/daimajia/swipe/SwipeLayout$4;->$SwitchMap$com$daimajia$swipe$SwipeLayout$DragEdge:[I

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    invoke-static {v1}, Lcom/daimajia/swipe/SwipeLayout;->access$000(Lcom/daimajia/swipe/SwipeLayout;)Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    move-result-object v1

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_152

    goto/16 :goto_144

    .line 283
    :pswitch_9d
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    return v0

    .line 285
    :pswitch_a4
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mShowMode:Lcom/daimajia/swipe/SwipeLayout$ShowMode;
    invoke-static {v0}, Lcom/daimajia/swipe/SwipeLayout;->access$200(Lcom/daimajia/swipe/SwipeLayout;)Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->PullOut:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    if-ne v0, v1, :cond_bd

    .line 286
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    if-le p2, v0, :cond_144

    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    return v0

    .line 288
    :cond_bd
    add-int v0, v4, p3

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v1

    if-ge v0, v1, :cond_ce

    .line 289
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    return v0

    .line 290
    :cond_ce
    add-int v0, v4, p3

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v1

    iget-object v2, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I
    invoke-static {v2}, Lcom/daimajia/swipe/SwipeLayout;->access$100(Lcom/daimajia/swipe/SwipeLayout;)I

    move-result v2

    add-int/2addr v1, v2

    if-le v0, v1, :cond_144

    .line 291
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I
    invoke-static {v1}, Lcom/daimajia/swipe/SwipeLayout;->access$100(Lcom/daimajia/swipe/SwipeLayout;)I

    move-result v1

    add-int/2addr v0, v1

    return v0

    .line 295
    :pswitch_ed
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mShowMode:Lcom/daimajia/swipe/SwipeLayout$ShowMode;
    invoke-static {v0}, Lcom/daimajia/swipe/SwipeLayout;->access$200(Lcom/daimajia/swipe/SwipeLayout;)Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->PullOut:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    if-ne v0, v1, :cond_114

    .line 296
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I
    invoke-static {v1}, Lcom/daimajia/swipe/SwipeLayout;->access$100(Lcom/daimajia/swipe/SwipeLayout;)I

    move-result v1

    sub-int/2addr v0, v1

    if-ge p2, v0, :cond_144

    .line 297
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I
    invoke-static {v1}, Lcom/daimajia/swipe/SwipeLayout;->access$100(Lcom/daimajia/swipe/SwipeLayout;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0

    .line 299
    :cond_114
    add-int v0, v4, p3

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v1

    if-lt v0, v1, :cond_125

    .line 300
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    return v0

    .line 301
    :cond_125
    add-int v0, v4, p3

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v1

    iget-object v2, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I
    invoke-static {v2}, Lcom/daimajia/swipe/SwipeLayout;->access$100(Lcom/daimajia/swipe/SwipeLayout;)I

    move-result v2

    sub-int/2addr v1, v2

    if-gt v0, v1, :cond_144

    .line 302
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I
    invoke-static {v1}, Lcom/daimajia/swipe/SwipeLayout;->access$100(Lcom/daimajia/swipe/SwipeLayout;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0

    .line 306
    .end local v3    # "surfaceView":Landroid/view/View;
    .end local v4    # "surfaceViewTop":I
    :cond_144
    :goto_144
    return p2

    nop

    :pswitch_data_146
    .packed-switch 0x1
        :pswitch_22
        :pswitch_4e
        :pswitch_1b
        :pswitch_1b
    .end packed-switch

    :pswitch_data_152
    .packed-switch 0x1
        :pswitch_a4
        :pswitch_ed
        :pswitch_9d
        :pswitch_9d
    .end packed-switch
.end method

.method public getViewHorizontalDragRange(Landroid/view/View;)I
    .registers 3
    .param p1, "child"    # Landroid/view/View;

    .line 320
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I
    invoke-static {v0}, Lcom/daimajia/swipe/SwipeLayout;->access$100(Lcom/daimajia/swipe/SwipeLayout;)I

    move-result v0

    return v0
.end method

.method public getViewVerticalDragRange(Landroid/view/View;)I
    .registers 3
    .param p1, "child"    # Landroid/view/View;

    .line 325
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mDragDistance:I
    invoke-static {v0}, Lcom/daimajia/swipe/SwipeLayout;->access$100(Lcom/daimajia/swipe/SwipeLayout;)I

    move-result v0

    return v0
.end method

.method public onViewPositionChanged(Landroid/view/View;IIII)V
    .registers 19
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "dx"    # I
    .param p5, "dy"    # I

    .line 342
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v4

    .line 343
    .local v4, "surfaceView":Landroid/view/View;
    if-nez v4, :cond_9

    return-void

    .line 344
    :cond_9
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getCurrentBottomView()Landroid/view/View;

    move-result-object v5

    .line 345
    .local v5, "currentBottomView":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v6

    .line 346
    .local v6, "evLeft":I
    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v7

    .line 347
    .local v7, "evRight":I
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v8

    .line 348
    .local v8, "evTop":I
    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v9

    .line 349
    .local v9, "evBottom":I
    if-ne p1, v4, :cond_4f

    .line 351
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mShowMode:Lcom/daimajia/swipe/SwipeLayout$ShowMode;
    invoke-static {v0}, Lcom/daimajia/swipe/SwipeLayout;->access$200(Lcom/daimajia/swipe/SwipeLayout;)Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->PullOut:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    if-ne v0, v1, :cond_10a

    if-eqz v5, :cond_10a

    .line 352
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    invoke-static {v0}, Lcom/daimajia/swipe/SwipeLayout;->access$000(Lcom/daimajia/swipe/SwipeLayout;)Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Left:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-eq v0, v1, :cond_41

    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    invoke-static {v0}, Lcom/daimajia/swipe/SwipeLayout;->access$000(Lcom/daimajia/swipe/SwipeLayout;)Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v0, v1, :cond_48

    .line 353
    :cond_41
    move/from16 v0, p4

    invoke-virtual {v5, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    goto/16 :goto_10a

    .line 355
    :cond_48
    move/from16 v0, p5

    invoke-virtual {v5, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    goto/16 :goto_10a

    .line 359
    :cond_4f
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getBottomViews()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10a

    .line 361
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mShowMode:Lcom/daimajia/swipe/SwipeLayout$ShowMode;
    invoke-static {v0}, Lcom/daimajia/swipe/SwipeLayout;->access$200(Lcom/daimajia/swipe/SwipeLayout;)Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$ShowMode;->PullOut:Lcom/daimajia/swipe/SwipeLayout$ShowMode;

    if-ne v0, v1, :cond_71

    .line 362
    move/from16 v0, p4

    invoke-virtual {v4, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 363
    move/from16 v0, p5

    invoke-virtual {v4, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    goto/16 :goto_10a

    .line 365
    :cond_71
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    invoke-static {v1}, Lcom/daimajia/swipe/SwipeLayout;->access$000(Lcom/daimajia/swipe/SwipeLayout;)Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    move-result-object v1

    # invokes: Lcom/daimajia/swipe/SwipeLayout;->computeBottomLayDown(Lcom/daimajia/swipe/SwipeLayout$DragEdge;)Landroid/graphics/Rect;
    invoke-static {v0, v1}, Lcom/daimajia/swipe/SwipeLayout;->access$400(Lcom/daimajia/swipe/SwipeLayout;Lcom/daimajia/swipe/SwipeLayout$DragEdge;)Landroid/graphics/Rect;

    move-result-object v10

    .line 366
    .local v10, "rect":Landroid/graphics/Rect;
    if-eqz v5, :cond_8a

    .line 367
    iget v0, v10, Landroid/graphics/Rect;->left:I

    iget v1, v10, Landroid/graphics/Rect;->top:I

    iget v2, v10, Landroid/graphics/Rect;->right:I

    iget v3, v10, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 370
    :cond_8a
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v0

    add-int v11, v0, p4

    .local v11, "newLeft":I
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v0

    add-int v12, v0, p5

    .line 372
    .local v12, "newTop":I
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    invoke-static {v0}, Lcom/daimajia/swipe/SwipeLayout;->access$000(Lcom/daimajia/swipe/SwipeLayout;)Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Left:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v0, v1, :cond_af

    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    if-ge v11, v0, :cond_af

    .line 373
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v11

    goto :goto_f9

    .line 374
    :cond_af
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    invoke-static {v0}, Lcom/daimajia/swipe/SwipeLayout;->access$000(Lcom/daimajia/swipe/SwipeLayout;)Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Right:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v0, v1, :cond_c8

    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v0

    if-le v11, v0, :cond_c8

    .line 375
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingLeft()I

    move-result v11

    goto :goto_f9

    .line 376
    :cond_c8
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    invoke-static {v0}, Lcom/daimajia/swipe/SwipeLayout;->access$000(Lcom/daimajia/swipe/SwipeLayout;)Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Top:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v0, v1, :cond_e1

    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    if-ge v12, v0, :cond_e1

    .line 377
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v12

    goto :goto_f9

    .line 378
    :cond_e1
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mCurrentDragEdge:Lcom/daimajia/swipe/SwipeLayout$DragEdge;
    invoke-static {v0}, Lcom/daimajia/swipe/SwipeLayout;->access$000(Lcom/daimajia/swipe/SwipeLayout;)Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$DragEdge;->Bottom:Lcom/daimajia/swipe/SwipeLayout$DragEdge;

    if-ne v0, v1, :cond_f9

    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v0

    if-le v12, v0, :cond_f9

    .line 379
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getPaddingTop()I

    move-result v12

    .line 381
    :cond_f9
    :goto_f9
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, v11

    iget-object v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v1}, Lcom/daimajia/swipe/SwipeLayout;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v12

    invoke-virtual {v4, v11, v12, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 385
    .end local v10    # "rect":Landroid/graphics/Rect;
    .end local v11    # "newLeft":I
    .end local v12    # "newTop":I
    :cond_10a
    :goto_10a
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0, v6, v8, v7, v9}, Lcom/daimajia/swipe/SwipeLayout;->dispatchRevealEvent(IIII)V

    .line 387
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    move/from16 v1, p4

    move/from16 v2, p5

    invoke-virtual {v0, v6, v8, v1, v2}, Lcom/daimajia/swipe/SwipeLayout;->dispatchSwipeEvent(IIII)V

    .line 389
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->invalidate()V

    .line 390
    return-void
.end method

.method public onViewReleased(Landroid/view/View;FF)V
    .registers 8
    .param p1, "releasedChild"    # Landroid/view/View;
    .param p2, "xvel"    # F
    .param p3, "yvel"    # F

    .line 331
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/widget/ViewDragHelper$Callback;->onViewReleased(Landroid/view/View;FF)V

    .line 332
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    # getter for: Lcom/daimajia/swipe/SwipeLayout;->mSwipeListeners:Ljava/util/List;
    invoke-static {v0}, Lcom/daimajia/swipe/SwipeLayout;->access$300(Lcom/daimajia/swipe/SwipeLayout;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/daimajia/swipe/SwipeLayout$SwipeListener;

    .line 333
    .local v3, "l":Lcom/daimajia/swipe/SwipeLayout$SwipeListener;
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-interface {v3, v0, p2, p3}, Lcom/daimajia/swipe/SwipeLayout$SwipeListener;->onHandRelease(Lcom/daimajia/swipe/SwipeLayout;FF)V

    .line 334
    .end local v3    # "l":Lcom/daimajia/swipe/SwipeLayout$SwipeListener;
    goto :goto_d

    .line 335
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_20
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    iget-boolean v1, p0, Lcom/daimajia/swipe/SwipeLayout$1;->isCloseBeforeDrag:Z

    invoke-virtual {v0, p2, p3, v1}, Lcom/daimajia/swipe/SwipeLayout;->processHandRelease(FFZ)V

    .line 337
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->invalidate()V

    .line 338
    return-void
.end method

.method public tryCaptureView(Landroid/view/View;I)Z
    .registers 6
    .param p1, "child"    # Landroid/view/View;
    .param p2, "pointerId"    # I

    .line 311
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v0

    if-eq p1, v0, :cond_14

    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getBottomViews()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    :cond_14
    const/4 v2, 0x1

    goto :goto_17

    :cond_16
    const/4 v2, 0x0

    .line 312
    .local v2, "result":Z
    :goto_17
    if-eqz v2, :cond_28

    .line 313
    iget-object v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->this$0:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getOpenStatus()Lcom/daimajia/swipe/SwipeLayout$Status;

    move-result-object v0

    sget-object v1, Lcom/daimajia/swipe/SwipeLayout$Status;->Close:Lcom/daimajia/swipe/SwipeLayout$Status;

    if-ne v0, v1, :cond_25

    const/4 v0, 0x1

    goto :goto_26

    :cond_25
    const/4 v0, 0x0

    :goto_26
    iput-boolean v0, p0, Lcom/daimajia/swipe/SwipeLayout$1;->isCloseBeforeDrag:Z

    .line 315
    :cond_28
    return v2
.end method

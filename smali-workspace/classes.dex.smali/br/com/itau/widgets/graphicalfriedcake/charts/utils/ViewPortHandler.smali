.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;
.super Ljava/lang/Object;
.source "ViewPortHandler.java"


# instance fields
.field protected mChartHeight:F

.field protected mChartWidth:F

.field protected mContentRect:Landroid/graphics/RectF;

.field protected final mMatrixTouch:Landroid/graphics/Matrix;

.field private mMaxScaleX:F

.field private mMinScaleX:F

.field private mMinScaleY:F

.field private mScaleX:F

.field private mScaleY:F

.field private mTransOffsetX:F

.field private mTransOffsetY:F


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMatrixTouch:Landroid/graphics/Matrix;

    .line 14
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    .line 16
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mChartWidth:F

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mChartHeight:F

    .line 20
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMinScaleY:F

    .line 23
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMinScaleX:F

    .line 26
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMaxScaleX:F

    .line 29
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mScaleX:F

    .line 32
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mScaleY:F

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mTransOffsetX:F

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mTransOffsetY:F

    .line 42
    return-void
.end method


# virtual methods
.method public canZoomInMoreX()Z
    .registers 3

    .line 493
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mScaleX:F

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMaxScaleX:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method public canZoomOutMoreX()Z
    .registers 3

    .line 489
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mScaleX:F

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMinScaleX:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method public declared-synchronized centerViewPort([FLandroid/view/View;)V
    .registers 8
    .param p1, "transformedPts"    # [F
    .param p2, "view"    # Landroid/view/View;

    monitor-enter p0

    .line 230
    :try_start_1
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 231
    .local v2, "save":Landroid/graphics/Matrix;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMatrixTouch:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 233
    const/4 v0, 0x0

    aget v0, p1, v0

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->offsetLeft()F

    move-result v1

    sub-float v3, v0, v1

    .line 234
    .local v3, "x":F
    const/4 v0, 0x1

    aget v0, p1, v0

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->offsetTop()F

    move-result v1

    sub-float v4, v0, v1

    .line 238
    .local v4, "y":F
    neg-float v0, v3

    neg-float v1, v4

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 240
    const/4 v0, 0x1

    invoke-virtual {p0, v2, p2, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->refresh(Landroid/graphics/Matrix;Landroid/view/View;Z)Landroid/graphics/Matrix;
    :try_end_26
    .catchall {:try_start_1 .. :try_end_26} :catchall_28

    .line 241
    monitor-exit p0

    return-void

    .end local v2    # "save":Landroid/graphics/Matrix;
    .end local v3    # "x":F
    .end local v4    # "y":F
    :catchall_28
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public contentBottom()F
    .registers 2

    .line 107
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    return v0
.end method

.method public contentHeight()F
    .registers 2

    .line 115
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    return v0
.end method

.method public contentLeft()F
    .registers 2

    .line 99
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    return v0
.end method

.method public contentRight()F
    .registers 2

    .line 103
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    return v0
.end method

.method public contentTop()F
    .registers 2

    .line 95
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    return v0
.end method

.method public contentWidth()F
    .registers 2

    .line 111
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    return v0
.end method

.method public fitScreen()Landroid/graphics/Matrix;
    .registers 5

    .line 197
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMinScaleX:F

    .line 198
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMinScaleY:F

    .line 200
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 201
    .local v2, "save":Landroid/graphics/Matrix;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMatrixTouch:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 203
    const/16 v0, 0x9

    new-array v3, v0, [F

    .line 205
    .local v3, "vals":[F
    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->getValues([F)V

    .line 208
    const/4 v0, 0x0

    const/4 v1, 0x2

    aput v0, v3, v1

    .line 209
    const/4 v0, 0x0

    const/4 v1, 0x5

    aput v0, v3, v1

    .line 210
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    aput v0, v3, v1

    .line 211
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x4

    aput v0, v3, v1

    .line 213
    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->setValues([F)V

    .line 215
    return-object v2
.end method

.method public getChartHeight()F
    .registers 2

    .line 127
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mChartHeight:F

    return v0
.end method

.method public getChartWidth()F
    .registers 2

    .line 131
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mChartWidth:F

    return v0
.end method

.method public getContentCenter()Landroid/graphics/PointF;
    .registers 4

    .line 123
    new-instance v0, Landroid/graphics/PointF;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0
.end method

.method public getContentRect()Landroid/graphics/RectF;
    .registers 2

    .line 119
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getMatrixTouch()Landroid/graphics/Matrix;
    .registers 2

    .line 360
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMatrixTouch:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getScaleX()F
    .registers 2

    .line 411
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mScaleX:F

    return v0
.end method

.method public getScaleY()F
    .registers 2

    .line 418
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mScaleY:F

    return v0
.end method

.method public hasChartDimens()Z
    .registers 3

    .line 66
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mChartHeight:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_10

    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mChartWidth:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_10

    .line 67
    const/4 v0, 0x1

    return v0

    .line 69
    :cond_10
    const/4 v0, 0x0

    return v0
.end method

.method public hasNoDragOffset()Z
    .registers 3

    .line 485
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mTransOffsetX:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_10

    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mTransOffsetY:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_10

    const/4 v0, 0x1

    goto :goto_11

    :cond_10
    const/4 v0, 0x0

    :goto_11
    return v0
.end method

.method public isFullyZoomedOut()Z
    .registers 2

    .line 428
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->isFullyZoomedOutX()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->isFullyZoomedOutY()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 429
    const/4 v0, 0x1

    return v0

    .line 431
    :cond_e
    const/4 v0, 0x0

    return v0
.end method

.method public isFullyZoomedOutX()Z
    .registers 3

    .line 453
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mScaleX:F

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMinScaleX:F

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_10

    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMinScaleX:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_12

    .line 454
    :cond_10
    const/4 v0, 0x0

    return v0

    .line 456
    :cond_12
    const/4 v0, 0x1

    return v0
.end method

.method public isFullyZoomedOutY()Z
    .registers 3

    .line 440
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mScaleY:F

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMinScaleY:F

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_10

    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMinScaleY:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_12

    .line 441
    :cond_10
    const/4 v0, 0x0

    return v0

    .line 443
    :cond_12
    const/4 v0, 0x1

    return v0
.end method

.method public isInBounds(FF)Z
    .registers 4
    .param p1, "x"    # F
    .param p2, "y"    # F

    .line 383
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->isInBoundsX(F)Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p0, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->isInBoundsY(F)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 384
    const/4 v0, 0x1

    return v0

    .line 386
    :cond_e
    const/4 v0, 0x0

    return v0
.end method

.method public isInBoundsBottom(F)Z
    .registers 4
    .param p1, "y"    # F

    .line 403
    const/high16 v0, 0x42c80000    # 100.0f

    mul-float/2addr v0, p1

    float-to-int v0, v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float p1, v0, v1

    .line 404
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, p1

    if-ltz v0, :cond_13

    const/4 v0, 0x1

    goto :goto_14

    :cond_13
    const/4 v0, 0x0

    :goto_14
    return v0
.end method

.method public isInBoundsLeft(F)Z
    .registers 3
    .param p1, "x"    # F

    .line 390
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method public isInBoundsRight(F)Z
    .registers 4
    .param p1, "x"    # F

    .line 394
    const/high16 v0, 0x42c80000    # 100.0f

    mul-float/2addr v0, p1

    float-to-int v0, v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float p1, v0, v1

    .line 395
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, p1

    if-ltz v0, :cond_13

    const/4 v0, 0x1

    goto :goto_14

    :cond_13
    const/4 v0, 0x0

    :goto_14
    return v0
.end method

.method public isInBoundsTop(F)Z
    .registers 3
    .param p1, "y"    # F

    .line 399
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    cmpg-float v0, v0, p1

    if-gtz v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method public isInBoundsX(F)Z
    .registers 3
    .param p1, "x"    # F

    .line 369
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->isInBoundsLeft(F)Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->isInBoundsRight(F)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 370
    const/4 v0, 0x1

    return v0

    .line 372
    :cond_e
    const/4 v0, 0x0

    return v0
.end method

.method public isInBoundsY(F)Z
    .registers 3
    .param p1, "y"    # F

    .line 376
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->isInBoundsTop(F)Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->isInBoundsBottom(F)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 377
    const/4 v0, 0x1

    return v0

    .line 379
    :cond_e
    const/4 v0, 0x0

    return v0
.end method

.method public limitTransAndScale(Landroid/graphics/Matrix;Landroid/graphics/RectF;)V
    .registers 17
    .param p1, "matrix"    # Landroid/graphics/Matrix;
    .param p2, "content"    # Landroid/graphics/RectF;

    .line 270
    const/16 v0, 0x9

    new-array v3, v0, [F

    .line 271
    .local v3, "vals":[F
    invoke-virtual {p1, v3}, Landroid/graphics/Matrix;->getValues([F)V

    .line 273
    const/4 v0, 0x2

    aget v4, v3, v0

    .line 274
    .local v4, "curTransX":F
    const/4 v0, 0x0

    aget v5, v3, v0

    .line 276
    .local v5, "curScaleX":F
    const/4 v0, 0x5

    aget v6, v3, v0

    .line 277
    .local v6, "curTransY":F
    const/4 v0, 0x4

    aget v7, v3, v0

    .line 280
    .local v7, "curScaleY":F
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMinScaleX:F

    invoke-static {v0, v5}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMaxScaleX:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mScaleX:F

    .line 283
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMinScaleY:F

    invoke-static {v0, v7}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mScaleY:F

    .line 285
    const/4 v8, 0x0

    .line 286
    .local v8, "width":F
    const/4 v9, 0x0

    .line 288
    .local v9, "height":F
    if-eqz p2, :cond_35

    .line 289
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v8

    .line 290
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v9

    .line 293
    :cond_35
    neg-float v0, v8

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mScaleX:F

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v1, v2

    mul-float v10, v0, v1

    .line 294
    .local v10, "maxTransX":F
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mTransOffsetX:F

    sub-float v0, v10, v0

    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mTransOffsetX:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v11

    .line 300
    .local v11, "newTransX":F
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mScaleY:F

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v0, v1

    mul-float v12, v9, v0

    .line 301
    .local v12, "maxTransY":F
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mTransOffsetY:F

    add-float/2addr v0, v12

    invoke-static {v6, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mTransOffsetY:F

    neg-float v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v13

    .line 307
    .local v13, "newTransY":F
    const/4 v0, 0x2

    aput v11, v3, v0

    .line 308
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mScaleX:F

    const/4 v1, 0x0

    aput v0, v3, v1

    .line 310
    const/4 v0, 0x5

    aput v13, v3, v0

    .line 311
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mScaleY:F

    const/4 v1, 0x4

    aput v0, v3, v1

    .line 313
    invoke-virtual {p1, v3}, Landroid/graphics/Matrix;->setValues([F)V

    .line 314
    return-void
.end method

.method public offsetBottom()F
    .registers 3

    .line 91
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mChartHeight:F

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, v1

    return v0
.end method

.method public offsetLeft()F
    .registers 2

    .line 79
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    return v0
.end method

.method public offsetRight()F
    .registers 3

    .line 83
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mChartWidth:F

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v1

    return v0
.end method

.method public offsetTop()F
    .registers 2

    .line 87
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    return v0
.end method

.method public refresh(Landroid/graphics/Matrix;Landroid/view/View;Z)Landroid/graphics/Matrix;
    .registers 6
    .param p1, "newMatrix"    # Landroid/graphics/Matrix;
    .param p2, "chart"    # Landroid/view/View;
    .param p3, "invalidate"    # Z

    .line 251
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMatrixTouch:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 254
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMatrixTouch:Landroid/graphics/Matrix;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->limitTransAndScale(Landroid/graphics/Matrix;Landroid/graphics/RectF;)V

    .line 256
    if-eqz p3, :cond_11

    .line 257
    invoke-virtual {p2}, Landroid/view/View;->invalidate()V

    .line 259
    :cond_11
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMatrixTouch:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 260
    return-object p1
.end method

.method public restrainViewPort(FFFF)V
    .registers 8
    .param p1, "offsetLeft"    # F
    .param p2, "offsetTop"    # F
    .param p3, "offsetRight"    # F
    .param p4, "offsetBottom"    # F

    .line 74
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mChartWidth:F

    sub-float/2addr v1, p3

    iget v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mChartHeight:F

    sub-float/2addr v2, p4

    invoke-virtual {v0, p1, p2, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 76
    return-void
.end method

.method public setChartDimens(FF)V
    .registers 7
    .param p1, "width"    # F
    .param p2, "height"    # F

    .line 53
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->offsetLeft()F

    move-result v0

    .line 54
    .local v0, "offsetLeft":F
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->offsetTop()F

    move-result v1

    .line 55
    .local v1, "offsetTop":F
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->offsetRight()F

    move-result v2

    .line 56
    .local v2, "offsetRight":F
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->offsetBottom()F

    move-result v3

    .line 58
    .local v3, "offsetBottom":F
    iput p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mChartHeight:F

    .line 59
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mChartWidth:F

    .line 61
    invoke-virtual {p0, v0, v1, v2, v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->restrainViewPort(FFFF)V

    .line 63
    return-void
.end method

.method public setDragOffsetX(F)V
    .registers 3
    .param p1, "offset"    # F

    .line 466
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mTransOffsetX:F

    .line 467
    return-void
.end method

.method public setDragOffsetY(F)V
    .registers 3
    .param p1, "offset"    # F

    .line 476
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mTransOffsetY:F

    .line 477
    return-void
.end method

.method public setMaximumScaleX(F)V
    .registers 4
    .param p1, "xScale"    # F

    .line 328
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMaxScaleX:F

    .line 330
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMatrixTouch:Landroid/graphics/Matrix;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->limitTransAndScale(Landroid/graphics/Matrix;Landroid/graphics/RectF;)V

    .line 331
    return-void
.end method

.method public setMinMaxScaleX(FF)V
    .registers 5
    .param p1, "minScaleX"    # F
    .param p2, "maxScaleX"    # F

    .line 335
    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_8

    .line 336
    const/high16 p1, 0x3f800000    # 1.0f

    .line 338
    :cond_8
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMinScaleX:F

    .line 339
    iput p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMaxScaleX:F

    .line 341
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMatrixTouch:Landroid/graphics/Matrix;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->limitTransAndScale(Landroid/graphics/Matrix;Landroid/graphics/RectF;)V

    .line 342
    return-void
.end method

.method public setMinimumScaleX(F)V
    .registers 4
    .param p1, "xScale"    # F

    .line 318
    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_8

    .line 319
    const/high16 p1, 0x3f800000    # 1.0f

    .line 321
    :cond_8
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMinScaleX:F

    .line 323
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMatrixTouch:Landroid/graphics/Matrix;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->limitTransAndScale(Landroid/graphics/Matrix;Landroid/graphics/RectF;)V

    .line 324
    return-void
.end method

.method public setMinimumScaleY(F)V
    .registers 4
    .param p1, "yScale"    # F

    .line 346
    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_8

    .line 347
    const/high16 p1, 0x3f800000    # 1.0f

    .line 349
    :cond_8
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMinScaleY:F

    .line 351
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMatrixTouch:Landroid/graphics/Matrix;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mContentRect:Landroid/graphics/RectF;

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->limitTransAndScale(Landroid/graphics/Matrix;Landroid/graphics/RectF;)V

    .line 352
    return-void
.end method

.method public zoom(FFFF)Landroid/graphics/Matrix;
    .registers 7
    .param p1, "scaleX"    # F
    .param p2, "scaleY"    # F
    .param p3, "x"    # F
    .param p4, "y"    # F

    .line 181
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    .line 182
    .local v1, "save":Landroid/graphics/Matrix;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMatrixTouch:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 186
    invoke-virtual {v1, p1, p2, p3, p4}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 188
    return-object v1
.end method

.method public zoomIn(FF)Landroid/graphics/Matrix;
    .registers 6
    .param p1, "x"    # F
    .param p2, "y"    # F

    .line 148
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 149
    .local v2, "save":Landroid/graphics/Matrix;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMatrixTouch:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 151
    const v0, 0x3fb33333    # 1.4f

    const v1, 0x3fb33333    # 1.4f

    invoke-virtual {v2, v0, v1, p1, p2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 153
    return-object v2
.end method

.method public zoomOut(FF)Landroid/graphics/Matrix;
    .registers 6
    .param p1, "x"    # F
    .param p2, "y"    # F

    .line 162
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 163
    .local v2, "save":Landroid/graphics/Matrix;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->mMatrixTouch:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 165
    const v0, 0x3f333333    # 0.7f

    const v1, 0x3f333333    # 0.7f

    invoke-virtual {v2, v0, v1, p1, p2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 167
    return-object v2
.end method

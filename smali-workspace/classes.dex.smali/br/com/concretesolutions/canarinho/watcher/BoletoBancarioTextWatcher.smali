.class public final Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;
.super Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;
.source "BoletoBancarioTextWatcher.java"


# static fields
.field private static final BOLETO_NORMAL:[C

.field private static final BOLETO_TRIBUTO:[C

.field private static final FILTRO_NORMAL:[Landroid/text/InputFilter;

.field private static final FILTRO_TRIBUTO:[Landroid/text/InputFilter;


# instance fields
.field private final resultadoParcial:Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

.field private final validador:Lbr/com/concretesolutions/canarinho/validator/Validador;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 20
    const-string v0, "#####.##### #####.###### #####.###### # ##############"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->BOLETO_NORMAL:[C

    .line 21
    const-string v0, "############ ############ ############ ############"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->BOLETO_TRIBUTO:[C

    .line 22
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/text/InputFilter;

    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    sget-object v2, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->BOLETO_TRIBUTO:[C

    array-length v2, v2

    invoke-direct {v1, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->FILTRO_TRIBUTO:[Landroid/text/InputFilter;

    .line 24
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/text/InputFilter;

    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    sget-object v2, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->BOLETO_NORMAL:[C

    array-length v2, v2

    invoke-direct {v1, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->FILTRO_NORMAL:[Landroid/text/InputFilter;

    return-void
.end method

.method public constructor <init>(Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;)V
    .registers 3
    .param p1, "callbackErros"    # Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;

    .line 35
    invoke-direct {p0}, Lbr/com/concretesolutions/canarinho/watcher/BaseCanarinhoTextWatcher;-><init>()V

    .line 27
    invoke-static {}, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->getInstance()Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;

    move-result-object v0

    iput-object v0, p0, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->validador:Lbr/com/concretesolutions/canarinho/validator/Validador;

    .line 28
    new-instance v0, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    invoke-direct {v0}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;-><init>()V

    iput-object v0, p0, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->resultadoParcial:Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    .line 36
    invoke-virtual {p0, p1}, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->setEventoDeValidacao(Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;)V

    .line 37
    return-void
.end method

.method private ehTributo(Landroid/text/Editable;)Z
    .registers 4
    .param p1, "e"    # Landroid/text/Editable;

    .line 129
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    const/16 v1, 0x38

    if-ne v0, v1, :cond_b

    const/4 v0, 0x1

    goto :goto_c

    :cond_b
    const/4 v0, 0x0

    :goto_c
    return v0
.end method

.method private verificaFiltro(Landroid/text/Editable;Z)V
    .registers 5
    .param p1, "s"    # Landroid/text/Editable;
    .param p2, "tributo"    # Z

    .line 120
    if-eqz p2, :cond_14

    invoke-interface {p1}, Landroid/text/Editable;->getFilters()[Landroid/text/InputFilter;

    move-result-object v0

    sget-object v1, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->FILTRO_TRIBUTO:[Landroid/text/InputFilter;

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 121
    sget-object v0, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->FILTRO_TRIBUTO:[Landroid/text/InputFilter;

    invoke-interface {p1, v0}, Landroid/text/Editable;->setFilters([Landroid/text/InputFilter;)V

    goto :goto_27

    .line 122
    :cond_14
    if-nez p2, :cond_27

    invoke-interface {p1}, Landroid/text/Editable;->getFilters()[Landroid/text/InputFilter;

    move-result-object v0

    sget-object v1, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->FILTRO_NORMAL:[Landroid/text/InputFilter;

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_27

    .line 123
    sget-object v0, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->FILTRO_NORMAL:[Landroid/text/InputFilter;

    invoke-interface {p1, v0}, Landroid/text/Editable;->setFilters([Landroid/text/InputFilter;)V

    .line 125
    :cond_27
    :goto_27
    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .registers 7
    .param p1, "s"    # Landroid/text/Editable;

    .line 45
    invoke-virtual {p0}, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->isMudancaInterna()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 46
    return-void

    .line 50
    :cond_7
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_2d

    .line 51
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->resultadoParcial:Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->mensagem(Ljava/lang/String;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->parcialmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->totalmenteValido(Z)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    .line 52
    invoke-virtual {p0}, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->getEventoDeValidacao()Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;

    move-result-object v0

    if-eqz v0, :cond_2d

    .line 53
    invoke-virtual {p0}, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->getEventoDeValidacao()Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;

    move-result-object v0

    const-string v1, ""

    invoke-interface {v0, v1}, Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;->parcialmenteValido(Ljava/lang/String;)V

    .line 57
    :cond_2d
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_38

    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->verificaFiltro(Landroid/text/Editable;Z)V

    .line 59
    return-void

    .line 62
    :cond_38
    invoke-direct {p0, p1}, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->ehTributo(Landroid/text/Editable;)Z

    move-result v2

    .line 63
    .local v2, "tributo":Z
    if-eqz v2, :cond_41

    sget-object v3, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->BOLETO_TRIBUTO:[C

    goto :goto_43

    :cond_41
    sget-object v3, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->BOLETO_NORMAL:[C

    .line 64
    .local v3, "mascara":[C
    :goto_43
    invoke-direct {p0, p1, v2}, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->verificaFiltro(Landroid/text/Editable;Z)V

    .line 67
    invoke-virtual {p0, p1, v3}, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->trataAdicaoRemocaoDeCaracter(Landroid/text/Editable;[C)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 68
    .local v4, "builder":Ljava/lang/StringBuilder;
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->validador:Lbr/com/concretesolutions/canarinho/validator/Validador;

    iget-object v1, p0, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->resultadoParcial:Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    invoke-virtual {p0, v0, v1, p1, v4}, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->atualizaTexto(Lbr/com/concretesolutions/canarinho/validator/Validador;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;Landroid/text/Editable;Ljava/lang/StringBuilder;)V

    .line 69
    return-void
.end method

.method protected efetuaValidacao(Lbr/com/concretesolutions/canarinho/validator/Validador;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;Landroid/text/Editable;)V
    .registers 10
    .param p1, "validador"    # Lbr/com/concretesolutions/canarinho/validator/Validador;
    .param p2, "resultadoParcial"    # Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;
    .param p3, "s"    # Landroid/text/Editable;

    .line 78
    invoke-interface {p1, p3, p2}, Lbr/com/concretesolutions/canarinho/validator/Validador;->ehValido(Landroid/text/Editable;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    .line 80
    invoke-virtual {p0}, Lbr/com/concretesolutions/canarinho/watcher/BoletoBancarioTextWatcher;->getEventoDeValidacao()Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;

    move-result-object v2

    .line 82
    .local v2, "callbackErros":Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;
    if-nez v2, :cond_a

    .line 83
    return-void

    .line 86
    :cond_a
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 87
    .local v3, "valorAtual":Ljava/lang/String;
    invoke-virtual {p2}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->isParcialmenteValido()Z

    move-result v0

    if-nez v0, :cond_56

    .line 89
    invoke-virtual {p2}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->getMensagem()Ljava/lang/String;

    move-result-object v4

    .line 90
    .local v4, "mensagem":Ljava/lang/String;
    invoke-interface {v2, v3, v4}, Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;->invalido(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    instance-of v0, v2, Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacaoDeBoleto;

    if-eqz v0, :cond_55

    .line 96
    const-string v0, "Primeiro"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 97
    const/4 v5, 0x1

    .local v5, "bloco":I
    goto :goto_4f

    .line 98
    .end local v5    # "bloco":I
    :cond_29
    const-string v0, "Segundo"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 99
    const/4 v5, 0x2

    .local v5, "bloco":I
    goto :goto_4f

    .line 100
    .end local v5    # "bloco":I
    :cond_33
    const-string v0, "Terceiro"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 101
    const/4 v5, 0x3

    .local v5, "bloco":I
    goto :goto_4f

    .line 102
    .end local v5    # "bloco":I
    :cond_3d
    const-string v0, "Quarto"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_47

    .line 103
    const/4 v5, 0x4

    .local v5, "bloco":I
    goto :goto_4f

    .line 105
    .end local v5    # "bloco":I
    :cond_47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Valor n\u00e3o reconhecido para bloco"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    .local v5, "bloco":I
    :goto_4f
    move-object v0, v2

    check-cast v0, Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacaoDeBoleto;

    invoke-interface {v0, v3, v5}, Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacaoDeBoleto;->invalido(Ljava/lang/String;I)V

    .line 111
    .end local v4    # "mensagem":Ljava/lang/String;
    .end local v5    # "bloco":I
    :cond_55
    goto :goto_63

    :cond_56
    invoke-virtual {p2}, Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;->isValido()Z

    move-result v0

    if-nez v0, :cond_60

    .line 112
    invoke-interface {v2, v3}, Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;->parcialmenteValido(Ljava/lang/String;)V

    goto :goto_63

    .line 114
    :cond_60
    invoke-interface {v2, v3}, Lbr/com/concretesolutions/canarinho/watcher/evento/EventoDeValidacao;->totalmenteValido(Ljava/lang/String;)V

    .line 116
    :goto_63
    return-void
.end method

.class final Lbr/com/itau/sdk/android/core/endpoint/k;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final v:Ljava/util/regex/Pattern;

.field private static final w:Ljava/util/regex/Pattern;

.field private static final x:[B

.field private static y:I


# instance fields
.field protected final a:Ljava/lang/reflect/Method;

.field protected final b:Lokhttp3/Headers$Builder;

.field protected final c:Lokhttp3/Headers$Builder;

.field protected d:Ljava/lang/reflect/Type;

.field protected e:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

.field protected f:Z

.field protected g:Z

.field protected h:Z

.field protected i:Z

.field protected j:Z

.field protected k:Z

.field protected l:Ljava/lang/String;

.field protected m:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field protected n:Ljava/lang/String;

.field protected o:Ljava/lang/reflect/Type;

.field protected p:Ljava/lang/String;

.field protected q:Z

.field protected r:I

.field protected s:I

.field protected t:[Ljava/lang/annotation/Annotation;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 44
    const/16 v0, 0x46e

    new-array v0, v0, [B

    fill-array-data v0, :array_40

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v0, 0x1a

    sput v0, Lbr/com/itau/sdk/android/core/endpoint/k;->y:I

    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v1, 0x80

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0x1f3

    aget-byte v1, v1, v2

    const/16 v2, 0x44e

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->v:Ljava/util/regex/Pattern;

    .line 45
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v1, 0x50

    aget-byte v0, v0, v1

    neg-int v0, v0

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0x61

    aget-byte v1, v1, v2

    const/16 v2, 0x6c

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->w:Ljava/util/regex/Pattern;

    return-void

    nop

    :array_40
    .array-data 1
        0x26t
        -0x62t
        -0x58t
        0x3dt
        0x7t
        -0x33t
        0x4et
        -0x38t
        -0x13t
        0x2et
        0x4t
        -0x1t
        0x7t
        -0x33t
        0x4et
        -0x38t
        -0x13t
        0x2et
        -0x29t
        -0x2t
        0xdt
        0x27t
        -0x31t
        0x31t
        -0x32t
        0x2at
        0x4t
        0x0t
        -0x8t
        -0x44t
        0x4ft
        0x2t
        0x6t
        -0x53t
        0x54t
        0x3t
        -0x4t
        0x1t
        0x0t
        0x4t
        0x3t
        -0xet
        0x0t
        0x1dt
        -0x4t
        0x1t
        0x9t
        -0x6t
        0x13t
        -0x15t
        0x12t
        -0xct
        -0x44t
        0x43t
        0xet
        -0xat
        0x16t
        -0x58t
        0x44t
        -0x1t
        0xet
        0x1t
        0x2t
        0x6t
        -0x53t
        0x43t
        0x4t
        -0x44t
        0x42t
        -0x40t
        0x24t
        0x2dt
        -0x2t
        0x1t
        -0x6t
        -0x1t
        0x12t
        -0xat
        0x7t
        0x0t
        -0x3ft
        0x22t
        -0x4et
        0x23t
        0x20t
        0x3t
        0x9t
        -0x5t
        0xat
        -0x9t
        -0x43t
        0x42t
        0xet
        0x1t
        0x2t
        0x6t
        -0x12t
        0x14t
        -0xat
        0x7t
        0x0t
        -0x4dt
        0x47t
        0xat
        0x7t
        -0x6t
        -0x9t
        -0x43t
        0x20t
        -0x1t
        0xet
        -0x58t
        0x50t
        0x0t
        -0x8t
        -0x44t
        0x29t
        0xdt
        0x1t
        -0x3t
        -0x2ft
        0x4et
        -0x7t
        0x10t
        -0xbt
        0x8t
        -0xat
        -0x43t
        0x45t
        0x2t
        -0x1t
        0xat
        -0xat
        0x12t
        -0x10t
        0x14t
        -0xat
        0x7t
        0x0t
        -0x4dt
        0x4at
        0xbt
        -0x52t
        0x42t
        0xct
        0x1t
        0x4t
        0x9t
        -0x11t
        0x0t
        -0x35t
        -0xdt
        0x27t
        0x2at
        0x7t
        -0x6t
        -0x9t
        -0x29t
        -0x19t
        0x6t
        0x4ft
        -0x52t
        0x42t
        0xet
        -0x9t
        -0x43t
        0x6t
        0x4ft
        -0x44t
        0x12t
        0x12t
        -0x10t
        0xdt
        -0x7t
        0x10t
        -0xet
        0xet
        0x2t
        -0x52t
        0x50t
        -0x8t
        -0x45t
        0x42t
        -0x40t
        0x21t
        0xet
        0x1dt
        -0x4t
        0x1t
        0x9t
        -0x6t
        0x13t
        -0x15t
        0x12t
        -0xct
        -0xft
        0x1at
        -0x4t
        0xet
        -0x10t
        0xet
        0x2t
        -0x11t
        0xct
        -0x4bt
        0x4et
        -0x7t
        0x10t
        -0xbt
        0x8t
        -0xat
        -0x43t
        0x54t
        -0xat
        0x8t
        0x7t
        -0x8t
        -0x7t
        -0x43t
        0x4ft
        0x2t
        0x6t
        -0x53t
        0x43t
        0x4t
        -0x44t
        0x42t
        0xet
        0x1t
        0x2t
        0x6t
        -0x12t
        0x14t
        -0xet
        0x0t
        0x11t
        0x12t
        0x14t
        -0xbt
        -0x47t
        0x51t
        -0xet
        0x12t
        -0x10t
        0xdt
        -0x7t
        0x10t
        -0xet
        0xet
        -0x51t
        0x4ft
        -0xct
        0xdt
        -0x7t
        -0x44t
        0x4et
        0x9t
        -0x1t
        0x2t
        -0x53t
        0x4et
        -0xbt
        0x14t
        -0x10t
        0x6t
        -0x47t
        0x6t
        0x4ft
        -0x44t
        -0xdt
        0x27t
        0x2at
        0x7t
        -0x6t
        -0x9t
        -0x29t
        -0x19t
        0x6t
        0x4ft
        -0x1t
        0xct
        0x1t
        -0x2bt
        0x4et
        -0x7t
        0x10t
        -0xbt
        0x8t
        -0xat
        -0x43t
        0x42t
        0xet
        0x1t
        0x2t
        0x6t
        -0x12t
        0x14t
        -0xat
        0x7t
        0x0t
        -0x4dt
        0x4at
        0xbt
        -0x52t
        0x53t
        -0xct
        0xdt
        0x5t
        -0xbt
        0xat
        -0xct
        0x0t
        -0x43t
        0x9t
        0x3et
        -0x36t
        0x3at
        -0x38t
        -0x1t
        -0xbt
        0x21t
        0x8t
        -0x1t
        0x10t
        -0x27t
        -0xbt
        0x21t
        0x11t
        0x0t
        0x5t
        0x2t
        -0x27t
        -0xbt
        0x46t
        0x10t
        -0x10t
        -0x34t
        -0x4t
        0x6t
        0x13t
        0x14t
        0xdt
        -0x20t
        0x23t
        -0x2t
        -0x7t
        0xct
        -0xct
        0xft
        0x1t
        -0x52t
        0x4at
        0xbt
        -0x52t
        0x4ft
        0x2t
        0x6t
        -0x53t
        0x42t
        0x3t
        0x1t
        0x3t
        0xct
        0x5t
        -0x53t
        0x4at
        0x6t
        -0x4dt
        0x2et
        0x1dt
        -0x4t
        0x1t
        0x9t
        -0x6t
        0x13t
        -0x15t
        0x12t
        -0xct
        -0xft
        0x1at
        -0x4t
        0xet
        -0x10t
        0xet
        0x2t
        -0x11t
        0xct
        -0x4bt
        0x44t
        -0x1t
        0xct
        0x1t
        0x8t
        -0x44t
        0x1dt
        -0x4t
        0x1t
        0x9t
        -0x6t
        0x13t
        -0x15t
        0x12t
        -0xct
        -0xft
        0x1at
        -0x4t
        0xet
        -0x10t
        0xet
        0x2t
        -0x11t
        0xct
        -0x4bt
        0x42t
        0xet
        -0x9t
        -0x43t
        0x2et
        0x1dt
        -0x4t
        0x1t
        0x9t
        -0x6t
        0x13t
        -0x15t
        0x12t
        -0xct
        -0x44t
        0x4et
        -0x7t
        0x10t
        -0xbt
        0x8t
        -0xat
        0x10t
        -0x52t
        0x4et
        0x9t
        -0x1t
        0x2t
        -0x53t
        0x53t
        -0xct
        0x10t
        0x2t
        -0x2t
        -0x3t
        -0x4dt
        0x34t
        0x22t
        -0x1t
        -0x8t
        0x6t
        -0x6t
        0x22t
        0x0t
        -0x40t
        0x36t
        0xet
        -0xat
        0x16t
        -0x58t
        0x24t
        -0x1t
        0xct
        0x1t
        -0x2bt
        0x4et
        -0x7t
        0x10t
        -0xbt
        0x8t
        -0xat
        -0x43t
        0x44t
        -0x1t
        0xet
        0x1t
        0x2t
        0x6t
        -0x53t
        0x44t
        0xdt
        0x0t
        0x7t
        -0x12t
        0x9t
        0x6t
        -0x4dt
        0x21t
        0x3t
        0x2et
        -0xat
        0x16t
        -0x58t
        0x50t
        0x4t
        -0x51t
        0x21t
        0x15t
        0x26t
        -0x8t
        -0xat
        0x0t
        -0x14t
        0x27t
        0x0t
        -0x3t
        0x6t
        0x0t
        -0x45t
        0x13t
        0xet
        0x5t
        -0xct
        -0x5t
        0x3t
        -0x44t
        0x4et
        -0x7t
        0x10t
        -0xbt
        0x8t
        -0xat
        0x10t
        -0x52t
        0x44t
        -0x1t
        0xet
        0x1t
        0x2t
        0x6t
        -0x53t
        0x53t
        -0xct
        0x10t
        0x2t
        -0x2t
        -0x3t
        -0x4dt
        0x57t
        -0x6t
        -0x5t
        -0x4t
        -0x35t
        -0x2t
        -0x5t
        -0x2bt
        0x3t
        0x4t
        0x4ft
        -0x50t
        -0x1t
        0x45t
        0xct
        -0x9t
        0xft
        -0x52t
        0x4ft
        0x2t
        0x6t
        -0x53t
        0x44t
        0xdt
        0x0t
        0x7t
        -0x12t
        0x9t
        0x6t
        -0x4dt
        0x3t
        0x5at
        -0x55t
        0x4ft
        0xbt
        -0x5at
        0xdt
        0x2t
        -0xat
        0xet
        -0x10t
        0x14t
        -0xat
        0x7t
        0x0t
        -0x4dt
        0x44t
        -0x1t
        0xet
        0x1t
        0x2t
        0x6t
        -0x53t
        0x43t
        0x4t
        -0x44t
        0x4ft
        0x8t
        -0x8t
        0x1t
        -0x4bt
        0x47t
        0xat
        0x4t
        -0x51t
        0x2et
        0x1dt
        -0x4t
        0x1t
        0x9t
        -0x6t
        0x13t
        -0x15t
        0x12t
        -0xct
        -0x44t
        0x44t
        -0x1t
        0xct
        0x1t
        0x8t
        -0x7t
        0x10t
        -0xbt
        0x8t
        -0xat
        -0x43t
        0x4at
        0xbt
        -0x52t
        0x4et
        -0xbt
        0xet
        -0x9t
        -0x2t
        0x14t
        -0x4t
        0x4t
        0x8t
        -0x4at
        -0x19t
        0x2t
        -0xat
        0xet
        -0x10t
        0x14t
        -0xat
        0x7t
        0x0t
        -0x4dt
        0x44t
        -0x1t
        0xet
        0x1t
        0x2t
        0x6t
        -0x53t
        0x43t
        0x4t
        -0x44t
        0x4ft
        0x8t
        -0x8t
        0x1t
        -0x4bt
        0x47t
        0xat
        0x4t
        -0x51t
        0x2et
        0x1dt
        -0x4t
        0x1t
        0x9t
        -0x6t
        0x13t
        -0x15t
        0x12t
        -0xct
        -0xft
        0x1at
        -0x4t
        0xet
        -0x10t
        0xet
        0x2t
        -0x11t
        0xct
        -0x4bt
        0x44t
        -0x1t
        0xct
        0x1t
        0x8t
        0x9t
        0x49t
        -0xet
        0x12t
        -0x10t
        0xdt
        -0x7t
        0x10t
        -0xet
        0xet
        -0x51t
        0x4t
        -0x4t
        -0x16t
        0x23t
        -0xct
        0x8t
        0x4t
        -0x6t
        -0x11t
        0x22t
        -0x1t
        -0x2t
        0x0t
        -0x6t
        -0x2t
        -0x5t
        -0x2bt
        0x51t
        -0xet
        0x14t
        -0xbt
        -0x47t
        0x3t
        0x4t
        0x4ft
        -0x50t
        -0x1t
        0x4et
        0x9t
        -0x1t
        0x2t
        -0x53t
        0x54t
        0x2t
        -0x12t
        0x12t
        0x3t
        -0x53t
        0x58t
        -0xdt
        0xct
        -0xbt
        -0x47t
        0x8t
        0x9t
        -0x7t
        0x8t
        -0x4t
        -0x16t
        0x23t
        -0xct
        0x8t
        0x4t
        -0x6t
        -0xdt
        0xft
        -0x3t
        0xbt
        -0x2t
        -0x5t
        -0x2bt
        0x52t
        0x5t
        -0xft
        0xet
        0x8t
        -0x58t
        0x54t
        0x2t
        -0x1t
        -0x8t
        0x6t
        -0x6t
        -0x46t
        0x3t
        0x4t
        0x4ft
        -0x50t
        -0x1t
        0x4et
        0x9t
        -0x1t
        0x2t
        -0x53t
        0x4ft
        0x2t
        0x6t
        -0x53t
        0x49t
        -0x6t
        0x16t
        -0x10t
        -0x44t
        0x53t
        -0xct
        0xct
        -0x3t
        -0xat
        0x3t
        0x3t
        -0x44t
        0x43t
        0xbt
        0x4t
        -0xbt
        0x9t
        -0x3ct
        -0xdt
        0x27t
        0x2at
        0x4t
        -0x51t
        0x45t
        0x16t
        -0xat
        -0xct
        0xdt
        -0x3t
        -0x5t
        -0x42t
        0x52t
        0x5t
        -0xft
        0xet
        0x8t
        -0x58t
        0x51t
        -0xet
        0x12t
        -0x10t
        0xdt
        -0x7t
        0x10t
        -0xet
        0xet
        0x2t
        -0x52t
        0x56t
        -0x1t
        -0xdt
        -0x44t
        0x21t
        0x12t
        0x25t
        -0xft
        0xet
        0x8t
        -0x4at
        0x13t
        0x14t
        0xdt
        -0x20t
        0x23t
        -0x2t
        -0x7t
        0xct
        -0xct
        0xft
        0x1t
        -0x52t
        0x4at
        0xbt
        -0x52t
        0x4ft
        0x2t
        0x6t
        -0x53t
        0x42t
        0x3t
        0x1t
        0x3t
        0xct
        0x5t
        -0x53t
        0x4at
        0x6t
        -0x4dt
        0x2et
        0x1dt
        -0x4t
        0x1t
        0x9t
        -0x6t
        0x13t
        -0x15t
        0x12t
        -0xct
        -0x44t
        0x44t
        -0x1t
        0xct
        0x1t
        0x8t
        -0x44t
        0x29t
        -0x8t
        0x9t
        -0xat
        0x8t
        -0x3t
        -0x6t
        -0x44t
        0x21t
        0x3t
        0x2et
        -0xat
        0x16t
        -0x58t
        0x4et
        -0x7t
        0x10t
        -0xbt
        0x8t
        -0xat
        -0x43t
        0x42t
        0xet
        0x1t
        0x2t
        0x6t
        -0x12t
        0x14t
        -0xat
        0x7t
        0x0t
        0x6t
        -0x52t
        0x47t
        0xat
        0x7t
        -0x6t
        -0x9t
        -0x35t
        0x9t
        0x1et
        -0x3t
        0x4t
        0x2t
        0xet
        0x2t
        -0x52t
        0x57t
        -0x14t
        0xct
        0xat
        -0xft
        -0x44t
        0x4et
        0x9t
        -0x1t
        0x2t
        -0x53t
        0x43t
        0x4t
        -0x44t
        0x4at
        0x6t
        -0x4dt
        0x55t
        -0xbt
        -0x2t
        -0x44t
        0x47t
        0xat
        0x4t
        -0x4t
        -0x4ct
        0x3t
        0x2dt
        0x14t
        0xdt
        -0x7t
        -0x2at
        -0x19t
        0x37t
        0xct
        0xct
        0xat
        -0xft
        -0x42t
        0xdt
        -0xdt
        0x27t
        0x2at
        0x7t
        -0x6t
        -0x9t
        -0x29t
        -0x19t
        0x3t
        0x4t
        0x4ft
        -0x50t
        0x20t
        -0x52t
        0x34t
        0x7t
        -0x33t
        0x4et
        -0x38t
        -0x13t
        0x2et
        0x4t
        -0x1t
        0x7t
        -0x33t
        0x4et
        -0x38t
        -0x13t
        0x2et
        -0x29t
        -0x2t
        0xdt
        0x27t
        -0x31t
        0x31t
        -0x32t
        0x0t
        0x34t
        0x22t
        0x3t
        -0xct
        -0x37t
        0x40t
        0x4t
        -0x7t
        0x3t
        0x6t
        -0x33t
        -0x19t
        0x51t
        0x3t
        -0xct
        -0x37t
        0x40t
        0x4t
        -0x7t
        0x3t
        0x6t
        0x29t
        -0x8t
        0x9t
        -0xat
        0x8t
        -0x3t
        -0x6t
        -0x44t
        0x23t
        0x20t
        0x3t
        0x9t
        -0x5t
        0xat
        -0x9t
        -0x43t
        0x42t
        0xet
        0x1t
        0x2t
        0x6t
        -0x12t
        0x14t
        -0xat
        0x7t
        0x0t
        0x6t
        -0x52t
        0x47t
        0xat
        0x7t
        -0x6t
        -0x9t
        -0x37t
        -0xbt
        0x50t
        0x0t
        -0x1t
        0xet
        -0x58t
        0x50t
        0x0t
        -0x8t
        -0x44t
        0x42t
        0xct
        0x1t
        0x4t
        0x9t
        -0x11t
        0x0t
        -0x29t
        -0x19t
        0x21t
        -0x1at
        0x4ft
        -0x46t
        -0xbt
        0x21t
        -0x1at
        0x4ft
        -0x44t
        0x9t
        0x1et
        -0x3t
        0x4t
        0x2t
        0xet
        0x2t
        -0x52t
        0x42t
        0xet
        0x1t
        0x2t
        0x6t
        -0x12t
        0x14t
        -0xat
        0x7t
        0x0t
        -0x4dt
        0x4at
        0xbt
        -0x52t
        0x46t
        0x9t
        0x4t
        0x5t
        0x6t
        -0x4at
    .end array-data
.end method

.method constructor <init>(Ljava/lang/reflect/Method;)V
    .registers 3

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Lokhttp3/Headers$Builder;

    invoke-direct {v0}, Lokhttp3/Headers$Builder;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->b:Lokhttp3/Headers$Builder;

    .line 49
    new-instance v0, Lokhttp3/Headers$Builder;

    invoke-direct {v0}, Lokhttp3/Headers$Builder;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->c:Lokhttp3/Headers$Builder;

    .line 73
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->a:Ljava/lang/reflect/Method;

    .line 74
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/endpoint/k;->a()V

    .line 75
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/endpoint/k;->c()V

    .line 76
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/endpoint/k;->b()V

    .line 77
    return-void
.end method

.method constructor <init>(Ljava/lang/reflect/Method;Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;)V
    .registers 4

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Lokhttp3/Headers$Builder;

    invoke-direct {v0}, Lokhttp3/Headers$Builder;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->b:Lokhttp3/Headers$Builder;

    .line 49
    new-instance v0, Lokhttp3/Headers$Builder;

    invoke-direct {v0}, Lokhttp3/Headers$Builder;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->c:Lokhttp3/Headers$Builder;

    .line 80
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->a:Ljava/lang/reflect/Method;

    .line 81
    invoke-direct {p0, p2}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;)V

    .line 82
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/endpoint/k;->c()V

    .line 83
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/endpoint/k;->b()V

    .line 84
    return-void
.end method

.method private varargs a(ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;
    .registers 8

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/4 v2, 0x6

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v3, 0x70

    aget-byte v2, v2, v3

    const/16 v3, 0x19c

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0x23b

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v3, 0x66

    aget-byte v2, v2, v3

    const/16 v3, 0x162

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    return-object v0
.end method

.method private varargs a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;
    .registers 8

    .line 123
    array-length v0, p2

    if-lez v0, :cond_7

    .line 124
    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 126
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->a:Ljava/lang/reflect/Method;

    .line 127
    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v3, 0x23b

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v4, 0x44

    aget-byte v3, v3, v4

    const/16 v4, 0x162

    invoke-static {v2, v4, v3}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->a:Ljava/lang/reflect/Method;

    invoke-virtual {v2}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v3, 0x23b

    aget-byte v2, v2, v3

    add-int/lit8 v2, v2, -0x1

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v4, 0x1c9

    aget-byte v3, v3, v4

    const/16 v4, 0x1d2

    invoke-static {v2, v4, v3}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 126
    return-object v0
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    rsub-int p1, p1, 0x452

    sget-object v5, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/4 v4, 0x0

    rsub-int/lit8 p0, p0, 0x5b

    rsub-int/lit8 p2, p2, 0x70

    new-instance v0, Ljava/lang/String;

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v5, :cond_18

    move v2, p1

    move v3, p0

    :goto_13
    add-int/2addr v2, v3

    add-int/lit8 p2, v2, -0x1

    add-int/lit8 p1, p1, 0x1

    :cond_18
    int-to-byte v2, p2

    aput-byte v2, v1, v4

    if-ne v4, p0, :cond_22

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_22
    move v2, p2

    aget-byte v3, v5, p1

    add-int/lit8 v4, v4, 0x1

    goto :goto_13
.end method

.method static a(Ljava/lang/String;)Ljava/util/Set;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)Ljava/util/Set<Ljava/lang/String;>;"
        }
    .end annotation

    .line 91
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->w:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 92
    new-instance v2, Ljava/util/LinkedHashSet;

    invoke-direct {v2}, Ljava/util/LinkedHashSet;-><init>()V

    .line 93
    :goto_b
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 94
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 96
    :cond_1a
    return-object v2
.end method

.method private a()V
    .registers 14

    .line 136
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->a:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v6

    array-length v7, v6

    const/4 v8, 0x0

    :goto_8
    if-ge v8, v7, :cond_16a

    aget-object v9, v6, v8

    .line 138
    invoke-interface {v9}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v10

    .line 140
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/CALL;

    if-ne v10, v0, :cond_2c

    .line 141
    move-object v11, v9

    check-cast v11, Lbr/com/itau/sdk/android/core/endpoint/http/CALL;

    .line 142
    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/CALL;->method()Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    move-result-object v0

    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/CALL;->opKey()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/CALL;->path()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/CALL;->deviceId()Z

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 143
    goto/16 :goto_166

    :cond_2c
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/POST;

    if-ne v10, v0, :cond_46

    .line 144
    move-object v11, v9

    check-cast v11, Lbr/com/itau/sdk/android/core/endpoint/http/POST;

    .line 145
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->POST:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/POST;->opKey()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/POST;->path()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/POST;->deviceId()Z

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 146
    goto/16 :goto_166

    :cond_46
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/GET;

    if-ne v10, v0, :cond_60

    .line 147
    move-object v11, v9

    check-cast v11, Lbr/com/itau/sdk/android/core/endpoint/http/GET;

    .line 148
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->GET:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/GET;->opKey()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/GET;->path()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/GET;->deviceId()Z

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 149
    goto/16 :goto_166

    :cond_60
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/PUT;

    if-ne v10, v0, :cond_7a

    .line 150
    move-object v11, v9

    check-cast v11, Lbr/com/itau/sdk/android/core/endpoint/http/PUT;

    .line 151
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->PUT:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/PUT;->opKey()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/PUT;->path()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/PUT;->deviceId()Z

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 152
    goto/16 :goto_166

    :cond_7a
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/DELETE;

    if-ne v10, v0, :cond_94

    .line 153
    move-object v11, v9

    check-cast v11, Lbr/com/itau/sdk/android/core/endpoint/http/DELETE;

    .line 154
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->DELETE:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/DELETE;->opKey()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/DELETE;->path()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/DELETE;->deviceId()Z

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 155
    goto/16 :goto_166

    :cond_94
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/PATCH;

    if-ne v10, v0, :cond_ae

    .line 156
    move-object v11, v9

    check-cast v11, Lbr/com/itau/sdk/android/core/endpoint/http/PATCH;

    .line 157
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->PATCH:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/PATCH;->opKey()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/PATCH;->path()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v11}, Lbr/com/itau/sdk/android/core/endpoint/http/PATCH;->deviceId()Z

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 158
    goto/16 :goto_166

    :cond_ae
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/legacy/Middleware;

    if-ne v10, v0, :cond_ba

    .line 159
    move-object v11, v9

    check-cast v11, Lbr/com/itau/sdk/android/core/endpoint/legacy/Middleware;

    .line 160
    invoke-direct {p0, v11}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Lbr/com/itau/sdk/android/core/endpoint/legacy/Middleware;)V

    .line 161
    goto/16 :goto_166

    :cond_ba
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareUniversal;

    if-ne v10, v0, :cond_c6

    .line 162
    move-object v11, v9

    check-cast v11, Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareUniversal;

    .line 163
    invoke-direct {p0, v11}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareUniversal;)V

    .line 164
    goto/16 :goto_166

    :cond_c6
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/PreLogin;

    if-ne v10, v0, :cond_131

    .line 165
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->c:Lokhttp3/Headers$Builder;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v3, 0x66

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v4, 0xf0

    aget-byte v3, v3, v4

    sget-object v4, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v5, 0x1b

    aget-byte v4, v4, v5

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Lokhttp3/Headers$Builder;[Ljava/lang/String;)V

    .line 167
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/security/did/DID;->getDID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    .line 168
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_110

    .line 169
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->b:Lokhttp3/Headers$Builder;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0x65

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/4 v3, 0x4

    aget-byte v2, v2, v3

    const/16 v3, 0x190

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v11}, Lokhttp3/Headers$Builder;->add(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Headers$Builder;

    .line 172
    :cond_110
    invoke-static {}, Lbr/com/itau/security/did/DID;->getWDID()Ljava/lang/String;

    move-result-object v12

    .line 173
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_130

    .line 174
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->b:Lokhttp3/Headers$Builder;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0x1e

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/4 v3, 0x4

    aget-byte v2, v2, v3

    const/16 v3, 0x162

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v12}, Lokhttp3/Headers$Builder;->add(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Headers$Builder;

    .line 176
    :cond_130
    goto :goto_166

    :cond_131
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/Headers;

    if-ne v10, v0, :cond_15f

    .line 177
    move-object v0, v9

    check-cast v0, Lbr/com/itau/sdk/android/core/endpoint/http/Headers;

    invoke-interface {v0}, Lbr/com/itau/sdk/android/core/endpoint/http/Headers;->value()[Ljava/lang/String;

    move-result-object v11

    .line 179
    array-length v0, v11

    if-nez v0, :cond_159

    .line 180
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v1, 0x13a

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0x1b

    aget-byte v1, v1, v2

    or-int/lit8 v2, v1, 0x30

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 182
    :cond_159
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->b:Lokhttp3/Headers$Builder;

    invoke-direct {p0, v0, v11}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Lokhttp3/Headers$Builder;[Ljava/lang/String;)V

    .line 183
    goto :goto_166

    :cond_15f
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/Cacheable;

    if-ne v10, v0, :cond_166

    .line 184
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->k:Z

    .line 136
    :cond_166
    :goto_166
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_8

    .line 188
    :cond_16a
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->e:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    if-nez v0, :cond_18a

    .line 189
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v1, 0x3ab

    aget-byte v0, v0, v1

    sget v1, Lbr/com/itau/sdk/android/core/endpoint/k;->y:I

    or-int/lit16 v1, v1, 0x321

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v3, 0x47

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 190
    :cond_18a
    return-void
.end method

.method private a(ILjava/lang/String;)V
    .registers 7

    .line 402
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->v:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_34

    .line 403
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v1, 0x9

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0x17

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    const/16 v2, 0x367

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/k;->w:Ljava/util/regex/Pattern;

    .line 404
    invoke-virtual {v2}, Ljava/util/regex/Pattern;->pattern()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p2, v1, v2

    .line 403
    invoke-direct {p0, p1, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 407
    :cond_34
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->m:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5c

    .line 408
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v1, 0x13c

    aget-byte v0, v0, v1

    sget v1, Lbr/com/itau/sdk/android/core/endpoint/k;->y:I

    add-int/lit8 v1, v1, 0x1

    const/16 v2, 0x231

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->l:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-direct {p0, p1, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 409
    :cond_5c
    return-void
.end method

.method private a(Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 9

    .line 231
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->e:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    if-eqz v0, :cond_26

    .line 232
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v1, 0x2b

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0xb8

    aget-byte v1, v1, v2

    const/16 v2, 0x3e6

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->e:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 235
    :cond_26
    iput-object p2, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->p:Ljava/lang/String;

    .line 236
    iput-boolean p4, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->q:Z

    .line 238
    if-nez p1, :cond_46

    .line 239
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v1, 0x66

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0x23

    aget-byte v1, v1, v2

    const/16 v2, 0x1e5

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 241
    :cond_46
    if-eqz p3, :cond_50

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_50

    const/4 v0, 0x1

    goto :goto_51

    :cond_50
    const/4 v0, 0x0

    :goto_51
    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->g:Z

    .line 243
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->g:Z

    if-eqz v0, :cond_5a

    .line 244
    invoke-direct {p0, p3}, Lbr/com/itau/sdk/android/core/endpoint/k;->b(Ljava/lang/String;)V

    .line 247
    :cond_5a
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->e:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    .line 248
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->e:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->hasBody(Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->f:Z

    .line 249
    return-void
.end method

.method private a(Lbr/com/itau/sdk/android/core/endpoint/legacy/Middleware;)V
    .registers 6

    .line 194
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->e:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    if-eqz v0, :cond_28

    .line 195
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v1, 0x2b

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0xb8

    aget-byte v1, v1, v2

    const/16 v2, 0x3e6

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->LEGACY:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->a:Ljava/lang/reflect/Method;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 198
    :cond_28
    invoke-interface {p1}, Lbr/com/itau/sdk/android/core/endpoint/legacy/Middleware;->operation()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->p:Ljava/lang/String;

    .line 199
    invoke-interface {p1}, Lbr/com/itau/sdk/android/core/endpoint/legacy/Middleware;->readTimeout()I

    move-result v0

    iput v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->r:I

    .line 200
    invoke-interface {p1}, Lbr/com/itau/sdk/android/core/endpoint/legacy/Middleware;->connectTimeout()I

    move-result v0

    iput v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->s:I

    .line 201
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->i:Z

    .line 203
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->p:Ljava/lang/String;

    if-eqz v0, :cond_4f

    const-string v0, ""

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->p:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_67

    .line 204
    :cond_4f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0x9

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v3, 0x25

    aget-byte v2, v2, v3

    const/16 v3, 0x211

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 207
    :cond_67
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->LEGACY:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->e:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    .line 208
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->f:Z

    .line 209
    return-void
.end method

.method private a(Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareUniversal;)V
    .registers 6

    .line 213
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->e:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    if-eqz v0, :cond_28

    .line 214
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v1, 0x2b

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0xb8

    aget-byte v1, v1, v2

    const/16 v2, 0x3e6

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->LEGACY:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->a:Ljava/lang/reflect/Method;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 217
    :cond_28
    invoke-interface {p1}, Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareUniversal;->opKey()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->p:Ljava/lang/String;

    .line 218
    invoke-interface {p1}, Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareUniversal;->readTimeout()I

    move-result v0

    iput v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->r:I

    .line 219
    invoke-interface {p1}, Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareUniversal;->connectTimeout()I

    move-result v0

    iput v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->s:I

    .line 220
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->h:Z

    .line 222
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->p:Ljava/lang/String;

    if-eqz v0, :cond_4f

    const-string v0, ""

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->p:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_67

    .line 223
    :cond_4f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0x350

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v3, 0x25

    aget-byte v2, v2, v3

    const/16 v3, 0x1d1

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 226
    :cond_67
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->LEGACY:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->e:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    .line 227
    return-void
.end method

.method private a(Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;)V
    .registers 7

    .line 102
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;->getMethod()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->valueByName(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    move-result-object v4

    .line 104
    if-nez v4, :cond_26

    .line 105
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v1, 0x3ab

    aget-byte v0, v0, v1

    sget v1, Lbr/com/itau/sdk/android/core/endpoint/k;->y:I

    or-int/lit16 v1, v1, 0x321

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v3, 0x47

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 107
    :cond_26
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;->getOp()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v4, v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 110
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->e:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    if-nez v0, :cond_4f

    .line 111
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v1, 0x3ab

    aget-byte v0, v0, v1

    sget v1, Lbr/com/itau/sdk/android/core/endpoint/k;->y:I

    or-int/lit16 v1, v1, 0x321

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v3, 0x47

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 113
    :cond_4f
    return-void
.end method

.method private a(Lokhttp3/Headers$Builder;[Ljava/lang/String;)V
    .registers 13

    .line 285
    move-object v3, p2

    array-length v4, v3

    const/4 v5, 0x0

    :goto_3
    if-ge v5, v4, :cond_4e

    aget-object v6, v3, v5

    .line 286
    const/16 v0, 0x3a

    invoke-virtual {v6, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    .line 287
    const/4 v0, -0x1

    if-eq v7, v0, :cond_1a

    if-eqz v7, :cond_1a

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v7, v0, :cond_39

    .line 288
    :cond_1a
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v1, 0x3ab

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0x17

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    const/16 v2, 0xa8

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v6, v1, v2

    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 291
    :cond_39
    const/4 v0, 0x0

    invoke-virtual {v6, v0, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 292
    add-int/lit8 v0, v7, 0x1

    invoke-virtual {v6, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    .line 293
    invoke-virtual {p1, v8, v9}, Lokhttp3/Headers$Builder;->add(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Headers$Builder;

    .line 285
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 295
    :cond_4e
    return-void
.end method

.method private b()V
    .registers 6

    .line 299
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->a:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getGenericReturnType()Ljava/lang/reflect/Type;

    move-result-object v3

    .line 301
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->h:Z

    if-nez v0, :cond_e

    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->i:Z

    if-eqz v0, :cond_30

    :cond_e
    const-class v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_30

    .line 302
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v1, 0x3ab

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0x53

    aget-byte v1, v1, v2

    const/16 v2, 0x2c8

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 305
    :cond_30
    sget-object v0, Ljava/lang/Void;->TYPE:Ljava/lang/Class;

    if-ne v3, v0, :cond_4e

    .line 306
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/4 v1, 0x7

    aget-byte v0, v0, v1

    neg-int v0, v0

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0x2b

    aget-byte v1, v1, v2

    const/16 v2, 0x253

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 308
    :cond_4e
    instance-of v0, v3, Ljava/lang/reflect/ParameterizedType;

    if-eqz v0, :cond_6d

    move-object v0, v3

    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getRawType()Ljava/lang/reflect/Type;

    move-result-object v0

    const-class v1, Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;

    if-ne v0, v1, :cond_6d

    .line 310
    move-object v4, v3

    check-cast v4, Ljava/lang/reflect/ParameterizedType;

    .line 311
    invoke-interface {v4}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->o:Ljava/lang/reflect/Type;

    .line 312
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->j:Z

    .line 313
    goto :goto_72

    .line 314
    :cond_6d
    iput-object v3, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->o:Ljava/lang/reflect/Type;

    .line 315
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->j:Z

    .line 317
    :goto_72
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .registers 10

    .line 253
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2f

    if-eq v0, v1, :cond_26

    .line 254
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v1, 0x13c

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    sget v1, Lbr/com/itau/sdk/android/core/endpoint/k;->y:I

    add-int/lit8 v1, v1, 0x1

    const/16 v2, 0x183

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 257
    :cond_26
    move-object v4, p1

    .line 258
    const/4 v5, 0x0

    .line 260
    const/16 v0, 0x3f

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    .line 262
    const/4 v0, -0x1

    if-eq v6, v0, :cond_6b

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v6, v0, :cond_6b

    .line 264
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 265
    add-int/lit8 v0, v6, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 268
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->w:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    .line 270
    invoke-virtual {v7}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_6b

    .line 271
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v1, 0x1b

    aget-byte v0, v0, v1

    or-int/lit16 v1, v0, 0x157

    sget v2, Lbr/com/itau/sdk/android/core/endpoint/k;->y:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v5, v1, v2

    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 276
    :cond_6b
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v7

    .line 278
    iput-object v4, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->l:Ljava/lang/String;

    .line 279
    iput-object v7, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->m:Ljava/util/Set;

    .line 280
    iput-object v5, p0, Lbr/com/itau/sdk/android/core/endpoint/k;->n:Ljava/lang/String;

    .line 281
    return-void
.end method

.method private c()V
    .registers 19

    .line 320
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/sdk/android/core/endpoint/k;->a:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getGenericParameterTypes()[Ljava/lang/reflect/Type;

    move-result-object v4

    .line 321
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/sdk/android/core/endpoint/k;->a:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getParameterAnnotations()[[Ljava/lang/annotation/Annotation;

    move-result-object v5

    .line 322
    array-length v6, v5

    .line 323
    new-array v7, v6, [Ljava/lang/annotation/Annotation;

    .line 325
    const/4 v8, 0x0

    .line 327
    const/4 v9, 0x0

    :goto_15
    if-ge v9, v6, :cond_189

    .line 328
    aget-object v10, v4, v9

    .line 329
    aget-object v11, v5, v9

    .line 331
    if-eqz v11, :cond_145

    .line 333
    move-object v12, v11

    array-length v13, v12

    const/4 v14, 0x0

    :goto_20
    if-ge v14, v13, :cond_145

    aget-object v15, v12, v14

    .line 335
    .line 336
    invoke-interface {v15}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v16

    .line 338
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/Path;

    move-object/from16 v1, v16

    if-ne v1, v0, :cond_44

    .line 340
    move-object v0, v15

    check-cast v0, Lbr/com/itau/sdk/android/core/endpoint/http/Path;

    invoke-interface {v0}, Lbr/com/itau/sdk/android/core/endpoint/http/Path;->value()Ljava/lang/String;

    move-result-object v17

    .line 342
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/sdk/android/core/endpoint/k;->g:Z

    if-eqz v0, :cond_42

    .line 343
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v9, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(ILjava/lang/String;)V

    .line 345
    :cond_42
    goto/16 :goto_e7

    :cond_44
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/Query;

    move-object/from16 v1, v16

    if-eq v1, v0, :cond_e7

    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/Header;

    move-object/from16 v1, v16

    if-ne v1, v0, :cond_52

    goto/16 :goto_e7

    .line 347
    :cond_52
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/Body;

    move-object/from16 v1, v16

    if-ne v1, v0, :cond_a5

    .line 349
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/sdk/android/core/endpoint/k;->i:Z

    if-eqz v0, :cond_81

    const-class v0, Ljava/util/Collection;

    move-object v1, v10

    check-cast v1, Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_81

    .line 350
    new-instance v0, Ljava/lang/RuntimeException;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0x1c0

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v3, 0x53

    aget-byte v2, v2, v3

    const/16 v3, 0x427

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 353
    :cond_81
    if-eqz v8, :cond_9f

    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/4 v1, 0x5

    aget-byte v0, v0, v1

    neg-int v0, v0

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0x53

    aget-byte v1, v1, v2

    const/16 v2, 0xcf

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    move-object/from16 v2, p0

    invoke-direct {v2, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 355
    :cond_9f
    move-object/from16 v0, p0

    iput-object v10, v0, Lbr/com/itau/sdk/android/core/endpoint/k;->d:Ljava/lang/reflect/Type;

    .line 356
    const/4 v8, 0x1

    goto :goto_e7

    .line 358
    :cond_a5
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/ReqProgress;

    move-object/from16 v1, v16

    if-ne v1, v0, :cond_e1

    .line 360
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/sdk/android/core/endpoint/k;->i:Z

    if-eqz v0, :cond_c7

    .line 361
    new-instance v0, Ljava/lang/RuntimeException;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0x47

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    or-int/lit16 v2, v1, 0xd1

    and-int/lit8 v3, v2, 0x30

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 364
    :cond_c7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/sdk/android/core/endpoint/k;->h:Z

    if-eqz v0, :cond_e7

    .line 365
    new-instance v0, Ljava/lang/RuntimeException;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0x53

    aget-byte v1, v1, v2

    or-int/lit16 v2, v1, 0x2dc

    and-int/lit8 v3, v2, 0x30

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 368
    :cond_e1
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/a/a;

    move-object/from16 v1, v16

    if-ne v1, v0, :cond_141

    .line 375
    :cond_e7
    :goto_e7
    aget-object v0, v7, v9

    if-eqz v0, :cond_11d

    .line 376
    sget v0, Lbr/com/itau/sdk/android/core/endpoint/k;->y:I

    add-int/lit8 v0, v0, 0x2

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0x13a

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v3, 0x53

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aget-object v2, v7, v9

    .line 378
    invoke-interface {v2}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 379
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 376
    move-object/from16 v2, p0

    invoke-direct {v2, v9, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 382
    :cond_11d
    if-eqz v16, :cond_13f

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/sdk/android/core/endpoint/k;->h:Z

    if-eqz v0, :cond_13f

    .line 383
    sget v0, Lbr/com/itau/sdk/android/core/endpoint/k;->y:I

    add-int/lit8 v0, v0, -0x2

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0x54

    aget-byte v1, v1, v2

    const/16 v2, 0x3a9

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    move-object/from16 v2, p0

    invoke-direct {v2, v9, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 386
    :cond_13f
    aput-object v15, v7, v9

    .line 333
    :cond_141
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_20

    .line 390
    :cond_145
    aget-object v0, v7, v9

    if-nez v0, :cond_185

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/sdk/android/core/endpoint/k;->h:Z

    if-nez v0, :cond_185

    .line 391
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0x50

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v3, 0x51

    aget-byte v2, v2, v3

    const/16 v3, 0x401

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/sdk/android/core/endpoint/k;->a:Ljava/lang/reflect/Method;

    invoke-virtual {v1}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    move-object/from16 v2, p0

    invoke-direct {v2, v9, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(ILjava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 327
    :cond_185
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_15

    .line 394
    :cond_189
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/sdk/android/core/endpoint/k;->f:Z

    if-nez v0, :cond_1ad

    if-eqz v8, :cond_1ad

    .line 395
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v1, 0xb8

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0x51

    aget-byte v1, v1, v2

    const/16 v2, 0x28c

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    move-object/from16 v2, p0

    invoke-direct {v2, v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    .line 397
    :cond_1ad
    move-object/from16 v0, p0

    iput-object v7, v0, Lbr/com/itau/sdk/android/core/endpoint/k;->t:[Ljava/lang/annotation/Annotation;

    .line 398
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 118
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v2, 0x2b7

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/k;->x:[B

    const/16 v3, 0x47

    aget-byte v2, v2, v3

    const/16 v3, 0x439

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/endpoint/k;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/CloneNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

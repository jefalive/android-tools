.class Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity$1;
.super Ljava/lang/Object;
.source "ErrorSystemActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;


# direct methods
.method constructor <init>(Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;)V
    .registers 2
    .param p1, "this$0"    # Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;

    .line 59
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 6
    .param p1, "v"    # Landroid/view/View;

    .line 61
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 62
    .local v2, "i":I
    sget v0, Lbr/com/itau/textovoz/R$id;->error_system_close:I

    if-ne v2, v0, :cond_28

    .line 63
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;

    invoke-virtual {v0}, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 65
    .local v3, "intent":Landroid/content/Intent;
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;

    # getter for: Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;->closeAppLibrary:Z
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;->access$000(Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 66
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1, v3}, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_23

    .line 68
    :cond_1d
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v3}, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;->setResult(ILandroid/content/Intent;)V

    .line 70
    :goto_23
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;

    invoke-virtual {v0}, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;->finish()V

    .line 72
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_28
    return-void
.end method

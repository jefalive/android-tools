.class public final Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;
.super Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;
.source "EmprestimosParceladosView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 39
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;-><init>(Landroid/content/Context;)V

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->alreadyInflated_:Z

    .line 36
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 40
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->init_()V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeSet"    # Landroid/util/AttributeSet;

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->alreadyInflated_:Z

    .line 36
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 45
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->init_()V

    .line 46
    return-void
.end method

.method private init_()V
    .registers 3

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 72
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 73
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 74
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 62
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->alreadyInflated_:Z

    .line 64
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0300ee

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 67
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->onFinishInflate()V

    .line 68
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 84
    const v0, 0x7f0e050e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->tituloCreditoSimplificado:Landroid/widget/TextView;

    .line 85
    const v0, 0x7f0e0510

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->textoCreditoValor:Landroid/widget/TextView;

    .line 86
    const v0, 0x7f0e0511

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->textoCreditoValorComParcelas:Landroid/widget/TextView;

    .line 87
    const v0, 0x7f0e0515

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->textoCreditoTaxaJurosValor:Landroid/widget/TextView;

    .line 88
    const v0, 0x7f0e0513

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->textoCreditoDiasPagamento:Landroid/widget/TextView;

    .line 89
    const v0, 0x7f0e0512

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->textoCreditoComSeguro:Landroid/widget/TextView;

    .line 90
    const v0, 0x7f0e0516

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->textoGarantiaDevedorSolidario:Landroid/widget/TextView;

    .line 91
    const v0, 0x7f0e050f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->ajudaCredito:Landroid/widget/ImageView;

    .line 92
    const v0, 0x7f0e050c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->llActivityEmprestimosParceladosOferta:Landroid/widget/LinearLayout;

    .line 93
    const v0, 0x7f0e0517

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->botaoPersonalizar:Landroid/widget/Button;

    .line 94
    const v0, 0x7f0e0518

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->botaoContinuar:Landroid/widget/Button;

    .line 95
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->ajudaCredito:Landroid/widget/ImageView;

    if-eqz v0, :cond_87

    .line 96
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->ajudaCredito:Landroid/widget/ImageView;

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_$1;-><init>(Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    :cond_87
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->botaoContinuar:Landroid/widget/Button;

    if-eqz v0, :cond_95

    .line 106
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->botaoContinuar:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_$2;-><init>(Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    :cond_95
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->botaoPersonalizar:Landroid/widget/Button;

    if-eqz v0, :cond_a3

    .line 116
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;->botaoPersonalizar:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_$3;-><init>(Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    :cond_a3
    return-void
.end method

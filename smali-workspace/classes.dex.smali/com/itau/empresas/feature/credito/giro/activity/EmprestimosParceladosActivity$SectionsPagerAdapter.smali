.class public Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity$SectionsPagerAdapter;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "EmprestimosParceladosActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SectionsPagerAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;


# direct methods
.method public constructor <init>(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;Landroid/support/v4/app/FragmentManager;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;
    .param p2, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;

    .line 303
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity$SectionsPagerAdapter;->this$0:Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;

    .line 304
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 305
    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 2

    .line 309
    const/4 v0, 0x1

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .registers 4
    .param p1, "position"    # I

    .line 314
    if-nez p1, :cond_1e

    .line 315
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity$SectionsPagerAdapter;->this$0:Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;

    # getter for: Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->fragmentCreditoSimplificado:Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->access$000(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;)Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;

    move-result-object v0

    if-nez v0, :cond_17

    .line 316
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity$SectionsPagerAdapter;->this$0:Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;

    .line 317
    invoke-static {}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_;->builder()Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_$FragmentBuilder_;

    move-result-object v1

    .line 318
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;

    move-result-object v1

    .line 316
    # setter for: Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->fragmentCreditoSimplificado:Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->access$002(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;)Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;

    .line 320
    :cond_17
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity$SectionsPagerAdapter;->this$0:Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;

    # getter for: Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->fragmentCreditoSimplificado:Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->access$000(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;)Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;

    move-result-object v0

    return-object v0

    .line 323
    :cond_1e
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity$SectionsPagerAdapter;->this$0:Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;

    # getter for: Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->fragmentContratados:Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->access$100(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;)Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;

    move-result-object v0

    if-nez v0, :cond_33

    .line 324
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity$SectionsPagerAdapter;->this$0:Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;

    invoke-static {}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment_;->builder()Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment_$FragmentBuilder_;

    move-result-object v1

    .line 325
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;

    move-result-object v1

    .line 324
    # setter for: Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->fragmentContratados:Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->access$102(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;)Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;

    .line 327
    :cond_33
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity$SectionsPagerAdapter;->this$0:Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;

    # getter for: Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->fragmentContratados:Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->access$100(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;)Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;

    move-result-object v0

    return-object v0
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .registers 4
    .param p1, "position"    # I

    .line 333
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity$SectionsPagerAdapter;->this$0:Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, p1

    return-object v0
.end method

.class public Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamentoGPS;
.super Ljava/lang/Object;
.source "AtualizaValorPagamentoGPS.java"


# instance fields
.field private atualizaDadosActivityListener:Lcom/itau/empresas/ui/util/listener/AtualizaValorTotalGps;

.field private dadosGps:Lcom/itau/empresas/api/universal/model/DadosGps;


# direct methods
.method public constructor <init>(Lcom/itau/empresas/ui/util/listener/AtualizaValorTotalGps;)V
    .registers 3
    .param p1, "atualizaValorTotalGps"    # Lcom/itau/empresas/ui/util/listener/AtualizaValorTotalGps;

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Lcom/itau/empresas/api/universal/model/DadosGps;

    invoke-direct {v0}, Lcom/itau/empresas/api/universal/model/DadosGps;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamentoGPS;->dadosGps:Lcom/itau/empresas/api/universal/model/DadosGps;

    .line 13
    iput-object p1, p0, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamentoGPS;->atualizaDadosActivityListener:Lcom/itau/empresas/ui/util/listener/AtualizaValorTotalGps;

    .line 14
    return-void
.end method


# virtual methods
.method public notifyObserver()V
    .registers 3

    .line 32
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamentoGPS;->atualizaDadosActivityListener:Lcom/itau/empresas/ui/util/listener/AtualizaValorTotalGps;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamentoGPS;->dadosGps:Lcom/itau/empresas/api/universal/model/DadosGps;

    invoke-interface {v0, v1}, Lcom/itau/empresas/ui/util/listener/AtualizaValorTotalGps;->atualizaValorTotal(Lcom/itau/empresas/api/universal/model/DadosGps;)V

    .line 33
    return-void
.end method

.method public setValorAtualizacaoMonetaria(Ljava/lang/String;)V
    .registers 3
    .param p1, "valorAcrescimo"    # Ljava/lang/String;

    .line 17
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamentoGPS;->dadosGps:Lcom/itau/empresas/api/universal/model/DadosGps;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/api/universal/model/DadosGps;->setValorAtualizacaoMonetaria(Ljava/lang/String;)V

    .line 18
    invoke-virtual {p0}, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamentoGPS;->notifyObserver()V

    .line 19
    return-void
.end method

.method public setValorInss(Ljava/lang/String;)V
    .registers 3
    .param p1, "valorInss"    # Ljava/lang/String;

    .line 27
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamentoGPS;->dadosGps:Lcom/itau/empresas/api/universal/model/DadosGps;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/api/universal/model/DadosGps;->setValorInss(Ljava/lang/String;)V

    .line 28
    invoke-virtual {p0}, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamentoGPS;->notifyObserver()V

    .line 29
    return-void
.end method

.method public setValorOutrasEntidades(Ljava/lang/String;)V
    .registers 3
    .param p1, "valorOutrasEntidades"    # Ljava/lang/String;

    .line 22
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamentoGPS;->dadosGps:Lcom/itau/empresas/api/universal/model/DadosGps;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/api/universal/model/DadosGps;->setValorOutrasEntidades(Ljava/lang/String;)V

    .line 23
    invoke-virtual {p0}, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamentoGPS;->notifyObserver()V

    .line 24
    return-void
.end method

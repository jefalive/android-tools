.class public Lcom/google/android/gms/tagmanager/zzdf;
.super Ljava/lang/Object;


# static fields
.field private static final zzblE:Ljava/lang/Object;

.field private static zzblF:Ljava/lang/Long;

.field private static zzblG:Ljava/lang/Double;

.field private static zzblH:Lcom/google/android/gms/tagmanager/zzde;

.field private static zzblI:Ljava/lang/String;

.field private static zzblJ:Ljava/lang/Boolean;

.field private static zzblK:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/Object;>;"
        }
    .end annotation
.end field

.field private static zzblL:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/Object;Ljava/lang/Object;>;"
        }
    .end annotation
.end field

.field private static zzblM:Lcom/google/android/gms/internal/zzag$zza;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblE:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Long;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/Long;-><init>(J)V

    sput-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblF:Ljava/lang/Long;

    new-instance v0, Ljava/lang/Double;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/Double;-><init>(D)V

    sput-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblG:Ljava/lang/Double;

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/tagmanager/zzde;->zzam(J)Lcom/google/android/gms/tagmanager/zzde;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblH:Lcom/google/android/gms/tagmanager/zzde;

    new-instance v0, Ljava/lang/String;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblI:Ljava/lang/String;

    new-instance v0, Ljava/lang/Boolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/Boolean;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblJ:Ljava/lang/Boolean;

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblK:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblL:Ljava/util/Map;

    sget-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblI:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzdf;->zzR(Ljava/lang/Object;)Lcom/google/android/gms/internal/zzag$zza;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblM:Lcom/google/android/gms/internal/zzag$zza;

    return-void
.end method

.method public static zzHF()Lcom/google/android/gms/internal/zzag$zza;
    .registers 1

    sget-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblM:Lcom/google/android/gms/internal/zzag$zza;

    return-object v0
.end method

.method public static zzM(Ljava/lang/Object;)Ljava/lang/String;
    .registers 2

    if-nez p0, :cond_5

    sget-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblI:Ljava/lang/String;

    goto :goto_9

    :cond_5
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_9
    return-object v0
.end method

.method public static zzQ(Ljava/lang/Object;)Ljava/lang/Boolean;
    .registers 2

    instance-of v0, p0, Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    move-object v0, p0

    check-cast v0, Ljava/lang/Boolean;

    goto :goto_10

    :cond_8
    invoke-static {p0}, Lcom/google/android/gms/tagmanager/zzdf;->zzM(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzdf;->zzgx(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    :goto_10
    return-object v0
.end method

.method public static zzR(Ljava/lang/Object;)Lcom/google/android/gms/internal/zzag$zza;
    .registers 12

    new-instance v2, Lcom/google/android/gms/internal/zzag$zza;

    invoke-direct {v2}, Lcom/google/android/gms/internal/zzag$zza;-><init>()V

    const/4 v3, 0x0

    instance-of v0, p0, Lcom/google/android/gms/internal/zzag$zza;

    if-eqz v0, :cond_e

    move-object v0, p0

    check-cast v0, Lcom/google/android/gms/internal/zzag$zza;

    return-object v0

    :cond_e
    instance-of v0, p0, Ljava/lang/String;

    if-eqz v0, :cond_1c

    const/4 v0, 0x1

    iput v0, v2, Lcom/google/android/gms/internal/zzag$zza;->type:I

    move-object v0, p0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/gms/internal/zzag$zza;->zzjx:Ljava/lang/String;

    goto/16 :goto_131

    :cond_1c
    instance-of v0, p0, Ljava/util/List;

    if-eqz v0, :cond_62

    const/4 v0, 0x2

    iput v0, v2, Lcom/google/android/gms/internal/zzag$zza;->type:I

    move-object v4, p0

    check-cast v4, Ljava/util/List;

    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_33
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_55

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/gms/tagmanager/zzdf;->zzR(Ljava/lang/Object;)Lcom/google/android/gms/internal/zzag$zza;

    move-result-object v8

    sget-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblM:Lcom/google/android/gms/internal/zzag$zza;

    if-ne v8, v0, :cond_48

    sget-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblM:Lcom/google/android/gms/internal/zzag$zza;

    return-object v0

    :cond_48
    if-nez v3, :cond_4e

    iget-boolean v0, v8, Lcom/google/android/gms/internal/zzag$zza;->zzjH:Z

    if-eqz v0, :cond_50

    :cond_4e
    const/4 v3, 0x1

    goto :goto_51

    :cond_50
    const/4 v3, 0x0

    :goto_51
    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_33

    :cond_55
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/internal/zzag$zza;

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/internal/zzag$zza;

    iput-object v0, v2, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    goto/16 :goto_131

    :cond_62
    instance-of v0, p0, Ljava/util/Map;

    if-eqz v0, :cond_d9

    const/4 v0, 0x3

    iput v0, v2, Lcom/google/android/gms/internal/zzag$zza;->type:I

    move-object v0, p0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v6, Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_86
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/util/Map$Entry;

    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzdf;->zzR(Ljava/lang/Object;)Lcom/google/android/gms/internal/zzag$zza;

    move-result-object v9

    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzdf;->zzR(Ljava/lang/Object;)Lcom/google/android/gms/internal/zzag$zza;

    move-result-object v10

    sget-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblM:Lcom/google/android/gms/internal/zzag$zza;

    if-eq v9, v0, :cond_ab

    sget-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblM:Lcom/google/android/gms/internal/zzag$zza;

    if-ne v10, v0, :cond_ae

    :cond_ab
    sget-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblM:Lcom/google/android/gms/internal/zzag$zza;

    return-object v0

    :cond_ae
    if-nez v3, :cond_b8

    iget-boolean v0, v9, Lcom/google/android/gms/internal/zzag$zza;->zzjH:Z

    if-nez v0, :cond_b8

    iget-boolean v0, v10, Lcom/google/android/gms/internal/zzag$zza;->zzjH:Z

    if-eqz v0, :cond_ba

    :cond_b8
    const/4 v3, 0x1

    goto :goto_bb

    :cond_ba
    const/4 v3, 0x0

    :goto_bb
    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_86

    :cond_c2
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/internal/zzag$zza;

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/internal/zzag$zza;

    iput-object v0, v2, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/internal/zzag$zza;

    invoke-interface {v6, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/internal/zzag$zza;

    iput-object v0, v2, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    goto :goto_131

    :cond_d9
    invoke-static {p0}, Lcom/google/android/gms/tagmanager/zzdf;->zzS(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e9

    const/4 v0, 0x1

    iput v0, v2, Lcom/google/android/gms/internal/zzag$zza;->type:I

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/internal/zzag$zza;->zzjx:Ljava/lang/String;

    goto :goto_131

    :cond_e9
    invoke-static {p0}, Lcom/google/android/gms/tagmanager/zzdf;->zzT(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f9

    const/4 v0, 0x6

    iput v0, v2, Lcom/google/android/gms/internal/zzag$zza;->type:I

    invoke-static {p0}, Lcom/google/android/gms/tagmanager/zzdf;->zzU(Ljava/lang/Object;)J

    move-result-wide v0

    iput-wide v0, v2, Lcom/google/android/gms/internal/zzag$zza;->zzjD:J

    goto :goto_131

    :cond_f9
    instance-of v0, p0, Ljava/lang/Boolean;

    if-eqz v0, :cond_10b

    const/16 v0, 0x8

    iput v0, v2, Lcom/google/android/gms/internal/zzag$zza;->type:I

    move-object v0, p0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, v2, Lcom/google/android/gms/internal/zzag$zza;->zzjE:Z

    goto :goto_131

    :cond_10b
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Converting to Value from unknown object type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    if-nez p0, :cond_11b

    const-string v1, "null"

    goto :goto_123

    :cond_11b
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_123
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzbg;->e(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblM:Lcom/google/android/gms/internal/zzag$zza;

    return-object v0

    :goto_131
    iput-boolean v3, v2, Lcom/google/android/gms/internal/zzag$zza;->zzjH:Z

    return-object v2
.end method

.method private static zzS(Ljava/lang/Object;)Z
    .registers 2

    instance-of v0, p0, Ljava/lang/Double;

    if-nez v0, :cond_15

    instance-of v0, p0, Ljava/lang/Float;

    if-nez v0, :cond_15

    instance-of v0, p0, Lcom/google/android/gms/tagmanager/zzde;

    if-eqz v0, :cond_17

    move-object v0, p0

    check-cast v0, Lcom/google/android/gms/tagmanager/zzde;

    invoke-virtual {v0}, Lcom/google/android/gms/tagmanager/zzde;->zzHu()Z

    move-result v0

    if-eqz v0, :cond_17

    :cond_15
    const/4 v0, 0x1

    goto :goto_18

    :cond_17
    const/4 v0, 0x0

    :goto_18
    return v0
.end method

.method private static zzT(Ljava/lang/Object;)Z
    .registers 2

    instance-of v0, p0, Ljava/lang/Byte;

    if-nez v0, :cond_1d

    instance-of v0, p0, Ljava/lang/Short;

    if-nez v0, :cond_1d

    instance-of v0, p0, Ljava/lang/Integer;

    if-nez v0, :cond_1d

    instance-of v0, p0, Ljava/lang/Long;

    if-nez v0, :cond_1d

    instance-of v0, p0, Lcom/google/android/gms/tagmanager/zzde;

    if-eqz v0, :cond_1f

    move-object v0, p0

    check-cast v0, Lcom/google/android/gms/tagmanager/zzde;

    invoke-virtual {v0}, Lcom/google/android/gms/tagmanager/zzde;->zzHv()Z

    move-result v0

    if-eqz v0, :cond_1f

    :cond_1d
    const/4 v0, 0x1

    goto :goto_20

    :cond_1f
    const/4 v0, 0x0

    :goto_20
    return v0
.end method

.method private static zzU(Ljava/lang/Object;)J
    .registers 3

    instance-of v0, p0, Ljava/lang/Number;

    if-eqz v0, :cond_c

    move-object v0, p0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    return-wide v0

    :cond_c
    const-string v0, "getInt64 received non-Number"

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzbg;->e(Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public static zzg(Lcom/google/android/gms/internal/zzag$zza;)Ljava/lang/String;
    .registers 2

    invoke-static {p0}, Lcom/google/android/gms/tagmanager/zzdf;->zzl(Lcom/google/android/gms/internal/zzag$zza;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzdf;->zzM(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static zzgx(Ljava/lang/String;)Ljava/lang/Boolean;
    .registers 2

    const-string v0, "true"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0

    :cond_b
    const-string v0, "false"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object v0

    :cond_16
    sget-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblJ:Ljava/lang/Boolean;

    return-object v0
.end method

.method public static zzk(Lcom/google/android/gms/internal/zzag$zza;)Ljava/lang/Boolean;
    .registers 2

    invoke-static {p0}, Lcom/google/android/gms/tagmanager/zzdf;->zzl(Lcom/google/android/gms/internal/zzag$zza;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzdf;->zzQ(Ljava/lang/Object;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static zzl(Lcom/google/android/gms/internal/zzag$zza;)Ljava/lang/Object;
    .registers 9

    if-nez p0, :cond_5

    sget-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblE:Ljava/lang/Object;

    return-object v0

    :cond_5
    iget v0, p0, Lcom/google/android/gms/internal/zzag$zza;->type:I

    packed-switch v0, :pswitch_data_e2

    goto/16 :goto_c7

    :pswitch_c
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjx:Ljava/lang/String;

    return-object v0

    :pswitch_f
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v3, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v4, v3

    const/4 v5, 0x0

    :goto_1b
    if-ge v5, v4, :cond_30

    aget-object v6, v3, v5

    invoke-static {v6}, Lcom/google/android/gms/tagmanager/zzdf;->zzl(Lcom/google/android/gms/internal/zzag$zza;)Ljava/lang/Object;

    move-result-object v7

    sget-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblE:Ljava/lang/Object;

    if-ne v7, v0, :cond_2a

    sget-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblE:Ljava/lang/Object;

    return-object v0

    :cond_2a
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_1b

    :cond_30
    return-object v2

    :pswitch_31
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v1, v1

    if-eq v0, v1, :cond_56

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Converting an invalid value to object: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzag$zza;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzbg;->e(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblE:Ljava/lang/Object;

    return-object v0

    :cond_56
    new-instance v2, Ljava/util/HashMap;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    invoke-direct {v2, v0}, Ljava/util/HashMap;-><init>(I)V

    const/4 v3, 0x0

    :goto_5f
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    if-ge v3, v0, :cond_85

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    aget-object v0, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzdf;->zzl(Lcom/google/android/gms/internal/zzag$zza;)Ljava/lang/Object;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    aget-object v0, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzdf;->zzl(Lcom/google/android/gms/internal/zzag$zza;)Ljava/lang/Object;

    move-result-object v5

    sget-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblE:Ljava/lang/Object;

    if-eq v4, v0, :cond_7c

    sget-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblE:Ljava/lang/Object;

    if-ne v5, v0, :cond_7f

    :cond_7c
    sget-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblE:Ljava/lang/Object;

    return-object v0

    :cond_7f
    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_5f

    :cond_85
    return-object v2

    :pswitch_86
    const-string v0, "Trying to convert a macro reference to object"

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzbg;->e(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblE:Ljava/lang/Object;

    return-object v0

    :pswitch_8e
    const-string v0, "Trying to convert a function id to object"

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzbg;->e(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblE:Ljava/lang/Object;

    return-object v0

    :pswitch_96
    iget-wide v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjD:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0

    :pswitch_9d
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjF:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v4, v3

    const/4 v5, 0x0

    :goto_a6
    if-ge v5, v4, :cond_bb

    aget-object v6, v3, v5

    invoke-static {v6}, Lcom/google/android/gms/tagmanager/zzdf;->zzg(Lcom/google/android/gms/internal/zzag$zza;)Ljava/lang/String;

    move-result-object v7

    sget-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblI:Ljava/lang/String;

    if-ne v7, v0, :cond_b5

    sget-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblE:Ljava/lang/Object;

    return-object v0

    :cond_b5
    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v5, v5, 0x1

    goto :goto_a6

    :cond_bb
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_c0
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjE:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :goto_c7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Failed to convert a value of type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/internal/zzag$zza;->type:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzbg;->e(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/tagmanager/zzdf;->zzblE:Ljava/lang/Object;

    return-object v0

    :pswitch_data_e2
    .packed-switch 0x1
        :pswitch_c
        :pswitch_f
        :pswitch_31
        :pswitch_86
        :pswitch_8e
        :pswitch_96
        :pswitch_9d
        :pswitch_c0
    .end packed-switch
.end method

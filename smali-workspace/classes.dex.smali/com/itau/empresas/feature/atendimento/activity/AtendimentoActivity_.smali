.class public final Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;
.super Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;
.source "AtendimentoActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 36
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;-><init>()V

    .line 40
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;

    .line 36
    invoke-super {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;

    .line 36
    invoke-super {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 52
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 53
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 54
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 55
    invoke-static {p0}, Lcom/itau/empresas/feature/logout/LogoutHelper_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/logout/LogoutHelper_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    .line 56
    invoke-static {p0}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;->controller:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

    .line 57
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;->afterInject()V

    .line 58
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 140
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_$4;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 148
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 128
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_$3;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 136
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 45
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;->init_(Landroid/os/Bundle;)V

    .line 46
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->onCreate(Landroid/os/Bundle;)V

    .line 47
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 48
    const v0, 0x7f03001d

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;->setContentView(I)V

    .line 49
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 5
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 92
    const v0, 0x7f0e00be

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;->root:Landroid/widget/LinearLayout;

    .line 93
    const v0, 0x7f0e00c5

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/widgets/NonScrollListView;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;->listaFaqs:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    .line 94
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 95
    const v0, 0x7f0e00c2

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/MensagemAlertaView;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;->mensagemAlertaView:Lcom/itau/empresas/ui/view/MensagemAlertaView;

    .line 96
    const v0, 0x7f0e05c6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;->linearCartaoAlerta:Landroid/widget/LinearLayout;

    .line 97
    const v0, 0x7f0e00c6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 98
    .local v1, "view_text_principais_duvidas":Landroid/view/View;
    const v0, 0x7f0e05e3

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 100
    .local v2, "view_linear_cartao_busca":Landroid/view/View;
    if-eqz v1, :cond_4f

    .line 101
    new-instance v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_$1;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    :cond_4f
    if-eqz v2, :cond_59

    .line 111
    new-instance v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_$2;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    :cond_59
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;->carregandoElementosDeTela()V

    .line 121
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 62
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->setContentView(I)V

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 64
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 74
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->setContentView(Landroid/view/View;)V

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 76
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 68
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 70
    return-void
.end method

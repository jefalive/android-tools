.class Landroid/support/v4/app/NotificationCompatApi24;
.super Ljava/lang/Object;
.source "NotificationCompatApi24.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v4/app/NotificationCompatApi24$Builder;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    return-void
.end method

.method public static addMessagingStyle(Landroid/support/v4/app/NotificationBuilderWithBuilderAccessor;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .registers 15
    .param p0, "b"    # Landroid/support/v4/app/NotificationBuilderWithBuilderAccessor;
    .param p1, "userDisplayName"    # Ljava/lang/CharSequence;
    .param p2, "conversationTitle"    # Ljava/lang/CharSequence;
    .param p3, "texts"    # Ljava/util/List;
    .param p4, "timestamps"    # Ljava/util/List;
    .param p5, "senders"    # Ljava/util/List;
    .param p6, "dataMimeTypes"    # Ljava/util/List;
    .param p7, "dataUris"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/support/v4/app/NotificationBuilderWithBuilderAccessor;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/util/List<Ljava/lang/CharSequence;>;Ljava/util/List<Ljava/lang/Long;>;Ljava/util/List<Ljava/lang/CharSequence;>;Ljava/util/List<Ljava/lang/String;>;Ljava/util/List<Landroid/net/Uri;>;)V"
        }
    .end annotation

    .line 150
    new-instance v0, Landroid/app/Notification$MessagingStyle;

    invoke-direct {v0, p1}, Landroid/app/Notification$MessagingStyle;-><init>(Ljava/lang/CharSequence;)V

    .line 151
    invoke-virtual {v0, p2}, Landroid/app/Notification$MessagingStyle;->setConversationTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$MessagingStyle;

    move-result-object v4

    .line 152
    .local v4, "style":Landroid/app/Notification$MessagingStyle;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_a
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v5, v0, :cond_46

    .line 153
    new-instance v6, Landroid/app/Notification$MessagingStyle$Message;

    .line 154
    invoke-interface {p3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {p4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {p5, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-direct {v6, v0, v1, v2, v3}, Landroid/app/Notification$MessagingStyle$Message;-><init>(Ljava/lang/CharSequence;JLjava/lang/CharSequence;)V

    .line 155
    .local v6, "message":Landroid/app/Notification$MessagingStyle$Message;
    invoke-interface {p6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_40

    .line 156
    invoke-interface {p6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    invoke-virtual {v6, v0, v1}, Landroid/app/Notification$MessagingStyle$Message;->setData(Ljava/lang/String;Landroid/net/Uri;)Landroid/app/Notification$MessagingStyle$Message;

    .line 158
    :cond_40
    invoke-virtual {v4, v6}, Landroid/app/Notification$MessagingStyle;->addMessage(Landroid/app/Notification$MessagingStyle$Message;)Landroid/app/Notification$MessagingStyle;

    .line 152
    .end local v6    # "message":Landroid/app/Notification$MessagingStyle$Message;
    add-int/lit8 v5, v5, 0x1

    goto :goto_a

    .line 160
    .end local v5    # "i":I
    :cond_46
    invoke-interface {p0}, Landroid/support/v4/app/NotificationBuilderWithBuilderAccessor;->getBuilder()Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/app/Notification$MessagingStyle;->setBuilder(Landroid/app/Notification$Builder;)V

    .line 161
    return-void
.end method

.class public final Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView_;
.super Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;
.source "LisCardValorView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 34
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;-><init>(Landroid/content/Context;)V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView_;->alreadyInflated_:Z

    .line 31
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 35
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView_;->init_()V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView_;->alreadyInflated_:Z

    .line 31
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 40
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView_;->init_()V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 44
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView_;->alreadyInflated_:Z

    .line 31
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 45
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView_;->init_()V

    .line 46
    return-void
.end method

.method private init_()V
    .registers 3

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 72
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 73
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 74
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 62
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView_;->alreadyInflated_:Z

    .line 64
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03017c

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 67
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->onFinishInflate()V

    .line 68
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 90
    const v0, 0x7f0e0552

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView_;->textViewValor:Landroid/widget/TextView;

    .line 91
    const v0, 0x7f0e066c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView_;->textViewObservacao:Landroid/widget/TextView;

    .line 92
    const v0, 0x7f0e066d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView_;->textViewAcao:Landroid/widget/TextView;

    .line 93
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView_;->afterViews()V

    .line 94
    return-void
.end method

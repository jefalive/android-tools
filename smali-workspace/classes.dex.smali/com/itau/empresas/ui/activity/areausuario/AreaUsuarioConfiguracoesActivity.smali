.class public Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "AreaUsuarioConfiguracoesActivity.java"

# interfaces
.implements Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnConfiguracoesItemSelected;
.implements Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnFingerprintChanged;


# instance fields
.field private fingerprintManager:Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;

.field private mAdapter:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;

.field rvConfiguracoes:Landroid/support/v7/widget/RecyclerView;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 41
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;

    .line 41
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->atualizarSwitchFingerprint()V

    return-void
.end method

.method static synthetic access$100(Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;

    .line 41
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->exibeDialogAviso()V

    return-void
.end method

.method private atualizarSwitchFingerprint()V
    .registers 3

    .line 160
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->mAdapter:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbr/com/itau/sdk/android/core/BackendClient;->hasKeyAccess(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->setFingerprintAtivado(Z)V

    .line 161
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 132
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 133
    const v1, 0x7f07069f

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 134
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 135
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 136
    return-void
.end method

.method private exibeDialogAviso()V
    .registers 4

    .line 202
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->UM_BOTAO:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 204
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 205
    const v1, 0x7f07022b

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 206
    const v1, 0x7f07048c

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNeutro(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v2

    .line 208
    .local v2, "dialogInterno":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    new-instance v0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity$3;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity$3;-><init>(Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNeutroListener(Landroid/view/View$OnClickListener;)V

    .line 214
    invoke-virtual {v2, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 215
    return-void
.end method

.method private getListaGeral()Ljava/util/List;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;>;"
        }
    .end annotation

    .line 102
    new-instance v4, Ljava/util/ArrayList;

    const/4 v0, 0x5

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 104
    .local v4, "itens":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;>;"
    new-instance v0, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;

    .line 105
    const v1, 0x7f07025c

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 106
    const v2, 0x7f070115

    invoke-virtual {p0, v2}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    .line 104
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    return-object v4
.end method

.method private getListaSeguranca()Ljava/util/List;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;>;"
        }
    .end annotation

    .line 81
    new-instance v4, Ljava/util/ArrayList;

    const/4 v0, 0x5

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 83
    .local v4, "itens":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;>;"
    new-instance v0, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;

    .line 84
    const v1, 0x7f070282

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 85
    const v2, 0x7f07061a

    invoke-virtual {p0, v2}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    .line 83
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    new-instance v0, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;

    .line 88
    const v1, 0x7f070265

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 89
    const v2, 0x7f07061d

    invoke-virtual {p0, v2}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    .line 87
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->isPossuiFingerprint()Z

    move-result v0

    if-eqz v0, :cond_51

    .line 93
    new-instance v0, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;

    .line 94
    const v1, 0x7f070273

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 95
    const v2, 0x7f07061b

    invoke-virtual {p0, v2}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/api/universal/model/ItemConfiguracoes;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)V

    .line 93
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    :cond_51
    return-object v4
.end method

.method public static intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;
    .registers 2
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;

    .line 223
    new-instance v0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity_$IntentBuilder_;-><init>(Landroid/support/v4/app/Fragment;)V

    return-object v0
.end method

.method private isPossuiFingerprint()Z
    .registers 2

    .line 143
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->fingerprintManager:Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;

    if-nez v0, :cond_a

    .line 144
    invoke-static {p0}, Lcom/itau/empresas/feature/chaveacesso/FingerPrintManagerFactory;->criaFingerPrintManager(Landroid/content/Context;)Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->fingerprintManager:Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;

    .line 147
    :cond_a
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->fingerprintManager:Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;

    if-eqz v0, :cond_1e

    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->verificaFingerprintHabilitado()Z

    move-result v0

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->fingerprintManager:Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;

    .line 148
    invoke-interface {v0}, Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_1e

    const/4 v0, 0x1

    goto :goto_1f

    :cond_1e
    const/4 v0, 0x0

    .line 147
    :goto_1f
    return v0
.end method

.method private montarAdapter()V
    .registers 6

    .line 66
    new-instance v0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;

    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->getListaSeguranca()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->getListaGeral()Ljava/util/List;

    move-result-object v2

    .line 67
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->isPossuiFingerprint()Z

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;-><init>(Ljava/util/List;Ljava/util/List;Z)V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->mAdapter:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->mAdapter:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;

    invoke-virtual {v0, p0}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->setClickListener(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnConfiguracoesItemSelected;)V

    .line 70
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->mAdapter:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;

    invoke-virtual {v0, p0}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;->setFingerprintListerner(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter$OnFingerprintChanged;)V

    .line 72
    new-instance v4, Landroid/support/v7/widget/LinearLayoutManager;

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {v4, p0, v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 75
    .local v4, "layoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->rvConfiguracoes:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->rvConfiguracoes:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->mAdapter:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioConfiguracoesAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 78
    return-void
.end method

.method private verificaFingerprintHabilitado()Z
    .registers 3

    .line 139
    invoke-static {}, Lcom/itau/empresas/ui/util/RemoteBundleUtils;->getInstance()Lcom/itau/empresas/ui/util/RemoteBundleUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/RemoteBundleUtils;->isFingerprint(Lcom/itau/empresas/CustomApplication;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method afterViews()V
    .registers 1

    .line 61
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->constroiToolbar()V

    .line 62
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->montarAdapter()V

    .line 63
    return-void
.end method

.method public aoSelecionar(I)V
    .registers 4
    .param p1, "opcaoSelecionada"    # I

    .line 113
    packed-switch p1, :pswitch_data_22

    goto :goto_21

    .line 115
    :pswitch_4
    invoke-static {p0}, Lcom/itau/empresas/ui/activity/GenericaActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;

    move-result-object v0

    const-class v1, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;

    .line 116
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;->fragmentClass(Ljava/lang/Class;)Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;

    move-result-object v0

    .line 117
    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 118
    goto :goto_21

    .line 120
    :pswitch_12
    invoke-static {p0}, Lcom/itau/empresas/feature/itoken/ITokenActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/itoken/ITokenActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/itoken/ITokenActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 121
    goto :goto_21

    .line 124
    :pswitch_1a
    invoke-static {p0}, Lcom/itau/empresas/ui/activity/BetaActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/BetaActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BetaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 125
    .line 129
    :goto_21
    :pswitch_21
    return-void

    :pswitch_data_22
    .packed-switch 0x1
        :pswitch_4
        :pswitch_12
        :pswitch_21
        :pswitch_1a
        :pswitch_1a
    .end packed-switch
.end method

.method public aoSelecionar(Z)V
    .registers 6
    .param p1, "ativado"    # Z

    .line 165
    if-eqz p1, :cond_13

    .line 166
    invoke-static {p0}, Lcom/itau/empresas/ui/activity/FingerprintActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/FingerprintActivity_$IntentBuilder_;

    move-result-object v0

    const-string v1, "isRedirecionaConfiguracoes"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/activity/FingerprintActivity_$IntentBuilder_;->extra(Ljava/lang/String;Z)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/FingerprintActivity_$IntentBuilder_;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/FingerprintActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    goto :goto_4e

    .line 168
    :cond_13
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->DOIS_BOTOES:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 170
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 171
    const v1, 0x7f070232

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 172
    const v1, 0x7f0704d2

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoPositivo(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 173
    const v1, 0x7f070479

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNegativo(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v3

    .line 175
    .local v3, "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    new-instance v0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity$1;

    invoke-direct {v0, p0, v3}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity$1;-><init>(Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v3, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNegativoListener(Landroid/view/View$OnClickListener;)V

    .line 183
    new-instance v0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity$2;

    invoke-direct {v0, p0, v3}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity$2;-><init>(Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v3, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setPositivoListener(Landroid/view/View$OnClickListener;)V

    .line 197
    invoke-virtual {v3, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 199
    .end local v3    # "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    :goto_4e
    return-void
.end method

.method protected onResume()V
    .registers 1

    .line 154
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->onResume()V

    .line 156
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioConfiguracoesActivity;->atualizarSwitchFingerprint()V

    .line 157
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 56
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.class public Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "AreaUsuarioActivity.java"

# interfaces
.implements Lcom/itau/empresas/ui/view/ItauToolbar$ItauToolbarListener;


# instance fields
.field iconeTrocaConta:Lcom/itau/empresas/ui/view/TextViewIcon;

.field private isExibindoConfiguracoes:Z

.field mItauToolbar:Lcom/itau/empresas/ui/view/ItauToolbar;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 28
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->isExibindoConfiguracoes:Z

    return-void
.end method

.method private carregarFragmentDeConfiguracoes()V
    .registers 3

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v1

    .line 80
    .local v1, "dadosOperador":Lcom/itau/empresas/api/model/DadosOperadorVO;
    invoke-static {v1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->novaInstancia(Lcom/itau/empresas/api/model/DadosOperadorVO;)Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->trocarFragment(Lcom/itau/empresas/ui/fragment/BaseFragment;)V

    .line 81
    const v0, 0x7f070102

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->disparaEventoAnalytics(Ljava/lang/String;)V

    .line 82
    return-void
.end method

.method private carregarFragmentDeContas()V
    .registers 2

    .line 85
    invoke-static {}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment_;->builder()Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment_$FragmentBuilder_;->build()Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->trocarFragment(Lcom/itau/empresas/ui/fragment/BaseFragment;)V

    .line 86
    const v0, 0x7f070104

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->disparaEventoAnalytics(Ljava/lang/String;)V

    .line 87
    return-void
.end method

.method private carregarInformacoesContaLogada(Lcom/itau/empresas/api/model/ContaOperadorVO;)V
    .registers 6
    .param p1, "contaSelecionada"    # Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 55
    if-nez p1, :cond_3

    .line 56
    return-void

    .line 58
    :cond_3
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->mItauToolbar:Lcom/itau/empresas/ui/view/ItauToolbar;

    .line 59
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getApelido()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/itau/empresas/ui/util/StringUtils;->converteTodaPrimeiraLetraParaMaiusculo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 58
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/ItauToolbar;->setTitulo(Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->mItauToolbar:Lcom/itau/empresas/ui/view/ItauToolbar;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 62
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 61
    const v2, 0x7f07058d

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/ItauToolbar;->setSubtitulo(Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->mItauToolbar:Lcom/itau/empresas/ui/view/ItauToolbar;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    .line 65
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 64
    const v2, 0x7f07058e

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/ItauToolbar;->setSubtitulo2(Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->mItauToolbar:Lcom/itau/empresas/ui/view/ItauToolbar;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 69
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getApelido()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 68
    const v2, 0x7f070094

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 67
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/ItauToolbar;->setTituloContentDescription(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->mItauToolbar:Lcom/itau/empresas/ui/view/ItauToolbar;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 72
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 71
    const v2, 0x7f07008a

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/ItauToolbar;->setSubtitulo1ContentDescription(Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->mItauToolbar:Lcom/itau/empresas/ui/view/ItauToolbar;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    .line 75
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 74
    const v2, 0x7f070093

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/ItauToolbar;->setSubtitulo2ContentDescription(Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method private disparaEventoAnalytics(Ljava/lang/String;)V
    .registers 5
    .param p1, "nome"    # Ljava/lang/String;

    .line 165
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v2

    .line 166
    .line 168
    .local v2, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    const v0, 0x7f0700d3

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 169
    const v1, 0x7f0700c3

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 166
    invoke-virtual {v2, p1, v0, v1}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    return-void
.end method

.method private exibeOpcaoDeTrocaDeContas(Z)V
    .registers 4
    .param p1, "isExibe"    # Z

    .line 152
    if-eqz p1, :cond_4

    const/4 v1, 0x0

    goto :goto_6

    :cond_4
    const/16 v1, 0x8

    .line 153
    .local v1, "visibilidade":I
    :goto_6
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->iconeTrocaConta:Lcom/itau/empresas/ui/view/TextViewIcon;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/TextViewIcon;->setVisibility(I)V

    .line 154
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 148
    new-instance v0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private isExibeTrocaDeContas()Z
    .registers 3

    .line 157
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getPerfilLogado()Lcom/itau/empresas/api/model/PerfilOperadorVO;

    move-result-object v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 158
    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getPerfilLogado()Lcom/itau/empresas/api/model/PerfilOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->getContasOperador()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_16

    .line 159
    :cond_14
    const/4 v0, 0x0

    return v0

    .line 161
    :cond_16
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getPerfilLogado()Lcom/itau/empresas/api/model/PerfilOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->getContasOperador()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_29

    const/4 v0, 0x1

    goto :goto_2a

    :cond_29
    const/4 v0, 0x0

    :goto_2a
    return v0
.end method

.method private trocarFragment(Lcom/itau/empresas/ui/fragment/BaseFragment;)V
    .registers 5
    .param p1, "fragment"    # Lcom/itau/empresas/ui/fragment/BaseFragment;

    .line 90
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 91
    .local v1, "fm":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 93
    .local v2, "ft":Landroid/support/v4/app/FragmentTransaction;
    const v0, 0x7f0e00bc

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 94
    const v0, 0x7f0e00bc

    invoke-virtual {v2, v0, p1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_1e

    .line 96
    :cond_18
    const v0, 0x7f0e00bc

    invoke-virtual {v2, v0, p1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 99
    :goto_1e
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 100
    return-void
.end method


# virtual methods
.method afterViews()V
    .registers 3

    .line 42
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->mItauToolbar:Lcom/itau/empresas/ui/view/ItauToolbar;

    invoke-virtual {v0, p0}, Lcom/itau/empresas/ui/view/ItauToolbar;->addItauToolbarListener(Lcom/itau/empresas/ui/view/ItauToolbar$ItauToolbarListener;)V

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->mItauToolbar:Lcom/itau/empresas/ui/view/ItauToolbar;

    .line 45
    const v1, 0x7f0700a5

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 44
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/ItauToolbar;->setSetaCimaContentDescription(Ljava/lang/String;)V

    .line 47
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->carregarFragmentDeConfiguracoes()V

    .line 49
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->isExibeTrocaDeContas()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->exibeOpcaoDeTrocaDeContas(Z)V

    .line 51
    const v0, 0x7f070103

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->disparaEventoAnalytics(Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method public onClick(Landroid/support/design/widget/AppBarLayout;Z)V
    .registers 5
    .param p1, "appBarLayout"    # Landroid/support/design/widget/AppBarLayout;
    .param p2, "isExpandido"    # Z

    .line 109
    if-nez p2, :cond_4

    const/4 v0, 0x1

    goto :goto_5

    :cond_4
    const/4 v0, 0x0

    :goto_5
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/support/design/widget/AppBarLayout;->setExpanded(ZZ)V

    .line 110
    return-void
.end method

.method public onClickSetaBaixo()V
    .registers 2

    .line 119
    iget-boolean v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->isExibindoConfiguracoes:Z

    if-nez v0, :cond_6

    const/4 v0, 0x1

    goto :goto_7

    :cond_6
    const/4 v0, 0x0

    :goto_7
    iput-boolean v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->isExibindoConfiguracoes:Z

    .line 121
    iget-boolean v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->isExibindoConfiguracoes:Z

    if-eqz v0, :cond_11

    .line 122
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->carregarFragmentDeConfiguracoes()V

    goto :goto_14

    .line 124
    :cond_11
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->carregarFragmentDeContas()V

    .line 126
    :goto_14
    return-void
.end method

.method public onClickSetaEsquerda()V
    .registers 1

    .line 114
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->onBackPressed()V

    .line 115
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/api/model/ContaOperadorVO;)V
    .registers 3
    .param p1, "conta"    # Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 129
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->escondeDialogoDeProgresso()V

    .line 130
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/CustomApplication;->setContaSelecionada(Lcom/itau/empresas/api/model/ContaOperadorVO;)V

    .line 131
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->limpaCachePendencias()V

    .line 133
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->carregarInformacoesContaLogada(Lcom/itau/empresas/api/model/ContaOperadorVO;)V

    .line 135
    const/16 v0, 0x63

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->setResult(I)V

    .line 137
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->finish()V

    .line 138
    return-void
.end method

.method protected onResume()V
    .registers 2

    .line 142
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->onResume()V

    .line 144
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->carregarInformacoesContaLogada(Lcom/itau/empresas/api/model/ContaOperadorVO;)V

    .line 145
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 104
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

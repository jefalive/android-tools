.class public Lbr/com/itau/sdk/android/leitorboleto/ui/utils/a;
.super Landroid/text/style/MetricAffectingSpan;


# instance fields
.field private final a:Landroid/graphics/Typeface;


# direct methods
.method public constructor <init>(Landroid/graphics/Typeface;)V
    .registers 4

    invoke-direct {p0}, Landroid/text/style/MetricAffectingSpan;-><init>()V

    if-nez p1, :cond_d

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "typeface is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    iput-object p1, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/a;->a:Landroid/graphics/Typeface;

    return-void
.end method

.method private a(Landroid/graphics/Paint;)V
    .registers 7

    invoke-virtual {p1}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    if-eqz v2, :cond_b

    invoke-virtual {v2}, Landroid/graphics/Typeface;->getStyle()I

    move-result v3

    goto :goto_c

    :cond_b
    const/4 v3, 0x0

    :goto_c
    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/a;->a:Landroid/graphics/Typeface;

    invoke-virtual {v0}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    xor-int/lit8 v0, v0, -0x1

    and-int v4, v3, v0

    and-int/lit8 v0, v4, 0x1

    if-eqz v0, :cond_1e

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    :cond_1e
    and-int/lit8 v0, v4, 0x2

    if-eqz v0, :cond_27

    const/high16 v0, -0x41800000    # -0.25f

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextSkewX(F)V

    :cond_27
    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/a;->a:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    return-void
.end method


# virtual methods
.method public updateDrawState(Landroid/text/TextPaint;)V
    .registers 2

    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/a;->a(Landroid/graphics/Paint;)V

    return-void
.end method

.method public updateMeasureState(Landroid/text/TextPaint;)V
    .registers 2

    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/a;->a(Landroid/graphics/Paint;)V

    return-void
.end method

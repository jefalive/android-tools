.class public Lcom/google/android/gms/internal/zzih;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/zzip$zzb;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private zzJA:Z

.field private zzJz:Z

.field private final zzLA:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<Ljava/lang/Thread;>;"
        }
    .end annotation
.end field

.field private zzLB:Ljava/lang/Boolean;

.field private zzLC:Z

.field private zzLD:Z

.field private final zzLq:Ljava/lang/String;

.field private final zzLr:Lcom/google/android/gms/internal/zzii;

.field private zzLs:Ljava/math/BigInteger;

.field private final zzLt:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<Lcom/google/android/gms/internal/zzig;>;"
        }
    .end annotation
.end field

.field private final zzLu:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/gms/internal/zzik;>;"
        }
    .end annotation
.end field

.field private zzLv:Z

.field private zzLw:I

.field private zzLx:Lcom/google/android/gms/internal/zzbv;

.field private zzLy:Lcom/google/android/gms/internal/zzbf;

.field private zzLz:Ljava/lang/String;

.field private zzpT:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

.field private final zzpV:Ljava/lang/Object;

.field private zzpl:Lcom/google/android/gms/internal/zzax;

.field private zzqA:Z

.field private zzsZ:Lcom/google/android/gms/internal/zzbe;

.field private zzta:Lcom/google/android/gms/internal/zzbd;

.field private final zztb:Lcom/google/android/gms/internal/zzha;

.field private zzzN:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/zzir;)V
    .registers 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    sget-object v0, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    iput-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLs:Ljava/math/BigInteger;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLt:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLu:Ljava/util/HashMap;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzih;->zzLv:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzih;->zzJz:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/internal/zzih;->zzLw:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzih;->zzqA:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLx:Lcom/google/android/gms/internal/zzbv;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzih;->zzJA:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzsZ:Lcom/google/android/gms/internal/zzbe;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLy:Lcom/google/android/gms/internal/zzbf;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzta:Lcom/google/android/gms/internal/zzbd;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLA:Ljava/util/LinkedList;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzih;->zztb:Lcom/google/android/gms/internal/zzha;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLB:Ljava/lang/Boolean;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzih;->zzLC:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzih;->zzLD:Z

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzir;->zzhs()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLq:Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/internal/zzii;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->zzLq:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/zzii;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLr:Lcom/google/android/gms/internal/zzii;

    return-void
.end method


# virtual methods
.method public getSessionId()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLq:Ljava/lang/String;

    return-object v0
.end method

.method public zzB(Z)V
    .registers 6

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzih;->zzJA:Z

    if-eq v0, p1, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/internal/zzip;->zzb(Landroid/content/Context;Z)Ljava/util/concurrent/Future;

    :cond_c
    iput-boolean p1, p0, Lcom/google/android/gms/internal/zzih;->zzJA:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/zzih;->zzG(Landroid/content/Context;)Lcom/google/android/gms/internal/zzbf;

    move-result-object v2

    if-eqz v2, :cond_24

    invoke-virtual {v2}, Lcom/google/android/gms/internal/zzbf;->isAlive()Z

    move-result v0

    if-nez v0, :cond_24

    const-string v0, "start fetching content..."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaJ(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/gms/internal/zzbf;->zzcG()V
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_26

    :cond_24
    monitor-exit v1

    goto :goto_29

    :catchall_26
    move-exception v3

    monitor-exit v1

    throw v3

    :goto_29
    return-void
.end method

.method public zzC(Z)V
    .registers 4

    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v0

    :try_start_3
    iput-boolean p1, p0, Lcom/google/android/gms/internal/zzih;->zzLC:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v0

    goto :goto_a

    :catchall_7
    move-exception v1

    monitor-exit v0

    throw v1

    :goto_a
    return-void
.end method

.method public zzG(Landroid/content/Context;)Lcom/google/android/gms/internal/zzbf;
    .registers 12

    sget-object v0, Lcom/google/android/gms/internal/zzbt;->zzwj:Lcom/google/android/gms/internal/zzbp;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzbp;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-static {}, Lcom/google/android/gms/internal/zzne;->zzsg()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzih;->zzgY()Z

    move-result v0

    if-eqz v0, :cond_1c

    :cond_1a
    const/4 v0, 0x0

    return-object v0

    :cond_1c
    iget-object v8, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v8

    :try_start_1f
    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzsZ:Lcom/google/android/gms/internal/zzbe;
    :try_end_21
    .catchall {:try_start_1f .. :try_end_21} :catchall_68

    if-nez v0, :cond_3a

    instance-of v0, p1, Landroid/app/Activity;

    if-nez v0, :cond_2a

    monitor-exit v8

    const/4 v0, 0x0

    return-object v0

    :cond_2a
    :try_start_2a
    new-instance v0, Lcom/google/android/gms/internal/zzbe;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Application;

    move-object v2, p1

    check-cast v2, Landroid/app/Activity;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/internal/zzbe;-><init>(Landroid/app/Application;Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzsZ:Lcom/google/android/gms/internal/zzbe;

    :cond_3a
    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzta:Lcom/google/android/gms/internal/zzbd;

    if-nez v0, :cond_45

    new-instance v0, Lcom/google/android/gms/internal/zzbd;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzbd;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzta:Lcom/google/android/gms/internal/zzbd;

    :cond_45
    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLy:Lcom/google/android/gms/internal/zzbf;

    if-nez v0, :cond_5f

    new-instance v0, Lcom/google/android/gms/internal/zzbf;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->zzsZ:Lcom/google/android/gms/internal/zzbe;

    iget-object v2, p0, Lcom/google/android/gms/internal/zzih;->zzta:Lcom/google/android/gms/internal/zzbd;

    new-instance v3, Lcom/google/android/gms/internal/zzha;

    iget-object v4, p0, Lcom/google/android/gms/internal/zzih;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/gms/internal/zzih;->zzpT:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/google/android/gms/internal/zzha;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Ljava/lang/Thread$UncaughtExceptionHandler;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/internal/zzbf;-><init>(Lcom/google/android/gms/internal/zzbe;Lcom/google/android/gms/internal/zzbd;Lcom/google/android/gms/internal/zzha;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLy:Lcom/google/android/gms/internal/zzbf;

    :cond_5f
    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLy:Lcom/google/android/gms/internal/zzbf;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzbf;->zzcG()V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLy:Lcom/google/android/gms/internal/zzbf;
    :try_end_66
    .catchall {:try_start_2a .. :try_end_66} :catchall_68

    monitor-exit v8

    return-object v0

    :catchall_68
    move-exception v9

    monitor-exit v8

    throw v9
.end method

.method public zza(Landroid/content/Context;Lcom/google/android/gms/internal/zzij;Ljava/lang/String;)Landroid/os/Bundle;
    .registers 13

    iget-object v2, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v0, "app"

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->zzLr:Lcom/google/android/gms/internal/zzii;

    invoke-virtual {v1, p1, p3}, Lcom/google/android/gms/internal/zzii;->zzc(Landroid/content/Context;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLu:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_22
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3f

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLu:Ljava/util/HashMap;

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/zzik;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzik;->toBundle()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_22

    :cond_3f
    const-string v0, "slots"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLt:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4f
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_64

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/android/gms/internal/zzig;

    invoke-virtual {v7}, Lcom/google/android/gms/internal/zzig;->toBundle()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4f

    :cond_64
    const-string v0, "ads"

    invoke-virtual {v3, v0, v5}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLt:Ljava/util/HashSet;

    invoke-interface {p2, v0}, Lcom/google/android/gms/internal/zzij;->zza(Ljava/util/HashSet;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLt:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V
    :try_end_73
    .catchall {:try_start_3 .. :try_end_73} :catchall_75

    monitor-exit v2

    return-object v3

    :catchall_75
    move-exception v8

    monitor-exit v2

    throw v8
.end method

.method public zza(Landroid/content/Context;Z)Ljava/util/concurrent/Future;
    .registers 6

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzih;->zzJz:Z

    if-eq p2, v0, :cond_f

    iput-boolean p2, p0, Lcom/google/android/gms/internal/zzih;->zzJz:Z

    invoke-static {p1, p2}, Lcom/google/android/gms/internal/zzip;->zza(Landroid/content/Context;Z)Ljava/util/concurrent/Future;
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_12

    move-result-object v0

    monitor-exit v1

    return-object v0

    :cond_f
    monitor-exit v1

    const/4 v0, 0x0

    return-object v0

    :catchall_12
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method public zza(Lcom/google/android/gms/internal/zzig;)V
    .registers 5

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLt:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_a

    monitor-exit v1

    goto :goto_d

    :catchall_a
    move-exception v2

    monitor-exit v1

    throw v2

    :goto_d
    return-void
.end method

.method public zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzik;)V
    .registers 6

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLu:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_a

    monitor-exit v1

    goto :goto_d

    :catchall_a
    move-exception v2

    monitor-exit v1

    throw v2

    :goto_d
    return-void
.end method

.method public zza(Ljava/lang/Thread;)V
    .registers 4

    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->zzpT:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    invoke-static {v0, p1, v1}, Lcom/google/android/gms/internal/zzha;->zza(Landroid/content/Context;Ljava/lang/Thread;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)Lcom/google/android/gms/internal/zzha;

    return-void
.end method

.method public zzaA(Ljava/lang/String;)Ljava/util/concurrent/Future;
    .registers 5

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    if-eqz p1, :cond_17

    :try_start_5
    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLz:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17

    iput-object p1, p0, Lcom/google/android/gms/internal/zzih;->zzLz:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/internal/zzip;->zzd(Landroid/content/Context;Ljava/lang/String;)Ljava/util/concurrent/Future;
    :try_end_14
    .catchall {:try_start_5 .. :try_end_14} :catchall_1a

    move-result-object v0

    monitor-exit v1

    return-object v0

    :cond_17
    monitor-exit v1

    const/4 v0, 0x0

    return-object v0

    :catchall_1a
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method public zzb(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V
    .registers 12
    .annotation build Landroid/annotation/TargetApi;
        value = 0x17
    .end annotation

    iget-object v7, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v7

    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzih;->zzqA:Z

    if-nez v0, :cond_70

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzih;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzih;->zzpT:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    invoke-static {p1, p0}, Lcom/google/android/gms/internal/zzip;->zza(Landroid/content/Context;Lcom/google/android/gms/internal/zzip$zzb;)Ljava/util/concurrent/Future;

    invoke-static {p1, p0}, Lcom/google/android/gms/internal/zzip;->zzb(Landroid/content/Context;Lcom/google/android/gms/internal/zzip$zzb;)Ljava/util/concurrent/Future;

    invoke-static {p1, p0}, Lcom/google/android/gms/internal/zzip;->zzc(Landroid/content/Context;Lcom/google/android/gms/internal/zzip$zzb;)Ljava/util/concurrent/Future;

    invoke-static {p1, p0}, Lcom/google/android/gms/internal/zzip;->zzd(Landroid/content/Context;Lcom/google/android/gms/internal/zzip$zzb;)Ljava/util/concurrent/Future;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/zzih;->zza(Ljava/lang/Thread;)V

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbC()Lcom/google/android/gms/internal/zzir;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->afmaVersion:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/internal/zzir;->zze(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzzN:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/internal/zzne;->zzsn()Z

    move-result v0

    if-eqz v0, :cond_41

    invoke-static {}, Landroid/security/NetworkSecurityPolicy;->getInstance()Landroid/security/NetworkSecurityPolicy;

    move-result-object v0

    invoke-virtual {v0}, Landroid/security/NetworkSecurityPolicy;->isCleartextTrafficPermitted()Z

    move-result v0

    if-nez v0, :cond_41

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzih;->zzLD:Z

    :cond_41
    new-instance v0, Lcom/google/android/gms/internal/zzax;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/zzih;->zzpT:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    new-instance v3, Lcom/google/android/gms/internal/zzeg;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/internal/zzih;->zzpT:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    sget-object v6, Lcom/google/android/gms/internal/zzbt;->zzvB:Lcom/google/android/gms/internal/zzbp;

    invoke-virtual {v6}, Lcom/google/android/gms/internal/zzbp;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/gms/internal/zzeg;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/internal/zzax;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Lcom/google/android/gms/internal/zzeg;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzpl:Lcom/google/android/gms/internal/zzax;

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzih;->zzhk()V

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbM()Lcom/google/android/gms/ads/internal/purchase/zzi;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/purchase/zzi;->zzz(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzih;->zzqA:Z
    :try_end_70
    .catchall {:try_start_3 .. :try_end_70} :catchall_72

    :cond_70
    monitor-exit v7

    goto :goto_75

    :catchall_72
    move-exception v8

    monitor-exit v7

    throw v8

    :goto_75
    return-void
.end method

.method public zzb(Ljava/lang/Boolean;)V
    .registers 4

    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v0

    :try_start_3
    iput-object p1, p0, Lcom/google/android/gms/internal/zzih;->zzLB:Ljava/lang/Boolean;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v0

    goto :goto_a

    :catchall_7
    move-exception v1

    monitor-exit v0

    throw v1

    :goto_a
    return-void
.end method

.method public zzb(Ljava/lang/Throwable;Z)V
    .registers 8

    new-instance v4, Lcom/google/android/gms/internal/zzha;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->zzpT:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/google/android/gms/internal/zzha;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Ljava/lang/Thread$UncaughtExceptionHandler;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    invoke-virtual {v4, p1, p2}, Lcom/google/android/gms/internal/zzha;->zza(Ljava/lang/Throwable;Z)V

    return-void
.end method

.method public zzb(Ljava/util/HashSet;)V
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/HashSet<Lcom/google/android/gms/internal/zzig;>;)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLt:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_a

    monitor-exit v1

    goto :goto_d

    :catchall_a
    move-exception v2

    monitor-exit v1

    throw v2

    :goto_d
    return-void
.end method

.method public zzd(ILjava/lang/String;)Ljava/lang/String;
    .registers 5

    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzpT:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->zzNb:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    goto :goto_13

    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/zze;->getRemoteResource(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object v1

    :goto_13
    if-nez v1, :cond_16

    return-object p2

    :cond_16
    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public zze(Landroid/os/Bundle;)V
    .registers 5

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    const-string v0, "use_https"

    :try_start_5
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    const-string v0, "use_https"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    goto :goto_14

    :cond_12
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzih;->zzJz:Z

    :goto_14
    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzih;->zzJz:Z

    const-string v0, "webview_cache_version"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    const-string v0, "webview_cache_version"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_27

    :cond_25
    iget v0, p0, Lcom/google/android/gms/internal/zzih;->zzLw:I

    :goto_27
    iput v0, p0, Lcom/google/android/gms/internal/zzih;->zzLw:I

    const-string v0, "content_url_opted_out"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3a

    const-string v0, "content_url_opted_out"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/zzih;->zzB(Z)V

    :cond_3a
    const-string v0, "content_url_hashes"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4a

    const-string v0, "content_url_hashes"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLz:Ljava/lang/String;
    :try_end_4a
    .catchall {:try_start_5 .. :try_end_4a} :catchall_4c

    :cond_4a
    monitor-exit v1

    goto :goto_4f

    :catchall_4c
    move-exception v2

    monitor-exit v1

    throw v2

    :goto_4f
    return-void
.end method

.method public zzgY()Z
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzih;->zzJA:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v1

    return v0

    :catchall_7
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method public zzgZ()Ljava/lang/String;
    .registers 6

    iget-object v2, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLs:Ljava/math/BigInteger;

    invoke-virtual {v0}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLs:Ljava/math/BigInteger;

    sget-object v1, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLs:Ljava/math/BigInteger;
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_15

    monitor-exit v2

    return-object v3

    :catchall_15
    move-exception v4

    monitor-exit v2

    throw v4
.end method

.method public zzha()Lcom/google/android/gms/internal/zzii;
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLr:Lcom/google/android/gms/internal/zzii;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v1

    return-object v0

    :catchall_7
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method public zzhb()Lcom/google/android/gms/internal/zzbv;
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLx:Lcom/google/android/gms/internal/zzbv;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v1

    return-object v0

    :catchall_7
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method public zzhc()Z
    .registers 5

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-boolean v2, p0, Lcom/google/android/gms/internal/zzih;->zzLv:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzih;->zzLv:Z
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_a

    monitor-exit v1

    return v2

    :catchall_a
    move-exception v3

    monitor-exit v1

    throw v3
.end method

.method public zzhd()Z
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzih;->zzJz:Z

    if-nez v0, :cond_b

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzih;->zzLD:Z
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_10

    if-eqz v0, :cond_d

    :cond_b
    const/4 v0, 0x1

    goto :goto_e

    :cond_d
    const/4 v0, 0x0

    :goto_e
    monitor-exit v1

    return v0

    :catchall_10
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method public zzhe()Ljava/lang/String;
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzzN:Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v1

    return-object v0

    :catchall_7
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method public zzhf()Ljava/lang/String;
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLz:Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v1

    return-object v0

    :catchall_7
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method public zzhg()Ljava/lang/Boolean;
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLB:Ljava/lang/Boolean;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v1

    return-object v0

    :catchall_7
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method public zzhh()Lcom/google/android/gms/internal/zzax;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzpl:Lcom/google/android/gms/internal/zzax;

    return-object v0
.end method

.method public zzhi()Z
    .registers 5

    iget-object v2, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget v0, p0, Lcom/google/android/gms/internal/zzih;->zzLw:I

    sget-object v1, Lcom/google/android/gms/internal/zzbt;->zzwA:Lcom/google/android/gms/internal/zzbp;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zzbp;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ge v0, v1, :cond_2b

    sget-object v0, Lcom/google/android/gms/internal/zzbt;->zzwA:Lcom/google/android/gms/internal/zzbp;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzbp;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/zzih;->zzLw:I

    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/google/android/gms/internal/zzih;->zzLw:I

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzip;->zza(Landroid/content/Context;I)Ljava/util/concurrent/Future;
    :try_end_28
    .catchall {:try_start_3 .. :try_end_28} :catchall_2e

    monitor-exit v2

    const/4 v0, 0x1

    return v0

    :cond_2b
    monitor-exit v2

    const/4 v0, 0x0

    return v0

    :catchall_2e
    move-exception v3

    monitor-exit v2

    throw v3
.end method

.method public zzhj()Z
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzih;->zzLC:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v1

    return v0

    :catchall_7
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method zzhk()V
    .registers 5

    new-instance v2, Lcom/google/android/gms/internal/zzbu;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzih;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzih;->zzpT:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->afmaVersion:Ljava/lang/String;

    invoke-direct {v2, v0, v1}, Lcom/google/android/gms/internal/zzbu;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    :try_start_b
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbH()Lcom/google/android/gms/internal/zzbw;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/zzbw;->zza(Lcom/google/android/gms/internal/zzbu;)Lcom/google/android/gms/internal/zzbv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzih;->zzLx:Lcom/google/android/gms/internal/zzbv;
    :try_end_15
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_15} :catch_16

    goto :goto_1c

    :catch_16
    move-exception v3

    const-string v0, "Cannot initialize CSI reporter."

    invoke-static {v0, v3}, Lcom/google/android/gms/internal/zzin;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1c
    return-void
.end method

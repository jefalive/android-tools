.class public final Lcom/itau/empresas/feature/login/LoginActivity_;
.super Lcom/itau/empresas/feature/login/LoginActivity;
.source "LoginActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/login/LoginActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 40
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity;-><init>()V

    .line 44
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/login/LoginActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/LoginActivity_;

    .line 40
    invoke-super {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/login/LoginActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/LoginActivity_;

    .line 40
    invoke-super {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 61
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 62
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 63
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 64
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->customApplication:Lcom/itau/empresas/CustomApplication;

    .line 65
    invoke-static {p0}, Lcom/itau/empresas/feature/login/controller/LoginController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/login/controller/LoginController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->loginController:Lcom/itau/empresas/feature/login/controller/LoginController;

    .line 66
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity_;->injectExtras_()V

    .line 67
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/LoginActivity_;->afterInject()V

    .line 68
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 147
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/LoginActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 148
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_5e

    .line 149
    const-string v0, "boletoPdfUri"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 150
    const-string v0, "boletoPdfUri"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->boletoPdfUri:Landroid/net/Uri;

    .line 152
    :cond_1c
    const-string v0, "instalarIToken"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 153
    const-string v0, "instalarIToken"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->instalarIToken:Z

    .line 155
    :cond_2c
    const-string v0, "operador"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 156
    const-string v0, "operador"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/OperadorVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->operador:Lcom/itau/empresas/api/model/OperadorVO;

    .line 158
    :cond_3e
    const-string v0, "lembrarAcesso"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 159
    const-string v0, "lembrarAcesso"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->lembrarAcesso:Z

    .line 161
    :cond_4e
    const-string v0, "iniciaExibindoTeclado"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5e

    .line 162
    const-string v0, "iniciaExibindoTeclado"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->iniciaExibindoTeclado:Z

    .line 165
    :cond_5e
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 190
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/login/LoginActivity_$5;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/login/LoginActivity_$5;-><init>(Lcom/itau/empresas/feature/login/LoginActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 198
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 178
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/login/LoginActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/login/LoginActivity_$4;-><init>(Lcom/itau/empresas/feature/login/LoginActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 186
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 53
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 54
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/login/LoginActivity_;->init_(Landroid/os/Bundle;)V

    .line 55
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/login/LoginActivity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 57
    const v0, 0x7f030050

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/login/LoginActivity_;->setContentView(I)V

    .line 58
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 6
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 102
    const v0, 0x7f0e00c9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->txtTitulo:Landroid/widget/TextView;

    .line 103
    const v0, 0x7f0e035d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->txtSubtitulo1:Landroid/widget/TextView;

    .line 104
    const v0, 0x7f0e035e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->txtSubtitulo2:Landroid/widget/TextView;

    .line 105
    const v0, 0x7f0e035c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->rlDadosContaEmOperacao:Landroid/widget/RelativeLayout;

    .line 106
    const v0, 0x7f0e0265

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/BotaoComProgresso;

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->botaoAcessar:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    .line 107
    const v0, 0x7f0e025a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 108
    const v0, 0x7f0e0263

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->textoLoginSenha:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f0e025f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->root:Landroid/view/ViewGroup;

    .line 110
    const v0, 0x7f0e0261

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 111
    .local v2, "view_cl_login_area_click_abrir_teclado":Landroid/view/View;
    const v0, 0x7f0e0266

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 113
    .local v3, "view_link_solucoes_negocio":Landroid/view/View;
    if-eqz v2, :cond_70

    .line 114
    new-instance v0, Lcom/itau/empresas/feature/login/LoginActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/login/LoginActivity_$1;-><init>(Lcom/itau/empresas/feature/login/LoginActivity_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    :cond_70
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->botaoAcessar:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    if-eqz v0, :cond_7e

    .line 124
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->botaoAcessar:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    new-instance v1, Lcom/itau/empresas/feature/login/LoginActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/login/LoginActivity_$2;-><init>(Lcom/itau/empresas/feature/login/LoginActivity_;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    :cond_7e
    if-eqz v3, :cond_88

    .line 134
    new-instance v0, Lcom/itau/empresas/feature/login/LoginActivity_$3;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/login/LoginActivity_$3;-><init>(Lcom/itau/empresas/feature/login/LoginActivity_;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    :cond_88
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/LoginActivity_;->aoCriar()V

    .line 144
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 72
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/login/LoginActivity;->setContentView(I)V

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 74
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 84
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/login/LoginActivity;->setContentView(Landroid/view/View;)V

    .line 85
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 86
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 78
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/login/LoginActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 80
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 169
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/login/LoginActivity;->setIntent(Landroid/content/Intent;)V

    .line 170
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity_;->injectExtras_()V

    .line 171
    return-void
.end method

.class public final Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;
.super Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;
.source "PagamentoGpsActiviy_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 40
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;-><init>()V

    .line 44
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;

    .line 40
    invoke-super {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;

    .line 40
    invoke-super {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 56
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 57
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 58
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->application:Lcom/itau/empresas/CustomApplication;

    .line 59
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->app:Lcom/itau/empresas/CustomApplication;

    .line 60
    invoke-static {p0}, Lcom/itau/empresas/controller/PagamentoGPSController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/controller/PagamentoGPSController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->controller:Lcom/itau/empresas/controller/PagamentoGPSController;

    .line 61
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->afterInject()V

    .line 62
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 310
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$15;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$15;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 318
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 298
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$14;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$14;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 306
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 49
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->init_(Landroid/os/Bundle;)V

    .line 50
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->onCreate(Landroid/os/Bundle;)V

    .line 51
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 52
    const v0, 0x7f030055

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->setContentView(I)V

    .line 53
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 11
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 96
    const v0, 0x7f0e028b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->root:Landroid/widget/LinearLayout;

    .line 97
    const v0, 0x7f0e05ab

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->campoNomeDaEmpresa:Landroid/widget/TextView;

    .line 98
    const v0, 0x7f0e0138

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->campoAgenciaConta:Landroid/widget/TextView;

    .line 99
    const v0, 0x7f0e04a9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->campoHorarioLimiteInicial:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0e04aa

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->campoHorarioLimiteFinal:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0e0139

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->campoCnpjPagador:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f0e03db

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->campoCompetencia:Landroid/widget/TextView;

    .line 103
    const v0, 0x7f0e05b0

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->iconeAbreListaCnpj:Landroid/widget/TextView;

    .line 104
    const v0, 0x7f0e03e5

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->campoValorTotalGps:Landroid/widget/EditText;

    .line 105
    const v0, 0x7f0e0317

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->campoDataPagamento:Landroid/widget/EditText;

    .line 106
    const v0, 0x7f0e03e0

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->campoValorInss:Landroid/widget/EditText;

    .line 107
    const v0, 0x7f0e03e4

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->campoAtualizacaoMonetaria:Landroid/widget/EditText;

    .line 108
    const v0, 0x7f0e03e2

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->campoValorOutrasEntidades:Landroid/widget/EditText;

    .line 109
    const v0, 0x7f0e02d9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->campoIdentificacaoComprovante:Landroid/widget/EditText;

    .line 110
    const v0, 0x7f0e03ca

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->campoReferenciaEmpresa:Landroid/widget/EditText;

    .line 111
    const v0, 0x7f0e03de

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->campoIdentificador:Landroid/widget/EditText;

    .line 112
    const v0, 0x7f0e03d8

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->campoCodigoPagamento:Landroid/widget/EditText;

    .line 113
    const v0, 0x7f0e03b0

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/EditTextCustomError;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->campoNomeContribuinte:Lcom/itau/empresas/ui/view/EditTextCustomError;

    .line 114
    const v0, 0x7f0e028d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->rlInclusaoGps:Landroid/widget/RelativeLayout;

    .line 115
    const v0, 0x7f0e028f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->rlAutorizacaoGps:Landroid/widget/RelativeLayout;

    .line 116
    const v0, 0x7f0e05ae

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->rlDadosCnpjPagador:Landroid/widget/RelativeLayout;

    .line 117
    const v0, 0x7f0e0285

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->rlHorarioLimite:Landroid/widget/RelativeLayout;

    .line 118
    const v0, 0x7f0e028c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->scrollGps:Landroid/widget/ScrollView;

    .line 119
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 120
    const v0, 0x7f0e03e9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 121
    .local v2, "view_imagem_informacoes":Landroid/view/View;
    const v0, 0x7f0e028e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 122
    .local v3, "view_botao_incluir_pagamento_gps":Landroid/view/View;
    const v0, 0x7f0e0291

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 123
    .local v4, "view_botao_incluir_autorizar_gps":Landroid/view/View;
    const v0, 0x7f0e0290

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 124
    .local v5, "view_botao_apenas_incluir_gps":Landroid/view/View;
    const v0, 0x7f0e03b3

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 125
    .local v6, "view_icone_periodo_apuracao":Landroid/view/View;
    const v0, 0x7f0e03c8

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 127
    .local v7, "view_icone_calendario_pagamento":Landroid/view/View;
    if-eqz v2, :cond_13c

    .line 128
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$1;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    :cond_13c
    if-eqz v3, :cond_146

    .line 138
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$2;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    :cond_146
    if-eqz v4, :cond_150

    .line 148
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$3;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$3;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;)V

    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    :cond_150
    if-eqz v5, :cond_15a

    .line 158
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$4;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$4;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;)V

    invoke-virtual {v5, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    :cond_15a
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->campoCnpjPagador:Landroid/widget/TextView;

    if-eqz v0, :cond_168

    .line 168
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->campoCnpjPagador:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$5;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$5;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    :cond_168
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->iconeAbreListaCnpj:Landroid/widget/TextView;

    if-eqz v0, :cond_176

    .line 178
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->iconeAbreListaCnpj:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$6;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$6;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 187
    :cond_176
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->rlDadosCnpjPagador:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_184

    .line 188
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->rlDadosCnpjPagador:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$7;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$7;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    :cond_184
    if-eqz v6, :cond_18e

    .line 198
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$8;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$8;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;)V

    invoke-virtual {v6, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 207
    :cond_18e
    if-eqz v7, :cond_198

    .line 208
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$9;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$9;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;)V

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 217
    :cond_198
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->campoDataPagamento:Landroid/widget/EditText;

    if-eqz v0, :cond_1a6

    .line 218
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->campoDataPagamento:Landroid/widget/EditText;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$10;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$10;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 228
    :cond_1a6
    const v0, 0x7f0e03e2

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/widget/TextView;

    .line 229
    .local v8, "view":Landroid/widget/TextView;
    if-eqz v8, :cond_1ba

    .line 230
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$11;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$11;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;)V

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 249
    .end local v8    # "view":Landroid/widget/TextView;
    :cond_1ba
    const v0, 0x7f0e03e0

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/widget/TextView;

    .line 250
    .local v8, "view":Landroid/widget/TextView;
    if-eqz v8, :cond_1ce

    .line 251
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$12;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$12;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;)V

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 270
    .end local v8    # "view":Landroid/widget/TextView;
    :cond_1ce
    const v0, 0x7f0e03e4

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/widget/TextView;

    .line 271
    .local v8, "view":Landroid/widget/TextView;
    if-eqz v8, :cond_1e2

    .line 272
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$13;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_$13;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;)V

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 290
    .end local v8    # "view":Landroid/widget/TextView;
    :cond_1e2
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->carregaElementosTela()V

    .line 291
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 66
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->setContentView(I)V

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 68
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 78
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->setContentView(Landroid/view/View;)V

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 80
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 72
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 74
    return-void
.end method

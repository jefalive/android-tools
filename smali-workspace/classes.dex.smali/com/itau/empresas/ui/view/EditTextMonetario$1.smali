.class Lcom/itau/empresas/ui/view/EditTextMonetario$1;
.super Lcom/itau/empresas/ui/util/SimpleTextWatcher;
.source "EditTextMonetario.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/view/EditTextMonetario;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/ui/view/EditTextMonetario;


# direct methods
.method constructor <init>(Lcom/itau/empresas/ui/view/EditTextMonetario;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/ui/view/EditTextMonetario;

    .line 28
    iput-object p1, p0, Lcom/itau/empresas/ui/view/EditTextMonetario$1;->this$0:Lcom/itau/empresas/ui/view/EditTextMonetario;

    invoke-direct {p0}, Lcom/itau/empresas/ui/util/SimpleTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 9
    .param p1, "editable"    # Landroid/text/Editable;

    .line 33
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 34
    .local v3, "s":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_76

    .line 35
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextMonetario$1;->this$0:Lcom/itau/empresas/ui/view/EditTextMonetario;

    invoke-virtual {v0, p0}, Lcom/itau/empresas/ui/view/EditTextMonetario;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 36
    const-string v0, "[R$,.]"

    const-string v1, ""

    invoke-virtual {v3, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 37
    .local v4, "cleanString":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_76

    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BRL "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/joda/money/Money;->parse(Ljava/lang/String;)Lorg/joda/money/Money;

    move-result-object v0

    new-instance v1, Ljava/math/BigDecimal;

    const/16 v2, 0x64

    invoke-direct {v1, v2}, Ljava/math/BigDecimal;-><init>(I)V

    sget-object v2, Ljava/math/RoundingMode;->HALF_DOWN:Ljava/math/RoundingMode;

    .line 39
    invoke-virtual {v0, v1, v2}, Lorg/joda/money/Money;->dividedBy(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v5

    .line 41
    .line 42
    .local v5, "money":Lorg/joda/money/Money;
    invoke-virtual {v5}, Lorg/joda/money/Money;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/view/EditTextMonetario$1;->this$0:Lcom/itau/empresas/ui/view/EditTextMonetario;

    # getter for: Lcom/itau/empresas/ui/view/EditTextMonetario;->mostraCifrao:Z
    invoke-static {v1}, Lcom/itau/empresas/ui/view/EditTextMonetario;->access$000(Lcom/itau/empresas/ui/view/EditTextMonetario;)Z

    move-result v1

    .line 41
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v6

    .line 44
    .local v6, "formatted":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextMonetario$1;->this$0:Lcom/itau/empresas/ui/view/EditTextMonetario;

    invoke-virtual {v0, v6}, Lcom/itau/empresas/ui/view/EditTextMonetario;->setText(Ljava/lang/CharSequence;)V

    .line 45
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextMonetario$1;->this$0:Lcom/itau/empresas/ui/view/EditTextMonetario;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/EditTextMonetario;->setSelection(I)V

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextMonetario$1;->this$0:Lcom/itau/empresas/ui/view/EditTextMonetario;

    # getter for: Lcom/itau/empresas/ui/view/EditTextMonetario;->onAfterTextChanged:Lcom/itau/empresas/ui/view/EditTextMonetario$OnAfterTextChanged;
    invoke-static {v0}, Lcom/itau/empresas/ui/view/EditTextMonetario;->access$100(Lcom/itau/empresas/ui/view/EditTextMonetario;)Lcom/itau/empresas/ui/view/EditTextMonetario$OnAfterTextChanged;

    move-result-object v0

    if-eqz v0, :cond_76

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextMonetario$1;->this$0:Lcom/itau/empresas/ui/view/EditTextMonetario;

    # getter for: Lcom/itau/empresas/ui/view/EditTextMonetario;->onAfterTextChanged:Lcom/itau/empresas/ui/view/EditTextMonetario$OnAfterTextChanged;
    invoke-static {v0}, Lcom/itau/empresas/ui/view/EditTextMonetario;->access$100(Lcom/itau/empresas/ui/view/EditTextMonetario;)Lcom/itau/empresas/ui/view/EditTextMonetario$OnAfterTextChanged;

    move-result-object v0

    invoke-interface {v0, v5}, Lcom/itau/empresas/ui/view/EditTextMonetario$OnAfterTextChanged;->onAfterTextChanged(Lorg/joda/money/Money;)V

    .line 51
    .end local v4    # "cleanString":Ljava/lang/String;
    .end local v5    # "money":Lorg/joda/money/Money;
    .end local v6    # "formatted":Ljava/lang/String;
    :cond_76
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextMonetario$1;->this$0:Lcom/itau/empresas/ui/view/EditTextMonetario;

    invoke-virtual {v0, p0}, Lcom/itau/empresas/ui/view/EditTextMonetario;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 52
    return-void
.end method

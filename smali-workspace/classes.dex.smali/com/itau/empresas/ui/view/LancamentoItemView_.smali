.class public final Lcom/itau/empresas/ui/view/LancamentoItemView_;
.super Lcom/itau/empresas/ui/view/LancamentoItemView;
.source "LancamentoItemView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 33
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/LancamentoItemView;-><init>(Landroid/content/Context;)V

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/LancamentoItemView_;->alreadyInflated_:Z

    .line 30
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/LancamentoItemView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 34
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/LancamentoItemView_;->init_()V

    .line 35
    return-void
.end method

.method private init_()V
    .registers 3

    .line 60
    iget-object v0, p0, Lcom/itau/empresas/ui/view/LancamentoItemView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 61
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 62
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 63
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 51
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/LancamentoItemView_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/LancamentoItemView_;->alreadyInflated_:Z

    .line 53
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/LancamentoItemView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0300b2

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/ui/view/LancamentoItemView_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/ui/view/LancamentoItemView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 56
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/ui/view/LancamentoItemView;->onFinishInflate()V

    .line 57
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 67
    const v0, 0x7f0e0429

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/LancamentoItemView_;->lancamentoItemDescricao:Landroid/widget/TextView;

    .line 68
    const v0, 0x7f0e042a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/LancamentoItemView_;->lancamentoItemValor:Landroid/widget/TextView;

    .line 69
    return-void
.end method

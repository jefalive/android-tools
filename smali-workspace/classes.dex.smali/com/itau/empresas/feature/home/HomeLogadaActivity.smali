.class public Lcom/itau/empresas/feature/home/HomeLogadaActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "HomeLogadaActivity.java"

# interfaces
.implements Lbr/com/itau/textovoz/SearchListenerBalance;
.implements Lcom/itau/empresas/feature/home/AbridorDeFragment;


# instance fields
.field private buscaGeral:Lcom/itau/empresas/feature/busca/BuscaGeral;

.field imgLogo:Lcom/itau/empresas/ui/view/TextViewIcon;

.field logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

.field private navigationListener:Lcom/itau/empresas/feature/home/NavigationListener;

.field navigationView:Landroid/support/design/widget/BottomNavigationView;

.field rlContainer:Landroid/widget/RelativeLayout;

.field rlDadosContaEmOperacao:Landroid/widget/RelativeLayout;

.field toolbar:Landroid/support/v7/widget/Toolbar;

.field txtSubtitulo1:Landroid/widget/TextView;

.field txtSubtitulo2:Landroid/widget/TextView;

.field txtTitulo:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 54
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/home/HomeLogadaActivity;)Landroid/support/v4/app/ActivityOptionsCompat;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/home/HomeLogadaActivity;

    .line 54
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->criarAnimacaoDeTransicao()Landroid/support/v4/app/ActivityOptionsCompat;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/home/HomeLogadaActivity;)Lcom/itau/empresas/CustomApplication;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/home/HomeLogadaActivity;

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->application:Lcom/itau/empresas/CustomApplication;

    return-object v0
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/home/HomeLogadaActivity;)Lcom/itau/empresas/CustomApplication;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/home/HomeLogadaActivity;

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->application:Lcom/itau/empresas/CustomApplication;

    return-object v0
.end method

.method private agendarAlarme()V
    .registers 7

    .line 186
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0}, Lcom/itau/empresas/AlarmeUtils;->usuarioEfetuouLogout(Lcom/itau/empresas/CustomApplication;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 187
    move-object v0, p0

    const/4 v1, 0x2

    const-wide/32 v2, 0x337f9800

    const-wide/32 v4, 0x337f9800

    invoke-static/range {v0 .. v5}, Lcom/itau/empresas/AlarmeUtils;->reagendarAlarme(Landroid/content/Context;IJJ)V

    .line 191
    const-string v0, "extrato_key"

    .line 192
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    iget-object v3, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 191
    invoke-static {v0, v1, v2, v3}, Lcom/itau/empresas/AlarmeUtils;->salvarDatetimeAlarme(Ljava/lang/String;JLcom/itau/empresas/CustomApplication;)V

    .line 195
    :cond_22
    const/4 v0, 0x3

    invoke-static {p0, v0}, Lcom/itau/empresas/AlarmeUtils;->cancelarAlarme(Landroid/content/Context;I)V

    .line 197
    const-string v0, "ano_novo_key"

    iget-object v1, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0, v1}, Lcom/itau/empresas/AlarmeUtils;->buscarDatetimeAlarme(Ljava/lang/String;Lcom/itau/empresas/CustomApplication;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_37

    .line 199
    invoke-static {p0}, Lcom/itau/empresas/NotificacaoUtils;->configurarNotificacaoFimAno(Landroid/content/Context;)V

    .line 201
    :cond_37
    return-void
.end method

.method private carregarInformacoesContaLogada()V
    .registers 6

    .line 242
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v4

    .line 243
    .local v4, "contaSelecionada":Lcom/itau/empresas/api/model/ContaOperadorVO;
    if-eqz v4, :cond_74

    .line 244
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->txtTitulo:Landroid/widget/TextView;

    .line 245
    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getApelido()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/itau/empresas/ui/util/StringUtils;->converteTodaPrimeiraLetraParaMaiusculo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 244
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->txtSubtitulo1:Landroid/widget/TextView;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 248
    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 247
    const v2, 0x7f07058d

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->txtSubtitulo2:Landroid/widget/TextView;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    .line 251
    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 250
    const v2, 0x7f07058e

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 253
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->rlDadosContaEmOperacao:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    .line 255
    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getApelido()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 256
    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 257
    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    .line 258
    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    .line 254
    const v2, 0x7f0700a7

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 253
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_7b

    .line 260
    :cond_74
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {p0, v0}, Lcom/itau/empresas/feature/logout/LogoutHelper;->trataSessaoExpirada(Landroid/app/Activity;Lcom/itau/empresas/CustomApplication;)V

    .line 262
    :goto_7b
    return-void
.end method

.method private criarAnimacaoDeTransicao()Landroid/support/v4/app/ActivityOptionsCompat;
    .registers 7

    .line 227
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->imgLogo:Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 228
    const v1, 0x7f070379

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 227
    invoke-static {v0, v1}, Landroid/support/v4/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/support/v4/util/Pair;

    move-result-object v2

    .line 230
    .local v2, "p1":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Landroid/view/View;Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->txtTitulo:Landroid/widget/TextView;

    .line 231
    const v1, 0x7f07037c

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 230
    invoke-static {v0, v1}, Landroid/support/v4/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/support/v4/util/Pair;

    move-result-object v3

    .line 232
    .local v3, "p2":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Landroid/view/View;Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->txtSubtitulo1:Landroid/widget/TextView;

    .line 233
    const v1, 0x7f07037a

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 232
    invoke-static {v0, v1}, Landroid/support/v4/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/support/v4/util/Pair;

    move-result-object v4

    .line 234
    .local v4, "p3":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Landroid/view/View;Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->txtSubtitulo2:Landroid/widget/TextView;

    .line 235
    const v1, 0x7f07037b

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 234
    invoke-static {v0, v1}, Landroid/support/v4/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/support/v4/util/Pair;

    move-result-object v5

    .line 237
    .local v5, "p4":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Landroid/view/View;Ljava/lang/String;>;"
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/support/v4/util/Pair;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object v3, v0, v1

    const/4 v1, 0x2

    aput-object v4, v0, v1

    const/4 v1, 0x3

    aput-object v5, v0, v1

    .line 238
    invoke-static {p0, v0}, Landroid/support/v4/app/ActivityOptionsCompat;->makeSceneTransitionAnimation(Landroid/app/Activity;[Landroid/support/v4/util/Pair;)Landroid/support/v4/app/ActivityOptionsCompat;

    move-result-object v0

    .line 237
    return-object v0
.end method

.method public static iniciaHomeLogadaExibindoAtendimento(Landroid/content/Context;)Landroid/content/Intent;
    .registers 4
    .param p0, "context"    # Landroid/content/Context;

    .line 95
    new-instance v2, Landroid/content/Intent;

    const-class v0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;

    invoke-direct {v2, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 96
    .local v2, "intent":Landroid/content/Intent;
    const-string v0, "EXTRA_EXIBIR_ATENDIMENTO"

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 97
    const/high16 v0, 0x24000000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 99
    return-object v2
.end method

.method public static iniciaHomeLogadaExibindoExtrato(Landroid/content/Context;)Landroid/content/Intent;
    .registers 4
    .param p0, "context"    # Landroid/content/Context;

    .line 87
    new-instance v2, Landroid/content/Intent;

    const-class v0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_;

    invoke-direct {v2, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 88
    .local v2, "intent":Landroid/content/Intent;
    const-string v0, "EXTRA_EXIBIR_EXTRATO"

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 89
    const/high16 v0, 0x24000000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 91
    return-object v2
.end method

.method private iniciarActivityDeBusca()V
    .registers 4

    .line 313
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->buscaGeral:Lcom/itau/empresas/feature/busca/BuscaGeral;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0, p0, v1}, Lcom/itau/empresas/feature/busca/BuscaGeral;->getHomeSearchIntentComExtra(Landroid/content/Context;Lcom/itau/empresas/CustomApplication;)Landroid/content/Intent;

    move-result-object v2

    .line 314
    .local v2, "intent":Landroid/content/Intent;
    const/16 v0, 0x64

    invoke-virtual {p0, v2, v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 315
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 103
    new-instance v0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;
    .registers 2
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;

    .line 107
    new-instance v0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;-><init>(Landroid/support/v4/app/Fragment;)V

    return-object v0
.end method

.method private recarregaFragmentAtual()V
    .registers 2

    .line 342
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->navigationListener:Lcom/itau/empresas/feature/home/NavigationListener;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/home/NavigationListener;->recarregarFragmentAtual()V

    .line 343
    return-void
.end method

.method private trataExibicaoIntent(Landroid/content/Intent;)V
    .registers 5
    .param p1, "intent"    # Landroid/content/Intent;

    .line 274
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 275
    .local v2, "extras":Landroid/os/Bundle;
    if-nez v2, :cond_7

    .line 276
    return-void

    .line 278
    :cond_7
    const-string v0, "EXTRA_EXIBIR_EXTRATO"

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 279
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->navigationView:Landroid/support/design/widget/BottomNavigationView;

    const v1, 0x7f0e06c8

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomNavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 280
    return-void

    .line 283
    :cond_1d
    const-string v0, "EXTRA_EXIBIR_ATENDIMENTO"

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 284
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->navigationView:Landroid/support/design/widget/BottomNavigationView;

    const v1, 0x7f0e06cb

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomNavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 286
    :cond_32
    return-void
.end method

.method private verificaToggle()V
    .registers 4

    .line 205
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->application:Lcom/itau/empresas/CustomApplication;

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_TOGGLE_ACAOFIMANO:Lcom/itau/empresas/CustomApplication$Exibicao;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->verificarPermissao(Lcom/itau/empresas/CustomApplication$Exibicao;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 206
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "mobilepj.toggle.acaoFimAno"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 207
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_33

    .line 209
    :cond_1f
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "mobilepj.toggle.acaoFimAno"

    .line 210
    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 211
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 214
    :goto_33
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->application:Lcom/itau/empresas/CustomApplication;

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_TOGGLE_FINGERPRINT:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 215
    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->verificarPermissao(Lcom/itau/empresas/CustomApplication$Exibicao;)Z

    move-result v0

    if-eqz v0, :cond_52

    .line 216
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "mobilepj.toggle.fingerPrint"

    .line 217
    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 218
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_66

    .line 220
    :cond_52
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "mobilepj.toggle.fingerPrint"

    .line 221
    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 222
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 224
    :goto_66
    return-void
.end method


# virtual methods
.method AfterViews()V
    .registers 3

    .line 148
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 150
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->rlContainer:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/itau/empresas/feature/home/HomeLogadaActivity$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity$1;-><init>(Lcom/itau/empresas/feature/home/HomeLogadaActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    new-instance v0, Lcom/itau/empresas/feature/home/NavigationListener;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/home/NavigationListener;-><init>(Lcom/itau/empresas/feature/home/HomeLogadaActivity;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->navigationListener:Lcom/itau/empresas/feature/home/NavigationListener;

    .line 168
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->navigationView:Landroid/support/design/widget/BottomNavigationView;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->navigationListener:Lcom/itau/empresas/feature/home/NavigationListener;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomNavigationView;->setOnNavigationItemSelectedListener(Landroid/support/design/widget/BottomNavigationView$OnNavigationItemSelectedListener;)V

    .line 170
    new-instance v0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->navigationView:Landroid/support/design/widget/BottomNavigationView;

    invoke-direct {v0, v1}, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;-><init>(Landroid/support/design/widget/BottomNavigationView;)V

    invoke-virtual {v0}, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->configureBottomNavigation()V

    .line 172
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->verificaToggle()V

    .line 173
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->agendarAlarme()V

    .line 175
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->trataExibicaoIntent(Landroid/content/Intent;)V

    .line 177
    new-instance v0, Lcom/itau/empresas/feature/busca/BuscaGeral;

    invoke-direct {v0}, Lcom/itau/empresas/feature/busca/BuscaGeral;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->buscaGeral:Lcom/itau/empresas/feature/busca/BuscaGeral;

    .line 178
    return-void
.end method

.method public abreFragment(Landroid/support/v4/app/Fragment;Z)V
    .registers 8
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p2, "addToBackStack"    # Z

    .line 347
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 348
    const v1, 0x7f040018

    const v2, 0x7f040019

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 349
    .local v3, "transaction":Landroid/support/v4/app/FragmentTransaction;
    if-eqz p2, :cond_27

    .line 350
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    .line 351
    .local v4, "name":Ljava/lang/String;
    const v0, 0x7f0e01b9

    invoke-virtual {v3, v0, p1, v4}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 352
    .end local v4    # "name":Ljava/lang/String;
    goto :goto_2d

    .line 353
    :cond_27
    const v0, 0x7f0e01b9

    invoke-virtual {v3, v0, p1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 355
    :goto_2d
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 356
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 5
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .line 135
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/activity/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 137
    const/16 v0, 0xc8

    if-ne p1, v0, :cond_e

    const/16 v0, 0x63

    if-ne p2, v0, :cond_e

    .line 138
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->recarregaFragmentAtual()V

    .line 141
    :cond_e
    const/16 v0, 0x64

    if-ne p1, v0, :cond_1a

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1a

    .line 142
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->buscaGeral:Lcom/itau/empresas/feature/busca/BuscaGeral;

    invoke-virtual {v0, p3, p0}, Lcom/itau/empresas/feature/busca/BuscaGeral;->redirecionarParaActivityBuscada(Landroid/content/Intent;Landroid/support/v7/app/AppCompatActivity;)V

    .line 144
    :cond_1a
    return-void
.end method

.method public onBackPressed()V
    .registers 3

    .line 123
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-lez v0, :cond_e

    .line 124
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->onBackPressed()V

    .line 125
    return-void

    .line 128
    :cond_e
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->navigationListener:Lcom/itau/empresas/feature/home/NavigationListener;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/home/NavigationListener;->isHomeSelecionada()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 129
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    invoke-virtual {v0, p0}, Lcom/itau/empresas/feature/logout/LogoutHelper;->sairDoApp(Landroid/support/v7/app/AppCompatActivity;)V

    goto :goto_28

    .line 131
    :cond_1c
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->navigationView:Landroid/support/design/widget/BottomNavigationView;

    const v1, 0x7f0e06c7

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomNavigationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 132
    :goto_28
    return-void
.end method

.method public onBalanceView()V
    .registers 3

    .line 319
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ViewUtils;->temConta(Lcom/itau/empresas/CustomApplication;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 320
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    invoke-virtual {v0, p0}, Lcom/itau/empresas/feature/logout/LogoutHelper;->trataSessaoExpirada(Landroid/app/Activity;)V

    .line 321
    return-void

    .line 324
    :cond_e
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ViewUtils;->getContaCompleta(Lcom/itau/empresas/CustomApplication;)Ljava/lang/String;

    move-result-object v1

    .line 325
    .local v1, "idConta":Ljava/lang/String;
    new-instance v0, Lcom/itau/empresas/feature/home/HomeLogadaActivity$2;

    invoke-direct {v0, p0, v1}, Lcom/itau/empresas/feature/home/HomeLogadaActivity$2;-><init>(Lcom/itau/empresas/feature/home/HomeLogadaActivity;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 339
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .param p1, "menu"    # Landroid/view/Menu;

    .line 296
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->buscaGeral:Lcom/itau/empresas/feature/busca/BuscaGeral;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/busca/BuscaGeral;->podeExibirBuscaGeral(Lcom/itau/empresas/CustomApplication;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 297
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0003

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 300
    :cond_14
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoSair;)V
    .registers 4
    .param p1, "e"    # Lcom/itau/empresas/Evento$EventoSair;

    .line 266
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/itau/empresas/feature/logout/LogoutHelper;->efetuarLogout(ZLandroid/app/Activity;)V

    .line 267
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoSairPoynt;)V
    .registers 4
    .param p1, "e"    # Lcom/itau/empresas/Evento$EventoSairPoynt;

    .line 270
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/itau/empresas/feature/logout/LogoutHelper;->efetuarLogout(ZLandroid/app/Activity;)V

    .line 271
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "intent"    # Landroid/content/Intent;

    .line 290
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 291
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->trataExibicaoIntent(Landroid/content/Intent;)V

    .line 292
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 305
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0e06cd

    if-ne v0, v1, :cond_c

    .line 306
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->iniciarActivityDeBusca()V

    .line 309
    :cond_c
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .registers 1

    .line 112
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->onResume()V

    .line 113
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->carregarInformacoesContaLogada()V

    .line 114
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 118
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.class public Lbr/com/itau/sdk/android/core/model/TokenDadosDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final pCanal:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "pCanal"
    .end annotation
.end field

.field private final pCtf:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "pCtf"
    .end annotation
.end field

.field private final pIdMobile:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "pIdMobile"
    .end annotation
.end field

.field private final pIdMobileItau:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "pIdMobileItau"
    .end annotation
.end field

.field private final pImei:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "pImei"
    .end annotation
.end field

.field private final pPossuiSemente:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "pPossuiSemente"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/TokenDadosDTO;->pImei:Ljava/lang/String;

    .line 28
    iput-object p2, p0, Lbr/com/itau/sdk/android/core/model/TokenDadosDTO;->pIdMobileItau:Ljava/lang/String;

    .line 29
    iput-object p3, p0, Lbr/com/itau/sdk/android/core/model/TokenDadosDTO;->pCanal:Ljava/lang/String;

    .line 30
    iput-object p4, p0, Lbr/com/itau/sdk/android/core/model/TokenDadosDTO;->pIdMobile:Ljava/lang/String;

    .line 31
    iput-object p5, p0, Lbr/com/itau/sdk/android/core/model/TokenDadosDTO;->pCtf:Ljava/lang/String;

    .line 32
    iput-object p6, p0, Lbr/com/itau/sdk/android/core/model/TokenDadosDTO;->pPossuiSemente:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public getpCanal()Ljava/lang/String;
    .registers 2

    .line 45
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/TokenDadosDTO;->pCanal:Ljava/lang/String;

    return-object v0
.end method

.method public getpCtf()Ljava/lang/String;
    .registers 2

    .line 53
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/TokenDadosDTO;->pCtf:Ljava/lang/String;

    return-object v0
.end method

.method public getpIdMobile()Ljava/lang/String;
    .registers 2

    .line 49
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/TokenDadosDTO;->pIdMobile:Ljava/lang/String;

    return-object v0
.end method

.method public getpIdMobileItau()Ljava/lang/String;
    .registers 2

    .line 41
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/TokenDadosDTO;->pIdMobileItau:Ljava/lang/String;

    return-object v0
.end method

.method public getpImei()Ljava/lang/String;
    .registers 2

    .line 37
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/TokenDadosDTO;->pImei:Ljava/lang/String;

    return-object v0
.end method

.method public getpPossuiSemente()Ljava/lang/String;
    .registers 2

    .line 57
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/TokenDadosDTO;->pPossuiSemente:Ljava/lang/String;

    return-object v0
.end method

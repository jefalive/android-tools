.class Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog$1;
.super Ljava/lang/Object;
.source "PedagioWebViewDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->aoInicializar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;

.field final synthetic val$tracker:Lcom/itau/empresas/ui/util/analytics/TrackerEvento;


# direct methods
.method constructor <init>(Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;Lcom/itau/empresas/ui/util/analytics/TrackerEvento;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;

    .line 68
    iput-object p1, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog$1;->this$0:Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;

    iput-object p2, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog$1;->val$tracker:Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 12
    .param p1, "adapterView"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "l"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/widget/AdapterView<*>;Landroid/view/View;IJ)V"
        }
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog$1;->this$0:Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;

    iget-object v0, v0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->chaves:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p3, v0, :cond_3b

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog$1;->this$0:Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;

    iget-object v0, v0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->labels:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/lang/String;

    .line 73
    .local v5, "label":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog$1;->val$tracker:Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    iget-object v2, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog$1;->this$0:Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;

    iget-object v2, v2, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->categoriaAnalytics:Ljava/lang/String;

    iget-object v3, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog$1;->this$0:Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;

    .line 75
    const v4, 0x7f0702a0

    invoke-virtual {v3, v4}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v5}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 76
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog$1;->this$0:Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog$1;->this$0:Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;

    iget-object v1, v1, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->chaves:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    # invokes: Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->inicializarWebView(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v5}, Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;->access$000(Lcom/itau/empresas/ui/dialog/PedagioWebViewDialog;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    .end local v5    # "label":Ljava/lang/String;
    :cond_3b
    return-void
.end method

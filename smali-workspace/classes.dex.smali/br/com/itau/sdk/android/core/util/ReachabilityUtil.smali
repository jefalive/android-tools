.class public final Lbr/com/itau/sdk/android/core/util/ReachabilityUtil;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:[B

.field private static c:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x24

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/util/ReachabilityUtil;->b:[B

    const/16 v0, 0x36

    sput v0, Lbr/com/itau/sdk/android/core/util/ReachabilityUtil;->c:I

    return-void

    :array_e
    .array-data 1
        0xet
        -0x80t
        -0xet
        0x5et
        -0xct
        0x1t
        0x0t
        0x9t
        0x2t
        -0x11t
        0xbt
        -0xdt
        0xdt
        -0xbt
        -0x5t
        -0xct
        0x0t
        0x4t
        0x36t
        0xbt
        0x0t
        -0x48t
        0x0t
        0x0t
        0x49t
        -0x3bt
        -0xbt
        0x13t
        -0x14t
        0x47t
        -0x35t
        -0xct
        0x2t
        0x3ft
        -0x34t
        -0x10t
    .end array-data
.end method

.method private constructor <init>()V
    .registers 1

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method private static a(Landroid/content/Context;)Landroid/net/ConnectivityManager;
    .registers 4

    .line 57
    sget-object v0, Lbr/com/itau/sdk/android/core/util/ReachabilityUtil;->b:[B

    const/4 v1, 0x5

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/util/ReachabilityUtil;->b:[B

    const/4 v2, 0x6

    aget-byte v1, v1, v2

    invoke-static {v0, v0, v1}, Lbr/com/itau/sdk/android/core/util/ReachabilityUtil;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    return-object v0
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    sget-object v5, Lbr/com/itau/sdk/android/core/util/ReachabilityUtil;->b:[B

    mul-int/lit8 p2, p2, 0xa

    add-int/lit8 p2, p2, 0xc

    mul-int/lit8 p0, p0, 0xb

    rsub-int/lit8 p0, p0, 0xf

    const/4 v4, -0x1

    mul-int/lit8 p1, p1, 0x5

    rsub-int/lit8 p1, p1, 0x68

    new-instance v0, Ljava/lang/String;

    new-array v1, p2, [B

    add-int/lit8 p2, p2, -0x1

    if-nez v5, :cond_1e

    move v2, p0

    move v3, p2

    :goto_19
    neg-int v3, v3

    add-int/lit8 p0, p0, 0x1

    add-int p1, v2, v3

    :cond_1e
    add-int/lit8 v4, v4, 0x1

    int-to-byte v2, p1

    aput-byte v2, v1, v4

    if-ne v4, p2, :cond_2a

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_2a
    move v2, p1

    aget-byte v3, v5, p0

    goto :goto_19
.end method

.method private static a(Ljava/net/Socket;)V
    .registers 2

    .line 67
    if-eqz p0, :cond_7

    .line 69
    :try_start_2
    invoke-virtual {p0}, Ljava/net/Socket;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_5} :catch_6

    .line 71
    goto :goto_7

    .line 70
    :catch_6
    move-exception v0

    .line 73
    :cond_7
    :goto_7
    return-void
.end method

.method private static b(Landroid/content/Context;)Landroid/net/NetworkInfo;
    .registers 2

    .line 61
    if-nez p0, :cond_4

    .line 62
    const/4 v0, 0x0

    return-object v0

    .line 63
    :cond_4
    invoke-static {p0}, Lbr/com/itau/sdk/android/core/util/ReachabilityUtil;->a(Landroid/content/Context;)Landroid/net/ConnectivityManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    return-object v0
.end method

.method public static isNetworkingAvailable(Landroid/content/Context;)Z
    .registers 3

    .line 19
    invoke-static {p0}, Lbr/com/itau/sdk/android/core/util/ReachabilityUtil;->b(Landroid/content/Context;)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 20
    if-eqz v1, :cond_14

    .line 21
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 22
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    goto :goto_15

    :cond_14
    const/4 v0, 0x0

    .line 20
    :goto_15
    return v0
.end method

.method public static isReachable()Z
    .registers 3

    .line 26
    sget-object v0, Lbr/com/itau/sdk/android/core/util/ReachabilityUtil;->b:[B

    const/4 v1, 0x6

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/util/ReachabilityUtil;->b:[B

    const/4 v2, 0x5

    aget-byte v1, v1, v2

    invoke-static {v0, v0, v1}, Lbr/com/itau/sdk/android/core/util/ReachabilityUtil;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/util/ReachabilityUtil;->isReachable(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isReachable(Ljava/lang/String;)Z
    .registers 3

    .line 30
    invoke-static {p0}, Landroid/webkit/URLUtil;->isNetworkUrl(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 31
    const/4 v0, 0x0

    return v0

    .line 34
    :cond_8
    :try_start_8
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 35
    invoke-static {v1}, Lbr/com/itau/sdk/android/core/util/ReachabilityUtil;->isReachable(Ljava/net/URL;)Z
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_10} :catch_12

    move-result v0

    return v0

    .line 36
    :catch_12
    move-exception v1

    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public static isReachable(Ljava/net/URL;)Z
    .registers 9

    .line 42
    if-nez p0, :cond_4

    .line 43
    const/4 v0, 0x0

    return v0

    .line 45
    :cond_4
    const/4 v4, 0x0

    .line 47
    :try_start_5
    new-instance v0, Ljava/net/Socket;

    invoke-virtual {p0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljava/net/URL;->getPort()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_17

    invoke-virtual {p0}, Ljava/net/URL;->getPort()I

    move-result v2

    goto :goto_1b

    :cond_17
    invoke-virtual {p0}, Ljava/net/URL;->getDefaultPort()I

    move-result v2

    :goto_1b
    invoke-direct {v0, v1, v2}, Ljava/net/Socket;-><init>(Ljava/lang/String;I)V

    move-object v4, v0

    .line 48
    invoke-virtual {v4}, Ljava/net/Socket;->isConnected()Z
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_22} :catch_27
    .catchall {:try_start_5 .. :try_end_22} :catchall_2d

    move-result v5

    .line 52
    invoke-static {v4}, Lbr/com/itau/sdk/android/core/util/ReachabilityUtil;->a(Ljava/net/Socket;)V

    .line 48
    return v5

    .line 49
    :catch_27
    move-exception v5

    .line 50
    const/4 v6, 0x0

    .line 52
    invoke-static {v4}, Lbr/com/itau/sdk/android/core/util/ReachabilityUtil;->a(Ljava/net/Socket;)V

    .line 50
    return v6

    .line 52
    :catchall_2d
    move-exception v7

    invoke-static {v4}, Lbr/com/itau/sdk/android/core/util/ReachabilityUtil;->a(Ljava/net/Socket;)V

    throw v7
.end method

.class public Landroid/support/graphics/drawable/PathParser$PathDataNode;
.super Ljava/lang/Object;
.source "PathParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/graphics/drawable/PathParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PathDataNode"
.end annotation


# instance fields
.field params:[F

.field type:C


# direct methods
.method constructor <init>(C[F)V
    .registers 3
    .param p1, "type"    # C
    .param p2, "params"    # [F

    .line 300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 301
    iput-char p1, p0, Landroid/support/graphics/drawable/PathParser$PathDataNode;->type:C

    .line 302
    iput-object p2, p0, Landroid/support/graphics/drawable/PathParser$PathDataNode;->params:[F

    .line 303
    return-void
.end method

.method constructor <init>(Landroid/support/graphics/drawable/PathParser$PathDataNode;)V
    .registers 5
    .param p1, "n"    # Landroid/support/graphics/drawable/PathParser$PathDataNode;

    .line 305
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 306
    iget-char v0, p1, Landroid/support/graphics/drawable/PathParser$PathDataNode;->type:C

    iput-char v0, p0, Landroid/support/graphics/drawable/PathParser$PathDataNode;->type:C

    .line 307
    iget-object v0, p1, Landroid/support/graphics/drawable/PathParser$PathDataNode;->params:[F

    iget-object v1, p1, Landroid/support/graphics/drawable/PathParser$PathDataNode;->params:[F

    array-length v1, v1

    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Landroid/support/graphics/drawable/PathParser;->copyOfRange([FII)[F

    move-result-object v0

    iput-object v0, p0, Landroid/support/graphics/drawable/PathParser$PathDataNode;->params:[F

    .line 308
    return-void
.end method

.method private static addCommand(Landroid/graphics/Path;[FCC[F)V
    .registers 26
    .param p0, "path"    # Landroid/graphics/Path;
    .param p1, "current"    # [F
    .param p2, "previousCmd"    # C
    .param p3, "cmd"    # C
    .param p4, "val"    # [F

    .line 345
    const/4 v11, 0x2

    .line 346
    .local v11, "incr":I
    const/4 v0, 0x0

    aget v12, p1, v0

    .line 347
    .local v12, "currentX":F
    const/4 v0, 0x1

    aget v13, p1, v0

    .line 348
    .local v13, "currentY":F
    const/4 v0, 0x2

    aget v14, p1, v0

    .line 349
    .local v14, "ctrlPointX":F
    const/4 v0, 0x3

    aget v15, p1, v0

    .line 350
    .local v15, "ctrlPointY":F
    const/4 v0, 0x4

    aget v16, p1, v0

    .line 351
    .local v16, "currentSegmentStartX":F
    const/4 v0, 0x5

    aget v17, p1, v0

    .line 355
    .local v17, "currentSegmentStartY":F
    packed-switch p3, :pswitch_data_382

    goto/16 :goto_32

    .line 358
    :pswitch_18
    invoke-virtual/range {p0 .. p0}, Landroid/graphics/Path;->close()V

    .line 362
    move/from16 v12, v16

    .line 363
    move/from16 v13, v17

    .line 364
    move/from16 v14, v16

    .line 365
    move/from16 v15, v17

    .line 366
    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13}, Landroid/graphics/Path;->moveTo(FF)V

    .line 367
    goto :goto_32

    .line 374
    :pswitch_29
    const/4 v11, 0x2

    .line 375
    goto :goto_32

    .line 380
    :pswitch_2b
    const/4 v11, 0x1

    .line 381
    goto :goto_32

    .line 384
    :pswitch_2d
    const/4 v11, 0x6

    .line 385
    goto :goto_32

    .line 390
    :pswitch_2f
    const/4 v11, 0x4

    .line 391
    goto :goto_32

    .line 394
    :pswitch_31
    const/4 v11, 0x7

    .line 398
    :goto_32
    :pswitch_32
    const/16 v20, 0x0

    .local v20, "k":I
    :goto_34
    move-object/from16 v0, p4

    array-length v0, v0

    move/from16 v1, v20

    if-ge v1, v0, :cond_36f

    .line 399
    packed-switch p3, :pswitch_data_3fa

    goto/16 :goto_369

    .line 401
    :pswitch_40
    add-int/lit8 v0, v20, 0x0

    aget v0, p4, v0

    add-float/2addr v12, v0

    .line 402
    add-int/lit8 v0, v20, 0x1

    aget v0, p4, v0

    add-float/2addr v13, v0

    .line 403
    if-lez v20, :cond_5b

    .line 407
    add-int/lit8 v0, v20, 0x0

    aget v0, p4, v0

    add-int/lit8 v1, v20, 0x1

    aget v1, p4, v1

    move-object/from16 v2, p0

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    goto/16 :goto_369

    .line 409
    :cond_5b
    add-int/lit8 v0, v20, 0x0

    aget v0, p4, v0

    add-int/lit8 v1, v20, 0x1

    aget v1, p4, v1

    move-object/from16 v2, p0

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->rMoveTo(FF)V

    .line 410
    move/from16 v16, v12

    .line 411
    move/from16 v17, v13

    .line 413
    goto/16 :goto_369

    .line 415
    :pswitch_6e
    add-int/lit8 v0, v20, 0x0

    aget v12, p4, v0

    .line 416
    add-int/lit8 v0, v20, 0x1

    aget v13, p4, v0

    .line 417
    if-lez v20, :cond_87

    .line 421
    add-int/lit8 v0, v20, 0x0

    aget v0, p4, v0

    add-int/lit8 v1, v20, 0x1

    aget v1, p4, v1

    move-object/from16 v2, p0

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_369

    .line 423
    :cond_87
    add-int/lit8 v0, v20, 0x0

    aget v0, p4, v0

    add-int/lit8 v1, v20, 0x1

    aget v1, p4, v1

    move-object/from16 v2, p0

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 424
    move/from16 v16, v12

    .line 425
    move/from16 v17, v13

    .line 427
    goto/16 :goto_369

    .line 429
    :pswitch_9a
    add-int/lit8 v0, v20, 0x0

    aget v0, p4, v0

    add-int/lit8 v1, v20, 0x1

    aget v1, p4, v1

    move-object/from16 v2, p0

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 430
    add-int/lit8 v0, v20, 0x0

    aget v0, p4, v0

    add-float/2addr v12, v0

    .line 431
    add-int/lit8 v0, v20, 0x1

    aget v0, p4, v0

    add-float/2addr v13, v0

    .line 432
    goto/16 :goto_369

    .line 434
    :pswitch_b3
    add-int/lit8 v0, v20, 0x0

    aget v0, p4, v0

    add-int/lit8 v1, v20, 0x1

    aget v1, p4, v1

    move-object/from16 v2, p0

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 435
    add-int/lit8 v0, v20, 0x0

    aget v12, p4, v0

    .line 436
    add-int/lit8 v0, v20, 0x1

    aget v13, p4, v0

    .line 437
    goto/16 :goto_369

    .line 439
    :pswitch_ca
    add-int/lit8 v0, v20, 0x0

    aget v0, p4, v0

    move-object/from16 v1, p0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 440
    add-int/lit8 v0, v20, 0x0

    aget v0, p4, v0

    add-float/2addr v12, v0

    .line 441
    goto/16 :goto_369

    .line 443
    :pswitch_db
    add-int/lit8 v0, v20, 0x0

    aget v0, p4, v0

    move-object/from16 v1, p0

    invoke-virtual {v1, v0, v13}, Landroid/graphics/Path;->lineTo(FF)V

    .line 444
    add-int/lit8 v0, v20, 0x0

    aget v12, p4, v0

    .line 445
    goto/16 :goto_369

    .line 447
    :pswitch_ea
    add-int/lit8 v0, v20, 0x0

    aget v0, p4, v0

    move-object/from16 v1, p0

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/graphics/Path;->rLineTo(FF)V

    .line 448
    add-int/lit8 v0, v20, 0x0

    aget v0, p4, v0

    add-float/2addr v13, v0

    .line 449
    goto/16 :goto_369

    .line 451
    :pswitch_fb
    add-int/lit8 v0, v20, 0x0

    aget v0, p4, v0

    move-object/from16 v1, p0

    invoke-virtual {v1, v12, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 452
    add-int/lit8 v0, v20, 0x0

    aget v13, p4, v0

    .line 453
    goto/16 :goto_369

    .line 455
    :pswitch_10a
    move-object/from16 v0, p0

    add-int/lit8 v1, v20, 0x0

    aget v1, p4, v1

    add-int/lit8 v2, v20, 0x1

    aget v2, p4, v2

    add-int/lit8 v3, v20, 0x2

    aget v3, p4, v3

    add-int/lit8 v4, v20, 0x3

    aget v4, p4, v4

    add-int/lit8 v5, v20, 0x4

    aget v5, p4, v5

    add-int/lit8 v6, v20, 0x5

    aget v6, p4, v6

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 458
    add-int/lit8 v0, v20, 0x2

    aget v0, p4, v0

    add-float v14, v12, v0

    .line 459
    add-int/lit8 v0, v20, 0x3

    aget v0, p4, v0

    add-float v15, v13, v0

    .line 460
    add-int/lit8 v0, v20, 0x4

    aget v0, p4, v0

    add-float/2addr v12, v0

    .line 461
    add-int/lit8 v0, v20, 0x5

    aget v0, p4, v0

    add-float/2addr v13, v0

    .line 463
    goto/16 :goto_369

    .line 465
    :pswitch_13f
    move-object/from16 v0, p0

    add-int/lit8 v1, v20, 0x0

    aget v1, p4, v1

    add-int/lit8 v2, v20, 0x1

    aget v2, p4, v2

    add-int/lit8 v3, v20, 0x2

    aget v3, p4, v3

    add-int/lit8 v4, v20, 0x3

    aget v4, p4, v4

    add-int/lit8 v5, v20, 0x4

    aget v5, p4, v5

    add-int/lit8 v6, v20, 0x5

    aget v6, p4, v6

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 467
    add-int/lit8 v0, v20, 0x4

    aget v12, p4, v0

    .line 468
    add-int/lit8 v0, v20, 0x5

    aget v13, p4, v0

    .line 469
    add-int/lit8 v0, v20, 0x2

    aget v14, p4, v0

    .line 470
    add-int/lit8 v0, v20, 0x3

    aget v15, p4, v0

    .line 471
    goto/16 :goto_369

    .line 473
    :pswitch_16e
    const/16 v18, 0x0

    .line 474
    .local v18, "reflectiveCtrlPointX":F
    const/16 v19, 0x0

    .line 475
    .local v19, "reflectiveCtrlPointY":F
    move/from16 v0, p2

    const/16 v1, 0x63

    if-eq v0, v1, :cond_18a

    move/from16 v0, p2

    const/16 v1, 0x73

    if-eq v0, v1, :cond_18a

    move/from16 v0, p2

    const/16 v1, 0x43

    if-eq v0, v1, :cond_18a

    move/from16 v0, p2

    const/16 v1, 0x53

    if-ne v0, v1, :cond_18e

    .line 477
    :cond_18a
    sub-float v18, v12, v14

    .line 478
    sub-float v19, v13, v15

    .line 480
    :cond_18e
    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    add-int/lit8 v3, v20, 0x0

    aget v3, p4, v3

    add-int/lit8 v4, v20, 0x1

    aget v4, p4, v4

    add-int/lit8 v5, v20, 0x2

    aget v5, p4, v5

    add-int/lit8 v6, v20, 0x3

    aget v6, p4, v6

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 484
    add-int/lit8 v0, v20, 0x0

    aget v0, p4, v0

    add-float v14, v12, v0

    .line 485
    add-int/lit8 v0, v20, 0x1

    aget v0, p4, v0

    add-float v15, v13, v0

    .line 486
    add-int/lit8 v0, v20, 0x2

    aget v0, p4, v0

    add-float/2addr v12, v0

    .line 487
    add-int/lit8 v0, v20, 0x3

    aget v0, p4, v0

    add-float/2addr v13, v0

    .line 488
    goto/16 :goto_369

    .line 490
    .end local v18    # "reflectiveCtrlPointX":F
    .end local v19    # "reflectiveCtrlPointY":F
    :pswitch_1bf
    move/from16 v18, v12

    .line 491
    .local v18, "reflectiveCtrlPointX":F
    move/from16 v19, v13

    .line 492
    .local v19, "reflectiveCtrlPointY":F
    move/from16 v0, p2

    const/16 v1, 0x63

    if-eq v0, v1, :cond_1db

    move/from16 v0, p2

    const/16 v1, 0x73

    if-eq v0, v1, :cond_1db

    move/from16 v0, p2

    const/16 v1, 0x43

    if-eq v0, v1, :cond_1db

    move/from16 v0, p2

    const/16 v1, 0x53

    if-ne v0, v1, :cond_1e5

    .line 494
    :cond_1db
    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr v0, v12

    sub-float v18, v0, v14

    .line 495
    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr v0, v13

    sub-float v19, v0, v15

    .line 497
    :cond_1e5
    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    add-int/lit8 v3, v20, 0x0

    aget v3, p4, v3

    add-int/lit8 v4, v20, 0x1

    aget v4, p4, v4

    add-int/lit8 v5, v20, 0x2

    aget v5, p4, v5

    add-int/lit8 v6, v20, 0x3

    aget v6, p4, v6

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 499
    add-int/lit8 v0, v20, 0x0

    aget v14, p4, v0

    .line 500
    add-int/lit8 v0, v20, 0x1

    aget v15, p4, v0

    .line 501
    add-int/lit8 v0, v20, 0x2

    aget v12, p4, v0

    .line 502
    add-int/lit8 v0, v20, 0x3

    aget v13, p4, v0

    .line 503
    goto/16 :goto_369

    .line 505
    .end local v18    # "reflectiveCtrlPointX":F
    .end local v19    # "reflectiveCtrlPointY":F
    :pswitch_210
    add-int/lit8 v0, v20, 0x0

    aget v0, p4, v0

    add-int/lit8 v1, v20, 0x1

    aget v1, p4, v1

    add-int/lit8 v2, v20, 0x2

    aget v2, p4, v2

    add-int/lit8 v3, v20, 0x3

    aget v3, p4, v3

    move-object/from16 v4, p0

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/Path;->rQuadTo(FFFF)V

    .line 506
    add-int/lit8 v0, v20, 0x0

    aget v0, p4, v0

    add-float v14, v12, v0

    .line 507
    add-int/lit8 v0, v20, 0x1

    aget v0, p4, v0

    add-float v15, v13, v0

    .line 508
    add-int/lit8 v0, v20, 0x2

    aget v0, p4, v0

    add-float/2addr v12, v0

    .line 509
    add-int/lit8 v0, v20, 0x3

    aget v0, p4, v0

    add-float/2addr v13, v0

    .line 510
    goto/16 :goto_369

    .line 512
    :pswitch_23d
    add-int/lit8 v0, v20, 0x0

    aget v0, p4, v0

    add-int/lit8 v1, v20, 0x1

    aget v1, p4, v1

    add-int/lit8 v2, v20, 0x2

    aget v2, p4, v2

    add-int/lit8 v3, v20, 0x3

    aget v3, p4, v3

    move-object/from16 v4, p0

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 513
    add-int/lit8 v0, v20, 0x0

    aget v14, p4, v0

    .line 514
    add-int/lit8 v0, v20, 0x1

    aget v15, p4, v0

    .line 515
    add-int/lit8 v0, v20, 0x2

    aget v12, p4, v0

    .line 516
    add-int/lit8 v0, v20, 0x3

    aget v13, p4, v0

    .line 517
    goto/16 :goto_369

    .line 519
    :pswitch_264
    const/16 v18, 0x0

    .line 520
    .local v18, "reflectiveCtrlPointX":F
    const/16 v19, 0x0

    .line 521
    .local v19, "reflectiveCtrlPointY":F
    move/from16 v0, p2

    const/16 v1, 0x71

    if-eq v0, v1, :cond_280

    move/from16 v0, p2

    const/16 v1, 0x74

    if-eq v0, v1, :cond_280

    move/from16 v0, p2

    const/16 v1, 0x51

    if-eq v0, v1, :cond_280

    move/from16 v0, p2

    const/16 v1, 0x54

    if-ne v0, v1, :cond_284

    .line 523
    :cond_280
    sub-float v18, v12, v14

    .line 524
    sub-float v19, v13, v15

    .line 526
    :cond_284
    add-int/lit8 v0, v20, 0x0

    aget v0, p4, v0

    add-int/lit8 v1, v20, 0x1

    aget v1, p4, v1

    move-object/from16 v2, p0

    move/from16 v3, v18

    move/from16 v4, v19

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/graphics/Path;->rQuadTo(FFFF)V

    .line 528
    add-float v14, v12, v18

    .line 529
    add-float v15, v13, v19

    .line 530
    add-int/lit8 v0, v20, 0x0

    aget v0, p4, v0

    add-float/2addr v12, v0

    .line 531
    add-int/lit8 v0, v20, 0x1

    aget v0, p4, v0

    add-float/2addr v13, v0

    .line 532
    goto/16 :goto_369

    .line 534
    .end local v18    # "reflectiveCtrlPointX":F
    .end local v19    # "reflectiveCtrlPointY":F
    :pswitch_2a5
    move/from16 v18, v12

    .line 535
    .local v18, "reflectiveCtrlPointX":F
    move/from16 v19, v13

    .line 536
    .local v19, "reflectiveCtrlPointY":F
    move/from16 v0, p2

    const/16 v1, 0x71

    if-eq v0, v1, :cond_2c1

    move/from16 v0, p2

    const/16 v1, 0x74

    if-eq v0, v1, :cond_2c1

    move/from16 v0, p2

    const/16 v1, 0x51

    if-eq v0, v1, :cond_2c1

    move/from16 v0, p2

    const/16 v1, 0x54

    if-ne v0, v1, :cond_2cb

    .line 538
    :cond_2c1
    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr v0, v12

    sub-float v18, v0, v14

    .line 539
    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr v0, v13

    sub-float v19, v0, v15

    .line 541
    :cond_2cb
    add-int/lit8 v0, v20, 0x0

    aget v0, p4, v0

    add-int/lit8 v1, v20, 0x1

    aget v1, p4, v1

    move-object/from16 v2, p0

    move/from16 v3, v18

    move/from16 v4, v19

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 543
    move/from16 v14, v18

    .line 544
    move/from16 v15, v19

    .line 545
    add-int/lit8 v0, v20, 0x0

    aget v12, p4, v0

    .line 546
    add-int/lit8 v0, v20, 0x1

    aget v13, p4, v0

    .line 547
    goto/16 :goto_369

    .line 550
    .end local v18    # "reflectiveCtrlPointX":F
    .end local v19    # "reflectiveCtrlPointY":F
    :pswitch_2ea
    move-object/from16 v0, p0

    move v1, v12

    move v2, v13

    add-int/lit8 v3, v20, 0x5

    aget v3, p4, v3

    add-float/2addr v3, v12

    add-int/lit8 v4, v20, 0x6

    aget v4, p4, v4

    add-float/2addr v4, v13

    add-int/lit8 v5, v20, 0x0

    aget v5, p4, v5

    add-int/lit8 v6, v20, 0x1

    aget v6, p4, v6

    add-int/lit8 v7, v20, 0x2

    aget v7, p4, v7

    add-int/lit8 v8, v20, 0x3

    aget v8, p4, v8

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-eqz v8, :cond_30f

    const/4 v8, 0x1

    goto :goto_310

    :cond_30f
    const/4 v8, 0x0

    :goto_310
    add-int/lit8 v9, v20, 0x4

    aget v9, p4, v9

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-eqz v9, :cond_31b

    const/4 v9, 0x1

    goto :goto_31c

    :cond_31b
    const/4 v9, 0x0

    :goto_31c
    invoke-static/range {v0 .. v9}, Landroid/support/graphics/drawable/PathParser$PathDataNode;->drawArc(Landroid/graphics/Path;FFFFFFFZZ)V

    .line 560
    add-int/lit8 v0, v20, 0x5

    aget v0, p4, v0

    add-float/2addr v12, v0

    .line 561
    add-int/lit8 v0, v20, 0x6

    aget v0, p4, v0

    add-float/2addr v13, v0

    .line 562
    move v14, v12

    .line 563
    move v15, v13

    .line 564
    goto :goto_369

    .line 566
    :pswitch_32c
    move-object/from16 v0, p0

    move v1, v12

    move v2, v13

    add-int/lit8 v3, v20, 0x5

    aget v3, p4, v3

    add-int/lit8 v4, v20, 0x6

    aget v4, p4, v4

    add-int/lit8 v5, v20, 0x0

    aget v5, p4, v5

    add-int/lit8 v6, v20, 0x1

    aget v6, p4, v6

    add-int/lit8 v7, v20, 0x2

    aget v7, p4, v7

    add-int/lit8 v8, v20, 0x3

    aget v8, p4, v8

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-eqz v8, :cond_34f

    const/4 v8, 0x1

    goto :goto_350

    :cond_34f
    const/4 v8, 0x0

    :goto_350
    add-int/lit8 v9, v20, 0x4

    aget v9, p4, v9

    const/4 v10, 0x0

    cmpl-float v9, v9, v10

    if-eqz v9, :cond_35b

    const/4 v9, 0x1

    goto :goto_35c

    :cond_35b
    const/4 v9, 0x0

    :goto_35c
    invoke-static/range {v0 .. v9}, Landroid/support/graphics/drawable/PathParser$PathDataNode;->drawArc(Landroid/graphics/Path;FFFFFFFZZ)V

    .line 576
    add-int/lit8 v0, v20, 0x5

    aget v12, p4, v0

    .line 577
    add-int/lit8 v0, v20, 0x6

    aget v13, p4, v0

    .line 578
    move v14, v12

    .line 579
    move v15, v13

    .line 582
    :goto_369
    :pswitch_369
    move/from16 p2, p3

    .line 398
    add-int v20, v20, v11

    goto/16 :goto_34

    .line 584
    .end local v20    # "k":I
    :cond_36f
    const/4 v0, 0x0

    aput v12, p1, v0

    .line 585
    const/4 v0, 0x1

    aput v13, p1, v0

    .line 586
    const/4 v0, 0x2

    aput v14, p1, v0

    .line 587
    const/4 v0, 0x3

    aput v15, p1, v0

    .line 588
    const/4 v0, 0x4

    aput v16, p1, v0

    .line 589
    const/4 v0, 0x5

    aput v17, p1, v0

    .line 590
    return-void

    :pswitch_data_382
    .packed-switch 0x41
        :pswitch_31
        :pswitch_32
        :pswitch_2d
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_2b
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_29
        :pswitch_29
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_2f
        :pswitch_32
        :pswitch_2f
        :pswitch_29
        :pswitch_32
        :pswitch_2b
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_18
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_31
        :pswitch_32
        :pswitch_2d
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_2b
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_29
        :pswitch_29
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_2f
        :pswitch_32
        :pswitch_2f
        :pswitch_29
        :pswitch_32
        :pswitch_2b
        :pswitch_32
        :pswitch_32
        :pswitch_32
        :pswitch_18
    .end packed-switch

    :pswitch_data_3fa
    .packed-switch 0x41
        :pswitch_32c
        :pswitch_369
        :pswitch_13f
        :pswitch_369
        :pswitch_369
        :pswitch_369
        :pswitch_369
        :pswitch_db
        :pswitch_369
        :pswitch_369
        :pswitch_369
        :pswitch_b3
        :pswitch_6e
        :pswitch_369
        :pswitch_369
        :pswitch_369
        :pswitch_23d
        :pswitch_369
        :pswitch_1bf
        :pswitch_2a5
        :pswitch_369
        :pswitch_fb
        :pswitch_369
        :pswitch_369
        :pswitch_369
        :pswitch_369
        :pswitch_369
        :pswitch_369
        :pswitch_369
        :pswitch_369
        :pswitch_369
        :pswitch_369
        :pswitch_2ea
        :pswitch_369
        :pswitch_10a
        :pswitch_369
        :pswitch_369
        :pswitch_369
        :pswitch_369
        :pswitch_ca
        :pswitch_369
        :pswitch_369
        :pswitch_369
        :pswitch_9a
        :pswitch_40
        :pswitch_369
        :pswitch_369
        :pswitch_369
        :pswitch_210
        :pswitch_369
        :pswitch_16e
        :pswitch_264
        :pswitch_369
        :pswitch_ea
    .end packed-switch
.end method

.method private static arcToBezier(Landroid/graphics/Path;DDDDDDDDD)V
    .registers 76
    .param p0, "p"    # Landroid/graphics/Path;
    .param p1, "cx"    # D
    .param p3, "cy"    # D
    .param p5, "a"    # D
    .param p7, "b"    # D
    .param p9, "e1x"    # D
    .param p11, "e1y"    # D
    .param p13, "theta"    # D
    .param p15, "start"    # D
    .param p17, "sweep"    # D

    .line 697
    const-wide/high16 v0, 0x4010000000000000L    # 4.0

    mul-double v0, v0, p17

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v7, v0

    .line 699
    .local v7, "numSegments":I
    move-wide/from16 v8, p15

    .line 700
    .local v8, "eta1":D
    invoke-static/range {p13 .. p14}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    .line 701
    .local v10, "cosTheta":D
    invoke-static/range {p13 .. p14}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    .line 702
    .local v12, "sinTheta":D
    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    .line 703
    .local v14, "cosEta1":D
    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    .line 704
    .local v16, "sinEta1":D
    move-wide/from16 v0, p5

    neg-double v0, v0

    mul-double/2addr v0, v10

    mul-double v0, v0, v16

    mul-double v2, p7, v12

    mul-double/2addr v2, v14

    sub-double v18, v0, v2

    .line 705
    .local v18, "ep1x":D
    move-wide/from16 v0, p5

    neg-double v0, v0

    mul-double/2addr v0, v12

    mul-double v0, v0, v16

    mul-double v2, p7, v10

    mul-double/2addr v2, v14

    add-double v20, v0, v2

    .line 707
    .local v20, "ep1y":D
    int-to-double v0, v7

    div-double v22, p17, v0

    .line 708
    .local v22, "anglePerSegment":D
    const/16 v24, 0x0

    .local v24, "i":I
    :goto_40
    move/from16 v0, v24

    if-ge v0, v7, :cond_101

    .line 709
    add-double v25, v8, v22

    .line 710
    .local v25, "eta2":D
    invoke-static/range {v25 .. v26}, Ljava/lang/Math;->sin(D)D

    move-result-wide v27

    .line 711
    .local v27, "sinEta2":D
    invoke-static/range {v25 .. v26}, Ljava/lang/Math;->cos(D)D

    move-result-wide v29

    .line 712
    .local v29, "cosEta2":D
    mul-double v0, p5, v10

    mul-double v0, v0, v29

    add-double v0, v0, p1

    mul-double v2, p7, v12

    mul-double v2, v2, v27

    sub-double v31, v0, v2

    .line 713
    .local v31, "e2x":D
    mul-double v0, p5, v12

    mul-double v0, v0, v29

    add-double v0, v0, p3

    mul-double v2, p7, v10

    mul-double v2, v2, v27

    add-double v33, v0, v2

    .line 714
    .local v33, "e2y":D
    move-wide/from16 v0, p5

    neg-double v0, v0

    mul-double/2addr v0, v10

    mul-double v0, v0, v27

    mul-double v2, p7, v12

    mul-double v2, v2, v29

    sub-double v35, v0, v2

    .line 715
    .local v35, "ep2x":D
    move-wide/from16 v0, p5

    neg-double v0, v0

    mul-double/2addr v0, v12

    mul-double v0, v0, v27

    mul-double v2, p7, v10

    mul-double v2, v2, v29

    add-double v37, v0, v2

    .line 716
    .local v37, "ep2y":D
    sub-double v0, v25, v8

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v39

    .line 717
    .local v39, "tanDiff2":D
    sub-double v0, v25, v8

    .line 718
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    mul-double v2, v2, v39

    mul-double v2, v2, v39

    const-wide/high16 v4, 0x4010000000000000L    # 4.0

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    div-double v41, v0, v2

    .line 719
    .local v41, "alpha":D
    mul-double v0, v41, v18

    add-double v43, p9, v0

    .line 720
    .local v43, "q1x":D
    mul-double v0, v41, v20

    add-double v45, p11, v0

    .line 721
    .local v45, "q1y":D
    mul-double v0, v41, v35

    sub-double v47, v31, v0

    .line 722
    .local v47, "q2x":D
    mul-double v0, v41, v37

    sub-double v49, v33, v0

    .line 726
    .local v49, "q2y":D
    move-wide/from16 v0, v43

    double-to-float v0, v0

    move-wide/from16 v1, p9

    double-to-float v1, v1

    sub-float v51, v0, v1

    .line 727
    .local v51, "delta_q1x":F
    move-wide/from16 v0, v45

    double-to-float v0, v0

    move-wide/from16 v1, p11

    double-to-float v1, v1

    sub-float v52, v0, v1

    .line 728
    .local v52, "delta_q1y":F
    move-wide/from16 v0, v47

    double-to-float v0, v0

    move-wide/from16 v1, p9

    double-to-float v1, v1

    sub-float v53, v0, v1

    .line 729
    .local v53, "delta_q2x":F
    move-wide/from16 v0, v49

    double-to-float v0, v0

    move-wide/from16 v1, p11

    double-to-float v1, v1

    sub-float v54, v0, v1

    .line 730
    .local v54, "delta_q2y":F
    move-wide/from16 v0, v31

    double-to-float v0, v0

    move-wide/from16 v1, p9

    double-to-float v1, v1

    sub-float v55, v0, v1

    .line 731
    .local v55, "delta_e2x":F
    move-wide/from16 v0, v33

    double-to-float v0, v0

    move-wide/from16 v1, p11

    double-to-float v1, v1

    sub-float v56, v0, v1

    .line 733
    .local v56, "delta_e2y":F
    move-object/from16 v0, p0

    move/from16 v1, v51

    move/from16 v2, v52

    move/from16 v3, v53

    move/from16 v4, v54

    move/from16 v5, v55

    move/from16 v6, v56

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->rCubicTo(FFFFFF)V

    .line 735
    move-wide/from16 v8, v25

    .line 736
    move-wide/from16 p9, v31

    .line 737
    move-wide/from16 p11, v33

    .line 738
    move-wide/from16 v18, v35

    .line 739
    move-wide/from16 v20, v37

    .line 708
    .end local v25    # "eta2":D
    .end local v27    # "sinEta2":D
    .end local v29    # "cosEta2":D
    .end local v31    # "e2x":D
    .end local v33    # "e2y":D
    .end local v35    # "ep2x":D
    .end local v37    # "ep2y":D
    .end local v39    # "tanDiff2":D
    .end local v41    # "alpha":D
    .end local v43    # "q1x":D
    .end local v45    # "q1y":D
    .end local v47    # "q2x":D
    .end local v49    # "q2y":D
    .end local v51    # "delta_q1x":F
    .end local v52    # "delta_q1y":F
    .end local v53    # "delta_q2x":F
    .end local v54    # "delta_q2y":F
    .end local v55    # "delta_e2x":F
    .end local v56    # "delta_e2y":F
    add-int/lit8 v24, v24, 0x1

    goto/16 :goto_40

    .line 741
    .end local v24    # "i":I
    :cond_101
    return-void
.end method

.method private static drawArc(Landroid/graphics/Path;FFFFFFFZZ)V
    .registers 73
    .param p0, "p"    # Landroid/graphics/Path;
    .param p1, "x0"    # F
    .param p2, "y0"    # F
    .param p3, "x1"    # F
    .param p4, "y1"    # F
    .param p5, "a"    # F
    .param p6, "b"    # F
    .param p7, "theta"    # F
    .param p8, "isMoreThanHalf"    # Z
    .param p9, "isPositiveArc"    # Z

    .line 604
    move/from16 v0, p7

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v19

    .line 606
    .local v19, "thetaD":D
    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->cos(D)D

    move-result-wide v21

    .line 607
    .local v21, "cosTheta":D
    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->sin(D)D

    move-result-wide v23

    .line 610
    .local v23, "sinTheta":D
    move/from16 v0, p1

    float-to-double v0, v0

    mul-double v0, v0, v21

    move/from16 v2, p2

    float-to-double v2, v2

    mul-double v2, v2, v23

    add-double/2addr v0, v2

    move/from16 v2, p5

    float-to-double v2, v2

    div-double v25, v0, v2

    .line 611
    .local v25, "x0p":D
    move/from16 v0, p1

    neg-float v0, v0

    float-to-double v0, v0

    mul-double v0, v0, v23

    move/from16 v2, p2

    float-to-double v2, v2

    mul-double v2, v2, v21

    add-double/2addr v0, v2

    move/from16 v2, p6

    float-to-double v2, v2

    div-double v27, v0, v2

    .line 612
    .local v27, "y0p":D
    move/from16 v0, p3

    float-to-double v0, v0

    mul-double v0, v0, v21

    move/from16 v2, p4

    float-to-double v2, v2

    mul-double v2, v2, v23

    add-double/2addr v0, v2

    move/from16 v2, p5

    float-to-double v2, v2

    div-double v29, v0, v2

    .line 613
    .local v29, "x1p":D
    move/from16 v0, p3

    neg-float v0, v0

    float-to-double v0, v0

    mul-double v0, v0, v23

    move/from16 v2, p4

    float-to-double v2, v2

    mul-double v2, v2, v21

    add-double/2addr v0, v2

    move/from16 v2, p6

    float-to-double v2, v2

    div-double v31, v0, v2

    .line 616
    .local v31, "y1p":D
    sub-double v33, v25, v29

    .line 617
    .local v33, "dx":D
    sub-double v35, v27, v31

    .line 618
    .local v35, "dy":D
    add-double v0, v25, v29

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    div-double v37, v0, v2

    .line 619
    .local v37, "xm":D
    add-double v0, v27, v31

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    div-double v39, v0, v2

    .line 621
    .local v39, "ym":D
    mul-double v0, v33, v33

    mul-double v2, v35, v35

    add-double v41, v0, v2

    .line 622
    .local v41, "dsq":D
    const-wide/16 v0, 0x0

    cmpl-double v0, v41, v0

    if-nez v0, :cond_75

    .line 623
    const-string v0, "PathParser"

    const-string v1, " Points are coincident"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 624
    return-void

    .line 626
    :cond_75
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    div-double v0, v0, v41

    const-wide/high16 v2, 0x3fd0000000000000L    # 0.25

    sub-double v43, v0, v2

    .line 627
    .local v43, "disc":D
    const-wide/16 v0, 0x0

    cmpg-double v0, v43, v0

    if-gez v0, :cond_c2

    .line 628
    const-string v0, "PathParser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Points are too far apart "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-wide/from16 v2, v41

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    invoke-static/range {v41 .. v42}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    const-wide v2, 0x3ffffff583a53b8eL    # 1.99999

    div-double/2addr v0, v2

    double-to-float v2, v0

    move/from16 v45, v2

    .line 630
    .local v45, "adjust":F
    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    mul-float v5, p5, v45

    mul-float v6, p6, v45

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    invoke-static/range {v0 .. v9}, Landroid/support/graphics/drawable/PathParser$PathDataNode;->drawArc(Landroid/graphics/Path;FFFFFFFZZ)V

    .line 632
    return-void

    .line 634
    .end local v45    # "adjust":F
    :cond_c2
    invoke-static/range {v43 .. v44}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v45

    .line 635
    .local v45, "s":D
    mul-double v47, v45, v33

    .line 636
    .local v47, "sdx":D
    mul-double v49, v45, v35

    .line 639
    .local v49, "sdy":D
    move/from16 v0, p8

    move/from16 v1, p9

    if-ne v0, v1, :cond_d5

    .line 640
    sub-double v51, v37, v49

    .line 641
    .local v51, "cx":D
    add-double v53, v39, v47

    .local v53, "cy":D
    goto :goto_d9

    .line 643
    .end local v51    # "cx":D
    .end local v53    # "cy":D
    :cond_d5
    add-double v51, v37, v49

    .line 644
    .local v51, "cx":D
    sub-double v53, v39, v47

    .line 647
    .local v53, "cy":D
    :goto_d9
    sub-double v0, v27, v53

    sub-double v2, v25, v51

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v55

    .line 649
    .local v55, "eta0":D
    sub-double v0, v31, v53

    sub-double v2, v29, v51

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v57

    .line 651
    .local v57, "eta1":D
    sub-double v59, v57, v55

    .line 652
    .local v59, "sweep":D
    const-wide/16 v0, 0x0

    cmpl-double v0, v59, v0

    if-ltz v0, :cond_f3

    const/4 v0, 0x1

    goto :goto_f4

    :cond_f3
    const/4 v0, 0x0

    :goto_f4
    move/from16 v1, p9

    if-eq v1, v0, :cond_10d

    .line 653
    const-wide/16 v0, 0x0

    cmpl-double v0, v59, v0

    if-lez v0, :cond_106

    .line 654
    const-wide v0, 0x401921fb54442d18L    # 6.283185307179586

    sub-double v59, v59, v0

    goto :goto_10d

    .line 656
    :cond_106
    const-wide v0, 0x401921fb54442d18L    # 6.283185307179586

    add-double v59, v59, v0

    .line 660
    :cond_10d
    :goto_10d
    move/from16 v0, p5

    float-to-double v0, v0

    mul-double v51, v51, v0

    .line 661
    move/from16 v0, p6

    float-to-double v0, v0

    mul-double v53, v53, v0

    .line 662
    move-wide/from16 v61, v51

    .line 663
    .local v61, "tcx":D
    mul-double v0, v51, v21

    mul-double v2, v53, v23

    sub-double v51, v0, v2

    .line 664
    mul-double v0, v61, v23

    mul-double v2, v53, v21

    add-double v53, v0, v2

    .line 666
    move-object/from16 v0, p0

    move-wide/from16 v1, v51

    move-wide/from16 v3, v53

    move/from16 v5, p5

    float-to-double v5, v5

    move/from16 v7, p6

    float-to-double v7, v7

    move/from16 v9, p1

    float-to-double v9, v9

    move/from16 v11, p2

    float-to-double v11, v11

    move-wide/from16 v13, v19

    move-wide/from16 v15, v55

    move-wide/from16 v17, v59

    invoke-static/range {v0 .. v18}, Landroid/support/graphics/drawable/PathParser$PathDataNode;->arcToBezier(Landroid/graphics/Path;DDDDDDDDD)V

    .line 667
    return-void
.end method

.method public static nodesToPath([Landroid/support/graphics/drawable/PathParser$PathDataNode;Landroid/graphics/Path;)V
    .registers 7
    .param p0, "node"    # [Landroid/support/graphics/drawable/PathParser$PathDataNode;
    .param p1, "path"    # Landroid/graphics/Path;

    .line 317
    const/4 v0, 0x6

    new-array v2, v0, [F

    .line 318
    .local v2, "current":[F
    const/16 v3, 0x6d

    .line 319
    .local v3, "previousCommand":C
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_6
    array-length v0, p0

    if-ge v4, v0, :cond_1b

    .line 320
    aget-object v0, p0, v4

    iget-char v0, v0, Landroid/support/graphics/drawable/PathParser$PathDataNode;->type:C

    aget-object v1, p0, v4

    iget-object v1, v1, Landroid/support/graphics/drawable/PathParser$PathDataNode;->params:[F

    invoke-static {p1, v2, v3, v0, v1}, Landroid/support/graphics/drawable/PathParser$PathDataNode;->addCommand(Landroid/graphics/Path;[FCC[F)V

    .line 321
    aget-object v0, p0, v4

    iget-char v3, v0, Landroid/support/graphics/drawable/PathParser$PathDataNode;->type:C

    .line 319
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 323
    .end local v4    # "i":I
    :cond_1b
    return-void
.end method

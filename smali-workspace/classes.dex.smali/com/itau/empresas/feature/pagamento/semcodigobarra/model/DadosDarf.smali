.class public Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;
.super Ljava/lang/Object;
.source "DadosDarf.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private dataPagamento:Ljava/lang/String;

.field private valorJuros:Ljava/lang/String;

.field private valorMulta:Ljava/lang/String;

.field private valorPrincipal:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getValorAPagar()Ljava/lang/String;
    .registers 2

    .line 50
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;->somaValoresDarf()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setDataPagamento(Ljava/lang/String;)V
    .registers 2
    .param p1, "dataPagamento"    # Ljava/lang/String;

    .line 66
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;->dataPagamento:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public setValorJuros(Ljava/lang/String;)V
    .registers 2
    .param p1, "valorJuros"    # Ljava/lang/String;

    .line 38
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;->valorJuros:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public setValorMulta(Ljava/lang/String;)V
    .registers 2
    .param p1, "valorMulta"    # Ljava/lang/String;

    .line 46
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;->valorMulta:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public setValorPrincipal(Ljava/lang/String;)V
    .registers 2
    .param p1, "valorPrincipal"    # Ljava/lang/String;

    .line 30
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;->valorPrincipal:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public somaValoresDarf()Ljava/math/BigDecimal;
    .registers 7

    .line 70
    new-instance v2, Ljava/math/BigDecimal;

    const-string v0, "0.00"

    invoke-direct {v2, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 71
    .local v2, "valorPrincipalFormatado":Ljava/math/BigDecimal;
    new-instance v3, Ljava/math/BigDecimal;

    const-string v0, "0.00"

    invoke-direct {v3, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 72
    .local v3, "valorJurosFormatado":Ljava/math/BigDecimal;
    new-instance v4, Ljava/math/BigDecimal;

    const-string v0, "0.00"

    invoke-direct {v4, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 74
    .local v4, "valorMultaFormatado":Ljava/math/BigDecimal;
    :try_start_15
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;->valorPrincipal:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2d

    .line 75
    new-instance v2, Ljava/math/BigDecimal;

    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;->valorPrincipal:Ljava/lang/String;

    .line 76
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorMonetarioParaDecimal(Ljava/lang/String;Z)Ljava/math/BigDecimal;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 79
    :cond_2d
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;->valorJuros:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_53

    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;->valorJuros:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, "000"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_53

    .line 80
    new-instance v3, Ljava/math/BigDecimal;

    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;->valorJuros:Ljava/lang/String;

    .line 81
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorMonetarioParaDecimal(Ljava/lang/String;Z)Ljava/math/BigDecimal;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 84
    :cond_53
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;->valorMulta:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6b

    .line 85
    new-instance v4, Ljava/math/BigDecimal;

    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;->valorMulta:Ljava/lang/String;

    .line 86
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorMonetarioParaDecimal(Ljava/lang/String;Z)Ljava/math/BigDecimal;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V
    :try_end_6b
    .catch Ljava/text/ParseException; {:try_start_15 .. :try_end_6b} :catch_6c

    .line 91
    :cond_6b
    goto :goto_75

    .line 89
    :catch_6c
    move-exception v5

    .line 90
    .local v5, "px":Ljava/text/ParseException;
    new-instance v0, Ljava/math/BigDecimal;

    const-string v1, "0.00"

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    return-object v0

    .line 92
    .end local v5    # "px":Ljava/text/ParseException;
    :goto_75
    invoke-virtual {v3, v4}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

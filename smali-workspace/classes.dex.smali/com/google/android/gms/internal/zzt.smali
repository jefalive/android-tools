.class public Lcom/google/android/gms/internal/zzt;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/zzf;


# static fields
.field protected static final DEBUG:Z

.field private static zzao:I

.field private static zzap:I


# instance fields
.field protected final zzaq:Lcom/google/android/gms/internal/zzy;

.field protected final zzar:Lcom/google/android/gms/internal/zzu;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    sget-boolean v0, Lcom/google/android/gms/internal/zzs;->DEBUG:Z

    sput-boolean v0, Lcom/google/android/gms/internal/zzt;->DEBUG:Z

    const/16 v0, 0xbb8

    sput v0, Lcom/google/android/gms/internal/zzt;->zzao:I

    const/16 v0, 0x1000

    sput v0, Lcom/google/android/gms/internal/zzt;->zzap:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/zzy;)V
    .registers 4

    new-instance v0, Lcom/google/android/gms/internal/zzu;

    sget v1, Lcom/google/android/gms/internal/zzt;->zzap:I

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/zzu;-><init>(I)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/internal/zzt;-><init>(Lcom/google/android/gms/internal/zzy;Lcom/google/android/gms/internal/zzu;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/zzy;Lcom/google/android/gms/internal/zzu;)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/zzt;->zzaq:Lcom/google/android/gms/internal/zzy;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzt;->zzar:Lcom/google/android/gms/internal/zzu;

    return-void
.end method

.method protected static zza([Lorg/apache/http/Header;)Ljava/util/Map;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([Lorg/apache/http/Header;)Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
        }
    .end annotation

    new-instance v2, Ljava/util/TreeMap;

    sget-object v0, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-direct {v2, v0}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    const/4 v3, 0x0

    :goto_8
    array-length v0, p0

    if-ge v3, v0, :cond_1d

    aget-object v0, p0, v3

    invoke-interface {v0}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v0

    aget-object v1, p0, v3

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_1d
    return-object v2
.end method

.method private zza(JLcom/google/android/gms/internal/zzk;[BLorg/apache/http/StatusLine;)V
    .registers 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JLcom/google/android/gms/internal/zzk<*>;[BLorg/apache/http/StatusLine;)V"
        }
    .end annotation

    sget-boolean v0, Lcom/google/android/gms/internal/zzt;->DEBUG:Z

    if-nez v0, :cond_b

    sget v0, Lcom/google/android/gms/internal/zzt;->zzao:I

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-lez v0, :cond_44

    :cond_b
    const-string v0, "HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    if-eqz p4, :cond_22

    array-length v2, p4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_24

    :cond_22
    const-string v2, "null"

    :goto_24
    const/4 v3, 0x2

    aput-object v2, v1, v3

    invoke-interface {p5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    invoke-virtual {p3}, Lcom/google/android/gms/internal/zzk;->zzu()Lcom/google/android/gms/internal/zzo;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/internal/zzo;->zze()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x4

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzs;->zzb(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_44
    return-void
.end method

.method private static zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzk;Lcom/google/android/gms/internal/zzr;)V
    .registers 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;Lcom/google/android/gms/internal/zzk<*>;Lcom/google/android/gms/internal/zzr;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/zzr;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzk;->zzu()Lcom/google/android/gms/internal/zzo;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzk;->zzt()I

    move-result v5

    :try_start_8
    invoke-interface {v4, p2}, Lcom/google/android/gms/internal/zzo;->zza(Lcom/google/android/gms/internal/zzr;)V
    :try_end_b
    .catch Lcom/google/android/gms/internal/zzr; {:try_start_8 .. :try_end_b} :catch_c

    goto :goto_24

    :catch_c
    move-exception v6

    const-string v0, "%s-timeout-giveup [timeout=%s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/zzk;->zzc(Ljava/lang/String;)V

    throw v6

    :goto_24
    const-string v0, "%s-retry [timeout=%s]"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/zzk;->zzc(Ljava/lang/String;)V

    return-void
.end method

.method private zza(Ljava/util/Map;Lcom/google/android/gms/internal/zzb$zza;)V
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;Lcom/google/android/gms/internal/zzb$zza;)V"
        }
    .end annotation

    if-nez p2, :cond_3

    return-void

    :cond_3
    iget-object v0, p2, Lcom/google/android/gms/internal/zzb$zza;->zzb:Ljava/lang/String;

    if-eqz v0, :cond_e

    const-string v0, "If-None-Match"

    iget-object v1, p2, Lcom/google/android/gms/internal/zzb$zza;->zzb:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_e
    iget-wide v0, p2, Lcom/google/android/gms/internal/zzb$zza;->zzd:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_26

    new-instance v4, Ljava/util/Date;

    iget-wide v0, p2, Lcom/google/android/gms/internal/zzb$zza;->zzd:J

    invoke-direct {v4, v0, v1}, Ljava/util/Date;-><init>(J)V

    const-string v0, "If-Modified-Since"

    invoke-static {v4}, Lorg/apache/http/impl/cookie/DateUtils;->formatDate(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_26
    return-void
.end method

.method private zza(Lorg/apache/http/HttpEntity;)[B
    .registers 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/gms/internal/zzp;
        }
    .end annotation

    new-instance v3, Lcom/google/android/gms/internal/zzaa;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzt;->zzar:Lcom/google/android/gms/internal/zzu;

    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v1

    long-to-int v1, v1

    invoke-direct {v3, v0, v1}, Lcom/google/android/gms/internal/zzaa;-><init>(Lcom/google/android/gms/internal/zzu;I)V

    const/4 v4, 0x0

    :try_start_d
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v5

    if-nez v5, :cond_19

    new-instance v0, Lcom/google/android/gms/internal/zzp;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzp;-><init>()V

    throw v0

    :cond_19
    iget-object v0, p0, Lcom/google/android/gms/internal/zzt;->zzar:Lcom/google/android/gms/internal/zzu;

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzu;->zzb(I)[B

    move-result-object v0

    move-object v4, v0

    :goto_22
    invoke-virtual {v5, v4}, Ljava/io/InputStream;->read([B)I

    move-result v0

    move v6, v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2f

    const/4 v0, 0x0

    invoke-virtual {v3, v4, v0, v6}, Lcom/google/android/gms/internal/zzaa;->write([BII)V

    goto :goto_22

    :cond_2f
    invoke-virtual {v3}, Lcom/google/android/gms/internal/zzaa;->toByteArray()[B
    :try_end_32
    .catchall {:try_start_d .. :try_end_32} :catchall_49

    move-result-object v7

    :try_start_33
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_36
    .catch Ljava/io/IOException; {:try_start_33 .. :try_end_36} :catch_37

    goto :goto_40

    :catch_37
    move-exception v8

    const-string v0, "Error occured when calling consumingContent"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzs;->zza(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_40
    iget-object v0, p0, Lcom/google/android/gms/internal/zzt;->zzar:Lcom/google/android/gms/internal/zzu;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/internal/zzu;->zza([B)V

    invoke-virtual {v3}, Lcom/google/android/gms/internal/zzaa;->close()V

    return-object v7

    :catchall_49
    move-exception v9

    :try_start_4a
    invoke-interface {p1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_4d
    .catch Ljava/io/IOException; {:try_start_4a .. :try_end_4d} :catch_4e

    goto :goto_57

    :catch_4e
    move-exception v10

    const-string v0, "Error occured when calling consumingContent"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzs;->zza(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_57
    iget-object v0, p0, Lcom/google/android/gms/internal/zzt;->zzar:Lcom/google/android/gms/internal/zzu;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/internal/zzu;->zza([B)V

    invoke-virtual {v3}, Lcom/google/android/gms/internal/zzaa;->close()V

    throw v9
.end method


# virtual methods
.method public zza(Lcom/google/android/gms/internal/zzk;)Lcom/google/android/gms/internal/zzi;
    .registers 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/google/android/gms/internal/zzk<*>;)Lcom/google/android/gms/internal/zzi;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/zzr;
        }
    .end annotation

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    :goto_4
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v11

    :try_start_a
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/internal/zzk;->zzi()Lcom/google/android/gms/internal/zzb$zza;

    move-result-object v0

    move-object/from16 v1, p0

    invoke-direct {v1, v12, v0}, Lcom/google/android/gms/internal/zzt;->zza(Ljava/util/Map;Lcom/google/android/gms/internal/zzb$zza;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzt;->zzaq:Lcom/google/android/gms/internal/zzy;

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v12}, Lcom/google/android/gms/internal/zzy;->zza(Lcom/google/android/gms/internal/zzk;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    move-object v9, v0

    invoke-interface {v9}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v13

    invoke-interface {v13}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v14

    invoke-interface {v9}, Lorg/apache/http/HttpResponse;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzt;->zza([Lorg/apache/http/Header;)Ljava/util/Map;

    move-result-object v0

    move-object v11, v0

    const/16 v0, 0x130

    if-ne v14, v0, :cond_67

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/internal/zzk;->zzi()Lcom/google/android/gms/internal/zzb$zza;

    move-result-object v15

    if-nez v15, :cond_4f

    new-instance v0, Lcom/google/android/gms/internal/zzi;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    sub-long v5, v1, v7

    const/16 v1, 0x130

    const/4 v2, 0x0

    move-object v3, v11

    const/4 v4, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/zzi;-><init>(I[BLjava/util/Map;ZJ)V
    :try_end_4e
    .catch Ljava/net/SocketTimeoutException; {:try_start_a .. :try_end_4e} :catch_ab
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_a .. :try_end_4e} :catch_ba
    .catch Ljava/net/MalformedURLException; {:try_start_a .. :try_end_4e} :catch_c9
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_4e} :catch_e7

    return-object v0

    :cond_4f
    :try_start_4f
    iget-object v0, v15, Lcom/google/android/gms/internal/zzb$zza;->zzg:Ljava/util/Map;

    invoke-interface {v0, v11}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    new-instance v0, Lcom/google/android/gms/internal/zzi;

    iget-object v2, v15, Lcom/google/android/gms/internal/zzb$zza;->data:[B

    iget-object v3, v15, Lcom/google/android/gms/internal/zzb$zza;->zzg:Ljava/util/Map;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long v5, v4, v7

    const/16 v1, 0x130

    const/4 v4, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/zzi;-><init>(I[BLjava/util/Map;ZJ)V
    :try_end_66
    .catch Ljava/net/SocketTimeoutException; {:try_start_4f .. :try_end_66} :catch_ab
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_4f .. :try_end_66} :catch_ba
    .catch Ljava/net/MalformedURLException; {:try_start_4f .. :try_end_66} :catch_c9
    .catch Ljava/io/IOException; {:try_start_4f .. :try_end_66} :catch_e7

    return-object v0

    :cond_67
    :try_start_67
    invoke-interface {v9}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    if-eqz v0, :cond_79

    invoke-interface {v9}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    move-object/from16 v1, p0

    invoke-direct {v1, v0}, Lcom/google/android/gms/internal/zzt;->zza(Lorg/apache/http/HttpEntity;)[B

    move-result-object v0

    move-object v10, v0

    goto :goto_7d

    :cond_79
    const/4 v0, 0x0

    new-array v0, v0, [B

    move-object v10, v0

    :goto_7d
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long v15, v0, v7

    move-object/from16 v0, p0

    move-wide v1, v15

    move-object/from16 v3, p1

    move-object v4, v10

    move-object v5, v13

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/zzt;->zza(JLcom/google/android/gms/internal/zzk;[BLorg/apache/http/StatusLine;)V

    const/16 v0, 0xc8

    if-lt v14, v0, :cond_95

    const/16 v0, 0x12b

    if-le v14, v0, :cond_9b

    :cond_95
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    :cond_9b
    new-instance v0, Lcom/google/android/gms/internal/zzi;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    sub-long v5, v1, v7

    move v1, v14

    move-object v2, v10

    move-object v3, v11

    const/4 v4, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/zzi;-><init>(I[BLjava/util/Map;ZJ)V
    :try_end_aa
    .catch Ljava/net/SocketTimeoutException; {:try_start_67 .. :try_end_aa} :catch_ab
    .catch Lorg/apache/http/conn/ConnectTimeoutException; {:try_start_67 .. :try_end_aa} :catch_ba
    .catch Ljava/net/MalformedURLException; {:try_start_67 .. :try_end_aa} :catch_c9
    .catch Ljava/io/IOException; {:try_start_67 .. :try_end_aa} :catch_e7

    return-object v0

    :catch_ab
    move-exception v12

    const-string v0, "socket"

    new-instance v1, Lcom/google/android/gms/internal/zzq;

    invoke-direct {v1}, Lcom/google/android/gms/internal/zzq;-><init>()V

    move-object/from16 v2, p1

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/internal/zzt;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzk;Lcom/google/android/gms/internal/zzr;)V

    goto/16 :goto_144

    :catch_ba
    move-exception v12

    const-string v0, "connection"

    new-instance v1, Lcom/google/android/gms/internal/zzq;

    invoke-direct {v1}, Lcom/google/android/gms/internal/zzq;-><init>()V

    move-object/from16 v2, p1

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/internal/zzt;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzk;Lcom/google/android/gms/internal/zzr;)V

    goto/16 :goto_144

    :catch_c9
    move-exception v12

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad URL "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/internal/zzk;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v12}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :catch_e7
    move-exception v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    if-eqz v9, :cond_f5

    invoke-interface {v9}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v13

    goto :goto_fb

    :cond_f5
    new-instance v0, Lcom/google/android/gms/internal/zzj;

    invoke-direct {v0, v12}, Lcom/google/android/gms/internal/zzj;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :goto_fb
    const-string v0, "Unexpected response code %d for %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/internal/zzk;->getUrl()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzs;->zzc(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz v10, :cond_13e

    new-instance v0, Lcom/google/android/gms/internal/zzi;

    move v1, v13

    move-object v2, v10

    move-object v3, v11

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long v5, v4, v7

    const/4 v4, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/zzi;-><init>(I[BLjava/util/Map;ZJ)V

    move-object v14, v0

    const/16 v0, 0x191

    if-eq v13, v0, :cond_12b

    const/16 v0, 0x193

    if-ne v13, v0, :cond_138

    :cond_12b
    const-string v0, "auth"

    new-instance v1, Lcom/google/android/gms/internal/zza;

    invoke-direct {v1, v14}, Lcom/google/android/gms/internal/zza;-><init>(Lcom/google/android/gms/internal/zzi;)V

    move-object/from16 v2, p1

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/internal/zzt;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzk;Lcom/google/android/gms/internal/zzr;)V

    goto :goto_144

    :cond_138
    new-instance v0, Lcom/google/android/gms/internal/zzp;

    invoke-direct {v0, v14}, Lcom/google/android/gms/internal/zzp;-><init>(Lcom/google/android/gms/internal/zzi;)V

    throw v0

    :cond_13e
    new-instance v0, Lcom/google/android/gms/internal/zzh;

    invoke-direct {v0, v14}, Lcom/google/android/gms/internal/zzh;-><init>(Lcom/google/android/gms/internal/zzi;)V

    throw v0

    :goto_144
    goto/16 :goto_4
.end method

.class public final Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;
.super Ljava/lang/Object;
.source "FormatadorBoleto.java"

# interfaces
.implements Lbr/com/concretesolutions/canarinho/formatador/Formatador;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto$SingletonHolder;
    }
.end annotation


# static fields
.field private static final FORMATADOR_NORMAL:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

.field private static final FORMATADOR_TRIBUTOS:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

.field private static final NORMAL_DESFORMATADO:Ljava/util/regex/Pattern;

.field private static final NORMAL_FORMATADO:Ljava/util/regex/Pattern;

.field private static final TRIBUTO_DESFORMATADO:Ljava/util/regex/Pattern;

.field private static final TRIBUTO_FORMATADO:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .line 8
    const-string v0, "(\\d{12})\\s(\\d{12})\\s(\\d{12})\\s(\\d{12})"

    .line 9
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->TRIBUTO_FORMATADO:Ljava/util/regex/Pattern;

    .line 10
    const-string v0, "(\\d{12})(\\d{12})(\\d{12})(\\d{12})"

    .line 11
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->TRIBUTO_DESFORMATADO:Ljava/util/regex/Pattern;

    .line 12
    new-instance v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->TRIBUTO_FORMATADO:Ljava/util/regex/Pattern;

    const-string v2, "$1 $2 $3 $4"

    sget-object v3, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->TRIBUTO_DESFORMATADO:Ljava/util/regex/Pattern;

    const-string v4, "$1$2$3$4"

    invoke-direct {v0, v1, v2, v3, v4}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;-><init>(Ljava/util/regex/Pattern;Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)V

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->FORMATADOR_TRIBUTOS:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    .line 15
    const-string v0, "(\\d{5})[.](\\d{5})\\s(\\d{5})[.](\\d{6})\\s(\\d{5})[.](\\d{6})\\s(\\d)\\s(\\d{14})"

    .line 16
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->NORMAL_FORMATADO:Ljava/util/regex/Pattern;

    .line 17
    const-string v0, "(\\d{5})(\\d{5})(\\d{5})(\\d{6})(\\d{5})(\\d{6})(\\d{1})(\\d{14})"

    .line 18
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->NORMAL_DESFORMATADO:Ljava/util/regex/Pattern;

    .line 19
    new-instance v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->NORMAL_FORMATADO:Ljava/util/regex/Pattern;

    const-string v2, "$1.$2 $3.$4 $5.$6 $7 $8"

    sget-object v3, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->NORMAL_DESFORMATADO:Ljava/util/regex/Pattern;

    const-string v4, "$1$2$3$4$5$6$7$8"

    invoke-direct {v0, v1, v2, v3, v4}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;-><init>(Ljava/util/regex/Pattern;Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)V

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->FORMATADOR_NORMAL:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto$1;)V
    .registers 2
    .param p1, "x0"    # Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto$1;

    .line 6
    invoke-direct {p0}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;-><init>()V

    return-void
.end method

.method private ehTributo(Ljava/lang/String;)Z
    .registers 4
    .param p1, "value"    # Ljava/lang/String;

    .line 68
    if-nez p1, :cond_a

    .line 69
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Valor n\u00e3o pode ser nulo"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_a
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x38

    if-ne v0, v1, :cond_15

    const/4 v0, 0x1

    goto :goto_16

    :cond_15
    const/4 v0, 0x0

    :goto_16
    return v0
.end method

.method static getInstance()Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;
    .registers 1

    .line 76
    # getter for: Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto$SingletonHolder;->INSTANCE:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;
    invoke-static {}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto$SingletonHolder;->access$000()Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public desformata(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .param p1, "value"    # Ljava/lang/String;

    .line 39
    invoke-direct {p0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->ehTributo(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 40
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->FORMATADOR_TRIBUTOS:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    invoke-virtual {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->desformata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 43
    :cond_d
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->FORMATADOR_NORMAL:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    invoke-virtual {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->desformata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public estaFormatado(Ljava/lang/String;)Z
    .registers 3
    .param p1, "value"    # Ljava/lang/String;

    .line 49
    invoke-direct {p0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->ehTributo(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 50
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->FORMATADOR_TRIBUTOS:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    invoke-virtual {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->estaFormatado(Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 53
    :cond_d
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->FORMATADOR_NORMAL:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    invoke-virtual {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->estaFormatado(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public formata(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .param p1, "value"    # Ljava/lang/String;

    .line 29
    invoke-direct {p0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->ehTributo(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 30
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->FORMATADOR_TRIBUTOS:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    invoke-virtual {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 33
    :cond_d
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->FORMATADOR_NORMAL:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    invoke-virtual {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public podeSerFormatado(Ljava/lang/String;)Z
    .registers 3
    .param p1, "value"    # Ljava/lang/String;

    .line 59
    invoke-direct {p0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->ehTributo(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 60
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->FORMATADOR_TRIBUTOS:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    invoke-virtual {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->podeSerFormatado(Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 63
    :cond_d
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->FORMATADOR_NORMAL:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    invoke-virtual {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->podeSerFormatado(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

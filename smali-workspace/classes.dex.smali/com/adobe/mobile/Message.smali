.class abstract Lcom/adobe/mobile/Message;
.super Ljava/lang/Object;
.source "Message.java"


# static fields
.field private static final JSON_DEFAULT_START_DATE:Ljava/lang/Long;

.field private static _blacklist:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
        }
    .end annotation
.end field

.field private static final _blacklistMutex:Ljava/lang/Object;

.field private static final _messageTypeDictionary:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Class;>;"
        }
    .end annotation
.end field

.field private static final _showRuleEnumDictionary:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Lcom/adobe/mobile/Messages$MessageShowRule;>;"
        }
    .end annotation
.end field


# instance fields
.field protected assets:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Ljava/util/ArrayList<Ljava/lang/String;>;>;"
        }
    .end annotation
.end field

.field protected audiences:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/adobe/mobile/MessageMatcher;>;"
        }
    .end annotation
.end field

.field protected endDate:Ljava/util/Date;

.field protected isVisible:Z

.field protected messageId:Ljava/lang/String;

.field protected orientationWhenShown:I

.field protected showOffline:Z

.field protected showRule:Lcom/adobe/mobile/Messages$MessageShowRule;

.field protected startDate:Ljava/util/Date;

.field protected triggers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/adobe/mobile/MessageMatcher;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .line 72
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/Message;->JSON_DEFAULT_START_DATE:Ljava/lang/Long;

    .line 94
    new-instance v0, Lcom/adobe/mobile/Message$1;

    invoke-direct {v0}, Lcom/adobe/mobile/Message$1;-><init>()V

    sput-object v0, Lcom/adobe/mobile/Message;->_messageTypeDictionary:Ljava/util/Map;

    .line 239
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/Message;->_blacklistMutex:Ljava/lang/Object;

    .line 422
    new-instance v0, Lcom/adobe/mobile/Message$2;

    invoke-direct {v0}, Lcom/adobe/mobile/Message$2;-><init>()V

    sput-object v0, Lcom/adobe/mobile/Message;->_showRuleEnumDictionary:Ljava/util/Map;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private mapFromString(Ljava/lang/String;)Ljava/util/HashMap;
    .registers 10
    .param p1, "string"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
        }
    .end annotation

    .line 435
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 437
    .local v4, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    :try_start_5
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 438
    .local v5, "obj":Lorg/json/JSONObject;
    invoke-virtual {v5}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v6

    .line 439
    .local v6, "keys":Ljava/util/Iterator;
    :goto_e
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 440
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/lang/String;

    .line 441
    .local v7, "key":Ljava/lang/String;
    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_26
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_26} :catch_28

    .line 442
    .end local v7    # "key":Ljava/lang/String;
    goto :goto_e

    .line 445
    .end local v5    # "obj":Lorg/json/JSONObject;
    .end local v6    # "keys":Ljava/util/Iterator;
    :cond_27
    goto :goto_38

    .line 443
    :catch_28
    move-exception v5

    .line 444
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Messages- Unable to deserialize blacklist(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 447
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_38
    return-object v4
.end method

.method private static messageShowRuleFromString(Ljava/lang/String;)Lcom/adobe/mobile/Messages$MessageShowRule;
    .registers 2
    .param p0, "showRule"    # Ljava/lang/String;

    .line 431
    sget-object v0, Lcom/adobe/mobile/Message;->_showRuleEnumDictionary:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/adobe/mobile/Messages$MessageShowRule;

    return-object v0
.end method

.method protected static messageWithJsonObject(Lorg/json/JSONObject;)Lcom/adobe/mobile/Message;
    .registers 10
    .param p0, "dictionary"    # Lorg/json/JSONObject;

    .line 104
    const-string v4, ""

    .line 110
    .local v4, "messageTypeString":Ljava/lang/String;
    const-string v0, "template"

    :try_start_4
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 111
    sget-object v0, Lcom/adobe/mobile/Message;->_messageTypeDictionary:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/lang/Class;

    .line 112
    .local v5, "messageClass":Ljava/lang/Class;
    invoke-virtual {v5}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/adobe/mobile/Message;

    .line 113
    .local v6, "message":Lcom/adobe/mobile/Message;
    invoke-virtual {v6, p0}, Lcom/adobe/mobile/Message;->initWithPayloadObject(Lorg/json/JSONObject;)Z
    :try_end_1c
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_1c} :catch_1e
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_1c} :catch_29
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_1c} :catch_37
    .catch Ljava/lang/InstantiationException; {:try_start_4 .. :try_end_1c} :catch_49

    move-result v7

    .line 130
    .local v7, "initComplete":Z
    goto :goto_5b

    .line 115
    .end local v5    # "messageClass":Ljava/lang/Class;
    .end local v6    # "message":Lcom/adobe/mobile/Message;
    .end local v7    # "initComplete":Z
    :catch_1e
    move-exception v8

    .line 116
    .local v8, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - template is required for in-app message"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 117
    const/4 v0, 0x0

    return-object v0

    .line 119
    .end local v8    # "ex":Lorg/json/JSONException;
    :catch_29
    move-exception v8

    .line 120
    .local v8, "ex":Ljava/lang/NullPointerException;
    const-string v0, "Messages - invalid template specified for message (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v4, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 121
    const/4 v0, 0x0

    return-object v0

    .line 123
    .end local v8    # "ex":Ljava/lang/NullPointerException;
    :catch_37
    move-exception v8

    .line 124
    .local v8, "ex":Ljava/lang/IllegalAccessException;
    const-string v0, "Messages - unable to create instance of message (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 125
    const/4 v0, 0x0

    return-object v0

    .line 127
    .end local v8    # "ex":Ljava/lang/IllegalAccessException;
    :catch_49
    move-exception v8

    .line 128
    .local v8, "ex":Ljava/lang/InstantiationException;
    const-string v0, "Messages - unable to create instance of message (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/lang/InstantiationException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 129
    const/4 v0, 0x0

    return-object v0

    .line 132
    .local v5, "messageClass":Ljava/lang/Class;
    .local v6, "message":Lcom/adobe/mobile/Message;
    .local v7, "initComplete":Z
    .end local v8    # "ex":Ljava/lang/InstantiationException;
    :goto_5b
    if-eqz v7, :cond_5f

    move-object v0, v6

    goto :goto_60

    :cond_5f
    const/4 v0, 0x0

    :goto_60
    return-object v0
.end method

.method private stringFromMap(Ljava/util/Map;)Ljava/lang/String;
    .registers 4
    .param p1, "map"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;)Ljava/lang/String;"
        }
    .end annotation

    .line 451
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 452
    .local v1, "obj":Lorg/json/JSONObject;
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected blacklist()V
    .registers 8

    .line 241
    sget-object v4, Lcom/adobe/mobile/Message;->_blacklistMutex:Ljava/lang/Object;

    monitor-enter v4

    .line 242
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/Message;->_blacklist:Ljava/util/HashMap;

    if-nez v0, :cond_d

    .line 243
    invoke-virtual {p0}, Lcom/adobe/mobile/Message;->loadBlacklist()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/Message;->_blacklist:Ljava/util/HashMap;

    .line 246
    :cond_d
    sget-object v0, Lcom/adobe/mobile/Message;->_blacklist:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/adobe/mobile/Message;->messageId:Ljava/lang/String;

    iget-object v2, p0, Lcom/adobe/mobile/Message;->showRule:Lcom/adobe/mobile/Messages$MessageShowRule;

    invoke-virtual {v2}, Lcom/adobe/mobile/Messages$MessageShowRule;->getValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    const-string v0, "Messages - adding message \"%s\" to blacklist"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/Message;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2b
    .catchall {:try_start_3 .. :try_end_2b} :catchall_50

    .line 250
    :try_start_2b
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 251
    .local v5, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v0, "messagesBlackList"

    sget-object v1, Lcom/adobe/mobile/Message;->_blacklist:Ljava/util/HashMap;

    invoke-direct {p0, v1}, Lcom/adobe/mobile/Message;->stringFromMap(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 252
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_3d
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_2b .. :try_end_3d} :catch_3e
    .catchall {:try_start_2b .. :try_end_3d} :catchall_50

    .line 256
    .end local v5    # "editor":Landroid/content/SharedPreferences$Editor;
    goto :goto_4e

    .line 254
    :catch_3e
    move-exception v5

    .line 255
    .local v5, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Messages - Error persisting blacklist map (%s)."

    const/4 v1, 0x1

    :try_start_42
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4e
    .catchall {:try_start_42 .. :try_end_4e} :catchall_50

    .line 257
    .end local v5    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_4e
    monitor-exit v4

    goto :goto_53

    :catchall_50
    move-exception v6

    monitor-exit v4

    throw v6

    .line 258
    :goto_53
    return-void
.end method

.method protected clickedThrough()V
    .registers 5

    .line 396
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 397
    .local v3, "contextData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "a.message.id"

    iget-object v1, p0, Lcom/adobe/mobile/Message;->messageId:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 398
    const-string v0, "a.message.clicked"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    const-string v0, "In-App Message"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getTimeSince1970()J

    move-result-wide v1

    invoke-static {v0, v3, v1, v2}, Lcom/adobe/mobile/AnalyticsTrackInternal;->trackInternal(Ljava/lang/String;Ljava/util/Map;J)V

    .line 402
    iget-object v0, p0, Lcom/adobe/mobile/Message;->showRule:Lcom/adobe/mobile/Messages$MessageShowRule;

    sget-object v1, Lcom/adobe/mobile/Messages$MessageShowRule;->MESSAGE_SHOW_RULE_UNTIL_CLICK:Lcom/adobe/mobile/Messages$MessageShowRule;

    if-ne v0, v1, :cond_28

    .line 403
    invoke-virtual {p0}, Lcom/adobe/mobile/Message;->blacklist()V

    .line 407
    :cond_28
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/adobe/mobile/Messages;->setCurrentMessage(Lcom/adobe/mobile/Message;)V

    .line 408
    return-void
.end method

.method protected description()Ljava/lang/String;
    .registers 3

    .line 411
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Message ID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/adobe/mobile/Message;->messageId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; Show Rule: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/adobe/mobile/Message;->showRule:Lcom/adobe/mobile/Messages$MessageShowRule;

    .line 414
    invoke-virtual {v1}, Lcom/adobe/mobile/Messages$MessageShowRule;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; Blacklisted: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 416
    invoke-virtual {p0}, Lcom/adobe/mobile/Message;->isBlacklisted()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected initWithPayloadObject(Lorg/json/JSONObject;)Z
    .registers 11
    .param p1, "dictionary"    # Lorg/json/JSONObject;

    .line 137
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-nez v0, :cond_a

    .line 138
    :cond_8
    const/4 v0, 0x0

    return v0

    .line 143
    :cond_a
    const-string v0, "messageId"

    :try_start_c
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/Message;->messageId:Ljava/lang/String;

    .line 144
    iget-object v0, p0, Lcom/adobe/mobile/Message;->messageId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_24

    .line 145
    const-string v0, "Messages - Unable to create message, messageId is empty"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_22
    .catch Lorg/json/JSONException; {:try_start_c .. :try_end_22} :catch_25

    .line 146
    const/4 v0, 0x0

    return v0

    .line 152
    :cond_24
    goto :goto_30

    .line 149
    :catch_25
    move-exception v5

    .line 150
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - Unable to create message, messageId is required"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 151
    const/4 v0, 0x0

    return v0

    .line 155
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_30
    const-string v0, "showRule"

    :try_start_32
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 156
    .local v5, "showRuleString":Ljava/lang/String;
    invoke-static {v5}, Lcom/adobe/mobile/Message;->messageShowRuleFromString(Ljava/lang/String;)Lcom/adobe/mobile/Messages$MessageShowRule;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/Message;->showRule:Lcom/adobe/mobile/Messages$MessageShowRule;

    .line 157
    iget-object v0, p0, Lcom/adobe/mobile/Message;->showRule:Lcom/adobe/mobile/Messages$MessageShowRule;

    if-eqz v0, :cond_46

    iget-object v0, p0, Lcom/adobe/mobile/Message;->showRule:Lcom/adobe/mobile/Messages$MessageShowRule;

    sget-object v1, Lcom/adobe/mobile/Messages$MessageShowRule;->MESSAGE_SHOW_RULE_UNKNOWN:Lcom/adobe/mobile/Messages$MessageShowRule;

    if-ne v0, v1, :cond_58

    .line 159
    :cond_46
    const-string v0, "Messages - Unable to create message \"%s\", showRule not valid (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/Message;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object v5, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_56
    .catch Lorg/json/JSONException; {:try_start_32 .. :try_end_56} :catch_59

    .line 160
    const/4 v0, 0x0

    return v0

    .line 166
    .end local v5    # "showRuleString":Ljava/lang/String;
    :cond_58
    goto :goto_69

    .line 163
    :catch_59
    move-exception v5

    .line 164
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - Unable to create message \"%s\", showRule is required"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/Message;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 165
    const/4 v0, 0x0

    return v0

    .line 170
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_69
    const-string v0, "startDate"

    :try_start_6b
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 171
    .local v5, "startDateConfig":J
    new-instance v0, Ljava/util/Date;

    const-wide/16 v1, 0x3e8

    mul-long/2addr v1, v5

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/adobe/mobile/Message;->startDate:Ljava/util/Date;
    :try_end_79
    .catch Lorg/json/JSONException; {:try_start_6b .. :try_end_79} :catch_7a

    .line 176
    .end local v5    # "startDateConfig":J
    goto :goto_98

    .line 173
    :catch_7a
    move-exception v5

    .line 174
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - Tried to read startDate from message \"%s\" but none found. Using default value"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/Message;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 175
    new-instance v0, Ljava/util/Date;

    sget-object v1, Lcom/adobe/mobile/Message;->JSON_DEFAULT_START_DATE:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    mul-long/2addr v1, v3

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/adobe/mobile/Message;->startDate:Ljava/util/Date;

    .line 180
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_98
    const-string v0, "endDate"

    :try_start_9a
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 181
    .local v5, "endDateConfig":J
    new-instance v0, Ljava/util/Date;

    const-wide/16 v1, 0x3e8

    mul-long/2addr v1, v5

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/adobe/mobile/Message;->endDate:Ljava/util/Date;
    :try_end_a8
    .catch Lorg/json/JSONException; {:try_start_9a .. :try_end_a8} :catch_a9

    .line 185
    .end local v5    # "endDateConfig":J
    goto :goto_b7

    .line 183
    :catch_a9
    move-exception v5

    .line 184
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - Tried to read endDate from message \"%s\" but none found. Using default value"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/Message;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 189
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_b7
    const-string v0, "showOffline"

    :try_start_b9
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/adobe/mobile/Message;->showOffline:Z
    :try_end_bf
    .catch Lorg/json/JSONException; {:try_start_b9 .. :try_end_bf} :catch_c0

    .line 194
    goto :goto_d1

    .line 191
    :catch_c0
    move-exception v5

    .line 192
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - Tried to read showOffline from message \"%s\" but none found. Using default value"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/Message;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 193
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/adobe/mobile/Message;->showOffline:Z

    .line 197
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_d1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/adobe/mobile/Message;->audiences:Ljava/util/ArrayList;

    .line 199
    const-string v0, "audiences"

    :try_start_da
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 200
    .local v5, "jsonAudiences":Lorg/json/JSONArray;
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    .line 201
    .local v6, "audienceCount":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_e3
    if-ge v7, v6, :cond_f5

    .line 202
    invoke-virtual {v5, v7}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 203
    .local v8, "matcher":Lorg/json/JSONObject;
    iget-object v0, p0, Lcom/adobe/mobile/Message;->audiences:Ljava/util/ArrayList;

    invoke-static {v8}, Lcom/adobe/mobile/MessageMatcher;->messageMatcherWithJsonObject(Lorg/json/JSONObject;)Lcom/adobe/mobile/MessageMatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_f2
    .catch Lorg/json/JSONException; {:try_start_da .. :try_end_f2} :catch_f6

    .line 201
    .end local v8    # "matcher":Lorg/json/JSONObject;
    add-int/lit8 v7, v7, 0x1

    goto :goto_e3

    .line 208
    .end local v5    # "jsonAudiences":Lorg/json/JSONArray;
    .end local v6    # "audienceCount":I
    .end local v7    # "i":I
    :cond_f5
    goto :goto_10b

    .line 206
    :catch_f6
    move-exception v5

    .line 207
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - failed to read audience for message \"%s\", error: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/Message;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v5}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 211
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_10b
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/adobe/mobile/Message;->triggers:Ljava/util/ArrayList;

    .line 213
    const-string v0, "triggers"

    :try_start_114
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 214
    .local v5, "jsonTriggers":Lorg/json/JSONArray;
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    .line 215
    .local v6, "triggersCount":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_11d
    if-ge v7, v6, :cond_12f

    .line 216
    invoke-virtual {v5, v7}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 217
    .local v8, "matcher":Lorg/json/JSONObject;
    iget-object v0, p0, Lcom/adobe/mobile/Message;->triggers:Ljava/util/ArrayList;

    invoke-static {v8}, Lcom/adobe/mobile/MessageMatcher;->messageMatcherWithJsonObject(Lorg/json/JSONObject;)Lcom/adobe/mobile/MessageMatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_12c
    .catch Lorg/json/JSONException; {:try_start_114 .. :try_end_12c} :catch_130

    .line 215
    .end local v8    # "matcher":Lorg/json/JSONObject;
    add-int/lit8 v7, v7, 0x1

    goto :goto_11d

    .line 222
    .end local v5    # "jsonTriggers":Lorg/json/JSONArray;
    .end local v6    # "triggersCount":I
    .end local v7    # "i":I
    :cond_12f
    goto :goto_145

    .line 220
    :catch_130
    move-exception v5

    .line 221
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - failed to read trigger for message \"%s\", error: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/Message;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v5}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 225
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_145
    iget-object v0, p0, Lcom/adobe/mobile/Message;->triggers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_15c

    .line 226
    const-string v0, "Messages - Unable to load message \"%s\" - at least one valid trigger is required for a message"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/Message;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 227
    const/4 v0, 0x0

    return v0

    .line 230
    :cond_15c
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/adobe/mobile/Message;->isVisible:Z

    .line 232
    const/4 v0, 0x1

    return v0
.end method

.method protected isBlacklisted()Z
    .registers 5

    .line 279
    sget-object v2, Lcom/adobe/mobile/Message;->_blacklistMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 280
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/Message;->_blacklist:Ljava/util/HashMap;

    if-nez v0, :cond_d

    .line 281
    invoke-virtual {p0}, Lcom/adobe/mobile/Message;->loadBlacklist()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/adobe/mobile/Message;->_blacklist:Ljava/util/HashMap;

    .line 284
    :cond_d
    sget-object v0, Lcom/adobe/mobile/Message;->_blacklist:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/adobe/mobile/Message;->messageId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_1c

    move-result-object v0

    if-eqz v0, :cond_19

    const/4 v0, 0x1

    goto :goto_1a

    :cond_19
    const/4 v0, 0x0

    :goto_1a
    monitor-exit v2

    return v0

    .line 285
    :catchall_1c
    move-exception v3

    monitor-exit v2

    throw v3
.end method

.method protected loadBlacklist()Ljava/util/HashMap;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
        }
    .end annotation

    .line 290
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "messagesBlackList"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 291
    .local v4, "blackListString":Ljava/lang/String;
    if-nez v4, :cond_13

    .line 292
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V
    :try_end_12
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_0 .. :try_end_12} :catch_18

    return-object v0

    .line 295
    :cond_13
    :try_start_13
    invoke-direct {p0, v4}, Lcom/adobe/mobile/Message;->mapFromString(Ljava/lang/String;)Ljava/util/HashMap;
    :try_end_16
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_13 .. :try_end_16} :catch_18

    move-result-object v0

    return-object v0

    .line 296
    .end local v4    # "blackListString":Ljava/lang/String;
    :catch_18
    move-exception v4

    .line 297
    .local v4, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Messaging - Unable to get shared preferences while loading blacklist. (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 298
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    return-object v0
.end method

.method protected removeFromBlacklist()V
    .registers 8

    .line 261
    invoke-virtual {p0}, Lcom/adobe/mobile/Message;->isBlacklisted()Z

    move-result v0

    if-eqz v0, :cond_45

    .line 262
    sget-object v4, Lcom/adobe/mobile/Message;->_blacklistMutex:Ljava/lang/Object;

    monitor-enter v4

    .line 263
    :try_start_9
    sget-object v0, Lcom/adobe/mobile/Message;->_blacklist:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/adobe/mobile/Message;->messageId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    const-string v0, "Messages - removing message \"%s\" from blacklist"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/Message;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1d
    .catchall {:try_start_9 .. :try_end_1d} :catchall_42

    .line 267
    :try_start_1d
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 268
    .local v5, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v0, "messagesBlackList"

    sget-object v1, Lcom/adobe/mobile/Message;->_blacklist:Ljava/util/HashMap;

    invoke-direct {p0, v1}, Lcom/adobe/mobile/Message;->stringFromMap(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 269
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_2f
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1d .. :try_end_2f} :catch_30
    .catchall {:try_start_1d .. :try_end_2f} :catchall_42

    .line 273
    .end local v5    # "editor":Landroid/content/SharedPreferences$Editor;
    goto :goto_40

    .line 271
    :catch_30
    move-exception v5

    .line 272
    .local v5, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Messages - Error persisting blacklist map (%s)."

    const/4 v1, 0x1

    :try_start_34
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_40
    .catchall {:try_start_34 .. :try_end_40} :catchall_42

    .line 274
    .end local v5    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_40
    monitor-exit v4

    goto :goto_45

    :catchall_42
    move-exception v6

    monitor-exit v4

    throw v6

    .line 276
    :cond_45
    :goto_45
    return-void
.end method

.method protected shouldShowForVariables(Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)Z
    .registers 12
    .param p1, "vars"    # Ljava/util/Map;
    .param p2, "cdata"    # Ljava/util/Map;
    .param p3, "lifecycleData"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;)Z"
        }
    .end annotation

    .line 305
    iget-boolean v0, p0, Lcom/adobe/mobile/Message;->isVisible:Z

    if-eqz v0, :cond_12

    iget v0, p0, Lcom/adobe/mobile/Message;->orientationWhenShown:I

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCurrentOrientation()I

    move-result v1

    if-eq v0, v1, :cond_12

    instance-of v0, p0, Lcom/adobe/mobile/MessageAlert;

    if-eqz v0, :cond_12

    .line 306
    const/4 v0, 0x1

    return v0

    .line 310
    :cond_12
    invoke-static {}, Lcom/adobe/mobile/Messages;->getCurrentMessage()Lcom/adobe/mobile/Message;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 311
    instance-of v0, p0, Lcom/adobe/mobile/MessageLocalNotification;

    if-nez v0, :cond_22

    instance-of v0, p0, Lcom/adobe/mobile/MessageTemplateCallback;

    if-nez v0, :cond_22

    .line 312
    const/4 v0, 0x0

    return v0

    .line 317
    :cond_22
    if-eqz p1, :cond_2a

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    if-gtz v0, :cond_34

    :cond_2a
    if-eqz p2, :cond_32

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v0

    if-gtz v0, :cond_34

    .line 318
    :cond_32
    const/4 v0, 0x0

    return v0

    .line 322
    :cond_34
    invoke-virtual {p0}, Lcom/adobe/mobile/Message;->isBlacklisted()Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 323
    const/4 v0, 0x0

    return v0

    .line 327
    :cond_3c
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->networkConnectivity()Z

    move-result v0

    if-nez v0, :cond_4c

    .line 328
    iget-boolean v0, p0, Lcom/adobe/mobile/Message;->showOffline:Z

    if-nez v0, :cond_4c

    .line 329
    const/4 v0, 0x0

    return v0

    .line 334
    :cond_4c
    new-instance v4, Ljava/util/Date;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getTimeSince1970()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    invoke-direct {v4, v0, v1}, Ljava/util/Date;-><init>(J)V

    .line 335
    .local v4, "now":Ljava/util/Date;
    iget-object v0, p0, Lcom/adobe/mobile/Message;->startDate:Ljava/util/Date;

    invoke-virtual {v4, v0}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_62

    .line 336
    const/4 v0, 0x0

    return v0

    .line 339
    :cond_62
    iget-object v0, p0, Lcom/adobe/mobile/Message;->endDate:Ljava/util/Date;

    if-eqz v0, :cond_70

    iget-object v0, p0, Lcom/adobe/mobile/Message;->endDate:Ljava/util/Date;

    invoke-virtual {v4, v0}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_70

    .line 340
    const/4 v0, 0x0

    return v0

    .line 344
    :cond_70
    iget-object v0, p0, Lcom/adobe/mobile/Message;->audiences:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_76
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_92

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/adobe/mobile/MessageMatcher;

    .line 345
    .local v6, "matcher":Lcom/adobe/mobile/MessageMatcher;
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/util/Map;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    invoke-virtual {v6, v0}, Lcom/adobe/mobile/MessageMatcher;->matchesInMaps([Ljava/util/Map;)Z

    move-result v0

    if-nez v0, :cond_91

    .line 346
    const/4 v0, 0x0

    return v0

    .line 348
    .end local v6    # "matcher":Lcom/adobe/mobile/MessageMatcher;
    :cond_91
    goto :goto_76

    .line 351
    :cond_92
    invoke-static {p2}, Lcom/adobe/mobile/StaticMethods;->cleanContextDataDictionary(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v5

    .line 353
    .local v5, "cdataCleaned":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v0, p0, Lcom/adobe/mobile/Message;->triggers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_9c
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_bb

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/adobe/mobile/MessageMatcher;

    .line 354
    .local v7, "matcher":Lcom/adobe/mobile/MessageMatcher;
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/util/Map;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object v5, v0, v1

    invoke-virtual {v7, v0}, Lcom/adobe/mobile/MessageMatcher;->matchesInMaps([Ljava/util/Map;)Z

    move-result v0

    if-nez v0, :cond_ba

    .line 355
    const/4 v0, 0x0

    return v0

    .line 357
    .end local v7    # "matcher":Lcom/adobe/mobile/MessageMatcher;
    :cond_ba
    goto :goto_9c

    .line 359
    :cond_bb
    const/4 v0, 0x1

    return v0
.end method

.method protected show()V
    .registers 5

    .line 363
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 364
    .local v3, "contextData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "a.message.id"

    iget-object v1, p0, Lcom/adobe/mobile/Message;->messageId:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 365
    const-string v0, "a.message.triggered"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    const-string v0, "In-App Message"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getTimeSince1970()J

    move-result-wide v1

    invoke-static {v0, v3, v1, v2}, Lcom/adobe/mobile/AnalyticsTrackInternal;->trackInternal(Ljava/lang/String;Ljava/util/Map;J)V

    .line 369
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCurrentOrientation()I

    move-result v0

    iput v0, p0, Lcom/adobe/mobile/Message;->orientationWhenShown:I

    .line 372
    iget-object v0, p0, Lcom/adobe/mobile/Message;->showRule:Lcom/adobe/mobile/Messages$MessageShowRule;

    sget-object v1, Lcom/adobe/mobile/Messages$MessageShowRule;->MESSAGE_SHOW_RULE_ONCE:Lcom/adobe/mobile/Messages$MessageShowRule;

    if-ne v0, v1, :cond_2e

    .line 373
    invoke-virtual {p0}, Lcom/adobe/mobile/Message;->blacklist()V

    .line 377
    :cond_2e
    instance-of v0, p0, Lcom/adobe/mobile/MessageAlert;

    if-nez v0, :cond_36

    instance-of v0, p0, Lcom/adobe/mobile/MessageFullScreen;

    if-eqz v0, :cond_39

    .line 378
    :cond_36
    invoke-static {p0}, Lcom/adobe/mobile/Messages;->setCurrentMessage(Lcom/adobe/mobile/Message;)V

    .line 380
    :cond_39
    return-void
.end method

.method protected viewed()V
    .registers 5

    .line 386
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 387
    .local v3, "contextData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "a.message.id"

    iget-object v1, p0, Lcom/adobe/mobile/Message;->messageId:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    const-string v0, "a.message.viewed"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389
    const-string v0, "In-App Message"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getTimeSince1970()J

    move-result-wide v1

    invoke-static {v0, v3, v1, v2}, Lcom/adobe/mobile/AnalyticsTrackInternal;->trackInternal(Ljava/lang/String;Ljava/util/Map;J)V

    .line 392
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/adobe/mobile/Messages;->setCurrentMessage(Lcom/adobe/mobile/Message;)V

    .line 393
    return-void
.end method

.class final enum Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;
.super Ljava/lang/Enum;
.source "WidgetSaldoMultiLimiteFragment.java"

# interfaces
.implements Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Estados"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;>;Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

.field public static final enum MOSTRA_SALDO:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

.field public static final enum SALDO_NAO_ENCONTRADO:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

.field public static final enum SALDO_VISIVEL:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 526
    new-instance v0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    const-string v1, "SALDO_NAO_ENCONTRADO"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;->SALDO_NAO_ENCONTRADO:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    new-instance v0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    const-string v1, "MOSTRA_SALDO"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;->MOSTRA_SALDO:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    new-instance v0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    const-string v1, "SALDO_VISIVEL"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;->SALDO_VISIVEL:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    .line 525
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    sget-object v1, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;->SALDO_NAO_ENCONTRADO:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;->MOSTRA_SALDO:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;->SALDO_VISIVEL:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;->$VALUES:[Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 525
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 525
    const-class v0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    return-object v0
.end method

.method public static values()[Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;
    .registers 1

    .line 525
    sget-object v0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;->$VALUES:[Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    invoke-virtual {v0}, [Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    return-object v0
.end method

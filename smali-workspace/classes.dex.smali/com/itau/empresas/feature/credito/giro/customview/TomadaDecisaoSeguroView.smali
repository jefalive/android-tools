.class public Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;
.super Landroid/widget/LinearLayout;
.source "TomadaDecisaoSeguroView.java"


# instance fields
.field botaoComSeguro:Landroid/widget/ToggleButton;

.field botaoSemSeguro:Landroid/widget/ToggleButton;

.field private deveBloquearMudancaComSeguro:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 37
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method private abrirConfirmacaoSemSeguro()V
    .registers 4

    .line 95
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->DOIS_BOTOES:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 96
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 97
    const v1, 0x7f070479

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNegativo(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 98
    const v1, 0x7f0704d2

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoPositivo(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 99
    const v1, 0x7f07052c

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v2

    .line 102
    .local v2, "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView$2;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView$2;-><init>(Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNegativoListener(Landroid/view/View$OnClickListener;)V

    .line 109
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView$3;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView$3;-><init>(Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setPositivoListener(Landroid/view/View$OnClickListener;)V

    .line 117
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 118
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;

    .line 26
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->alternarParaSemSeguro()V

    return-void
.end method

.method private alternarParaSemSeguro()V
    .registers 2

    .line 122
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->visualizacaoSemSeguro()V

    .line 124
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->notificarTomadaDecisao(Z)V

    .line 125
    return-void
.end method

.method private notificarTomadaDecisao(Z)V
    .registers 4
    .param p1, "comSeguro"    # Z

    .line 128
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/Evento$EventoTomadaDecisaoSeguro;

    invoke-direct {v1, p1}, Lcom/itau/empresas/Evento$EventoTomadaDecisaoSeguro;-><init>(Z)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 129
    return-void
.end method

.method private visualizacaoComSeguro()V
    .registers 4

    .line 143
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->botaoSemSeguro:Landroid/widget/ToggleButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 144
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->botaoComSeguro:Landroid/widget/ToggleButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 146
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->botaoComSeguro:Landroid/widget/ToggleButton;

    .line 147
    const v1, 0x7f02006b

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setBackgroundResource(I)V

    .line 148
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->botaoSemSeguro:Landroid/widget/ToggleButton;

    const v1, 0x7f020068

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setBackgroundResource(I)V

    .line 149
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->botaoComSeguro:Landroid/widget/ToggleButton;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c0105

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setTextColor(I)V

    .line 150
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->botaoSemSeguro:Landroid/widget/ToggleButton;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c00cd

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setTextColor(I)V

    .line 151
    return-void
.end method

.method private visualizacaoSemSeguro()V
    .registers 4

    .line 133
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->botaoSemSeguro:Landroid/widget/ToggleButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 134
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->botaoComSeguro:Landroid/widget/ToggleButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 136
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->botaoComSeguro:Landroid/widget/ToggleButton;

    const v1, 0x7f02006a

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setBackgroundResource(I)V

    .line 137
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->botaoSemSeguro:Landroid/widget/ToggleButton;

    const v1, 0x7f020069

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setBackgroundResource(I)V

    .line 138
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->botaoComSeguro:Landroid/widget/ToggleButton;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c00cd

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setTextColor(I)V

    .line 139
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->botaoSemSeguro:Landroid/widget/ToggleButton;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c0105

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setTextColor(I)V

    .line 140
    return-void
.end method


# virtual methods
.method public alternarTomadaDeDecisao(Z)V
    .registers 2
    .param p1, "comSeguro"    # Z

    .line 79
    if-eqz p1, :cond_6

    .line 81
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->visualizacaoComSeguro()V

    goto :goto_9

    .line 84
    :cond_6
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->visualizacaoSemSeguro()V

    .line 86
    :goto_9
    return-void
.end method

.method aoClicarComSeguro()V
    .registers 4

    .line 47
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->deveBloquearMudancaComSeguro:Z

    if-eqz v0, :cond_37

    .line 49
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->UM_BOTAO:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 50
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 51
    const v1, 0x7f07048c

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNeutro(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 52
    const v1, 0x7f070668

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v2

    .line 55
    .local v2, "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView$1;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView$1;-><init>(Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNeutroListener(Landroid/view/View$OnClickListener;)V

    .line 62
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 63
    .end local v2    # "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    goto :goto_3e

    .line 65
    :cond_37
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->visualizacaoComSeguro()V

    .line 67
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->notificarTomadaDecisao(Z)V

    .line 69
    :goto_3e
    return-void
.end method

.method aoClicarSemSeguro()V
    .registers 1

    .line 74
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->abrirConfirmacaoSemSeguro()V

    .line 75
    return-void
.end method

.method public setDeveBloquearMudancaComSeguro(Z)V
    .registers 2
    .param p1, "deveBloquearMudancaComSeguro"    # Z

    .line 90
    iput-boolean p1, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->deveBloquearMudancaComSeguro:Z

    .line 91
    return-void
.end method

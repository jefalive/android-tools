.class final Lcom/google/android/gms/internal/zzsw;
.super Ljava/lang/Object;


# instance fields
.field final tag:I

.field final zzbuv:[B


# direct methods
.method constructor <init>(I[B)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/zzsw;->tag:I

    iput-object p2, p0, Lcom/google/android/gms/internal/zzsw;->zzbuv:[B

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1, "o"    # Ljava/lang/Object;

    if-ne p1, p0, :cond_4

    const/4 v0, 0x1

    return v0

    :cond_4
    instance-of v0, p1, Lcom/google/android/gms/internal/zzsw;

    if-nez v0, :cond_a

    const/4 v0, 0x0

    return v0

    :cond_a
    move-object v2, p1

    check-cast v2, Lcom/google/android/gms/internal/zzsw;

    iget v0, p0, Lcom/google/android/gms/internal/zzsw;->tag:I

    iget v1, v2, Lcom/google/android/gms/internal/zzsw;->tag:I

    if-ne v0, v1, :cond_1f

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsw;->zzbuv:[B

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsw;->zzbuv:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_1f

    const/4 v0, 0x1

    goto :goto_20

    :cond_1f
    const/4 v0, 0x0

    :goto_20
    return v0
.end method

.method public hashCode()I
    .registers 4

    const/16 v2, 0x11

    iget v0, p0, Lcom/google/android/gms/internal/zzsw;->tag:I

    add-int/lit16 v2, v0, 0x20f

    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsw;->zzbuv:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    add-int v2, v0, v1

    return v2
.end method

.method writeTo(Lcom/google/android/gms/internal/zzsn;)V
    .registers 3
    .param p1, "output"    # Lcom/google/android/gms/internal/zzsn;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/google/android/gms/internal/zzsw;->tag:I

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/zzsn;->zzmB(I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsw;->zzbuv:[B

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/zzsn;->zzH([B)V

    return-void
.end method

.method zzz()I
    .registers 3

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/gms/internal/zzsw;->tag:I

    invoke-static {v0}, Lcom/google/android/gms/internal/zzsn;->zzmC(I)I

    move-result v0

    add-int/lit8 v1, v0, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsw;->zzbuv:[B

    array-length v0, v0

    add-int/2addr v1, v0

    return v1
.end method

.class public Lcom/google/android/gms/ads/internal/zzm;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# direct methods
.method private static zza(Lcom/google/android/gms/internal/zzfb;)Lcom/google/android/gms/ads/internal/formats/zzd;
    .registers 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/gms/ads/internal/formats/zzd;

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzfb;->getHeadline()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzfb;->getImages()Ljava/util/List;

    move-result-object v2

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzfb;->getBody()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzfb;->zzdK()Lcom/google/android/gms/internal/zzch;

    move-result-object v4

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzfb;->getCallToAction()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzfb;->getStarRating()D

    move-result-wide v6

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzfb;->getStore()Ljava/lang/String;

    move-result-object v8

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzfb;->getPrice()Ljava/lang/String;

    move-result-object v9

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzfb;->getExtras()Landroid/os/Bundle;

    move-result-object v11

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gms/ads/internal/formats/zzd;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/gms/internal/zzch;Ljava/lang/String;DLjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/ads/internal/formats/zza;Landroid/os/Bundle;)V

    return-object v0
.end method

.method private static zza(Lcom/google/android/gms/internal/zzfc;)Lcom/google/android/gms/ads/internal/formats/zze;
    .registers 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/gms/ads/internal/formats/zze;

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzfc;->getHeadline()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzfc;->getImages()Ljava/util/List;

    move-result-object v2

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzfc;->getBody()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzfc;->zzdO()Lcom/google/android/gms/internal/zzch;

    move-result-object v4

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzfc;->getCallToAction()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzfc;->getAdvertiser()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzfc;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/ads/internal/formats/zze;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/gms/internal/zzch;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/ads/internal/formats/zza;Landroid/os/Bundle;)V

    return-object v0
.end method

.method static zza(Lcom/google/android/gms/internal/zzfb;Lcom/google/android/gms/internal/zzfc;Lcom/google/android/gms/ads/internal/zzf$zza;)Lcom/google/android/gms/internal/zzdf;
    .registers 4

    new-instance v0, Lcom/google/android/gms/ads/internal/zzm$5;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/android/gms/ads/internal/zzm$5;-><init>(Lcom/google/android/gms/internal/zzfb;Lcom/google/android/gms/ads/internal/zzf$zza;Lcom/google/android/gms/internal/zzfc;)V

    return-object v0
.end method

.method static zza(Ljava/util/concurrent/CountDownLatch;)Lcom/google/android/gms/internal/zzdf;
    .registers 2

    new-instance v0, Lcom/google/android/gms/ads/internal/zzm$3;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/internal/zzm$3;-><init>(Ljava/util/concurrent/CountDownLatch;)V

    return-object v0
.end method

.method private static zza(Landroid/graphics/Bitmap;)Ljava/lang/String;
    .registers 6

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    if-nez p0, :cond_f

    const-string v0, "Bitmap is null. Returning empty string"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    const-string v0, ""

    return-object v0

    :cond_f
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    const/4 v0, 0x0

    invoke-static {v3, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "data:image/png;base64,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static zza(Lcom/google/android/gms/internal/zzch;)Ljava/lang/String;
    .registers 4

    if-nez p0, :cond_a

    const-string v0, "Image is null. Returning empty string"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    const-string v0, ""

    return-object v0

    :cond_a
    const/4 v1, 0x0

    :try_start_b
    invoke-interface {p0}, Lcom/google/android/gms/internal/zzch;->getUri()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;
    :try_end_14
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_14} :catch_17

    move-result-object v0

    return-object v0

    :cond_16
    goto :goto_1d

    :catch_17
    move-exception v2

    const-string v0, "Unable to get image uri. Trying data uri next"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    :goto_1d
    invoke-static {p0}, Lcom/google/android/gms/ads/internal/zzm;->zzb(Lcom/google/android/gms/internal/zzch;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static zza(Landroid/os/Bundle;Ljava/lang/String;)Lorg/json/JSONObject;
    .registers 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    if-eqz p0, :cond_d

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_d
    return-object v1

    :cond_e
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    :goto_17
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/String;

    invoke-virtual {p0, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2b

    goto :goto_17

    :cond_2b
    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v0, "image"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_50

    invoke-virtual {p0, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    instance-of v0, v6, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4a

    move-object v0, v6

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/zzm;->zza(Landroid/graphics/Bitmap;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v4, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_4f

    :cond_4a
    const-string v0, "Invalid type. An image type extra should return a bitmap"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    :goto_4f
    goto :goto_69

    :cond_50
    invoke-virtual {p0, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    instance-of v0, v6, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_5e

    const-string v0, "Invalid asset type. Bitmap should be returned only for image type"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    goto :goto_69

    :cond_5e
    invoke-virtual {p0, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :goto_69
    goto/16 :goto_17

    :cond_6b
    return-object v1
.end method

.method public static zza(Lcom/google/android/gms/internal/zzif;Lcom/google/android/gms/ads/internal/zzf$zza;)V
    .registers 10

    invoke-static {p0}, Lcom/google/android/gms/ads/internal/zzm;->zzg(Lcom/google/android/gms/internal/zzif;)Z

    move-result v0

    if-eqz v0, :cond_91

    iget-object v3, p0, Lcom/google/android/gms/internal/zzif;->zzED:Lcom/google/android/gms/internal/zzjp;

    invoke-interface {v3}, Lcom/google/android/gms/internal/zzjp;->getView()Landroid/view/View;

    move-result-object v4

    if-nez v4, :cond_14

    const-string v0, "AdWebView is null"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    return-void

    :cond_14
    :try_start_14
    iget-object v0, p0, Lcom/google/android/gms/internal/zzif;->zzCp:Lcom/google/android/gms/internal/zzen;

    iget-object v5, v0, Lcom/google/android/gms/internal/zzen;->zzBM:Ljava/util/List;

    if-eqz v5, :cond_20

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_26

    :cond_20
    const-string v0, "No template ids present in mediation response"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V
    :try_end_25
    .catch Landroid/os/RemoteException; {:try_start_14 .. :try_end_25} :catch_8a

    return-void

    :cond_26
    :try_start_26
    iget-object v0, p0, Lcom/google/android/gms/internal/zzif;->zzCq:Lcom/google/android/gms/internal/zzey;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzey;->zzeF()Lcom/google/android/gms/internal/zzfb;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/gms/internal/zzif;->zzCq:Lcom/google/android/gms/internal/zzey;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzey;->zzeG()Lcom/google/android/gms/internal/zzfc;

    move-result-object v7

    const-string v0, "2"

    invoke-interface {v5, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5b

    if-eqz v6, :cond_5b

    invoke-static {v4}, Lcom/google/android/gms/dynamic/zze;->zzC(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/zzd;

    move-result-object v0

    invoke-interface {v6, v0}, Lcom/google/android/gms/internal/zzfb;->zzd(Lcom/google/android/gms/dynamic/zzd;)V

    invoke-interface {v6}, Lcom/google/android/gms/internal/zzfb;->getOverrideImpressionRecording()Z

    move-result v0

    if-nez v0, :cond_4c

    invoke-interface {v6}, Lcom/google/android/gms/internal/zzfb;->recordImpression()V

    :cond_4c
    invoke-interface {v3}, Lcom/google/android/gms/internal/zzjp;->zzhU()Lcom/google/android/gms/internal/zzjq;

    move-result-object v0

    const-string v1, "/nativeExpressViewClicked"

    const/4 v2, 0x0

    invoke-static {v6, v2, p1}, Lcom/google/android/gms/ads/internal/zzm;->zza(Lcom/google/android/gms/internal/zzfb;Lcom/google/android/gms/internal/zzfc;Lcom/google/android/gms/ads/internal/zzf$zza;)Lcom/google/android/gms/internal/zzdf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/zzjq;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzdf;)V

    goto :goto_89

    :cond_5b
    const-string v0, "1"

    invoke-interface {v5, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_84

    if-eqz v7, :cond_84

    invoke-static {v4}, Lcom/google/android/gms/dynamic/zze;->zzC(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/zzd;

    move-result-object v0

    invoke-interface {v7, v0}, Lcom/google/android/gms/internal/zzfc;->zzd(Lcom/google/android/gms/dynamic/zzd;)V

    invoke-interface {v7}, Lcom/google/android/gms/internal/zzfc;->getOverrideImpressionRecording()Z

    move-result v0

    if-nez v0, :cond_75

    invoke-interface {v7}, Lcom/google/android/gms/internal/zzfc;->recordImpression()V

    :cond_75
    invoke-interface {v3}, Lcom/google/android/gms/internal/zzjp;->zzhU()Lcom/google/android/gms/internal/zzjq;

    move-result-object v0

    const-string v1, "/nativeExpressViewClicked"

    const/4 v2, 0x0

    invoke-static {v2, v7, p1}, Lcom/google/android/gms/ads/internal/zzm;->zza(Lcom/google/android/gms/internal/zzfb;Lcom/google/android/gms/internal/zzfc;Lcom/google/android/gms/ads/internal/zzf$zza;)Lcom/google/android/gms/internal/zzdf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/zzjq;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzdf;)V

    goto :goto_89

    :cond_84
    const-string v0, "No matching template id and mapper"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V
    :try_end_89
    .catch Landroid/os/RemoteException; {:try_start_26 .. :try_end_89} :catch_8a

    :goto_89
    goto :goto_91

    :catch_8a
    move-exception v5

    const-string v0, "Error occurred while recording impression and registering for clicks"

    invoke-static {v0, v5}, Lcom/google/android/gms/internal/zzin;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void

    :cond_91
    :goto_91
    return-void
.end method

.method private static zza(Lcom/google/android/gms/internal/zzjp;)V
    .registers 3

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzjp;->zzif()Landroid/view/View$OnClickListener;

    move-result-object v1

    if-eqz v1, :cond_d

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzjp;->getView()Landroid/view/View;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_d
    return-void
.end method

.method private static zza(Lcom/google/android/gms/internal/zzjp;Lcom/google/android/gms/ads/internal/formats/zzd;Ljava/lang/String;)V
    .registers 5

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzjp;->zzhU()Lcom/google/android/gms/internal/zzjq;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/ads/internal/zzm$1;

    invoke-direct {v1, p1, p2, p0}, Lcom/google/android/gms/ads/internal/zzm$1;-><init>(Lcom/google/android/gms/ads/internal/formats/zzd;Ljava/lang/String;Lcom/google/android/gms/internal/zzjp;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzjq;->zza(Lcom/google/android/gms/internal/zzjq$zza;)V

    return-void
.end method

.method private static zza(Lcom/google/android/gms/internal/zzjp;Lcom/google/android/gms/ads/internal/formats/zze;Ljava/lang/String;)V
    .registers 5

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzjp;->zzhU()Lcom/google/android/gms/internal/zzjq;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/ads/internal/zzm$2;

    invoke-direct {v1, p1, p2, p0}, Lcom/google/android/gms/ads/internal/zzm$2;-><init>(Lcom/google/android/gms/ads/internal/formats/zze;Ljava/lang/String;Lcom/google/android/gms/internal/zzjp;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzjq;->zza(Lcom/google/android/gms/internal/zzjq$zza;)V

    return-void
.end method

.method private static zza(Lcom/google/android/gms/internal/zzjp;Ljava/util/concurrent/CountDownLatch;)V
    .registers 5

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzjp;->zzhU()Lcom/google/android/gms/internal/zzjq;

    move-result-object v0

    const-string v1, "/nativeExpressAssetsLoaded"

    invoke-static {p1}, Lcom/google/android/gms/ads/internal/zzm;->zza(Ljava/util/concurrent/CountDownLatch;)Lcom/google/android/gms/internal/zzdf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/zzjq;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzdf;)V

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzjp;->zzhU()Lcom/google/android/gms/internal/zzjq;

    move-result-object v0

    const-string v1, "/nativeExpressAssetsLoadingFailed"

    invoke-static {p1}, Lcom/google/android/gms/ads/internal/zzm;->zzb(Ljava/util/concurrent/CountDownLatch;)Lcom/google/android/gms/internal/zzdf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/zzjq;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzdf;)V

    return-void
.end method

.method public static zza(Lcom/google/android/gms/internal/zzjp;Lcom/google/android/gms/internal/zzes;Ljava/util/concurrent/CountDownLatch;)Z
    .registers 6

    const/4 v1, 0x0

    :try_start_1
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/ads/internal/zzm;->zzb(Lcom/google/android/gms/internal/zzjp;Lcom/google/android/gms/internal/zzes;Ljava/util/concurrent/CountDownLatch;)Z
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_4} :catch_7
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_4} :catch_e

    move-result v0

    move v1, v0

    goto :goto_13

    :catch_7
    move-exception v2

    const-string v0, "Unable to invoke load assets"

    invoke-static {v0, v2}, Lcom/google/android/gms/internal/zzin;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_13

    :catch_e
    move-exception v2

    invoke-virtual {p2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v2

    :goto_13
    if-nez v1, :cond_18

    invoke-virtual {p2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :cond_18
    return v1
.end method

.method static zzb(Ljava/util/concurrent/CountDownLatch;)Lcom/google/android/gms/internal/zzdf;
    .registers 2

    new-instance v0, Lcom/google/android/gms/ads/internal/zzm$4;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/internal/zzm$4;-><init>(Ljava/util/concurrent/CountDownLatch;)V

    return-object v0
.end method

.method private static zzb(Lcom/google/android/gms/internal/zzch;)Ljava/lang/String;
    .registers 5

    const/4 v1, 0x0

    :try_start_1
    invoke-interface {p0}, Lcom/google/android/gms/internal/zzch;->zzdJ()Lcom/google/android/gms/dynamic/zzd;

    move-result-object v2

    if-nez v2, :cond_f

    const-string v0, "Drawable is null. Returning empty string"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_c} :catch_17

    const-string v0, ""

    return-object v0

    :cond_f
    :try_start_f
    invoke-static {v2}, Lcom/google/android/gms/dynamic/zze;->zzp(Lcom/google/android/gms/dynamic/zzd;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/graphics/drawable/Drawable;
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_f .. :try_end_16} :catch_17

    goto :goto_20

    :catch_17
    move-exception v2

    const-string v0, "Unable to get drawable. Returning empty string"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    const-string v0, ""

    return-object v0

    :goto_20
    instance-of v0, v1, Landroid/graphics/drawable/BitmapDrawable;

    if-nez v0, :cond_2c

    const-string v0, "Drawable is not an instance of BitmapDrawable. Returning empty string"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    const-string v0, ""

    return-object v0

    :cond_2c
    move-object v2, v1

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/ads/internal/zzm;->zza(Landroid/graphics/Bitmap;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic zzb(Landroid/os/Bundle;Ljava/lang/String;)Lorg/json/JSONObject;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    invoke-static {p0, p1}, Lcom/google/android/gms/ads/internal/zzm;->zza(Landroid/os/Bundle;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    return-object v0
.end method

.method static synthetic zzb(Lcom/google/android/gms/internal/zzjp;)V
    .registers 1

    invoke-static {p0}, Lcom/google/android/gms/ads/internal/zzm;->zza(Lcom/google/android/gms/internal/zzjp;)V

    return-void
.end method

.method private static zzb(Lcom/google/android/gms/internal/zzjp;Lcom/google/android/gms/internal/zzes;Ljava/util/concurrent/CountDownLatch;)Z
    .registers 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-interface {p0}, Lcom/google/android/gms/internal/zzjp;->getView()Landroid/view/View;

    move-result-object v6

    if-nez v6, :cond_d

    const-string v0, "AdWebView is null"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0

    :cond_d
    const/4 v0, 0x4

    invoke-virtual {v6, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p1, Lcom/google/android/gms/internal/zzes;->zzCp:Lcom/google/android/gms/internal/zzen;

    iget-object v7, v0, Lcom/google/android/gms/internal/zzen;->zzBM:Ljava/util/List;

    if-eqz v7, :cond_1d

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_24

    :cond_1d
    const-string v0, "No template ids present in mediation response"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0

    :cond_24
    invoke-static {p0, p2}, Lcom/google/android/gms/ads/internal/zzm;->zza(Lcom/google/android/gms/internal/zzjp;Ljava/util/concurrent/CountDownLatch;)V

    iget-object v0, p1, Lcom/google/android/gms/internal/zzes;->zzCq:Lcom/google/android/gms/internal/zzey;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzey;->zzeF()Lcom/google/android/gms/internal/zzfb;

    move-result-object v8

    iget-object v0, p1, Lcom/google/android/gms/internal/zzes;->zzCq:Lcom/google/android/gms/internal/zzey;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzey;->zzeG()Lcom/google/android/gms/internal/zzfc;

    move-result-object v9

    const-string v0, "2"

    invoke-interface {v7, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_49

    if-eqz v8, :cond_49

    invoke-static {v8}, Lcom/google/android/gms/ads/internal/zzm;->zza(Lcom/google/android/gms/internal/zzfb;)Lcom/google/android/gms/ads/internal/formats/zzd;

    move-result-object v10

    iget-object v0, p1, Lcom/google/android/gms/internal/zzes;->zzCp:Lcom/google/android/gms/internal/zzen;

    iget-object v0, v0, Lcom/google/android/gms/internal/zzen;->zzBL:Ljava/lang/String;

    invoke-static {p0, v10, v0}, Lcom/google/android/gms/ads/internal/zzm;->zza(Lcom/google/android/gms/internal/zzjp;Lcom/google/android/gms/ads/internal/formats/zzd;Ljava/lang/String;)V

    goto :goto_66

    :cond_49
    const-string v0, "1"

    invoke-interface {v7, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5f

    if-eqz v9, :cond_5f

    invoke-static {v9}, Lcom/google/android/gms/ads/internal/zzm;->zza(Lcom/google/android/gms/internal/zzfc;)Lcom/google/android/gms/ads/internal/formats/zze;

    move-result-object v10

    iget-object v0, p1, Lcom/google/android/gms/internal/zzes;->zzCp:Lcom/google/android/gms/internal/zzen;

    iget-object v0, v0, Lcom/google/android/gms/internal/zzen;->zzBL:Ljava/lang/String;

    invoke-static {p0, v10, v0}, Lcom/google/android/gms/ads/internal/zzm;->zza(Lcom/google/android/gms/internal/zzjp;Lcom/google/android/gms/ads/internal/formats/zze;Ljava/lang/String;)V

    goto :goto_66

    :cond_5f
    const-string v0, "No matching template id and mapper"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0

    :goto_66
    iget-object v0, p1, Lcom/google/android/gms/internal/zzes;->zzCp:Lcom/google/android/gms/internal/zzen;

    iget-object v10, v0, Lcom/google/android/gms/internal/zzen;->zzBJ:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/gms/internal/zzes;->zzCp:Lcom/google/android/gms/internal/zzen;

    iget-object v11, v0, Lcom/google/android/gms/internal/zzen;->zzBK:Ljava/lang/String;

    if-eqz v11, :cond_7c

    move-object v0, p0

    move-object v1, v11

    move-object v2, v10

    const-string v3, "text/html"

    const-string v4, "UTF-8"

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/internal/zzjp;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_83

    :cond_7c
    const-string v0, "text/html"

    const-string v1, "UTF-8"

    invoke-interface {p0, v10, v0, v1}, Lcom/google/android/gms/internal/zzjp;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_83
    const/4 v0, 0x1

    return v0
.end method

.method private static zzc(Ljava/lang/Object;)Lcom/google/android/gms/internal/zzch;
    .registers 2

    instance-of v0, p0, Landroid/os/IBinder;

    if-eqz v0, :cond_c

    move-object v0, p0

    check-cast v0, Landroid/os/IBinder;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzch$zza;->zzt(Landroid/os/IBinder;)Lcom/google/android/gms/internal/zzch;

    move-result-object v0

    return-object v0

    :cond_c
    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic zzd(Ljava/lang/Object;)Lcom/google/android/gms/internal/zzch;
    .registers 2

    invoke-static {p0}, Lcom/google/android/gms/ads/internal/zzm;->zzc(Ljava/lang/Object;)Lcom/google/android/gms/internal/zzch;

    move-result-object v0

    return-object v0
.end method

.method public static zzf(Lcom/google/android/gms/internal/zzif;)Landroid/view/View;
    .registers 3

    if-nez p0, :cond_9

    const-string v0, "AdState is null"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->e(Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0

    :cond_9
    invoke-static {p0}, Lcom/google/android/gms/ads/internal/zzm;->zzg(Lcom/google/android/gms/internal/zzif;)Z

    move-result v0

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/google/android/gms/internal/zzif;->zzED:Lcom/google/android/gms/internal/zzjp;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzjp;->getView()Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_16
    :try_start_16
    iget-object v0, p0, Lcom/google/android/gms/internal/zzif;->zzCq:Lcom/google/android/gms/internal/zzey;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzey;->getView()Lcom/google/android/gms/dynamic/zzd;

    move-result-object v1

    if-nez v1, :cond_25

    const-string v0, "View in mediation adapter is null."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V
    :try_end_23
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_23} :catch_2c

    const/4 v0, 0x0

    return-object v0

    :cond_25
    :try_start_25
    invoke-static {v1}, Lcom/google/android/gms/dynamic/zze;->zzp(Lcom/google/android/gms/dynamic/zzd;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;
    :try_end_2b
    .catch Landroid/os/RemoteException; {:try_start_25 .. :try_end_2b} :catch_2c

    return-object v0

    :catch_2c
    move-exception v1

    const-string v0, "Could not get View from mediation adapter."

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzin;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public static zzg(Lcom/google/android/gms/internal/zzif;)Z
    .registers 2

    if-eqz p0, :cond_12

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzif;->zzHT:Z

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/gms/internal/zzif;->zzCp:Lcom/google/android/gms/internal/zzen;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/gms/internal/zzif;->zzCp:Lcom/google/android/gms/internal/zzen;

    iget-object v0, v0, Lcom/google/android/gms/internal/zzen;->zzBJ:Ljava/lang/String;

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    goto :goto_13

    :cond_12
    const/4 v0, 0x0

    :goto_13
    return v0
.end method

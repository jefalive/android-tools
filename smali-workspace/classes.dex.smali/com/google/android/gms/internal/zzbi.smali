.class public Lcom/google/android/gms/internal/zzbi;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# direct methods
.method private static zza(Ljava/lang/Character$UnicodeBlock;)Z
    .registers 2

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->BOPOMOFO:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_38

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->BOPOMOFO_EXTENDED:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_38

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_COMPATIBILITY:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_38

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_COMPATIBILITY_IDEOGRAPHS:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_38

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_38

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_38

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_38

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_38

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->ENCLOSED_CJK_LETTERS_AND_MONTHS:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_38

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->HANGUL_JAMO:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_38

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->HANGUL_SYLLABLES:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_38

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->HIRAGANA:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_38

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->KATAKANA:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_38

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->KATAKANA_PHONETIC_EXTENSIONS:Ljava/lang/Character$UnicodeBlock;

    if-ne p0, v0, :cond_3a

    :cond_38
    const/4 v0, 0x1

    goto :goto_3b

    :cond_3a
    const/4 v0, 0x0

    :goto_3b
    return v0
.end method

.method static zzi(I)Z
    .registers 3

    invoke-static {p0}, Ljava/lang/Character;->isLetter(I)Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-static {p0}, Ljava/lang/Character$UnicodeBlock;->of(I)Ljava/lang/Character$UnicodeBlock;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/internal/zzbi;->zza(Ljava/lang/Character$UnicodeBlock;)Z

    move-result v0

    if-nez v0, :cond_16

    invoke-static {p0}, Lcom/google/android/gms/internal/zzbi;->zzj(I)Z

    move-result v0

    if-eqz v0, :cond_18

    :cond_16
    const/4 v0, 0x1

    return v0

    :cond_18
    const/4 v0, 0x0

    return v0
.end method

.method private static zzj(I)Z
    .registers 2

    const v0, 0xff66

    if-lt p0, v0, :cond_a

    const v0, 0xff9d

    if-le p0, v0, :cond_14

    :cond_a
    const v0, 0xffa1

    if-lt p0, v0, :cond_16

    const v0, 0xffdc

    if-gt p0, v0, :cond_16

    :cond_14
    const/4 v0, 0x1

    goto :goto_17

    :cond_16
    const/4 v0, 0x0

    :goto_17
    return v0
.end method

.method public static zzx(Ljava/lang/String;)I
    .registers 6

    const/4 v3, 0x0

    const-string v0, "UTF-8"

    :try_start_3
    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_6
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_3 .. :try_end_6} :catch_8

    move-result-object v3

    goto :goto_d

    :catch_8
    move-exception v4

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    :goto_d
    array-length v0, v3

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v3, v1, v0, v2}, Lcom/google/android/gms/internal/zznd;->zza([BIII)I

    move-result v0

    return v0
.end method

.method public static zzy(Ljava/lang/String;)[Ljava/lang/String;
    .registers 11

    if-nez p0, :cond_4

    const/4 v0, 0x0

    return-object v0

    :cond_4
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    :goto_14
    if-ge v5, v4, :cond_64

    invoke-static {v3, v5}, Ljava/lang/Character;->codePointAt([CI)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Character;->charCount(I)I

    move-result v9

    invoke-static {v8}, Lcom/google/android/gms/internal/zzbi;->zzi(I)Z

    move-result v0

    if-eqz v0, :cond_3a

    if-eqz v7, :cond_30

    new-instance v0, Ljava/lang/String;

    sub-int v1, v5, v6

    invoke-direct {v0, v3, v6, v1}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_30
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3, v5, v9}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v7, 0x0

    goto :goto_61

    :cond_3a
    invoke-static {v8}, Ljava/lang/Character;->isLetterOrDigit(I)Z

    move-result v0

    if-nez v0, :cond_4f

    invoke-static {v8}, Ljava/lang/Character;->getType(I)I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_4f

    invoke-static {v8}, Ljava/lang/Character;->getType(I)I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_54

    :cond_4f
    if-nez v7, :cond_52

    move v6, v5

    :cond_52
    const/4 v7, 0x1

    goto :goto_61

    :cond_54
    if-eqz v7, :cond_61

    new-instance v0, Ljava/lang/String;

    sub-int v1, v5, v6

    invoke-direct {v0, v3, v6, v1}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v7, 0x0

    :cond_61
    :goto_61
    add-int/2addr v5, v9

    goto/16 :goto_14

    :cond_64
    if-eqz v7, :cond_70

    new-instance v0, Ljava/lang/String;

    sub-int v1, v5, v6

    invoke-direct {v0, v3, v6, v1}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_70
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v8, v0, [Ljava/lang/String;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

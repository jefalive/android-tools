.class public final enum Lbr/com/itau/sdk/android/core/RouterEnvironment;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/sdk/android/core/RouterEnvironment;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lbr/com/itau/sdk/android/core/RouterEnvironment;

.field public static final enum INTERNET:Lbr/com/itau/sdk/android/core/RouterEnvironment;

.field public static final enum MOBILE:Lbr/com/itau/sdk/android/core/RouterEnvironment;

.field public static final enum PILOTO:Lbr/com/itau/sdk/android/core/RouterEnvironment;

.field private static final a:[B

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 10
    const/16 v0, 0x15

    new-array v0, v0, [B

    fill-array-data v0, :array_66

    sput-object v0, Lbr/com/itau/sdk/android/core/RouterEnvironment;->a:[B

    const/16 v0, 0xf7

    sput v0, Lbr/com/itau/sdk/android/core/RouterEnvironment;->b:I

    new-instance v0, Lbr/com/itau/sdk/android/core/RouterEnvironment;

    sget-object v1, Lbr/com/itau/sdk/android/core/RouterEnvironment;->a:[B

    const/16 v2, 0x12

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/RouterEnvironment;->a:[B

    const/16 v3, 0x12

    aget-byte v2, v2, v3

    const/4 v3, 0x0

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/RouterEnvironment;->a(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/RouterEnvironment;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/RouterEnvironment;->PILOTO:Lbr/com/itau/sdk/android/core/RouterEnvironment;

    .line 15
    new-instance v0, Lbr/com/itau/sdk/android/core/RouterEnvironment;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/RouterEnvironment;->a(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/RouterEnvironment;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/RouterEnvironment;->INTERNET:Lbr/com/itau/sdk/android/core/RouterEnvironment;

    .line 20
    new-instance v0, Lbr/com/itau/sdk/android/core/RouterEnvironment;

    sget-object v1, Lbr/com/itau/sdk/android/core/RouterEnvironment;->a:[B

    const/4 v2, 0x0

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/RouterEnvironment;->a:[B

    const/16 v3, 0x8

    aget-byte v2, v2, v3

    neg-int v2, v2

    const/4 v3, 0x0

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/RouterEnvironment;->a(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/RouterEnvironment;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/RouterEnvironment;->MOBILE:Lbr/com/itau/sdk/android/core/RouterEnvironment;

    .line 6
    const/4 v0, 0x3

    new-array v0, v0, [Lbr/com/itau/sdk/android/core/RouterEnvironment;

    sget-object v1, Lbr/com/itau/sdk/android/core/RouterEnvironment;->PILOTO:Lbr/com/itau/sdk/android/core/RouterEnvironment;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/RouterEnvironment;->INTERNET:Lbr/com/itau/sdk/android/core/RouterEnvironment;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/RouterEnvironment;->MOBILE:Lbr/com/itau/sdk/android/core/RouterEnvironment;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/sdk/android/core/RouterEnvironment;->$VALUES:[Lbr/com/itau/sdk/android/core/RouterEnvironment;

    return-void

    nop

    :array_66
    .array-data 1
        0xdt
        -0x10t
        0x79t
        0x8t
        0x5t
        0x6t
        -0xft
        0xdt
        -0x4t
        -0x9t
        0xft
        -0x7t
        0x3t
        0x3t
        0x5t
        -0x5t
        0x2t
        -0xdt
        0x7t
        0x3t
        -0x7t
    .end array-data
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    sget-object v4, Lbr/com/itau/sdk/android/core/RouterEnvironment;->a:[B

    add-int/lit8 p0, p0, 0x4

    mul-int/lit8 p1, p1, 0x2

    add-int/lit8 p1, p1, 0x6

    const/4 v5, 0x0

    add-int/lit8 p2, p2, 0x49

    new-instance v0, Ljava/lang/String;

    new-array v1, p1, [B

    add-int/lit8 p1, p1, -0x1

    if-nez v4, :cond_19

    move v2, p0

    move v3, p2

    :goto_15
    add-int p2, v2, v3

    add-int/lit8 p0, p0, 0x1

    :cond_19
    int-to-byte v2, p2

    aput-byte v2, v1, v5

    if-ne v5, p1, :cond_23

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_23
    move v2, p2

    add-int/lit8 v5, v5, 0x1

    aget-byte v3, v4, p0

    goto :goto_15
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/RouterEnvironment;
    .registers 2

    .line 6
    const-class v0, Lbr/com/itau/sdk/android/core/RouterEnvironment;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/RouterEnvironment;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/sdk/android/core/RouterEnvironment;
    .registers 1

    .line 6
    sget-object v0, Lbr/com/itau/sdk/android/core/RouterEnvironment;->$VALUES:[Lbr/com/itau/sdk/android/core/RouterEnvironment;

    invoke-virtual {v0}, [Lbr/com/itau/sdk/android/core/RouterEnvironment;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/sdk/android/core/RouterEnvironment;

    return-object v0
.end method

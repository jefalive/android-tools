.class Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity$1;
.super Ljava/lang/Object;
.source "GiroDataActivity.java"

# interfaces
.implements Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog$OnDateSetListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->aoClicarNoBotaoData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;

    .line 106
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity$1;->this$0:Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDateSet(Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;III)V
    .registers 8
    .param p1, "datePickerDialog"    # Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;
    .param p2, "i"    # I
    .param p3, "i1"    # I
    .param p4, "i2"    # I

    .line 110
    # invokes: Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->convertaDatePickerParaString(Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;)Ljava/lang/String;
    invoke-static {p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->access$000(Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;)Ljava/lang/String;

    move-result-object v2

    .line 111
    .local v2, "data":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity$1;->this$0:Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->campoData:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity$1;->this$0:Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 113
    invoke-static {p1}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDatePickerParaStringComBarra(Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;)Ljava/lang/String;

    move-result-object v1

    .line 112
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setDataPrimeiroPagamento(Ljava/lang/String;)V

    .line 114
    return-void
.end method

.class public Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;


# instance fields
.field private dialog:Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleError(Lbr/com/itau/sdk/android/core/exception/BackendException;)Ljava/lang/Throwable;
    .registers 3

    .line 13
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;->dialog:Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;

    invoke-virtual {v0, p1}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->handleError(Lbr/com/itau/sdk/android/core/exception/BackendException;)V

    .line 14
    return-object p1
.end method

.method public setDialog(Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;)V
    .registers 2

    .line 18
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;->dialog:Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;

    .line 19
    return-void
.end method

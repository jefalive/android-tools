.class Lcom/nineoldandroids/animation/KeyframeSet;
.super Ljava/lang/Object;
.source "KeyframeSet.java"


# instance fields
.field mEvaluator:Lcom/nineoldandroids/animation/TypeEvaluator;

.field mFirstKeyframe:Lcom/nineoldandroids/animation/Keyframe;

.field mInterpolator:Landroid/view/animation/Interpolator;

.field mKeyframes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/nineoldandroids/animation/Keyframe;>;"
        }
    .end annotation
.end field

.field mLastKeyframe:Lcom/nineoldandroids/animation/Keyframe;

.field mNumKeyframes:I


# direct methods
.method public varargs constructor <init>([Lcom/nineoldandroids/animation/Keyframe;)V
    .registers 5
    .param p1, "keyframes"    # [Lcom/nineoldandroids/animation/Keyframe;

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    array-length v0, p1

    iput v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mNumKeyframes:I

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    .line 47
    iget-object v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 48
    iget-object v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nineoldandroids/animation/Keyframe;

    iput-object v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mFirstKeyframe:Lcom/nineoldandroids/animation/Keyframe;

    .line 49
    iget-object v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    iget v1, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mNumKeyframes:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nineoldandroids/animation/Keyframe;

    iput-object v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mLastKeyframe:Lcom/nineoldandroids/animation/Keyframe;

    .line 50
    iget-object v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mLastKeyframe:Lcom/nineoldandroids/animation/Keyframe;

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/Keyframe;->getInterpolator()Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 51
    return-void
.end method

.method public static varargs ofFloat([F)Lcom/nineoldandroids/animation/KeyframeSet;
    .registers 6
    .param p0, "values"    # [F

    .line 69
    array-length v2, p0

    .line 70
    .local v2, "numKeyframes":I
    const/4 v0, 0x2

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    new-array v3, v0, [Lcom/nineoldandroids/animation/Keyframe$FloatKeyframe;

    .line 71
    .local v3, "keyframes":[Lcom/nineoldandroids/animation/Keyframe$FloatKeyframe;
    const/4 v0, 0x1

    if-ne v2, v0, :cond_24

    .line 72
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/nineoldandroids/animation/Keyframe;->ofFloat(F)Lcom/nineoldandroids/animation/Keyframe;

    move-result-object v0

    check-cast v0, Lcom/nineoldandroids/animation/Keyframe$FloatKeyframe;

    const/4 v1, 0x0

    aput-object v0, v3, v1

    .line 73
    const/4 v0, 0x0

    aget v0, p0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1, v0}, Lcom/nineoldandroids/animation/Keyframe;->ofFloat(FF)Lcom/nineoldandroids/animation/Keyframe;

    move-result-object v0

    check-cast v0, Lcom/nineoldandroids/animation/Keyframe$FloatKeyframe;

    const/4 v1, 0x1

    aput-object v0, v3, v1

    goto :goto_46

    .line 75
    :cond_24
    const/4 v0, 0x0

    aget v0, p0, v0

    const/4 v1, 0x0

    invoke-static {v1, v0}, Lcom/nineoldandroids/animation/Keyframe;->ofFloat(FF)Lcom/nineoldandroids/animation/Keyframe;

    move-result-object v0

    check-cast v0, Lcom/nineoldandroids/animation/Keyframe$FloatKeyframe;

    const/4 v1, 0x0

    aput-object v0, v3, v1

    .line 76
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_32
    if-ge v4, v2, :cond_46

    .line 77
    int-to-float v0, v4

    add-int/lit8 v1, v2, -0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    aget v1, p0, v4

    invoke-static {v0, v1}, Lcom/nineoldandroids/animation/Keyframe;->ofFloat(FF)Lcom/nineoldandroids/animation/Keyframe;

    move-result-object v0

    check-cast v0, Lcom/nineoldandroids/animation/Keyframe$FloatKeyframe;

    aput-object v0, v3, v4

    .line 76
    add-int/lit8 v4, v4, 0x1

    goto :goto_32

    .line 80
    .end local v4    # "i":I
    :cond_46
    :goto_46
    new-instance v0, Lcom/nineoldandroids/animation/FloatKeyframeSet;

    invoke-direct {v0, v3}, Lcom/nineoldandroids/animation/FloatKeyframeSet;-><init>([Lcom/nineoldandroids/animation/Keyframe$FloatKeyframe;)V

    return-object v0
.end method

.method public static varargs ofInt([I)Lcom/nineoldandroids/animation/KeyframeSet;
    .registers 6
    .param p0, "values"    # [I

    .line 54
    array-length v2, p0

    .line 55
    .local v2, "numKeyframes":I
    const/4 v0, 0x2

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    new-array v3, v0, [Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;

    .line 56
    .local v3, "keyframes":[Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;
    const/4 v0, 0x1

    if-ne v2, v0, :cond_24

    .line 57
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/nineoldandroids/animation/Keyframe;->ofInt(F)Lcom/nineoldandroids/animation/Keyframe;

    move-result-object v0

    check-cast v0, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;

    const/4 v1, 0x0

    aput-object v0, v3, v1

    .line 58
    const/4 v0, 0x0

    aget v0, p0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1, v0}, Lcom/nineoldandroids/animation/Keyframe;->ofInt(FI)Lcom/nineoldandroids/animation/Keyframe;

    move-result-object v0

    check-cast v0, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;

    const/4 v1, 0x1

    aput-object v0, v3, v1

    goto :goto_46

    .line 60
    :cond_24
    const/4 v0, 0x0

    aget v0, p0, v0

    const/4 v1, 0x0

    invoke-static {v1, v0}, Lcom/nineoldandroids/animation/Keyframe;->ofInt(FI)Lcom/nineoldandroids/animation/Keyframe;

    move-result-object v0

    check-cast v0, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;

    const/4 v1, 0x0

    aput-object v0, v3, v1

    .line 61
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_32
    if-ge v4, v2, :cond_46

    .line 62
    int-to-float v0, v4

    add-int/lit8 v1, v2, -0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    aget v1, p0, v4

    invoke-static {v0, v1}, Lcom/nineoldandroids/animation/Keyframe;->ofInt(FI)Lcom/nineoldandroids/animation/Keyframe;

    move-result-object v0

    check-cast v0, Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;

    aput-object v0, v3, v4

    .line 61
    add-int/lit8 v4, v4, 0x1

    goto :goto_32

    .line 65
    .end local v4    # "i":I
    :cond_46
    :goto_46
    new-instance v0, Lcom/nineoldandroids/animation/IntKeyframeSet;

    invoke-direct {v0, v3}, Lcom/nineoldandroids/animation/IntKeyframeSet;-><init>([Lcom/nineoldandroids/animation/Keyframe$IntKeyframe;)V

    return-object v0
.end method

.method public static varargs ofObject([Ljava/lang/Object;)Lcom/nineoldandroids/animation/KeyframeSet;
    .registers 6
    .param p0, "values"    # [Ljava/lang/Object;

    .line 116
    array-length v2, p0

    .line 117
    .local v2, "numKeyframes":I
    const/4 v0, 0x2

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    new-array v3, v0, [Lcom/nineoldandroids/animation/Keyframe$ObjectKeyframe;

    .line 118
    .local v3, "keyframes":[Lcom/nineoldandroids/animation/Keyframe$ObjectKeyframe;
    const/4 v0, 0x1

    if-ne v2, v0, :cond_24

    .line 119
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/nineoldandroids/animation/Keyframe;->ofObject(F)Lcom/nineoldandroids/animation/Keyframe;

    move-result-object v0

    check-cast v0, Lcom/nineoldandroids/animation/Keyframe$ObjectKeyframe;

    const/4 v1, 0x0

    aput-object v0, v3, v1

    .line 120
    const/4 v0, 0x0

    aget-object v0, p0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1, v0}, Lcom/nineoldandroids/animation/Keyframe;->ofObject(FLjava/lang/Object;)Lcom/nineoldandroids/animation/Keyframe;

    move-result-object v0

    check-cast v0, Lcom/nineoldandroids/animation/Keyframe$ObjectKeyframe;

    const/4 v1, 0x1

    aput-object v0, v3, v1

    goto :goto_46

    .line 122
    :cond_24
    const/4 v0, 0x0

    aget-object v0, p0, v0

    const/4 v1, 0x0

    invoke-static {v1, v0}, Lcom/nineoldandroids/animation/Keyframe;->ofObject(FLjava/lang/Object;)Lcom/nineoldandroids/animation/Keyframe;

    move-result-object v0

    check-cast v0, Lcom/nineoldandroids/animation/Keyframe$ObjectKeyframe;

    const/4 v1, 0x0

    aput-object v0, v3, v1

    .line 123
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_32
    if-ge v4, v2, :cond_46

    .line 124
    int-to-float v0, v4

    add-int/lit8 v1, v2, -0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    aget-object v1, p0, v4

    invoke-static {v0, v1}, Lcom/nineoldandroids/animation/Keyframe;->ofObject(FLjava/lang/Object;)Lcom/nineoldandroids/animation/Keyframe;

    move-result-object v0

    check-cast v0, Lcom/nineoldandroids/animation/Keyframe$ObjectKeyframe;

    aput-object v0, v3, v4

    .line 123
    add-int/lit8 v4, v4, 0x1

    goto :goto_32

    .line 127
    .end local v4    # "i":I
    :cond_46
    :goto_46
    new-instance v0, Lcom/nineoldandroids/animation/KeyframeSet;

    invoke-direct {v0, v3}, Lcom/nineoldandroids/animation/KeyframeSet;-><init>([Lcom/nineoldandroids/animation/Keyframe;)V

    return-object v0
.end method


# virtual methods
.method public clone()Lcom/nineoldandroids/animation/KeyframeSet;
    .registers 6

    .line 144
    iget-object v1, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    .line 145
    .local v1, "keyframes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/nineoldandroids/animation/Keyframe;>;"
    iget-object v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 146
    .local v2, "numKeyframes":I
    new-array v3, v2, [Lcom/nineoldandroids/animation/Keyframe;

    .line 147
    .local v3, "newKeyframes":[Lcom/nineoldandroids/animation/Keyframe;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_b
    if-ge v4, v2, :cond_1c

    .line 148
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/nineoldandroids/animation/Keyframe;

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/Keyframe;->clone()Lcom/nineoldandroids/animation/Keyframe;

    move-result-object v0

    aput-object v0, v3, v4

    .line 147
    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    .line 150
    .end local v4    # "i":I
    :cond_1c
    new-instance v4, Lcom/nineoldandroids/animation/KeyframeSet;

    invoke-direct {v4, v3}, Lcom/nineoldandroids/animation/KeyframeSet;-><init>([Lcom/nineoldandroids/animation/Keyframe;)V

    .line 151
    .local v4, "newSet":Lcom/nineoldandroids/animation/KeyframeSet;
    return-object v4
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 33
    invoke-virtual {p0}, Lcom/nineoldandroids/animation/KeyframeSet;->clone()Lcom/nineoldandroids/animation/KeyframeSet;

    move-result-object v0

    return-object v0
.end method

.method public getValue(F)Ljava/lang/Object;
    .registers 11
    .param p1, "fraction"    # F

    .line 169
    iget v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mNumKeyframes:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_22

    .line 170
    iget-object v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mInterpolator:Landroid/view/animation/Interpolator;

    if-eqz v0, :cond_f

    .line 171
    iget-object v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v0, p1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result p1

    .line 173
    :cond_f
    iget-object v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mEvaluator:Lcom/nineoldandroids/animation/TypeEvaluator;

    iget-object v1, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mFirstKeyframe:Lcom/nineoldandroids/animation/Keyframe;

    invoke-virtual {v1}, Lcom/nineoldandroids/animation/Keyframe;->getValue()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mLastKeyframe:Lcom/nineoldandroids/animation/Keyframe;

    invoke-virtual {v2}, Lcom/nineoldandroids/animation/Keyframe;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, p1, v1, v2}, Lcom/nineoldandroids/animation/TypeEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 176
    :cond_22
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_5b

    .line 177
    iget-object v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/nineoldandroids/animation/Keyframe;

    .line 178
    .local v3, "nextKeyframe":Lcom/nineoldandroids/animation/Keyframe;
    invoke-virtual {v3}, Lcom/nineoldandroids/animation/Keyframe;->getInterpolator()Landroid/view/animation/Interpolator;

    move-result-object v4

    .line 179
    .local v4, "interpolator":Landroid/view/animation/Interpolator;
    if-eqz v4, :cond_3b

    .line 180
    invoke-interface {v4, p1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result p1

    .line 182
    :cond_3b
    iget-object v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mFirstKeyframe:Lcom/nineoldandroids/animation/Keyframe;

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/Keyframe;->getFraction()F

    move-result v5

    .line 183
    .local v5, "prevFraction":F
    sub-float v0, p1, v5

    invoke-virtual {v3}, Lcom/nineoldandroids/animation/Keyframe;->getFraction()F

    move-result v1

    sub-float/2addr v1, v5

    div-float v6, v0, v1

    .line 185
    .local v6, "intervalFraction":F
    iget-object v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mEvaluator:Lcom/nineoldandroids/animation/TypeEvaluator;

    iget-object v1, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mFirstKeyframe:Lcom/nineoldandroids/animation/Keyframe;

    invoke-virtual {v1}, Lcom/nineoldandroids/animation/Keyframe;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v3}, Lcom/nineoldandroids/animation/Keyframe;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v6, v1, v2}, Lcom/nineoldandroids/animation/TypeEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 187
    .end local v3    # "nextKeyframe":Lcom/nineoldandroids/animation/Keyframe;
    .end local v4    # "interpolator":Landroid/view/animation/Interpolator;
    .end local v5    # "prevFraction":F
    .end local v6    # "intervalFraction":F
    :cond_5b
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_9a

    .line 188
    iget-object v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    iget v1, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mNumKeyframes:I

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/nineoldandroids/animation/Keyframe;

    .line 189
    .local v3, "prevKeyframe":Lcom/nineoldandroids/animation/Keyframe;
    iget-object v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mLastKeyframe:Lcom/nineoldandroids/animation/Keyframe;

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/Keyframe;->getInterpolator()Landroid/view/animation/Interpolator;

    move-result-object v4

    .line 190
    .local v4, "interpolator":Landroid/view/animation/Interpolator;
    if-eqz v4, :cond_7a

    .line 191
    invoke-interface {v4, p1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result p1

    .line 193
    :cond_7a
    invoke-virtual {v3}, Lcom/nineoldandroids/animation/Keyframe;->getFraction()F

    move-result v5

    .line 194
    .local v5, "prevFraction":F
    sub-float v0, p1, v5

    iget-object v1, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mLastKeyframe:Lcom/nineoldandroids/animation/Keyframe;

    invoke-virtual {v1}, Lcom/nineoldandroids/animation/Keyframe;->getFraction()F

    move-result v1

    sub-float/2addr v1, v5

    div-float v6, v0, v1

    .line 196
    .local v6, "intervalFraction":F
    iget-object v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mEvaluator:Lcom/nineoldandroids/animation/TypeEvaluator;

    invoke-virtual {v3}, Lcom/nineoldandroids/animation/Keyframe;->getValue()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mLastKeyframe:Lcom/nineoldandroids/animation/Keyframe;

    invoke-virtual {v2}, Lcom/nineoldandroids/animation/Keyframe;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v6, v1, v2}, Lcom/nineoldandroids/animation/TypeEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 199
    .end local v3    # "prevKeyframe":Lcom/nineoldandroids/animation/Keyframe;
    .end local v4    # "interpolator":Landroid/view/animation/Interpolator;
    .end local v5    # "prevFraction":F
    .end local v6    # "intervalFraction":F
    :cond_9a
    iget-object v3, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mFirstKeyframe:Lcom/nineoldandroids/animation/Keyframe;

    .line 200
    .local v3, "prevKeyframe":Lcom/nineoldandroids/animation/Keyframe;
    const/4 v4, 0x1

    .local v4, "i":I
    :goto_9d
    iget v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mNumKeyframes:I

    if-ge v4, v0, :cond_dc

    .line 201
    iget-object v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/nineoldandroids/animation/Keyframe;

    .line 202
    .local v5, "nextKeyframe":Lcom/nineoldandroids/animation/Keyframe;
    invoke-virtual {v5}, Lcom/nineoldandroids/animation/Keyframe;->getFraction()F

    move-result v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_d8

    .line 203
    invoke-virtual {v5}, Lcom/nineoldandroids/animation/Keyframe;->getInterpolator()Landroid/view/animation/Interpolator;

    move-result-object v6

    .line 204
    .local v6, "interpolator":Landroid/view/animation/Interpolator;
    if-eqz v6, :cond_bc

    .line 205
    invoke-interface {v6, p1}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result p1

    .line 207
    :cond_bc
    invoke-virtual {v3}, Lcom/nineoldandroids/animation/Keyframe;->getFraction()F

    move-result v7

    .line 208
    .local v7, "prevFraction":F
    sub-float v0, p1, v7

    invoke-virtual {v5}, Lcom/nineoldandroids/animation/Keyframe;->getFraction()F

    move-result v1

    sub-float/2addr v1, v7

    div-float v8, v0, v1

    .line 210
    .local v8, "intervalFraction":F
    iget-object v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mEvaluator:Lcom/nineoldandroids/animation/TypeEvaluator;

    invoke-virtual {v3}, Lcom/nineoldandroids/animation/Keyframe;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v5}, Lcom/nineoldandroids/animation/Keyframe;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v8, v1, v2}, Lcom/nineoldandroids/animation/TypeEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 213
    .end local v6    # "interpolator":Landroid/view/animation/Interpolator;
    .end local v7    # "prevFraction":F
    .end local v8    # "intervalFraction":F
    :cond_d8
    move-object v3, v5

    .line 200
    .end local v5    # "nextKeyframe":Lcom/nineoldandroids/animation/Keyframe;
    add-int/lit8 v4, v4, 0x1

    goto :goto_9d

    .line 216
    .end local v4    # "i":I
    :cond_dc
    iget-object v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mLastKeyframe:Lcom/nineoldandroids/animation/Keyframe;

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/Keyframe;->getValue()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public setEvaluator(Lcom/nineoldandroids/animation/TypeEvaluator;)V
    .registers 2
    .param p1, "evaluator"    # Lcom/nineoldandroids/animation/TypeEvaluator;

    .line 139
    iput-object p1, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mEvaluator:Lcom/nineoldandroids/animation/TypeEvaluator;

    .line 140
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .line 221
    const-string v2, " "

    .line 222
    .local v2, "returnVal":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_3
    iget v0, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mNumKeyframes:I

    if-ge v3, v0, :cond_2d

    .line 223
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/nineoldandroids/animation/KeyframeSet;->mKeyframes:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/nineoldandroids/animation/Keyframe;

    invoke-virtual {v1}, Lcom/nineoldandroids/animation/Keyframe;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 222
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 225
    .end local v3    # "i":I
    :cond_2d
    return-object v2
.end method

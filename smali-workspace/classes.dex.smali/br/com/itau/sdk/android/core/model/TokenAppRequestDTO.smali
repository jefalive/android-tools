.class public final Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final deviceId:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id_mobile"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 2

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;->deviceId:Ljava/lang/String;

    .line 15
    return-void
.end method


# virtual methods
.method public getDeviceId()Ljava/lang/String;
    .registers 2

    .line 18
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/TokenAppRequestDTO;->deviceId:Ljava/lang/String;

    return-object v0
.end method

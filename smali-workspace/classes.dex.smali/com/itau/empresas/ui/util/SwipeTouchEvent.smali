.class public Lcom/itau/empresas/ui/util/SwipeTouchEvent;
.super Ljava/lang/Object;
.source "SwipeTouchEvent.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/util/SwipeTouchEvent$Listener;
    }
.end annotation


# instance fields
.field private listener:Lcom/itau/empresas/ui/util/SwipeTouchEvent$Listener;

.field private x1:F

.field private x2:F


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .line 18
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    sparse-switch v0, :sswitch_data_46

    goto :goto_43

    .line 20
    :sswitch_8
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/util/SwipeTouchEvent;->x1:F

    .line 21
    goto :goto_43

    .line 23
    :sswitch_f
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/util/SwipeTouchEvent;->x2:F

    .line 24
    iget v0, p0, Lcom/itau/empresas/ui/util/SwipeTouchEvent;->x2:F

    iget v1, p0, Lcom/itau/empresas/ui/util/SwipeTouchEvent;->x1:F

    sub-float v2, v0, v1

    .line 26
    .local v2, "deltaX":F
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x43160000    # 150.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_41

    .line 27
    iget v0, p0, Lcom/itau/empresas/ui/util/SwipeTouchEvent;->x2:F

    iget v1, p0, Lcom/itau/empresas/ui/util/SwipeTouchEvent;->x1:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_37

    .line 28
    iget-object v0, p0, Lcom/itau/empresas/ui/util/SwipeTouchEvent;->listener:Lcom/itau/empresas/ui/util/SwipeTouchEvent$Listener;

    if-eqz v0, :cond_43

    .line 29
    iget-object v0, p0, Lcom/itau/empresas/ui/util/SwipeTouchEvent;->listener:Lcom/itau/empresas/ui/util/SwipeTouchEvent$Listener;

    invoke-interface {v0}, Lcom/itau/empresas/ui/util/SwipeTouchEvent$Listener;->onSwipeRight()V

    goto :goto_43

    .line 32
    :cond_37
    iget-object v0, p0, Lcom/itau/empresas/ui/util/SwipeTouchEvent;->listener:Lcom/itau/empresas/ui/util/SwipeTouchEvent$Listener;

    if-eqz v0, :cond_43

    .line 33
    iget-object v0, p0, Lcom/itau/empresas/ui/util/SwipeTouchEvent;->listener:Lcom/itau/empresas/ui/util/SwipeTouchEvent$Listener;

    invoke-interface {v0}, Lcom/itau/empresas/ui/util/SwipeTouchEvent$Listener;->onSwipeLeft()V

    goto :goto_43

    .line 38
    :cond_41
    const/4 v0, 0x0

    return v0

    .line 43
    .end local v2    # "deltaX":F
    :cond_43
    :goto_43
    const/4 v0, 0x1

    return v0

    nop

    :sswitch_data_46
    .sparse-switch
        0x0 -> :sswitch_8
        0x1 -> :sswitch_f
    .end sparse-switch
.end method

.method public setListener(Lcom/itau/empresas/ui/util/SwipeTouchEvent$Listener;)V
    .registers 2
    .param p1, "listener"    # Lcom/itau/empresas/ui/util/SwipeTouchEvent$Listener;

    .line 13
    iput-object p1, p0, Lcom/itau/empresas/ui/util/SwipeTouchEvent;->listener:Lcom/itau/empresas/ui/util/SwipeTouchEvent$Listener;

    .line 14
    return-void
.end method

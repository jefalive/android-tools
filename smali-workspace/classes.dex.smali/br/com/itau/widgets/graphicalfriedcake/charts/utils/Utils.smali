.class public abstract Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field private static final BRAZIL:Ljava/util/Locale;

.field private static final POW_10:[I

.field private static final REAL:Ljava/text/DecimalFormatSymbols;

.field private static mMaximumFlingVelocity:I

.field private static mMetrics:Landroid/util/DisplayMetrics;

.field private static mMinimumFlingVelocity:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 35
    const/16 v0, 0x32

    sput v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->mMinimumFlingVelocity:I

    .line 36
    const/16 v0, 0x1f40

    sput v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->mMaximumFlingVelocity:I

    .line 38
    new-instance v0, Ljava/util/Locale;

    const-string v1, "pt"

    const-string v2, "BR"

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->BRAZIL:Ljava/util/Locale;

    .line 39
    new-instance v0, Ljava/text/DecimalFormatSymbols;

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->BRAZIL:Ljava/util/Locale;

    invoke-direct {v0, v1}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->REAL:Ljava/text/DecimalFormatSymbols;

    .line 254
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_26

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->POW_10:[I

    return-void

    :array_26
    .array-data 4
        0x1
        0xa
        0x64
        0x3e8
        0x2710
        0x186a0
        0xf4240
        0x989680
        0x5f5e100
        0x3b9aca00
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static calcTextHeight(Landroid/graphics/Paint;Ljava/lang/String;)I
    .registers 5
    .param p0, "paint"    # Landroid/graphics/Paint;
    .param p1, "demoText"    # Ljava/lang/String;

    .line 172
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 173
    .local v2, "r":Landroid/graphics/Rect;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0, v2}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 174
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v0

    return v0
.end method

.method public static calcTextSize(Landroid/graphics/Paint;Ljava/lang/String;)Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;
    .registers 6
    .param p0, "paint"    # Landroid/graphics/Paint;
    .param p1, "demoText"    # Ljava/lang/String;

    .line 197
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 198
    .local v3, "r":Landroid/graphics/Rect;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 199
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;-><init>(FF)V

    return-object v0
.end method

.method public static calcTextWidth(Landroid/graphics/Paint;Ljava/lang/String;)I
    .registers 3
    .param p0, "paint"    # Landroid/graphics/Paint;
    .param p1, "demoText"    # Ljava/lang/String;

    .line 159
    invoke-virtual {p0, p1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public static convertDpToPixel(F)F
    .registers 5
    .param p0, "dp"    # F

    .line 113
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->mMetrics:Landroid/util/DisplayMetrics;

    if-nez v0, :cond_c

    .line 115
    const-string v0, "MPChartLib-Utils"

    const-string v1, "Utils NOT INITIALIZED. You need to call Utils.init(...) at least once before calling Utils.convertDpToPixel(...). Otherwise conversion does not take place."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    return p0

    .line 122
    :cond_c
    sget-object v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->mMetrics:Landroid/util/DisplayMetrics;

    .line 123
    .local v2, "metrics":Landroid/util/DisplayMetrics;
    iget v0, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v0, v0

    const/high16 v1, 0x43200000    # 160.0f

    div-float/2addr v0, v1

    mul-float v3, p0, v0

    .line 124
    .local v3, "px":F
    return v3
.end method

.method public static convertIntegers(Ljava/util/List;)[I
    .registers 4
    .param p0, "integers"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/Integer;>;)[I"
        }
    .end annotation

    .line 380
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v1, v0, [I

    .line 382
    .local v1, "ret":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_7
    array-length v0, v1

    if-ge v2, v0, :cond_19

    .line 383
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v1, v2

    .line 382
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 386
    .end local v2    # "i":I
    :cond_19
    return-object v1
.end method

.method public static convertPixelsToDp(F)F
    .registers 5
    .param p0, "px"    # F

    .line 136
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->mMetrics:Landroid/util/DisplayMetrics;

    if-nez v0, :cond_c

    .line 138
    const-string v0, "MPChartLib-Utils"

    const-string v1, "Utils NOT INITIALIZED. You need to call Utils.init(...) at least once before calling Utils.convertPixelsToDp(...). Otherwise conversion does not take place."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    return p0

    .line 145
    :cond_c
    sget-object v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->mMetrics:Landroid/util/DisplayMetrics;

    .line 146
    .local v2, "metrics":Landroid/util/DisplayMetrics;
    iget v0, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v0, v0

    const/high16 v1, 0x43200000    # 160.0f

    div-float/2addr v0, v1

    div-float v3, p0, v0

    .line 147
    .local v3, "dp":F
    return v3
.end method

.method public static convertStrings(Ljava/util/List;)[Ljava/lang/String;
    .registers 4
    .param p0, "strings"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/String;>;)[Ljava/lang/String;"
        }
    .end annotation

    .line 397
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v1, v0, [Ljava/lang/String;

    .line 399
    .local v1, "ret":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_7
    array-length v0, v1

    if-ge v2, v0, :cond_15

    .line 400
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v1, v2

    .line 399
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 403
    .end local v2    # "i":I
    :cond_15
    return-object v1
.end method

.method public static formatDecimal(DI)Ljava/lang/String;
    .registers 8
    .param p0, "number"    # D
    .param p2, "digits"    # I

    .line 89
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 90
    .local v2, "a":Ljava/lang/StringBuffer;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_6
    if-ge v3, p2, :cond_17

    .line 91
    if-nez v3, :cond_f

    .line 92
    const-string v0, "."

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 93
    :cond_f
    const-string v0, "0"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 90
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 96
    .end local v3    # "i":I
    :cond_17
    new-instance v3, Ljava/text/DecimalFormat;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "###,###,###,##0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 97
    .local v3, "nf":Ljava/text/DecimalFormat;
    invoke-virtual {v3, p0, p1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    .line 99
    .local v4, "formatted":Ljava/lang/String;
    return-object v4
.end method

.method public static formatNumber(FIZ)Ljava/lang/String;
    .registers 14
    .param p0, "number"    # F
    .param p1, "digitCount"    # I
    .param p2, "separateThousands"    # Z

    .line 268
    const/16 v0, 0x23

    new-array v2, v0, [C

    .line 270
    .local v2, "out":[C
    const/4 v3, 0x0

    .line 271
    .local v3, "neg":Z
    const/4 v0, 0x0

    cmpl-float v0, p0, v0

    if-nez v0, :cond_d

    .line 272
    const-string v0, "0"

    return-object v0

    .line 275
    :cond_d
    const/4 v4, 0x0

    .line 276
    .local v4, "zero":Z
    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p0, v0

    if-gez v0, :cond_1b

    const/high16 v0, -0x40800000    # -1.0f

    cmpl-float v0, p0, v0

    if-lez v0, :cond_1b

    .line 277
    const/4 v4, 0x1

    .line 280
    :cond_1b
    const/4 v0, 0x0

    cmpg-float v0, p0, v0

    if-gez v0, :cond_22

    .line 281
    const/4 v3, 0x1

    .line 282
    neg-float p0, p0

    .line 285
    :cond_22
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->POW_10:[I

    array-length v0, v0

    if-le p1, v0, :cond_2c

    .line 286
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->POW_10:[I

    array-length v0, v0

    add-int/lit8 p1, v0, -0x1

    .line 289
    :cond_2c
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->POW_10:[I

    aget v0, v0, p1

    int-to-float v0, v0

    mul-float/2addr p0, v0

    .line 290
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-long v5, v0

    .line 291
    .local v5, "lval":J
    array-length v0, v2

    add-int/lit8 v7, v0, -0x1

    .line 292
    .local v7, "ind":I
    const/4 v8, 0x0

    .line 293
    .local v8, "charCount":I
    const/4 v9, 0x0

    .line 295
    .local v9, "decimalPointAdded":Z
    :goto_3c
    const-wide/16 v0, 0x0

    cmp-long v0, v5, v0

    if-nez v0, :cond_46

    add-int/lit8 v0, p1, 0x1

    if-ge v8, v0, :cond_93

    .line 296
    :cond_46
    const-wide/16 v0, 0xa

    rem-long v0, v5, v0

    long-to-int v10, v0

    .line 297
    .local v10, "digit":I
    const-wide/16 v0, 0xa

    div-long/2addr v5, v0

    .line 298
    move v0, v7

    add-int/lit8 v7, v7, -0x1

    add-int/lit8 v1, v10, 0x30

    int-to-char v1, v1

    aput-char v1, v2, v0

    .line 299
    add-int/lit8 v8, v8, 0x1

    .line 302
    if-ne v8, p1, :cond_65

    .line 303
    move v0, v7

    add-int/lit8 v7, v7, -0x1

    const/16 v1, 0x2c

    aput-char v1, v2, v0

    .line 304
    add-int/lit8 v8, v8, 0x1

    .line 305
    const/4 v9, 0x1

    goto :goto_91

    .line 308
    :cond_65
    if-eqz p2, :cond_91

    const-wide/16 v0, 0x0

    cmp-long v0, v5, v0

    if-eqz v0, :cond_91

    if-le v8, p1, :cond_91

    .line 310
    if-eqz v9, :cond_81

    .line 312
    sub-int v0, v8, p1

    rem-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_91

    .line 313
    move v0, v7

    add-int/lit8 v7, v7, -0x1

    const/16 v1, 0x2e

    aput-char v1, v2, v0

    .line 314
    add-int/lit8 v8, v8, 0x1

    goto :goto_91

    .line 319
    :cond_81
    sub-int v0, v8, p1

    rem-int/lit8 v0, v0, 0x4

    const/4 v1, 0x3

    if-ne v0, v1, :cond_91

    .line 320
    move v0, v7

    add-int/lit8 v7, v7, -0x1

    const/16 v1, 0x2e

    aput-char v1, v2, v0

    .line 321
    add-int/lit8 v8, v8, 0x1

    .line 325
    .end local v10    # "digit":I
    :cond_91
    :goto_91
    goto/16 :goto_3c

    .line 328
    :cond_93
    if-eqz v4, :cond_9e

    .line 329
    move v0, v7

    add-int/lit8 v7, v7, -0x1

    const/16 v1, 0x30

    aput-char v1, v2, v0

    .line 330
    add-int/lit8 v8, v8, 0x1

    .line 334
    :cond_9e
    if-eqz v3, :cond_a9

    .line 335
    move v0, v7

    add-int/lit8 v7, v7, -0x1

    const/16 v1, 0x2d

    aput-char v1, v2, v0

    .line 336
    add-int/lit8 v8, v8, 0x1

    .line 339
    :cond_a9
    array-length v0, v2

    sub-int v10, v0, v8

    .line 342
    .local v10, "start":I
    array-length v0, v2

    sub-int/2addr v0, v10

    invoke-static {v2, v10, v0}, Ljava/lang/String;->valueOf([CII)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getClosestDataSetIndex(Ljava/util/List;FLbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;)I
    .registers 9
    .param p0, "valsAtIndex"    # Ljava/util/List;
    .param p1, "val"    # F
    .param p2, "axis"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;>;FLbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;)I"
        }
    .end annotation

    .line 433
    const v1, -0x7fffffff

    .line 434
    .local v1, "index":I
    const v2, 0x7f7fffff    # Float.MAX_VALUE

    .line 436
    .local v2, "distance":F
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_7
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_35

    .line 438
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;

    .line 440
    .local v4, "sel":Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;
    if-eqz p2, :cond_1e

    iget-object v0, v4, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;->dataSet:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getAxisDependency()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    move-result-object v0

    if-ne v0, p2, :cond_32

    .line 442
    :cond_1e
    iget v0, v4, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;->val:F

    sub-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 443
    .local v5, "cdistance":F
    cmpg-float v0, v5, v2

    if-gez v0, :cond_32

    .line 444
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;

    iget v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;->dataSetIndex:I

    .line 445
    move v2, v5

    .line 436
    .end local v4    # "sel":Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;
    .end local v5    # "cdistance":F
    :cond_32
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 450
    .end local v3    # "i":I
    :cond_35
    return v1
.end method

.method public static getDecimals(F)I
    .registers 4
    .param p0, "number"    # F

    .line 368
    float-to-double v0, p0

    invoke-static {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->roundToNextSignificant(D)F

    move-result v2

    .line 369
    .local v2, "i":F
    float-to-double v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->log10(D)D

    move-result-wide v0

    neg-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public static getLegendFormatDigits(FI)I
    .registers 6
    .param p0, "step"    # F
    .param p1, "bonus"    # I

    .line 233
    float-to-double v0, p0

    const-wide v2, 0x3ee4c305a3adef92L    # 9.9E-6

    cmpg-double v0, v0, v2

    if-gez v0, :cond_d

    .line 234
    add-int/lit8 v0, p1, 0x6

    return v0

    .line 235
    :cond_d
    float-to-double v0, p0

    const-wide v2, 0x3f19f3c70c996b76L    # 9.9E-5

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1a

    .line 236
    add-int/lit8 v0, p1, 0x5

    return v0

    .line 237
    :cond_1a
    float-to-double v0, p0

    const-wide v2, 0x3f50385c67dfe32aL    # 9.9E-4

    cmpg-double v0, v0, v2

    if-gez v0, :cond_27

    .line 238
    add-int/lit8 v0, p1, 0x4

    return v0

    .line 239
    :cond_27
    float-to-double v0, p0

    const-wide v2, 0x3f84467381d7dbf5L    # 0.0099

    cmpg-double v0, v0, v2

    if-gez v0, :cond_34

    .line 240
    add-int/lit8 v0, p1, 0x3

    return v0

    .line 241
    :cond_34
    float-to-double v0, p0

    const-wide v2, 0x3fb95810624dd2f2L    # 0.099

    cmpg-double v0, v0, v2

    if-gez v0, :cond_41

    .line 242
    add-int/lit8 v0, p1, 0x2

    return v0

    .line 243
    :cond_41
    float-to-double v0, p0

    const-wide v2, 0x3fefae147ae147aeL    # 0.99

    cmpg-double v0, v0, v2

    if-gez v0, :cond_4e

    .line 244
    add-int/lit8 v0, p1, 0x1

    return v0

    .line 246
    :cond_4e
    add-int/lit8 v0, p1, 0x0

    return v0
.end method

.method public static getLineHeight(Landroid/graphics/Paint;)F
    .registers 4
    .param p0, "paint"    # Landroid/graphics/Paint;

    .line 178
    invoke-virtual {p0}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v2

    .line 179
    .local v2, "metrics":Landroid/graphics/Paint$FontMetrics;
    iget v0, v2, Landroid/graphics/Paint$FontMetrics;->descent:F

    iget v1, v2, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float/2addr v0, v1

    return v0
.end method

.method public static getLineSpacing(Landroid/graphics/Paint;)F
    .registers 4
    .param p0, "paint"    # Landroid/graphics/Paint;

    .line 183
    invoke-virtual {p0}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v2

    .line 184
    .local v2, "metrics":Landroid/graphics/Paint$FontMetrics;
    iget v0, v2, Landroid/graphics/Paint$FontMetrics;->ascent:F

    iget v1, v2, Landroid/graphics/Paint$FontMetrics;->top:F

    sub-float/2addr v0, v1

    iget v1, v2, Landroid/graphics/Paint$FontMetrics;->bottom:F

    add-float/2addr v0, v1

    return v0
.end method

.method public static getMaximumFlingVelocity()I
    .registers 1

    .line 544
    sget v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->mMaximumFlingVelocity:I

    return v0
.end method

.method public static getMinimumDistance(Ljava/util/List;FLbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;)F
    .registers 8
    .param p0, "valsAtIndex"    # Ljava/util/List;
    .param p1, "val"    # F
    .param p2, "axis"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;>;FLbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;)F"
        }
    .end annotation

    .line 465
    const v1, 0x7f7fffff    # Float.MAX_VALUE

    .line 467
    .local v1, "distance":F
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_4
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_28

    .line 469
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;

    .line 471
    .local v3, "sel":Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;
    iget-object v0, v3, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;->dataSet:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getAxisDependency()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    move-result-object v0

    if-ne v0, p2, :cond_25

    .line 473
    iget v0, v3, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;->val:F

    sub-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 474
    .local v4, "cdistance":F
    cmpg-float v0, v4, v1

    if-gez v0, :cond_25

    .line 475
    move v1, v4

    .line 467
    .end local v3    # "sel":Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;
    .end local v4    # "cdistance":F
    :cond_25
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 480
    .end local v2    # "i":I
    :cond_28
    return v1
.end method

.method public static getMinimumFlingVelocity()I
    .registers 1

    .line 540
    sget v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->mMinimumFlingVelocity:I

    return v0
.end method

.method public static getNormalizedAngle(F)F
    .registers 2
    .param p0, "angle"    # F

    .line 551
    :goto_0
    const/4 v0, 0x0

    cmpg-float v0, p0, v0

    if-gez v0, :cond_9

    .line 552
    const/high16 v0, 0x43b40000    # 360.0f

    add-float/2addr p0, v0

    goto :goto_0

    .line 554
    :cond_9
    const/high16 v0, 0x43b40000    # 360.0f

    rem-float v0, p0, v0

    return v0
.end method

.method public static getPercentualFormatter()Ljava/text/DecimalFormat;
    .registers 3

    .line 558
    new-instance v2, Ljava/text/DecimalFormat;

    const-string v0, "##0.00"

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->REAL:Ljava/text/DecimalFormatSymbols;

    invoke-direct {v2, v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    .line 559
    .local v2, "df":Ljava/text/DecimalFormat;
    const-string v0, "%"

    invoke-virtual {v2, v0}, Ljava/text/DecimalFormat;->setPositiveSuffix(Ljava/lang/String;)V

    .line 560
    return-object v2
.end method

.method public static getPosition(Landroid/graphics/PointF;FF)Landroid/graphics/PointF;
    .registers 11
    .param p0, "center"    # Landroid/graphics/PointF;
    .param p1, "dist"    # F
    .param p2, "angle"    # F

    .line 494
    new-instance v7, Landroid/graphics/PointF;

    iget v0, p0, Landroid/graphics/PointF;->x:F

    float-to-double v0, v0

    float-to-double v2, p1

    float-to-double v4, p2

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    double-to-float v0, v0

    iget v1, p0, Landroid/graphics/PointF;->y:F

    float-to-double v1, v1

    float-to-double v3, p1

    float-to-double v5, p2

    .line 495
    invoke-static {v5, v6}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v5

    mul-double/2addr v3, v5

    add-double/2addr v1, v3

    double-to-float v1, v1

    invoke-direct {v7, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 496
    .local v7, "p":Landroid/graphics/PointF;
    return-object v7
.end method

.method public static init(Landroid/content/Context;)V
    .registers 4
    .param p0, "context"    # Landroid/content/Context;

    .line 47
    if-nez p0, :cond_f

    .line 49
    invoke-static {}, Landroid/view/ViewConfiguration;->getMinimumFlingVelocity()I

    move-result v0

    sput v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->mMinimumFlingVelocity:I

    .line 51
    invoke-static {}, Landroid/view/ViewConfiguration;->getMaximumFlingVelocity()I

    move-result v0

    sput v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->mMaximumFlingVelocity:I

    goto :goto_29

    .line 54
    :cond_f
    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    .line 55
    .local v1, "viewConfiguration":Landroid/view/ViewConfiguration;
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v0

    sput v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->mMinimumFlingVelocity:I

    .line 56
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    sput v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->mMaximumFlingVelocity:I

    .line 58
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 59
    .local v2, "res":Landroid/content/res/Resources;
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->mMetrics:Landroid/util/DisplayMetrics;

    .line 61
    .end local v1    # "viewConfiguration":Landroid/view/ViewConfiguration;
    .end local v2    # "res":Landroid/content/res/Resources;
    :goto_29
    return-void
.end method

.method public static init(Landroid/content/res/Resources;)V
    .registers 2
    .param p0, "res"    # Landroid/content/res/Resources;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 72
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->mMetrics:Landroid/util/DisplayMetrics;

    .line 75
    invoke-static {}, Landroid/view/ViewConfiguration;->getMinimumFlingVelocity()I

    move-result v0

    sput v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->mMinimumFlingVelocity:I

    .line 77
    invoke-static {}, Landroid/view/ViewConfiguration;->getMaximumFlingVelocity()I

    move-result v0

    sput v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->mMaximumFlingVelocity:I

    .line 78
    return-void
.end method

.method public static nextUp(D)D
    .registers 6
    .param p0, "d"    # D

    .line 414
    const-wide/high16 v0, 0x7ff0000000000000L    # Double.POSITIVE_INFINITY

    cmpl-double v0, p0, v0

    if-nez v0, :cond_7

    .line 415
    return-wide p0

    .line 417
    :cond_7
    const-wide/16 v0, 0x0

    add-double/2addr p0, v0

    .line 418
    invoke-static {p0, p1}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v2, p0, v2

    if-ltz v2, :cond_17

    const-wide/16 v2, 0x1

    goto :goto_19

    :cond_17
    const-wide/16 v2, -0x1

    :goto_19
    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public static postInvalidateOnAnimation(Landroid/view/View;)V
    .registers 3
    .param p0, "view"    # Landroid/view/View;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .line 533
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_a

    .line 534
    invoke-virtual {p0}, Landroid/view/View;->postInvalidateOnAnimation()V

    goto :goto_f

    .line 536
    :cond_a
    const-wide/16 v0, 0xa

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->postInvalidateDelayed(J)V

    .line 537
    :goto_f
    return-void
.end method

.method public static roundToNextSignificant(D)F
    .registers 11
    .param p0, "number"    # D

    .line 352
    const-wide/16 v0, 0x0

    cmpg-double v0, p0, v0

    if-gez v0, :cond_8

    neg-double v0, p0

    goto :goto_9

    :cond_8
    move-wide v0, p0

    :goto_9
    invoke-static {v0, v1}, Ljava/lang/Math;->log10(D)D

    move-result-wide v0

    double-to-float v0, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-float v4, v0

    .line 353
    .local v4, "d":F
    float-to-int v0, v4

    rsub-int/lit8 v5, v0, 0x1

    .line 354
    .local v5, "pw":I
    int-to-double v0, v5

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v6, v0

    .line 355
    .local v6, "magnitude":F
    float-to-double v0, v6

    mul-double/2addr v0, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v7

    .line 356
    .local v7, "shifted":J
    long-to-float v0, v7

    div-float/2addr v0, v6

    return v0
.end method

.method public static velocityTrackerPointerUpCleanUpIfNecessary(Landroid/view/MotionEvent;Landroid/view/VelocityTracker;)V
    .registers 14
    .param p0, "ev"    # Landroid/view/MotionEvent;
    .param p1, "tracker"    # Landroid/view/VelocityTracker;

    .line 504
    sget v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->mMaximumFlingVelocity:I

    int-to-float v0, v0

    const/16 v1, 0x3e8

    invoke-virtual {p1, v1, v0}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 505
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v2

    .line 506
    .local v2, "upIndex":I
    invoke-virtual {p0, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    .line 507
    .local v3, "id1":I
    invoke-virtual {p1, v3}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v4

    .line 508
    .local v4, "x1":F
    invoke-virtual {p1, v3}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v5

    .line 509
    .local v5, "y1":F
    const/4 v6, 0x0

    .local v6, "i":I
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    .local v7, "count":I
    :goto_1d
    if-ge v6, v7, :cond_40

    .line 510
    if-ne v6, v2, :cond_22

    .line 511
    goto :goto_3d

    .line 513
    :cond_22
    invoke-virtual {p0, v6}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v8

    .line 514
    .local v8, "id2":I
    invoke-virtual {p1, v8}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v0

    mul-float v9, v4, v0

    .line 515
    .local v9, "x":F
    invoke-virtual {p1, v8}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v0

    mul-float v10, v5, v0

    .line 517
    .local v10, "y":F
    add-float v11, v9, v10

    .line 518
    .local v11, "dot":F
    const/4 v0, 0x0

    cmpg-float v0, v11, v0

    if-gez v0, :cond_3d

    .line 519
    invoke-virtual {p1}, Landroid/view/VelocityTracker;->clear()V

    .line 520
    goto :goto_40

    .line 509
    .end local v8    # "id2":I
    .end local v9    # "x":F
    .end local v10    # "y":F
    .end local v11    # "dot":F
    :cond_3d
    :goto_3d
    add-int/lit8 v6, v6, 0x1

    goto :goto_1d

    .line 523
    .end local v6    # "i":I
    .end local v7    # "count":I
    :cond_40
    :goto_40
    return-void
.end method

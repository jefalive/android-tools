.class Lcom/itau/empresas/feature/home/HomeLogadaActivity$2;
.super Ljava/lang/Object;
.source "HomeLogadaActivity.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/home/HomeLogadaActivity;->onBalanceView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/home/HomeLogadaActivity;

.field final synthetic val$idConta:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/home/HomeLogadaActivity;Ljava/lang/String;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/home/HomeLogadaActivity;

    .line 325
    iput-object p1, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity$2;->this$0:Lcom/itau/empresas/feature/home/HomeLogadaActivity;

    iput-object p2, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity$2;->val$idConta:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 4

    .line 329
    :try_start_0
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity$2;->this$0:Lcom/itau/empresas/feature/home/HomeLogadaActivity;

    # getter for: Lcom/itau/empresas/feature/home/HomeLogadaActivity;->application:Lcom/itau/empresas/CustomApplication;
    invoke-static {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->access$100(Lcom/itau/empresas/feature/home/HomeLogadaActivity;)Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    const-string v1, "saldo"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->inicioMonitorarTempo(Ljava/lang/String;)V

    .line 330
    const-class v0, Lcom/itau/empresas/api/Api;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->api(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity$2;->val$idConta:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/itau/empresas/api/Api;->consultaSaldo(Ljava/lang/String;)Lcom/itau/empresas/api/model/SaldoVO;

    move-result-object v2

    .line 331
    .local v2, "saldoVO":Lcom/itau/empresas/api/model/SaldoVO;
    invoke-virtual {v2}, Lcom/itau/empresas/api/model/SaldoVO;->getSaldoDisponivelSaque()Ljava/lang/Double;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setValueBalance(Ljava/lang/Double;)V

    .line 332
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity$2;->this$0:Lcom/itau/empresas/feature/home/HomeLogadaActivity;

    # getter for: Lcom/itau/empresas/feature/home/HomeLogadaActivity;->application:Lcom/itau/empresas/CustomApplication;
    invoke-static {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->access$200(Lcom/itau/empresas/feature/home/HomeLogadaActivity;)Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    const-string v1, "saldo"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->fimMonitorarTempo(Ljava/lang/String;)V
    :try_end_33
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_33} :catch_34

    .line 336
    .end local v2    # "saldoVO":Lcom/itau/empresas/api/model/SaldoVO;
    goto :goto_3f

    .line 333
    :catch_34
    move-exception v2

    .line 334
    .local v2, "ex":Ljava/lang/Exception;
    const-string v0, "busca"

    const-string v1, "Consulta saldoDisponive saque: "

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 335
    invoke-static {}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->isErrorBalance()V

    .line 337
    .end local v2    # "ex":Ljava/lang/Exception;
    :goto_3f
    return-void
.end method

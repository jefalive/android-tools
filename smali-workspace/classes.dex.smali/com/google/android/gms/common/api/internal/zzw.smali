.class public Lcom/google/android/gms/common/api/internal/zzw;
.super Landroid/support/v4/app/Fragment;

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/common/api/internal/zzw$zza;,
        Lcom/google/android/gms/common/api/internal/zzw$zzb;
    }
.end annotation


# instance fields
.field private mStarted:Z

.field private zzaiA:I

.field private zzaiB:Lcom/google/android/gms/common/ConnectionResult;

.field private final zzaiC:Landroid/os/Handler;

.field protected zzaiD:Lcom/google/android/gms/common/api/internal/zzn;

.field private final zzaiE:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<Lcom/google/android/gms/common/api/internal/zzw$zza;>;"
        }
    .end annotation
.end field

.field private zzaiz:Z


# direct methods
.method public constructor <init>()V
    .registers 3

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiA:I

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiC:Landroid/os/Handler;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiE:Landroid/util/SparseArray;

    return-void
.end method

.method static synthetic zza(Lcom/google/android/gms/common/api/internal/zzw;I)I
    .registers 2

    iput p1, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiA:I

    return p1
.end method

.method static synthetic zza(Lcom/google/android/gms/common/api/internal/zzw;Lcom/google/android/gms/common/ConnectionResult;)Lcom/google/android/gms/common/ConnectionResult;
    .registers 2

    iput-object p1, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiB:Lcom/google/android/gms/common/ConnectionResult;

    return-object p1
.end method

.method public static zza(Landroid/support/v4/app/FragmentActivity;)Lcom/google/android/gms/common/api/internal/zzw;
    .registers 6

    const-string v0, "Must be called from main thread of process"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/zzx;->zzcD(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v0, "GmsSupportLifecycleFrag"

    :try_start_b
    invoke-virtual {v3, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/common/api/internal/zzw;
    :try_end_12
    .catch Ljava/lang/ClassCastException; {:try_start_b .. :try_end_12} :catch_13

    goto :goto_1c

    :catch_13
    move-exception v4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Fragment with tag GmsSupportLifecycleFrag is not a SupportLifecycleFragment"

    invoke-direct {v0, v1, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :goto_1c
    if-eqz v2, :cond_24

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/internal/zzw;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_26

    :cond_24
    const/4 v0, 0x0

    return-object v0

    :cond_26
    return-object v2
.end method

.method private zza(ILcom/google/android/gms/common/ConnectionResult;)V
    .registers 7

    const-string v0, "GmsSupportLifecycleFrag"

    const-string v1, "Unresolved error while connecting client. Stopping auto-manage."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiE:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/common/api/internal/zzw$zza;

    if-eqz v2, :cond_1c

    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/api/internal/zzw;->zzbD(I)V

    iget-object v3, v2, Lcom/google/android/gms/common/api/internal/zzw$zza;->zzaiH:Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;

    if-eqz v3, :cond_1c

    invoke-interface {v3, p2}, Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;->onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V

    :cond_1c
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzw;->zzpP()V

    return-void
.end method

.method static synthetic zza(Lcom/google/android/gms/common/api/internal/zzw;ILcom/google/android/gms/common/ConnectionResult;)V
    .registers 3

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/api/internal/zzw;->zza(ILcom/google/android/gms/common/ConnectionResult;)V

    return-void
.end method

.method static synthetic zza(Lcom/google/android/gms/common/api/internal/zzw;)Z
    .registers 2

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->mStarted:Z

    return v0
.end method

.method static synthetic zza(Lcom/google/android/gms/common/api/internal/zzw;Z)Z
    .registers 2

    iput-boolean p1, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiz:Z

    return p1
.end method

.method public static zzb(Landroid/support/v4/app/FragmentActivity;)Lcom/google/android/gms/common/api/internal/zzw;
    .registers 5

    invoke-static {p0}, Lcom/google/android/gms/common/api/internal/zzw;->zza(Landroid/support/v4/app/FragmentActivity;)Lcom/google/android/gms/common/api/internal/zzw;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    if-nez v2, :cond_2c

    invoke-static {}, Lcom/google/android/gms/common/api/internal/zzw;->zzpO()Lcom/google/android/gms/common/api/internal/zzw;

    move-result-object v2

    if-nez v2, :cond_1c

    const-string v0, "GmsSupportLifecycleFrag"

    const-string v1, "Unable to find connection error message resources (Did you include play-services-base and the proper proguard rules?); error dialogs may be unavailable."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Lcom/google/android/gms/common/api/internal/zzw;

    invoke-direct {v2}, Lcom/google/android/gms/common/api/internal/zzw;-><init>()V

    :cond_1c
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const-string v1, "GmsSupportLifecycleFrag"

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    :cond_2c
    return-object v2
.end method

.method static synthetic zzb(Lcom/google/android/gms/common/api/internal/zzw;)Z
    .registers 2

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiz:Z

    return v0
.end method

.method static synthetic zzc(Lcom/google/android/gms/common/api/internal/zzw;)Landroid/os/Handler;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiC:Landroid/os/Handler;

    return-object v0
.end method

.method private static zzi(Lcom/google/android/gms/common/ConnectionResult;)Ljava/lang/String;
    .registers 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/common/ConnectionResult;->getErrorMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/common/zze;->getErrorString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static zzpO()Lcom/google/android/gms/common/api/internal/zzw;
    .registers 5

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v0, "com.google.android.gms.common.api.internal.SupportLifecycleFragmentImpl"

    :try_start_4
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_7
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_7} :catch_a
    .catch Ljava/lang/LinkageError; {:try_start_4 .. :try_end_7} :catch_a
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_7} :catch_a

    move-result-object v0

    move-object v2, v0

    goto :goto_1b

    :catch_a
    move-exception v4

    const-string v0, "GmsSupportLifecycleFrag"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1b

    const-string v0, "GmsSupportLifecycleFrag"

    const-string v1, "Unable to find SupportLifecycleFragmentImpl class"

    invoke-static {v0, v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1b
    :goto_1b
    if-eqz v2, :cond_36

    :try_start_1d
    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/internal/zzw;
    :try_end_23
    .catch Ljava/lang/IllegalAccessException; {:try_start_1d .. :try_end_23} :catch_25
    .catch Ljava/lang/InstantiationException; {:try_start_1d .. :try_end_23} :catch_25
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_1d .. :try_end_23} :catch_25
    .catch Ljava/lang/RuntimeException; {:try_start_1d .. :try_end_23} :catch_25

    move-object v3, v0

    goto :goto_36

    :catch_25
    move-exception v4

    const-string v0, "GmsSupportLifecycleFrag"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_36

    const-string v0, "GmsSupportLifecycleFrag"

    const-string v1, "Unable to instantiate SupportLifecycleFragmentImpl class"

    invoke-static {v0, v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_36
    :goto_36
    return-object v3
.end method


# virtual methods
.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 8
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "fd"    # Ljava/io/FileDescriptor;
    .param p3, "writer"    # Ljava/io/PrintWriter;
    .param p4, "args"    # [Ljava/lang/String;

    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v4/app/Fragment;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_4
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiE:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1b

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiE:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/common/api/internal/zzw$zza;

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/google/android/gms/common/api/internal/zzw$zza;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_1b
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    const/4 v3, 0x0

    sparse-switch p1, :sswitch_data_34

    goto :goto_26

    :sswitch_5
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzw;->zzpQ()Lcom/google/android/gms/common/zzc;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzw;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/zzc;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_26

    const/4 v3, 0x1

    goto :goto_26

    :sswitch_15
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1a

    const/4 v3, 0x1

    goto :goto_26

    :cond_1a
    if-nez p2, :cond_26

    new-instance v0, Lcom/google/android/gms/common/ConnectionResult;

    const/16 v1, 0xd

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiB:Lcom/google/android/gms/common/ConnectionResult;

    :cond_26
    :goto_26
    if-eqz v3, :cond_2c

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzw;->zzpP()V

    goto :goto_33

    :cond_2c
    iget v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiA:I

    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiB:Lcom/google/android/gms/common/ConnectionResult;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/api/internal/zzw;->zza(ILcom/google/android/gms/common/ConnectionResult;)V

    :goto_33
    return-void

    :sswitch_data_34
    .sparse-switch
        0x1 -> :sswitch_15
        0x2 -> :sswitch_5
    .end sparse-switch
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .registers 6
    .param p1, "dialogInterface"    # Landroid/content/DialogInterface;

    iget v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiA:I

    new-instance v1, Lcom/google/android/gms/common/ConnectionResult;

    const/16 v2, 0xd

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/api/internal/zzw;->zza(ILcom/google/android/gms/common/ConnectionResult;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_30

    const-string v0, "resolving_error"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiz:Z

    const-string v0, "failed_client_id"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiA:I

    iget v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiA:I

    if-ltz v0, :cond_30

    new-instance v0, Lcom/google/android/gms/common/ConnectionResult;

    const-string v1, "failed_status"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "failed_resolution"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/app/PendingIntent;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiB:Lcom/google/android/gms/common/ConnectionResult;

    :cond_30
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "outState"    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "resolving_error"

    iget-boolean v1, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiz:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiA:I

    if-ltz v0, :cond_2b

    const-string v0, "failed_client_id"

    iget v1, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiA:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "failed_status"

    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiB:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {v1}, Lcom/google/android/gms/common/ConnectionResult;->getErrorCode()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "failed_resolution"

    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiB:Lcom/google/android/gms/common/ConnectionResult;

    invoke-virtual {v1}, Lcom/google/android/gms/common/ConnectionResult;->getResolution()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_2b
    return-void
.end method

.method public onStart()V
    .registers 4

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->mStarted:Z

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiz:Z

    if-nez v0, :cond_24

    const/4 v1, 0x0

    :goto_b
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiE:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_24

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiE:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/common/api/internal/zzw$zza;

    iget-object v0, v2, Lcom/google/android/gms/common/api/internal/zzw$zza;->zzaiG:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->connect()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_24
    return-void
.end method

.method public onStop()V
    .registers 4

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->mStarted:Z

    const/4 v1, 0x0

    :goto_7
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiE:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_20

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiE:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/common/api/internal/zzw$zza;

    iget-object v0, v2, Lcom/google/android/gms/common/api/internal/zzw$zza;->zzaiG:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->disconnect()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_20
    return-void
.end method

.method public zza(ILcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)V
    .registers 8

    const-string v0, "GoogleApiClient instance cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/zzx;->zzb(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiE:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    if-gez v0, :cond_f

    const/4 v0, 0x1

    goto :goto_10

    :cond_f
    const/4 v0, 0x0

    :goto_10
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Already managing a GoogleApiClient with id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->zza(ZLjava/lang/Object;)V

    new-instance v3, Lcom/google/android/gms/common/api/internal/zzw$zza;

    invoke-direct {v3, p0, p1, p2, p3}, Lcom/google/android/gms/common/api/internal/zzw$zza;-><init>(Lcom/google/android/gms/common/api/internal/zzw;ILcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;)V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiE:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->mStarted:Z

    if-eqz v0, :cond_3b

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiz:Z

    if-nez v0, :cond_3b

    invoke-virtual {p2}, Lcom/google/android/gms/common/api/GoogleApiClient;->connect()V

    :cond_3b
    return-void
.end method

.method protected zzb(ILcom/google/android/gms/common/ConnectionResult;)V
    .registers 6

    const-string v0, "GmsSupportLifecycleFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to connect due to user resolvable error "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Lcom/google/android/gms/common/api/internal/zzw;->zzi(Lcom/google/android/gms/common/ConnectionResult;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/api/internal/zzw;->zza(ILcom/google/android/gms/common/ConnectionResult;)V

    return-void
.end method

.method public zzbD(I)V
    .registers 4

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiE:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/gms/common/api/internal/zzw$zza;

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiE:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/internal/zzw$zza;->zzpR()V

    :cond_13
    return-void
.end method

.method protected zzc(ILcom/google/android/gms/common/ConnectionResult;)V
    .registers 5

    const-string v0, "GmsSupportLifecycleFrag"

    const-string v1, "Unable to connect, GooglePlayServices is updating."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/api/internal/zzw;->zza(ILcom/google/android/gms/common/ConnectionResult;)V

    return-void
.end method

.method protected zzpP()V
    .registers 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiz:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiA:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiB:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiD:Lcom/google/android/gms/common/api/internal/zzn;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiD:Lcom/google/android/gms/common/api/internal/zzn;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/internal/zzn;->unregister()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiD:Lcom/google/android/gms/common/api/internal/zzn;

    :cond_15
    const/4 v1, 0x0

    :goto_16
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiE:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_2f

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzw;->zzaiE:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/common/api/internal/zzw$zza;

    iget-object v0, v2, Lcom/google/android/gms/common/api/internal/zzw$zza;->zzaiG:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->connect()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_16

    :cond_2f
    return-void
.end method

.method protected zzpQ()Lcom/google/android/gms/common/zzc;
    .registers 2

    invoke-static {}, Lcom/google/android/gms/common/zzc;->zzoK()Lcom/google/android/gms/common/zzc;

    move-result-object v0

    return-object v0
.end method

.class final Lcom/adobe/mobile/MobileConfig;
.super Ljava/lang/Object;
.source "MobileConfig.java"


# static fields
.field private static final DEFAULT_PRIVACY_STATUS:Lcom/adobe/mobile/MobilePrivacyStatus;

.field private static _instance:Lcom/adobe/mobile/MobileConfig;

.field private static final _instanceMutex:Ljava/lang/Object;

.field private static _userDefinedInputStream:Ljava/io/InputStream;

.field private static final _userDefinedInputStreamMutex:Ljava/lang/Object;

.field private static final _usingAnalyticsMutex:Ljava/lang/Object;

.field private static final _usingAudienceManagerMutex:Ljava/lang/Object;

.field private static final _usingGooglePlayServicesMutex:Ljava/lang/Object;

.field private static final _usingTargetMutex:Ljava/lang/Object;


# instance fields
.field private _aamAnalyticsForwardingEnabled:Z

.field private _aamServer:Ljava/lang/String;

.field private _aamTimeout:I

.field private _acquisitionAppIdentifier:Ljava/lang/String;

.field private _acquisitionServer:Ljava/lang/String;

.field private _adobeDataCallback:Lcom/adobe/mobile/Config$AdobeDataCallback;

.field private _backdateSessionInfoEnabled:Z

.field private _batchLimit:I

.field private _callbackTemplates:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/adobe/mobile/Message;>;"
        }
    .end annotation
.end field

.field private _characterSet:Ljava/lang/String;

.field private _clientCode:Ljava/lang/String;

.field private _defaultLocationTimeout:I

.field private _inAppMessages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/adobe/mobile/Message;>;"
        }
    .end annotation
.end field

.field private _lifecycleTimeout:I

.field private _marketingCloudOrganizationId:Ljava/lang/String;

.field private _messagesURL:Ljava/lang/String;

.field private _networkConnectivity:Z

.field private _offlineTrackingEnabled:Z

.field private _piiRequests:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/adobe/mobile/Message;>;"
        }
    .end annotation
.end field

.field private _pointsOfInterest:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/util/List<Ljava/lang/Object;>;>;"
        }
    .end annotation
.end field

.field private _pointsOfInterestURL:Ljava/lang/String;

.field private _privacyStatus:Lcom/adobe/mobile/MobilePrivacyStatus;

.field private _referrerTimeout:I

.field private _reportSuiteIds:Ljava/lang/String;

.field private _ssl:Z

.field private _trackingServer:Ljava/lang/String;

.field private _usingAnalytics:Ljava/lang/Boolean;

.field private _usingAudienceManager:Ljava/lang/Boolean;

.field private _usingGooglePlayServices:Ljava/lang/Boolean;

.field private _usingTarget:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 91
    sget-object v0, Lcom/adobe/mobile/MobilePrivacyStatus;->MOBILE_PRIVACY_STATUS_OPT_IN:Lcom/adobe/mobile/MobilePrivacyStatus;

    sput-object v0, Lcom/adobe/mobile/MobileConfig;->DEFAULT_PRIVACY_STATUS:Lcom/adobe/mobile/MobilePrivacyStatus;

    .line 151
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/MobileConfig;->_instance:Lcom/adobe/mobile/MobileConfig;

    .line 152
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/MobileConfig;->_instanceMutex:Ljava/lang/Object;

    .line 409
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/MobileConfig;->_usingAnalyticsMutex:Ljava/lang/Object;

    .line 428
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/MobileConfig;->_usingGooglePlayServicesMutex:Ljava/lang/Object;

    .line 439
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/MobileConfig;->_usingAudienceManagerMutex:Ljava/lang/Object;

    .line 460
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/MobileConfig;->_usingTargetMutex:Ljava/lang/Object;

    .line 961
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/MobileConfig;->_userDefinedInputStream:Ljava/io/InputStream;

    .line 962
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/MobileConfig;->_userDefinedInputStreamMutex:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .registers 14

    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/adobe/mobile/MobileConfig;->_networkConnectivity:Z

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_adobeDataCallback:Lcom/adobe/mobile/Config$AdobeDataCallback;

    .line 104
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_reportSuiteIds:Ljava/lang/String;

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_trackingServer:Ljava/lang/String;

    .line 106
    const-string v0, "UTF-8"

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_characterSet:Ljava/lang/String;

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/adobe/mobile/MobileConfig;->_ssl:Z

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/adobe/mobile/MobileConfig;->_offlineTrackingEnabled:Z

    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/adobe/mobile/MobileConfig;->_backdateSessionInfoEnabled:Z

    .line 110
    const/16 v0, 0x12c

    iput v0, p0, Lcom/adobe/mobile/MobileConfig;->_lifecycleTimeout:I

    .line 111
    const/4 v0, 0x0

    iput v0, p0, Lcom/adobe/mobile/MobileConfig;->_referrerTimeout:I

    .line 112
    const/4 v0, 0x0

    iput v0, p0, Lcom/adobe/mobile/MobileConfig;->_batchLimit:I

    .line 113
    sget-object v0, Lcom/adobe/mobile/MobileConfig;->DEFAULT_PRIVACY_STATUS:Lcom/adobe/mobile/MobilePrivacyStatus;

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_privacyStatus:Lcom/adobe/mobile/MobilePrivacyStatus;

    .line 114
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_pointsOfInterest:Ljava/util/List;

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_pointsOfInterestURL:Ljava/lang/String;

    .line 120
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_clientCode:Ljava/lang/String;

    .line 121
    const/4 v0, 0x2

    iput v0, p0, Lcom/adobe/mobile/MobileConfig;->_defaultLocationTimeout:I

    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_aamServer:Ljava/lang/String;

    .line 127
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/adobe/mobile/MobileConfig;->_aamAnalyticsForwardingEnabled:Z

    .line 128
    const/4 v0, 0x2

    iput v0, p0, Lcom/adobe/mobile/MobileConfig;->_aamTimeout:I

    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_acquisitionServer:Ljava/lang/String;

    .line 134
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_acquisitionAppIdentifier:Ljava/lang/String;

    .line 139
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_messagesURL:Ljava/lang/String;

    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_inAppMessages:Ljava/util/ArrayList;

    .line 141
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_callbackTemplates:Ljava/util/ArrayList;

    .line 142
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_piiRequests:Ljava/util/ArrayList;

    .line 146
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_marketingCloudOrganizationId:Ljava/lang/String;

    .line 408
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_usingAnalytics:Ljava/lang/Boolean;

    .line 427
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_usingGooglePlayServices:Ljava/lang/Boolean;

    .line 438
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_usingAudienceManager:Ljava/lang/Boolean;

    .line 459
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_usingTarget:Ljava/lang/Boolean;

    .line 169
    invoke-virtual {p0}, Lcom/adobe/mobile/MobileConfig;->startNotifier()V

    .line 172
    invoke-direct {p0}, Lcom/adobe/mobile/MobileConfig;->loadBundleConfig()Lorg/json/JSONObject;

    move-result-object v4

    .line 175
    .local v4, "configSettings":Lorg/json/JSONObject;
    if-nez v4, :cond_6a

    .line 176
    return-void

    .line 179
    :cond_6a
    const/4 v5, 0x0

    .line 181
    .local v5, "analyticsSettings":Lorg/json/JSONObject;
    const-string v0, "analytics"

    :try_start_6d
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_70
    .catch Lorg/json/JSONException; {:try_start_6d .. :try_end_70} :catch_73

    move-result-object v0

    move-object v5, v0

    .line 184
    goto :goto_7c

    .line 182
    :catch_73
    move-exception v6

    .line 183
    .local v6, "ex":Lorg/json/JSONException;
    const-string v0, "Analytics - Not configured."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 187
    .end local v6    # "ex":Lorg/json/JSONException;
    :goto_7c
    if-eqz v5, :cond_15b

    .line 189
    const-string v0, "server"

    :try_start_80
    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_trackingServer:Ljava/lang/String;

    .line 190
    const-string v0, "rsids"

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_reportSuiteIds:Ljava/lang/String;
    :try_end_8e
    .catch Lorg/json/JSONException; {:try_start_80 .. :try_end_8e} :catch_8f

    .line 195
    goto :goto_9e

    .line 191
    :catch_8f
    move-exception v6

    .line 192
    .local v6, "e":Lorg/json/JSONException;
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_trackingServer:Ljava/lang/String;

    .line 193
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_reportSuiteIds:Ljava/lang/String;

    .line 194
    const-string v0, "Analytics - Not Configured."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 198
    .end local v6    # "e":Lorg/json/JSONException;
    :goto_9e
    const-string v0, "charset"

    :try_start_a0
    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_characterSet:Ljava/lang/String;
    :try_end_a6
    .catch Lorg/json/JSONException; {:try_start_a0 .. :try_end_a6} :catch_a7

    .line 201
    goto :goto_ac

    .line 199
    :catch_a7
    move-exception v6

    .line 200
    .local v6, "e":Lorg/json/JSONException;
    const-string v0, "UTF-8"

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_characterSet:Ljava/lang/String;

    .line 204
    .end local v6    # "e":Lorg/json/JSONException;
    :goto_ac
    const-string v0, "ssl"

    :try_start_ae
    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/adobe/mobile/MobileConfig;->_ssl:Z
    :try_end_b4
    .catch Lorg/json/JSONException; {:try_start_ae .. :try_end_b4} :catch_b5

    .line 207
    goto :goto_b9

    .line 205
    :catch_b5
    move-exception v6

    .line 206
    .local v6, "e":Lorg/json/JSONException;
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/adobe/mobile/MobileConfig;->_ssl:Z

    .line 210
    .end local v6    # "e":Lorg/json/JSONException;
    :goto_b9
    const-string v0, "offlineEnabled"

    :try_start_bb
    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/adobe/mobile/MobileConfig;->_offlineTrackingEnabled:Z
    :try_end_c1
    .catch Lorg/json/JSONException; {:try_start_bb .. :try_end_c1} :catch_c2

    .line 213
    goto :goto_c6

    .line 211
    :catch_c2
    move-exception v6

    .line 212
    .local v6, "e":Lorg/json/JSONException;
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/adobe/mobile/MobileConfig;->_offlineTrackingEnabled:Z

    .line 216
    .end local v6    # "e":Lorg/json/JSONException;
    :goto_c6
    const-string v0, "backdateSessionInfo"

    :try_start_c8
    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/adobe/mobile/MobileConfig;->_backdateSessionInfoEnabled:Z
    :try_end_ce
    .catch Lorg/json/JSONException; {:try_start_c8 .. :try_end_ce} :catch_cf

    .line 219
    goto :goto_d3

    .line 217
    :catch_cf
    move-exception v6

    .line 218
    .local v6, "e":Lorg/json/JSONException;
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/adobe/mobile/MobileConfig;->_backdateSessionInfoEnabled:Z

    .line 222
    .end local v6    # "e":Lorg/json/JSONException;
    :goto_d3
    const-string v0, "lifecycleTimeout"

    :try_start_d5
    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/adobe/mobile/MobileConfig;->_lifecycleTimeout:I
    :try_end_db
    .catch Lorg/json/JSONException; {:try_start_d5 .. :try_end_db} :catch_dc

    .line 225
    goto :goto_e1

    .line 223
    :catch_dc
    move-exception v6

    .line 224
    .local v6, "e":Lorg/json/JSONException;
    const/16 v0, 0x12c

    iput v0, p0, Lcom/adobe/mobile/MobileConfig;->_lifecycleTimeout:I

    .line 228
    .end local v6    # "e":Lorg/json/JSONException;
    :goto_e1
    const-string v0, "referrerTimeout"

    :try_start_e3
    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/adobe/mobile/MobileConfig;->_referrerTimeout:I
    :try_end_e9
    .catch Lorg/json/JSONException; {:try_start_e3 .. :try_end_e9} :catch_ea

    .line 231
    goto :goto_ee

    .line 229
    :catch_ea
    move-exception v6

    .line 230
    .local v6, "e":Lorg/json/JSONException;
    const/4 v0, 0x0

    iput v0, p0, Lcom/adobe/mobile/MobileConfig;->_referrerTimeout:I

    .line 234
    .end local v6    # "e":Lorg/json/JSONException;
    :goto_ee
    const-string v0, "batchLimit"

    :try_start_f0
    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/adobe/mobile/MobileConfig;->_batchLimit:I
    :try_end_f6
    .catch Lorg/json/JSONException; {:try_start_f0 .. :try_end_f6} :catch_f7

    .line 237
    goto :goto_fb

    .line 235
    :catch_f7
    move-exception v6

    .line 236
    .local v6, "e":Lorg/json/JSONException;
    const/4 v0, 0x0

    iput v0, p0, Lcom/adobe/mobile/MobileConfig;->_batchLimit:I

    .line 241
    .end local v6    # "e":Lorg/json/JSONException;
    :goto_fb
    :try_start_fb
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "PrivacyStatus"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11b

    .line 242
    invoke-static {}, Lcom/adobe/mobile/MobilePrivacyStatus;->values()[Lcom/adobe/mobile/MobilePrivacyStatus;

    move-result-object v0

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "PrivacyStatus"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_privacyStatus:Lcom/adobe/mobile/MobilePrivacyStatus;
    :try_end_11a
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_fb .. :try_end_11a} :catch_130

    goto :goto_12f

    .line 247
    :cond_11b
    const-string v0, "privacyDefault"

    :try_start_11d
    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_120
    .catch Lorg/json/JSONException; {:try_start_11d .. :try_end_120} :catch_122
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_11d .. :try_end_120} :catch_130

    move-result-object v6

    .line 250
    .local v6, "privacyString":Ljava/lang/String;
    goto :goto_124

    .line 248
    .end local v6    # "privacyString":Ljava/lang/String;
    :catch_122
    move-exception v7

    .line 249
    .local v7, "e":Lorg/json/JSONException;
    const/4 v6, 0x0

    .line 252
    .local v6, "privacyString":Ljava/lang/String;
    .end local v7    # "e":Lorg/json/JSONException;
    :goto_124
    if-eqz v6, :cond_12b

    :try_start_126
    invoke-direct {p0, v6}, Lcom/adobe/mobile/MobileConfig;->privacyStatusFromString(Ljava/lang/String;)Lcom/adobe/mobile/MobilePrivacyStatus;

    move-result-object v0

    goto :goto_12d

    :cond_12b
    sget-object v0, Lcom/adobe/mobile/MobileConfig;->DEFAULT_PRIVACY_STATUS:Lcom/adobe/mobile/MobilePrivacyStatus;

    :goto_12d
    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_privacyStatus:Lcom/adobe/mobile/MobilePrivacyStatus;
    :try_end_12f
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_126 .. :try_end_12f} :catch_130

    .line 258
    .end local v6    # "privacyString":Ljava/lang/String;
    :goto_12f
    goto :goto_141

    .line 255
    :catch_130
    move-exception v6

    .line 256
    .local v6, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Config - Error pulling privacy from shared preferences. (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 257
    return-void

    .line 261
    .end local v6    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_141
    const-string v0, "poi"

    :try_start_143
    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 262
    .local v6, "poiArray":Lorg/json/JSONArray;
    invoke-direct {p0, v6}, Lcom/adobe/mobile/MobileConfig;->loadPoiFromJsonArray(Lorg/json/JSONArray;)V
    :try_end_14a
    .catch Lorg/json/JSONException; {:try_start_143 .. :try_end_14a} :catch_14b

    .line 265
    .end local v6    # "poiArray":Lorg/json/JSONArray;
    goto :goto_15b

    .line 263
    :catch_14b
    move-exception v6

    .line 264
    .local v6, "e":Lorg/json/JSONException;
    const-string v0, "Analytics - Malformed POI List(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 270
    .end local v6    # "e":Lorg/json/JSONException;
    :cond_15b
    :goto_15b
    const/4 v6, 0x0

    .line 272
    .local v6, "targetSettings":Lorg/json/JSONObject;
    const-string v0, "target"

    :try_start_15e
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_161
    .catch Lorg/json/JSONException; {:try_start_15e .. :try_end_161} :catch_164

    move-result-object v0

    move-object v6, v0

    .line 275
    goto :goto_16d

    .line 273
    :catch_164
    move-exception v7

    .line 274
    .local v7, "ex":Lorg/json/JSONException;
    const-string v0, "Target - Not Configured."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 277
    .end local v7    # "ex":Lorg/json/JSONException;
    :goto_16d
    if-eqz v6, :cond_191

    .line 279
    const-string v0, "clientCode"

    :try_start_171
    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_clientCode:Ljava/lang/String;
    :try_end_177
    .catch Lorg/json/JSONException; {:try_start_171 .. :try_end_177} :catch_178

    .line 283
    goto :goto_184

    .line 280
    :catch_178
    move-exception v7

    .line 281
    .local v7, "e":Lorg/json/JSONException;
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_clientCode:Ljava/lang/String;

    .line 282
    const-string v0, "Target - Not Configured."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 286
    .end local v7    # "e":Lorg/json/JSONException;
    :goto_184
    const-string v0, "timeout"

    :try_start_186
    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/adobe/mobile/MobileConfig;->_defaultLocationTimeout:I
    :try_end_18c
    .catch Lorg/json/JSONException; {:try_start_186 .. :try_end_18c} :catch_18d

    .line 289
    goto :goto_191

    .line 287
    :catch_18d
    move-exception v7

    .line 288
    .local v7, "e":Lorg/json/JSONException;
    const/4 v0, 0x2

    iput v0, p0, Lcom/adobe/mobile/MobileConfig;->_defaultLocationTimeout:I

    .line 293
    .end local v7    # "e":Lorg/json/JSONException;
    :cond_191
    :goto_191
    const/4 v7, 0x0

    .line 296
    .local v7, "aamSettings":Lorg/json/JSONObject;
    const-string v0, "audienceManager"

    :try_start_194
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_197
    .catch Lorg/json/JSONException; {:try_start_194 .. :try_end_197} :catch_19a

    move-result-object v0

    move-object v7, v0

    .line 299
    goto :goto_1a3

    .line 297
    :catch_19a
    move-exception v8

    .line 298
    .local v8, "e":Lorg/json/JSONException;
    const-string v0, "Audience Manager - Not Configured."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 301
    .end local v8    # "e":Lorg/json/JSONException;
    :goto_1a3
    if-eqz v7, :cond_1e0

    .line 303
    const-string v0, "server"

    :try_start_1a7
    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_aamServer:Ljava/lang/String;
    :try_end_1ad
    .catch Lorg/json/JSONException; {:try_start_1a7 .. :try_end_1ad} :catch_1ae

    .line 307
    goto :goto_1ba

    .line 304
    :catch_1ae
    move-exception v8

    .line 305
    .local v8, "ex":Lorg/json/JSONException;
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_aamServer:Ljava/lang/String;

    .line 306
    const-string v0, "Audience Manager - Not Configured."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 310
    .end local v8    # "ex":Lorg/json/JSONException;
    :goto_1ba
    const-string v0, "analyticsForwardingEnabled"

    :try_start_1bc
    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/adobe/mobile/MobileConfig;->_aamAnalyticsForwardingEnabled:Z
    :try_end_1c2
    .catch Lorg/json/JSONException; {:try_start_1bc .. :try_end_1c2} :catch_1c3

    .line 313
    goto :goto_1c7

    .line 311
    :catch_1c3
    move-exception v8

    .line 312
    .local v8, "ex":Lorg/json/JSONException;
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/adobe/mobile/MobileConfig;->_aamAnalyticsForwardingEnabled:Z

    .line 315
    .end local v8    # "ex":Lorg/json/JSONException;
    :goto_1c7
    iget-boolean v0, p0, Lcom/adobe/mobile/MobileConfig;->_aamAnalyticsForwardingEnabled:Z

    if-eqz v0, :cond_1d3

    .line 316
    const-string v0, "Audience Manager - Analytics Server-Side Forwarding Is Enabled."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 320
    :cond_1d3
    const-string v0, "timeout"

    :try_start_1d5
    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/adobe/mobile/MobileConfig;->_aamTimeout:I
    :try_end_1db
    .catch Lorg/json/JSONException; {:try_start_1d5 .. :try_end_1db} :catch_1dc

    .line 323
    goto :goto_1e0

    .line 321
    :catch_1dc
    move-exception v8

    .line 322
    .local v8, "e":Lorg/json/JSONException;
    const/4 v0, 0x2

    iput v0, p0, Lcom/adobe/mobile/MobileConfig;->_aamTimeout:I

    .line 327
    .end local v8    # "e":Lorg/json/JSONException;
    :cond_1e0
    :goto_1e0
    const/4 v8, 0x0

    .line 330
    .local v8, "acquisitionSettings":Lorg/json/JSONObject;
    const-string v0, "acquisition"

    :try_start_1e3
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_1e6
    .catch Lorg/json/JSONException; {:try_start_1e3 .. :try_end_1e6} :catch_1e9

    move-result-object v0

    move-object v8, v0

    .line 333
    goto :goto_1f2

    .line 331
    :catch_1e9
    move-exception v9

    .line 332
    .local v9, "e":Lorg/json/JSONException;
    const-string v0, "Acquisition - Not Configured."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 335
    .end local v9    # "e":Lorg/json/JSONException;
    :goto_1f2
    if-eqz v8, :cond_214

    .line 337
    const-string v0, "appid"

    :try_start_1f6
    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_acquisitionAppIdentifier:Ljava/lang/String;

    .line 338
    const-string v0, "server"

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_acquisitionServer:Ljava/lang/String;
    :try_end_204
    .catch Lorg/json/JSONException; {:try_start_1f6 .. :try_end_204} :catch_205

    .line 344
    goto :goto_214

    .line 340
    :catch_205
    move-exception v9

    .line 341
    .local v9, "ex":Lorg/json/JSONException;
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_acquisitionAppIdentifier:Ljava/lang/String;

    .line 342
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_acquisitionServer:Ljava/lang/String;

    .line 343
    const-string v0, "Acquisition - Not configured correctly (missing server or app identifier)."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 348
    .end local v9    # "ex":Lorg/json/JSONException;
    :cond_214
    :goto_214
    const/4 v9, 0x0

    .line 350
    .local v9, "remoteURLs":Lorg/json/JSONObject;
    const-string v0, "remotes"

    :try_start_217
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_21a
    .catch Lorg/json/JSONException; {:try_start_217 .. :try_end_21a} :catch_21d

    move-result-object v0

    move-object v9, v0

    .line 354
    goto :goto_226

    .line 352
    :catch_21d
    move-exception v10

    .line 353
    .local v10, "e":Lorg/json/JSONException;
    const-string v0, "Remotes - Not Configured."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 356
    .end local v10    # "e":Lorg/json/JSONException;
    :goto_226
    if-eqz v9, :cond_25a

    .line 358
    const-string v0, "messages"

    :try_start_22a
    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_messagesURL:Ljava/lang/String;
    :try_end_230
    .catch Lorg/json/JSONException; {:try_start_22a .. :try_end_230} :catch_231

    .line 361
    goto :goto_241

    .line 359
    :catch_231
    move-exception v10

    .line 360
    .local v10, "e":Lorg/json/JSONException;
    const-string v0, "Config - No in-app messages remote url loaded (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v10}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 364
    .end local v10    # "e":Lorg/json/JSONException;
    :goto_241
    const-string v0, "analytics.poi"

    :try_start_243
    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_pointsOfInterestURL:Ljava/lang/String;
    :try_end_249
    .catch Lorg/json/JSONException; {:try_start_243 .. :try_end_249} :catch_24a

    .line 367
    goto :goto_25a

    .line 365
    :catch_24a
    move-exception v10

    .line 366
    .local v10, "e":Lorg/json/JSONException;
    const-string v0, "Config - No points of interest remote url loaded (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v10}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 371
    .end local v10    # "e":Lorg/json/JSONException;
    :cond_25a
    :goto_25a
    const/4 v10, 0x0

    .line 373
    .local v10, "jsonMessages":Lorg/json/JSONArray;
    const-string v0, "messages"

    :try_start_25d
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_260
    .catch Lorg/json/JSONException; {:try_start_25d .. :try_end_260} :catch_263

    move-result-object v0

    move-object v10, v0

    .line 376
    goto :goto_26c

    .line 374
    :catch_263
    move-exception v11

    .line 375
    .local v11, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - Not configured locally."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 378
    .end local v11    # "ex":Lorg/json/JSONException;
    :goto_26c
    if-eqz v10, :cond_277

    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_277

    .line 379
    invoke-direct {p0, v10}, Lcom/adobe/mobile/MobileConfig;->loadMessagesFromJsonArray(Lorg/json/JSONArray;)V

    .line 383
    :cond_277
    const/4 v11, 0x0

    .line 385
    .local v11, "marketingCloudSettings":Lorg/json/JSONObject;
    const-string v0, "marketingCloud"

    :try_start_27a
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_27d
    .catch Lorg/json/JSONException; {:try_start_27a .. :try_end_27d} :catch_280

    move-result-object v0

    move-object v11, v0

    .line 388
    goto :goto_289

    .line 386
    :catch_280
    move-exception v12

    .line 387
    .local v12, "ex":Lorg/json/JSONException;
    const-string v0, "Marketing Cloud - Not configured locally."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 390
    .end local v12    # "ex":Lorg/json/JSONException;
    :goto_289
    if-eqz v11, :cond_2a0

    .line 392
    const-string v0, "org"

    :try_start_28d
    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_marketingCloudOrganizationId:Ljava/lang/String;
    :try_end_293
    .catch Lorg/json/JSONException; {:try_start_28d .. :try_end_293} :catch_294

    .line 396
    goto :goto_2a0

    .line 393
    :catch_294
    move-exception v12

    .line 394
    .local v12, "ex":Lorg/json/JSONException;
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_marketingCloudOrganizationId:Ljava/lang/String;

    .line 395
    const-string v0, "Visitor - ID Service Not Configured."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 400
    .end local v12    # "ex":Lorg/json/JSONException;
    :cond_2a0
    :goto_2a0
    invoke-virtual {p0}, Lcom/adobe/mobile/MobileConfig;->loadCachedRemotes()V

    .line 403
    invoke-direct {p0}, Lcom/adobe/mobile/MobileConfig;->updateBlacklist()V

    .line 404
    return-void
.end method

.method static synthetic access$000(Lcom/adobe/mobile/MobileConfig;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/adobe/mobile/MobileConfig;

    .line 42
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_messagesURL:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/adobe/mobile/MobileConfig;)V
    .registers 1
    .param p0, "x0"    # Lcom/adobe/mobile/MobileConfig;

    .line 42
    invoke-direct {p0}, Lcom/adobe/mobile/MobileConfig;->loadMessageImages()V

    return-void
.end method

.method static synthetic access$200(Lcom/adobe/mobile/MobileConfig;)V
    .registers 1
    .param p0, "x0"    # Lcom/adobe/mobile/MobileConfig;

    .line 42
    invoke-direct {p0}, Lcom/adobe/mobile/MobileConfig;->updateBlacklist()V

    return-void
.end method

.method static synthetic access$300(Lcom/adobe/mobile/MobileConfig;)Ljava/util/ArrayList;
    .registers 2
    .param p0, "x0"    # Lcom/adobe/mobile/MobileConfig;

    .line 42
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_inAppMessages:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/adobe/mobile/MobileConfig;)Z
    .registers 2
    .param p0, "x0"    # Lcom/adobe/mobile/MobileConfig;

    .line 42
    iget-boolean v0, p0, Lcom/adobe/mobile/MobileConfig;->_networkConnectivity:Z

    return v0
.end method

.method static synthetic access$402(Lcom/adobe/mobile/MobileConfig;Z)Z
    .registers 2
    .param p0, "x0"    # Lcom/adobe/mobile/MobileConfig;
    .param p1, "x1"    # Z

    .line 42
    iput-boolean p1, p0, Lcom/adobe/mobile/MobileConfig;->_networkConnectivity:Z

    return p1
.end method

.method protected static getInstance()Lcom/adobe/mobile/MobileConfig;
    .registers 3

    .line 154
    sget-object v1, Lcom/adobe/mobile/MobileConfig;->_instanceMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 155
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/MobileConfig;->_instance:Lcom/adobe/mobile/MobileConfig;

    if-nez v0, :cond_e

    .line 156
    new-instance v0, Lcom/adobe/mobile/MobileConfig;

    invoke-direct {v0}, Lcom/adobe/mobile/MobileConfig;-><init>()V

    sput-object v0, Lcom/adobe/mobile/MobileConfig;->_instance:Lcom/adobe/mobile/MobileConfig;

    .line 159
    :cond_e
    sget-object v0, Lcom/adobe/mobile/MobileConfig;->_instance:Lcom/adobe/mobile/MobileConfig;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    monitor-exit v1

    return-object v0

    .line 160
    :catchall_12
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method private loadBundleConfig()Lorg/json/JSONObject;
    .registers 9

    .line 847
    const/4 v4, 0x0

    .line 851
    .local v4, "jsonData":Lorg/json/JSONObject;
    sget-object v6, Lcom/adobe/mobile/MobileConfig;->_userDefinedInputStreamMutex:Ljava/lang/Object;

    monitor-enter v6

    .line 852
    :try_start_4
    sget-object v5, Lcom/adobe/mobile/MobileConfig;->_userDefinedInputStream:Ljava/io/InputStream;
    :try_end_6
    .catchall {:try_start_4 .. :try_end_6} :catchall_8

    .line 853
    .local v5, "userPath":Ljava/io/InputStream;
    monitor-exit v6

    goto :goto_b

    .end local v5    # "userPath":Ljava/io/InputStream;
    :catchall_8
    move-exception v7

    monitor-exit v6

    throw v7

    .line 857
    .local v5, "userPath":Ljava/io/InputStream;
    :goto_b
    if-eqz v5, :cond_1a

    .line 858
    const-string v0, "Config - Attempting to load config file from override stream"

    const/4 v1, 0x0

    :try_start_10
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 859
    invoke-direct {p0, v5}, Lcom/adobe/mobile/MobileConfig;->loadConfigFromStream(Ljava/io/InputStream;)Lorg/json/JSONObject;
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_18} :catch_1b
    .catch Lorg/json/JSONException; {:try_start_10 .. :try_end_18} :catch_2c

    move-result-object v0

    move-object v4, v0

    .line 867
    :cond_1a
    goto :goto_3c

    .line 862
    :catch_1b
    move-exception v6

    .line 863
    .local v6, "e":Ljava/io/IOException;
    const-string v0, "Config - Error loading user defined config (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 867
    .end local v6    # "e":Ljava/io/IOException;
    goto :goto_3c

    .line 865
    :catch_2c
    move-exception v6

    .line 866
    .local v6, "e":Lorg/json/JSONException;
    const-string v0, "Config - Error parsing user defined config (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 870
    .end local v6    # "e":Lorg/json/JSONException;
    :goto_3c
    if-nez v4, :cond_7f

    .line 871
    if-eqz v5, :cond_48

    .line 872
    const-string v0, "Config - Failed attempting to load custom config, will fall back to standard config location."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 875
    :cond_48
    const-string v0, "Config - Attempting to load config file from default location"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 876
    const-string v0, "ADBMobileConfig.json"

    invoke-direct {p0, v0}, Lcom/adobe/mobile/MobileConfig;->loadConfigFile(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 880
    if-nez v4, :cond_7f

    .line 881
    const-string v0, "Config - Could not find config file at expected location.  Attempting to load from www folder"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 882
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "www"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ADBMobileConfig.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/adobe/mobile/MobileConfig;->loadConfigFile(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 886
    :cond_7f
    return-object v4
.end method

.method private loadConfigFile(Ljava/lang/String;)Lorg/json/JSONObject;
    .registers 9
    .param p1, "configFilePath"    # Ljava/lang/String;

    .line 890
    const/4 v4, 0x0

    .line 892
    .local v4, "jsonData":Lorg/json/JSONObject;
    :try_start_1
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_8} :catch_1f
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_8} :catch_30
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_1 .. :try_end_8} :catch_41

    move-result-object v5

    .line 893
    .local v5, "resources":Landroid/content/res/Resources;
    if-nez v5, :cond_d

    .line 894
    const/4 v0, 0x0

    return-object v0

    .line 897
    :cond_d
    :try_start_d
    invoke-virtual {v5}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_10} :catch_1f
    .catch Lorg/json/JSONException; {:try_start_d .. :try_end_10} :catch_30
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_d .. :try_end_10} :catch_41

    move-result-object v6

    .line 898
    .local v6, "assets":Landroid/content/res/AssetManager;
    if-nez v6, :cond_15

    .line 899
    const/4 v0, 0x0

    return-object v0

    .line 902
    :cond_15
    :try_start_15
    invoke-virtual {v6, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/adobe/mobile/MobileConfig;->loadConfigFromStream(Ljava/io/InputStream;)Lorg/json/JSONObject;
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_1c} :catch_1f
    .catch Lorg/json/JSONException; {:try_start_15 .. :try_end_1c} :catch_30
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_15 .. :try_end_1c} :catch_41

    move-result-object v0

    move-object v4, v0

    .line 912
    .end local v5    # "resources":Landroid/content/res/Resources;
    .end local v6    # "assets":Landroid/content/res/AssetManager;
    goto :goto_51

    .line 904
    :catch_1f
    move-exception v5

    .line 905
    .local v5, "e":Ljava/io/IOException;
    const-string v0, "Config - Exception loading config file (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 912
    .end local v5    # "e":Ljava/io/IOException;
    goto :goto_51

    .line 907
    :catch_30
    move-exception v5

    .line 908
    .local v5, "e":Lorg/json/JSONException;
    const-string v0, "Config - Exception parsing config file (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 912
    .end local v5    # "e":Lorg/json/JSONException;
    goto :goto_51

    .line 910
    :catch_41
    move-exception v5

    .line 911
    .local v5, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Config - Null context when attempting to read config file (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 914
    .end local v5    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_51
    return-object v4
.end method

.method private loadConfigFromStream(Ljava/io/InputStream;)Lorg/json/JSONObject;
    .registers 12
    .param p1, "stream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 931
    if-nez p1, :cond_4

    .line 932
    const/4 v0, 0x0

    return-object v0

    .line 937
    :cond_4
    :try_start_4
    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v0

    new-array v4, v0, [B

    .line 938
    .local v4, "data":[B
    invoke-virtual {p1, v4}, Ljava/io/InputStream;->read([B)I

    .line 941
    new-instance v5, Ljava/lang/String;

    const-string v0, "UTF-8"

    invoke-direct {v5, v4, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 942
    .local v5, "jsonString":Ljava/lang/String;
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_19} :catch_2e
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_19} :catch_53
    .catchall {:try_start_4 .. :try_end_19} :catchall_78

    .line 952
    :try_start_19
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_1c} :catch_1d

    .line 955
    goto :goto_2d

    .line 953
    :catch_1d
    move-exception v7

    .line 954
    .local v7, "e":Ljava/io/IOException;
    const-string v0, "Config - Unable to close stream (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 955
    .end local v7    # "e":Ljava/io/IOException;
    :goto_2d
    return-object v6

    .line 944
    .end local v4    # "data":[B
    .end local v5    # "jsonString":Ljava/lang/String;
    :catch_2e
    move-exception v4

    .line 945
    .local v4, "e":Ljava/io/IOException;
    const-string v0, "Config - Exception when reading config (%s)"

    const/4 v1, 0x1

    :try_start_32
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3e
    .catchall {:try_start_32 .. :try_end_3e} :catchall_78

    .line 952
    .end local v4    # "e":Ljava/io/IOException;
    :try_start_3e
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_41
    .catch Ljava/io/IOException; {:try_start_3e .. :try_end_41} :catch_42

    .line 955
    goto :goto_8e

    .line 953
    :catch_42
    move-exception v4

    .line 954
    .local v4, "e":Ljava/io/IOException;
    const-string v0, "Config - Unable to close stream (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 956
    .end local v4    # "e":Ljava/io/IOException;
    goto :goto_8e

    .line 947
    :catch_53
    move-exception v4

    .line 948
    .local v4, "e":Ljava/lang/NullPointerException;
    const-string v0, "Config - Stream closed when attempting to load config (%s)"

    const/4 v1, 0x1

    :try_start_57
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_63
    .catchall {:try_start_57 .. :try_end_63} :catchall_78

    .line 952
    .end local v4    # "e":Ljava/lang/NullPointerException;
    :try_start_63
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_66
    .catch Ljava/io/IOException; {:try_start_63 .. :try_end_66} :catch_67

    .line 955
    goto :goto_8e

    .line 953
    :catch_67
    move-exception v4

    .line 954
    .local v4, "e":Ljava/io/IOException;
    const-string v0, "Config - Unable to close stream (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 956
    .end local v4    # "e":Ljava/io/IOException;
    goto :goto_8e

    .line 951
    :catchall_78
    move-exception v8

    .line 952
    :try_start_79
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_7c
    .catch Ljava/io/IOException; {:try_start_79 .. :try_end_7c} :catch_7d

    .line 955
    goto :goto_8d

    .line 953
    :catch_7d
    move-exception v9

    .line 954
    .local v9, "e":Ljava/io/IOException;
    const-string v0, "Config - Unable to close stream (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v9}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 955
    .end local v9    # "e":Ljava/io/IOException;
    :goto_8d
    throw v8

    .line 958
    :goto_8e
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    return-object v0
.end method

.method private loadMessageImages()V
    .registers 3

    .line 1110
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getMessageImageCachingExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/MobileConfig$12;

    invoke-direct {v1, p0}, Lcom/adobe/mobile/MobileConfig$12;-><init>(Lcom/adobe/mobile/MobileConfig;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 1151
    return-void
.end method

.method private loadMessagesDataFromRemote(Lorg/json/JSONObject;)V
    .registers 6
    .param p1, "jsonData"    # Lorg/json/JSONObject;

    .line 1018
    if-nez p1, :cond_10

    .line 1019
    const-string v0, "Messages - Remote messages config was null, falling back to bundled messages"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1020
    const-string v0, "messageImages"

    invoke-static {v0}, Lcom/adobe/mobile/RemoteDownload;->deleteFilesInDirectory(Ljava/lang/String;)V

    .line 1021
    return-void

    .line 1025
    :cond_10
    const/4 v2, 0x0

    .line 1027
    .local v2, "jsonMessages":Lorg/json/JSONArray;
    const-string v0, "messages"

    :try_start_13
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_16
    .catch Lorg/json/JSONException; {:try_start_13 .. :try_end_16} :catch_19

    move-result-object v0

    move-object v2, v0

    .line 1031
    goto :goto_22

    .line 1029
    :catch_19
    move-exception v3

    .line 1030
    .local v3, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - Remote messages not configured, falling back to bundled messages"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1033
    .end local v3    # "ex":Lorg/json/JSONException;
    :goto_22
    const-string v0, "Messages - Using remote definition for messages"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1036
    if-eqz v2, :cond_32

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-gtz v0, :cond_41

    .line 1037
    :cond_32
    const-string v0, "messageImages"

    invoke-static {v0}, Lcom/adobe/mobile/RemoteDownload;->deleteFilesInDirectory(Ljava/lang/String;)V

    .line 1038
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_inAppMessages:Ljava/util/ArrayList;

    .line 1039
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_callbackTemplates:Ljava/util/ArrayList;

    .line 1040
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_piiRequests:Ljava/util/ArrayList;

    .line 1041
    return-void

    .line 1044
    :cond_41
    invoke-direct {p0, v2}, Lcom/adobe/mobile/MobileConfig;->loadMessagesFromJsonArray(Lorg/json/JSONArray;)V

    .line 1045
    return-void
.end method

.method private loadMessagesFromJsonArray(Lorg/json/JSONArray;)V
    .registers 13
    .param p1, "messages"    # Lorg/json/JSONArray;

    .line 1050
    :try_start_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1051
    .local v4, "tempInAppMessages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/adobe/mobile/Message;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1052
    .local v5, "tempCallbackTemplates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/adobe/mobile/Message;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1054
    .local v6, "tempPiiRequests":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/adobe/mobile/Message;>;"
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v7

    .line 1056
    .local v7, "messageCount":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_14
    if-ge v8, v7, :cond_55

    .line 1057
    invoke-virtual {p1, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    .line 1059
    .local v9, "messageJson":Lorg/json/JSONObject;
    invoke-static {v9}, Lcom/adobe/mobile/Message;->messageWithJsonObject(Lorg/json/JSONObject;)Lcom/adobe/mobile/Message;

    move-result-object v10

    .line 1061
    .local v10, "message":Lcom/adobe/mobile/Message;
    if-eqz v10, :cond_52

    .line 1062
    const-string v0, "Messages - loaded message - %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v10}, Lcom/adobe/mobile/Message;->description()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1065
    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/adobe/mobile/MessageTemplatePii;

    if-ne v0, v1, :cond_3b

    .line 1066
    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_52

    .line 1067
    :cond_3b
    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/adobe/mobile/MessageTemplateCallback;

    if-eq v0, v1, :cond_4b

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/adobe/mobile/MessageOpenURL;

    if-ne v0, v1, :cond_4f

    .line 1068
    :cond_4b
    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_52

    .line 1070
    :cond_4f
    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1056
    .end local v9    # "messageJson":Lorg/json/JSONObject;
    .end local v10    # "message":Lcom/adobe/mobile/Message;
    :cond_52
    :goto_52
    add-int/lit8 v8, v8, 0x1

    goto :goto_14

    .line 1075
    .end local v8    # "i":I
    :cond_55
    iput-object v4, p0, Lcom/adobe/mobile/MobileConfig;->_inAppMessages:Ljava/util/ArrayList;

    .line 1076
    iput-object v5, p0, Lcom/adobe/mobile/MobileConfig;->_callbackTemplates:Ljava/util/ArrayList;

    .line 1077
    iput-object v6, p0, Lcom/adobe/mobile/MobileConfig;->_piiRequests:Ljava/util/ArrayList;
    :try_end_5b
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_5b} :catch_5c

    .line 1081
    .end local v4    # "tempInAppMessages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/adobe/mobile/Message;>;"
    .end local v4
    .end local v5    # "tempCallbackTemplates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/adobe/mobile/Message;>;"
    .end local v5
    .end local v6    # "tempPiiRequests":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/adobe/mobile/Message;>;"
    .end local v6
    .end local v7    # "messageCount":I
    goto :goto_6c

    .line 1079
    :catch_5c
    move-exception v4

    .line 1080
    .local v4, "e":Lorg/json/JSONException;
    const-string v0, "Messages - Unable to parse messages JSON (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1082
    .end local v4    # "e":Lorg/json/JSONException;
    :goto_6c
    return-void
.end method

.method private loadPoiFromJsonArray(Lorg/json/JSONArray;)V
    .registers 10
    .param p1, "poiArray"    # Lorg/json/JSONArray;

    .line 991
    if-nez p1, :cond_3

    .line 992
    return-void

    .line 995
    :cond_3
    :try_start_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_pointsOfInterest:Ljava/util/List;

    .line 996
    const/4 v4, 0x0

    .local v4, "i":I
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v5

    .local v5, "count":I
    :goto_f
    if-ge v4, v5, :cond_4f

    .line 997
    invoke-virtual {p1, v4}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v6

    .line 998
    .local v6, "singlePOI":Lorg/json/JSONArray;
    new-instance v7, Ljava/util/ArrayList;

    const/4 v0, 0x4

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1000
    .local v7, "singlePoint":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1001
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1002
    const/4 v0, 0x2

    invoke-virtual {v6, v0}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1003
    const/4 v0, 0x3

    invoke-virtual {v6, v0}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1005
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_pointsOfInterest:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4c
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_4c} :catch_50

    .line 996
    .end local v6    # "singlePOI":Lorg/json/JSONArray;
    .end local v7    # "singlePoint":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v7
    add-int/lit8 v4, v4, 0x1

    goto :goto_f

    .line 1010
    .end local v4    # "i":I
    .end local v5    # "count":I
    :cond_4f
    goto :goto_60

    .line 1008
    :catch_50
    move-exception v4

    .line 1009
    .local v4, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - Unable to parse remote points of interest JSON (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1011
    .end local v4    # "ex":Lorg/json/JSONException;
    :goto_60
    return-void
.end method

.method private privacyStatusFromString(Ljava/lang/String;)Lcom/adobe/mobile/MobilePrivacyStatus;
    .registers 3
    .param p1, "string"    # Ljava/lang/String;

    .line 972
    if-eqz p1, :cond_23

    .line 973
    const-string v0, "optedin"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 974
    sget-object v0, Lcom/adobe/mobile/MobilePrivacyStatus;->MOBILE_PRIVACY_STATUS_OPT_IN:Lcom/adobe/mobile/MobilePrivacyStatus;

    return-object v0

    .line 976
    :cond_d
    const-string v0, "optedout"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 977
    sget-object v0, Lcom/adobe/mobile/MobilePrivacyStatus;->MOBILE_PRIVACY_STATUS_OPT_OUT:Lcom/adobe/mobile/MobilePrivacyStatus;

    return-object v0

    .line 979
    :cond_18
    const-string v0, "optunknown"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 980
    sget-object v0, Lcom/adobe/mobile/MobilePrivacyStatus;->MOBILE_PRIVACY_STATUS_UNKNOWN:Lcom/adobe/mobile/MobilePrivacyStatus;

    return-object v0

    .line 984
    :cond_23
    sget-object v0, Lcom/adobe/mobile/MobileConfig;->DEFAULT_PRIVACY_STATUS:Lcom/adobe/mobile/MobilePrivacyStatus;

    return-object v0
.end method

.method public static setUserDefinedConfigPath(Ljava/io/InputStream;)V
    .registers 4
    .param p0, "stream"    # Ljava/io/InputStream;

    .line 964
    sget-object v1, Lcom/adobe/mobile/MobileConfig;->_userDefinedInputStreamMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 965
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/MobileConfig;->_userDefinedInputStream:Ljava/io/InputStream;

    if-nez v0, :cond_9

    .line 966
    sput-object p0, Lcom/adobe/mobile/MobileConfig;->_userDefinedInputStream:Ljava/io/InputStream;
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_b

    .line 968
    :cond_9
    monitor-exit v1

    goto :goto_e

    :catchall_b
    move-exception v2

    monitor-exit v1

    throw v2

    .line 969
    :goto_e
    return-void
.end method

.method private updateBlacklist()V
    .registers 6

    .line 1085
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_inAppMessages:Ljava/util/ArrayList;

    if-eqz v0, :cond_39

    .line 1086
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_inAppMessages:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_39

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/adobe/mobile/Message;

    .line 1087
    .local v3, "message":Lcom/adobe/mobile/Message;
    invoke-virtual {v3}, Lcom/adobe/mobile/Message;->loadBlacklist()Ljava/util/HashMap;

    move-result-object v4

    .line 1090
    .local v4, "blackList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-virtual {v3}, Lcom/adobe/mobile/Message;->isBlacklisted()Z

    move-result v0

    if-eqz v0, :cond_38

    iget-object v0, v3, Lcom/adobe/mobile/Message;->showRule:Lcom/adobe/mobile/Messages$MessageShowRule;

    invoke-virtual {v0}, Lcom/adobe/mobile/Messages$MessageShowRule;->getValue()I

    move-result v0

    iget-object v1, v3, Lcom/adobe/mobile/Message;->messageId:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v0, v1, :cond_38

    .line 1091
    invoke-virtual {v3}, Lcom/adobe/mobile/Message;->removeFromBlacklist()V

    .line 1093
    .end local v3    # "message":Lcom/adobe/mobile/Message;
    .end local v4    # "blackList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v4
    :cond_38
    goto :goto_a

    .line 1096
    :cond_39
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_callbackTemplates:Ljava/util/ArrayList;

    if-eqz v0, :cond_72

    .line 1097
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_callbackTemplates:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_43
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_72

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/adobe/mobile/Message;

    .line 1098
    .local v3, "message":Lcom/adobe/mobile/Message;
    invoke-virtual {v3}, Lcom/adobe/mobile/Message;->loadBlacklist()Ljava/util/HashMap;

    move-result-object v4

    .line 1101
    .local v4, "blackList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-virtual {v3}, Lcom/adobe/mobile/Message;->isBlacklisted()Z

    move-result v0

    if-eqz v0, :cond_71

    iget-object v0, v3, Lcom/adobe/mobile/Message;->showRule:Lcom/adobe/mobile/Messages$MessageShowRule;

    invoke-virtual {v0}, Lcom/adobe/mobile/Messages$MessageShowRule;->getValue()I

    move-result v0

    iget-object v1, v3, Lcom/adobe/mobile/Message;->messageId:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v0, v1, :cond_71

    .line 1102
    invoke-virtual {v3}, Lcom/adobe/mobile/Message;->removeFromBlacklist()V

    .line 1104
    .end local v3    # "message":Lcom/adobe/mobile/Message;
    .end local v4    # "blackList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v4
    :cond_71
    goto :goto_43

    .line 1106
    :cond_72
    return-void
.end method


# virtual methods
.method protected downloadRemoteConfigs()V
    .registers 3

    .line 674
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getMessagesExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/MobileConfig$8;

    invoke-direct {v1, p0}, Lcom/adobe/mobile/MobileConfig$8;-><init>(Lcom/adobe/mobile/MobileConfig;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 694
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getThirdPartyCallbacksExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/MobileConfig$9;

    invoke-direct {v1, p0}, Lcom/adobe/mobile/MobileConfig$9;-><init>(Lcom/adobe/mobile/MobileConfig;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 713
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getPIIExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/MobileConfig$10;

    invoke-direct {v1, p0}, Lcom/adobe/mobile/MobileConfig$10;-><init>(Lcom/adobe/mobile/MobileConfig;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 733
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_pointsOfInterestURL:Ljava/lang/String;

    if-eqz v0, :cond_3a

    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_pointsOfInterestURL:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3a

    .line 734
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_pointsOfInterestURL:Ljava/lang/String;

    new-instance v1, Lcom/adobe/mobile/MobileConfig$11;

    invoke-direct {v1, p0}, Lcom/adobe/mobile/MobileConfig$11;-><init>(Lcom/adobe/mobile/MobileConfig;)V

    invoke-static {v0, v1}, Lcom/adobe/mobile/RemoteDownload;->remoteDownloadAsync(Ljava/lang/String;Lcom/adobe/mobile/RemoteDownload$RemoteDownloadBlock;)V

    .line 749
    :cond_3a
    return-void
.end method

.method protected getAamAnalyticsForwardingEnabled()Z
    .registers 2

    .line 655
    iget-boolean v0, p0, Lcom/adobe/mobile/MobileConfig;->_aamAnalyticsForwardingEnabled:Z

    return v0
.end method

.method protected getAamServer()Ljava/lang/String;
    .registers 2

    .line 653
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_aamServer:Ljava/lang/String;

    return-object v0
.end method

.method protected getAamTimeout()I
    .registers 2

    .line 657
    iget v0, p0, Lcom/adobe/mobile/MobileConfig;->_aamTimeout:I

    return v0
.end method

.method protected getAcquisitionAppId()Ljava/lang/String;
    .registers 2

    .line 663
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_acquisitionAppIdentifier:Ljava/lang/String;

    return-object v0
.end method

.method protected getAcquisitionServer()Ljava/lang/String;
    .registers 2

    .line 666
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_acquisitionServer:Ljava/lang/String;

    return-object v0
.end method

.method protected getAnalyticsResponseType()I
    .registers 2

    .line 635
    iget-boolean v0, p0, Lcom/adobe/mobile/MobileConfig;->_aamAnalyticsForwardingEnabled:Z

    if-eqz v0, :cond_7

    const/16 v0, 0xa

    goto :goto_8

    :cond_7
    const/4 v0, 0x0

    :goto_8
    return v0
.end method

.method protected getBackdateSessionInfoEnabled()Z
    .registers 2

    .line 528
    iget-boolean v0, p0, Lcom/adobe/mobile/MobileConfig;->_backdateSessionInfoEnabled:Z

    return v0
.end method

.method protected getBatchLimit()I
    .registers 2

    .line 535
    iget v0, p0, Lcom/adobe/mobile/MobileConfig;->_batchLimit:I

    return v0
.end method

.method protected getCallbackTemplates()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lcom/adobe/mobile/Message;>;"
        }
    .end annotation

    .line 828
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_callbackTemplates:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected getCharacterSet()Ljava/lang/String;
    .registers 2

    .line 517
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_characterSet:Ljava/lang/String;

    return-object v0
.end method

.method protected getInAppMessages()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lcom/adobe/mobile/Message;>;"
        }
    .end annotation

    .line 821
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_inAppMessages:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected getLifecycleTimeout()I
    .registers 2

    .line 531
    iget v0, p0, Lcom/adobe/mobile/MobileConfig;->_lifecycleTimeout:I

    return v0
.end method

.method protected getMarketingCloudOrganizationId()Ljava/lang/String;
    .registers 2

    .line 836
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_marketingCloudOrganizationId:Ljava/lang/String;

    return-object v0
.end method

.method protected getNetworkConnectivity(Landroid/content/Context;)Z
    .registers 9
    .param p1, "context"    # Landroid/content/Context;

    .line 1194
    const/4 v4, 0x1

    .line 1197
    .local v4, "tempNetworkConnectivity":Z
    if-eqz p1, :cond_37

    .line 1199
    const-string v0, "connectivity"

    :try_start_5
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/net/ConnectivityManager;

    .line 1201
    .local v5, "connectivityManager":Landroid/net/ConnectivityManager;
    if-eqz v5, :cond_2f

    .line 1203
    invoke-virtual {v5}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v6

    .line 1205
    .local v6, "activeNetworkInfo":Landroid/net/NetworkInfo;
    if-eqz v6, :cond_25

    .line 1207
    invoke-virtual {v6}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-virtual {v6}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_22

    const/4 v0, 0x1

    goto :goto_23

    :cond_22
    const/4 v0, 0x0

    :goto_23
    move v4, v0

    goto :goto_2e

    .line 1211
    :cond_25
    const/4 v4, 0x0

    .line 1212
    const-string v0, "Analytics - Unable to determine connectivity status due to there being no default network currently active"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1214
    .end local v6    # "activeNetworkInfo":Landroid/net/NetworkInfo;
    :goto_2e
    goto :goto_37

    .line 1216
    :cond_2f
    const-string v0, "Analytics - Unable to determine connectivity status due to the system service requested being unrecognized"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_37
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_37} :catch_38
    .catch Ljava/lang/SecurityException; {:try_start_5 .. :try_end_37} :catch_49
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_37} :catch_5a

    .line 1228
    .end local v5    # "connectivityManager":Landroid/net/ConnectivityManager;
    :cond_37
    :goto_37
    goto :goto_6a

    .line 1220
    :catch_38
    move-exception v5

    .line 1221
    .local v5, "e":Ljava/lang/NullPointerException;
    const-string v0, "Analytics - Unable to determine connectivity status due to an unexpected error (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/NullPointerException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1228
    .end local v5    # "e":Ljava/lang/NullPointerException;
    goto :goto_6a

    .line 1223
    :catch_49
    move-exception v5

    .line 1224
    .local v5, "e":Ljava/lang/SecurityException;
    const-string v0, "Analytics - Unable to access connectivity status due to a security error (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/SecurityException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1228
    .end local v5    # "e":Ljava/lang/SecurityException;
    goto :goto_6a

    .line 1226
    :catch_5a
    move-exception v5

    .line 1227
    .local v5, "e":Ljava/lang/Exception;
    const-string v0, "Analytics - Unable to access connectivity status due to an unexpected error (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1230
    .end local v5    # "e":Ljava/lang/Exception;
    :goto_6a
    return v4
.end method

.method protected getOfflineTrackingEnabled()Z
    .registers 2

    .line 525
    iget-boolean v0, p0, Lcom/adobe/mobile/MobileConfig;->_offlineTrackingEnabled:Z

    return v0
.end method

.method protected getPrivacyStatus()Lcom/adobe/mobile/MobilePrivacyStatus;
    .registers 2

    .line 622
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_privacyStatus:Lcom/adobe/mobile/MobilePrivacyStatus;

    return-object v0
.end method

.method protected getReferrerTimeout()I
    .registers 3

    .line 631
    iget v0, p0, Lcom/adobe/mobile/MobileConfig;->_referrerTimeout:I

    mul-int/lit16 v0, v0, 0x3e8

    return v0
.end method

.method protected getReportSuiteIds()Ljava/lang/String;
    .registers 2

    .line 509
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_reportSuiteIds:Ljava/lang/String;

    return-object v0
.end method

.method protected getSSL()Z
    .registers 2

    .line 521
    iget-boolean v0, p0, Lcom/adobe/mobile/MobileConfig;->_ssl:Z

    return v0
.end method

.method protected getTrackingServer()Ljava/lang/String;
    .registers 2

    .line 513
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_trackingServer:Ljava/lang/String;

    return-object v0
.end method

.method protected getVisitorIdServiceEnabled()Z
    .registers 2

    .line 840
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_marketingCloudOrganizationId:Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_marketingCloudOrganizationId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_e

    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

.method protected invokeAdobeDataCallback(Lcom/adobe/mobile/Config$MobileDataEvent;Ljava/util/Map;)V
    .registers 5
    .param p1, "event"    # Lcom/adobe/mobile/Config$MobileDataEvent;
    .param p2, "data"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/adobe/mobile/Config$MobileDataEvent;Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;)V"
        }
    .end annotation

    .line 490
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_adobeDataCallback:Lcom/adobe/mobile/Config$AdobeDataCallback;

    if-nez v0, :cond_d

    .line 491
    const-string v0, "Config - A callback has not been registered for Adobe events."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 492
    return-void

    .line 495
    :cond_d
    if-eqz p2, :cond_1a

    .line 496
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_adobeDataCallback:Lcom/adobe/mobile/Config$AdobeDataCallback;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, p2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-interface {v0, p1, v1}, Lcom/adobe/mobile/Config$AdobeDataCallback;->call(Lcom/adobe/mobile/Config$MobileDataEvent;Ljava/util/Map;)V

    goto :goto_20

    .line 498
    :cond_1a
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_adobeDataCallback:Lcom/adobe/mobile/Config$AdobeDataCallback;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/adobe/mobile/Config$AdobeDataCallback;->call(Lcom/adobe/mobile/Config$MobileDataEvent;Ljava/util/Map;)V

    .line 501
    :goto_20
    return-void
.end method

.method protected loadCachedRemotes()V
    .registers 2

    .line 918
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_messagesURL:Ljava/lang/String;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_messagesURL:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_15

    .line 919
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_messagesURL:Ljava/lang/String;

    invoke-static {v0}, Lcom/adobe/mobile/RemoteDownload;->getFileForCachedURL(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/adobe/mobile/MobileConfig;->updateMessagesData(Ljava/io/File;)V

    .line 922
    :cond_15
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_pointsOfInterestURL:Ljava/lang/String;

    if-eqz v0, :cond_2a

    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_pointsOfInterestURL:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2a

    .line 923
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_pointsOfInterestURL:Ljava/lang/String;

    invoke-static {v0}, Lcom/adobe/mobile/RemoteDownload;->getFileForCachedURL(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/adobe/mobile/MobileConfig;->updatePOIData(Ljava/io/File;)V

    .line 925
    :cond_2a
    return-void
.end method

.method protected mobileReferrerConfigured()Z
    .registers 2

    .line 481
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_acquisitionServer:Ljava/lang/String;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_acquisitionAppIdentifier:Ljava/lang/String;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_acquisitionServer:Ljava/lang/String;

    .line 482
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1a

    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_acquisitionAppIdentifier:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1a

    const/4 v0, 0x1

    goto :goto_1b

    :cond_1a
    const/4 v0, 0x0

    :goto_1b
    return v0
.end method

.method protected mobileUsingAnalytics()Z
    .registers 5

    .line 411
    sget-object v2, Lcom/adobe/mobile/MobileConfig;->_usingAnalyticsMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 412
    :try_start_3
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_usingAnalytics:Ljava/lang/Boolean;

    if-nez v0, :cond_40

    .line 413
    .line 414
    invoke-virtual {p0}, Lcom/adobe/mobile/MobileConfig;->getReportSuiteIds()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_29

    .line 415
    invoke-virtual {p0}, Lcom/adobe/mobile/MobileConfig;->getReportSuiteIds()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_29

    .line 416
    invoke-virtual {p0}, Lcom/adobe/mobile/MobileConfig;->getTrackingServer()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_29

    .line 417
    invoke-virtual {p0}, Lcom/adobe/mobile/MobileConfig;->getTrackingServer()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_29

    const/4 v0, 0x1

    goto :goto_2a

    :cond_29
    const/4 v0, 0x0

    .line 414
    :goto_2a
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_usingAnalytics:Ljava/lang/Boolean;

    .line 419
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_usingAnalytics:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_40

    .line 420
    const-string v0, "Analytics - Your config file is not set up to use Analytics(missing report suite id(s) or tracking server information)"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 423
    :cond_40
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_usingAnalytics:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_45
    .catchall {:try_start_3 .. :try_end_45} :catchall_48

    move-result v0

    monitor-exit v2

    return v0

    .line 424
    :catchall_48
    move-exception v3

    monitor-exit v2

    throw v3
.end method

.method protected mobileUsingAudienceManager()Z
    .registers 5

    .line 441
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->isWearableApp()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 442
    const/4 v0, 0x0

    return v0

    .line 444
    :cond_8
    sget-object v2, Lcom/adobe/mobile/MobileConfig;->_usingAudienceManagerMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 445
    :try_start_b
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_usingAudienceManager:Ljava/lang/Boolean;

    if-nez v0, :cond_38

    .line 446
    .line 447
    invoke-virtual {p0}, Lcom/adobe/mobile/MobileConfig;->getAamServer()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 448
    invoke-virtual {p0}, Lcom/adobe/mobile/MobileConfig;->getAamServer()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_21

    const/4 v0, 0x1

    goto :goto_22

    :cond_21
    const/4 v0, 0x0

    .line 447
    :goto_22
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_usingAudienceManager:Ljava/lang/Boolean;

    .line 450
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_usingAudienceManager:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_38

    .line 451
    const-string v0, "Audience Manager - Your config file is not set up to use Audience Manager(missing audience manager server information)"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 455
    :cond_38
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_usingAudienceManager:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_3d
    .catchall {:try_start_b .. :try_end_3d} :catchall_40

    move-result v0

    monitor-exit v2

    return v0

    .line 456
    :catchall_40
    move-exception v3

    monitor-exit v2

    throw v3
.end method

.method protected mobileUsingGooglePlayServices()Z
    .registers 4

    .line 430
    sget-object v1, Lcom/adobe/mobile/MobileConfig;->_usingGooglePlayServicesMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 431
    :try_start_3
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_usingGooglePlayServices:Ljava/lang/Boolean;

    if-nez v0, :cond_11

    .line 432
    invoke-static {}, Lcom/adobe/mobile/WearableFunctionBridge;->isGooglePlayServicesEnabled()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_usingGooglePlayServices:Ljava/lang/Boolean;

    .line 434
    :cond_11
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig;->_usingGooglePlayServices:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_19

    move-result v0

    monitor-exit v1

    return v0

    .line 435
    :catchall_19
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method protected networkConnectivity()Z
    .registers 2

    .line 1159
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->isWearableApp()Z

    move-result v0

    if-nez v0, :cond_a

    iget-boolean v0, p0, Lcom/adobe/mobile/MobileConfig;->_networkConnectivity:Z

    if-eqz v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    goto :goto_d

    :cond_c
    const/4 v0, 0x0

    :goto_d
    return v0
.end method

.method protected setPrivacyStatus(Lcom/adobe/mobile/MobilePrivacyStatus;)V
    .registers 7
    .param p1, "status"    # Lcom/adobe/mobile/MobilePrivacyStatus;

    .line 540
    if-nez p1, :cond_3

    .line 541
    return-void

    .line 545
    :cond_3
    sget-object v0, Lcom/adobe/mobile/MobilePrivacyStatus;->MOBILE_PRIVACY_STATUS_UNKNOWN:Lcom/adobe/mobile/MobilePrivacyStatus;

    if-ne p1, v0, :cond_14

    iget-boolean v0, p0, Lcom/adobe/mobile/MobileConfig;->_offlineTrackingEnabled:Z

    if-nez v0, :cond_14

    .line 546
    const-string v0, "Analytics - Cannot set privacy status to unknown when offline tracking is disabled"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 547
    return-void

    .line 551
    :cond_14
    sget-object v0, Lcom/adobe/mobile/MobilePrivacyStatus;->MOBILE_PRIVACY_STATUS_OPT_IN:Lcom/adobe/mobile/MobilePrivacyStatus;

    if-ne p1, v0, :cond_3c

    .line 552
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getAnalyticsExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/MobileConfig$1;

    invoke-direct {v1, p0}, Lcom/adobe/mobile/MobileConfig$1;-><init>(Lcom/adobe/mobile/MobileConfig;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 559
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getThirdPartyCallbacksExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/MobileConfig$2;

    invoke-direct {v1, p0}, Lcom/adobe/mobile/MobileConfig$2;-><init>(Lcom/adobe/mobile/MobileConfig;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 566
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getPIIExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/MobileConfig$3;

    invoke-direct {v1, p0}, Lcom/adobe/mobile/MobileConfig$3;-><init>(Lcom/adobe/mobile/MobileConfig;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 575
    :cond_3c
    sget-object v0, Lcom/adobe/mobile/MobilePrivacyStatus;->MOBILE_PRIVACY_STATUS_OPT_OUT:Lcom/adobe/mobile/MobilePrivacyStatus;

    if-ne p1, v0, :cond_70

    .line 576
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getAnalyticsExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/MobileConfig$4;

    invoke-direct {v1, p0}, Lcom/adobe/mobile/MobileConfig$4;-><init>(Lcom/adobe/mobile/MobileConfig;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 586
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getThirdPartyCallbacksExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/MobileConfig$5;

    invoke-direct {v1, p0}, Lcom/adobe/mobile/MobileConfig$5;-><init>(Lcom/adobe/mobile/MobileConfig;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 593
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getPIIExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/MobileConfig$6;

    invoke-direct {v1, p0}, Lcom/adobe/mobile/MobileConfig$6;-><init>(Lcom/adobe/mobile/MobileConfig;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 600
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getAudienceExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/MobileConfig$7;

    invoke-direct {v1, p0}, Lcom/adobe/mobile/MobileConfig$7;-><init>(Lcom/adobe/mobile/MobileConfig;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 609
    :cond_70
    iput-object p1, p0, Lcom/adobe/mobile/MobileConfig;->_privacyStatus:Lcom/adobe/mobile/MobilePrivacyStatus;

    .line 611
    invoke-virtual {p1}, Lcom/adobe/mobile/MobilePrivacyStatus;->getValue()I

    move-result v0

    invoke-static {v0}, Lcom/adobe/mobile/WearableFunctionBridge;->syncPrivacyStatusToWearable(I)V

    .line 613
    :try_start_79
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedPreferencesEditor()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 614
    .local v4, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v0, "PrivacyStatus"

    invoke-virtual {p1}, Lcom/adobe/mobile/MobilePrivacyStatus;->getValue()I

    move-result v1

    invoke-interface {v4, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 615
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_89
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_79 .. :try_end_89} :catch_8a

    .line 618
    .end local v4    # "editor":Landroid/content/SharedPreferences$Editor;
    goto :goto_9a

    .line 616
    :catch_8a
    move-exception v4

    .line 617
    .local v4, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Config - Error persisting privacy status (%s)."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 619
    .end local v4    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_9a
    return-void
.end method

.method protected startNotifier()V
    .registers 8

    .line 1165
    new-instance v4, Landroid/content/IntentFilter;

    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v4, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1167
    .local v4, "filter":Landroid/content/IntentFilter;
    const/4 v5, 0x0

    .line 1169
    .local v5, "appCtx":Landroid/content/Context;
    :try_start_8
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getSharedContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;
    :try_end_f
    .catch Lcom/adobe/mobile/StaticMethods$NullContextException; {:try_start_8 .. :try_end_f} :catch_12

    move-result-object v0

    move-object v5, v0

    .line 1172
    goto :goto_22

    .line 1170
    :catch_12
    move-exception v6

    .line 1171
    .local v6, "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    const-string v0, "Analytics - Error registering network receiver (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Lcom/adobe/mobile/StaticMethods$NullContextException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1174
    .end local v6    # "e":Lcom/adobe/mobile/StaticMethods$NullContextException;
    :goto_22
    if-nez v5, :cond_25

    return-void

    .line 1176
    :cond_25
    new-instance v0, Lcom/adobe/mobile/MobileConfig$13;

    invoke-direct {v0, p0}, Lcom/adobe/mobile/MobileConfig$13;-><init>(Lcom/adobe/mobile/MobileConfig;)V

    invoke-virtual {v5, v0, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1190
    return-void
.end method

.method protected updateMessagesData(Ljava/io/File;)V
    .registers 11
    .param p1, "file"    # Ljava/io/File;

    .line 753
    const/4 v5, 0x0

    .line 756
    .local v5, "fis":Ljava/io/FileInputStream;
    if-nez p1, :cond_6

    .line 772
    nop

    .line 773
    .line 778
    nop

    .line 757
    .line 776
    .line 777
    .local v6, "e":Ljava/io/IOException;
    .end local v6    # "e":Ljava/io/IOException;
    return-void

    .line 760
    :cond_6
    :try_start_6
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v5, v0

    .line 761
    invoke-direct {p0, v5}, Lcom/adobe/mobile/MobileConfig;->loadConfigFromStream(Ljava/io/InputStream;)Lorg/json/JSONObject;

    move-result-object v4

    .line 762
    .local v4, "jsonData":Lorg/json/JSONObject;
    invoke-direct {p0, v4}, Lcom/adobe/mobile/MobileConfig;->loadMessagesDataFromRemote(Lorg/json/JSONObject;)V
    :try_end_13
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_13} :catch_2c
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_13} :catch_53
    .catchall {:try_start_6 .. :try_end_13} :catchall_7a

    .line 772
    if-eqz v5, :cond_18

    .line 773
    :try_start_15
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_18} :catch_1a

    .line 778
    :cond_18
    goto/16 :goto_92

    .line 776
    :catch_1a
    move-exception v6

    .line 777
    .local v6, "e":Ljava/io/IOException;
    const-string v0, "Messages - Unable to close file stream (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 779
    .end local v6    # "e":Ljava/io/IOException;
    goto/16 :goto_92

    .line 764
    .end local v4    # "jsonData":Lorg/json/JSONObject;
    :catch_2c
    move-exception v6

    .line 765
    .local v6, "e":Lorg/json/JSONException;
    const-string v0, "Messages - Unable to read messages remote config file, falling back to bundled messages (%s)"

    const/4 v1, 0x1

    :try_start_30
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3c
    .catchall {:try_start_30 .. :try_end_3c} :catchall_7a

    .line 772
    .end local v6    # "e":Lorg/json/JSONException;
    if-eqz v5, :cond_41

    .line 773
    :try_start_3e
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_41
    .catch Ljava/io/IOException; {:try_start_3e .. :try_end_41} :catch_42

    .line 778
    :cond_41
    goto :goto_92

    .line 776
    :catch_42
    move-exception v6

    .line 777
    .local v6, "e":Ljava/io/IOException;
    const-string v0, "Messages - Unable to close file stream (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 779
    .end local v6    # "e":Ljava/io/IOException;
    goto :goto_92

    .line 767
    :catch_53
    move-exception v6

    .line 768
    .local v6, "e":Ljava/io/IOException;
    const-string v0, "Messages - Unable to open messages config file, falling back to bundled messages (%s)"

    const/4 v1, 0x1

    :try_start_57
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_63
    .catchall {:try_start_57 .. :try_end_63} :catchall_7a

    .line 772
    .end local v6    # "e":Ljava/io/IOException;
    if-eqz v5, :cond_68

    .line 773
    :try_start_65
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_68
    .catch Ljava/io/IOException; {:try_start_65 .. :try_end_68} :catch_69

    .line 778
    :cond_68
    goto :goto_92

    .line 776
    :catch_69
    move-exception v6

    .line 777
    .local v6, "e":Ljava/io/IOException;
    const-string v0, "Messages - Unable to close file stream (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 779
    .end local v6    # "e":Ljava/io/IOException;
    goto :goto_92

    .line 771
    :catchall_7a
    move-exception v7

    .line 772
    if-eqz v5, :cond_80

    .line 773
    :try_start_7d
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_80
    .catch Ljava/io/IOException; {:try_start_7d .. :try_end_80} :catch_81

    .line 778
    :cond_80
    goto :goto_91

    .line 776
    :catch_81
    move-exception v8

    .line 777
    .local v8, "e":Ljava/io/IOException;
    const-string v0, "Messages - Unable to close file stream (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v8}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 778
    .end local v8    # "e":Ljava/io/IOException;
    :goto_91
    throw v7

    .line 780
    :goto_92
    return-void
.end method

.method protected updatePOIData(Ljava/io/File;)V
    .registers 12
    .param p1, "file"    # Ljava/io/File;

    .line 787
    const/4 v5, 0x0

    .line 790
    .local v5, "fis":Ljava/io/FileInputStream;
    if-nez p1, :cond_6

    .line 810
    nop

    .line 811
    .line 816
    nop

    .line 791
    .line 814
    .line 815
    .local v6, "e":Ljava/io/IOException;
    .end local v6    # "e":Ljava/io/IOException;
    return-void

    .line 794
    :cond_6
    :try_start_6
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v5, v0

    .line 795
    invoke-direct {p0, v5}, Lcom/adobe/mobile/MobileConfig;->loadConfigFromStream(Ljava/io/InputStream;)Lorg/json/JSONObject;

    move-result-object v4

    .line 796
    .local v4, "jsonData":Lorg/json/JSONObject;
    if-eqz v4, :cond_21

    .line 797
    const-string v0, "analytics"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 798
    .local v6, "analytics":Lorg/json/JSONObject;
    const-string v0, "poi"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 799
    .local v7, "poiArray":Lorg/json/JSONArray;
    invoke-direct {p0, v7}, Lcom/adobe/mobile/MobileConfig;->loadPoiFromJsonArray(Lorg/json/JSONArray;)V
    :try_end_21
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_21} :catch_3a
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_21} :catch_61
    .catchall {:try_start_6 .. :try_end_21} :catchall_88

    .line 810
    .end local v6    # "analytics":Lorg/json/JSONObject;
    .end local v7    # "poiArray":Lorg/json/JSONArray;
    :cond_21
    if-eqz v5, :cond_26

    .line 811
    :try_start_23
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_26
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_26} :catch_28

    .line 816
    :cond_26
    goto/16 :goto_a0

    .line 814
    :catch_28
    move-exception v6

    .line 815
    .local v6, "e":Ljava/io/IOException;
    const-string v0, "Config - Unable to close file stream (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 817
    .end local v6    # "e":Ljava/io/IOException;
    goto/16 :goto_a0

    .line 802
    .end local v4    # "jsonData":Lorg/json/JSONObject;
    :catch_3a
    move-exception v6

    .line 803
    .local v6, "ex":Lorg/json/JSONException;
    const-string v0, "Config - Unable to read points of interest remote config file, falling back to bundled poi (%s)"

    const/4 v1, 0x1

    :try_start_3e
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4a
    .catchall {:try_start_3e .. :try_end_4a} :catchall_88

    .line 810
    .end local v6    # "ex":Lorg/json/JSONException;
    if-eqz v5, :cond_4f

    .line 811
    :try_start_4c
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_4f
    .catch Ljava/io/IOException; {:try_start_4c .. :try_end_4f} :catch_50

    .line 816
    :cond_4f
    goto :goto_a0

    .line 814
    :catch_50
    move-exception v6

    .line 815
    .local v6, "e":Ljava/io/IOException;
    const-string v0, "Config - Unable to close file stream (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 817
    .end local v6    # "e":Ljava/io/IOException;
    goto :goto_a0

    .line 805
    :catch_61
    move-exception v6

    .line 806
    .local v6, "ex":Ljava/io/IOException;
    const-string v0, "Config - Unable to open points of interest config file, falling back to bundled poi (%s)"

    const/4 v1, 0x1

    :try_start_65
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_71
    .catchall {:try_start_65 .. :try_end_71} :catchall_88

    .line 810
    .end local v6    # "ex":Ljava/io/IOException;
    if-eqz v5, :cond_76

    .line 811
    :try_start_73
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_76
    .catch Ljava/io/IOException; {:try_start_73 .. :try_end_76} :catch_77

    .line 816
    :cond_76
    goto :goto_a0

    .line 814
    :catch_77
    move-exception v6

    .line 815
    .local v6, "e":Ljava/io/IOException;
    const-string v0, "Config - Unable to close file stream (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 817
    .end local v6    # "e":Ljava/io/IOException;
    goto :goto_a0

    .line 809
    :catchall_88
    move-exception v8

    .line 810
    if-eqz v5, :cond_8e

    .line 811
    :try_start_8b
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_8e
    .catch Ljava/io/IOException; {:try_start_8b .. :try_end_8e} :catch_8f

    .line 816
    :cond_8e
    goto :goto_9f

    .line 814
    :catch_8f
    move-exception v9

    .line 815
    .local v9, "e":Ljava/io/IOException;
    const-string v0, "Config - Unable to close file stream (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v9}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 816
    .end local v9    # "e":Ljava/io/IOException;
    :goto_9f
    throw v8

    .line 818
    :goto_a0
    return-void
.end method

.class final Lcom/facebook/LegacyTokenHelper;
.super Ljava/lang/Object;
.source "LegacyTokenHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private cache:Landroid/content/SharedPreferences;

.field private cacheKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 91
    const-class v0, Lcom/facebook/LegacyTokenHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/LegacyTokenHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 121
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/LegacyTokenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 122
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cacheKey"    # Ljava/lang/String;

    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    const-string v0, "context"

    invoke-static {p1, v0}, Lcom/facebook/internal/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    invoke-static {p2}, Lcom/facebook/internal/Utility;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    const-string v0, "com.facebook.SharedPreferencesTokenCachingStrategy.DEFAULT_KEY"

    goto :goto_12

    :cond_11
    move-object v0, p2

    :goto_12
    iput-object v0, p0, Lcom/facebook/LegacyTokenHelper;->cacheKey:Ljava/lang/String;

    .line 132
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 133
    .local v2, "applicationContext":Landroid/content/Context;
    if-eqz v2, :cond_1c

    move-object v0, v2

    goto :goto_1d

    :cond_1c
    move-object v0, p1

    :goto_1d
    move-object p1, v0

    .line 135
    iget-object v0, p0, Lcom/facebook/LegacyTokenHelper;->cacheKey:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/LegacyTokenHelper;->cache:Landroid/content/SharedPreferences;

    .line 138
    return-void
.end method

.method private deserializeKey(Ljava/lang/String;Landroid/os/Bundle;)V
    .registers 13
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 441
    iget-object v0, p0, Lcom/facebook/LegacyTokenHelper;->cache:Landroid/content/SharedPreferences;

    const-string v1, "{}"

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 442
    .local v2, "jsonString":Ljava/lang/String;
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 444
    .local v3, "json":Lorg/json/JSONObject;
    const-string v0, "valueType"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 446
    .local v4, "valueType":Ljava/lang/String;
    const-string v0, "bool"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 447
    const-string v0, "value"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p2, p1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_264

    .line 448
    :cond_26
    const-string v0, "bool[]"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 449
    const-string v0, "value"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 450
    .local v5, "jsonArray":Lorg/json/JSONArray;
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v0

    new-array v6, v0, [Z

    .line 451
    .local v6, "array":[Z
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_3b
    array-length v0, v6

    if-ge v7, v0, :cond_47

    .line 452
    invoke-virtual {v5, v7}, Lorg/json/JSONArray;->getBoolean(I)Z

    move-result v0

    aput-boolean v0, v6, v7

    .line 451
    add-int/lit8 v7, v7, 0x1

    goto :goto_3b

    .line 454
    .end local v7    # "i":I
    :cond_47
    invoke-virtual {p2, p1, v6}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 455
    .end local v5    # "jsonArray":Lorg/json/JSONArray;
    .end local v6    # "array":[Z
    goto/16 :goto_264

    :cond_4c
    const-string v0, "byte"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_60

    .line 456
    const-string v0, "value"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    int-to-byte v0, v0

    invoke-virtual {p2, p1, v0}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    goto/16 :goto_264

    .line 457
    :cond_60
    const-string v0, "byte[]"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_87

    .line 458
    const-string v0, "value"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 459
    .local v5, "jsonArray":Lorg/json/JSONArray;
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v0

    new-array v6, v0, [B

    .line 460
    .local v6, "array":[B
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_75
    array-length v0, v6

    if-ge v7, v0, :cond_82

    .line 461
    invoke-virtual {v5, v7}, Lorg/json/JSONArray;->getInt(I)I

    move-result v0

    int-to-byte v0, v0

    aput-byte v0, v6, v7

    .line 460
    add-int/lit8 v7, v7, 0x1

    goto :goto_75

    .line 463
    .end local v7    # "i":I
    :cond_82
    invoke-virtual {p2, p1, v6}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 464
    .end local v5    # "jsonArray":Lorg/json/JSONArray;
    .end local v6    # "array":[B
    goto/16 :goto_264

    :cond_87
    const-string v0, "short"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9b

    .line 465
    const-string v0, "value"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    int-to-short v0, v0

    invoke-virtual {p2, p1, v0}, Landroid/os/Bundle;->putShort(Ljava/lang/String;S)V

    goto/16 :goto_264

    .line 466
    :cond_9b
    const-string v0, "short[]"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c2

    .line 467
    const-string v0, "value"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 468
    .local v5, "jsonArray":Lorg/json/JSONArray;
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v0

    new-array v6, v0, [S

    .line 469
    .local v6, "array":[S
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_b0
    array-length v0, v6

    if-ge v7, v0, :cond_bd

    .line 470
    invoke-virtual {v5, v7}, Lorg/json/JSONArray;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    aput-short v0, v6, v7

    .line 469
    add-int/lit8 v7, v7, 0x1

    goto :goto_b0

    .line 472
    .end local v7    # "i":I
    :cond_bd
    invoke-virtual {p2, p1, v6}, Landroid/os/Bundle;->putShortArray(Ljava/lang/String;[S)V

    .line 473
    .end local v5    # "jsonArray":Lorg/json/JSONArray;
    .end local v6    # "array":[S
    goto/16 :goto_264

    :cond_c2
    const-string v0, "int"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d5

    .line 474
    const-string v0, "value"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, p1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_264

    .line 475
    :cond_d5
    const-string v0, "int[]"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_fb

    .line 476
    const-string v0, "value"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 477
    .local v5, "jsonArray":Lorg/json/JSONArray;
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v0

    new-array v6, v0, [I

    .line 478
    .local v6, "array":[I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_ea
    array-length v0, v6

    if-ge v7, v0, :cond_f6

    .line 479
    invoke-virtual {v5, v7}, Lorg/json/JSONArray;->getInt(I)I

    move-result v0

    aput v0, v6, v7

    .line 478
    add-int/lit8 v7, v7, 0x1

    goto :goto_ea

    .line 481
    .end local v7    # "i":I
    :cond_f6
    invoke-virtual {p2, p1, v6}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 482
    .end local v5    # "jsonArray":Lorg/json/JSONArray;
    .end local v6    # "array":[I
    goto/16 :goto_264

    :cond_fb
    const-string v0, "long"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10e

    .line 483
    const-string v0, "value"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto/16 :goto_264

    .line 484
    :cond_10e
    const-string v0, "long[]"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_134

    .line 485
    const-string v0, "value"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 486
    .local v5, "jsonArray":Lorg/json/JSONArray;
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v0

    new-array v6, v0, [J

    .line 487
    .local v6, "array":[J
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_123
    array-length v0, v6

    if-ge v7, v0, :cond_12f

    .line 488
    invoke-virtual {v5, v7}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v0

    aput-wide v0, v6, v7

    .line 487
    add-int/lit8 v7, v7, 0x1

    goto :goto_123

    .line 490
    .end local v7    # "i":I
    :cond_12f
    invoke-virtual {p2, p1, v6}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 491
    .end local v5    # "jsonArray":Lorg/json/JSONArray;
    .end local v6    # "array":[J
    goto/16 :goto_264

    :cond_134
    const-string v0, "float"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_148

    .line 492
    const-string v0, "value"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-virtual {p2, p1, v0}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    goto/16 :goto_264

    .line 493
    :cond_148
    const-string v0, "float[]"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16f

    .line 494
    const-string v0, "value"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 495
    .local v5, "jsonArray":Lorg/json/JSONArray;
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v0

    new-array v6, v0, [F

    .line 496
    .local v6, "array":[F
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_15d
    array-length v0, v6

    if-ge v7, v0, :cond_16a

    .line 497
    invoke-virtual {v5, v7}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v0

    double-to-float v0, v0

    aput v0, v6, v7

    .line 496
    add-int/lit8 v7, v7, 0x1

    goto :goto_15d

    .line 499
    .end local v7    # "i":I
    :cond_16a
    invoke-virtual {p2, p1, v6}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    .line 500
    .end local v5    # "jsonArray":Lorg/json/JSONArray;
    .end local v6    # "array":[F
    goto/16 :goto_264

    :cond_16f
    const-string v0, "double"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_182

    .line 501
    const-string v0, "value"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-virtual {p2, p1, v0, v1}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    goto/16 :goto_264

    .line 502
    :cond_182
    const-string v0, "double[]"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a8

    .line 503
    const-string v0, "value"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 504
    .local v5, "jsonArray":Lorg/json/JSONArray;
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v0

    new-array v6, v0, [D

    .line 505
    .local v6, "array":[D
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_197
    array-length v0, v6

    if-ge v7, v0, :cond_1a3

    .line 506
    invoke-virtual {v5, v7}, Lorg/json/JSONArray;->getDouble(I)D

    move-result-wide v0

    aput-wide v0, v6, v7

    .line 505
    add-int/lit8 v7, v7, 0x1

    goto :goto_197

    .line 508
    .end local v7    # "i":I
    :cond_1a3
    invoke-virtual {p2, p1, v6}, Landroid/os/Bundle;->putDoubleArray(Ljava/lang/String;[D)V

    .line 509
    .end local v5    # "jsonArray":Lorg/json/JSONArray;
    .end local v6    # "array":[D
    goto/16 :goto_264

    :cond_1a8
    const-string v0, "char"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c9

    .line 510
    const-string v0, "value"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 511
    .local v5, "charString":Ljava/lang/String;
    if-eqz v5, :cond_1c7

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1c7

    .line 512
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {p2, p1, v0}, Landroid/os/Bundle;->putChar(Ljava/lang/String;C)V

    .line 514
    .end local v5    # "charString":Ljava/lang/String;
    :cond_1c7
    goto/16 :goto_264

    :cond_1c9
    const-string v0, "char[]"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1fd

    .line 515
    const-string v0, "value"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 516
    .local v5, "jsonArray":Lorg/json/JSONArray;
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v0

    new-array v6, v0, [C

    .line 517
    .local v6, "array":[C
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1de
    array-length v0, v6

    if-ge v7, v0, :cond_1f8

    .line 518
    invoke-virtual {v5, v7}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 519
    .local v8, "charString":Ljava/lang/String;
    if-eqz v8, :cond_1f5

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1f5

    .line 520
    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    aput-char v0, v6, v7

    .line 517
    .end local v8    # "charString":Ljava/lang/String;
    :cond_1f5
    add-int/lit8 v7, v7, 0x1

    goto :goto_1de

    .line 523
    .end local v7    # "i":I
    :cond_1f8
    invoke-virtual {p2, p1, v6}, Landroid/os/Bundle;->putCharArray(Ljava/lang/String;[C)V

    .line 524
    .end local v5    # "jsonArray":Lorg/json/JSONArray;
    .end local v6    # "array":[C
    goto/16 :goto_264

    :cond_1fd
    const-string v0, "string"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_210

    .line 525
    const-string v0, "value"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, p1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_264

    .line 526
    :cond_210
    const-string v0, "stringList"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_241

    .line 527
    const-string v0, "value"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 528
    .local v5, "jsonArray":Lorg/json/JSONArray;
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    .line 529
    .local v6, "numStrings":I
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 530
    .local v7, "stringList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_228
    if-ge v8, v6, :cond_23d

    .line 531
    invoke-virtual {v5, v8}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v9

    .line 532
    .local v9, "jsonStringValue":Ljava/lang/Object;
    sget-object v0, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    if-ne v9, v0, :cond_234

    const/4 v0, 0x0

    goto :goto_237

    :cond_234
    move-object v0, v9

    check-cast v0, Ljava/lang/String;

    :goto_237
    invoke-virtual {v7, v8, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 530
    .end local v9    # "jsonStringValue":Ljava/lang/Object;
    add-int/lit8 v8, v8, 0x1

    goto :goto_228

    .line 536
    .end local v8    # "i":I
    :cond_23d
    invoke-virtual {p2, p1, v7}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 537
    .end local v5    # "jsonArray":Lorg/json/JSONArray;
    .end local v6    # "numStrings":I
    .end local v7    # "stringList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7
    goto :goto_264

    :cond_241
    const-string v0, "enum"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_264

    .line 539
    const-string v0, "enumType"

    :try_start_24b
    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 541
    .local v5, "enumType":Ljava/lang/String;
    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    .line 543
    .local v6, "enumClass":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Enum;>;"
    const-string v0, "value"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v7

    .line 544
    .local v7, "enumValue":Ljava/lang/Enum;, "Ljava/lang/Enum<*>;"
    invoke-virtual {p2, p1, v7}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V
    :try_end_260
    .catch Ljava/lang/ClassNotFoundException; {:try_start_24b .. :try_end_260} :catch_261
    .catch Ljava/lang/IllegalArgumentException; {:try_start_24b .. :try_end_260} :catch_263

    .line 547
    .end local v5    # "enumType":Ljava/lang/String;
    .end local v6    # "enumClass":Ljava/lang/Class;, "Ljava/lang/Class<+Ljava/lang/Enum;>;"
    .end local v6
    .end local v7    # "enumValue":Ljava/lang/Enum;, "Ljava/lang/Enum<*>;"
    .end local v7
    goto :goto_264

    .line 545
    :catch_261
    move-exception v5

    .line 547
    goto :goto_264

    .line 546
    :catch_263
    move-exception v5

    .line 549
    :cond_264
    :goto_264
    return-void
.end method

.method public static getApplicationId(Landroid/os/Bundle;)Ljava/lang/String;
    .registers 2
    .param p0, "bundle"    # Landroid/os/Bundle;

    .line 299
    const-string v0, "bundle"

    invoke-static {p0, v0}, Lcom/facebook/internal/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 300
    const-string v0, "com.facebook.TokenCachingStrategy.ApplicationId"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static getDate(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/Date;
    .registers 6
    .param p0, "bundle"    # Landroid/os/Bundle;
    .param p1, "key"    # Ljava/lang/String;

    .line 309
    if-nez p0, :cond_4

    .line 310
    const/4 v0, 0x0

    return-object v0

    .line 313
    :cond_4
    const-wide/high16 v0, -0x8000000000000000L

    invoke-virtual {p0, p1, v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 314
    .local v2, "n":J
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, v2, v0

    if-nez v0, :cond_12

    .line 315
    const/4 v0, 0x0

    return-object v0

    .line 318
    :cond_12
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public static getSource(Landroid/os/Bundle;)Lcom/facebook/AccessTokenSource;
    .registers 3
    .param p0, "bundle"    # Landroid/os/Bundle;

    .line 263
    const-string v0, "bundle"

    invoke-static {p0, v0}, Lcom/facebook/internal/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 264
    const-string v0, "com.facebook.TokenCachingStrategy.AccessTokenSource"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 265
    const-string v0, "com.facebook.TokenCachingStrategy.AccessTokenSource"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/AccessTokenSource;

    return-object v0

    .line 267
    :cond_16
    const-string v0, "com.facebook.TokenCachingStrategy.IsSSO"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 268
    .local v1, "isSSO":Z
    if-eqz v1, :cond_21

    sget-object v0, Lcom/facebook/AccessTokenSource;->FACEBOOK_APPLICATION_WEB:Lcom/facebook/AccessTokenSource;

    goto :goto_23

    :cond_21
    sget-object v0, Lcom/facebook/AccessTokenSource;->WEB_VIEW:Lcom/facebook/AccessTokenSource;

    :goto_23
    return-object v0
.end method

.method public static getToken(Landroid/os/Bundle;)Ljava/lang/String;
    .registers 2
    .param p0, "bundle"    # Landroid/os/Bundle;

    .line 208
    const-string v0, "bundle"

    invoke-static {p0, v0}, Lcom/facebook/internal/Validate;->notNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 209
    const-string v0, "com.facebook.TokenCachingStrategy.Token"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static hasTokenInformation(Landroid/os/Bundle;)Z
    .registers 7
    .param p0, "bundle"    # Landroid/os/Bundle;

    .line 190
    if-nez p0, :cond_4

    .line 191
    const/4 v0, 0x0

    return v0

    .line 194
    :cond_4
    const-string v0, "com.facebook.TokenCachingStrategy.Token"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 195
    .local v3, "token":Ljava/lang/String;
    if-eqz v3, :cond_12

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_14

    .line 196
    :cond_12
    const/4 v0, 0x0

    return v0

    .line 199
    :cond_14
    const-string v0, "com.facebook.TokenCachingStrategy.ExpirationDate"

    const-wide/16 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 200
    .local v4, "expiresMilliseconds":J
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-nez v0, :cond_24

    .line 201
    const/4 v0, 0x0

    return v0

    .line 204
    :cond_24
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public clear()V
    .registers 2

    .line 186
    iget-object v0, p0, Lcom/facebook/LegacyTokenHelper;->cache:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 187
    return-void
.end method

.method public load()Landroid/os/Bundle;
    .registers 10

    .line 141
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 143
    .local v4, "settings":Landroid/os/Bundle;
    iget-object v0, p0, Lcom/facebook/LegacyTokenHelper;->cache:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v5

    .line 145
    .local v5, "allCachedEntries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_13
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/lang/String;

    .line 147
    .local v7, "key":Ljava/lang/String;
    :try_start_20
    invoke-direct {p0, v7, v4}, Lcom/facebook/LegacyTokenHelper;->deserializeKey(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_23
    .catch Lorg/json/JSONException; {:try_start_20 .. :try_end_23} :catch_24

    .line 153
    goto :goto_4c

    .line 148
    :catch_24
    move-exception v8

    .line 150
    .local v8, "e":Lorg/json/JSONException;
    sget-object v0, Lcom/facebook/LoggingBehavior;->CACHE:Lcom/facebook/LoggingBehavior;

    sget-object v1, Lcom/facebook/LegacyTokenHelper;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error reading cached value for key: \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' -- "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x5

    invoke-static {v0, v3, v1, v2}, Lcom/facebook/internal/Logger;->log(Lcom/facebook/LoggingBehavior;ILjava/lang/String;Ljava/lang/String;)V

    .line 152
    const/4 v0, 0x0

    return-object v0

    .line 154
    .end local v7    # "key":Ljava/lang/String;
    .end local v8    # "e":Lorg/json/JSONException;
    :goto_4c
    goto :goto_13

    .line 156
    .end local v6    # "i$":Ljava/util/Iterator;
    :cond_4d
    return-object v4
.end method

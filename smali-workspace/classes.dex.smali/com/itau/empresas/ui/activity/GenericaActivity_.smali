.class public final Lcom/itau/empresas/ui/activity/GenericaActivity_;
.super Lcom/itau/empresas/ui/activity/GenericaActivity;
.source "GenericaActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 33
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/GenericaActivity;-><init>()V

    .line 37
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/GenericaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/ui/activity/GenericaActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/GenericaActivity_;

    .line 33
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/GenericaActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/ui/activity/GenericaActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/GenericaActivity_;

    .line 33
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/GenericaActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 50
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/GenericaActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 51
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 52
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/GenericaActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 53
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/GenericaActivity_;->injectExtras_()V

    .line 54
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/GenericaActivity_;->afterInject()V

    .line 55
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 95
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/GenericaActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 96
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_1c

    .line 97
    const-string v0, "fragmentClass"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 98
    const-string v0, "fragmentClass"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/GenericaActivity_;->fragmentClass:Ljava/lang/Class;

    .line 101
    :cond_1c
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 126
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/ui/activity/GenericaActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/GenericaActivity_$2;-><init>(Lcom/itau/empresas/ui/activity/GenericaActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 134
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 114
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/ui/activity/GenericaActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/GenericaActivity_$1;-><init>(Lcom/itau/empresas/ui/activity/GenericaActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 122
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 42
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/GenericaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 43
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/activity/GenericaActivity_;->init_(Landroid/os/Bundle;)V

    .line 44
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/GenericaActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 46
    const v0, 0x7f030038

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/GenericaActivity_;->setContentView(I)V

    .line 47
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 89
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/GenericaActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 90
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/GenericaActivity_;->aoInicializar()V

    .line 91
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 59
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/GenericaActivity;->setContentView(I)V

    .line 60
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/GenericaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 61
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 71
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/GenericaActivity;->setContentView(Landroid/view/View;)V

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/GenericaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 73
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 65
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/ui/activity/GenericaActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/GenericaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 67
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 105
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/GenericaActivity;->setIntent(Landroid/content/Intent;)V

    .line 106
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/GenericaActivity_;->injectExtras_()V

    .line 107
    return-void
.end method

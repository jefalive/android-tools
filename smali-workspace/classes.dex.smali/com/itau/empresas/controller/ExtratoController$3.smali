.class Lcom/itau/empresas/controller/ExtratoController$3;
.super Ljava/lang/Object;
.source "ExtratoController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/controller/ExtratoController;->consultaLancamentos(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/controller/ExtratoController;

.field final synthetic val$dataFinal:Ljava/lang/String;

.field final synthetic val$dataInicial:Ljava/lang/String;

.field final synthetic val$idconta:Ljava/lang/String;

.field final synthetic val$reduzido:Ljava/lang/Boolean;


# direct methods
.method constructor <init>(Lcom/itau/empresas/controller/ExtratoController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    .registers 6
    .param p1, "this$0"    # Lcom/itau/empresas/controller/ExtratoController;

    .line 50
    iput-object p1, p0, Lcom/itau/empresas/controller/ExtratoController$3;->this$0:Lcom/itau/empresas/controller/ExtratoController;

    iput-object p2, p0, Lcom/itau/empresas/controller/ExtratoController$3;->val$idconta:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/controller/ExtratoController$3;->val$dataInicial:Ljava/lang/String;

    iput-object p4, p0, Lcom/itau/empresas/controller/ExtratoController$3;->val$dataFinal:Ljava/lang/String;

    iput-object p5, p0, Lcom/itau/empresas/controller/ExtratoController$3;->val$reduzido:Ljava/lang/Boolean;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 6

    .line 53
    iget-object v0, p0, Lcom/itau/empresas/controller/ExtratoController$3;->this$0:Lcom/itau/empresas/controller/ExtratoController;

    iget-object v0, v0, Lcom/itau/empresas/controller/ExtratoController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    const-string v1, "extrato"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->inicioMonitorarTempo(Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/controller/ExtratoController$3;->this$0:Lcom/itau/empresas/controller/ExtratoController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/ExtratoController;->api()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/controller/ExtratoController$3;->val$idconta:Ljava/lang/String;

    iget-object v2, p0, Lcom/itau/empresas/controller/ExtratoController$3;->val$dataInicial:Ljava/lang/String;

    iget-object v3, p0, Lcom/itau/empresas/controller/ExtratoController$3;->val$dataFinal:Ljava/lang/String;

    iget-object v4, p0, Lcom/itau/empresas/controller/ExtratoController$3;->val$reduzido:Ljava/lang/Boolean;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/itau/empresas/api/Api;->consultaLancamentos(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)Lcom/itau/empresas/api/model/ExtratoVO;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/BaseController;->post(Ljava/lang/Object;)V

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/controller/ExtratoController$3;->this$0:Lcom/itau/empresas/controller/ExtratoController;

    iget-object v0, v0, Lcom/itau/empresas/controller/ExtratoController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    const-string v1, "extrato"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->fimMonitorarTempo(Ljava/lang/String;)V

    .line 56
    return-void
.end method

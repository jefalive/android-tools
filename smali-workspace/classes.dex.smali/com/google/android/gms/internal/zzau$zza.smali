.class public Lcom/google/android/gms/internal/zzau$zza;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/zzbb;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/zzau;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "zza"
.end annotation


# instance fields
.field private zzsq:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<Lcom/google/android/gms/ads/internal/formats/zzh;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/internal/formats/zzh;)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzau$zza;->zzsq:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public zzco()Landroid/view/View;
    .registers 3

    iget-object v0, p0, Lcom/google/android/gms/internal/zzau$zza;->zzsq:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/gms/ads/internal/formats/zzh;

    if-eqz v1, :cond_10

    invoke-interface {v1}, Lcom/google/android/gms/ads/internal/formats/zzh;->zzdS()Landroid/view/View;

    move-result-object v0

    goto :goto_11

    :cond_10
    const/4 v0, 0x0

    :goto_11
    return-object v0
.end method

.method public zzcp()Z
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzau$zza;->zzsq:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method public zzcq()Lcom/google/android/gms/internal/zzbb;
    .registers 3

    new-instance v0, Lcom/google/android/gms/internal/zzau$zzb;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzau$zza;->zzsq:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/ads/internal/formats/zzh;

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/zzau$zzb;-><init>(Lcom/google/android/gms/ads/internal/formats/zzh;)V

    return-object v0
.end method

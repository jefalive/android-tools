.class public Lcom/itau/empresas/api/model/ContatoVO;
.super Ljava/lang/Object;
.source "ContatoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private apelido:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "apelido"
    .end annotation
.end field

.field private cpfCnpj:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf_cnpj"
    .end annotation
.end field

.field private email:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "email"
    .end annotation
.end field

.field private favorito:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "favorito"
    .end annotation
.end field

.field private idContato:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id_contato"
    .end annotation
.end field

.field private mesmoCpfCnpjAcesso:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "mesmo_cpf_cpnj_acesso"
    .end annotation
.end field

.field private nomeContato:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_contato"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

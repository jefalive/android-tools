.class Lcom/adobe/mobile/AnalyticsWorker$1;
.super Ljava/lang/Object;
.source "AnalyticsWorker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adobe/mobile/AnalyticsWorker;->workerThread()Ljava/lang/Runnable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adobe/mobile/AnalyticsWorker;


# direct methods
.method constructor <init>(Lcom/adobe/mobile/AnalyticsWorker;)V
    .registers 2
    .param p1, "this$0"    # Lcom/adobe/mobile/AnalyticsWorker;

    .line 230
    iput-object p1, p0, Lcom/adobe/mobile/AnalyticsWorker$1;->this$0:Lcom/adobe/mobile/AnalyticsWorker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 16

    .line 233
    invoke-static {}, Lcom/adobe/mobile/AnalyticsWorker;->sharedInstance()Lcom/adobe/mobile/AnalyticsWorker;

    move-result-object v6

    .line 236
    .local v6, "worker":Lcom/adobe/mobile/AnalyticsWorker;
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 239
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 241
    .local v7, "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "Accept-Language"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getDefaultAcceptLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v7, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 242
    const-string v0, "User-Agent"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getDefaultUserAgent()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v7, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    :goto_20
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getPrivacyStatus()Lcom/adobe/mobile/MobilePrivacyStatus;

    move-result-object v0

    sget-object v1, Lcom/adobe/mobile/MobilePrivacyStatus;->MOBILE_PRIVACY_STATUS_OPT_IN:Lcom/adobe/mobile/MobilePrivacyStatus;

    if-ne v0, v1, :cond_1ac

    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->networkConnectivity()Z

    move-result v0

    if-eqz v0, :cond_1ac

    .line 247
    invoke-virtual {v6}, Lcom/adobe/mobile/AnalyticsWorker;->selectOldestHit()Lcom/adobe/mobile/AbstractHitDatabase$Hit;

    move-result-object v8

    .line 249
    .local v8, "hit":Lcom/adobe/mobile/AbstractHitDatabase$Hit;
    if-nez v8, :cond_3e

    .line 250
    goto/16 :goto_1ac

    .line 254
    :cond_3e
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getOfflineTrackingEnabled()Z

    move-result v0

    if-eqz v0, :cond_ad

    .line 256
    iget-wide v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->timestamp:J

    iget-wide v2, v6, Lcom/adobe/mobile/AnalyticsWorker;->lastHitTimestamp:J

    sub-long v9, v0, v2

    .line 259
    .local v9, "delta":J
    const-wide/16 v0, 0x0

    cmp-long v0, v9, v0

    if-gez v0, :cond_ac

    .line 261
    iget-wide v0, v6, Lcom/adobe/mobile/AnalyticsWorker;->lastHitTimestamp:J

    const-wide/16 v2, 0x1

    add-long v11, v0, v2

    .line 262
    .local v11, "newTimestamp":J
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "&ts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->timestamp:J

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 263
    .local v13, "oldTSString":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "&ts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v11, v12}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 265
    .local v14, "newTSString":Ljava/lang/String;
    iget-object v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->urlFragment:Ljava/lang/String;

    invoke-virtual {v0, v13, v14}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->urlFragment:Ljava/lang/String;

    .line 267
    const-string v0, "Analytics - Adjusting out of order hit timestamp(%d->%d)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-wide v2, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->timestamp:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 269
    iput-wide v11, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->timestamp:J

    .line 271
    .end local v9    # "delta":J
    .end local v11    # "newTimestamp":J
    .end local v13    # "oldTSString":Ljava/lang/String;
    .end local v14    # "newTSString":Ljava/lang/String;
    :cond_ac
    goto :goto_cb

    .line 273
    :cond_ad
    iget-wide v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->timestamp:J

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getTimeSince1970()J

    move-result-wide v2

    const-wide/16 v4, 0x3c

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_cb

    .line 279
    :try_start_ba
    iget-object v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->identifier:Ljava/lang/String;

    invoke-virtual {v6, v0}, Lcom/adobe/mobile/AnalyticsWorker;->deleteHit(Ljava/lang/String;)V
    :try_end_bf
    .catch Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException; {:try_start_ba .. :try_end_bf} :catch_c1

    .line 284
    goto/16 :goto_20

    .line 281
    :catch_c1
    move-exception v9

    .line 282
    .local v9, "ex":Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException;
    invoke-static {}, Lcom/adobe/mobile/AnalyticsWorker;->sharedInstance()Lcom/adobe/mobile/AnalyticsWorker;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/adobe/mobile/AnalyticsWorker;->resetDatabase(Ljava/lang/Exception;)V

    .line 283
    goto/16 :goto_1ac

    .line 294
    .end local v9    # "ex":Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException;
    :cond_cb
    :goto_cb
    iget-object v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->urlFragment:Ljava/lang/String;

    const-string v1, "ndh"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d8

    iget-object v11, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->urlFragment:Ljava/lang/String;

    goto :goto_e8

    :cond_d8
    iget-object v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->urlFragment:Ljava/lang/String;

    iget-object v1, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->urlFragment:Ljava/lang/String;

    const/16 v2, 0x3f

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    .line 297
    .local v11, "postBody":Ljava/lang/String;
    :goto_e8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    # invokes: Lcom/adobe/mobile/AnalyticsWorker;->getBaseURL()Ljava/lang/String;
    invoke-static {}, Lcom/adobe/mobile/AnalyticsWorker;->access$000()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    # getter for: Lcom/adobe/mobile/AnalyticsWorker;->randomGen:Ljava/security/SecureRandom;
    invoke-static {}, Lcom/adobe/mobile/AnalyticsWorker;->access$100()Ljava/security/SecureRandom;

    move-result-object v1

    const v2, 0x5f5e100

    invoke-virtual {v1, v2}, Ljava/security/SecureRandom;->nextInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/adobe/mobile/AnalyticsWorker$1;->this$0:Lcom/adobe/mobile/AnalyticsWorker;

    iget-object v1, v1, Lcom/adobe/mobile/AnalyticsWorker;->logPrefix:Ljava/lang/String;

    const/16 v2, 0x1388

    invoke-static {v0, v11, v7, v2, v1}, Lcom/adobe/mobile/RequestHandler;->retrieveAnalyticsRequestData(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;ILjava/lang/String;)[B

    move-result-object v12

    .line 299
    .local v12, "response":[B
    if-nez v12, :cond_118

    .line 301
    const-wide/16 v9, 0x1e

    .local v9, "delay":J
    goto/16 :goto_181

    .line 302
    .end local v9    # "delay":J
    :cond_118
    array-length v0, v12

    const/4 v1, 0x1

    if-le v0, v1, :cond_16d

    .line 305
    :try_start_11c
    iget-object v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->identifier:Ljava/lang/String;

    invoke-virtual {v6, v0}, Lcom/adobe/mobile/AnalyticsWorker;->deleteHit(Ljava/lang/String;)V

    .line 306
    iget-wide v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->timestamp:J

    iput-wide v0, v6, Lcom/adobe/mobile/AnalyticsWorker;->lastHitTimestamp:J

    .line 308
    new-instance v13, Ljava/lang/String;

    const-string v0, "UTF-8"

    invoke-direct {v13, v12, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 309
    .local v13, "responseString":Ljava/lang/String;
    new-instance v14, Lorg/json/JSONObject;

    invoke-direct {v14, v13}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 312
    .local v14, "jsonResponse":Lorg/json/JSONObject;
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getAudienceExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/adobe/mobile/AnalyticsWorker$1$1;

    invoke-direct {v1, p0, v14}, Lcom/adobe/mobile/AnalyticsWorker$1$1;-><init>(Lcom/adobe/mobile/AnalyticsWorker$1;Lorg/json/JSONObject;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_13d
    .catch Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException; {:try_start_11c .. :try_end_13d} :catch_13f
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_11c .. :try_end_13d} :catch_149
    .catch Lorg/json/JSONException; {:try_start_11c .. :try_end_13d} :catch_15b

    .line 325
    .end local v13    # "responseString":Ljava/lang/String;
    .end local v14    # "jsonResponse":Lorg/json/JSONObject;
    goto/16 :goto_20

    .line 318
    :catch_13f
    move-exception v13

    .line 319
    .local v13, "ex":Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException;
    invoke-static {}, Lcom/adobe/mobile/AnalyticsWorker;->sharedInstance()Lcom/adobe/mobile/AnalyticsWorker;

    move-result-object v0

    invoke-virtual {v0, v13}, Lcom/adobe/mobile/AnalyticsWorker;->resetDatabase(Ljava/lang/Exception;)V

    .line 320
    goto/16 :goto_1ac

    .line 321
    .end local v13    # "ex":Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException;
    :catch_149
    move-exception v13

    .line 322
    .local v13, "e":Ljava/io/UnsupportedEncodingException;
    const-string v0, "Audience Manager - Unable to decode server response (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v13}, Ljava/io/UnsupportedEncodingException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 325
    .end local v13    # "e":Ljava/io/UnsupportedEncodingException;
    goto/16 :goto_20

    .line 323
    :catch_15b
    move-exception v13

    .line 324
    .local v13, "e":Lorg/json/JSONException;
    const-string v0, "Audience Manager - Unable to parse JSON data (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v13}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 326
    .end local v13    # "e":Lorg/json/JSONException;
    goto/16 :goto_20

    .line 330
    :cond_16d
    :try_start_16d
    iget-object v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->identifier:Ljava/lang/String;

    invoke-virtual {v6, v0}, Lcom/adobe/mobile/AnalyticsWorker;->deleteHit(Ljava/lang/String;)V

    .line 331
    iget-wide v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->timestamp:J

    iput-wide v0, v6, Lcom/adobe/mobile/AnalyticsWorker;->lastHitTimestamp:J
    :try_end_176
    .catch Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException; {:try_start_16d .. :try_end_176} :catch_178

    .line 336
    goto/16 :goto_20

    .line 333
    :catch_178
    move-exception v13

    .line 334
    .local v13, "ex":Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException;
    invoke-static {}, Lcom/adobe/mobile/AnalyticsWorker;->sharedInstance()Lcom/adobe/mobile/AnalyticsWorker;

    move-result-object v0

    invoke-virtual {v0, v13}, Lcom/adobe/mobile/AnalyticsWorker;->resetDatabase(Ljava/lang/Exception;)V

    .line 335
    goto :goto_1ac

    .line 342
    .local v9, "delay":J
    .end local v13    # "ex":Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException;
    :goto_181
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_182
    int-to-long v0, v13

    cmp-long v0, v0, v9

    if-gez v0, :cond_199

    :try_start_187
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->networkConnectivity()Z

    move-result v0

    if-eqz v0, :cond_199

    .line 343
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_196
    .catch Ljava/lang/Exception; {:try_start_187 .. :try_end_196} :catch_19a

    .line 342
    add-int/lit8 v13, v13, 0x1

    goto :goto_182

    .line 348
    .end local v13    # "i":I
    :cond_199
    goto :goto_1aa

    .line 346
    :catch_19a
    move-exception v13

    .line 347
    .local v13, "e":Ljava/lang/Exception;
    const-string v0, "Analytics - Background Thread Interrupted(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v13}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 349
    .end local v8    # "hit":Lcom/adobe/mobile/AbstractHitDatabase$Hit;
    .end local v9    # "delay":J
    .end local v11    # "postBody":Ljava/lang/String;
    .end local v12    # "response":[B
    .end local v13    # "e":Ljava/lang/Exception;
    :goto_1aa
    goto/16 :goto_20

    .line 352
    :cond_1ac
    :goto_1ac
    const/4 v0, 0x0

    iput-boolean v0, v6, Lcom/adobe/mobile/AnalyticsWorker;->bgThreadActive:Z

    .line 353
    return-void
.end method

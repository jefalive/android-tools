.class public final Lsc/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljavax/net/ssl/HostnameVerifier;


# static fields
.field private static c:B

.field private static d:B

.field private static e:B

.field private static f:B

.field private static final g:[[Ljava/lang/String;

.field private static final h:[S

.field private static i:I


# instance fields
.field final a:Ljavax/net/ssl/SSLSocketFactory;

.field private b:I


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .line 34
    const/16 v0, 0x2e0

    new-array v0, v0, [S

    fill-array-data v0, :array_1c2

    sput-object v0, Lsc/e;->h:[S

    const/16 v0, 0xb8

    sput v0, Lsc/e;->i:I

    const/4 v0, 0x0

    const/16 v1, 0xf1

    const/16 v2, 0x10

    invoke-static {v0, v1, v2}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Byte;->parseByte(Ljava/lang/String;)B

    move-result v0

    sput-byte v0, Lsc/e;->c:B

    .line 35
    const/4 v0, 0x0

    const/16 v1, 0xc7

    const/16 v2, 0x11

    invoke-static {v0, v1, v2}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Byte;->parseByte(Ljava/lang/String;)B

    move-result v0

    sput-byte v0, Lsc/e;->d:B

    .line 36
    const/4 v0, 0x0

    const/16 v1, 0xa0

    const/16 v2, 0x12

    invoke-static {v0, v1, v2}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Byte;->parseByte(Ljava/lang/String;)B

    move-result v0

    sput-byte v0, Lsc/e;->e:B

    .line 37
    const/4 v0, 0x0

    const/16 v1, 0x80

    const/16 v2, 0x13

    invoke-static {v0, v1, v2}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Byte;->parseByte(Ljava/lang/String;)B

    move-result v0

    sput-byte v0, Lsc/e;->f:B

    .line 39
    const/4 v0, 0x6

    new-array v0, v0, [[Ljava/lang/String;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/16 v2, 0x13

    const/16 v3, 0x22

    const/16 v4, 0x4f

    invoke-static {v2, v3, v4}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lsc/e;->h:[S

    const/4 v3, 0x0

    aget-short v2, v2, v3

    add-int/lit8 v2, v2, 0x1

    int-to-byte v2, v2

    sget v3, Lsc/e;->i:I

    or-int/lit16 v3, v3, 0x204

    int-to-short v3, v3

    const/16 v4, 0x49

    invoke-static {v2, v3, v4}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const/16 v2, 0x1f

    const/16 v3, 0x233

    const/16 v4, 0x11

    invoke-static {v2, v3, v4}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const/16 v2, 0x27

    const/16 v3, 0x292

    const/16 v4, 0x14

    invoke-static {v2, v3, v4}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/16 v2, 0x13

    const/16 v3, 0x22

    const/16 v4, 0x4f

    invoke-static {v2, v3, v4}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/16 v2, 0xe

    const/16 v3, 0x18d

    const/16 v4, 0x4b

    invoke-static {v2, v3, v4}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const/16 v2, 0x1f

    const/16 v3, 0x252

    const/16 v4, 0x14

    invoke-static {v2, v3, v4}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    sget v2, Lsc/e;->i:I

    and-int/lit8 v2, v2, 0x54

    int-to-short v2, v2

    const/16 v3, 0x27

    const/16 v4, 0x127

    invoke-static {v3, v4, v2}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/16 v2, 0x13

    const/16 v3, 0x22

    const/16 v4, 0x4f

    invoke-static {v2, v3, v4}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/16 v2, 0xf

    const/16 v3, 0xf

    const/16 v4, 0x57

    invoke-static {v2, v3, v4}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    sget-object v2, Lsc/e;->h:[S

    const/4 v3, 0x3

    aget-short v2, v2, v3

    int-to-short v2, v2

    const/16 v3, 0x1f

    const/16 v4, 0x4e

    invoke-static {v3, v4, v2}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const/16 v2, 0x27

    const/16 v3, 0x214

    const/16 v4, 0x19

    invoke-static {v2, v3, v4}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/16 v2, 0x13

    const/16 v3, 0x22

    const/16 v4, 0x4f

    invoke-static {v2, v3, v4}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/16 v2, 0xe

    const/16 v3, 0x1d0

    const/16 v4, 0x4b

    invoke-static {v2, v3, v4}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const/16 v2, 0x1f

    const/16 v3, 0xf1

    const/16 v4, 0x12

    invoke-static {v2, v3, v4}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    sget v2, Lsc/e;->i:I

    and-int/lit8 v2, v2, 0x5e

    int-to-short v2, v2

    const/16 v3, 0x27

    const/16 v4, 0x1b4

    invoke-static {v3, v4, v2}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/16 v2, 0x13

    const/16 v3, 0x22

    const/16 v4, 0x4f

    invoke-static {v2, v3, v4}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/16 v2, 0xf

    const/16 v3, 0x100

    const/16 v4, 0x57

    invoke-static {v2, v3, v4}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const/16 v2, 0x1f

    const/16 v3, 0x17f

    const/16 v4, 0x11

    invoke-static {v2, v3, v4}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const/16 v2, 0x27

    const/16 v3, 0xc7

    const/16 v4, 0x41

    invoke-static {v2, v3, v4}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const/16 v2, 0x12

    const/4 v3, 0x0

    const/16 v4, 0x4f

    invoke-static {v2, v3, v4}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/16 v2, 0xe

    const/16 v3, 0x2b2

    const/16 v4, 0x57

    invoke-static {v2, v3, v4}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    sget v2, Lsc/e;->i:I

    and-int/lit8 v2, v2, 0x60

    int-to-byte v2, v2

    shl-int/lit8 v3, v2, 0x2

    int-to-short v3, v3

    or-int/lit16 v4, v3, 0x1f6e

    int-to-short v4, v4

    invoke-static {v2, v3, v4}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const/16 v2, 0x27

    const/16 v3, 0x15e

    const/16 v4, 0x43

    invoke-static {v2, v3, v4}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sput-object v0, Lsc/e;->g:[[Ljava/lang/String;

    return-void

    :array_1c2
    .array-data 2
        0x9s
        -0xds
        -0x32s
        0x17s
        -0xfb5s
        -0x100as
        -0xfc9s
        -0xfc6s
        -0xfc3s
        -0xf7fs
        -0xfcbs
        -0xfb5s
        -0x100as
        -0xfc9s
        -0xfc6s
        -0xfc3s
        -0xf81s
        -0xfc9s
        -0xfcds
        -0xfafs
        -0xfd6s
        -0xf7bs
        -0xff7s
        -0xfces
        -0xfc0s
        -0xf83s
        -0xff6s
        -0xfd2s
        -0xfc2s
        -0xfc2s
        -0xf79s
        -0xffds
        -0xfcds
        -0xfafs
        -0xfd6s
        -0xf7bs
        -0xff7s
        -0xfces
        -0xfc0s
        -0xf83s
        -0xff6s
        -0xfd2s
        -0xfb5s
        -0x1009s
        -0xfc0s
        -0xfd1s
        -0xf92s
        -0xfccs
        -0xfc7s
        -0xfc2s
        -0xfe4s
        -0xfafs
        -0xfcas
        -0xfbds
        -0xfc0s
        -0xfd3s
        -0xf8fs
        -0xfeds
        -0xfcas
        -0xfc1s
        -0xff3s
        -0xfbfs
        -0xf96s
        -0xff0s
        -0xf93s
        -0xfc4s
        -0xfc3s
        -0xfbes
        -0xfc7s
        -0xfc2s
        -0xfebs
        -0xf91s
        -0xff4s
        -0xfc2s
        -0xfc5s
        -0xf91s
        -0xff3s
        -0xf91s
        -0xfc5s
        -0xfbbs
        -0xff6s
        -0xfbes
        -0xfc5s
        -0xf93s
        -0xfc3s
        -0xfees
        -0xfc5s
        -0xf90s
        -0xff3s
        -0xf97s
        -0xfebs
        -0xf96s
        -0xfc3s
        -0xfbfs
        -0xfc2s
        -0xfefs
        -0xf96s
        -0xff2s
        -0xf92s
        -0xfb8s
        -0xfc0s
        -0xfc9s
        -0xfc7s
        -0xfc3s
        -0xfc0s
        -0xf70s
        -0xfb4s
        -0xfcfs
        -0xfb9s
        -0xfbas
        -0xfcds
        -0xf76s
        -0xfe3s
        -0xf73s
        -0xff5s
        -0xfc2s
        -0xfbbs
        -0xfb9s
        -0xfees
        -0xfc1s
        -0xfc8s
        -0xfb3s
        -0xfd5s
        -0xfbes
        -0xfc1s
        -0xfd0s
        -0xfb7s
        -0xfcfs
        -0xfb7s
        -0xfcds
        -0xfafs
        -0xfc2s
        -0xfd5s
        -0xfc3s
        -0xfb3s
        -0xfd1s
        -0xfc4s
        -0xfb3s
        -0xfbcs
        -0xfcas
        -0xfcbs
        -0xfc3s
        -0xfb1s
        -0xfd4s
        -0xfb0s
        -0xfc9s
        -0xfbbs
        -0xfc7s
        -0xfd1s
        -0xfb5s
        -0xfc1s
        -0xfcfs
        -0xfb4s
        -0xfcds
        -0xfb2s
        -0xfc9s
        -0xfces
        -0xfbfs
        -0xfc5s
        -0xfafs
        -0xfcas
        -0xfces
        -0xfafs
        -0xfc6s
        -0xfc3s
        -0xfcfs
        -0xfc3s
        -0xfb5s
        -0xfccs
        -0xfc2s
        -0xfb4s
        -0xfcfs
        -0xfb3s
        -0xfc1s
        -0xfc6s
        -0xfc5s
        -0xfcds
        -0xfc3s
        -0xfc1s
        -0xfc2s
        -0xfb5s
        -0xfces
        -0xfb3s
        -0xfd1s
        -0xfb4s
        -0xfcfs
        -0xfbas
        -0xfc7s
        -0xfbds
        -0xfc1s
        -0xfc9s
        -0xfbcs
        -0xff7s
        -0xfbds
        -0xf95s
        -0xff4s
        -0xfc2s
        -0xf8cs
        -0xff3s
        -0xfc7s
        -0xf8ds
        -0xfcas
        -0xfbas
        -0xfc1s
        -0xfc2s
        -0xfc8s
        -0xfbds
        -0xfcas
        -0xfecs
        -0xfc3s
        -0xfc2s
        -0xf8fs
        -0xfc2s
        -0xfcas
        -0xfbfs
        -0xfc5s
        -0xfefs
        -0xf92s
        -0xfc4s
        -0xfbcs
        -0xff3s
        -0xf95s
        -0xfc5s
        -0xfbbs
        -0xfc0s
        -0xfbas
        -0xfc9s
        -0xfcfs
        -0xfc2s
        -0xfbfs
        -0xfc5s
        -0xf70s
        -0x1018s
        -0xfads
        -0xfcds
        -0xfbfs
        -0xfbds
        -0xfbfs
        -0xfd5s
        -0xfb7s
        -0xfc7s
        -0xfbbs
        -0xf7bs
        -0x1005s
        -0xfc4s
        -0xfcfs
        -0xfc4s
        -0xfb7s
        -0xfbfs
        -0xfc5s
        -0xfbcs
        -0xfc0s
        -0xfd5s
        -0xfb3s
        -0xfc4s
        -0xfc8s
        -0xf7ds
        -0xffds
        -0xfcds
        -0xfafs
        -0xfd6s
        -0xf7bs
        -0xff7s
        -0xfces
        -0xfc0s
        -0xf83s
        -0xff6s
        -0xfd2s
        -0xfc2s
        -0xfc2s
        -0xfc2s
        -0xfd6s
        -0xfb8s
        -0xfc0s
        -0xfc9s
        -0xfc7s
        -0xfc3s
        -0xfc0s
        -0xf8as
        -0xfa8s
        -0xfc2s
        -0xfc2s
        -0xfeds
        -0xf94s
        -0xfc3s
        -0xfefs
        -0xf94s
        -0xff3s
        -0xf8fs
        -0xff4s
        -0xfc2s
        -0xf8ds
        -0xff3s
        -0xf91s
        -0xff5s
        -0xf93s
        -0xfc3s
        -0xfbds
        -0xfc4s
        -0xfc8s
        -0xfecs
        -0xf94s
        -0xfc4s
        -0xfbcs
        -0xfc5s
        -0xff5s
        -0xfbfs
        -0xf98s
        -0xfbds
        -0xfc4s
        -0xfc1s
        -0xfc3s
        -0xff1s
        -0xf8ds
        -0xfc8s
        -0xfc4s
        -0xfbcs
        -0xfc1s
        -0xfc8s
        -0xfeds
        -0xf90s
        -0xfc4s
        -0xfc8s
        -0xf7ds
        -0x1006s
        -0xfb4s
        -0xfc6s
        -0xfc3s
        -0xf87s
        -0xff7s
        -0xfces
        -0xfc0s
        -0xf83s
        -0xff6s
        -0xfd2s
        -0xfc6s
        -0xfd2s
        -0xfb6s
        -0xfccs
        -0xfc5s
        -0xfbfs
        -0xfb4s
        -0xfbfs
        -0xfd6s
        -0xfc0s
        -0xfb0s
        -0xfd4s
        -0xfb0s
        -0xfc5s
        -0xfc4s
        -0xfc3s
        -0xfbds
        -0xfd2s
        -0xfafs
        -0xfc9s
        -0xfbcs
        -0xfd4s
        -0xfb7s
        -0xfbas
        -0xfcbs
        -0xfccs
        -0xfb0s
        -0xfd4s
        -0xfc2s
        -0xfb8s
        -0xfces
        -0xfdfs
        -0xfa5s
        -0xf90s
        -0xff6s
        -0xfbfs
        -0xf90s
        -0xff3s
        -0xf9as
        -0xfecs
        -0xfc5s
        -0xfc2s
        -0xf95s
        -0xfbes
        -0xfbds
        -0xff7s
        -0xf8ds
        -0xff3s
        -0xf98s
        -0xfbds
        -0xff3s
        -0xf97s
        -0xff0s
        -0xfc0s
        -0xf91s
        -0xff0s
        -0xfc7s
        -0xfc2s
        -0xf8fs
        -0xfc7s
        -0xfbbs
        -0xff6s
        -0xf96s
        -0xfbds
        -0xff2s
        -0xf93s
        -0xfc3s
        -0xfc4s
        -0xfbas
        -0xfc3s
        -0xfc6s
        -0xff0s
        -0xfa8s
        -0xfc2s
        -0xfbbs
        -0xfc2s
        -0xfc2s
        -0xfd1s
        -0xfc3s
        -0xfc7s
        -0xf6es
        -0x1003s
        -0xfc7s
        -0xfd0s
        -0xfb3s
        -0xfcfs
        -0xf8as
        -0xfa8s
        -0xfc7s
        -0xfc5s
        -0xfbes
        -0xff3s
        -0xfc0s
        -0xf93s
        -0xff4s
        -0xf91s
        -0xfc1s
        -0xfc6s
        -0xfbes
        -0xfc0s
        -0xff5s
        -0xf92s
        -0xfc5s
        -0xfebs
        -0xf97s
        -0xfefs
        -0xf93s
        -0xfc7s
        -0xfecs
        -0xf97s
        -0xfbfs
        -0xfbes
        -0xfc2s
        -0xff4s
        -0xf96s
        -0xfbds
        -0xff2s
        -0xf90s
        -0xfc7s
        -0xfbds
        -0xfc9s
        -0xfbcs
        -0xff6s
        -0xf90s
        -0xff3s
        -0xfc2s
        -0xf93s
        -0xfc2s
        -0xf82s
        -0xfbbs
        -0xfc0s
        -0xffds
        -0xfcds
        -0xfafs
        -0xfd6s
        -0xf7bs
        -0xff7s
        -0xfces
        -0xfc0s
        -0xf83s
        -0xff6s
        -0xfd2s
        -0xfc8s
        -0xfcds
        -0xfb1s
        -0xfc4s
        -0xfd2s
        -0xfc0s
        -0xfb0s
        -0xfd5s
        -0xfc3s
        -0xfc2s
        -0xfb2s
        -0xfc5s
        -0xfd0s
        -0xfb2s
        -0xfces
        -0xfbas
        -0xfbfs
        -0xfc0s
        -0xfc4s
        -0xfc4s
        -0xfc3s
        -0xfbcs
        -0xfc2s
        -0xfd0s
        -0xfb8s
        -0xfc0s
        -0xfd0s
        -0xfc4s
        -0xfb2s
        -0xfd3s
        -0xfads
        -0xfc2s
        -0xfc2s
        -0xfc2s
        -0xfd6s
        -0xfb4s
        -0xfcfs
        -0xfb9s
        -0xfbas
        -0xfcds
        -0xf90s
        -0xfa8s
        -0xf9as
        -0xfc2s
        -0xfefs
        -0xfc1s
        -0xfc2s
        -0xf8ds
        -0xff6s
        -0xf95s
        -0xfbds
        -0xfc8s
        -0xfecs
        -0xfc5s
        -0xfc2s
        -0xfc3s
        -0xfbds
        -0xf98s
        -0xff0s
        -0xf8ds
        -0xff5s
        -0xf8fs
        -0xfcbs
        -0xfbds
        -0xff1s
        -0xf96s
        -0xfees
        -0xf95s
        -0xfbes
        -0xfc6s
        -0xff2s
        -0xf90s
        -0xfc3s
        -0xfc3s
        -0xff0s
        -0xf97s
        -0xfbds
        -0xfc1s
        -0xfc4s
        -0xfefs
        -0xfc6s
        -0xfees
        -0xfc8s
        -0xfb9s
        -0xfbas
        -0xf7es
        -0x1010s
        -0xfc3s
        -0xfc7s
        -0xf6es
        -0x100bs
        -0xfc7s
        -0xfbds
        -0xfcds
        -0xfb7s
        -0xfbas
        -0xfcds
        -0xfbfs
        -0xfd3s
        -0xfads
        -0xf7ds
        -0xff5s
        -0xfc2s
        -0xfbbs
        -0xfb9s
        -0xfees
        -0xfc1s
        -0xfc8s
        -0xfb3s
        -0xfd5s
        -0xfbes
        -0xfcds
        -0xfb1s
        0x1019s
        -0xff5s
        -0xf8es
        -0xfc8s
        -0xfc0s
        -0xfc3s
        -0xff1s
        -0xfc0s
        -0xf95s
        -0xfbfs
        -0xff4s
        -0xf8ds
        -0xff4s
        -0xf93s
        -0xfbes
        -0xfc5s
        -0xfc5s
        -0xfbes
        -0xff4s
        -0xf91s
        -0xfc1s
        -0xfc4s
        -0xfc5s
        -0xfc1s
        -0xfc5s
        -0xfbcs
        -0xff4s
        -0xfbfs
        -0xfc4s
        -0xfbfs
        -0xf93s
        -0xfc0s
        -0xfc4s
        -0xfcfs
        -0xfc4s
        -0xf6es
        -0x1006s
        -0xfcds
        -0xfb8s
        -0xfd0s
        -0xf6fs
        -0x1010s
        -0xfc3s
        -0xfc7s
        -0xf6es
        -0x100fs
        -0xfb6s
        -0xfd5s
        -0xfb1s
        -0xfc7s
        -0xfccs
        -0xfb1s
        -0xfc3s
        -0xfc8s
        -0xfc3s
        -0xfcds
        -0xfb4s
        -0xfc2s
        -0xfc4s
        -0xfcfs
        -0xfb7s
        -0xfc1s
        -0xfcfs
        -0xfb6s
        -0xfbfs
        -0xfc2s
        -0xfc2s
        -0xfc4s
        -0xfcds
        -0xfafs
        -0xfcas
        -0xfc2s
        -0xfbas
        -0xfd3s
        -0xfc3s
        -0xfc6s
        -0xfb0s
        -0xfc4s
        -0xfc5s
        -0xfc2s
        -0xfbfs
        -0xfc4s
        -0xfcfs
        -0xfc4s
        -0xf6es
        -0x1010s
        -0xfc3s
        -0xfc7s
        -0xf6es
        -0x100fs
        -0xfb6s
        -0xfd5s
        -0xfb1s
        -0xfc7s
        -0xf90s
        -0xfees
        -0xfcds
        -0xfafs
        -0xfd6s
        -0xf6ds
        -0x1017s
        -0xfbbs
        -0xfbds
        -0xfbbs
        -0xfc1s
        -0xfcfs
        -0xfb7s
        -0xfces
        -0xf73s
        -0x1015s
        -0xf7ds
        -0xff5s
        -0xf8fs
        -0xfc2s
        -0xf82s
        -0xfbbs
        -0xfc0s
        -0x1006s
        -0xfb4s
        -0xfc6s
        -0xfc3s
        -0xf87s
        -0xff7s
        -0xfces
        -0xfc0s
        -0xf83s
        -0xff6s
        -0xfd2s
        -0xf90s
        -0xfces
        -0xfeds
        -0xfafs
        -0xfd6s
        -0xf6ds
        -0xff7s
        -0xfdbs
        -0xfbds
        -0xfbbs
        -0xfc1s
        -0xfcfs
        -0xfb7s
        -0xfces
        -0xf73s
        -0xff5s
        -0xf9ds
        -0xfd5s
    .end array-data
.end method

.method private constructor <init>()V
    .registers 4

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    const/16 v1, 0xf1

    const/16 v2, 0x10

    invoke-static {v0, v1, v2}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lsc/e;->b:I

    .line 65
    invoke-direct {p0}, Lsc/e;->c()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    iput-object v0, p0, Lsc/e;->a:Ljavax/net/ssl/SSLSocketFactory;

    .line 66
    return-void
.end method

.method synthetic constructor <init>(B)V
    .registers 2

    .line 29
    invoke-direct {p0}, Lsc/e;-><init>()V

    return-void
.end method

.method private static a(Ljava/security/cert/X509Certificate;)I
    .registers 8

    .line 138
    const/4 v4, 0x1

    .line 139
    invoke-virtual {p0}, Ljava/security/cert/X509Certificate;->getSerialNumber()Ljava/math/BigInteger;

    move-result-object v0

    .line 140
    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v5

    .line 142
    const/4 v0, 0x2

    const/16 v1, 0x1b6

    const/16 v2, 0x21

    invoke-static {v0, v1, v2}, Lsc/e;->a(ISI)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lsc/e;->h:[S

    const/4 v2, 0x1

    aget-short v1, v1, v2

    neg-int v1, v1

    int-to-byte v1, v1

    const/16 v2, 0x134

    const/16 v3, 0x3f

    invoke-static {v1, v2, v3}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/security/cert/X509Certificate;->getNotAfter()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 143
    const/4 v0, 0x2

    const/16 v1, 0x1b6

    const/16 v2, 0x21

    invoke-static {v0, v1, v2}, Lsc/e;->a(ISI)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0xb

    const/16 v2, 0xd2

    const/16 v3, 0x3f

    invoke-static {v1, v2, v3}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    const/4 v6, 0x0

    :goto_51
    sget-object v0, Lsc/e;->g:[[Ljava/lang/String;

    array-length v0, v0

    if-ge v6, v0, :cond_ba

    if-eqz v4, :cond_ba

    .line 146
    sget-object v0, Lsc/e;->g:[[Ljava/lang/String;

    aget-object v0, v0, v6

    sget-byte v1, Lsc/e;->e:B

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_70

    const/4 v4, 0x0

    goto :goto_71

    :cond_70
    const/4 v4, 0x1

    .line 147
    :goto_71
    const/4 v0, 0x2

    const/16 v1, 0x1b6

    const/16 v2, 0x21

    invoke-static {v0, v1, v2}, Lsc/e;->a(ISI)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x6

    const/16 v2, 0x264

    const/16 v3, 0x53

    invoke-static {v1, v2, v3}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0x137

    const/16 v3, 0x1a

    invoke-static {v1, v2, v3}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lsc/e;->g:[[Ljava/lang/String;

    aget-object v1, v1, v6

    sget-byte v2, Lsc/e;->e:B

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x2

    const/16 v2, 0x160

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 145
    add-int/lit8 v6, v6, 0x1

    goto :goto_51

    .line 150
    :cond_ba
    const/4 v0, 0x1

    if-ne v4, v0, :cond_be

    return v4

    .line 153
    :cond_be
    const/4 v4, 0x1

    .line 154
    :try_start_bf
    invoke-virtual {p0}, Ljava/security/cert/X509Certificate;->getEncoded()[B

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/security/commons/Crypto;->sha1([B)[B

    move-result-object v0

    .line 155
    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->bytes2hexStr([B)Ljava/lang/String;

    move-result-object p0

    .line 157
    const/4 v0, 0x2

    const/16 v1, 0x1b6

    const/16 v2, 0x21

    invoke-static {v0, v1, v2}, Lsc/e;->a(ISI)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0xc

    const/16 v2, 0x1c2

    const/16 v3, 0x3f

    invoke-static {v1, v2, v3}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    const/4 v5, 0x0

    :goto_ea
    sget-object v0, Lsc/e;->g:[[Ljava/lang/String;

    array-length v0, v0

    if-ge v5, v0, :cond_153

    if-eqz v4, :cond_153

    .line 160
    sget-object v0, Lsc/e;->g:[[Ljava/lang/String;

    aget-object v0, v0, v5

    sget-byte v1, Lsc/e;->f:B

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_109

    const/4 v4, 0x0

    goto :goto_10a

    :cond_109
    const/4 v4, 0x1

    .line 161
    :goto_10a
    const/4 v0, 0x2

    const/16 v1, 0x1b6

    const/16 v2, 0x21

    invoke-static {v0, v1, v2}, Lsc/e;->a(ISI)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x7

    const/16 v2, 0x26b

    const/16 v3, 0x53

    invoke-static {v1, v2, v3}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0x137

    const/16 v3, 0x1a

    invoke-static {v1, v2, v3}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lsc/e;->g:[[Ljava/lang/String;

    aget-object v1, v1, v5

    sget-byte v2, Lsc/e;->f:B

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x2

    const/16 v2, 0x160

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
    :try_end_150
    .catch Ljava/lang/Exception; {:try_start_bf .. :try_end_150} :catch_154

    .line 159
    add-int/lit8 v5, v5, 0x1

    goto :goto_ea

    .line 164
    :cond_153
    return v4

    .line 166
    .line 167
    :catch_154
    const/4 v0, 0x2

    return v0
.end method

.method private static a(ISI)Ljava/lang/String;
    .registers 9

    rsub-int p1, p1, 0x2cd

    new-instance v0, Ljava/lang/String;

    add-int/lit8 p2, p2, 0x20

    const/4 v4, 0x0

    add-int/lit8 p0, p0, 0x1

    sget-object v5, Lsc/e;->h:[S

    new-array v1, p0, [C

    add-int/lit8 p0, p0, -0x1

    if-nez v5, :cond_17

    move v2, p2

    move v3, p0

    :goto_13
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit16 p2, v2, -0xfc2

    :cond_17
    int-to-char v2, p2

    aput-char v2, v1, v4

    move v2, v4

    add-int/lit8 v4, v4, 0x1

    if-ne v2, p0, :cond_27

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_27
    move v2, p2

    add-int/lit8 p1, p1, 0x1

    aget-short v3, v5, p1

    goto :goto_13
.end method

.method public static a()Lsc/e;
    .registers 1

    .line 61
    invoke-static {}, Lsc/g;->a()Lsc/e;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/security/cert/X509Certificate;)V
    .registers 9

    .line 172
    const/4 v0, 0x2

    const/16 v1, 0x1b6

    const/16 v2, 0x21

    invoke-static {v0, v1, v2}, Lsc/e;->a(ISI)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lsc/e;->h:[S

    const/4 v2, 0x1

    aget-short v1, v1, v2

    neg-int v1, v1

    int-to-short v1, v1

    const/4 v2, 0x7

    const/16 v3, 0x2c3

    invoke-static {v2, v3, v1}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    const/4 p0, 0x0

    .line 177
    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getSubjectX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/security/auth/x500/X500Principal;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 179
    const/4 v4, 0x0

    :goto_30
    sget-object v0, Lsc/e;->g:[[Ljava/lang/String;

    array-length v0, v0

    if-ge v4, v0, :cond_56

    .line 180
    sget-object v0, Lsc/e;->g:[[Ljava/lang/String;

    aget-object v0, v0, v4

    sget-byte v1, Lsc/e;->d:B

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    .line 181
    sget-object v0, Lsc/e;->g:[[Ljava/lang/String;

    aget-object v0, v0, v4

    sget-byte v1, Lsc/e;->c:B

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    .line 182
    if-eqz v5, :cond_53

    if-eqz v6, :cond_53

    .line 183
    const/4 p0, 0x1

    .line 184
    goto :goto_56

    .line 179
    :cond_53
    add-int/lit8 v4, v4, 0x1

    goto :goto_30

    .line 188
    :cond_56
    :goto_56
    if-nez p0, :cond_6e

    new-instance v0, Ljavax/net/ssl/SSLException;

    sget-object v1, Lsc/e;->h:[S

    const/4 v2, 0x1

    aget-short v1, v1, v2

    neg-int v1, v1

    int-to-byte v1, v1

    or-int/lit8 v2, v1, 0x22

    int-to-short v2, v2

    const/16 v3, 0x43

    invoke-static {v1, v2, v3}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljavax/net/ssl/SSLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 189
    :cond_6e
    return-void
.end method

.method private c()Ljavax/net/ssl/SSLSocketFactory;
    .registers 7

    .line 74
    invoke-static {p0}, Ljavax/net/ssl/HttpsURLConnection;->setDefaultHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V

    .line 76
    new-instance v4, Lsc/f;

    invoke-direct {v4, p0}, Lsc/f;-><init>(Lsc/e;)V

    .line 88
    const/4 v5, 0x0

    .line 90
    const/4 v0, 0x2

    const/16 v1, 0x136

    const/16 v2, 0x33

    :try_start_e
    invoke-static {v0, v1, v2}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;
    :try_end_15
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_e .. :try_end_15} :catch_18

    move-result-object v0

    move-object v5, v0

    .line 92
    nop

    .line 91
    .line 93
    :catch_18
    if-nez v5, :cond_29

    .line 95
    const/4 v0, 0x2

    const/16 v1, 0x1ed

    const/16 v2, 0x34

    :try_start_1f
    invoke-static {v0, v1, v2}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;
    :try_end_26
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1f .. :try_end_26} :catch_29

    move-result-object v0

    move-object v5, v0

    .line 97
    nop

    .line 96
    .line 100
    :catch_29
    :cond_29
    if-nez v5, :cond_3e

    .line 101
    new-instance v0, Lbr/com/itau/security/commons/exception/BadCryptoDataException;

    sget v1, Lsc/e;->i:I

    ushr-int/lit8 v1, v1, 0x2

    int-to-short v1, v1

    const/16 v2, 0xc

    const/16 v3, 0x25e

    invoke-static {v2, v3, v1}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/security/commons/exception/BadCryptoDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :cond_3e
    const/4 v0, 0x1

    :try_start_3f
    new-array v0, v0, [Ljavax/net/ssl/TrustManager;

    const/4 v1, 0x0

    aput-object v4, v0, v1

    new-instance v1, Ljava/security/SecureRandom;

    invoke-direct {v1}, Ljava/security/SecureRandom;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v5, v2, v0, v1}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V
    :try_end_4d
    .catch Ljava/lang/Exception; {:try_start_3f .. :try_end_4d} :catch_4e

    .line 111
    goto :goto_6d

    .line 105
    .line 107
    :catch_4e
    const/4 v0, 0x1

    :try_start_4f
    new-array v0, v0, [Ljavax/net/ssl/TrustManager;

    const/4 v1, 0x0

    aput-object v4, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v5, v1, v0, v2}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V
    :try_end_59
    .catch Ljava/security/KeyManagementException; {:try_start_4f .. :try_end_59} :catch_5a

    .line 110
    goto :goto_6d

    .line 108
    .line 109
    :catch_5a
    new-instance v0, Lbr/com/itau/security/commons/exception/BadCryptoDataException;

    sget v1, Lsc/e;->i:I

    and-int/lit16 v1, v1, 0x3e0

    int-to-short v1, v1

    const/16 v2, 0x1e

    const/16 v3, 0x23

    invoke-static {v2, v1, v3}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/security/commons/exception/BadCryptoDataException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :goto_6d
    invoke-virtual {v5}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z
    .registers 7

    .line 118
    const/4 v0, 0x2

    const/16 v1, 0x1b6

    const/16 v2, 0x21

    invoke-static {v0, v1, v2}, Lsc/e;->a(ISI)Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lsc/e;->h:[S

    const/4 v2, 0x1

    aget-short v1, v1, v2

    neg-int v1, v1

    int-to-short v1, v1

    const/4 v2, 0x7

    const/16 v3, 0x2ca

    invoke-static {v2, v3, v1}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    :try_start_22
    invoke-interface {p2}, Ljavax/net/ssl/SSLSession;->getPeerCertificates()[Ljava/security/cert/Certificate;

    move-result-object p2

    .line 121
    const/4 v0, 0x2

    aget-object v0, p2, v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    invoke-static {v0}, Lsc/e;->a(Ljava/security/cert/X509Certificate;)I

    move-result v0

    .line 122
    const/4 v1, 0x1

    if-ne v0, v1, :cond_42

    new-instance v0, Ljavax/net/ssl/SSLException;

    const/16 v1, 0x12

    const/16 v2, 0x60

    const/16 v3, 0x43

    invoke-static {v1, v2, v3}, Lsc/e;->a(ISI)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljavax/net/ssl/SSLException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_42
    iget v0, p0, Lsc/e;->b:I

    aget-object v0, p2, v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    invoke-static {p1, v0}, Lsc/e;->a(Ljava/lang/String;Ljava/security/cert/X509Certificate;)V
    :try_end_4b
    .catch Ljavax/net/ssl/SSLException; {:try_start_22 .. :try_end_4b} :catch_4d

    .line 124
    const/4 v0, 0x1

    return v0

    .line 125
    .line 126
    :catch_4d
    const/4 v0, 0x0

    return v0
.end method

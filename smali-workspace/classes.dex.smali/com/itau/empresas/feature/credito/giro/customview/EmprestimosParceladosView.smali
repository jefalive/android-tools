.class public Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;
.super Landroid/widget/LinearLayout;
.source "EmprestimosParceladosView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;
    }
.end annotation


# instance fields
.field ajudaCredito:Landroid/widget/ImageView;

.field botaoContinuar:Landroid/widget/Button;

.field botaoPersonalizar:Landroid/widget/Button;

.field llActivityEmprestimosParceladosOferta:Landroid/widget/LinearLayout;

.field private ofertaProdutosParcelados:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

.field textoCreditoComSeguro:Landroid/widget/TextView;

.field textoCreditoDiasPagamento:Landroid/widget/TextView;

.field textoCreditoTaxaJurosValor:Landroid/widget/TextView;

.field textoCreditoValor:Landroid/widget/TextView;

.field textoCreditoValorComParcelas:Landroid/widget/TextView;

.field textoGarantiaDevedorSolidario:Landroid/widget/TextView;

.field private tipoOferta:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

.field tituloCreditoSimplificado:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 66
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeSet"    # Landroid/util/AttributeSet;

    .line 70
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    return-void
.end method

.method private abreMensagemAjuda()V
    .registers 4

    .line 271
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->UM_BOTAO:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 272
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 273
    const v1, 0x7f0704f7

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 274
    const v1, 0x7f07048c

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNeutro(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v2

    .line 276
    .local v2, "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$1;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$1;-><init>(Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNeutroListener(Landroid/view/View$OnClickListener;)V

    .line 282
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 283
    return-void
.end method

.method private retornaOfertaPeloSufixoDoTipoOferta(Ljava/util/List;Ljava/lang/String;)Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;
    .registers 7
    .param p1, "lista"    # Ljava/util/List;
    .param p2, "sufixo"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;>;Ljava/lang/String;)Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;"
        }
    .end annotation

    .line 367
    const/4 v1, 0x0

    .line 369
    .local v1, "ofertaProdutoParcelado":Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;
    if-eqz p1, :cond_2d

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2d

    .line 370
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2d

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;

    .line 372
    .local v3, "oferta":Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;
    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->getTipoOferta()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2c

    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->getTipoOferta()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 373
    move-object v1, v3

    .line 374
    goto :goto_2d

    .line 376
    .end local v3    # "oferta":Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;
    :cond_2c
    goto :goto_d

    .line 379
    :cond_2d
    :goto_2d
    return-object v1
.end method

.method private setaParcela(IDZ)Ljava/lang/String;
    .registers 9
    .param p1, "quantidadeParcelas"    # I
    .param p2, "valorParcela"    # D
    .param p4, "acessibilidade"    # Z

    .line 243
    const-string v2, " x "

    .line 244
    .local v2, "vezes":Ljava/lang/String;
    if-eqz p4, :cond_6

    .line 245
    const-string v2, " vezes de "

    .line 248
    :cond_6
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    .line 249
    .local v3, "valorParcelaStr":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 251
    const/4 v1, 0x1

    invoke-static {v3, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 249
    return-object v0
.end method

.method private static setaParcelaAte(IZ)Ljava/lang/String;
    .registers 5
    .param p0, "quantidadeParcelas"    # I
    .param p1, "acessibilidade"    # Z

    .line 233
    const-string v2, "x "

    .line 234
    .local v2, "vezes":Ljava/lang/String;
    if-eqz p1, :cond_6

    .line 235
    const-string v2, " vezes "

    .line 238
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "At\u00e9 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static setaQuantidadeDiasVencimento(I)Ljava/lang/String;
    .registers 2
    .param p0, "dias"    # I

    .line 256
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->setaQuantidadeDiasVencimento(IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static setaQuantidadeDiasVencimento(IZ)Ljava/lang/String;
    .registers 4
    .param p0, "dias"    # I
    .param p1, "exibirAte"    # Z

    .line 261
    if-eqz p1, :cond_1c

    .line 263
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "At\u00e9 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " dias"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 266
    :cond_1c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " dias"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method aoClicarEmAjuda()V
    .registers 1

    .line 288
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->abreMensagemAjuda()V

    .line 289
    return-void
.end method

.method aoClicarEmContinuar()V
    .registers 5

    .line 294
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/Evento$EventoBuscaSimulacaoEspecialOuPersonalizada;

    .line 296
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->getOfertaProdutosParcelados()Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/Evento$EventoBuscaSimulacaoEspecialOuPersonalizada;-><init>(Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;Z)V

    .line 294
    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 298
    return-void
.end method

.method aoClicarEmPersonalizar()V
    .registers 8

    .line 313
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->tipoOferta:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->GIRO_CARTOES_COM_GIRO_AVAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_61

    .line 315
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->getOfertaProdutosParcelados()Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    move-result-object v4

    .line 317
    .line 318
    .local v4, "produtosParceladosSaida":Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getListaOfertaProdutosParcelados()Ljava/util/List;

    move-result-object v0

    const-string v1, "giro_devedor_solidario"

    .line 317
    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->retornaOfertaPeloSufixoDoTipoOferta(Ljava/util/List;Ljava/lang/String;)Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;

    move-result-object v5

    .line 321
    .line 322
    .local v5, "ofertaProdutosParceladosVO":Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getOfertaEspecialProdutosParceladosVO()Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;

    move-result-object v6

    .line 324
    .local v6, "ofertaEspecial":Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;
    if-eqz v5, :cond_60

    if-eqz v6, :cond_60

    .line 326
    .line 328
    invoke-virtual {v5}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->getQuantidadeMinimaParcelas()I

    move-result v0

    int-to-long v0, v0

    .line 327
    invoke-virtual {v4, v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->setPrazoMinimoContratacao(J)V

    .line 329
    .line 331
    invoke-virtual {v5}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->getQuantidadeMaximaParcelas()I

    move-result v0

    int-to-long v0, v0

    .line 330
    invoke-virtual {v4, v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->setPrazoMaximoContratacao(J)V

    .line 333
    invoke-virtual {v5}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->getCodigoProduto()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->setCodigoProduto(Ljava/lang/String;)V

    .line 334
    invoke-virtual {v5}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->getNomeProduto()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->setNomeProduto(Ljava/lang/String;)V

    .line 335
    .line 336
    invoke-virtual {v5}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->isHasIndicadorSeguro()Z

    move-result v0

    invoke-virtual {v6, v0}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->setHasIndicadorSeguro(Z)V

    .line 337
    .line 338
    invoke-virtual {v5}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->getCodigoPlano()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->setCodigoPlanoFinanciamento(Ljava/lang/String;)V

    .line 339
    invoke-virtual {v5}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->getTipoOferta()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->setTipoOferta(Ljava/lang/String;)V

    .line 341
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/Evento$EventoBuscaSimulacaoEspecialOuPersonalizada;

    const/4 v2, 0x1

    invoke-direct {v1, v4, v2}, Lcom/itau/empresas/Evento$EventoBuscaSimulacaoEspecialOuPersonalizada;-><init>(Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;Z)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 346
    .end local v4    # "produtosParceladosSaida":Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;
    .end local v5    # "ofertaProdutosParceladosVO":Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;
    .end local v6    # "ofertaEspecial":Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;
    :cond_60
    goto :goto_72

    .line 348
    :cond_61
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/Evento$EventoBuscaSimulacaoEspecialOuPersonalizada;

    .line 350
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->getOfertaProdutosParcelados()Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/Evento$EventoBuscaSimulacaoEspecialOuPersonalizada;-><init>(Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;Z)V

    .line 348
    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 353
    :goto_72
    return-void
.end method

.method public bind(Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;)V
    .registers 29
    .param p1, "saidaVO"    # Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;
    .param p2, "tipoOferta"    # Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    .line 94
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->setOfertaProdutosParcelados(Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;)V

    .line 95
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->setTipoOferta(Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;)V

    .line 97
    .line 98
    invoke-virtual/range {p1 .. p1}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getOfertaEspecialProdutosParceladosVO()Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;

    move-result-object v4

    .line 100
    .local v4, "ofertaEspecial":Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;
    const-string v5, ""

    .line 101
    .local v5, "nomeProduto":Ljava/lang/String;
    new-instance v6, Ljava/math/BigDecimal;

    const/4 v0, 0x0

    invoke-direct {v6, v0}, Ljava/math/BigDecimal;-><init>(I)V

    .line 102
    .local v6, "valorOferta":Ljava/math/BigDecimal;
    const-wide/16 v7, 0x0

    .line 103
    .local v7, "valorParcela":D
    const/4 v9, 0x0

    .line 104
    .local v9, "quantidadeParcelas":I
    const/4 v10, 0x0

    .line 105
    .local v10, "quantidadeDiasPagamento":I
    const-wide/16 v11, 0x0

    .line 106
    .local v11, "taxaJuros":D
    const/4 v13, 0x0

    .line 107
    .local v13, "indicadorSeguro":Z
    const-string v14, ""

    .line 109
    .local v14, "garantiaDevedorSolidario":Ljava/lang/String;
    const-wide/16 v16, 0x0

    .line 110
    .local v16, "taxaJurosMaxima":D
    const-wide/16 v18, 0x0

    .line 112
    .local v18, "taxaJurosMinima":D
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->botaoContinuar:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 114
    sget-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->GIRO_AVAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    move-object/from16 v1, p2

    invoke-virtual {v1, v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_43

    sget-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->GIRO_CARTOES_COM_GIRO_AVAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    .line 115
    move-object/from16 v1, p2

    invoke-virtual {v1, v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b9

    .line 117
    :cond_43
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getValorOferta()Ljava/math/BigDecimal;

    move-result-object v6

    .line 118
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getValorParcela()D

    move-result-wide v7

    .line 119
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getQuantidadeParcelas()I

    move-result v9

    .line 120
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getQuantidadeDiasPrimeiroPagamento()I

    move-result v10

    .line 121
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getTaxaJuros()D

    move-result-wide v11

    .line 122
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->isHasIndicadorSeguro()Z

    move-result v13

    .line 123
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getTipoOferta()Ljava/lang/String;

    move-result-object v14

    .line 125
    sget-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->GIRO_CARTOES_COM_GIRO_AVAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    move-object/from16 v1, p2

    invoke-virtual {v1, v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9c

    .line 127
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->botaoContinuar:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 131
    invoke-virtual/range {p1 .. p1}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getListaOfertaProdutosParcelados()Ljava/util/List;

    move-result-object v15

    .line 133
    .local v15, "ofertas":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;>;"
    const-string v0, "giro_devedor_solidario"

    move-object/from16 v1, p0

    invoke-direct {v1, v15, v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->retornaOfertaPeloSufixoDoTipoOferta(Ljava/util/List;Ljava/lang/String;)Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;

    move-result-object v20

    .line 136
    .local v20, "ofertaProdutosParceladosVO":Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;
    if-eqz v20, :cond_9c

    .line 138
    invoke-virtual/range {v20 .. v20}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->getValorMaximoContratacao()Ljava/math/BigDecimal;

    move-result-object v6

    .line 139
    .line 140
    invoke-virtual/range {v20 .. v20}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->getQuantidadeDiasPrimeiroPagamento()I

    move-result v10

    .line 141
    invoke-virtual/range {v20 .. v20}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->getQuantidadeMaximaParcelas()I

    move-result v9

    .line 142
    invoke-virtual/range {v20 .. v20}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->getJurosTaxaMaxima()D

    move-result-wide v16

    .line 143
    invoke-virtual/range {v20 .. v20}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->getJurosTaxaMinima()D

    move-result-wide v18

    .line 144
    invoke-virtual/range {v20 .. v20}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->getTipoOferta()Ljava/lang/String;

    move-result-object v14

    .line 145
    invoke-virtual/range {v20 .. v20}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->isHasIndicadorSeguro()Z

    move-result v13

    .line 149
    .end local v15    # "ofertas":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;>;"
    .end local v15
    .end local v20    # "ofertaProdutosParceladosVO":Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;
    :cond_9c
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->isHasIndicadorSeguro()Z

    move-result v0

    if-eqz v0, :cond_ae

    .line 150
    invoke-virtual/range {p0 .. p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f07057c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_b9

    .line 152
    :cond_ae
    invoke-virtual/range {p0 .. p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f07057b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 158
    :cond_b9
    :goto_b9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->tituloCreditoSimplificado:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    .line 164
    .local v20, "valorOfertaStr":Ljava/lang/String;
    const-string v21, ""

    .line 165
    .local v21, "pagarEm":Ljava/lang/String;
    const-string v22, ""

    .line 168
    .local v22, "pagarEmAcessibilidade":Ljava/lang/String;
    const-string v23, ""

    .line 171
    .local v23, "comPrimeiroPagamento":Ljava/lang/String;
    const-string v24, ""

    .line 172
    .local v24, "taxaDeJuros":Ljava/lang/String;
    const-string v25, ""

    .line 174
    .local v25, "taxaDeJurosAcessibilidade":Ljava/lang/String;
    sget-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->GIRO_AVAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    move-object/from16 v1, p2

    invoke-virtual {v1, v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f7

    .line 176
    move-object/from16 v0, p0

    const/4 v1, 0x0

    invoke-direct {v0, v9, v7, v8, v1}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->setaParcela(IDZ)Ljava/lang/String;

    move-result-object v21

    .line 178
    move-object/from16 v0, p0

    const/4 v1, 0x1

    invoke-direct {v0, v9, v7, v8, v1}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->setaParcela(IDZ)Ljava/lang/String;

    move-result-object v22

    .line 180
    invoke-static {v10}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->setaQuantidadeDiasVencimento(I)Ljava/lang/String;

    move-result-object v23

    .line 182
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v11, v12, v0, v1}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagem(DIZ)Ljava/lang/String;

    move-result-object v24

    .line 185
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {v11, v12, v0, v1}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagem(DIZ)Ljava/lang/String;

    move-result-object v25

    goto :goto_120

    .line 187
    :cond_f7
    sget-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->GIRO_CARTOES_COM_GIRO_AVAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    move-object/from16 v1, p2

    invoke-virtual {v1, v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_120

    .line 189
    const/4 v0, 0x0

    invoke-static {v9, v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->setaParcelaAte(IZ)Ljava/lang/String;

    move-result-object v21

    .line 191
    const/4 v0, 0x1

    invoke-static {v9, v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->setaParcelaAte(IZ)Ljava/lang/String;

    move-result-object v22

    .line 193
    const/4 v0, 0x1

    invoke-static {v10, v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->setaQuantidadeDiasVencimento(IZ)Ljava/lang/String;

    move-result-object v23

    .line 195
    .line 196
    move-wide/from16 v0, v18

    move-wide/from16 v2, v16

    invoke-static {v0, v1, v2, v3}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemDeAte(DD)Ljava/lang/String;

    move-result-object v24

    .line 198
    move-wide/from16 v0, v18

    move-wide/from16 v2, v16

    invoke-static {v0, v1, v2, v3}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemDeAte(DD)Ljava/lang/String;

    move-result-object v25

    .line 202
    :cond_120
    :goto_120
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->textoCreditoValor:Landroid/widget/TextView;

    .line 203
    move-object/from16 v1, v20

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 202
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->textoCreditoValorComParcelas:Landroid/widget/TextView;

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 205
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->textoCreditoDiasPagamento:Landroid/widget/TextView;

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 206
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->textoCreditoTaxaJurosValor:Landroid/widget/TextView;

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 208
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->textoCreditoTaxaJurosValor:Landroid/widget/TextView;

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 209
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->textoCreditoValorComParcelas:Landroid/widget/TextView;

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 211
    if-eqz v13, :cond_166

    .line 212
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->textoCreditoComSeguro:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_16e

    .line 214
    :cond_166
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->textoCreditoComSeguro:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 217
    :goto_16e
    const-string v0, "giro_devedor_solidario"

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17f

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->textoGarantiaDevedorSolidario:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_187

    .line 220
    :cond_17f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->textoGarantiaDevedorSolidario:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 226
    :goto_187
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->llActivityEmprestimosParceladosOferta:Landroid/widget/LinearLayout;

    invoke-virtual/range {p0 .. p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 227
    const v2, 0x7f0701b3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 226
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 228
    return-void
.end method

.method public getOfertaProdutosParcelados()Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;
    .registers 2

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->ofertaProdutosParcelados:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    return-object v0
.end method

.method public setOfertaProdutosParcelados(Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;)V
    .registers 2
    .param p1, "ofertaProdutosParcelados"    # Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    .line 79
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->ofertaProdutosParcelados:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    .line 80
    return-void
.end method

.method public setTipoOferta(Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;)V
    .registers 2
    .param p1, "tipoOferta"    # Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    .line 89
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;->tipoOferta:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    .line 90
    return-void
.end method

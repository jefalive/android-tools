.class Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$1;
.super Ljava/lang/Object;
.source "AreaUsuarioSelecaoContaAdapter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->ordenar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Ljava/util/Comparator<Lcom/itau/empresas/api/model/ContaOperadorVO;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;


# direct methods
.method constructor <init>(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

    .line 63
    iput-object p1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$1;->this$0:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/itau/empresas/api/model/ContaOperadorVO;Lcom/itau/empresas/api/model/ContaOperadorVO;)I
    .registers 6
    .param p1, "contaOperadorVO"    # Lcom/itau/empresas/api/model/ContaOperadorVO;
    .param p2, "t1"    # Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 65
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    .line 67
    .local v2, "comparacaoAgencia":I
    if-eqz v2, :cond_f

    .line 68
    return v2

    .line 70
    :cond_f
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 5

    .line 63
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-object v1, p2

    check-cast v1, Lcom/itau/empresas/api/model/ContaOperadorVO;

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$1;->compare(Lcom/itau/empresas/api/model/ContaOperadorVO;Lcom/itau/empresas/api/model/ContaOperadorVO;)I

    move-result v0

    return v0
.end method

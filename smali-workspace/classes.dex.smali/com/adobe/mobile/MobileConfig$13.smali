.class Lcom/adobe/mobile/MobileConfig$13;
.super Landroid/content/BroadcastReceiver;
.source "MobileConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adobe/mobile/MobileConfig;->startNotifier()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adobe/mobile/MobileConfig;


# direct methods
.method constructor <init>(Lcom/adobe/mobile/MobileConfig;)V
    .registers 2
    .param p1, "this$0"    # Lcom/adobe/mobile/MobileConfig;

    .line 1176
    iput-object p1, p0, Lcom/adobe/mobile/MobileConfig$13;->this$0:Lcom/adobe/mobile/MobileConfig;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 1179
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig$13;->this$0:Lcom/adobe/mobile/MobileConfig;

    iget-object v1, p0, Lcom/adobe/mobile/MobileConfig$13;->this$0:Lcom/adobe/mobile/MobileConfig;

    invoke-virtual {v1, p1}, Lcom/adobe/mobile/MobileConfig;->getNetworkConnectivity(Landroid/content/Context;)Z

    move-result v1

    # setter for: Lcom/adobe/mobile/MobileConfig;->_networkConnectivity:Z
    invoke-static {v0, v1}, Lcom/adobe/mobile/MobileConfig;->access$402(Lcom/adobe/mobile/MobileConfig;Z)Z

    .line 1181
    iget-object v0, p0, Lcom/adobe/mobile/MobileConfig$13;->this$0:Lcom/adobe/mobile/MobileConfig;

    # getter for: Lcom/adobe/mobile/MobileConfig;->_networkConnectivity:Z
    invoke-static {v0}, Lcom/adobe/mobile/MobileConfig;->access$400(Lcom/adobe/mobile/MobileConfig;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 1182
    const-string v0, "Analytics - Network status changed (reachable)"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1183
    invoke-static {}, Lcom/adobe/mobile/AnalyticsWorker;->sharedInstance()Lcom/adobe/mobile/AnalyticsWorker;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/adobe/mobile/AnalyticsWorker;->kick(Z)V

    goto :goto_2c

    .line 1186
    :cond_24
    const-string v0, "Analytics - Network status changed (unreachable)"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1188
    :goto_2c
    return-void
.end method

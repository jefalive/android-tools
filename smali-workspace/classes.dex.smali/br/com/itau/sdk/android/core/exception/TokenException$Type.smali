.class public final enum Lbr/com/itau/sdk/android/core/exception/TokenException$Type;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/sdk/android/core/exception/TokenException;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/sdk/android/core/exception/TokenException$Type;>;"
    }
.end annotation


# static fields
.field private static final $:[B

.field private static $$:I

.field private static final synthetic $VALUES:[Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

.field public static final enum CANCELED:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

.field public static final enum INTERRUPTED:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

.field public static final enum INVALID:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

.field public static final enum UNEXPECTED:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    new-instance v0, Ljava/lang/String;

    sget-object v5, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->$:[B

    mul-int/lit8 p2, p2, 0x6

    rsub-int/lit8 p2, p2, 0x55

    rsub-int/lit8 p1, p1, 0xb

    const/4 v4, 0x0

    add-int/lit8 p0, p0, 0x4

    new-array v1, p1, [B

    if-nez v5, :cond_16

    move v2, p0

    move v3, p2

    :goto_13
    add-int/2addr v2, v3

    add-int/lit8 p2, v2, -0x1

    :cond_16
    move v2, v4

    add-int/lit8 p0, p0, 0x1

    add-int/lit8 v4, v4, 0x1

    int-to-byte v3, p2

    aput-byte v3, v1, v2

    if-ne v4, p1, :cond_25

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_25
    move v2, p2

    aget-byte v3, v5, p0

    goto :goto_13
.end method

.method static constructor <clinit>()V
    .registers 5

    .line 13
    const/16 v0, 0x24

    new-array v0, v0, [B

    fill-array-data v0, :array_9e

    sput-object v0, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->$:[B

    const/16 v0, 0xd8

    sput v0, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->$$:I

    new-instance v0, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->$:[B

    const/16 v2, 0x14

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->$:[B

    const/16 v3, 0x16

    aget-byte v2, v2, v3

    invoke-static {v1, v2, v2}, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->CANCELED:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    .line 18
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->$:[B

    const/16 v2, 0xf

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->$:[B

    const/16 v3, 0x1f

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->$:[B

    const/16 v4, 0x8

    aget-byte v3, v3, v4

    neg-int v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->INVALID:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    .line 23
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->$:[B

    const/16 v2, 0xc

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, 0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->$:[B

    const/16 v3, 0x12

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->$:[B

    const/16 v4, 0x8

    aget-byte v3, v3, v4

    neg-int v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->INTERRUPTED:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    .line 28
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->$:[B

    const/16 v2, 0x21

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->$:[B

    const/16 v3, 0x1e

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->$:[B

    const/16 v4, 0x12

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->UNEXPECTED:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    .line 9
    const/4 v0, 0x4

    new-array v0, v0, [Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->CANCELED:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->INVALID:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->INTERRUPTED:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->UNEXPECTED:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->$VALUES:[Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    return-void

    nop

    :array_9e
    .array-data 1
        0x20t
        -0x1ct
        0x77t
        -0x5t
        0x6t
        0x9t
        -0x14t
        0xct
        -0x2t
        -0x4t
        -0x6t
        -0x8t
        0x14t
        -0x7t
        -0xat
        -0x1t
        0x12t
        -0xet
        0x0t
        -0x1t
        0xet
        -0xat
        0x3t
        0x8t
        -0x6t
        0x0t
        0x6t
        0x7t
        -0xet
        0xet
        0x1t
        0x4t
        -0x4t
        0x5t
        -0xet
        0x0t
    .end array-data
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/exception/TokenException$Type;
    .registers 2

    .line 9
    const-class v0, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/sdk/android/core/exception/TokenException$Type;
    .registers 1

    .line 9
    sget-object v0, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->$VALUES:[Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    invoke-virtual {v0}, [Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    return-object v0
.end method

.class public Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;
.super Ljava/lang/Object;
.source "SnackbarUtils.java"

# interfaces
.implements Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/util/SnackbarUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GerenciadorDelegate"
.end annotation


# instance fields
.field private final root:Landroid/view/ViewGroup;

.field private topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .registers 2
    .param p1, "root"    # Landroid/view/ViewGroup;

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;->root:Landroid/view/ViewGroup;

    .line 41
    return-void
.end method


# virtual methods
.method public dismissSnackbar()V
    .registers 2

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    if-eqz v0, :cond_9

    .line 45
    iget-object v0, p0, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismiss()V

    .line 46
    :cond_9
    return-void
.end method

.method public showSnackbar(Ljava/lang/CharSequence;I)V
    .registers 4
    .param p1, "texto"    # Ljava/lang/CharSequence;
    .param p2, "duracao"    # I

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;->root:Landroid/view/ViewGroup;

    invoke-static {v0, p1, p2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 50
    return-void
.end method

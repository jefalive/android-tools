.class final Lbr/com/itau/sdk/android/core/endpoint/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbr/com/itau/sdk/android/core/endpoint/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:Ljava/lang/Object;>Ljava/lang/Object;Lbr/com/itau/sdk/android/core/endpoint/a<TT;>;"
    }
.end annotation


# static fields
.field private static final j:[B

.field private static k:I


# instance fields
.field a:Lbr/com/itau/sdk/android/core/endpoint/p;

.field private final b:Ljava/lang/String;

.field private final c:Lokhttp3/OkHttpClient;

.field private final d:Lbr/com/itau/sdk/android/core/endpoint/k;

.field private final e:[Ljava/lang/Object;

.field private final f:Z

.field private final g:Lbr/com/itau/sdk/android/core/endpoint/y;

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x72

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/n;->j:[B

    const/16 v0, 0x9d

    sput v0, Lbr/com/itau/sdk/android/core/endpoint/n;->k:I

    return-void

    :array_e
    .array-data 1
        0x3t
        -0x4ft
        0x4et
        -0xbt
        -0x42t
        -0xet
        0x9t
        0x43t
        -0x43t
        -0xet
        0xat
        -0x16t
        0x3et
        0x19t
        -0x1et
        -0x7t
        0x0t
        0xet
        -0xet
        0x51t
        -0x53t
        0xct
        -0xct
        0x3t
        0x2t
        0x3t
        0x0t
        0x43t
        -0x58t
        0xdt
        -0xct
        0xbt
        0x47t
        -0x54t
        -0x2t
        0x12t
        -0x14t
        -0x2t
        0x1t
        0x38t
        0x19t
        -0x2at
        -0x4t
        0x0t
        0x8t
        0x44t
        -0x4ft
        -0x2t
        -0x6t
        0x53t
        -0x54t
        -0x3t
        0x4t
        -0x1t
        0x0t
        -0x4t
        -0x3t
        0xet
        0x0t
        -0x2ct
        -0x7t
        0xct
        0x3t
        -0x4t
        -0x16t
        0x58t
        -0x46t
        -0x14t
        0x12t
        0x1t
        -0x13t
        0x0t
        0xet
        0x0t
        -0x2dt
        -0x7t
        0x8t
        0x7t
        -0xbt
        0x46t
        -0x4et
        0x53t
        -0x44t
        -0xdt
        0x0t
        -0x9t
        0x10t
        -0xet
        -0x3t
        0x53t
        -0x53t
        0xct
        -0xft
        0x2t
        0x0t
        0x0t
        -0x6t
        0xdt
        0x44t
        -0x55t
        0x4t
        0x4et
        -0x4et
        0xbt
        -0x10t
        -0x1t
        0xat
        0x0t
        0x43t
        -0x55t
        -0x6t
        0x8t
        0xat
        0x36t
    .end array-data
.end method

.method constructor <init>(Ljava/lang/String;Lokhttp3/OkHttpClient;ZLbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;Lbr/com/itau/sdk/android/core/endpoint/k;[Ljava/lang/Object;)V
    .registers 8

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->b:Ljava/lang/String;

    .line 46
    iput-object p2, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->c:Lokhttp3/OkHttpClient;

    .line 47
    iput-object p5, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->d:Lbr/com/itau/sdk/android/core/endpoint/k;

    .line 48
    iput-object p6, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->e:[Ljava/lang/Object;

    .line 49
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/y;

    invoke-direct {v0, p4}, Lbr/com/itau/sdk/android/core/endpoint/y;-><init>(Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;)V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->g:Lbr/com/itau/sdk/android/core/endpoint/y;

    .line 50
    iput-boolean p3, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->f:Z

    .line 51
    return-void
.end method

.method private a(Lokhttp3/Response;)Lbr/com/itau/sdk/android/core/endpoint/b;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lokhttp3/Response;)Lbr/com/itau/sdk/android/core/endpoint/b<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 82
    invoke-virtual {p1}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v2

    .line 85
    invoke-virtual {p1}, Lokhttp3/Response;->newBuilder()Lokhttp3/Response$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lokhttp3/Response$Builder;->body(Lokhttp3/ResponseBody;)Lokhttp3/Response$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Response$Builder;->build()Lokhttp3/Response;

    move-result-object p1

    .line 87
    :try_start_11
    invoke-direct {p0, p1, v2}, Lbr/com/itau/sdk/android/core/endpoint/n;->a(Lokhttp3/Response;Lokhttp3/ResponseBody;)Lbr/com/itau/sdk/android/core/endpoint/b;
    :try_end_14
    .catchall {:try_start_11 .. :try_end_14} :catchall_19

    move-result-object v3

    .line 89
    invoke-virtual {v2}, Lokhttp3/ResponseBody;->close()V

    .line 87
    return-object v3

    .line 89
    :catchall_19
    move-exception v4

    invoke-virtual {v2}, Lokhttp3/ResponseBody;->close()V

    throw v4
.end method

.method private a(Lokhttp3/Response;Lokhttp3/ResponseBody;)Lbr/com/itau/sdk/android/core/endpoint/b;
    .registers 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lokhttp3/Response;Lokhttp3/ResponseBody;)Lbr/com/itau/sdk/android/core/endpoint/b<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 95
    invoke-virtual/range {p1 .. p1}, Lokhttp3/Response;->code()I

    move-result v6

    .line 96
    const/4 v7, 0x0

    .line 97
    const/4 v8, 0x0

    .line 100
    new-instance v10, Lbr/com/itau/sdk/android/core/exception/c;

    move-object/from16 v0, p2

    invoke-direct {v10, v0}, Lbr/com/itau/sdk/android/core/exception/c;-><init>(Lokhttp3/ResponseBody;)V

    .line 103
    const/16 v0, 0xc8

    if-lt v6, v0, :cond_15

    const/16 v0, 0x12c

    if-lt v6, v0, :cond_5a

    .line 104
    :cond_15
    :try_start_15
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/e;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v2, Lbr/com/itau/sdk/android/core/endpoint/n;->k:I

    and-int/lit8 v2, v2, 0x73

    const/16 v3, 0x3c

    const/16 v4, 0x32

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core/endpoint/n;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/n;->j:[B

    const/16 v3, 0x10

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/n;->j:[B

    const/16 v4, 0x4f

    aget-byte v3, v3, v4

    sget-object v4, Lbr/com/itau/sdk/android/core/endpoint/n;->j:[B

    const/16 v5, 0x10

    aget-byte v4, v4, v5

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core/endpoint/n;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v10}, Lbr/com/itau/sdk/android/core/exception/c;->string()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v6}, Lbr/com/itau/sdk/android/core/exception/e;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 108
    :cond_5a
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->b:Lbr/com/itau/sdk/android/core/endpoint/i;

    invoke-virtual {v0, v10}, Lbr/com/itau/sdk/android/core/endpoint/i;->a(Lokhttp3/ResponseBody;)Lbr/com/itau/sdk/android/core/model/ResponseDTO;

    move-result-object v11

    .line 109
    invoke-virtual {v11}, Lbr/com/itau/sdk/android/core/model/ResponseDTO;->getStatusCode()I

    move-result v6

    .line 110
    invoke-virtual {v11}, Lbr/com/itau/sdk/android/core/model/ResponseDTO;->getHeader()Ljava/util/List;

    move-result-object v9

    .line 112
    invoke-virtual {v11}, Lbr/com/itau/sdk/android/core/model/ResponseDTO;->getSdkData()Lbr/com/itau/sdk/android/core/model/SdkDataDTO;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/endpoint/q;->a(Lbr/com/itau/sdk/android/core/model/SdkDataDTO;)V

    .line 114
    invoke-virtual {v11}, Lbr/com/itau/sdk/android/core/model/ResponseDTO;->getSecurityData()Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;

    move-result-object v12

    .line 115
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->g:Lbr/com/itau/sdk/android/core/endpoint/y;

    invoke-virtual {v0, v12}, Lbr/com/itau/sdk/android/core/endpoint/y;->a(Lbr/com/itau/sdk/android/core/model/SecurityDataDTO;)Lbr/com/itau/sdk/android/core/model/TokenAppDataDTO;

    move-result-object v13

    .line 117
    const/16 v0, 0xc8

    if-lt v6, v0, :cond_bf

    const/16 v0, 0x12c

    if-ge v6, v0, :cond_bf

    .line 118
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->d:Lbr/com/itau/sdk/android/core/endpoint/k;

    iget-boolean v0, v0, Lbr/com/itau/sdk/android/core/endpoint/k;->k:Z

    if-eqz v0, :cond_a3

    .line 119
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->d:Lbr/com/itau/sdk/android/core/endpoint/k;

    iget-object v0, v0, Lbr/com/itau/sdk/android/core/endpoint/k;->a:Ljava/lang/reflect/Method;

    const-class v1, Lbr/com/itau/sdk/android/core/endpoint/http/Cacheable;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Lbr/com/itau/sdk/android/core/endpoint/http/Cacheable;

    .line 120
    invoke-interface {v14}, Lbr/com/itau/sdk/android/core/endpoint/http/Cacheable;->cleanCacheOnSuccess()Z

    move-result v0

    if-eqz v0, :cond_9e

    .line 121
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/endpoint/n;->c()V

    goto :goto_a3

    .line 122
    :cond_9e
    if-eqz v12, :cond_a3

    .line 123
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/endpoint/n;->b()V

    .line 126
    :cond_a3
    :goto_a3
    if-eqz v12, :cond_b0

    if-nez v13, :cond_b0

    .line 127
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->g:Lbr/com/itau/sdk/android/core/endpoint/y;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->d:Lbr/com/itau/sdk/android/core/endpoint/k;

    iget-object v1, v1, Lbr/com/itau/sdk/android/core/endpoint/k;->p:Ljava/lang/String;

    invoke-virtual {v0, v11, v1}, Lbr/com/itau/sdk/android/core/endpoint/y;->a(Lbr/com/itau/sdk/android/core/model/ResponseDTO;Ljava/lang/String;)V

    .line 129
    :cond_b0
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->a:Lcom/google/gson/Gson;

    invoke-virtual {v11}, Lbr/com/itau/sdk/android/core/model/ResponseDTO;->getData()Lcom/google/gson/JsonElement;

    move-result-object v1

    iget-object v2, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->d:Lbr/com/itau/sdk/android/core/endpoint/k;

    iget-object v2, v2, Lbr/com/itau/sdk/android/core/endpoint/k;->o:Ljava/lang/reflect/Type;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/Gson;->fromJson(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v7

    goto :goto_d2

    .line 131
    :cond_bf
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/endpoint/n;->b()V

    .line 132
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->c:Lokhttp3/MediaType;

    sget-object v1, Lbr/com/itau/sdk/android/core/n;->a:Lcom/google/gson/Gson;

    invoke-virtual {v11}, Lbr/com/itau/sdk/android/core/model/ResponseDTO;->getData()Lcom/google/gson/JsonElement;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/gson/Gson;->toJson(Lcom/google/gson/JsonElement;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lokhttp3/ResponseBody;->create(Lokhttp3/MediaType;Ljava/lang/String;)Lokhttp3/ResponseBody;
    :try_end_d1
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_15 .. :try_end_d1} :catch_d3
    .catch Ljava/lang/RuntimeException; {:try_start_15 .. :try_end_d1} :catch_f6

    move-result-object v8

    .line 145
    :goto_d2
    goto :goto_103

    .line 135
    :catch_d3
    move-exception v11

    .line 136
    new-instance v12, Lbr/com/itau/sdk/android/core/endpoint/b;

    move-object/from16 v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v12, v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/b;-><init>(Lokhttp3/Response;Ljava/lang/Object;Lokhttp3/ResponseBody;)V

    .line 137
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/b;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/n;->j:[B

    const/16 v2, 0xe

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/n;->j:[B

    const/16 v3, 0x10

    aget-byte v2, v2, v3

    or-int/lit8 v3, v2, 0x23

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/n;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v12, v11}, Lbr/com/itau/sdk/android/core/exception/b;-><init>(Ljava/lang/String;Lbr/com/itau/sdk/android/core/endpoint/b;Lcom/google/gson/JsonSyntaxException;)V

    throw v0

    .line 138
    :catch_f6
    move-exception v11

    .line 141
    invoke-virtual {v10}, Lbr/com/itau/sdk/android/core/exception/c;->b()Z

    move-result v0

    if-eqz v0, :cond_102

    .line 142
    invoke-virtual {v10}, Lbr/com/itau/sdk/android/core/exception/c;->a()Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 144
    :cond_102
    throw v11

    .line 148
    :goto_103
    const/16 v0, 0x342

    if-eq v6, v0, :cond_10b

    const/16 v0, 0x343

    if-ne v6, v0, :cond_111

    .line 149
    :cond_10b
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/h;

    invoke-direct {v0}, Lbr/com/itau/sdk/android/core/exception/h;-><init>()V

    throw v0

    .line 153
    :cond_111
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->d:Lbr/com/itau/sdk/android/core/endpoint/k;

    iget-boolean v0, v0, Lbr/com/itau/sdk/android/core/endpoint/k;->j:Z

    if-eqz v0, :cond_124

    .line 154
    new-instance v12, Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;

    invoke-direct {v12, v7, v9, v6}, Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;-><init>(Ljava/lang/Object;Ljava/util/List;I)V

    .line 155
    new-instance v11, Lbr/com/itau/sdk/android/core/endpoint/b;

    move-object/from16 v0, p1

    invoke-direct {v11, v0, v12, v8}, Lbr/com/itau/sdk/android/core/endpoint/b;-><init>(Lokhttp3/Response;Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;Lokhttp3/ResponseBody;)V

    .line 156
    goto :goto_12b

    .line 157
    :cond_124
    new-instance v11, Lbr/com/itau/sdk/android/core/endpoint/b;

    move-object/from16 v0, p1

    invoke-direct {v11, v0, v7, v8}, Lbr/com/itau/sdk/android/core/endpoint/b;-><init>(Lokhttp3/Response;Ljava/lang/Object;Lokhttp3/ResponseBody;)V

    .line 160
    :goto_12b
    const/16 v0, 0x190

    if-lt v6, v0, :cond_13b

    .line 161
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v11, v0}, Lbr/com/itau/sdk/android/core/endpoint/b;->a(Ljava/lang/Integer;)V

    .line 162
    invoke-static {v11, v8}, Lbr/com/itau/sdk/android/core/exception/BackendException;->httpError(Lbr/com/itau/sdk/android/core/endpoint/b;Lokhttp3/ResponseBody;)Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    throw v0

    .line 165
    :cond_13b
    return-object v11
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    rsub-int/lit8 p1, p1, 0x49

    sget-object v5, Lbr/com/itau/sdk/android/core/endpoint/n;->j:[B

    add-int/lit8 p0, p0, 0xb

    const/4 v4, 0x0

    add-int/lit8 p2, p2, 0x20

    new-instance v0, Ljava/lang/String;

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v5, :cond_17

    move v2, p1

    move v3, p2

    :goto_13
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p2, v2, -0x1

    :cond_17
    int-to-byte v2, p2

    aput-byte v2, v1, v4

    add-int/lit8 p1, p1, 0x1

    if-ne v4, p0, :cond_23

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_23
    move v2, p2

    aget-byte v3, v5, p1

    add-int/lit8 v4, v4, 0x1

    goto :goto_13
.end method

.method private d()Lokhttp3/Call;
    .registers 5

    .line 73
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/p;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->b:Ljava/lang/String;

    iget-object v2, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->d:Lbr/com/itau/sdk/android/core/endpoint/k;

    iget-boolean v3, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->f:Z

    invoke-direct {v0, v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/p;-><init>(Ljava/lang/String;Lbr/com/itau/sdk/android/core/endpoint/k;Z)V

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->e:[Ljava/lang/Object;

    .line 74
    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/p;->a([Ljava/lang/Object;)Lbr/com/itau/sdk/android/core/endpoint/p;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->a:Lbr/com/itau/sdk/android/core/endpoint/p;

    .line 76
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->c:Lokhttp3/OkHttpClient;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->a:Lbr/com/itau/sdk/android/core/endpoint/p;

    iget-object v2, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->d:Lbr/com/itau/sdk/android/core/endpoint/k;

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/d;->a(Lokhttp3/OkHttpClient;Lbr/com/itau/sdk/android/core/endpoint/p;Lbr/com/itau/sdk/android/core/endpoint/k;)Lokhttp3/Call;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lbr/com/itau/sdk/android/core/endpoint/e;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Lbr/com/itau/sdk/android/core/endpoint/e<TT;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 63
    move-object v4, p0

    monitor-enter v4

    .line 64
    :try_start_2
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->h:Z

    if-eqz v0, :cond_1d

    new-instance v0, Ljava/lang/IllegalStateException;

    sget v1, Lbr/com/itau/sdk/android/core/endpoint/n;->k:I

    and-int/lit8 v1, v1, 0x7

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/n;->j:[B

    const/16 v3, 0x5c

    aget-byte v2, v2, v3

    neg-int v2, v2

    const/16 v3, 0x21

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/n;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1d
    .catchall {:try_start_2 .. :try_end_1d} :catchall_1f

    .line 65
    :cond_1d
    monitor-exit v4

    goto :goto_22

    :catchall_1f
    move-exception v5

    monitor-exit v4

    throw v5

    .line 67
    :goto_22
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/endpoint/n;->d()Lokhttp3/Call;

    move-result-object v4

    .line 68
    invoke-interface {v4}, Lokhttp3/Call;->execute()Lokhttp3/Response;

    move-result-object v5

    .line 69
    invoke-direct {p0, v5}, Lbr/com/itau/sdk/android/core/endpoint/n;->a(Lokhttp3/Response;)Lbr/com/itau/sdk/android/core/endpoint/b;

    move-result-object v0

    return-object v0
.end method

.method b()V
    .registers 6

    .line 169
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->a:Lcom/google/gson/Gson;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->a:Lbr/com/itau/sdk/android/core/endpoint/p;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/endpoint/p;->b()Lbr/com/itau/sdk/android/core/model/RequestDTO;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 171
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getFlowManager()Lbr/com/itau/sdk/android/core/FlowManager;

    move-result-object v1

    iget-object v2, p0, Lbr/com/itau/sdk/android/core/endpoint/n;->d:Lbr/com/itau/sdk/android/core/endpoint/k;

    iget-object v2, v2, Lbr/com/itau/sdk/android/core/endpoint/k;->p:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lbr/com/itau/sdk/android/core/FlowManager;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 170
    invoke-static {v0}, Lbr/com/itau/sdk/android/core/endpoint/a/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 173
    invoke-static {}, Lbr/com/itau/sdk/android/core/endpoint/a/c;->a()Lbr/com/itau/sdk/android/core/endpoint/a/c;

    move-result-object v0

    invoke-virtual {v0, v4}, Lbr/com/itau/sdk/android/core/endpoint/a/c;->b(Ljava/lang/String;)V

    .line 174
    return-void
.end method

.method c()V
    .registers 2

    .line 177
    invoke-static {}, Lbr/com/itau/sdk/android/core/endpoint/a/c;->a()Lbr/com/itau/sdk/android/core/endpoint/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/endpoint/a/c;->b()V

    .line 178
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 58
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/n;->j:[B

    const/16 v2, 0x2c

    aget-byte v1, v1, v2

    const/16 v2, 0x21

    const/16 v3, 0x23

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/n;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/CloneNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.class public Lcom/google/android/gms/internal/zzbh;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/zzbh$zza;
    }
.end annotation


# instance fields
.field private final zztp:I

.field private final zztq:I

.field private final zztr:I

.field private final zzts:Lcom/google/android/gms/internal/zzbg;


# direct methods
.method public constructor <init>(I)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/zzbj;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzbj;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzbh;->zzts:Lcom/google/android/gms/internal/zzbg;

    iput p1, p0, Lcom/google/android/gms/internal/zzbh;->zztq:I

    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/gms/internal/zzbh;->zztp:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/internal/zzbh;->zztr:I

    return-void
.end method

.method private zzv(Ljava/lang/String;)Ljava/lang/String;
    .registers 8

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v0, v2

    if-nez v0, :cond_c

    const-string v0, ""

    return-object v0

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzbh;->zzcM()Lcom/google/android/gms/internal/zzbh$zza;

    move-result-object v3

    new-instance v0, Lcom/google/android/gms/internal/zzbh$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/zzbh$1;-><init>(Lcom/google/android/gms/internal/zzbh;)V

    invoke-static {v2, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    const/4 v4, 0x0

    :goto_19
    array-length v0, v2

    if-ge v4, v0, :cond_43

    iget v0, p0, Lcom/google/android/gms/internal/zzbh;->zztq:I

    if-ge v4, v0, :cond_43

    aget-object v0, v2, v4

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2d

    goto :goto_40

    :cond_2d
    :try_start_2d
    iget-object v0, p0, Lcom/google/android/gms/internal/zzbh;->zzts:Lcom/google/android/gms/internal/zzbg;

    aget-object v1, v2, v4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzbg;->zzu(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/internal/zzbh$zza;->write([B)V
    :try_end_38
    .catch Ljava/io/IOException; {:try_start_2d .. :try_end_38} :catch_39

    goto :goto_40

    :catch_39
    move-exception v5

    const-string v0, "Error while writing hash to byteStream"

    invoke-static {v0, v5}, Lcom/google/android/gms/internal/zzin;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_43

    :goto_40
    add-int/lit8 v4, v4, 0x1

    goto :goto_19

    :cond_43
    :goto_43
    invoke-virtual {v3}, Lcom/google/android/gms/internal/zzbh$zza;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public zza(Ljava/util/ArrayList;)Ljava/lang/String;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Ljava/lang/String;>;)Ljava/lang/String;"
        }
    .end annotation

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/String;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_9

    :cond_25
    iget v0, p0, Lcom/google/android/gms/internal/zzbh;->zztr:I

    sparse-switch v0, :sswitch_data_40

    goto :goto_3d

    :sswitch_2b
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/zzbh;->zzw(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :sswitch_34
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/zzbh;->zzv(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :goto_3d
    const-string v0, ""

    return-object v0

    :sswitch_data_40
    .sparse-switch
        0x0 -> :sswitch_2b
        0x1 -> :sswitch_34
    .end sparse-switch
.end method

.method zzcM()Lcom/google/android/gms/internal/zzbh$zza;
    .registers 2

    new-instance v0, Lcom/google/android/gms/internal/zzbh$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzbh$zza;-><init>()V

    return-object v0
.end method

.method zzw(Ljava/lang/String;)Ljava/lang/String;
    .registers 10

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v0, v2

    if-nez v0, :cond_c

    const-string v0, ""

    return-object v0

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzbh;->zzcM()Lcom/google/android/gms/internal/zzbh$zza;

    move-result-object v3

    new-instance v4, Ljava/util/PriorityQueue;

    iget v0, p0, Lcom/google/android/gms/internal/zzbh;->zztq:I

    new-instance v1, Lcom/google/android/gms/internal/zzbh$2;

    invoke-direct {v1, p0}, Lcom/google/android/gms/internal/zzbh$2;-><init>(Lcom/google/android/gms/internal/zzbh;)V

    invoke-direct {v4, v0, v1}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    const/4 v5, 0x0

    :goto_1d
    array-length v0, v2

    if-ge v5, v0, :cond_34

    aget-object v6, v2, v5

    invoke-static {v6}, Lcom/google/android/gms/internal/zzbi;->zzy(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    array-length v0, v7

    if-nez v0, :cond_2a

    goto :goto_31

    :cond_2a
    iget v0, p0, Lcom/google/android/gms/internal/zzbh;->zztq:I

    iget v1, p0, Lcom/google/android/gms/internal/zzbh;->zztp:I

    invoke-static {v7, v0, v1, v4}, Lcom/google/android/gms/internal/zzbk;->zza([Ljava/lang/String;IILjava/util/PriorityQueue;)V

    :goto_31
    add-int/lit8 v5, v5, 0x1

    goto :goto_1d

    :cond_34
    invoke-virtual {v4}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_38
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_59

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/gms/internal/zzbk$zza;

    :try_start_45
    iget-object v0, p0, Lcom/google/android/gms/internal/zzbh;->zzts:Lcom/google/android/gms/internal/zzbg;

    iget-object v1, v6, Lcom/google/android/gms/internal/zzbk$zza;->zztx:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzbg;->zzu(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/internal/zzbh$zza;->write([B)V
    :try_end_50
    .catch Ljava/io/IOException; {:try_start_45 .. :try_end_50} :catch_51

    goto :goto_58

    :catch_51
    move-exception v7

    const-string v0, "Error while writing hash to byteStream"

    invoke-static {v0, v7}, Lcom/google/android/gms/internal/zzin;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_59

    :goto_58
    goto :goto_38

    :cond_59
    :goto_59
    invoke-virtual {v3}, Lcom/google/android/gms/internal/zzbh$zza;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

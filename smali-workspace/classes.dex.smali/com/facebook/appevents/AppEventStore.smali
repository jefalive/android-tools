.class Lcom/facebook/appevents/AppEventStore;
.super Ljava/lang/Object;
.source "AppEventStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/facebook/appevents/AppEventStore$MovedClassObjectInputStream;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 45
    const-class v0, Lcom/facebook/appevents/AppEventStore;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/appevents/AppEventStore;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    return-void
.end method

.method public static declared-synchronized persistEvents(Lcom/facebook/appevents/AccessTokenAppIdPair;Lcom/facebook/appevents/SessionEventsState;)V
    .registers 6
    .param p0, "accessTokenAppIdPair"    # Lcom/facebook/appevents/AccessTokenAppIdPair;
    .param p1, "appEvents"    # Lcom/facebook/appevents/SessionEventsState;

    const-class v3, Lcom/facebook/appevents/AppEventStore;

    monitor-enter v3

    .line 51
    :try_start_3
    invoke-static {}, Lcom/facebook/appevents/internal/AppEventUtility;->assertIsNotMainThread()V

    .line 52
    invoke-static {}, Lcom/facebook/appevents/AppEventStore;->readAndClearStore()Lcom/facebook/appevents/PersistedEvents;

    move-result-object v2

    .line 54
    .local v2, "persistedEvents":Lcom/facebook/appevents/PersistedEvents;
    invoke-virtual {v2, p0}, Lcom/facebook/appevents/PersistedEvents;->containsKey(Lcom/facebook/appevents/AccessTokenAppIdPair;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 55
    invoke-virtual {v2, p0}, Lcom/facebook/appevents/PersistedEvents;->get(Lcom/facebook/appevents/AccessTokenAppIdPair;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/appevents/SessionEventsState;->getEventsToPersist()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_23

    .line 59
    :cond_1c
    invoke-virtual {p1}, Lcom/facebook/appevents/SessionEventsState;->getEventsToPersist()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, p0, v0}, Lcom/facebook/appevents/PersistedEvents;->addEvents(Lcom/facebook/appevents/AccessTokenAppIdPair;Ljava/util/List;)V

    .line 62
    :goto_23
    invoke-static {v2}, Lcom/facebook/appevents/AppEventStore;->saveEventsToDisk(Lcom/facebook/appevents/PersistedEvents;)V
    :try_end_26
    .catchall {:try_start_3 .. :try_end_26} :catchall_28

    .line 63
    monitor-exit v3

    return-void

    .end local v2    # "persistedEvents":Lcom/facebook/appevents/PersistedEvents;
    :catchall_28
    move-exception p0

    monitor-exit v3

    throw p0
.end method

.method public static declared-synchronized persistEvents(Lcom/facebook/appevents/AppEventCollection;)V
    .registers 7
    .param p0, "eventsToPersist"    # Lcom/facebook/appevents/AppEventCollection;

    const-class v5, Lcom/facebook/appevents/AppEventStore;

    monitor-enter v5

    .line 67
    :try_start_3
    invoke-static {}, Lcom/facebook/appevents/internal/AppEventUtility;->assertIsNotMainThread()V

    .line 68
    invoke-static {}, Lcom/facebook/appevents/AppEventStore;->readAndClearStore()Lcom/facebook/appevents/PersistedEvents;

    move-result-object v1

    .line 69
    .local v1, "persistedEvents":Lcom/facebook/appevents/PersistedEvents;
    invoke-virtual {p0}, Lcom/facebook/appevents/AppEventCollection;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_12
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/facebook/appevents/AccessTokenAppIdPair;

    .line 70
    .local v3, "accessTokenAppIdPair":Lcom/facebook/appevents/AccessTokenAppIdPair;
    invoke-virtual {p0, v3}, Lcom/facebook/appevents/AppEventCollection;->get(Lcom/facebook/appevents/AccessTokenAppIdPair;)Lcom/facebook/appevents/SessionEventsState;

    move-result-object v4

    .line 72
    .local v4, "sessionEventsState":Lcom/facebook/appevents/SessionEventsState;
    invoke-virtual {v4}, Lcom/facebook/appevents/SessionEventsState;->getEventsToPersist()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/facebook/appevents/PersistedEvents;->addEvents(Lcom/facebook/appevents/AccessTokenAppIdPair;Ljava/util/List;)V

    .line 75
    .end local v3    # "accessTokenAppIdPair":Lcom/facebook/appevents/AccessTokenAppIdPair;
    .end local v4    # "sessionEventsState":Lcom/facebook/appevents/SessionEventsState;
    goto :goto_12

    .line 77
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_2b
    invoke-static {v1}, Lcom/facebook/appevents/AppEventStore;->saveEventsToDisk(Lcom/facebook/appevents/PersistedEvents;)V
    :try_end_2e
    .catchall {:try_start_3 .. :try_end_2e} :catchall_30

    .line 78
    monitor-exit v5

    return-void

    .end local v1    # "persistedEvents":Lcom/facebook/appevents/PersistedEvents;
    :catchall_30
    move-exception p0

    monitor-exit v5

    throw p0
.end method

.method public static declared-synchronized readAndClearStore()Lcom/facebook/appevents/PersistedEvents;
    .registers 9

    const-class v8, Lcom/facebook/appevents/AppEventStore;

    monitor-enter v8

    .line 82
    :try_start_3
    invoke-static {}, Lcom/facebook/appevents/internal/AppEventUtility;->assertIsNotMainThread()V

    .line 84
    const/4 v2, 0x0

    .line 85
    .local v2, "ois":Lcom/facebook/appevents/AppEventStore$MovedClassObjectInputStream;
    const/4 v3, 0x0

    .line 86
    .local v3, "persistedEvents":Lcom/facebook/appevents/PersistedEvents;
    invoke-static {}, Lcom/facebook/FacebookSdk;->getApplicationContext()Landroid/content/Context;
    :try_end_b
    .catchall {:try_start_3 .. :try_end_b} :catchall_8f

    move-result-object v4

    .line 88
    .local v4, "context":Landroid/content/Context;
    const-string v0, "AppEventsLogger.persistedevents"

    :try_start_e
    invoke-virtual {v4, v0}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v5

    .line 89
    .local v5, "is":Ljava/io/InputStream;
    new-instance v0, Lcom/facebook/appevents/AppEventStore$MovedClassObjectInputStream;

    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v5}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Lcom/facebook/appevents/AppEventStore$MovedClassObjectInputStream;-><init>(Ljava/io/InputStream;)V

    move-object v2, v0

    .line 91
    invoke-virtual {v2}, Lcom/facebook/appevents/AppEventStore$MovedClassObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appevents/PersistedEvents;
    :try_end_23
    .catch Ljava/io/FileNotFoundException; {:try_start_e .. :try_end_23} :catch_3a
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_23} :catch_51
    .catchall {:try_start_e .. :try_end_23} :catchall_6f

    move-object v3, v0

    .line 97
    .end local v5    # "is":Ljava/io/InputStream;
    :try_start_24
    invoke-static {v2}, Lcom/facebook/internal/Utility;->closeQuietly(Ljava/io/Closeable;)V
    :try_end_27
    .catchall {:try_start_24 .. :try_end_27} :catchall_8f

    .line 106
    const-string v0, "AppEventsLogger.persistedevents"

    :try_start_29
    invoke-virtual {v4, v0}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_30
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_30} :catch_31
    .catchall {:try_start_29 .. :try_end_30} :catchall_8f

    .line 109
    goto :goto_86

    .line 107
    :catch_31
    move-exception v5

    .line 108
    .local v5, "ex":Ljava/lang/Exception;
    :try_start_32
    sget-object v0, Lcom/facebook/appevents/AppEventStore;->TAG:Ljava/lang/String;

    const-string v1, "Got unexpected exception when removing events file: "

    invoke-static {v0, v1, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_39
    .catchall {:try_start_32 .. :try_end_39} :catchall_8f

    .line 110
    .end local v5    # "ex":Ljava/lang/Exception;
    goto :goto_86

    .line 92
    :catch_3a
    move-exception v5

    .line 97
    :try_start_3b
    invoke-static {v2}, Lcom/facebook/internal/Utility;->closeQuietly(Ljava/io/Closeable;)V
    :try_end_3e
    .catchall {:try_start_3b .. :try_end_3e} :catchall_8f

    .line 106
    const-string v0, "AppEventsLogger.persistedevents"

    :try_start_40
    invoke-virtual {v4, v0}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_47
    .catch Ljava/lang/Exception; {:try_start_40 .. :try_end_47} :catch_48
    .catchall {:try_start_40 .. :try_end_47} :catchall_8f

    .line 109
    goto :goto_86

    .line 107
    :catch_48
    move-exception v5

    .line 108
    .local v5, "ex":Ljava/lang/Exception;
    :try_start_49
    sget-object v0, Lcom/facebook/appevents/AppEventStore;->TAG:Ljava/lang/String;

    const-string v1, "Got unexpected exception when removing events file: "

    invoke-static {v0, v1, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_50
    .catchall {:try_start_49 .. :try_end_50} :catchall_8f

    .line 110
    .end local v5    # "ex":Ljava/lang/Exception;
    goto :goto_86

    .line 94
    :catch_51
    move-exception v5

    .line 95
    .local v5, "e":Ljava/lang/Exception;
    :try_start_52
    sget-object v0, Lcom/facebook/appevents/AppEventStore;->TAG:Ljava/lang/String;

    const-string v1, "Got unexpected exception while reading events: "

    invoke-static {v0, v1, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_59
    .catchall {:try_start_52 .. :try_end_59} :catchall_6f

    .line 97
    .end local v5    # "e":Ljava/lang/Exception;
    :try_start_59
    invoke-static {v2}, Lcom/facebook/internal/Utility;->closeQuietly(Ljava/io/Closeable;)V
    :try_end_5c
    .catchall {:try_start_59 .. :try_end_5c} :catchall_8f

    .line 106
    const-string v0, "AppEventsLogger.persistedevents"

    :try_start_5e
    invoke-virtual {v4, v0}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_65
    .catch Ljava/lang/Exception; {:try_start_5e .. :try_end_65} :catch_66
    .catchall {:try_start_5e .. :try_end_65} :catchall_8f

    .line 109
    goto :goto_86

    .line 107
    :catch_66
    move-exception v5

    .line 108
    .local v5, "ex":Ljava/lang/Exception;
    :try_start_67
    sget-object v0, Lcom/facebook/appevents/AppEventStore;->TAG:Ljava/lang/String;

    const-string v1, "Got unexpected exception when removing events file: "

    invoke-static {v0, v1, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6e
    .catchall {:try_start_67 .. :try_end_6e} :catchall_8f

    .line 110
    .end local v5    # "ex":Ljava/lang/Exception;
    goto :goto_86

    .line 97
    :catchall_6f
    move-exception v6

    :try_start_70
    invoke-static {v2}, Lcom/facebook/internal/Utility;->closeQuietly(Ljava/io/Closeable;)V
    :try_end_73
    .catchall {:try_start_70 .. :try_end_73} :catchall_8f

    .line 106
    const-string v0, "AppEventsLogger.persistedevents"

    :try_start_75
    invoke-virtual {v4, v0}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_7c
    .catch Ljava/lang/Exception; {:try_start_75 .. :try_end_7c} :catch_7d
    .catchall {:try_start_75 .. :try_end_7c} :catchall_8f

    .line 109
    goto :goto_85

    .line 107
    :catch_7d
    move-exception v7

    .line 108
    .local v7, "ex":Ljava/lang/Exception;
    :try_start_7e
    sget-object v0, Lcom/facebook/appevents/AppEventStore;->TAG:Ljava/lang/String;

    const-string v1, "Got unexpected exception when removing events file: "

    invoke-static {v0, v1, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 109
    .end local v7    # "ex":Ljava/lang/Exception;
    :goto_85
    throw v6

    .line 112
    :goto_86
    if-nez v3, :cond_8d

    .line 113
    new-instance v3, Lcom/facebook/appevents/PersistedEvents;

    invoke-direct {v3}, Lcom/facebook/appevents/PersistedEvents;-><init>()V
    :try_end_8d
    .catchall {:try_start_7e .. :try_end_8d} :catchall_8f

    .line 116
    :cond_8d
    monitor-exit v8

    return-object v3

    .end local v2    # "ois":Lcom/facebook/appevents/AppEventStore$MovedClassObjectInputStream;
    .end local v3    # "persistedEvents":Lcom/facebook/appevents/PersistedEvents;
    .end local v4    # "context":Landroid/content/Context;
    :catchall_8f
    move-exception v2

    monitor-exit v8

    throw v2
.end method

.method private static saveEventsToDisk(Lcom/facebook/appevents/PersistedEvents;)V
    .registers 10
    .param p0, "eventsToPersist"    # Lcom/facebook/appevents/PersistedEvents;

    .line 122
    const/4 v4, 0x0

    .line 123
    .local v4, "oos":Ljava/io/ObjectOutputStream;
    invoke-static {}, Lcom/facebook/FacebookSdk;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    .line 125
    .local v5, "context":Landroid/content/Context;
    :try_start_5
    new-instance v0, Ljava/io/ObjectOutputStream;

    new-instance v1, Ljava/io/BufferedOutputStream;

    const-string v2, "AppEventsLogger.persistedevents"

    const/4 v3, 0x0

    invoke-virtual {v5, v2, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v0, v1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object v4, v0

    .line 128
    invoke-virtual {v4, p0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_1a} :catch_1e
    .catchall {:try_start_5 .. :try_end_1a} :catchall_35

    .line 137
    invoke-static {v4}, Lcom/facebook/internal/Utility;->closeQuietly(Ljava/io/Closeable;)V

    .line 138
    goto :goto_3a

    .line 129
    :catch_1e
    move-exception v6

    .line 130
    .local v6, "e":Ljava/lang/Exception;
    :try_start_1f
    sget-object v0, Lcom/facebook/appevents/AppEventStore;->TAG:Ljava/lang/String;

    const-string v1, "Got unexpected exception while persisting events: "

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_26
    .catchall {:try_start_1f .. :try_end_26} :catchall_35

    .line 132
    const-string v0, "AppEventsLogger.persistedevents"

    :try_start_28
    invoke-virtual {v5, v0}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_2f
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_2f} :catch_30
    .catchall {:try_start_28 .. :try_end_2f} :catchall_35

    .line 135
    goto :goto_31

    .line 133
    :catch_30
    move-exception v7

    .line 137
    .end local v6    # "e":Ljava/lang/Exception;
    :goto_31
    invoke-static {v4}, Lcom/facebook/internal/Utility;->closeQuietly(Ljava/io/Closeable;)V

    .line 138
    goto :goto_3a

    .line 137
    :catchall_35
    move-exception v8

    invoke-static {v4}, Lcom/facebook/internal/Utility;->closeQuietly(Ljava/io/Closeable;)V

    throw v8

    .line 139
    :goto_3a
    return-void
.end method

.class public Lcom/itau/empresas/api/model/FrancesinhaVO$AVencer;
.super Ljava/lang/Object;
.source "FrancesinhaVO.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/api/model/FrancesinhaVO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AVencer"
.end annotation


# instance fields
.field private periodo:Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "periodo"
    .end annotation
.end field

.field private quantidadeTitulos:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade_titulos"
    .end annotation
.end field

.field private valorTitulos:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_titulos"
    .end annotation
.end field


# virtual methods
.method public getPeriodo()Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;
    .registers 2

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/api/model/FrancesinhaVO$AVencer;->periodo:Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;

    return-object v0
.end method

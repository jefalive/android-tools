.class public final Lcom/google/android/gms/internal/zzag$zza;
.super Lcom/google/android/gms/internal/zzso;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/zzag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "zza"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/zzso<Lcom/google/android/gms/internal/zzag$zza;>;"
    }
.end annotation


# static fields
.field private static volatile zzjw:[Lcom/google/android/gms/internal/zzag$zza;


# instance fields
.field public type:I

.field public zzjA:[Lcom/google/android/gms/internal/zzag$zza;

.field public zzjB:Ljava/lang/String;

.field public zzjC:Ljava/lang/String;

.field public zzjD:J

.field public zzjE:Z

.field public zzjF:[Lcom/google/android/gms/internal/zzag$zza;

.field public zzjG:[I

.field public zzjH:Z

.field public zzjx:Ljava/lang/String;

.field public zzjy:[Lcom/google/android/gms/internal/zzag$zza;

.field public zzjz:[Lcom/google/android/gms/internal/zzag$zza;


# direct methods
.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzso;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzag$zza;->zzR()Lcom/google/android/gms/internal/zzag$zza;

    return-void
.end method

.method public static zzQ()[Lcom/google/android/gms/internal/zzag$zza;
    .registers 3

    sget-object v0, Lcom/google/android/gms/internal/zzag$zza;->zzjw:[Lcom/google/android/gms/internal/zzag$zza;

    if-nez v0, :cond_15

    sget-object v1, Lcom/google/android/gms/internal/zzss;->zzbut:Ljava/lang/Object;

    monitor-enter v1

    :try_start_7
    sget-object v0, Lcom/google/android/gms/internal/zzag$zza;->zzjw:[Lcom/google/android/gms/internal/zzag$zza;

    if-nez v0, :cond_10

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/internal/zzag$zza;

    sput-object v0, Lcom/google/android/gms/internal/zzag$zza;->zzjw:[Lcom/google/android/gms/internal/zzag$zza;
    :try_end_10
    .catchall {:try_start_7 .. :try_end_10} :catchall_12

    :cond_10
    monitor-exit v1

    goto :goto_15

    :catchall_12
    move-exception v2

    monitor-exit v1

    throw v2

    :cond_15
    :goto_15
    sget-object v0, Lcom/google/android/gms/internal/zzag$zza;->zzjw:[Lcom/google/android/gms/internal/zzag$zza;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .param p1, "o"    # Ljava/lang/Object;

    if-ne p1, p0, :cond_4

    const/4 v0, 0x1

    return v0

    :cond_4
    instance-of v0, p1, Lcom/google/android/gms/internal/zzag$zza;

    if-nez v0, :cond_a

    const/4 v0, 0x0

    return v0

    :cond_a
    move-object v4, p1

    check-cast v4, Lcom/google/android/gms/internal/zzag$zza;

    iget v0, p0, Lcom/google/android/gms/internal/zzag$zza;->type:I

    iget v1, v4, Lcom/google/android/gms/internal/zzag$zza;->type:I

    if-eq v0, v1, :cond_15

    const/4 v0, 0x0

    return v0

    :cond_15
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjx:Ljava/lang/String;

    if-nez v0, :cond_1f

    iget-object v0, v4, Lcom/google/android/gms/internal/zzag$zza;->zzjx:Ljava/lang/String;

    if-eqz v0, :cond_2b

    const/4 v0, 0x0

    return v0

    :cond_1f
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjx:Ljava/lang/String;

    iget-object v1, v4, Lcom/google/android/gms/internal/zzag$zza;->zzjx:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2b

    const/4 v0, 0x0

    return v0

    :cond_2b
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    iget-object v1, v4, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzss;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_37

    const/4 v0, 0x0

    return v0

    :cond_37
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    iget-object v1, v4, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzss;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_43

    const/4 v0, 0x0

    return v0

    :cond_43
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    iget-object v1, v4, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzss;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4f

    const/4 v0, 0x0

    return v0

    :cond_4f
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjB:Ljava/lang/String;

    if-nez v0, :cond_59

    iget-object v0, v4, Lcom/google/android/gms/internal/zzag$zza;->zzjB:Ljava/lang/String;

    if-eqz v0, :cond_65

    const/4 v0, 0x0

    return v0

    :cond_59
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjB:Ljava/lang/String;

    iget-object v1, v4, Lcom/google/android/gms/internal/zzag$zza;->zzjB:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_65

    const/4 v0, 0x0

    return v0

    :cond_65
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjC:Ljava/lang/String;

    if-nez v0, :cond_6f

    iget-object v0, v4, Lcom/google/android/gms/internal/zzag$zza;->zzjC:Ljava/lang/String;

    if-eqz v0, :cond_7b

    const/4 v0, 0x0

    return v0

    :cond_6f
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjC:Ljava/lang/String;

    iget-object v1, v4, Lcom/google/android/gms/internal/zzag$zza;->zzjC:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7b

    const/4 v0, 0x0

    return v0

    :cond_7b
    iget-wide v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjD:J

    iget-wide v2, v4, Lcom/google/android/gms/internal/zzag$zza;->zzjD:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_85

    const/4 v0, 0x0

    return v0

    :cond_85
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjE:Z

    iget-boolean v1, v4, Lcom/google/android/gms/internal/zzag$zza;->zzjE:Z

    if-eq v0, v1, :cond_8d

    const/4 v0, 0x0

    return v0

    :cond_8d
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjF:[Lcom/google/android/gms/internal/zzag$zza;

    iget-object v1, v4, Lcom/google/android/gms/internal/zzag$zza;->zzjF:[Lcom/google/android/gms/internal/zzag$zza;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzss;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_99

    const/4 v0, 0x0

    return v0

    :cond_99
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    iget-object v1, v4, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzss;->equals([I[I)Z

    move-result v0

    if-nez v0, :cond_a5

    const/4 v0, 0x0

    return v0

    :cond_a5
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjH:Z

    iget-boolean v1, v4, Lcom/google/android/gms/internal/zzag$zza;->zzjH:Z

    if-eq v0, v1, :cond_ad

    const/4 v0, 0x0

    return v0

    :cond_ad
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    if-eqz v0, :cond_b9

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzsq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c9

    :cond_b9
    iget-object v0, v4, Lcom/google/android/gms/internal/zzag$zza;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    if-eqz v0, :cond_c5

    iget-object v0, v4, Lcom/google/android/gms/internal/zzag$zza;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzsq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c7

    :cond_c5
    const/4 v0, 0x1

    goto :goto_c8

    :cond_c7
    const/4 v0, 0x0

    :goto_c8
    return v0

    :cond_c9
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    iget-object v1, v4, Lcom/google/android/gms/internal/zzag$zza;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzsq;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .registers 8

    const/16 v6, 0x11

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v6, v0, 0x20f

    mul-int/lit8 v0, v6, 0x1f

    iget v1, p0, Lcom/google/android/gms/internal/zzag$zza;->type:I

    add-int v6, v0, v1

    mul-int/lit8 v0, v6, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjx:Ljava/lang/String;

    if-nez v1, :cond_1e

    const/4 v1, 0x0

    goto :goto_24

    :cond_1e
    iget-object v1, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjx:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_24
    add-int v6, v0, v1

    mul-int/lit8 v0, v6, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    invoke-static {v1}, Lcom/google/android/gms/internal/zzss;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int v6, v0, v1

    mul-int/lit8 v0, v6, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    invoke-static {v1}, Lcom/google/android/gms/internal/zzss;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int v6, v0, v1

    mul-int/lit8 v0, v6, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    invoke-static {v1}, Lcom/google/android/gms/internal/zzss;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int v6, v0, v1

    mul-int/lit8 v0, v6, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjB:Ljava/lang/String;

    if-nez v1, :cond_4c

    const/4 v1, 0x0

    goto :goto_52

    :cond_4c
    iget-object v1, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjB:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_52
    add-int v6, v0, v1

    mul-int/lit8 v0, v6, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjC:Ljava/lang/String;

    if-nez v1, :cond_5c

    const/4 v1, 0x0

    goto :goto_62

    :cond_5c
    iget-object v1, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjC:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_62
    add-int v6, v0, v1

    mul-int/lit8 v0, v6, 0x1f

    iget-wide v1, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjD:J

    iget-wide v3, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjD:J

    const/16 v5, 0x20

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int v6, v0, v1

    mul-int/lit8 v0, v6, 0x1f

    iget-boolean v1, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjE:Z

    if-eqz v1, :cond_7a

    const/16 v1, 0x4cf

    goto :goto_7c

    :cond_7a
    const/16 v1, 0x4d5

    :goto_7c
    add-int v6, v0, v1

    mul-int/lit8 v0, v6, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjF:[Lcom/google/android/gms/internal/zzag$zza;

    invoke-static {v1}, Lcom/google/android/gms/internal/zzss;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int v6, v0, v1

    mul-int/lit8 v0, v6, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    invoke-static {v1}, Lcom/google/android/gms/internal/zzss;->hashCode([I)I

    move-result v1

    add-int v6, v0, v1

    mul-int/lit8 v0, v6, 0x1f

    iget-boolean v1, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjH:Z

    if-eqz v1, :cond_9b

    const/16 v1, 0x4cf

    goto :goto_9d

    :cond_9b
    const/16 v1, 0x4d5

    :goto_9d
    add-int v6, v0, v1

    mul-int/lit8 v0, v6, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzag$zza;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    if-eqz v1, :cond_ad

    iget-object v1, p0, Lcom/google/android/gms/internal/zzag$zza;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zzsq;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_af

    :cond_ad
    const/4 v1, 0x0

    goto :goto_b5

    :cond_af
    iget-object v1, p0, Lcom/google/android/gms/internal/zzag$zza;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zzsq;->hashCode()I

    move-result v1

    :goto_b5
    add-int v6, v0, v1

    return v6
.end method

.method public synthetic mergeFrom(Lcom/google/android/gms/internal/zzsm;)Lcom/google/android/gms/internal/zzsu;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/zzag$zza;->zzk(Lcom/google/android/gms/internal/zzsm;)Lcom/google/android/gms/internal/zzag$zza;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/android/gms/internal/zzsn;)V
    .registers 8
    .param p1, "output"    # Lcom/google/android/gms/internal/zzsn;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/google/android/gms/internal/zzag$zza;->type:I

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzA(II)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjx:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjx:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzn(ILjava/lang/String;)V

    :cond_16
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    if-eqz v0, :cond_32

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    if-lez v0, :cond_32

    const/4 v4, 0x0

    :goto_20
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    if-ge v4, v0, :cond_32

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    aget-object v5, v0, v4

    if-eqz v5, :cond_2f

    const/4 v0, 0x3

    invoke-virtual {p1, v0, v5}, Lcom/google/android/gms/internal/zzsn;->zza(ILcom/google/android/gms/internal/zzsu;)V

    :cond_2f
    add-int/lit8 v4, v4, 0x1

    goto :goto_20

    :cond_32
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    if-eqz v0, :cond_4e

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    if-lez v0, :cond_4e

    const/4 v4, 0x0

    :goto_3c
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    if-ge v4, v0, :cond_4e

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    aget-object v5, v0, v4

    if-eqz v5, :cond_4b

    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, Lcom/google/android/gms/internal/zzsn;->zza(ILcom/google/android/gms/internal/zzsu;)V

    :cond_4b
    add-int/lit8 v4, v4, 0x1

    goto :goto_3c

    :cond_4e
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    if-eqz v0, :cond_6a

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    if-lez v0, :cond_6a

    const/4 v4, 0x0

    :goto_58
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    if-ge v4, v0, :cond_6a

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    aget-object v5, v0, v4

    if-eqz v5, :cond_67

    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, Lcom/google/android/gms/internal/zzsn;->zza(ILcom/google/android/gms/internal/zzsu;)V

    :cond_67
    add-int/lit8 v4, v4, 0x1

    goto :goto_58

    :cond_6a
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjB:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7a

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjB:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzn(ILjava/lang/String;)V

    :cond_7a
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjC:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8a

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjC:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzn(ILjava/lang/String;)V

    :cond_8a
    iget-wide v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjD:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_99

    iget-wide v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjD:J

    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/android/gms/internal/zzsn;->zzb(IJ)V

    :cond_99
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjH:Z

    if-eqz v0, :cond_a4

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjH:Z

    const/16 v1, 0x9

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/internal/zzsn;->zze(IZ)V

    :cond_a4
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    if-eqz v0, :cond_bf

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    array-length v0, v0

    if-lez v0, :cond_bf

    const/4 v4, 0x0

    :goto_ae
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    array-length v0, v0

    if-ge v4, v0, :cond_bf

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    aget v0, v0, v4

    const/16 v1, 0xa

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzA(II)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_ae

    :cond_bf
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjF:[Lcom/google/android/gms/internal/zzag$zza;

    if-eqz v0, :cond_dc

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjF:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    if-lez v0, :cond_dc

    const/4 v4, 0x0

    :goto_c9
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjF:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    if-ge v4, v0, :cond_dc

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjF:[Lcom/google/android/gms/internal/zzag$zza;

    aget-object v5, v0, v4

    if-eqz v5, :cond_d9

    const/16 v0, 0xb

    invoke-virtual {p1, v0, v5}, Lcom/google/android/gms/internal/zzsn;->zza(ILcom/google/android/gms/internal/zzsu;)V

    :cond_d9
    add-int/lit8 v4, v4, 0x1

    goto :goto_c9

    :cond_dc
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjE:Z

    if-eqz v0, :cond_e7

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjE:Z

    const/16 v1, 0xc

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/internal/zzsn;->zze(IZ)V

    :cond_e7
    invoke-super {p0, p1}, Lcom/google/android/gms/internal/zzso;->writeTo(Lcom/google/android/gms/internal/zzsn;)V

    return-void
.end method

.method public zzR()Lcom/google/android/gms/internal/zzag$zza;
    .registers 3

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzag$zza;->type:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjx:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/internal/zzag$zza;->zzQ()[Lcom/google/android/gms/internal/zzag$zza;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    invoke-static {}, Lcom/google/android/gms/internal/zzag$zza;->zzQ()[Lcom/google/android/gms/internal/zzag$zza;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    invoke-static {}, Lcom/google/android/gms/internal/zzag$zza;->zzQ()[Lcom/google/android/gms/internal/zzag$zza;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjB:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjC:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjD:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjE:Z

    invoke-static {}, Lcom/google/android/gms/internal/zzag$zza;->zzQ()[Lcom/google/android/gms/internal/zzag$zza;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjF:[Lcom/google/android/gms/internal/zzag$zza;

    sget-object v0, Lcom/google/android/gms/internal/zzsx;->zzbuw:[I

    iput-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjH:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzbuu:I

    return-object p0
.end method

.method public zzk(Lcom/google/android/gms/internal/zzsm;)Lcom/google/android/gms/internal/zzag$zza;
    .registers 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzIX()I

    move-result v3

    sparse-switch v3, :sswitch_data_1f4

    goto :goto_9

    :sswitch_8
    return-object p0

    :goto_9
    invoke-virtual {p0, p1, v3}, Lcom/google/android/gms/internal/zzag$zza;->zza(Lcom/google/android/gms/internal/zzsm;I)Z

    move-result v0

    if-nez v0, :cond_1f2

    return-object p0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJb()I

    move-result v4

    packed-switch v4, :pswitch_data_22e

    goto :goto_1a

    :pswitch_18
    iput v4, p0, Lcom/google/android/gms/internal/zzag$zza;->type:I

    :goto_1a
    goto/16 :goto_1f2

    :sswitch_1c
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjx:Ljava/lang/String;

    goto/16 :goto_1f2

    :sswitch_24
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/zzsx;->zzc(Lcom/google/android/gms/internal/zzsm;I)I

    move-result v4

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    if-nez v0, :cond_30

    const/4 v5, 0x0

    goto :goto_33

    :cond_30
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v5, v0

    :goto_33
    add-int v0, v5, v4

    new-array v6, v0, [Lcom/google/android/gms/internal/zzag$zza;

    if-eqz v5, :cond_40

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v6, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_40
    :goto_40
    array-length v0, v6

    add-int/lit8 v0, v0, -0x1

    if-ge v5, v0, :cond_57

    new-instance v0, Lcom/google/android/gms/internal/zzag$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzag$zza;-><init>()V

    aput-object v0, v6, v5

    aget-object v0, v6, v5

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/zzsm;->zza(Lcom/google/android/gms/internal/zzsu;)V

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzIX()I

    add-int/lit8 v5, v5, 0x1

    goto :goto_40

    :cond_57
    new-instance v0, Lcom/google/android/gms/internal/zzag$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzag$zza;-><init>()V

    aput-object v0, v6, v5

    aget-object v0, v6, v5

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/zzsm;->zza(Lcom/google/android/gms/internal/zzsu;)V

    iput-object v6, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    goto/16 :goto_1f2

    :sswitch_67
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/zzsx;->zzc(Lcom/google/android/gms/internal/zzsm;I)I

    move-result v4

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    if-nez v0, :cond_73

    const/4 v5, 0x0

    goto :goto_76

    :cond_73
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v5, v0

    :goto_76
    add-int v0, v5, v4

    new-array v6, v0, [Lcom/google/android/gms/internal/zzag$zza;

    if-eqz v5, :cond_83

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v6, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_83
    :goto_83
    array-length v0, v6

    add-int/lit8 v0, v0, -0x1

    if-ge v5, v0, :cond_9a

    new-instance v0, Lcom/google/android/gms/internal/zzag$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzag$zza;-><init>()V

    aput-object v0, v6, v5

    aget-object v0, v6, v5

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/zzsm;->zza(Lcom/google/android/gms/internal/zzsu;)V

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzIX()I

    add-int/lit8 v5, v5, 0x1

    goto :goto_83

    :cond_9a
    new-instance v0, Lcom/google/android/gms/internal/zzag$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzag$zza;-><init>()V

    aput-object v0, v6, v5

    aget-object v0, v6, v5

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/zzsm;->zza(Lcom/google/android/gms/internal/zzsu;)V

    iput-object v6, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    goto/16 :goto_1f2

    :sswitch_aa
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/zzsx;->zzc(Lcom/google/android/gms/internal/zzsm;I)I

    move-result v4

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    if-nez v0, :cond_b6

    const/4 v5, 0x0

    goto :goto_b9

    :cond_b6
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v5, v0

    :goto_b9
    add-int v0, v5, v4

    new-array v6, v0, [Lcom/google/android/gms/internal/zzag$zza;

    if-eqz v5, :cond_c6

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v6, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_c6
    :goto_c6
    array-length v0, v6

    add-int/lit8 v0, v0, -0x1

    if-ge v5, v0, :cond_dd

    new-instance v0, Lcom/google/android/gms/internal/zzag$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzag$zza;-><init>()V

    aput-object v0, v6, v5

    aget-object v0, v6, v5

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/zzsm;->zza(Lcom/google/android/gms/internal/zzsu;)V

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzIX()I

    add-int/lit8 v5, v5, 0x1

    goto :goto_c6

    :cond_dd
    new-instance v0, Lcom/google/android/gms/internal/zzag$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzag$zza;-><init>()V

    aput-object v0, v6, v5

    aget-object v0, v6, v5

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/zzsm;->zza(Lcom/google/android/gms/internal/zzsu;)V

    iput-object v6, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    goto/16 :goto_1f2

    :sswitch_ed
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjB:Ljava/lang/String;

    goto/16 :goto_1f2

    :sswitch_f5
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjC:Ljava/lang/String;

    goto/16 :goto_1f2

    :sswitch_fd
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJa()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjD:J

    goto/16 :goto_1f2

    :sswitch_105
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJc()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjH:Z

    goto/16 :goto_1f2

    :sswitch_10d
    const/16 v0, 0x50

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/zzsx;->zzc(Lcom/google/android/gms/internal/zzsm;I)I

    move-result v4

    new-array v5, v4, [I

    const/4 v6, 0x0

    const/4 v7, 0x0

    :goto_117
    if-ge v7, v4, :cond_12e

    if-eqz v7, :cond_11e

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzIX()I

    :cond_11e
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJb()I

    move-result v8

    packed-switch v8, :pswitch_data_242

    goto :goto_12b

    :pswitch_126
    move v0, v6

    add-int/lit8 v6, v6, 0x1

    aput v8, v5, v0

    :goto_12b
    add-int/lit8 v7, v7, 0x1

    goto :goto_117

    :cond_12e
    if-eqz v6, :cond_1f2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    if-nez v0, :cond_136

    const/4 v7, 0x0

    goto :goto_139

    :cond_136
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    array-length v7, v0

    :goto_139
    if-nez v7, :cond_141

    array-length v0, v5

    if-ne v6, v0, :cond_141

    iput-object v5, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    goto :goto_154

    :cond_141
    add-int v0, v7, v6

    new-array v8, v0, [I

    if-eqz v7, :cond_14e

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v8, v2, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_14e
    const/4 v0, 0x0

    invoke-static {v5, v0, v8, v7, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v8, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    :goto_154
    goto/16 :goto_1f2

    :sswitch_156
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJf()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/android/gms/internal/zzsm;->zzmq(I)I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->getPosition()I

    move-result v7

    :goto_163
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJk()I

    move-result v0

    if-lez v0, :cond_174

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJb()I

    move-result v0

    packed-switch v0, :pswitch_data_268

    goto :goto_173

    :pswitch_171
    add-int/lit8 v6, v6, 0x1

    :goto_173
    goto :goto_163

    :cond_174
    if-eqz v6, :cond_1a5

    invoke-virtual {p1, v7}, Lcom/google/android/gms/internal/zzsm;->zzms(I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    if-nez v0, :cond_17f

    const/4 v8, 0x0

    goto :goto_182

    :cond_17f
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    array-length v8, v0

    :goto_182
    add-int v0, v8, v6

    new-array v9, v0, [I

    if-eqz v8, :cond_18f

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v9, v2, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_18f
    :goto_18f
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJk()I

    move-result v0

    if-lez v0, :cond_1a3

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJb()I

    move-result v10

    packed-switch v10, :pswitch_data_28e

    goto :goto_1a2

    :pswitch_19d
    move v0, v8

    add-int/lit8 v8, v8, 0x1

    aput v10, v9, v0

    :goto_1a2
    goto :goto_18f

    :cond_1a3
    iput-object v9, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    :cond_1a5
    invoke-virtual {p1, v5}, Lcom/google/android/gms/internal/zzsm;->zzmr(I)V

    goto/16 :goto_1f2

    :sswitch_1aa
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/zzsx;->zzc(Lcom/google/android/gms/internal/zzsm;I)I

    move-result v4

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjF:[Lcom/google/android/gms/internal/zzag$zza;

    if-nez v0, :cond_1b6

    const/4 v5, 0x0

    goto :goto_1b9

    :cond_1b6
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjF:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v5, v0

    :goto_1b9
    add-int v0, v5, v4

    new-array v6, v0, [Lcom/google/android/gms/internal/zzag$zza;

    if-eqz v5, :cond_1c6

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjF:[Lcom/google/android/gms/internal/zzag$zza;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v6, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1c6
    :goto_1c6
    array-length v0, v6

    add-int/lit8 v0, v0, -0x1

    if-ge v5, v0, :cond_1dd

    new-instance v0, Lcom/google/android/gms/internal/zzag$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzag$zza;-><init>()V

    aput-object v0, v6, v5

    aget-object v0, v6, v5

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/zzsm;->zza(Lcom/google/android/gms/internal/zzsu;)V

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzIX()I

    add-int/lit8 v5, v5, 0x1

    goto :goto_1c6

    :cond_1dd
    new-instance v0, Lcom/google/android/gms/internal/zzag$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzag$zza;-><init>()V

    aput-object v0, v6, v5

    aget-object v0, v6, v5

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/zzsm;->zza(Lcom/google/android/gms/internal/zzsu;)V

    iput-object v6, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjF:[Lcom/google/android/gms/internal/zzag$zza;

    goto :goto_1f2

    :sswitch_1ec
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJc()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjE:Z

    :cond_1f2
    :goto_1f2
    goto/16 :goto_0

    :sswitch_data_1f4
    .sparse-switch
        0x0 -> :sswitch_8
        0x8 -> :sswitch_10
        0x12 -> :sswitch_1c
        0x1a -> :sswitch_24
        0x22 -> :sswitch_67
        0x2a -> :sswitch_aa
        0x32 -> :sswitch_ed
        0x3a -> :sswitch_f5
        0x40 -> :sswitch_fd
        0x48 -> :sswitch_105
        0x50 -> :sswitch_10d
        0x52 -> :sswitch_156
        0x5a -> :sswitch_1aa
        0x60 -> :sswitch_1ec
    .end sparse-switch

    :pswitch_data_22e
    .packed-switch 0x1
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
    .end packed-switch

    :pswitch_data_242
    .packed-switch 0x1
        :pswitch_126
        :pswitch_126
        :pswitch_126
        :pswitch_126
        :pswitch_126
        :pswitch_126
        :pswitch_126
        :pswitch_126
        :pswitch_126
        :pswitch_126
        :pswitch_126
        :pswitch_126
        :pswitch_126
        :pswitch_126
        :pswitch_126
        :pswitch_126
        :pswitch_126
    .end packed-switch

    :pswitch_data_268
    .packed-switch 0x1
        :pswitch_171
        :pswitch_171
        :pswitch_171
        :pswitch_171
        :pswitch_171
        :pswitch_171
        :pswitch_171
        :pswitch_171
        :pswitch_171
        :pswitch_171
        :pswitch_171
        :pswitch_171
        :pswitch_171
        :pswitch_171
        :pswitch_171
        :pswitch_171
        :pswitch_171
    .end packed-switch

    :pswitch_data_28e
    .packed-switch 0x1
        :pswitch_19d
        :pswitch_19d
        :pswitch_19d
        :pswitch_19d
        :pswitch_19d
        :pswitch_19d
        :pswitch_19d
        :pswitch_19d
        :pswitch_19d
        :pswitch_19d
        :pswitch_19d
        :pswitch_19d
        :pswitch_19d
        :pswitch_19d
        :pswitch_19d
        :pswitch_19d
        :pswitch_19d
    .end packed-switch
.end method

.method protected zzz()I
    .registers 9

    invoke-super {p0}, Lcom/google/android/gms/internal/zzso;->zzz()I

    move-result v4

    iget v0, p0, Lcom/google/android/gms/internal/zzag$zza;->type:I

    const/4 v1, 0x1

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzC(II)I

    move-result v0

    add-int/2addr v4, v0

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjx:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1e

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjx:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzo(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v4, v0

    :cond_1e
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    if-eqz v0, :cond_3c

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    if-lez v0, :cond_3c

    const/4 v5, 0x0

    :goto_28
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    if-ge v5, v0, :cond_3c

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    aget-object v6, v0, v5

    if-eqz v6, :cond_39

    const/4 v0, 0x3

    invoke-static {v0, v6}, Lcom/google/android/gms/internal/zzsn;->zzc(ILcom/google/android/gms/internal/zzsu;)I

    move-result v0

    add-int/2addr v4, v0

    :cond_39
    add-int/lit8 v5, v5, 0x1

    goto :goto_28

    :cond_3c
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    if-eqz v0, :cond_5a

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    if-lez v0, :cond_5a

    const/4 v5, 0x0

    :goto_46
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    if-ge v5, v0, :cond_5a

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    aget-object v6, v0, v5

    if-eqz v6, :cond_57

    const/4 v0, 0x4

    invoke-static {v0, v6}, Lcom/google/android/gms/internal/zzsn;->zzc(ILcom/google/android/gms/internal/zzsu;)I

    move-result v0

    add-int/2addr v4, v0

    :cond_57
    add-int/lit8 v5, v5, 0x1

    goto :goto_46

    :cond_5a
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    if-eqz v0, :cond_78

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    if-lez v0, :cond_78

    const/4 v5, 0x0

    :goto_64
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    if-ge v5, v0, :cond_78

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    aget-object v6, v0, v5

    if-eqz v6, :cond_75

    const/4 v0, 0x5

    invoke-static {v0, v6}, Lcom/google/android/gms/internal/zzsn;->zzc(ILcom/google/android/gms/internal/zzsu;)I

    move-result v0

    add-int/2addr v4, v0

    :cond_75
    add-int/lit8 v5, v5, 0x1

    goto :goto_64

    :cond_78
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjB:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8a

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjB:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzo(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v4, v0

    :cond_8a
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjC:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9c

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjC:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzo(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v4, v0

    :cond_9c
    iget-wide v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjD:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_ad

    iget-wide v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjD:J

    const/16 v2, 0x8

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/internal/zzsn;->zzd(IJ)I

    move-result v0

    add-int/2addr v4, v0

    :cond_ad
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjH:Z

    if-eqz v0, :cond_ba

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjH:Z

    const/16 v1, 0x9

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzf(IZ)I

    move-result v0

    add-int/2addr v4, v0

    :cond_ba
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    if-eqz v0, :cond_dd

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    array-length v0, v0

    if-lez v0, :cond_dd

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_c5
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    array-length v0, v0

    if-ge v6, v0, :cond_d6

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    aget v7, v0, v6

    invoke-static {v7}, Lcom/google/android/gms/internal/zzsn;->zzmx(I)I

    move-result v0

    add-int/2addr v5, v0

    add-int/lit8 v6, v6, 0x1

    goto :goto_c5

    :cond_d6
    add-int/2addr v4, v5

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v4, v0

    :cond_dd
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjF:[Lcom/google/android/gms/internal/zzag$zza;

    if-eqz v0, :cond_fc

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjF:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    if-lez v0, :cond_fc

    const/4 v5, 0x0

    :goto_e7
    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjF:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    if-ge v5, v0, :cond_fc

    iget-object v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjF:[Lcom/google/android/gms/internal/zzag$zza;

    aget-object v6, v0, v5

    if-eqz v6, :cond_f9

    const/16 v0, 0xb

    invoke-static {v0, v6}, Lcom/google/android/gms/internal/zzsn;->zzc(ILcom/google/android/gms/internal/zzsu;)I

    move-result v0

    add-int/2addr v4, v0

    :cond_f9
    add-int/lit8 v5, v5, 0x1

    goto :goto_e7

    :cond_fc
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjE:Z

    if-eqz v0, :cond_109

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzag$zza;->zzjE:Z

    const/16 v1, 0xc

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzf(IZ)I

    move-result v0

    add-int/2addr v4, v0

    :cond_109
    return v4
.end method

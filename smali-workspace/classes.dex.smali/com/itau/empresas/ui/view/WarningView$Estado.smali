.class public final enum Lcom/itau/empresas/ui/view/WarningView$Estado;
.super Ljava/lang/Enum;
.source "WarningView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/view/WarningView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Estado"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/itau/empresas/ui/view/WarningView$Estado;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/itau/empresas/ui/view/WarningView$Estado;

.field public static final enum AGUARDANDO:Lcom/itau/empresas/ui/view/WarningView$Estado;

.field public static final enum ATENCAO:Lcom/itau/empresas/ui/view/WarningView$Estado;

.field public static final enum SUCESSO:Lcom/itau/empresas/ui/view/WarningView$Estado;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 21
    new-instance v0, Lcom/itau/empresas/ui/view/WarningView$Estado;

    const-string v1, "SUCESSO"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/ui/view/WarningView$Estado;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/ui/view/WarningView$Estado;->SUCESSO:Lcom/itau/empresas/ui/view/WarningView$Estado;

    new-instance v0, Lcom/itau/empresas/ui/view/WarningView$Estado;

    const-string v1, "AGUARDANDO"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/ui/view/WarningView$Estado;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/ui/view/WarningView$Estado;->AGUARDANDO:Lcom/itau/empresas/ui/view/WarningView$Estado;

    new-instance v0, Lcom/itau/empresas/ui/view/WarningView$Estado;

    const-string v1, "ATENCAO"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/ui/view/WarningView$Estado;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/ui/view/WarningView$Estado;->ATENCAO:Lcom/itau/empresas/ui/view/WarningView$Estado;

    .line 20
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/itau/empresas/ui/view/WarningView$Estado;

    sget-object v1, Lcom/itau/empresas/ui/view/WarningView$Estado;->SUCESSO:Lcom/itau/empresas/ui/view/WarningView$Estado;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/ui/view/WarningView$Estado;->AGUARDANDO:Lcom/itau/empresas/ui/view/WarningView$Estado;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/ui/view/WarningView$Estado;->ATENCAO:Lcom/itau/empresas/ui/view/WarningView$Estado;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/ui/view/WarningView$Estado;->$VALUES:[Lcom/itau/empresas/ui/view/WarningView$Estado;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/itau/empresas/ui/view/WarningView$Estado;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 20
    const-class v0, Lcom/itau/empresas/ui/view/WarningView$Estado;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/WarningView$Estado;

    return-object v0
.end method

.method public static values()[Lcom/itau/empresas/ui/view/WarningView$Estado;
    .registers 1

    .line 20
    sget-object v0, Lcom/itau/empresas/ui/view/WarningView$Estado;->$VALUES:[Lcom/itau/empresas/ui/view/WarningView$Estado;

    invoke-virtual {v0}, [Lcom/itau/empresas/ui/view/WarningView$Estado;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/itau/empresas/ui/view/WarningView$Estado;

    return-object v0
.end method

.class public Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;
.super Landroid/widget/LinearLayout;
.source "ItemContatoRecargaView.java"


# instance fields
.field application:Lcom/itau/empresas/CustomApplication;

.field botaoEditarContato:Landroid/widget/ImageButton;

.field botaoExcluir:Landroid/widget/ImageButton;

.field private contato:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

.field contatoRecargaController:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

.field textoNomeContato:Landroid/widget/TextView;

.field textoNumeroContato:Landroid/widget/TextView;

.field textoOperadoraContato:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 46
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 50
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 54
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;)Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;

    .line 31
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->contato:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    return-object v0
.end method

.method private static constuirTextOperadoraContato(Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;)Ljava/lang/String;
    .registers 5
    .param p0, "item"    # Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 132
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getEstadoOperadora()Ljava/lang/String;

    move-result-object v2

    .line 133
    .local v2, "estadoOperadora":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v3

    .line 134
    .local v3, "nomeOperadora":Ljava/lang/String;
    const-string v0, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_28

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_29

    :cond_28
    move-object v0, v3

    :goto_29
    return-object v0
.end method


# virtual methods
.method public ajustaDados(Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;Z)Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;
    .registers 7
    .param p1, "item"    # Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;
    .param p2, "editar"    # Z

    .line 116
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->contato:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 117
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->textoNomeContato:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getApelido()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->textoNumeroContato:Landroid/widget/TextView;

    .line 119
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getDdd()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getTelefone()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/ViewUtils;->formataNumeroTelefonico(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 118
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->textoOperadoraContato:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->constuirTextOperadoraContato(Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    if-eqz p2, :cond_38

    .line 122
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->botaoEditarContato:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 123
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->botaoExcluir:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_44

    .line 125
    :cond_38
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->botaoEditarContato:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 126
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->botaoExcluir:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 128
    :goto_44
    return-object p0
.end method

.method clicouEmContato()V
    .registers 6

    .line 102
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 104
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0702e7

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 105
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0702c6

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 107
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->contato:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/activity/BaseActivity;->publicaEvento(Ljava/lang/Object;)V

    .line 108
    return-void
.end method

.method editarContato()V
    .registers 6

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 67
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0702e7

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 68
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0702a5

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 70
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->contato:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 71
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->contatoRecarga(Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;)Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;->EDITAR_CONTATO:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;

    .line 72
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->tipo(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity$TIPOS;)Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;

    move-result-object v0

    .line 73
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaCadastroContatoActivity_$IntentBuilder_;->startForResult(I)Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 74
    return-void
.end method

.method excluirContato()V
    .registers 6

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 80
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0702e7

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 81
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f070295

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 83
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/SnackbarUtils;->tentaDispensarSnackbarNo(Ljava/lang/Object;)V

    .line 84
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 85
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->textoNomeContato:Landroid/widget/TextView;

    .line 87
    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 85
    const v3, 0x7f0706bb

    invoke-virtual {v1, v3, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView$1;-><init>(Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;)V

    .line 88
    const v2, 0x7f0704d2

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 96
    const v1, 0x7f070479

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 98
    return-void
.end method

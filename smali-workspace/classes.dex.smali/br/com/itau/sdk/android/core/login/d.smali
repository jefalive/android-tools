.class final synthetic Lbr/com/itau/sdk/android/core/login/d;
.super Ljava/lang/Object;

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# instance fields
.field private final a:Lbr/com/itau/sdk/android/core/login/b;

.field private final b:Ljava/lang/String;

.field private final c:Lbr/com/itau/sdk/android/core/login/EletronicPassword;


# direct methods
.method private constructor <init>(Lbr/com/itau/sdk/android/core/login/b;Ljava/lang/String;Lbr/com/itau/sdk/android/core/login/EletronicPassword;)V
    .registers 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbr/com/itau/sdk/android/core/login/d;->a:Lbr/com/itau/sdk/android/core/login/b;

    iput-object p2, p0, Lbr/com/itau/sdk/android/core/login/d;->b:Ljava/lang/String;

    iput-object p3, p0, Lbr/com/itau/sdk/android/core/login/d;->c:Lbr/com/itau/sdk/android/core/login/EletronicPassword;

    return-void
.end method

.method public static a(Lbr/com/itau/sdk/android/core/login/b;Ljava/lang/String;Lbr/com/itau/sdk/android/core/login/EletronicPassword;)Lbr/com/itau/sdk/android/core/type/BackendCall;
    .registers 4

    new-instance v0, Lbr/com/itau/sdk/android/core/login/d;

    invoke-direct {v0, p0, p1, p2}, Lbr/com/itau/sdk/android/core/login/d;-><init>(Lbr/com/itau/sdk/android/core/login/b;Ljava/lang/String;Lbr/com/itau/sdk/android/core/login/EletronicPassword;)V

    return-object v0
.end method


# virtual methods
.method public callBackend()V
    .registers 4
    .annotation runtime Ljava/lang/invoke/LambdaForm$Hidden;
    .end annotation

    iget-object v0, p0, Lbr/com/itau/sdk/android/core/login/d;->a:Lbr/com/itau/sdk/android/core/login/b;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/login/d;->b:Ljava/lang/String;

    iget-object v2, p0, Lbr/com/itau/sdk/android/core/login/d;->c:Lbr/com/itau/sdk/android/core/login/EletronicPassword;

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/login/b;->a(Lbr/com/itau/sdk/android/core/login/b;Ljava/lang/String;Lbr/com/itau/sdk/android/core/login/EletronicPassword;)V

    return-void
.end method

.class public final Lbr/com/itau/textovoz/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final abc_action_bar_content_inset_material:I = 0x7f08001a

.field public static final abc_action_bar_content_inset_with_nav:I = 0x7f08001b

.field public static final abc_action_bar_default_height_material:I = 0x7f080002

.field public static final abc_action_bar_default_padding_end_material:I = 0x7f08001c

.field public static final abc_action_bar_default_padding_start_material:I = 0x7f08001d

.field public static final abc_action_bar_elevation_material:I = 0x7f080040

.field public static final abc_action_bar_icon_vertical_padding_material:I = 0x7f080041

.field public static final abc_action_bar_overflow_padding_end_material:I = 0x7f080042

.field public static final abc_action_bar_overflow_padding_start_material:I = 0x7f080043

.field public static final abc_action_bar_progress_bar_size:I = 0x7f080003

.field public static final abc_action_bar_stacked_max_height:I = 0x7f080044

.field public static final abc_action_bar_stacked_tab_max_width:I = 0x7f080045

.field public static final abc_action_bar_subtitle_bottom_margin_material:I = 0x7f080046

.field public static final abc_action_bar_subtitle_top_margin_material:I = 0x7f080047

.field public static final abc_action_button_min_height_material:I = 0x7f080048

.field public static final abc_action_button_min_width_material:I = 0x7f080049

.field public static final abc_action_button_min_width_overflow_material:I = 0x7f08004a

.field public static final abc_alert_dialog_button_bar_height:I = 0x7f080000

.field public static final abc_button_inset_horizontal_material:I = 0x7f08004b

.field public static final abc_button_inset_vertical_material:I = 0x7f08004c

.field public static final abc_button_padding_horizontal_material:I = 0x7f08004d

.field public static final abc_button_padding_vertical_material:I = 0x7f08004e

.field public static final abc_cascading_menus_min_smallest_width:I = 0x7f08004f

.field public static final abc_config_prefDialogWidth:I = 0x7f08000b

.field public static final abc_control_corner_material:I = 0x7f080050

.field public static final abc_control_inset_material:I = 0x7f080051

.field public static final abc_control_padding_material:I = 0x7f080052

.field public static final abc_dialog_fixed_height_major:I = 0x7f08000c

.field public static final abc_dialog_fixed_height_minor:I = 0x7f08000d

.field public static final abc_dialog_fixed_width_major:I = 0x7f08000e

.field public static final abc_dialog_fixed_width_minor:I = 0x7f08000f

.field public static final abc_dialog_list_padding_vertical_material:I = 0x7f080053

.field public static final abc_dialog_min_width_major:I = 0x7f080010

.field public static final abc_dialog_min_width_minor:I = 0x7f080011

.field public static final abc_dialog_padding_material:I = 0x7f080054

.field public static final abc_dialog_padding_top_material:I = 0x7f080055

.field public static final abc_disabled_alpha_material_dark:I = 0x7f080056

.field public static final abc_disabled_alpha_material_light:I = 0x7f080057

.field public static final abc_dropdownitem_icon_width:I = 0x7f080058

.field public static final abc_dropdownitem_text_padding_left:I = 0x7f080059

.field public static final abc_dropdownitem_text_padding_right:I = 0x7f08005a

.field public static final abc_edit_text_inset_bottom_material:I = 0x7f08005b

.field public static final abc_edit_text_inset_horizontal_material:I = 0x7f08005c

.field public static final abc_edit_text_inset_top_material:I = 0x7f08005d

.field public static final abc_floating_window_z:I = 0x7f08005e

.field public static final abc_list_item_padding_horizontal_material:I = 0x7f08005f

.field public static final abc_panel_menu_list_width:I = 0x7f080060

.field public static final abc_progress_bar_height_material:I = 0x7f080061

.field public static final abc_search_view_preferred_height:I = 0x7f080062

.field public static final abc_search_view_preferred_width:I = 0x7f080063

.field public static final abc_seekbar_track_background_height_material:I = 0x7f080064

.field public static final abc_seekbar_track_progress_height_material:I = 0x7f080065

.field public static final abc_select_dialog_padding_start_material:I = 0x7f080066

.field public static final abc_switch_padding:I = 0x7f08003a

.field public static final abc_text_size_body_1_material:I = 0x7f080067

.field public static final abc_text_size_body_2_material:I = 0x7f080068

.field public static final abc_text_size_button_material:I = 0x7f080069

.field public static final abc_text_size_caption_material:I = 0x7f08006a

.field public static final abc_text_size_display_1_material:I = 0x7f08006b

.field public static final abc_text_size_display_2_material:I = 0x7f08006c

.field public static final abc_text_size_display_3_material:I = 0x7f08006d

.field public static final abc_text_size_display_4_material:I = 0x7f08006e

.field public static final abc_text_size_headline_material:I = 0x7f08006f

.field public static final abc_text_size_large_material:I = 0x7f080070

.field public static final abc_text_size_medium_material:I = 0x7f080071

.field public static final abc_text_size_menu_header_material:I = 0x7f080072

.field public static final abc_text_size_menu_material:I = 0x7f080073

.field public static final abc_text_size_small_material:I = 0x7f080074

.field public static final abc_text_size_subhead_material:I = 0x7f080075

.field public static final abc_text_size_subtitle_material_toolbar:I = 0x7f080004

.field public static final abc_text_size_title_material:I = 0x7f080076

.field public static final abc_text_size_title_material_toolbar:I = 0x7f080005

.field public static final about_image_arrow_margin_right_size:I = 0x7f080077

.field public static final about_img_arrow_margin_top:I = 0x7f080078

.field public static final about_relative_item_size:I = 0x7f080079

.field public static final about_text_header_size:I = 0x7f08007a

.field public static final about_text_size:I = 0x7f08007b

.field public static final activity_horizontal_margin:I = 0x7f08003e

.field public static final activity_logo_left:I = 0x7f08007e

.field public static final activity_logo_left_large:I = 0x7f08007f

.field public static final activity_logo_left_small:I = 0x7f080080

.field public static final activity_logo_top:I = 0x7f080081

.field public static final activity_logo_top_large:I = 0x7f080082

.field public static final activity_logo_top_small:I = 0x7f080083

.field public static final activity_vertical_margin:I = 0x7f080084

.field public static final application_app_title:I = 0x7f080086

.field public static final application_header_height:I = 0x7f080087

.field public static final application_item_height:I = 0x7f080088

.field public static final application_item_image_height:I = 0x7f080089

.field public static final application_item_info_height:I = 0x7f08008a

.field public static final application_item_info_margin_top:I = 0x7f08008b

.field public static final application_item_wifth:I = 0x7f08008c

.field public static final barcode_informations_height:I = 0x7f080096

.field public static final barcode_informations_margin:I = 0x7f080097

.field public static final birthday_notification_margin:I = 0x7f080098

.field public static final button_login_activity_height:I = 0x7f0800a4

.field public static final button_login_activity_width:I = 0x7f0800a5

.field public static final card_footer_logged_home_height:I = 0x7f0800ac

.field public static final card_header_logged_home_height:I = 0x7f0800ad

.field public static final card_toggle_logged_home_height:I = 0x7f0800ae

.field public static final cardview_compat_inset_shadow:I = 0x7f0800af

.field public static final cardview_default_elevation:I = 0x7f0800b0

.field public static final cardview_default_radius:I = 0x7f0800b1

.field public static final chat_avatar_top_margin:I = 0x7f0800b2

.field public static final chat_between_avatar_and_bubble_margin:I = 0x7f0800b3

.field public static final chat_between_edit_and_button_margin:I = 0x7f0800b4

.field public static final chat_manager_circular_letter_height:I = 0x7f0800b5

.field public static final chat_manager_circular_letter_width:I = 0x7f0800b6

.field public static final chat_message_list_bottom_padding:I = 0x7f0800b7

.field public static final chat_message_list_horizontal_padding:I = 0x7f0800b8

.field public static final chat_message_top_margin:I = 0x7f0800b9

.field public static final chat_send_container_horizontal_padding:I = 0x7f0800ba

.field public static final chat_send_container_vertical_padding:I = 0x7f0800bb

.field public static final chat_send_content_height:I = 0x7f0800bc

.field public static final chat_shortcut_height:I = 0x7f0800bd

.field public static final chat_shortcut_margin:I = 0x7f0800be

.field public static final contact_header_text_title_height:I = 0x7f0800d6

.field public static final contact_stroke_line_height:I = 0x7f0800d7

.field public static final contact_us_height:I = 0x7f0800d8

.field public static final credit_card_password_height:I = 0x7f0800d9

.field public static final credit_cards_pager_indicador_margin_top:I = 0x7f0800da

.field public static final default_border_radius:I = 0x7f0800db

.field public static final default_corner_radius_3_dp:I = 0x7f0800de

.field public static final default_corner_radius_6_dp:I = 0x7f0800df

.field public static final default_display_text_65_sp:I = 0x7f0800e0

.field public static final default_margin_100_dp:I = 0x7f0800e9

.field public static final default_margin_10_dp:I = 0x7f0800ea

.field public static final default_margin_128_dp:I = 0x7f0800eb

.field public static final default_margin_12_dp:I = 0x7f0800ec

.field public static final default_margin_13_dp:I = 0x7f0800ed

.field public static final default_margin_14_dp:I = 0x7f0800ee

.field public static final default_margin_15_dp:I = 0x7f0800ef

.field public static final default_margin_16_dp:I = 0x7f0800f0

.field public static final default_margin_17_dp:I = 0x7f0800f1

.field public static final default_margin_18_dp:I = 0x7f0800f2

.field public static final default_margin_1_dp:I = 0x7f0800f3

.field public static final default_margin_20_dp:I = 0x7f0800f4

.field public static final default_margin_22_dp:I = 0x7f0800f5

.field public static final default_margin_232_dp:I = 0x7f0800f6

.field public static final default_margin_24_dp:I = 0x7f0800f7

.field public static final default_margin_25_dp:I = 0x7f0800f8

.field public static final default_margin_28_dp:I = 0x7f0800f9

.field public static final default_margin_2_dp:I = 0x7f0800fa

.field public static final default_margin_30_dp:I = 0x7f0800fb

.field public static final default_margin_32_dp:I = 0x7f0800fc

.field public static final default_margin_35_dp:I = 0x7f0800fd

.field public static final default_margin_40_dp:I = 0x7f0800fe

.field public static final default_margin_44_dp:I = 0x7f0800ff

.field public static final default_margin_48_dp:I = 0x7f080100

.field public static final default_margin_4_dp:I = 0x7f080101

.field public static final default_margin_50_dp:I = 0x7f080102

.field public static final default_margin_55_dp:I = 0x7f080103

.field public static final default_margin_5_dp:I = 0x7f080104

.field public static final default_margin_60_dp:I = 0x7f080105

.field public static final default_margin_70_dp:I = 0x7f080106

.field public static final default_margin_75_dp:I = 0x7f080107

.field public static final default_margin_8_dp:I = 0x7f080108

.field public static final default_margin_96_dp:I = 0x7f080109

.field public static final default_margin_9_dp:I = 0x7f08010a

.field public static final default_margin_tiny:I = 0x7f08010b

.field public static final default_padding_2_dp:I = 0x7f08010c

.field public static final default_padding_4_dp:I = 0x7f08010d

.field public static final default_text_10_sp:I = 0x7f080110

.field public static final default_text_11_sp:I = 0x7f080111

.field public static final default_text_12_sp:I = 0x7f080112

.field public static final default_text_13_sp:I = 0x7f080113

.field public static final default_text_14_sp:I = 0x7f080114

.field public static final default_text_16_sp:I = 0x7f080115

.field public static final default_text_18_dp:I = 0x7f080116

.field public static final default_text_18_sp:I = 0x7f080117

.field public static final default_text_19_sp:I = 0x7f080118

.field public static final default_text_20_sp:I = 0x7f080119

.field public static final default_text_24_sp:I = 0x7f08011a

.field public static final default_text_25_sp:I = 0x7f08011b

.field public static final default_text_28_sp:I = 0x7f08011c

.field public static final default_text_30_sp:I = 0x7f08011d

.field public static final default_text_32_sp:I = 0x7f08011e

.field public static final default_text_36_sp:I = 0x7f08011f

.field public static final default_text_40_sp:I = 0x7f080120

.field public static final default_text_48_sp:I = 0x7f080121

.field public static final default_text_60_sp:I = 0x7f080122

.field public static final default_text_8_sp:I = 0x7f080123

.field public static final design_appbar_elevation:I = 0x7f08012c

.field public static final design_bottom_navigation_active_item_max_width:I = 0x7f08012d

.field public static final design_bottom_navigation_active_text_size:I = 0x7f08012e

.field public static final design_bottom_navigation_height:I = 0x7f08012f

.field public static final design_bottom_navigation_item_max_width:I = 0x7f080131

.field public static final design_bottom_navigation_margin:I = 0x7f080133

.field public static final design_bottom_navigation_text_size:I = 0x7f080134

.field public static final design_bottom_sheet_modal_elevation:I = 0x7f080135

.field public static final design_bottom_sheet_peek_height_min:I = 0x7f080136

.field public static final design_fab_border_width:I = 0x7f080137

.field public static final design_fab_elevation:I = 0x7f080138

.field public static final design_fab_image_size:I = 0x7f080139

.field public static final design_fab_size_mini:I = 0x7f08013a

.field public static final design_fab_size_normal:I = 0x7f08013b

.field public static final design_fab_translation_z_pressed:I = 0x7f08013c

.field public static final design_navigation_elevation:I = 0x7f08013d

.field public static final design_navigation_icon_padding:I = 0x7f08013e

.field public static final design_navigation_icon_size:I = 0x7f08013f

.field public static final design_navigation_max_width:I = 0x7f08001e

.field public static final design_navigation_padding_bottom:I = 0x7f080140

.field public static final design_navigation_separator_vertical_padding:I = 0x7f080141

.field public static final design_snackbar_action_inline_max_width:I = 0x7f08001f

.field public static final design_snackbar_background_corner_radius:I = 0x7f080020

.field public static final design_snackbar_elevation:I = 0x7f080142

.field public static final design_snackbar_extra_spacing_horizontal:I = 0x7f080021

.field public static final design_snackbar_max_width:I = 0x7f080022

.field public static final design_snackbar_min_width:I = 0x7f080023

.field public static final design_snackbar_padding_horizontal:I = 0x7f080143

.field public static final design_snackbar_padding_vertical:I = 0x7f080144

.field public static final design_snackbar_padding_vertical_2lines:I = 0x7f080024

.field public static final design_snackbar_text_size:I = 0x7f080145

.field public static final design_tab_max_width:I = 0x7f080146

.field public static final design_tab_scrollable_min_width:I = 0x7f080025

.field public static final design_tab_text_size:I = 0x7f080147

.field public static final design_tab_text_size_2line:I = 0x7f080148

.field public static final disabled_alpha_material_dark:I = 0x7f080153

.field public static final disabled_alpha_material_light:I = 0x7f080154

.field public static final error_label_spacing:I = 0x7f080155

.field public static final faq_text_item_size:I = 0x7f080157

.field public static final fastscroller_handle_corner:I = 0x7f080158

.field public static final fastscroller_handle_size:I = 0x7f080159

.field public static final fastscroller_reack_corner:I = 0x7f08015a

.field public static final fastscroller_track_height:I = 0x7f08015b

.field public static final fastscroller_track_padding:I = 0x7f08015c

.field public static final fastscroller_track_width:I = 0x7f08015d

.field public static final floating_label_bottom_spacing:I = 0x7f08015e

.field public static final floating_label_inside_spacing:I = 0x7f08015f

.field public static final floating_label_top_spacing:I = 0x7f080161

.field public static final global_rounded_button_corner:I = 0x7f08016d

.field public static final global_rounded_corner:I = 0x7f08016e

.field public static final highlight_alpha_material_colored:I = 0x7f080171

.field public static final highlight_alpha_material_dark:I = 0x7f080172

.field public static final highlight_alpha_material_light:I = 0x7f080173

.field public static final image_logo_margin_left:I = 0x7f080178

.field public static final image_logo_margin_top:I = 0x7f080179

.field public static final itausdkcore_default_container_padding:I = 0x7f08017e

.field public static final itausdkcore_default_loading_height:I = 0x7f08017f

.field public static final itausdkcore_default_loading_width:I = 0x7f080180

.field public static final item_touch_helper_max_drag_scroll_per_frame:I = 0x7f080185

.field public static final item_touch_helper_swipe_escape_max_velocity:I = 0x7f080186

.field public static final item_touch_helper_swipe_escape_velocity:I = 0x7f080187

.field public static final label_text_size:I = 0x7f080189

.field public static final limit_progressbar_height:I = 0x7f08018b

.field public static final limit_progressbar_padding:I = 0x7f08018c

.field public static final login_logo_margin_top:I = 0x7f080194

.field public static final login_logo_padding_top:I = 0x7f080195

.field public static final login_textinputlayout_padding_left:I = 0x7f08019a

.field public static final login_textinputlayout_padding_right:I = 0x7f08019b

.field public static final menu_item_height:I = 0x7f0801ad

.field public static final my_manager_avatar_height:I = 0x7f0801b8

.field public static final notification_large_icon_height:I = 0x7f0801bc

.field public static final notification_large_icon_width:I = 0x7f0801bd

.field public static final notification_padding_bottom:I = 0x7f0801be

.field public static final notification_padding_right:I = 0x7f0801bf

.field public static final notification_padding_top:I = 0x7f0801c0

.field public static final notification_subtext_size:I = 0x7f0801c4

.field public static final open_value_picker_button_height:I = 0x7f0801c7

.field public static final open_value_picker_button_margin:I = 0x7f0801c8

.field public static final open_value_picker_button_width:I = 0x7f0801c9

.field public static final open_value_picker_coin_margin:I = 0x7f0801ca

.field public static final open_value_picker_height:I = 0x7f0801cb

.field public static final open_value_picker_value_margin:I = 0x7f0801cc

.field public static final profile_exchange_carrier_margin_top:I = 0x7f0801d8

.field public static final profile_exchange_name_margin:I = 0x7f0801d9

.field public static final profile_exchange_padding:I = 0x7f0801da

.field public static final profile_exchange_profile_one_margin:I = 0x7f0801db

.field public static final profile_exchange_profile_two_margin:I = 0x7f0801dc

.field public static final recharge_headers_height:I = 0x7f0801dd

.field public static final recharge_receipt_header_height:I = 0x7f0801de

.field public static final recharge_receipt_padding:I = 0x7f0801df

.field public static final right_left_spinner_padding:I = 0x7f0801e5

.field public static final sales_notification_margin:I = 0x7f0801e6

.field public static final search_header_list_56_dp:I = 0x7f0801e7

.field public static final search_header_list_60_dp:I = 0x7f0801e8

.field public static final search_item_list_56_dp:I = 0x7f0801e9

.field public static final search_item_list_64_dp:I = 0x7f0801ea

.field public static final search_margin_30_dp:I = 0x7f0801eb

.field public static final snackbar_elevation:I = 0x7f0801f5

.field public static final spinner_height:I = 0x7f0801f6

.field public static final spinner_item_height:I = 0x7f0801f7

.field public static final status_bar_height:I = 0x7f0801f8

.field public static final task_notification_margin:I = 0x7f0801f9

.field public static final token_coachmark_arrow_padding:I = 0x7f0801fb

.field public static final token_slider_arrow_half_size:I = 0x7f0801fc

.field public static final token_slider_button_height:I = 0x7f0801fd

.field public static final token_slider_button_width:I = 0x7f0801fe

.field public static final token_slider_height:I = 0x7f0801ff

.field public static final token_slider_itoken_padding_bottom:I = 0x7f080200

.field public static final token_slider_option_height:I = 0x7f080201

.field public static final token_slider_option_negative_height:I = 0x7f080202

.field public static final token_slider_padding_bottom:I = 0x7f080203

.field public static final transfer_balance_item_height:I = 0x7f080209

.field public static final transfer_contact_edit_height:I = 0x7f08020a

.field public static final transfer_header_to_height:I = 0x7f08020b

.field public static final transfer_item_avatar_container:I = 0x7f08020c

.field public static final transfer_item_avatar_size:I = 0x7f08020d

.field public static final transfer_item_header_height:I = 0x7f08020e

.field public static final transfer_item_height:I = 0x7f08020f

.field public static final transfer_my_account_avatar:I = 0x7f080210

.field public static final transfer_receipt_header_height:I = 0x7f080211

.field public static final transfer_receipt_padding:I = 0x7f080212

.field public static final transfer_repeat_for_width:I = 0x7f080213

.field public static final transfer_search_container_height:I = 0x7f080214

.field public static final transfer_search_edit_text_height:I = 0x7f080215

.field public static final transfer_type_transaction_height:I = 0x7f080216

.field public static final ttlm_default_corner_radius:I = 0x7f080217

.field public static final ttlm_default_padding:I = 0x7f080218

.field public static final ttlm_default_stroke_weight:I = 0x7f080219

.field public static final underline_bottom_spacing:I = 0x7f08021a

.field public static final underline_top_spacing:I = 0x7f08021b

.field public static final value_picker_margin_labels:I = 0x7f080001

.field public static final value_picker_margin_text:I = 0x7f08021c

.field public static final value_picker_stroke_2x_width:I = 0x7f08021d

.field public static final value_picker_stroke_width:I = 0x7f08021e

.field public static final value_picker_text_width:I = 0x7f08021f

.field public static final view_item_height:I = 0x7f080222

.field public static final view_slider_workaround_padding_bottom:I = 0x7f080225

.field public static final virtual_keyboard_button_access_margin_left:I = 0x7f080226

.field public static final virtual_keyboard_button_access_margin_right:I = 0x7f080227

.field public static final virtual_keyboard_button_default_height:I = 0x7f080228

.field public static final virtual_keyboard_button_default_width:I = 0x7f080229

.field public static final virtual_keyboard_marginTop:I = 0x7f08022a

.field public static final virtual_keyboard_margin_left:I = 0x7f08022b

.field public static final virtual_keyboard_margin_right:I = 0x7f08022c

.field public static final voucher_action_container_width:I = 0x7f08022d

.field public static final voucher_item_size:I = 0x7f08022e


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 601
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

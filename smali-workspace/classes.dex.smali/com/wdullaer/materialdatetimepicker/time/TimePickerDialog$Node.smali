.class Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog$Node;
.super Ljava/lang/Object;
.source "TimePickerDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Node"
.end annotation


# instance fields
.field private mChildren:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog$Node;>;"
        }
    .end annotation
.end field

.field private mLegalKeys:[I


# direct methods
.method public varargs constructor <init>([I)V
    .registers 3
    .param p1, "legalKeys"    # [I

    .line 1024
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1025
    iput-object p1, p0, Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog$Node;->mLegalKeys:[I

    .line 1026
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog$Node;->mChildren:Ljava/util/ArrayList;

    .line 1027
    return-void
.end method


# virtual methods
.method public addChild(Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog$Node;)V
    .registers 3
    .param p1, "child"    # Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog$Node;

    .line 1030
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog$Node;->mChildren:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1031
    return-void
.end method

.method public canReach(I)Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog$Node;
    .registers 5
    .param p1, "key"    # I

    .line 1043
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog$Node;->mChildren:Ljava/util/ArrayList;

    if-nez v0, :cond_6

    .line 1044
    const/4 v0, 0x0

    return-object v0

    .line 1046
    :cond_6
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog$Node;->mChildren:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog$Node;

    .line 1047
    .local v2, "child":Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog$Node;
    invoke-virtual {v2, p1}, Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog$Node;->containsKey(I)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 1048
    return-object v2

    .line 1050
    .end local v2    # "child":Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog$Node;
    :cond_20
    goto :goto_c

    .line 1051
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_21
    const/4 v0, 0x0

    return-object v0
.end method

.method public containsKey(I)Z
    .registers 4
    .param p1, "key"    # I

    .line 1034
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog$Node;->mLegalKeys:[I

    array-length v0, v0

    if-ge v1, v0, :cond_11

    .line 1035
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/time/TimePickerDialog$Node;->mLegalKeys:[I

    aget v0, v0, v1

    if-ne v0, p1, :cond_e

    .line 1036
    const/4 v0, 0x1

    return v0

    .line 1034
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1039
    .end local v1    # "i":I
    :cond_11
    const/4 v0, 0x0

    return v0
.end method

.class public Lbr/com/itau/sdk/android/core/model/DadosContaDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final agencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia"
    .end annotation
.end field

.field private final conta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta"
    .end annotation
.end field

.field private final dac:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dac"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/DadosContaDTO;->agencia:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lbr/com/itau/sdk/android/core/model/DadosContaDTO;->conta:Ljava/lang/String;

    .line 18
    iput-object p3, p0, Lbr/com/itau/sdk/android/core/model/DadosContaDTO;->dac:Ljava/lang/String;

    .line 19
    return-void
.end method


# virtual methods
.method public getAgencia()Ljava/lang/String;
    .registers 2

    .line 23
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/DadosContaDTO;->agencia:Ljava/lang/String;

    return-object v0
.end method

.method public getConta()Ljava/lang/String;
    .registers 2

    .line 27
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/DadosContaDTO;->conta:Ljava/lang/String;

    return-object v0
.end method

.method public getDac()Ljava/lang/String;
    .registers 2

    .line 31
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/DadosContaDTO;->dac:Ljava/lang/String;

    return-object v0
.end method

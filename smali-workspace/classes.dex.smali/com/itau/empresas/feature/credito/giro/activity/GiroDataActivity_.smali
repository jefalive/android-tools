.class public final Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;
.super Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;
.source "GiroDataActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 39
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;-><init>()V

    .line 43
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;

    .line 39
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;

    .line 39
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 57
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 58
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 59
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 60
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;->simulacaoPropostasController:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController;

    .line 61
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;->injectExtras_()V

    .line 62
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;->afterInject()V

    .line 63
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 129
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 130
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_2e

    .line 131
    const-string v0, "ofertaEspecialAceita"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 132
    const-string v0, "ofertaEspecialAceita"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 134
    :cond_1c
    const-string v0, "simulacaoPropostaSaidaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 135
    const-string v0, "simulacaoPropostaSaidaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 138
    :cond_2e
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 163
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$4;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 171
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 151
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$3;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 159
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 50
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;->init_(Landroid/os/Bundle;)V

    .line 51
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 53
    const v0, 0x7f030039

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;->setContentView(I)V

    .line 54
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 5
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 97
    const v0, 0x7f0e0192

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;->root:Landroid/widget/RelativeLayout;

    .line 98
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 99
    const v0, 0x7f0e0196

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;->campoValorEmprestimo:Landroid/widget/EditText;

    .line 100
    const v0, 0x7f0e019b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;->campoData:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0e019d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;->textoAviso:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f0e0199

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 103
    .local v1, "view_rl_data_container":Landroid/view/View;
    const v0, 0x7f0e018f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 105
    .local v2, "view_botao_continuar":Landroid/view/View;
    if-eqz v1, :cond_4f

    .line 106
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$1;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    :cond_4f
    if-eqz v2, :cond_59

    .line 116
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$2;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    :cond_59
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;->aoCarregarTela()V

    .line 126
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 67
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->setContentView(I)V

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 69
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 79
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->setContentView(Landroid/view/View;)V

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 81
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 73
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 75
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 142
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->setIntent(Landroid/content/Intent;)V

    .line 143
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_;->injectExtras_()V

    .line 144
    return-void
.end method

.class public Lcom/itau/empresas/controller/ExtratoController;
.super Lcom/itau/empresas/controller/BaseController;
.source "ExtratoController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/itau/empresas/controller/BaseController<Lcom/itau/empresas/api/Api;>;"
    }
.end annotation


# instance fields
.field app:Lcom/itau/empresas/CustomApplication;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 16
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method


# virtual methods
.method public consultaLancamentos(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "idconta"    # Ljava/lang/String;
    .param p2, "dataInicial"    # Ljava/lang/String;
    .param p3, "dataFinal"    # Ljava/lang/String;

    .line 45
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/itau/empresas/controller/ExtratoController;->consultaLancamentos(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 46
    return-void
.end method

.method public consultaLancamentos(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    .registers 11
    .param p1, "idconta"    # Ljava/lang/String;
    .param p2, "dataInicial"    # Ljava/lang/String;
    .param p3, "dataFinal"    # Ljava/lang/String;
    .param p4, "reduzido"    # Ljava/lang/Boolean;

    .line 50
    new-instance v0, Lcom/itau/empresas/controller/ExtratoController$3;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/controller/ExtratoController$3;-><init>(Lcom/itau/empresas/controller/ExtratoController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 58
    return-void
.end method

.method public consultaLis()V
    .registers 3

    .line 103
    iget-object v0, p0, Lcom/itau/empresas/controller/ExtratoController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v1

    .line 104
    .local v1, "contaSelecionada":Lcom/itau/empresas/api/model/ContaOperadorVO;
    if-eqz v1, :cond_10

    .line 105
    new-instance v0, Lcom/itau/empresas/controller/ExtratoController$7;

    invoke-direct {v0, p0, v1}, Lcom/itau/empresas/controller/ExtratoController$7;-><init>(Lcom/itau/empresas/controller/ExtratoController;Lcom/itau/empresas/api/model/ContaOperadorVO;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 116
    :cond_10
    return-void
.end method

.method public consultaLis(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "agencia"    # Ljava/lang/String;
    .param p2, "conta"    # Ljava/lang/String;
    .param p3, "dac"    # Ljava/lang/String;

    .line 33
    new-instance v0, Lcom/itau/empresas/controller/ExtratoController$2;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/itau/empresas/controller/ExtratoController$2;-><init>(Lcom/itau/empresas/controller/ExtratoController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 41
    return-void
.end method

.method public consultaMultiLimite()V
    .registers 3

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/controller/ExtratoController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v1

    .line 88
    .local v1, "contaSelecionada":Lcom/itau/empresas/api/model/ContaOperadorVO;
    if-eqz v1, :cond_10

    .line 89
    new-instance v0, Lcom/itau/empresas/controller/ExtratoController$6;

    invoke-direct {v0, p0, v1}, Lcom/itau/empresas/controller/ExtratoController$6;-><init>(Lcom/itau/empresas/controller/ExtratoController;Lcom/itau/empresas/api/model/ContaOperadorVO;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 100
    :cond_10
    return-void
.end method

.method public consultaMultiLimite(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .param p1, "agencia"    # Ljava/lang/String;
    .param p2, "conta"    # Ljava/lang/String;
    .param p3, "dac"    # Ljava/lang/String;
    .param p4, "incluso"    # Ljava/lang/String;

    .line 62
    new-instance v0, Lcom/itau/empresas/controller/ExtratoController$4;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/controller/ExtratoController$4;-><init>(Lcom/itau/empresas/controller/ExtratoController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 70
    return-void
.end method

.method public consultaSaldo()V
    .registers 2

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/controller/ExtratoController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 74
    new-instance v0, Lcom/itau/empresas/controller/ExtratoController$5;

    invoke-direct {v0, p0}, Lcom/itau/empresas/controller/ExtratoController$5;-><init>(Lcom/itau/empresas/controller/ExtratoController;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 83
    :cond_10
    return-void
.end method

.method public consultaSaldo(Ljava/lang/String;)V
    .registers 3
    .param p1, "idconta"    # Ljava/lang/String;

    .line 22
    new-instance v0, Lcom/itau/empresas/controller/ExtratoController$1;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/controller/ExtratoController$1;-><init>(Lcom/itau/empresas/controller/ExtratoController;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 30
    return-void
.end method

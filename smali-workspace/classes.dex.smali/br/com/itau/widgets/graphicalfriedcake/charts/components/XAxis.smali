.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;
.source "XAxis.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;
    }
.end annotation


# instance fields
.field private mAvoidFirstLastClipping:Z

.field public mAxisLabelModulus:I

.field private mIsAxisModulusCustom:Z

.field public mLabelHeight:I

.field public mLabelWidth:I

.field private mPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

.field private mSpaceBetweenLabels:I

.field protected mValues:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field public mYAxisLabelModulus:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 83
    invoke-direct {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;-><init>()V

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mValues:Ljava/util/List;

    .line 25
    const/4 v0, 0x1

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mLabelWidth:I

    .line 32
    const/4 v0, 0x1

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mLabelHeight:I

    .line 39
    const/4 v0, 0x4

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mSpaceBetweenLabels:I

    .line 47
    const/4 v0, 0x1

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mAxisLabelModulus:I

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mIsAxisModulusCustom:Z

    .line 62
    const/4 v0, 0x1

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mYAxisLabelModulus:I

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mAvoidFirstLastClipping:Z

    .line 73
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->TOP:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    .line 84
    return-void
.end method


# virtual methods
.method public getLongestLabel()Ljava/lang/String;
    .registers 6

    .line 215
    const-string v2, ""

    .line 218
    .local v2, "longest":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_3
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mValues:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_22

    .line 219
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mValues:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/String;

    .line 222
    .local v4, "text":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_1f

    .line 223
    move-object v2, v4

    .line 218
    .end local v4    # "text":Ljava/lang/String;
    :cond_1f
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 227
    .end local v3    # "i":I
    :cond_22
    return-object v2
.end method

.method public getPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;
    .registers 2

    .line 91
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    return-object v0
.end method

.method public getSpaceBetweenLabels()I
    .registers 2

    .line 166
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mSpaceBetweenLabels:I

    return v0
.end method

.method public getValues()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 207
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mValues:Ljava/util/List;

    return-object v0
.end method

.method public isAvoidFirstLastClippingEnabled()Z
    .registers 2

    .line 187
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mAvoidFirstLastClipping:Z

    return v0
.end method

.method public isAxisModulusCustom()Z
    .registers 2

    .line 155
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mIsAxisModulusCustom:Z

    return v0
.end method

.method public resetLabelsToSkip()V
    .registers 2

    .line 144
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mIsAxisModulusCustom:Z

    .line 145
    return-void
.end method

.method public setAvoidFirstLastClipping(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 177
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mAvoidFirstLastClipping:Z

    .line 178
    return-void
.end method

.method public setLabelsToSkip(I)V
    .registers 3
    .param p1, "count"    # I

    .line 129
    if-gez p1, :cond_3

    .line 130
    const/4 p1, 0x0

    .line 133
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mIsAxisModulusCustom:Z

    .line 134
    add-int/lit8 v0, p1, 0x1

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mAxisLabelModulus:I

    .line 135
    return-void
.end method

.method public setPosition(Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;)V
    .registers 2
    .param p1, "pos"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    .line 101
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    .line 102
    return-void
.end method

.method public setSpaceBetweenLabels(I)V
    .registers 2
    .param p1, "spaceCharacters"    # I

    .line 113
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mSpaceBetweenLabels:I

    .line 114
    return-void
.end method

.method public setValues(Ljava/util/List;)V
    .registers 2
    .param p1, "values"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/String;>;)V"
        }
    .end annotation

    .line 197
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mValues:Ljava/util/List;

    .line 198
    return-void
.end method

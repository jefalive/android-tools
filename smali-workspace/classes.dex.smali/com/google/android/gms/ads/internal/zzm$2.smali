.class final Lcom/google/android/gms/ads/internal/zzm$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/zzjq$zza;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/ads/internal/zzm;->zza(Lcom/google/android/gms/internal/zzjp;Lcom/google/android/gms/ads/internal/formats/zze;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic zzqr:Ljava/lang/String;

.field final synthetic zzqs:Lcom/google/android/gms/internal/zzjp;

.field final synthetic zzqt:Lcom/google/android/gms/ads/internal/formats/zze;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/internal/formats/zze;Ljava/lang/String;Lcom/google/android/gms/internal/zzjp;)V
    .registers 4

    iput-object p1, p0, Lcom/google/android/gms/ads/internal/zzm$2;->zzqt:Lcom/google/android/gms/ads/internal/formats/zze;

    iput-object p2, p0, Lcom/google/android/gms/ads/internal/zzm$2;->zzqr:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/ads/internal/zzm$2;->zzqs:Lcom/google/android/gms/internal/zzjp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public zza(Lcom/google/android/gms/internal/zzjp;Z)V
    .registers 12

    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v0, "headline"

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/zzm$2;->zzqt:Lcom/google/android/gms/ads/internal/formats/zze;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/formats/zze;->getHeadline()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "body"

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/zzm$2;->zzqt:Lcom/google/android/gms/ads/internal/formats/zze;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/formats/zze;->getBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "call_to_action"

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/zzm$2;->zzqt:Lcom/google/android/gms/ads/internal/formats/zze;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/formats/zze;->getCallToAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "advertiser"

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/zzm$2;->zzqt:Lcom/google/android/gms/ads/internal/formats/zze;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/formats/zze;->getAdvertiser()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "logo"

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/zzm$2;->zzqt:Lcom/google/android/gms/ads/internal/formats/zze;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/formats/zze;->zzdO()Lcom/google/android/gms/internal/zzch;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/ads/internal/zzm;->zza(Lcom/google/android/gms/internal/zzch;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4}, Lorg/json/JSONArray;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/zzm$2;->zzqt:Lcom/google/android/gms/ads/internal/formats/zze;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/formats/zze;->getImages()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_67

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_51
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_67

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/gms/ads/internal/zzm;->zzd(Ljava/lang/Object;)Lcom/google/android/gms/internal/zzch;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/gms/ads/internal/zzm;->zza(Lcom/google/android/gms/internal/zzch;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_51

    :cond_67
    const-string v0, "images"

    invoke-virtual {v3, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "extras"

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/zzm$2;->zzqt:Lcom/google/android/gms/ads/internal/formats/zze;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/formats/zze;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/zzm$2;->zzqr:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/ads/internal/zzm;->zzb(Landroid/os/Bundle;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    const-string v0, "assets"

    invoke-virtual {v6, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "template_id"

    const-string v1, "1"

    invoke-virtual {v6, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/zzm$2;->zzqs:Lcom/google/android/gms/internal/zzjp;

    const-string v1, "google.afma.nativeExpressAds.loadAssets"

    invoke-interface {v0, v1, v6}, Lcom/google/android/gms/internal/zzjp;->zza(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_95
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_95} :catch_96

    goto :goto_9c

    :catch_96
    move-exception v3

    const-string v0, "Exception occurred when loading assets"

    invoke-static {v0, v3}, Lcom/google/android/gms/internal/zzin;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_9c
    return-void
.end method

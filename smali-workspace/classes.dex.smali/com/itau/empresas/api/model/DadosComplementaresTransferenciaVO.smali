.class public Lcom/itau/empresas/api/model/DadosComplementaresTransferenciaVO;
.super Ljava/lang/Object;
.source "DadosComplementaresTransferenciaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private agenciaDestino:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia_destino"
    .end annotation
.end field

.field private agenciaOrigem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia_origem"
    .end annotation
.end field

.field private codigoBancoDestino:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_banco_destino"
    .end annotation
.end field

.field private complementoContaDestino:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "complemento_conta_destino"
    .end annotation
.end field

.field private complementoContaOrigem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "complemento_conta_origem"
    .end annotation
.end field

.field private contaDestino:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta_destino"
    .end annotation
.end field

.field private contaOrigem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta_origem"
    .end annotation
.end field

.field private cpfCnpjDestino:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf_cnpj_destino"
    .end annotation
.end field

.field private cpfCnpjOrigem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf_cnpj_origem"
    .end annotation
.end field

.field private descricaoComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_comprovante"
    .end annotation
.end field

.field private descricaoExtratoDestino:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_extrato_destino"
    .end annotation
.end field

.field private digitoVerificadorContaDestino:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_verificador_conta_destino"
    .end annotation
.end field

.field private digitoVerificadorContaOrigem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_verificador_conta_origem"
    .end annotation
.end field

.field private finalidade:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "finalidade"
    .end annotation
.end field

.field private nomeBancoDestino:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_banco_destino"
    .end annotation
.end field

.field private nomeFavorecido:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_favorecido"
    .end annotation
.end field

.field private tipoTransferencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_transferencia"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public interface abstract Lbr/com/concretesolutions/canarinho/validator/Validador;
.super Ljava/lang/Object;
.source "Validador.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;
    }
.end annotation


# static fields
.field public static final BOLETO:Lbr/com/concretesolutions/canarinho/validator/Validador;

.field public static final CEP:Lbr/com/concretesolutions/canarinho/validator/Validador;

.field public static final CNPJ:Lbr/com/concretesolutions/canarinho/validator/Validador;

.field public static final CPF:Lbr/com/concretesolutions/canarinho/validator/Validador;

.field public static final TELFONE:Lbr/com/concretesolutions/canarinho/validator/Validador;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 19
    invoke-static {}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF;->getInstance()Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/validator/Validador;->CPF:Lbr/com/concretesolutions/canarinho/validator/Validador;

    .line 24
    invoke-static {}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCNPJ;->getInstance()Lbr/com/concretesolutions/canarinho/validator/ValidadorCNPJ;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/validator/Validador;->CNPJ:Lbr/com/concretesolutions/canarinho/validator/Validador;

    .line 29
    invoke-static {}, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->getInstance()Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/validator/Validador;->BOLETO:Lbr/com/concretesolutions/canarinho/validator/Validador;

    .line 34
    invoke-static {}, Lbr/com/concretesolutions/canarinho/validator/ValidadorTelefone;->getInstance()Lbr/com/concretesolutions/canarinho/validator/ValidadorTelefone;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/validator/Validador;->TELFONE:Lbr/com/concretesolutions/canarinho/validator/Validador;

    .line 39
    invoke-static {}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCEP;->getInstance()Lbr/com/concretesolutions/canarinho/validator/ValidadorCEP;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/validator/Validador;->CEP:Lbr/com/concretesolutions/canarinho/validator/Validador;

    return-void
.end method


# virtual methods
.method public abstract ehValido(Landroid/text/Editable;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;
.end method

.method public abstract ehValido(Ljava/lang/String;)Z
.end method

.class public Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;
.super Ljava/lang/Object;
.source "ComprovantesVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private canalOrigem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "canal_origem"
    .end annotation
.end field

.field private dataComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_comprovante"
    .end annotation
.end field

.field private idComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id_comprovante"
    .end annotation
.end field

.field private indicadorComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_comprovante"
    .end annotation
.end field

.field private indicadorEmail:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_email"
    .end annotation
.end field

.field private literalIdentificacaoComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "literal_identificacao_comprovante"
    .end annotation
.end field

.field private nomeOperacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_operacao"
    .end annotation
.end field

.field private valor:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDataComprovante()Ljava/lang/String;
    .registers 2

    .line 39
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->dataComprovante:Ljava/lang/String;

    return-object v0
.end method

.method public getIdComprovante()Ljava/lang/String;
    .registers 2

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->idComprovante:Ljava/lang/String;

    return-object v0
.end method

.method public getIndicadorEmail()Ljava/lang/String;
    .registers 2

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->indicadorEmail:Ljava/lang/String;

    return-object v0
.end method

.method public getLiteralIdentificacaoComprovante()Ljava/lang/String;
    .registers 2

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->literalIdentificacaoComprovante:Ljava/lang/String;

    return-object v0
.end method

.method public getNomeOperacao()Ljava/lang/String;
    .registers 2

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->nomeOperacao:Ljava/lang/String;

    return-object v0
.end method

.method public getValor()Ljava/lang/String;
    .registers 2

    .line 59
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->valor:Ljava/lang/String;

    return-object v0
.end method

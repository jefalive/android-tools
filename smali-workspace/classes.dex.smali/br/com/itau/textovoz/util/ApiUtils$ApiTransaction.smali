.class public final enum Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;
.super Ljava/lang/Enum;
.source "ApiUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/util/ApiUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ApiTransaction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;

.field public static final enum TRANSACTION:Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;


# instance fields
.field private item:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 41
    new-instance v0, Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;

    const-string v1, "TRANSACTION"

    const-string v2, "pagamento,transfer\u00eancia"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;->TRANSACTION:Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;

    .line 40
    const/4 v0, 0x1

    new-array v0, v0, [Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;

    sget-object v1, Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;->TRANSACTION:Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;->$VALUES:[Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .registers 4
    .param p3, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    iput-object p3, p0, Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;->item:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 40
    const-class v0, Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;
    .registers 1

    .line 40
    sget-object v0, Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;->$VALUES:[Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;

    invoke-virtual {v0}, [Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;

    return-object v0
.end method


# virtual methods
.method public getItem()Ljava/lang/String;
    .registers 2

    .line 49
    iget-object v0, p0, Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;->item:Ljava/lang/String;

    return-object v0
.end method

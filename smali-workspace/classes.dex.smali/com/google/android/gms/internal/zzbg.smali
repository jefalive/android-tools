.class public abstract Lcom/google/android/gms/internal/zzbg;
.super Ljava/lang/Object;


# static fields
.field private static zzto:Ljava/security/MessageDigest;


# instance fields
.field protected zzpV:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/internal/zzbg;->zzto:Ljava/security/MessageDigest;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzbg;->zzpV:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method protected zzcL()Ljava/security/MessageDigest;
    .registers 6

    iget-object v1, p0, Lcom/google/android/gms/internal/zzbg;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/google/android/gms/internal/zzbg;->zzto:Ljava/security/MessageDigest;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/google/android/gms/internal/zzbg;->zzto:Ljava/security/MessageDigest;
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_20

    monitor-exit v1

    return-object v0

    :cond_b
    const/4 v2, 0x0

    :goto_c
    const/4 v0, 0x2

    if-ge v2, v0, :cond_1c

    const-string v0, "MD5"

    :try_start_11
    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzbg;->zzto:Ljava/security/MessageDigest;
    :try_end_17
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_11 .. :try_end_17} :catch_18
    .catchall {:try_start_11 .. :try_end_17} :catchall_20

    goto :goto_19

    :catch_18
    move-exception v3

    :goto_19
    add-int/lit8 v2, v2, 0x1

    goto :goto_c

    :cond_1c
    :try_start_1c
    sget-object v0, Lcom/google/android/gms/internal/zzbg;->zzto:Ljava/security/MessageDigest;
    :try_end_1e
    .catchall {:try_start_1c .. :try_end_1e} :catchall_20

    monitor-exit v1

    return-object v0

    :catchall_20
    move-exception v4

    monitor-exit v1

    throw v4
.end method

.method abstract zzu(Ljava/lang/String;)[B
.end method

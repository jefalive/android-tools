.class final Lorg/threeten/bp/OffsetDateTime$2;
.super Ljava/lang/Object;
.source "OffsetDateTime.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/threeten/bp/OffsetDateTime;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Ljava/util/Comparator<Lorg/threeten/bp/OffsetDateTime;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 5
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .line 136
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/OffsetDateTime;

    move-object v1, p2

    check-cast v1, Lorg/threeten/bp/OffsetDateTime;

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/OffsetDateTime$2;->compare(Lorg/threeten/bp/OffsetDateTime;Lorg/threeten/bp/OffsetDateTime;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/threeten/bp/OffsetDateTime;Lorg/threeten/bp/OffsetDateTime;)I
    .registers 8
    .param p1, "datetime1"    # Lorg/threeten/bp/OffsetDateTime;
    .param p2, "datetime2"    # Lorg/threeten/bp/OffsetDateTime;

    .line 139
    invoke-virtual {p1}, Lorg/threeten/bp/OffsetDateTime;->toEpochSecond()J

    move-result-wide v0

    invoke-virtual {p2}, Lorg/threeten/bp/OffsetDateTime;->toEpochSecond()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lorg/threeten/bp/jdk8/Jdk8Methods;->compareLongs(JJ)I

    move-result v4

    .line 140
    .local v4, "cmp":I
    if-nez v4, :cond_1c

    .line 141
    invoke-virtual {p1}, Lorg/threeten/bp/OffsetDateTime;->getNano()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p2}, Lorg/threeten/bp/OffsetDateTime;->getNano()I

    move-result v2

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Lorg/threeten/bp/jdk8/Jdk8Methods;->compareLongs(JJ)I

    move-result v4

    .line 143
    :cond_1c
    return v4
.end method

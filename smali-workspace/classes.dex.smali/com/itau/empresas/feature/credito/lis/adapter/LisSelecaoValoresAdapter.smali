.class public final Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "LisSelecaoValoresAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$LisSelecaoValoresOutrosViewHolder;,
        Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$LisSelecaoValoresViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter<Landroid/support/v7/widget/RecyclerView$ViewHolder;>;"
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private final limiteOfertados:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/credito/lis/model/LisLimiteOfertadoVO;>;"
        }
    .end annotation
.end field

.field private final ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

.field private final possuiLisAdicional:Z


# direct methods
.method public constructor <init>(Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;ZLandroid/content/Context;)V
    .registers 5
    .param p1, "ofertaContratacaoLisVo"    # Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;
    .param p2, "possuiLisAdicional"    # Z
    .param p3, "context"    # Landroid/content/Context;

    .line 38
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    .line 40
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;->getLimitesOfertados()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->limiteOfertados:Ljava/util/List;

    .line 41
    iput-object p3, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->context:Landroid/content/Context;

    .line 42
    iput-boolean p2, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->possuiLisAdicional:Z

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;)Z
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;

    .line 24
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->possuiLisAdicional:Z

    return v0
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;)Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;

    .line 24
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    return-object v0
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;)Landroid/content/Context;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;

    .line 24
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->context:Landroid/content/Context;

    return-object v0
.end method

.method private static bind(Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$LisSelecaoValoresOutrosViewHolder;)V
    .registers 3
    .param p0, "holder"    # Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$LisSelecaoValoresOutrosViewHolder;

    .line 123
    # getter for: Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$LisSelecaoValoresOutrosViewHolder;->textoOutrosValores:Landroid/widget/TextView;
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$LisSelecaoValoresOutrosViewHolder;->access$600(Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$LisSelecaoValoresOutrosViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f07041f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 124
    return-void
.end method

.method private bind(Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$LisSelecaoValoresViewHolder;I)V
    .registers 7
    .param p1, "holder"    # Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$LisSelecaoValoresViewHolder;
    .param p2, "position"    # I

    .line 100
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->limiteOfertados:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/feature/credito/lis/model/LisLimiteOfertadoVO;

    .line 102
    .line 103
    .local v2, "limiteOfertado":Lcom/itau/empresas/feature/credito/lis/model/LisLimiteOfertadoVO;
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/lis/model/LisLimiteOfertadoVO;->getLimiteOfertado()F

    move-result v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v3

    .line 105
    .local v3, "valorFormtado":Ljava/lang/String;
    # getter for: Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$LisSelecaoValoresViewHolder;->textoValor:Landroid/widget/TextView;
    invoke-static {p1}, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$LisSelecaoValoresViewHolder;->access$500(Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$LisSelecaoValoresViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$2;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$2;-><init>(Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;Lcom/itau/empresas/feature/credito/lis/model/LisLimiteOfertadoVO;)V

    invoke-virtual {p1, v0}, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$LisSelecaoValoresViewHolder;->setListener(Landroid/view/View$OnClickListener;)V

    .line 120
    return-void
.end method


# virtual methods
.method public getItemCount()I
    .registers 3

    .line 128
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->limiteOfertados:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getItemViewType(I)I
    .registers 3
    .param p1, "position"    # I

    .line 76
    const/4 v0, 0x3

    if-eq v0, p1, :cond_8

    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$Adapter;->getItemViewType(I)I

    move-result v0

    goto :goto_9

    :cond_8
    const/4 v0, 0x3

    :goto_9
    return v0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .registers 6
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .line 83
    const/4 v0, 0x3

    if-ne p2, v0, :cond_a

    .line 84
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$LisSelecaoValoresOutrosViewHolder;

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->bind(Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$LisSelecaoValoresOutrosViewHolder;)V

    goto :goto_10

    .line 86
    :cond_a
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$LisSelecaoValoresViewHolder;

    invoke-direct {p0, v0, p2}, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->bind(Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$LisSelecaoValoresViewHolder;I)V

    .line 89
    :goto_10
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    const/16 v1, 0x3e8

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/itau/empresas/ui/util/AnimationUtils;->animateAlpha(Landroid/view/View;IZ)V

    .line 91
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .registers 7
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .line 48
    const/4 v0, 0x3

    if-ne v0, p2, :cond_22

    .line 50
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 51
    const v1, 0x7f030106

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 53
    .local v3, "view":Landroid/view/View;
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$1;-><init>(Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$LisSelecaoValoresOutrosViewHolder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v3, v1}, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$LisSelecaoValoresOutrosViewHolder;-><init>(Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;Landroid/view/View;Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$1;)V

    return-object v0

    .line 67
    .end local v3    # "view":Landroid/view/View;
    :cond_22
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 68
    const v1, 0x7f030107

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 70
    .local v3, "view":Landroid/view/View;
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$LisSelecaoValoresViewHolder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v3, v1}, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$LisSelecaoValoresViewHolder;-><init>(Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;Landroid/view/View;Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$1;)V

    return-object v0
.end method

.method public onViewDetachedFromWindow(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .registers 3
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .line 95
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$Adapter;->onViewDetachedFromWindow(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 96
    iget-object v0, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 97
    return-void
.end method

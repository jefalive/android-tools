.class public Lcom/google/android/gms/ads/internal/overlay/zzt;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final zzEY:Ljava/lang/String;

.field private final zzEZ:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

.field private final zzFa:Lcom/google/android/gms/internal/zzbz;

.field private final zzFb:Lcom/google/android/gms/internal/zzcb;

.field private final zzFc:Lcom/google/android/gms/internal/zziv;

.field private final zzFd:[J

.field private final zzFe:[Ljava/lang/String;

.field private zzFf:Lcom/google/android/gms/internal/zzbz;

.field private zzFg:Lcom/google/android/gms/internal/zzbz;

.field private zzFh:Lcom/google/android/gms/internal/zzbz;

.field private zzFi:Lcom/google/android/gms/internal/zzbz;

.field private zzFj:Z

.field private zzFk:Lcom/google/android/gms/ads/internal/overlay/zzi;

.field private zzFl:Z

.field private zzFm:Z

.field private zzFn:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Ljava/lang/String;Lcom/google/android/gms/internal/zzcb;Lcom/google/android/gms/internal/zzbz;)V
    .registers 16

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/zziv$zzb;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zziv$zzb;-><init>()V

    const-string v1, "min_1"

    const-wide/16 v2, 0x1

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/internal/zziv$zzb;->zza(Ljava/lang/String;DD)Lcom/google/android/gms/internal/zziv$zzb;

    move-result-object v0

    const-string v1, "1_5"

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v4, 0x4014000000000000L    # 5.0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/internal/zziv$zzb;->zza(Ljava/lang/String;DD)Lcom/google/android/gms/internal/zziv$zzb;

    move-result-object v0

    const-string v1, "5_10"

    const-wide/high16 v2, 0x4014000000000000L    # 5.0

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/internal/zziv$zzb;->zza(Ljava/lang/String;DD)Lcom/google/android/gms/internal/zziv$zzb;

    move-result-object v0

    const-string v1, "10_20"

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    const-wide/high16 v4, 0x4034000000000000L    # 20.0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/internal/zziv$zzb;->zza(Ljava/lang/String;DD)Lcom/google/android/gms/internal/zziv$zzb;

    move-result-object v0

    const-string v1, "20_30"

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    const-wide/high16 v4, 0x403e000000000000L    # 30.0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/internal/zziv$zzb;->zza(Ljava/lang/String;DD)Lcom/google/android/gms/internal/zziv$zzb;

    move-result-object v0

    const-string v1, "30_max"

    const-wide/high16 v2, 0x403e000000000000L    # 30.0

    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/internal/zziv$zzb;->zza(Ljava/lang/String;DD)Lcom/google/android/gms/internal/zziv$zzb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zziv$zzb;->zzhA()Lcom/google/android/gms/internal/zziv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFc:Lcom/google/android/gms/internal/zziv;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFn:J

    iput-object p1, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzEZ:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iput-object p3, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzEY:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFb:Lcom/google/android/gms/internal/zzcb;

    iput-object p5, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFa:Lcom/google/android/gms/internal/zzbz;

    sget-object v0, Lcom/google/android/gms/internal/zzbt;->zzvV:Lcom/google/android/gms/internal/zzbp;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzbp;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/String;

    if-nez v6, :cond_71

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFe:[Ljava/lang/String;

    const/4 v0, 0x0

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFd:[J

    return-void

    :cond_71
    const-string v0, ","

    invoke-static {v6, v0}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    array-length v0, v7

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFe:[Ljava/lang/String;

    array-length v0, v7

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFd:[J

    const/4 v8, 0x0

    :goto_82
    array-length v0, v7

    if-ge v8, v0, :cond_9f

    :try_start_85
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFd:[J

    aget-object v1, v7, v8

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    aput-wide v1, v0, v8
    :try_end_8f
    .catch Ljava/lang/NumberFormatException; {:try_start_85 .. :try_end_8f} :catch_90

    goto :goto_9c

    :catch_90
    move-exception v9

    const-string v0, "Unable to parse frame hash target time number."

    invoke-static {v0, v9}, Lcom/google/android/gms/internal/zzin;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFd:[J

    const-wide/16 v1, -0x1

    aput-wide v1, v0, v8

    :goto_9c
    add-int/lit8 v8, v8, 0x1

    goto :goto_82

    :cond_9f
    return-void
.end method

.method private zzc(Lcom/google/android/gms/ads/internal/overlay/zzi;)V
    .registers 11

    sget-object v0, Lcom/google/android/gms/internal/zzbt;->zzvW:Lcom/google/android/gms/internal/zzbp;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzbp;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/android/gms/ads/internal/overlay/zzi;->getCurrentPosition()I

    move-result v0

    int-to-long v4, v0

    const/4 v6, 0x0

    :goto_12
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFe:[Ljava/lang/String;

    array-length v0, v0

    if-ge v6, v0, :cond_38

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFe:[Ljava/lang/String;

    aget-object v0, v0, v6

    if-eqz v0, :cond_1e

    goto :goto_35

    :cond_1e
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFd:[J

    aget-wide v7, v0, v6

    sub-long v0, v4, v7

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-lez v0, :cond_35

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFe:[Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/ads/internal/overlay/zzt;->zza(Landroid/view/TextureView;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    goto :goto_38

    :cond_35
    :goto_35
    add-int/lit8 v6, v6, 0x1

    goto :goto_12

    :cond_38
    :goto_38
    return-void
.end method

.method private zzfN()V
    .registers 10

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFh:Lcom/google/android/gms/internal/zzbz;

    if-eqz v0, :cond_2e

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFi:Lcom/google/android/gms/internal/zzbz;

    if-nez v0, :cond_2e

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFb:Lcom/google/android/gms/internal/zzcb;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFh:Lcom/google/android/gms/internal/zzbz;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "vff"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/zzbx;->zza(Lcom/google/android/gms/internal/zzcb;Lcom/google/android/gms/internal/zzbz;[Ljava/lang/String;)Z

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFb:Lcom/google/android/gms/internal/zzcb;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFa:Lcom/google/android/gms/internal/zzbz;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "vtt"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/zzbx;->zza(Lcom/google/android/gms/internal/zzcb;Lcom/google/android/gms/internal/zzbz;[Ljava/lang/String;)Z

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFb:Lcom/google/android/gms/internal/zzcb;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzbx;->zzb(Lcom/google/android/gms/internal/zzcb;)Lcom/google/android/gms/internal/zzbz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFi:Lcom/google/android/gms/internal/zzbz;

    :cond_2e
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbG()Lcom/google/android/gms/internal/zzmq;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzmq;->nanoTime()J

    move-result-wide v5

    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFj:Z

    if-eqz v0, :cond_5b

    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFm:Z

    if-eqz v0, :cond_5b

    iget-wide v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFn:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5b

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    long-to-double v0, v0

    iget-wide v2, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFn:J

    sub-long v2, v5, v2

    long-to-double v2, v2

    div-double v7, v0, v2

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFc:Lcom/google/android/gms/internal/zziv;

    invoke-virtual {v0, v7, v8}, Lcom/google/android/gms/internal/zziv;->zza(D)V

    :cond_5b
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFj:Z

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFm:Z

    iput-wide v5, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFn:J

    return-void
.end method


# virtual methods
.method public onStop()V
    .registers 10

    sget-object v0, Lcom/google/android/gms/internal/zzbt;->zzvU:Lcom/google/android/gms/internal/zzbp;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzbp;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_c6

    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFl:Z

    if-nez v0, :cond_c6

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    const-string v0, "type"

    const-string v1, "native-player-metrics"

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "request"

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzEY:Ljava/lang/String;

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "player"

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFk:Lcom/google/android/gms/ads/internal/overlay/zzi;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/overlay/zzi;->zzeZ()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFc:Lcom/google/android/gms/internal/zziv;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zziv;->getBuckets()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3a
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_84

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/google/android/gms/internal/zziv$zza;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fps_c_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v8, Lcom/google/android/gms/internal/zziv$zza;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, v8, Lcom/google/android/gms/internal/zziv$zza;->count:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fps_p_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v8, Lcom/google/android/gms/internal/zziv$zza;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-wide v1, v8, Lcom/google/android/gms/internal/zziv$zza;->zzMu:D

    invoke-static {v1, v2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3a

    :cond_84
    const/4 v7, 0x0

    :goto_85
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFd:[J

    array-length v0, v0

    if-ge v7, v0, :cond_b2

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFe:[Ljava/lang/String;

    aget-object v8, v0, v7

    if-nez v8, :cond_91

    goto :goto_af

    :cond_91
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "fh_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFd:[J

    aget-wide v1, v1, v7

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :goto_af
    add-int/lit8 v7, v7, 0x1

    goto :goto_85

    :cond_b2
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbC()Lcom/google/android/gms/internal/zzir;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzEZ:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->afmaVersion:Ljava/lang/String;

    const-string v3, "gmob-apps"

    move-object v4, v6

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/internal/zzir;->zza(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFl:Z

    :cond_c6
    return-void
.end method

.method zza(Landroid/view/TextureView;)Ljava/lang/String;
    .registers 17

    move-object/from16 v0, p1

    const/16 v1, 0x8

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/view/TextureView;->getBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v4

    const-wide/16 v5, 0x0

    const-wide/16 v7, 0x3f

    const/4 v9, 0x0

    :goto_f
    const/16 v0, 0x8

    if-ge v9, v0, :cond_41

    const/4 v10, 0x0

    :goto_14
    const/16 v0, 0x8

    if-ge v10, v0, :cond_3e

    invoke-virtual {v4, v10, v9}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v11

    invoke-static {v11}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    invoke-static {v11}, Landroid/graphics/Color;->red(I)I

    move-result v1

    add-int/2addr v0, v1

    invoke-static {v11}, Landroid/graphics/Color;->green(I)I

    move-result v1

    add-int v12, v0, v1

    const/16 v0, 0x80

    if-le v12, v0, :cond_32

    const-wide/16 v13, 0x1

    goto :goto_34

    :cond_32
    const-wide/16 v13, 0x0

    :goto_34
    long-to-int v0, v7

    shl-long v0, v13, v0

    or-long/2addr v5, v0

    add-int/lit8 v10, v10, 0x1

    const-wide/16 v0, 0x1

    sub-long/2addr v7, v0

    goto :goto_14

    :cond_3e
    add-int/lit8 v9, v9, 0x1

    goto :goto_f

    :cond_41
    const-string v0, "%016X"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public zza(Lcom/google/android/gms/ads/internal/overlay/zzi;)V
    .registers 7

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFb:Lcom/google/android/gms/internal/zzcb;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFa:Lcom/google/android/gms/internal/zzbz;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "vpc"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/zzbx;->zza(Lcom/google/android/gms/internal/zzcb;Lcom/google/android/gms/internal/zzbz;[Ljava/lang/String;)Z

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFb:Lcom/google/android/gms/internal/zzcb;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzbx;->zzb(Lcom/google/android/gms/internal/zzcb;)Lcom/google/android/gms/internal/zzbz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFf:Lcom/google/android/gms/internal/zzbz;

    iput-object p1, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFk:Lcom/google/android/gms/ads/internal/overlay/zzi;

    return-void
.end method

.method public zzb(Lcom/google/android/gms/ads/internal/overlay/zzi;)V
    .registers 2

    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzfN()V

    invoke-direct {p0, p1}, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzc(Lcom/google/android/gms/ads/internal/overlay/zzi;)V

    return-void
.end method

.method public zzfO()V
    .registers 6

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFj:Z

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFg:Lcom/google/android/gms/internal/zzbz;

    if-eqz v0, :cond_22

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFh:Lcom/google/android/gms/internal/zzbz;

    if-nez v0, :cond_22

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFb:Lcom/google/android/gms/internal/zzcb;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFg:Lcom/google/android/gms/internal/zzbz;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "vfp"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/zzbx;->zza(Lcom/google/android/gms/internal/zzcb;Lcom/google/android/gms/internal/zzbz;[Ljava/lang/String;)Z

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFb:Lcom/google/android/gms/internal/zzcb;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzbx;->zzb(Lcom/google/android/gms/internal/zzcb;)Lcom/google/android/gms/internal/zzbz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFh:Lcom/google/android/gms/internal/zzbz;

    :cond_22
    return-void
.end method

.method public zzfP()V
    .registers 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFj:Z

    return-void
.end method

.method public zzfz()V
    .registers 6

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFf:Lcom/google/android/gms/internal/zzbz;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFg:Lcom/google/android/gms/internal/zzbz;

    if-eqz v0, :cond_9

    :cond_8
    return-void

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFb:Lcom/google/android/gms/internal/zzcb;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFf:Lcom/google/android/gms/internal/zzbz;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "vfr"

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/zzbx;->zza(Lcom/google/android/gms/internal/zzcb;Lcom/google/android/gms/internal/zzbz;[Ljava/lang/String;)Z

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFb:Lcom/google/android/gms/internal/zzcb;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzbx;->zzb(Lcom/google/android/gms/internal/zzcb;)Lcom/google/android/gms/internal/zzbz;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/zzt;->zzFg:Lcom/google/android/gms/internal/zzbz;

    return-void
.end method

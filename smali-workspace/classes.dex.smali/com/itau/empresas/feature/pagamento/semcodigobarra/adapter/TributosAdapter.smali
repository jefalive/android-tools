.class public Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/TributosAdapter;
.super Landroid/widget/BaseAdapter;
.source "TributosAdapter.java"


# instance fields
.field context:Landroid/content/Context;

.field private itemTributos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/ItemTributos;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 20
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 2

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/TributosAdapter;->itemTributos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/ItemTributos;
    .registers 3
    .param p1, "i"    # I

    .line 58
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/TributosAdapter;->itemTributos:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/ItemTributos;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .registers 3

    .line 19
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/TributosAdapter;->getItem(I)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/ItemTributos;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .param p1, "i"    # I

    .line 33
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 10
    .param p1, "i"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .line 43
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/TributosAdapter;->context:Landroid/content/Context;

    const-string v1, "layout_inflater"

    .line 44
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/LayoutInflater;

    .line 45
    .local v2, "inflater":Landroid/view/LayoutInflater;
    if-nez p2, :cond_16

    const v0, 0x7f030114

    const/4 v1, 0x0

    invoke-virtual {v2, v0, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    goto :goto_17

    :cond_16
    move-object v3, p2

    .line 48
    .local v3, "view":Landroid/view/View;
    :goto_17
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/TributosAdapter;->getItem(I)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/ItemTributos;

    move-result-object v4

    .line 49
    .local v4, "itemTributo":Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/ItemTributos;
    const v0, 0x7f0e030b

    invoke-static {v3, v0}, Lcom/itau/empresas/ui/util/ViewHolder;->get(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/TextView;

    .line 50
    .local v5, "itemMenu":Landroid/widget/TextView;
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/TributosAdapter;->itemTributos:Ljava/util/List;

    if-eqz v0, :cond_30

    .line 51
    invoke-virtual {v4}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/ItemTributos;->getItemMenu()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    :cond_30
    return-object v3
.end method

.method public setListaItensTributos(Ljava/util/List;)V
    .registers 2
    .param p1, "itemTributos"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/ItemTributos;>;)V"
        }
    .end annotation

    .line 28
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/TributosAdapter;->itemTributos:Ljava/util/List;

    .line 29
    return-void
.end method

.class public Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;
.super Ljava/lang/Object;
.source "ResultadoSimulacaoRecargaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field cliente:Lcom/itau/empresas/api/model/DadosClienteVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cliente"
    .end annotation
.end field

.field recarga:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "recarga"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRecarga()Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;
    .registers 2

    .line 20
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;->recarga:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ResultadoSimulacaoRecargaVO{cliente="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;->cliente:Lcom/itau/empresas/api/model/DadosClienteVO;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", recarga="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;->recarga:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class final Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;
.super Ljava/lang/Object;
.source "DateTimeFormatterBuilder.java"

# interfaces
.implements Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/threeten/bp/format/DateTimeFormatterBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "CompositePrinterParser"
.end annotation


# instance fields
.field private final optional:Z

.field private final printerParsers:[Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;


# direct methods
.method constructor <init>(Ljava/util/List;Z)V
    .registers 4
    .param p1, "printerParsers"    # Ljava/util/List;
    .param p2, "optional"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;>;Z)V"
        }
    .end annotation

    .line 1966
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;

    invoke-direct {p0, v0, p2}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;-><init>([Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;Z)V

    .line 1967
    return-void
.end method

.method constructor <init>([Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;Z)V
    .registers 3
    .param p1, "printerParsers"    # [Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;
    .param p2, "optional"    # Z

    .line 1969
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1970
    iput-object p1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;->printerParsers:[Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;

    .line 1971
    iput-boolean p2, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;->optional:Z

    .line 1972
    return-void
.end method


# virtual methods
.method public parse(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;I)I
    .registers 10
    .param p1, "context"    # Lorg/threeten/bp/format/DateTimeParseContext;
    .param p2, "text"    # Ljava/lang/CharSequence;
    .param p3, "position"    # I

    .line 2010
    iget-boolean v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;->optional:Z

    if-eqz v0, :cond_23

    .line 2011
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeParseContext;->startOptional()V

    .line 2012
    move v1, p3

    .line 2013
    .local v1, "pos":I
    iget-object v2, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;->printerParsers:[Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;

    .local v2, "arr$":[Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;
    array-length v3, v2

    .local v3, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_c
    if-ge v4, v3, :cond_1e

    aget-object v5, v2, v4

    .line 2014
    .local v5, "pp":Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;
    invoke-interface {v5, p1, p2, v1}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;->parse(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;I)I

    move-result v1

    .line 2015
    if-gez v1, :cond_1b

    .line 2016
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->endOptional(Z)V

    .line 2017
    return p3

    .line 2013
    .end local v5    # "pp":Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;
    :cond_1b
    add-int/lit8 v4, v4, 0x1

    goto :goto_c

    .line 2020
    .end local v2    # "arr$":[Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;
    .end local v3    # "len$":I
    .end local v4    # "i$":I
    :cond_1e
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->endOptional(Z)V

    .line 2021
    return v1

    .line 2023
    .end local v1    # "pos":I
    :cond_23
    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;->printerParsers:[Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;

    .local v1, "arr$":[Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;
    array-length v2, v1

    .local v2, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_27
    if-ge v3, v2, :cond_35

    aget-object v4, v1, v3

    .line 2024
    .local v4, "pp":Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;
    invoke-interface {v4, p1, p2, p3}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;->parse(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;I)I

    move-result p3

    .line 2025
    if-gez p3, :cond_32

    .line 2026
    goto :goto_35

    .line 2023
    .end local v4    # "pp":Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;
    :cond_32
    add-int/lit8 v3, v3, 0x1

    goto :goto_27

    .line 2029
    .end local v1    # "arr$":[Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;
    .end local v2    # "len$":I
    .end local v3    # "i$":I
    :cond_35
    :goto_35
    return p3
.end method

.method public print(Lorg/threeten/bp/format/DateTimePrintContext;Ljava/lang/StringBuilder;)Z
    .registers 11
    .param p1, "context"    # Lorg/threeten/bp/format/DateTimePrintContext;
    .param p2, "buf"    # Ljava/lang/StringBuilder;

    .line 1989
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    .line 1990
    .local v1, "length":I
    iget-boolean v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;->optional:Z

    if-eqz v0, :cond_b

    .line 1991
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimePrintContext;->startOptional()V

    .line 1994
    :cond_b
    :try_start_b
    iget-object v2, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;->printerParsers:[Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;

    .local v2, "arr$":[Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;
    array-length v3, v2

    .local v3, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_f
    if-ge v4, v3, :cond_28

    aget-object v5, v2, v4

    .line 1995
    .local v5, "pp":Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;
    invoke-interface {v5, p1, p2}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;->print(Lorg/threeten/bp/format/DateTimePrintContext;Ljava/lang/StringBuilder;)Z

    move-result v0

    if-nez v0, :cond_25

    .line 1996
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->setLength(I)V
    :try_end_1c
    .catchall {:try_start_b .. :try_end_1c} :catchall_30

    .line 1997
    const/4 v6, 0x1

    .line 2001
    iget-boolean v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;->optional:Z

    if-eqz v0, :cond_24

    .line 2002
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimePrintContext;->endOptional()V

    :cond_24
    return v6

    .line 1994
    .end local v5    # "pp":Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;
    :cond_25
    add-int/lit8 v4, v4, 0x1

    goto :goto_f

    .line 2001
    .end local v2    # "arr$":[Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;
    .end local v3    # "len$":I
    .end local v4    # "i$":I
    :cond_28
    iget-boolean v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;->optional:Z

    if-eqz v0, :cond_39

    .line 2002
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimePrintContext;->endOptional()V

    goto :goto_39

    .line 2001
    :catchall_30
    move-exception v7

    iget-boolean v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;->optional:Z

    if-eqz v0, :cond_38

    .line 2002
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimePrintContext;->endOptional()V

    :cond_38
    throw v7

    .line 2005
    :cond_39
    :goto_39
    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    .line 2035
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2036
    .local v1, "buf":Ljava/lang/StringBuilder;
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;->printerParsers:[Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;

    if-eqz v0, :cond_2f

    .line 2037
    iget-boolean v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;->optional:Z

    if-eqz v0, :cond_10

    const-string v0, "["

    goto :goto_12

    :cond_10
    const-string v0, "("

    :goto_12
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2038
    iget-object v2, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;->printerParsers:[Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;

    .local v2, "arr$":[Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;
    array-length v3, v2

    .local v3, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_19
    if-ge v4, v3, :cond_23

    aget-object v5, v2, v4

    .line 2039
    .local v5, "pp":Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2038
    .end local v5    # "pp":Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;
    add-int/lit8 v4, v4, 0x1

    goto :goto_19

    .line 2041
    .end local v2    # "arr$":[Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;
    .end local v3    # "len$":I
    .end local v4    # "i$":I
    :cond_23
    iget-boolean v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;->optional:Z

    if-eqz v0, :cond_2a

    const-string v0, "]"

    goto :goto_2c

    :cond_2a
    const-string v0, ")"

    :goto_2c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2043
    :cond_2f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public withOptional(Z)Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;
    .registers 4
    .param p1, "optional"    # Z

    .line 1981
    iget-boolean v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;->optional:Z

    if-ne p1, v0, :cond_5

    .line 1982
    return-object p0

    .line 1984
    :cond_5
    new-instance v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;

    iget-object v1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;->printerParsers:[Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;

    invoke-direct {v0, v1, p1}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$CompositePrinterParser;-><init>([Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;Z)V

    return-object v0
.end method

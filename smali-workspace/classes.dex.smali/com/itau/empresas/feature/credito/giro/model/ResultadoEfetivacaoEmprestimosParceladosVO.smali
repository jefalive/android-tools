.class public Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;
.super Ljava/lang/Object;
.source "ResultadoEfetivacaoEmprestimosParceladosVO.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO$BandeirasFormulario;,
        Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO$DadosPeriodicidade;,
        Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO$DadosProduto;
    }
.end annotation


# instance fields
.field private agencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia"
    .end annotation
.end field

.field private agenciaCartao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia_cartao"
    .end annotation
.end field

.field private bandeirasFormulario:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "bandeiras_formulario"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO$BandeirasFormulario;>;"
        }
    .end annotation
.end field

.field private cnpjCliente:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cnpj_cliente"
    .end annotation
.end field

.field private codigoAutenticacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_autenticacao"
    .end annotation
.end field

.field private codigoBandeiraTrava:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_bandeira_trava"
    .end annotation
.end field

.field private codigoComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_tipo_comprovante"
    .end annotation
.end field

.field private codigoContrato:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_contrato"
    .end annotation
.end field

.field private codigoOperacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_operacao"
    .end annotation
.end field

.field private codigoPlanoFinanciamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_plano_financiamento"
    .end annotation
.end field

.field private codigoProdutoAmortizado:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_produto_amortizado"
    .end annotation
.end field

.field private conta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta"
    .end annotation
.end field

.field private contaCartao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta_cartao"
    .end annotation
.end field

.field private cpfOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf_operador"
    .end annotation
.end field

.field private dadosPeriodicidade:Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO$DadosPeriodicidade;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dados_periodicidade"
    .end annotation
.end field

.field private dadosProduto:Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO$DadosProduto;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dados_produto"
    .end annotation
.end field

.field private dataContratacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_contratacao"
    .end annotation
.end field

.field private dataHoraTransacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_hora_transacao"
    .end annotation
.end field

.field private dataLiberacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_liberacao"
    .end annotation
.end field

.field private dataOperacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_operacao"
    .end annotation
.end field

.field private dataPrazoVencimento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_prazo_vencimento"
    .end annotation
.end field

.field private dataPrimeiroPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_primeiro_pagamento"
    .end annotation
.end field

.field private dataUltimoPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_ultimo_pagamento"
    .end annotation
.end field

.field private descricaoBandeiraCartao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_bandeira_cartao"
    .end annotation
.end field

.field private descricaoBandeiraTrava:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_bandeira_trava"
    .end annotation
.end field

.field private descricaoPrazo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_prazo"
    .end annotation
.end field

.field private diaVencimento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dia_vencimento"
    .end annotation
.end field

.field private digitoConta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_conta"
    .end annotation
.end field

.field private digitoContaCartao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_conta_cartao"
    .end annotation
.end field

.field private horarioOperacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "horario_operacao"
    .end annotation
.end field

.field private indicadorOfertaLoop:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_oferta_loop"
    .end annotation
.end field

.field private indicadorRecebivelCartao:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_recebivel_cartao"
    .end annotation
.end field

.field private indicadorTaxaBasica:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_taxa_basica"
    .end annotation
.end field

.field private nomeCliente:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_cliente"
    .end annotation
.end field

.field private nomeOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_operador"
    .end annotation
.end field

.field private numeroConvenio:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_convenio"
    .end annotation
.end field

.field private numeroOperacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_operacao"
    .end annotation
.end field

.field private numeroProposta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_proposta"
    .end annotation
.end field

.field private numeroVersaoTipoComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_versao_tipo_comprovante"
    .end annotation
.end field

.field private prazoDias:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "prazo_dias"
    .end annotation
.end field

.field private quantidadeParcelas:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade_parcelas"
    .end annotation
.end field

.field private solicitacaoTravaGarantiaCartao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "solicitacao_trava_garantia_cartao"
    .end annotation
.end field

.field private valorEmprestimo:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_emprestimo"
    .end annotation
.end field

.field private versaoConvenio:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "versao_convenio"
    .end annotation
.end field

.field private versaoProposta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "versao_proposta"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDataOperacao()Ljava/lang/String;
    .registers 2

    .line 155
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;->dataOperacao:Ljava/lang/String;

    return-object v0
.end method

.method public getHorarioOperacao()Ljava/lang/String;
    .registers 2

    .line 160
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;->horarioOperacao:Ljava/lang/String;

    return-object v0
.end method

.method public getValorEmprestimo()F
    .registers 2

    .line 146
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;->valorEmprestimo:F

    return v0
.end method

.class public abstract Lcom/daimajia/swipe/adapters/BaseSwipeAdapter;
.super Landroid/widget/BaseAdapter;
.source "BaseSwipeAdapter.java"

# interfaces
.implements Lcom/daimajia/swipe/interfaces/SwipeItemMangerInterface;
.implements Lcom/daimajia/swipe/interfaces/SwipeAdapterInterface;


# instance fields
.field protected mItemManger:Lcom/daimajia/swipe/implments/SwipeItemAdapterMangerImpl;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 16
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 18
    new-instance v0, Lcom/daimajia/swipe/implments/SwipeItemAdapterMangerImpl;

    invoke-direct {v0, p0}, Lcom/daimajia/swipe/implments/SwipeItemAdapterMangerImpl;-><init>(Landroid/widget/BaseAdapter;)V

    iput-object v0, p0, Lcom/daimajia/swipe/adapters/BaseSwipeAdapter;->mItemManger:Lcom/daimajia/swipe/implments/SwipeItemAdapterMangerImpl;

    return-void
.end method


# virtual methods
.method public abstract fillValues(ILandroid/view/View;)V
.end method

.method public abstract generateView(ILandroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .line 48
    move-object v1, p2

    .line 49
    .local v1, "v":Landroid/view/View;
    if-nez v1, :cond_d

    .line 50
    invoke-virtual {p0, p1, p3}, Lcom/daimajia/swipe/adapters/BaseSwipeAdapter;->generateView(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 51
    iget-object v0, p0, Lcom/daimajia/swipe/adapters/BaseSwipeAdapter;->mItemManger:Lcom/daimajia/swipe/implments/SwipeItemAdapterMangerImpl;

    invoke-virtual {v0, v1, p1}, Lcom/daimajia/swipe/implments/SwipeItemAdapterMangerImpl;->initialize(Landroid/view/View;I)V

    goto :goto_12

    .line 53
    :cond_d
    iget-object v0, p0, Lcom/daimajia/swipe/adapters/BaseSwipeAdapter;->mItemManger:Lcom/daimajia/swipe/implments/SwipeItemAdapterMangerImpl;

    invoke-virtual {v0, v1, p1}, Lcom/daimajia/swipe/implments/SwipeItemAdapterMangerImpl;->updateConvertView(Landroid/view/View;I)V

    .line 55
    :goto_12
    invoke-virtual {p0, p1, v1}, Lcom/daimajia/swipe/adapters/BaseSwipeAdapter;->fillValues(ILandroid/view/View;)V

    .line 56
    return-object v1
.end method

.method public setMode(Lcom/daimajia/swipe/util/Attributes$Mode;)V
    .registers 3
    .param p1, "mode"    # Lcom/daimajia/swipe/util/Attributes$Mode;

    .line 106
    iget-object v0, p0, Lcom/daimajia/swipe/adapters/BaseSwipeAdapter;->mItemManger:Lcom/daimajia/swipe/implments/SwipeItemAdapterMangerImpl;

    invoke-virtual {v0, p1}, Lcom/daimajia/swipe/implments/SwipeItemAdapterMangerImpl;->setMode(Lcom/daimajia/swipe/util/Attributes$Mode;)V

    .line 107
    return-void
.end method

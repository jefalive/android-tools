.class public Lcom/itau/empresas/feature/mapa/MapaFragment;
.super Lcom/itau/empresas/ui/fragment/BaseFragment;
.source "MapaFragment.java"

# interfaces
.implements Lcom/google/android/gms/maps/OnMapReadyCallback;
.implements Lcom/google/android/gms/maps/GoogleMap$OnMarkerClickListener;
.implements Lcom/google/android/gms/maps/GoogleMap$OnCameraChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/mapa/MapaFragment$PanelSlideListener;
    }
.end annotation


# instance fields
.field private agencias:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;"
        }
    .end annotation
.end field

.field imgCaixaExclusivo:Landroid/widget/ImageView;

.field imgHorarioDiferente:Landroid/widget/ImageView;

.field lastPosition:Lcom/google/android/gms/maps/model/LatLng;

.field linearMapDragView:Landroid/widget/LinearLayout;

.field llIconesServicos:Landroid/widget/LinearLayout;

.field llMapaTitulo:Landroid/widget/LinearLayout;

.field private map:Lcom/google/android/gms/maps/GoogleMap;

.field mapaView:Lcom/google/android/gms/maps/MapView;

.field slidingUpPanelMapa:Lcom/sothree/slidinguppanel/SlidingUpPanelLayout;

.field textoAtendimentoEmpresas:Landroid/widget/TextView;

.field textoBairroAgencia:Landroid/widget/TextView;

.field textoEnderecoAgencia:Landroid/widget/TextView;

.field textoHorarioAtendimento:Landroid/widget/TextView;

.field textoNomeAgencia:Landroid/widget/TextView;

.field textoServicos:Landroid/widget/TextView;

.field textoTelefone:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 53
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/mapa/MapaFragment;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/mapa/MapaFragment;

    .line 53
    invoke-direct {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->exibeMensagem()V

    return-void
.end method

.method private addAgencias(Ljava/util/List;)V
    .registers 12
    .param p1, "agencias"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;)V"
        }
    .end annotation

    .line 292
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->map:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v0, :cond_97

    .line 293
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->clear()V

    .line 294
    .line 295
    const v0, 0x7f02018b

    invoke-static {v0}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->fromResource(I)Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v7

    .line 296
    .line 297
    .local v7, "icon":Lcom/google/android/gms/maps/model/BitmapDescriptor;
    const v0, 0x7f02018c

    invoke-static {v0}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->fromResource(I)Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v8

    .line 298
    .local v8, "iconPers":Lcom/google/android/gms/maps/model/BitmapDescriptor;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_18
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v9, v0, :cond_97

    .line 299
    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/AgenciaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/AgenciaVO;->getNome()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_68

    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/AgenciaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/AgenciaVO;->getNome()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PERS"

    .line 300
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_68

    .line 301
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->map:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v1, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    invoke-virtual {v1, v8}, Lcom/google/android/gms/maps/model/MarkerOptions;->icon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    .line 302
    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/itau/empresas/api/model/AgenciaVO;

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/AgenciaVO;->getLatitude()D

    move-result-wide v3

    .line 303
    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/itau/empresas/api/model/AgenciaVO;

    invoke-virtual {v5}, Lcom/itau/empresas/api/model/AgenciaVO;->getLongitude()D

    move-result-wide v5

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 302
    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->position(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    .line 301
    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/GoogleMap;->addMarker(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/Marker;

    goto :goto_93

    .line 306
    :cond_68
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->map:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v1, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    invoke-virtual {v1, v7}, Lcom/google/android/gms/maps/model/MarkerOptions;->icon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    .line 307
    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/itau/empresas/api/model/AgenciaVO;

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/AgenciaVO;->getLatitude()D

    move-result-wide v3

    .line 308
    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/itau/empresas/api/model/AgenciaVO;

    invoke-virtual {v5}, Lcom/itau/empresas/api/model/AgenciaVO;->getLongitude()D

    move-result-wide v5

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 307
    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->position(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    .line 306
    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/GoogleMap;->addMarker(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/Marker;

    .line 298
    :goto_93
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_18

    .line 313
    .end local v7    # "icon":Lcom/google/android/gms/maps/model/BitmapDescriptor;
    .end local v8    # "iconPers":Lcom/google/android/gms/maps/model/BitmapDescriptor;
    .end local v9    # "i":I
    :cond_97
    return-void
.end method

.method private atualizaDadosDaAgencia(Lcom/itau/empresas/api/model/AgenciaVO;)V
    .registers 5
    .param p1, "agencia"    # Lcom/itau/empresas/api/model/AgenciaVO;

    .line 365
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/AgenciaVO;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/AgenciaVO;->getNome()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 367
    .local v2, "nome":Ljava/lang/String;
    const v0, 0x7f070296

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/itau/empresas/feature/mapa/MapaFragment;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->textoNomeAgencia:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 370
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->textoEnderecoAgencia:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/AgenciaVO;->getEndereco()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 371
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->textoBairroAgencia:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/AgenciaVO;->getComplementoEndereco()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 372
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->textoTelefone:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/AgenciaVO;->getTelefone()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 373
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->textoHorarioAtendimento:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/AgenciaVO;->getHorario()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 375
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/AgenciaVO;->getAtendePJ()Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 376
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->textoAtendimentoEmpresas:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_66

    .line 379
    :cond_5f
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->textoAtendimentoEmpresas:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 381
    :goto_66
    return-void
.end method

.method private atualizaServicos(Z)V
    .registers 4
    .param p1, "servicos"    # Z

    .line 384
    if-eqz p1, :cond_f

    .line 385
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->textoServicos:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 386
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->llIconesServicos:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1d

    .line 389
    :cond_f
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->textoServicos:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 390
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->llIconesServicos:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 392
    :goto_1d
    return-void
.end method

.method private buscaAgenciaPorPosicao(Lcom/google/android/gms/maps/model/LatLng;)Lcom/itau/empresas/api/model/AgenciaVO;
    .registers 8
    .param p1, "position"    # Lcom/google/android/gms/maps/model/LatLng;

    .line 357
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->agencias:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/itau/empresas/api/model/AgenciaVO;

    .line 358
    .local v5, "agencia":Lcom/itau/empresas/api/model/AgenciaVO;
    iget-wide v0, p1, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    invoke-virtual {v5}, Lcom/itau/empresas/api/model/AgenciaVO;->getLatitude()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_28

    iget-wide v0, p1, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    .line 359
    invoke-virtual {v5}, Lcom/itau/empresas/api/model/AgenciaVO;->getLongitude()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_28

    .line 360
    return-object v5

    .line 358
    .end local v5    # "agencia":Lcom/itau/empresas/api/model/AgenciaVO;
    :cond_28
    goto :goto_6

    .line 361
    :cond_29
    const/4 v0, 0x0

    return-object v0
.end method

.method private carregaAgenciasPorPosicao(Lcom/google/android/gms/maps/model/LatLng;)V
    .registers 6
    .param p1, "position"    # Lcom/google/android/gms/maps/model/LatLng;

    .line 328
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/mapa/MapaFragment;->buscaAgenciaPorPosicao(Lcom/google/android/gms/maps/model/LatLng;)Lcom/itau/empresas/api/model/AgenciaVO;

    move-result-object v2

    .line 329
    .local v2, "agencia":Lcom/itau/empresas/api/model/AgenciaVO;
    if-nez v2, :cond_7

    .line 330
    return-void

    .line 331
    :cond_7
    invoke-direct {p0, v2}, Lcom/itau/empresas/feature/mapa/MapaFragment;->atualizaDadosDaAgencia(Lcom/itau/empresas/api/model/AgenciaVO;)V

    .line 333
    const/4 v3, 0x0

    .line 334
    .local v3, "servicos":Z
    invoke-virtual {v2}, Lcom/itau/empresas/api/model/AgenciaVO;->getHorarioDiferenciado()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_22

    .line 335
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->textoHorarioAtendimento:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/AgenciaVO;->getHorarioDiferenciado()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 336
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->imgHorarioDiferente:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 337
    const/4 v3, 0x1

    goto :goto_32

    .line 340
    :cond_22
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->textoHorarioAtendimento:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/AgenciaVO;->getHorario()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->imgHorarioDiferente:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 344
    :goto_32
    invoke-virtual {v2}, Lcom/itau/empresas/api/model/AgenciaVO;->getCaixaExclusivo()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4c

    const-string v0, "1"

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/AgenciaVO;->getCaixaExclusivo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 345
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->imgCaixaExclusivo:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 346
    const/4 v3, 0x1

    goto :goto_53

    .line 349
    :cond_4c
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->imgCaixaExclusivo:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 352
    :goto_53
    invoke-direct {p0, v3}, Lcom/itau/empresas/feature/mapa/MapaFragment;->atualizaServicos(Z)V

    .line 353
    invoke-direct {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->mudaEstadoSlidingUpPanel()V

    .line 354
    return-void
.end method

.method private exibeMensagem()V
    .registers 4

    .line 316
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070634

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 317
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 318
    return-void
.end method

.method private getMinhaPosicaoAtual()Lcom/google/android/gms/maps/model/LatLng;
    .registers 9

    .line 429
    :try_start_0
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->application:Lcom/itau/empresas/CustomApplication;

    const-string v1, "location"

    .line 430
    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/location/LocationManager;

    .line 431
    .local v5, "service":Landroid/location/LocationManager;
    invoke-virtual {v5}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v6

    .line 433
    .local v6, "providers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v7

    .line 434
    .local v7, "location":Landroid/location/Location;
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v7}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {v7}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V
    :try_end_27
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_27} :catch_28

    return-object v0

    .line 435
    .end local v5    # "service":Landroid/location/LocationManager;
    .end local v6    # "providers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v6
    .end local v7    # "location":Landroid/location/Location;
    :catch_28
    move-exception v5

    .line 436
    .local v5, "e":Ljava/lang/Exception;
    const-string v0, "MapaFragment"

    const-string v1, "getMinhaPosicaoAtual: "

    invoke-static {v0, v1, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 437
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "n\u00e3o foi poss\u00edvel recuperar sua localiza\u00e7\u00e3o"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 439
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 441
    .end local v5    # "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    return-object v0
.end method

.method private inicializarMapa()V
    .registers 4

    .line 156
    :try_start_0
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/maps/MapsInitializer;->initialize(Landroid/content/Context;)I
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_b} :catch_c

    .line 159
    goto :goto_14

    .line 157
    :catch_c
    move-exception v2

    .line 158
    .local v2, "e":Ljava/lang/Exception;
    const-string v0, "MapaFragment"

    const-string v1, "inicializarMapa: "

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 160
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_14
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->mapaView:Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/maps/MapView;->getMapAsync(Lcom/google/android/gms/maps/OnMapReadyCallback;)V

    .line 161
    return-void
.end method

.method private mostrarDialogPermissoesMapa()V
    .registers 5

    .line 247
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->baseActivity()Lcom/itau/empresas/ui/activity/BaseActivity;

    move-result-object v2

    .line 249
    .local v2, "activity":Lcom/itau/empresas/ui/activity/BaseActivity;
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 250
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->cancelable(Z)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 251
    const v1, 0x7f0704f8

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->UM_BOTAO:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 252
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 253
    const v1, 0x7f07048c

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNeutro(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 254
    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v3

    .line 256
    .local v3, "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    new-instance v0, Lcom/itau/empresas/feature/mapa/MapaFragment$3;

    invoke-direct {v0, p0, v3}, Lcom/itau/empresas/feature/mapa/MapaFragment$3;-><init>(Lcom/itau/empresas/feature/mapa/MapaFragment;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v3, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNeutroListener(Landroid/view/View$OnClickListener;)V

    .line 263
    invoke-virtual {v3, v2}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 264
    return-void
.end method

.method private mudaEstadoSlidingUpPanel()V
    .registers 3

    .line 395
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->slidingUpPanelMapa:Lcom/sothree/slidinguppanel/SlidingUpPanelLayout;

    invoke-virtual {v0}, Lcom/sothree/slidinguppanel/SlidingUpPanelLayout;->getPanelState()Lcom/sothree/slidinguppanel/SlidingUpPanelLayout$PanelState;

    move-result-object v0

    sget-object v1, Lcom/sothree/slidinguppanel/SlidingUpPanelLayout$PanelState;->EXPANDED:Lcom/sothree/slidinguppanel/SlidingUpPanelLayout$PanelState;

    if-eq v0, v1, :cond_11

    .line 397
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->slidingUpPanelMapa:Lcom/sothree/slidinguppanel/SlidingUpPanelLayout;

    sget-object v1, Lcom/sothree/slidinguppanel/SlidingUpPanelLayout$PanelState;->EXPANDED:Lcom/sothree/slidinguppanel/SlidingUpPanelLayout$PanelState;

    invoke-virtual {v0, v1}, Lcom/sothree/slidinguppanel/SlidingUpPanelLayout;->setPanelState(Lcom/sothree/slidinguppanel/SlidingUpPanelLayout$PanelState;)V

    .line 399
    :cond_11
    return-void
.end method

.method private setaLatLngDefault()V
    .registers 7

    .line 268
    const-wide v2, -0x3fc8731120b9b3ebL    # -23.5505199

    .line 269
    .local v2, "latitude":D
    const-wide v4, -0x3fb8aeefb7b36014L    # -46.6333094

    .line 270
    .local v4, "longitude":D
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->lastPosition:Lcom/google/android/gms/maps/model/LatLng;

    .line 271
    return-void
.end method


# virtual methods
.method carregar()V
    .registers 4

    .line 101
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->setHasOptionsMenu(Z)V

    .line 102
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->mapaView:Lcom/google/android/gms/maps/MapView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/MapView;->onCreate(Landroid/os/Bundle;)V

    .line 104
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->slidingUpPanelMapa:Lcom/sothree/slidinguppanel/SlidingUpPanelLayout;

    new-instance v1, Lcom/itau/empresas/feature/mapa/MapaFragment$PanelSlideListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/itau/empresas/feature/mapa/MapaFragment$PanelSlideListener;-><init>(Lcom/itau/empresas/feature/mapa/MapaFragment;Lcom/itau/empresas/feature/mapa/MapaFragment$1;)V

    invoke-virtual {v0, v1}, Lcom/sothree/slidinguppanel/SlidingUpPanelLayout;->setPanelSlideListener(Lcom/sothree/slidinguppanel/SlidingUpPanelLayout$PanelSlideListener;)V

    .line 105
    return-void
.end method

.method menuFilter()V
    .registers 4

    .line 414
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/feature/mapa/MapaActivity;

    .line 416
    .local v2, "activity":Lcom/itau/empresas/feature/mapa/MapaActivity;
    if-eqz v2, :cond_f

    .line 417
    iget-object v0, v2, Lcom/itau/empresas/feature/mapa/MapaActivity;->drawerLayoutFilter:Landroid/support/v4/widget/DrawerLayout;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->openDrawer(I)V

    .line 419
    :cond_f
    return-void
.end method

.method public onCameraChange(Lcom/google/android/gms/maps/model/CameraPosition;)V
    .registers 10
    .param p1, "cameraPosition"    # Lcom/google/android/gms/maps/model/CameraPosition;

    .line 165
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v1, p1, Lcom/google/android/gms/maps/model/CameraPosition;->target:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v1, v1, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-object v3, p1, Lcom/google/android/gms/maps/model/CameraPosition;->target:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v3, v3, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->lastPosition:Lcom/google/android/gms/maps/model/LatLng;

    invoke-static {v0, v1}, Lcom/google/maps/android/SphericalUtil;->computeDistanceBetween(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;)D

    move-result-wide v6

    .line 168
    .local v6, "distance":D
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/mapa/MapaActivity;

    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v2, p1, Lcom/google/android/gms/maps/model/CameraPosition;->target:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-object v4, p1, Lcom/google/android/gms/maps/model/CameraPosition;->target:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, v4, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 169
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/mapa/MapaActivity;->setLatLong(Lcom/google/android/gms/maps/model/LatLng;)V

    .line 171
    const-wide v0, 0x409f400000000000L    # 2000.0

    cmpl-double v0, v6, v0

    if-lez v0, :cond_4c

    .line 172
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v1, p1, Lcom/google/android/gms/maps/model/CameraPosition;->target:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v1, v1, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    iget-object v3, p1, Lcom/google/android/gms/maps/model/CameraPosition;->target:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v3, v3, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->lastPosition:Lcom/google/android/gms/maps/model/LatLng;

    .line 174
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/mapa/MapaActivity;

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->lastPosition:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/mapa/MapaActivity;->atualizaMapa(Lcom/google/android/gms/maps/model/LatLng;)V

    .line 176
    :cond_4c
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 6
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .line 403
    const v0, 0x7f0f0005

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 405
    const v0, 0x7f0e06c5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 406
    .local v1, "itemMenu":Landroid/view/MenuItem;
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 408
    const v0, 0x7f0e06c6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 409
    .local v2, "itemMenuPesquisa":Landroid/view/MenuItem;
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 410
    return-void
.end method

.method public onDestroyView()V
    .registers 2

    .line 140
    invoke-super {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->onDestroyView()V

    .line 141
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->mapaView:Lcom/google/android/gms/maps/MapView;

    if-eqz v0, :cond_c

    .line 142
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->mapaView:Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapView;->onDestroy()V

    .line 144
    :cond_c
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoListaMapas;)V
    .registers 4
    .param p1, "eventoListaMapas"    # Lcom/itau/empresas/Evento$EventoListaMapas;

    .line 88
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 90
    .local v1, "activity":Landroid/support/v4/app/FragmentActivity;
    instance-of v0, v1, Lcom/itau/empresas/ui/activity/BaseActivity;

    if-eqz v0, :cond_e

    .line 91
    move-object v0, v1

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 92
    :cond_e
    invoke-static {v1}, Lcom/itau/empresas/ui/util/SnackbarUtils;->tentaDispensarSnackbarNo(Ljava/lang/Object;)V

    .line 94
    invoke-static {}, Lcom/itau/empresas/Evento$EventoListaMapas;->getAgencias()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->agencias:Ljava/util/List;

    .line 95
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->agencias:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->addAgencias(Ljava/util/List;)V

    .line 96
    return-void
.end method

.method public onMapReady(Lcom/google/android/gms/maps/GoogleMap;)V
    .registers 8
    .param p1, "googleMap"    # Lcom/google/android/gms/maps/GoogleMap;

    .line 180
    iput-object p1, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->map:Lcom/google/android/gms/maps/GoogleMap;

    .line 182
    invoke-direct {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getMinhaPosicaoAtual()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->lastPosition:Lcom/google/android/gms/maps/model/LatLng;

    .line 183
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->lastPosition:Lcom/google/android/gms/maps/model/LatLng;

    if-nez v0, :cond_1d

    .line 185
    const-wide v2, -0x3fc8731120b9b3ebL    # -23.5505199

    .line 186
    .local v2, "latitude":D
    const-wide v4, -0x3fb8aeefb7b36014L    # -46.6333094

    .line 187
    .local v4, "longitude":D
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->lastPosition:Lcom/google/android/gms/maps/model/LatLng;

    .line 190
    .end local v2    # "latitude":D
    .end local v4    # "longitude":D
    :cond_1d
    new-instance v0, Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;-><init>()V

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->lastPosition:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->target(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    move-result-object v0

    const/high16 v1, 0x41700000    # 15.0f

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->zoom(F)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    move-result-object v0

    .line 191
    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->build()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v2

    .line 192
    .local v2, "cameraPosition":Lcom/google/android/gms/maps/model/CameraPosition;
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-static {v2}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newCameraPosition(Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/GoogleMap;->animateCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 193
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->map:Lcom/google/android/gms/maps/GoogleMap;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/GoogleMap;->setBuildingsEnabled(Z)V

    .line 194
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/UiSettings;->setAllGesturesEnabled(Z)V

    .line 195
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/UiSettings;->setCompassEnabled(Z)V

    .line 196
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/UiSettings;->setMapToolbarEnabled(Z)V

    .line 197
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/UiSettings;->setZoomControlsEnabled(Z)V

    .line 198
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/maps/GoogleMap;->setOnMarkerClickListener(Lcom/google/android/gms/maps/GoogleMap$OnMarkerClickListener;)V

    .line 199
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->map:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v1, Lcom/itau/empresas/feature/mapa/MapaFragment$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/mapa/MapaFragment$1;-><init>(Lcom/itau/empresas/feature/mapa/MapaFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/GoogleMap;->setOnMyLocationButtonClickListener(Lcom/google/android/gms/maps/GoogleMap$OnMyLocationButtonClickListener;)V

    .line 219
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->map:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v1, Lcom/itau/empresas/feature/mapa/MapaFragment$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/mapa/MapaFragment$2;-><init>(Lcom/itau/empresas/feature/mapa/MapaFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/GoogleMap;->setOnMapClickListener(Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;)V

    .line 225
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->map:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/maps/GoogleMap;->setOnCameraChangeListener(Lcom/google/android/gms/maps/GoogleMap$OnCameraChangeListener;)V

    .line 227
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/mapa/MapaActivity;

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->lastPosition:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/mapa/MapaActivity;->atualizaMapa(Lcom/google/android/gms/maps/model/LatLng;)V

    .line 228
    .line 229
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v0, v1}, Landroid/support/v4/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_ab

    .line 231
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-static {v0, v1}, Landroid/support/v4/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_ab

    .line 233
    return-void

    .line 236
    :cond_ab
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->map:Lcom/google/android/gms/maps/GoogleMap;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/GoogleMap;->setMyLocationEnabled(Z)V

    .line 239
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->agencias:Ljava/util/List;

    if-eqz v0, :cond_ba

    .line 240
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->agencias:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->addAgencias(Ljava/util/List;)V

    .line 243
    :cond_ba
    return-void
.end method

.method public onMarkerClick(Lcom/google/android/gms/maps/model/Marker;)Z
    .registers 3
    .param p1, "marker"    # Lcom/google/android/gms/maps/model/Marker;

    .line 322
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/Marker;->hideInfoWindow()V

    .line 323
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/Marker;->getPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->carregaAgenciasPorPosicao(Lcom/google/android/gms/maps/model/LatLng;)V

    .line 324
    const/4 v0, 0x0

    return v0
.end method

.method public onPause()V
    .registers 2

    .line 148
    invoke-super {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->onPause()V

    .line 149
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->mapaView:Lcom/google/android/gms/maps/MapView;

    if-eqz v0, :cond_c

    .line 150
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->mapaView:Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapView;->onPause()V

    .line 152
    :cond_c
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .registers 6
    .param p1, "requestCode"    # I
    .param p2, "permissions"    # [Ljava/lang/String;
    .param p3, "grantResults"    # [I

    .line 277
    const/4 v0, 0x2

    if-ne p1, v0, :cond_22

    .line 278
    array-length v0, p3

    if-lez v0, :cond_b

    const/4 v0, 0x0

    aget v0, p3, v0

    if-eqz v0, :cond_1f

    .line 280
    :cond_b
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 281
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionShowLocation()[Ljava/lang/String;

    move-result-object v1

    .line 280
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/PermissoesUtil;->shouldShowRequestPermissionRationale(Landroid/app/Activity;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 282
    invoke-direct {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->mostrarDialogPermissoesMapa()V

    .line 285
    :cond_1c
    invoke-direct {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->setaLatLngDefault()V

    .line 287
    :cond_1f
    invoke-direct {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->inicializarMapa()V

    .line 289
    :cond_22
    return-void
.end method

.method public onResume()V
    .registers 3

    .line 109
    invoke-super {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->onResume()V

    .line 110
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->mapaView:Lcom/google/android/gms/maps/MapView;

    if-eqz v0, :cond_c

    .line 111
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->mapaView:Lcom/google/android/gms/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/MapView;->onResume()V

    .line 119
    .line 120
    :cond_c
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionShowLocation()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/PermissoesUtil;->hasSelfPermissions(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_37

    .line 122
    .line 123
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionShowLocation()[Ljava/lang/String;

    move-result-object v0

    .line 122
    invoke-static {p0, v0}, Lcom/itau/empresas/ui/util/PermissoesUtil;->shouldShowRequestPermissionRationale(Landroid/support/v4/app/Fragment;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 124
    invoke-direct {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->exibeMensagem()V

    .line 125
    invoke-direct {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->setaLatLngDefault()V

    .line 126
    invoke-direct {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->inicializarMapa()V

    goto :goto_3a

    .line 129
    :cond_2e
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionShowLocation()[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/mapa/MapaFragment;->requestPermissions([Ljava/lang/String;I)V

    goto :goto_3a

    .line 134
    :cond_37
    invoke-direct {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->inicializarMapa()V

    .line 136
    :goto_3a
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 446
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public setAgencias(Ljava/util/List;)V
    .registers 2
    .param p1, "agencias"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;)V"
        }
    .end annotation

    .line 82
    iput-object p1, p0, Lcom/itau/empresas/feature/mapa/MapaFragment;->agencias:Ljava/util/List;

    .line 83
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/mapa/MapaFragment;->addAgencias(Ljava/util/List;)V

    .line 84
    return-void
.end method

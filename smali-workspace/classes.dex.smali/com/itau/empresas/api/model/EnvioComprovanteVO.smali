.class public Lcom/itau/empresas/api/model/EnvioComprovanteVO;
.super Ljava/lang/Object;
.source "EnvioComprovanteVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private enderecoEmail:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "endereco_email"
    .end annotation
.end field

.field private mensagem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "mensagem"
    .end annotation
.end field

.field private nome:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setEnderecoEmail(Ljava/lang/String;)V
    .registers 2
    .param p1, "enderecoEmail"    # Ljava/lang/String;

    .line 27
    iput-object p1, p0, Lcom/itau/empresas/api/model/EnvioComprovanteVO;->enderecoEmail:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public setMensagem(Ljava/lang/String;)V
    .registers 2
    .param p1, "mensagem"    # Ljava/lang/String;

    .line 35
    iput-object p1, p0, Lcom/itau/empresas/api/model/EnvioComprovanteVO;->mensagem:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public setNome(Ljava/lang/String;)V
    .registers 2
    .param p1, "nome"    # Ljava/lang/String;

    .line 23
    iput-object p1, p0, Lcom/itau/empresas/api/model/EnvioComprovanteVO;->nome:Ljava/lang/String;

    .line 24
    return-void
.end method

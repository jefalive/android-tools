.class public Lcom/itau/empresas/feature/login/controller/LoginController;
.super Lcom/itau/empresas/controller/BaseController;
.source "LoginController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/itau/empresas/controller/BaseController<Lcom/itau/empresas/feature/login/LoginApi;>;"
    }
.end annotation


# instance fields
.field app:Lcom/itau/empresas/CustomApplication;

.field menuController:Lcom/itau/empresas/feature/login/controller/MenuController;

.field operadorController:Lcom/itau/empresas/feature/login/controller/OperadorController;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 21
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/login/controller/LoginController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/controller/LoginController;

    .line 21
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/controller/LoginController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 21
    invoke-static {p0}, Lcom/itau/empresas/feature/login/controller/LoginController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/login/controller/LoginController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/api/model/LoginInputVO;
    .registers 5
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/controller/LoginController;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/feature/login/controller/LoginController;->getInputVO(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/api/model/LoginInputVO;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/itau/empresas/feature/login/controller/LoginController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/controller/LoginController;

    .line 21
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/controller/LoginController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/itau/empresas/feature/login/controller/LoginController;Lcom/itau/empresas/api/model/PerfilOperadorVO;)Z
    .registers 3
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/controller/LoginController;
    .param p1, "x1"    # Lcom/itau/empresas/api/model/PerfilOperadorVO;

    .line 21
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/login/controller/LoginController;->verificaTrocaSenha(Lcom/itau/empresas/api/model/PerfilOperadorVO;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/itau/empresas/feature/login/controller/LoginController;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/controller/LoginController;

    .line 21
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/controller/LoginController;->criaSessao()V

    return-void
.end method

.method private criaSessao()V
    .registers 2

    .line 99
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/LoginController;->menuController:Lcom/itau/empresas/feature/login/controller/MenuController;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/controller/MenuController;->carregarMenu()V

    .line 100
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/LoginController;->operadorController:Lcom/itau/empresas/feature/login/controller/OperadorController;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/controller/OperadorController;->buscaContasOperador()V

    .line 101
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/LoginController;->operadorController:Lcom/itau/empresas/feature/login/controller/OperadorController;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/controller/OperadorController;->buscaDadosOperador()V

    .line 102
    new-instance v0, Lcom/itau/empresas/Evento$EventoLoginFinalizado;

    invoke-direct {v0}, Lcom/itau/empresas/Evento$EventoLoginFinalizado;-><init>()V

    invoke-static {v0}, Lcom/itau/empresas/feature/login/controller/LoginController;->post(Ljava/lang/Object;)V

    .line 103
    return-void
.end method

.method private getInputVO(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/api/model/LoginInputVO;
    .registers 5
    .param p1, "operador"    # Ljava/lang/String;
    .param p2, "senha"    # Ljava/lang/String;
    .param p3, "idTeclado"    # Ljava/lang/String;

    .line 107
    new-instance v0, Lcom/itau/empresas/api/model/LoginInputVO;

    invoke-direct {v0}, Lcom/itau/empresas/api/model/LoginInputVO;-><init>()V

    .line 108
    .local v0, "loginInputVO":Lcom/itau/empresas/api/model/LoginInputVO;
    invoke-virtual {v0, p1}, Lcom/itau/empresas/api/model/LoginInputVO;->setCodigoOperador(Ljava/lang/String;)V

    .line 109
    invoke-virtual {v0, p3}, Lcom/itau/empresas/api/model/LoginInputVO;->setIdTeclado(Ljava/lang/String;)V

    .line 110
    invoke-virtual {v0, p2}, Lcom/itau/empresas/api/model/LoginInputVO;->setSenha(Ljava/lang/String;)V

    .line 111
    return-object v0
.end method

.method private verificaTrocaSenha(Lcom/itau/empresas/api/model/PerfilOperadorVO;)Z
    .registers 4
    .param p1, "perfilOperadorVO"    # Lcom/itau/empresas/api/model/PerfilOperadorVO;

    .line 65
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->isSenhaExpirada()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->isPrimeiroAcesso()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 66
    new-instance v0, Lcom/itau/empresas/Evento$EventoTrocaDeSenha;

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->isPrimeiroAcesso()Z

    move-result v1

    invoke-direct {v0, v1}, Lcom/itau/empresas/Evento$EventoTrocaDeSenha;-><init>(Z)V

    invoke-static {v0}, Lcom/itau/empresas/feature/login/controller/LoginController;->post(Ljava/lang/Object;)V

    .line 67
    const/4 v0, 0x1

    return v0

    .line 68
    :cond_1a
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->isSenhaExpirada()Z

    move-result v0

    if-nez v0, :cond_30

    .line 69
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->isPrimeiroAcesso()Z

    move-result v0

    if-eqz v0, :cond_30

    .line 70
    new-instance v0, Lcom/itau/empresas/Evento$EventoPrimeiroAcesso;

    invoke-direct {v0}, Lcom/itau/empresas/Evento$EventoPrimeiroAcesso;-><init>()V

    invoke-static {v0}, Lcom/itau/empresas/feature/login/controller/LoginController;->post(Ljava/lang/Object;)V

    .line 71
    const/4 v0, 0x1

    return v0

    .line 72
    :cond_30
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->isSenhaExpirada()Z

    move-result v0

    if-eqz v0, :cond_44

    .line 73
    new-instance v0, Lcom/itau/empresas/Evento$EventoTrocaDeSenha;

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->isPrimeiroAcesso()Z

    move-result v1

    invoke-direct {v0, v1}, Lcom/itau/empresas/Evento$EventoTrocaDeSenha;-><init>(Z)V

    invoke-static {v0}, Lcom/itau/empresas/feature/login/controller/LoginController;->post(Ljava/lang/Object;)V

    .line 74
    const/4 v0, 0x1

    return v0

    .line 76
    :cond_44
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public autenticar(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "operador"    # Ljava/lang/String;
    .param p2, "senha"    # Ljava/lang/String;
    .param p3, "idTeclado"    # Ljava/lang/String;

    .line 46
    new-instance v0, Lcom/itau/empresas/feature/login/controller/LoginController$2;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/itau/empresas/feature/login/controller/LoginController$2;-><init>(Lcom/itau/empresas/feature/login/controller/LoginController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 62
    return-void
.end method

.method public autenticarFingerprint(Ljava/lang/String;)V
    .registers 5
    .param p1, "codigoOperador"    # Ljava/lang/String;

    .line 81
    :try_start_0
    const-class v0, Lcom/itau/empresas/api/model/PerfilOperadorVO;

    .line 82
    invoke-static {v0, p1}, Lbr/com/itau/sdk/android/core/BackendClient;->loginKeyAccess(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/api/model/PerfilOperadorVO;

    .line 84
    .local v2, "perfilOperadorVO":Lcom/itau/empresas/api/model/PerfilOperadorVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/LoginController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0, v2}, Lcom/itau/empresas/CustomApplication;->setPerfilLogado(Lcom/itau/empresas/api/model/PerfilOperadorVO;)V

    .line 85
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/LoginController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/CustomApplication;->setCodigoOperador(Ljava/lang/String;)V

    .line 87
    invoke-direct {p0, v2}, Lcom/itau/empresas/feature/login/controller/LoginController;->verificaTrocaSenha(Lcom/itau/empresas/api/model/PerfilOperadorVO;)Z
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_16} :catch_1e

    move-result v0

    if-eqz v0, :cond_1a

    .line 88
    return-void

    .line 90
    :cond_1a
    :try_start_1a
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/controller/LoginController;->criaSessao()V
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1d} :catch_1e

    .line 95
    .end local v2    # "perfilOperadorVO":Lcom/itau/empresas/api/model/PerfilOperadorVO;
    goto :goto_30

    .line 92
    :catch_1e
    move-exception v2

    .line 93
    .local v2, "ex":Ljava/lang/Exception;
    const-string v0, "logon"

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 94
    new-instance v0, Lcom/itau/empresas/Evento$EventoErroLoginFingerPrint;

    invoke-direct {v0}, Lcom/itau/empresas/Evento$EventoErroLoginFingerPrint;-><init>()V

    invoke-static {v0}, Lcom/itau/empresas/feature/login/controller/LoginController;->post(Ljava/lang/Object;)V

    .line 96
    .end local v2    # "ex":Ljava/lang/Exception;
    :goto_30
    return-void
.end method

.method public buscaTeclado(Ljava/lang/String;)V
    .registers 3
    .param p1, "codigoOperador"    # Ljava/lang/String;

    .line 33
    new-instance v0, Lcom/itau/empresas/feature/login/controller/LoginController$1;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/feature/login/controller/LoginController$1;-><init>(Lcom/itau/empresas/feature/login/controller/LoginController;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 43
    return-void
.end method

.class Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView$1;
.super Ljava/lang/Object;
.source "ItemContatoRecargaView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->excluirContato()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;

    .line 88
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView$1;->this$0:Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .line 92
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView$1;->this$0:Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->mostraDialogoDeProgresso()V

    .line 93
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView$1;->this$0:Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;

    iget-object v0, v0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->contatoRecargaController:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView$1;->this$0:Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;

    # getter for: Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->contato:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;
    invoke-static {v1}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->access$000(Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;)Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getIdContato()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->excluiContatoRecarga(Ljava/lang/String;)V

    .line 94
    return-void
.end method

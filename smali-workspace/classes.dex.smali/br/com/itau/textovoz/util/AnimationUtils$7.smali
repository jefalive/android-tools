.class final Lbr/com/itau/textovoz/util/AnimationUtils$7;
.super Landroid/view/animation/Animation;
.source "AnimationUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/textovoz/util/AnimationUtils;->collapseAnimation(Landroid/view/View;ILandroid/view/animation/Animation$AnimationListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$initialHeight:I

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;I)V
    .registers 3

    .line 184
    iput-object p1, p0, Lbr/com/itau/textovoz/util/AnimationUtils$7;->val$view:Landroid/view/View;

    iput p2, p0, Lbr/com/itau/textovoz/util/AnimationUtils$7;->val$initialHeight:I

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .registers 6
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .line 187
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-nez v0, :cond_e

    .line 188
    iget-object v0, p0, Lbr/com/itau/textovoz/util/AnimationUtils$7;->val$view:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_23

    .line 190
    :cond_e
    iget-object v0, p0, Lbr/com/itau/textovoz/util/AnimationUtils$7;->val$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lbr/com/itau/textovoz/util/AnimationUtils$7;->val$initialHeight:I

    iget v2, p0, Lbr/com/itau/textovoz/util/AnimationUtils$7;->val$initialHeight:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    float-to-int v2, v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 192
    iget-object v0, p0, Lbr/com/itau/textovoz/util/AnimationUtils$7;->val$view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 194
    :goto_23
    return-void
.end method

.method public willChangeBounds()Z
    .registers 2

    .line 198
    const/4 v0, 0x1

    return v0
.end method

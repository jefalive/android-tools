.class Lbr/com/itau/widgets/graphicalfriedcake/GraphicView$1;
.super Ljava/lang/Object;
.source "GraphicView.java"

# interfaces
.implements Lbr/com/itau/widgets/graphicalfriedcake/PageIndicator$Configuration;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->initPiechart()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

.field final synthetic val$pa:Lbr/com/itau/widgets/graphicalfriedcake/DetailsPageAdapter;


# direct methods
.method constructor <init>(Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;Lbr/com/itau/widgets/graphicalfriedcake/DetailsPageAdapter;)V
    .registers 3
    .param p1, "this$0"    # Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

    .line 86
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView$1;->this$0:Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

    iput-object p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView$1;->val$pa:Lbr/com/itau/widgets/graphicalfriedcake/DetailsPageAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public count()I
    .registers 3

    .line 90
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView$1;->val$pa:Lbr/com/itau/widgets/graphicalfriedcake/DetailsPageAdapter;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/DetailsPageAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public selected(I)Z
    .registers 3
    .param p1, "i"    # I

    .line 95
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView$1;->this$0:Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

    # getter for: Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->pagerDetailsList:Landroid/support/v4/view/ViewPager;
    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->access$000(Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-ne v0, p1, :cond_e

    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

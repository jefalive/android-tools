.class public final Lcom/itau/empresas/ui/activity/WebViewActivity_;
.super Lcom/itau/empresas/ui/activity/WebViewActivity;
.source "WebViewActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/activity/WebViewActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 32
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;-><init>()V

    .line 36
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/ui/activity/WebViewActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/WebViewActivity_;

    .line 32
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/ui/activity/WebViewActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/WebViewActivity_;

    .line 32
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private findSupportFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
    .registers 3
    .param p1, "tag"    # Ljava/lang/String;

    .line 95
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity_;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 50
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 51
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 52
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 53
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity_;->injectExtras_()V

    .line 54
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity_;->afterInject()V

    .line 55
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 99
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 100
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_2a

    .line 101
    const-string v0, "chaveMobile"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 102
    const-string v0, "chaveMobile"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity_;->chaveMobile:Ljava/lang/String;

    .line 104
    :cond_1a
    const-string v0, "nomeMenu"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 105
    const-string v0, "nomeMenu"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity_;->nomeMenu:Ljava/lang/String;

    .line 108
    :cond_2a
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/WebViewActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 76
    new-instance v0, Lcom/itau/empresas/ui/activity/WebViewActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/WebViewActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 133
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/ui/activity/WebViewActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/WebViewActivity_$2;-><init>(Lcom/itau/empresas/ui/activity/WebViewActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 141
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 121
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/ui/activity/WebViewActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/WebViewActivity_$1;-><init>(Lcom/itau/empresas/ui/activity/WebViewActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 129
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 42
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 43
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/activity/WebViewActivity_;->init_(Landroid/os/Bundle;)V

    .line 44
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/WebViewActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 46
    const v0, 0x7f03006b

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/WebViewActivity_;->setContentView(I)V

    .line 47
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 89
    const v0, 0x7f0e025a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 90
    const-string v0, "webviewfragment"

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/activity/WebViewActivity_;->findSupportFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity_;->webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 91
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity_;->aoCarregarWebView()V

    .line 92
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 59
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/WebViewActivity;->setContentView(I)V

    .line 60
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 61
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 71
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/WebViewActivity;->setContentView(Landroid/view/View;)V

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 73
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 65
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/ui/activity/WebViewActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 67
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 112
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/WebViewActivity;->setIntent(Landroid/content/Intent;)V

    .line 113
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity_;->injectExtras_()V

    .line 114
    return-void
.end method

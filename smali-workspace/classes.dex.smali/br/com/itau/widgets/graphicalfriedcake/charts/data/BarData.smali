.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleData;
.source "BarData.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleData<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;>;"
    }
.end annotation


# instance fields
.field private mGroupSpace:F


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 26
    invoke-direct {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleData;-><init>()V

    .line 16
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;->mGroupSpace:F

    .line 27
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .registers 3
    .param p1, "xVals"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/String;>;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleData;-><init>(Ljava/util/List;)V

    .line 16
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;->mGroupSpace:F

    .line 32
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;)V
    .registers 4
    .param p1, "xVals"    # Ljava/util/List;
    .param p2, "dataSet"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/String;>;Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;)V"
        }
    .end annotation

    .line 51
    invoke-static {p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;->toList(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleData;-><init>(Ljava/util/List;Ljava/util/List;)V

    .line 16
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;->mGroupSpace:F

    .line 52
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .registers 4
    .param p1, "xVals"    # Ljava/util/List;
    .param p2, "dataSets"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/String;>;Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;>;)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleData;-><init>(Ljava/util/List;Ljava/util/List;)V

    .line 16
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;->mGroupSpace:F

    .line 42
    return-void
.end method

.method public constructor <init>([Ljava/lang/String;)V
    .registers 3
    .param p1, "xVals"    # [Ljava/lang/String;

    .line 36
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleData;-><init>([Ljava/lang/String;)V

    .line 16
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;->mGroupSpace:F

    .line 37
    return-void
.end method

.method public constructor <init>([Ljava/lang/String;Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;)V
    .registers 4
    .param p1, "xVals"    # [Ljava/lang/String;
    .param p2, "dataSet"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;

    .line 56
    invoke-static {p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;->toList(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleData;-><init>([Ljava/lang/String;Ljava/util/List;)V

    .line 16
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;->mGroupSpace:F

    .line 57
    return-void
.end method

.method public constructor <init>([Ljava/lang/String;Ljava/util/List;)V
    .registers 4
    .param p1, "xVals"    # [Ljava/lang/String;
    .param p2, "dataSets"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([Ljava/lang/String;Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;>;)V"
        }
    .end annotation

    .line 46
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleData;-><init>([Ljava/lang/String;Ljava/util/List;)V

    .line 16
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;->mGroupSpace:F

    .line 47
    return-void
.end method

.method private static toList(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;)Ljava/util/List;
    .registers 3
    .param p0, "dataSet"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;)Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;>;"
        }
    .end annotation

    .line 61
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 62
    .local v1, "sets":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;>;"
    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    return-object v1
.end method


# virtual methods
.method public getGroupSpace()F
    .registers 3

    .line 77
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_b

    .line 78
    const/4 v0, 0x0

    return v0

    .line 80
    :cond_b
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;->mGroupSpace:F

    return v0
.end method

.method public isGrouped()Z
    .registers 3

    .line 103
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_b

    const/4 v0, 0x1

    goto :goto_c

    :cond_b
    const/4 v0, 0x0

    :goto_c
    return v0
.end method

.method public setGroupSpace(F)V
    .registers 3
    .param p1, "percent"    # F

    .line 92
    const/high16 v0, 0x42c80000    # 100.0f

    div-float v0, p1, v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarData;->mGroupSpace:F

    .line 93
    return-void
.end method

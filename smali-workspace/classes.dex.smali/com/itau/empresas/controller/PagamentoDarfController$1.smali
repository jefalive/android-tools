.class Lcom/itau/empresas/controller/PagamentoDarfController$1;
.super Ljava/lang/Object;
.source "PagamentoDarfController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/controller/PagamentoDarfController;->consultaLimitesHorario(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/controller/PagamentoDarfController;

.field final synthetic val$tipoOperacaoSispag:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/controller/PagamentoDarfController;Ljava/lang/String;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/controller/PagamentoDarfController;

    .line 47
    iput-object p1, p0, Lcom/itau/empresas/controller/PagamentoDarfController$1;->this$0:Lcom/itau/empresas/controller/PagamentoDarfController;

    iput-object p2, p0, Lcom/itau/empresas/controller/PagamentoDarfController$1;->val$tipoOperacaoSispag:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 3

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoDarfController$1;->this$0:Lcom/itau/empresas/controller/PagamentoDarfController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/PagamentoDarfController;->api()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/controller/PagamentoDarfController$1;->val$tipoOperacaoSispag:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/itau/empresas/api/Api;->consultaLimitesHorarioPagamento(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/BaseController;->post(Ljava/lang/Object;)V

    .line 51
    return-void
.end method

.class public abstract Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
.super Ljava/lang/Object;
.source "DataSet.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field protected mAxisDependency:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

.field protected mColors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/Integer;>;"
        }
    .end annotation
.end field

.field protected mDrawValues:Z

.field protected mHighlightEnabled:Z

.field private mLabel:Ljava/lang/String;

.field protected mLastEnd:I

.field protected mLastStart:I

.field private mValueColor:I

.field protected mValueFormatter:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;

.field private mValueTextSize:F

.field private mValueTypeface:Landroid/graphics/Typeface;

.field private mVisible:Z

.field protected mYMax:F

.field protected mYMin:F

.field protected mYVals:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<TT;>;"
        }
    .end annotation
.end field

.field private mYValueSum:F


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;)V
    .registers 7
    .param p1, "yVals"    # Ljava/util/List;
    .param p2, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<TT;>;Ljava/lang/String;)V"
        }
    .end annotation

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mColors:Ljava/util/List;

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMax:F

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMin:F

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYValueSum:F

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mLastStart:I

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mLastEnd:I

    .line 49
    const-string v0, "DataSet"

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mLabel:Ljava/lang/String;

    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mVisible:Z

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mDrawValues:Z

    .line 58
    const/high16 v0, -0x1000000

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mValueColor:I

    .line 61
    const/high16 v0, 0x41880000    # 17.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mValueTextSize:F

    .line 70
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mAxisDependency:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mHighlightEnabled:Z

    .line 85
    iput-object p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mLabel:Ljava/lang/String;

    .line 86
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    .line 88
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    if-nez v0, :cond_40

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    .line 91
    :cond_40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mColors:Ljava/util/List;

    .line 94
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mColors:Ljava/util/List;

    const/16 v1, 0x8c

    const/16 v2, 0xea

    const/16 v3, 0xff

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mLastStart:I

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mLastEnd:I

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->calcMinMax(II)V

    .line 97
    invoke-direct {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->calcYValueSum()V

    .line 98
    return-void
.end method

.method private calcYValueSum()V
    .registers 5

    .line 155
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYValueSum:F

    .line 157
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_4
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_27

    .line 158
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    .line 159
    .local v3, "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    if-eqz v3, :cond_24

    .line 160
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYValueSum:F

    invoke-virtual {v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYValueSum:F

    .line 157
    .end local v3    # "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    :cond_24
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 162
    .end local v2    # "i":I
    :cond_27
    return-void
.end method


# virtual methods
.method public addColor(I)V
    .registers 4
    .param p1, "color"    # I

    .line 638
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mColors:Ljava/util/List;

    if-nez v0, :cond_b

    .line 639
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mColors:Ljava/util/List;

    .line 640
    :cond_b
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mColors:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 641
    return-void
.end method

.method public addEntry(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;)V
    .registers 4
    .param p1, "e"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    .line 475
    if-nez p1, :cond_3

    .line 476
    return-void

    .line 478
    :cond_3
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v1

    .line 480
    .local v1, "val":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    if-nez v0, :cond_12

    .line 481
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    .line 484
    :cond_12
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1f

    .line 485
    iput v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMax:F

    .line 486
    iput v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMin:F

    goto :goto_2f

    .line 488
    :cond_1f
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMax:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_27

    .line 489
    iput v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMax:F

    .line 490
    :cond_27
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMin:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2f

    .line 491
    iput v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMin:F

    .line 494
    :cond_2f
    :goto_2f
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYValueSum:F

    add-float/2addr v0, v1

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYValueSum:F

    .line 497
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 498
    return-void
.end method

.method public addEntryOrdered(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;)V
    .registers 7
    .param p1, "e"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    .line 511
    if-nez p1, :cond_3

    .line 512
    return-void

    .line 514
    :cond_3
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v3

    .line 516
    .local v3, "val":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    if-nez v0, :cond_12

    .line 517
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    .line 520
    :cond_12
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1f

    .line 521
    iput v3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMax:F

    .line 522
    iput v3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMin:F

    goto :goto_2f

    .line 524
    :cond_1f
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMax:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_27

    .line 525
    iput v3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMax:F

    .line 526
    :cond_27
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMin:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2f

    .line 527
    iput v3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMin:F

    .line 530
    :cond_2f
    :goto_2f
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYValueSum:F

    add-float/2addr v0, v3

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYValueSum:F

    .line 532
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_78

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v0

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v1

    if-le v0, v1, :cond_78

    .line 534
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getEntryIndex(I)I

    move-result v4

    .line 535
    .local v4, "closestIndex":I
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v0

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v1

    if-ge v0, v1, :cond_72

    .line 536
    add-int/lit8 v4, v4, 0x1

    .line 537
    :cond_72
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0, v4, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 538
    return-void

    .line 541
    .end local v4    # "closestIndex":I
    :cond_78
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 542
    return-void
.end method

.method protected calcMinMax(II)V
    .registers 9
    .param p1, "start"    # I
    .param p2, "end"    # I

    .line 112
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 114
    .local v2, "yValCount":I
    if-nez v2, :cond_9

    .line 115
    return-void

    .line 119
    :cond_9
    if-eqz p2, :cond_d

    if-lt p2, v2, :cond_10

    .line 120
    :cond_d
    add-int/lit8 v3, v2, -0x1

    .local v3, "endValue":I
    goto :goto_11

    .line 122
    .end local v3    # "endValue":I
    :cond_10
    move v3, p2

    .line 124
    .local v3, "endValue":I
    :goto_11
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mLastStart:I

    .line 125
    iput v3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mLastEnd:I

    .line 127
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMin:F

    .line 128
    const v0, -0x800001

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMax:F

    .line 130
    move v4, p1

    .local v4, "i":I
    :goto_20
    if-gt v4, v3, :cond_5a

    .line 132
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    .line 134
    .local v5, "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    if-eqz v5, :cond_57

    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_57

    .line 136
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMin:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_47

    .line 137
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMin:F

    .line 139
    :cond_47
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMax:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_57

    .line 140
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMax:F

    .line 130
    .end local v5    # "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    :cond_57
    add-int/lit8 v4, v4, 0x1

    goto :goto_20

    .line 144
    .end local v4    # "i":I
    :cond_5a
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMin:F

    const v1, 0x7f7fffff    # Float.MAX_VALUE

    cmpl-float v0, v0, v1

    if-nez v0, :cond_69

    .line 145
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMin:F

    .line 146
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMax:F

    .line 148
    :cond_69
    return-void
.end method

.method public clear()V
    .registers 2

    .line 836
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 837
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mLastStart:I

    .line 838
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mLastEnd:I

    .line 839
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->notifyDataSetChanged()V

    .line 840
    return-void
.end method

.method public contains(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;)Z
    .registers 5
    .param p1, "e"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    .line 824
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    .line 825
    .local v2, "entry":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 826
    const/4 v0, 0x1

    return v0

    .line 827
    .end local v2    # "entry":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    :cond_1b
    goto :goto_6

    .line 829
    :cond_1c
    const/4 v0, 0x0

    return v0
.end method

.method public abstract copy()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<TT;>;"
        }
    .end annotation
.end method

.method public getAxisDependency()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;
    .registers 2

    .line 431
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mAxisDependency:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    return-object v0
.end method

.method public getColor()I
    .registers 3

    .line 681
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mColors:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getColor(I)I
    .registers 4
    .param p1, "index"    # I

    .line 671
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mColors:Ljava/util/List;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mColors:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int v1, p1, v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getColors()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/Integer;>;"
        }
    .end annotation

    .line 660
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mColors:Ljava/util/List;

    return-object v0
.end method

.method public getEntriesForXIndex(I)Ljava/util/List;
    .registers 9
    .param p1, "x"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)Ljava/util/List<TT;>;"
        }
    .end annotation

    .line 257
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 259
    .local v2, "entries":Ljava/util/List;, "Ljava/util/List<TT;>;"
    const/4 v3, 0x0

    .line 260
    .local v3, "low":I
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v4, v0, -0x1

    .line 262
    .local v4, "high":I
    :goto_e
    if-gt v3, v4, :cond_62

    .line 263
    add-int v0, v4, v3

    div-int/lit8 v5, v0, 0x2

    .line 264
    .local v5, "m":I
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    .line 266
    .local v6, "entry":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;, "TT;"
    invoke-virtual {v6}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v0

    if-ne p1, v0, :cond_55

    .line 267
    :goto_23
    if-lez v5, :cond_38

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    add-int/lit8 v1, v5, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v0

    if-ne v0, p1, :cond_38

    .line 268
    add-int/lit8 v5, v5, -0x1

    goto :goto_23

    .line 270
    :cond_38
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    .line 271
    :goto_3e
    if-ge v5, v4, :cond_55

    .line 273
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    .line 274
    invoke-virtual {v6}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v0

    if-ne v0, p1, :cond_55

    .line 276
    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271
    add-int/lit8 v5, v5, 0x1

    goto :goto_3e

    .line 285
    :cond_55
    invoke-virtual {v6}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v0

    if-le p1, v0, :cond_5e

    .line 286
    add-int/lit8 v3, v5, 0x1

    goto :goto_60

    .line 288
    :cond_5e
    add-int/lit8 v4, v5, -0x1

    .line 289
    .end local v5    # "m":I
    .end local v6    # "entry":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;, "TT;"
    .end local v6
    :goto_60
    goto/16 :goto_e

    .line 291
    :cond_62
    return-object v2
.end method

.method public getEntryCount()I
    .registers 2

    .line 170
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEntryForXIndex(I)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    .registers 4
    .param p1, "x"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .line 204
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getEntryIndex(I)I

    move-result v1

    .line 205
    .local v1, "index":I
    const/4 v0, -0x1

    if-le v1, v0, :cond_10

    .line 206
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    return-object v0

    .line 207
    :cond_10
    const/4 v0, 0x0

    return-object v0
.end method

.method public getEntryIndex(I)I
    .registers 8
    .param p1, "x"    # I

    .line 222
    const/4 v2, 0x0

    .line 223
    .local v2, "low":I
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v3, v0, -0x1

    .line 224
    .local v3, "high":I
    const/4 v4, -0x1

    .line 226
    .local v4, "closest":I
    :goto_a
    if-gt v2, v3, :cond_49

    .line 227
    add-int v0, v3, v2

    div-int/lit8 v5, v0, 0x2

    .line 229
    .local v5, "m":I
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v0

    if-ne p1, v0, :cond_34

    .line 230
    :goto_1e
    if-lez v5, :cond_33

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    add-int/lit8 v1, v5, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v0

    if-ne v0, p1, :cond_33

    .line 231
    add-int/lit8 v5, v5, -0x1

    goto :goto_1e

    .line 233
    :cond_33
    return v5

    .line 236
    :cond_34
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v0

    if-le p1, v0, :cond_45

    .line 237
    add-int/lit8 v2, v5, 0x1

    goto :goto_47

    .line 239
    :cond_45
    add-int/lit8 v3, v5, -0x1

    .line 241
    :goto_47
    move v4, v5

    .line 242
    .end local v5    # "m":I
    goto :goto_a

    .line 244
    :cond_49
    return v4
.end method

.method public getEntryPosition(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;)I
    .registers 4
    .param p1, "e"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    .line 719
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1b

    .line 720
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    invoke-virtual {p1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->equalTo(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 721
    return v1

    .line 719
    :cond_18
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 724
    .end local v1    # "i":I
    :cond_1b
    const/4 v0, -0x1

    return v0
.end method

.method public getIndexInEntries(I)I
    .registers 4
    .param p1, "xIndex"    # I

    .line 350
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1b

    .line 351
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v0

    if-ne p1, v0, :cond_18

    .line 352
    return v1

    .line 350
    :cond_18
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 355
    .end local v1    # "i":I
    :cond_1b
    const/4 v0, -0x1

    return v0
.end method

.method public getLabel()Ljava/lang/String;
    .registers 2

    .line 402
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getValueCount()I
    .registers 2

    .line 336
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getValueFormatter()Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;
    .registers 3

    .line 750
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mValueFormatter:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;

    if-nez v0, :cond_b

    .line 751
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/DefaultValueFormatter;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/DefaultValueFormatter;-><init>(I)V

    return-object v0

    .line 752
    :cond_b
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mValueFormatter:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;

    return-object v0
.end method

.method public getValueTextColor()I
    .registers 2

    .line 780
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mValueColor:I

    return v0
.end method

.method public getValueTextSize()F
    .registers 2

    .line 811
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mValueTextSize:F

    return v0
.end method

.method public getValueTypeface()Landroid/graphics/Typeface;
    .registers 2

    .line 793
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mValueTypeface:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public getYMax()F
    .registers 2

    .line 327
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMax:F

    return v0
.end method

.method public getYMin()F
    .registers 2

    .line 318
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYMin:F

    return v0
.end method

.method public getYValForXIndex(I)F
    .registers 4
    .param p1, "xIndex"    # I

    .line 184
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getEntryForXIndex(I)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    move-result-object v1

    .line 186
    .local v1, "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    if-eqz v1, :cond_11

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v0

    if-ne v0, p1, :cond_11

    .line 187
    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    return v0

    .line 189
    :cond_11
    const/high16 v0, 0x7fc00000    # NaNf

    return v0
.end method

.method public getYVals()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<TT;>;"
        }
    .end annotation

    .line 300
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    return-object v0
.end method

.method public getYValueSum()F
    .registers 2

    .line 309
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYValueSum:F

    return v0
.end method

.method public isDrawValuesEnabled()Z
    .registers 2

    .line 461
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mDrawValues:Z

    return v0
.end method

.method public isHighlightEnabled()Z
    .registers 2

    .line 707
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mHighlightEnabled:Z

    return v0
.end method

.method public isVisible()Z
    .registers 2

    .line 422
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mVisible:Z

    return v0
.end method

.method public needsDefaultFormatter()Z
    .registers 2

    .line 762
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mValueFormatter:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;

    if-nez v0, :cond_6

    .line 763
    const/4 v0, 0x1

    return v0

    .line 764
    :cond_6
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mValueFormatter:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;

    instance-of v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/DefaultValueFormatter;

    if-eqz v0, :cond_e

    .line 765
    const/4 v0, 0x1

    return v0

    .line 767
    :cond_e
    const/4 v0, 0x0

    return v0
.end method

.method public notifyDataSetChanged()V
    .registers 3

    .line 104
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mLastStart:I

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mLastEnd:I

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->calcMinMax(II)V

    .line 105
    invoke-direct {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->calcYValueSum()V

    .line 106
    return-void
.end method

.method public removeEntry(I)Z
    .registers 4
    .param p1, "xIndex"    # I

    .line 579
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getEntryForXIndex(I)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    move-result-object v1

    .line 580
    .local v1, "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;, "TT;"
    invoke-virtual {p0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->removeEntry(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;)Z

    move-result v0

    return v0
.end method

.method public removeEntry(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;)Z
    .registers 6
    .param p1, "e"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .line 554
    if-nez p1, :cond_4

    .line 555
    const/4 v0, 0x0

    return v0

    .line 558
    :cond_4
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v2

    .line 560
    .local v2, "removed":Z
    if-eqz v2, :cond_1c

    .line 562
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v3

    .line 563
    .local v3, "val":F
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYValueSum:F

    sub-float/2addr v0, v3

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYValueSum:F

    .line 565
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mLastStart:I

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mLastEnd:I

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->calcMinMax(II)V

    .line 568
    .end local v3    # "val":F
    :cond_1c
    return v2
.end method

.method public resetColors()V
    .registers 2

    .line 688
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mColors:Ljava/util/List;

    .line 689
    return-void
.end method

.method public setAxisDependency(Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;)V
    .registers 2
    .param p1, "dependency"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    .line 441
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mAxisDependency:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    .line 442
    return-void
.end method

.method public setColor(I)V
    .registers 4
    .param p1, "color"    # I

    .line 650
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->resetColors()V

    .line 651
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mColors:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 652
    return-void
.end method

.method public setColors(Ljava/util/List;)V
    .registers 2
    .param p1, "colors"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/Integer;>;)V"
        }
    .end annotation

    .line 595
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mColors:Ljava/util/List;

    .line 596
    return-void
.end method

.method public setColors([I)V
    .registers 3
    .param p1, "colors"    # [I

    .line 608
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ColorTemplate;->createColors([I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mColors:Ljava/util/List;

    .line 609
    return-void
.end method

.method public setColors([ILandroid/content/Context;)V
    .registers 9
    .param p1, "colors"    # [I
    .param p2, "c"    # Landroid/content/Context;

    .line 623
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 625
    .local v1, "clrs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    move-object v2, p1

    array-length v3, v2

    const/4 v4, 0x0

    :goto_8
    if-ge v4, v3, :cond_1e

    aget v5, v2, v4

    .line 626
    .local v5, "color":I
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 625
    .end local v5    # "color":I
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    .line 629
    :cond_1e
    iput-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mColors:Ljava/util/List;

    .line 630
    return-void
.end method

.method public setDrawValues(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 452
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mDrawValues:Z

    .line 453
    return-void
.end method

.method public setHighlightEnabled(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 698
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mHighlightEnabled:Z

    .line 699
    return-void
.end method

.method public setLabel(Ljava/lang/String;)V
    .registers 2
    .param p1, "label"    # Ljava/lang/String;

    .line 393
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mLabel:Ljava/lang/String;

    .line 394
    return-void
.end method

.method public setValueFormatter(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;)V
    .registers 2
    .param p1, "f"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;

    .line 738
    if-nez p1, :cond_3

    .line 739
    return-void

    .line 741
    :cond_3
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mValueFormatter:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;

    .line 742
    return-void
.end method

.method public setValueTextColor(I)V
    .registers 2
    .param p1, "color"    # I

    .line 776
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mValueColor:I

    .line 777
    return-void
.end method

.method public setValueTextSize(F)V
    .registers 3
    .param p1, "size"    # F

    .line 802
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mValueTextSize:F

    .line 803
    return-void
.end method

.method public setValueTypeface(Landroid/graphics/Typeface;)V
    .registers 2
    .param p1, "tf"    # Landroid/graphics/Typeface;

    .line 789
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mValueTypeface:Landroid/graphics/Typeface;

    .line 790
    return-void
.end method

.method public setVisible(Z)V
    .registers 2
    .param p1, "visible"    # Z

    .line 412
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mVisible:Z

    .line 413
    return-void
.end method

.method public toSimpleString()Ljava/lang/String;
    .registers 4

    .line 382
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 383
    .local v2, "buffer":Ljava/lang/StringBuffer;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DataSet, label: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mLabel:Ljava/lang/String;

    if-nez v1, :cond_17

    const-string v1, ""

    goto :goto_19

    :cond_17
    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mLabel:Ljava/lang/String;

    :goto_19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", entries: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 384
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .line 367
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 368
    .local v2, "buffer":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->toSimpleString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 369
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_d
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_3a

    .line 370
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->mYVals:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 369
    add-int/lit8 v3, v3, 0x1

    goto :goto_d

    .line 372
    .end local v3    # "i":I
    :cond_3a
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

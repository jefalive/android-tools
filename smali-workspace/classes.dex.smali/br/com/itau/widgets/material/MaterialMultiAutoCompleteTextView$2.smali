.class Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$2;
.super Ljava/lang/Object;
.source "MaterialMultiAutoCompleteTextView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initFloatingLabel()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;


# direct methods
.method constructor <init>(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)V
    .registers 2

    .line 810
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$2;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 4
    .param p1, "s"    # Landroid/text/Editable;

    .line 821
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$2;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    # getter for: Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelEnabled:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->access$200(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 822
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_26

    .line 823
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$2;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    # getter for: Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelShown:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->access$300(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 824
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$2;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    const/4 v1, 0x0

    # setter for: Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelShown:Z
    invoke-static {v0, v1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->access$302(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;Z)Z

    .line 825
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$2;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    # invokes: Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getLabelAnimator()Lcom/nineoldandroids/animation/ObjectAnimator;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->access$400(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->reverse()V

    goto :goto_3d

    .line 827
    :cond_26
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$2;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    # getter for: Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelShown:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->access$300(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)Z

    move-result v0

    if-nez v0, :cond_3d

    .line 828
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$2;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    const/4 v1, 0x1

    # setter for: Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelShown:Z
    invoke-static {v0, v1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->access$302(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;Z)Z

    .line 829
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$2;->this$0:Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    # invokes: Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getLabelAnimator()Lcom/nineoldandroids/animation/ObjectAnimator;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->access$400(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->start()V

    .line 832
    :cond_3d
    :goto_3d
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .line 813
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .line 817
    return-void
.end method

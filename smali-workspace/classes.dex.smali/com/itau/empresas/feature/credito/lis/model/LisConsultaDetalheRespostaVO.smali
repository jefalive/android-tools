.class public Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;
.super Ljava/lang/Object;
.source "LisConsultaDetalheRespostaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO$LisConsultaRespostaVO;
    }
.end annotation


# instance fields
.field private codigoProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_produto"
    .end annotation
.end field

.field private dataVencimentoOperacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_vencimento_operacao"
    .end annotation
.end field

.field private limiteContratado:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "limite_contratado"
    .end annotation
.end field

.field private nomeProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_produto"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCodigoProduto()Ljava/lang/String;
    .registers 2

    .line 23
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;->codigoProduto:Ljava/lang/String;

    return-object v0
.end method

.method public getDataVencimentoOperacao()Ljava/lang/String;
    .registers 2

    .line 35
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;->dataVencimentoOperacao:Ljava/lang/String;

    return-object v0
.end method

.method public getLimiteContratado()F
    .registers 2

    .line 31
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;->limiteContratado:F

    return v0
.end method

.method public getNomeProduto()Ljava/lang/String;
    .registers 2

    .line 27
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;->nomeProduto:Ljava/lang/String;

    return-object v0
.end method

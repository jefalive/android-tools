.class Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$4;
.super Ljava/lang/Object;
.source "AtendimentoController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->buscaAlertas(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

.field final synthetic val$idAlerta:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;Ljava/lang/String;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

    .line 57
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$4;->this$0:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

    iput-object p2, p0, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$4;->val$idAlerta:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 6

    .line 60
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$4;->this$0:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

    # invokes: Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->api()Ljava/lang/Object;
    invoke-static {v0}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->access$600(Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$4;->val$idAlerta:Ljava/lang/String;

    iget-object v2, p0, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$4;->this$0:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

    iget-object v2, v2, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->app:Lcom/itau/empresas/CustomApplication;

    .line 61
    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v2

    const-string v3, "mobile"

    const-string v4, "O"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/itau/empresas/api/Api;->buscaAlertas(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/feature/atendimento/model/AlertaVO;

    move-result-object v0

    .line 60
    # invokes: Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->post(Ljava/lang/Object;)V
    invoke-static {v0}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->access$700(Ljava/lang/Object;)V

    .line 63
    return-void
.end method

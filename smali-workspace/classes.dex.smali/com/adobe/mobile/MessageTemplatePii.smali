.class final Lcom/adobe/mobile/MessageTemplatePii;
.super Lcom/adobe/mobile/MessageTemplateCallback;
.source "MessageTemplatePii.java"


# direct methods
.method constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Lcom/adobe/mobile/MessageTemplateCallback;-><init>()V

    return-void
.end method


# virtual methods
.method protected getQueue()Lcom/adobe/mobile/ThirdPartyQueue;
    .registers 2

    .line 35
    invoke-static {}, Lcom/adobe/mobile/PiiQueue;->sharedInstance()Lcom/adobe/mobile/PiiQueue;

    move-result-object v0

    return-object v0
.end method

.method protected initWithPayloadObject(Lorg/json/JSONObject;)Z
    .registers 6
    .param p1, "dictionary"    # Lorg/json/JSONObject;

    .line 14
    invoke-super {p0, p1}, Lcom/adobe/mobile/MessageTemplateCallback;->initWithPayloadObject(Lorg/json/JSONObject;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 16
    const/4 v0, 0x0

    return v0

    .line 19
    :cond_8
    iget-object v0, p0, Lcom/adobe/mobile/MessageTemplatePii;->templateUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_22

    iget-object v0, p0, Lcom/adobe/mobile/MessageTemplatePii;->templateUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, "https"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_31

    .line 20
    :cond_22
    const-string v0, "Data Callback - Unable to create data callback %s, templateurl is empty or does not use https for request"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/MessageTemplatePii;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 21
    const/4 v0, 0x0

    return v0

    .line 24
    :cond_31
    const/4 v0, 0x1

    return v0
.end method

.method protected logPrefix()Ljava/lang/String;
    .registers 2

    .line 29
    const-string v0, "PII"

    return-object v0
.end method

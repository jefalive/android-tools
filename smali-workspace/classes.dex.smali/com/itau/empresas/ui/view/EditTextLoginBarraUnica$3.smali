.class Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$3;
.super Ljava/lang/Object;
.source "EditTextLoginBarraUnica.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->aposCarregar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;


# direct methods
.method constructor <init>(Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    .line 111
    iput-object p1, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$3;->this$0:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .registers 7
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .line 114
    if-eqz p3, :cond_a

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x42

    if-eq v1, v0, :cond_1d

    :cond_a
    const/4 v0, 0x6

    if-ne p2, v0, :cond_26

    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$3;->this$0:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    .line 115
    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->isValido()Z

    move-result v0

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$3;->this$0:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    # getter for: Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->listener:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;
    invoke-static {v0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->access$000(Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;)Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;

    move-result-object v0

    if-eqz v0, :cond_26

    .line 116
    :cond_1d
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$3;->this$0:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    # getter for: Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->listener:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;
    invoke-static {v0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->access$000(Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;)Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;->preencheuAgenciaContaOuOperador()V

    .line 118
    :cond_26
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$3;->this$0:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    .line 120
    .local v2, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$3;->this$0:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    iget-object v0, v0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 121
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$3;->this$0:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    iget-object v0, v0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->campoCodigoOperador:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 122
    const/4 v0, 0x0

    return v0
.end method

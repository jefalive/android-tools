.class Lorg/joda/money/DefaultCurrencyUnitDataProvider;
.super Lorg/joda/money/CurrencyUnitDataProvider;
.source "DefaultCurrencyUnitDataProvider.java"


# static fields
.field private static final REGEX_LINE:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 40
    const-string v0, "([A-Z]{3}),(-1|[0-9]{1,3}),(-1|[0-9]),([A-Z]*)#?.*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/joda/money/DefaultCurrencyUnitDataProvider;->REGEX_LINE:Ljava/util/regex/Pattern;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .line 37
    invoke-direct {p0}, Lorg/joda/money/CurrencyUnitDataProvider;-><init>()V

    return-void
.end method

.method private loadCurrenciesFromFile(Ljava/lang/String;Z)V
    .registers 18
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "isNecessary"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 61
    const/4 v3, 0x0

    .line 62
    .local v3, "in":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 64
    .local v4, "resultEx":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    move-object v3, v0

    .line 65
    if-nez v3, :cond_32

    if-eqz p2, :cond_32

    .line 66
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Data file "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_32
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_32} :catch_a5
    .catchall {:try_start_2 .. :try_end_32} :catchall_a8

    .line 67
    :cond_32
    if-nez v3, :cond_3d

    if-nez p2, :cond_3d

    .line 93
    if-eqz v3, :cond_3c

    .line 94
    nop

    .line 96
    .line 97
    .line 98
    .line 99
    .line 101
    .local v5, "ignored":Ljava/io/IOException;
    .end local v5    # "ignored":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    :cond_3c
    return-void

    .line 70
    :cond_3d
    :try_start_3d
    new-instance v5, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/InputStreamReader;

    const-string v1, "UTF-8"

    invoke-direct {v0, v3, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v5, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 72
    .local v5, "reader":Ljava/io/BufferedReader;
    :goto_49
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .local v6, "line":Ljava/lang/String;
    if-eqz v6, :cond_9e

    .line 73
    sget-object v0, Lorg/joda/money/DefaultCurrencyUnitDataProvider;->REGEX_LINE:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v6}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    .line 74
    .local v7, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v7}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_9c

    .line 75
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 76
    .local v8, "countryCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x4

    invoke-virtual {v7, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    .line 77
    .local v9, "codeStr":Ljava/lang/String;
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v10

    .line 78
    .local v10, "currencyCode":Ljava/lang/String;
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_74

    .line 79
    goto :goto_49

    .line 81
    :cond_74
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_75
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v11, v0, :cond_87

    .line 82
    add-int/lit8 v0, v11, 0x2

    invoke-virtual {v9, v11, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    add-int/lit8 v11, v11, 0x2

    goto :goto_75

    .line 84
    .end local v11    # "i":I
    :cond_87
    const/4 v0, 0x2

    invoke-virtual {v7, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    .line 85
    .local v11, "numericCode":I
    const/4 v0, 0x3

    invoke-virtual {v7, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 86
    .local v12, "digits":I
    invoke-virtual {p0, v10, v11, v12, v8}, Lorg/joda/money/DefaultCurrencyUnitDataProvider;->registerCurrency(Ljava/lang/String;IILjava/util/List;)V
    :try_end_9c
    .catch Ljava/lang/Exception; {:try_start_3d .. :try_end_9c} :catch_a5
    .catchall {:try_start_3d .. :try_end_9c} :catchall_a8

    .line 88
    .end local v7    # "matcher":Ljava/util/regex/Matcher;
    .end local v8    # "countryCodes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v8
    .end local v9    # "codeStr":Ljava/lang/String;
    .end local v10    # "currencyCode":Ljava/lang/String;
    .end local v11    # "numericCode":I
    .end local v12    # "digits":I
    :cond_9c
    goto/16 :goto_49

    .line 93
    .end local v5    # "reader":Ljava/io/BufferedReader;
    .end local v6    # "line":Ljava/lang/String;
    :cond_9e
    if-eqz v3, :cond_b7

    .line 94
    nop

    .line 96
    .line 97
    .line 98
    .line 99
    .line 101
    .local v5, "ignored":Ljava/io/IOException;
    .end local v5    # "ignored":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    goto :goto_b7

    .line 89
    :catch_a5
    move-exception v5

    .line 90
    .local v5, "ex":Ljava/lang/Exception;
    move-object v4, v5

    .line 91
    :try_start_a7
    throw v5
    :try_end_a8
    .catchall {:try_start_a7 .. :try_end_a8} :catchall_a8

    .line 93
    .end local v5    # "ex":Ljava/lang/Exception;
    :catchall_a8
    move-exception v13

    if-eqz v3, :cond_b6

    .line 94
    if-eqz v4, :cond_b3

    .line 96
    :try_start_ad
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_b0
    .catch Ljava/io/IOException; {:try_start_ad .. :try_end_b0} :catch_b1

    .line 99
    goto :goto_b6

    .line 97
    :catch_b1
    move-exception v14

    .line 98
    .local v14, "ignored":Ljava/io/IOException;
    throw v4

    .line 101
    .end local v14    # "ignored":Ljava/io/IOException;
    :cond_b3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    :cond_b6
    :goto_b6
    throw v13

    .line 105
    :cond_b7
    :goto_b7
    return-void
.end method


# virtual methods
.method protected registerCurrencies()V
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 49
    const-string v0, "/org/joda/money/MoneyData.csv"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lorg/joda/money/DefaultCurrencyUnitDataProvider;->loadCurrenciesFromFile(Ljava/lang/String;Z)V

    .line 50
    const-string v0, "/org/joda/money/MoneyDataExtension.csv"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/joda/money/DefaultCurrencyUnitDataProvider;->loadCurrenciesFromFile(Ljava/lang/String;Z)V

    .line 51
    return-void
.end method

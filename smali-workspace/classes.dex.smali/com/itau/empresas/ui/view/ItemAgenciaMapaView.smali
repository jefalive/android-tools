.class public Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;
.super Landroid/widget/LinearLayout;
.source "ItemAgenciaMapaView.java"


# instance fields
.field imgCaixaExclusivo:Landroid/widget/ImageView;

.field imgHorarioDiferente:Landroid/widget/ImageView;

.field llDetalhesAgencia:Landroid/widget/LinearLayout;

.field llItemAgenciaMapa:Landroid/widget/LinearLayout;

.field textoAtendimentoEmpresas:Landroid/widget/TextView;

.field textoBairroAgencia:Landroid/widget/TextView;

.field textoEnderecoAgencia:Landroid/widget/TextView;

.field textoHorarioAtendimento:Landroid/widget/TextView;

.field textoNomeAgencia:Landroid/widget/TextView;

.field textoServicos:Landroid/widget/TextView;

.field textoTelefone:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 32
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 40
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    return-void
.end method


# virtual methods
.method public ajustaDados(Lcom/itau/empresas/api/model/AgenciaVO;)Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;
    .registers 6
    .param p1, "agencia"    # Lcom/itau/empresas/api/model/AgenciaVO;

    .line 64
    const/4 v3, 0x0

    .line 66
    .local v3, "servicos":Z
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;->textoNomeAgencia:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/AgenciaVO;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/AgenciaVO;->getNome()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;->textoEnderecoAgencia:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/AgenciaVO;->getEndereco()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;->textoBairroAgencia:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/AgenciaVO;->getComplementoEndereco()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;->textoTelefone:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/AgenciaVO;->getTelefone()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;->textoHorarioAtendimento:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/AgenciaVO;->getHorario()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/AgenciaVO;->getCaixaExclusivo()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_63

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/AgenciaVO;->getCaixaExclusivo()Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_63

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;->imgCaixaExclusivo:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 74
    const/4 v3, 0x1

    goto :goto_6a

    .line 76
    :cond_63
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;->imgCaixaExclusivo:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 79
    :goto_6a
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/AgenciaVO;->getHorarioDiferenciado()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_78

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;->imgHorarioDiferente:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 81
    const/4 v3, 0x1

    goto :goto_7f

    .line 83
    :cond_78
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;->imgHorarioDiferente:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 86
    :goto_7f
    if-eqz v3, :cond_88

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;->textoServicos:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_8f

    .line 89
    :cond_88
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;->textoServicos:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 92
    :goto_8f
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/AgenciaVO;->getAtendePJ()Z

    move-result v0

    if-eqz v0, :cond_9c

    .line 93
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;->textoAtendimentoEmpresas:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_a3

    .line 95
    :cond_9c
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;->textoAtendimentoEmpresas:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 98
    :goto_a3
    return-object p0
.end method

.method carregaView()V
    .registers 3

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;->llItemAgenciaMapa:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView$1;-><init>(Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    return-void
.end method

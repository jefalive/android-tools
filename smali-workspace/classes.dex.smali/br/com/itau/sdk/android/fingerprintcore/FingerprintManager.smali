.class public interface abstract Lbr/com/itau/sdk/android/fingerprintcore/FingerprintManager;
.super Ljava/lang/Object;
.source "FingerprintManager.java"


# virtual methods
.method public abstract authenticate(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;)Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCanceller;
.end method

.method public abstract isAvailable()Z
.end method

.method public abstract isUnavailable()Z
.end method

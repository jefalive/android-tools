.class Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContasFilter;
.super Landroid/widget/Filter;
.source "AreaUsuarioSelecaoContaAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContasFilter"
.end annotation


# instance fields
.field private contas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/ContaOperadorVO;>;"
        }
    .end annotation
.end field

.field private contasFiltrada:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/ContaOperadorVO;>;"
        }
    .end annotation
.end field

.field private dataset:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

.field final synthetic this$0:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;


# direct methods
.method public constructor <init>(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;Ljava/util/List;)V
    .registers 5
    .param p2, "adapter"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;
    .param p3, "lista"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;Ljava/util/List<Lcom/itau/empresas/api/model/ContaOperadorVO;>;)V"
        }
    .end annotation

    .line 199
    iput-object p1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContasFilter;->this$0:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

    .line 200
    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    .line 201
    iput-object p2, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContasFilter;->dataset:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

    .line 202
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0, p3}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContasFilter;->contas:Ljava/util/List;

    .line 203
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContasFilter;->contasFiltrada:Ljava/util/List;

    .line 205
    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .registers 8
    .param p1, "charSequence"    # Ljava/lang/CharSequence;

    .line 209
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContasFilter;->contasFiltrada:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 211
    new-instance v2, Landroid/widget/Filter$FilterResults;

    invoke-direct {v2}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 213
    .local v2, "filterResults":Landroid/widget/Filter$FilterResults;
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_18

    .line 214
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContasFilter;->contasFiltrada:Ljava/util/List;

    iget-object v1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContasFilter;->contas:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_67

    .line 216
    :cond_18
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContasFilter;->contas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_67

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 217
    .local v4, "conta":Lcom/itau/empresas/api/model/ContaOperadorVO;
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 219
    .local v5, "textoDaBusca":Ljava/lang/String;
    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getApelido()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_61

    .line 220
    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_61

    .line 221
    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_66

    .line 222
    :cond_61
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContasFilter;->contasFiltrada:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    .end local v4    # "conta":Lcom/itau/empresas/api/model/ContaOperadorVO;
    .end local v5    # "textoDaBusca":Ljava/lang/String;
    :cond_66
    goto :goto_1e

    .line 227
    :cond_67
    :goto_67
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContasFilter;->contasFiltrada:Ljava/util/List;

    iput-object v0, v2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 228
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContasFilter;->contasFiltrada:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v2, Landroid/widget/Filter$FilterResults;->count:I

    .line 230
    return-object v2
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .registers 5
    .param p1, "charSequence"    # Ljava/lang/CharSequence;
    .param p2, "filterResults"    # Landroid/widget/Filter$FilterResults;

    .line 235
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContasFilter;->dataset:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

    iget-object v1, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v1, Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->setListaDeContas(Ljava/util/List;)V

    .line 236
    return-void
.end method

.class public final Lcom/google/android/gms/internal/zzmv;
.super Ljava/lang/Object;


# static fields
.field private static zzaob:Landroid/content/IntentFilter;

.field private static zzaoc:J

.field private static zzaod:F


# direct methods
.method static constructor <clinit>()V
    .registers 2

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/zzmv;->zzaob:Landroid/content/IntentFilter;

    const/high16 v0, 0x7fc00000    # NaNf

    sput v0, Lcom/google/android/gms/internal/zzmv;->zzaod:F

    return-void
.end method

.method public static zzax(Landroid/content/Context;)I
    .registers 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x14
    .end annotation

    if-eqz p0, :cond_8

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_a

    :cond_8
    const/4 v0, -0x1

    return v0

    :cond_a
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/internal/zzmv;->zzaob:Landroid/content/IntentFilter;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v3

    if-nez v3, :cond_19

    const/4 v4, 0x0

    goto :goto_20

    :cond_19
    const-string v0, "plugged"

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    :goto_20
    and-int/lit8 v0, v4, 0x7

    if-eqz v0, :cond_26

    const/4 v5, 0x1

    goto :goto_27

    :cond_26
    const/4 v5, 0x0

    :goto_27
    const-string v0, "power"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/os/PowerManager;

    if-nez v7, :cond_34

    const/4 v0, -0x1

    return v0

    :cond_34
    invoke-static {}, Lcom/google/android/gms/internal/zzne;->zzsl()Z

    move-result v0

    if-eqz v0, :cond_3f

    invoke-virtual {v7}, Landroid/os/PowerManager;->isInteractive()Z

    move-result v6

    goto :goto_43

    :cond_3f
    invoke-virtual {v7}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v6

    :goto_43
    if-eqz v6, :cond_47

    const/4 v0, 0x1

    goto :goto_48

    :cond_47
    const/4 v0, 0x0

    :goto_48
    shl-int/lit8 v0, v0, 0x1

    if-eqz v5, :cond_4e

    const/4 v1, 0x1

    goto :goto_4f

    :cond_4e
    const/4 v1, 0x0

    :goto_4f
    or-int/2addr v0, v1

    return v0
.end method

.method public static declared-synchronized zzay(Landroid/content/Context;)F
    .registers 9

    const-class v7, Lcom/google/android/gms/internal/zzmv;

    monitor-enter v7

    :try_start_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sget-wide v2, Lcom/google/android/gms/internal/zzmv;->zzaoc:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xea60

    cmp-long v0, v0, v2

    if-gez v0, :cond_1d

    sget v0, Lcom/google/android/gms/internal/zzmv;->zzaod:F

    const/high16 v1, 0x7fc00000    # NaNf

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1d

    sget v0, Lcom/google/android/gms/internal/zzmv;->zzaod:F

    monitor-exit v7

    return v0

    :cond_1d
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/internal/zzmv;->zzaob:Landroid/content/IntentFilter;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v4

    if-eqz v4, :cond_3d

    const-string v0, "level"

    const/4 v1, -0x1

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const-string v0, "scale"

    const/4 v1, -0x1

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    int-to-float v0, v5

    int-to-float v1, v6

    div-float/2addr v0, v1

    sput v0, Lcom/google/android/gms/internal/zzmv;->zzaod:F

    :cond_3d
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/internal/zzmv;->zzaoc:J

    sget v0, Lcom/google/android/gms/internal/zzmv;->zzaod:F
    :try_end_45
    .catchall {:try_start_3 .. :try_end_45} :catchall_47

    monitor-exit v7

    return v0

    :catchall_47
    move-exception p0

    monitor-exit v7

    throw p0
.end method

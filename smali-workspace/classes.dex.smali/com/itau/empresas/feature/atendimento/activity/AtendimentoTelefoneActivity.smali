.class public Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "AtendimentoTelefoneActivity.java"


# instance fields
.field controller:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

.field private listaItens:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
        }
    .end annotation
.end field

.field listaTelefones:Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;

.field private listaTitulo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field private opkeys:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field root:Landroid/widget/LinearLayout;

.field private telefoneExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 36
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private configuraListaTelefones()V
    .registers 6

    .line 72
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->listaTelefones:Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;

    .line 73
    const v2, 0x7f030154

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/view/ViewGroup;

    .line 75
    .local v4, "header":Landroid/view/ViewGroup;
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->listaTelefones:Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v4, v1, v2}, Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 76
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->listaTelefones:Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->telefoneExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->listaTelefones:Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;

    .line 78
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c012f

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 77
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;->setChildDivider(Landroid/graphics/drawable/Drawable;)V

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->listaTelefones:Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;

    new-instance v1, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity$1;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;)V

    .line 81
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;->setOnGroupExpandListener(Landroid/widget/ExpandableListView$OnGroupExpandListener;)V

    .line 91
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 65
    const v1, 0x7f070716

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 68
    return-void
.end method

.method public static intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity_$IntentBuilder_;
    .registers 2
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;

    .line 135
    new-instance v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity_$IntentBuilder_;-><init>(Landroid/support/v4/app/Fragment;)V

    return-object v0
.end method


# virtual methods
.method carregaElementosDeTela()V
    .registers 5

    .line 53
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->constroiToolbar()V

    .line 54
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/AtendimentoUtil;->preparaListTelefones(Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->listaItens:Ljava/util/HashMap;

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->listaTitulo:Ljava/util/List;

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->listaTitulo:Ljava/util/List;

    iget-object v1, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->listaItens:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 57
    new-instance v0, Lcom/itau/empresas/adapter/PadraoExpandableListAdapter;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->listaTitulo:Ljava/util/List;

    iget-object v3, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->listaItens:Ljava/util/HashMap;

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/adapter/PadraoExpandableListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/util/HashMap;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->telefoneExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    .line 59
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->configuraListaTelefones()V

    .line 60
    return-void
.end method

.method public hitAnalytics()Ljava/lang/String;
    .registers 2

    .line 119
    const v0, 0x7f070716

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;

    .line 94
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 95
    return-void

    .line 98
    :cond_7
    const-string v0, "ItauEmpresas"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Erro na Chamada:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 100
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->escondeDialogoDeProgresso()V

    .line 101
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->root:Landroid/widget/LinearLayout;

    const v1, 0x7f0701ef

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 103
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 6
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 106
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 107
    return-void

    .line 109
    :cond_7
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v3

    .line 110
    .local v3, "exception":Lbr/com/itau/sdk/android/core/exception/BackendException;
    const-string v0, "ItauEmpresas"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Erro na Chamada:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 112
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->escondeDialogoDeProgresso()V

    .line 113
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 114
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 115
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 124
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->opkeys:Ljava/util/List;

    if-nez v0, :cond_b

    .line 125
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->opkeys:Ljava/util/List;

    .line 127
    :cond_b
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoTelefoneActivity;->opkeys:Ljava/util/List;

    return-object v0
.end method

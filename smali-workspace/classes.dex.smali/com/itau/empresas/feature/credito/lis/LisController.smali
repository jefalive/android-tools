.class public Lcom/itau/empresas/feature/credito/lis/LisController;
.super Lcom/itau/empresas/controller/BaseController;
.source "LisController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/itau/empresas/controller/BaseController<Lcom/itau/empresas/api/Api;>;"
    }
.end annotation


# instance fields
.field app:Lcom/itau/empresas/CustomApplication;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 18
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/credito/lis/LisController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/LisController;

    .line 18
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/LisController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 18
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/lis/LisController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/itau/empresas/feature/credito/lis/LisController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/LisController;

    .line 18
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/LisController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 18
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/lis/LisController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/credito/lis/LisController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/LisController;

    .line 18
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/LisController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 18
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/lis/LisController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$400(Lcom/itau/empresas/feature/credito/lis/LisController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/LisController;

    .line 18
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/LisController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 18
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/lis/LisController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$600(Lcom/itau/empresas/feature/credito/lis/LisController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/LisController;

    .line 18
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/LisController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 18
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/lis/LisController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$800(Lcom/itau/empresas/feature/credito/lis/LisController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/LisController;

    .line 18
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/LisController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 18
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/lis/LisController;->post(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public buscaConsultaDetalha(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "idProduto"    # Ljava/lang/String;
    .param p2, "cpfRepresentante"    # Ljava/lang/String;

    .line 84
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/LisController$5;

    invoke-direct {v0, p0, p1, p2}, Lcom/itau/empresas/feature/credito/lis/LisController$5;-><init>(Lcom/itau/empresas/feature/credito/lis/LisController;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 98
    return-void
.end method

.method public buscaOfertaChequeEspecial(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "cpfRepresentante"    # Ljava/lang/String;
    .param p2, "codigoProduto"    # Ljava/lang/String;
    .param p3, "tipoOperacao"    # Ljava/lang/String;

    .line 44
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/LisController$2;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/itau/empresas/feature/credito/lis/LisController$2;-><init>(Lcom/itau/empresas/feature/credito/lis/LisController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 57
    return-void
.end method

.method public buscaOfertaResumo(Ljava/lang/String;)V
    .registers 3
    .param p1, "cpfRepresentante"    # Ljava/lang/String;

    .line 27
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/LisController$1;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/feature/credito/lis/LisController$1;-><init>(Lcom/itau/empresas/feature/credito/lis/LisController;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 38
    return-void
.end method

.method public consultaContratadaComprovante(Ljava/lang/String;)V
    .registers 3
    .param p1, "idComprovante"    # Ljava/lang/String;

    .line 102
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/LisController$6;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/feature/credito/lis/LisController$6;-><init>(Lcom/itau/empresas/feature/credito/lis/LisController;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 111
    return-void
.end method

.method public efetuaContratacao(Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;Ljava/lang/String;)V
    .registers 4
    .param p1, "envioVO"    # Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;
    .param p2, "produtoLis"    # Ljava/lang/String;

    .line 74
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/LisController$4;

    invoke-direct {v0, p0, p2, p1}, Lcom/itau/empresas/feature/credito/lis/LisController$4;-><init>(Lcom/itau/empresas/feature/credito/lis/LisController;Ljava/lang/String;Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 80
    return-void
.end method

.method public efetuaSimulacao(Ljava/lang/String;Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;Ljava/lang/String;)V
    .registers 5
    .param p1, "tipoOperacao"    # Ljava/lang/String;
    .param p2, "envioVO"    # Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;
    .param p3, "produtoLis"    # Ljava/lang/String;

    .line 62
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/LisController$3;

    invoke-direct {v0, p0, p3, p1, p2}, Lcom/itau/empresas/feature/credito/lis/LisController$3;-><init>(Lcom/itau/empresas/feature/credito/lis/LisController;Ljava/lang/String;Ljava/lang/String;Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 70
    return-void
.end method

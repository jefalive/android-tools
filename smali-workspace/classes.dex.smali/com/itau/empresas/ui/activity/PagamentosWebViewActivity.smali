.class public Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;
.super Lcom/itau/empresas/ui/activity/WebViewActivity;
.source "PagamentosWebViewActivity.java"


# instance fields
.field codigoBarra:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 14
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;-><init>()V

    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 48
    new-instance v0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method protected configuraParametrosWebView()V
    .registers 6

    .line 21
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/WebViewActivity;->configuraParametrosWebView()V

    .line 24
    sget-object v0, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->CONCESSIONARIA:Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->codigoBarra:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->isTipo(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_73

    .line 25
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    const-string v1, "codbar1"

    sget-object v2, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->CONCESSIONARIA:Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    iget-object v3, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->codigoBarra:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->extraiBloco(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->addParameter(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 26
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    const-string v1, "codbar2"

    sget-object v2, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->CONCESSIONARIA:Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    iget-object v3, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->codigoBarra:Ljava/lang/String;

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->extraiBloco(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->addParameter(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 27
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    const-string v1, "codbar3"

    sget-object v2, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->CONCESSIONARIA:Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    iget-object v3, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->codigoBarra:Ljava/lang/String;

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4}, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->extraiBloco(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->addParameter(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 28
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    const-string v1, "codbar4"

    sget-object v2, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->CONCESSIONARIA:Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    iget-object v3, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->codigoBarra:Ljava/lang/String;

    const/4 v4, 0x4

    invoke-virtual {v2, v3, v4}, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->extraiBloco(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->addParameter(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 29
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    const-string v1, "codbar5"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->addParameter(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    const-string v1, "codbar6"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->addParameter(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 31
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    const-string v1, "codbar7"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->addParameter(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 32
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    const-string v1, "codbar8"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->addParameter(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    goto/16 :goto_fe

    .line 35
    :cond_73
    sget-object v0, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->NORMAL:Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->codigoBarra:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->isTipo(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_fe

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    const-string v1, "codbar1"

    sget-object v2, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->NORMAL:Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    iget-object v3, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->codigoBarra:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->extraiBloco(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->addParameter(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 37
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    const-string v1, "codbar2"

    sget-object v2, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->NORMAL:Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    iget-object v3, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->codigoBarra:Ljava/lang/String;

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->extraiBloco(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->addParameter(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    const-string v1, "codbar3"

    sget-object v2, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->NORMAL:Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    iget-object v3, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->codigoBarra:Ljava/lang/String;

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4}, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->extraiBloco(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->addParameter(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 39
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    const-string v1, "codbar4"

    sget-object v2, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->NORMAL:Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    iget-object v3, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->codigoBarra:Ljava/lang/String;

    const/4 v4, 0x4

    invoke-virtual {v2, v3, v4}, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->extraiBloco(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->addParameter(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 40
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    const-string v1, "codbar5"

    sget-object v2, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->NORMAL:Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    iget-object v3, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->codigoBarra:Ljava/lang/String;

    const/4 v4, 0x5

    invoke-virtual {v2, v3, v4}, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->extraiBloco(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->addParameter(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    const-string v1, "codbar6"

    sget-object v2, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->NORMAL:Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    iget-object v3, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->codigoBarra:Ljava/lang/String;

    const/4 v4, 0x6

    invoke-virtual {v2, v3, v4}, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->extraiBloco(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->addParameter(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 42
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    const-string v1, "codbar7"

    sget-object v2, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->NORMAL:Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    iget-object v3, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->codigoBarra:Ljava/lang/String;

    const/4 v4, 0x7

    invoke-virtual {v2, v3, v4}, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->extraiBloco(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->addParameter(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 43
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    const-string v1, "codbar8"

    sget-object v2, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->NORMAL:Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    iget-object v3, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->codigoBarra:Ljava/lang/String;

    const/16 v4, 0x8

    invoke-virtual {v2, v3, v4}, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->extraiBloco(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->addParameter(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 45
    :cond_fe
    :goto_fe
    return-void
.end method

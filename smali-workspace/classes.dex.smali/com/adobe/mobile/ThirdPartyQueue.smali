.class Lcom/adobe/mobile/ThirdPartyQueue;
.super Lcom/adobe/mobile/AbstractHitDatabase;
.source "ThirdPartyQueue.java"


# static fields
.field private static final _hitsSelectedColumns:[Ljava/lang/String;

.field private static _instance:Lcom/adobe/mobile/ThirdPartyQueue;

.field private static final _instanceMutex:Ljava/lang/Object;


# instance fields
.field private _preparedInsertStatement:Landroid/database/sqlite/SQLiteStatement;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 35
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "ID"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "URL"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "POSTBODY"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "POSTTYPE"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "TIMESTAMP"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "TIMEOUT"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sput-object v0, Lcom/adobe/mobile/ThirdPartyQueue;->_hitsSelectedColumns:[Ljava/lang/String;

    .line 53
    const/4 v0, 0x0

    sput-object v0, Lcom/adobe/mobile/ThirdPartyQueue;->_instance:Lcom/adobe/mobile/ThirdPartyQueue;

    .line 54
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/adobe/mobile/ThirdPartyQueue;->_instanceMutex:Ljava/lang/Object;

    return-void
.end method

.method protected constructor <init>()V
    .registers 4

    .line 68
    invoke-direct {p0}, Lcom/adobe/mobile/AbstractHitDatabase;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue;->_preparedInsertStatement:Landroid/database/sqlite/SQLiteStatement;

    .line 69
    invoke-virtual {p0}, Lcom/adobe/mobile/ThirdPartyQueue;->fileName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue;->fileName:Ljava/lang/String;

    .line 70
    invoke-virtual {p0}, Lcom/adobe/mobile/ThirdPartyQueue;->logPrefix()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue;->logPrefix:Ljava/lang/String;

    .line 71
    const-string v0, "CREATE TABLE IF NOT EXISTS HITS (ID INTEGER PRIMARY KEY AUTOINCREMENT, URL TEXT, POSTBODY TEXT, POSTTYPE TEXT, TIMESTAMP INTEGER, TIMEOUT INTEGER)"

    iput-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue;->dbCreateStatement:Ljava/lang/String;

    .line 72
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/adobe/mobile/ThirdPartyQueue;->lastHitTimestamp:J

    .line 74
    new-instance v0, Ljava/io/File;

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCacheDirectory()Ljava/io/File;

    move-result-object v1

    iget-object v2, p0, Lcom/adobe/mobile/ThirdPartyQueue;->fileName:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/adobe/mobile/ThirdPartyQueue;->initDatabaseBacking(Ljava/io/File;)V

    .line 75
    invoke-virtual {p0}, Lcom/adobe/mobile/ThirdPartyQueue;->getTrackingQueueSize()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/adobe/mobile/ThirdPartyQueue;->numberOfUnsentHits:J

    .line 76
    return-void
.end method

.method protected static sharedInstance()Lcom/adobe/mobile/ThirdPartyQueue;
    .registers 3

    .line 56
    sget-object v1, Lcom/adobe/mobile/ThirdPartyQueue;->_instanceMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 57
    :try_start_3
    sget-object v0, Lcom/adobe/mobile/ThirdPartyQueue;->_instance:Lcom/adobe/mobile/ThirdPartyQueue;

    if-nez v0, :cond_e

    .line 58
    new-instance v0, Lcom/adobe/mobile/ThirdPartyQueue;

    invoke-direct {v0}, Lcom/adobe/mobile/ThirdPartyQueue;-><init>()V

    sput-object v0, Lcom/adobe/mobile/ThirdPartyQueue;->_instance:Lcom/adobe/mobile/ThirdPartyQueue;

    .line 61
    :cond_e
    sget-object v0, Lcom/adobe/mobile/ThirdPartyQueue;->_instance:Lcom/adobe/mobile/ThirdPartyQueue;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    monitor-exit v1

    return-object v0

    .line 62
    :catchall_12
    move-exception v2

    monitor-exit v1

    throw v2
.end method


# virtual methods
.method protected fileName()Ljava/lang/String;
    .registers 2

    .line 145
    const-string v0, "ADBMobile3rdPartyDataCache.sqlite"

    return-object v0
.end method

.method protected getWorker()Lcom/adobe/mobile/ThirdPartyQueue;
    .registers 2

    .line 208
    invoke-static {}, Lcom/adobe/mobile/ThirdPartyQueue;->sharedInstance()Lcom/adobe/mobile/ThirdPartyQueue;

    move-result-object v0

    return-object v0
.end method

.method protected logPrefix()Ljava/lang/String;
    .registers 2

    .line 141
    const-string v0, "External Callback"

    return-object v0
.end method

.method protected prepareStatements()V
    .registers 6

    .line 154
    :try_start_0
    iget-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "INSERT INTO HITS (URL, POSTBODY, POSTTYPE, TIMESTAMP, TIMEOUT) VALUES (?, ?, ?, ?, ?)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue;->_preparedInsertStatement:Landroid/database/sqlite/SQLiteStatement;
    :try_end_a
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_a} :catch_b
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_a} :catch_21
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_a} :catch_37

    .line 164
    goto :goto_4c

    .line 156
    :catch_b
    move-exception v4

    .line 157
    .local v4, "x":Ljava/lang/NullPointerException;
    const-string v0, "%s - Unable to create database due to an invalid path (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/ThirdPartyQueue;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v4}, Ljava/lang/NullPointerException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 164
    .end local v4    # "x":Ljava/lang/NullPointerException;
    goto :goto_4c

    .line 159
    :catch_21
    move-exception v4

    .line 160
    .local v4, "x":Landroid/database/SQLException;
    const-string v0, "%s - Unable to create database due to a sql error (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/ThirdPartyQueue;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v4}, Landroid/database/SQLException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 164
    .end local v4    # "x":Landroid/database/SQLException;
    goto :goto_4c

    .line 162
    :catch_37
    move-exception v4

    .line 163
    .local v4, "x":Ljava/lang/Exception;
    const-string v0, "%s - Unable to create database due to an unexpected error (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/ThirdPartyQueue;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v4}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 165
    .end local v4    # "x":Ljava/lang/Exception;
    :goto_4c
    return-void
.end method

.method protected queue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V
    .registers 16
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "postBody"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;
    .param p4, "timestamp"    # J
    .param p6, "timeout"    # J

    .line 82
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v4

    .line 83
    .local v4, "mobileConfigInstance":Lcom/adobe/mobile/MobileConfig;
    if-nez v4, :cond_14

    .line 84
    const-string v0, "%s - Cannot send hit, MobileConfig is null (this really shouldn\'t happen)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/ThirdPartyQueue;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    return-void

    .line 88
    :cond_14
    invoke-virtual {v4}, Lcom/adobe/mobile/MobileConfig;->getPrivacyStatus()Lcom/adobe/mobile/MobilePrivacyStatus;

    move-result-object v0

    sget-object v1, Lcom/adobe/mobile/MobilePrivacyStatus;->MOBILE_PRIVACY_STATUS_OPT_OUT:Lcom/adobe/mobile/MobilePrivacyStatus;

    if-ne v0, v1, :cond_2a

    .line 89
    const-string v0, "%s - Ignoring hit due to privacy status being opted out"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/ThirdPartyQueue;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    return-void

    .line 94
    :cond_2a
    iget-object v5, p0, Lcom/adobe/mobile/ThirdPartyQueue;->dbMutex:Ljava/lang/Object;

    monitor-enter v5

    .line 97
    :try_start_2d
    iget-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue;->_preparedInsertStatement:Landroid/database/sqlite/SQLiteStatement;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 100
    if-eqz p2, :cond_42

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_42

    .line 101
    iget-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue;->_preparedInsertStatement:Landroid/database/sqlite/SQLiteStatement;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_48

    .line 104
    :cond_42
    iget-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue;->_preparedInsertStatement:Landroid/database/sqlite/SQLiteStatement;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 107
    :goto_48
    if-eqz p3, :cond_57

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_57

    .line 108
    iget-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue;->_preparedInsertStatement:Landroid/database/sqlite/SQLiteStatement;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p3}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    goto :goto_5d

    .line 111
    :cond_57
    iget-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue;->_preparedInsertStatement:Landroid/database/sqlite/SQLiteStatement;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    .line 114
    :goto_5d
    iget-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue;->_preparedInsertStatement:Landroid/database/sqlite/SQLiteStatement;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p4, p5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 116
    iget-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue;->_preparedInsertStatement:Landroid/database/sqlite/SQLiteStatement;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p6, p7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 117
    iget-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue;->_preparedInsertStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 118
    iget-wide v0, p0, Lcom/adobe/mobile/ThirdPartyQueue;->numberOfUnsentHits:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/adobe/mobile/ThirdPartyQueue;->numberOfUnsentHits:J

    .line 121
    iget-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue;->_preparedInsertStatement:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V
    :try_end_7a
    .catch Landroid/database/SQLException; {:try_start_2d .. :try_end_7a} :catch_7b
    .catch Ljava/lang/Exception; {:try_start_2d .. :try_end_7a} :catch_90
    .catchall {:try_start_2d .. :try_end_7a} :catchall_a6

    .line 130
    goto :goto_a4

    .line 123
    :catch_7b
    move-exception v6

    .line 124
    .local v6, "e":Landroid/database/SQLException;
    const-string v0, "%s - Unable to insert url (%s)"

    const/4 v1, 0x2

    :try_start_7f
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/ThirdPartyQueue;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 125
    invoke-virtual {p0, v6}, Lcom/adobe/mobile/ThirdPartyQueue;->resetDatabase(Ljava/lang/Exception;)V

    .line 130
    .end local v6    # "e":Landroid/database/SQLException;
    goto :goto_a4

    .line 127
    :catch_90
    move-exception v6

    .line 128
    .local v6, "e":Ljava/lang/Exception;
    const-string v0, "%s - Unknown error while inserting url (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/ThirdPartyQueue;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 129
    invoke-virtual {p0, v6}, Lcom/adobe/mobile/ThirdPartyQueue;->resetDatabase(Ljava/lang/Exception;)V
    :try_end_a4
    .catchall {:try_start_7f .. :try_end_a4} :catchall_a6

    .line 131
    .end local v6    # "e":Ljava/lang/Exception;
    :goto_a4
    monitor-exit v5

    goto :goto_a9

    :catchall_a6
    move-exception v7

    monitor-exit v5

    throw v7

    .line 133
    :goto_a9
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/adobe/mobile/ThirdPartyQueue;->kick(Z)V

    .line 134
    return-void
.end method

.method protected selectOldestHit()Lcom/adobe/mobile/AbstractHitDatabase$Hit;
    .registers 16

    .line 169
    const/4 v9, 0x0

    .line 171
    .local v9, "hit":Lcom/adobe/mobile/AbstractHitDatabase$Hit;
    iget-object v10, p0, Lcom/adobe/mobile/ThirdPartyQueue;->dbMutex:Ljava/lang/Object;

    monitor-enter v10

    .line 172
    const/4 v11, 0x0

    .line 176
    .local v11, "cursor":Landroid/database/Cursor;
    :try_start_5
    iget-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "HITS"

    sget-object v2, Lcom/adobe/mobile/ThirdPartyQueue;->_hitsSelectedColumns:[Ljava/lang/String;

    const-string v7, "ID ASC"

    const-string v8, "1"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    move-object v11, v0

    .line 178
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 180
    new-instance v0, Lcom/adobe/mobile/AbstractHitDatabase$Hit;

    invoke-direct {v0}, Lcom/adobe/mobile/AbstractHitDatabase$Hit;-><init>()V

    move-object v9, v0

    .line 181
    const/4 v0, 0x0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->identifier:Ljava/lang/String;

    .line 182
    const/4 v0, 0x1

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->urlFragment:Ljava/lang/String;

    .line 183
    const/4 v0, 0x2

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->postBody:Ljava/lang/String;

    .line 184
    const/4 v0, 0x3

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->postType:Ljava/lang/String;

    .line 185
    const/4 v0, 0x4

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v9, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->timestamp:J

    .line 186
    const/4 v0, 0x5

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v9, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->timeout:I
    :try_end_4e
    .catch Landroid/database/SQLException; {:try_start_5 .. :try_end_4e} :catch_54
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_4e} :catch_6f
    .catchall {:try_start_5 .. :try_end_4e} :catchall_8a

    .line 198
    :cond_4e
    if-eqz v11, :cond_91

    .line 199
    :try_start_50
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_53
    .catchall {:try_start_50 .. :try_end_53} :catchall_93

    goto :goto_91

    .line 189
    :catch_54
    move-exception v12

    .line 191
    .local v12, "e":Landroid/database/SQLException;
    const-string v0, "%s - Unable to read from database (%s)"

    const/4 v1, 0x2

    :try_start_58
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/ThirdPartyQueue;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v12}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_69
    .catchall {:try_start_58 .. :try_end_69} :catchall_8a

    .line 198
    .end local v12    # "e":Landroid/database/SQLException;
    if-eqz v11, :cond_91

    .line 199
    :try_start_6b
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_6e
    .catchall {:try_start_6b .. :try_end_6e} :catchall_93

    goto :goto_91

    .line 193
    :catch_6f
    move-exception v12

    .line 195
    .local v12, "e":Ljava/lang/Exception;
    const-string v0, "%s - Unknown error reading from database (%s)"

    const/4 v1, 0x2

    :try_start_73
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/ThirdPartyQueue;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v12}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_84
    .catchall {:try_start_73 .. :try_end_84} :catchall_8a

    .line 198
    .end local v12    # "e":Ljava/lang/Exception;
    if-eqz v11, :cond_91

    .line 199
    :try_start_86
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_89
    .catchall {:try_start_86 .. :try_end_89} :catchall_93

    goto :goto_91

    .line 198
    :catchall_8a
    move-exception v13

    if-eqz v11, :cond_90

    .line 199
    :try_start_8d
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_90
    throw v13
    :try_end_91
    .catchall {:try_start_8d .. :try_end_91} :catchall_93

    .line 202
    .end local v11    # "cursor":Landroid/database/Cursor;
    :cond_91
    :goto_91
    monitor-exit v10

    goto :goto_96

    :catchall_93
    move-exception v14

    monitor-exit v10

    throw v14

    .line 204
    :goto_96
    return-object v9
.end method

.method protected workerThread()Ljava/lang/Runnable;
    .registers 3

    .line 215
    invoke-virtual {p0}, Lcom/adobe/mobile/ThirdPartyQueue;->getWorker()Lcom/adobe/mobile/ThirdPartyQueue;

    move-result-object v1

    .line 217
    .local v1, "worker":Lcom/adobe/mobile/ThirdPartyQueue;
    new-instance v0, Lcom/adobe/mobile/ThirdPartyQueue$1;

    invoke-direct {v0, p0, v1}, Lcom/adobe/mobile/ThirdPartyQueue$1;-><init>(Lcom/adobe/mobile/ThirdPartyQueue;Lcom/adobe/mobile/ThirdPartyQueue;)V

    return-object v0
.end method

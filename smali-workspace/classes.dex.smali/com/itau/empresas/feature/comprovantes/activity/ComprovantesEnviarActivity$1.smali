.class Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity$1;
.super Lbr/com/itau/widgets/material/validation/METValidator;
.source "ComprovantesEnviarActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->carregaElementosDaTela()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;Ljava/lang/String;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;
    .param p2, "arg0"    # Ljava/lang/String;

    .line 84
    iput-object p1, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity$1;->this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;

    invoke-direct {p0, p2}, Lbr/com/itau/widgets/material/validation/METValidator;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public isValid(Ljava/lang/CharSequence;Z)Z
    .registers 5
    .param p1, "charSequence"    # Ljava/lang/CharSequence;
    .param p2, "b"    # Z

    .line 87
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 88
    .local v1, "tamanho":I
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity$1;->this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->campoComentario:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->getMinCharacters()I

    move-result v0

    if-lt v1, v0, :cond_1a

    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity$1;->this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->campoComentario:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 90
    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->getMaxCharacters()I

    move-result v0

    if-gt v1, v0, :cond_1a

    const/4 v0, 0x1

    goto :goto_1b

    :cond_1a
    const/4 v0, 0x0

    .line 88
    :goto_1b
    return v0
.end method

.class public final Lbr/com/itau/textovoz/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final ActionBar:[I

.field public static final ActionBarLayout:[I

.field public static final ActionBarLayout_android_layout_gravity:I = 0x0

.field public static final ActionBar_background:I = 0xa

.field public static final ActionBar_backgroundSplit:I = 0xc

.field public static final ActionBar_backgroundStacked:I = 0xb

.field public static final ActionBar_contentInsetEnd:I = 0x15

.field public static final ActionBar_contentInsetEndWithActions:I = 0x19

.field public static final ActionBar_contentInsetLeft:I = 0x16

.field public static final ActionBar_contentInsetRight:I = 0x17

.field public static final ActionBar_contentInsetStart:I = 0x14

.field public static final ActionBar_contentInsetStartWithNavigation:I = 0x18

.field public static final ActionBar_customNavigationLayout:I = 0xd

.field public static final ActionBar_displayOptions:I = 0x3

.field public static final ActionBar_divider:I = 0x9

.field public static final ActionBar_elevation:I = 0x1a

.field public static final ActionBar_height:I = 0x0

.field public static final ActionBar_hideOnContentScroll:I = 0x13

.field public static final ActionBar_homeAsUpIndicator:I = 0x1c

.field public static final ActionBar_homeLayout:I = 0xe

.field public static final ActionBar_icon:I = 0x7

.field public static final ActionBar_indeterminateProgressStyle:I = 0x10

.field public static final ActionBar_itemPadding:I = 0x12

.field public static final ActionBar_logo:I = 0x8

.field public static final ActionBar_navigationMode:I = 0x2

.field public static final ActionBar_popupTheme:I = 0x1b

.field public static final ActionBar_progressBarPadding:I = 0x11

.field public static final ActionBar_progressBarStyle:I = 0xf

.field public static final ActionBar_subtitle:I = 0x4

.field public static final ActionBar_subtitleTextStyle:I = 0x6

.field public static final ActionBar_title:I = 0x1

.field public static final ActionBar_titleTextStyle:I = 0x5

.field public static final ActionMenuItemView:[I

.field public static final ActionMenuItemView_android_minWidth:I = 0x0

.field public static final ActionMenuView:[I

.field public static final ActionMode:[I

.field public static final ActionMode_background:I = 0x3

.field public static final ActionMode_backgroundSplit:I = 0x4

.field public static final ActionMode_closeItemLayout:I = 0x5

.field public static final ActionMode_height:I = 0x0

.field public static final ActionMode_subtitleTextStyle:I = 0x2

.field public static final ActionMode_titleTextStyle:I = 0x1

.field public static final ActivityChooserView:[I

.field public static final ActivityChooserView_expandActivityOverflowButtonDrawable:I = 0x1

.field public static final ActivityChooserView_initialActivityCount:I = 0x0

.field public static final AlertDialog:[I

.field public static final AlertDialog_android_layout:I = 0x0

.field public static final AlertDialog_buttonPanelSideLayout:I = 0x1

.field public static final AlertDialog_listItemLayout:I = 0x5

.field public static final AlertDialog_listLayout:I = 0x2

.field public static final AlertDialog_multiChoiceItemLayout:I = 0x3

.field public static final AlertDialog_singleChoiceItemLayout:I = 0x4

.field public static final AppBarLayout:[I

.field public static final AppBarLayoutStates:[I

.field public static final AppBarLayoutStates_state_collapsed:I = 0x0

.field public static final AppBarLayoutStates_state_collapsible:I = 0x1

.field public static final AppBarLayout_Layout:[I

.field public static final AppBarLayout_Layout_layout_scrollFlags:I = 0x0

.field public static final AppBarLayout_Layout_layout_scrollInterpolator:I = 0x1

.field public static final AppBarLayout_android_background:I = 0x0

.field public static final AppBarLayout_elevation:I = 0x1

.field public static final AppBarLayout_expanded:I = 0x2

.field public static final AppCompatImageView:[I

.field public static final AppCompatImageView_android_src:I = 0x0

.field public static final AppCompatImageView_srcCompat:I = 0x1

.field public static final AppCompatSeekBar:[I

.field public static final AppCompatSeekBar_android_thumb:I = 0x0

.field public static final AppCompatSeekBar_tickMark:I = 0x1

.field public static final AppCompatSeekBar_tickMarkTint:I = 0x2

.field public static final AppCompatSeekBar_tickMarkTintMode:I = 0x3

.field public static final AppCompatTextHelper:[I

.field public static final AppCompatTextHelper_android_drawableBottom:I = 0x2

.field public static final AppCompatTextHelper_android_drawableEnd:I = 0x6

.field public static final AppCompatTextHelper_android_drawableLeft:I = 0x3

.field public static final AppCompatTextHelper_android_drawableRight:I = 0x4

.field public static final AppCompatTextHelper_android_drawableStart:I = 0x5

.field public static final AppCompatTextHelper_android_drawableTop:I = 0x1

.field public static final AppCompatTextHelper_android_textAppearance:I = 0x0

.field public static final AppCompatTextView:[I

.field public static final AppCompatTextView_android_textAppearance:I = 0x0

.field public static final AppCompatTextView_textAllCaps:I = 0x1

.field public static final AppCompatTheme:[I

.field public static final AppCompatTheme_actionBarDivider:I = 0x17

.field public static final AppCompatTheme_actionBarItemBackground:I = 0x18

.field public static final AppCompatTheme_actionBarPopupTheme:I = 0x11

.field public static final AppCompatTheme_actionBarSize:I = 0x16

.field public static final AppCompatTheme_actionBarSplitStyle:I = 0x13

.field public static final AppCompatTheme_actionBarStyle:I = 0x12

.field public static final AppCompatTheme_actionBarTabBarStyle:I = 0xd

.field public static final AppCompatTheme_actionBarTabStyle:I = 0xc

.field public static final AppCompatTheme_actionBarTabTextStyle:I = 0xe

.field public static final AppCompatTheme_actionBarTheme:I = 0x14

.field public static final AppCompatTheme_actionBarWidgetTheme:I = 0x15

.field public static final AppCompatTheme_actionButtonStyle:I = 0x32

.field public static final AppCompatTheme_actionDropDownStyle:I = 0x2e

.field public static final AppCompatTheme_actionMenuTextAppearance:I = 0x19

.field public static final AppCompatTheme_actionMenuTextColor:I = 0x1a

.field public static final AppCompatTheme_actionModeBackground:I = 0x1d

.field public static final AppCompatTheme_actionModeCloseButtonStyle:I = 0x1c

.field public static final AppCompatTheme_actionModeCloseDrawable:I = 0x1f

.field public static final AppCompatTheme_actionModeCopyDrawable:I = 0x21

.field public static final AppCompatTheme_actionModeCutDrawable:I = 0x20

.field public static final AppCompatTheme_actionModeFindDrawable:I = 0x25

.field public static final AppCompatTheme_actionModePasteDrawable:I = 0x22

.field public static final AppCompatTheme_actionModePopupWindowStyle:I = 0x27

.field public static final AppCompatTheme_actionModeSelectAllDrawable:I = 0x23

.field public static final AppCompatTheme_actionModeShareDrawable:I = 0x24

.field public static final AppCompatTheme_actionModeSplitBackground:I = 0x1e

.field public static final AppCompatTheme_actionModeStyle:I = 0x1b

.field public static final AppCompatTheme_actionModeWebSearchDrawable:I = 0x26

.field public static final AppCompatTheme_actionOverflowButtonStyle:I = 0xf

.field public static final AppCompatTheme_actionOverflowMenuStyle:I = 0x10

.field public static final AppCompatTheme_activityChooserViewStyle:I = 0x3a

.field public static final AppCompatTheme_alertDialogButtonGroupStyle:I = 0x5e

.field public static final AppCompatTheme_alertDialogCenterButtons:I = 0x5f

.field public static final AppCompatTheme_alertDialogStyle:I = 0x5d

.field public static final AppCompatTheme_alertDialogTheme:I = 0x60

.field public static final AppCompatTheme_android_windowAnimationStyle:I = 0x1

.field public static final AppCompatTheme_android_windowIsFloating:I = 0x0

.field public static final AppCompatTheme_autoCompleteTextViewStyle:I = 0x65

.field public static final AppCompatTheme_borderlessButtonStyle:I = 0x37

.field public static final AppCompatTheme_buttonBarButtonStyle:I = 0x34

.field public static final AppCompatTheme_buttonBarNegativeButtonStyle:I = 0x63

.field public static final AppCompatTheme_buttonBarNeutralButtonStyle:I = 0x64

.field public static final AppCompatTheme_buttonBarPositiveButtonStyle:I = 0x62

.field public static final AppCompatTheme_buttonBarStyle:I = 0x33

.field public static final AppCompatTheme_buttonStyle:I = 0x66

.field public static final AppCompatTheme_buttonStyleSmall:I = 0x67

.field public static final AppCompatTheme_checkboxStyle:I = 0x68

.field public static final AppCompatTheme_checkedTextViewStyle:I = 0x69

.field public static final AppCompatTheme_colorAccent:I = 0x55

.field public static final AppCompatTheme_colorBackgroundFloating:I = 0x5c

.field public static final AppCompatTheme_colorButtonNormal:I = 0x59

.field public static final AppCompatTheme_colorControlActivated:I = 0x57

.field public static final AppCompatTheme_colorControlHighlight:I = 0x58

.field public static final AppCompatTheme_colorControlNormal:I = 0x56

.field public static final AppCompatTheme_colorPrimary:I = 0x53

.field public static final AppCompatTheme_colorPrimaryDark:I = 0x54

.field public static final AppCompatTheme_colorSwitchThumbNormal:I = 0x5a

.field public static final AppCompatTheme_controlBackground:I = 0x5b

.field public static final AppCompatTheme_dialogPreferredPadding:I = 0x2c

.field public static final AppCompatTheme_dialogTheme:I = 0x2b

.field public static final AppCompatTheme_dividerHorizontal:I = 0x39

.field public static final AppCompatTheme_dividerVertical:I = 0x38

.field public static final AppCompatTheme_dropDownListViewStyle:I = 0x4b

.field public static final AppCompatTheme_dropdownListPreferredItemHeight:I = 0x2f

.field public static final AppCompatTheme_editTextBackground:I = 0x40

.field public static final AppCompatTheme_editTextColor:I = 0x3f

.field public static final AppCompatTheme_editTextStyle:I = 0x6a

.field public static final AppCompatTheme_homeAsUpIndicator:I = 0x31

.field public static final AppCompatTheme_imageButtonStyle:I = 0x41

.field public static final AppCompatTheme_listChoiceBackgroundIndicator:I = 0x52

.field public static final AppCompatTheme_listDividerAlertDialog:I = 0x2d

.field public static final AppCompatTheme_listMenuViewStyle:I = 0x72

.field public static final AppCompatTheme_listPopupWindowStyle:I = 0x4c

.field public static final AppCompatTheme_listPreferredItemHeight:I = 0x46

.field public static final AppCompatTheme_listPreferredItemHeightLarge:I = 0x48

.field public static final AppCompatTheme_listPreferredItemHeightSmall:I = 0x47

.field public static final AppCompatTheme_listPreferredItemPaddingLeft:I = 0x49

.field public static final AppCompatTheme_listPreferredItemPaddingRight:I = 0x4a

.field public static final AppCompatTheme_panelBackground:I = 0x4f

.field public static final AppCompatTheme_panelMenuListTheme:I = 0x51

.field public static final AppCompatTheme_panelMenuListWidth:I = 0x50

.field public static final AppCompatTheme_popupMenuStyle:I = 0x3d

.field public static final AppCompatTheme_popupWindowStyle:I = 0x3e

.field public static final AppCompatTheme_radioButtonStyle:I = 0x6b

.field public static final AppCompatTheme_ratingBarStyle:I = 0x6c

.field public static final AppCompatTheme_ratingBarStyleIndicator:I = 0x6d

.field public static final AppCompatTheme_ratingBarStyleSmall:I = 0x6e

.field public static final AppCompatTheme_searchViewStyle:I = 0x45

.field public static final AppCompatTheme_seekBarStyle:I = 0x6f

.field public static final AppCompatTheme_selectableItemBackground:I = 0x35

.field public static final AppCompatTheme_selectableItemBackgroundBorderless:I = 0x36

.field public static final AppCompatTheme_spinnerDropDownItemStyle:I = 0x30

.field public static final AppCompatTheme_spinnerStyle:I = 0x70

.field public static final AppCompatTheme_switchStyle:I = 0x71

.field public static final AppCompatTheme_textAppearanceLargePopupMenu:I = 0x28

.field public static final AppCompatTheme_textAppearanceListItem:I = 0x4d

.field public static final AppCompatTheme_textAppearanceListItemSmall:I = 0x4e

.field public static final AppCompatTheme_textAppearancePopupMenuHeader:I = 0x2a

.field public static final AppCompatTheme_textAppearanceSearchResultSubtitle:I = 0x43

.field public static final AppCompatTheme_textAppearanceSearchResultTitle:I = 0x42

.field public static final AppCompatTheme_textAppearanceSmallPopupMenu:I = 0x29

.field public static final AppCompatTheme_textColorAlertDialogListItem:I = 0x61

.field public static final AppCompatTheme_textColorSearchUrl:I = 0x44

.field public static final AppCompatTheme_toolbarNavigationButtonStyle:I = 0x3c

.field public static final AppCompatTheme_toolbarStyle:I = 0x3b

.field public static final AppCompatTheme_windowActionBar:I = 0x2

.field public static final AppCompatTheme_windowActionBarOverlay:I = 0x4

.field public static final AppCompatTheme_windowActionModeOverlay:I = 0x5

.field public static final AppCompatTheme_windowFixedHeightMajor:I = 0x9

.field public static final AppCompatTheme_windowFixedHeightMinor:I = 0x7

.field public static final AppCompatTheme_windowFixedWidthMajor:I = 0x6

.field public static final AppCompatTheme_windowFixedWidthMinor:I = 0x8

.field public static final AppCompatTheme_windowMinWidthMajor:I = 0xa

.field public static final AppCompatTheme_windowMinWidthMinor:I = 0xb

.field public static final AppCompatTheme_windowNoTitle:I = 0x3

.field public static final BottomSheetBehavior_Layout:[I

.field public static final BottomSheetBehavior_Layout_behavior_hideable:I = 0x1

.field public static final BottomSheetBehavior_Layout_behavior_peekHeight:I = 0x0

.field public static final BottomSheetBehavior_Layout_behavior_skipCollapsed:I = 0x2

.field public static final ButtonBarLayout:[I

.field public static final ButtonBarLayout_allowStacking:I = 0x0

.field public static final CardView:[I

.field public static final CardView_android_minHeight:I = 0x1

.field public static final CardView_android_minWidth:I = 0x0

.field public static final CardView_cardBackgroundColor:I = 0x2

.field public static final CardView_cardCornerRadius:I = 0x3

.field public static final CardView_cardElevation:I = 0x4

.field public static final CardView_cardMaxElevation:I = 0x5

.field public static final CardView_cardPreventCornerOverlap:I = 0x7

.field public static final CardView_cardUseCompatPadding:I = 0x6

.field public static final CardView_contentPadding:I = 0x8

.field public static final CardView_contentPaddingBottom:I = 0xc

.field public static final CardView_contentPaddingLeft:I = 0x9

.field public static final CardView_contentPaddingRight:I = 0xa

.field public static final CardView_contentPaddingTop:I = 0xb

.field public static final CollapsingToolbarLayout:[I

.field public static final CollapsingToolbarLayout_Layout:[I

.field public static final CollapsingToolbarLayout_Layout_layout_collapseMode:I = 0x0

.field public static final CollapsingToolbarLayout_Layout_layout_collapseParallaxMultiplier:I = 0x1

.field public static final CollapsingToolbarLayout_collapsedTitleGravity:I = 0xd

.field public static final CollapsingToolbarLayout_collapsedTitleTextAppearance:I = 0x7

.field public static final CollapsingToolbarLayout_contentScrim:I = 0x8

.field public static final CollapsingToolbarLayout_expandedTitleGravity:I = 0xe

.field public static final CollapsingToolbarLayout_expandedTitleMargin:I = 0x1

.field public static final CollapsingToolbarLayout_expandedTitleMarginBottom:I = 0x5

.field public static final CollapsingToolbarLayout_expandedTitleMarginEnd:I = 0x4

.field public static final CollapsingToolbarLayout_expandedTitleMarginStart:I = 0x2

.field public static final CollapsingToolbarLayout_expandedTitleMarginTop:I = 0x3

.field public static final CollapsingToolbarLayout_expandedTitleTextAppearance:I = 0x6

.field public static final CollapsingToolbarLayout_scrimAnimationDuration:I = 0xc

.field public static final CollapsingToolbarLayout_scrimVisibleHeightTrigger:I = 0xb

.field public static final CollapsingToolbarLayout_statusBarScrim:I = 0x9

.field public static final CollapsingToolbarLayout_title:I = 0x0

.field public static final CollapsingToolbarLayout_titleEnabled:I = 0xf

.field public static final CollapsingToolbarLayout_toolbarId:I = 0xa

.field public static final ColorStateListItem:[I

.field public static final ColorStateListItem_alpha:I = 0x2

.field public static final ColorStateListItem_android_alpha:I = 0x1

.field public static final ColorStateListItem_android_color:I = 0x0

.field public static final CompoundButton:[I

.field public static final CompoundButton_android_button:I = 0x0

.field public static final CompoundButton_buttonTint:I = 0x1

.field public static final CompoundButton_buttonTintMode:I = 0x2

.field public static final CompoundDrawables:[I

.field public static final CompoundDrawables_iconBottom:I = 0x3

.field public static final CompoundDrawables_iconEnd:I = 0x5

.field public static final CompoundDrawables_iconLeft:I = 0x0

.field public static final CompoundDrawables_iconRight:I = 0x2

.field public static final CompoundDrawables_iconStart:I = 0x4

.field public static final CompoundDrawables_iconTop:I = 0x1

.field public static final CoordinatorLayout:[I

.field public static final CoordinatorLayout_Layout:[I

.field public static final CoordinatorLayout_Layout_android_layout_gravity:I = 0x0

.field public static final CoordinatorLayout_Layout_layout_anchor:I = 0x2

.field public static final CoordinatorLayout_Layout_layout_anchorGravity:I = 0x4

.field public static final CoordinatorLayout_Layout_layout_behavior:I = 0x1

.field public static final CoordinatorLayout_Layout_layout_dodgeInsetEdges:I = 0x6

.field public static final CoordinatorLayout_Layout_layout_insetEdge:I = 0x5

.field public static final CoordinatorLayout_Layout_layout_keyline:I = 0x3

.field public static final CoordinatorLayout_keylines:I = 0x0

.field public static final CoordinatorLayout_statusBarBackground:I = 0x1

.field public static final CurrencyValueView:[I

.field public static final CurrencyValueView_cv_currencyColor:I = 0x0

.field public static final CurrencyValueView_cv_currencyText:I = 0x6

.field public static final CurrencyValueView_cv_currencyTextSize:I = 0x2

.field public static final CurrencyValueView_cv_currencyTypeface:I = 0x4

.field public static final CurrencyValueView_cv_show_default_currency_on_create:I = 0x8

.field public static final CurrencyValueView_cv_valueColor:I = 0x1

.field public static final CurrencyValueView_cv_valueText:I = 0x7

.field public static final CurrencyValueView_cv_valueTextSize:I = 0x3

.field public static final CurrencyValueView_cv_valueTypeface:I = 0x5

.field public static final CustomTextView:[I

.field public static final CustomTextView_font:I = 0x0

.field public static final DesignTheme:[I

.field public static final DesignTheme_bottomSheetDialogTheme:I = 0x0

.field public static final DesignTheme_bottomSheetStyle:I = 0x1

.field public static final DesignTheme_textColorError:I = 0x2

.field public static final DrawerArrowToggle:[I

.field public static final DrawerArrowToggle_arrowHeadLength:I = 0x4

.field public static final DrawerArrowToggle_arrowShaftLength:I = 0x5

.field public static final DrawerArrowToggle_barLength:I = 0x6

.field public static final DrawerArrowToggle_color:I = 0x0

.field public static final DrawerArrowToggle_drawableSize:I = 0x2

.field public static final DrawerArrowToggle_gapBetweenBars:I = 0x3

.field public static final DrawerArrowToggle_spinBars:I = 0x1

.field public static final DrawerArrowToggle_thickness:I = 0x7

.field public static final FloatingActionButton:[I

.field public static final FloatingActionButton_Behavior_Layout:[I

.field public static final FloatingActionButton_Behavior_Layout_behavior_autoHide:I = 0x0

.field public static final FloatingActionButton_backgroundTint:I = 0x6

.field public static final FloatingActionButton_backgroundTintMode:I = 0x7

.field public static final FloatingActionButton_borderWidth:I = 0x4

.field public static final FloatingActionButton_elevation:I = 0x0

.field public static final FloatingActionButton_fabSize:I = 0x2

.field public static final FloatingActionButton_pressedTranslationZ:I = 0x3

.field public static final FloatingActionButton_rippleColor:I = 0x1

.field public static final FloatingActionButton_useCompatPadding:I = 0x5

.field public static final FontIconDrawable:[I

.field public static final FontIconDrawable_autoMirrored:I = 0x3

.field public static final FontIconDrawable_needMirroring:I = 0x4

.field public static final FontIconDrawable_text:I = 0x0

.field public static final FontIconDrawable_textColor:I = 0x1

.field public static final FontIconDrawable_textSize:I = 0x2

.field public static final ForegroundLinearLayout:[I

.field public static final ForegroundLinearLayout_android_foreground:I = 0x0

.field public static final ForegroundLinearLayout_android_foregroundGravity:I = 0x1

.field public static final ForegroundLinearLayout_foregroundInsidePadding:I = 0x2

.field public static final LinearLayoutCompat:[I

.field public static final LinearLayoutCompat_Layout:[I

.field public static final LinearLayoutCompat_Layout_android_layout_gravity:I = 0x0

.field public static final LinearLayoutCompat_Layout_android_layout_height:I = 0x2

.field public static final LinearLayoutCompat_Layout_android_layout_weight:I = 0x3

.field public static final LinearLayoutCompat_Layout_android_layout_width:I = 0x1

.field public static final LinearLayoutCompat_android_baselineAligned:I = 0x2

.field public static final LinearLayoutCompat_android_baselineAlignedChildIndex:I = 0x3

.field public static final LinearLayoutCompat_android_gravity:I = 0x0

.field public static final LinearLayoutCompat_android_orientation:I = 0x1

.field public static final LinearLayoutCompat_android_weightSum:I = 0x4

.field public static final LinearLayoutCompat_divider:I = 0x5

.field public static final LinearLayoutCompat_dividerPadding:I = 0x8

.field public static final LinearLayoutCompat_measureWithLargestChild:I = 0x6

.field public static final LinearLayoutCompat_showDividers:I = 0x7

.field public static final ListPopupWindow:[I

.field public static final ListPopupWindow_android_dropDownHorizontalOffset:I = 0x0

.field public static final ListPopupWindow_android_dropDownVerticalOffset:I = 0x1

.field public static final MenuGroup:[I

.field public static final MenuGroup_android_checkableBehavior:I = 0x5

.field public static final MenuGroup_android_enabled:I = 0x0

.field public static final MenuGroup_android_id:I = 0x1

.field public static final MenuGroup_android_menuCategory:I = 0x3

.field public static final MenuGroup_android_orderInCategory:I = 0x4

.field public static final MenuGroup_android_visible:I = 0x2

.field public static final MenuItem:[I

.field public static final MenuItem_actionLayout:I = 0xe

.field public static final MenuItem_actionProviderClass:I = 0x10

.field public static final MenuItem_actionViewClass:I = 0xf

.field public static final MenuItem_android_alphabeticShortcut:I = 0x9

.field public static final MenuItem_android_checkable:I = 0xb

.field public static final MenuItem_android_checked:I = 0x3

.field public static final MenuItem_android_enabled:I = 0x1

.field public static final MenuItem_android_icon:I = 0x0

.field public static final MenuItem_android_id:I = 0x2

.field public static final MenuItem_android_menuCategory:I = 0x5

.field public static final MenuItem_android_numericShortcut:I = 0xa

.field public static final MenuItem_android_onClick:I = 0xc

.field public static final MenuItem_android_orderInCategory:I = 0x6

.field public static final MenuItem_android_title:I = 0x7

.field public static final MenuItem_android_titleCondensed:I = 0x8

.field public static final MenuItem_android_visible:I = 0x4

.field public static final MenuItem_showAsAction:I = 0xd

.field public static final MenuView:[I

.field public static final MenuView_android_headerBackground:I = 0x4

.field public static final MenuView_android_horizontalDivider:I = 0x2

.field public static final MenuView_android_itemBackground:I = 0x5

.field public static final MenuView_android_itemIconDisabledAlpha:I = 0x6

.field public static final MenuView_android_itemTextAppearance:I = 0x1

.field public static final MenuView_android_verticalDivider:I = 0x3

.field public static final MenuView_android_windowAnimationStyle:I = 0x0

.field public static final MenuView_preserveIconSpacing:I = 0x7

.field public static final MenuView_subMenuArrow:I = 0x8

.field public static final NavigationView:[I

.field public static final NavigationView_android_background:I = 0x0

.field public static final NavigationView_android_fitsSystemWindows:I = 0x1

.field public static final NavigationView_android_maxWidth:I = 0x2

.field public static final NavigationView_elevation:I = 0x3

.field public static final NavigationView_headerLayout:I = 0x9

.field public static final NavigationView_itemBackground:I = 0x7

.field public static final NavigationView_itemIconTint:I = 0x5

.field public static final NavigationView_itemTextAppearance:I = 0x8

.field public static final NavigationView_itemTextColor:I = 0x6

.field public static final NavigationView_menu:I = 0x4

.field public static final PopupWindow:[I

.field public static final PopupWindowBackgroundState:[I

.field public static final PopupWindowBackgroundState_state_above_anchor:I = 0x0

.field public static final PopupWindow_android_popupAnimationStyle:I = 0x1

.field public static final PopupWindow_android_popupBackground:I = 0x0

.field public static final PopupWindow_overlapAnchor:I = 0x2

.field public static final Pulsator4Droid:[I

.field public static final Pulsator4Droid_pulse_color:I = 0x2

.field public static final Pulsator4Droid_pulse_count:I = 0x0

.field public static final Pulsator4Droid_pulse_duration:I = 0x1

.field public static final Pulsator4Droid_pulse_interpolator:I = 0x6

.field public static final Pulsator4Droid_pulse_maxScale:I = 0x4

.field public static final Pulsator4Droid_pulse_repeat:I = 0x3

.field public static final Pulsator4Droid_pulse_startFromScratch:I = 0x5

.field public static final RecyclerView:[I

.field public static final RecyclerView_android_descendantFocusability:I = 0x1

.field public static final RecyclerView_android_orientation:I = 0x0

.field public static final RecyclerView_layoutManager:I = 0x2

.field public static final RecyclerView_reverseLayout:I = 0x4

.field public static final RecyclerView_spanCount:I = 0x3

.field public static final RecyclerView_stackFromEnd:I = 0x5

.field public static final ScrimInsetsFrameLayout:[I

.field public static final ScrimInsetsFrameLayout_insetForeground:I = 0x0

.field public static final ScrollingViewBehavior_Layout:[I

.field public static final ScrollingViewBehavior_Layout_behavior_overlapTop:I = 0x0

.field public static final SearchView:[I

.field public static final SearchView_android_focusable:I = 0x0

.field public static final SearchView_android_imeOptions:I = 0x3

.field public static final SearchView_android_inputType:I = 0x2

.field public static final SearchView_android_maxWidth:I = 0x1

.field public static final SearchView_closeIcon:I = 0x8

.field public static final SearchView_commitIcon:I = 0xd

.field public static final SearchView_defaultQueryHint:I = 0x7

.field public static final SearchView_goIcon:I = 0x9

.field public static final SearchView_iconifiedByDefault:I = 0x5

.field public static final SearchView_layout:I = 0x4

.field public static final SearchView_queryBackground:I = 0xf

.field public static final SearchView_queryHint:I = 0x6

.field public static final SearchView_searchHintIcon:I = 0xb

.field public static final SearchView_searchIcon:I = 0xa

.field public static final SearchView_submitBackground:I = 0x10

.field public static final SearchView_suggestionRowLayout:I = 0xe

.field public static final SearchView_voiceIcon:I = 0xc

.field public static final SnackbarLayout:[I

.field public static final SnackbarLayout_android_maxWidth:I = 0x0

.field public static final SnackbarLayout_elevation:I = 0x1

.field public static final SnackbarLayout_maxActionInlineWidth:I = 0x2

.field public static final Spinner:[I

.field public static final Spinner_android_dropDownWidth:I = 0x3

.field public static final Spinner_android_entries:I = 0x0

.field public static final Spinner_android_popupBackground:I = 0x1

.field public static final Spinner_android_prompt:I = 0x2

.field public static final Spinner_popupTheme:I = 0x4

.field public static final SwitchCompat:[I

.field public static final SwitchCompat_android_textOff:I = 0x1

.field public static final SwitchCompat_android_textOn:I = 0x0

.field public static final SwitchCompat_android_thumb:I = 0x2

.field public static final SwitchCompat_showText:I = 0xd

.field public static final SwitchCompat_splitTrack:I = 0xc

.field public static final SwitchCompat_switchMinWidth:I = 0xa

.field public static final SwitchCompat_switchPadding:I = 0xb

.field public static final SwitchCompat_switchTextAppearance:I = 0x9

.field public static final SwitchCompat_thumbTextPadding:I = 0x8

.field public static final SwitchCompat_thumbTint:I = 0x3

.field public static final SwitchCompat_thumbTintMode:I = 0x4

.field public static final SwitchCompat_track:I = 0x5

.field public static final SwitchCompat_trackTint:I = 0x6

.field public static final SwitchCompat_trackTintMode:I = 0x7

.field public static final TabItem:[I

.field public static final TabItem_android_icon:I = 0x0

.field public static final TabItem_android_layout:I = 0x1

.field public static final TabItem_android_text:I = 0x2

.field public static final TabLayout:[I

.field public static final TabLayout_tabBackground:I = 0x3

.field public static final TabLayout_tabContentStart:I = 0x2

.field public static final TabLayout_tabGravity:I = 0x5

.field public static final TabLayout_tabIndicatorColor:I = 0x0

.field public static final TabLayout_tabIndicatorHeight:I = 0x1

.field public static final TabLayout_tabMaxWidth:I = 0x7

.field public static final TabLayout_tabMinWidth:I = 0x6

.field public static final TabLayout_tabMode:I = 0x4

.field public static final TabLayout_tabPadding:I = 0xf

.field public static final TabLayout_tabPaddingBottom:I = 0xe

.field public static final TabLayout_tabPaddingEnd:I = 0xd

.field public static final TabLayout_tabPaddingStart:I = 0xb

.field public static final TabLayout_tabPaddingTop:I = 0xc

.field public static final TabLayout_tabSelectedTextColor:I = 0xa

.field public static final TabLayout_tabTextAppearance:I = 0x8

.field public static final TabLayout_tabTextColor:I = 0x9

.field public static final TextAppearance:[I

.field public static final TextAppearance_android_shadowColor:I = 0x5

.field public static final TextAppearance_android_shadowDx:I = 0x6

.field public static final TextAppearance_android_shadowDy:I = 0x7

.field public static final TextAppearance_android_shadowRadius:I = 0x8

.field public static final TextAppearance_android_textColor:I = 0x3

.field public static final TextAppearance_android_textSize:I = 0x0

.field public static final TextAppearance_android_textStyle:I = 0x2

.field public static final TextAppearance_android_typeface:I = 0x1

.field public static final TextAppearance_textAllCaps:I = 0x9

.field public static final TextInputLayout:[I

.field public static final TextInputLayout_android_hint:I = 0x1

.field public static final TextInputLayout_android_textColorHint:I = 0x0

.field public static final TextInputLayout_counterEnabled:I = 0x6

.field public static final TextInputLayout_counterMaxLength:I = 0x7

.field public static final TextInputLayout_counterOverflowTextAppearance:I = 0x9

.field public static final TextInputLayout_counterTextAppearance:I = 0x8

.field public static final TextInputLayout_errorEnabled:I = 0x4

.field public static final TextInputLayout_errorTextAppearance:I = 0x5

.field public static final TextInputLayout_hintAnimationEnabled:I = 0xa

.field public static final TextInputLayout_hintEnabled:I = 0x3

.field public static final TextInputLayout_hintTextAppearance:I = 0x2

.field public static final TextInputLayout_passwordToggleContentDescription:I = 0xd

.field public static final TextInputLayout_passwordToggleDrawable:I = 0xc

.field public static final TextInputLayout_passwordToggleEnabled:I = 0xb

.field public static final TextInputLayout_passwordToggleTint:I = 0xe

.field public static final TextInputLayout_passwordToggleTintMode:I = 0xf

.field public static final Toolbar:[I

.field public static final Toolbar_android_gravity:I = 0x0

.field public static final Toolbar_android_minHeight:I = 0x1

.field public static final Toolbar_buttonGravity:I = 0x15

.field public static final Toolbar_collapseContentDescription:I = 0x17

.field public static final Toolbar_collapseIcon:I = 0x16

.field public static final Toolbar_contentInsetEnd:I = 0x6

.field public static final Toolbar_contentInsetEndWithActions:I = 0xa

.field public static final Toolbar_contentInsetLeft:I = 0x7

.field public static final Toolbar_contentInsetRight:I = 0x8

.field public static final Toolbar_contentInsetStart:I = 0x5

.field public static final Toolbar_contentInsetStartWithNavigation:I = 0x9

.field public static final Toolbar_logo:I = 0x4

.field public static final Toolbar_logoDescription:I = 0x1a

.field public static final Toolbar_maxButtonHeight:I = 0x14

.field public static final Toolbar_navigationContentDescription:I = 0x19

.field public static final Toolbar_navigationIcon:I = 0x18

.field public static final Toolbar_popupTheme:I = 0xb

.field public static final Toolbar_subtitle:I = 0x3

.field public static final Toolbar_subtitleTextAppearance:I = 0xd

.field public static final Toolbar_subtitleTextColor:I = 0x1c

.field public static final Toolbar_title:I = 0x2

.field public static final Toolbar_titleMargin:I = 0xe

.field public static final Toolbar_titleMarginBottom:I = 0x12

.field public static final Toolbar_titleMarginEnd:I = 0x10

.field public static final Toolbar_titleMarginStart:I = 0xf

.field public static final Toolbar_titleMarginTop:I = 0x11

.field public static final Toolbar_titleMargins:I = 0x13

.field public static final Toolbar_titleTextAppearance:I = 0xc

.field public static final Toolbar_titleTextColor:I = 0x1b

.field public static final View:[I

.field public static final ViewBackgroundHelper:[I

.field public static final ViewBackgroundHelper_android_background:I = 0x0

.field public static final ViewBackgroundHelper_backgroundTint:I = 0x1

.field public static final ViewBackgroundHelper_backgroundTintMode:I = 0x2

.field public static final ViewStubCompat:[I

.field public static final ViewStubCompat_android_id:I = 0x0

.field public static final ViewStubCompat_android_inflatedId:I = 0x2

.field public static final ViewStubCompat_android_layout:I = 0x1

.field public static final View_android_focusable:I = 0x1

.field public static final View_android_theme:I = 0x0

.field public static final View_paddingEnd:I = 0x3

.field public static final View_paddingStart:I = 0x2

.field public static final View_theme:I = 0x4

.field public static final customAttrs:[I

.field public static final customAttrs_text_color_theme:I = 0x0

.field public static final itausdkcore_TokenTheme:[I

.field public static final itausdkcore_TokenTheme_itausdkcore_custom_font_bold_path:I = 0xa

.field public static final itausdkcore_TokenTheme_itausdkcore_custom_font_regular_path:I = 0x9

.field public static final itausdkcore_TokenTheme_itausdkcore_custom_icon_aplicativo_token:I = 0x2

.field public static final itausdkcore_TokenTheme_itausdkcore_custom_icon_back:I = 0x6

.field public static final itausdkcore_TokenTheme_itausdkcore_custom_icon_chaveiro_token:I = 0x3

.field public static final itausdkcore_TokenTheme_itausdkcore_custom_icon_close:I = 0x7

.field public static final itausdkcore_TokenTheme_itausdkcore_custom_icon_sms_token:I = 0x1

.field public static final itausdkcore_TokenTheme_itausdkcore_custom_loading_height:I = 0x5

.field public static final itausdkcore_TokenTheme_itausdkcore_custom_loading_width:I = 0x4

.field public static final itausdkcore_TokenTheme_itausdkcore_custom_primary_color:I = 0x0

.field public static final itausdkcore_TokenTheme_itausdkcore_custom_status_bar_color:I = 0x8

.field public static final itausdkcore_TokenTypeView:[I

.field public static final itausdkcore_TokenTypeView_itausdkcore_showSecondaryAction:I = 0x2

.field public static final itausdkcore_TokenTypeView_itausdkcore_tokenIcon:I = 0x3

.field public static final itausdkcore_TokenTypeView_itausdkcore_tokenLabel:I = 0x1

.field public static final itausdkcore_TokenTypeView_itausdkcore_tokenTypeStyle:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 2024
    const/16 v0, 0x1d

    new-array v0, v0, [I

    fill-array-data v0, :array_1f8

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->ActionBar:[I

    .line 2025
    const/4 v0, 0x1

    new-array v0, v0, [I

    fill-array-data v0, :array_236

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->ActionBarLayout:[I

    .line 2056
    const/4 v0, 0x1

    new-array v0, v0, [I

    fill-array-data v0, :array_23c

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->ActionMenuItemView:[I

    .line 2058
    const/4 v0, 0x0

    new-array v0, v0, [I

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->ActionMenuView:[I

    .line 2059
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_242

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->ActionMode:[I

    .line 2066
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_252

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->ActivityChooserView:[I

    .line 2069
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_25a

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->AlertDialog:[I

    .line 2076
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_26a

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->AppBarLayout:[I

    .line 2077
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_274

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->AppBarLayoutStates:[I

    .line 2080
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_27c

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->AppBarLayout_Layout:[I

    .line 2086
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_284

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->AppCompatImageView:[I

    .line 2089
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_28c

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->AppCompatSeekBar:[I

    .line 2094
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_298

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->AppCompatTextHelper:[I

    .line 2102
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_2aa

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->AppCompatTextView:[I

    .line 2105
    const/16 v0, 0x73

    new-array v0, v0, [I

    fill-array-data v0, :array_2b2

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->AppCompatTheme:[I

    .line 2221
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_39c

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->BottomSheetBehavior_Layout:[I

    .line 2225
    const/4 v0, 0x1

    new-array v0, v0, [I

    fill-array-data v0, :array_3a6

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->ButtonBarLayout:[I

    .line 2227
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_3ac

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->CardView:[I

    .line 2241
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_3ca

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->CollapsingToolbarLayout:[I

    .line 2242
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_3ee

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->CollapsingToolbarLayout_Layout:[I

    .line 2261
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_3f6

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->ColorStateListItem:[I

    .line 2265
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_400

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->CompoundButton:[I

    .line 2269
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_40a

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->CompoundDrawables:[I

    .line 2276
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_41a

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->CoordinatorLayout:[I

    .line 2277
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_422

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->CoordinatorLayout_Layout:[I

    .line 2287
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_434

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->CurrencyValueView:[I

    .line 2297
    const/4 v0, 0x1

    new-array v0, v0, [I

    fill-array-data v0, :array_44a

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->CustomTextView:[I

    .line 2299
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_450

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->DesignTheme:[I

    .line 2303
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_45a

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->DrawerArrowToggle:[I

    .line 2312
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_46e

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->FloatingActionButton:[I

    .line 2313
    const/4 v0, 0x1

    new-array v0, v0, [I

    fill-array-data v0, :array_482

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->FloatingActionButton_Behavior_Layout:[I

    .line 2323
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_488

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->FontIconDrawable:[I

    .line 2329
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_496

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->ForegroundLinearLayout:[I

    .line 2333
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_4a0

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->LinearLayoutCompat:[I

    .line 2334
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_4b6

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->LinearLayoutCompat_Layout:[I

    .line 2348
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_4c2

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->ListPopupWindow:[I

    .line 2351
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_4ca

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->MenuGroup:[I

    .line 2358
    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_4da

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->MenuItem:[I

    .line 2376
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_500

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->MenuView:[I

    .line 2386
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_516

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->NavigationView:[I

    .line 2397
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_52e

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->PopupWindow:[I

    .line 2398
    const/4 v0, 0x1

    new-array v0, v0, [I

    fill-array-data v0, :array_538

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->PopupWindowBackgroundState:[I

    .line 2403
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_53e

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->Pulsator4Droid:[I

    .line 2411
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_550

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->RecyclerView:[I

    .line 2418
    const/4 v0, 0x1

    new-array v0, v0, [I

    fill-array-data v0, :array_560

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->ScrimInsetsFrameLayout:[I

    .line 2420
    const/4 v0, 0x1

    new-array v0, v0, [I

    fill-array-data v0, :array_566

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->ScrollingViewBehavior_Layout:[I

    .line 2422
    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_56c

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->SearchView:[I

    .line 2440
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_592

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->SnackbarLayout:[I

    .line 2444
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_59c

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->Spinner:[I

    .line 2450
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_5aa

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->SwitchCompat:[I

    .line 2465
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_5ca

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->TabItem:[I

    .line 2469
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_5d4

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->TabLayout:[I

    .line 2486
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_5f8

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->TextAppearance:[I

    .line 2496
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_610

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->TextInputLayout:[I

    .line 2513
    const/16 v0, 0x1d

    new-array v0, v0, [I

    fill-array-data v0, :array_634

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->Toolbar:[I

    .line 2543
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_672

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->View:[I

    .line 2544
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_680

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->ViewBackgroundHelper:[I

    .line 2548
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_68a

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->ViewStubCompat:[I

    .line 2557
    const/4 v0, 0x1

    new-array v0, v0, [I

    fill-array-data v0, :array_694

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->customAttrs:[I

    .line 2559
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_69a

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->itausdkcore_TokenTheme:[I

    .line 2571
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_6b8

    sput-object v0, Lbr/com/itau/textovoz/R$styleable;->itausdkcore_TokenTypeView:[I

    return-void

    :array_1f8
    .array-data 4
        0x7f010004
        0x7f010033
        0x7f010036
        0x7f010037
        0x7f010038
        0x7f010039
        0x7f01003a
        0x7f01003b
        0x7f01003c
        0x7f01003d
        0x7f01003e
        0x7f01003f
        0x7f010040
        0x7f010041
        0x7f010042
        0x7f010043
        0x7f010044
        0x7f010045
        0x7f010046
        0x7f010047
        0x7f010048
        0x7f010049
        0x7f01004a
        0x7f01004b
        0x7f01004c
        0x7f01004d
        0x7f01004e
        0x7f01004f
        0x7f010094
    .end array-data

    :array_236
    .array-data 4
        0x10100b3
    .end array-data

    :array_23c
    .array-data 4
        0x101013f
    .end array-data

    :array_242
    .array-data 4
        0x7f010004
        0x7f010039
        0x7f01003a
        0x7f01003e
        0x7f010040
        0x7f010050
    .end array-data

    :array_252
    .array-data 4
        0x7f010051
        0x7f010052
    .end array-data

    :array_25a
    .array-data 4
        0x10100f2
        0x7f010056
        0x7f010057
        0x7f010058
        0x7f010059
        0x7f01005a
    .end array-data

    :array_26a
    .array-data 4
        0x10100d4
        0x7f01004e
        0x7f01005b
    .end array-data

    :array_274
    .array-data 4
        0x7f01005c
        0x7f01005d
    .end array-data

    :array_27c
    .array-data 4
        0x7f01005e
        0x7f01005f
    .end array-data

    :array_284
    .array-data 4
        0x1010119
        0x7f010060
    .end array-data

    :array_28c
    .array-data 4
        0x1010142
        0x7f010061
        0x7f010062
        0x7f010063
    .end array-data

    :array_298
    .array-data 4
        0x1010034
        0x101016d
        0x101016e
        0x101016f
        0x1010170
        0x1010392
        0x1010393
    .end array-data

    :array_2aa
    .array-data 4
        0x1010034
        0x7f010064
    .end array-data

    :array_2b2
    .array-data 4
        0x1010057
        0x10100ae
        0x7f010065
        0x7f010066
        0x7f010067
        0x7f010068
        0x7f010069
        0x7f01006a
        0x7f01006b
        0x7f01006c
        0x7f01006d
        0x7f01006e
        0x7f01006f
        0x7f010070
        0x7f010071
        0x7f010072
        0x7f010073
        0x7f010074
        0x7f010075
        0x7f010076
        0x7f010077
        0x7f010078
        0x7f010079
        0x7f01007a
        0x7f01007b
        0x7f01007c
        0x7f01007d
        0x7f01007e
        0x7f01007f
        0x7f010080
        0x7f010081
        0x7f010082
        0x7f010083
        0x7f010084
        0x7f010085
        0x7f010086
        0x7f010087
        0x7f010088
        0x7f010089
        0x7f01008a
        0x7f01008b
        0x7f01008c
        0x7f01008d
        0x7f01008e
        0x7f01008f
        0x7f010090
        0x7f010091
        0x7f010092
        0x7f010093
        0x7f010094
        0x7f010095
        0x7f010096
        0x7f010097
        0x7f010098
        0x7f010099
        0x7f01009a
        0x7f01009b
        0x7f01009c
        0x7f01009d
        0x7f01009e
        0x7f01009f
        0x7f0100a0
        0x7f0100a1
        0x7f0100a2
        0x7f0100a3
        0x7f0100a4
        0x7f0100a5
        0x7f0100a6
        0x7f0100a7
        0x7f0100a8
        0x7f0100a9
        0x7f0100aa
        0x7f0100ab
        0x7f0100ac
        0x7f0100ad
        0x7f0100ae
        0x7f0100af
        0x7f0100b0
        0x7f0100b1
        0x7f0100b2
        0x7f0100b3
        0x7f0100b4
        0x7f0100b5
        0x7f0100b6
        0x7f0100b7
        0x7f0100b8
        0x7f0100b9
        0x7f0100ba
        0x7f0100bb
        0x7f0100bc
        0x7f0100bd
        0x7f0100be
        0x7f0100bf
        0x7f0100c0
        0x7f0100c1
        0x7f0100c2
        0x7f0100c3
        0x7f0100c4
        0x7f0100c5
        0x7f0100c6
        0x7f0100c7
        0x7f0100c8
        0x7f0100c9
        0x7f0100ca
        0x7f0100cb
        0x7f0100cc
        0x7f0100cd
        0x7f0100ce
        0x7f0100cf
        0x7f0100d0
        0x7f0100d1
        0x7f0100d2
        0x7f0100d3
        0x7f0100d4
        0x7f0100d5
    .end array-data

    :array_39c
    .array-data 4
        0x7f0100d6
        0x7f0100d7
        0x7f0100d8
    .end array-data

    :array_3a6
    .array-data 4
        0x7f0100e1
    .end array-data

    :array_3ac
    .array-data 4
        0x101013f
        0x1010140
        0x7f0100e2
        0x7f0100e3
        0x7f0100e4
        0x7f0100e5
        0x7f0100e6
        0x7f0100e7
        0x7f0100e8
        0x7f0100e9
        0x7f0100ea
        0x7f0100eb
        0x7f0100ec
    .end array-data

    :array_3ca
    .array-data 4
        0x7f010033
        0x7f0100f2
        0x7f0100f3
        0x7f0100f4
        0x7f0100f5
        0x7f0100f6
        0x7f0100f7
        0x7f0100f8
        0x7f0100f9
        0x7f0100fa
        0x7f0100fb
        0x7f0100fc
        0x7f0100fd
        0x7f0100fe
        0x7f0100ff
        0x7f010100
    .end array-data

    :array_3ee
    .array-data 4
        0x7f010101
        0x7f010102
    .end array-data

    :array_3f6
    .array-data 4
        0x10101a5
        0x101031f
        0x7f010103
    .end array-data

    :array_400
    .array-data 4
        0x1010107
        0x7f010104
        0x7f010105
    .end array-data

    :array_40a
    .array-data 4
        0x7f010106
        0x7f010107
        0x7f010108
        0x7f010109
        0x7f01010a
        0x7f01010b
    .end array-data

    :array_41a
    .array-data 4
        0x7f01010c
        0x7f01010d
    .end array-data

    :array_422
    .array-data 4
        0x10100b3
        0x7f01010e
        0x7f01010f
        0x7f010110
        0x7f010111
        0x7f010112
        0x7f010113
    .end array-data

    :array_434
    .array-data 4
        0x7f010115
        0x7f010116
        0x7f010117
        0x7f010118
        0x7f010119
        0x7f01011a
        0x7f01011b
        0x7f01011c
        0x7f01011d
    .end array-data

    :array_44a
    .array-data 4
        0x7f01011e
    .end array-data

    :array_450
    .array-data 4
        0x7f01011f
        0x7f010120
        0x7f010121
    .end array-data

    :array_45a
    .array-data 4
        0x7f010122
        0x7f010123
        0x7f010124
        0x7f010125
        0x7f010126
        0x7f010127
        0x7f010128
        0x7f010129
    .end array-data

    :array_46e
    .array-data 4
        0x7f01004e
        0x7f01012e
        0x7f01012f
        0x7f010130
        0x7f010131
        0x7f010132
        0x7f01025d
        0x7f01025e
    .end array-data

    :array_482
    .array-data 4
        0x7f010133
    .end array-data

    :array_488
    .array-data 4
        0x7f010134
        0x7f010135
        0x7f010136
        0x7f010137
        0x7f010138
    .end array-data

    :array_496
    .array-data 4
        0x1010109
        0x1010200
        0x7f010139
    .end array-data

    :array_4a0
    .array-data 4
        0x10100af
        0x10100c4
        0x1010126
        0x1010127
        0x1010128
        0x7f01003d
        0x7f010152
        0x7f010153
        0x7f010154
    .end array-data

    :array_4b6
    .array-data 4
        0x10100b3
        0x10100f4
        0x10100f5
        0x1010181
    .end array-data

    :array_4c2
    .array-data 4
        0x10102ac
        0x10102ad
    .end array-data

    :array_4ca
    .array-data 4
        0x101000e
        0x10100d0
        0x1010194
        0x10101de
        0x10101df
        0x10101e0
    .end array-data

    :array_4da
    .array-data 4
        0x1010002
        0x101000e
        0x10100d0
        0x1010106
        0x1010194
        0x10101de
        0x10101df
        0x10101e1
        0x10101e2
        0x10101e3
        0x10101e4
        0x10101e5
        0x101026f
        0x7f0101ba
        0x7f0101bb
        0x7f0101bc
        0x7f0101bd
    .end array-data

    :array_500
    .array-data 4
        0x10100ae
        0x101012c
        0x101012d
        0x101012e
        0x101012f
        0x1010130
        0x1010131
        0x7f0101be
        0x7f0101bf
    .end array-data

    :array_516
    .array-data 4
        0x10100d4
        0x10100dd
        0x101011f
        0x7f01004e
        0x7f0101c0
        0x7f0101c1
        0x7f0101c2
        0x7f0101c3
        0x7f0101c4
        0x7f0101c5
    .end array-data

    :array_52e
    .array-data 4
        0x1010176
        0x10102c9
        0x7f0101c9
    .end array-data

    :array_538
    .array-data 4
        0x7f0101ca
    .end array-data

    :array_53e
    .array-data 4
        0x7f0101cb
        0x7f0101cc
        0x7f0101cd
        0x7f0101ce
        0x7f0101cf
        0x7f0101d0
        0x7f0101d1
    .end array-data

    :array_550
    .array-data 4
        0x10100c4
        0x10100f1
        0x7f0101d2
        0x7f0101d3
        0x7f0101d4
        0x7f0101d5
    .end array-data

    :array_560
    .array-data 4
        0x7f0101d6
    .end array-data

    :array_566
    .array-data 4
        0x7f0101d9
    .end array-data

    :array_56c
    .array-data 4
        0x10100da
        0x101011f
        0x1010220
        0x1010264
        0x7f0101da
        0x7f0101db
        0x7f0101dc
        0x7f0101dd
        0x7f0101de
        0x7f0101df
        0x7f0101e0
        0x7f0101e1
        0x7f0101e2
        0x7f0101e3
        0x7f0101e4
        0x7f0101e5
        0x7f0101e6
    .end array-data

    :array_592
    .array-data 4
        0x101011f
        0x7f01004e
        0x7f0101f8
    .end array-data

    :array_59c
    .array-data 4
        0x10100b2
        0x1010176
        0x101017b
        0x1010262
        0x7f01004f
    .end array-data

    :array_5aa
    .array-data 4
        0x1010124
        0x1010125
        0x1010142
        0x7f010200
        0x7f010201
        0x7f010202
        0x7f010203
        0x7f010204
        0x7f010205
        0x7f010206
        0x7f010207
        0x7f010208
        0x7f010209
        0x7f01020a
    .end array-data

    :array_5ca
    .array-data 4
        0x1010002
        0x10100f2
        0x101014f
    .end array-data

    :array_5d4
    .array-data 4
        0x7f01020b
        0x7f01020c
        0x7f01020d
        0x7f01020e
        0x7f01020f
        0x7f010210
        0x7f010211
        0x7f010212
        0x7f010213
        0x7f010214
        0x7f010215
        0x7f010216
        0x7f010217
        0x7f010218
        0x7f010219
        0x7f01021a
    .end array-data

    :array_5f8
    .array-data 4
        0x1010095
        0x1010096
        0x1010097
        0x1010098
        0x101009a
        0x1010161
        0x1010162
        0x1010163
        0x1010164
        0x7f010064
    .end array-data

    :array_610
    .array-data 4
        0x101009a
        0x1010150
        0x7f01021b
        0x7f01021c
        0x7f01021d
        0x7f01021e
        0x7f01021f
        0x7f010220
        0x7f010221
        0x7f010222
        0x7f010223
        0x7f010224
        0x7f010225
        0x7f010226
        0x7f010227
        0x7f010228
    .end array-data

    :array_634
    .array-data 4
        0x10100af
        0x1010140
        0x7f010033
        0x7f010038
        0x7f01003c
        0x7f010048
        0x7f010049
        0x7f01004a
        0x7f01004b
        0x7f01004c
        0x7f01004d
        0x7f01004f
        0x7f010234
        0x7f010235
        0x7f010236
        0x7f010237
        0x7f010238
        0x7f010239
        0x7f01023a
        0x7f01023b
        0x7f01023c
        0x7f01023d
        0x7f01023e
        0x7f01023f
        0x7f010240
        0x7f010241
        0x7f010242
        0x7f010243
        0x7f010244
    .end array-data

    :array_672
    .array-data 4
        0x1010000
        0x10100da
        0x7f01025a
        0x7f01025b
        0x7f01025c
    .end array-data

    :array_680
    .array-data 4
        0x10100d4
        0x7f01025d
        0x7f01025e
    .end array-data

    :array_68a
    .array-data 4
        0x10100d0
        0x10100f2
        0x10100f3
    .end array-data

    :array_694
    .array-data 4
        0x7f010277
    .end array-data

    :array_69a
    .array-data 4
        0x7f010278
        0x7f010279
        0x7f01027a
        0x7f01027b
        0x7f01027c
        0x7f01027d
        0x7f01027e
        0x7f01027f
        0x7f010280
        0x7f010281
        0x7f010282
        0x7f010283
        0x7f010284
    .end array-data

    :array_6b8
    .array-data 4
        0x7f010285
        0x7f010286
        0x7f010287
        0x7f010288
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 2023
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;
.super Landroid/support/v7/widget/CardView;
.source "LisCardValorView.java"


# instance fields
.field textViewAcao:Landroid/widget/TextView;

.field textViewObservacao:Landroid/widget/TextView;

.field textViewValor:Landroid/widget/TextView;

.field private textoAcao:Ljava/lang/String;

.field private textoObservacao:Ljava/lang/String;

.field private textoValor:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 30
    invoke-direct {p0, p1}, Landroid/support/v7/widget/CardView;-><init>(Landroid/content/Context;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 34
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    invoke-direct {p0, p2}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->init(Landroid/util/AttributeSet;)V

    .line 36
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->iniciaLayout()V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 40
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    invoke-direct {p0, p2}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->init(Landroid/util/AttributeSet;)V

    .line 42
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->iniciaLayout()V

    .line 43
    return-void
.end method

.method private iniciaLayout()V
    .registers 5

    .line 87
    const/4 v0, 0x1

    new-array v2, v0, [I

    fill-array-data v2, :array_32

    .line 88
    .local v2, "attrs":[I
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 89
    .local v3, "typedArray":Landroid/content/res/TypedArray;
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 90
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 92
    const/high16 v0, 0x40800000    # 4.0f

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->setCardElevation(F)V

    .line 93
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0800b1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->setRadius(F)V

    .line 94
    return-void

    nop

    :array_32
    .array-data 4
        0x7f010098
    .end array-data
.end method

.method private init(Landroid/util/AttributeSet;)V
    .registers 8
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .line 71
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/R$styleable;->LisCardValor:[I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 77
    .local v4, "a":Landroid/content/res/TypedArray;
    const/4 v0, 0x0

    :try_start_11
    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->textoValor:Ljava/lang/String;

    .line 78
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->textoObservacao:Ljava/lang/String;

    .line 79
    const/4 v0, 0x2

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->textoAcao:Ljava/lang/String;
    :try_end_25
    .catchall {:try_start_11 .. :try_end_25} :catchall_29

    .line 81
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 82
    goto :goto_2e

    .line 81
    :catchall_29
    move-exception v5

    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    throw v5

    .line 83
    :goto_2e
    return-void
.end method


# virtual methods
.method afterViews()V
    .registers 3

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->textViewValor:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->textoValor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->textViewObservacao:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->textoObservacao:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->textViewAcao:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->textoAcao:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    return-void
.end method

.method public setContentDescriptionTextoObservacao(Ljava/lang/String;)V
    .registers 3
    .param p1, "contentDescription"    # Ljava/lang/String;

    .line 60
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->textViewObservacao:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 61
    return-void
.end method

.method public setTextoObservacao(Ljava/lang/String;)V
    .registers 3
    .param p1, "textoObservacao"    # Ljava/lang/String;

    .line 55
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->textoObservacao:Ljava/lang/String;

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->textViewObservacao:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    return-void
.end method

.method public setTextoValor(Ljava/lang/String;)V
    .registers 3
    .param p1, "textoValor"    # Ljava/lang/String;

    .line 50
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->textoValor:Ljava/lang/String;

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->textViewValor:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 52
    return-void
.end method

.class public Lcom/itau/empresas/feature/validador/filter/AlphaNumericoFilter;
.super Ljava/lang/Object;
.source "AlphaNumericoFilter.java"

# interfaces
.implements Landroid/text/InputFilter;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static retornaCaracteresAlphaNumericos(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;
    .registers 7
    .param p0, "source"    # Ljava/lang/CharSequence;
    .param p1, "start"    # I
    .param p2, "end"    # I

    .line 26
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 27
    .local v1, "builder":Ljava/lang/StringBuilder;
    move v2, p1

    .local v2, "i":I
    :goto_6
    if-ge v2, p2, :cond_18

    .line 28
    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    .line 29
    .local v3, "c":C
    invoke-static {v3}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 30
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 27
    .end local v3    # "c":C
    :cond_15
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 34
    .end local v2    # "i":I
    :cond_18
    return-object v1
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .registers 11
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .line 18
    invoke-static {p1, p2, p3}, Lcom/itau/empresas/feature/validador/filter/AlphaNumericoFilter;->retornaCaracteresAlphaNumericos(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 20
    .local v2, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    sub-int v1, p3, p2

    if-ne v0, v1, :cond_e

    const/4 v3, 0x1

    goto :goto_f

    :cond_e
    const/4 v3, 0x0

    .line 21
    .local v3, "todosValidos":Z
    :goto_f
    if-eqz v3, :cond_13

    const/4 v0, 0x0

    goto :goto_17

    :cond_13
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_17
    return-object v0
.end method

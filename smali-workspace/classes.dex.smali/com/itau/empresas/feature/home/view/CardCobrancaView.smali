.class public Lcom/itau/empresas/feature/home/view/CardCobrancaView;
.super Lcom/itau/empresas/feature/home/view/ICardView;
.source "CardCobrancaView.java"

# interfaces
.implements Lcom/itau/empresas/api/ApiConsumidor;


# instance fields
.field controller:Lcom/itau/empresas/feature/home/CobrancaController;

.field graficoCobranca:Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

.field llCardCobrancaDetalhe:Landroid/widget/LinearLayout;

.field textoCardCobrancaSemMovimentacao:Landroid/widget/TextView;

.field textoValorTotalReceber:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 37
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/view/ICardView;-><init>(Landroid/content/Context;)V

    .line 38
    return-void
.end method


# virtual methods
.method afterViews()V
    .registers 3

    .line 52
    invoke-static {p0}, Lcom/itau/empresas/ui/util/EventBusUtils;->registraEventBus(Landroid/view/View;)V

    .line 53
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->preferenciasTAG()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->buscaEstadoWidget(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 54
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->fazerRequest()V

    .line 56
    :cond_10
    const-string v0, ""

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->setTextoVerMaisDireito(Ljava/lang/String;Z)V

    .line 57
    const-string v0, ""

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->setTextoVerMaisEsquerdo(Ljava/lang/String;Z)V

    .line 58
    return-void
.end method

.method protected fazerRequest()V
    .registers 7

    .line 65
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->setCarregando(Z)V

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->controller:Lcom/itau/empresas/feature/home/CobrancaController;

    const-string v2, "data_movimentacao"

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/itau/empresas/feature/home/CobrancaController;->consultaWidget(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    return-void
.end method

.method protected fecharCard()V
    .registers 3

    .line 71
    invoke-super {p0}, Lcom/itau/empresas/feature/home/view/ICardView;->fecharCard()V

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->llCardCobrancaDetalhe:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->textoCardCobrancaSemMovimentacao:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->divisorCard:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 75
    return-void
.end method

.method protected layoutResId()I
    .registers 2

    .line 42
    const v0, 0x7f030155

    return v0
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 4
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 92
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 93
    return-void

    .line 96
    :cond_7
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->setCarregando(Z)V

    .line 97
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->textoCardCobrancaSemMovimentacao:Landroid/widget/TextView;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->textoCardCobrancaSemMovimentacao:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->llCardCobrancaDetalhe:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 100
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->expandirCard()V

    .line 101
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoWidgetCobranca;)V
    .registers 4
    .param p1, "eventoWidgetCobranca"    # Lcom/itau/empresas/Evento$EventoWidgetCobranca;

    .line 78
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->setCarregando(Z)V

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->graficoCobranca:Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

    invoke-static {}, Lcom/itau/empresas/Evento$EventoWidgetCobranca;->getCobrancas()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;->init(Ljava/util/List;)V

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->llCardCobrancaDetalhe:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 82
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->textoCardCobrancaSemMovimentacao:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 83
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->expandirCard()V

    .line 84
    return-void
.end method

.method public onEventMainThread(Ljava/lang/Float;)V
    .registers 5
    .param p1, "total"    # Ljava/lang/Float;

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->textoValorTotalReceber:Landroid/widget/TextView;

    .line 88
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    .line 87
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 105
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "francesinha"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected preferenciasTAG()Ljava/lang/String;
    .registers 2

    .line 47
    const-string v0, "card_cobranca"

    return-object v0
.end method

.method public setTextoTituloCard(Ljava/lang/String;)V
    .registers 3
    .param p1, "titulo"    # Ljava/lang/String;

    .line 61
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->textoTituloCard:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    return-void
.end method

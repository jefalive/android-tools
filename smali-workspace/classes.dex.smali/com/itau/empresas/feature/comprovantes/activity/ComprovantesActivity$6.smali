.class Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$6;
.super Ljava/lang/Object;
.source "ComprovantesActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->preparaDialogDeFiltro()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    .line 238
    iput-object p1, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$6;->this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 9
    .param p1, "v"    # Landroid/view/View;

    .line 241
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$6;->this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    iget-object v1, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$6;->this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    iget-object v1, v1, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->dialogFiltro:Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;

    invoke-virtual {v1}, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;->getEscolhePeriodo()Lbr/com/itau/library/LinearValuePickerView;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/library/LinearValuePickerView;->getSelectedPosition()I

    move-result v1

    # setter for: Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->diaSelecionadoIndex:I
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->access$102(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;I)I

    .line 242
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$6;->this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->dialogFiltro:Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;

    iget-object v1, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$6;->this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    # getter for: Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->diaSelecionadoIndex:I
    invoke-static {v1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->access$100(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;->getDia(I)I

    move-result v6

    .line 243
    .local v6, "dataSelecionada":I
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$6;->this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    iget-object v1, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$6;->this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    const v2, 0x7f0702b0

    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$6;->this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    .line 244
    const v3, 0x7f07031d

    invoke-virtual {v2, v3}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/Long;

    int-to-long v4, v6

    invoke-direct {v3, v4, v5}, Ljava/lang/Long;-><init>(J)V

    .line 243
    invoke-virtual {v0, v1, v2, v3}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->analyticsHit(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 246
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$6;->this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->dialogFiltro:Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;->dismiss()V

    .line 247
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity$6;->this$0:Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;

    invoke-static {v6}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->subtraiDiasDeHoje(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->carregarListaComprovantes(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;->access$200(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesActivity;Ljava/lang/String;)V

    .line 248
    return-void
.end method

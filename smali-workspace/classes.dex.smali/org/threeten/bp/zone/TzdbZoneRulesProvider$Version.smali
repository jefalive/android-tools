.class Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;
.super Ljava/lang/Object;
.source "TzdbZoneRulesProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/threeten/bp/zone/TzdbZoneRulesProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Version"
.end annotation


# instance fields
.field private final regionArray:[Ljava/lang/String;

.field private final ruleData:Ljava/util/concurrent/atomic/AtomicReferenceArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReferenceArray<Ljava/lang/Object;>;"
        }
    .end annotation
.end field

.field private final ruleIndices:[S

.field private final versionId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;[Ljava/lang/String;[SLjava/util/concurrent/atomic/AtomicReferenceArray;)V
    .registers 5
    .param p1, "versionId"    # Ljava/lang/String;
    .param p2, "regionIds"    # [Ljava/lang/String;
    .param p3, "ruleIndices"    # [S
    .param p4, "ruleData"    # Ljava/util/concurrent/atomic/AtomicReferenceArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;[Ljava/lang/String;[SLjava/util/concurrent/atomic/AtomicReferenceArray<Ljava/lang/Object;>;)V"
        }
    .end annotation

    .line 293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 294
    iput-object p4, p0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;->ruleData:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    .line 295
    iput-object p1, p0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;->versionId:Ljava/lang/String;

    .line 296
    iput-object p2, p0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;->regionArray:[Ljava/lang/String;

    .line 297
    iput-object p3, p0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;->ruleIndices:[S

    .line 298
    return-void
.end method

.method static synthetic access$000(Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;

    .line 287
    iget-object v0, p0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;->versionId:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method createRule(S)Lorg/threeten/bp/zone/ZoneRules;
    .registers 6
    .param p1, "index"    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 313
    iget-object v0, p0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;->ruleData:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 314
    .local v1, "obj":Ljava/lang/Object;
    instance-of v0, v1, [B

    if-eqz v0, :cond_23

    .line 315
    move-object v0, v1

    check-cast v0, [B

    move-object v2, v0

    check-cast v2, [B

    .line 316
    .local v2, "bytes":[B
    new-instance v3, Ljava/io/DataInputStream;

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v3, v0}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 317
    .local v3, "dis":Ljava/io/DataInputStream;
    invoke-static {v3}, Lorg/threeten/bp/zone/Ser;->read(Ljava/io/DataInput;)Ljava/lang/Object;

    move-result-object v1

    .line 318
    iget-object v0, p0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;->ruleData:Ljava/util/concurrent/atomic/AtomicReferenceArray;

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/atomic/AtomicReferenceArray;->set(ILjava/lang/Object;)V

    .line 320
    .end local v2    # "bytes":[B
    .end local v3    # "dis":Ljava/io/DataInputStream;
    :cond_23
    move-object v0, v1

    check-cast v0, Lorg/threeten/bp/zone/ZoneRules;

    return-object v0
.end method

.method getRules(Ljava/lang/String;)Lorg/threeten/bp/zone/ZoneRules;
    .registers 7
    .param p1, "regionId"    # Ljava/lang/String;

    .line 301
    iget-object v0, p0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;->regionArray:[Ljava/lang/String;

    invoke-static {v0, p1}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    .line 302
    .local v3, "regionIndex":I
    if-gez v3, :cond_a

    .line 303
    const/4 v0, 0x0

    return-object v0

    .line 306
    :cond_a
    :try_start_a
    iget-object v0, p0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;->ruleIndices:[S

    aget-short v0, v0, v3

    invoke-virtual {p0, v0}, Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;->createRule(S)Lorg/threeten/bp/zone/ZoneRules;
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_11} :catch_13

    move-result-object v0

    return-object v0

    .line 307
    :catch_13
    move-exception v4

    .line 308
    .local v4, "ex":Ljava/lang/Exception;
    new-instance v0, Lorg/threeten/bp/zone/ZoneRulesException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid binary time-zone data: TZDB:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", version: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;->versionId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v4}, Lorg/threeten/bp/zone/ZoneRulesException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .line 325
    iget-object v0, p0, Lorg/threeten/bp/zone/TzdbZoneRulesProvider$Version;->versionId:Ljava/lang/String;

    return-object v0
.end method

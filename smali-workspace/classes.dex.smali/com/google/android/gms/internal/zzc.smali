.class public Lcom/google/android/gms/internal/zzc;
.super Ljava/lang/Thread;


# static fields
.field private static final DEBUG:Z


# instance fields
.field private final zzh:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<Lcom/google/android/gms/internal/zzk<*>;>;"
        }
    .end annotation
.end field

.field private final zzi:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue<Lcom/google/android/gms/internal/zzk<*>;>;"
        }
    .end annotation
.end field

.field private final zzj:Lcom/google/android/gms/internal/zzb;

.field private final zzk:Lcom/google/android/gms/internal/zzn;

.field private volatile zzl:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    sget-boolean v0, Lcom/google/android/gms/internal/zzs;->DEBUG:Z

    sput-boolean v0, Lcom/google/android/gms/internal/zzc;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/BlockingQueue;Lcom/google/android/gms/internal/zzb;Lcom/google/android/gms/internal/zzn;)V
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/concurrent/BlockingQueue<Lcom/google/android/gms/internal/zzk<*>;>;Ljava/util/concurrent/BlockingQueue<Lcom/google/android/gms/internal/zzk<*>;>;Lcom/google/android/gms/internal/zzb;Lcom/google/android/gms/internal/zzn;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzc;->zzl:Z

    iput-object p1, p0, Lcom/google/android/gms/internal/zzc;->zzh:Ljava/util/concurrent/BlockingQueue;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzc;->zzi:Ljava/util/concurrent/BlockingQueue;

    iput-object p3, p0, Lcom/google/android/gms/internal/zzc;->zzj:Lcom/google/android/gms/internal/zzb;

    iput-object p4, p0, Lcom/google/android/gms/internal/zzc;->zzk:Lcom/google/android/gms/internal/zzn;

    return-void
.end method

.method static synthetic zza(Lcom/google/android/gms/internal/zzc;)Ljava/util/concurrent/BlockingQueue;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzc;->zzi:Ljava/util/concurrent/BlockingQueue;

    return-object v0
.end method


# virtual methods
.method public quit()V
    .registers 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzc;->zzl:Z

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzc;->interrupt()V

    return-void
.end method

.method public run()V
    .registers 7

    sget-boolean v0, Lcom/google/android/gms/internal/zzc;->DEBUG:Z

    if-eqz v0, :cond_c

    const-string v0, "start new dispatcher"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzs;->zza(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_c
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzc;->zzj:Lcom/google/android/gms/internal/zzb;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzb;->zza()V

    :goto_16
    :try_start_16
    iget-object v0, p0, Lcom/google/android/gms/internal/zzc;->zzh:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/google/android/gms/internal/zzk;

    const-string v0, "cache-queue-take"

    invoke-virtual {v3, v0}, Lcom/google/android/gms/internal/zzk;->zzc(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/gms/internal/zzk;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_30

    const-string v0, "cache-discard-canceled"

    invoke-virtual {v3, v0}, Lcom/google/android/gms/internal/zzk;->zzd(Ljava/lang/String;)V
    :try_end_2f
    .catch Ljava/lang/InterruptedException; {:try_start_16 .. :try_end_2f} :catch_95

    goto :goto_16

    :cond_30
    :try_start_30
    iget-object v0, p0, Lcom/google/android/gms/internal/zzc;->zzj:Lcom/google/android/gms/internal/zzb;

    invoke-virtual {v3}, Lcom/google/android/gms/internal/zzk;->zzh()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/zzb;->zza(Ljava/lang/String;)Lcom/google/android/gms/internal/zzb$zza;

    move-result-object v4

    if-nez v4, :cond_47

    const-string v0, "cache-miss"

    invoke-virtual {v3, v0}, Lcom/google/android/gms/internal/zzk;->zzc(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzc;->zzi:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, v3}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_46
    .catch Ljava/lang/InterruptedException; {:try_start_30 .. :try_end_46} :catch_95

    goto :goto_16

    :cond_47
    :try_start_47
    invoke-virtual {v4}, Lcom/google/android/gms/internal/zzb$zza;->zzb()Z

    move-result v0

    if-eqz v0, :cond_5b

    const-string v0, "cache-hit-expired"

    invoke-virtual {v3, v0}, Lcom/google/android/gms/internal/zzk;->zzc(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/google/android/gms/internal/zzk;->zza(Lcom/google/android/gms/internal/zzb$zza;)Lcom/google/android/gms/internal/zzk;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzc;->zzi:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, v3}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_5a
    .catch Ljava/lang/InterruptedException; {:try_start_47 .. :try_end_5a} :catch_95

    goto :goto_16

    :cond_5b
    const-string v0, "cache-hit"

    :try_start_5d
    invoke-virtual {v3, v0}, Lcom/google/android/gms/internal/zzk;->zzc(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/gms/internal/zzi;

    iget-object v1, v4, Lcom/google/android/gms/internal/zzb$zza;->data:[B

    iget-object v2, v4, Lcom/google/android/gms/internal/zzb$zza;->zzg:Ljava/util/Map;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/internal/zzi;-><init>([BLjava/util/Map;)V

    invoke-virtual {v3, v0}, Lcom/google/android/gms/internal/zzk;->zza(Lcom/google/android/gms/internal/zzi;)Lcom/google/android/gms/internal/zzm;

    move-result-object v5

    const-string v0, "cache-hit-parsed"

    invoke-virtual {v3, v0}, Lcom/google/android/gms/internal/zzk;->zzc(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/google/android/gms/internal/zzb$zza;->zzc()Z

    move-result v0

    if-nez v0, :cond_7e

    iget-object v0, p0, Lcom/google/android/gms/internal/zzc;->zzk:Lcom/google/android/gms/internal/zzn;

    invoke-interface {v0, v3, v5}, Lcom/google/android/gms/internal/zzn;->zza(Lcom/google/android/gms/internal/zzk;Lcom/google/android/gms/internal/zzm;)V

    goto :goto_93

    :cond_7e
    const-string v0, "cache-hit-refresh-needed"

    invoke-virtual {v3, v0}, Lcom/google/android/gms/internal/zzk;->zzc(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/google/android/gms/internal/zzk;->zza(Lcom/google/android/gms/internal/zzb$zza;)Lcom/google/android/gms/internal/zzk;

    const/4 v0, 0x1

    iput-boolean v0, v5, Lcom/google/android/gms/internal/zzm;->zzai:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/zzc;->zzk:Lcom/google/android/gms/internal/zzn;

    new-instance v1, Lcom/google/android/gms/internal/zzc$1;

    invoke-direct {v1, p0, v3}, Lcom/google/android/gms/internal/zzc$1;-><init>(Lcom/google/android/gms/internal/zzc;Lcom/google/android/gms/internal/zzk;)V

    invoke-interface {v0, v3, v5, v1}, Lcom/google/android/gms/internal/zzn;->zza(Lcom/google/android/gms/internal/zzk;Lcom/google/android/gms/internal/zzm;Ljava/lang/Runnable;)V
    :try_end_93
    .catch Ljava/lang/InterruptedException; {:try_start_5d .. :try_end_93} :catch_95

    :goto_93
    goto/16 :goto_16

    :catch_95
    move-exception v3

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzc;->zzl:Z

    if-eqz v0, :cond_9b

    return-void

    :cond_9b
    goto/16 :goto_16
.end method

.class public Lcom/itau/empresas/ui/view/FiltroExtratoView;
.super Landroid/widget/LinearLayout;
.source "FiltroExtratoView.java"


# instance fields
.field dias:[Ljava/lang/String;

.field linearvaluepicker:Lbr/com/itau/library/LinearValuePickerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 28
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method


# virtual methods
.method aposCarregarView()V
    .registers 3

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/ui/view/FiltroExtratoView;->linearvaluepicker:Lbr/com/itau/library/LinearValuePickerView;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/FiltroExtratoView;->dias:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbr/com/itau/library/LinearValuePickerView;->setValues([Ljava/lang/String;)V

    .line 48
    return-void
.end method

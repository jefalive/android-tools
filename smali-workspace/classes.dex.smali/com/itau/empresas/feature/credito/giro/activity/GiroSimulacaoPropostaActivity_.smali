.class public final Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;
.super Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;
.source "GiroSimulacaoPropostaActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 44
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;-><init>()V

    .line 48
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;

    .line 44
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;

    .line 44
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 64
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 65
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 66
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 67
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->simulacaoController:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;

    .line 68
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->simulacaoPropostaController:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController;

    .line 69
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->injectExtras_()V

    .line 70
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->afterInject()V

    .line 71
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 128
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 129
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_50

    .line 130
    const-string v0, "ofertaProdutosParceladosSaida"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 131
    const-string v0, "ofertaProdutosParceladosSaida"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->ofertaProdutosParceladosSaida:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    .line 133
    :cond_1c
    const-string v0, "ofertaEspecialAceita"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 134
    const-string v0, "ofertaEspecialAceita"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 136
    :cond_2e
    const-string v0, "simulacaoPropostaSaidaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 137
    const-string v0, "simulacaoPropostaSaidaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 139
    :cond_40
    const-string v0, "modoPersonalizado"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_50

    .line 140
    const-string v0, "modoPersonalizado"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->modoPersonalizado:Z

    .line 143
    :cond_50
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 168
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_$3;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 176
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 156
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_$2;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 164
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 57
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->init_(Landroid/os/Bundle;)V

    .line 58
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 60
    const v0, 0x7f03003a

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->setContentView(I)V

    .line 61
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 105
    const v0, 0x7f0e019e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->root:Landroid/widget/RelativeLayout;

    .line 106
    const v0, 0x7f0e0196

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->campoValorEmprestimo:Landroid/widget/EditText;

    .line 107
    const v0, 0x7f0e01a3

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->campoDataEmprestimo:Landroid/widget/EditText;

    .line 108
    const v0, 0x7f0e01a7

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->textoParcelasComSeguroAval:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f0e018f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->botaoContinuar:Landroid/widget/Button;

    .line 110
    const v0, 0x7f0e01a9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->giroPersonalizadoParcelasView:Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;

    .line 111
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 112
    const v0, 0x7f0e01a8

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->tomadaDecisaoSeguroView:Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;

    .line 113
    const v0, 0x7f0e01ac

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->textoValoresTarifaContratacao:Landroid/widget/TextView;

    .line 114
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->botaoContinuar:Landroid/widget/Button;

    if-eqz v0, :cond_71

    .line 115
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->botaoContinuar:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_$1;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    :cond_71
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->aoCarregarTela()V

    .line 125
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 75
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->setContentView(I)V

    .line 76
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 77
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 87
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->setContentView(Landroid/view/View;)V

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 89
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 81
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 82
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 83
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 147
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->setIntent(Landroid/content/Intent;)V

    .line 148
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_;->injectExtras_()V

    .line 149
    return-void
.end method

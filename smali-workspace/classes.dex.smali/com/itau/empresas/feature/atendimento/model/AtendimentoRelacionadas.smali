.class public Lcom/itau/empresas/feature/atendimento/model/AtendimentoRelacionadas;
.super Ljava/lang/Object;
.source "AtendimentoRelacionadas.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private id:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id"
    .end annotation
.end field

.field private titulo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "titulo"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .registers 2

    .line 16
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoRelacionadas;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getTitulo()Ljava/lang/String;
    .registers 2

    .line 20
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoRelacionadas;->titulo:Ljava/lang/String;

    return-object v0
.end method

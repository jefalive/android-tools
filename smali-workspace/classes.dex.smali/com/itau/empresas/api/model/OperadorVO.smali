.class public Lcom/itau/empresas/api/model/OperadorVO;
.super Ljava/lang/Object;
.source "OperadorVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private chaveAcesso:Ljava/util/Map;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "status_chave_acesso"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field private final codigoOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_operador"
    .end annotation
.end field

.field private final nomeOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_operador"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .param p1, "nomeOperador"    # Ljava/lang/String;
    .param p2, "codigoOperador"    # Ljava/lang/String;

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/itau/empresas/api/model/OperadorVO;->nomeOperador:Ljava/lang/String;

    .line 21
    iput-object p2, p0, Lcom/itau/empresas/api/model/OperadorVO;->codigoOperador:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method public getChaveAcesso()Ljava/util/Map;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/itau/empresas/api/model/OperadorVO;->chaveAcesso:Ljava/util/Map;

    return-object v0
.end method

.method public getCodigoOperador()Ljava/lang/String;
    .registers 2

    .line 25
    iget-object v0, p0, Lcom/itau/empresas/api/model/OperadorVO;->codigoOperador:Ljava/lang/String;

    return-object v0
.end method

.method public getNomeOperador()Ljava/lang/String;
    .registers 2

    .line 29
    iget-object v0, p0, Lcom/itau/empresas/api/model/OperadorVO;->nomeOperador:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/api/model/OperadorVO;->codigoOperador:Ljava/lang/String;

    return-object v0
.end method

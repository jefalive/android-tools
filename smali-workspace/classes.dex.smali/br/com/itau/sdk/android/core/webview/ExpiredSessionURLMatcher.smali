.class public final Lbr/com/itau/sdk/android/core/webview/ExpiredSessionURLMatcher;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final $:[B

.field private static $$:I

.field private static final EXPIRED_SESSION:Ljava/util/regex/Pattern;

.field private static final MOBILE_REGEX:Ljava/util/regex/Pattern;

.field private static final SIGN_IN_REGEX:Ljava/util/regex/Pattern;


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    sget-object v5, Lbr/com/itau/sdk/android/core/webview/ExpiredSessionURLMatcher;->$:[B

    mul-int/lit8 p1, p1, 0x3

    rsub-int/lit8 p1, p1, 0x3c

    const/4 v4, 0x0

    mul-int/lit8 p0, p0, 0x2

    add-int/lit8 p0, p0, 0x2e

    rsub-int/lit8 p2, p2, 0x1f

    new-instance v0, Ljava/lang/String;

    new-array v1, p2, [B

    if-nez v5, :cond_18

    move v2, p0

    move v3, p2

    :goto_15
    add-int/2addr v2, v3

    add-int/lit8 p0, v2, -0xe

    :cond_18
    move v2, v4

    add-int/lit8 v4, v4, 0x1

    int-to-byte v3, p0

    aput-byte v3, v1, v2

    if-ne v4, p2, :cond_25

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_25
    move v2, p0

    add-int/lit8 p1, p1, 0x1

    aget-byte v3, v5, p1

    goto :goto_15
.end method

.method static constructor <clinit>()V
    .registers 3

    .line 9
    const/16 v0, 0x56

    new-array v0, v0, [B

    fill-array-data v0, :array_48

    sput-object v0, Lbr/com/itau/sdk/android/core/webview/ExpiredSessionURLMatcher;->$:[B

    const/16 v0, 0x62

    sput v0, Lbr/com/itau/sdk/android/core/webview/ExpiredSessionURLMatcher;->$$:I

    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ExpiredSessionURLMatcher;->$:[B

    const/16 v1, 0xb

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ExpiredSessionURLMatcher;->$:[B

    const/16 v2, 0x24

    aget-byte v1, v1, v2

    const/4 v2, 0x0

    invoke-static {v2, v0, v1}, Lbr/com/itau/sdk/android/core/webview/ExpiredSessionURLMatcher;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/itau/sdk/android/core/webview/ExpiredSessionURLMatcher;->SIGN_IN_REGEX:Ljava/util/regex/Pattern;

    .line 10
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ExpiredSessionURLMatcher;->$:[B

    const/4 v1, 0x4

    aget-byte v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lbr/com/itau/sdk/android/core/webview/ExpiredSessionURLMatcher;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/itau/sdk/android/core/webview/ExpiredSessionURLMatcher;->MOBILE_REGEX:Ljava/util/regex/Pattern;

    .line 11
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ExpiredSessionURLMatcher;->$:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Lbr/com/itau/sdk/android/core/webview/ExpiredSessionURLMatcher;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/itau/sdk/android/core/webview/ExpiredSessionURLMatcher;->EXPIRED_SESSION:Ljava/util/regex/Pattern;

    return-void

    nop

    :array_48
    .array-data 1
        0x5t
        -0x7t
        0x3et
        -0x1at
        0xat
        0xct
        0x15t
        0x2bt
        0x31t
        0x6t
        0x10t
        0x13t
        -0x21t
        -0x9t
        0x41t
        0xft
        0x29t
        -0xdt
        0x2et
        -0x11t
        -0x24t
        0xct
        0x41t
        -0x20t
        0x41t
        0x20t
        0xbt
        0x16t
        -0x41t
        0x13t
        0xat
        0xat
        0xct
        0x15t
        0x28t
        0x33t
        0x3t
        0x10t
        0x17t
        0x14t
        0x3t
        0x1bt
        0x7t
        -0x20t
        0x3dt
        0xat
        0xbt
        0x8t
        0xct
        0x21t
        0x3t
        0x1bt
        0x7t
        -0x33t
        0x41t
        0x20t
        0xbt
        0x16t
        -0x41t
        0x13t
        0xat
        0xat
        0xct
        0x15t
        0x32t
        0x20t
        0x1ct
        0xet
        -0x4t
        0x1ct
        -0x1ct
        0x41t
        0x6t
        0x7t
        0x17t
        -0x3t
        0x11t
        0xbt
        -0x25t
        0x41t
        0x20t
        0xbt
        0x16t
        -0x41t
        0x13t
        0xat
    .end array-data
.end method

.method private constructor <init>()V
    .registers 1

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method

.method public static isExpiredUrl(Ljava/lang/String;)Z
    .registers 3

    .line 18
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ExpiredSessionURLMatcher;->SIGN_IN_REGEX:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_18

    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ExpiredSessionURLMatcher;->MOBILE_REGEX:Ljava/util/regex/Pattern;

    .line 19
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_1a

    :cond_18
    const/4 v1, 0x1

    goto :goto_1b

    :cond_1a
    const/4 v1, 0x0

    .line 21
    :goto_1b
    if-eqz v1, :cond_23

    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->isValidSession()Z

    move-result v0

    if-eqz v0, :cond_2f

    :cond_23
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ExpiredSessionURLMatcher;->EXPIRED_SESSION:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_31

    :cond_2f
    const/4 v0, 0x1

    goto :goto_32

    :cond_31
    const/4 v0, 0x0

    :goto_32
    return v0
.end method

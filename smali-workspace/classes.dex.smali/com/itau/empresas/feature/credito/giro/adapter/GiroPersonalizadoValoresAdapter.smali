.class public Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;
.super Landroid/widget/BaseAdapter;
.source "GiroPersonalizadoValoresAdapter.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mListValoresEmprestimo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;>;"
        }
    .end annotation
.end field

.field valorMaximo:F

.field valorMinimo:F


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 26
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method private adicionarItemOutroValor()V
    .registers 6

    .line 91
    const/4 v2, 0x0

    .line 93
    .local v2, "jaExisteItemOutroValor":Z
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->mListValoresEmprestimo:Ljava/util/List;

    if-eqz v0, :cond_47

    .line 95
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->mListValoresEmprestimo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;

    .line 97
    .local v4, "valoresEmprestimo":Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;->getEnumValoresEmprestimo()Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    move-result-object v0

    if-eqz v0, :cond_2b

    .line 98
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;->getEnumValoresEmprestimo()Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;->OUTRO_VALOR:Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    .line 99
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 101
    const/4 v2, 0x1

    .line 103
    .end local v4    # "valoresEmprestimo":Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;
    :cond_2b
    goto :goto_b

    .line 105
    :cond_2c
    if-nez v2, :cond_47

    .line 107
    new-instance v3, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;

    invoke-direct {v3}, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;-><init>()V

    .line 108
    .local v3, "parcelasEmprestimo":Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->valorMaximo:F

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;->setValorMaximo(F)V

    .line 109
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->valorMinimo:F

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;->setValorMinimo(F)V

    .line 110
    sget-object v0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;->OUTRO_VALOR:Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    .line 111
    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;->setEnumValoresEmprestimo(Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;)V

    .line 113
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->mListValoresEmprestimo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    .end local v3    # "parcelasEmprestimo":Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;
    :cond_47
    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 2

    .line 157
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->mListValoresEmprestimo:Ljava/util/List;

    if-eqz v0, :cond_b

    .line 159
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->mListValoresEmprestimo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0

    .line 162
    :cond_b
    const/4 v0, 0x0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 3
    .param p1, "position"    # I

    .line 167
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->mListValoresEmprestimo:Ljava/util/List;

    if-eqz v0, :cond_b

    .line 169
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->mListValoresEmprestimo:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 172
    :cond_b
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .param p1, "position"    # I

    .line 176
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .registers 4
    .param p1, "position"    # I

    .line 237
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->mListValoresEmprestimo:Ljava/util/List;

    if-eqz v0, :cond_3e

    .line 239
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->mListValoresEmprestimo:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;->getEnumValoresEmprestimo()Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    move-result-object v0

    if-eqz v0, :cond_3c

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->mListValoresEmprestimo:Ljava/util/List;

    .line 240
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;->getEnumValoresEmprestimo()Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;->CARREGADO_DA_API:Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    .line 241
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3a

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->mListValoresEmprestimo:Ljava/util/List;

    .line 242
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;->getEnumValoresEmprestimo()Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;->CARREGADO_MANUALMENTE:Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    .line 243
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 245
    :cond_3a
    const/4 v0, 0x0

    return v0

    .line 248
    :cond_3c
    const/4 v0, 0x1

    return v0

    .line 252
    :cond_3e
    const/4 v0, 0x0

    return v0
.end method

.method public getItems()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;>;"
        }
    .end annotation

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->mListValoresEmprestimo:Ljava/util/List;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 13
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .line 182
    move-object v2, p2

    .line 183
    .local v2, "view":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->getItemViewType(I)I

    move-result v3

    .line 185
    .local v3, "viewType":I
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->mListValoresEmprestimo:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;

    .line 187
    .local v5, "valoresEmprestimo":Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;
    if-nez v2, :cond_2e

    .line 189
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/view/LayoutInflater;

    .line 191
    .local v4, "inflater":Landroid/view/LayoutInflater;
    if-nez v3, :cond_26

    .line 193
    const v0, 0x7f030115

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    goto :goto_2e

    .line 196
    :cond_26
    const v0, 0x7f030116

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 200
    .end local v4    # "inflater":Landroid/view/LayoutInflater;
    :cond_2e
    :goto_2e
    if-nez v3, :cond_4c

    .line 202
    const v0, 0x7f0e02d5

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/TextView;

    .line 204
    .local v6, "textoValor":Landroid/widget/TextView;
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v6, v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 206
    .line 208
    invoke-virtual {v5}, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;->getValoresEmprestimo()Ljava/math/BigDecimal;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v0

    .line 206
    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    .end local v6    # "textoValor":Landroid/widget/TextView;
    goto :goto_87

    .line 211
    :cond_4c
    const v0, 0x7f0e01b7

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/TextView;

    .line 212
    .line 213
    .local v6, "textoAuxiliar":Landroid/widget/TextView;
    const v0, 0x7f0e01ae

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/widget/RelativeLayout;

    .line 214
    .local v7, "rlContainer":Landroid/widget/RelativeLayout;
    const v0, 0x7f0e0555

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/widget/TextView;

    .line 216
    .local v8, "textoOutroValor":Landroid/widget/TextView;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0043

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 218
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v8, v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 220
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter$1;-><init>(Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;)V

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 228
    .end local v6    # "textoAuxiliar":Landroid/widget/TextView;
    .end local v7    # "rlContainer":Landroid/widget/RelativeLayout;
    .end local v8    # "textoOutroValor":Landroid/widget/TextView;
    :goto_87
    return-object v2
.end method

.method public getViewTypeCount()I
    .registers 2

    .line 232
    const/4 v0, 0x2

    return v0
.end method

.method public setContext(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 47
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->mContext:Landroid/content/Context;

    .line 48
    return-void
.end method

.method public setData(Ljava/util/List;FF)V
    .registers 7
    .param p1, "listValoresEmprestimo"    # Ljava/util/List;
    .param p2, "valorMinimo"    # F
    .param p3, "valorMaximo"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;>;FF)V"
        }
    .end annotation

    .line 53
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->mListValoresEmprestimo:Ljava/util/List;

    .line 54
    iput p2, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->valorMinimo:F

    .line 55
    iput p3, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->valorMaximo:F

    .line 57
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->mListValoresEmprestimo:Ljava/util/List;

    if-nez v0, :cond_11

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->mListValoresEmprestimo:Ljava/util/List;

    .line 63
    :cond_11
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->mListValoresEmprestimo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_17
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_30

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;

    .line 65
    .local v2, "valoresEmprestimoVO":Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;->getEnumValoresEmprestimo()Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    move-result-object v0

    if-nez v0, :cond_2f

    .line 67
    sget-object v0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;->CARREGADO_DA_API:Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    .line 68
    invoke-virtual {v2, v0}, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;->setEnumValoresEmprestimo(Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;)V

    .line 71
    .end local v2    # "valoresEmprestimoVO":Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;
    :cond_2f
    goto :goto_17

    .line 73
    :cond_30
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoValoresAdapter;->adicionarItemOutroValor()V

    .line 75
    return-void
.end method

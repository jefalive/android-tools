.class public Lcom/itau/empresas/feature/busca/BuscaGeral;
.super Ljava/lang/Object;
.source "BuscaGeral.java"


# instance fields
.field private menuAcesso:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private obterMenuAcesso(Ljava/util/List;)Ljava/util/ArrayList;
    .registers 7
    .param p1, "menuVOListApplication"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;)Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
        }
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/feature/busca/BuscaGeral;->menuAcesso:Ljava/util/ArrayList;

    if-nez v0, :cond_5f

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/busca/BuscaGeral;->menuAcesso:Ljava/util/ArrayList;

    .line 34
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_c
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5f

    .line 35
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/itau/empresas/api/model/MenuVO;

    .line 37
    .local v3, "menuVO":Lcom/itau/empresas/api/model/MenuVO;
    const-string v0, "in\u00edcio"

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/MenuVO;->getNome()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 38
    goto :goto_5c

    .line 41
    :cond_26
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_44

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_44

    .line 43
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v4

    .line 44
    .local v4, "menuFilhos":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/busca/BuscaGeral;->menuAcesso:Ljava/util/ArrayList;

    invoke-direct {p0, v4}, Lcom/itau/empresas/feature/busca/BuscaGeral;->retornaMenuFilhos(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 45
    .end local v4    # "menuFilhos":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;"
    .end local v4
    goto :goto_5c

    .line 46
    :cond_44
    new-instance v4, Lbr/com/itau/textovoz/model/Menu;

    invoke-direct {v4}, Lbr/com/itau/textovoz/model/Menu;-><init>()V

    .line 47
    .local v4, "menu":Lbr/com/itau/textovoz/model/Menu;
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/MenuVO;->getNome()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lbr/com/itau/textovoz/model/Menu;->setName(Ljava/lang/String;)V

    .line 48
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lbr/com/itau/textovoz/model/Menu;->setOpKey(Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/busca/BuscaGeral;->menuAcesso:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    .end local v3    # "menuVO":Lcom/itau/empresas/api/model/MenuVO;
    .end local v4    # "menu":Lbr/com/itau/textovoz/model/Menu;
    :goto_5c
    add-int/lit8 v2, v2, 0x1

    goto :goto_c

    .line 54
    .end local v2    # "i":I
    :cond_5f
    iget-object v0, p0, Lcom/itau/empresas/feature/busca/BuscaGeral;->menuAcesso:Ljava/util/ArrayList;

    return-object v0
.end method

.method private retornaMenuFilhos(Ljava/util/List;)Ljava/util/ArrayList;
    .registers 6
    .param p1, "menuList"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;)Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
        }
    .end annotation

    .line 58
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 59
    .local v1, "retorno":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_6
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_31

    .line 60
    new-instance v3, Lbr/com/itau/textovoz/model/Menu;

    invoke-direct {v3}, Lbr/com/itau/textovoz/model/Menu;-><init>()V

    .line 61
    .local v3, "menu":Lbr/com/itau/textovoz/model/Menu;
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getNome()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lbr/com/itau/textovoz/model/Menu;->setName(Ljava/lang/String;)V

    .line 62
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lbr/com/itau/textovoz/model/Menu;->setOpKey(Ljava/lang/String;)V

    .line 63
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    .end local v3    # "menu":Lbr/com/itau/textovoz/model/Menu;
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 65
    .end local v2    # "j":I
    :cond_31
    return-object v1
.end method


# virtual methods
.method public getHomeSearchIntentComExtra(Landroid/content/Context;Lcom/itau/empresas/CustomApplication;)Landroid/content/Intent;
    .registers 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "application"    # Lcom/itau/empresas/CustomApplication;

    .line 107
    .line 108
    invoke-virtual {p2}, Lcom/itau/empresas/CustomApplication;->getMenuVO()Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/busca/BuscaGeral;->obterMenuAcesso(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    .line 110
    .local v2, "listaMenuComponente":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
    sget-object v3, Lbr/com/itau/textovoz/util/SearchContext;->DEFAULT:Lbr/com/itau/textovoz/util/SearchContext;

    .line 112
    .local v3, "searchContext":Lbr/com/itau/textovoz/util/SearchContext;
    new-instance v4, Landroid/content/Intent;

    const-class v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    invoke-direct {v4, p1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 113
    .local v4, "intent":Landroid/content/Intent;
    const v0, 0x7f07048d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    const v0, 0x7f070474

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 116
    const v0, 0x7f0706ca

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lbr/com/itau/textovoz/util/ApiUtils$Segment;->DEFAULT:Lbr/com/itau/textovoz/util/ApiUtils$Segment;

    invoke-virtual {v1}, Lbr/com/itau/textovoz/util/ApiUtils$Segment;->getItem()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 117
    const v0, 0x7f07023c

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 118
    const v0, 0x7f07023b

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 119
    const v0, 0x7f07023a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;->BALANCE:Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;

    .line 120
    invoke-virtual {v1}, Lbr/com/itau/textovoz/util/ApiUtils$ApiCard;->getItem()Ljava/lang/String;

    move-result-object v1

    .line 119
    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 121
    const v0, 0x7f07023f

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;->TRANSACTION:Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;

    .line 122
    invoke-virtual {v1}, Lbr/com/itau/textovoz/util/ApiUtils$ApiTransaction;->getItem()Ljava/lang/String;

    move-result-object v1

    .line 121
    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 123
    const v0, 0x7f070239

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 124
    const v0, 0x7f07023d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 125
    const v0, 0x7f07023e

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 126
    const v0, 0x7f0701b7

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 128
    return-object v4
.end method

.method public podeExibirBuscaGeral(Lcom/itau/empresas/CustomApplication;)Z
    .registers 3
    .param p1, "application"    # Lcom/itau/empresas/CustomApplication;

    .line 98
    invoke-virtual {p1}, Lcom/itau/empresas/CustomApplication;->getPermissaoAutorizante()Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 99
    invoke-virtual {p1}, Lcom/itau/empresas/CustomApplication;->getPermissaoAutorizante()Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_17

    .line 101
    sget-object v0, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_WIDGET_BUSCA:Lcom/itau/empresas/CustomApplication$Exibicao;

    invoke-virtual {p1, v0}, Lcom/itau/empresas/CustomApplication;->verificarPermissao(Lcom/itau/empresas/CustomApplication$Exibicao;)Z

    move-result v0

    return v0

    .line 103
    :cond_17
    const/4 v0, 0x0

    return v0
.end method

.method public redirecionarParaActivityBuscada(Landroid/content/Intent;Landroid/support/v7/app/AppCompatActivity;)V
    .registers 10
    .param p1, "data"    # Landroid/content/Intent;
    .param p2, "activity"    # Landroid/support/v7/app/AppCompatActivity;

    .line 69
    const v0, 0x7f070209

    invoke-virtual {p2, v0}, Landroid/support/v7/app/AppCompatActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 71
    .local v2, "objectExtract":Z
    if-eqz v2, :cond_12

    .line 72
    invoke-static {p2}, Lcom/itau/empresas/feature/busca/BuscaGeralNavigation;->irParaContaCorrente(Landroid/support/v7/app/AppCompatActivity;)V

    .line 73
    return-void

    .line 76
    :cond_12
    const v0, 0x7f070126

    invoke-virtual {p2, v0}, Landroid/support/v7/app/AppCompatActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 78
    .local v3, "objectCardViewTel":Z
    if-eqz v3, :cond_24

    .line 79
    invoke-static {p2}, Lcom/itau/empresas/feature/busca/BuscaGeralNavigation;->irParaAtendimento(Landroid/support/v7/app/AppCompatActivity;)V

    .line 80
    return-void

    .line 83
    :cond_24
    const v0, 0x7f070475

    invoke-virtual {p2, v0}, Landroid/support/v7/app/AppCompatActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    .line 84
    .local v4, "objectMenu":Ljava/lang/Object;
    if-eqz v4, :cond_3c

    .line 85
    move-object v5, v4

    check-cast v5, Lbr/com/itau/textovoz/model/Menu;

    .line 86
    .local v5, "menu":Lbr/com/itau/textovoz/model/Menu;
    invoke-virtual {v5}, Lbr/com/itau/textovoz/model/Menu;->getOpKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/itau/empresas/feature/busca/BuscaGeralNavigation;->abreLink(Landroid/support/v7/app/AppCompatActivity;Ljava/lang/String;)V

    .line 87
    return-void

    .line 90
    .end local v5    # "menu":Lbr/com/itau/textovoz/model/Menu;
    :cond_3c
    const v0, 0x7f070211

    invoke-virtual {p2, v0}, Landroid/support/v7/app/AppCompatActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    .line 91
    .local v5, "objectFaq":Ljava/lang/Object;
    if-eqz v5, :cond_4f

    .line 92
    move-object v6, v5

    check-cast v6, Lbr/com/itau/textovoz/model/Faq;

    .line 93
    .local v6, "faq":Lbr/com/itau/textovoz/model/Faq;
    invoke-static {v6, p2}, Lcom/itau/empresas/feature/busca/BuscaGeralNavigation;->irParaFaqs(Lbr/com/itau/textovoz/model/Faq;Landroid/content/Context;)V

    .line 95
    .end local v6    # "faq":Lbr/com/itau/textovoz/model/Faq;
    :cond_4f
    return-void
.end method

.class final Lcom/adobe/mobile/MessageAlert$MessageShower$PositiveClickHandler;
.super Ljava/lang/Object;
.source "MessageAlert.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adobe/mobile/MessageAlert$MessageShower;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PositiveClickHandler"
.end annotation


# instance fields
.field private final message:Lcom/adobe/mobile/MessageAlert;


# direct methods
.method public constructor <init>(Lcom/adobe/mobile/MessageAlert;)V
    .registers 2
    .param p1, "messageToActOn"    # Lcom/adobe/mobile/MessageAlert;

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    iput-object p1, p0, Lcom/adobe/mobile/MessageAlert$MessageShower$PositiveClickHandler;->message:Lcom/adobe/mobile/MessageAlert;

    .line 147
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 10
    .param p1, "dialogInterface"    # Landroid/content/DialogInterface;
    .param p2, "i"    # I

    .line 152
    iget-object v0, p0, Lcom/adobe/mobile/MessageAlert$MessageShower$PositiveClickHandler;->message:Lcom/adobe/mobile/MessageAlert;

    invoke-virtual {v0}, Lcom/adobe/mobile/MessageAlert;->clickedThrough()V

    .line 153
    iget-object v0, p0, Lcom/adobe/mobile/MessageAlert$MessageShower$PositiveClickHandler;->message:Lcom/adobe/mobile/MessageAlert;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/adobe/mobile/MessageAlert;->isVisible:Z

    .line 156
    iget-object v0, p0, Lcom/adobe/mobile/MessageAlert$MessageShower$PositiveClickHandler;->message:Lcom/adobe/mobile/MessageAlert;

    iget-object v0, v0, Lcom/adobe/mobile/MessageAlert;->url:Ljava/lang/String;

    if-eqz v0, :cond_9c

    iget-object v0, p0, Lcom/adobe/mobile/MessageAlert$MessageShower$PositiveClickHandler;->message:Lcom/adobe/mobile/MessageAlert;

    iget-object v0, v0, Lcom/adobe/mobile/MessageAlert;->url:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_9c

    .line 158
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 159
    .local v4, "tokens":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "{userId}"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getVisitorID()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2a

    const-string v1, ""

    goto :goto_2e

    :cond_2a
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getVisitorID()Ljava/lang/String;

    move-result-object v1

    :goto_2e
    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    const-string v0, "{trackingId}"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getAID()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3c

    const-string v1, ""

    goto :goto_40

    :cond_3c
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getAID()Ljava/lang/String;

    move-result-object v1

    :goto_40
    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    const-string v0, "{messageId}"

    iget-object v1, p0, Lcom/adobe/mobile/MessageAlert$MessageShower$PositiveClickHandler;->message:Lcom/adobe/mobile/MessageAlert;

    iget-object v1, v1, Lcom/adobe/mobile/MessageAlert;->messageId:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    const-string v0, "{lifetimeValue}"

    invoke-static {}, Lcom/adobe/mobile/AnalyticsTrackLifetimeValueIncrease;->getLifetimeValue()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    iget-object v0, p0, Lcom/adobe/mobile/MessageAlert$MessageShower$PositiveClickHandler;->message:Lcom/adobe/mobile/MessageAlert;

    iget-object v1, p0, Lcom/adobe/mobile/MessageAlert$MessageShower$PositiveClickHandler;->message:Lcom/adobe/mobile/MessageAlert;

    iget-object v1, v1, Lcom/adobe/mobile/MessageAlert;->url:Ljava/lang/String;

    invoke-static {v1, v4}, Lcom/adobe/mobile/StaticMethods;->expandTokens(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/adobe/mobile/MessageAlert;->url:Ljava/lang/String;

    .line 168
    :try_start_65
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCurrentActivity()Landroid/app/Activity;
    :try_end_68
    .catch Lcom/adobe/mobile/StaticMethods$NullActivityException; {:try_start_65 .. :try_end_68} :catch_6a

    move-result-object v5

    .line 173
    .local v5, "currentActivity":Landroid/app/Activity;
    goto :goto_76

    .line 170
    .end local v5    # "currentActivity":Landroid/app/Activity;
    :catch_6a
    move-exception v6

    .line 171
    .local v6, "ex":Lcom/adobe/mobile/StaticMethods$NullActivityException;
    invoke-virtual {v6}, Lcom/adobe/mobile/StaticMethods$NullActivityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 172
    return-void

    .line 176
    .local v5, "currentActivity":Landroid/app/Activity;
    .end local v6    # "ex":Lcom/adobe/mobile/StaticMethods$NullActivityException;
    :goto_76
    :try_start_76
    new-instance v6, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 177
    .local v6, "intent":Landroid/content/Intent;
    iget-object v0, p0, Lcom/adobe/mobile/MessageAlert$MessageShower$PositiveClickHandler;->message:Lcom/adobe/mobile/MessageAlert;

    iget-object v0, v0, Lcom/adobe/mobile/MessageAlert;->url:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 178
    invoke-virtual {v5, v6}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_8b
    .catch Ljava/lang/Exception; {:try_start_76 .. :try_end_8b} :catch_8c

    .line 181
    .end local v6    # "intent":Landroid/content/Intent;
    goto :goto_9c

    .line 179
    :catch_8c
    move-exception v6

    .line 180
    .local v6, "ex":Ljava/lang/Exception;
    const-string v0, "Messages - Could not load click-through intent for message (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 183
    .end local v4    # "tokens":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4
    .end local v5    # "currentActivity":Landroid/app/Activity;
    .end local v6    # "ex":Ljava/lang/Exception;
    :cond_9c
    :goto_9c
    return-void
.end method

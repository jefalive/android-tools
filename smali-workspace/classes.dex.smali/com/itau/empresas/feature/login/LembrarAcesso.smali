.class public final Lcom/itau/empresas/feature/login/LembrarAcesso;
.super Ljava/lang/Object;
.source "LembrarAcesso.java"


# instance fields
.field private final sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/SharedPreferences;)V
    .registers 2
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/itau/empresas/feature/login/LembrarAcesso;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 19
    return-void
.end method

.method private gravarDados(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "informacao"    # Ljava/lang/String;

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LembrarAcesso;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 45
    .local v1, "ed":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 47
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 48
    return-void
.end method


# virtual methods
.method public apagarAcesso()V
    .registers 3

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LembrarAcesso;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "LEMBRARACESSOOPERADOR"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LembrarAcesso;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "LEMBRARACESSONOME"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 56
    return-void
.end method

.method public consultarAcesso()Lcom/itau/empresas/api/model/OperadorVO;
    .registers 6

    .line 27
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LembrarAcesso;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "LEMBRARACESSOOPERADOR"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 28
    .local v3, "operador":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LembrarAcesso;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "LEMBRARACESSONOME"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 29
    .local v4, "nome":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1c

    const/4 v0, 0x0

    goto :goto_21

    :cond_1c
    new-instance v0, Lcom/itau/empresas/api/model/OperadorVO;

    invoke-direct {v0, v4, v3}, Lcom/itau/empresas/api/model/OperadorVO;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_21
    return-object v0
.end method

.method public lembrarAcesso(Lcom/itau/empresas/api/model/OperadorVO;)V
    .registers 4
    .param p1, "informacao"    # Lcom/itau/empresas/api/model/OperadorVO;

    .line 37
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/LembrarAcesso;->apagarAcesso()V

    .line 38
    const-string v0, "LEMBRARACESSOOPERADOR"

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/OperadorVO;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/login/LembrarAcesso;->gravarDados(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    const-string v0, "LEMBRARACESSONOME"

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/OperadorVO;->getNomeOperador()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/login/LembrarAcesso;->gravarDados(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    return-void
.end method

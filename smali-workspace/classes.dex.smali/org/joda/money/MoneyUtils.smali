.class public final Lorg/joda/money/MoneyUtils;
.super Ljava/lang/Object;
.source "MoneyUtils.java"


# direct methods
.method private constructor <init>()V
    .registers 1

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    return-void
.end method

.method public static add(Lorg/joda/money/BigMoney;Lorg/joda/money/BigMoney;)Lorg/joda/money/BigMoney;
    .registers 3
    .param p0, "money1"    # Lorg/joda/money/BigMoney;
    .param p1, "money2"    # Lorg/joda/money/BigMoney;

    .line 248
    if-nez p0, :cond_3

    .line 249
    return-object p1

    .line 251
    :cond_3
    if-nez p1, :cond_6

    .line 252
    return-object p0

    .line 254
    :cond_6
    invoke-virtual {p0, p1}, Lorg/joda/money/BigMoney;->plus(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public static add(Lorg/joda/money/Money;Lorg/joda/money/Money;)Lorg/joda/money/Money;
    .registers 3
    .param p0, "money1"    # Lorg/joda/money/Money;
    .param p1, "money2"    # Lorg/joda/money/Money;

    .line 161
    if-nez p0, :cond_3

    .line 162
    return-object p1

    .line 164
    :cond_3
    if-nez p1, :cond_6

    .line 165
    return-object p0

    .line 167
    :cond_6
    invoke-virtual {p0, p1}, Lorg/joda/money/Money;->plus(Lorg/joda/money/Money;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method static checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V
    .registers 3
    .param p0, "object"    # Ljava/lang/Object;
    .param p1, "message"    # Ljava/lang/String;

    .line 32
    if-nez p0, :cond_8

    .line 33
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0, p1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_8
    return-void
.end method

.method public static isNegative(Lorg/joda/money/BigMoneyProvider;)Z
    .registers 2
    .param p0, "moneyProvider"    # Lorg/joda/money/BigMoneyProvider;

    .line 90
    if-eqz p0, :cond_e

    invoke-interface {p0}, Lorg/joda/money/BigMoneyProvider;->toBigMoney()Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->isNegative()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

.method public static isNegativeOrZero(Lorg/joda/money/BigMoneyProvider;)Z
    .registers 2
    .param p0, "moneyProvider"    # Lorg/joda/money/BigMoneyProvider;

    .line 102
    if-eqz p0, :cond_c

    invoke-interface {p0}, Lorg/joda/money/BigMoneyProvider;->toBigMoney()Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->isNegativeOrZero()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

.method public static isPositive(Lorg/joda/money/BigMoneyProvider;)Z
    .registers 2
    .param p0, "moneyProvider"    # Lorg/joda/money/BigMoneyProvider;

    .line 66
    if-eqz p0, :cond_e

    invoke-interface {p0}, Lorg/joda/money/BigMoneyProvider;->toBigMoney()Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->isPositive()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

.method public static isPositiveOrZero(Lorg/joda/money/BigMoneyProvider;)Z
    .registers 2
    .param p0, "moneyProvider"    # Lorg/joda/money/BigMoneyProvider;

    .line 78
    if-eqz p0, :cond_c

    invoke-interface {p0}, Lorg/joda/money/BigMoneyProvider;->toBigMoney()Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->isPositiveOrZero()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

.method public static isZero(Lorg/joda/money/BigMoneyProvider;)Z
    .registers 2
    .param p0, "moneyProvider"    # Lorg/joda/money/BigMoneyProvider;

    .line 54
    if-eqz p0, :cond_c

    invoke-interface {p0}, Lorg/joda/money/BigMoneyProvider;->toBigMoney()Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->isZero()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

.method public static max(Lorg/joda/money/BigMoney;Lorg/joda/money/BigMoney;)Lorg/joda/money/BigMoney;
    .registers 3
    .param p0, "money1"    # Lorg/joda/money/BigMoney;
    .param p1, "money2"    # Lorg/joda/money/BigMoney;

    .line 205
    if-nez p0, :cond_3

    .line 206
    return-object p1

    .line 208
    :cond_3
    if-nez p1, :cond_6

    .line 209
    return-object p0

    .line 211
    :cond_6
    invoke-virtual {p0, p1}, Lorg/joda/money/BigMoney;->compareTo(Lorg/joda/money/BigMoneyProvider;)I

    move-result v0

    if-lez v0, :cond_e

    move-object v0, p0

    goto :goto_f

    :cond_e
    move-object v0, p1

    :goto_f
    return-object v0
.end method

.method public static max(Lorg/joda/money/Money;Lorg/joda/money/Money;)Lorg/joda/money/Money;
    .registers 3
    .param p0, "money1"    # Lorg/joda/money/Money;
    .param p1, "money2"    # Lorg/joda/money/Money;

    .line 118
    if-nez p0, :cond_3

    .line 119
    return-object p1

    .line 121
    :cond_3
    if-nez p1, :cond_6

    .line 122
    return-object p0

    .line 124
    :cond_6
    invoke-virtual {p0, p1}, Lorg/joda/money/Money;->compareTo(Lorg/joda/money/BigMoneyProvider;)I

    move-result v0

    if-lez v0, :cond_e

    move-object v0, p0

    goto :goto_f

    :cond_e
    move-object v0, p1

    :goto_f
    return-object v0
.end method

.method public static min(Lorg/joda/money/BigMoney;Lorg/joda/money/BigMoney;)Lorg/joda/money/BigMoney;
    .registers 3
    .param p0, "money1"    # Lorg/joda/money/BigMoney;
    .param p1, "money2"    # Lorg/joda/money/BigMoney;

    .line 226
    if-nez p0, :cond_3

    .line 227
    return-object p1

    .line 229
    :cond_3
    if-nez p1, :cond_6

    .line 230
    return-object p0

    .line 232
    :cond_6
    invoke-virtual {p0, p1}, Lorg/joda/money/BigMoney;->compareTo(Lorg/joda/money/BigMoneyProvider;)I

    move-result v0

    if-gez v0, :cond_e

    move-object v0, p0

    goto :goto_f

    :cond_e
    move-object v0, p1

    :goto_f
    return-object v0
.end method

.method public static min(Lorg/joda/money/Money;Lorg/joda/money/Money;)Lorg/joda/money/Money;
    .registers 3
    .param p0, "money1"    # Lorg/joda/money/Money;
    .param p1, "money2"    # Lorg/joda/money/Money;

    .line 139
    if-nez p0, :cond_3

    .line 140
    return-object p1

    .line 142
    :cond_3
    if-nez p1, :cond_6

    .line 143
    return-object p0

    .line 145
    :cond_6
    invoke-virtual {p0, p1}, Lorg/joda/money/Money;->compareTo(Lorg/joda/money/BigMoneyProvider;)I

    move-result v0

    if-gez v0, :cond_e

    move-object v0, p0

    goto :goto_f

    :cond_e
    move-object v0, p1

    :goto_f
    return-object v0
.end method

.method public static subtract(Lorg/joda/money/BigMoney;Lorg/joda/money/BigMoney;)Lorg/joda/money/BigMoney;
    .registers 3
    .param p0, "money1"    # Lorg/joda/money/BigMoney;
    .param p1, "money2"    # Lorg/joda/money/BigMoney;

    .line 270
    if-nez p1, :cond_3

    .line 271
    return-object p0

    .line 273
    :cond_3
    if-nez p0, :cond_a

    .line 274
    invoke-virtual {p1}, Lorg/joda/money/BigMoney;->negated()Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0

    .line 276
    :cond_a
    invoke-virtual {p0, p1}, Lorg/joda/money/BigMoney;->minus(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public static subtract(Lorg/joda/money/Money;Lorg/joda/money/Money;)Lorg/joda/money/Money;
    .registers 3
    .param p0, "money1"    # Lorg/joda/money/Money;
    .param p1, "money2"    # Lorg/joda/money/Money;

    .line 183
    if-nez p1, :cond_3

    .line 184
    return-object p0

    .line 186
    :cond_3
    if-nez p0, :cond_a

    .line 187
    invoke-virtual {p1}, Lorg/joda/money/Money;->negated()Lorg/joda/money/Money;

    move-result-object v0

    return-object v0

    .line 189
    :cond_a
    invoke-virtual {p0, p1}, Lorg/joda/money/Money;->minus(Lorg/joda/money/Money;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

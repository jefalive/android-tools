.class Lcom/itau/empresas/controller/TrocaSenhaController$2;
.super Ljava/lang/Object;
.source "TrocaSenhaController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/controller/TrocaSenhaController;->trocarSenhaObrigatoria(Ljava/lang/String;Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/controller/TrocaSenhaController;

.field final synthetic val$cadastroSenhaVO:Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;

.field final synthetic val$codigoOperador:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/controller/TrocaSenhaController;Ljava/lang/String;Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;)V
    .registers 4
    .param p1, "this$0"    # Lcom/itau/empresas/controller/TrocaSenhaController;

    .line 37
    iput-object p1, p0, Lcom/itau/empresas/controller/TrocaSenhaController$2;->this$0:Lcom/itau/empresas/controller/TrocaSenhaController;

    iput-object p2, p0, Lcom/itau/empresas/controller/TrocaSenhaController$2;->val$codigoOperador:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/controller/TrocaSenhaController$2;->val$cadastroSenhaVO:Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 5

    .line 40
    iget-object v0, p0, Lcom/itau/empresas/controller/TrocaSenhaController$2;->this$0:Lcom/itau/empresas/controller/TrocaSenhaController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/TrocaSenhaController;->api()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/controller/TrocaSenhaController$2;->val$codigoOperador:Ljava/lang/String;

    iget-object v2, p0, Lcom/itau/empresas/controller/TrocaSenhaController$2;->val$cadastroSenhaVO:Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;

    .line 41
    invoke-interface {v0, v1, v2}, Lcom/itau/empresas/api/Api;->trocarSenhaObrigatoria(Ljava/lang/String;Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;)Lcom/itau/empresas/api/model/ResultadoCadastroSenhaEletronicaVO;

    move-result-object v3

    .line 42
    .local v3, "resultado":Lcom/itau/empresas/api/model/ResultadoCadastroSenhaEletronicaVO;
    iget-object v0, p0, Lcom/itau/empresas/controller/TrocaSenhaController$2;->this$0:Lcom/itau/empresas/controller/TrocaSenhaController;

    iget-object v0, v0, Lcom/itau/empresas/controller/TrocaSenhaController;->menuController:Lcom/itau/empresas/feature/login/controller/MenuController;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/controller/MenuController;->carregarMenu()V

    .line 43
    iget-object v0, p0, Lcom/itau/empresas/controller/TrocaSenhaController$2;->this$0:Lcom/itau/empresas/controller/TrocaSenhaController;

    iget-object v0, v0, Lcom/itau/empresas/controller/TrocaSenhaController;->operadorController:Lcom/itau/empresas/feature/login/controller/OperadorController;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/controller/OperadorController;->buscaDadosOperador()V

    .line 45
    invoke-static {v3}, Lcom/itau/empresas/controller/BaseController;->post(Ljava/lang/Object;)V

    .line 46
    return-void
.end method

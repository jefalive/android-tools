.class Lcom/google/android/gms/ads/internal/request/LargeParcelTeleporter$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/ads/internal/request/LargeParcelTeleporter;->zzf([B)Landroid/os/ParcelFileDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic zzIt:Ljava/io/OutputStream;

.field final synthetic zzIu:[B

.field final synthetic zzIv:Lcom/google/android/gms/ads/internal/request/LargeParcelTeleporter;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/internal/request/LargeParcelTeleporter;Ljava/io/OutputStream;[B)V
    .registers 4

    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/LargeParcelTeleporter$1;->zzIv:Lcom/google/android/gms/ads/internal/request/LargeParcelTeleporter;

    iput-object p2, p0, Lcom/google/android/gms/ads/internal/request/LargeParcelTeleporter$1;->zzIt:Ljava/io/OutputStream;

    iput-object p3, p0, Lcom/google/android/gms/ads/internal/request/LargeParcelTeleporter$1;->zzIu:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    const/4 v2, 0x0

    :try_start_1
    new-instance v0, Ljava/io/DataOutputStream;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/LargeParcelTeleporter$1;->zzIt:Ljava/io/OutputStream;

    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object v2, v0

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/LargeParcelTeleporter$1;->zzIu:[B

    array-length v0, v0

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/LargeParcelTeleporter$1;->zzIu:[B

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_14} :catch_20
    .catchall {:try_start_1 .. :try_end_14} :catchall_3a

    if-nez v2, :cond_1c

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/LargeParcelTeleporter$1;->zzIt:Ljava/io/OutputStream;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzna;->zzb(Ljava/io/Closeable;)V

    goto :goto_47

    :cond_1c
    invoke-static {v2}, Lcom/google/android/gms/internal/zzna;->zzb(Ljava/io/Closeable;)V

    goto :goto_47

    :catch_20
    move-exception v3

    const-string v0, "Error transporting the ad response"

    :try_start_23
    invoke-static {v0, v3}, Lcom/google/android/gms/internal/zzin;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbF()Lcom/google/android/gms/internal/zzih;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v3, v1}, Lcom/google/android/gms/internal/zzih;->zzb(Ljava/lang/Throwable;Z)V
    :try_end_2e
    .catchall {:try_start_23 .. :try_end_2e} :catchall_3a

    if-nez v2, :cond_36

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/LargeParcelTeleporter$1;->zzIt:Ljava/io/OutputStream;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzna;->zzb(Ljava/io/Closeable;)V

    goto :goto_47

    :cond_36
    invoke-static {v2}, Lcom/google/android/gms/internal/zzna;->zzb(Ljava/io/Closeable;)V

    goto :goto_47

    :catchall_3a
    move-exception v4

    if-nez v2, :cond_43

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/LargeParcelTeleporter$1;->zzIt:Ljava/io/OutputStream;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzna;->zzb(Ljava/io/Closeable;)V

    goto :goto_46

    :cond_43
    invoke-static {v2}, Lcom/google/android/gms/internal/zzna;->zzb(Ljava/io/Closeable;)V

    :goto_46
    throw v4

    :goto_47
    return-void
.end method

.class public Lcom/google/ads/conversiontracking/e$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ads/conversiontracking/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field protected a:J

.field final synthetic b:Lcom/google/ads/conversiontracking/e;


# direct methods
.method public constructor <init>(Lcom/google/ads/conversiontracking/e;)V
    .registers 4

    .line 143
    iput-object p1, p0, Lcom/google/ads/conversiontracking/e$b;->b:Lcom/google/ads/conversiontracking/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/ads/conversiontracking/e$b;->a:J

    return-void
.end method

.method private a()V
    .registers 5

    .line 198
    iget-wide v0, p0, Lcom/google/ads/conversiontracking/e$b;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_d

    .line 199
    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, Lcom/google/ads/conversiontracking/e$b;->a:J

    goto :goto_1b

    .line 201
    :cond_d
    iget-wide v0, p0, Lcom/google/ads/conversiontracking/e$b;->a:J

    const-wide/16 v2, 0x2

    mul-long/2addr v0, v2

    const-wide/32 v2, 0xea60

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/ads/conversiontracking/e$b;->a:J

    .line 203
    :goto_1b
    return-void
.end method


# virtual methods
.method public run()V
    .registers 8

    .line 153
    :try_start_0
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e$b;->b:Lcom/google/ads/conversiontracking/e;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/ads/conversiontracking/e;->b(Lcom/google/ads/conversiontracking/e;Z)Z

    .line 155
    :goto_6
    const/4 v4, 0x0

    .line 156
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e$b;->b:Lcom/google/ads/conversiontracking/e;

    invoke-static {v0}, Lcom/google/ads/conversiontracking/e;->a(Lcom/google/ads/conversiontracking/e;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5
    :try_end_e
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_e} :catch_9c

    .line 157
    :goto_e
    :try_start_e
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e$b;->b:Lcom/google/ads/conversiontracking/e;

    invoke-static {v0}, Lcom/google/ads/conversiontracking/e;->f(Lcom/google/ads/conversiontracking/e;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 158
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e$b;->b:Lcom/google/ads/conversiontracking/e;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/ads/conversiontracking/e;->a(Lcom/google/ads/conversiontracking/e;Z)Z

    .line 159
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e$b;->b:Lcom/google/ads/conversiontracking/e;

    invoke-static {v0}, Lcom/google/ads/conversiontracking/e;->a(Lcom/google/ads/conversiontracking/e;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    goto :goto_e

    .line 161
    :cond_2a
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e$b;->b:Lcom/google/ads/conversiontracking/e;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/ads/conversiontracking/e;->a(Lcom/google/ads/conversiontracking/e;Z)Z

    .line 162
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e$b;->b:Lcom/google/ads/conversiontracking/e;

    invoke-static {v0}, Lcom/google/ads/conversiontracking/e;->f(Lcom/google/ads/conversiontracking/e;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/google/ads/conversiontracking/d;
    :try_end_3e
    .catchall {:try_start_e .. :try_end_3e} :catchall_40

    .line 163
    monitor-exit v5

    goto :goto_43

    :catchall_40
    move-exception v6

    monitor-exit v5

    :try_start_42
    throw v6

    .line 164
    :goto_43
    if-nez v4, :cond_46

    .line 165
    goto :goto_6

    .line 168
    :cond_46
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e$b;->b:Lcom/google/ads/conversiontracking/e;

    .line 169
    invoke-static {v0}, Lcom/google/ads/conversiontracking/e;->c(Lcom/google/ads/conversiontracking/e;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, v4, Lcom/google/ads/conversiontracking/d;->e:Ljava/lang/String;

    iget-object v2, v4, Lcom/google/ads/conversiontracking/d;->f:Ljava/lang/String;

    iget-boolean v3, v4, Lcom/google/ads/conversiontracking/d;->b:Z

    .line 168
    invoke-static {v0, v1, v2, v3}, Lcom/google/ads/conversiontracking/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_62

    .line 171
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e$b;->b:Lcom/google/ads/conversiontracking/e;

    invoke-static {v0}, Lcom/google/ads/conversiontracking/e;->e(Lcom/google/ads/conversiontracking/e;)Lcom/google/ads/conversiontracking/f;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/ads/conversiontracking/f;->a(Lcom/google/ads/conversiontracking/d;)V

    .line 172
    goto :goto_6

    .line 174
    :cond_62
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e$b;->b:Lcom/google/ads/conversiontracking/e;

    invoke-virtual {v0, v4}, Lcom/google/ads/conversiontracking/e;->a(Lcom/google/ads/conversiontracking/d;)I

    move-result v5

    .line 175
    const/4 v0, 0x2

    if-ne v5, v0, :cond_79

    .line 176
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e$b;->b:Lcom/google/ads/conversiontracking/e;

    invoke-static {v0}, Lcom/google/ads/conversiontracking/e;->e(Lcom/google/ads/conversiontracking/e;)Lcom/google/ads/conversiontracking/f;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/ads/conversiontracking/f;->a(Lcom/google/ads/conversiontracking/d;)V

    .line 177
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/ads/conversiontracking/e$b;->a:J

    goto :goto_9a

    .line 178
    :cond_79
    if-nez v5, :cond_8d

    .line 179
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e$b;->b:Lcom/google/ads/conversiontracking/e;

    invoke-static {v0}, Lcom/google/ads/conversiontracking/e;->e(Lcom/google/ads/conversiontracking/e;)Lcom/google/ads/conversiontracking/f;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/ads/conversiontracking/f;->c(Lcom/google/ads/conversiontracking/d;)V

    .line 181
    invoke-direct {p0}, Lcom/google/ads/conversiontracking/e$b;->a()V

    .line 182
    iget-wide v0, p0, Lcom/google/ads/conversiontracking/e$b;->a:J

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    goto :goto_9a

    .line 184
    :cond_8d
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e$b;->b:Lcom/google/ads/conversiontracking/e;

    invoke-static {v0}, Lcom/google/ads/conversiontracking/e;->e(Lcom/google/ads/conversiontracking/e;)Lcom/google/ads/conversiontracking/f;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/ads/conversiontracking/f;->c(Lcom/google/ads/conversiontracking/d;)V

    .line 185
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/ads/conversiontracking/e$b;->a:J
    :try_end_9a
    .catch Ljava/lang/InterruptedException; {:try_start_42 .. :try_end_9a} :catch_9c

    .line 187
    :goto_9a
    goto/16 :goto_6

    .line 188
    :catch_9c
    move-exception v4

    .line 189
    const-string v0, "GoogleConversionReporter"

    const-string v1, "Dispatch thread is interrupted."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e$b;->b:Lcom/google/ads/conversiontracking/e;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/ads/conversiontracking/e;->b(Lcom/google/ads/conversiontracking/e;Z)Z

    .line 192
    return-void
.end method

.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;
.source "PieData.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 19
    invoke-direct {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;-><init>()V

    .line 20
    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .registers 2
    .param p1, "xVals"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/String;>;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;-><init>(Ljava/util/List;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;)V
    .registers 4
    .param p1, "xVals"    # Ljava/util/List;
    .param p2, "dataSet"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/String;>;Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;)V"
        }
    .end annotation

    .line 34
    invoke-static {p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->toList(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;-><init>(Ljava/util/List;Ljava/util/List;)V

    .line 35
    return-void
.end method

.method public constructor <init>([Ljava/lang/String;)V
    .registers 2
    .param p1, "xVals"    # [Ljava/lang/String;

    .line 29
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;-><init>([Ljava/lang/String;)V

    .line 30
    return-void
.end method

.method public constructor <init>([Ljava/lang/String;Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;)V
    .registers 4
    .param p1, "xVals"    # [Ljava/lang/String;
    .param p2, "dataSet"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;

    .line 39
    invoke-static {p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->toList(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;-><init>([Ljava/lang/String;Ljava/util/List;)V

    .line 40
    return-void
.end method

.method private static toList(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;)Ljava/util/List;
    .registers 3
    .param p0, "dataSet"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;)Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;>;"
        }
    .end annotation

    .line 44
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 45
    .local v1, "sets":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;>;"
    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    return-object v1
.end method


# virtual methods
.method public getDataSet()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;
    .registers 3

    .line 69
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->mDataSets:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;

    return-object v0
.end method

.method public bridge synthetic getDataSetByIndex(I)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
    .registers 3

    .line 15
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->getDataSetByIndex(I)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;

    move-result-object v0

    return-object v0
.end method

.method public getDataSetByIndex(I)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;
    .registers 3
    .param p1, "index"    # I

    .line 75
    if-nez p1, :cond_7

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->getDataSet()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;

    move-result-object v0

    goto :goto_8

    :cond_7
    const/4 v0, 0x0

    :goto_8
    return-object v0
.end method

.method public bridge synthetic getDataSetByLabel(Ljava/lang/String;Z)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
    .registers 4

    .line 15
    invoke-virtual {p0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->getDataSetByLabel(Ljava/lang/String;Z)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;

    move-result-object v0

    return-object v0
.end method

.method public getDataSetByLabel(Ljava/lang/String;Z)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;
    .registers 5
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "ignorecase"    # Z

    .line 81
    if-eqz p2, :cond_21

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->mDataSets:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->mDataSets:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;

    goto :goto_3f

    :cond_1f
    const/4 v0, 0x0

    goto :goto_3f

    :cond_21
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->mDataSets:Ljava/util/List;

    .line 82
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3e

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->mDataSets:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;

    goto :goto_3f

    :cond_3e
    const/4 v0, 0x0

    :goto_3f
    return-object v0
.end method

.method public setDataSet(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;)V
    .registers 3
    .param p1, "dataSet"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;

    .line 56
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->mDataSets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 57
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->mDataSets:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->init()V

    .line 59
    return-void
.end method

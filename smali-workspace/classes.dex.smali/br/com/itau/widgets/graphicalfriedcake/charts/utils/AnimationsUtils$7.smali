.class final Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$7;
.super Ljava/lang/Object;
.source "AnimationsUtils.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils;->animateTextColorChange(Landroid/widget/TextView;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$textView:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/widget/TextView;)V
    .registers 2

    .line 217
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$7;->val$textView:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .registers 4
    .param p1, "animator"    # Landroid/animation/ValueAnimator;

    .line 220
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$7;->val$textView:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 221
    return-void
.end method

.class public final Lorg/androidannotations/api/BackgroundExecutor;
.super Ljava/lang/Object;
.source "BackgroundExecutor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/androidannotations/api/BackgroundExecutor$WrongThreadListener;,
        Lorg/androidannotations/api/BackgroundExecutor$Task;
    }
.end annotation


# static fields
.field private static final CURRENT_SERIAL:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field public static final DEFAULT_EXECUTOR:Ljava/util/concurrent/Executor;

.field public static final DEFAULT_WRONG_THREAD_LISTENER:Lorg/androidannotations/api/BackgroundExecutor$WrongThreadListener;

.field private static final TASKS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lorg/androidannotations/api/BackgroundExecutor$Task;>;"
        }
    .end annotation
.end field

.field private static executor:Ljava/util/concurrent/Executor;

.field private static wrongThreadListener:Lorg/androidannotations/api/BackgroundExecutor$WrongThreadListener;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .line 36
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    sput-object v0, Lorg/androidannotations/api/BackgroundExecutor;->DEFAULT_EXECUTOR:Ljava/util/concurrent/Executor;

    .line 37
    sget-object v0, Lorg/androidannotations/api/BackgroundExecutor;->DEFAULT_EXECUTOR:Ljava/util/concurrent/Executor;

    sput-object v0, Lorg/androidannotations/api/BackgroundExecutor;->executor:Ljava/util/concurrent/Executor;

    .line 47
    new-instance v0, Lorg/androidannotations/api/BackgroundExecutor$1;

    invoke-direct {v0}, Lorg/androidannotations/api/BackgroundExecutor$1;-><init>()V

    sput-object v0, Lorg/androidannotations/api/BackgroundExecutor;->DEFAULT_WRONG_THREAD_LISTENER:Lorg/androidannotations/api/BackgroundExecutor$WrongThreadListener;

    .line 70
    sget-object v0, Lorg/androidannotations/api/BackgroundExecutor;->DEFAULT_WRONG_THREAD_LISTENER:Lorg/androidannotations/api/BackgroundExecutor$WrongThreadListener;

    sput-object v0, Lorg/androidannotations/api/BackgroundExecutor;->wrongThreadListener:Lorg/androidannotations/api/BackgroundExecutor$WrongThreadListener;

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lorg/androidannotations/api/BackgroundExecutor;->TASKS:Ljava/util/List;

    .line 73
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lorg/androidannotations/api/BackgroundExecutor;->CURRENT_SERIAL:Ljava/lang/ThreadLocal;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    return-void
.end method

.method static synthetic access$700()Ljava/lang/ThreadLocal;
    .registers 1

    .line 32
    sget-object v0, Lorg/androidannotations/api/BackgroundExecutor;->CURRENT_SERIAL:Ljava/lang/ThreadLocal;

    return-object v0
.end method

.method static synthetic access$800()Ljava/util/List;
    .registers 1

    .line 32
    sget-object v0, Lorg/androidannotations/api/BackgroundExecutor;->TASKS:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$900(Ljava/lang/String;)Lorg/androidannotations/api/BackgroundExecutor$Task;
    .registers 2
    .param p0, "x0"    # Ljava/lang/String;

    .line 32
    invoke-static {p0}, Lorg/androidannotations/api/BackgroundExecutor;->take(Ljava/lang/String;)Lorg/androidannotations/api/BackgroundExecutor$Task;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized cancelAll(Ljava/lang/String;Z)V
    .registers 8
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "mayInterruptIfRunning"    # Z

    const-class v5, Lorg/androidannotations/api/BackgroundExecutor;

    monitor-enter v5

    .line 251
    :try_start_3
    sget-object v0, Lorg/androidannotations/api/BackgroundExecutor;->TASKS:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v3, v0, -0x1

    .local v3, "i":I
    :goto_b
    if-ltz v3, :cond_6d

    .line 252
    sget-object v0, Lorg/androidannotations/api/BackgroundExecutor;->TASKS:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lorg/androidannotations/api/BackgroundExecutor$Task;

    .line 253
    .local v4, "task":Lorg/androidannotations/api/BackgroundExecutor$Task;
    # getter for: Lorg/androidannotations/api/BackgroundExecutor$Task;->id:Ljava/lang/String;
    invoke-static {v4}, Lorg/androidannotations/api/BackgroundExecutor$Task;->access$000(Lorg/androidannotations/api/BackgroundExecutor$Task;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6a

    .line 254
    # getter for: Lorg/androidannotations/api/BackgroundExecutor$Task;->future:Ljava/util/concurrent/Future;
    invoke-static {v4}, Lorg/androidannotations/api/BackgroundExecutor$Task;->access$300(Lorg/androidannotations/api/BackgroundExecutor$Task;)Ljava/util/concurrent/Future;

    move-result-object v0

    if-eqz v0, :cond_3c

    .line 255
    # getter for: Lorg/androidannotations/api/BackgroundExecutor$Task;->future:Ljava/util/concurrent/Future;
    invoke-static {v4}, Lorg/androidannotations/api/BackgroundExecutor$Task;->access$300(Lorg/androidannotations/api/BackgroundExecutor$Task;)Ljava/util/concurrent/Future;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 256
    # getter for: Lorg/androidannotations/api/BackgroundExecutor$Task;->managed:Ljava/util/concurrent/atomic/AtomicBoolean;
    invoke-static {v4}, Lorg/androidannotations/api/BackgroundExecutor$Task;->access$500(Lorg/androidannotations/api/BackgroundExecutor$Task;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_6a

    .line 262
    # invokes: Lorg/androidannotations/api/BackgroundExecutor$Task;->postExecute()V
    invoke-static {v4}, Lorg/androidannotations/api/BackgroundExecutor$Task;->access$600(Lorg/androidannotations/api/BackgroundExecutor$Task;)V

    goto :goto_6a

    .line 264
    :cond_3c
    # getter for: Lorg/androidannotations/api/BackgroundExecutor$Task;->executionAsked:Z
    invoke-static {v4}, Lorg/androidannotations/api/BackgroundExecutor$Task;->access$200(Lorg/androidannotations/api/BackgroundExecutor$Task;)Z

    move-result v0

    if-eqz v0, :cond_65

    .line 265
    const-string v0, "BackgroundExecutor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "A task with id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lorg/androidannotations/api/BackgroundExecutor$Task;->id:Ljava/lang/String;
    invoke-static {v4}, Lorg/androidannotations/api/BackgroundExecutor$Task;->access$000(Lorg/androidannotations/api/BackgroundExecutor$Task;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cannot be cancelled (the executor set does not support it)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6a

    .line 268
    :cond_65
    sget-object v0, Lorg/androidannotations/api/BackgroundExecutor;->TASKS:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;
    :try_end_6a
    .catchall {:try_start_3 .. :try_end_6a} :catchall_6f

    .line 251
    .end local v4    # "task":Lorg/androidannotations/api/BackgroundExecutor$Task;
    :cond_6a
    :goto_6a
    add-int/lit8 v3, v3, -0x1

    goto :goto_b

    .line 272
    .end local v3    # "i":I
    :cond_6d
    monitor-exit v5

    return-void

    :catchall_6f
    move-exception p0

    monitor-exit v5

    throw p0
.end method

.method private static directExecute(Ljava/lang/Runnable;J)Ljava/util/concurrent/Future;
    .registers 7
    .param p0, "runnable"    # Ljava/lang/Runnable;
    .param p1, "delay"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/Runnable;J)Ljava/util/concurrent/Future<*>;"
        }
    .end annotation

    .line 97
    const/4 v2, 0x0

    .line 98
    .local v2, "future":Ljava/util/concurrent/Future;, "Ljava/util/concurrent/Future<*>;"
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_21

    .line 100
    sget-object v0, Lorg/androidannotations/api/BackgroundExecutor;->executor:Ljava/util/concurrent/Executor;

    instance-of v0, v0, Ljava/util/concurrent/ScheduledExecutorService;

    if-nez v0, :cond_15

    .line 101
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The executor set does not support scheduling"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_15
    sget-object v0, Lorg/androidannotations/api/BackgroundExecutor;->executor:Ljava/util/concurrent/Executor;

    move-object v3, v0

    check-cast v3, Ljava/util/concurrent/ScheduledExecutorService;

    .line 104
    .local v3, "scheduledExecutorService":Ljava/util/concurrent/ScheduledExecutorService;
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, p0, p1, p2, v0}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v2

    .line 105
    .end local v3    # "scheduledExecutorService":Ljava/util/concurrent/ScheduledExecutorService;
    goto :goto_36

    .line 106
    :cond_21
    sget-object v0, Lorg/androidannotations/api/BackgroundExecutor;->executor:Ljava/util/concurrent/Executor;

    instance-of v0, v0, Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_31

    .line 107
    sget-object v0, Lorg/androidannotations/api/BackgroundExecutor;->executor:Ljava/util/concurrent/Executor;

    move-object v3, v0

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    .line 108
    .local v3, "executorService":Ljava/util/concurrent/ExecutorService;
    invoke-interface {v3, p0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v2

    .line 109
    .end local v3    # "executorService":Ljava/util/concurrent/ExecutorService;
    goto :goto_36

    .line 111
    :cond_31
    sget-object v0, Lorg/androidannotations/api/BackgroundExecutor;->executor:Ljava/util/concurrent/Executor;

    invoke-interface {v0, p0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 114
    :goto_36
    return-object v2
.end method

.method public static declared-synchronized execute(Lorg/androidannotations/api/BackgroundExecutor$Task;)V
    .registers 4
    .param p0, "task"    # Lorg/androidannotations/api/BackgroundExecutor$Task;

    const-class v2, Lorg/androidannotations/api/BackgroundExecutor;

    monitor-enter v2

    .line 131
    :try_start_3
    # getter for: Lorg/androidannotations/api/BackgroundExecutor$Task;->id:Ljava/lang/String;
    invoke-static {p0}, Lorg/androidannotations/api/BackgroundExecutor$Task;->access$000(Lorg/androidannotations/api/BackgroundExecutor$Task;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_f

    # getter for: Lorg/androidannotations/api/BackgroundExecutor$Task;->serial:Ljava/lang/String;
    invoke-static {p0}, Lorg/androidannotations/api/BackgroundExecutor$Task;->access$100(Lorg/androidannotations/api/BackgroundExecutor$Task;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 133
    :cond_f
    sget-object v0, Lorg/androidannotations/api/BackgroundExecutor;->TASKS:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    :cond_14
    # getter for: Lorg/androidannotations/api/BackgroundExecutor$Task;->serial:Ljava/lang/String;
    invoke-static {p0}, Lorg/androidannotations/api/BackgroundExecutor$Task;->access$100(Lorg/androidannotations/api/BackgroundExecutor$Task;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_24

    # getter for: Lorg/androidannotations/api/BackgroundExecutor$Task;->serial:Ljava/lang/String;
    invoke-static {p0}, Lorg/androidannotations/api/BackgroundExecutor$Task;->access$100(Lorg/androidannotations/api/BackgroundExecutor$Task;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/androidannotations/api/BackgroundExecutor;->hasSerialRunning(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_33

    .line 136
    :cond_24
    const/4 v0, 0x1

    # setter for: Lorg/androidannotations/api/BackgroundExecutor$Task;->executionAsked:Z
    invoke-static {p0, v0}, Lorg/androidannotations/api/BackgroundExecutor$Task;->access$202(Lorg/androidannotations/api/BackgroundExecutor$Task;Z)Z

    .line 137
    # getter for: Lorg/androidannotations/api/BackgroundExecutor$Task;->remainingDelay:J
    invoke-static {p0}, Lorg/androidannotations/api/BackgroundExecutor$Task;->access$400(Lorg/androidannotations/api/BackgroundExecutor$Task;)J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lorg/androidannotations/api/BackgroundExecutor;->directExecute(Ljava/lang/Runnable;J)Ljava/util/concurrent/Future;

    move-result-object v0

    # setter for: Lorg/androidannotations/api/BackgroundExecutor$Task;->future:Ljava/util/concurrent/Future;
    invoke-static {p0, v0}, Lorg/androidannotations/api/BackgroundExecutor$Task;->access$302(Lorg/androidannotations/api/BackgroundExecutor$Task;Ljava/util/concurrent/Future;)Ljava/util/concurrent/Future;
    :try_end_33
    .catchall {:try_start_3 .. :try_end_33} :catchall_35

    .line 139
    :cond_33
    monitor-exit v2

    return-void

    :catchall_35
    move-exception p0

    monitor-exit v2

    throw p0
.end method

.method private static hasSerialRunning(Ljava/lang/String;)Z
    .registers 4
    .param p0, "serial"    # Ljava/lang/String;

    .line 332
    sget-object v0, Lorg/androidannotations/api/BackgroundExecutor;->TASKS:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lorg/androidannotations/api/BackgroundExecutor$Task;

    .line 333
    .local v2, "task":Lorg/androidannotations/api/BackgroundExecutor$Task;
    # getter for: Lorg/androidannotations/api/BackgroundExecutor$Task;->executionAsked:Z
    invoke-static {v2}, Lorg/androidannotations/api/BackgroundExecutor$Task;->access$200(Lorg/androidannotations/api/BackgroundExecutor$Task;)Z

    move-result v0

    if-eqz v0, :cond_25

    # getter for: Lorg/androidannotations/api/BackgroundExecutor$Task;->serial:Ljava/lang/String;
    invoke-static {v2}, Lorg/androidannotations/api/BackgroundExecutor$Task;->access$100(Lorg/androidannotations/api/BackgroundExecutor$Task;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 334
    const/4 v0, 0x1

    return v0

    .line 336
    .end local v2    # "task":Lorg/androidannotations/api/BackgroundExecutor$Task;
    :cond_25
    goto :goto_6

    .line 337
    :cond_26
    const/4 v0, 0x0

    return v0
.end method

.method private static take(Ljava/lang/String;)Lorg/androidannotations/api/BackgroundExecutor$Task;
    .registers 4
    .param p0, "serial"    # Ljava/lang/String;

    .line 349
    sget-object v0, Lorg/androidannotations/api/BackgroundExecutor;->TASKS:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 350
    .local v1, "len":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_7
    if-ge v2, v1, :cond_27

    .line 351
    sget-object v0, Lorg/androidannotations/api/BackgroundExecutor;->TASKS:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/androidannotations/api/BackgroundExecutor$Task;

    # getter for: Lorg/androidannotations/api/BackgroundExecutor$Task;->serial:Ljava/lang/String;
    invoke-static {v0}, Lorg/androidannotations/api/BackgroundExecutor$Task;->access$100(Lorg/androidannotations/api/BackgroundExecutor$Task;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 352
    sget-object v0, Lorg/androidannotations/api/BackgroundExecutor;->TASKS:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/androidannotations/api/BackgroundExecutor$Task;

    return-object v0

    .line 350
    :cond_24
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 355
    .end local v2    # "i":I
    :cond_27
    const/4 v0, 0x0

    return-object v0
.end method

.class public Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Produto;
.super Ljava/lang/Object;
.source "DetalhesMultilimiteXMLVO.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Produto"
.end annotation


# instance fields
.field protected detalhes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;>;"
        }
    .end annotation

    .annotation runtime Lorg/simpleframework/xml/ElementList;
        inline = true
        name = "DETALHE"
        required = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDetalhes()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;>;"
        }
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Produto;->detalhes:Ljava/util/List;

    return-object v0
.end method

.method public setDetalhes(Ljava/util/List;)V
    .registers 2
    .param p1, "detalhes"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;>;)V"
        }
    .end annotation

    .line 64
    iput-object p1, p0, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Produto;->detalhes:Ljava/util/List;

    .line 65
    return-void
.end method

.class public Lcom/itau/empresas/feature/credito/lis/model/OfertaResumoLisVO;
.super Ljava/lang/Object;
.source "OfertaResumoLisVO.java"


# instance fields
.field private indicadorAlteracao:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_alteracao"
    .end annotation
.end field

.field private indicadorContratacao:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_contratacao"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isIndicadorContratacao()Z
    .registers 2

    .line 13
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/model/OfertaResumoLisVO;->indicadorContratacao:Z

    return v0
.end method

.class public final Lbr/com/concretesolutions/canarinho/validator/ValidadorCPFCNPJ;
.super Ljava/lang/Object;
.source "ValidadorCPFCNPJ.java"

# interfaces
.implements Lbr/com/concretesolutions/canarinho/validator/Validador;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/concretesolutions/canarinho/validator/ValidadorCPFCNPJ$SingletonHolder;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/concretesolutions/canarinho/validator/ValidadorCPFCNPJ$1;)V
    .registers 2
    .param p1, "x0"    # Lbr/com/concretesolutions/canarinho/validator/ValidadorCPFCNPJ$1;

    .line 10
    invoke-direct {p0}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCPFCNPJ;-><init>()V

    return-void
.end method

.method private ehCpf(Ljava/lang/String;)Z
    .registers 5
    .param p1, "valor"    # Ljava/lang/String;

    .line 47
    if-nez p1, :cond_a

    .line 48
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Valor n\u00e3o pode ser nulo"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_a
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->PADRAO_SOMENTE_NUMEROS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 52
    .local v2, "desformatado":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xc

    if-ge v0, v1, :cond_20

    const/4 v0, 0x1

    goto :goto_21

    :cond_20
    const/4 v0, 0x0

    :goto_21
    return v0
.end method

.method public static getInstance()Lbr/com/concretesolutions/canarinho/validator/ValidadorCPFCNPJ;
    .registers 1

    .line 17
    # getter for: Lbr/com/concretesolutions/canarinho/validator/ValidadorCPFCNPJ$SingletonHolder;->INSTANCE:Lbr/com/concretesolutions/canarinho/validator/ValidadorCPFCNPJ;
    invoke-static {}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCPFCNPJ$SingletonHolder;->access$000()Lbr/com/concretesolutions/canarinho/validator/ValidadorCPFCNPJ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public ehValido(Landroid/text/Editable;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;
    .registers 5
    .param p1, "valor"    # Landroid/text/Editable;
    .param p2, "resultadoParcial"    # Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    .line 35
    if-eqz p2, :cond_4

    if-nez p1, :cond_c

    .line 36
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Valores n\u00e3o podem ser nulos"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_c
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCPFCNPJ;->ehCpf(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 40
    invoke-static {}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF;->getInstance()Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF;->ehValido(Landroid/text/Editable;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    return-object v0

    .line 43
    :cond_1f
    invoke-static {}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCNPJ;->getInstance()Lbr/com/concretesolutions/canarinho/validator/ValidadorCNPJ;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCNPJ;->ehValido(Landroid/text/Editable;Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;)Lbr/com/concretesolutions/canarinho/validator/Validador$ResultadoParcial;

    move-result-object v0

    return-object v0
.end method

.method public ehValido(Ljava/lang/String;)Z
    .registers 4
    .param p1, "valor"    # Ljava/lang/String;

    .line 22
    if-eqz p1, :cond_12

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xb

    if-eq v0, v1, :cond_14

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xe

    if-eq v0, v1, :cond_14

    .line 23
    :cond_12
    const/4 v0, 0x0

    return v0

    .line 26
    :cond_14
    invoke-direct {p0, p1}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCPFCNPJ;->ehCpf(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 27
    invoke-static {}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF;->getInstance()Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF;->ehValido(Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 30
    :cond_23
    invoke-static {}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCNPJ;->getInstance()Lbr/com/concretesolutions/canarinho/validator/ValidadorCNPJ;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbr/com/concretesolutions/canarinho/validator/ValidadorCNPJ;->ehValido(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

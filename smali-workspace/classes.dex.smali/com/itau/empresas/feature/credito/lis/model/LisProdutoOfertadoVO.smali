.class public final Lcom/itau/empresas/feature/credito/lis/model/LisProdutoOfertadoVO;
.super Ljava/lang/Object;
.source "LisProdutoOfertadoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final produtoOfertado:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "produto_ofertado"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 2
    .param p1, "produtoOfertado"    # Ljava/lang/String;

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/lis/model/LisProdutoOfertadoVO;->produtoOfertado:Ljava/lang/String;

    .line 14
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1, "o"    # Ljava/lang/Object;

    .line 18
    if-ne p0, p1, :cond_4

    .line 19
    const/4 v0, 0x1

    return v0

    .line 21
    :cond_4
    if-eqz p1, :cond_10

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_12

    .line 22
    :cond_10
    const/4 v0, 0x0

    return v0

    .line 25
    :cond_12
    move-object v2, p1

    check-cast v2, Lcom/itau/empresas/feature/credito/lis/model/LisProdutoOfertadoVO;

    .line 27
    .local v2, "that":Lcom/itau/empresas/feature/credito/lis/model/LisProdutoOfertadoVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisProdutoOfertadoVO;->produtoOfertado:Ljava/lang/String;

    iget-object v1, v2, Lcom/itau/empresas/feature/credito/lis/model/LisProdutoOfertadoVO;->produtoOfertado:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .registers 2

    .line 33
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisProdutoOfertadoVO;->produtoOfertado:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.class public final Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;
.super Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;
.source "ComprovantesEnviarActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 39
    invoke-direct {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;-><init>()V

    .line 43
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;

    .line 39
    invoke-super {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;

    .line 39
    invoke-super {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 57
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 58
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 59
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 60
    invoke-static {p0}, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->comprovantesController:Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;

    .line 61
    invoke-direct {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->injectExtras_()V

    .line 62
    invoke-virtual {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->afterInject()V

    .line 63
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 121
    invoke-virtual {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 122
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_2e

    .line 123
    const-string v0, "comprovantesVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 124
    const-string v0, "comprovantesVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    .line 126
    :cond_1c
    const-string v0, "recargaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 127
    const-string v0, "recargaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->recargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    .line 130
    :cond_2e
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 84
    new-instance v0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 155
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$3;-><init>(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 163
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 143
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$2;-><init>(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 151
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 50
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->init_(Landroid/os/Bundle;)V

    .line 51
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 53
    const v0, 0x7f030027

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->setContentView(I)V

    .line 54
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 97
    const v0, 0x7f0e010e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->root:Landroid/widget/LinearLayout;

    .line 98
    const v0, 0x7f0e0110

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->textoComprovante:Landroid/widget/TextView;

    .line 99
    const v0, 0x7f0e0111

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->textoDataComprovante:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0e0112

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->textoNomeComprovante:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0e0113

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->textoValorComprovante:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f0e0114

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/material/MaterialEditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->campoEmail:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 103
    const v0, 0x7f0e0115

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/material/MaterialEditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->campoComentario:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 104
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 105
    const v0, 0x7f0e0116

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 107
    .local v1, "view_botao_enviar":Landroid/view/View;
    if-eqz v1, :cond_69

    .line 108
    new-instance v0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$1;-><init>(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    :cond_69
    invoke-virtual {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->carregaElementosDaTela()V

    .line 118
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 67
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->setContentView(I)V

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 69
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 79
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->setContentView(Landroid/view/View;)V

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 81
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 73
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 75
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 134
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->setIntent(Landroid/content/Intent;)V

    .line 135
    invoke-direct {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_;->injectExtras_()V

    .line 136
    return-void
.end method

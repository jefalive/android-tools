.class public Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "LisIntroducaoActivity.java"


# static fields
.field static condicoesGeraisAdicionalPDF:Ljava/lang/String;

.field static condicoesGeraisPDF:Ljava/lang/String;


# instance fields
.field controller:Lcom/itau/empresas/feature/credito/lis/LisController;

.field cvLisAdicional:Landroid/view/View;

.field llLisChequeEspecial:Landroid/view/View;

.field ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

.field scroView:Landroid/view/View;

.field textPreAprovado:Landroid/widget/TextView;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 34
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private configuraAcessibilidade()V
    .registers 5

    .line 120
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->llLisChequeEspecial:Landroid/view/View;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    .line 122
    const v2, 0x7f070404

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 123
    const v2, 0x7f070408

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 124
    const v2, 0x7f070432

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    .line 121
    const v2, 0x7f07041d

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 120
    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 125
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 57
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 59
    const v1, 0x7f07040a

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 60
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 62
    return-void
.end method

.method private iniciaLayout()V
    .registers 8

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    .line 68
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;->getValorNovoLimite()F

    move-result v0

    .line 67
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v4

    .line 69
    .local v4, "valorNovoLimite":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v4, v0, v1

    const v1, 0x7f070421

    invoke-virtual {p0, v1, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 70
    .line 71
    .local v5, "textoNovoLimite":Ljava/lang/String;
    const v0, 0x7f0c014d

    invoke-static {p0, v0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v4, v2, v3

    .line 70
    invoke-static {v0, v1, v5, v2}, Lcom/itau/empresas/ui/util/ViewUtils;->destacaStringAlteraCor(IZLjava/lang/String;[Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v6

    .line 73
    .local v6, "spannable":Landroid/text/Spannable;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->textPreAprovado:Landroid/widget/TextView;

    sget-object v1, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v6, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 75
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->possuiLisAdicional()Z

    move-result v0

    if-nez v0, :cond_43

    .line 76
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->cvLisAdicional:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 79
    :cond_43
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->configuraAcessibilidade()V

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->scroView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 81
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 128
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private possuiLisAdicional()Z
    .registers 4

    .line 115
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;->getProdutosOfertados()Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/model/LisProdutoOfertadoVO;

    const-string v2, "L03"

    invoke-direct {v1, v2}, Lcom/itau/empresas/feature/credito/lis/model/LisProdutoOfertadoVO;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method aoCarregarTela()V
    .registers 3

    .line 51
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->constroiToolbar()V

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->scroView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 53
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->iniciaLayout()V

    .line 54
    return-void
.end method

.method condicoesEspeciais()V
    .registers 3

    .line 90
    .line 91
    invoke-static {p0}, Lcom/itau/empresas/ui/activity/PDFActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/PDFActivity_$IntentBuilder_;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->condicoesGeraisPDF:Ljava/lang/String;

    .line 92
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/activity/PDFActivity_$IntentBuilder_;->url(Ljava/lang/String;)Lcom/itau/empresas/ui/activity/PDFActivity_$IntentBuilder_;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/PDFActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 94
    return-void
.end method

.method condicoesEspeciaisAdicional()V
    .registers 3

    .line 98
    .line 99
    invoke-static {p0}, Lcom/itau/empresas/ui/activity/PDFActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/PDFActivity_$IntentBuilder_;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->condicoesGeraisAdicionalPDF:Ljava/lang/String;

    .line 100
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/activity/PDFActivity_$IntentBuilder_;->url(Ljava/lang/String;)Lcom/itau/empresas/ui/activity/PDFActivity_$IntentBuilder_;

    move-result-object v0

    .line 101
    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/PDFActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 102
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 85
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method queroConhecer()V
    .registers 3

    .line 106
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    if-eqz v0, :cond_19

    .line 107
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    .line 108
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_$IntentBuilder_;->ofertaContratacaoLisVo(Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;)Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_$IntentBuilder_;

    move-result-object v0

    .line 109
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->possuiLisAdicional()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_$IntentBuilder_;->possuiProdutoLisAdiciona(Z)Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_$IntentBuilder_;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 112
    :cond_19
    return-void
.end method

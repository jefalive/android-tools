.class public final Lcom/itau/empresas/ui/util/ValorMonetarioUtils;
.super Ljava/lang/Object;
.source "ValorMonetarioUtils.java"


# static fields
.field public static final CURRENCY_UNIT:Lorg/joda/money/CurrencyUnit;

.field private static final FORMATADOR_MOEDA:Ljava/text/DecimalFormat;

.field private static final FORMATADOR_MOEDA_SEM_CIFRAO:Ljava/text/DecimalFormat;

.field public static final LOCALE_PT:Ljava/util/Locale;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 22
    const-string v0, "BRL"

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->CURRENCY_UNIT:Lorg/joda/money/CurrencyUnit;

    .line 23
    new-instance v0, Ljava/util/Locale;

    const-string v1, "pt"

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->LOCALE_PT:Ljava/util/Locale;

    .line 24
    new-instance v0, Ljava/util/Locale;

    const-string v1, "pt"

    const-string v2, "BR"

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Ljava/text/NumberFormat;->getCurrencyInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    check-cast v0, Ljava/text/DecimalFormat;

    sput-object v0, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->FORMATADOR_MOEDA:Ljava/text/DecimalFormat;

    .line 25
    new-instance v0, Ljava/util/Locale;

    const-string v1, "pt"

    const-string v2, "BR"

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Ljava/text/NumberFormat;->getCurrencyInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    check-cast v0, Ljava/text/DecimalFormat;

    sput-object v0, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->FORMATADOR_MOEDA_SEM_CIFRAO:Ljava/text/DecimalFormat;

    .line 28
    sget-object v0, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->FORMATADOR_MOEDA:Ljava/text/DecimalFormat;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->configurarFormatoMoeda(Ljava/text/DecimalFormat;Z)V

    .line 29
    sget-object v0, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->FORMATADOR_MOEDA_SEM_CIFRAO:Ljava/text/DecimalFormat;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->configurarFormatoMoeda(Ljava/text/DecimalFormat;Z)V

    .line 30
    return-void
.end method

.method private static configurarFormatoMoeda(Ljava/text/DecimalFormat;Z)V
    .registers 5
    .param p0, "formatter"    # Ljava/text/DecimalFormat;
    .param p1, "cifrao"    # Z

    .line 36
    if-eqz p1, :cond_3b

    .line 37
    invoke-virtual {p0}, Ljava/text/DecimalFormat;->getCurrency()Ljava/util/Currency;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Currency;->getSymbol()Ljava/lang/String;

    move-result-object v2

    .line 38
    .local v2, "symbol":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " -"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/text/DecimalFormat;->setNegativePrefix(Ljava/lang/String;)V

    .line 39
    const-string v0, ""

    invoke-virtual {p0, v0}, Ljava/text/DecimalFormat;->setNegativeSuffix(Ljava/lang/String;)V

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/text/DecimalFormat;->setPositivePrefix(Ljava/lang/String;)V

    .line 43
    .end local v2    # "symbol":Ljava/lang/String;
    :cond_3b
    invoke-virtual {p0}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object v2

    .line 44
    .local v2, "decimalFormatSymbols":Ljava/text/DecimalFormatSymbols;
    if-nez p1, :cond_46

    .line 45
    const-string v0, ""

    invoke-virtual {v2, v0}, Ljava/text/DecimalFormatSymbols;->setCurrencySymbol(Ljava/lang/String;)V

    .line 46
    :cond_46
    const/16 v0, 0x2e

    invoke-virtual {v2, v0}, Ljava/text/DecimalFormatSymbols;->setGroupingSeparator(C)V

    .line 47
    invoke-virtual {p0, v2}, Ljava/text/DecimalFormat;->setDecimalFormatSymbols(Ljava/text/DecimalFormatSymbols;)V

    .line 48
    return-void
.end method

.method public static destacaValorMonetario(Ljava/lang/String;)Landroid/text/SpannableString;
    .registers 7
    .param p0, "valor"    # Ljava/lang/String;

    .line 134
    const-string v0, " "

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 136
    .local v4, "splitted":[Ljava/lang/String;
    new-instance v5, Landroid/text/SpannableString;

    invoke-direct {v5, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 137
    .local v5, "spannableString":Landroid/text/SpannableString;
    const-string v0, " "

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_3e

    .line 138
    new-instance v0, Landroid/text/style/RelativeSizeSpan;

    const v1, 0x3f59999a    # 0.85f

    invoke-direct {v0, v1}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    const/4 v1, 0x0

    aget-object v1, v4, v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v5, v0, v2, v1, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 139
    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v1, 0x0

    aget-object v1, v4, v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 141
    :cond_3e
    return-object v5
.end method

.method public static destacaValorMonetario(Ljava/lang/String;F)Landroid/text/SpannableString;
    .registers 3
    .param p0, "valor"    # Ljava/lang/String;
    .param p1, "porcentagem"    # F

    .line 149
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->destacaValorMonetario(Ljava/lang/String;FZ)Landroid/text/SpannableString;

    move-result-object v0

    return-object v0
.end method

.method private static destacaValorMonetario(Ljava/lang/String;FZ)Landroid/text/SpannableString;
    .registers 9
    .param p0, "valor"    # Ljava/lang/String;
    .param p1, "porcentagem"    # F
    .param p2, "bold"    # Z

    .line 160
    const-string v0, " "

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 162
    .local v4, "splitted":[Ljava/lang/String;
    new-instance v5, Landroid/text/SpannableString;

    invoke-direct {v5, p0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 163
    .local v5, "spannableString":Landroid/text/SpannableString;
    const-string v0, " "

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_3d

    .line 164
    new-instance v0, Landroid/text/style/RelativeSizeSpan;

    invoke-direct {v0, p1}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    const/4 v1, 0x0

    aget-object v1, v4, v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v5, v0, v2, v1, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 165
    if-eqz p2, :cond_3d

    .line 166
    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v1, 0x0

    aget-object v1, v4, v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v5, v0, v1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 168
    :cond_3d
    return-object v5
.end method

.method public static formataParcelasEValor(Landroid/content/Context;IF)Ljava/lang/String;
    .registers 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "parcelas"    # I
    .param p2, "valor"    # F

    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " de "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p2, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static formataTaxaCustoEfetivoTotal(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "taxaEfetivoTotalMes"    # Ljava/lang/String;
    .param p2, "taxaEfetivoTotalAno"    # Ljava/lang/String;

    .line 254
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemSimples(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 255
    const v1, 0x7f070547

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 256
    invoke-static {p2}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemSimples(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 257
    const v1, 0x7f070546

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 254
    return-object v0
.end method

.method public static formataTaxaJuros(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "taxamensal"    # Ljava/lang/String;
    .param p2, "taxaanual"    # Ljava/lang/String;

    .line 297
    const-string v2, "%"

    .line 298
    .local v2, "simboloPercentual":Ljava/lang/String;
    move-object v3, p1

    .line 299
    .local v3, "taxaMensal":Ljava/lang/String;
    move-object v4, p2

    .line 302
    .local v4, "taxaAnual":Ljava/lang/String;
    invoke-virtual {v3, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1b

    .line 303
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 306
    :cond_1b
    invoke-virtual {v4, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_32

    .line 307
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 310
    :cond_32
    const-string v0, "."

    const-string v1, ","

    invoke-virtual {v3, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 311
    const-string v0, "."

    const-string v1, ","

    invoke-virtual {v4, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 313
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 314
    const v1, 0x7f070547

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 316
    const v1, 0x7f070545

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 313
    return-object v0
.end method

.method public static formataTextoValorEParcelas(Landroid/content/Context;Z)Ljava/lang/String;
    .registers 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "temSeguro"    # Z

    .line 274
    if-eqz p1, :cond_5

    .line 275
    const-string v2, " (com seguro)"

    .local v2, "labelSeguro":Ljava/lang/String;
    goto :goto_7

    .line 277
    .end local v2    # "labelSeguro":Ljava/lang/String;
    :cond_5
    const-string v2, ""

    .line 280
    .local v2, "labelSeguro":Ljava/lang/String;
    :goto_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f0701be

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static formataValorDecimalParaMonetario(DZ)Ljava/lang/String;
    .registers 4
    .param p0, "valor"    # D
    .param p2, "comCifrao"    # Z

    .line 81
    invoke-static {p0, p1}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static formataValorDecimalParaMonetario(FZ)Ljava/lang/String;
    .registers 4
    .param p0, "valor"    # F
    .param p1, "comCifrao"    # Z

    .line 74
    float-to-double v0, p0

    invoke-static {v0, v1}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static formataValorDecimalParaMonetario(JZ)Ljava/lang/String;
    .registers 4
    .param p0, "valor"    # J
    .param p2, "comCifrao"    # Z

    .line 98
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p0, p1}, Ljava/math/BigDecimal;-><init>(J)V

    invoke-static {v0, p2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;
    .registers 4
    .param p0, "valor"    # Ljava/lang/String;
    .param p1, "comCifrao"    # Z

    .line 88
    if-eqz p0, :cond_c

    .line 89
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 91
    :cond_c
    new-instance v0, Ljava/math/BigDecimal;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-static {v0, p1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;
    .registers 3
    .param p0, "valor"    # Ljava/math/BigDecimal;
    .param p1, "comCifrao"    # Z

    .line 110
    if-eqz p1, :cond_9

    sget-object v0, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->FORMATADOR_MOEDA:Ljava/text/DecimalFormat;

    .line 111
    invoke-virtual {v0, p0}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_f

    :cond_9
    sget-object v0, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->FORMATADOR_MOEDA_SEM_CIFRAO:Ljava/text/DecimalFormat;

    .line 112
    invoke-virtual {v0, p0}, Ljava/text/DecimalFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 110
    :goto_f
    return-object v0
.end method

.method public static formataValorDecimalParaMonetarioComCifrao(D)Ljava/lang/String;
    .registers 7
    .param p0, "valor"    # D

    .line 349
    const-wide/16 v0, 0x0

    cmpg-double v0, p0, v0

    if-gez v0, :cond_8

    const/4 v3, 0x1

    goto :goto_9

    :cond_8
    const/4 v3, 0x0

    .line 351
    .line 353
    .local v3, "negativo":Z
    :goto_9
    invoke-static {p0, p1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(DZ)Ljava/lang/String;

    move-result-object v4

    .line 355
    .local v4, "valorFormatado":Ljava/lang/String;
    if-eqz v3, :cond_28

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_29

    :cond_28
    move-object v0, v4

    :goto_29
    return-object v0
.end method

.method public static formataValorMonetarioParaDecimal(Ljava/lang/String;Z)Ljava/math/BigDecimal;
    .registers 6
    .param p0, "valor"    # Ljava/lang/String;
    .param p1, "cifrao"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 62
    if-eqz p1, :cond_5

    sget-object v2, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->FORMATADOR_MOEDA:Ljava/text/DecimalFormat;

    goto :goto_7

    :cond_5
    sget-object v2, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->FORMATADOR_MOEDA_SEM_CIFRAO:Ljava/text/DecimalFormat;

    .line 64
    .local v2, "formatter":Ljava/text/DecimalFormat;
    :goto_7
    :try_start_7
    new-instance v0, Ljava/math/BigDecimal;

    invoke-virtual {v2, p0}, Ljava/text/DecimalFormat;->parseObject(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V
    :try_end_14
    .catch Ljava/text/ParseException; {:try_start_7 .. :try_end_14} :catch_15

    return-object v0

    .line 65
    :catch_15
    move-exception v3

    .line 66
    .local v3, "px":Ljava/text/ParseException;
    throw v3
.end method

.method public static formataValorTotalFinanciado(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "taxa"    # Ljava/lang/String;

    .line 215
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 216
    invoke-static {p1}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemSimples(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 218
    const v1, 0x7f0701c9

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 215
    return-object v0
.end method

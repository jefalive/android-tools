.class public Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;
.super Landroid/widget/RelativeLayout;
.source "PulseLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/textovoz/ui/view/custom/PulseLayout$PulseView;
    }
.end annotation


# static fields
.field private static final DEFAULT_COLOR:I


# instance fields
.field private final animatorListener:Landroid/animation/Animator$AnimatorListener;

.field private animatorSet:Landroid/animation/AnimatorSet;

.field private centerX:F

.field private centerY:F

.field private color:I

.field private count:I

.field private duration:I

.field private interpolator:I

.field private isStarted:Z

.field private paint:Landroid/graphics/Paint;

.field private radius:F

.field private repeat:I

.field private startFromScratch:Z

.field private final views:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Landroid/view/View;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 35
    const/4 v0, 0x0

    const/16 v1, 0x74

    const/16 v2, 0xc1

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    sput v0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->DEFAULT_COLOR:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;

    .line 57
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 65
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->views:Ljava/util/List;

    .line 329
    new-instance v0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout$1;

    invoke-direct {v0, p0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout$1;-><init>(Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;)V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->animatorListener:Landroid/animation/Animator$AnimatorListener;

    .line 68
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lbr/com/itau/textovoz/R$styleable;->Pulsator4Droid:[I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, p2, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 71
    .local v4, "attr":Landroid/content/res/TypedArray;
    const/4 v0, 0x4

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->count:I

    .line 72
    const/16 v0, 0x1b58

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->duration:I

    .line 73
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->repeat:I

    .line 74
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->startFromScratch:Z

    .line 75
    sget v0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->DEFAULT_COLOR:I

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->color:I

    .line 76
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->interpolator:I

    .line 79
    :try_start_31
    sget v0, Lbr/com/itau/textovoz/R$styleable;->Pulsator4Droid_pulse_count:I

    const/4 v1, 0x4

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->count:I

    .line 80
    sget v0, Lbr/com/itau/textovoz/R$styleable;->Pulsator4Droid_pulse_duration:I

    const/16 v1, 0x1b58

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->duration:I

    .line 82
    sget v0, Lbr/com/itau/textovoz/R$styleable;->Pulsator4Droid_pulse_repeat:I

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->repeat:I

    .line 83
    sget v0, Lbr/com/itau/textovoz/R$styleable;->Pulsator4Droid_pulse_startFromScratch:I

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->startFromScratch:Z

    .line 85
    sget v0, Lbr/com/itau/textovoz/R$styleable;->Pulsator4Droid_pulse_color:I

    sget v1, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->DEFAULT_COLOR:I

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->color:I

    .line 86
    sget v0, Lbr/com/itau/textovoz/R$styleable;->Pulsator4Droid_pulse_interpolator:I

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->interpolator:I
    :try_end_69
    .catchall {:try_start_31 .. :try_end_69} :catchall_6d

    .line 89
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 90
    goto :goto_72

    .line 89
    :catchall_6d
    move-exception v5

    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    throw v5

    .line 93
    :goto_72
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->paint:Landroid/graphics/Paint;

    .line 94
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->paint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 95
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 96
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->color:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 99
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->build()V

    .line 100
    return-void
.end method

.method static synthetic access$000(Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;)F
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;

    .line 25
    iget v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->centerX:F

    return v0
.end method

.method static synthetic access$100(Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;)F
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;

    .line 25
    iget v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->centerY:F

    return v0
.end method

.method static synthetic access$200(Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;)F
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;

    .line 25
    iget v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->radius:F

    return v0
.end method

.method static synthetic access$300(Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;)Landroid/graphics/Paint;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;

    .line 25
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->paint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$402(Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;Z)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;
    .param p1, "x1"    # Z

    .line 25
    iput-boolean p1, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->isStarted:Z

    return p1
.end method

.method private build()V
    .registers 15

    .line 236
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    const/4 v1, -0x1

    invoke-direct {v4, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 240
    .local v4, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->repeat:I

    if-nez v0, :cond_d

    const/4 v5, -0x1

    goto :goto_f

    :cond_d
    iget v5, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->repeat:I

    .line 242
    .local v5, "repeatCount":I
    :goto_f
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 243
    .local v6, "animators":Ljava/util/List;, "Ljava/util/List<Landroid/animation/Animator;>;"
    const/4 v7, 0x0

    .local v7, "index":I
    :goto_15
    iget v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->count:I

    if-ge v7, v0, :cond_8d

    .line 245
    new-instance v8, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout$PulseView;

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v8, p0, v0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout$PulseView;-><init>(Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;Landroid/content/Context;)V

    .line 246
    .local v8, "pulseView":Lbr/com/itau/textovoz/ui/view/custom/PulseLayout$PulseView;
    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout$PulseView;->setScaleX(F)V

    .line 247
    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout$PulseView;->setScaleY(F)V

    .line 248
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v8, v0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout$PulseView;->setAlpha(F)V

    .line 250
    invoke-virtual {p0, v8, v7, v4}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 251
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->views:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 253
    iget v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->duration:I

    mul-int/2addr v0, v7

    iget v1, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->count:I

    div-int/2addr v0, v1

    int-to-long v9, v0

    .line 256
    .local v9, "delay":J
    const-string v0, "ScaleX"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_b4

    invoke-static {v8, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v11

    .line 257
    .local v11, "scaleXAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v11, v5}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 258
    const/4 v0, 0x1

    invoke-virtual {v11, v0}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    .line 259
    invoke-virtual {v11, v9, v10}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 260
    invoke-interface {v6, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 262
    const-string v0, "ScaleY"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_bc

    invoke-static {v8, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v12

    .line 263
    .local v12, "scaleYAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v12, v5}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 264
    const/4 v0, 0x1

    invoke-virtual {v12, v0}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    .line 265
    invoke-virtual {v12, v9, v10}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 266
    invoke-interface {v6, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 268
    const-string v0, "Alpha"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_c4

    invoke-static {v8, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v13

    .line 269
    .local v13, "alphaAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v13, v5}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 270
    const/4 v0, 0x1

    invoke-virtual {v13, v0}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    .line 271
    invoke-virtual {v13, v9, v10}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 272
    invoke-interface {v6, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    .end local v8    # "pulseView":Lbr/com/itau/textovoz/ui/view/custom/PulseLayout$PulseView;
    .end local v9    # "delay":J
    .end local v11    # "scaleXAnimator":Landroid/animation/ObjectAnimator;
    .end local v12    # "scaleYAnimator":Landroid/animation/ObjectAnimator;
    .end local v13    # "alphaAnimator":Landroid/animation/ObjectAnimator;
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_15

    .line 275
    .end local v7    # "index":I
    :cond_8d
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->animatorSet:Landroid/animation/AnimatorSet;

    .line 276
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, v6}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 277
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->animatorSet:Landroid/animation/AnimatorSet;

    iget v1, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->interpolator:I

    invoke-static {v1}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->createInterpolator(I)Landroid/view/animation/Interpolator;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 278
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->animatorSet:Landroid/animation/AnimatorSet;

    iget v1, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->duration:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 279
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->animatorSet:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->animatorListener:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 280
    return-void

    :array_b4
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_bc
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_c4
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private static createInterpolator(I)Landroid/view/animation/Interpolator;
    .registers 2
    .param p0, "type"    # I

    .line 294
    packed-switch p0, :pswitch_data_1c

    goto :goto_16

    .line 296
    :pswitch_4
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    return-object v0

    .line 298
    :pswitch_a
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    return-object v0

    .line 300
    :pswitch_10
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    return-object v0

    .line 302
    :goto_16
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    return-object v0

    :pswitch_data_1c
    .packed-switch 0x1
        :pswitch_4
        :pswitch_a
        :pswitch_10
    .end packed-switch
.end method

.method private reset()V
    .registers 2

    .line 283
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->isStarted()Z

    move-result v0

    .line 285
    .local v0, "isStarted":Z
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->clear()V

    .line 286
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->build()V

    .line 288
    if-eqz v0, :cond_f

    .line 289
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->start()V

    .line 291
    :cond_f
    return-void
.end method


# virtual methods
.method public clear()V
    .registers 4

    .line 205
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->stop()V

    .line 208
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->views:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/View;

    .line 209
    .local v2, "view":Landroid/view/View;
    invoke-virtual {p0, v2}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->removeView(Landroid/view/View;)V

    .line 210
    .end local v2    # "view":Landroid/view/View;
    goto :goto_9

    .line 211
    :cond_1a
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->views:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 212
    return-void
.end method

.method public declared-synchronized isStarted()Z
    .registers 3

    monitor-enter p0

    .line 130
    :try_start_1
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->animatorSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_b

    iget-boolean v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->isStarted:Z
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_e

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    goto :goto_c

    :cond_b
    const/4 v0, 0x0

    :goto_c
    monitor-exit p0

    return v0

    :catchall_e
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method protected onDetachedFromWindow()V
    .registers 2

    .line 308
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 310
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->animatorSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_f

    .line 311
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 312
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->animatorSet:Landroid/animation/AnimatorSet;

    .line 314
    :cond_f
    return-void
.end method

.method public onMeasure(II)V
    .registers 7
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .line 193
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->getPaddingRight()I

    move-result v1

    sub-int v2, v0, v1

    .line 194
    .local v2, "width":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->getPaddingBottom()I

    move-result v1

    sub-int v3, v0, v1

    .line 196
    .local v3, "height":I
    int-to-float v0, v2

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->centerX:F

    .line 197
    int-to-float v0, v3

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->centerY:F

    .line 198
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v0, v1

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->radius:F

    .line 200
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 201
    return-void
.end method

.method public setColor(I)V
    .registers 3
    .param p1, "color"    # I

    .line 170
    iget v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->color:I

    if-eq p1, v0, :cond_f

    .line 171
    iput p1, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->color:I

    .line 173
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->paint:Landroid/graphics/Paint;

    if-eqz v0, :cond_f

    .line 174
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 177
    :cond_f
    return-void
.end method

.method public setCount(I)V
    .registers 4
    .param p1, "count"    # I

    .line 142
    if-gez p1, :cond_a

    .line 143
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "count cannot be negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 146
    :cond_a
    iget v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->count:I

    if-eq p1, v0, :cond_16

    .line 147
    iput p1, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->count:I

    .line 148
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->reset()V

    .line 149
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->invalidate()V

    .line 151
    :cond_16
    return-void
.end method

.method public setDuration(I)V
    .registers 4
    .param p1, "millis"    # I

    .line 154
    if-gez p1, :cond_a

    .line 155
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Duration cannot be negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 158
    :cond_a
    iget v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->duration:I

    if-eq p1, v0, :cond_16

    .line 159
    iput p1, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->duration:I

    .line 160
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->reset()V

    .line 161
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->invalidate()V

    .line 163
    :cond_16
    return-void
.end method

.method public setInterpolator(I)V
    .registers 3
    .param p1, "type"    # I

    .line 184
    iget v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->interpolator:I

    if-eq p1, v0, :cond_c

    .line 185
    iput p1, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->interpolator:I

    .line 186
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->reset()V

    .line 187
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->invalidate()V

    .line 189
    :cond_c
    return-void
.end method

.method public declared-synchronized start()V
    .registers 9

    monitor-enter p0

    .line 103
    :try_start_1
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->animatorSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->isStarted:Z

    if-eqz v0, :cond_b

    .line 104
    :cond_9
    monitor-exit p0

    return-void

    .line 107
    :cond_b
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 109
    iget-boolean v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->startFromScratch:Z

    if-nez v0, :cond_3f

    .line 110
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->getChildAnimations()Ljava/util/ArrayList;

    move-result-object v2

    .line 111
    .local v2, "animators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1e
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/animation/Animator;

    .line 112
    .local v4, "animator":Landroid/animation/Animator;
    move-object v5, v4

    check-cast v5, Landroid/animation/ObjectAnimator;

    .line 114
    .local v5, "objectAnimator":Landroid/animation/ObjectAnimator;
    invoke-virtual {v5}, Landroid/animation/ObjectAnimator;->getStartDelay()J

    move-result-wide v6

    .line 115
    .local v6, "delay":J
    const-wide/16 v0, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 116
    iget v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->duration:I

    int-to-long v0, v0

    sub-long/2addr v0, v6

    invoke-virtual {v5, v0, v1}, Landroid/animation/ObjectAnimator;->setCurrentPlayTime(J)V
    :try_end_3e
    .catchall {:try_start_1 .. :try_end_3e} :catchall_41

    .line 117
    .end local v4    # "animator":Landroid/animation/Animator;
    .end local v5    # "objectAnimator":Landroid/animation/ObjectAnimator;
    .end local v6    # "delay":J
    goto :goto_1e

    .line 119
    .end local v2    # "animators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/animation/Animator;>;"
    .end local v2
    :cond_3f
    monitor-exit p0

    return-void

    :catchall_41
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized stop()V
    .registers 3

    monitor-enter p0

    .line 122
    :try_start_1
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->animatorSet:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->isStarted:Z

    if-nez v0, :cond_b

    .line 123
    :cond_9
    monitor-exit p0

    return-void

    .line 126
    :cond_b
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/custom/PulseLayout;->animatorSet:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V
    :try_end_10
    .catchall {:try_start_1 .. :try_end_10} :catchall_12

    .line 127
    monitor-exit p0

    return-void

    :catchall_12
    move-exception v1

    monitor-exit p0

    throw v1
.end method

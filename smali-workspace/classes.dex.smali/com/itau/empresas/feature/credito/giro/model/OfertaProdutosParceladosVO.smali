.class public Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;
.super Ljava/lang/Object;
.source "OfertaProdutosParceladosVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private codigoPlano:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_plano"
    .end annotation
.end field

.field private codigoProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_produto"
    .end annotation
.end field

.field private descricaoOfertaComSeguro:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_oferta_com_seguro"
    .end annotation
.end field

.field private descricaoOfertaSemSeguro:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_oferta_sem_seguro"
    .end annotation
.end field

.field private hasIndicadorCartao:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_cartao"
    .end annotation
.end field

.field private hasIndicadorSeguro:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_seguro"
    .end annotation
.end field

.field private indicadorTransbordo:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_transbordo"
    .end annotation
.end field

.field private jurosTaxaMaxima:D
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros_maxima"
    .end annotation
.end field

.field private jurosTaxaMinima:D
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros_minima"
    .end annotation
.end field

.field private nomeProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_produto"
    .end annotation
.end field

.field private quantidadeDiasPrimeiroPagamento:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade_dias_primeiro_pagamento"
    .end annotation
.end field

.field private quantidadeMaximaParcelas:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade_maxima_parcelas"
    .end annotation
.end field

.field private quantidadeMinimaParcelas:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade_minima_parcelas"
    .end annotation
.end field

.field private tipoOferta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_oferta"
    .end annotation
.end field

.field private valorMaximoComSeguro:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_maximo_com_seguro"
    .end annotation
.end field

.field private valorMaximoContratacao:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_maximo_contratacao"
    .end annotation
.end field

.field private valorMaximoSemSeguro:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_maximo_sem_seguro"
    .end annotation
.end field

.field private valorMinimoComSeguro:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_minimo_com_seguro"
    .end annotation
.end field

.field private valorMinimoContratacao:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_minimo_contratacao"
    .end annotation
.end field

.field private valorMinimoSemSeguro:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_minimo_sem_seguro"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCodigoPlano()Ljava/lang/String;
    .registers 2

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->codigoPlano:Ljava/lang/String;

    return-object v0
.end method

.method public getCodigoProduto()Ljava/lang/String;
    .registers 2

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->codigoProduto:Ljava/lang/String;

    return-object v0
.end method

.method public getIndicadorTransbordo()Z
    .registers 2

    .line 205
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->indicadorTransbordo:Z

    return v0
.end method

.method public getJurosTaxaMaxima()D
    .registers 3

    .line 127
    iget-wide v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->jurosTaxaMaxima:D

    return-wide v0
.end method

.method public getJurosTaxaMinima()D
    .registers 3

    .line 119
    iget-wide v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->jurosTaxaMinima:D

    return-wide v0
.end method

.method public getNomeProduto()Ljava/lang/String;
    .registers 2

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->nomeProduto:Ljava/lang/String;

    return-object v0
.end method

.method public getQuantidadeDiasPrimeiroPagamento()I
    .registers 2

    .line 111
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->quantidadeDiasPrimeiroPagamento:I

    return v0
.end method

.method public getQuantidadeMaximaParcelas()I
    .registers 2

    .line 95
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->quantidadeMaximaParcelas:I

    return v0
.end method

.method public getQuantidadeMinimaParcelas()I
    .registers 2

    .line 103
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->quantidadeMinimaParcelas:I

    return v0
.end method

.method public getTipoOferta()Ljava/lang/String;
    .registers 2

    .line 200
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->tipoOferta:Ljava/lang/String;

    return-object v0
.end method

.method public getValorMaximoContratacao()Ljava/math/BigDecimal;
    .registers 2

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->valorMaximoContratacao:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public isHasIndicadorSeguro()Z
    .registers 2

    .line 167
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosVO;->hasIndicadorSeguro:Z

    return v0
.end method

.class public Lcom/itau/empresas/BoletoUtils$Boleto;
.super Ljava/lang/Object;
.source "BoletoUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/BoletoUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Boleto"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/BoletoUtils$Boleto$Estado;
    }
.end annotation


# instance fields
.field private codigo:Ljava/lang/String;

.field private estado:Lcom/itau/empresas/BoletoUtils$Boleto$Estado;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCodigo()Ljava/lang/String;
    .registers 2

    .line 169
    iget-object v0, p0, Lcom/itau/empresas/BoletoUtils$Boleto;->codigo:Ljava/lang/String;

    return-object v0
.end method

.method public getEstado()Lcom/itau/empresas/BoletoUtils$Boleto$Estado;
    .registers 2

    .line 161
    iget-object v0, p0, Lcom/itau/empresas/BoletoUtils$Boleto;->estado:Lcom/itau/empresas/BoletoUtils$Boleto$Estado;

    return-object v0
.end method

.method public setCodigo(Ljava/lang/String;)V
    .registers 2
    .param p1, "codigo"    # Ljava/lang/String;

    .line 173
    iput-object p1, p0, Lcom/itau/empresas/BoletoUtils$Boleto;->codigo:Ljava/lang/String;

    .line 174
    return-void
.end method

.method public setEstado(Lcom/itau/empresas/BoletoUtils$Boleto$Estado;)V
    .registers 2
    .param p1, "estado"    # Lcom/itau/empresas/BoletoUtils$Boleto$Estado;

    .line 165
    iput-object p1, p0, Lcom/itau/empresas/BoletoUtils$Boleto;->estado:Lcom/itau/empresas/BoletoUtils$Boleto$Estado;

    .line 166
    return-void
.end method

.class public Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter;
.super Ljava/lang/Object;
.source "DetalhesMultilimiteConverter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final DIGITOS_INICIAIS:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 27
    const-string v0, "^\\d+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter;->DIGITOS_INICIAIS:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private converteDetalhesEmAtributosVO(Ljava/util/List;)Ljava/util/List;
    .registers 10
    .param p1, "detalhes"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;>;)Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;>;"
        }
    .end annotation

    .line 62
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 63
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_50

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;

    .line 64
    .local v3, "d":Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;
    new-instance v4, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;

    invoke-direct {v4}, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;-><init>()V

    .line 65
    .local v4, "atributo":Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;
    invoke-virtual {v3}, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;->getLiteral()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;->setLiteral(Ljava/lang/String;)V

    .line 66
    invoke-virtual {v3}, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;->getValor()Ljava/lang/String;

    move-result-object v5

    .line 67
    .local v5, "valor":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;->getSinal()Ljava/lang/String;

    move-result-object v6

    .line 68
    .local v6, "sinal":Ljava/lang/String;
    if-eqz v5, :cond_49

    if-eqz v6, :cond_49

    .line 69
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 71
    const-string v0, "P"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_42

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_42

    const/4 v7, 0x1

    goto :goto_43

    :cond_42
    const/4 v7, 0x0

    .line 73
    .local v7, "negativo":Z
    :goto_43
    if-eqz v7, :cond_49

    .line 74
    invoke-direct {p0, v5}, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter;->insereSinalValor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 78
    .end local v7    # "negativo":Z
    :cond_49
    invoke-virtual {v4, v5}, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;->setValor(Ljava/lang/String;)V

    .line 79
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    .end local v3    # "d":Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;
    .end local v4    # "atributo":Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;
    .end local v5    # "valor":Ljava/lang/String;
    .end local v6    # "sinal":Ljava/lang/String;
    goto :goto_9

    .line 81
    :cond_50
    return-object v1
.end method

.method private converteDicionarioEmProdutosVO(Ljava/util/Map;)Ljava/util/List;
    .registers 7
    .param p1, "dict"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;>;>;)Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;>;"
        }
    .end annotation

    .line 47
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 48
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;>;"
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_33

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/String;

    .line 49
    .local v3, "titulo":Ljava/lang/String;
    new-instance v4, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;

    invoke-direct {v4}, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;-><init>()V

    .line 50
    .local v4, "produto":Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;
    invoke-virtual {v4, v3}, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;->setTitulo(Ljava/lang/String;)V

    .line 51
    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter;->converteDetalhesEmAtributosVO(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;->setAtributos(Ljava/util/List;)V

    .line 53
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    .end local v3    # "titulo":Ljava/lang/String;
    .end local v4    # "produto":Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;
    goto :goto_d

    .line 56
    :cond_33
    return-object v1
.end method

.method private indexFirstDigit(Ljava/lang/String;)I
    .registers 5
    .param p1, "s"    # Ljava/lang/String;

    .line 95
    const/4 v1, -0x1

    .line 96
    .local v1, "indexFirstDigit":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v2, v0, :cond_17

    .line 97
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isDigit(C)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 98
    move v1, v2

    .line 99
    goto :goto_17

    .line 96
    :cond_14
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 103
    .end local v2    # "i":I
    :cond_17
    :goto_17
    return v1
.end method

.method private insereSinalValor(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .param p1, "valor"    # Ljava/lang/String;

    .line 85
    invoke-direct {p0, p1}, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter;->indexFirstDigit(Ljava/lang/String;)I

    move-result v2

    .line 87
    .local v2, "i":I
    const/4 v0, -0x1

    if-ne v2, v0, :cond_8

    .line 88
    return-object p1

    .line 91
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private juntaDicionarioProdutos(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;
    .registers 8
    .param p1, "produtoA"    # Ljava/util/Map;
    .param p2, "produtoB"    # Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;>;>;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;>;>;)Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;>;>;"
        }
    .end annotation

    .line 108
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 110
    .local v2, "result":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;>;>;"
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/String;

    .line 111
    .local v4, "nomeProduto":Ljava/lang/String;
    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    .end local v4    # "nomeProduto":Ljava/lang/String;
    goto :goto_d

    .line 114
    :cond_22
    invoke-interface {p2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_55

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/String;

    .line 115
    .local v4, "nomeProduto":Ljava/lang/String;
    invoke-interface {v2, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 116
    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {p2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_54

    .line 118
    :cond_4d
    invoke-interface {p2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    .end local v4    # "nomeProduto":Ljava/lang/String;
    :goto_54
    goto :goto_2a

    .line 122
    :cond_55
    return-object v2
.end method

.method private limparDetalhes(Ljava/util/List;)Ljava/util/List;
    .registers 10
    .param p1, "produto"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<+Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Produto;>;)Ljava/util/List<Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Produto;>;"
        }
    .end annotation

    .line 153
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 155
    .local v1, "listaProd":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Produto;>;"
    if-nez p1, :cond_8

    .line 156
    return-object v1

    .line 159
    :cond_8
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_c
    :goto_c
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_63

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Produto;

    .line 160
    .local v3, "p":Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Produto;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 161
    .local v4, "listaDetalhe":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;>;"
    invoke-virtual {v3}, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Produto;->getDetalhes()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-virtual {v3}, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Produto;->getDetalhes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 162
    goto :goto_c

    .line 165
    :cond_2f
    invoke-virtual {v3}, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Produto;->getDetalhes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_37
    :goto_37
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_55

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;

    .line 166
    .local v6, "d":Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;
    invoke-virtual {v6}, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;->getLiteral()Ljava/lang/String;

    move-result-object v7

    .line 167
    .local v7, "literal":Ljava/lang/String;
    if-eqz v7, :cond_37

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_51

    .line 168
    goto :goto_37

    .line 170
    :cond_51
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    .end local v6    # "d":Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;
    .end local v7    # "literal":Ljava/lang/String;
    goto :goto_37

    .line 172
    :cond_55
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_61

    .line 173
    invoke-virtual {v3, v4}, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Produto;->setDetalhes(Ljava/util/List;)V

    .line 174
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    .end local v3    # "p":Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Produto;
    .end local v4    # "listaDetalhe":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;>;"
    .end local v4
    :cond_61
    goto/16 :goto_c

    .line 178
    :cond_63
    return-object v1
.end method

.method private removerDigitosIniciais(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .param p1, "titulo"    # Ljava/lang/String;

    .line 149
    sget-object v0, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter;->DIGITOS_INICIAIS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceFirst(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private transformaProdutosEmDicionario(Ljava/util/List;)Ljava/util/Map;
    .registers 11
    .param p1, "listaProdutos"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<+Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Produto;>;)Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;>;>;"
        }
    .end annotation

    .line 127
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    .line 129
    .local v2, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;>;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_50

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Produto;

    .line 130
    .local v4, "p":Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Produto;
    const/4 v5, 0x0

    .line 131
    .local v5, "titulo":Ljava/lang/String;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 132
    .local v6, "detalhes":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;>;"
    invoke-virtual {v4}, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Produto;->getDetalhes()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_24
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4a

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;

    .line 133
    .local v8, "d":Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;
    invoke-virtual {v8}, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;->getLiteral()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Nome"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_46

    .line 134
    invoke-virtual {v8}, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;->getValor()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter;->removerDigitosIniciais(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_49

    .line 136
    :cond_46
    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    .end local v8    # "d":Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;
    :goto_49
    goto :goto_24

    .line 140
    :cond_4a
    if-eqz v5, :cond_4f

    .line 141
    invoke-interface {v2, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    .end local v4    # "p":Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Produto;
    .end local v5    # "titulo":Ljava/lang/String;
    .end local v6    # "detalhes":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;>;"
    .end local v6
    :cond_4f
    goto :goto_9

    .line 145
    :cond_50
    return-object v2
.end method


# virtual methods
.method public toModel(Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO;)Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteVO;
    .registers 9
    .param p1, "detalhesMultilimite"    # Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO;

    .line 31
    new-instance v2, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteVO;

    invoke-direct {v2}, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteVO;-><init>()V

    .line 33
    .local v2, "result":Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteVO;
    invoke-virtual {p1}, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO;->getDados()Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Dados;

    move-result-object v3

    .line 35
    .local v3, "dados":Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Dados;
    invoke-virtual {v3}, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Dados;->getProdutoYW04()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter;->limparDetalhes(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 36
    .local v4, "produtosYW04":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Produto;>;"
    invoke-virtual {v3}, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Dados;->getProdutoYW10()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter;->limparDetalhes(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    .line 38
    .line 39
    .local v5, "produtosYW10":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Produto;>;"
    invoke-direct {p0, v4}, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter;->transformaProdutosEmDicionario(Ljava/util/List;)Ljava/util/Map;

    move-result-object v0

    .line 40
    invoke-direct {p0, v5}, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter;->transformaProdutosEmDicionario(Ljava/util/List;)Ljava/util/Map;

    move-result-object v1

    .line 38
    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter;->juntaDicionarioProdutos(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v6

    .line 41
    .local v6, "dictProdutos":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;>;>;"
    invoke-direct {p0, v6}, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter;->converteDicionarioEmProdutosVO(Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteVO;->setProdutos(Ljava/util/List;)V

    .line 42
    return-object v2
.end method

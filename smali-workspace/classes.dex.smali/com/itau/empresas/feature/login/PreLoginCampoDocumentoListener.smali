.class public Lcom/itau/empresas/feature/login/PreLoginCampoDocumentoListener;
.super Ljava/lang/Object;
.source "PreLoginCampoDocumentoListener.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/login/PreLoginCampoDocumentoListener$NotificaCampoDocumentoAlterado;
    }
.end annotation


# instance fields
.field private notificaCampoDocumentoAlterado:Lcom/itau/empresas/feature/login/PreLoginCampoDocumentoListener$NotificaCampoDocumentoAlterado;


# direct methods
.method private constructor <init>()V
    .registers 1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/widget/EditText;Lcom/itau/empresas/feature/login/PreLoginCampoDocumentoListener$NotificaCampoDocumentoAlterado;)V
    .registers 3
    .param p1, "campoDocumento"    # Landroid/widget/EditText;
    .param p2, "notificaCampoDocumentoAlterado"    # Lcom/itau/empresas/feature/login/PreLoginCampoDocumentoListener$NotificaCampoDocumentoAlterado;

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-virtual {p1, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 23
    iput-object p2, p0, Lcom/itau/empresas/feature/login/PreLoginCampoDocumentoListener;->notificaCampoDocumentoAlterado:Lcom/itau/empresas/feature/login/PreLoginCampoDocumentoListener$NotificaCampoDocumentoAlterado;

    .line 24
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 3
    .param p1, "s"    # Landroid/text/Editable;

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginCampoDocumentoListener;->notificaCampoDocumentoAlterado:Lcom/itau/empresas/feature/login/PreLoginCampoDocumentoListener$NotificaCampoDocumentoAlterado;

    invoke-interface {v0}, Lcom/itau/empresas/feature/login/PreLoginCampoDocumentoListener$NotificaCampoDocumentoAlterado;->campoDocumentoAlterado()V

    .line 39
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .line 29
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .line 34
    return-void
.end method

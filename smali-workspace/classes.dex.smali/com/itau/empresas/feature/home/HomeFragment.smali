.class public Lcom/itau/empresas/feature/home/HomeFragment;
.super Lcom/itau/empresas/ui/fragment/BaseFragment;
.source "HomeFragment.java"


# instance fields
.field private adapter:Lcom/itau/empresas/feature/home/adapter/HomeAdapter;

.field private isPoynt:Z

.field private listaHome:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/Object;>;"
        }
    .end annotation
.end field

.field logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

.field pendenciasController:Lcom/itau/empresas/feature/pendencias/PendenciasController;

.field private pendenciasVO:Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

.field rvHome:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 39
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;-><init>()V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->listaHome:Ljava/util/List;

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->isPoynt:Z

    return-void
.end method

.method private adicionaErroPendencias()V
    .registers 3

    .line 127
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->pendenciasVO:Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;->setTotalPendencias(I)V

    .line 128
    invoke-static {}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismissPrevious()V

    .line 129
    return-void
.end method

.method private apresentaAlertasDaHome()V
    .registers 2

    .line 95
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/HomeFragment;->deveMostrarSinoPendencias()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 96
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/HomeFragment;->buscaPendencias()V

    .line 97
    return-void

    .line 100
    :cond_a
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/HomeFragment;->montaCardsHome()V

    .line 101
    return-void
.end method

.method private atualizaAdapter(Ljava/util/List;)V
    .registers 3
    .param p1, "items"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/Object;>;)V"
        }
    .end annotation

    .line 132
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->adapter:Lcom/itau/empresas/feature/home/adapter/HomeAdapter;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/home/adapter/HomeAdapter;->atualizaAdapter(Ljava/util/List;)V

    .line 133
    return-void
.end method

.method private buscaHomeTabbar()Ljava/util/List;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;"
        }
    .end annotation

    .line 163
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getMenuVO()Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v0

    if-eqz v0, :cond_35

    .line 164
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getMenuVO()Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_16
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/itau/empresas/api/model/MenuVO;

    .line 165
    .local v3, "menuVO":Lcom/itau/empresas/api/model/MenuVO;
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v0

    const-string v1, "tabbar_home"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    .line 166
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 168
    .end local v3    # "menuVO":Lcom/itau/empresas/api/model/MenuVO;
    :cond_34
    goto :goto_16

    .line 170
    :cond_35
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private buscaPendencias()V
    .registers 6

    .line 109
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getPendenciasVO()Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 110
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getPendenciasVO()Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->pendenciasVO:Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

    .line 111
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/HomeFragment;->montaCardsHome()V

    .line 112
    return-void

    .line 115
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ViewUtils;->temConta(Lcom/itau/empresas/CustomApplication;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 116
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->pendenciasController:Lcom/itau/empresas/feature/pendencias/PendenciasController;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/HomeFragment;->application:Lcom/itau/empresas/CustomApplication;

    .line 117
    invoke-static {v1}, Lcom/itau/empresas/ui/util/ViewUtils;->getContaCompleta(Lcom/itau/empresas/CustomApplication;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/home/HomeFragment;->application:Lcom/itau/empresas/CustomApplication;

    .line 118
    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v2

    .line 119
    invoke-static {}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->getDataAtual()Ljava/lang/String;

    move-result-object v3

    .line 120
    invoke-static {}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->getDataAtual()Ljava/lang/String;

    move-result-object v4

    .line 116
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/itau/empresas/feature/pendencias/PendenciasController;->buscaPendencias(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3f

    .line 122
    :cond_36
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/logout/LogoutHelper;->trataSessaoExpirada(Landroid/app/Activity;)V

    .line 124
    :goto_3f
    return-void
.end method

.method private criaListaCardsHome()V
    .registers 5

    .line 196
    new-instance v3, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 197
    .local v3, "manager":Landroid/support/v7/widget/RecyclerView$LayoutManager;
    new-instance v0, Lcom/itau/empresas/feature/home/adapter/HomeAdapter;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/itau/empresas/feature/home/HomeFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/home/adapter/HomeAdapter;-><init>(Ljava/util/List;Lcom/itau/empresas/CustomApplication;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->adapter:Lcom/itau/empresas/feature/home/adapter/HomeAdapter;

    .line 199
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->rvHome:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 200
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->rvHome:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/HomeFragment;->adapter:Lcom/itau/empresas/feature/home/adapter/HomeAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 202
    return-void
.end method

.method private deveMostrarSinoPendencias()Z
    .registers 3

    .line 104
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->application:Lcom/itau/empresas/CustomApplication;

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_SINO_PENDENCIAS:Lcom/itau/empresas/CustomApplication$Exibicao;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->verificarPermissao(Lcom/itau/empresas/CustomApplication$Exibicao;)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-boolean v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->isPoynt:Z

    if-nez v0, :cond_10

    const/4 v0, 0x1

    goto :goto_11

    :cond_10
    const/4 v0, 0x0

    :goto_11
    return v0
.end method

.method private disparaEventoAnalytics()V
    .registers 5

    .line 205
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v3

    .line 207
    .line 208
    .local v3, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    const v0, 0x7f0700e4

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 209
    const v1, 0x7f0700c9

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/home/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 210
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/home/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 207
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    .line 213
    const v0, 0x7f0700e3

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 214
    const v1, 0x7f0700bd

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/home/HomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    .line 212
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    return-void
.end method

.method private montaCardsHome()V
    .registers 4

    .line 175
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->adapter:Lcom/itau/empresas/feature/home/adapter/HomeAdapter;

    if-eqz v0, :cond_5

    .line 176
    return-void

    .line 179
    :cond_5
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/HomeFragment;->criaListaCardsHome()V

    .line 181
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/HomeFragment;->verificaPermissoes()Ljava/util/List;

    move-result-object v2

    .line 183
    .local v2, "cards":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->listaHome:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 185
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->listaHome:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 186
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->listaHome:Ljava/util/List;

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->listaHome:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/home/HomeFragment;->atualizaAdapter(Ljava/util/List;)V

    .line 188
    return-void
.end method

.method private salvaPendenciasEmCache(Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;)V
    .registers 3
    .param p1, "pendencias"    # Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

    .line 191
    iput-object p1, p0, Lcom/itau/empresas/feature/home/HomeFragment;->pendenciasVO:Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

    .line 192
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/CustomApplication;->setPendenciasVO(Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;)V

    .line 193
    return-void
.end method

.method private verificaPermissoes()Ljava/util/List;
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;"
        }
    .end annotation

    .line 136
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/HomeFragment;->buscaHomeTabbar()Ljava/util/List;

    move-result-object v2

    .line 137
    .local v2, "cardsHome":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 139
    .local v3, "cardsRemover":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_d
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_61

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/itau/empresas/api/model/MenuVO;

    .line 140
    .local v5, "menu":Lcom/itau/empresas/api/model/MenuVO;
    invoke-virtual {v5}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v6

    .line 141
    .local v6, "chaveMobile":Ljava/lang/String;
    const-string v0, "card_de_saldo"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->application:Lcom/itau/empresas/CustomApplication;

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_WIDGET_SALDO:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 142
    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->verificarPermissao(Lcom/itau/empresas/CustomApplication$Exibicao;)Z

    move-result v0

    if-nez v0, :cond_34

    .line 143
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    goto :goto_d

    .line 146
    :cond_34
    const-string v0, "card_de_cobranca"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4a

    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->application:Lcom/itau/empresas/CustomApplication;

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_WIDGET_COBRANCAFRANCESINHA:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 147
    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->verificarPermissao(Lcom/itau/empresas/CustomApplication$Exibicao;)Z

    move-result v0

    if-nez v0, :cond_4a

    .line 148
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    goto :goto_d

    .line 151
    :cond_4a
    const-string v0, "card_de_pendencias"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5f

    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->application:Lcom/itau/empresas/CustomApplication;

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_SINO_PENDENCIAS:Lcom/itau/empresas/CustomApplication$Exibicao;

    .line 152
    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->verificarPermissao(Lcom/itau/empresas/CustomApplication$Exibicao;)Z

    move-result v0

    if-nez v0, :cond_5f

    .line 153
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    .end local v5    # "menu":Lcom/itau/empresas/api/model/MenuVO;
    .end local v6    # "chaveMobile":Ljava/lang/String;
    :cond_5f
    goto/16 :goto_d

    .line 157
    :cond_61
    invoke-interface {v2, v3}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 159
    return-object v2
.end method


# virtual methods
.method afterViews()V
    .registers 7

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0}, Lcom/itau/empresas/AlarmeUtils;->usuarioEfetuouLogout(Lcom/itau/empresas/CustomApplication;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 76
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x2

    const-wide/32 v2, 0x337f9800

    const-wide/32 v4, 0x337f9800

    invoke-static/range {v0 .. v5}, Lcom/itau/empresas/AlarmeUtils;->reagendarAlarme(Landroid/content/Context;IJJ)V

    .line 80
    const-string v0, "extrato_key"

    .line 81
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    iget-object v3, p0, Lcom/itau/empresas/feature/home/HomeFragment;->application:Lcom/itau/empresas/CustomApplication;

    .line 80
    invoke-static {v0, v1, v2, v3}, Lcom/itau/empresas/AlarmeUtils;->salvarDatetimeAlarme(Ljava/lang/String;JLcom/itau/empresas/CustomApplication;)V

    .line 84
    :cond_25
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/itau/empresas/AlarmeUtils;->cancelarAlarme(Landroid/content/Context;I)V

    .line 86
    const-string v0, "ano_novo_key"

    iget-object v1, p0, Lcom/itau/empresas/feature/home/HomeFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0, v1}, Lcom/itau/empresas/AlarmeUtils;->buscarDatetimeAlarme(Ljava/lang/String;Lcom/itau/empresas/CustomApplication;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_42

    .line 88
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/NotificacaoUtils;->configurarNotificacaoFimAno(Landroid/content/Context;)V

    .line 91
    :cond_42
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/HomeFragment;->disparaEventoAnalytics()V

    .line 92
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$HTTP;)V
    .registers 5
    .param p1, "httpError"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$HTTP;

    .line 225
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$HTTP;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    .line 226
    .local v2, "exception":Lbr/com/itau/sdk/android/core/exception/BackendException;
    const-string v0, "listaPendenciasUsuario"

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 227
    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getStatusCode()I

    move-result v0

    const/16 v1, 0x1a6

    if-eq v0, v1, :cond_20

    .line 228
    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getStatusCode()I

    move-result v0

    const/16 v1, 0x1f7

    if-ne v0, v1, :cond_23

    .line 229
    :cond_20
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/HomeFragment;->adicionaErroPendencias()V

    .line 231
    :cond_23
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/HomeFragment;->montaCardsHome()V

    .line 232
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 236
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    .line 237
    .local v2, "exception":Lbr/com/itau/sdk/android/core/exception/BackendException;
    const-string v0, "listaPendenciasUsuario"

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 238
    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getStatusCode()I

    move-result v0

    const/16 v1, 0x1a6

    if-eq v0, v1, :cond_20

    .line 239
    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getStatusCode()I

    move-result v0

    const/16 v1, 0x1f7

    if-ne v0, v1, :cond_23

    .line 240
    :cond_20
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/HomeFragment;->adicionaErroPendencias()V

    .line 242
    :cond_23
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/HomeFragment;->montaCardsHome()V

    .line 243
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;)V
    .registers 2
    .param p1, "pendencias"    # Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

    .line 219
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/HomeFragment;->salvaPendenciasEmCache(Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;)V

    .line 220
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/HomeFragment;->montaCardsHome()V

    .line 221
    return-void
.end method

.method public onResume()V
    .registers 1

    .line 65
    invoke-super {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->onResume()V

    .line 66
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/HomeFragment;->apresentaAlertasDaHome()V

    .line 67
    return-void
.end method

.method public onStart()V
    .registers 2

    .line 59
    invoke-super {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->onStart()V

    .line 60
    invoke-static {}, Lcom/itau/empresas/ui/util/PoyntUtils;->isPoynt()Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/home/HomeFragment;->isPoynt:Z

    .line 61
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 54
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.class Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$1;
.super Ljava/lang/Object;
.source "OperadoresAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->onBindViewHolder(Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ViewHolder;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;

.field final synthetic val$operador:Lcom/itau/empresas/api/model/OperadorVO;

.field final synthetic val$posicaoSelecionado:I


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;Lcom/itau/empresas/api/model/OperadorVO;I)V
    .registers 4
    .param p1, "this$0"    # Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;

    .line 50
    iput-object p1, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$1;->this$0:Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;

    iput-object p2, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$1;->val$operador:Lcom/itau/empresas/api/model/OperadorVO;

    iput p3, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$1;->val$posicaoSelecionado:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 4
    .param p1, "v"    # Landroid/view/View;

    .line 53
    iget-object v0, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$1;->this$0:Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;

    # getter for: Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->listenerItemSelecionado:Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ListenerItemSelecionado;
    invoke-static {v0}, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->access$000(Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;)Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ListenerItemSelecionado;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$1;->val$operador:Lcom/itau/empresas/api/model/OperadorVO;

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ListenerItemSelecionado;->itemSelcecionado(Lcom/itau/empresas/api/model/OperadorVO;)V

    .line 54
    iget v0, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$1;->val$posicaoSelecionado:I

    iget-object v1, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$1;->this$0:Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;

    # getter for: Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->indexSelecionado:I
    invoke-static {v1}, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->access$100(Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;)I

    move-result v1

    if-eq v0, v1, :cond_27

    iget-object v0, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$1;->this$0:Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;

    # getter for: Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->itemSselecionado:Landroid/widget/RadioButton;
    invoke-static {v0}, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->access$200(Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;)Landroid/widget/RadioButton;

    move-result-object v0

    if-eqz v0, :cond_27

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$1;->this$0:Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;

    # getter for: Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->itemSselecionado:Landroid/widget/RadioButton;
    invoke-static {v0}, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->access$200(Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;)Landroid/widget/RadioButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 57
    :cond_27
    iget-object v0, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$1;->this$0:Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;

    iget v1, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$1;->val$posicaoSelecionado:I

    # setter for: Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->indexSelecionado:I
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->access$102(Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;I)I

    .line 58
    iget-object v0, p0, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$1;->this$0:Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;

    move-object v1, p1

    check-cast v1, Landroid/widget/RadioButton;

    # setter for: Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->itemSselecionado:Landroid/widget/RadioButton;
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;->access$202(Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter;Landroid/widget/RadioButton;)Landroid/widget/RadioButton;

    .line 59
    return-void
.end method

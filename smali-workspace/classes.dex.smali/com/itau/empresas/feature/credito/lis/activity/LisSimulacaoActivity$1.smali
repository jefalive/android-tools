.class Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity$1;
.super Ljava/lang/Object;
.source "LisSimulacaoActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->contratar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;

    .line 212
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity$1;->this$0:Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 5
    .param p1, "v"    # Landroid/view/View;

    .line 215
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity$1;->this$0:Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->mostraDialogoDeProgresso()V

    .line 216
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity$1;->this$0:Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->controller:Lcom/itau/empresas/feature/credito/lis/LisController;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity$1;->this$0:Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;

    # invokes: Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->geraContratacaoEnvio()Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;
    invoke-static {v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->access$000(Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;)Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity$1;->this$0:Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;

    iget-boolean v2, v2, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->lisAdicional:Z

    .line 217
    invoke-static {v2}, Lcom/itau/empresas/feature/credito/lis/LisConstants;->getTipoProduto(Z)Ljava/lang/String;

    move-result-object v2

    .line 216
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/credito/lis/LisController;->efetuaContratacao(Lcom/itau/empresas/feature/credito/lis/model/LisContratacaoEnvioVO;Ljava/lang/String;)V

    .line 218
    return-void
.end method

.class public Lcom/itau/empresas/api/model/FavorecidoVO;
.super Ljava/lang/Object;
.source "FavorecidoVO.java"


# instance fields
.field private agencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia"
    .end annotation
.end field

.field private banco:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "banco"
    .end annotation
.end field

.field private conta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta"
    .end annotation
.end field

.field private digitoVerificadorConta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_verificador_conta"
    .end annotation
.end field

.field private documento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "documento"
    .end annotation
.end field

.field private favorito:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "favorito"
    .end annotation
.end field

.field private id:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id"
    .end annotation
.end field

.field private nome:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome"
    .end annotation
.end field

.field private tipoPessoa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_pessoa"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

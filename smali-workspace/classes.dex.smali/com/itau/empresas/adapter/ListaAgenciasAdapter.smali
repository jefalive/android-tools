.class public Lcom/itau/empresas/adapter/ListaAgenciasAdapter;
.super Landroid/widget/BaseAdapter;
.source "ListaAgenciasAdapter.java"

# interfaces
.implements Landroid/widget/Filterable;


# instance fields
.field private agencias:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;"
        }
    .end annotation
.end field

.field private listaAgencias:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;"
        }
    .end annotation
.end field

.field private mActivity:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .registers 2
    .param p1, "mActivity"    # Landroid/app/Activity;

    .line 25
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->mActivity:Landroid/app/Activity;

    .line 27
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/adapter/ListaAgenciasAdapter;)Ljava/util/List;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    .line 19
    iget-object v0, p0, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->listaAgencias:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$102(Lcom/itau/empresas/adapter/ListaAgenciasAdapter;Ljava/util/List;)Ljava/util/List;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/adapter/ListaAgenciasAdapter;
    .param p1, "x1"    # Ljava/util/List;

    .line 19
    iput-object p1, p0, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->agencias:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$200(Lcom/itau/empresas/adapter/ListaAgenciasAdapter;)Landroid/app/Activity;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    .line 19
    iget-object v0, p0, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->mActivity:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .registers 2

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->agencias:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .registers 2

    .line 63
    new-instance v0, Lcom/itau/empresas/adapter/ListaAgenciasAdapter$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/adapter/ListaAgenciasAdapter$1;-><init>(Lcom/itau/empresas/adapter/ListaAgenciasAdapter;)V

    .line 117
    .local v0, "filter":Landroid/widget/Filter;
    return-object v0
.end method

.method public getItem(I)Lcom/itau/empresas/api/model/AgenciaVO;
    .registers 3
    .param p1, "i"    # I

    .line 58
    iget-object v0, p0, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->agencias:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/AgenciaVO;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .registers 3

    .line 19
    invoke-virtual {p0, p1}, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->getItem(I)Lcom/itau/empresas/api/model/AgenciaVO;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .param p1, "i"    # I

    .line 36
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 6
    .param p1, "i"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "viewGroup"    # Landroid/view/ViewGroup;

    .line 47
    if-nez p2, :cond_8

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView_;->build(Landroid/content/Context;)Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;

    move-result-object p2

    .line 51
    :cond_8
    invoke-virtual {p0, p1}, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->getItem(I)Lcom/itau/empresas/api/model/AgenciaVO;

    move-result-object v1

    .line 53
    .local v1, "agencia":Lcom/itau/empresas/api/model/AgenciaVO;
    move-object v0, p2

    check-cast v0, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;->ajustaDados(Lcom/itau/empresas/api/model/AgenciaVO;)Lcom/itau/empresas/ui/view/ItemAgenciaMapaView;

    move-result-object v0

    return-object v0
.end method

.method public setItems(Ljava/util/List;)V
    .registers 3
    .param p1, "agencias"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;)V"
        }
    .end annotation

    .line 30
    iput-object p1, p0, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->agencias:Ljava/util/List;

    .line 31
    iget-object v0, p0, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->agencias:Ljava/util/List;

    iput-object v0, p0, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->listaAgencias:Ljava/util/List;

    .line 32
    return-void
.end method

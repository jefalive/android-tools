.class Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$2;
.super Ljava/lang/Object;
.source "MaterialAutoCompleteTextView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->initFloatingLabel()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;


# direct methods
.method constructor <init>(Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;)V
    .registers 2

    .line 814
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$2;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 4
    .param p1, "s"    # Landroid/text/Editable;

    .line 825
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$2;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    # getter for: Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->floatingLabelEnabled:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->access$200(Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 826
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_26

    .line 827
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$2;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    # getter for: Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->floatingLabelShown:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->access$300(Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;)Z

    move-result v0

    if-eqz v0, :cond_3d

    .line 828
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$2;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    const/4 v1, 0x0

    # setter for: Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->floatingLabelShown:Z
    invoke-static {v0, v1}, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->access$302(Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;Z)Z

    .line 829
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$2;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    # invokes: Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->getLabelAnimator()Lcom/nineoldandroids/animation/ObjectAnimator;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->access$400(Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->reverse()V

    goto :goto_3d

    .line 831
    :cond_26
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$2;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    # getter for: Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->floatingLabelShown:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->access$300(Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;)Z

    move-result v0

    if-nez v0, :cond_3d

    .line 832
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$2;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    const/4 v1, 0x1

    # setter for: Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->floatingLabelShown:Z
    invoke-static {v0, v1}, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->access$302(Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;Z)Z

    .line 833
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$2;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    # invokes: Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->getLabelAnimator()Lcom/nineoldandroids/animation/ObjectAnimator;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->access$400(Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->start()V

    .line 836
    :cond_3d
    :goto_3d
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .line 817
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .line 821
    return-void
.end method

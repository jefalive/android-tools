.class Lcom/google/android/gms/internal/zzeg$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/internal/zzeg;->zzep()Lcom/google/android/gms/internal/zzeg$zze;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic zzBd:Lcom/google/android/gms/internal/zzeg$zze;

.field final synthetic zzBe:Lcom/google/android/gms/internal/zzeg;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/zzeg;Lcom/google/android/gms/internal/zzeg$zze;)V
    .registers 3

    iput-object p1, p0, Lcom/google/android/gms/internal/zzeg$1;->zzBe:Lcom/google/android/gms/internal/zzeg;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzeg$1;->zzBd:Lcom/google/android/gms/internal/zzeg$zze;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 8

    iget-object v0, p0, Lcom/google/android/gms/internal/zzeg$1;->zzBe:Lcom/google/android/gms/internal/zzeg;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzeg$1;->zzBe:Lcom/google/android/gms/internal/zzeg;

    invoke-static {v1}, Lcom/google/android/gms/internal/zzeg;->zza(Lcom/google/android/gms/internal/zzeg;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/zzeg$1;->zzBe:Lcom/google/android/gms/internal/zzeg;

    invoke-static {v2}, Lcom/google/android/gms/internal/zzeg;->zzb(Lcom/google/android/gms/internal/zzeg;)Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/zzeg;->zza(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)Lcom/google/android/gms/internal/zzed;

    move-result-object v4

    new-instance v0, Lcom/google/android/gms/internal/zzeg$1$1;

    invoke-direct {v0, p0, v4}, Lcom/google/android/gms/internal/zzeg$1$1;-><init>(Lcom/google/android/gms/internal/zzeg$1;Lcom/google/android/gms/internal/zzed;)V

    invoke-interface {v4, v0}, Lcom/google/android/gms/internal/zzed;->zza(Lcom/google/android/gms/internal/zzed$zza;)V

    const-string v0, "/jsLoaded"

    new-instance v1, Lcom/google/android/gms/internal/zzeg$1$2;

    invoke-direct {v1, p0, v4}, Lcom/google/android/gms/internal/zzeg$1$2;-><init>(Lcom/google/android/gms/internal/zzeg$1;Lcom/google/android/gms/internal/zzed;)V

    invoke-interface {v4, v0, v1}, Lcom/google/android/gms/internal/zzed;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzdf;)V

    new-instance v5, Lcom/google/android/gms/internal/zzja;

    invoke-direct {v5}, Lcom/google/android/gms/internal/zzja;-><init>()V

    new-instance v6, Lcom/google/android/gms/internal/zzeg$1$3;

    invoke-direct {v6, p0, v4, v5}, Lcom/google/android/gms/internal/zzeg$1$3;-><init>(Lcom/google/android/gms/internal/zzeg$1;Lcom/google/android/gms/internal/zzed;Lcom/google/android/gms/internal/zzja;)V

    invoke-virtual {v5, v6}, Lcom/google/android/gms/internal/zzja;->set(Ljava/lang/Object;)V

    const-string v0, "/requestReload"

    invoke-interface {v4, v0, v6}, Lcom/google/android/gms/internal/zzed;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzdf;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzeg$1;->zzBe:Lcom/google/android/gms/internal/zzeg;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzeg;->zzf(Lcom/google/android/gms/internal/zzeg;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ".js"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4e

    iget-object v0, p0, Lcom/google/android/gms/internal/zzeg$1;->zzBe:Lcom/google/android/gms/internal/zzeg;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzeg;->zzf(Lcom/google/android/gms/internal/zzeg;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Lcom/google/android/gms/internal/zzed;->zzZ(Ljava/lang/String;)V

    goto :goto_6f

    :cond_4e
    iget-object v0, p0, Lcom/google/android/gms/internal/zzeg$1;->zzBe:Lcom/google/android/gms/internal/zzeg;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzeg;->zzf(Lcom/google/android/gms/internal/zzeg;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "<html>"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_66

    iget-object v0, p0, Lcom/google/android/gms/internal/zzeg$1;->zzBe:Lcom/google/android/gms/internal/zzeg;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzeg;->zzf(Lcom/google/android/gms/internal/zzeg;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Lcom/google/android/gms/internal/zzed;->zzab(Ljava/lang/String;)V

    goto :goto_6f

    :cond_66
    iget-object v0, p0, Lcom/google/android/gms/internal/zzeg$1;->zzBe:Lcom/google/android/gms/internal/zzeg;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzeg;->zzf(Lcom/google/android/gms/internal/zzeg;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Lcom/google/android/gms/internal/zzed;->zzaa(Ljava/lang/String;)V

    :goto_6f
    sget-object v0, Lcom/google/android/gms/internal/zzir;->zzMc:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/internal/zzeg$1$4;

    invoke-direct {v1, p0, v4}, Lcom/google/android/gms/internal/zzeg$1$4;-><init>(Lcom/google/android/gms/internal/zzeg$1;Lcom/google/android/gms/internal/zzed;)V

    sget v2, Lcom/google/android/gms/internal/zzeg$zza;->zzBm:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.class public Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck$Contract;
    }
.end annotation


# static fields
.field private static final $:[S

.field private static $$:I


# instance fields
.field private androidVersion:I

.field private checkVersion:Z

.field private message:Ljava/lang/String;

.field private timesToShow:I

.field private webviewVersion:I


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    add-int/lit8 p1, p1, 0x50

    const/4 v5, 0x0

    rsub-int p0, p0, 0xac

    new-instance v0, Ljava/lang/String;

    sget-object v4, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$:[S

    add-int/lit8 p2, p2, 0x3

    new-array v1, p2, [B

    if-nez v4, :cond_16

    move v2, p2

    move v3, p1

    :goto_11
    add-int/lit8 p0, p0, 0x1

    add-int/2addr v2, v3

    add-int/lit8 p1, v2, 0xe

    :cond_16
    int-to-byte v2, p1

    aput-byte v2, v1, v5

    add-int/lit8 v5, v5, 0x1

    if-ne v5, p2, :cond_22

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_22
    move v2, p1

    aget-short v3, v4, p0

    goto :goto_11
.end method

.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0xbd

    new-array v0, v0, [S

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$:[S

    const/16 v0, 0x3b

    sput v0, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$$:I

    return-void

    :array_e
    .array-data 2
        0x36s
        0x78s
        0xcs
        -0x3as
        0x3s
        0x3s
        -0x1fs
        0x31s
        -0x3as
        -0x1ds
        -0x1s
        0x20s
        -0x39s
        -0x16s
        -0x1as
        0x31s
        -0x41s
        -0x16s
        -0x7s
        -0x12s
        -0x7s
        -0xbs
        0x20s
        -0x49s
        0x5s
        -0x16s
        -0x19s
        -0x1s
        -0x17s
        0x73s
        -0x8as
        -0x19s
        -0x8s
        -0x16s
        0x31s
        -0x49s
        -0x6s
        0x25s
        -0x3bs
        -0x1cs
        0x2s
        0x1ds
        -0x4ds
        0x1s
        -0x12s
        -0x11s
        -0x14s
        -0x10s
        0x5s
        -0x19s
        -0x1s
        -0x15s
        0x23s
        -0x65s
        0x1ds
        -0x21s
        0x8bs
        -0x68s
        -0x40s
        -0xds
        0x23s
        -0x4bs
        -0xcs
        -0x7s
        -0x5s
        -0x17s
        -0x19s
        0x3s
        -0x54s
        0x66s
        -0x4ds
        0x5s
        -0xds
        -0x22s
        -0x3s
        -0x11s
        0x3s
        -0x23s
        0x2ds
        -0x39s
        -0x16s
        0x25s
        -0x4bs
        -0x2s
        -0x10s
        -0xbs
        -0xfs
        -0xfs
        -0x17s
        -0x5s
        -0x8s
        -0x1ds
        0x2ds
        -0x4as
        -0x3s
        0x23s
        -0x3bs
        -0x1cs
        0x2s
        0x1ds
        -0x3bs
        -0x18s
        -0x4s
        -0xds
        -0x1ds
        -0x6s
        -0x1as
        0x31s
        -0x4ds
        -0x1s
        -0x18s
        0x0s
        -0x11s
        -0x14s
        -0x13s
        0x2es
        -0x40s
        -0x1bs
        0x31s
        -0x67s
        0x1as
        -0xes
        -0x16s
        -0x9s
        -0x15s
        0x2ds
        -0x5es
        0xes
        -0x19s
        0xas
        -0x59s
        -0x2s
        -0x10s
        -0x4ds
        0x2bs
        -0x6s
        -0xes
        -0x16s
        -0x9s
        -0x15s
        -0x45s
        0x25s
        -0x1s
        -0x18s
        0x0s
        -0x11s
        -0x14s
        -0x13s
        -0x44s
        0x3bs
        -0x20s
        -0x11s
        0x6s
        -0x1bs
        -0x12s
        0x4s
        -0x20s
        -0x11s
        0x6s
        -0x1bs
        -0x12s
        0x4s
        -0x57s
        0x3as
        -0x1fs
        -0x1s
        -0xds
        -0x18s
        -0x8s
        -0xfs
        -0x23s
        0x6s
        -0x20s
        -0x11s
        0x6s
        -0x1bs
        -0x12s
        0x4s
        -0x35s
        0x14s
        -0x1bs
        -0xds
        -0xfs
        -0x1s
        -0x1bs
        -0x5s
        -0x19s
        -0xcs
        0x0s
    .end array-data
.end method

.method public constructor <init>(ZIIILjava/lang/String;)V
    .registers 6

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-boolean p1, p0, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->checkVersion:Z

    .line 20
    iput p2, p0, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->androidVersion:I

    .line 21
    iput p3, p0, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->webviewVersion:I

    .line 22
    iput p4, p0, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->timesToShow:I

    .line 23
    iput-object p5, p0, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->message:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public static performCheck(Landroid/content/Context;Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck$Contract;)V
    .registers 10

    .line 47
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$:[S

    const/16 v2, 0x2f

    aget-short v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$:[S

    const/4 v3, 0x2

    aget-short v2, v2, v3

    const/16 v3, 0x27

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$(III)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lbr/com/itau/sdk/android/core/RemoteBundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;

    .line 49
    if-nez v6, :cond_42

    .line 50
    new-instance v0, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$:[S

    const/16 v2, 0x6f

    aget-short v1, v1, v2

    or-int/lit8 v2, v1, 0x7d

    const/16 v3, 0xa8

    invoke-static {v3, v1, v2}, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$(III)Ljava/lang/String;

    move-result-object v5

    const/4 v1, 0x1

    const/16 v2, 0x18

    const/16 v3, 0x37

    const/4 v4, 0x3

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;-><init>(ZIIILjava/lang/String;)V

    move-object v6, v0

    .line 54
    :cond_42
    :try_start_42
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    sget v1, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$$:I

    and-int/lit16 v1, v1, 0xed

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$:[S

    const/16 v3, 0x72

    aget-short v2, v2, v3

    neg-int v2, v2

    sget-object v3, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$:[S

    const/16 v4, 0x1c

    aget-short v3, v3, v4

    neg-int v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v7

    .line 55
    invoke-interface {p1, v6, v7}, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck$Contract;->checkWebviewVersion(Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;Landroid/content/pm/PackageInfo;)V
    :try_end_64
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_42 .. :try_end_64} :catch_65

    .line 57
    goto :goto_66

    .line 56
    :catch_65
    move-exception v7

    .line 58
    :goto_66
    return-void
.end method


# virtual methods
.method public getAndroidVersion()I
    .registers 2

    .line 31
    iget v0, p0, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->androidVersion:I

    return v0
.end method

.method public getHowManyUpdateDialogShowed(Landroid/content/Context;)I
    .registers 7

    .line 69
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$:[S

    const/16 v1, 0x6f

    aget-short v0, v0, v1

    or-int/lit8 v1, v0, 0x27

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$:[S

    const/16 v3, 0x56

    aget-short v2, v2, v3

    neg-int v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$(III)Ljava/lang/String;

    move-result-object v0

    .line 70
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$:[S

    const/16 v2, 0x28

    aget-short v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$:[S

    const/16 v3, 0x12

    aget-short v2, v2, v3

    neg-int v2, v2

    sget-object v3, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$:[S

    const/16 v4, 0x6f

    aget-short v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$(III)Ljava/lang/String;

    move-result-object v1

    .line 71
    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 69
    return v0
.end method

.method public getMessage()Ljava/lang/String;
    .registers 2

    .line 43
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getTimesToShow()I
    .registers 2

    .line 39
    iget v0, p0, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->timesToShow:I

    return v0
.end method

.method public getWebviewVersion()I
    .registers 2

    .line 35
    iget v0, p0, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->webviewVersion:I

    return v0
.end method

.method public incrementHowManyUpdateDialogShowed(Landroid/content/Context;)V
    .registers 7

    .line 61
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$:[S

    const/16 v1, 0x6f

    aget-short v0, v0, v1

    or-int/lit8 v1, v0, 0x27

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$:[S

    const/16 v3, 0x56

    aget-short v2, v2, v3

    neg-int v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$(III)Ljava/lang/String;

    move-result-object v0

    .line 62
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 63
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$:[S

    const/16 v2, 0x28

    aget-short v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$:[S

    const/16 v3, 0x12

    aget-short v2, v2, v3

    neg-int v2, v2

    sget-object v3, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$:[S

    const/16 v4, 0x6f

    aget-short v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->$(III)Ljava/lang/String;

    move-result-object v1

    .line 64
    invoke-virtual {p0, p1}, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->getHowManyUpdateDialogShowed(Landroid/content/Context;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 65
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 66
    return-void
.end method

.method public isCheckVersion()Z
    .registers 2

    .line 27
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->checkVersion:Z

    return v0
.end method

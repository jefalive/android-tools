.class public final Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;
.super Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;
.source "AreaUsuarioMenuAbertoFragment_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_$FragmentBuilder_;
    }
.end annotation


# instance fields
.field private contentView_:Landroid/view/View;

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 26
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;-><init>()V

    .line 30
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method public static builder()Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_$FragmentBuilder_;
    .registers 1

    .line 82
    new-instance v0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_$FragmentBuilder_;

    invoke-direct {v0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_$FragmentBuilder_;-><init>()V

    return-object v0
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 68
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 69
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->application:Lcom/itau/empresas/CustomApplication;

    .line 70
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->mAdapter:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioMenuAbertoAdapter;

    .line 71
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/logout/LogoutHelper_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/logout/LogoutHelper_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    .line 72
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/SessaoController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/controller/SessaoController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->controller:Lcom/itau/empresas/controller/SessaoController;

    .line 73
    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .registers 3
    .param p1, "id"    # I

    .line 43
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_6

    .line 44
    const/4 v0, 0x0

    return-object v0

    .line 46
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->contentView_:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 35
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 36
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->init_(Landroid/os/Bundle;)V

    .line 37
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->onCreate(Landroid/os/Bundle;)V

    .line 38
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 39
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 51
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->contentView_:Landroid/view/View;

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_14

    .line 53
    const v0, 0x7f030095

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->contentView_:Landroid/view/View;

    .line 55
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->contentView_:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .registers 2

    .line 60
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->onDestroyView()V

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->contentView_:Landroid/view/View;

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->textoNomeOperador:Landroid/widget/TextView;

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->textoCodigoOperador:Landroid/widget/TextView;

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->rvConfiguracoes:Landroid/support/v7/widget/RecyclerView;

    .line 65
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 87
    const v0, 0x7f0e03f2

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->textoNomeOperador:Landroid/widget/TextView;

    .line 88
    const v0, 0x7f0e03f3

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->textoCodigoOperador:Landroid/widget/TextView;

    .line 89
    const v0, 0x7f0e00bd

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->rvConfiguracoes:Landroid/support/v7/widget/RecyclerView;

    .line 90
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->afterViews()V

    .line 91
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 77
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioMenuAbertoFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 79
    return-void
.end method

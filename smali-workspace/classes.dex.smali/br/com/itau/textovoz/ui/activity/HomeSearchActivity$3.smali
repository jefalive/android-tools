.class Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$3;
.super Landroid/support/design/widget/Snackbar$Callback;
.source "HomeSearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->showRationaleMessage(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

.field final synthetic val$text:Ljava/lang/String;


# direct methods
.method constructor <init>(Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;Ljava/lang/String;)V
    .registers 3
    .param p1, "this$0"    # Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    .line 385
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$3;->this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    iput-object p2, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$3;->val$text:Ljava/lang/String;

    invoke-direct {p0}, Landroid/support/design/widget/Snackbar$Callback;-><init>()V

    return-void
.end method


# virtual methods
.method public onShown(Landroid/support/design/widget/Snackbar;)V
    .registers 4
    .param p1, "snackbar"    # Landroid/support/design/widget/Snackbar;

    .line 388
    invoke-virtual {p1}, Landroid/support/design/widget/Snackbar;->getView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$3;->val$text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 389
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_18

    .line 390
    invoke-virtual {p1}, Landroid/support/design/widget/Snackbar;->getView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x4000

    invoke-virtual {v0, v1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 392
    :cond_18
    return-void
.end method

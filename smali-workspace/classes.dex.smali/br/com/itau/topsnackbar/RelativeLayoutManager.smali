.class final Lbr/com/itau/topsnackbar/RelativeLayoutManager;
.super Lbr/com/itau/topsnackbar/ViewManager;
.source "RelativeLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbr/com/itau/topsnackbar/ViewManager<Landroid/widget/RelativeLayout;>;"
    }
.end annotation


# instance fields
.field private final firstView:Landroid/view/View;

.field private final toolbar:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/widget/RelativeLayout;)V
    .registers 3
    .param p1, "parent"    # Landroid/widget/RelativeLayout;

    .line 16
    invoke-direct {p0, p1}, Lbr/com/itau/topsnackbar/ViewManager;-><init>(Landroid/view/ViewGroup;)V

    .line 17
    invoke-static {p1}, Lbr/com/itau/topsnackbar/RelativeLayoutManager;->findToolbar(Landroid/widget/RelativeLayout;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/topsnackbar/RelativeLayoutManager;->toolbar:Landroid/view/View;

    .line 18
    iget-object v0, p0, Lbr/com/itau/topsnackbar/RelativeLayoutManager;->toolbar:Landroid/view/View;

    invoke-direct {p0, v0}, Lbr/com/itau/topsnackbar/RelativeLayoutManager;->findViewBellow(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/topsnackbar/RelativeLayoutManager;->firstView:Landroid/view/View;

    .line 19
    return-void
.end method

.method private static findToolbar(Landroid/widget/RelativeLayout;)Landroid/view/View;
    .registers 6
    .param p0, "parent"    # Landroid/widget/RelativeLayout;

    .line 22
    invoke-virtual {p0}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v1

    .line 23
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_5
    if-ge v2, v1, :cond_1f

    .line 24
    invoke-virtual {p0, v2}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 25
    .local v3, "view":Landroid/view/View;
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    .line 26
    .local v4, "simpleName":Ljava/lang/String;
    const-string v0, "Toolbar"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 27
    return-object v3

    .line 23
    .end local v3    # "view":Landroid/view/View;
    .end local v4    # "simpleName":Ljava/lang/String;
    :cond_1c
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 29
    .end local v2    # "i":I
    :cond_1f
    const/4 v0, 0x0

    return-object v0
.end method

.method private findViewBellow(Landroid/view/View;)Landroid/view/View;
    .registers 10
    .param p1, "toolbar"    # Landroid/view/View;

    .line 33
    if-nez p1, :cond_4

    const/4 v0, 0x0

    return-object v0

    .line 34
    :cond_4
    iget-object v0, p0, Lbr/com/itau/topsnackbar/RelativeLayoutManager;->parent:Landroid/view/ViewGroup;

    check-cast v0, Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v1

    .line 35
    .local v1, "childCount":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 36
    .local v2, "toolbarId":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_11
    if-ge v3, v1, :cond_2f

    .line 37
    iget-object v0, p0, Lbr/com/itau/topsnackbar/RelativeLayoutManager;->parent:Landroid/view/ViewGroup;

    check-cast v0, Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 38
    .local v4, "view":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 39
    .local v5, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v5}, Landroid/widget/RelativeLayout$LayoutParams;->getRules()[I

    move-result-object v6

    .line 40
    .local v6, "rules":[I
    const/4 v0, 0x3

    aget v7, v6, v0

    .line 41
    .local v7, "bellowId":I
    if-ne v7, v2, :cond_2c

    return-object v4

    .line 36
    .end local v4    # "view":Landroid/view/View;
    .end local v5    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v6    # "rules":[I
    .end local v7    # "bellowId":I
    :cond_2c
    add-int/lit8 v3, v3, 0x1

    goto :goto_11

    .line 43
    .end local v3    # "i":I
    :cond_2f
    const/4 v0, 0x0

    return-object v0
.end method

.method private makeFirstViewBelow(Landroid/view/View;)V
    .registers 5
    .param p1, "content"    # Landroid/view/View;

    .line 56
    iget-object v0, p0, Lbr/com/itau/topsnackbar/RelativeLayoutManager;->firstView:Landroid/view/View;

    if-nez v0, :cond_5

    return-void

    .line 57
    :cond_5
    iget-object v0, p0, Lbr/com/itau/topsnackbar/RelativeLayoutManager;->firstView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 58
    .local v2, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const/4 v1, 0x3

    invoke-virtual {v2, v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 59
    iget-object v0, p0, Lbr/com/itau/topsnackbar/RelativeLayoutManager;->firstView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 60
    return-void
.end method


# virtual methods
.method public addContent(Landroid/view/View;)V
    .registers 5
    .param p1, "content"    # Landroid/view/View;

    .line 47
    iget-object v0, p0, Lbr/com/itau/topsnackbar/RelativeLayoutManager;->parent:Landroid/view/ViewGroup;

    check-cast v0, Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 48
    iget-object v0, p0, Lbr/com/itau/topsnackbar/RelativeLayoutManager;->toolbar:Landroid/view/View;

    if-nez v0, :cond_c

    return-void

    .line 49
    :cond_c
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 50
    .local v2, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lbr/com/itau/topsnackbar/RelativeLayoutManager;->toolbar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    const/4 v1, 0x3

    invoke-virtual {v2, v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 51
    invoke-virtual {p1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 52
    invoke-direct {p0, p1}, Lbr/com/itau/topsnackbar/RelativeLayoutManager;->makeFirstViewBelow(Landroid/view/View;)V

    .line 53
    return-void
.end method

.method public removeContent(Landroid/view/View;)V
    .registers 3
    .param p1, "content"    # Landroid/view/View;

    .line 63
    iget-object v0, p0, Lbr/com/itau/topsnackbar/RelativeLayoutManager;->parent:Landroid/view/ViewGroup;

    check-cast v0, Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 64
    iget-object v0, p0, Lbr/com/itau/topsnackbar/RelativeLayoutManager;->toolbar:Landroid/view/View;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lbr/com/itau/topsnackbar/RelativeLayoutManager;->firstView:Landroid/view/View;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lbr/com/itau/topsnackbar/RelativeLayoutManager;->toolbar:Landroid/view/View;

    invoke-direct {p0, v0}, Lbr/com/itau/topsnackbar/RelativeLayoutManager;->makeFirstViewBelow(Landroid/view/View;)V

    .line 65
    :cond_14
    return-void
.end method

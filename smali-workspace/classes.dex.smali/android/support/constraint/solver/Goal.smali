.class public Landroid/support/constraint/solver/Goal;
.super Ljava/lang/Object;
.source "Goal.java"


# instance fields
.field variables:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Landroid/support/constraint/solver/SolverVariable;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/constraint/solver/Goal;->variables:Ljava/util/ArrayList;

    return-void
.end method

.method private initFromSystemErrors(Landroid/support/constraint/solver/LinearSystem;)V
    .registers 8
    .param p1, "system"    # Landroid/support/constraint/solver/LinearSystem;

    .line 66
    iget-object v0, p0, Landroid/support/constraint/solver/Goal;->variables:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 67
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_6
    iget v0, p1, Landroid/support/constraint/solver/LinearSystem;->mNumColumns:I

    if-ge v3, v0, :cond_33

    .line 68
    iget-object v0, p1, Landroid/support/constraint/solver/LinearSystem;->mCache:Landroid/support/constraint/solver/Cache;

    iget-object v0, v0, Landroid/support/constraint/solver/Cache;->mIndexedVariables:[Landroid/support/constraint/solver/SolverVariable;

    aget-object v4, v0, v3

    .line 69
    .local v4, "variable":Landroid/support/constraint/solver/SolverVariable;
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_11
    const/4 v0, 0x6

    if-ge v5, v0, :cond_1c

    .line 70
    iget-object v0, v4, Landroid/support/constraint/solver/SolverVariable;->strengthVector:[F

    const/4 v1, 0x0

    aput v1, v0, v5

    .line 69
    add-int/lit8 v5, v5, 0x1

    goto :goto_11

    .line 72
    .end local v5    # "j":I
    :cond_1c
    iget-object v0, v4, Landroid/support/constraint/solver/SolverVariable;->strengthVector:[F

    iget v1, v4, Landroid/support/constraint/solver/SolverVariable;->strength:I

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    .line 73
    iget-object v0, v4, Landroid/support/constraint/solver/SolverVariable;->mType:Landroid/support/constraint/solver/SolverVariable$Type;

    sget-object v1, Landroid/support/constraint/solver/SolverVariable$Type;->ERROR:Landroid/support/constraint/solver/SolverVariable$Type;

    if-eq v0, v1, :cond_2b

    .line 74
    goto :goto_30

    .line 76
    :cond_2b
    iget-object v0, p0, Landroid/support/constraint/solver/Goal;->variables:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    .end local v4    # "variable":Landroid/support/constraint/solver/SolverVariable;
    :goto_30
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 78
    .end local v3    # "i":I
    :cond_33
    return-void
.end method


# virtual methods
.method getPivotCandidate()Landroid/support/constraint/solver/SolverVariable;
    .registers 9

    .line 37
    iget-object v0, p0, Landroid/support/constraint/solver/Goal;->variables:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 38
    .local v1, "count":I
    const/4 v2, 0x0

    .line 39
    .local v2, "candidate":Landroid/support/constraint/solver/SolverVariable;
    const/4 v3, 0x0

    .line 42
    .local v3, "strength":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_9
    if-ge v4, v1, :cond_35

    .line 43
    iget-object v0, p0, Landroid/support/constraint/solver/Goal;->variables:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/support/constraint/solver/SolverVariable;

    .line 44
    .local v5, "element":Landroid/support/constraint/solver/SolverVariable;
    const/4 v6, 0x5

    .local v6, "k":I
    :goto_15
    if-ltz v6, :cond_32

    .line 45
    iget-object v0, v5, Landroid/support/constraint/solver/SolverVariable;->strengthVector:[F

    aget v7, v0, v6

    .line 46
    .local v7, "value":F
    if-nez v2, :cond_26

    const/4 v0, 0x0

    cmpg-float v0, v7, v0

    if-gez v0, :cond_26

    if-lt v6, v3, :cond_26

    .line 47
    move v3, v6

    .line 48
    move-object v2, v5

    .line 50
    :cond_26
    const/4 v0, 0x0

    cmpl-float v0, v7, v0

    if-lez v0, :cond_2f

    if-le v6, v3, :cond_2f

    .line 51
    move v3, v6

    .line 52
    const/4 v2, 0x0

    .line 44
    .end local v7    # "value":F
    :cond_2f
    add-int/lit8 v6, v6, -0x1

    goto :goto_15

    .line 42
    .end local v5    # "element":Landroid/support/constraint/solver/SolverVariable;
    .end local v6    # "k":I
    :cond_32
    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    .line 56
    .end local v4    # "i":I
    :cond_35
    return-object v2
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    .line 119
    const-string v2, "Goal: "

    .line 120
    .local v2, "representation":Ljava/lang/String;
    iget-object v0, p0, Landroid/support/constraint/solver/Goal;->variables:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 122
    .local v3, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_9
    if-ge v4, v3, :cond_2c

    .line 123
    iget-object v0, p0, Landroid/support/constraint/solver/Goal;->variables:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/support/constraint/solver/SolverVariable;

    .line 124
    .local v5, "element":Landroid/support/constraint/solver/SolverVariable;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v5}, Landroid/support/constraint/solver/SolverVariable;->strengthsToString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 122
    .end local v5    # "element":Landroid/support/constraint/solver/SolverVariable;
    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    .line 126
    .end local v4    # "i":I
    :cond_2c
    return-object v2
.end method

.method updateFromSystem(Landroid/support/constraint/solver/LinearSystem;)V
    .registers 15
    .param p1, "system"    # Landroid/support/constraint/solver/LinearSystem;

    .line 86
    invoke-direct {p0, p1}, Landroid/support/constraint/solver/Goal;->initFromSystemErrors(Landroid/support/constraint/solver/LinearSystem;)V

    .line 87
    iget-object v0, p0, Landroid/support/constraint/solver/Goal;->variables:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 88
    .local v3, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_a
    if-ge v4, v3, :cond_5c

    .line 89
    iget-object v0, p0, Landroid/support/constraint/solver/Goal;->variables:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/support/constraint/solver/SolverVariable;

    .line 90
    .local v5, "element":Landroid/support/constraint/solver/SolverVariable;
    iget v0, v5, Landroid/support/constraint/solver/SolverVariable;->definitionId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_58

    .line 91
    iget v0, v5, Landroid/support/constraint/solver/SolverVariable;->definitionId:I

    invoke-virtual {p1, v0}, Landroid/support/constraint/solver/LinearSystem;->getRow(I)Landroid/support/constraint/solver/ArrayRow;

    move-result-object v6

    .line 92
    .local v6, "definition":Landroid/support/constraint/solver/ArrayRow;
    iget-object v7, v6, Landroid/support/constraint/solver/ArrayRow;->variables:Landroid/support/constraint/solver/ArrayLinkedVariables;

    .line 93
    .local v7, "variables":Landroid/support/constraint/solver/ArrayLinkedVariables;
    iget v8, v7, Landroid/support/constraint/solver/ArrayLinkedVariables;->currentSize:I

    .line 94
    .local v8, "size":I
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_25
    if-ge v9, v8, :cond_55

    .line 95
    invoke-virtual {v7, v9}, Landroid/support/constraint/solver/ArrayLinkedVariables;->getVariable(I)Landroid/support/constraint/solver/SolverVariable;

    move-result-object v10

    .line 96
    .local v10, "var":Landroid/support/constraint/solver/SolverVariable;
    if-nez v10, :cond_2e

    .line 97
    goto :goto_52

    .line 99
    :cond_2e
    invoke-virtual {v7, v9}, Landroid/support/constraint/solver/ArrayLinkedVariables;->getVariableValue(I)F

    move-result v11

    .line 100
    .local v11, "value":F
    const/4 v12, 0x0

    .local v12, "k":I
    :goto_33
    const/4 v0, 0x6

    if-ge v12, v0, :cond_45

    .line 101
    iget-object v0, v10, Landroid/support/constraint/solver/SolverVariable;->strengthVector:[F

    aget v1, v0, v12

    iget-object v2, v5, Landroid/support/constraint/solver/SolverVariable;->strengthVector:[F

    aget v2, v2, v12

    mul-float/2addr v2, v11

    add-float/2addr v1, v2

    aput v1, v0, v12

    .line 100
    add-int/lit8 v12, v12, 0x1

    goto :goto_33

    .line 103
    .end local v12    # "k":I
    :cond_45
    iget-object v0, p0, Landroid/support/constraint/solver/Goal;->variables:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_52

    .line 104
    iget-object v0, p0, Landroid/support/constraint/solver/Goal;->variables:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    .end local v10    # "var":Landroid/support/constraint/solver/SolverVariable;
    .end local v11    # "value":F
    :cond_52
    :goto_52
    add-int/lit8 v9, v9, 0x1

    goto :goto_25

    .line 107
    .end local v9    # "j":I
    :cond_55
    invoke-virtual {v5}, Landroid/support/constraint/solver/SolverVariable;->clearStrengths()V

    .line 88
    .end local v5    # "element":Landroid/support/constraint/solver/SolverVariable;
    .end local v6    # "definition":Landroid/support/constraint/solver/ArrayRow;
    .end local v7    # "variables":Landroid/support/constraint/solver/ArrayLinkedVariables;
    .end local v8    # "size":I
    :cond_58
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_a

    .line 110
    .end local v4    # "i":I
    :cond_5c
    return-void
.end method

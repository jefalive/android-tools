.class public Lcom/itau/empresas/feature/itoken/ITokenOrientacoesSegurancaActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "ITokenOrientacoesSegurancaActivity.java"


# instance fields
.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 23
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 61
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenOrientacoesSegurancaActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 62
    const v1, 0x7f07062a

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 65
    return-void
.end method

.method public static intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/feature/itoken/ITokenOrientacoesSegurancaActivity_$IntentBuilder_;
    .registers 2
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;

    .line 72
    new-instance v0, Lcom/itau/empresas/feature/itoken/ITokenOrientacoesSegurancaActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/itoken/ITokenOrientacoesSegurancaActivity_$IntentBuilder_;-><init>(Landroid/support/v4/app/Fragment;)V

    return-object v0
.end method


# virtual methods
.method aoCarregarTela()V
    .registers 1

    .line 28
    invoke-direct {p0}, Lcom/itau/empresas/feature/itoken/ITokenOrientacoesSegurancaActivity;->constroiToolbar()V

    .line 29
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 6
    .param p1, "menu"    # Landroid/view/Menu;

    .line 38
    invoke-static {}, Lcom/itau/empresas/ui/util/PoyntUtils;->isPoynt()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 39
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/ITokenOrientacoesSegurancaActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0005

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 41
    const v0, 0x7f0e06c6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 42
    .local v2, "itemMenuPesquisa":Landroid/view/MenuItem;
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 44
    const v0, 0x7f0e06c5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 45
    .local v3, "itemMenuFiltro":Landroid/view/MenuItem;
    const/4 v0, 0x0

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 48
    .end local v2    # "itemMenuPesquisa":Landroid/view/MenuItem;
    .end local v3    # "itemMenuFiltro":Landroid/view/MenuItem;
    :cond_26
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 53
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0e06cf

    if-ne v0, v1, :cond_10

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/ITokenOrientacoesSegurancaActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/ViewUtils;->aoClicarBotaoSairMenu(Lcom/itau/empresas/CustomApplication;Lcom/itau/empresas/ui/activity/BaseActivity;)V

    .line 55
    const/4 v0, 0x1

    return v0

    .line 57
    :cond_10
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 33
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

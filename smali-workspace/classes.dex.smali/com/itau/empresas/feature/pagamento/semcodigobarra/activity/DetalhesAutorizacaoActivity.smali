.class public Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "DetalhesAutorizacaoActivity.java"


# instance fields
.field app:Lcom/itau/empresas/CustomApplication;

.field campoAgenciaConta:Landroid/widget/TextView;

.field campoCnpjPagador:Landroid/widget/TextView;

.field campoCodigoOperadorAutorizador:Landroid/widget/TextView;

.field campoCpfOperadorAutorizador:Landroid/widget/TextView;

.field campoCpfOperadorInclusao:Landroid/widget/TextView;

.field campoDataAutorizacao:Landroid/widget/TextView;

.field campoDataInclusao:Landroid/widget/TextView;

.field campoHoraAutorizacao:Landroid/widget/TextView;

.field campoHoraInclusao:Landroid/widget/TextView;

.field campoNomeEmpresa:Landroid/widget/TextView;

.field campoNomeOperadorAutorizador:Landroid/widget/TextView;

.field campoNomeOperadorInclusao:Landroid/widget/TextView;

.field detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

.field imagemAlerta:Landroid/widget/TextView;

.field labelAutorizacao:Landroid/widget/TextView;

.field labelInclusao:Landroid/widget/TextView;

.field llDetalhesPagamento:Landroid/widget/LinearLayout;

.field mensagemAlerta:Landroid/widget/TextView;

.field motivo:Landroid/widget/TextView;

.field pagamentoDarfController:Lcom/itau/empresas/controller/PagamentoDarfController;

.field rlAlerta:Landroid/widget/RelativeLayout;

.field rlAutorizacaoDetalhe:Landroid/widget/RelativeLayout;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 40
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private constroiToolbar()V
    .registers 4

    .line 113
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f020136

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(I)V

    .line 114
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 115
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 116
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 119
    :cond_1b
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0e05c2

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/TextView;

    .line 120
    .local v2, "tituloToolbar":Landroid/widget/TextView;
    const v0, 0x7f070490

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity$1;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    return-void
.end method

.method private consultaDadosCliente()V
    .registers 2

    .line 195
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->pagamentoDarfController:Lcom/itau/empresas/controller/PagamentoDarfController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/PagamentoDarfController;->consultaDadosPerfilLogado()Lcom/itau/empresas/api/model/PerfilOperadorVO;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->atualizaDadosPerfilLogado(Lcom/itau/empresas/api/model/PerfilOperadorVO;)V

    .line 196
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 199
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private mostraDadosEtapaAutorizacaoPagamento()V
    .registers 6

    .line 166
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getListaEtapas()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_66

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;

    .line 167
    .local v4, "etapasAutorizacaoVO":Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;
    invoke-virtual {v4}, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;->getEtapa()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Autoriza\u00e7\u00e3o"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_65

    .line 168
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->rlAutorizacaoDetalhe:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 169
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->labelAutorizacao:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;->getEtapa()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->campoNomeOperadorAutorizador:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;->getNomeOperador()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->campoCpfOperadorAutorizador:Landroid/widget/TextView;

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CPF_CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    .line 172
    invoke-virtual {v4}, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;->getCpfOperador()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 171
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->campoCodigoOperadorAutorizador:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->campoHoraAutorizacao:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;->getHoraEtapa()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->campoDataAutorizacao:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;->getDataEtapa()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    .end local v4    # "etapasAutorizacaoVO":Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;
    :cond_65
    goto :goto_a

    .line 178
    :cond_66
    return-void
.end method

.method private mostraDadosEtapasInclusaoDoPagamento()V
    .registers 5

    .line 154
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getListaEtapas()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_51

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;

    .line 155
    .local v3, "etapasAutorizacaoVO":Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;->getEtapa()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Inclus\u00e3o Online"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 156
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->labelInclusao:Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;->getEtapa()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    :cond_2c
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->campoNomeOperadorInclusao:Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;->getNomeOperador()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->campoHoraInclusao:Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;->getHoraEtapa()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->campoDataInclusao:Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;->getDataEtapa()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->campoCpfOperadorInclusao:Landroid/widget/TextView;

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;->getCpfOperador()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    .end local v3    # "etapasAutorizacaoVO":Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;
    goto :goto_a

    .line 163
    :cond_51
    return-void
.end method

.method private mostraDadosPagamento()V
    .registers 5

    .line 147
    new-instance v2, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/DetalhesAutorizacaoTributosAdapter;

    invoke-direct {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/DetalhesAutorizacaoTributosAdapter;-><init>()V

    .line 148
    .local v2, "adapter":Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/DetalhesAutorizacaoTributosAdapter;
    new-instance v0, Lcom/itau/empresas/ui/util/BuildDadosComprovante;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-direct {v0, v1, p0}, Lcom/itau/empresas/ui/util/BuildDadosComprovante;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/BuildDadosComprovante;->getDadosComprovante()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/DetalhesAutorizacaoTributosAdapter;->setList(Ljava/util/List;)V

    .line 149
    new-instance v3, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;

    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->llDetalhesPagamento:Landroid/widget/LinearLayout;

    invoke-direct {v3, v0, v2}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;-><init>(Landroid/widget/LinearLayout;Landroid/widget/Adapter;)V

    .line 150
    .local v3, "layoutWrapper":Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;
    invoke-virtual {v3}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->init()V

    .line 151
    return-void
.end method

.method private mostraStatusPagamento()V
    .registers 6

    .line 132
    new-instance v3, Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;

    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    .line 133
    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getSituacao()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    .line 134
    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getCodigoMotivo()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v0, v1}, Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    .line 137
    .local v3, "dadosAlertaAutorizacaoTributos":Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/DadosAlertaAutorizacaoTributos;->getDadodStatusAutorizacaoTributos()Lcom/itau/empresas/api/model/StatusPagamentoTributos;

    move-result-object v4

    .line 138
    .local v4, "statusPagamentoTributos":Lcom/itau/empresas/api/model/StatusPagamentoTributos;
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->mensagemAlerta:Landroid/widget/TextView;

    .line 139
    invoke-virtual {v4}, Lcom/itau/empresas/api/model/StatusPagamentoTributos;->getTituloPagamento()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0704d8

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/itau/empresas/api/model/StatusPagamentoTributos;->getMensagemStatus()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 138
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->imagemAlerta:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/itau/empresas/api/model/StatusPagamentoTributos;->getIcone()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 141
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->motivo:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 142
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->motivo:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getDescricaoMotivo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->rlAlerta:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Lcom/itau/empresas/api/model/StatusPagamentoTributos;->getColorBackGround()I

    move-result v1

    invoke-static {p0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 144
    return-void
.end method


# virtual methods
.method public atualizaDadosPerfilLogado(Lcom/itau/empresas/api/model/PerfilOperadorVO;)V
    .registers 6
    .param p1, "perfilOperadorVO"    # Lcom/itau/empresas/api/model/PerfilOperadorVO;

    .line 181
    if-eqz p1, :cond_63

    .line 182
    .line 183
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->getContasOperador()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_19

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->getContasOperador()Ljava/util/List;

    move-result-object v0

    .line 184
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/itau/empresas/api/model/ContaOperadorVO;

    goto :goto_1e

    :cond_19
    new-instance v3, Lcom/itau/empresas/api/model/ContaOperadorVO;

    invoke-direct {v3}, Lcom/itau/empresas/api/model/ContaOperadorVO;-><init>()V

    .line 185
    .local v3, "contaOperadorVO":Lcom/itau/empresas/api/model/ContaOperadorVO;
    :goto_1e
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->campoAgenciaConta:Landroid/widget/TextView;

    .line 186
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_42

    new-instance v1, Ljava/lang/StringBuilder;

    .line 187
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto :goto_44

    :cond_42
    const-string v1, ""

    :goto_44
    check-cast v1, Ljava/lang/CharSequence;

    .line 185
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->campoNomeEmpresa:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->getNomeEmpresa()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 190
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->campoCnpjPagador:Landroid/widget/TextView;

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;->CPF_CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getCnpj()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    .end local v3    # "contaOperadorVO":Lcom/itau/empresas/api/model/ContaOperadorVO;
    :cond_63
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 97
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public updateView()V
    .registers 2

    .line 102
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->constroiToolbar()V

    .line 103
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->mostraStatusPagamento()V

    .line 104
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->mostraDadosPagamento()V

    .line 105
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->consultaDadosCliente()V

    .line 106
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getListaEtapas()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1e

    .line 107
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->mostraDadosEtapasInclusaoDoPagamento()V

    .line 108
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/DetalhesAutorizacaoActivity;->mostraDadosEtapaAutorizacaoPagamento()V

    .line 110
    :cond_1e
    return-void
.end method

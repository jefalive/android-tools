.class Lcom/google/android/gms/tagmanager/zzcu;
.super Lcom/google/android/gms/tagmanager/zzct;


# static fields
.field private static final zzbkP:Ljava/lang/Object;

.field private static zzbla:Lcom/google/android/gms/tagmanager/zzcu;


# instance fields
.field private connected:Z

.field private zzbkR:Lcom/google/android/gms/tagmanager/zzau;

.field private volatile zzbkS:Lcom/google/android/gms/tagmanager/zzas;

.field private zzbkT:I

.field private zzbkU:Z

.field private zzbkV:Z

.field private zzbkW:Z

.field private zzbkX:Lcom/google/android/gms/tagmanager/zzav;

.field private zzbkZ:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/tagmanager/zzcu;->zzbkP:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .registers 2

    invoke-direct {p0}, Lcom/google/android/gms/tagmanager/zzct;-><init>()V

    const v0, 0x1b7740

    iput v0, p0, Lcom/google/android/gms/tagmanager/zzcu;->zzbkT:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/tagmanager/zzcu;->zzbkU:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/tagmanager/zzcu;->zzbkV:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/tagmanager/zzcu;->connected:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/tagmanager/zzcu;->zzbkW:Z

    new-instance v0, Lcom/google/android/gms/tagmanager/zzcu$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/tagmanager/zzcu$1;-><init>(Lcom/google/android/gms/tagmanager/zzcu;)V

    iput-object v0, p0, Lcom/google/android/gms/tagmanager/zzcu;->zzbkX:Lcom/google/android/gms/tagmanager/zzav;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/tagmanager/zzcu;->zzbkZ:Z

    return-void
.end method

.method public static zzHo()Lcom/google/android/gms/tagmanager/zzcu;
    .registers 1

    sget-object v0, Lcom/google/android/gms/tagmanager/zzcu;->zzbla:Lcom/google/android/gms/tagmanager/zzcu;

    if-nez v0, :cond_b

    new-instance v0, Lcom/google/android/gms/tagmanager/zzcu;

    invoke-direct {v0}, Lcom/google/android/gms/tagmanager/zzcu;-><init>()V

    sput-object v0, Lcom/google/android/gms/tagmanager/zzcu;->zzbla:Lcom/google/android/gms/tagmanager/zzcu;

    :cond_b
    sget-object v0, Lcom/google/android/gms/tagmanager/zzcu;->zzbla:Lcom/google/android/gms/tagmanager/zzcu;

    return-object v0
.end method

.method static synthetic zze(Lcom/google/android/gms/tagmanager/zzcu;)Lcom/google/android/gms/tagmanager/zzau;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/tagmanager/zzcu;->zzbkR:Lcom/google/android/gms/tagmanager/zzau;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized dispatch()V
    .registers 4

    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/gms/tagmanager/zzcu;->zzbkV:Z

    if-nez v0, :cond_f

    const-string v0, "Dispatch call queued. Dispatch will run once initialization is complete."

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzbg;->v(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/tagmanager/zzcu;->zzbkU:Z

    monitor-exit p0

    return-void

    :cond_f
    iget-object v0, p0, Lcom/google/android/gms/tagmanager/zzcu;->zzbkS:Lcom/google/android/gms/tagmanager/zzas;

    new-instance v1, Lcom/google/android/gms/tagmanager/zzcu$3;

    invoke-direct {v1, p0}, Lcom/google/android/gms/tagmanager/zzcu$3;-><init>(Lcom/google/android/gms/tagmanager/zzcu;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/tagmanager/zzas;->zzj(Ljava/lang/Runnable;)V
    :try_end_19
    .catchall {:try_start_1 .. :try_end_19} :catchall_1b

    monitor-exit p0

    return-void

    :catchall_1b
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.class public final Lbr/com/itau/security/securestorage/SecureStorage;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/SharedPreferences;


# static fields
.field private static a:Ljava/lang/String;

.field private static b:Ljava/lang/String;

.field private static c:Ljava/lang/String;

.field private static d:Ljava/lang/String;

.field private static final f:[B

.field private static g:I


# instance fields
.field private final e:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 23
    const/16 v0, 0x4e

    new-array v0, v0, [B

    fill-array-data v0, :array_5a

    sput-object v0, Lbr/com/itau/security/securestorage/SecureStorage;->f:[B

    const/16 v0, 0xe1

    sput v0, Lbr/com/itau/security/securestorage/SecureStorage;->g:I

    sget-object v0, Lbr/com/itau/security/securestorage/SecureStorage;->f:[B

    const/4 v1, 0x3

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    or-int/lit8 v1, v0, 0x18

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/securestorage/SecureStorage;->f:[B

    const/16 v3, 0x2e

    aget-byte v2, v2, v3

    add-int/lit8 v2, v2, 0x1

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/securestorage/SecureStorage;->a(BBS)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbr/com/itau/security/securestorage/SecureStorage;->a:Ljava/lang/String;

    .line 24
    sget-object v0, Lbr/com/itau/security/securestorage/SecureStorage;->f:[B

    const/4 v1, 0x3

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    or-int/lit8 v1, v0, 0x1c

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/securestorage/SecureStorage;->f:[B

    const/16 v3, 0x4b

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/securestorage/SecureStorage;->a(BBS)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbr/com/itau/security/securestorage/SecureStorage;->b:Ljava/lang/String;

    .line 26
    sget-object v0, Lbr/com/itau/security/securestorage/SecureStorage;->f:[B

    const/4 v1, 0x3

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lbr/com/itau/security/securestorage/SecureStorage;->f:[B

    const/16 v2, 0x1a

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/securestorage/SecureStorage;->f:[B

    const/16 v3, 0x30

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/securestorage/SecureStorage;->a(BBS)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbr/com/itau/security/securestorage/SecureStorage;->c:Ljava/lang/String;

    .line 27
    const/4 v0, 0x0

    sput-object v0, Lbr/com/itau/security/securestorage/SecureStorage;->d:Ljava/lang/String;

    return-void

    :array_5a
    .array-data 1
        0x40t
        0x52t
        -0x54t
        0x0t
        -0x31t
        -0xdt
        0xft
        0x34t
        -0x2t
        -0x30t
        -0xdt
        0x3ft
        -0x3ct
        0xbt
        -0x5t
        0x3dt
        -0x5t
        -0x36t
        0x8t
        -0xet
        0x1t
        0x28t
        0x7t
        -0x2dt
        0x40t
        -0x2t
        0x2t
        -0x4t
        0x2t
        -0x40t
        0x10t
        0x3t
        -0xdt
        0x8t
        -0xct
        0xbt
        -0xdt
        0x4t
        0x7t
        0x6t
        0x6t
        -0x9t
        0x6t
        0x14t
        0xet
        0x7t
        0x11t
        -0x4t
        0x5t
        -0x2t
        -0x38t
        0x5t
        0x4t
        -0x4t
        0x8t
        0x35t
        -0x34t
        -0x9t
        -0x3ct
        0x3at
        0x9t
        -0x6t
        -0x2ft
        -0x1t
        0x0t
        0x9t
        0x2t
        -0x3t
        0x35t
        -0x2et
        -0x8t
        0x32t
        0x4t
        0x10t
        -0x4t
        -0xat
        0x14t
        -0x13t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 6

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-static {p1}, Lbr/com/itau/security/securestorage/SecureStorage;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbr/com/itau/security/securestorage/SecureStorage;->d:Ljava/lang/String;

    .line 33
    sget-object v0, Lbr/com/itau/security/securestorage/SecureStorage;->f:[B

    const/16 v1, 0x30

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget v1, Lbr/com/itau/security/securestorage/SecureStorage;->g:I

    and-int/lit8 v1, v1, 0x3f

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/securestorage/SecureStorage;->f:[B

    const/4 v3, 0x3

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/securestorage/SecureStorage;->a(BBS)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/security/securestorage/SecureStorage;->e:Landroid/content/SharedPreferences;

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 4

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1}, Lbr/com/itau/security/securestorage/SecureStorage;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbr/com/itau/security/securestorage/SecureStorage;->d:Ljava/lang/String;

    .line 38
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/security/securestorage/SecureStorage;->e:Landroid/content/SharedPreferences;

    .line 39
    return-void
.end method

.method public static synthetic a(Lbr/com/itau/security/securestorage/SecureStorage;)Landroid/content/SharedPreferences;
    .registers 2

    .line 21
    iget-object v0, p0, Lbr/com/itau/security/securestorage/SecureStorage;->e:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private static a(BBS)Ljava/lang/String;
    .registers 9

    sget-object v5, Lbr/com/itau/security/securestorage/SecureStorage;->f:[B

    mul-int/lit8 p2, p2, 0x3

    rsub-int/lit8 p2, p2, 0x49

    new-instance v0, Ljava/lang/String;

    mul-int/lit8 p0, p0, 0x2

    rsub-int/lit8 p0, p0, 0x10

    const/4 v4, 0x0

    mul-int/lit8 p1, p1, 0x2

    add-int/lit8 p1, p1, 0x31

    new-array v1, p0, [B

    if-nez v5, :cond_1d

    move v2, p0

    move v3, p1

    :goto_17
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p1, v2, 0x1

    add-int/lit8 p2, p2, 0x1

    :cond_1d
    move v2, v4

    int-to-byte v3, p1

    add-int/lit8 v4, v4, 0x1

    aput-byte v3, v1, v2

    if-ne v4, p0, :cond_2e

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2e
    move v2, p1

    aget-byte v3, v5, p2

    goto :goto_17
.end method

.method private static a(Landroid/content/Context;)Ljava/lang/String;
    .registers 7

    .line 43
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lbr/com/itau/security/securestorage/SecureStorage;->f:[B

    const/16 v2, 0x1f

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/securestorage/SecureStorage;->f:[B

    const/4 v3, 0x5

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    const/16 v3, 0x18

    invoke-static {v1, v3, v2}, Lbr/com/itau/security/securestorage/SecureStorage;->a(BBS)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 44
    sget-object v0, Lbr/com/itau/security/securestorage/SecureStorage;->f:[B

    const/4 v1, 0x3

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    int-to-byte v1, v0

    or-int/lit8 v2, v1, 0x17

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/securestorage/SecureStorage;->a(BBS)Ljava/lang/String;

    .line 46
    invoke-static {p0}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object p0

    .line 47
    sget-object v0, Lbr/com/itau/security/securestorage/SecureStorage;->f:[B

    const/4 v1, 0x3

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    int-to-byte v1, v0

    or-int/lit8 v2, v1, 0x17

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/securestorage/SecureStorage;->a(BBS)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v4

    .line 49
    const/4 v5, 0x0

    :goto_40
    array-length v0, p0

    if-ge v5, v0, :cond_4e

    .line 50
    aget-byte v0, p0, v5

    aget-byte v1, v4, v5

    xor-int/2addr v0, v1

    int-to-byte v0, v0

    aput-byte v0, v4, v5

    .line 49
    add-int/lit8 v5, v5, 0x1

    goto :goto_40

    .line 52
    :cond_4e
    invoke-static {v4}, Lbr/com/itau/security/commons/binary/ByteUtils;->to([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static decrypt(Ljava/lang/String;)Ljava/lang/String;
    .registers 4

    .line 173
    sget-object v1, Lbr/com/itau/security/securestorage/SecureStorage;->c:Ljava/lang/String;

    sget-object v2, Lbr/com/itau/security/securestorage/SecureStorage;->d:Ljava/lang/String;

    .line 6184
    invoke-static {p0}, Lbr/com/itau/security/commons/binary/ByteUtils;->b64Decode(Ljava/lang/String;)[B

    move-result-object p0

    .line 6185
    invoke-static {v1}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v1

    .line 6186
    invoke-static {v2}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v2

    .line 6187
    invoke-static {p0, v1, v2}, Lbr/com/itau/security/commons/Crypto;->pkcs7PaddingDecrypt([B[B[B)[B

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->to([B)Ljava/lang/String;

    move-result-object v0

    .line 173
    return-object v0
.end method

.method public static encrypt(Ljava/lang/String;)Ljava/lang/String;
    .registers 4

    .line 169
    sget-object v1, Lbr/com/itau/security/securestorage/SecureStorage;->c:Ljava/lang/String;

    sget-object v2, Lbr/com/itau/security/securestorage/SecureStorage;->d:Ljava/lang/String;

    .line 6177
    invoke-static {p0}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object p0

    .line 6178
    invoke-static {v1}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v1

    .line 6179
    invoke-static {v2}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v2

    .line 6180
    invoke-static {p0, v1, v2}, Lbr/com/itau/security/commons/Crypto;->pkcs7PaddingEncrypt([B[B[B)[B

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->b64Encode([B)Ljava/lang/String;

    move-result-object v0

    .line 169
    return-object v0
.end method


# virtual methods
.method public final contains(Ljava/lang/String;)Z
    .registers 4

    .line 138
    iget-object v0, p0, Lbr/com/itau/security/securestorage/SecureStorage;->e:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lbr/com/itau/security/securestorage/SecureStorage;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final edit()Landroid/content/SharedPreferences$Editor;
    .registers 3

    .line 147
    new-instance v0, Lss/c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lss/c;-><init>(Lbr/com/itau/security/securestorage/SecureStorage;B)V

    return-object v0
.end method

.method public final getAll()Ljava/util/Map;
    .registers 7

    .line 62
    iget-object v0, p0, Lbr/com/itau/security/securestorage/SecureStorage;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v2

    .line 64
    if-nez v2, :cond_d

    .line 65
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    return-object v0

    .line 67
    :cond_d
    new-instance v3, Ljava/util/HashMap;

    .line 68
    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 70
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_57

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/util/Map$Entry;

    .line 72
    :try_start_2b
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    .line 74
    if-eqz v5, :cond_46

    .line 76
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lbr/com/itau/security/securestorage/SecureStorage;->decrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 77
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbr/com/itau/security/securestorage/SecureStorage;->decrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 76
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_46
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_46} :catch_47

    .line 84
    :cond_46
    goto :goto_1e

    .line 80
    .line 82
    :catch_47
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    .line 83
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 82
    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    goto :goto_1e

    .line 86
    :cond_57
    return-object v3
.end method

.method public final getBoolean(Ljava/lang/String;Z)Z
    .registers 4

    .line 133
    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lbr/com/itau/security/securestorage/SecureStorage;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final getFloat(Ljava/lang/String;F)F
    .registers 4

    .line 128
    invoke-static {p2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lbr/com/itau/security/securestorage/SecureStorage;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public final getInt(Ljava/lang/String;I)I
    .registers 4

    .line 118
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lbr/com/itau/security/securestorage/SecureStorage;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final getLong(Ljava/lang/String;J)J
    .registers 6

    .line 123
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lbr/com/itau/security/securestorage/SecureStorage;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 10

    .line 91
    invoke-virtual {p0, p1}, Lbr/com/itau/security/securestorage/SecureStorage;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_28

    move-object v3, p1

    .line 1142
    iget-object v0, p0, Lbr/com/itau/security/securestorage/SecureStorage;->e:Landroid/content/SharedPreferences;

    .line 1161
    sget-object v6, Lbr/com/itau/security/securestorage/SecureStorage;->a:Ljava/lang/String;

    sget-object v4, Lbr/com/itau/security/securestorage/SecureStorage;->b:Ljava/lang/String;

    .line 1177
    invoke-static {v3}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v5

    .line 1178
    invoke-static {v6}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v6

    .line 1179
    invoke-static {v4}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v4

    .line 1180
    invoke-static {v5, v6, v4}, Lbr/com/itau/security/commons/Crypto;->pkcs7PaddingEncrypt([B[B[B)[B

    move-result-object v1

    invoke-static {v1}, Lbr/com/itau/security/commons/binary/ByteUtils;->b64Encode([B)Ljava/lang/String;

    move-result-object v1

    .line 1142
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    .line 91
    if-nez v0, :cond_28

    .line 92
    return-object p2

    .line 94
    :cond_28
    iget-object v0, p0, Lbr/com/itau/security/securestorage/SecureStorage;->e:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lbr/com/itau/security/securestorage/SecureStorage;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 96
    if-eqz p2, :cond_8f

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8f

    .line 97
    move-object v3, p1

    move-object p1, p2

    .line 1192
    move-object v2, p0

    iget-object v0, p0, Lbr/com/itau/security/securestorage/SecureStorage;->e:Landroid/content/SharedPreferences;

    .line 2161
    sget-object v6, Lbr/com/itau/security/securestorage/SecureStorage;->a:Ljava/lang/String;

    sget-object v4, Lbr/com/itau/security/securestorage/SecureStorage;->b:Ljava/lang/String;

    .line 2177
    invoke-static {v3}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v5

    .line 2178
    invoke-static {v6}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v6

    .line 2179
    invoke-static {v4}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v4

    .line 2180
    invoke-static {v5, v6, v4}, Lbr/com/itau/security/commons/Crypto;->pkcs7PaddingEncrypt([B[B[B)[B

    move-result-object v1

    invoke-static {v1}, Lbr/com/itau/security/commons/binary/ByteUtils;->b64Encode([B)Ljava/lang/String;

    move-result-object v1

    .line 1192
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1194
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8e

    .line 1196
    .line 3165
    sget-object v5, Lbr/com/itau/security/securestorage/SecureStorage;->a:Ljava/lang/String;

    sget-object v6, Lbr/com/itau/security/securestorage/SecureStorage;->b:Ljava/lang/String;

    .line 3184
    invoke-static {v4}, Lbr/com/itau/security/commons/binary/ByteUtils;->b64Decode(Ljava/lang/String;)[B

    move-result-object v4

    .line 3185
    invoke-static {v5}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v5

    .line 3186
    invoke-static {v6}, Lbr/com/itau/security/commons/binary/ByteUtils;->from(Ljava/lang/String;)[B

    move-result-object v6

    .line 3187
    invoke-static {v4, v5, v6}, Lbr/com/itau/security/commons/Crypto;->pkcs7PaddingDecrypt([B[B[B)[B

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/security/commons/binary/ByteUtils;->to([B)Ljava/lang/String;

    move-result-object v0

    .line 1198
    invoke-static {v0}, Lbr/com/itau/security/securestorage/SecureStorage;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1199
    iget-object v0, v2, Lbr/com/itau/security/securestorage/SecureStorage;->e:Landroid/content/SharedPreferences;

    .line 1200
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1201
    invoke-static {v3}, Lbr/com/itau/security/securestorage/SecureStorage;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1202
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 97
    .line 1207
    :cond_8e
    move-object v2, p1

    .line 99
    :cond_8f
    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_96

    return-object p2

    :cond_96
    invoke-static {v2}, Lbr/com/itau/security/securestorage/SecureStorage;->decrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;
    .registers 6

    .line 104
    iget-object v0, p0, Lbr/com/itau/security/securestorage/SecureStorage;->e:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lbr/com/itau/security/securestorage/SecureStorage;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object p1

    .line 105
    invoke-interface {p1, p2}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 106
    return-object p2

    .line 108
    :cond_11
    new-instance p2, Ljava/util/HashSet;

    invoke-direct {p2}, Ljava/util/HashSet;-><init>()V

    .line 110
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1a
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/String;

    .line 111
    invoke-static {v2}, Lbr/com/itau/security/securestorage/SecureStorage;->decrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1a

    .line 113
    :cond_2f
    return-object p2
.end method

.method public final registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .registers 3

    .line 152
    iget-object v0, p0, Lbr/com/itau/security/securestorage/SecureStorage;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 153
    return-void
.end method

.method public final unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V
    .registers 3

    .line 157
    iget-object v0, p0, Lbr/com/itau/security/securestorage/SecureStorage;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 158
    return-void
.end method

.class public final Lcom/itau/empresas/ui/util/maquinaestado/Estado;
.super Ljava/lang/Object;
.source "Estado.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;,
        Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;
    }
.end annotation


# instance fields
.field private final gones:[Landroid/view/View;

.field private final onChangeState:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

.field private final views:[Landroid/view/View;

.field private final visibles:[Landroid/view/View;


# direct methods
.method private constructor <init>([Landroid/view/View;[Landroid/view/View;[Landroid/view/View;Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)V
    .registers 5
    .param p1, "views"    # [Landroid/view/View;
    .param p2, "visibles"    # [Landroid/view/View;
    .param p3, "gones"    # [Landroid/view/View;
    .param p4, "onChangeState"    # Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/itau/empresas/ui/util/maquinaestado/Estado;->views:[Landroid/view/View;

    .line 19
    iput-object p2, p0, Lcom/itau/empresas/ui/util/maquinaestado/Estado;->visibles:[Landroid/view/View;

    .line 20
    iput-object p3, p0, Lcom/itau/empresas/ui/util/maquinaestado/Estado;->gones:[Landroid/view/View;

    .line 21
    iput-object p4, p0, Lcom/itau/empresas/ui/util/maquinaestado/Estado;->onChangeState:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 22
    return-void
.end method

.method synthetic constructor <init>([Landroid/view/View;[Landroid/view/View;[Landroid/view/View;Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;Lcom/itau/empresas/ui/util/maquinaestado/Estado$1;)V
    .registers 6
    .param p1, "x0"    # [Landroid/view/View;
    .param p2, "x1"    # [Landroid/view/View;
    .param p3, "x2"    # [Landroid/view/View;
    .param p4, "x3"    # Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;
    .param p5, "x4"    # Lcom/itau/empresas/ui/util/maquinaestado/Estado$1;

    .line 5
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/itau/empresas/ui/util/maquinaestado/Estado;-><init>([Landroid/view/View;[Landroid/view/View;[Landroid/view/View;Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)V

    return-void
.end method


# virtual methods
.method public getGones()[Landroid/view/View;
    .registers 2

    .line 29
    iget-object v0, p0, Lcom/itau/empresas/ui/util/maquinaestado/Estado;->gones:[Landroid/view/View;

    return-object v0
.end method

.method public getOnChangeState()Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;
    .registers 2

    .line 34
    iget-object v0, p0, Lcom/itau/empresas/ui/util/maquinaestado/Estado;->onChangeState:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    return-object v0
.end method

.method public getVisibles()[Landroid/view/View;
    .registers 2

    .line 25
    iget-object v0, p0, Lcom/itau/empresas/ui/util/maquinaestado/Estado;->visibles:[Landroid/view/View;

    return-object v0
.end method

.class public Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;
.super Ljava/lang/Object;
.source "ParcelasEmprestimoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;
    }
.end annotation


# instance fields
.field private enumValoresEmprestimo:Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

.field private parcelaMaxima:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "parcela_maxima"
    .end annotation
.end field

.field private parcelaMinima:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "parcela_minima"
    .end annotation
.end field

.field private quantidade:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade"
    .end annotation
.end field

.field private taxaJurosMes:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros_mes"
    .end annotation
.end field

.field private valor:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getEnumValoresEmprestimo()Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;
    .registers 2

    .line 76
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->enumValoresEmprestimo:Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    return-object v0
.end method

.method public getQuantidade()J
    .registers 3

    .line 38
    iget-wide v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->quantidade:J

    return-wide v0
.end method

.method public getValor()F
    .registers 2

    .line 46
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->valor:F

    return v0
.end method

.method public setEnumValoresEmprestimo(Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;)V
    .registers 2
    .param p1, "enumValoresEmprestimo"    # Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    .line 80
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->enumValoresEmprestimo:Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    .line 81
    return-void
.end method

.method public setParcelaMaxima(J)V
    .registers 3
    .param p1, "parcelaMaxima"    # J

    .line 96
    iput-wide p1, p0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->parcelaMaxima:J

    .line 97
    return-void
.end method

.method public setParcelaMinima(J)V
    .registers 3
    .param p1, "parcelaMinima"    # J

    .line 92
    iput-wide p1, p0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->parcelaMinima:J

    .line 93
    return-void
.end method

.method public setQuantidade(J)V
    .registers 3
    .param p1, "quantidade"    # J

    .line 42
    iput-wide p1, p0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->quantidade:J

    .line 43
    return-void
.end method

.method public setValor(F)V
    .registers 2
    .param p1, "valor"    # F

    .line 84
    iput p1, p0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->valor:F

    .line 85
    return-void
.end method

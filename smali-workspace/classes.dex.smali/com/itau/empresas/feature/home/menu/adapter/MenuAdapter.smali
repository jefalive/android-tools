.class public Lcom/itau/empresas/feature/home/menu/adapter/MenuAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "MenuAdapter.java"

# interfaces
.implements Lcom/itau/empresas/feature/home/menu/adapter/ViewHolderClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter<Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;>;Lcom/itau/empresas/feature/home/menu/adapter/ViewHolderClickListener;"
    }
.end annotation


# instance fields
.field private cardMenu:Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;

.field private final listMenu:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;"
        }
    .end annotation
.end field

.field private final regraLayoutAdapter:Lcom/itau/empresas/feature/home/menu/adapter/RegraLayoutAdapter;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/itau/empresas/feature/home/menu/adapter/RegraLayoutAdapter;Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;)V
    .registers 4
    .param p1, "listMenu"    # Ljava/util/List;
    .param p2, "regraLayoutAdapter"    # Lcom/itau/empresas/feature/home/menu/adapter/RegraLayoutAdapter;
    .param p3, "selecionaItemMenu"    # Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;Lcom/itau/empresas/feature/home/menu/adapter/RegraLayoutAdapter;Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuAdapter;->listMenu:Ljava/util/List;

    .line 30
    iput-object p2, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuAdapter;->regraLayoutAdapter:Lcom/itau/empresas/feature/home/menu/adapter/RegraLayoutAdapter;

    .line 31
    iput-object p3, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuAdapter;->cardMenu:Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;

    .line 32
    return-void
.end method

.method private configuraViewHolder(Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;I)V
    .registers 7
    .param p1, "viewHolder"    # Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;
    .param p2, "posicao"    # I

    .line 69
    invoke-direct {p0, p2}, Lcom/itau/empresas/feature/home/menu/adapter/MenuAdapter;->pegaItemMenu(I)Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v1

    .line 71
    .local v1, "menu":Lcom/itau/empresas/api/model/MenuVO;
    if-eqz v1, :cond_37

    .line 73
    iget-object v0, p1, Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 76
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/MenuVO;->getIcone()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 77
    const v3, 0x7f07024b

    .line 78
    .local v3, "iconResource":I
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;->setMenuIcon(Ljava/lang/String;)V

    goto :goto_30

    .line 80
    .line 81
    .end local v3    # "iconResource":I
    :cond_21
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/MenuVO;->getIcone()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/StringUtils;->StringParaCharUnicode(Ljava/lang/String;)Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Character;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;->setMenuIcon(Ljava/lang/String;)V

    .line 84
    :goto_30
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/MenuVO;->getNome()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;->setMenuText(Ljava/lang/String;)V

    .line 86
    .end local v2    # "context":Landroid/content/Context;
    :cond_37
    return-void
.end method

.method private pegaItemMenu(I)Lcom/itau/empresas/api/model/MenuVO;
    .registers 3
    .param p1, "posicao"    # I

    .line 89
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuAdapter;->listMenu:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/MenuVO;

    return-object v0
.end method


# virtual methods
.method public clicadoNaPosicao(I)V
    .registers 5
    .param p1, "posicao"    # I

    .line 94
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuAdapter;->cardMenu:Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;

    if-eqz v0, :cond_22

    .line 95
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/menu/adapter/MenuAdapter;->pegaItemMenu(I)Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v1

    .line 97
    .local v1, "menu":Lcom/itau/empresas/api/model/MenuVO;
    if-eqz v1, :cond_22

    .line 98
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1c

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1c

    const/4 v2, 0x1

    goto :goto_1d

    :cond_1c
    const/4 v2, 0x0

    .line 99
    .local v2, "temSubMenu":Z
    :goto_1d
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuAdapter;->cardMenu:Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;

    invoke-interface {v0, v1, v2}, Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;->menuSelecionado(Lcom/itau/empresas/api/model/MenuVO;Z)V

    .line 102
    .end local v1    # "menu":Lcom/itau/empresas/api/model/MenuVO;
    .end local v2    # "temSubMenu":Z
    :cond_22
    return-void
.end method

.method public getItemCount()I
    .registers 2

    .line 58
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuAdapter;->listMenu:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .registers 3
    .param p1, "position"    # I

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuAdapter;->regraLayoutAdapter:Lcom/itau/empresas/feature/home/menu/adapter/RegraLayoutAdapter;

    invoke-interface {v0, p1}, Lcom/itau/empresas/feature/home/menu/adapter/RegraLayoutAdapter;->calculaViewType(I)I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .registers 4

    .line 16
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;

    invoke-virtual {p0, v0, p2}, Lcom/itau/empresas/feature/home/menu/adapter/MenuAdapter;->onBindViewHolder(Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;I)V
    .registers 3
    .param p1, "holder"    # Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;
    .param p2, "position"    # I

    .line 53
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/feature/home/menu/adapter/MenuAdapter;->configuraViewHolder(Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;I)V

    .line 54
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .registers 4

    .line 16
    invoke-virtual {p0, p1, p2}, Lcom/itau/empresas/feature/home/menu/adapter/MenuAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;
    .registers 7
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .line 37
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 40
    .local v2, "inflater":Landroid/view/LayoutInflater;
    if-nez p2, :cond_13

    .line 41
    const v0, 0x7f030157

    const/4 v1, 0x0

    invoke-virtual {v2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .local v3, "layout":Landroid/view/View;
    goto :goto_1b

    .line 44
    .line 45
    .end local v3    # "layout":Landroid/view/View;
    :cond_13
    const v0, 0x7f030158

    const/4 v1, 0x0

    invoke-virtual {v2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 48
    .local v3, "layout":Landroid/view/View;
    :goto_1b
    new-instance v0, Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;

    invoke-direct {v0, v3, p0}, Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;-><init>(Landroid/view/View;Lcom/itau/empresas/feature/home/menu/adapter/ViewHolderClickListener;)V

    return-object v0
.end method

.class public final Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;
.super Ljava/lang/Object;
.source "LisCondicoesGeraisVO.java"


# instance fields
.field private final descricao:Ljava/lang/String;

.field private final descricaoAcessibilidade:Ljava/lang/String;

.field private final icone:Ljava/lang/String;

.field private final titulo:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "icone"    # Ljava/lang/String;
    .param p2, "titulo"    # Ljava/lang/String;
    .param p3, "descricao"    # Ljava/lang/String;
    .param p4, "descricaoAcessibilidade"    # Ljava/lang/String;

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;->icone:Ljava/lang/String;

    .line 16
    iput-object p2, p0, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;->titulo:Ljava/lang/String;

    .line 17
    iput-object p3, p0, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;->descricao:Ljava/lang/String;

    .line 18
    iput-object p4, p0, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;->descricaoAcessibilidade:Ljava/lang/String;

    .line 19
    return-void
.end method


# virtual methods
.method public getDescricao()Ljava/lang/String;
    .registers 2

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;->descricao:Ljava/lang/String;

    return-object v0
.end method

.method public getDescricaoAcessibilidade()Ljava/lang/String;
    .registers 2

    .line 35
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;->descricaoAcessibilidade:Ljava/lang/String;

    return-object v0
.end method

.method public getIcone()Ljava/lang/String;
    .registers 2

    .line 22
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;->icone:Ljava/lang/String;

    return-object v0
.end method

.method public getTitulo()Ljava/lang/String;
    .registers 2

    .line 26
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisCondicoesGeraisVO;->titulo:Ljava/lang/String;

    return-object v0
.end method

.class public Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "RecargaDetalheComprovanteActivity.java"


# instance fields
.field dadosRecarga:Lcom/itau/empresas/feature/recarga/efetivacao/DadosRecargaView;

.field llCampoAutenticacao:Landroid/widget/LinearLayout;

.field llCampoComprovante:Landroid/widget/LinearLayout;

.field llRoot:Landroid/widget/LinearLayout;

.field nome:Ljava/lang/String;

.field resultadoEfetivacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

.field scrollView:Landroid/widget/ScrollView;

.field textoAutenticacao:Landroid/widget/TextView;

.field textoComprovante:Landroid/widget/TextView;

.field textoDebitado:Landroid/widget/TextView;

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field warning:Lcom/itau/empresas/ui/view/WarningView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 45
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 89
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 91
    const v1, 0x7f0706b8

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 94
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 252
    new-instance v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private preencheDadosIniciais()V
    .registers 1

    .line 97
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->preencherDadosWarning()V

    .line 98
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->preencherDadosComprovante()V

    .line 99
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->preencherDadosRecarga()V

    .line 100
    return-void
.end method

.method private preencherCampo(Landroid/widget/LinearLayout;Landroid/widget/TextView;Ljava/lang/String;)V
    .registers 5
    .param p1, "group"    # Landroid/widget/LinearLayout;
    .param p2, "campo"    # Landroid/widget/TextView;
    .param p3, "texto"    # Ljava/lang/String;

    .line 138
    if-eqz p3, :cond_c

    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 139
    :cond_c
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_19

    .line 141
    :cond_12
    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 145
    :goto_19
    return-void
.end method

.method private preencherDadosComprovante()V
    .registers 8

    .line 122
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->resultadoEfetivacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;->getComprovante()Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;

    move-result-object v3

    .line 123
    .local v3, "comprovanteVO":Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;
    if-eqz v3, :cond_24

    .line 124
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->llCampoComprovante:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->textoComprovante:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->resultadoEfetivacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    .line 125
    invoke-virtual {v2}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;->getRecarga()Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getIdentificacaoComprovante()Ljava/lang/String;

    move-result-object v2

    .line 124
    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->preencherCampo(Landroid/widget/LinearLayout;Landroid/widget/TextView;Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->llCampoAutenticacao:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->textoAutenticacao:Landroid/widget/TextView;

    .line 127
    invoke-virtual {v3}, Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;->getCodigoAutenticacao()Ljava/lang/String;

    move-result-object v2

    .line 126
    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->preencherCampo(Landroid/widget/LinearLayout;Landroid/widget/TextView;Ljava/lang/String;)V

    .line 130
    :cond_24
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->resultadoEfetivacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;->getCliente()Lcom/itau/empresas/api/model/DadosClienteVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/DadosClienteVO;->getAgencia()Ljava/lang/String;

    move-result-object v4

    .line 131
    .local v4, "agencia":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->resultadoEfetivacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;->getCliente()Lcom/itau/empresas/api/model/DadosClienteVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/DadosClienteVO;->getConta()Ljava/lang/String;

    move-result-object v5

    .line 132
    .local v5, "conta":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->resultadoEfetivacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;->getCliente()Lcom/itau/empresas/api/model/DadosClienteVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/DadosClienteVO;->getDigitoVerificadorDaConta()Ljava/lang/String;

    move-result-object v6

    .line 134
    .local v6, "dac":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->textoDebitado:Landroid/widget/TextView;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v4, v1, v2

    const/4 v2, 0x1

    aput-object v5, v1, v2

    const/4 v2, 0x2

    aput-object v6, v1, v2

    const v2, 0x7f070551

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    return-void
.end method

.method private preencherDadosRecarga()V
    .registers 7

    .line 151
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->resultadoEfetivacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;->getRecarga()Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;

    move-result-object v3

    .line 152
    .local v3, "recarga":Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;
    invoke-virtual {v3}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getNomeRegiao()Ljava/lang/String;

    move-result-object v4

    .line 154
    .local v4, "nomeRegiao":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->dadosRecarga:Lcom/itau/empresas/feature/recarga/efetivacao/DadosRecargaView;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->nome:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/DadosRecargaView;->setNome(Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->dadosRecarga:Lcom/itau/empresas/feature/recarga/efetivacao/DadosRecargaView;

    invoke-virtual {v0, v4}, Lcom/itau/empresas/feature/recarga/efetivacao/DadosRecargaView;->setRegiao(Ljava/lang/String;)V

    .line 157
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->resultadoEfetivacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;->getRecarga()Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;

    move-result-object v5

    .line 158
    .local v5, "recargaVO":Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->dadosRecarga:Lcom/itau/empresas/feature/recarga/efetivacao/DadosRecargaView;

    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/DadosRecargaView;->setOperadora(Ljava/lang/String;)V

    .line 159
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->dadosRecarga:Lcom/itau/empresas/feature/recarga/efetivacao/DadosRecargaView;

    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getValorRecarga()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/DadosRecargaView;->setValor(Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->dadosRecarga:Lcom/itau/empresas/feature/recarga/efetivacao/DadosRecargaView;

    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getDddTelefone()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getNumeroTelefone()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/DadosRecargaView;->setCelular(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    return-void
.end method

.method private preencherDadosWarning()V
    .registers 8

    .line 103
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->resultadoEfetivacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;->getComprovante()Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;

    move-result-object v5

    .line 104
    .local v5, "comprovanteVO":Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;
    if-nez v5, :cond_10

    .line 105
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->warning:Lcom/itau/empresas/ui/view/WarningView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/WarningView;->setVisibility(I)V

    .line 106
    return-void

    .line 108
    :cond_10
    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;->getStatusPagamento()Ljava/lang/String;

    move-result-object v6

    .line 109
    .local v6, "statusPagamento":Ljava/lang/String;
    const v0, 0x7f0704bc

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 110
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->warning:Lcom/itau/empresas/ui/view/WarningView;

    sget-object v1, Lcom/itau/empresas/ui/view/WarningView$Estado;->AGUARDANDO:Lcom/itau/empresas/ui/view/WarningView$Estado;

    const v2, 0x7f070660

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/view/WarningView;->setMensagem(Lcom/itau/empresas/ui/view/WarningView$Estado;I)V

    goto :goto_53

    .line 113
    :cond_2c
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->warning:Lcom/itau/empresas/ui/view/WarningView;

    sget-object v1, Lcom/itau/empresas/ui/view/WarningView$Estado;->SUCESSO:Lcom/itau/empresas/ui/view/WarningView$Estado;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 115
    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;->getDataPagamento()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 116
    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;->getHoraPagamento()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataHorario(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 114
    const v3, 0x7f07065f

    invoke-virtual {p0, v3, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 113
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/view/WarningView;->setMensagem(Lcom/itau/empresas/ui/view/WarningView$Estado;Ljava/lang/String;)V

    .line 118
    :goto_53
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->warning:Lcom/itau/empresas/ui/view/WarningView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/WarningView;->setVisibility(I)V

    .line 119
    return-void
.end method


# virtual methods
.method protected aoClicarCompartilhar()V
    .registers 6

    .line 204
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 205
    const v2, 0x7f0702e6

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 206
    const v3, 0x7f0702a1

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 208
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->criarFotoComprovante()Ljava/io/File;

    move-result-object v4

    .line 209
    .local v4, "f":Ljava/io/File;
    if-eqz v4, :cond_25

    .line 210
    invoke-static {p0, v4}, Lcom/itau/empresas/ui/util/CompartilhamentoUtils;->compartilharArquivoImagem(Landroid/app/Activity;Ljava/io/File;)V

    .line 212
    :cond_25
    return-void
.end method

.method protected aoClicarSalvar()V
    .registers 6

    .line 189
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 190
    const v2, 0x7f0702e6

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 191
    const v3, 0x7f0702c2

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 193
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->criarFotoComprovante()Ljava/io/File;

    move-result-object v4

    .line 194
    .local v4, "f":Ljava/io/File;
    if-eqz v4, :cond_25

    .line 195
    invoke-static {p0, v4}, Lcom/itau/empresas/ui/util/BitmapUtils;->abrirArquivoImagem(Landroid/content/Context;Ljava/io/File;)V

    .line 197
    :cond_25
    return-void
.end method

.method carregaElementosDaTela()V
    .registers 1

    .line 84
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->constroiToolbar()V

    .line 85
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->preencheDadosIniciais()V

    .line 86
    return-void
.end method

.method criarFotoComprovante()Ljava/io/File;
    .registers 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "LongLogTag"
        }
    .end annotation

    .line 170
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 171
    .local v2, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->warning:Lcom/itau/empresas/ui/view/WarningView;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;->scrollView:Landroid/widget/ScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    const/4 v3, 0x0

    .line 176
    .local v3, "f":Ljava/io/File;
    const-string v0, "recarga"

    :try_start_1c
    invoke-static {p0, v2, v0}, Lcom/itau/empresas/ui/util/BitmapUtils;->salvarViewsPrint(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)Ljava/io/File;
    :try_end_1f
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1f} :catch_22

    move-result-object v0

    move-object v3, v0

    .line 179
    goto :goto_2a

    .line 177
    :catch_22
    move-exception v4

    .line 178
    .local v4, "e":Ljava/io/IOException;
    const-string v0, "RecargaDetalheComprovanteActivity"

    const-string v1, "Erro ao criar comprovante"

    invoke-static {v0, v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 181
    .end local v4    # "e":Ljava/io/IOException;
    :goto_2a
    return-object v3
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 7
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .line 216
    if-nez p1, :cond_2d

    const/4 v0, -0x1

    if-ne p2, v0, :cond_2d

    .line 217
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->UM_BOTAO:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 219
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 220
    const v1, 0x7f070520

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 221
    const v1, 0x7f07048b

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNeutro(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 222
    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v2

    .line 223
    .local v2, "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    new-instance v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity$1;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity$1;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaDetalheComprovanteActivity;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNeutroListener(Landroid/view/View$OnClickListener;)V

    .line 229
    invoke-virtual {v2, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 231
    .end local v2    # "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    :cond_2d
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/activity/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 232
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 3
    .param p1, "menu"    # Landroid/view/Menu;

    .line 241
    invoke-static {p0, p1}, Lcom/itau/empresas/ui/util/PoyntUtils;->onCreateOptionsMenu(Landroid/app/Activity;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 246
    invoke-static {p0, p1}, Lcom/itau/empresas/ui/util/PoyntUtils;->onOptionsItemSelected(Lcom/itau/empresas/ui/activity/BaseActivity;Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 247
    const/4 v0, 0x1

    return v0

    .line 248
    :cond_8
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 236
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

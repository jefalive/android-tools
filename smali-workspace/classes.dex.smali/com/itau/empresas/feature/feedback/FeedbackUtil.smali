.class final Lcom/itau/empresas/feature/feedback/FeedbackUtil;
.super Ljava/lang/Object;
.source "FeedbackUtil.java"


# direct methods
.method static getQuantidadeAcessos(Landroid/content/SharedPreferences;)I
    .registers 3
    .param p0, "sharedPreferences"    # Landroid/content/SharedPreferences;

    .line 25
    const-string v0, "FB-S"

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method static salvaQuantidadeOperacoesSucesso(Landroid/content/SharedPreferences;)V
    .registers 4
    .param p0, "sharedPreferences"    # Landroid/content/SharedPreferences;

    .line 14
    invoke-static {p0}, Lcom/itau/empresas/feature/feedback/FeedbackUtil;->getQuantidadeAcessos(Landroid/content/SharedPreferences;)I

    move-result v2

    .line 15
    .local v2, "qtd":I
    add-int/lit8 v2, v2, 0x1

    .line 17
    const/4 v0, 0x7

    if-gt v2, v0, :cond_16

    .line 18
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "FB-S"

    .line 19
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 20
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 22
    :cond_16
    return-void
.end method

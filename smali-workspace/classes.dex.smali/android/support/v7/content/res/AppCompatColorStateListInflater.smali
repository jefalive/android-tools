.class final Landroid/support/v7/content/res/AppCompatColorStateListInflater;
.super Ljava/lang/Object;
.source "AppCompatColorStateListInflater.java"


# direct methods
.method public static createFromXml(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/content/res/Resources$Theme;)Landroid/content/res/ColorStateList;
    .registers 7
    .param p0, "r"    # Landroid/content/res/Resources;
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2, "theme"    # Landroid/content/res/Resources$Theme;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 55
    invoke-static {p1}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v2

    .line 58
    .local v2, "attrs":Landroid/util/AttributeSet;
    :goto_4
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    move v3, v0

    .local v3, "type":I
    const/4 v1, 0x2

    if-eq v0, v1, :cond_10

    const/4 v0, 0x1

    if-eq v3, v0, :cond_10

    goto :goto_4

    .line 63
    :cond_10
    const/4 v0, 0x2

    if-eq v3, v0, :cond_1b

    .line 64
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v1, "No start tag found"

    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_1b
    invoke-static {p0, p1, v2, p2}, Landroid/support/v7/content/res/AppCompatColorStateListInflater;->createFromXmlInner(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method private static createFromXmlInner(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/content/res/ColorStateList;
    .registers 8
    .param p0, "r"    # Landroid/content/res/Resources;
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "theme"    # Landroid/content/res/Resources$Theme;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 82
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 83
    .local v3, "name":Ljava/lang/String;
    const-string v0, "selector"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2d

    .line 84
    new-instance v0, Lorg/xmlpull/v1/XmlPullParserException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 85
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": invalid color state list tag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88
    :cond_2d
    invoke-static {p0, p1, p2, p3}, Landroid/support/v7/content/res/AppCompatColorStateListInflater;->inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method private static inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)Landroid/content/res/ColorStateList;
    .registers 23
    .param p0, "r"    # Landroid/content/res/Resources;
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "theme"    # Landroid/content/res/Resources$Theme;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 97
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v0

    add-int/lit8 v4, v0, 0x1

    .line 100
    .local v4, "innerDepth":I
    const/high16 v7, -0x10000

    .line 102
    .local v7, "defaultColor":I
    const/16 v0, 0x14

    new-array v8, v0, [[I

    .line 103
    .local v8, "stateSpecList":[[I
    array-length v0, v8

    new-array v9, v0, [I

    .line 104
    .local v9, "colorList":[I
    const/4 v10, 0x0

    .line 106
    .local v10, "listSize":I
    :cond_10
    :goto_10
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    move v6, v0

    .local v6, "type":I
    const/4 v1, 0x1

    if-eq v0, v1, :cond_d5

    .line 107
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v0

    move v5, v0

    .local v5, "depth":I
    if-ge v0, v4, :cond_22

    const/4 v0, 0x3

    if-eq v6, v0, :cond_d5

    .line 108
    :cond_22
    const/4 v0, 0x2

    if-ne v6, v0, :cond_10

    if-gt v5, v4, :cond_10

    .line 109
    invoke-interface/range {p1 .. p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "item"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_34

    .line 110
    goto :goto_10

    .line 113
    :cond_34
    sget-object v0, Landroid/support/v7/appcompat/R$styleable;->ColorStateListItem:[I

    move-object/from16 v1, p0

    move-object/from16 v2, p3

    move-object/from16 v3, p2

    invoke-static {v1, v2, v3, v0}, Landroid/support/v7/content/res/AppCompatColorStateListInflater;->obtainAttributes(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v11

    .line 114
    .local v11, "a":Landroid/content/res/TypedArray;
    sget v0, Landroid/support/v7/appcompat/R$styleable;->ColorStateListItem_android_color:I

    const v1, -0xff01

    invoke-virtual {v11, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v12

    .line 117
    .local v12, "baseColor":I
    const/high16 v13, 0x3f800000    # 1.0f

    .line 118
    .local v13, "alphaMod":F
    sget v0, Landroid/support/v7/appcompat/R$styleable;->ColorStateListItem_android_alpha:I

    invoke-virtual {v11, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 119
    sget v0, Landroid/support/v7/appcompat/R$styleable;->ColorStateListItem_android_alpha:I

    invoke-virtual {v11, v0, v13}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v13

    goto :goto_68

    .line 120
    :cond_5a
    sget v0, Landroid/support/v7/appcompat/R$styleable;->ColorStateListItem_alpha:I

    invoke-virtual {v11, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_68

    .line 121
    sget v0, Landroid/support/v7/appcompat/R$styleable;->ColorStateListItem_alpha:I

    invoke-virtual {v11, v0, v13}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v13

    .line 124
    :cond_68
    :goto_68
    invoke-virtual {v11}, Landroid/content/res/TypedArray;->recycle()V

    .line 127
    const/4 v14, 0x0

    .line 128
    .local v14, "j":I
    invoke-interface/range {p2 .. p2}, Landroid/util/AttributeSet;->getAttributeCount()I

    move-result v15

    .line 129
    .local v15, "numAttrs":I
    new-array v0, v15, [I

    move-object/from16 v16, v0

    .line 130
    .local v16, "stateSpec":[I
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_76
    move/from16 v0, v17

    if-ge v0, v15, :cond_af

    .line 131
    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/util/AttributeSet;->getAttributeNameResource(I)I

    move-result v18

    .line 132
    .local v18, "stateResId":I
    move/from16 v0, v18

    const v1, 0x10101a5

    if-eq v0, v1, :cond_ac

    move/from16 v0, v18

    const v1, 0x101031f

    if-eq v0, v1, :cond_ac

    sget v0, Landroid/support/v7/appcompat/R$attr;->alpha:I

    move/from16 v1, v18

    if-eq v1, v0, :cond_ac

    .line 135
    move v0, v14

    add-int/lit8 v14, v14, 0x1

    move-object/from16 v1, p2

    move/from16 v2, v17

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/util/AttributeSet;->getAttributeBooleanValue(IZ)Z

    move-result v1

    if-eqz v1, :cond_a7

    move/from16 v1, v18

    goto :goto_aa

    :cond_a7
    move/from16 v1, v18

    neg-int v1, v1

    :goto_aa
    aput v1, v16, v0

    .line 130
    .end local v18    # "stateResId":I
    :cond_ac
    add-int/lit8 v17, v17, 0x1

    goto :goto_76

    .line 139
    .end local v17    # "i":I
    :cond_af
    move-object/from16 v0, v16

    invoke-static {v0, v14}, Landroid/util/StateSet;->trimStateSet([II)[I

    move-result-object v16

    .line 144
    invoke-static {v12, v13}, Landroid/support/v7/content/res/AppCompatColorStateListInflater;->modulateColorAlpha(IF)I

    move-result v17

    .line 145
    .local v17, "color":I
    if-eqz v10, :cond_c0

    move-object/from16 v0, v16

    array-length v0, v0

    if-nez v0, :cond_c2

    .line 146
    :cond_c0
    move/from16 v7, v17

    .line 149
    :cond_c2
    move/from16 v0, v17

    invoke-static {v9, v10, v0}, Landroid/support/v7/content/res/GrowingArrayUtils;->append([III)[I

    move-result-object v9

    .line 150
    move-object/from16 v0, v16

    invoke-static {v8, v10, v0}, Landroid/support/v7/content/res/GrowingArrayUtils;->append([Ljava/lang/Object;ILjava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, [[I

    .line 151
    add-int/lit8 v10, v10, 0x1

    .line 152
    .end local v5    # "depth":I
    .end local v11    # "a":Landroid/content/res/TypedArray;
    .end local v12    # "baseColor":I
    .end local v13    # "alphaMod":F
    .end local v14    # "j":I
    .end local v15    # "numAttrs":I
    .end local v16    # "stateSpec":[I
    .end local v17    # "color":I
    goto/16 :goto_10

    .line 154
    :cond_d5
    new-array v11, v10, [I

    .line 155
    .local v11, "colors":[I
    new-array v12, v10, [[I

    .line 156
    .local v12, "stateSpecs":[[I
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v9, v0, v11, v1, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 157
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v8, v0, v12, v1, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 159
    new-instance v0, Landroid/content/res/ColorStateList;

    invoke-direct {v0, v12, v11}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    return-object v0
.end method

.method private static modulateColorAlpha(IF)I
    .registers 3
    .param p0, "color"    # I
    .param p1, "alphaMod"    # F

    .line 169
    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {p0, v0}, Landroid/support/v4/graphics/ColorUtils;->setAlphaComponent(II)I

    move-result v0

    return v0
.end method

.method private static obtainAttributes(Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    .registers 6
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "theme"    # Landroid/content/res/Resources$Theme;
    .param p2, "set"    # Landroid/util/AttributeSet;
    .param p3, "attrs"    # [I

    .line 164
    if-nez p1, :cond_7

    invoke-virtual {p0, p2, p3}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    goto :goto_d

    .line 165
    :cond_7
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1, p2, p3, v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    :goto_d
    return-object v0
.end method

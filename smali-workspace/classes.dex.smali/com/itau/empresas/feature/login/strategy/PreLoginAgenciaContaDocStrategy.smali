.class public Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;
.super Ljava/lang/Object;
.source "PreLoginAgenciaContaDocStrategy.java"

# interfaces
.implements Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;
.implements Lcom/itau/empresas/feature/login/strategy/PreLoginControllerListener;


# instance fields
.field private final agencia:Ljava/lang/String;

.field private final conta:Ljava/lang/String;

.field private final controller:Lcom/itau/empresas/feature/login/controller/PreLoginController;

.field private final dac:Ljava/lang/String;

.field private final doc:Ljava/lang/String;

.field private operadorVOs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/OperadorVO;>;"
        }
    .end annotation
.end field

.field private final strategy:Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;

.field private final tipoDocumento:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/login/controller/PreLoginController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;)V
    .registers 9
    .param p1, "controller"    # Lcom/itau/empresas/feature/login/controller/PreLoginController;
    .param p2, "agencia"    # Ljava/lang/String;
    .param p3, "conta"    # Ljava/lang/String;
    .param p4, "dac"    # Ljava/lang/String;
    .param p5, "doc"    # Ljava/lang/String;
    .param p6, "tipoDocumento"    # Ljava/lang/String;
    .param p7, "strategy"    # Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;->controller:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    .line 29
    iput-object p5, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;->doc:Ljava/lang/String;

    .line 30
    iput-object p6, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;->tipoDocumento:Ljava/lang/String;

    .line 31
    iget-object v0, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;->controller:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    invoke-virtual {v0, p0}, Lcom/itau/empresas/feature/login/controller/PreLoginController;->setListener(Lcom/itau/empresas/feature/login/strategy/PreLoginControllerListener;)V

    .line 32
    iput-object p2, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;->agencia:Ljava/lang/String;

    .line 33
    iput-object p3, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;->conta:Ljava/lang/String;

    .line 34
    iput-object p4, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;->dac:Ljava/lang/String;

    .line 35
    iput-object p7, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;->strategy:Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;

    .line 36
    return-void
.end method


# virtual methods
.method public backendException(Lbr/com/itau/sdk/android/core/exception/BackendException;)V
    .registers 4
    .param p1, "ex"    # Lbr/com/itau/sdk/android/core/exception/BackendException;

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;->strategy:Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;->falha(Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method public executar()V
    .registers 8

    .line 40
    iget-object v0, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;->controller:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;->agencia:Ljava/lang/String;

    iget-object v2, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;->conta:Ljava/lang/String;

    iget-object v3, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;->dac:Ljava/lang/String;

    iget-object v4, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;->doc:Ljava/lang/String;

    iget-object v5, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;->tipoDocumento:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual/range {v0 .. v6}, Lcom/itau/empresas/feature/login/controller/PreLoginController;->buscaOperador(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    return-void
.end method

.method public getListaOperadores()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/api/model/OperadorVO;>;"
        }
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;->operadorVOs:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public listaOperadores(Ljava/util/List;)V
    .registers 5
    .param p1, "operadorVOs"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/OperadorVO;>;)V"
        }
    .end annotation

    .line 49
    iput-object p1, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;->operadorVOs:Ljava/util/List;

    .line 50
    invoke-static {p1}, Lcom/itau/empresas/feature/login/strategy/ValidadorStrategy;->possuiSomenteUmOperador(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 52
    new-instance v0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;

    invoke-direct {v0}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;-><init>()V

    .line 53
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/api/model/OperadorVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/OperadorVO;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->operador(Ljava/lang/String;)Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->build()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;

    move-result-object v2

    .line 54
    .local v2, "operador":Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;->controller:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;->strategy:Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;

    invoke-static {v0, v2, v1}, Lcom/itau/empresas/feature/login/strategy/PreLoginStrategyFactory;->operadorUnico(Lcom/itau/empresas/feature/login/controller/PreLoginController;Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;)Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;

    move-result-object v0

    invoke-interface {v0}, Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;->executar()V

    .line 55
    .end local v2    # "operador":Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;
    goto :goto_32

    .line 56
    :cond_2c
    iget-object v0, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;->strategy:Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;->proximoPasso(I)V

    .line 58
    :goto_32
    return-void
.end method

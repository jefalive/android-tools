.class Lorg/simpleframework/xml/stream/Indenter;
.super Ljava/lang/Object;
.source "Indenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/simpleframework/xml/stream/Indenter$Cache;
    }
.end annotation


# instance fields
.field private cache:Lorg/simpleframework/xml/stream/Indenter$Cache;

.field private count:I

.field private indent:I

.field private index:I


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 64
    new-instance v0, Lorg/simpleframework/xml/stream/Format;

    invoke-direct {v0}, Lorg/simpleframework/xml/stream/Format;-><init>()V

    invoke-direct {p0, v0}, Lorg/simpleframework/xml/stream/Indenter;-><init>(Lorg/simpleframework/xml/stream/Format;)V

    .line 65
    return-void
.end method

.method public constructor <init>(Lorg/simpleframework/xml/stream/Format;)V
    .registers 3
    .param p1, "format"    # Lorg/simpleframework/xml/stream/Format;

    .line 76
    const/16 v0, 0x10

    invoke-direct {p0, p1, v0}, Lorg/simpleframework/xml/stream/Indenter;-><init>(Lorg/simpleframework/xml/stream/Format;I)V

    .line 77
    return-void
.end method

.method private constructor <init>(Lorg/simpleframework/xml/stream/Format;I)V
    .registers 4
    .param p1, "format"    # Lorg/simpleframework/xml/stream/Format;
    .param p2, "size"    # I

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    invoke-virtual {p1}, Lorg/simpleframework/xml/stream/Format;->getIndent()I

    move-result v0

    iput v0, p0, Lorg/simpleframework/xml/stream/Indenter;->indent:I

    .line 90
    new-instance v0, Lorg/simpleframework/xml/stream/Indenter$Cache;

    invoke-direct {v0, p2}, Lorg/simpleframework/xml/stream/Indenter$Cache;-><init>(I)V

    iput-object v0, p0, Lorg/simpleframework/xml/stream/Indenter;->cache:Lorg/simpleframework/xml/stream/Indenter$Cache;

    .line 91
    return-void
.end method

.method private create()Ljava/lang/String;
    .registers 5

    .line 173
    iget v0, p0, Lorg/simpleframework/xml/stream/Indenter;->count:I

    add-int/lit8 v0, v0, 0x1

    new-array v2, v0, [C

    .line 175
    .local v2, "text":[C
    iget v0, p0, Lorg/simpleframework/xml/stream/Indenter;->count:I

    if-lez v0, :cond_21

    .line 176
    const/16 v0, 0xa

    const/4 v1, 0x0

    aput-char v0, v2, v1

    .line 178
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_10
    iget v0, p0, Lorg/simpleframework/xml/stream/Indenter;->count:I

    if-gt v3, v0, :cond_1b

    .line 179
    const/16 v0, 0x20

    aput-char v0, v2, v3

    .line 178
    add-int/lit8 v3, v3, 0x1

    goto :goto_10

    .line 181
    .end local v3    # "i":I
    :cond_1b
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([C)V

    return-object v0

    .line 183
    :cond_21
    const-string v0, "\n"

    return-object v0
.end method

.method private indent(I)Ljava/lang/String;
    .registers 4
    .param p1, "index"    # I

    .line 151
    iget v0, p0, Lorg/simpleframework/xml/stream/Indenter;->indent:I

    if-lez v0, :cond_1e

    .line 152
    iget-object v0, p0, Lorg/simpleframework/xml/stream/Indenter;->cache:Lorg/simpleframework/xml/stream/Indenter$Cache;

    invoke-virtual {v0, p1}, Lorg/simpleframework/xml/stream/Indenter$Cache;->get(I)Ljava/lang/String;

    move-result-object v1

    .line 154
    .local v1, "text":Ljava/lang/String;
    if-nez v1, :cond_15

    .line 155
    invoke-direct {p0}, Lorg/simpleframework/xml/stream/Indenter;->create()Ljava/lang/String;

    move-result-object v1

    .line 156
    iget-object v0, p0, Lorg/simpleframework/xml/stream/Indenter;->cache:Lorg/simpleframework/xml/stream/Indenter$Cache;

    invoke-virtual {v0, p1, v1}, Lorg/simpleframework/xml/stream/Indenter$Cache;->set(ILjava/lang/String;)V

    .line 158
    :cond_15
    iget-object v0, p0, Lorg/simpleframework/xml/stream/Indenter;->cache:Lorg/simpleframework/xml/stream/Indenter$Cache;

    invoke-virtual {v0}, Lorg/simpleframework/xml/stream/Indenter$Cache;->size()I

    move-result v0

    if-lez v0, :cond_1e

    .line 159
    return-object v1

    .line 162
    .end local v1    # "text":Ljava/lang/String;
    :cond_1e
    const-string v0, ""

    return-object v0
.end method


# virtual methods
.method public pop()Ljava/lang/String;
    .registers 4

    .line 132
    iget v0, p0, Lorg/simpleframework/xml/stream/Indenter;->index:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/simpleframework/xml/stream/Indenter;->index:I

    invoke-direct {p0, v0}, Lorg/simpleframework/xml/stream/Indenter;->indent(I)Ljava/lang/String;

    move-result-object v2

    .line 134
    .local v2, "text":Ljava/lang/String;
    iget v0, p0, Lorg/simpleframework/xml/stream/Indenter;->indent:I

    if-lez v0, :cond_15

    .line 135
    iget v0, p0, Lorg/simpleframework/xml/stream/Indenter;->count:I

    iget v1, p0, Lorg/simpleframework/xml/stream/Indenter;->indent:I

    sub-int/2addr v0, v1

    iput v0, p0, Lorg/simpleframework/xml/stream/Indenter;->count:I

    .line 137
    :cond_15
    return-object v2
.end method

.method public push()Ljava/lang/String;
    .registers 4

    .line 114
    iget v0, p0, Lorg/simpleframework/xml/stream/Indenter;->index:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lorg/simpleframework/xml/stream/Indenter;->index:I

    invoke-direct {p0, v0}, Lorg/simpleframework/xml/stream/Indenter;->indent(I)Ljava/lang/String;

    move-result-object v2

    .line 116
    .local v2, "text":Ljava/lang/String;
    iget v0, p0, Lorg/simpleframework/xml/stream/Indenter;->indent:I

    if-lez v0, :cond_15

    .line 117
    iget v0, p0, Lorg/simpleframework/xml/stream/Indenter;->count:I

    iget v1, p0, Lorg/simpleframework/xml/stream/Indenter;->indent:I

    add-int/2addr v0, v1

    iput v0, p0, Lorg/simpleframework/xml/stream/Indenter;->count:I

    .line 119
    :cond_15
    return-object v2
.end method

.method public top()Ljava/lang/String;
    .registers 2

    .line 101
    iget v0, p0, Lorg/simpleframework/xml/stream/Indenter;->index:I

    invoke-direct {p0, v0}, Lorg/simpleframework/xml/stream/Indenter;->indent(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

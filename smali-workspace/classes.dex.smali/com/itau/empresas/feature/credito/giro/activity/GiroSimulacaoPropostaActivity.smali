.class public Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "GiroSimulacaoPropostaActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field botaoContinuar:Landroid/widget/Button;

.field campoDataEmprestimo:Landroid/widget/EditText;

.field campoValorEmprestimo:Landroid/widget/EditText;

.field private carregandoOutroPlano:Z

.field private condicaoCarregaToolbar:Z

.field giroPersonalizadoParcelasView:Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;

.field modoPersonalizado:Z

.field ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

.field ofertaProdutosParceladosSaida:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

.field root:Landroid/widget/RelativeLayout;

.field simulacaoController:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;

.field simulacaoPropostaController:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController;

.field simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

.field textoParcelasComSeguroAval:Landroid/widget/TextView;

.field textoValoresTarifaContratacao:Landroid/widget/TextView;

.field tomadaDecisaoSeguroView:Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field private topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 45
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->condicaoCarregaToolbar:Z

    return-void
.end method

.method private alterarTextoParcelasComSeguroAval(Z)V
    .registers 4
    .param p1, "visivel"    # Z

    .line 347
    if-eqz p1, :cond_9

    .line 348
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->textoParcelasComSeguroAval:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_10

    .line 350
    :cond_9
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->textoParcelasComSeguroAval:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 354
    :goto_10
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->alternarTextoTarifa(Z)V

    .line 355
    return-void
.end method

.method private alternarMensagemIndicandoSelecao(Z)V
    .registers 5
    .param p1, "exibir"    # Z

    .line 441
    if-eqz p1, :cond_17

    .line 443
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->root:Landroid/widget/RelativeLayout;

    const v1, 0x7f07066a

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 444
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    goto :goto_20

    .line 448
    :cond_17
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    if-eqz v0, :cond_20

    .line 449
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismiss()V

    .line 451
    :cond_20
    :goto_20
    return-void
.end method

.method private alternarTextoTarifa(Z)V
    .registers 4
    .param p1, "comSeguro"    # Z

    .line 359
    if-eqz p1, :cond_f

    .line 361
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->textoValoresTarifaContratacao:Landroid/widget/TextView;

    .line 362
    const v1, 0x7f070688

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1b

    .line 366
    :cond_f
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->textoValoresTarifaContratacao:Landroid/widget/TextView;

    .line 367
    const v1, 0x7f070689

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 370
    :goto_1b
    return-void
.end method

.method private buscaSimulacao(Z)V
    .registers 4
    .param p1, "carregandoOutroPlano"    # Z

    .line 374
    iput-boolean p1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->carregandoOutroPlano:Z

    .line 376
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->mostraDialogoDeProgresso()V

    .line 377
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    if-eqz v0, :cond_e

    .line 378
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismiss()V

    .line 379
    :cond_e
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->simulacaoController:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;->consultaSimulacao(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)V

    .line 383
    return-void
.end method

.method private buscaSimulacaoPersonalizada()V
    .registers 8

    .line 387
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    if-eqz v0, :cond_9

    .line 388
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismiss()V

    .line 389
    :cond_9
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->mostraDialogoDeProgresso()V

    .line 391
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->simulacaoPropostaController:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController;->consultarSimulacaoProposta(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)V

    .line 393
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->giroPersonalizadoParcelasView:Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->clearData()V

    .line 395
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->modoPersonalizado:Z

    if-eqz v0, :cond_33

    .line 397
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->giroPersonalizadoParcelasView:Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;

    .line 398
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 400
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getPrazoMinimoContratacao()J

    move-result-wide v3

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 401
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getPrazoMaximoContratacao()J

    move-result-wide v5

    .line 397
    const/4 v2, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->setData(Landroid/content/Context;Ljava/util/List;JJ)V

    goto :goto_49

    .line 405
    :cond_33
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->giroPersonalizadoParcelasView:Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;

    .line 406
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaProdutosParceladosSaida:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    .line 408
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getPrazoMinimoContratacao()J

    move-result-wide v3

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaProdutosParceladosSaida:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    .line 409
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getPrazoMaximoContratacao()J

    move-result-wide v5

    .line 405
    const/4 v2, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->setData(Landroid/content/Context;Ljava/util/List;JJ)V

    .line 412
    :goto_49
    return-void
.end method

.method private carregarDadosNaTela(Ljava/math/BigDecimal;Ljava/lang/String;)V
    .registers 5
    .param p1, "valorEmprestimo"    # Ljava/math/BigDecimal;
    .param p2, "dataPrimeiraParcela"    # Ljava/lang/String;

    .line 340
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->campoValorEmprestimo:Landroid/widget/EditText;

    .line 341
    const/4 v1, 0x1

    invoke-static {p1, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v1

    .line 340
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 342
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->campoDataEmprestimo:Landroid/widget/EditText;

    invoke-virtual {v0, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 343
    return-void
.end method

.method private carregarListaDeParcelas(Ljava/util/List;JJ)V
    .registers 13
    .param p1, "parcelasEmprestimoLista"    # Ljava/util/List;
    .param p2, "prazoMinimoContratacao"    # J
    .param p4, "prazoMaximoContratacao"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;>;JJ)V"
        }
    .end annotation

    .line 418
    if-eqz p1, :cond_14

    .line 420
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->giroPersonalizadoParcelasView:Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->clearData()V

    .line 422
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->giroPersonalizadoParcelasView:Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;

    .line 423
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    move-object v2, p1

    move-wide v3, p2

    move-wide v5, p4

    .line 422
    invoke-virtual/range {v0 .. v6}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->setData(Landroid/content/Context;Ljava/util/List;JJ)V

    goto :goto_25

    .line 429
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->giroPersonalizadoParcelasView:Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->clearData()V

    .line 431
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->giroPersonalizadoParcelasView:Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;

    .line 432
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    move-wide v3, p2

    move-wide v5, p4

    .line 431
    const/4 v2, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->setData(Landroid/content/Context;Ljava/util/List;JJ)V

    .line 437
    :goto_25
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 325
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->condicaoCarregaToolbar:Z

    if-eqz v0, :cond_8

    .line 326
    const v1, 0x7f070699

    .local v1, "id":I
    goto :goto_b

    .line 328
    .end local v1    # "id":I
    :cond_8
    const v1, 0x7f07069a

    .line 331
    .local v1, "id":I
    :goto_b
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 333
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 334
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 335
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 336
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 454
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method protected aoCarregarTela()V
    .registers 7

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    if-eqz v0, :cond_9

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismiss()V

    .line 83
    :cond_9
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->alternarMensagemIndicandoSelecao(Z)V

    .line 86
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->modoPersonalizado:Z

    if-eqz v0, :cond_3f

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getDadosSeguroVO()Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;->isIndicadorSeguro()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->condicaoCarregaToolbar:Z

    .line 90
    :cond_20
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getValorEmprestimo()Ljava/math/BigDecimal;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 92
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getDataPrimeiroPagamento()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 90
    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->carregarDadosNaTela(Ljava/math/BigDecimal;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 95
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getDataPrimeiroPagamento()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->setDataPrimeiroPagamento(Ljava/lang/String;)V

    goto :goto_97

    .line 96
    :cond_3f
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaProdutosParceladosSaida:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    if-eqz v0, :cond_97

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaProdutosParceladosSaida:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    .line 97
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getOfertaEspecialProdutosParceladosVO()Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;

    move-result-object v0

    if-eqz v0, :cond_97

    .line 99
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaProdutosParceladosSaida:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getOfertaEspecialProdutosParceladosVO()Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->isHasIndicadorSeguro()Z

    move-result v0

    if-eqz v0, :cond_5a

    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->condicaoCarregaToolbar:Z

    .line 104
    :cond_5a
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-direct {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 105
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaProdutosParceladosSaida:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->ajustaDados(Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;Z)V

    .line 106
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 108
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getConta()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x7

    invoke-static {v2, v1}, Lcom/itau/empresas/ui/util/ViewUtils;->formataAdicionandoZeros(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 107
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setConta(Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 110
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getDataPrimeiroPagamento()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setDataPrimeiroPagamento(Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getValorEmprestimo()Ljava/math/BigDecimal;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 114
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getDataPrimeiroPagamento()Ljava/lang/String;

    move-result-object v1

    .line 113
    invoke-static {v1}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 112
    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->carregarDadosNaTela(Ljava/math/BigDecimal;Ljava/lang/String;)V

    .line 117
    :cond_97
    :goto_97
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getDadosSeguroVO()Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;

    move-result-object v0

    if-eqz v0, :cond_cd

    .line 119
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->tomadaDecisaoSeguroView:Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 120
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getDadosSeguroVO()Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;->isIndicadorSeguro()Z

    move-result v1

    .line 119
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->alternarTomadaDeDecisao(Z)V

    .line 122
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 123
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getDadosSeguroVO()Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;->isIndicadorSeguro()Z

    move-result v0

    .line 122
    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->alterarTextoParcelasComSeguroAval(Z)V

    .line 125
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getDadosSeguroVO()Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;->isIndicadorSeguro()Z

    move-result v0

    if-nez v0, :cond_cd

    .line 127
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->tomadaDecisaoSeguroView:Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->setDeveBloquearMudancaComSeguro(Z)V

    .line 131
    :cond_cd
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    if-eqz v0, :cond_e7

    .line 133
    move-object v0, p0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getParcelasEmprestimoVO()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 134
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getPrazoMinimoContratacao()J

    move-result-wide v2

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 135
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getPrazoMaximoContratacao()J

    move-result-wide v4

    .line 133
    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->carregarListaDeParcelas(Ljava/util/List;JJ)V

    .line 138
    :cond_e7
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->constroiToolbar()V

    .line 139
    return-void
.end method

.method aoClicarNoBotaoContinuar()V
    .registers 6

    .line 307
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->giroPersonalizadoParcelasView:Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->getSelectedItem()Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;

    move-result-object v4

    .line 309
    .local v4, "parcelasEmprestimoVO":Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;
    if-eqz v4, :cond_23

    .line 311
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->alternarMensagemIndicandoSelecao(Z)V

    .line 312
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    new-instance v1, Ljava/lang/Integer;

    .line 314
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->getQuantidade()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    .line 313
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setQuantidadeParcelas(Ljava/lang/Integer;)V

    .line 315
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->buscaSimulacao(Z)V

    goto :goto_27

    .line 319
    :cond_23
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->alternarMensagemIndicandoSelecao(Z)V

    .line 321
    :goto_27
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .registers 7
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .line 288
    const/4 v0, -0x1

    if-ne p2, v0, :cond_28

    const/4 v0, 0x1

    if-ne p1, v0, :cond_28

    const-string v0, "parcela"

    .line 289
    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 291
    const-string v0, "parcela"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 293
    .local v2, "valor":I
    if-lez v2, :cond_28

    .line 295
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 296
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setQuantidadeParcelas(Ljava/lang/Integer;)V

    .line 298
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->buscaSimulacao(Z)V

    .line 302
    .end local v2    # "valor":I
    :cond_28
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/activity/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 303
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 2
    .param p1, "v"    # Landroid/view/View;

    .line 143
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->onBackPressed()V

    .line 144
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;

    .line 249
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 250
    return-void

    .line 253
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->escondeDialogoDeProgresso()V

    .line 255
    const-string v0, "ItauEmpresas"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Erro na Chamanda: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 256
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->root:Landroid/widget/RelativeLayout;

    const v1, 0x7f0701f2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;II)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 257
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 258
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 6
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 267
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 268
    return-void

    .line 271
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->escondeDialogoDeProgresso()V

    .line 273
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v3

    .line 274
    .local v3, "exception":Lbr/com/itau/sdk/android/core/exception/BackendException;
    const-string v0, "ItauEmpresas"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Erro na Chamanda: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 275
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->root:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 276
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 278
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoGiroOutroPlanoSelecionado;)V
    .registers 5
    .param p1, "eventoGiroOutroPlanoSelecionado"    # Lcom/itau/empresas/Evento$EventoGiroOutroPlanoSelecionado;

    .line 149
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->alternarMensagemIndicandoSelecao(Z)V

    .line 151
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->giroPersonalizadoParcelasView:Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->removeSelection()V

    .line 153
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 154
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getPrazoMinimoContratacao()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->parcelaMinima(J)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 155
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getPrazoMaximoContratacao()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->parcelaMaxima(J)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 156
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getDadosSeguroVO()Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;->isIndicadorSeguro()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->temSeguro(Z)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;->PARCELA:Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    .line 157
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->exibicao(Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    move-result-object v0

    .line 158
    const v1, 0x8000

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->flags(I)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    .line 159
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->startForResult(I)Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 160
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoTomadaDecisaoSeguro;)V
    .registers 5
    .param p1, "eventoTomadaDecisaoSeguro"    # Lcom/itau/empresas/Evento$EventoTomadaDecisaoSeguro;

    .line 224
    invoke-virtual {p1}, Lcom/itau/empresas/Evento$EventoTomadaDecisaoSeguro;->getComSeguro()Z

    move-result v1

    .line 226
    .local v1, "comSeguro":Z
    iput-boolean v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->condicaoCarregaToolbar:Z

    .line 227
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->constroiToolbar()V

    .line 229
    invoke-direct {p0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->alterarTextoParcelasComSeguroAval(Z)V

    .line 231
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getDadosSeguroVO()Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;

    move-result-object v0

    if-nez v0, :cond_1e

    .line 233
    new-instance v2, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;

    invoke-direct {v2}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;-><init>()V

    .line 234
    .local v2, "dadosSeguroVO":Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-virtual {v0, v2}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setDadosSeguroVO(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;)V

    .line 237
    .end local v2    # "dadosSeguroVO":Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;
    :cond_1e
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getDadosSeguroVO()Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;->setIndicadorSeguro(Z)V

    .line 239
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->buscaSimulacaoPersonalizada()V

    .line 240
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;)V
    .registers 8
    .param p1, "propostaSaidaVO"    # Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 200
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->escondeDialogoDeProgresso()V

    .line 202
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 204
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->modoPersonalizado:Z

    if-eqz v0, :cond_14

    .line 206
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 207
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getDataPrimeiroPagamento()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->setDataPrimeiroPagamento(Ljava/lang/String;)V

    .line 210
    :cond_14
    move-object v0, p0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getParcelasEmprestimoVO()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 211
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getPrazoMinimoContratacao()J

    move-result-wide v2

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 212
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getPrazoMaximoContratacao()J

    move-result-wide v4

    .line 210
    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->carregarListaDeParcelas(Ljava/util/List;JJ)V

    .line 214
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;)V
    .registers 8
    .param p1, "simulacaoSaidaVO"    # Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    .line 169
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->escondeDialogoDeProgresso()V

    .line 171
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->giroPersonalizadoParcelasView:Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->getSelectedItem()Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;

    move-result-object v5

    .line 173
    .local v5, "parcelasEmprestimoVO":Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->carregandoOutroPlano:Z

    if-eqz v0, :cond_41

    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getParcelasEmprestimo()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_41

    .line 174
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getParcelasEmprestimo()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_41

    .line 176
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->giroPersonalizadoParcelasView:Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;

    .line 177
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getParcelasEmprestimo()Ljava/util/ArrayList;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->getQuantidade()J

    move-result-wide v1

    .line 178
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getParcelasEmprestimo()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;

    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->getValor()F

    move-result v3

    .line 176
    invoke-virtual {v0, v1, v2, v3}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->addItem(JF)V

    goto :goto_76

    .line 180
    :cond_41
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->carregandoOutroPlano:Z

    if-nez v0, :cond_76

    if-eqz v5, :cond_76

    .line 182
    invoke-virtual {v5}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->getQuantidade()J

    move-result-wide v0

    long-to-int v0, v0

    invoke-virtual {p1, v0}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->setQuantidadeParcelas(I)V

    .line 183
    invoke-virtual {v5}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->getValor()F

    move-result v0

    invoke-virtual {p1, v0}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->setValorParcela(F)V

    .line 184
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 185
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;->ofertaEspecialAceitaVO(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;

    move-result-object v0

    .line 186
    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;->simulacaoSaidaVO(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;

    move-result-object v0

    iget-boolean v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->modoPersonalizado:Z

    .line 187
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;->fluxoPersonalizado(Z)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;

    move-result-object v0

    .line 188
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;->exibirSimulacao(Z)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;

    move-result-object v0

    .line 189
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 191
    :cond_76
    :goto_76
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 282
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "creditoParceladoSimular"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "creditoParceladoPersonalizadoSimular"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

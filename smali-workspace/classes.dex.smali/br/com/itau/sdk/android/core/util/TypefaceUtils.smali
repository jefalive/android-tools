.class public Lbr/com/itau/sdk/android/core/util/TypefaceUtils;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Landroid/graphics/Typeface;

.field private static b:Landroid/graphics/Typeface;


# direct methods
.method public static applyBold(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/widget/TextView;Ljava/lang/String;)V
    .registers 11

    .line 52
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 53
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 55
    new-instance v5, Lbr/com/itau/sdk/android/core/util/TypefaceSpan;

    .line 56
    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 55
    invoke-static {p0, v0}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getFontBold(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-direct {v5, v0}, Lbr/com/itau/sdk/android/core/util/TypefaceSpan;-><init>(Landroid/graphics/Typeface;)V

    .line 57
    new-instance v6, Lbr/com/itau/sdk/android/core/util/TypefaceSpan;

    .line 58
    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 57
    invoke-static {p0, v0}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getFontRegular(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-direct {v6, v0}, Lbr/com/itau/sdk/android/core/util/TypefaceSpan;-><init>(Landroid/graphics/Typeface;)V

    .line 60
    invoke-virtual {v3, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 61
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    const/16 v2, 0x21

    invoke-virtual {v4, v6, v1, v0, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 62
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v7

    const/16 v1, 0x21

    invoke-virtual {v4, v5, v7, v0, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 64
    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    return-void
.end method

.method public static getBoldText(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;
    .registers 8

    .line 75
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3, p2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 76
    new-instance v4, Lbr/com/itau/sdk/android/core/util/TypefaceSpan;

    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-static {p0, v0}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getFontBold(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-direct {v4, v0}, Lbr/com/itau/sdk/android/core/util/TypefaceSpan;-><init>(Landroid/graphics/Typeface;)V

    .line 77
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    const/16 v2, 0x21

    invoke-virtual {v3, v4, v1, v0, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 78
    return-object v3
.end method

.method public static getFontBold(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;
    .registers 4

    .line 37
    sget-object v0, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->b:Landroid/graphics/Typeface;

    if-nez v0, :cond_18

    .line 39
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->getFontBold()Ljava/lang/String;

    move-result-object v1

    .line 40
    if-eqz v1, :cond_11

    .line 41
    invoke-static {p1, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->b:Landroid/graphics/Typeface;

    goto :goto_18

    .line 43
    :cond_11
    const/4 v0, 0x1

    invoke-static {v0}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->b:Landroid/graphics/Typeface;

    .line 46
    :cond_18
    :goto_18
    sget-object v0, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->b:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public static getFontRegular(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;
    .registers 4

    .line 24
    sget-object v0, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->a:Landroid/graphics/Typeface;

    if-nez v0, :cond_18

    .line 25
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->getFontRegular()Ljava/lang/String;

    move-result-object v1

    .line 27
    if-eqz v1, :cond_11

    .line 28
    invoke-static {p1, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->a:Landroid/graphics/Typeface;

    goto :goto_18

    .line 30
    :cond_11
    const/4 v0, 0x0

    invoke-static {v0}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->a:Landroid/graphics/Typeface;

    .line 33
    :cond_18
    :goto_18
    sget-object v0, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->a:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public static getRegularText(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/Context;Ljava/lang/String;)Ljava/lang/CharSequence;
    .registers 8

    .line 68
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3, p2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 69
    new-instance v4, Lbr/com/itau/sdk/android/core/util/TypefaceSpan;

    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-static {p0, v0}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getFontRegular(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-direct {v4, v0}, Lbr/com/itau/sdk/android/core/util/TypefaceSpan;-><init>(Landroid/graphics/Typeface;)V

    .line 70
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    const/16 v2, 0x21

    invoke-virtual {v3, v4, v1, v0, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 71
    return-object v3
.end method

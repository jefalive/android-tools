.class public final Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;
.super Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;
.source "TransferenciaRecebidasDetalheActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 39
    invoke-direct {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;-><init>()V

    .line 43
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;

    .line 39
    invoke-super {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;

    .line 39
    invoke-super {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 57
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 58
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 59
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 60
    invoke-direct {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->injectExtras_()V

    .line 61
    invoke-virtual {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->afterInject()V

    .line 62
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 131
    invoke-virtual {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 132
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_2e

    .line 133
    const-string v0, "acao"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 134
    const-string v0, "acao"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->acao:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;

    .line 136
    :cond_1c
    const-string v0, "transferenciaRecebidasVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 137
    const-string v0, "transferenciaRecebidasVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->transferenciaRecebidasVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    .line 140
    :cond_2e
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 165
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_$4;-><init>(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 173
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 153
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_$3;-><init>(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 161
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 50
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->init_(Landroid/os/Bundle;)V

    .line 51
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 53
    const v0, 0x7f03006a

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->setContentView(I)V

    .line 54
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 96
    const v0, 0x7f0e0348

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->llTransferenciaRecebidasDetalhes:Landroid/view/View;

    .line 97
    const v0, 0x7f0e0096

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->scrollView:Landroid/widget/ScrollView;

    .line 98
    const v0, 0x7f0e02b0

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/WarningView;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->warning:Lcom/itau/empresas/ui/view/WarningView;

    .line 99
    const v0, 0x7f0e034a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->campoTipoTransferencia:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0e034b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->campoValorTranferencia:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0e034d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->textoRemetente:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f0e034f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->textoCpfCnpj:Landroid/widget/TextView;

    .line 103
    const v0, 0x7f0e019a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->textoData:Landroid/widget/TextView;

    .line 104
    const v0, 0x7f0e02ae

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->botaoSalvar:Landroid/widget/Button;

    .line 105
    const v0, 0x7f0e01f0

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->botaoCompartilhar:Landroid/widget/Button;

    .line 106
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 107
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->botaoSalvar:Landroid/widget/Button;

    if-eqz v0, :cond_85

    .line 108
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->botaoSalvar:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_$1;-><init>(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    :cond_85
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->botaoCompartilhar:Landroid/widget/Button;

    if-eqz v0, :cond_93

    .line 118
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->botaoCompartilhar:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_$2;-><init>(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    :cond_93
    invoke-virtual {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->carregaElementosDaTela()V

    .line 128
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 66
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->setContentView(I)V

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 68
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 78
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->setContentView(Landroid/view/View;)V

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 80
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 72
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 74
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 144
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->setIntent(Landroid/content/Intent;)V

    .line 145
    invoke-direct {p0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_;->injectExtras_()V

    .line 146
    return-void
.end method

.class Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$1;
.super Ljava/lang/Object;
.source "PendenciasAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->ajustaViewTypeLista(Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderLista;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;

.field final synthetic val$pendenciasVO:Lcom/itau/empresas/feature/pendencias/PendenciaVO;

.field final synthetic val$viewHolderLista:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderLista;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;Lcom/itau/empresas/feature/pendencias/PendenciaVO;Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderLista;)V
    .registers 4
    .param p1, "this$0"    # Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;

    .line 332
    iput-object p1, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$1;->this$0:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;

    iput-object p2, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$1;->val$pendenciasVO:Lcom/itau/empresas/feature/pendencias/PendenciaVO;

    iput-object p3, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$1;->val$viewHolderLista:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderLista;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 7
    .param p1, "v"    # Landroid/view/View;

    .line 335
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$1;->val$pendenciasVO:Lcom/itau/empresas/feature/pendencias/PendenciaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pendencias/PendenciaVO;->getCanalParaResolverPendencia()Ljava/lang/String;

    move-result-object v2

    .line 336
    .local v2, "canal":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$1;->val$pendenciasVO:Lcom/itau/empresas/feature/pendencias/PendenciaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pendencias/PendenciaVO;->getChaveMenuMobile()Ljava/lang/String;

    move-result-object v3

    .line 337
    .local v3, "chave":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$1;->val$viewHolderLista:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderLista;

    iget-object v0, v0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderLista;->parentView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 338
    .local v4, "context":Landroid/content/Context;
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$1;->this$0:Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;

    iget-object v1, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$1;->val$pendenciasVO:Lcom/itau/empresas/feature/pendencias/PendenciaVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/pendencias/PendenciaVO;->getDescricaoTarefa()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->disparaGA(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;->access$200(Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;Ljava/lang/String;)V

    .line 339
    const-string v0, "app_mobile"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 340
    .line 341
    invoke-static {v4}, Lcom/itau/empresas/ui/activity/WebViewActivity_;->intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/WebViewActivity_$IntentBuilder_;

    move-result-object v0

    .line 342
    invoke-virtual {v0, v3}, Lcom/itau/empresas/ui/activity/WebViewActivity_$IntentBuilder_;->chaveMobile(Ljava/lang/String;)Lcom/itau/empresas/ui/activity/WebViewActivity_$IntentBuilder_;

    move-result-object v0

    .line 343
    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/WebViewActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    goto :goto_42

    .line 344
    :cond_33
    const-string v0, "internet"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 345
    .line 346
    invoke-static {v4}, Lcom/itau/empresas/feature/pendencias/activity/PendenciasConsultarWebActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pendencias/activity/PendenciasConsultarWebActivity_$IntentBuilder_;

    move-result-object v0

    .line 347
    invoke-virtual {v0}, Lcom/itau/empresas/feature/pendencias/activity/PendenciasConsultarWebActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 349
    :cond_42
    :goto_42
    return-void
.end method

.class public Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;
.super Lcom/itau/empresas/ui/fragment/BaseFragment;
.source "AreaUsuarioSelecaoContaFragment.java"

# interfaces
.implements Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$OnClick;
.implements Lcom/itau/empresas/SearchViewDelay$SearchViewDelayCallback;


# instance fields
.field private adapter:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

.field campoBusca:Lcom/itau/empresas/ui/view/CustomSearchView;

.field private contaSelecionada:Lcom/itau/empresas/api/model/ContaOperadorVO;

.field private contasOperador:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/ContaOperadorVO;>;"
        }
    .end annotation
.end field

.field controller:Lcom/itau/empresas/controller/SessaoController;

.field mApplication:Lcom/itau/empresas/CustomApplication;

.field rvSelecaoConta:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 30
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;-><init>()V

    return-void
.end method

.method private montarAdapter()V
    .registers 6

    .line 78
    new-instance v0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

    invoke-direct {v0}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->adapter:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->adapter:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

    invoke-virtual {v0, p0}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->setClickListener(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$OnClick;)V

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->mApplication:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getPerfilLogado()Lcom/itau/empresas/api/model/PerfilOperadorVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->getContasOperador()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->contasOperador:Ljava/util/List;

    .line 83
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->removeContaAtual()V

    .line 85
    new-instance v4, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v4, v0, v1, v2}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 87
    .local v4, "layoutManager":Landroid/support/v7/widget/LinearLayoutManager;
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->rvSelecaoConta:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/itau/empresas/ui/util/DividerItemDecoration;

    .line 89
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0201a1

    invoke-static {v2, v3}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, v3, v2}, Lcom/itau/empresas/ui/util/DividerItemDecoration;-><init>(ILandroid/graphics/drawable/Drawable;)V

    .line 87
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 90
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->rvSelecaoConta:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 91
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->adapter:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->contasOperador:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->setListaDeContas(Ljava/util/List;)V

    .line 92
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->adapter:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->contaSelecionada:Lcom/itau/empresas/api/model/ContaOperadorVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->setContaSelecionada(Lcom/itau/empresas/api/model/ContaOperadorVO;)V

    .line 94
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->rvSelecaoConta:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->adapter:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 96
    return-void
.end method

.method private removeContaAtual()V
    .registers 3

    .line 105
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->mApplication:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->contaSelecionada:Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 106
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->contasOperador:Ljava/util/List;

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->contaSelecionada:Lcom/itau/empresas/api/model/ContaOperadorVO;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 107
    return-void
.end method


# virtual methods
.method public afterViews()V
    .registers 3

    .line 50
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->montarAdapter()V

    .line 51
    new-instance v1, Lcom/itau/empresas/SearchViewDelay;

    invoke-direct {v1, p0}, Lcom/itau/empresas/SearchViewDelay;-><init>(Lcom/itau/empresas/SearchViewDelay$SearchViewDelayCallback;)V

    .line 52
    .local v1, "searchViewDelay":Lcom/itau/empresas/SearchViewDelay;
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->campoBusca:Lcom/itau/empresas/ui/view/CustomSearchView;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/CustomSearchView;->setOnQueryTextListener(Landroid/support/v7/widget/SearchView$OnQueryTextListener;)V

    .line 53
    return-void
.end method

.method public aoClicarNaConta(Lcom/itau/empresas/api/model/ContaOperadorVO;)V
    .registers 4
    .param p1, "contaOperadorVO"    # Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 62
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->mostraDialogoDeProgresso()V

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->controller:Lcom/itau/empresas/controller/SessaoController;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/controller/SessaoController;->trocarConta(Lcom/itau/empresas/api/model/ContaOperadorVO;)V

    .line 64
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->campoBusca:Lcom/itau/empresas/ui/view/CustomSearchView;

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/TecladoUtils;->esconderTecladoDaView(Landroid/content/Context;Landroid/view/View;)V

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->adapter:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->setContaSelecionada(Lcom/itau/empresas/api/model/ContaOperadorVO;)V

    .line 66
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 57
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public publicarResultados(Ljava/lang/String;)V
    .registers 3
    .param p1, "text"    # Ljava/lang/String;

    .line 70
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->adapter:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->setTextoBuscado(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->adapter:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

    invoke-virtual {v0}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 72
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/TecladoUtils;->verificarTecladoAberto(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 73
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioSelecaoContaFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/TecladoUtils;->esconderTeclado(Landroid/app/Activity;)V

    .line 75
    :cond_1f
    return-void
.end method

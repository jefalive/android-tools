.class Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$8;
.super Ljava/lang/Object;
.source "TransferenciaRecebidasActivity.java"

# interfaces
.implements Lbr/com/itau/widgets/hintview/OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->adicionaHint(Landroid/view/MenuItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    .line 436
    iput-object p1, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$8;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss()V
    .registers 5

    .line 439
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$8;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    # getter for: Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->fechouHintComClique:Z
    invoke-static {v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->access$400(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 440
    return-void

    .line 442
    :cond_9
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$8;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    # getter for: Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->hintUtils:Lcom/itau/empresas/ui/util/HintUtils;
    invoke-static {v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->access$500(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)Lcom/itau/empresas/ui/util/HintUtils;

    move-result-object v0

    const-string v1, "CHVTRF-QTD"

    .line 443
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/HintUtils;->consultarPreferenciasQuantidade(Ljava/lang/String;)I

    move-result v3

    .line 445
    .local v3, "quantidade":I
    const/4 v0, 0x1

    if-le v3, v0, :cond_25

    .line 446
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$8;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    # getter for: Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->hintUtils:Lcom/itau/empresas/ui/util/HintUtils;
    invoke-static {v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->access$500(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)Lcom/itau/empresas/ui/util/HintUtils;

    move-result-object v0

    const-string v1, "CHVTRF-ID"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/HintUtils;->desativarHint(Ljava/lang/String;Z)V

    .line 448
    return-void

    .line 450
    :cond_25
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity$8;->this$0:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;

    # getter for: Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->hintUtils:Lcom/itau/empresas/ui/util/HintUtils;
    invoke-static {v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;->access$500(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasActivity;)Lcom/itau/empresas/ui/util/HintUtils;

    move-result-object v0

    const-string v1, "CHVTRF-QTD"

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v1, v3}, Lcom/itau/empresas/ui/util/HintUtils;->registraAcesso(Ljava/lang/String;I)V

    .line 452
    return-void
.end method

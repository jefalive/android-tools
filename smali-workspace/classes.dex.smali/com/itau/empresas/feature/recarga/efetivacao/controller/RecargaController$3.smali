.class Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$3;
.super Ljava/lang/Object;
.source "RecargaController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->simulaRecarga(Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

.field final synthetic val$idTelefone:Ljava/lang/String;

.field final synthetic val$recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)V
    .registers 4
    .param p1, "this$0"    # Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    .line 67
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$3;->this$0:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    iput-object p2, p0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$3;->val$idTelefone:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$3;->val$recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 4

    .line 70
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$3;->this$0:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    # invokes: Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->api()Ljava/lang/Object;
    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->access$600(Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$3;->val$idTelefone:Ljava/lang/String;

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController$3;->val$recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    invoke-interface {v0, v1, v2}, Lcom/itau/empresas/api/Api;->simulaRecarga(Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;

    move-result-object v0

    # invokes: Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->post(Ljava/lang/Object;)V
    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->access$700(Ljava/lang/Object;)V

    .line 71
    return-void
.end method

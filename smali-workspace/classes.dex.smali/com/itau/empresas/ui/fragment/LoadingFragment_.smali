.class public final Lcom/itau/empresas/ui/fragment/LoadingFragment_;
.super Lcom/itau/empresas/ui/fragment/LoadingFragment;
.source "LoadingFragment_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/fragment/LoadingFragment_$FragmentBuilder_;
    }
.end annotation


# instance fields
.field private contentView_:Landroid/view/View;

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 20
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/LoadingFragment;-><init>()V

    .line 24
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/LoadingFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method public static builder()Lcom/itau/empresas/ui/fragment/LoadingFragment_$FragmentBuilder_;
    .registers 1

    .line 69
    new-instance v0, Lcom/itau/empresas/ui/fragment/LoadingFragment_$FragmentBuilder_;

    invoke-direct {v0}, Lcom/itau/empresas/ui/fragment/LoadingFragment_$FragmentBuilder_;-><init>()V

    return-object v0
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 59
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/LoadingFragment_;->application:Lcom/itau/empresas/CustomApplication;

    .line 60
    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .registers 3
    .param p1, "id"    # I

    .line 37
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/LoadingFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_6

    .line 38
    const/4 v0, 0x0

    return-object v0

    .line 40
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/LoadingFragment_;->contentView_:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 29
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/LoadingFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 30
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/fragment/LoadingFragment_;->init_(Landroid/os/Bundle;)V

    .line 31
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/fragment/LoadingFragment;->onCreate(Landroid/os/Bundle;)V

    .line 32
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 33
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 45
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/fragment/LoadingFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/LoadingFragment_;->contentView_:Landroid/view/View;

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/LoadingFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_14

    .line 47
    const v0, 0x7f0300b6

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/LoadingFragment_;->contentView_:Landroid/view/View;

    .line 49
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/LoadingFragment_;->contentView_:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .registers 2

    .line 54
    invoke-super {p0}, Lcom/itau/empresas/ui/fragment/LoadingFragment;->onDestroyView()V

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/LoadingFragment_;->contentView_:Landroid/view/View;

    .line 56
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 64
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/ui/fragment/LoadingFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/LoadingFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 66
    return-void
.end method

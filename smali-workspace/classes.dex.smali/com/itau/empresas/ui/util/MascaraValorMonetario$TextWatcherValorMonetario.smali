.class Lcom/itau/empresas/ui/util/MascaraValorMonetario$TextWatcherValorMonetario;
.super Ljava/lang/Object;
.source "MascaraValorMonetario.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/util/MascaraValorMonetario;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TextWatcherValorMonetario"
.end annotation


# instance fields
.field ediTxt:Landroid/widget/EditText;

.field moeda:Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;

.field notificarAlteracao:Lcom/itau/empresas/ui/util/INotificarAlteracaoView;

.field simboloValorMonetarioRegex:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/widget/EditText;Ljava/lang/String;Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;Lcom/itau/empresas/ui/util/INotificarAlteracaoView;)V
    .registers 5
    .param p1, "ediTxt"    # Landroid/widget/EditText;
    .param p2, "simboloValorMonetarioRegex"    # Ljava/lang/String;
    .param p3, "moeda"    # Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;
    .param p4, "notificarAlteracao"    # Lcom/itau/empresas/ui/util/INotificarAlteracaoView;

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$TextWatcherValorMonetario;->ediTxt:Landroid/widget/EditText;

    .line 40
    iput-object p3, p0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$TextWatcherValorMonetario;->moeda:Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;

    .line 41
    iput-object p2, p0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$TextWatcherValorMonetario;->simboloValorMonetarioRegex:Ljava/lang/String;

    .line 42
    iput-object p4, p0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$TextWatcherValorMonetario;->notificarAlteracao:Lcom/itau/empresas/ui/util/INotificarAlteracaoView;

    .line 43
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 9
    .param p1, "editable"    # Landroid/text/Editable;

    .line 57
    iget-object v0, p0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$TextWatcherValorMonetario;->ediTxt:Landroid/widget/EditText;

    if-nez v0, :cond_5

    .line 58
    return-void

    .line 61
    :cond_5
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 62
    .local v4, "s":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$TextWatcherValorMonetario;->ediTxt:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 63
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 64
    return-void

    .line 66
    :cond_15
    const-string v0, ".0"

    invoke-virtual {v4, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 69
    :cond_30
    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$TextWatcherValorMonetario;->simboloValorMonetarioRegex:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 70
    .local v5, "cleanString":Ljava/lang/String;
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, v5}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x2

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Ljava/math/BigDecimal;->setScale(II)Ljava/math/BigDecimal;

    move-result-object v0

    new-instance v1, Ljava/math/BigDecimal;

    const/16 v2, 0x64

    invoke-direct {v1, v2}, Ljava/math/BigDecimal;-><init>(I)V

    .line 71
    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;I)Ljava/math/BigDecimal;

    move-result-object v6

    .line 72
    .local v6, "parsed":Ljava/math/BigDecimal;
    iget-object v0, p0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$TextWatcherValorMonetario;->moeda:Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;

    sget-object v1, Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;->REAL:Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6f

    .line 73
    new-instance v0, Ljava/util/Locale;

    const-string v1, "pt"

    const-string v2, "BR"

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Ljava/text/NumberFormat;->getCurrencyInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    .line 74
    invoke-virtual {v0, v6}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .local v3, "formatted":Ljava/lang/String;
    goto :goto_80

    .line 76
    .end local v3    # "formatted":Ljava/lang/String;
    :cond_6f
    new-instance v0, Ljava/util/Locale;

    const-string v1, "en"

    const-string v2, "US"

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Ljava/text/NumberFormat;->getCurrencyInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    .line 77
    invoke-virtual {v0, v6}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 79
    .local v3, "formatted":Ljava/lang/String;
    :goto_80
    iget-object v0, p0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$TextWatcherValorMonetario;->ediTxt:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$TextWatcherValorMonetario;->ediTxt:Landroid/widget/EditText;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$TextWatcherValorMonetario;->ediTxt:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 82
    iget-object v0, p0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$TextWatcherValorMonetario;->notificarAlteracao:Lcom/itau/empresas/ui/util/INotificarAlteracaoView;

    if-eqz v0, :cond_9e

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$TextWatcherValorMonetario;->notificarAlteracao:Lcom/itau/empresas/ui/util/INotificarAlteracaoView;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$TextWatcherValorMonetario;->ediTxt:Landroid/widget/EditText;

    invoke-interface {v0, v1}, Lcom/itau/empresas/ui/util/INotificarAlteracaoView;->NotificarAlteracao(Landroid/view/View;)V

    .line 85
    :cond_9e
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .line 53
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .line 48
    return-void
.end method

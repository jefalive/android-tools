.class Lcom/itau/empresas/feature/mapa/MapaListaFragment$2;
.super Ljava/lang/Object;
.source "MapaListaFragment.java"

# interfaces
.implements Landroid/support/v7/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/mapa/MapaListaFragment;->pesquisar()Landroid/support/v7/widget/SearchView$OnQueryTextListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/mapa/MapaListaFragment;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/mapa/MapaListaFragment;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/mapa/MapaListaFragment;

    .line 89
    iput-object p1, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment$2;->this$0:Lcom/itau/empresas/feature/mapa/MapaListaFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryTextChange(Ljava/lang/String;)Z
    .registers 3
    .param p1, "newText"    # Ljava/lang/String;

    .line 98
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment$2;->this$0:Lcom/itau/empresas/feature/mapa/MapaListaFragment;

    # getter for: Lcom/itau/empresas/feature/mapa/MapaListaFragment;->adapter:Lcom/itau/empresas/adapter/ListaAgenciasAdapter;
    invoke-static {v0}, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->access$000(Lcom/itau/empresas/feature/mapa/MapaListaFragment;)Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 99
    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .registers 3
    .param p1, "query"    # Ljava/lang/String;

    .line 92
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment$2;->this$0:Lcom/itau/empresas/feature/mapa/MapaListaFragment;

    # getter for: Lcom/itau/empresas/feature/mapa/MapaListaFragment;->adapter:Lcom/itau/empresas/adapter/ListaAgenciasAdapter;
    invoke-static {v0}, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->access$000(Lcom/itau/empresas/feature/mapa/MapaListaFragment;)Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 93
    const/4 v0, 0x0

    return v0
.end method

.class public Lnet/hockeyapp/android/utils/Base64;
.super Ljava/lang/Object;
.source "Base64.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/hockeyapp/android/utils/Base64$Encoder;,
        Lnet/hockeyapp/android/utils/Base64$Coder;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 30
    const-class v0, Lnet/hockeyapp/android/utils/Base64;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    sput-boolean v0, Lnet/hockeyapp/android/utils/Base64;->$assertionsDisabled:Z

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .line 743
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static encode([BI)[B
    .registers 4
    .param p0, "input"    # [B
    .param p1, "flags"    # I

    .line 496
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0, p1}, Lnet/hockeyapp/android/utils/Base64;->encode([BIII)[B

    move-result-object v0

    return-object v0
.end method

.method public static encode([BIII)[B
    .registers 8
    .param p0, "input"    # [B
    .param p1, "offset"    # I
    .param p2, "len"    # I
    .param p3, "flags"    # I

    .line 513
    new-instance v2, Lnet/hockeyapp/android/utils/Base64$Encoder;

    const/4 v0, 0x0

    invoke-direct {v2, p3, v0}, Lnet/hockeyapp/android/utils/Base64$Encoder;-><init>(I[B)V

    .line 516
    .local v2, "encoder":Lnet/hockeyapp/android/utils/Base64$Encoder;
    div-int/lit8 v0, p2, 0x3

    mul-int/lit8 v3, v0, 0x4

    .line 519
    .local v3, "output_len":I
    iget-boolean v0, v2, Lnet/hockeyapp/android/utils/Base64$Encoder;->do_padding:Z

    if-eqz v0, :cond_15

    .line 520
    rem-int/lit8 v0, p2, 0x3

    if-lez v0, :cond_21

    .line 521
    add-int/lit8 v3, v3, 0x4

    goto :goto_21

    .line 524
    :cond_15
    rem-int/lit8 v0, p2, 0x3

    packed-switch v0, :pswitch_data_50

    goto :goto_21

    .line 525
    :pswitch_1b
    goto :goto_21

    .line 526
    :pswitch_1c
    add-int/lit8 v3, v3, 0x2

    goto :goto_21

    .line 527
    :pswitch_1f
    add-int/lit8 v3, v3, 0x3

    .line 532
    :cond_21
    :goto_21
    iget-boolean v0, v2, Lnet/hockeyapp/android/utils/Base64$Encoder;->do_newline:Z

    if-eqz v0, :cond_36

    if-lez p2, :cond_36

    .line 533
    add-int/lit8 v0, p2, -0x1

    div-int/lit8 v0, v0, 0x39

    add-int/lit8 v0, v0, 0x1

    iget-boolean v1, v2, Lnet/hockeyapp/android/utils/Base64$Encoder;->do_cr:Z

    if-eqz v1, :cond_33

    const/4 v1, 0x2

    goto :goto_34

    :cond_33
    const/4 v1, 0x1

    :goto_34
    mul-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 537
    :cond_36
    new-array v0, v3, [B

    iput-object v0, v2, Lnet/hockeyapp/android/utils/Base64$Encoder;->output:[B

    .line 538
    const/4 v0, 0x1

    invoke-virtual {v2, p0, p1, p2, v0}, Lnet/hockeyapp/android/utils/Base64$Encoder;->process([BIIZ)Z

    .line 540
    sget-boolean v0, Lnet/hockeyapp/android/utils/Base64;->$assertionsDisabled:Z

    if-nez v0, :cond_4c

    iget v0, v2, Lnet/hockeyapp/android/utils/Base64$Encoder;->op:I

    if-eq v0, v3, :cond_4c

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 542
    :cond_4c
    iget-object v0, v2, Lnet/hockeyapp/android/utils/Base64$Encoder;->output:[B

    return-object v0

    nop

    :pswitch_data_50
    .packed-switch 0x0
        :pswitch_1b
        :pswitch_1c
        :pswitch_1f
    .end packed-switch
.end method

.method public static encodeToString([BI)Ljava/lang/String;
    .registers 6
    .param p0, "input"    # [B
    .param p1, "flags"    # I

    .line 456
    :try_start_0
    new-instance v0, Ljava/lang/String;

    invoke-static {p0, p1}, Lnet/hockeyapp/android/utils/Base64;->encode([BI)[B

    move-result-object v1

    const-string v2, "US-ASCII"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_b
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_b} :catch_c

    return-object v0

    .line 457
    :catch_c
    move-exception v3

    .line 459
    .local v3, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

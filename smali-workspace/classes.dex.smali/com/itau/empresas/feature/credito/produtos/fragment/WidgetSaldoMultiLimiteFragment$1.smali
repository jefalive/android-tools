.class Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$1;
.super Ljava/lang/Object;
.source "WidgetSaldoMultiLimiteFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->onEventMainThread(Lcom/itau/empresas/api/model/SaldoVO;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;

    .line 199
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$1;->this$0:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 6
    .param p1, "v"    # Landroid/view/View;

    .line 203
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$1;->this$0:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;

    # getter for: Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->access$000(Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;->MOSTRA_SALDO:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 204
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$1;->this$0:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$1;->this$0:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;

    const v2, 0x7f0702d4

    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$1;->this$0:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;

    const v3, 0x7f070319

    invoke-virtual {v2, v3}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$1;->this$0:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->fecharWidget()V

    .line 206
    return-void
.end method

.class Lcom/itau/empresas/feature/credito/lis/LisController$3;
.super Ljava/lang/Object;
.source "LisController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/credito/lis/LisController;->efetuaSimulacao(Ljava/lang/String;Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/credito/lis/LisController;

.field final synthetic val$envioVO:Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;

.field final synthetic val$produtoLis:Ljava/lang/String;

.field final synthetic val$tipoOperacao:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/credito/lis/LisController;Ljava/lang/String;Ljava/lang/String;Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;)V
    .registers 5
    .param p1, "this$0"    # Lcom/itau/empresas/feature/credito/lis/LisController;

    .line 62
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/lis/LisController$3;->this$0:Lcom/itau/empresas/feature/credito/lis/LisController;

    iput-object p2, p0, Lcom/itau/empresas/feature/credito/lis/LisController$3;->val$produtoLis:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/feature/credito/lis/LisController$3;->val$tipoOperacao:Ljava/lang/String;

    iput-object p4, p0, Lcom/itau/empresas/feature/credito/lis/LisController$3;->val$envioVO:Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 5

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/LisController$3;->this$0:Lcom/itau/empresas/feature/credito/lis/LisController;

    # invokes: Lcom/itau/empresas/feature/credito/lis/LisController;->api()Ljava/lang/Object;
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/lis/LisController;->access$400(Lcom/itau/empresas/feature/credito/lis/LisController;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/LisController$3;->val$produtoLis:Ljava/lang/String;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/LisController$3;->val$tipoOperacao:Ljava/lang/String;

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/LisController$3;->val$envioVO:Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;

    .line 66
    invoke-interface {v0, v1, v2, v3}, Lcom/itau/empresas/api/Api;->efetuaSimulacaoLis(Ljava/lang/String;Ljava/lang/String;Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;)Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;

    move-result-object v0

    .line 65
    # invokes: Lcom/itau/empresas/feature/credito/lis/LisController;->post(Ljava/lang/Object;)V
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/lis/LisController;->access$500(Ljava/lang/Object;)V

    .line 67
    return-void
.end method

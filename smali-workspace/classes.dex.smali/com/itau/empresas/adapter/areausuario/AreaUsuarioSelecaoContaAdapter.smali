.class public Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "AreaUsuarioSelecaoContaAdapter.java"

# interfaces
.implements Landroid/widget/Filterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContasFilter;,
        Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$EmptyStateViewHolder;,
        Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;,
        Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$OnClick;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter<Landroid/support/v7/widget/RecyclerView$ViewHolder;>;Landroid/widget/Filterable;"
    }
.end annotation


# instance fields
.field private contaSelecionada:Lcom/itau/empresas/api/model/ContaOperadorVO;

.field private context:Landroid/content/Context;

.field private filter:Landroid/widget/Filter;

.field private listaContas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/ContaOperadorVO;>;"
        }
    .end annotation
.end field

.field private onClickListener:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$OnClick;

.field private textoBuscado:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 26
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    return-void
.end method

.method static synthetic access$600(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;)Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$OnClick;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

    .line 26
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->onClickListener:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$OnClick;

    return-object v0
.end method

.method static synthetic access$700(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;)Ljava/util/List;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

    .line 26
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->listaContas:Ljava/util/List;

    return-object v0
.end method

.method private configuraContaViewHolder(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;I)V
    .registers 9
    .param p1, "holder"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;
    .param p2, "position"    # I

    .line 136
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->listaContas:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 137
    .local v5, "conta":Lcom/itau/empresas/api/model/ContaOperadorVO;
    if-eqz v5, :cond_99

    .line 138
    # getter for: Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->textoNome:Landroid/widget/TextView;
    invoke-static {p1}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->access$200(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v5}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getApelido()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    # getter for: Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->textoAgencia:Landroid/widget/TextView;
    invoke-static {p1}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->access$300(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->context:Landroid/content/Context;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    .line 140
    invoke-virtual {v5}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const v3, 0x7f07058d

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 139
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    # getter for: Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->textoConta:Landroid/widget/TextView;
    invoke-static {p1}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->access$400(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->context:Landroid/content/Context;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 142
    invoke-virtual {v5}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 143
    invoke-virtual {v5}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 142
    const v3, 0x7f07058e

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 141
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    # getter for: Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->rlItemListaContas:Landroid/widget/RelativeLayout;
    invoke-static {p1}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->access$500(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;)Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->context:Landroid/content/Context;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    .line 147
    invoke-virtual {v5}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getApelido()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v5}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v5}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v2, v4

    .line 148
    invoke-virtual {v5}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    aput-object v3, v2, v4

    .line 146
    const v3, 0x7f0700b0

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 145
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 150
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->contaSelecionada:Lcom/itau/empresas/api/model/ContaOperadorVO;

    invoke-virtual {v5, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_91

    .line 151
    iget-object v0, p1, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0c0052

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_99

    .line 153
    :cond_91
    iget-object v0, p1, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->itemView:Landroid/view/View;

    const v1, 0x7f0c015d

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 156
    :cond_99
    :goto_99
    return-void
.end method

.method private configuraEmptyViewHolder(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$EmptyStateViewHolder;)V
    .registers 7
    .param p1, "holder"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$EmptyStateViewHolder;

    .line 130
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->context:Landroid/content/Context;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->textoBuscado:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->textoBuscado:Ljava/lang/String;

    goto :goto_e

    :cond_c
    const-string v2, ""

    :goto_e
    const/4 v3, 0x0

    aput-object v2, v1, v3

    const v2, 0x7f07011e

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 132
    .local v4, "msg":Ljava/lang/String;
    # getter for: Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$EmptyStateViewHolder;->mensagemEmptyState:Landroid/widget/TextView;
    invoke-static {p1}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$EmptyStateViewHolder;->access$100(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$EmptyStateViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    return-void
.end method

.method private ordenar()V
    .registers 3

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->listaContas:Ljava/util/List;

    new-instance v1, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$1;-><init>(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 74
    return-void
.end method


# virtual methods
.method public getFilter()Landroid/widget/Filter;
    .registers 3

    .line 106
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->filter:Landroid/widget/Filter;

    if-nez v0, :cond_d

    .line 107
    new-instance v0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContasFilter;

    iget-object v1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->listaContas:Ljava/util/List;

    invoke-direct {v0, p0, p0, v1}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContasFilter;-><init>(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;Ljava/util/List;)V

    iput-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->filter:Landroid/widget/Filter;

    .line 109
    :cond_d
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->filter:Landroid/widget/Filter;

    return-object v0
.end method

.method public getItemCount()I
    .registers 3

    .line 114
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->listaContas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 115
    .local v1, "size":I
    if-lez v1, :cond_9

    .line 116
    return v1

    .line 117
    :cond_9
    const/4 v0, 0x1

    return v0
.end method

.method public getItemViewType(I)I
    .registers 3
    .param p1, "position"    # I

    .line 122
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->listaContas:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 123
    const/4 v0, 0x0

    return v0

    .line 125
    :cond_a
    const/4 v0, 0x1

    return v0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .registers 6
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .line 95
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_e

    .line 96
    move-object v2, p1

    check-cast v2, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;

    .line 97
    .local v2, "contaViewHolder":Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;
    invoke-direct {p0, v2, p2}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->configuraContaViewHolder(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;I)V

    .line 98
    .end local v2    # "contaViewHolder":Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;
    goto :goto_1a

    :cond_e
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v0

    if-nez v0, :cond_1a

    .line 99
    move-object v2, p1

    check-cast v2, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$EmptyStateViewHolder;

    .line 100
    .local v2, "emptyStateViewHolder":Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$EmptyStateViewHolder;
    invoke-direct {p0, v2}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->configuraEmptyViewHolder(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$EmptyStateViewHolder;)V

    .line 102
    .end local v2    # "emptyStateViewHolder":Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$EmptyStateViewHolder;
    :cond_1a
    :goto_1a
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .registers 8
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .line 78
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->context:Landroid/content/Context;

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->context:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 80
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const/4 v3, 0x0

    .line 83
    .local v3, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    const/4 v0, 0x1

    if-ne p2, v0, :cond_1f

    .line 84
    const v0, 0x7f0300f2

    const/4 v1, 0x0

    invoke-virtual {v2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 85
    .local v4, "view":Landroid/view/View;
    new-instance v3, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;

    const/4 v0, 0x0

    invoke-direct {v3, p0, v4, v0}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;-><init>(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;Landroid/view/View;Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$1;)V

    goto :goto_2e

    .line 86
    .end local v4    # "view":Landroid/view/View;
    :cond_1f
    if-nez p2, :cond_2e

    .line 87
    const v0, 0x7f03008c

    const/4 v1, 0x0

    invoke-virtual {v2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 88
    .local v4, "view":Landroid/view/View;
    new-instance v3, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$EmptyStateViewHolder;

    invoke-direct {v3, p0, v4}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$EmptyStateViewHolder;-><init>(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;Landroid/view/View;)V

    .line 90
    .end local v4    # "view":Landroid/view/View;
    :cond_2e
    :goto_2e
    return-object v3
.end method

.method public setClickListener(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$OnClick;)V
    .registers 2
    .param p1, "listener"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$OnClick;

    .line 48
    iput-object p1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->onClickListener:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$OnClick;

    .line 49
    return-void
.end method

.method public setContaSelecionada(Lcom/itau/empresas/api/model/ContaOperadorVO;)V
    .registers 2
    .param p1, "conta"    # Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 58
    iput-object p1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->contaSelecionada:Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 59
    invoke-virtual {p0}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->notifyDataSetChanged()V

    .line 60
    return-void
.end method

.method public setListaDeContas(Ljava/util/List;)V
    .registers 2
    .param p1, "contas"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/ContaOperadorVO;>;)V"
        }
    .end annotation

    .line 52
    iput-object p1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->listaContas:Ljava/util/List;

    .line 53
    invoke-direct {p0}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->ordenar()V

    .line 54
    invoke-virtual {p0}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->notifyDataSetChanged()V

    .line 55
    return-void
.end method

.method public setTextoBuscado(Ljava/lang/String;)V
    .registers 2
    .param p1, "texto"    # Ljava/lang/String;

    .line 44
    iput-object p1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->textoBuscado:Ljava/lang/String;

    .line 45
    return-void
.end method

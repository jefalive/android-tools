.class public abstract Lcom/google/android/gms/dynamic/zzg;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/dynamic/zzg$zza;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:Ljava/lang/Object;>Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final zzavI:Ljava/lang/String;

.field private zzavJ:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Ljava/lang/String;)V
    .registers 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/dynamic/zzg;->zzavI:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final zzaB(Landroid/content/Context;)Ljava/lang/Object;
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/dynamic/zzg$zza;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzg;->zzavJ:Ljava/lang/Object;

    if-nez v0, :cond_48

    invoke-static {p1}, Lcom/google/android/gms/common/internal/zzx;->zzz(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/gms/common/zze;->getRemoteContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v2

    if-nez v2, :cond_15

    new-instance v0, Lcom/google/android/gms/dynamic/zzg$zza;

    const-string v1, "Could not get remote context."

    invoke-direct {v0, v1}, Lcom/google/android/gms/dynamic/zzg$zza;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_15
    invoke-virtual {v2}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    :try_start_19
    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzg;->zzavI:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/os/IBinder;

    invoke-virtual {p0, v5}, Lcom/google/android/gms/dynamic/zzg;->zzd(Landroid/os/IBinder;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/dynamic/zzg;->zzavJ:Ljava/lang/Object;
    :try_end_2c
    .catch Ljava/lang/ClassNotFoundException; {:try_start_19 .. :try_end_2c} :catch_2d
    .catch Ljava/lang/InstantiationException; {:try_start_19 .. :try_end_2c} :catch_36
    .catch Ljava/lang/IllegalAccessException; {:try_start_19 .. :try_end_2c} :catch_3f

    goto :goto_48

    :catch_2d
    move-exception v4

    new-instance v0, Lcom/google/android/gms/dynamic/zzg$zza;

    const-string v1, "Could not load creator class."

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/dynamic/zzg$zza;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :catch_36
    move-exception v4

    new-instance v0, Lcom/google/android/gms/dynamic/zzg$zza;

    const-string v1, "Could not instantiate creator."

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/dynamic/zzg$zza;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :catch_3f
    move-exception v4

    new-instance v0, Lcom/google/android/gms/dynamic/zzg$zza;

    const-string v1, "Could not access creator."

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/dynamic/zzg$zza;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_48
    :goto_48
    iget-object v0, p0, Lcom/google/android/gms/dynamic/zzg;->zzavJ:Ljava/lang/Object;

    return-object v0
.end method

.method protected abstract zzd(Landroid/os/IBinder;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/os/IBinder;)TT;"
        }
    .end annotation
.end method

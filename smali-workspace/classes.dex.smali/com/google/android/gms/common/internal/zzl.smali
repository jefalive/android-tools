.class public abstract Lcom/google/android/gms/common/internal/zzl;
.super Ljava/lang/Object;


# static fields
.field private static final zzalX:Ljava/lang/Object;

.field private static zzalY:Lcom/google/android/gms/common/internal/zzl;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/internal/zzl;->zzalX:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static zzau(Landroid/content/Context;)Lcom/google/android/gms/common/internal/zzl;
    .registers 5

    sget-object v2, Lcom/google/android/gms/common/internal/zzl;->zzalX:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    sget-object v0, Lcom/google/android/gms/common/internal/zzl;->zzalY:Lcom/google/android/gms/common/internal/zzl;

    if-nez v0, :cond_12

    new-instance v0, Lcom/google/android/gms/common/internal/zzm;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/internal/zzm;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/common/internal/zzl;->zzalY:Lcom/google/android/gms/common/internal/zzl;
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_14

    :cond_12
    monitor-exit v2

    goto :goto_17

    :catchall_14
    move-exception v3

    monitor-exit v2

    throw v3

    :goto_17
    sget-object v0, Lcom/google/android/gms/common/internal/zzl;->zzalY:Lcom/google/android/gms/common/internal/zzl;

    return-object v0
.end method


# virtual methods
.method public abstract zza(Ljava/lang/String;Landroid/content/ServiceConnection;Ljava/lang/String;)Z
.end method

.method public abstract zzb(Ljava/lang/String;Landroid/content/ServiceConnection;Ljava/lang/String;)V
.end method

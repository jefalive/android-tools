.class public Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;
.super Ljava/lang/Object;
.source "FrancesinhaVO.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/api/model/FrancesinhaVO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Periodo"
.end annotation


# instance fields
.field private primeiroPeriodo:Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "primeiro_periodo"
    .end annotation
.end field

.field private quartoPeriodo:Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quarto_periodo"
    .end annotation
.end field

.field private segundoPeriodo:Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "segundo_periodo"
    .end annotation
.end field

.field private terceiroPeriodo:Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "terceiro_periodo"
    .end annotation
.end field


# virtual methods
.method public getPrimeiroPeriodo()Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;
    .registers 2

    .line 97
    iget-object v0, p0, Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;->primeiroPeriodo:Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;

    return-object v0
.end method

.method public getQuartoPeriodo()Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;
    .registers 2

    .line 110
    iget-object v0, p0, Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;->quartoPeriodo:Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;

    return-object v0
.end method

.method public getSegundoPeriodo()Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;
    .registers 2

    .line 101
    iget-object v0, p0, Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;->segundoPeriodo:Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;

    return-object v0
.end method

.method public getTerceiroPeriodo()Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;
    .registers 2

    .line 106
    iget-object v0, p0, Lcom/itau/empresas/api/model/FrancesinhaVO$Periodo;->terceiroPeriodo:Lcom/itau/empresas/api/model/FrancesinhaVO$DadosPeriodo;

    return-object v0
.end method

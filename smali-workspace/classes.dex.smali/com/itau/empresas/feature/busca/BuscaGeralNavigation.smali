.class public Lcom/itau/empresas/feature/busca/BuscaGeralNavigation;
.super Ljava/lang/Object;
.source "BuscaGeralNavigation.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static abreLink(Landroid/support/v7/app/AppCompatActivity;Ljava/lang/String;)V
    .registers 6
    .param p0, "activity"    # Landroid/support/v7/app/AppCompatActivity;
    .param p1, "chaveMobile"    # Ljava/lang/String;

    .line 30
    new-instance v1, Lcom/itau/empresas/api/model/MenuVO;

    invoke-direct {v1}, Lcom/itau/empresas/api/model/MenuVO;-><init>()V

    .line 31
    .local v1, "item":Lcom/itau/empresas/api/model/MenuVO;
    invoke-virtual {v1, p1}, Lcom/itau/empresas/api/model/MenuVO;->setChaveMobile(Ljava/lang/String;)V

    .line 32
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->getInstance(Ljava/lang/String;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    move-result-object v2

    .line 33
    .local v2, "informacaoMenu":Lcom/itau/empresas/ui/util/menu/InformacaoMenu;
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->getCommand()Lcom/itau/empresas/ui/util/menu/Command;

    move-result-object v3

    .line 34
    .local v3, "command":Lcom/itau/empresas/ui/util/menu/Command;
    if-eqz v3, :cond_19

    .line 35
    invoke-interface {v3, p0, v1}, Lcom/itau/empresas/ui/util/menu/Command;->executar(Landroid/support/v7/app/AppCompatActivity;Lcom/itau/empresas/api/model/MenuVO;)V

    .line 37
    :cond_19
    return-void
.end method

.method public static irParaAtendimento(Landroid/support/v7/app/AppCompatActivity;)V
    .registers 2
    .param p0, "activity"    # Landroid/support/v7/app/AppCompatActivity;

    .line 26
    const-string v0, "tabbar_ajuda"

    invoke-static {p0, v0}, Lcom/itau/empresas/feature/busca/BuscaGeralNavigation;->abreLink(Landroid/support/v7/app/AppCompatActivity;Ljava/lang/String;)V

    .line 27
    return-void
.end method

.method public static irParaContaCorrente(Landroid/support/v7/app/AppCompatActivity;)V
    .registers 2
    .param p0, "activity"    # Landroid/support/v7/app/AppCompatActivity;

    .line 22
    const-string v0, "tabbar_extrato"

    invoke-static {p0, v0}, Lcom/itau/empresas/feature/busca/BuscaGeralNavigation;->abreLink(Landroid/support/v7/app/AppCompatActivity;Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method public static irParaFaqs(Lbr/com/itau/textovoz/model/Faq;Landroid/content/Context;)V
    .registers 10
    .param p0, "faq"    # Lbr/com/itau/textovoz/model/Faq;
    .param p1, "context"    # Landroid/content/Context;

    .line 41
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 42
    .local v3, "channels":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 44
    .local v4, "links":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;>;"
    invoke-virtual {p0}, Lbr/com/itau/textovoz/model/Faq;->getChannels()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_54

    invoke-virtual {p0}, Lbr/com/itau/textovoz/model/Faq;->getChannels()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_54

    .line 45
    invoke-virtual {p0}, Lbr/com/itau/textovoz/model/Faq;->getChannels()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_22
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_54

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lbr/com/itau/textovoz/model/Channel;

    .line 46
    .local v6, "channel":Lbr/com/itau/textovoz/model/Channel;
    new-instance v7, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;

    invoke-direct {v7}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;-><init>()V

    .line 47
    .local v7, "ch":Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;
    invoke-virtual {v6}, Lbr/com/itau/textovoz/model/Channel;->getChannel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;->setCanal(Ljava/lang/String;)V

    .line 48
    invoke-virtual {v6}, Lbr/com/itau/textovoz/model/Channel;->getExplanation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;->setTextoExplicativo(Ljava/lang/String;)V

    .line 49
    invoke-virtual {v6}, Lbr/com/itau/textovoz/model/Channel;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;->setUrl(Ljava/lang/String;)V

    .line 50
    invoke-virtual {v6}, Lbr/com/itau/textovoz/model/Channel;->getIcon()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;->setIcone(Ljava/lang/String;)V

    .line 51
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    .end local v6    # "channel":Lbr/com/itau/textovoz/model/Channel;
    .end local v7    # "ch":Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;
    goto :goto_22

    .line 55
    :cond_54
    invoke-virtual {p0}, Lbr/com/itau/textovoz/model/Faq;->getLinks()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_90

    invoke-virtual {p0}, Lbr/com/itau/textovoz/model/Faq;->getLinks()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_90

    .line 56
    invoke-virtual {p0}, Lbr/com/itau/textovoz/model/Faq;->getLinks()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_6c
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_90

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lbr/com/itau/textovoz/model/Link;

    .line 57
    .local v6, "link":Lbr/com/itau/textovoz/model/Link;
    new-instance v7, Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;

    invoke-direct {v7}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;-><init>()V

    .line 58
    .local v7, "ch":Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;
    invoke-virtual {v6}, Lbr/com/itau/textovoz/model/Link;->get_ref()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;->setRef(Ljava/lang/String;)V

    .line 59
    invoke-virtual {v6}, Lbr/com/itau/textovoz/model/Link;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;->setTexto(Ljava/lang/String;)V

    .line 60
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    .end local v6    # "link":Lbr/com/itau/textovoz/model/Link;
    .end local v7    # "ch":Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;
    goto :goto_6c

    .line 64
    :cond_90
    invoke-static {p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;

    move-result-object v0

    .line 65
    const v1, 0x7f070709

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lbr/com/itau/textovoz/model/Faq;->getQuestion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;->extra(Ljava/lang/String;Ljava/lang/String;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;

    .line 66
    const v1, 0x7f070714

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lbr/com/itau/textovoz/model/Faq;->getAnswer()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;->extra(Ljava/lang/String;Ljava/lang/String;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;

    .line 67
    const v1, 0x7f07070a

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;->extra(Ljava/lang/String;Ljava/io/Serializable;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;

    .line 68
    const v1, 0x7f0706ff

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;->extra(Ljava/lang/String;Ljava/io/Serializable;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;

    .line 69
    invoke-virtual {v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 70
    return-void
.end method

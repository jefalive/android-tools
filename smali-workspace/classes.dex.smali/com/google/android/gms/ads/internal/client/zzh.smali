.class public Lcom/google/android/gms/ads/internal/client/zzh;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# static fields
.field public static final zzug:Lcom/google/android/gms/ads/internal/client/zzh;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    new-instance v0, Lcom/google/android/gms/ads/internal/client/zzh;

    invoke-direct {v0}, Lcom/google/android/gms/ads/internal/client/zzh;-><init>()V

    sput-object v0, Lcom/google/android/gms/ads/internal/client/zzh;->zzug:Lcom/google/android/gms/ads/internal/client/zzh;

    return-void
.end method

.method protected constructor <init>()V
    .registers 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static zzcO()Lcom/google/android/gms/ads/internal/client/zzh;
    .registers 1

    sget-object v0, Lcom/google/android/gms/ads/internal/client/zzh;->zzug:Lcom/google/android/gms/ads/internal/client/zzh;

    return-object v0
.end method


# virtual methods
.method public zza(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/zzaa;)Lcom/google/android/gms/ads/internal/client/AdRequestParcel;
    .registers 42

    const/16 v20, 0x7

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/ads/internal/client/zzaa;->getBirthday()Ljava/util/Date;

    move-result-object v21

    if-eqz v21, :cond_d

    invoke-virtual/range {v21 .. v21}, Ljava/util/Date;->getTime()J

    move-result-wide v22

    goto :goto_f

    :cond_d
    const-wide/16 v22, -0x1

    :goto_f
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/ads/internal/client/zzaa;->getContentUrl()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/ads/internal/client/zzaa;->getGender()I

    move-result v25

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/ads/internal/client/zzaa;->getKeywords()Ljava/util/Set;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2d

    new-instance v0, Ljava/util/ArrayList;

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v27

    goto :goto_2f

    :cond_2d
    const/16 v27, 0x0

    :goto_2f
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/client/zzaa;->isTestDevice(Landroid/content/Context;)Z

    move-result v28

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/ads/internal/client/zzaa;->zzdd()I

    move-result v29

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/ads/internal/client/zzaa;->getLocation()Landroid/location/Location;

    move-result-object v30

    const-class v0, Lcom/google/ads/mediation/admob/AdMobAdapter;

    move-object/from16 v1, p2

    invoke-virtual {v1, v0}, Lcom/google/android/gms/ads/internal/client/zzaa;->getNetworkExtrasBundle(Ljava/lang/Class;)Landroid/os/Bundle;

    move-result-object v31

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/ads/internal/client/zzaa;->getManualImpressionsEnabled()Z

    move-result v32

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/ads/internal/client/zzaa;->getPublisherProvidedId()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/ads/internal/client/zzaa;->zzda()Lcom/google/android/gms/ads/search/SearchAdRequest;

    move-result-object v34

    if-eqz v34, :cond_5f

    new-instance v35, Lcom/google/android/gms/ads/internal/client/SearchAdRequestParcel;

    move-object/from16 v0, v35

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/internal/client/SearchAdRequestParcel;-><init>(Lcom/google/android/gms/ads/search/SearchAdRequest;)V

    goto :goto_61

    :cond_5f
    const/16 v35, 0x0

    :goto_61
    const/16 v36, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v37

    if-eqz v37, :cond_7f

    invoke-virtual/range {v37 .. v37}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v38

    invoke-static {}, Lcom/google/android/gms/ads/internal/client/zzn;->zzcS()Lcom/google/android/gms/ads/internal/util/client/zza;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    move-object/from16 v2, v38

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/ads/internal/util/client/zza;->zza([Ljava/lang/StackTraceElement;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    :cond_7f
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/ads/internal/client/zzaa;->isDesignedForFamilies()Z

    move-result v38

    new-instance v0, Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/ads/internal/client/zzaa;->zzdc()Landroid/os/Bundle;

    move-result-object v14

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/ads/internal/client/zzaa;->getCustomTargeting()Landroid/os/Bundle;

    move-result-object v15

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/ads/internal/client/zzaa;->zzde()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v16

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/ads/internal/client/zzaa;->zzcZ()Ljava/lang/String;

    move-result-object v17

    move/from16 v1, v20

    move-wide/from16 v2, v22

    move-object/from16 v4, v31

    move/from16 v5, v25

    move-object/from16 v6, v27

    move/from16 v7, v28

    move/from16 v8, v29

    move/from16 v9, v32

    move-object/from16 v10, v33

    move-object/from16 v11, v35

    move-object/from16 v12, v30

    move-object/from16 v13, v24

    move-object/from16 v18, v36

    move/from16 v19, v38

    invoke-direct/range {v0 .. v19}, Lcom/google/android/gms/ads/internal/client/AdRequestParcel;-><init>(IJLandroid/os/Bundle;ILjava/util/List;ZIZLjava/lang/String;Lcom/google/android/gms/ads/internal/client/SearchAdRequestParcel;Landroid/location/Location;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

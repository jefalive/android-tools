.class Lcom/google/android/gms/internal/zzer$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/internal/zzer;->zza(JJ)Lcom/google/android/gms/internal/zzes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic zzCl:Lcom/google/android/gms/internal/zzeq;

.field final synthetic zzCm:Lcom/google/android/gms/internal/zzer;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/zzer;Lcom/google/android/gms/internal/zzeq;)V
    .registers 3

    iput-object p1, p0, Lcom/google/android/gms/internal/zzer$1;->zzCm:Lcom/google/android/gms/internal/zzer;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzer$1;->zzCl:Lcom/google/android/gms/internal/zzeq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    iget-object v0, p0, Lcom/google/android/gms/internal/zzer$1;->zzCm:Lcom/google/android/gms/internal/zzer;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzer;->zza(Lcom/google/android/gms/internal/zzer;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    :try_start_7
    iget-object v0, p0, Lcom/google/android/gms/internal/zzer$1;->zzCm:Lcom/google/android/gms/internal/zzer;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzer;->zzb(Lcom/google/android/gms/internal/zzer;)I
    :try_end_c
    .catchall {:try_start_7 .. :try_end_c} :catchall_7e

    move-result v0

    const/4 v1, -0x2

    if-eq v0, v1, :cond_12

    monitor-exit v2

    return-void

    :cond_12
    :try_start_12
    iget-object v0, p0, Lcom/google/android/gms/internal/zzer$1;->zzCm:Lcom/google/android/gms/internal/zzer;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzer$1;->zzCm:Lcom/google/android/gms/internal/zzer;

    invoke-static {v1}, Lcom/google/android/gms/internal/zzer;->zzc(Lcom/google/android/gms/internal/zzer;)Lcom/google/android/gms/internal/zzey;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzer;->zza(Lcom/google/android/gms/internal/zzer;Lcom/google/android/gms/internal/zzey;)Lcom/google/android/gms/internal/zzey;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzer$1;->zzCm:Lcom/google/android/gms/internal/zzer;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzer;->zzd(Lcom/google/android/gms/internal/zzer;)Lcom/google/android/gms/internal/zzey;

    move-result-object v0

    if-nez v0, :cond_2d

    iget-object v0, p0, Lcom/google/android/gms/internal/zzer$1;->zzCm:Lcom/google/android/gms/internal/zzer;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzer;->zzr(I)V
    :try_end_2b
    .catchall {:try_start_12 .. :try_end_2b} :catchall_7e

    monitor-exit v2

    return-void

    :cond_2d
    :try_start_2d
    iget-object v0, p0, Lcom/google/android/gms/internal/zzer$1;->zzCm:Lcom/google/android/gms/internal/zzer;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzer;->zze(Lcom/google/android/gms/internal/zzer;)Z

    move-result v0

    if-eqz v0, :cond_6e

    iget-object v0, p0, Lcom/google/android/gms/internal/zzer$1;->zzCm:Lcom/google/android/gms/internal/zzer;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzer;->zza(Lcom/google/android/gms/internal/zzer;I)Z

    move-result v0

    if-nez v0, :cond_6e

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Ignoring adapter "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzer$1;->zzCm:Lcom/google/android/gms/internal/zzer;

    invoke-static {v1}, Lcom/google/android/gms/internal/zzer;->zzf(Lcom/google/android/gms/internal/zzer;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " as delayed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " impression is not supported"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzer$1;->zzCm:Lcom/google/android/gms/internal/zzer;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzer;->zzr(I)V
    :try_end_6c
    .catchall {:try_start_2d .. :try_end_6c} :catchall_7e

    monitor-exit v2

    return-void

    :cond_6e
    :try_start_6e
    iget-object v0, p0, Lcom/google/android/gms/internal/zzer$1;->zzCl:Lcom/google/android/gms/internal/zzeq;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzer$1;->zzCm:Lcom/google/android/gms/internal/zzer;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzeq;->zza(Lcom/google/android/gms/internal/zzes$zza;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzer$1;->zzCm:Lcom/google/android/gms/internal/zzer;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzer$1;->zzCl:Lcom/google/android/gms/internal/zzeq;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzer;->zza(Lcom/google/android/gms/internal/zzer;Lcom/google/android/gms/internal/zzeq;)V
    :try_end_7c
    .catchall {:try_start_6e .. :try_end_7c} :catchall_7e

    monitor-exit v2

    goto :goto_81

    :catchall_7e
    move-exception v3

    monitor-exit v2

    throw v3

    :goto_81
    return-void
.end method

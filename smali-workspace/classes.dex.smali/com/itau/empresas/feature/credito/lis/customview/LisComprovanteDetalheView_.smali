.class public final Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;
.super Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;
.source "LisComprovanteDetalheView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 36
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;-><init>(Landroid/content/Context;)V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->alreadyInflated_:Z

    .line 33
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 37
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->init_()V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->alreadyInflated_:Z

    .line 33
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 42
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->init_()V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->alreadyInflated_:Z

    .line 33
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 47
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->init_()V

    .line 48
    return-void
.end method

.method private init_()V
    .registers 3

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 79
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 80
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 81
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 69
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->alreadyInflated_:Z

    .line 71
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03017d

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 74
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->onFinishInflate()V

    .line 75
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 103
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 104
    const v0, 0x7f0e0201

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->comprovanteValores:Landroid/widget/LinearLayout;

    .line 105
    const v0, 0x7f0e0675

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->assinaturaEletronica:Landroid/widget/LinearLayout;

    .line 106
    const v0, 0x7f0e0678

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->devedorSolidario:Landroid/widget/LinearLayout;

    .line 107
    const v0, 0x7f0e067b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->dadosControle:Landroid/widget/LinearLayout;

    .line 108
    const v0, 0x7f0e0670

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->comprovanteDadosConta:Landroid/widget/LinearLayout;

    .line 109
    const v0, 0x7f0e01ea

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->svLisComprovanteDetalhe:Landroid/widget/ScrollView;

    .line 110
    const v0, 0x7f0e01e9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView_;->lisComprovanteDetalhe:Landroid/view/View;

    .line 111
    return-void
.end method

.class public Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;
.super Landroid/support/v4/app/Fragment;
.source "ReceiptsFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;,
        Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptHolder;
    }
.end annotation


# static fields
.field private static ARG_KEY:Ljava/lang/String;


# instance fields
.field private moreItemsLinear:Landroid/widget/LinearLayout;

.field private receipts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lbr/com/itau/textovoz/model/Receipt;>;"
        }
    .end annotation
.end field

.field private receiptsAdapter:Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;

.field private receiptsRecyclerView:Landroid/support/v7/widget/RecyclerView;

.field private receiptsSubList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lbr/com/itau/textovoz/model/Receipt;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 33
    const-string v0, "receipts_key"

    sput-object v0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->ARG_KEY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .line 32
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;)Landroid/widget/LinearLayout;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;

    .line 32
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->moreItemsLinear:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$100(Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;)Ljava/util/List;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;

    .line 32
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->receipts:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;)Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;

    .line 32
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->receiptsAdapter:Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .line 32
    invoke-direct {p0, p1}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->getValueAsCurrency(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .line 32
    invoke-direct {p0, p1}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->formatDateString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private formatDateString(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .param p1, "dateString"    # Ljava/lang/String;

    .line 128
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v0, "yyyyMMdd"

    invoke-direct {v1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 129
    .local v1, "dateFormat":Ljava/text/SimpleDateFormat;
    const-string v2, ""

    .line 132
    .local v2, "formattedDate":Ljava/lang/String;
    :try_start_9
    invoke-virtual {v1, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    .line 133
    .local v3, "date":Ljava/util/Date;
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v0, "dd/MM/yyyy"

    invoke-direct {v1, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 134
    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_17
    .catch Ljava/text/ParseException; {:try_start_9 .. :try_end_17} :catch_1a

    move-result-object v0

    move-object v2, v0

    .line 138
    .end local v3    # "date":Ljava/util/Date;
    goto :goto_1e

    .line 136
    :catch_1a
    move-exception v3

    .line 137
    .local v3, "e":Ljava/text/ParseException;
    invoke-virtual {v3}, Ljava/text/ParseException;->printStackTrace()V

    .line 139
    .end local v3    # "e":Ljava/text/ParseException;
    :goto_1e
    return-object v2
.end method

.method private getValueAsCurrency(Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .param p1, "stringValue"    # Ljava/lang/String;

    .line 143
    invoke-static {}, Ljava/text/NumberFormat;->getCurrencyInstance()Ljava/text/NumberFormat;

    move-result-object v2

    .line 144
    .local v2, "format":Ljava/text/NumberFormat;
    invoke-virtual {v2}, Ljava/text/NumberFormat;->getCurrency()Ljava/util/Currency;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Currency;->getSymbol()Ljava/lang/String;

    move-result-object v3

    .line 145
    .local v3, "currencySymbol":Ljava/lang/String;
    invoke-direct {p0, p1}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->stringValueToDouble(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v4

    .line 146
    .local v4, "doubleValue":Ljava/lang/Double;
    invoke-virtual {v2, v4}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 148
    .local v5, "formattedValue":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v3, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance(Ljava/util/ArrayList;)Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;
    .registers 4
    .param p0, "receipts"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Receipt;>;)Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;"
        }
    .end annotation

    .line 44
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 45
    .local v1, "args":Landroid/os/Bundle;
    sget-object v0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->ARG_KEY:Ljava/lang/String;

    invoke-virtual {v1, v0, p0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 47
    new-instance v2, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;

    invoke-direct {v2}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;-><init>()V

    .line 48
    .local v2, "fragment":Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;
    invoke-virtual {v2, v1}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 49
    return-object v2
.end method

.method private stringValueToDouble(Ljava/lang/String;)Ljava/lang/Double;
    .registers 8
    .param p1, "stringValue"    # Ljava/lang/String;

    .line 152
    invoke-static {p1}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v4

    .line 153
    .local v4, "rawValueToDouble":Ljava/lang/Double;
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    .line 155
    .local v5, "actualValue":Ljava/lang/Double;
    return-object v5
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 54
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 56
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->ARG_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->receipts:Ljava/util/List;

    .line 57
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->receipts:Ljava/util/List;

    if-eqz v0, :cond_26

    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->receipts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x4

    if-le v0, v1, :cond_26

    .line 58
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->receipts:Ljava/util/List;

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->receiptsSubList:Ljava/util/List;

    .line 60
    :cond_26
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 12
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 65
    sget v0, Lbr/com/itau/textovoz/R$layout;->fragment_receipts_list:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 67
    .local v5, "v":Landroid/view/View;
    sget v0, Lbr/com/itau/textovoz/R$id;->text_name_title_list:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    .line 68
    .local v6, "headerTitleTextView":Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;
    sget v0, Lbr/com/itau/textovoz/R$string;->receipts_fragment_header:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    sget v0, Lbr/com/itau/textovoz/R$id;->more_items_btn:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->moreItemsLinear:Landroid/widget/LinearLayout;

    .line 71
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->moreItemsLinear:Landroid/widget/LinearLayout;

    new-instance v1, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$1;

    invoke-direct {v1, p0}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$1;-><init>(Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    sget v0, Lbr/com/itau/textovoz/R$id;->edit_query_more:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    .line 81
    .local v7, "moreItemsText":Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;
    sget v0, Lbr/com/itau/textovoz/R$string;->more_items_receipt:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->receiptsSubList:Ljava/util/List;

    if-eqz v0, :cond_53

    .line 84
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->moreItemsLinear:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 85
    new-instance v0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->receiptsSubList:Ljava/util/List;

    invoke-direct {v0, p0, v1}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;-><init>(Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;Ljava/util/List;)V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->receiptsAdapter:Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;

    goto :goto_5c

    .line 87
    :cond_53
    new-instance v0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->receipts:Ljava/util/List;

    invoke-direct {v0, p0, v1}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;-><init>(Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;Ljava/util/List;)V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->receiptsAdapter:Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;

    .line 90
    :goto_5c
    sget v0, Lbr/com/itau/textovoz/R$id;->recycler_view_receipts_search:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->receiptsRecyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 91
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->receiptsRecyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 92
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->receiptsRecyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/DefaultItemAnimator;

    invoke-direct {v1}, Landroid/support/v7/widget/DefaultItemAnimator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 93
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->receiptsRecyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lbr/com/itau/textovoz/ui/view/DividerItemDecoration;

    .line 94
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lbr/com/itau/textovoz/R$drawable;->divider_search_list:I

    invoke-static {v2, v3}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4}, Lbr/com/itau/textovoz/ui/view/DividerItemDecoration;-><init>(Landroid/graphics/drawable/Drawable;ZZ)V

    .line 93
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 95
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->receiptsRecyclerView:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    .line 96
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->receiptsRecyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->receiptsAdapter:Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 98
    return-object v5
.end method

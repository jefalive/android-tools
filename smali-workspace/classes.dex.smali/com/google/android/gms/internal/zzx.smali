.class public Lcom/google/android/gms/internal/zzx;
.super Ljava/lang/Object;


# direct methods
.method public static zza(Ljava/util/Map;)Ljava/lang/String;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;)Ljava/lang/String;"
        }
    .end annotation

    const-string v0, "ISO-8859-1"

    invoke-static {p0, v0}, Lcom/google/android/gms/internal/zzx;->zzb(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static zzb(Lcom/google/android/gms/internal/zzi;)Lcom/google/android/gms/internal/zzb$zza;
    .registers 28

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/internal/zzi;->zzA:Ljava/util/Map;

    const-wide/16 v5, 0x0

    const-wide/16 v7, 0x0

    const-wide/16 v9, 0x0

    const-wide/16 v11, 0x0

    const-wide/16 v13, 0x0

    const-wide/16 v15, 0x0

    const-wide/16 v17, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const-string v0, "Date"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/String;

    if-eqz v22, :cond_2c

    invoke-static/range {v22 .. v22}, Lcom/google/android/gms/internal/zzx;->zzg(Ljava/lang/String;)J

    move-result-wide v5

    :cond_2c
    const-string v0, "Cache-Control"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/String;

    if-eqz v22, :cond_b5

    const/16 v19, 0x1

    const-string v0, ","

    move-object/from16 v1, v22

    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v23

    const/16 v24, 0x0

    :goto_44
    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v1, v24

    if-ge v1, v0, :cond_b5

    aget-object v0, v23, v24

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v25

    const-string v0, "no-cache"

    move-object/from16 v1, v25

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_65

    const-string v0, "no-store"

    move-object/from16 v1, v25

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_67

    :cond_65
    const/4 v0, 0x0

    return-object v0

    :cond_67
    const-string v0, "max-age="

    move-object/from16 v1, v25

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_81

    move-object/from16 v0, v25

    const/16 v1, 0x8

    :try_start_75
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_7c
    .catch Ljava/lang/Exception; {:try_start_75 .. :try_end_7c} :catch_7f

    move-result-wide v0

    move-wide v15, v0

    goto :goto_b2

    :catch_7f
    move-exception v26

    goto :goto_b2

    :cond_81
    const-string v0, "stale-while-revalidate="

    move-object/from16 v1, v25

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9c

    move-object/from16 v0, v25

    const/16 v1, 0x17

    :try_start_8f
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_96
    .catch Ljava/lang/Exception; {:try_start_8f .. :try_end_96} :catch_9a

    move-result-wide v0

    move-wide/from16 v17, v0

    goto :goto_b2

    :catch_9a
    move-exception v26

    goto :goto_b2

    :cond_9c
    const-string v0, "must-revalidate"

    move-object/from16 v1, v25

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b0

    const-string v0, "proxy-revalidate"

    move-object/from16 v1, v25

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b2

    :cond_b0
    const/16 v20, 0x1

    :cond_b2
    :goto_b2
    add-int/lit8 v24, v24, 0x1

    goto :goto_44

    :cond_b5
    const-string v0, "Expires"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/String;

    if-eqz v22, :cond_c5

    invoke-static/range {v22 .. v22}, Lcom/google/android/gms/internal/zzx;->zzg(Ljava/lang/String;)J

    move-result-wide v9

    :cond_c5
    const-string v0, "Last-Modified"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v22, v0

    check-cast v22, Ljava/lang/String;

    if-eqz v22, :cond_d5

    invoke-static/range {v22 .. v22}, Lcom/google/android/gms/internal/zzx;->zzg(Ljava/lang/String;)J

    move-result-wide v7

    :cond_d5
    const-string v0, "ETag"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v21, v0

    check-cast v21, Ljava/lang/String;

    if-eqz v19, :cond_f1

    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, v15

    add-long v11, v2, v0

    if-eqz v20, :cond_ea

    move-wide v13, v11

    goto :goto_f0

    :cond_ea
    const-wide/16 v0, 0x3e8

    mul-long v0, v0, v17

    add-long v13, v11, v0

    :goto_f0
    goto :goto_100

    :cond_f1
    const-wide/16 v0, 0x0

    cmp-long v0, v5, v0

    if-lez v0, :cond_100

    cmp-long v0, v9, v5

    if-ltz v0, :cond_100

    sub-long v0, v9, v5

    add-long v11, v2, v0

    move-wide v13, v11

    :cond_100
    :goto_100
    new-instance v0, Lcom/google/android/gms/internal/zzb$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzb$zza;-><init>()V

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzi;->data:[B

    move-object/from16 v1, v23

    iput-object v0, v1, Lcom/google/android/gms/internal/zzb$zza;->data:[B

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    iput-object v0, v1, Lcom/google/android/gms/internal/zzb$zza;->zzb:Ljava/lang/String;

    move-object/from16 v0, v23

    iput-wide v11, v0, Lcom/google/android/gms/internal/zzb$zza;->zzf:J

    move-object/from16 v0, v23

    iput-wide v13, v0, Lcom/google/android/gms/internal/zzb$zza;->zze:J

    move-object/from16 v0, v23

    iput-wide v5, v0, Lcom/google/android/gms/internal/zzb$zza;->zzc:J

    move-object/from16 v0, v23

    iput-wide v7, v0, Lcom/google/android/gms/internal/zzb$zza;->zzd:J

    move-object/from16 v0, v23

    iput-object v4, v0, Lcom/google/android/gms/internal/zzb$zza;->zzg:Ljava/util/Map;

    return-object v23
.end method

.method public static zzb(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;Ljava/lang/String;)Ljava/lang/String;"
        }
    .end annotation

    const-string v0, "Content-Type"

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/String;

    if-eqz v2, :cond_37

    const-string v0, ";"

    invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    :goto_12
    array-length v0, v3

    if-ge v4, v0, :cond_37

    aget-object v0, v3, v4

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    array-length v0, v5

    const/4 v1, 0x2

    if-ne v0, v1, :cond_34

    const/4 v0, 0x0

    aget-object v0, v5, v0

    const-string v1, "charset"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_34

    const/4 v0, 0x1

    aget-object v0, v5, v0

    return-object v0

    :cond_34
    add-int/lit8 v4, v4, 0x1

    goto :goto_12

    :cond_37
    return-object p1
.end method

.method public static zzg(Ljava/lang/String;)J
    .registers 4

    :try_start_0
    invoke-static {p0}, Lorg/apache/http/impl/cookie/DateUtils;->parseDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J
    :try_end_7
    .catch Lorg/apache/http/impl/cookie/DateParseException; {:try_start_0 .. :try_end_7} :catch_9

    move-result-wide v0

    return-wide v0

    :catch_9
    move-exception v2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.class Landroid/support/v4/util/ContainerHelpers;
.super Ljava/lang/Object;
.source "ContainerHelpers.java"


# static fields
.field static final EMPTY_INTS:[I

.field static final EMPTY_LONGS:[J

.field static final EMPTY_OBJECTS:[Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 20
    const/4 v0, 0x0

    new-array v0, v0, [I

    sput-object v0, Landroid/support/v4/util/ContainerHelpers;->EMPTY_INTS:[I

    .line 21
    const/4 v0, 0x0

    new-array v0, v0, [J

    sput-object v0, Landroid/support/v4/util/ContainerHelpers;->EMPTY_LONGS:[J

    .line 22
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    sput-object v0, Landroid/support/v4/util/ContainerHelpers;->EMPTY_OBJECTS:[Ljava/lang/Object;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static binarySearch([III)I
    .registers 9
    .param p0, "array"    # [I
    .param p1, "size"    # I
    .param p2, "value"    # I

    .line 46
    const/4 v2, 0x0

    .line 47
    .local v2, "lo":I
    add-int/lit8 v3, p1, -0x1

    .line 49
    .local v3, "hi":I
    :goto_3
    if-gt v2, v3, :cond_17

    .line 50
    add-int v0, v2, v3

    ushr-int/lit8 v4, v0, 0x1

    .line 51
    .local v4, "mid":I
    aget v5, p0, v4

    .line 53
    .local v5, "midVal":I
    if-ge v5, p2, :cond_10

    .line 54
    add-int/lit8 v2, v4, 0x1

    goto :goto_16

    .line 55
    :cond_10
    if-le v5, p2, :cond_15

    .line 56
    add-int/lit8 v3, v4, -0x1

    goto :goto_16

    .line 58
    :cond_15
    return v4

    .line 60
    .end local v4    # "mid":I
    .end local v5    # "midVal":I
    :goto_16
    goto :goto_3

    .line 61
    :cond_17
    xor-int/lit8 v0, v2, -0x1

    return v0
.end method

.method static binarySearch([JIJ)I
    .registers 11
    .param p0, "array"    # [J
    .param p1, "size"    # I
    .param p2, "value"    # J

    .line 65
    const/4 v2, 0x0

    .line 66
    .local v2, "lo":I
    add-int/lit8 v3, p1, -0x1

    .line 68
    .local v3, "hi":I
    :goto_3
    if-gt v2, v3, :cond_1b

    .line 69
    add-int v0, v2, v3

    ushr-int/lit8 v4, v0, 0x1

    .line 70
    .local v4, "mid":I
    aget-wide v5, p0, v4

    .line 72
    .local v5, "midVal":J
    cmp-long v0, v5, p2

    if-gez v0, :cond_12

    .line 73
    add-int/lit8 v2, v4, 0x1

    goto :goto_1a

    .line 74
    :cond_12
    cmp-long v0, v5, p2

    if-lez v0, :cond_19

    .line 75
    add-int/lit8 v3, v4, -0x1

    goto :goto_1a

    .line 77
    :cond_19
    return v4

    .line 79
    .end local v4    # "mid":I
    .end local v5    # "midVal":J
    :goto_1a
    goto :goto_3

    .line 80
    :cond_1b
    xor-int/lit8 v0, v2, -0x1

    return v0
.end method

.method public static equal(Ljava/lang/Object;Ljava/lang/Object;)Z
    .registers 3
    .param p0, "a"    # Ljava/lang/Object;
    .param p1, "b"    # Ljava/lang/Object;

    .line 41
    if-eq p0, p1, :cond_a

    if-eqz p0, :cond_c

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    goto :goto_d

    :cond_c
    const/4 v0, 0x0

    :goto_d
    return v0
.end method

.method public static idealByteArraySize(I)I
    .registers 4
    .param p0, "need"    # I

    .line 33
    const/4 v2, 0x4

    .local v2, "i":I
    :goto_1
    const/16 v0, 0x20

    if-ge v2, v0, :cond_13

    .line 34
    const/4 v0, 0x1

    shl-int/2addr v0, v2

    add-int/lit8 v0, v0, -0xc

    if-gt p0, v0, :cond_10

    .line 35
    const/4 v0, 0x1

    shl-int/2addr v0, v2

    add-int/lit8 v0, v0, -0xc

    return v0

    .line 33
    :cond_10
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 37
    .end local v2    # "i":I
    :cond_13
    return p0
.end method

.method public static idealIntArraySize(I)I
    .registers 3
    .param p0, "need"    # I

    .line 25
    mul-int/lit8 v0, p0, 0x4

    invoke-static {v0}, Landroid/support/v4/util/ContainerHelpers;->idealByteArraySize(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x4

    return v0
.end method

.method public static idealLongArraySize(I)I
    .registers 3
    .param p0, "need"    # I

    .line 29
    mul-int/lit8 v0, p0, 0x8

    invoke-static {v0}, Landroid/support/v4/util/ContainerHelpers;->idealByteArraySize(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x8

    return v0
.end method

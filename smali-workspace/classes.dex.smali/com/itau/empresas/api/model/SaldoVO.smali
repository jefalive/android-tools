.class public Lcom/itau/empresas/api/model/SaldoVO;
.super Ljava/lang/Object;
.source "SaldoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private disponivelSaque:Ljava/lang/Double;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "disponivel_saque"
    .end annotation
.end field

.field private limiteTotal:D
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "limite_total"
    .end annotation
.end field

.field private saldoDisponivelSaque:Ljava/lang/Double;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "saldo_disponivel_saque"
    .end annotation
.end field

.field private valorLimiteUtilizado:D
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_limite_utilizado"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDisponivelSaque()Ljava/lang/Double;
    .registers 2

    .line 22
    iget-object v0, p0, Lcom/itau/empresas/api/model/SaldoVO;->disponivelSaque:Ljava/lang/Double;

    return-object v0
.end method

.method public getSaldoDisponivelSaque()Ljava/lang/Double;
    .registers 2

    .line 26
    iget-object v0, p0, Lcom/itau/empresas/api/model/SaldoVO;->saldoDisponivelSaque:Ljava/lang/Double;

    return-object v0
.end method

.method public getValorLimiteUtilizado()D
    .registers 3

    .line 30
    iget-wide v0, p0, Lcom/itau/empresas/api/model/SaldoVO;->valorLimiteUtilizado:D

    return-wide v0
.end method

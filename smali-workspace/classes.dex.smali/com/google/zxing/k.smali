.class public final Lcom/google/zxing/k;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:[B

.field private c:[Lcom/google/zxing/m;

.field private final d:Lcom/google/zxing/a;

.field private e:Ljava/util/Map;

.field private final f:J


# direct methods
.method public constructor <init>(Ljava/lang/String;[B[Lcom/google/zxing/m;Lcom/google/zxing/a;)V
    .registers 12

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-direct/range {v0 .. v6}, Lcom/google/zxing/k;-><init>(Ljava/lang/String;[B[Lcom/google/zxing/m;Lcom/google/zxing/a;J)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[B[Lcom/google/zxing/m;Lcom/google/zxing/a;J)V
    .registers 8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/zxing/k;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/zxing/k;->b:[B

    iput-object p3, p0, Lcom/google/zxing/k;->c:[Lcom/google/zxing/m;

    iput-object p4, p0, Lcom/google/zxing/k;->d:Lcom/google/zxing/a;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/zxing/k;->e:Ljava/util/Map;

    iput-wide p5, p0, Lcom/google/zxing/k;->f:J

    return-void
.end method


# virtual methods
.method public a(Lcom/google/zxing/l;Ljava/lang/Object;)V
    .registers 5

    iget-object v0, p0, Lcom/google/zxing/k;->e:Ljava/util/Map;

    if-nez v0, :cond_d

    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/google/zxing/l;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/google/zxing/k;->e:Ljava/util/Map;

    :cond_d
    iget-object v0, p0, Lcom/google/zxing/k;->e:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a()[B
    .registers 2

    iget-object v0, p0, Lcom/google/zxing/k;->b:[B

    return-object v0
.end method

.method public b()[Lcom/google/zxing/m;
    .registers 2

    iget-object v0, p0, Lcom/google/zxing/k;->c:[Lcom/google/zxing/m;

    return-object v0
.end method

.method public c()Ljava/util/Map;
    .registers 2

    iget-object v0, p0, Lcom/google/zxing/k;->e:Ljava/util/Map;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lcom/google/zxing/k;->a:Ljava/lang/String;

    return-object v0
.end method

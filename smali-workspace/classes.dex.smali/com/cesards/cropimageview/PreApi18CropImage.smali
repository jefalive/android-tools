.class public Lcom/cesards/cropimageview/PreApi18CropImage;
.super Lcom/cesards/cropimageview/CropImage;
.source "PreApi18CropImage.java"

# interfaces
.implements Lcom/cesards/cropimageview/ImageMaths;


# instance fields
.field private matrix:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(Lcom/cesards/cropimageview/CropImageView;)V
    .registers 2
    .param p1, "imageView"    # Lcom/cesards/cropimageview/CropImageView;

    .line 10
    invoke-direct {p0, p1}, Lcom/cesards/cropimageview/CropImage;-><init>(Lcom/cesards/cropimageview/CropImageView;)V

    .line 12
    invoke-direct {p0, p1}, Lcom/cesards/cropimageview/PreApi18CropImage;->init(Lcom/cesards/cropimageview/CropImageView;)V

    .line 13
    return-void
.end method

.method private init(Lcom/cesards/cropimageview/CropImageView;)V
    .registers 4
    .param p1, "imageView"    # Lcom/cesards/cropimageview/CropImageView;

    .line 16
    invoke-virtual {p1}, Lcom/cesards/cropimageview/CropImageView;->getCropType()Lcom/cesards/cropimageview/CropImageView$CropType;

    move-result-object v0

    sget-object v1, Lcom/cesards/cropimageview/CropImageView$CropType;->NONE:Lcom/cesards/cropimageview/CropImageView$CropType;

    if-eq v0, v1, :cond_f

    .line 17
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/cesards/cropimageview/PreApi18CropImage;->matrix:Landroid/graphics/Matrix;

    .line 19
    :cond_f
    return-void
.end method


# virtual methods
.method public getMatrix()Landroid/graphics/Matrix;
    .registers 2

    .line 23
    iget-object v0, p0, Lcom/cesards/cropimageview/PreApi18CropImage;->matrix:Landroid/graphics/Matrix;

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/cesards/cropimageview/PreApi18CropImage;->imageView:Lcom/cesards/cropimageview/CropImageView;

    invoke-virtual {v0}, Lcom/cesards/cropimageview/CropImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    goto :goto_d

    :cond_b
    iget-object v0, p0, Lcom/cesards/cropimageview/PreApi18CropImage;->matrix:Landroid/graphics/Matrix;

    :goto_d
    return-object v0
.end method

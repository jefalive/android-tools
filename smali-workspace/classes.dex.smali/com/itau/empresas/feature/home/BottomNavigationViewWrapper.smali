.class public final Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;
.super Ljava/lang/Object;
.source "BottomNavigationViewWrapper.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final bottomNavigationView:Landroid/support/design/widget/BottomNavigationView;

.field private final icontopMargin:I

.field private volatile lastMenuItem:Landroid/support/design/internal/BottomNavigationItemView;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 21
    const-class v0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/support/design/widget/BottomNavigationView;)V
    .registers 4
    .param p1, "bottomNavigationView"    # Landroid/support/design/widget/BottomNavigationView;

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->bottomNavigationView:Landroid/support/design/widget/BottomNavigationView;

    .line 31
    invoke-virtual {p1}, Landroid/support/design/widget/BottomNavigationView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 32
    const v1, 0x7f080130

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->icontopMargin:I

    .line 33
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;)Landroid/support/design/internal/BottomNavigationItemView;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;

    .line 19
    iget-object v0, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->lastMenuItem:Landroid/support/design/internal/BottomNavigationItemView;

    return-object v0
.end method

.method static synthetic access$002(Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;Landroid/support/design/internal/BottomNavigationItemView;)Landroid/support/design/internal/BottomNavigationItemView;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;
    .param p1, "x1"    # Landroid/support/design/internal/BottomNavigationItemView;

    .line 19
    iput-object p1, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->lastMenuItem:Landroid/support/design/internal/BottomNavigationItemView;

    return-object p1
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;Landroid/widget/ImageView;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .line 19
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->descentralizaItem(Landroid/widget/ImageView;)V

    return-void
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;Landroid/view/View;Landroid/view/View;)V
    .registers 3
    .param p0, "x0"    # Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Landroid/view/View;

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->mostraTitulo(Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$300(Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;Landroid/widget/ImageView;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .line 19
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->centralizaItem(Landroid/widget/ImageView;)V

    return-void
.end method

.method static synthetic access$400(Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;Landroid/view/View;Landroid/view/View;)V
    .registers 3
    .param p0, "x0"    # Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Landroid/view/View;

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->escondeTitulo(Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method private alteraVisibilidadeShiftMode(Landroid/support/design/internal/BottomNavigationMenuView;)V
    .registers 5
    .param p1, "menuView"    # Landroid/support/design/internal/BottomNavigationMenuView;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NoSuchFieldException;,
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    .line 55
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "mShiftingMode"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 56
    .local v2, "shiftingMode":Ljava/lang/reflect/Field;
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 57
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v2, p1, v0}, Ljava/lang/reflect/Field;->setBoolean(Ljava/lang/Object;Z)V

    .line 58
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 59
    return-void
.end method

.method private centralizaItem(Landroid/widget/ImageView;)V
    .registers 4
    .param p1, "icon"    # Landroid/widget/ImageView;

    .line 104
    invoke-virtual {p1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 105
    .local v1, "iconParams":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v0, 0x11

    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 106
    iget v0, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->icontopMargin:I

    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 107
    return-void
.end method

.method private changeBackgroundDrawable(Landroid/support/design/internal/BottomNavigationItemView;)V
    .registers 12
    .param p1, "item"    # Landroid/support/design/internal/BottomNavigationItemView;

    .line 73
    const v0, 0x7f0e037d

    invoke-virtual {p1, v0}, Landroid/support/design/internal/BottomNavigationItemView;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 74
    .local v7, "smallLabel":Landroid/view/View;
    const v0, 0x7f0e037e

    invoke-virtual {p1, v0}, Landroid/support/design/internal/BottomNavigationItemView;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 75
    .local v8, "largeLabel":Landroid/view/View;
    const v0, 0x7f0e008c

    invoke-virtual {p1, v0}, Landroid/support/design/internal/BottomNavigationItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/widget/ImageView;

    .line 76
    .local v9, "icon":Landroid/widget/ImageView;
    invoke-virtual {p1}, Landroid/support/design/internal/BottomNavigationItemView;->getItemData()Landroid/support/v7/view/menu/MenuItemImpl;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/view/menu/MenuItemImpl;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/design/internal/BottomNavigationItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 78
    invoke-virtual {p1}, Landroid/support/design/internal/BottomNavigationItemView;->getItemData()Landroid/support/v7/view/menu/MenuItemImpl;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;

    move-object v2, p0

    move-object v3, p1

    move-object v4, v9

    move-object v5, v7

    move-object v6, v8

    invoke-direct/range {v1 .. v6}, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;-><init>(Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;Landroid/support/design/internal/BottomNavigationItemView;Landroid/widget/ImageView;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/view/menu/MenuItemImpl;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 95
    return-void
.end method

.method private clicaEmPrimeiroItem()V
    .registers 4

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->bottomNavigationView:Landroid/support/design/widget/BottomNavigationView;

    invoke-virtual {v0}, Landroid/support/design/widget/BottomNavigationView;->getMenu()Landroid/view/Menu;

    move-result-object v2

    .line 64
    .local v2, "menu":Landroid/view/Menu;
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const/4 v1, 0x0

    invoke-interface {v2, v0, v1}, Landroid/view/Menu;->performIdentifierAction(II)Z

    .line 65
    return-void
.end method

.method private descentralizaItem(Landroid/widget/ImageView;)V
    .registers 4
    .param p1, "icon"    # Landroid/widget/ImageView;

    .line 98
    invoke-virtual {p1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 99
    .local v1, "iconParams":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v0, 0x31

    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 100
    iget v0, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->icontopMargin:I

    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 101
    return-void
.end method

.method private disableShift(Landroid/support/design/internal/BottomNavigationItemView;)V
    .registers 3
    .param p1, "item"    # Landroid/support/design/internal/BottomNavigationItemView;

    .line 68
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/design/internal/BottomNavigationItemView;->setShiftingMode(Z)V

    .line 69
    invoke-virtual {p1}, Landroid/support/design/internal/BottomNavigationItemView;->getItemData()Landroid/support/v7/view/menu/MenuItemImpl;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/view/menu/MenuItemImpl;->isChecked()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/support/design/internal/BottomNavigationItemView;->setChecked(Z)V

    .line 70
    return-void
.end method

.method private escondeTitulo(Landroid/view/View;Landroid/view/View;)V
    .registers 4
    .param p1, "smallLabel"    # Landroid/view/View;
    .param p2, "largeLabel"    # Landroid/view/View;

    .line 110
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 111
    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 112
    return-void
.end method

.method private mostraTitulo(Landroid/view/View;Landroid/view/View;)V
    .registers 4
    .param p1, "smallLabel"    # Landroid/view/View;
    .param p2, "largeLabel"    # Landroid/view/View;

    .line 115
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 116
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 117
    return-void
.end method


# virtual methods
.method configureBottomNavigation()V
    .registers 6

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->bottomNavigationView:Landroid/support/design/widget/BottomNavigationView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/BottomNavigationView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/support/design/internal/BottomNavigationMenuView;

    .line 38
    .local v2, "menuView":Landroid/support/design/internal/BottomNavigationMenuView;
    :try_start_a
    invoke-direct {p0, v2}, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->alteraVisibilidadeShiftMode(Landroid/support/design/internal/BottomNavigationMenuView;)V

    .line 39
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_e
    invoke-virtual {v2}, Landroid/support/design/internal/BottomNavigationMenuView;->getChildCount()I

    move-result v0

    if-ge v3, v0, :cond_24

    .line 40
    .line 41
    invoke-virtual {v2, v3}, Landroid/support/design/internal/BottomNavigationMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/support/design/internal/BottomNavigationItemView;

    .line 42
    .local v4, "item":Landroid/support/design/internal/BottomNavigationItemView;
    invoke-direct {p0, v4}, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->disableShift(Landroid/support/design/internal/BottomNavigationItemView;)V

    .line 43
    invoke-direct {p0, v4}, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->changeBackgroundDrawable(Landroid/support/design/internal/BottomNavigationItemView;)V
    :try_end_21
    .catch Ljava/lang/NoSuchFieldException; {:try_start_a .. :try_end_21} :catch_25
    .catch Ljava/lang/IllegalAccessException; {:try_start_a .. :try_end_21} :catch_2e

    .line 39
    .end local v4    # "item":Landroid/support/design/internal/BottomNavigationItemView;
    add-int/lit8 v3, v3, 0x1

    goto :goto_e

    .line 49
    .end local v3    # "i":I
    :cond_24
    goto :goto_36

    .line 45
    :catch_25
    move-exception v3

    .line 46
    .local v3, "e":Ljava/lang/NoSuchFieldException;
    sget-object v0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->TAG:Ljava/lang/String;

    const-string v1, "N\u00e3o foi poss\u00edvel acessar o campo de shift mode"

    invoke-static {v0, v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 49
    .end local v3    # "e":Ljava/lang/NoSuchFieldException;
    goto :goto_36

    .line 47
    :catch_2e
    move-exception v3

    .line 48
    .local v3, "e":Ljava/lang/IllegalAccessException;
    sget-object v0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->TAG:Ljava/lang/String;

    const-string v1, "N\u00e3o foi poss\u00edvel alterar o valor de shift"

    invoke-static {v0, v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 50
    .end local v3    # "e":Ljava/lang/IllegalAccessException;
    :goto_36
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->clicaEmPrimeiroItem()V

    .line 51
    return-void
.end method

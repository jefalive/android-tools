.class final Lcom/adobe/mobile/MessageMatcherGreaterThanOrEqual;
.super Lcom/adobe/mobile/MessageMatcher;
.source "MessageMatcherGreaterThanOrEqual.java"


# direct methods
.method constructor <init>()V
    .registers 1

    .line 21
    invoke-direct {p0}, Lcom/adobe/mobile/MessageMatcher;-><init>()V

    return-void
.end method


# virtual methods
.method protected matches(Ljava/lang/Object;)Z
    .registers 9
    .param p1, "value"    # Ljava/lang/Object;

    .line 25
    invoke-virtual {p0, p1}, Lcom/adobe/mobile/MessageMatcherGreaterThanOrEqual;->tryParseDouble(Ljava/lang/Object;)Ljava/lang/Double;

    move-result-object v4

    .line 26
    .local v4, "valueAsDouble":Ljava/lang/Double;
    if-nez v4, :cond_8

    .line 27
    const/4 v0, 0x0

    return v0

    .line 30
    :cond_8
    iget-object v0, p0, Lcom/adobe/mobile/MessageMatcherGreaterThanOrEqual;->values:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_e
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 31
    .local v6, "v":Ljava/lang/Object;
    instance-of v0, v6, Ljava/lang/Number;

    if-eqz v0, :cond_2d

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    move-object v2, v6

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_2d

    .line 32
    const/4 v0, 0x1

    return v0

    .line 34
    .end local v6    # "v":Ljava/lang/Object;
    :cond_2d
    goto :goto_e

    .line 36
    :cond_2e
    const/4 v0, 0x0

    return v0
.end method

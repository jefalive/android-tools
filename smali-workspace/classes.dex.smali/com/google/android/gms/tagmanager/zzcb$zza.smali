.class final enum Lcom/google/android/gms/tagmanager/zzcb$zza;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/tagmanager/zzcb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "zza"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/google/android/gms/tagmanager/zzcb$zza;>;"
    }
.end annotation


# static fields
.field public static final enum zzbjU:Lcom/google/android/gms/tagmanager/zzcb$zza;

.field public static final enum zzbjV:Lcom/google/android/gms/tagmanager/zzcb$zza;

.field public static final enum zzbjW:Lcom/google/android/gms/tagmanager/zzcb$zza;

.field private static final synthetic zzbjX:[Lcom/google/android/gms/tagmanager/zzcb$zza;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    new-instance v0, Lcom/google/android/gms/tagmanager/zzcb$zza;

    const-string v1, "NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/tagmanager/zzcb$zza;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/tagmanager/zzcb$zza;->zzbjU:Lcom/google/android/gms/tagmanager/zzcb$zza;

    new-instance v0, Lcom/google/android/gms/tagmanager/zzcb$zza;

    const-string v1, "CONTAINER"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/tagmanager/zzcb$zza;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/tagmanager/zzcb$zza;->zzbjV:Lcom/google/android/gms/tagmanager/zzcb$zza;

    new-instance v0, Lcom/google/android/gms/tagmanager/zzcb$zza;

    const-string v1, "CONTAINER_DEBUG"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/tagmanager/zzcb$zza;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/tagmanager/zzcb$zza;->zzbjW:Lcom/google/android/gms/tagmanager/zzcb$zza;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/gms/tagmanager/zzcb$zza;

    sget-object v1, Lcom/google/android/gms/tagmanager/zzcb$zza;->zzbjU:Lcom/google/android/gms/tagmanager/zzcb$zza;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/tagmanager/zzcb$zza;->zzbjV:Lcom/google/android/gms/tagmanager/zzcb$zza;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/tagmanager/zzcb$zza;->zzbjW:Lcom/google/android/gms/tagmanager/zzcb$zza;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/gms/tagmanager/zzcb$zza;->zzbjX:[Lcom/google/android/gms/tagmanager/zzcb$zza;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/tagmanager/zzcb$zza;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    const-class v0, Lcom/google/android/gms/tagmanager/zzcb$zza;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/tagmanager/zzcb$zza;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/tagmanager/zzcb$zza;
    .registers 1

    sget-object v0, Lcom/google/android/gms/tagmanager/zzcb$zza;->zzbjX:[Lcom/google/android/gms/tagmanager/zzcb$zza;

    invoke-virtual {v0}, [Lcom/google/android/gms/tagmanager/zzcb$zza;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/tagmanager/zzcb$zza;

    return-object v0
.end method

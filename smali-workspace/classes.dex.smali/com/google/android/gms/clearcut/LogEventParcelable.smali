.class public Lcom/google/android/gms/clearcut/LogEventParcelable;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/clearcut/zzd;


# instance fields
.field public final versionCode:I

.field public zzafh:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

.field public zzafi:[B

.field public zzafj:[I

.field public final zzafk:Lcom/google/android/gms/internal/zzsz$zzd;

.field public final zzafl:Lcom/google/android/gms/clearcut/zzb$zzb;

.field public final zzafm:Lcom/google/android/gms/clearcut/zzb$zzb;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    new-instance v0, Lcom/google/android/gms/clearcut/zzd;

    invoke-direct {v0}, Lcom/google/android/gms/clearcut/zzd;-><init>()V

    sput-object v0, Lcom/google/android/gms/clearcut/LogEventParcelable;->CREATOR:Lcom/google/android/gms/clearcut/zzd;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/playlog/internal/PlayLoggerContext;[B[I)V
    .registers 6
    .param p1, "versionCode"    # I
    .param p2, "playLoggerContext"    # Lcom/google/android/gms/playlog/internal/PlayLoggerContext;
    .param p3, "logEventBytes"    # [B
    .param p4, "testCodes"    # [I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->versionCode:I

    iput-object p2, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafh:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    iput-object p3, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafi:[B

    iput-object p4, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafj:[I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafk:Lcom/google/android/gms/internal/zzsz$zzd;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafl:Lcom/google/android/gms/clearcut/zzb$zzb;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafm:Lcom/google/android/gms/clearcut/zzb$zzb;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/playlog/internal/PlayLoggerContext;Lcom/google/android/gms/internal/zzsz$zzd;Lcom/google/android/gms/clearcut/zzb$zzb;Lcom/google/android/gms/clearcut/zzb$zzb;[I)V
    .registers 7
    .param p1, "playLoggerContext"    # Lcom/google/android/gms/playlog/internal/PlayLoggerContext;
    .param p2, "logEvent"    # Lcom/google/android/gms/internal/zzsz$zzd;
    .param p3, "extensionProducer"    # Lcom/google/android/gms/clearcut/zzb$zzb;
    .param p4, "clientVisualElementsProducer"    # Lcom/google/android/gms/clearcut/zzb$zzb;
    .param p5, "testCodes"    # [I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->versionCode:I

    iput-object p1, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafh:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    iput-object p2, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafk:Lcom/google/android/gms/internal/zzsz$zzd;

    iput-object p3, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafl:Lcom/google/android/gms/clearcut/zzb$zzb;

    iput-object p4, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafm:Lcom/google/android/gms/clearcut/zzb$zzb;

    iput-object p5, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafj:[I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1, "other"    # Ljava/lang/Object;

    if-ne p0, p1, :cond_4

    const/4 v0, 0x1

    return v0

    :cond_4
    instance-of v0, p1, Lcom/google/android/gms/clearcut/LogEventParcelable;

    if-eqz v0, :cond_51

    move-object v2, p1

    check-cast v2, Lcom/google/android/gms/clearcut/LogEventParcelable;

    iget v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->versionCode:I

    iget v1, v2, Lcom/google/android/gms/clearcut/LogEventParcelable;->versionCode:I

    if-ne v0, v1, :cond_4f

    iget-object v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafh:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    iget-object v1, v2, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafh:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzw;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4f

    iget-object v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafi:[B

    iget-object v1, v2, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafi:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-eqz v0, :cond_4f

    iget-object v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafj:[I

    iget-object v1, v2, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafj:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    if-eqz v0, :cond_4f

    iget-object v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafk:Lcom/google/android/gms/internal/zzsz$zzd;

    iget-object v1, v2, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafk:Lcom/google/android/gms/internal/zzsz$zzd;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzw;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4f

    iget-object v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafl:Lcom/google/android/gms/clearcut/zzb$zzb;

    iget-object v1, v2, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafl:Lcom/google/android/gms/clearcut/zzb$zzb;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzw;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4f

    iget-object v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafm:Lcom/google/android/gms/clearcut/zzb$zzb;

    iget-object v1, v2, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafm:Lcom/google/android/gms/clearcut/zzb$zzb;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzw;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4f

    const/4 v0, 0x1

    goto :goto_50

    :cond_4f
    const/4 v0, 0x0

    :goto_50
    return v0

    :cond_51
    const/4 v0, 0x0

    return v0
.end method

.method public hashCode()I
    .registers 4

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    iget v1, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->versionCode:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafh:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafi:[B

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafj:[I

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafk:Lcom/google/android/gms/internal/zzsz$zzd;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafl:Lcom/google/android/gms/clearcut/zzb$zzb;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafm:Lcom/google/android/gms/clearcut/zzb$zzb;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/google/android/gms/common/internal/zzw;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "LogEventParcelable["

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->versionCode:I

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ", "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafh:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ", "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafi:[B

    if-nez v0, :cond_21

    const/4 v0, 0x0

    goto :goto_28

    :cond_21
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafi:[B

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    :goto_28
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafj:[I

    if-nez v0, :cond_36

    const/4 v0, 0x0

    goto :goto_4c

    :cond_36
    const-string v0, ", "

    invoke-static {v0}, Lcom/google/android/gms/common/internal/zzv;->zzcL(Ljava/lang/String;)Lcom/google/android/gms/common/internal/zzv;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [[I

    iget-object v2, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafj:[I

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/zzv;->zza(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    :goto_4c
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafk:Lcom/google/android/gms/internal/zzsz$zzd;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ", "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafl:Lcom/google/android/gms/clearcut/zzb$zzb;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ", "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/gms/clearcut/LogEventParcelable;->zzafm:Lcom/google/android/gms/clearcut/zzb$zzb;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, "]"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 3
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/clearcut/zzd;->zza(Lcom/google/android/gms/clearcut/LogEventParcelable;Landroid/os/Parcel;I)V

    return-void
.end method

.class public final Lokio/Buffer;
.super Ljava/lang/Object;
.source "Buffer.java"

# interfaces
.implements Lokio/BufferedSource;
.implements Lokio/BufferedSink;
.implements Ljava/lang/Cloneable;


# static fields
.field private static final DIGITS:[B


# instance fields
.field head:Lokio/Segment;

.field size:J


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 48
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_a

    sput-object v0, Lokio/Buffer;->DIGITS:[B

    return-void

    :array_a
    .array-data 1
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x61t
        0x62t
        0x63t
        0x64t
        0x65t
        0x66t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    return-void
.end method


# virtual methods
.method public buffer()Lokio/Buffer;
    .registers 1

    .line 64
    return-object p0
.end method

.method public clear()V
    .registers 4

    .line 799
    :try_start_0
    iget-wide v0, p0, Lokio/Buffer;->size:J

    invoke-virtual {p0, v0, v1}, Lokio/Buffer;->skip(J)V
    :try_end_5
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_5} :catch_6

    .line 802
    goto :goto_d

    .line 800
    :catch_6
    move-exception v2

    .line 801
    .local v2, "e":Ljava/io/EOFException;
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 803
    .end local v2    # "e":Ljava/io/EOFException;
    :goto_d
    return-void
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 47
    invoke-virtual {p0}, Lokio/Buffer;->clone()Lokio/Buffer;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lokio/Buffer;
    .registers 7

    .line 1607
    new-instance v4, Lokio/Buffer;

    invoke-direct {v4}, Lokio/Buffer;-><init>()V

    .line 1608
    .local v4, "result":Lokio/Buffer;
    iget-wide v0, p0, Lokio/Buffer;->size:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_e

    return-object v4

    .line 1610
    :cond_e
    new-instance v0, Lokio/Segment;

    iget-object v1, p0, Lokio/Buffer;->head:Lokio/Segment;

    invoke-direct {v0, v1}, Lokio/Segment;-><init>(Lokio/Segment;)V

    iput-object v0, v4, Lokio/Buffer;->head:Lokio/Segment;

    .line 1611
    iget-object v0, v4, Lokio/Buffer;->head:Lokio/Segment;

    iget-object v2, v4, Lokio/Buffer;->head:Lokio/Segment;

    iget-object v1, v4, Lokio/Buffer;->head:Lokio/Segment;

    iput-object v1, v2, Lokio/Segment;->prev:Lokio/Segment;

    iput-object v1, v0, Lokio/Segment;->next:Lokio/Segment;

    .line 1612
    iget-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    iget-object v5, v0, Lokio/Segment;->next:Lokio/Segment;

    .local v5, "s":Lokio/Segment;
    :goto_25
    iget-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    if-eq v5, v0, :cond_38

    .line 1613
    iget-object v0, v4, Lokio/Buffer;->head:Lokio/Segment;

    iget-object v0, v0, Lokio/Segment;->prev:Lokio/Segment;

    new-instance v1, Lokio/Segment;

    invoke-direct {v1, v5}, Lokio/Segment;-><init>(Lokio/Segment;)V

    invoke-virtual {v0, v1}, Lokio/Segment;->push(Lokio/Segment;)Lokio/Segment;

    .line 1612
    iget-object v5, v5, Lokio/Segment;->next:Lokio/Segment;

    goto :goto_25

    .line 1615
    .end local v5    # "s":Lokio/Segment;
    :cond_38
    iget-wide v0, p0, Lokio/Buffer;->size:J

    iput-wide v0, v4, Lokio/Buffer;->size:J

    .line 1616
    return-object v4
.end method

.method public close()V
    .registers 1

    .line 1506
    return-void
.end method

.method public completeSegmentByteCount()J
    .registers 6

    .line 260
    iget-wide v2, p0, Lokio/Buffer;->size:J

    .line 261
    .local v2, "result":J
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_b

    const-wide/16 v0, 0x0

    return-wide v0

    .line 264
    :cond_b
    iget-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    iget-object v4, v0, Lokio/Segment;->prev:Lokio/Segment;

    .line 265
    .local v4, "tail":Lokio/Segment;
    iget v0, v4, Lokio/Segment;->limit:I

    const/16 v1, 0x2000

    if-ge v0, v1, :cond_20

    iget-boolean v0, v4, Lokio/Segment;->owner:Z

    if-eqz v0, :cond_20

    .line 266
    iget v0, v4, Lokio/Segment;->limit:I

    iget v1, v4, Lokio/Segment;->pos:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    sub-long/2addr v2, v0

    .line 269
    :cond_20
    return-wide v2
.end method

.method public copyTo(Lokio/Buffer;JJ)Lokio/Buffer;
    .registers 14
    .param p1, "out"    # Lokio/Buffer;
    .param p2, "offset"    # J
    .param p4, "byteCount"    # J

    .line 167
    if-nez p1, :cond_a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "out == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 168
    :cond_a
    iget-wide v0, p0, Lokio/Buffer;->size:J

    move-wide v2, p2

    move-wide v4, p4

    invoke-static/range {v0 .. v5}, Lokio/Util;->checkOffsetAndCount(JJJ)V

    .line 169
    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-nez v0, :cond_18

    return-object p0

    .line 171
    :cond_18
    iget-wide v0, p1, Lokio/Buffer;->size:J

    add-long/2addr v0, p4

    iput-wide v0, p1, Lokio/Buffer;->size:J

    .line 174
    iget-object v6, p0, Lokio/Buffer;->head:Lokio/Segment;

    .line 175
    .local v6, "s":Lokio/Segment;
    :goto_1f
    iget v0, v6, Lokio/Segment;->limit:I

    iget v1, v6, Lokio/Segment;->pos:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_33

    .line 176
    iget v0, v6, Lokio/Segment;->limit:I

    iget v1, v6, Lokio/Segment;->pos:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    sub-long/2addr p2, v0

    .line 175
    iget-object v6, v6, Lokio/Segment;->next:Lokio/Segment;

    goto :goto_1f

    .line 180
    :cond_33
    :goto_33
    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-lez v0, :cond_6f

    .line 181
    new-instance v7, Lokio/Segment;

    invoke-direct {v7, v6}, Lokio/Segment;-><init>(Lokio/Segment;)V

    .line 182
    .local v7, "copy":Lokio/Segment;
    iget v0, v7, Lokio/Segment;->pos:I

    int-to-long v0, v0

    add-long/2addr v0, p2

    long-to-int v0, v0

    iput v0, v7, Lokio/Segment;->pos:I

    .line 183
    iget v0, v7, Lokio/Segment;->pos:I

    long-to-int v1, p4

    add-int/2addr v0, v1

    iget v1, v7, Lokio/Segment;->limit:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, v7, Lokio/Segment;->limit:I

    .line 184
    iget-object v0, p1, Lokio/Buffer;->head:Lokio/Segment;

    if-nez v0, :cond_5c

    .line 185
    iput-object v7, v7, Lokio/Segment;->prev:Lokio/Segment;

    iput-object v7, v7, Lokio/Segment;->next:Lokio/Segment;

    iput-object v7, p1, Lokio/Buffer;->head:Lokio/Segment;

    goto :goto_63

    .line 187
    :cond_5c
    iget-object v0, p1, Lokio/Buffer;->head:Lokio/Segment;

    iget-object v0, v0, Lokio/Segment;->prev:Lokio/Segment;

    invoke-virtual {v0, v7}, Lokio/Segment;->push(Lokio/Segment;)Lokio/Segment;

    .line 189
    :goto_63
    iget v0, v7, Lokio/Segment;->limit:I

    iget v1, v7, Lokio/Segment;->pos:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    sub-long/2addr p4, v0

    .line 190
    const-wide/16 p2, 0x0

    .line 180
    .end local v7    # "copy":Lokio/Segment;
    iget-object v6, v6, Lokio/Segment;->next:Lokio/Segment;

    goto :goto_33

    .line 193
    :cond_6f
    return-object p0
.end method

.method public emitCompleteSegments()Lokio/Buffer;
    .registers 1

    .line 90
    return-object p0
.end method

.method public bridge synthetic emitCompleteSegments()Lokio/BufferedSink;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 47
    invoke-virtual {p0}, Lokio/Buffer;->emitCompleteSegments()Lokio/Buffer;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 16
    .param p1, "o"    # Ljava/lang/Object;

    .line 1552
    if-ne p0, p1, :cond_4

    const/4 v0, 0x1

    return v0

    .line 1553
    :cond_4
    instance-of v0, p1, Lokio/Buffer;

    if-nez v0, :cond_a

    const/4 v0, 0x0

    return v0

    .line 1554
    :cond_a
    move-object v4, p1

    check-cast v4, Lokio/Buffer;

    .line 1555
    .local v4, "that":Lokio/Buffer;
    iget-wide v0, p0, Lokio/Buffer;->size:J

    iget-wide v2, v4, Lokio/Buffer;->size:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_17

    const/4 v0, 0x0

    return v0

    .line 1556
    :cond_17
    iget-wide v0, p0, Lokio/Buffer;->size:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_21

    const/4 v0, 0x1

    return v0

    .line 1558
    :cond_21
    iget-object v5, p0, Lokio/Buffer;->head:Lokio/Segment;

    .line 1559
    .local v5, "sa":Lokio/Segment;
    iget-object v6, v4, Lokio/Buffer;->head:Lokio/Segment;

    .line 1560
    .local v6, "sb":Lokio/Segment;
    iget v7, v5, Lokio/Segment;->pos:I

    .line 1561
    .local v7, "posA":I
    iget v8, v6, Lokio/Segment;->pos:I

    .line 1563
    .local v8, "posB":I
    const-wide/16 v9, 0x0

    .local v9, "pos":J
    :goto_2b
    iget-wide v0, p0, Lokio/Buffer;->size:J

    cmp-long v0, v9, v0

    if-gez v0, :cond_69

    .line 1564
    iget v0, v5, Lokio/Segment;->limit:I

    sub-int/2addr v0, v7

    iget v1, v6, Lokio/Segment;->limit:I

    sub-int/2addr v1, v8

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-long v11, v0

    .line 1566
    .local v11, "count":J
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_3d
    int-to-long v0, v13

    cmp-long v0, v0, v11

    if-gez v0, :cond_57

    .line 1567
    iget-object v0, v5, Lokio/Segment;->data:[B

    move v1, v7

    add-int/lit8 v7, v7, 0x1

    aget-byte v0, v0, v1

    iget-object v1, v6, Lokio/Segment;->data:[B

    move v2, v8

    add-int/lit8 v8, v8, 0x1

    aget-byte v1, v1, v2

    if-eq v0, v1, :cond_54

    const/4 v0, 0x0

    return v0

    .line 1566
    :cond_54
    add-int/lit8 v13, v13, 0x1

    goto :goto_3d

    .line 1570
    .end local v13    # "i":I
    :cond_57
    iget v0, v5, Lokio/Segment;->limit:I

    if-ne v7, v0, :cond_5f

    .line 1571
    iget-object v5, v5, Lokio/Segment;->next:Lokio/Segment;

    .line 1572
    iget v7, v5, Lokio/Segment;->pos:I

    .line 1575
    :cond_5f
    iget v0, v6, Lokio/Segment;->limit:I

    if-ne v8, v0, :cond_67

    .line 1576
    iget-object v6, v6, Lokio/Segment;->next:Lokio/Segment;

    .line 1577
    iget v8, v6, Lokio/Segment;->pos:I

    .line 1563
    :cond_67
    add-long/2addr v9, v11

    goto :goto_2b

    .line 1581
    .end local v9    # "pos":J
    .end local v11    # "count":J
    :cond_69
    const/4 v0, 0x1

    return v0
.end method

.method public exhausted()Z
    .registers 5

    .line 98
    iget-wide v0, p0, Lokio/Buffer;->size:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method public flush()V
    .registers 1

    .line 1503
    return-void
.end method

.method public getByte(J)B
    .registers 11
    .param p1, "pos"    # J

    .line 295
    iget-wide v0, p0, Lokio/Buffer;->size:J

    move-wide v2, p1

    const-wide/16 v4, 0x1

    invoke-static/range {v0 .. v5}, Lokio/Util;->checkOffsetAndCount(JJJ)V

    .line 296
    iget-object v6, p0, Lokio/Buffer;->head:Lokio/Segment;

    .line 297
    .local v6, "s":Lokio/Segment;
    :goto_a
    iget v0, v6, Lokio/Segment;->limit:I

    iget v1, v6, Lokio/Segment;->pos:I

    sub-int v7, v0, v1

    .line 298
    .local v7, "segmentByteCount":I
    int-to-long v0, v7

    cmp-long v0, p1, v0

    if-gez v0, :cond_1e

    iget-object v0, v6, Lokio/Segment;->data:[B

    iget v1, v6, Lokio/Segment;->pos:I

    long-to-int v2, p1

    add-int/2addr v1, v2

    aget-byte v0, v0, v1

    return v0

    .line 299
    :cond_1e
    int-to-long v0, v7

    sub-long/2addr p1, v0

    .line 296
    .end local v7    # "segmentByteCount":I
    iget-object v6, v6, Lokio/Segment;->next:Lokio/Segment;

    goto :goto_a
.end method

.method public hashCode()I
    .registers 7

    .line 1585
    iget-object v2, p0, Lokio/Buffer;->head:Lokio/Segment;

    .line 1586
    .local v2, "s":Lokio/Segment;
    if-nez v2, :cond_6

    const/4 v0, 0x0

    return v0

    .line 1587
    :cond_6
    const/4 v3, 0x1

    .line 1589
    .local v3, "result":I
    :cond_7
    iget v4, v2, Lokio/Segment;->pos:I

    .local v4, "pos":I
    iget v5, v2, Lokio/Segment;->limit:I

    .local v5, "limit":I
    :goto_b
    if-ge v4, v5, :cond_18

    .line 1590
    mul-int/lit8 v0, v3, 0x1f

    iget-object v1, v2, Lokio/Segment;->data:[B

    aget-byte v1, v1, v4

    add-int v3, v0, v1

    .line 1589
    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    .line 1592
    .end local v4    # "pos":I
    .end local v5    # "limit":I
    :cond_18
    iget-object v2, v2, Lokio/Segment;->next:Lokio/Segment;

    .line 1593
    iget-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    if-ne v2, v0, :cond_7

    .line 1594
    return v3
.end method

.method public indexOf(B)J
    .registers 4
    .param p1, "b"    # B

    .line 1263
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lokio/Buffer;->indexOf(BJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public indexOf(BJ)J
    .registers 12
    .param p1, "b"    # B
    .param p2, "fromIndex"    # J

    .line 1271
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gez v0, :cond_e

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "fromIndex < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1279
    :cond_e
    iget-object v2, p0, Lokio/Buffer;->head:Lokio/Segment;

    .line 1280
    .local v2, "s":Lokio/Segment;
    if-nez v2, :cond_15

    .line 1282
    const-wide/16 v0, -0x1

    return-wide v0

    .line 1283
    :cond_15
    iget-wide v0, p0, Lokio/Buffer;->size:J

    sub-long/2addr v0, p2

    cmp-long v0, v0, p2

    if-gez v0, :cond_2c

    .line 1285
    iget-wide v3, p0, Lokio/Buffer;->size:J

    .line 1286
    .local v3, "offset":J
    :goto_1e
    cmp-long v0, v3, p2

    if-lez v0, :cond_3e

    .line 1287
    iget-object v2, v2, Lokio/Segment;->prev:Lokio/Segment;

    .line 1288
    iget v0, v2, Lokio/Segment;->limit:I

    iget v1, v2, Lokio/Segment;->pos:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    sub-long/2addr v3, v0

    goto :goto_1e

    .line 1292
    .end local v3    # "offset":J
    :cond_2c
    const-wide/16 v3, 0x0

    .line 1293
    .local v3, "offset":J
    :goto_2e
    iget v0, v2, Lokio/Segment;->limit:I

    iget v1, v2, Lokio/Segment;->pos:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    add-long/2addr v0, v3

    move-wide v5, v0

    .local v5, "nextOffset":J
    cmp-long v0, v0, p2

    if-gez v0, :cond_3e

    .line 1294
    iget-object v2, v2, Lokio/Segment;->next:Lokio/Segment;

    .line 1295
    move-wide v3, v5

    goto :goto_2e

    .line 1301
    .end local v5    # "nextOffset":J
    :cond_3e
    :goto_3e
    iget-wide v0, p0, Lokio/Buffer;->size:J

    cmp-long v0, v3, v0

    if-gez v0, :cond_69

    .line 1302
    iget-object v5, v2, Lokio/Segment;->data:[B

    .line 1303
    .local v5, "data":[B
    iget v0, v2, Lokio/Segment;->pos:I

    int-to-long v0, v0

    add-long/2addr v0, p2

    sub-long/2addr v0, v3

    long-to-int v6, v0

    .local v6, "pos":I
    iget v7, v2, Lokio/Segment;->limit:I

    .local v7, "limit":I
    :goto_4e
    if-ge v6, v7, :cond_5e

    .line 1304
    aget-byte v0, v5, v6

    if-ne v0, p1, :cond_5b

    .line 1305
    iget v0, v2, Lokio/Segment;->pos:I

    sub-int v0, v6, v0

    int-to-long v0, v0

    add-long/2addr v0, v3

    return-wide v0

    .line 1303
    :cond_5b
    add-int/lit8 v6, v6, 0x1

    goto :goto_4e

    .line 1310
    .end local v6    # "pos":I
    .end local v7    # "limit":I
    :cond_5e
    iget v0, v2, Lokio/Segment;->limit:I

    iget v1, v2, Lokio/Segment;->pos:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    add-long/2addr v3, v0

    .line 1311
    move-wide p2, v3

    .line 1312
    iget-object v2, v2, Lokio/Segment;->next:Lokio/Segment;

    .line 1313
    .end local v5    # "data":[B
    goto :goto_3e

    .line 1315
    :cond_69
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public read([BII)I
    .registers 12
    .param p1, "sink"    # [B
    .param p2, "offset"    # I
    .param p3, "byteCount"    # I

    .line 775
    array-length v0, p1

    int-to-long v0, v0

    int-to-long v2, p2

    int-to-long v4, p3

    invoke-static/range {v0 .. v5}, Lokio/Util;->checkOffsetAndCount(JJJ)V

    .line 777
    iget-object v6, p0, Lokio/Buffer;->head:Lokio/Segment;

    .line 778
    .local v6, "s":Lokio/Segment;
    if-nez v6, :cond_d

    const/4 v0, -0x1

    return v0

    .line 779
    :cond_d
    iget v0, v6, Lokio/Segment;->limit:I

    iget v1, v6, Lokio/Segment;->pos:I

    sub-int/2addr v0, v1

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 780
    .local v7, "toCopy":I
    iget-object v0, v6, Lokio/Segment;->data:[B

    iget v1, v6, Lokio/Segment;->pos:I

    invoke-static {v0, v1, p1, p2, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 782
    iget v0, v6, Lokio/Segment;->pos:I

    add-int/2addr v0, v7

    iput v0, v6, Lokio/Segment;->pos:I

    .line 783
    iget-wide v0, p0, Lokio/Buffer;->size:J

    int-to-long v2, v7

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lokio/Buffer;->size:J

    .line 785
    iget v0, v6, Lokio/Segment;->pos:I

    iget v1, v6, Lokio/Segment;->limit:I

    if-ne v0, v1, :cond_37

    .line 786
    invoke-virtual {v6}, Lokio/Segment;->pop()Lokio/Segment;

    move-result-object v0

    iput-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    .line 787
    invoke-static {v6}, Lokio/SegmentPool;->recycle(Lokio/Segment;)V

    .line 790
    :cond_37
    return v7
.end method

.method public read(Lokio/Buffer;J)J
    .registers 8
    .param p1, "sink"    # Lokio/Buffer;
    .param p2, "byteCount"    # J

    .line 1254
    if-nez p1, :cond_a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sink == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1255
    :cond_a
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gez v0, :cond_29

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1256
    :cond_29
    iget-wide v0, p0, Lokio/Buffer;->size:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_34

    const-wide/16 v0, -0x1

    return-wide v0

    .line 1257
    :cond_34
    iget-wide v0, p0, Lokio/Buffer;->size:J

    cmp-long v0, p2, v0

    if-lez v0, :cond_3c

    iget-wide p2, p0, Lokio/Buffer;->size:J

    .line 1258
    :cond_3c
    invoke-virtual {p1, p0, p2, p3}, Lokio/Buffer;->write(Lokio/Buffer;J)V

    .line 1259
    return-wide p2
.end method

.method public readByte()B
    .registers 10

    .line 273
    iget-wide v0, p0, Lokio/Buffer;->size:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_10

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "size == 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 275
    :cond_10
    iget-object v4, p0, Lokio/Buffer;->head:Lokio/Segment;

    .line 276
    .local v4, "segment":Lokio/Segment;
    iget v5, v4, Lokio/Segment;->pos:I

    .line 277
    .local v5, "pos":I
    iget v6, v4, Lokio/Segment;->limit:I

    .line 279
    .local v6, "limit":I
    iget-object v7, v4, Lokio/Segment;->data:[B

    .line 280
    .local v7, "data":[B
    move v0, v5

    add-int/lit8 v5, v5, 0x1

    aget-byte v8, v7, v0

    .line 281
    .local v8, "b":B
    iget-wide v0, p0, Lokio/Buffer;->size:J

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lokio/Buffer;->size:J

    .line 283
    if-ne v5, v6, :cond_30

    .line 284
    invoke-virtual {v4}, Lokio/Segment;->pop()Lokio/Segment;

    move-result-object v0

    iput-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    .line 285
    invoke-static {v4}, Lokio/SegmentPool;->recycle(Lokio/Segment;)V

    goto :goto_32

    .line 287
    :cond_30
    iput v5, v4, Lokio/Segment;->pos:I

    .line 290
    :goto_32
    return v8
.end method

.method public readByteArray()[B
    .registers 4

    .line 744
    :try_start_0
    iget-wide v0, p0, Lokio/Buffer;->size:J

    invoke-virtual {p0, v0, v1}, Lokio/Buffer;->readByteArray(J)[B
    :try_end_5
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_5} :catch_7

    move-result-object v0

    return-object v0

    .line 745
    :catch_7
    move-exception v2

    .line 746
    .local v2, "e":Ljava/io/EOFException;
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public readByteArray(J)[B
    .registers 10
    .param p1, "byteCount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    .line 751
    iget-wide v0, p0, Lokio/Buffer;->size:J

    move-wide v4, p1

    const-wide/16 v2, 0x0

    invoke-static/range {v0 .. v5}, Lokio/Util;->checkOffsetAndCount(JJJ)V

    .line 752
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p1, v0

    if-lez v0, :cond_28

    .line 753
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "byteCount > Integer.MAX_VALUE: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 756
    :cond_28
    long-to-int v0, p1

    new-array v6, v0, [B

    .line 757
    .local v6, "result":[B
    invoke-virtual {p0, v6}, Lokio/Buffer;->readFully([B)V

    .line 758
    return-object v6
.end method

.method public readByteString()Lokio/ByteString;
    .registers 3

    .line 525
    new-instance v0, Lokio/ByteString;

    invoke-virtual {p0}, Lokio/Buffer;->readByteArray()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lokio/ByteString;-><init>([B)V

    return-object v0
.end method

.method public readByteString(J)Lokio/ByteString;
    .registers 5
    .param p1, "byteCount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    .line 529
    new-instance v0, Lokio/ByteString;

    invoke-virtual {p0, p1, p2}, Lokio/Buffer;->readByteArray(J)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lokio/ByteString;-><init>([B)V

    return-object v0
.end method

.method public readFully([B)V
    .registers 5
    .param p1, "sink"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    .line 766
    const/4 v1, 0x0

    .line 767
    .local v1, "offset":I
    :goto_1
    array-length v0, p1

    if-ge v1, v0, :cond_15

    .line 768
    array-length v0, p1

    sub-int/2addr v0, v1

    invoke-virtual {p0, p1, v1, v0}, Lokio/Buffer;->read([BII)I

    move-result v2

    .line 769
    .local v2, "read":I
    const/4 v0, -0x1

    if-ne v2, v0, :cond_13

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 770
    :cond_13
    add-int/2addr v1, v2

    .line 771
    .end local v2    # "read":I
    goto :goto_1

    .line 772
    :cond_15
    return-void
.end method

.method public readHexadecimalUnsignedLong()J
    .registers 16

    .line 469
    iget-wide v0, p0, Lokio/Buffer;->size:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_10

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "size == 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 471
    :cond_10
    const-wide/16 v4, 0x0

    .line 472
    .local v4, "value":J
    const/4 v6, 0x0

    .line 473
    .local v6, "seen":I
    const/4 v7, 0x0

    .line 476
    .local v7, "done":Z
    :cond_14
    iget-object v8, p0, Lokio/Buffer;->head:Lokio/Segment;

    .line 478
    .local v8, "segment":Lokio/Segment;
    iget-object v9, v8, Lokio/Segment;->data:[B

    .line 479
    .local v9, "data":[B
    iget v10, v8, Lokio/Segment;->pos:I

    .line 480
    .local v10, "pos":I
    iget v11, v8, Lokio/Segment;->limit:I

    .line 482
    .local v11, "limit":I
    :goto_1c
    if-ge v10, v11, :cond_a3

    .line 485
    aget-byte v13, v9, v10

    .line 486
    .local v13, "b":B
    const/16 v0, 0x30

    if-lt v13, v0, :cond_2b

    const/16 v0, 0x39

    if-gt v13, v0, :cond_2b

    .line 487
    add-int/lit8 v12, v13, -0x30

    .local v12, "digit":I
    goto :goto_66

    .line 488
    .end local v12    # "digit":I
    :cond_2b
    const/16 v0, 0x61

    if-lt v13, v0, :cond_38

    const/16 v0, 0x66

    if-gt v13, v0, :cond_38

    .line 489
    add-int/lit8 v0, v13, -0x61

    add-int/lit8 v12, v0, 0xa

    .local v12, "digit":I
    goto :goto_66

    .line 490
    .end local v12    # "digit":I
    :cond_38
    const/16 v0, 0x41

    if-lt v13, v0, :cond_45

    const/16 v0, 0x46

    if-gt v13, v0, :cond_45

    .line 491
    add-int/lit8 v0, v13, -0x41

    add-int/lit8 v12, v0, 0xa

    .local v12, "digit":I
    goto :goto_66

    .line 493
    .end local v12    # "digit":I
    :cond_45
    if-nez v6, :cond_64

    .line 494
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected leading [0-9a-fA-F] character but was 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 495
    invoke-static {v13}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 498
    :cond_64
    const/4 v7, 0x1

    .line 499
    goto :goto_a3

    .line 503
    .local v12, "digit":I
    :goto_66
    const-wide/high16 v0, -0x1000000000000000L    # -3.105036184601418E231

    and-long/2addr v0, v4

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_99

    .line 504
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, v4, v5}, Lokio/Buffer;->writeHexadecimalUnsignedLong(J)Lokio/Buffer;

    move-result-object v0

    invoke-virtual {v0, v13}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    move-result-object v14

    .line 505
    .local v14, "buffer":Lokio/Buffer;
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Number too large: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v14}, Lokio/Buffer;->readUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 508
    .end local v14    # "buffer":Lokio/Buffer;
    :cond_99
    const/4 v0, 0x4

    shl-long/2addr v4, v0

    .line 509
    int-to-long v0, v12

    or-long/2addr v4, v0

    .line 482
    .end local v12    # "digit":I
    .end local v13    # "b":B
    add-int/lit8 v10, v10, 0x1

    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1c

    .line 512
    :cond_a3
    :goto_a3
    if-ne v10, v11, :cond_af

    .line 513
    invoke-virtual {v8}, Lokio/Segment;->pop()Lokio/Segment;

    move-result-object v0

    iput-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    .line 514
    invoke-static {v8}, Lokio/SegmentPool;->recycle(Lokio/Segment;)V

    goto :goto_b1

    .line 516
    :cond_af
    iput v10, v8, Lokio/Segment;->pos:I

    .line 518
    .end local v8    # "segment":Lokio/Segment;
    .end local v9    # "data":[B
    .end local v10    # "pos":I
    .end local v11    # "limit":I
    :goto_b1
    if-nez v7, :cond_b7

    iget-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    if-nez v0, :cond_14

    .line 520
    :cond_b7
    iget-wide v0, p0, Lokio/Buffer;->size:J

    int-to-long v2, v6

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lokio/Buffer;->size:J

    .line 521
    return-wide v4
.end method

.method public readInt()I
    .registers 10

    .line 333
    iget-wide v0, p0, Lokio/Buffer;->size:J

    const-wide/16 v2, 0x4

    cmp-long v0, v0, v2

    if-gez v0, :cond_23

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "size < 4: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lokio/Buffer;->size:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 335
    :cond_23
    iget-object v4, p0, Lokio/Buffer;->head:Lokio/Segment;

    .line 336
    .local v4, "segment":Lokio/Segment;
    iget v5, v4, Lokio/Segment;->pos:I

    .line 337
    .local v5, "pos":I
    iget v6, v4, Lokio/Segment;->limit:I

    .line 340
    .local v6, "limit":I
    sub-int v0, v6, v5

    const/4 v1, 0x4

    if-ge v0, v1, :cond_50

    .line 341
    invoke-virtual {p0}, Lokio/Buffer;->readByte()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    .line 342
    invoke-virtual {p0}, Lokio/Buffer;->readByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    .line 343
    invoke-virtual {p0}, Lokio/Buffer;->readByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    .line 344
    invoke-virtual {p0}, Lokio/Buffer;->readByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    .line 341
    return v0

    .line 347
    :cond_50
    iget-object v7, v4, Lokio/Segment;->data:[B

    .line 348
    .local v7, "data":[B
    move v0, v5

    add-int/lit8 v5, v5, 0x1

    aget-byte v0, v7, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    move v1, v5

    add-int/lit8 v5, v5, 0x1

    aget-byte v1, v7, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    move v1, v5

    add-int/lit8 v5, v5, 0x1

    aget-byte v1, v7, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    move v1, v5

    add-int/lit8 v5, v5, 0x1

    aget-byte v1, v7, v1

    and-int/lit16 v1, v1, 0xff

    or-int v8, v0, v1

    .line 352
    .local v8, "i":I
    iget-wide v0, p0, Lokio/Buffer;->size:J

    const-wide/16 v2, 0x4

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lokio/Buffer;->size:J

    .line 354
    if-ne v5, v6, :cond_8b

    .line 355
    invoke-virtual {v4}, Lokio/Segment;->pop()Lokio/Segment;

    move-result-object v0

    iput-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    .line 356
    invoke-static {v4}, Lokio/SegmentPool;->recycle(Lokio/Segment;)V

    goto :goto_8d

    .line 358
    :cond_8b
    iput v5, v4, Lokio/Segment;->pos:I

    .line 361
    :goto_8d
    return v8
.end method

.method public readIntLe()I
    .registers 2

    .line 403
    invoke-virtual {p0}, Lokio/Buffer;->readInt()I

    move-result v0

    invoke-static {v0}, Lokio/Util;->reverseBytesInt(I)I

    move-result v0

    return v0
.end method

.method public readShort()S
    .registers 10

    .line 304
    iget-wide v0, p0, Lokio/Buffer;->size:J

    const-wide/16 v2, 0x2

    cmp-long v0, v0, v2

    if-gez v0, :cond_23

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "size < 2: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lokio/Buffer;->size:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 306
    :cond_23
    iget-object v4, p0, Lokio/Buffer;->head:Lokio/Segment;

    .line 307
    .local v4, "segment":Lokio/Segment;
    iget v5, v4, Lokio/Segment;->pos:I

    .line 308
    .local v5, "pos":I
    iget v6, v4, Lokio/Segment;->limit:I

    .line 311
    .local v6, "limit":I
    sub-int v0, v6, v5

    const/4 v1, 0x2

    if-ge v0, v1, :cond_40

    .line 312
    invoke-virtual {p0}, Lokio/Buffer;->readByte()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    .line 313
    invoke-virtual {p0}, Lokio/Buffer;->readByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int v7, v0, v1

    .line 314
    .local v7, "s":I
    int-to-short v0, v7

    return v0

    .line 317
    .end local v7    # "s":I
    :cond_40
    iget-object v7, v4, Lokio/Segment;->data:[B

    .line 318
    .local v7, "data":[B
    move v0, v5

    add-int/lit8 v5, v5, 0x1

    aget-byte v0, v7, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    move v1, v5

    add-int/lit8 v5, v5, 0x1

    aget-byte v1, v7, v1

    and-int/lit16 v1, v1, 0xff

    or-int v8, v0, v1

    .line 320
    .local v8, "s":I
    iget-wide v0, p0, Lokio/Buffer;->size:J

    const-wide/16 v2, 0x2

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lokio/Buffer;->size:J

    .line 322
    if-ne v5, v6, :cond_67

    .line 323
    invoke-virtual {v4}, Lokio/Segment;->pop()Lokio/Segment;

    move-result-object v0

    iput-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    .line 324
    invoke-static {v4}, Lokio/SegmentPool;->recycle(Lokio/Segment;)V

    goto :goto_69

    .line 326
    :cond_67
    iput v5, v4, Lokio/Segment;->pos:I

    .line 329
    :goto_69
    int-to-short v0, v8

    return v0
.end method

.method public readShortLe()S
    .registers 2

    .line 399
    invoke-virtual {p0}, Lokio/Buffer;->readShort()S

    move-result v0

    invoke-static {v0}, Lokio/Util;->reverseBytesShort(S)S

    move-result v0

    return v0
.end method

.method public readString(JLjava/nio/charset/Charset;)Ljava/lang/String;
    .registers 12
    .param p1, "byteCount"    # J
    .param p3, "charset"    # Ljava/nio/charset/Charset;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    .line 606
    iget-wide v0, p0, Lokio/Buffer;->size:J

    move-wide v4, p1

    const-wide/16 v2, 0x0

    invoke-static/range {v0 .. v5}, Lokio/Util;->checkOffsetAndCount(JJJ)V

    .line 607
    if-nez p3, :cond_12

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "charset == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 608
    :cond_12
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p1, v0

    if-lez v0, :cond_32

    .line 609
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "byteCount > Integer.MAX_VALUE: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 611
    :cond_32
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_3b

    const-string v0, ""

    return-object v0

    .line 613
    :cond_3b
    iget-object v6, p0, Lokio/Buffer;->head:Lokio/Segment;

    .line 614
    .local v6, "s":Lokio/Segment;
    iget v0, v6, Lokio/Segment;->pos:I

    int-to-long v0, v0

    add-long/2addr v0, p1

    iget v2, v6, Lokio/Segment;->limit:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_52

    .line 616
    new-instance v0, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lokio/Buffer;->readByteArray(J)[B

    move-result-object v1

    invoke-direct {v0, v1, p3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    return-object v0

    .line 619
    :cond_52
    new-instance v7, Ljava/lang/String;

    iget-object v0, v6, Lokio/Segment;->data:[B

    iget v1, v6, Lokio/Segment;->pos:I

    long-to-int v2, p1

    invoke-direct {v7, v0, v1, v2, p3}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    .line 620
    .local v7, "result":Ljava/lang/String;
    iget v0, v6, Lokio/Segment;->pos:I

    int-to-long v0, v0

    add-long/2addr v0, p1

    long-to-int v0, v0

    iput v0, v6, Lokio/Segment;->pos:I

    .line 621
    iget-wide v0, p0, Lokio/Buffer;->size:J

    sub-long/2addr v0, p1

    iput-wide v0, p0, Lokio/Buffer;->size:J

    .line 623
    iget v0, v6, Lokio/Segment;->pos:I

    iget v1, v6, Lokio/Segment;->limit:I

    if-ne v0, v1, :cond_77

    .line 624
    invoke-virtual {v6}, Lokio/Segment;->pop()Lokio/Segment;

    move-result-object v0

    iput-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    .line 625
    invoke-static {v6}, Lokio/SegmentPool;->recycle(Lokio/Segment;)V

    .line 628
    :cond_77
    return-object v7
.end method

.method public readUtf8()Ljava/lang/String;
    .registers 5

    .line 587
    :try_start_0
    iget-wide v0, p0, Lokio/Buffer;->size:J

    sget-object v2, Lokio/Util;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v0, v1, v2}, Lokio/Buffer;->readString(JLjava/nio/charset/Charset;)Ljava/lang/String;
    :try_end_7
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_7} :catch_9

    move-result-object v0

    return-object v0

    .line 588
    :catch_9
    move-exception v3

    .line 589
    .local v3, "e":Ljava/io/EOFException;
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public readUtf8(J)Ljava/lang/String;
    .registers 4
    .param p1, "byteCount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    .line 594
    sget-object v0, Lokio/Util;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p0, p1, p2, v0}, Lokio/Buffer;->readString(JLjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method readUtf8Line(J)Ljava/lang/String;
    .registers 6
    .param p1, "newline"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    .line 653
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_20

    const-wide/16 v0, 0x1

    sub-long v0, p1, v0

    invoke-virtual {p0, v0, v1}, Lokio/Buffer;->getByte(J)B

    move-result v0

    const/16 v1, 0xd

    if-ne v0, v1, :cond_20

    .line 655
    const-wide/16 v0, 0x1

    sub-long v0, p1, v0

    invoke-virtual {p0, v0, v1}, Lokio/Buffer;->readUtf8(J)Ljava/lang/String;

    move-result-object v2

    .line 656
    .local v2, "result":Ljava/lang/String;
    const-wide/16 v0, 0x2

    invoke-virtual {p0, v0, v1}, Lokio/Buffer;->skip(J)V

    .line 657
    return-object v2

    .line 661
    .end local v2    # "result":Ljava/lang/String;
    :cond_20
    invoke-virtual {p0, p1, p2}, Lokio/Buffer;->readUtf8(J)Ljava/lang/String;

    move-result-object v2

    .line 662
    .local v2, "result":Ljava/lang/String;
    const-wide/16 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lokio/Buffer;->skip(J)V

    .line 663
    return-object v2
.end method

.method public readUtf8LineStrict()Ljava/lang/String;
    .registers 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    .line 642
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lokio/Buffer;->indexOf(B)J

    move-result-wide v6

    .line 643
    .local v6, "newline":J
    const-wide/16 v0, -0x1

    cmp-long v0, v6, v0

    if-nez v0, :cond_55

    .line 644
    new-instance v8, Lokio/Buffer;

    invoke-direct {v8}, Lokio/Buffer;-><init>()V

    .line 645
    .local v8, "data":Lokio/Buffer;
    move-object v0, p0

    move-object v1, v8

    iget-wide v2, p0, Lokio/Buffer;->size:J

    const-wide/16 v4, 0x20

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    const-wide/16 v2, 0x0

    invoke-virtual/range {v0 .. v5}, Lokio/Buffer;->copyTo(Lokio/Buffer;JJ)Lokio/Buffer;

    .line 646
    new-instance v0, Ljava/io/EOFException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\\n not found: size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lokio/Buffer;->size()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " content="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 647
    invoke-virtual {v8}, Lokio/Buffer;->readByteString()Lokio/ByteString;

    move-result-object v2

    invoke-virtual {v2}, Lokio/ByteString;->hex()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\u2026"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 649
    .end local v8    # "data":Lokio/Buffer;
    :cond_55
    invoke-virtual {p0, v6, v7}, Lokio/Buffer;->readUtf8Line(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public require(J)V
    .registers 5
    .param p1, "byteCount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    .line 102
    iget-wide v0, p0, Lokio/Buffer;->size:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_c

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 103
    :cond_c
    return-void
.end method

.method public size()J
    .registers 3

    .line 60
    iget-wide v0, p0, Lokio/Buffer;->size:J

    return-wide v0
.end method

.method public skip(J)V
    .registers 9
    .param p1, "byteCount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;
        }
    .end annotation

    .line 807
    :goto_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_44

    .line 808
    iget-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    if-nez v0, :cond_10

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 810
    :cond_10
    iget-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    iget v0, v0, Lokio/Segment;->limit:I

    iget-object v1, p0, Lokio/Buffer;->head:Lokio/Segment;

    iget v1, v1, Lokio/Segment;->pos:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v4, v0

    .line 811
    .local v4, "toSkip":I
    iget-wide v0, p0, Lokio/Buffer;->size:J

    int-to-long v2, v4

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lokio/Buffer;->size:J

    .line 812
    int-to-long v0, v4

    sub-long/2addr p1, v0

    .line 813
    iget-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    iget v1, v0, Lokio/Segment;->pos:I

    add-int/2addr v1, v4

    iput v1, v0, Lokio/Segment;->pos:I

    .line 815
    iget-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    iget v0, v0, Lokio/Segment;->pos:I

    iget-object v1, p0, Lokio/Buffer;->head:Lokio/Segment;

    iget v1, v1, Lokio/Segment;->limit:I

    if-ne v0, v1, :cond_43

    .line 816
    iget-object v5, p0, Lokio/Buffer;->head:Lokio/Segment;

    .line 817
    .local v5, "toRecycle":Lokio/Segment;
    invoke-virtual {v5}, Lokio/Segment;->pop()Lokio/Segment;

    move-result-object v0

    iput-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    .line 818
    invoke-static {v5}, Lokio/SegmentPool;->recycle(Lokio/Segment;)V

    .line 820
    .end local v4    # "toSkip":I
    .end local v5    # "toRecycle":Lokio/Segment;
    :cond_43
    goto :goto_0

    .line 821
    :cond_44
    return-void
.end method

.method public snapshot()Lokio/ByteString;
    .registers 5

    .line 1621
    iget-wide v0, p0, Lokio/Buffer;->size:J

    const-wide/32 v2, 0x7fffffff

    cmp-long v0, v0, v2

    if-lez v0, :cond_24

    .line 1622
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "size > Integer.MAX_VALUE: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lokio/Buffer;->size:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1624
    :cond_24
    iget-wide v0, p0, Lokio/Buffer;->size:J

    long-to-int v0, v0

    invoke-virtual {p0, v0}, Lokio/Buffer;->snapshot(I)Lokio/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public snapshot(I)Lokio/ByteString;
    .registers 3
    .param p1, "byteCount"    # I

    .line 1631
    if-nez p1, :cond_5

    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    return-object v0

    .line 1632
    :cond_5
    new-instance v0, Lokio/SegmentedByteString;

    invoke-direct {v0, p0, p1}, Lokio/SegmentedByteString;-><init>(Lokio/Buffer;I)V

    return-object v0
.end method

.method public timeout()Lokio/Timeout;
    .registers 2

    .line 1509
    sget-object v0, Lokio/Timeout;->NONE:Lokio/Timeout;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .line 1602
    invoke-virtual {p0}, Lokio/Buffer;->snapshot()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method writableSegment(I)Lokio/Segment;
    .registers 6
    .param p1, "minimumCapacity"    # I

    .line 1148
    const/4 v0, 0x1

    if-lt p1, v0, :cond_7

    const/16 v0, 0x2000

    if-le p1, v0, :cond_d

    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1150
    :cond_d
    iget-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    if-nez v0, :cond_22

    .line 1151
    invoke-static {}, Lokio/SegmentPool;->take()Lokio/Segment;

    move-result-object v0

    iput-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    .line 1152
    iget-object v1, p0, Lokio/Buffer;->head:Lokio/Segment;

    iget-object v2, p0, Lokio/Buffer;->head:Lokio/Segment;

    iget-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    iput-object v0, v2, Lokio/Segment;->prev:Lokio/Segment;

    iput-object v0, v1, Lokio/Segment;->next:Lokio/Segment;

    return-object v0

    .line 1155
    :cond_22
    iget-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    iget-object v3, v0, Lokio/Segment;->prev:Lokio/Segment;

    .line 1156
    .local v3, "tail":Lokio/Segment;
    iget v0, v3, Lokio/Segment;->limit:I

    add-int/2addr v0, p1

    const/16 v1, 0x2000

    if-gt v0, v1, :cond_31

    iget-boolean v0, v3, Lokio/Segment;->owner:Z

    if-nez v0, :cond_39

    .line 1157
    :cond_31
    invoke-static {}, Lokio/SegmentPool;->take()Lokio/Segment;

    move-result-object v0

    invoke-virtual {v3, v0}, Lokio/Segment;->push(Lokio/Segment;)Lokio/Segment;

    move-result-object v3

    .line 1159
    :cond_39
    return-object v3
.end method

.method public write(Lokio/ByteString;)Lokio/Buffer;
    .registers 4
    .param p1, "byteString"    # Lokio/ByteString;

    .line 824
    if-nez p1, :cond_a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "byteString == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 825
    :cond_a
    invoke-virtual {p1, p0}, Lokio/ByteString;->write(Lokio/Buffer;)V

    .line 826
    return-object p0
.end method

.method public write([B)Lokio/Buffer;
    .registers 4
    .param p1, "source"    # [B

    .line 967
    if-nez p1, :cond_a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 968
    :cond_a
    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lokio/Buffer;->write([BII)Lokio/Buffer;

    move-result-object v0

    return-object v0
.end method

.method public write([BII)Lokio/Buffer;
    .registers 13
    .param p1, "source"    # [B
    .param p2, "offset"    # I
    .param p3, "byteCount"    # I

    .line 972
    if-nez p1, :cond_a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 973
    :cond_a
    array-length v0, p1

    int-to-long v0, v0

    int-to-long v2, p2

    int-to-long v4, p3

    invoke-static/range {v0 .. v5}, Lokio/Util;->checkOffsetAndCount(JJJ)V

    .line 975
    add-int v6, p2, p3

    .line 976
    .local v6, "limit":I
    :goto_13
    if-ge p2, v6, :cond_32

    .line 977
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lokio/Buffer;->writableSegment(I)Lokio/Segment;

    move-result-object v7

    .line 979
    .local v7, "tail":Lokio/Segment;
    sub-int v0, v6, p2

    iget v1, v7, Lokio/Segment;->limit:I

    rsub-int v1, v1, 0x2000

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 980
    .local v8, "toCopy":I
    iget-object v0, v7, Lokio/Segment;->data:[B

    iget v1, v7, Lokio/Segment;->limit:I

    invoke-static {p1, p2, v0, v1, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 982
    add-int/2addr p2, v8

    .line 983
    iget v0, v7, Lokio/Segment;->limit:I

    add-int/2addr v0, v8

    iput v0, v7, Lokio/Segment;->limit:I

    .line 984
    .end local v7    # "tail":Lokio/Segment;
    .end local v8    # "toCopy":I
    goto :goto_13

    .line 986
    :cond_32
    iget-wide v0, p0, Lokio/Buffer;->size:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, Lokio/Buffer;->size:J

    .line 987
    return-object p0
.end method

.method public bridge synthetic write(Lokio/ByteString;)Lokio/BufferedSink;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 47
    invoke-virtual {p0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic write([B)Lokio/BufferedSink;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 47
    invoke-virtual {p0, p1}, Lokio/Buffer;->write([B)Lokio/Buffer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic write([BII)Lokio/BufferedSink;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 47
    invoke-virtual {p0, p1, p2, p3}, Lokio/Buffer;->write([BII)Lokio/Buffer;

    move-result-object v0

    return-object v0
.end method

.method public write(Lokio/Buffer;J)V
    .registers 14
    .param p1, "source"    # Lokio/Buffer;
    .param p2, "byteCount"    # J

    .line 1213
    if-nez p1, :cond_a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1214
    :cond_a
    if-ne p1, p0, :cond_14

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == this"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1215
    :cond_14
    iget-wide v0, p1, Lokio/Buffer;->size:J

    move-wide v4, p2

    const-wide/16 v2, 0x0

    invoke-static/range {v0 .. v5}, Lokio/Util;->checkOffsetAndCount(JJJ)V

    .line 1217
    :goto_1c
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_a5

    .line 1219
    iget-object v0, p1, Lokio/Buffer;->head:Lokio/Segment;

    iget v0, v0, Lokio/Segment;->limit:I

    iget-object v1, p1, Lokio/Buffer;->head:Lokio/Segment;

    iget v1, v1, Lokio/Segment;->pos:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    cmp-long v0, p2, v0

    if-gez v0, :cond_6e

    .line 1220
    iget-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    if-eqz v0, :cond_39

    iget-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    iget-object v6, v0, Lokio/Segment;->prev:Lokio/Segment;

    goto :goto_3a

    :cond_39
    const/4 v6, 0x0

    .line 1221
    .local v6, "tail":Lokio/Segment;
    :goto_3a
    if-eqz v6, :cond_65

    iget-boolean v0, v6, Lokio/Segment;->owner:Z

    if-eqz v0, :cond_65

    iget v0, v6, Lokio/Segment;->limit:I

    int-to-long v0, v0

    add-long/2addr v0, p2

    iget-boolean v2, v6, Lokio/Segment;->shared:Z

    if-eqz v2, :cond_4a

    const/4 v2, 0x0

    goto :goto_4c

    :cond_4a
    iget v2, v6, Lokio/Segment;->pos:I

    :goto_4c
    int-to-long v2, v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x2000

    cmp-long v0, v0, v2

    if-gtz v0, :cond_65

    .line 1224
    iget-object v0, p1, Lokio/Buffer;->head:Lokio/Segment;

    long-to-int v1, p2

    invoke-virtual {v0, v6, v1}, Lokio/Segment;->writeTo(Lokio/Segment;I)V

    .line 1225
    iget-wide v0, p1, Lokio/Buffer;->size:J

    sub-long/2addr v0, p2

    iput-wide v0, p1, Lokio/Buffer;->size:J

    .line 1226
    iget-wide v0, p0, Lokio/Buffer;->size:J

    add-long/2addr v0, p2

    iput-wide v0, p0, Lokio/Buffer;->size:J

    .line 1227
    return-void

    .line 1231
    :cond_65
    iget-object v0, p1, Lokio/Buffer;->head:Lokio/Segment;

    long-to-int v1, p2

    invoke-virtual {v0, v1}, Lokio/Segment;->split(I)Lokio/Segment;

    move-result-object v0

    iput-object v0, p1, Lokio/Buffer;->head:Lokio/Segment;

    .line 1236
    .end local v6    # "tail":Lokio/Segment;
    :cond_6e
    iget-object v6, p1, Lokio/Buffer;->head:Lokio/Segment;

    .line 1237
    .local v6, "segmentToMove":Lokio/Segment;
    iget v0, v6, Lokio/Segment;->limit:I

    iget v1, v6, Lokio/Segment;->pos:I

    sub-int/2addr v0, v1

    int-to-long v7, v0

    .line 1238
    .local v7, "movedByteCount":J
    invoke-virtual {v6}, Lokio/Segment;->pop()Lokio/Segment;

    move-result-object v0

    iput-object v0, p1, Lokio/Buffer;->head:Lokio/Segment;

    .line 1239
    iget-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    if-nez v0, :cond_8d

    .line 1240
    iput-object v6, p0, Lokio/Buffer;->head:Lokio/Segment;

    .line 1241
    iget-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    iget-object v2, p0, Lokio/Buffer;->head:Lokio/Segment;

    iget-object v1, p0, Lokio/Buffer;->head:Lokio/Segment;

    iput-object v1, v2, Lokio/Segment;->prev:Lokio/Segment;

    iput-object v1, v0, Lokio/Segment;->next:Lokio/Segment;

    goto :goto_98

    .line 1243
    :cond_8d
    iget-object v0, p0, Lokio/Buffer;->head:Lokio/Segment;

    iget-object v9, v0, Lokio/Segment;->prev:Lokio/Segment;

    .line 1244
    .local v9, "tail":Lokio/Segment;
    invoke-virtual {v9, v6}, Lokio/Segment;->push(Lokio/Segment;)Lokio/Segment;

    move-result-object v9

    .line 1245
    invoke-virtual {v9}, Lokio/Segment;->compact()V

    .line 1247
    .end local v9    # "tail":Lokio/Segment;
    :goto_98
    iget-wide v0, p1, Lokio/Buffer;->size:J

    sub-long/2addr v0, v7

    iput-wide v0, p1, Lokio/Buffer;->size:J

    .line 1248
    iget-wide v0, p0, Lokio/Buffer;->size:J

    add-long/2addr v0, v7

    iput-wide v0, p0, Lokio/Buffer;->size:J

    .line 1249
    sub-long/2addr p2, v7

    .line 1250
    .end local v6    # "segmentToMove":Lokio/Segment;
    .end local v7    # "movedByteCount":J
    goto/16 :goto_1c

    .line 1251
    :cond_a5
    return-void
.end method

.method public writeAll(Lokio/Source;)J
    .registers 10
    .param p1, "source"    # Lokio/Source;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 991
    if-nez p1, :cond_a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 992
    :cond_a
    const-wide/16 v4, 0x0

    .line 993
    .local v4, "totalBytesRead":J
    :goto_c
    const-wide/16 v0, 0x2000

    invoke-interface {p1, p0, v0, v1}, Lokio/Source;->read(Lokio/Buffer;J)J

    move-result-wide v0

    move-wide v6, v0

    .local v6, "readCount":J
    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1b

    .line 994
    add-long/2addr v4, v6

    goto :goto_c

    .line 996
    .end local v6    # "readCount":J
    :cond_1b
    return-wide v4
.end method

.method public writeByte(I)Lokio/Buffer;
    .registers 7
    .param p1, "b"    # I

    .line 1009
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lokio/Buffer;->writableSegment(I)Lokio/Segment;

    move-result-object v4

    .line 1010
    .local v4, "tail":Lokio/Segment;
    iget-object v0, v4, Lokio/Segment;->data:[B

    iget v1, v4, Lokio/Segment;->limit:I

    add-int/lit8 v2, v1, 0x1

    iput v2, v4, Lokio/Segment;->limit:I

    int-to-byte v2, p1

    aput-byte v2, v0, v1

    .line 1011
    iget-wide v0, p0, Lokio/Buffer;->size:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lokio/Buffer;->size:J

    .line 1012
    return-object p0
.end method

.method public bridge synthetic writeByte(I)Lokio/BufferedSink;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 47
    invoke-virtual {p0, p1}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    move-result-object v0

    return-object v0
.end method

.method public writeDecimalLong(J)Lokio/Buffer;
    .registers 13
    .param p1, "v"    # J

    .line 1069
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_d

    .line 1071
    const/16 v0, 0x30

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    move-result-object v0

    return-object v0

    .line 1074
    :cond_d
    const/4 v4, 0x0

    .line 1075
    .local v4, "negative":Z
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_23

    .line 1076
    neg-long p1, p1

    .line 1077
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_22

    .line 1078
    const-string v0, "-9223372036854775808"

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeUtf8(Ljava/lang/String;)Lokio/Buffer;

    move-result-object v0

    return-object v0

    .line 1080
    :cond_22
    const/4 v4, 0x1

    .line 1084
    :cond_23
    const-wide/32 v0, 0x5f5e100

    cmp-long v0, p1, v0

    if-gez v0, :cond_70

    const-wide/16 v0, 0x2710

    cmp-long v0, p1, v0

    if-gez v0, :cond_4e

    const-wide/16 v0, 0x64

    cmp-long v0, p1, v0

    if-gez v0, :cond_42

    const-wide/16 v0, 0xa

    cmp-long v0, p1, v0

    if-gez v0, :cond_3f

    const/4 v5, 0x1

    goto/16 :goto_e8

    :cond_3f
    const/4 v5, 0x2

    goto/16 :goto_e8

    :cond_42
    const-wide/16 v0, 0x3e8

    cmp-long v0, p1, v0

    if-gez v0, :cond_4b

    const/4 v5, 0x3

    goto/16 :goto_e8

    :cond_4b
    const/4 v5, 0x4

    goto/16 :goto_e8

    :cond_4e
    const-wide/32 v0, 0xf4240

    cmp-long v0, p1, v0

    if-gez v0, :cond_62

    const-wide/32 v0, 0x186a0

    cmp-long v0, p1, v0

    if-gez v0, :cond_5f

    const/4 v5, 0x5

    goto/16 :goto_e8

    :cond_5f
    const/4 v5, 0x6

    goto/16 :goto_e8

    :cond_62
    const-wide/32 v0, 0x989680

    cmp-long v0, p1, v0

    if-gez v0, :cond_6c

    const/4 v5, 0x7

    goto/16 :goto_e8

    :cond_6c
    const/16 v5, 0x8

    goto/16 :goto_e8

    :cond_70
    const-wide v0, 0xe8d4a51000L

    cmp-long v0, p1, v0

    if-gez v0, :cond_9e

    const-wide v0, 0x2540be400L

    cmp-long v0, p1, v0

    if-gez v0, :cond_8f

    const-wide/32 v0, 0x3b9aca00

    cmp-long v0, p1, v0

    if-gez v0, :cond_8c

    const/16 v5, 0x9

    goto :goto_e8

    :cond_8c
    const/16 v5, 0xa

    goto :goto_e8

    :cond_8f
    const-wide v0, 0x174876e800L

    cmp-long v0, p1, v0

    if-gez v0, :cond_9b

    const/16 v5, 0xb

    goto :goto_e8

    :cond_9b
    const/16 v5, 0xc

    goto :goto_e8

    :cond_9e
    const-wide v0, 0x38d7ea4c68000L

    cmp-long v0, p1, v0

    if-gez v0, :cond_c2

    const-wide v0, 0x9184e72a000L

    cmp-long v0, p1, v0

    if-gez v0, :cond_b3

    const/16 v5, 0xd

    goto :goto_e8

    :cond_b3
    const-wide v0, 0x5af3107a4000L

    cmp-long v0, p1, v0

    if-gez v0, :cond_bf

    const/16 v5, 0xe

    goto :goto_e8

    :cond_bf
    const/16 v5, 0xf

    goto :goto_e8

    :cond_c2
    const-wide v0, 0x16345785d8a0000L

    cmp-long v0, p1, v0

    if-gez v0, :cond_da

    const-wide v0, 0x2386f26fc10000L

    cmp-long v0, p1, v0

    if-gez v0, :cond_d7

    const/16 v5, 0x10

    goto :goto_e8

    :cond_d7
    const/16 v5, 0x11

    goto :goto_e8

    :cond_da
    const-wide v0, 0xde0b6b3a7640000L

    cmp-long v0, p1, v0

    if-gez v0, :cond_e6

    const/16 v5, 0x12

    goto :goto_e8

    :cond_e6
    const/16 v5, 0x13

    .line 1103
    .local v5, "width":I
    :goto_e8
    if-eqz v4, :cond_ec

    .line 1104
    add-int/lit8 v5, v5, 0x1

    .line 1107
    :cond_ec
    invoke-virtual {p0, v5}, Lokio/Buffer;->writableSegment(I)Lokio/Segment;

    move-result-object v6

    .line 1108
    .local v6, "tail":Lokio/Segment;
    iget-object v7, v6, Lokio/Segment;->data:[B

    .line 1109
    .local v7, "data":[B
    iget v0, v6, Lokio/Segment;->limit:I

    add-int v8, v0, v5

    .line 1110
    .local v8, "pos":I
    :goto_f6
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_10d

    .line 1111
    const-wide/16 v0, 0xa

    rem-long v0, p1, v0

    long-to-int v9, v0

    .line 1112
    .local v9, "digit":I
    add-int/lit8 v8, v8, -0x1

    sget-object v0, Lokio/Buffer;->DIGITS:[B

    aget-byte v0, v0, v9

    aput-byte v0, v7, v8

    .line 1113
    const-wide/16 v0, 0xa

    div-long/2addr p1, v0

    .line 1114
    .end local v9    # "digit":I
    goto :goto_f6

    .line 1115
    :cond_10d
    if-eqz v4, :cond_115

    .line 1116
    add-int/lit8 v8, v8, -0x1

    const/16 v0, 0x2d

    aput-byte v0, v7, v8

    .line 1119
    :cond_115
    iget v0, v6, Lokio/Segment;->limit:I

    add-int/2addr v0, v5

    iput v0, v6, Lokio/Segment;->limit:I

    .line 1120
    iget-wide v0, p0, Lokio/Buffer;->size:J

    int-to-long v2, v5

    add-long/2addr v0, v2

    iput-wide v0, p0, Lokio/Buffer;->size:J

    .line 1121
    return-object p0
.end method

.method public bridge synthetic writeDecimalLong(J)Lokio/BufferedSink;
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 47
    invoke-virtual {p0, p1, p2}, Lokio/Buffer;->writeDecimalLong(J)Lokio/Buffer;

    move-result-object v0

    return-object v0
.end method

.method public writeHexadecimalUnsignedLong(J)Lokio/Buffer;
    .registers 12
    .param p1, "v"    # J

    .line 1125
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_d

    .line 1127
    const/16 v0, 0x30

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    move-result-object v0

    return-object v0

    .line 1130
    :cond_d
    invoke-static {p1, p2}, Ljava/lang/Long;->highestOneBit(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->numberOfTrailingZeros(J)I

    move-result v0

    div-int/lit8 v0, v0, 0x4

    add-int/lit8 v4, v0, 0x1

    .line 1132
    .local v4, "width":I
    invoke-virtual {p0, v4}, Lokio/Buffer;->writableSegment(I)Lokio/Segment;

    move-result-object v5

    .line 1133
    .local v5, "tail":Lokio/Segment;
    iget-object v6, v5, Lokio/Segment;->data:[B

    .line 1134
    .local v6, "data":[B
    iget v0, v5, Lokio/Segment;->limit:I

    add-int/2addr v0, v4

    add-int/lit8 v7, v0, -0x1

    .local v7, "pos":I
    iget v8, v5, Lokio/Segment;->limit:I

    .local v8, "start":I
    :goto_26
    if-lt v7, v8, :cond_37

    .line 1135
    sget-object v0, Lokio/Buffer;->DIGITS:[B

    const-wide/16 v1, 0xf

    and-long/2addr v1, p1

    long-to-int v1, v1

    aget-byte v0, v0, v1

    aput-byte v0, v6, v7

    .line 1136
    const/4 v0, 0x4

    ushr-long/2addr p1, v0

    .line 1134
    add-int/lit8 v7, v7, -0x1

    goto :goto_26

    .line 1138
    .end local v7    # "pos":I
    .end local v8    # "start":I
    :cond_37
    iget v0, v5, Lokio/Segment;->limit:I

    add-int/2addr v0, v4

    iput v0, v5, Lokio/Segment;->limit:I

    .line 1139
    iget-wide v0, p0, Lokio/Buffer;->size:J

    int-to-long v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lokio/Buffer;->size:J

    .line 1140
    return-object p0
.end method

.method public bridge synthetic writeHexadecimalUnsignedLong(J)Lokio/BufferedSink;
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 47
    invoke-virtual {p0, p1, p2}, Lokio/Buffer;->writeHexadecimalUnsignedLong(J)Lokio/Buffer;

    move-result-object v0

    return-object v0
.end method

.method public writeInt(I)Lokio/Buffer;
    .registers 9
    .param p1, "i"    # I

    .line 1031
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lokio/Buffer;->writableSegment(I)Lokio/Segment;

    move-result-object v4

    .line 1032
    .local v4, "tail":Lokio/Segment;
    iget-object v5, v4, Lokio/Segment;->data:[B

    .line 1033
    .local v5, "data":[B
    iget v6, v4, Lokio/Segment;->limit:I

    .line 1034
    .local v6, "limit":I
    move v0, v6

    add-int/lit8 v6, v6, 0x1

    ushr-int/lit8 v1, p1, 0x18

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, v5, v0

    .line 1035
    move v0, v6

    add-int/lit8 v6, v6, 0x1

    ushr-int/lit8 v1, p1, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, v5, v0

    .line 1036
    move v0, v6

    add-int/lit8 v6, v6, 0x1

    ushr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, v5, v0

    .line 1037
    move v0, v6

    add-int/lit8 v6, v6, 0x1

    and-int/lit16 v1, p1, 0xff

    int-to-byte v1, v1

    aput-byte v1, v5, v0

    .line 1038
    iput v6, v4, Lokio/Segment;->limit:I

    .line 1039
    iget-wide v0, p0, Lokio/Buffer;->size:J

    const-wide/16 v2, 0x4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lokio/Buffer;->size:J

    .line 1040
    return-object p0
.end method

.method public bridge synthetic writeInt(I)Lokio/BufferedSink;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 47
    invoke-virtual {p0, p1}, Lokio/Buffer;->writeInt(I)Lokio/Buffer;

    move-result-object v0

    return-object v0
.end method

.method public writeShort(I)Lokio/Buffer;
    .registers 9
    .param p1, "s"    # I

    .line 1016
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lokio/Buffer;->writableSegment(I)Lokio/Segment;

    move-result-object v4

    .line 1017
    .local v4, "tail":Lokio/Segment;
    iget-object v5, v4, Lokio/Segment;->data:[B

    .line 1018
    .local v5, "data":[B
    iget v6, v4, Lokio/Segment;->limit:I

    .line 1019
    .local v6, "limit":I
    move v0, v6

    add-int/lit8 v6, v6, 0x1

    ushr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, v5, v0

    .line 1020
    move v0, v6

    add-int/lit8 v6, v6, 0x1

    and-int/lit16 v1, p1, 0xff

    int-to-byte v1, v1

    aput-byte v1, v5, v0

    .line 1021
    iput v6, v4, Lokio/Segment;->limit:I

    .line 1022
    iget-wide v0, p0, Lokio/Buffer;->size:J

    const-wide/16 v2, 0x2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lokio/Buffer;->size:J

    .line 1023
    return-object p0
.end method

.method public bridge synthetic writeShort(I)Lokio/BufferedSink;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 47
    invoke-virtual {p0, p1}, Lokio/Buffer;->writeShort(I)Lokio/Buffer;

    move-result-object v0

    return-object v0
.end method

.method public writeString(Ljava/lang/String;IILjava/nio/charset/Charset;)Lokio/Buffer;
    .registers 9
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "beginIndex"    # I
    .param p3, "endIndex"    # I
    .param p4, "charset"    # Ljava/nio/charset/Charset;

    .line 951
    if-nez p1, :cond_a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "string == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 952
    :cond_a
    if-gez p2, :cond_25

    new-instance v0, Ljava/lang/IllegalAccessError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "beginIndex < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 953
    :cond_25
    if-ge p3, p2, :cond_4a

    .line 954
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "endIndex < beginIndex: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 956
    :cond_4a
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le p3, v0, :cond_77

    .line 957
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "endIndex > string.length: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 958
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 960
    :cond_77
    if-nez p4, :cond_81

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "charset == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 961
    :cond_81
    sget-object v0, Lokio/Util;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p4, v0}, Ljava/nio/charset/Charset;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8e

    invoke-virtual {p0, p1}, Lokio/Buffer;->writeUtf8(Ljava/lang/String;)Lokio/Buffer;

    move-result-object v0

    return-object v0

    .line 962
    :cond_8e
    invoke-virtual {p1, p2, p3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    .line 963
    .local v3, "data":[B
    array-length v0, v3

    const/4 v1, 0x0

    invoke-virtual {p0, v3, v1, v0}, Lokio/Buffer;->write([BII)Lokio/Buffer;

    move-result-object v0

    return-object v0
.end method

.method public writeString(Ljava/lang/String;Ljava/nio/charset/Charset;)Lokio/Buffer;
    .registers 5
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "charset"    # Ljava/nio/charset/Charset;

    .line 946
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0, p2}, Lokio/Buffer;->writeString(Ljava/lang/String;IILjava/nio/charset/Charset;)Lokio/Buffer;

    move-result-object v0

    return-object v0
.end method

.method public writeUtf8(Ljava/lang/String;)Lokio/Buffer;
    .registers 4
    .param p1, "string"    # Ljava/lang/String;

    .line 830
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lokio/Buffer;->writeUtf8(Ljava/lang/String;II)Lokio/Buffer;

    move-result-object v0

    return-object v0
.end method

.method public writeUtf8(Ljava/lang/String;II)Lokio/Buffer;
    .registers 15
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "beginIndex"    # I
    .param p3, "endIndex"    # I

    .line 834
    if-nez p1, :cond_a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "string == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 835
    :cond_a
    if-gez p2, :cond_25

    new-instance v0, Ljava/lang/IllegalAccessError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "beginIndex < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalAccessError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 836
    :cond_25
    if-ge p3, p2, :cond_4a

    .line 837
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "endIndex < beginIndex: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 839
    :cond_4a
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le p3, v0, :cond_77

    .line 840
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "endIndex > string.length: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 841
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 845
    :cond_77
    move v4, p2

    .local v4, "i":I
    :goto_78
    if-ge v4, p3, :cond_152

    .line 846
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 848
    .local v5, "c":I
    const/16 v0, 0x80

    if-ge v5, v0, :cond_c0

    .line 849
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lokio/Buffer;->writableSegment(I)Lokio/Segment;

    move-result-object v6

    .line 850
    .local v6, "tail":Lokio/Segment;
    iget-object v7, v6, Lokio/Segment;->data:[B

    .line 851
    .local v7, "data":[B
    iget v0, v6, Lokio/Segment;->limit:I

    sub-int v8, v0, v4

    .line 852
    .local v8, "segmentOffset":I
    rsub-int v0, v8, 0x2000

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 855
    .local v9, "runLimit":I
    move v0, v4

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v0, v8

    int-to-byte v1, v5

    aput-byte v1, v7, v0

    .line 859
    :goto_9a
    if-ge v4, v9, :cond_ad

    .line 860
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 861
    const/16 v0, 0x80

    if-lt v5, v0, :cond_a5

    goto :goto_ad

    .line 862
    :cond_a5
    move v0, v4

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v0, v8

    int-to-byte v1, v5

    aput-byte v1, v7, v0

    goto :goto_9a

    .line 865
    :cond_ad
    :goto_ad
    add-int v0, v4, v8

    iget v1, v6, Lokio/Segment;->limit:I

    sub-int v10, v0, v1

    .line 866
    .local v10, "runSize":I
    iget v0, v6, Lokio/Segment;->limit:I

    add-int/2addr v0, v10

    iput v0, v6, Lokio/Segment;->limit:I

    .line 867
    iget-wide v0, p0, Lokio/Buffer;->size:J

    int-to-long v2, v10

    add-long/2addr v0, v2

    iput-wide v0, p0, Lokio/Buffer;->size:J

    .line 869
    .end local v6    # "tail":Lokio/Segment;
    .end local v7    # "data":[B
    .end local v8    # "segmentOffset":I
    .end local v9    # "runLimit":I
    .end local v10    # "runSize":I
    goto/16 :goto_150

    :cond_c0
    const/16 v0, 0x800

    if-ge v5, v0, :cond_d6

    .line 871
    shr-int/lit8 v0, v5, 0x6

    or-int/lit16 v0, v0, 0xc0

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    .line 872
    and-int/lit8 v0, v5, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    .line 873
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_150

    .line 875
    :cond_d6
    const v0, 0xd800

    if-lt v5, v0, :cond_e0

    const v0, 0xdfff

    if-le v5, v0, :cond_fb

    .line 877
    :cond_e0
    shr-int/lit8 v0, v5, 0xc

    or-int/lit16 v0, v0, 0xe0

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    .line 878
    shr-int/lit8 v0, v5, 0x6

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    .line 879
    and-int/lit8 v0, v5, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    .line 880
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_150

    .line 885
    :cond_fb
    add-int/lit8 v0, v4, 0x1

    if-ge v0, p3, :cond_106

    add-int/lit8 v0, v4, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v6

    goto :goto_107

    :cond_106
    const/4 v6, 0x0

    .line 886
    .local v6, "low":I
    :goto_107
    const v0, 0xdbff

    if-gt v5, v0, :cond_116

    const v0, 0xdc00

    if-lt v6, v0, :cond_116

    const v0, 0xdfff

    if-le v6, v0, :cond_11f

    .line 887
    :cond_116
    const/16 v0, 0x3f

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    .line 888
    add-int/lit8 v4, v4, 0x1

    .line 889
    goto/16 :goto_78

    .line 895
    :cond_11f
    const v0, -0xd801

    and-int/2addr v0, v5

    shl-int/lit8 v0, v0, 0xa

    const v1, -0xdc01

    and-int/2addr v1, v6

    or-int/2addr v0, v1

    const/high16 v1, 0x10000

    add-int v7, v1, v0

    .line 898
    .local v7, "codePoint":I
    shr-int/lit8 v0, v7, 0x12

    or-int/lit16 v0, v0, 0xf0

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    .line 899
    shr-int/lit8 v0, v7, 0xc

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    .line 900
    shr-int/lit8 v0, v7, 0x6

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    .line 901
    and-int/lit8 v0, v7, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    .line 902
    add-int/lit8 v4, v4, 0x2

    .line 904
    .end local v5    # "c":I
    .end local v6    # "low":I
    .end local v7    # "codePoint":I
    :goto_150
    goto/16 :goto_78

    .line 906
    .end local v4    # "i":I
    :cond_152
    return-object p0
.end method

.method public bridge synthetic writeUtf8(Ljava/lang/String;)Lokio/BufferedSink;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 47
    invoke-virtual {p0, p1}, Lokio/Buffer;->writeUtf8(Ljava/lang/String;)Lokio/Buffer;

    move-result-object v0

    return-object v0
.end method

.method public writeUtf8CodePoint(I)Lokio/Buffer;
    .registers 5
    .param p1, "codePoint"    # I

    .line 910
    const/16 v0, 0x80

    if-ge p1, v0, :cond_9

    .line 912
    invoke-virtual {p0, p1}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    goto/16 :goto_a3

    .line 914
    :cond_9
    const/16 v0, 0x800

    if-ge p1, v0, :cond_1d

    .line 916
    shr-int/lit8 v0, p1, 0x6

    or-int/lit16 v0, v0, 0xc0

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    .line 917
    and-int/lit8 v0, p1, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    goto/16 :goto_a3

    .line 919
    :cond_1d
    const/high16 v0, 0x10000

    if-ge p1, v0, :cond_60

    .line 920
    const v0, 0xd800

    if-lt p1, v0, :cond_48

    const v0, 0xdfff

    if-gt p1, v0, :cond_48

    .line 921
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected code point: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 922
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 926
    :cond_48
    shr-int/lit8 v0, p1, 0xc

    or-int/lit16 v0, v0, 0xe0

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    .line 927
    shr-int/lit8 v0, p1, 0x6

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    .line 928
    and-int/lit8 v0, p1, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    goto :goto_a3

    .line 930
    :cond_60
    const v0, 0x10ffff

    if-gt p1, v0, :cond_86

    .line 932
    shr-int/lit8 v0, p1, 0x12

    or-int/lit16 v0, v0, 0xf0

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    .line 933
    shr-int/lit8 v0, p1, 0xc

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    .line 934
    shr-int/lit8 v0, p1, 0x6

    and-int/lit8 v0, v0, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    .line 935
    and-int/lit8 v0, p1, 0x3f

    or-int/lit16 v0, v0, 0x80

    invoke-virtual {p0, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    goto :goto_a3

    .line 938
    :cond_86
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected code point: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 939
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 942
    :goto_a3
    return-object p0
.end method

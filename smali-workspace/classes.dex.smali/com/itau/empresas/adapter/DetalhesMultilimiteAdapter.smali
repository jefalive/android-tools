.class public Lcom/itau/empresas/adapter/DetalhesMultilimiteAdapter;
.super Landroid/widget/BaseAdapter;
.source "DetalhesMultilimiteAdapter.java"


# instance fields
.field private atributos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;>;"
        }
    .end annotation
.end field

.field context:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 20
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 2

    .line 33
    iget-object v0, p0, Lcom/itau/empresas/adapter/DetalhesMultilimiteAdapter;->atributos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;
    .registers 3
    .param p1, "position"    # I

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/adapter/DetalhesMultilimiteAdapter;->atributos:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .registers 3

    .line 19
    invoke-virtual {p0, p1}, Lcom/itau/empresas/adapter/DetalhesMultilimiteAdapter;->getItem(I)Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .param p1, "position"    # I

    .line 43
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .line 49
    invoke-virtual {p0, p1}, Lcom/itau/empresas/adapter/DetalhesMultilimiteAdapter;->getItem(I)Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;

    move-result-object v1

    .line 51
    .local v1, "atributo":Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;
    if-nez p2, :cond_c

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/adapter/DetalhesMultilimiteAdapter;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/itau/empresas/ui/view/DetalheProdutoView_;->build(Landroid/content/Context;)Lcom/itau/empresas/ui/view/DetalheProdutoView;

    move-result-object p2

    .line 55
    :cond_c
    move-object v2, p2

    check-cast v2, Lcom/itau/empresas/ui/view/DetalheProdutoView;

    .line 57
    .local v2, "view":Lcom/itau/empresas/ui/view/DetalheProdutoView;
    invoke-virtual {v2, v1}, Lcom/itau/empresas/ui/view/DetalheProdutoView;->bind(Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;)V

    .line 59
    return-object v2
.end method

.method public notifyDataSetChanged()V
    .registers 1

    .line 64
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 65
    return-void
.end method

.method public setData(Ljava/util/List;)V
    .registers 2
    .param p1, "atributos"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteAtributoVO;>;)V"
        }
    .end annotation

    .line 28
    iput-object p1, p0, Lcom/itau/empresas/adapter/DetalhesMultilimiteAdapter;->atributos:Ljava/util/List;

    .line 29
    return-void
.end method

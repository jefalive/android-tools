.class public Lcom/itau/empresas/feature/credito/produtos/model/ProdutosParceladosVO;
.super Ljava/lang/Object;
.source "ProdutosParceladosVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private codigoPlanoFinanciamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_plano_financiamento"
    .end annotation
.end field

.field private codigoProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_produto"
    .end annotation
.end field

.field private descricaoProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_produto"
    .end annotation
.end field

.field private indicadorCartao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_cartao"
    .end annotation
.end field

.field private indicadorSeguro:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_seguro"
    .end annotation
.end field

.field private mensagemSeguroProtecao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "mensagem_seguro_protecao"
    .end annotation
.end field

.field private nomeProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_produto"
    .end annotation
.end field

.field private valorDisponivelContratacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_disponivel_contratacao"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getNomeProduto()Ljava/lang/String;
    .registers 2

    .line 34
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/model/ProdutosParceladosVO;->nomeProduto:Ljava/lang/String;

    return-object v0
.end method

.method public getValorDisponivelContratacao()Ljava/lang/String;
    .registers 2

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/model/ProdutosParceladosVO;->valorDisponivelContratacao:Ljava/lang/String;

    return-object v0
.end method

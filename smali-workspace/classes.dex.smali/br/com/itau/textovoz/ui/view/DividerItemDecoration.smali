.class public Lbr/com/itau/textovoz/ui/view/DividerItemDecoration;
.super Landroid/support/v7/widget/RecyclerView$ItemDecoration;
.source "DividerItemDecoration.java"


# instance fields
.field private closeWithDivider:Z

.field private divider:Landroid/graphics/drawable/Drawable;

.field private startWithDivider:Z


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;ZZ)V
    .registers 4
    .param p1, "divider"    # Landroid/graphics/drawable/Drawable;
    .param p2, "startWithDivider"    # Z
    .param p3, "closeWithDivider"    # Z

    .line 19
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$ItemDecoration;-><init>()V

    .line 20
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/view/DividerItemDecoration;->divider:Landroid/graphics/drawable/Drawable;

    .line 21
    iput-boolean p2, p0, Lbr/com/itau/textovoz/ui/view/DividerItemDecoration;->startWithDivider:Z

    .line 22
    iput-boolean p3, p0, Lbr/com/itau/textovoz/ui/view/DividerItemDecoration;->closeWithDivider:Z

    .line 23
    return-void
.end method


# virtual methods
.method public getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$State;)V
    .registers 8
    .param p1, "outRect"    # Landroid/graphics/Rect;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/support/v7/widget/RecyclerView;
    .param p4, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .line 27
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/widget/RecyclerView$ItemDecoration;->getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$State;)V

    .line 29
    iget-boolean v0, p0, Lbr/com/itau/textovoz/ui/view/DividerItemDecoration;->startWithDivider:Z

    if-nez v0, :cond_e

    invoke-virtual {p3, p2}, Landroid/support/v7/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_e

    .line 30
    return-void

    .line 33
    :cond_e
    iget-boolean v0, p0, Lbr/com/itau/textovoz/ui/view/DividerItemDecoration;->closeWithDivider:Z

    if-eqz v0, :cond_26

    .line 34
    invoke-virtual {p3, p2}, Landroid/support/v7/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result v1

    .line 35
    .local v1, "pos":I
    invoke-virtual {p4}, Landroid/support/v7/widget/RecyclerView$State;->getItemCount()I

    move-result v2

    .line 37
    .local v2, "childCount":I
    add-int/lit8 v0, v2, -0x1

    if-ne v1, v0, :cond_26

    .line 38
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/DividerItemDecoration;->divider:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 42
    .end local v1    # "pos":I
    .end local v2    # "childCount":I
    :cond_26
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/DividerItemDecoration;->divider:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 43
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$State;)V
    .registers 14
    .param p1, "c"    # Landroid/graphics/Canvas;
    .param p2, "parent"    # Landroid/support/v7/widget/RecyclerView;
    .param p3, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .line 47
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v2

    .line 48
    .local v2, "dividerLeft":I
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v1

    sub-int v3, v0, v1

    .line 50
    .local v3, "dividerRight":I
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v4

    .line 52
    .local v4, "childCount":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_13
    add-int/lit8 v0, v4, -0x1

    if-ge v5, v0, :cond_3f

    .line 53
    invoke-virtual {p2, v5}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 55
    .local v6, "child":Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    .line 57
    .local v7, "params":Landroid/support/v7/widget/RecyclerView$LayoutParams;
    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v0

    iget v1, v7, Landroid/support/v7/widget/RecyclerView$LayoutParams;->bottomMargin:I

    add-int v8, v0, v1

    .line 58
    .local v8, "dividerTop":I
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/DividerItemDecoration;->divider:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    add-int v9, v8, v0

    .line 60
    .local v9, "dividerBottom":I
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/DividerItemDecoration;->divider:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2, v8, v3, v9}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 61
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/DividerItemDecoration;->divider:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 52
    .end local v6    # "child":Landroid/view/View;
    .end local v7    # "params":Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .end local v8    # "dividerTop":I
    .end local v9    # "dividerBottom":I
    add-int/lit8 v5, v5, 0x1

    goto :goto_13

    .line 63
    .end local v5    # "i":I
    :cond_3f
    return-void
.end method

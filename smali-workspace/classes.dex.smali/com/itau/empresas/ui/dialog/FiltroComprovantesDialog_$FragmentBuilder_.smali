.class public Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_$FragmentBuilder_;
.super Lorg/androidannotations/api/builder/FragmentBuilder;
.source "FiltroComprovantesDialog_.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FragmentBuilder_"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/androidannotations/api/builder/FragmentBuilder<Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_$FragmentBuilder_;Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 100
    invoke-direct {p0}, Lorg/androidannotations/api/builder/FragmentBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog;
    .registers 3

    .line 106
    new-instance v1, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;

    invoke-direct {v1}, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;-><init>()V

    .line 107
    .local v1, "fragment_":Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_$FragmentBuilder_;->args:Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_;->setArguments(Landroid/os/Bundle;)V

    .line 108
    return-object v1
.end method

.method public selecionado(Ljava/lang/Integer;)Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_$FragmentBuilder_;
    .registers 4
    .param p1, "selecionado"    # Ljava/lang/Integer;

    .line 118
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FiltroComprovantesDialog_$FragmentBuilder_;->args:Landroid/os/Bundle;

    const-string v1, "selecionado"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 119
    return-object p0
.end method

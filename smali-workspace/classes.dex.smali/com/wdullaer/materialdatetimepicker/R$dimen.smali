.class public final Lcom/wdullaer/materialdatetimepicker/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wdullaer/materialdatetimepicker/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final mdtp_ampm_label_size:I = 0x7f080012

.field public static final mdtp_ampm_left_padding:I = 0x7f080013

.field public static final mdtp_date_picker_component_width:I = 0x7f080026

.field public static final mdtp_date_picker_header_height:I = 0x7f080014

.field public static final mdtp_date_picker_header_text_size:I = 0x7f080027

.field public static final mdtp_date_picker_view_animator_height:I = 0x7f080028

.field public static final mdtp_day_number_select_circle_radius:I = 0x7f080029

.field public static final mdtp_day_number_size:I = 0x7f08002a

.field public static final mdtp_dialog_height:I = 0x7f080006

.field public static final mdtp_done_button_height:I = 0x7f0801a5

.field public static final mdtp_done_label_size:I = 0x7f0801a6

.field public static final mdtp_extra_time_label_margin:I = 0x7f08002b

.field public static final mdtp_footer_height:I = 0x7f080015

.field public static final mdtp_header_height:I = 0x7f080016

.field public static final mdtp_left_side_width:I = 0x7f080007

.field public static final mdtp_material_button_height:I = 0x7f0801a7

.field public static final mdtp_material_button_minwidth:I = 0x7f0801a8

.field public static final mdtp_material_button_textpadding_horizontal:I = 0x7f0801a9

.field public static final mdtp_material_button_textsize:I = 0x7f0801aa

.field public static final mdtp_minimum_margin_sides:I = 0x7f08002c

.field public static final mdtp_minimum_margin_top_bottom:I = 0x7f08002d

.field public static final mdtp_month_day_label_text_size:I = 0x7f08002e

.field public static final mdtp_month_label_size:I = 0x7f08002f

.field public static final mdtp_month_list_item_header_height:I = 0x7f080030

.field public static final mdtp_month_list_item_padding:I = 0x7f080031

.field public static final mdtp_month_list_item_size:I = 0x7f080032

.field public static final mdtp_month_select_circle_radius:I = 0x7f080033

.field public static final mdtp_picker_dimen:I = 0x7f080034

.field public static final mdtp_selected_calendar_layout_height:I = 0x7f080035

.field public static final mdtp_selected_date_day_size:I = 0x7f080008

.field public static final mdtp_selected_date_height:I = 0x7f0801ab

.field public static final mdtp_selected_date_month_size:I = 0x7f080009

.field public static final mdtp_selected_date_year_size:I = 0x7f08000a

.field public static final mdtp_separator_padding:I = 0x7f080017

.field public static final mdtp_time_label_right_padding:I = 0x7f080018

.field public static final mdtp_time_label_size:I = 0x7f080019

.field public static final mdtp_time_picker_header_text_size:I = 0x7f0801ac

.field public static final mdtp_time_picker_height:I = 0x7f080036

.field public static final mdtp_year_label_height:I = 0x7f080037

.field public static final mdtp_year_label_text_size:I = 0x7f080038


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public Lcom/itau/empresas/PreferenciaUtils;
.super Ljava/lang/Object;
.source "PreferenciaUtils.java"


# instance fields
.field private final operadorPrefs:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const-string v0, "estado_widget_home"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/PreferenciaUtils;->operadorPrefs:Landroid/content/SharedPreferences;

    .line 15
    return-void
.end method


# virtual methods
.method public get(Ljava/lang/String;)Z
    .registers 4
    .param p1, "preferencia"    # Ljava/lang/String;

    .line 18
    iget-object v0, p0, Lcom/itau/empresas/PreferenciaUtils;->operadorPrefs:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public set(Ljava/lang/String;Z)V
    .registers 4
    .param p1, "preferencia"    # Ljava/lang/String;
    .param p2, "estado"    # Z

    .line 22
    iget-object v0, p0, Lcom/itau/empresas/PreferenciaUtils;->operadorPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 23
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 24
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 25
    return-void
.end method

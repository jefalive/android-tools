.class public Lcom/itau/empresas/api/model/ConsultaCnpjVO;
.super Ljava/lang/Object;
.source "ConsultaCnpjVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private acaoContraCliente:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_existe_bloqueio_juridico_acao_contra_cliente"
    .end annotation
.end field

.field private acaoContraContaEmpresa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_existe_bloqueio_acao_contra_conta_empresa"
    .end annotation
.end field

.field private agencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia"
    .end annotation
.end field

.field private agenciaResponsavelConta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia_responsavel_segmento_conta"
    .end annotation
.end field

.field private atuacaoEmpresa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_tipo_atuacao_empresa"
    .end annotation
.end field

.field private bloqueadaMovimentacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_conta_bloqueada_movimentacao"
    .end annotation
.end field

.field private clientePrivate:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_novo_cliente_private"
    .end annotation
.end field

.field private codigoBanco:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigoBanco"
    .end annotation
.end field

.field private codigoClienteEmpresaSegmento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_cliente_empresa_segmento"
    .end annotation
.end field

.field private codigoSegmentoConta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_segmento_conta"
    .end annotation
.end field

.field private codigoSituacaoConta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_situacao_conta"
    .end annotation
.end field

.field private conta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta"
    .end annotation
.end field

.field private cpfCnpj:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf_cnpj"
    .end annotation
.end field

.field private dataUltimaAlteracaoConta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_ultima_alteracao_cadastro_conta"
    .end annotation
.end field

.field private digitoVerificador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digitoVerificador"
    .end annotation
.end field

.field private necessidadesEspeciais:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_ciente_necessidades_especiais"
    .end annotation
.end field

.field private nomeCliente:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_cliente"
    .end annotation
.end field

.field private situacaoJuridicaConta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "situacao_juridica_conta"
    .end annotation
.end field

.field private tipoClientePrivate:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_cliente_private"
    .end annotation
.end field

.field private tipoPessoa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_pessoa"
    .end annotation
.end field

.field private titularidade:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "titularidade"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

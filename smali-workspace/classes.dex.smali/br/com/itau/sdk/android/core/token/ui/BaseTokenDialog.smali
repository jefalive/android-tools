.class public abstract Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;
.super Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;
.source "SourceFile"


# static fields
.field private static final $:[B

.field private static $$:I = 0x0

.field public static final APP:Ljava/lang/String; = "aplicativo"

.field public static final CHAVEIRO:Ljava/lang/String; = "chaveiro"

.field public static final OPKEY:Ljava/lang/String; = "opkey"

.field public static final SMS:Ljava/lang/String; = "sms"


# instance fields
.field private isUnlockTokenScenario:Z

.field private opKey:Ljava/lang/String;

.field private tokenSelecionado:Ljava/lang/String;

.field private uiModel:Lbr/com/itau/sdk/android/core/model/UiModelDTO;


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    add-int/lit8 p2, p2, 0x3

    rsub-int/lit8 p1, p1, 0x73

    const/4 v5, 0x0

    add-int/lit8 p0, p0, 0x4

    sget-object v4, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->$:[B

    new-instance v0, Ljava/lang/String;

    new-array v1, p2, [B

    add-int/lit8 p2, p2, -0x1

    if-nez v4, :cond_18

    move v2, p1

    move v3, p0

    :goto_13
    add-int/2addr v2, v3

    add-int/lit8 p0, p0, 0x1

    add-int/lit8 p1, v2, -0x5

    :cond_18
    int-to-byte v2, p1

    aput-byte v2, v1, v5

    if-ne v5, p2, :cond_22

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_22
    move v2, p1

    aget-byte v3, v4, p0

    add-int/lit8 v5, v5, 0x1

    goto :goto_13
.end method

.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x2a

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->$:[B

    const/16 v0, 0x26

    sput v0, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->$$:I

    return-void

    :array_e
    .array-data 1
        0x7et
        -0x28t
        0x7ft
        -0x3bt
        0x6t
        0x0t
        -0x1t
        0x19t
        0x14t
        0x1t
        0x2t
        -0x1t
        0x3t
        0x18t
        -0x6t
        0x12t
        -0x2t
        -0x1t
        0xbt
        0x5t
        0x5t
        0xat
        -0x2t
        0x1at
        -0xct
        0x9t
        0xet
        0x2t
        0x20t
        0x1t
        -0x1t
        0xet
        -0x49t
        0x48t
        0x3t
        0x12t
        -0x6t
        0x7t
        0xct
        -0x6t
        0x8t
        0x10t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 2

    .line 16
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->opKey:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public checkTokenDataIsValid()Z
    .registers 8

    .line 104
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->getUiModel()Lbr/com/itau/sdk/android/core/model/UiModelDTO;

    move-result-object v0

    if-nez v0, :cond_3a

    .line 105
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/TokenException;

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->CANCELED:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    sget-object v2, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->$:[B

    const/16 v3, 0xd

    aget-byte v2, v2, v3

    or-int/lit8 v3, v2, 0x7

    sget-object v4, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->$:[B

    const/16 v5, 0x26

    aget-byte v4, v4, v5

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->$(III)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbr/com/itau/sdk/android/core/exception/TokenException;-><init>(Lbr/com/itau/sdk/android/core/exception/TokenException$Type;Ljava/lang/String;Ljava/lang/Exception;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->tokenException(Lbr/com/itau/sdk/android/core/exception/TokenException;)Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v6

    .line 106
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->opKey:Ljava/lang/String;

    invoke-virtual {v6, v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->setOpKey(Ljava/lang/String;)V

    .line 107
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Token;

    invoke-direct {v1, v6}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Token;-><init>(Lbr/com/itau/sdk/android/core/exception/BackendException;)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 108
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->dismissAllowingStateLoss()V

    .line 110
    const/4 v0, 0x0

    return v0

    .line 112
    :cond_3a
    const/4 v0, 0x1

    return v0
.end method

.method protected getOpKey()Ljava/lang/String;
    .registers 2

    .line 72
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->opKey:Ljava/lang/String;

    return-object v0
.end method

.method protected getTokenSelecionado()Ljava/lang/String;
    .registers 2

    .line 60
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->tokenSelecionado:Ljava/lang/String;

    return-object v0
.end method

.method protected getUiModel()Lbr/com/itau/sdk/android/core/model/UiModelDTO;
    .registers 2

    .line 64
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->uiModel:Lbr/com/itau/sdk/android/core/model/UiModelDTO;

    return-object v0
.end method

.method protected isUnlockTokenScenario()Z
    .registers 2

    .line 80
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->isUnlockTokenScenario:Z

    return v0
.end method

.method protected isValidToken(Ljava/lang/String;)Z
    .registers 6

    .line 88
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_27

    sget-object v0, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->$:[B

    const/16 v1, 0x29

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    sget-object v1, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->$:[B

    const/16 v2, 0x17

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, 0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->$:[B

    const/4 v3, 0x5

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_27

    const/4 v0, 0x1

    goto :goto_28

    :cond_27
    const/4 v0, 0x0

    :goto_28
    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 6

    .line 49
    invoke-super {p0, p1}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->onCreate(Landroid/os/Bundle;)V

    .line 50
    if-eqz p1, :cond_1c

    .line 51
    sget-object v0, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->$:[B

    const/4 v1, 0x5

    aget-byte v0, v0, v1

    add-int/lit8 v1, v0, 0x4

    sget-object v2, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->$:[B

    const/16 v3, 0xa

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->opKey:Ljava/lang/String;

    .line 53
    :cond_1c
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 6

    .line 43
    sget-object v0, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->$:[B

    const/4 v1, 0x5

    aget-byte v0, v0, v1

    add-int/lit8 v1, v0, 0x4

    sget-object v2, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->$:[B

    const/16 v3, 0xa

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->$(III)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->opKey:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    invoke-super {p0, p1}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 45
    return-void
.end method

.method public onStart()V
    .registers 3

    .line 93
    invoke-super {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->onStart()V

    .line 94
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;->TOKEN:Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/BackendClient;->trackEnterScreen(Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;)V

    .line 95
    return-void
.end method

.method public onStop()V
    .registers 3

    .line 99
    invoke-super {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->onStop()V

    .line 100
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;->TOKEN:Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/BackendClient;->trackLeaveScreen(Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;)V

    .line 101
    return-void
.end method

.method protected setIsUnlockTokenScenario(Z)V
    .registers 2

    .line 84
    iput-boolean p1, p0, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->isUnlockTokenScenario:Z

    .line 85
    return-void
.end method

.method public setOpKey(Ljava/lang/String;)V
    .registers 2

    .line 76
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->opKey:Ljava/lang/String;

    .line 77
    return-void
.end method

.method public setTokenSelecionado(Ljava/lang/String;)V
    .registers 2

    .line 56
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->tokenSelecionado:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public setUiModel(Lbr/com/itau/sdk/android/core/model/UiModelDTO;)V
    .registers 2

    .line 68
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/token/ui/BaseTokenDialog;->uiModel:Lbr/com/itau/sdk/android/core/model/UiModelDTO;

    .line 69
    return-void
.end method

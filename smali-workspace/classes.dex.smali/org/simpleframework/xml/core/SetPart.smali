.class Lorg/simpleframework/xml/core/SetPart;
.super Ljava/lang/Object;
.source "SetPart.java"

# interfaces
.implements Lorg/simpleframework/xml/core/MethodPart;


# instance fields
.field private final cache:Lorg/simpleframework/xml/util/Cache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/simpleframework/xml/util/Cache<Ljava/lang/annotation/Annotation;>;"
        }
    .end annotation
.end field

.field private final label:Ljava/lang/annotation/Annotation;

.field private final list:[Ljava/lang/annotation/Annotation;

.field private final method:Ljava/lang/reflect/Method;

.field private final name:Ljava/lang/String;

.field private final type:Lorg/simpleframework/xml/core/MethodType;


# direct methods
.method public constructor <init>(Lorg/simpleframework/xml/core/MethodName;Ljava/lang/annotation/Annotation;[Ljava/lang/annotation/Annotation;)V
    .registers 5
    .param p1, "method"    # Lorg/simpleframework/xml/core/MethodName;
    .param p2, "label"    # Ljava/lang/annotation/Annotation;
    .param p3, "list"    # [Ljava/lang/annotation/Annotation;

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Lorg/simpleframework/xml/util/ConcurrentCache;

    invoke-direct {v0}, Lorg/simpleframework/xml/util/ConcurrentCache;-><init>()V

    iput-object v0, p0, Lorg/simpleframework/xml/core/SetPart;->cache:Lorg/simpleframework/xml/util/Cache;

    .line 81
    invoke-virtual {p1}, Lorg/simpleframework/xml/core/MethodName;->getMethod()Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lorg/simpleframework/xml/core/SetPart;->method:Ljava/lang/reflect/Method;

    .line 82
    invoke-virtual {p1}, Lorg/simpleframework/xml/core/MethodName;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/simpleframework/xml/core/SetPart;->name:Ljava/lang/String;

    .line 83
    invoke-virtual {p1}, Lorg/simpleframework/xml/core/MethodName;->getType()Lorg/simpleframework/xml/core/MethodType;

    move-result-object v0

    iput-object v0, p0, Lorg/simpleframework/xml/core/SetPart;->type:Lorg/simpleframework/xml/core/MethodType;

    .line 84
    iput-object p2, p0, Lorg/simpleframework/xml/core/SetPart;->label:Ljava/lang/annotation/Annotation;

    .line 85
    iput-object p3, p0, Lorg/simpleframework/xml/core/SetPart;->list:[Ljava/lang/annotation/Annotation;

    .line 86
    return-void
.end method


# virtual methods
.method public getAnnotation()Ljava/lang/annotation/Annotation;
    .registers 2

    .line 155
    iget-object v0, p0, Lorg/simpleframework/xml/core/SetPart;->label:Ljava/lang/annotation/Annotation;

    return-object v0
.end method

.method public getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
    .registers 8
    .param p1, "type"    # Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::Ljava/lang/annotation/Annotation;>(Ljava/lang/Class<TT;>;)TT;"
        }
    .end annotation

    .line 168
    iget-object v0, p0, Lorg/simpleframework/xml/core/SetPart;->cache:Lorg/simpleframework/xml/util/Cache;

    invoke-interface {v0}, Lorg/simpleframework/xml/util/Cache;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 169
    iget-object v1, p0, Lorg/simpleframework/xml/core/SetPart;->list:[Ljava/lang/annotation/Annotation;

    .local v1, "arr$":[Ljava/lang/annotation/Annotation;
    array-length v2, v1

    .local v2, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_c
    if-ge v3, v2, :cond_1c

    aget-object v4, v1, v3

    .line 170
    .local v4, "entry":Ljava/lang/annotation/Annotation;
    invoke-interface {v4}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v5

    .line 171
    .local v5, "key":Ljava/lang/Class;
    iget-object v0, p0, Lorg/simpleframework/xml/core/SetPart;->cache:Lorg/simpleframework/xml/util/Cache;

    invoke-interface {v0, v5, v4}, Lorg/simpleframework/xml/util/Cache;->cache(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 169
    .end local v4    # "entry":Ljava/lang/annotation/Annotation;
    .end local v5    # "key":Ljava/lang/Class;
    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    .line 174
    .end local v1    # "arr$":[Ljava/lang/annotation/Annotation;
    .end local v2    # "len$":I
    .end local v3    # "i$":I
    :cond_1c
    iget-object v0, p0, Lorg/simpleframework/xml/core/SetPart;->cache:Lorg/simpleframework/xml/util/Cache;

    invoke-interface {v0, p1}, Lorg/simpleframework/xml/util/Cache;->fetch(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/annotation/Annotation;

    return-object v0
.end method

.method public getDeclaringClass()Ljava/lang/Class;
    .registers 2

    .line 144
    iget-object v0, p0, Lorg/simpleframework/xml/core/SetPart;->method:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public getDependent()Ljava/lang/Class;
    .registers 3

    .line 120
    iget-object v0, p0, Lorg/simpleframework/xml/core/SetPart;->method:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/simpleframework/xml/core/Reflector;->getParameterDependent(Ljava/lang/reflect/Method;I)Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public getDependents()[Ljava/lang/Class;
    .registers 3

    .line 133
    iget-object v0, p0, Lorg/simpleframework/xml/core/SetPart;->method:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/simpleframework/xml/core/Reflector;->getParameterDependents(Ljava/lang/reflect/Method;I)[Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method

.method public getMethod()Ljava/lang/reflect/Method;
    .registers 3

    .line 197
    iget-object v0, p0, Lorg/simpleframework/xml/core/SetPart;->method:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->isAccessible()Z

    move-result v0

    if-nez v0, :cond_e

    .line 198
    iget-object v0, p0, Lorg/simpleframework/xml/core/SetPart;->method:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 200
    :cond_e
    iget-object v0, p0, Lorg/simpleframework/xml/core/SetPart;->method:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method public getMethodType()Lorg/simpleframework/xml/core/MethodType;
    .registers 2

    .line 186
    iget-object v0, p0, Lorg/simpleframework/xml/core/SetPart;->type:Lorg/simpleframework/xml/core/MethodType;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    .line 96
    iget-object v0, p0, Lorg/simpleframework/xml/core/SetPart;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/Class;
    .registers 3

    .line 107
    iget-object v0, p0, Lorg/simpleframework/xml/core/SetPart;->method:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .line 212
    iget-object v0, p0, Lorg/simpleframework/xml/core/SetPart;->method:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->toGenericString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

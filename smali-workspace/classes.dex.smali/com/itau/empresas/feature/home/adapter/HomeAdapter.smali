.class public Lcom/itau/empresas/feature/home/adapter/HomeAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "HomeAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardSimples;,
        Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardPendencias;,
        Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardCobrancaExpansivel;,
        Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardSaldoExpansivel;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter<Landroid/support/v7/widget/RecyclerView$ViewHolder;>;"
    }
.end annotation


# instance fields
.field private mApplication:Lcom/itau/empresas/CustomApplication;

.field private mListaHome:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/Object;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/itau/empresas/CustomApplication;)V
    .registers 3
    .param p1, "listaHome"    # Ljava/util/List;
    .param p2, "application"    # Lcom/itau/empresas/CustomApplication;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/Object;>;Lcom/itau/empresas/CustomApplication;)V"
        }
    .end annotation

    .line 36
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/itau/empresas/feature/home/adapter/HomeAdapter;->mListaHome:Ljava/util/List;

    .line 38
    iput-object p2, p0, Lcom/itau/empresas/feature/home/adapter/HomeAdapter;->mApplication:Lcom/itau/empresas/CustomApplication;

    .line 39
    return-void
.end method

.method private configuraCardPendencias(Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardPendencias;)V
    .registers 7
    .param p1, "cardPendencias"    # Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardPendencias;

    .line 157
    const/4 v2, 0x0

    .line 158
    .local v2, "quantidadePendencias":I
    iget-object v0, p0, Lcom/itau/empresas/feature/home/adapter/HomeAdapter;->mApplication:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getPendenciasVO()Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 159
    iget-object v0, p0, Lcom/itau/empresas/feature/home/adapter/HomeAdapter;->mApplication:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getPendenciasVO()Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;->getTotalPendencias()I

    move-result v2

    .line 161
    :cond_13
    sget-object v3, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->NAO_POSSUI_PENDENCIAS:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    .line 162
    .local v3, "estadosPendencias":Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;
    iget-object v0, p1, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardPendencias;->parentView:Lcom/itau/empresas/ui/view/CardAlertaView;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/CardAlertaView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 163
    const v1, 0x7f070513

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 165
    .local v4, "textoCardAlerta":Ljava/lang/String;
    if-lez v2, :cond_44

    .line 166
    sget-object v3, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->POSSUI_PENDENCIAS:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    .line 168
    const/4 v0, 0x1

    if-le v2, v0, :cond_37

    .line 169
    iget-object v0, p1, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardPendencias;->parentView:Lcom/itau/empresas/ui/view/CardAlertaView;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/CardAlertaView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 170
    const v1, 0x7f070510

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_44

    .line 172
    :cond_37
    iget-object v0, p1, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardPendencias;->parentView:Lcom/itau/empresas/ui/view/CardAlertaView;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/CardAlertaView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 173
    const v1, 0x7f07050f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 177
    :cond_44
    :goto_44
    iget-object v0, p1, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardPendencias;->parentView:Lcom/itau/empresas/ui/view/CardAlertaView;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/CardAlertaView;->getTextoCardInformativo()Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x50

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 178
    iget-object v0, p1, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardPendencias;->parentView:Lcom/itau/empresas/ui/view/CardAlertaView;

    invoke-virtual {v0, v2, v4, v3}, Lcom/itau/empresas/ui/view/CardAlertaView;->bind(ILjava/lang/String;Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;)V

    .line 179
    return-void
.end method


# virtual methods
.method public atualizaAdapter(Ljava/util/List;)V
    .registers 3
    .param p1, "items"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/Object;>;)V"
        }
    .end annotation

    .line 94
    iget-object v0, p0, Lcom/itau/empresas/feature/home/adapter/HomeAdapter;->mListaHome:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 95
    iget-object v0, p0, Lcom/itau/empresas/feature/home/adapter/HomeAdapter;->mListaHome:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 96
    return-void
.end method

.method public getItemCount()I
    .registers 2

    .line 183
    iget-object v0, p0, Lcom/itau/empresas/feature/home/adapter/HomeAdapter;->mListaHome:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .registers 7
    .param p1, "position"    # I

    .line 189
    const/4 v1, 0x3

    .line 191
    .local v1, "itemViewType":I
    iget-object v0, p0, Lcom/itau/empresas/feature/home/adapter/HomeAdapter;->mListaHome:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/itau/empresas/api/model/MenuVO;

    if-eqz v0, :cond_47

    .line 193
    iget-object v0, p0, Lcom/itau/empresas/feature/home/adapter/HomeAdapter;->mListaHome:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/api/model/MenuVO;

    .line 195
    .local v2, "cardsExibicao":Lcom/itau/empresas/api/model/MenuVO;
    invoke-virtual {v2}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v3

    const/4 v4, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_48

    goto :goto_3e

    :sswitch_21
    const-string v0, "card_de_saldo"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3e

    const/4 v4, 0x0

    goto :goto_3e

    :sswitch_2b
    const-string v0, "card_de_cobranca"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3e

    const/4 v4, 0x1

    goto :goto_3e

    :sswitch_35
    const-string v0, "card_de_pendencias"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3e

    const/4 v4, 0x2

    :cond_3e
    :goto_3e
    packed-switch v4, :pswitch_data_56

    goto :goto_47

    .line 197
    :pswitch_42
    const/4 v1, 0x1

    .line 198
    goto :goto_47

    .line 200
    :pswitch_44
    const/4 v1, 0x2

    .line 201
    goto :goto_47

    .line 203
    :pswitch_46
    const/4 v1, 0x0

    .line 204
    .line 210
    .end local v2    # "cardsExibicao":Lcom/itau/empresas/api/model/MenuVO;
    :cond_47
    :goto_47
    return v1

    :sswitch_data_48
    .sparse-switch
        -0x39b55305 -> :sswitch_35
        0x251bbfa -> :sswitch_21
        0x3bb69856 -> :sswitch_2b
    .end sparse-switch

    :pswitch_data_56
    .packed-switch 0x0
        :pswitch_42
        :pswitch_44
        :pswitch_46
    .end packed-switch
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .registers 7
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .line 122
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v0

    if-nez v0, :cond_d

    .line 124
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardPendencias;

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/home/adapter/HomeAdapter;->configuraCardPendencias(Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardPendencias;)V

    .line 126
    return-void

    .line 129
    :cond_d
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2a

    .line 131
    move-object v3, p1

    check-cast v3, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardSaldoExpansivel;

    .line 132
    .local v3, "cardSaldoExpansivel":Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardSaldoExpansivel;
    iget-object v0, v3, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardSaldoExpansivel;->parentView:Lcom/itau/empresas/feature/home/view/CardContasView;

    iget-object v1, v3, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardSaldoExpansivel;->parentView:Lcom/itau/empresas/feature/home/view/CardContasView;

    .line 133
    invoke-virtual {v1}, Lcom/itau/empresas/feature/home/view/CardContasView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0704c3

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 132
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/home/view/CardContasView;->setTextoTituloCard(Ljava/lang/String;)V

    .line 135
    return-void

    .line 138
    .end local v3    # "cardSaldoExpansivel":Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardSaldoExpansivel;
    :cond_2a
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_47

    .line 140
    move-object v3, p1

    check-cast v3, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardCobrancaExpansivel;

    .line 141
    .local v3, "cardCobranca":Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardCobrancaExpansivel;
    iget-object v0, v3, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardCobrancaExpansivel;->parentView:Lcom/itau/empresas/feature/home/view/CardCobrancaView;

    iget-object v1, v3, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardCobrancaExpansivel;->parentView:Lcom/itau/empresas/feature/home/view/CardCobrancaView;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 142
    const v2, 0x7f07050c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 141
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->setTextoTituloCard(Ljava/lang/String;)V

    .line 144
    return-void

    .line 147
    .end local v3    # "cardCobranca":Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardCobrancaExpansivel;
    :cond_47
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_6a

    .line 148
    move-object v3, p1

    check-cast v3, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardSimples;

    .line 149
    .local v3, "cardSimples":Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardSimples;
    iget-object v0, v3, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardSimples;->parentView:Lcom/itau/empresas/feature/home/view/ICardSimplesView;

    const-string v1, "fale sobre o app"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/home/view/ICardSimplesView;->setaTitulo(Ljava/lang/String;)V

    .line 150
    iget-object v0, v3, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardSimples;->parentView:Lcom/itau/empresas/feature/home/view/ICardSimplesView;

    iget-object v1, v3, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardSimples;->parentView:Lcom/itau/empresas/feature/home/view/ICardSimplesView;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/home/view/ICardSimplesView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 151
    const v2, 0x7f070258

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 150
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/home/view/ICardSimplesView;->setaIcone(Ljava/lang/String;)V

    .line 153
    .end local v3    # "cardSimples":Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardSimples;
    :cond_6a
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .registers 6
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .line 102
    packed-switch p2, :pswitch_data_40

    goto :goto_30

    .line 104
    :pswitch_4
    new-instance v2, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardPendencias;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/view/CardAlertaView_;->build(Landroid/content/Context;)Lcom/itau/empresas/ui/view/CardAlertaView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {v2, v0, v1}, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardPendencias;-><init>(Lcom/itau/empresas/ui/view/CardAlertaView;Lcom/itau/empresas/feature/home/adapter/HomeAdapter$1;)V

    .line 105
    .local v2, "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    goto :goto_3e

    .line 107
    .end local v2    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :pswitch_13
    new-instance v2, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardSaldoExpansivel;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/home/view/CardContasView_;->build(Landroid/content/Context;)Lcom/itau/empresas/feature/home/view/CardContasView;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardSaldoExpansivel;-><init>(Lcom/itau/empresas/feature/home/view/CardContasView;)V

    .line 108
    .local v2, "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    goto :goto_3e

    .line 110
    .end local v2    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :pswitch_21
    new-instance v2, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardCobrancaExpansivel;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->build(Landroid/content/Context;)Lcom/itau/empresas/feature/home/view/CardCobrancaView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {v2, v0, v1}, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardCobrancaExpansivel;-><init>(Lcom/itau/empresas/feature/home/view/CardCobrancaView;Lcom/itau/empresas/feature/home/adapter/HomeAdapter$1;)V

    .line 111
    .local v2, "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    goto :goto_3e

    .line 113
    .end local v2    # "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :goto_30
    new-instance v2, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardSimples;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/home/view/ICardSimplesView_;->build(Landroid/content/Context;)Lcom/itau/empresas/feature/home/view/ICardSimplesView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {v2, v0, v1}, Lcom/itau/empresas/feature/home/adapter/HomeAdapter$ViewHolderCardSimples;-><init>(Lcom/itau/empresas/feature/home/view/ICardSimplesView;Lcom/itau/empresas/feature/home/adapter/HomeAdapter$1;)V

    .line 116
    .local v2, "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :goto_3e
    return-object v2

    nop

    :pswitch_data_40
    .packed-switch 0x0
        :pswitch_4
        :pswitch_13
        :pswitch_21
    .end packed-switch
.end method

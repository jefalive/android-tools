.class public final Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;
.super Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;
.source "RecargaConfirmacaoCadastroContatoActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 42
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;-><init>()V

    .line 46
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;

    .line 42
    invoke-super {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;

    .line 42
    invoke-super {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 62
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 63
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 64
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 65
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->app:Lcom/itau/empresas/CustomApplication;

    .line 66
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->recargaController:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    .line 67
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->injectExtras_()V

    .line 68
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->afterInject()V

    .line 69
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 138
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 139
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_52

    .line 140
    const-string v0, "adicionado"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 141
    const-string v0, "adicionado"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->adicionado:Ljava/lang/Boolean;

    .line 143
    :cond_1c
    const-string v0, "contatoRecargaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 144
    const-string v0, "contatoRecargaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->contatoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 146
    :cond_2e
    const-string v0, "operadoraVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 147
    const-string v0, "operadoraVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->operadoraVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;

    .line 149
    :cond_40
    const-string v0, "regiaoVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_52

    .line 150
    const-string v0, "regiaoVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->regiaoVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;

    .line 153
    :cond_52
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 90
    new-instance v0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 178
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$4;-><init>(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 186
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 166
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$3;-><init>(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 174
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 55
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->init_(Landroid/os/Bundle;)V

    .line 56
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->onCreate(Landroid/os/Bundle;)V

    .line 57
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 58
    const v0, 0x7f030029

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->setContentView(I)V

    .line 59
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 103
    const v0, 0x7f0e0118

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->root:Landroid/widget/LinearLayout;

    .line 104
    const v0, 0x7f0e011b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->textoConfirmacao:Landroid/widget/TextView;

    .line 105
    const v0, 0x7f0e011e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->textoNomeContato:Landroid/widget/TextView;

    .line 106
    const v0, 0x7f0e011f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->textoNumeroContato:Landroid/widget/TextView;

    .line 107
    const v0, 0x7f0e0120

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->textoOperadoraContato:Landroid/widget/TextView;

    .line 108
    const v0, 0x7f0e0178

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->textoEscolhaPeriodo:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f0e0179

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/library/LinearValuePickerView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->escolhePeriodo:Lbr/com/itau/library/LinearValuePickerView;

    .line 110
    const v0, 0x7f0e0121

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->valorRecargaContato:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;

    .line 111
    const v0, 0x7f0e0122

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->botaoIncluirCadastroContato:Landroid/widget/Button;

    .line 112
    const v0, 0x7f0e0123

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->botaoAutorizarCadastroContato:Landroid/widget/Button;

    .line 113
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 114
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->botaoIncluirCadastroContato:Landroid/widget/Button;

    if-eqz v0, :cond_87

    .line 115
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->botaoIncluirCadastroContato:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$1;-><init>(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    :cond_87
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->botaoAutorizarCadastroContato:Landroid/widget/Button;

    if-eqz v0, :cond_95

    .line 125
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->botaoAutorizarCadastroContato:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_$2;-><init>(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    :cond_95
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->carregaElementosDaTela()V

    .line 135
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 73
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->setContentView(I)V

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 75
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 85
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->setContentView(Landroid/view/View;)V

    .line 86
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 87
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 79
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 81
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 157
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity;->setIntent(Landroid/content/Intent;)V

    .line 158
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaConfirmacaoCadastroContatoActivity_;->injectExtras_()V

    .line 159
    return-void
.end method

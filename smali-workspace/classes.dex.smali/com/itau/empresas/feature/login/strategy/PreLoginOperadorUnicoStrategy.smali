.class Lcom/itau/empresas/feature/login/strategy/PreLoginOperadorUnicoStrategy;
.super Ljava/lang/Object;
.source "PreLoginOperadorUnicoStrategy.java"

# interfaces
.implements Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;
.implements Lcom/itau/empresas/feature/login/strategy/PreLoginControllerListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private controller:Lcom/itau/empresas/feature/login/controller/PreLoginController;

.field private operador:Ljava/lang/String;

.field private strategy:Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 12
    const-class v0, Lcom/itau/empresas/feature/login/strategy/PreLoginOperadorUnicoStrategy;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/feature/login/strategy/PreLoginOperadorUnicoStrategy;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/itau/empresas/feature/login/controller/PreLoginController;Ljava/lang/String;Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;)V
    .registers 4
    .param p1, "controller"    # Lcom/itau/empresas/feature/login/controller/PreLoginController;
    .param p2, "operador"    # Ljava/lang/String;
    .param p3, "strategy"    # Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginOperadorUnicoStrategy;->controller:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    .line 24
    invoke-virtual {p1, p0}, Lcom/itau/empresas/feature/login/controller/PreLoginController;->setListener(Lcom/itau/empresas/feature/login/strategy/PreLoginControllerListener;)V

    .line 25
    iput-object p2, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginOperadorUnicoStrategy;->operador:Ljava/lang/String;

    .line 26
    iput-object p3, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginOperadorUnicoStrategy;->strategy:Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;

    .line 27
    return-void
.end method


# virtual methods
.method public backendException(Lbr/com/itau/sdk/android/core/exception/BackendException;)V
    .registers 5
    .param p1, "ex"    # Lbr/com/itau/sdk/android/core/exception/BackendException;

    .line 47
    sget-object v0, Lcom/itau/empresas/feature/login/strategy/PreLoginOperadorUnicoStrategy;->TAG:Ljava/lang/String;

    const-string v1, ">>> Erro ao Executar Strategy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v2

    .line 49
    .local v2, "error":Lbr/com/itau/sdk/android/core/model/ErrorDTO;
    if-eqz v2, :cond_16

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginOperadorUnicoStrategy;->strategy:Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;->falha(Ljava/lang/String;)V

    .line 52
    :cond_16
    return-void
.end method

.method public executar()V
    .registers 3

    .line 31
    sget-object v0, Lcom/itau/empresas/feature/login/strategy/PreLoginOperadorUnicoStrategy;->TAG:Ljava/lang/String;

    const-string v1, ">>> Executa Strategy de Operador Unico"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    iget-object v0, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginOperadorUnicoStrategy;->controller:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginOperadorUnicoStrategy;->operador:Ljava/lang/String;

    .line 33
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/controller/PreLoginController;->buscaOperador(Ljava/lang/String;)V

    const-string v0, "LogLegal"
    const-string v1, "Blaaaaaaaaaaaah"
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    return-void
.end method

.method public listaOperadores(Ljava/util/List;)V
    .registers 4
    .param p1, "operadorVOs"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/OperadorVO;>;)V"
        }
    .end annotation

    .line 38
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_14

    .line 39
    iget-object v0, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginOperadorUnicoStrategy;->strategy:Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;

    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/api/model/OperadorVO;

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;->sucesso(Lcom/itau/empresas/api/model/OperadorVO;)V

    goto :goto_1a

    .line 41
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginOperadorUnicoStrategy;->strategy:Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;->proximoPasso(I)V

    .line 43
    :goto_1a
    return-void
.end method

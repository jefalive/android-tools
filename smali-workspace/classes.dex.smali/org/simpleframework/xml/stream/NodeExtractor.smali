.class Lorg/simpleframework/xml/stream/NodeExtractor;
.super Ljava/util/LinkedList;
.source "NodeExtractor.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedList<Lorg/w3c/dom/Node;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lorg/w3c/dom/Document;)V
    .registers 2
    .param p1, "source"    # Lorg/w3c/dom/Document;

    .line 47
    invoke-direct {p0}, Ljava/util/LinkedList;-><init>()V

    .line 48
    invoke-direct {p0, p1}, Lorg/simpleframework/xml/stream/NodeExtractor;->extract(Lorg/w3c/dom/Document;)V

    .line 49
    return-void
.end method

.method private extract(Lorg/w3c/dom/Document;)V
    .registers 4
    .param p1, "source"    # Lorg/w3c/dom/Document;

    .line 60
    invoke-interface {p1}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v1

    .line 62
    .local v1, "node":Lorg/w3c/dom/Node;
    if-eqz v1, :cond_c

    .line 63
    invoke-virtual {p0, v1}, Lorg/simpleframework/xml/stream/NodeExtractor;->offer(Ljava/lang/Object;)Z

    .line 64
    invoke-direct {p0, v1}, Lorg/simpleframework/xml/stream/NodeExtractor;->extract(Lorg/w3c/dom/Node;)V

    .line 66
    :cond_c
    return-void
.end method

.method private extract(Lorg/w3c/dom/Node;)V
    .registers 8
    .param p1, "source"    # Lorg/w3c/dom/Node;

    .line 77
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 78
    .local v1, "list":Lorg/w3c/dom/NodeList;
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    .line 80
    .local v2, "length":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_9
    if-ge v3, v2, :cond_20

    .line 81
    invoke-interface {v1, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 82
    .local v4, "node":Lorg/w3c/dom/Node;
    invoke-interface {v4}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v5

    .line 84
    .local v5, "type":S
    const/16 v0, 0x8

    if-eq v5, v0, :cond_1d

    .line 85
    invoke-virtual {p0, v4}, Lorg/simpleframework/xml/stream/NodeExtractor;->offer(Ljava/lang/Object;)Z

    .line 86
    invoke-direct {p0, v4}, Lorg/simpleframework/xml/stream/NodeExtractor;->extract(Lorg/w3c/dom/Node;)V

    .line 80
    .end local v4    # "node":Lorg/w3c/dom/Node;
    .end local v5    # "type":S
    :cond_1d
    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    .line 89
    .end local v3    # "i":I
    :cond_20
    return-void
.end method

.class final Lcom/adobe/mobile/MessageMatcherStartsWith;
.super Lcom/adobe/mobile/MessageMatcher;
.source "MessageMatcherStartsWith.java"


# direct methods
.method constructor <init>()V
    .registers 1

    .line 23
    invoke-direct {p0}, Lcom/adobe/mobile/MessageMatcher;-><init>()V

    return-void
.end method


# virtual methods
.method protected matches(Ljava/lang/Object;)Z
    .registers 9
    .param p1, "value"    # Ljava/lang/Object;

    .line 26
    instance-of v2, p1, Ljava/lang/String;

    .line 27
    .local v2, "valueIsString":Z
    instance-of v3, p1, Ljava/lang/Number;

    .line 28
    .local v3, "valueIsNumber":Z
    if-nez v2, :cond_a

    if-nez v3, :cond_a

    .line 29
    const/4 v0, 0x0

    return v0

    .line 32
    :cond_a
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 33
    .local v4, "stringToMatch":Ljava/lang/String;
    iget-object v0, p0, Lcom/adobe/mobile/MessageMatcherStartsWith;->values:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_14
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 34
    .local v6, "v":Ljava/lang/Object;
    instance-of v0, v6, Ljava/lang/String;

    if-eqz v0, :cond_4b

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "(?i)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4b

    .line 35
    const/4 v0, 0x1

    return v0

    .line 37
    .end local v6    # "v":Ljava/lang/Object;
    :cond_4b
    goto :goto_14

    .line 39
    :cond_4c
    const/4 v0, 0x0

    return v0
.end method

.class public final Lbr/com/itau/widgets/hintview/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/widgets/hintview/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final abc_action_bar_content_inset_material:I = 0x7f08001a

.field public static final abc_action_bar_default_height_material:I = 0x7f080002

.field public static final abc_action_bar_default_padding_end_material:I = 0x7f08001c

.field public static final abc_action_bar_default_padding_start_material:I = 0x7f08001d

.field public static final abc_action_bar_icon_vertical_padding_material:I = 0x7f080041

.field public static final abc_action_bar_overflow_padding_end_material:I = 0x7f080042

.field public static final abc_action_bar_overflow_padding_start_material:I = 0x7f080043

.field public static final abc_action_bar_progress_bar_size:I = 0x7f080003

.field public static final abc_action_bar_stacked_max_height:I = 0x7f080044

.field public static final abc_action_bar_stacked_tab_max_width:I = 0x7f080045

.field public static final abc_action_bar_subtitle_bottom_margin_material:I = 0x7f080046

.field public static final abc_action_bar_subtitle_top_margin_material:I = 0x7f080047

.field public static final abc_action_button_min_height_material:I = 0x7f080048

.field public static final abc_action_button_min_width_material:I = 0x7f080049

.field public static final abc_action_button_min_width_overflow_material:I = 0x7f08004a

.field public static final abc_alert_dialog_button_bar_height:I = 0x7f080000

.field public static final abc_button_inset_horizontal_material:I = 0x7f08004b

.field public static final abc_button_inset_vertical_material:I = 0x7f08004c

.field public static final abc_button_padding_horizontal_material:I = 0x7f08004d

.field public static final abc_button_padding_vertical_material:I = 0x7f08004e

.field public static final abc_config_prefDialogWidth:I = 0x7f08000b

.field public static final abc_control_corner_material:I = 0x7f080050

.field public static final abc_control_inset_material:I = 0x7f080051

.field public static final abc_control_padding_material:I = 0x7f080052

.field public static final abc_dialog_fixed_height_major:I = 0x7f08000c

.field public static final abc_dialog_fixed_height_minor:I = 0x7f08000d

.field public static final abc_dialog_fixed_width_major:I = 0x7f08000e

.field public static final abc_dialog_fixed_width_minor:I = 0x7f08000f

.field public static final abc_dialog_list_padding_vertical_material:I = 0x7f080053

.field public static final abc_dialog_min_width_major:I = 0x7f080010

.field public static final abc_dialog_min_width_minor:I = 0x7f080011

.field public static final abc_dialog_padding_material:I = 0x7f080054

.field public static final abc_dialog_padding_top_material:I = 0x7f080055

.field public static final abc_disabled_alpha_material_dark:I = 0x7f080056

.field public static final abc_disabled_alpha_material_light:I = 0x7f080057

.field public static final abc_dropdownitem_icon_width:I = 0x7f080058

.field public static final abc_dropdownitem_text_padding_left:I = 0x7f080059

.field public static final abc_dropdownitem_text_padding_right:I = 0x7f08005a

.field public static final abc_edit_text_inset_bottom_material:I = 0x7f08005b

.field public static final abc_edit_text_inset_horizontal_material:I = 0x7f08005c

.field public static final abc_edit_text_inset_top_material:I = 0x7f08005d

.field public static final abc_floating_window_z:I = 0x7f08005e

.field public static final abc_list_item_padding_horizontal_material:I = 0x7f08005f

.field public static final abc_panel_menu_list_width:I = 0x7f080060

.field public static final abc_search_view_preferred_width:I = 0x7f080063

.field public static final abc_seekbar_track_background_height_material:I = 0x7f080064

.field public static final abc_seekbar_track_progress_height_material:I = 0x7f080065

.field public static final abc_select_dialog_padding_start_material:I = 0x7f080066

.field public static final abc_switch_padding:I = 0x7f08003a

.field public static final abc_text_size_body_1_material:I = 0x7f080067

.field public static final abc_text_size_body_2_material:I = 0x7f080068

.field public static final abc_text_size_button_material:I = 0x7f080069

.field public static final abc_text_size_caption_material:I = 0x7f08006a

.field public static final abc_text_size_display_1_material:I = 0x7f08006b

.field public static final abc_text_size_display_2_material:I = 0x7f08006c

.field public static final abc_text_size_display_3_material:I = 0x7f08006d

.field public static final abc_text_size_display_4_material:I = 0x7f08006e

.field public static final abc_text_size_headline_material:I = 0x7f08006f

.field public static final abc_text_size_large_material:I = 0x7f080070

.field public static final abc_text_size_medium_material:I = 0x7f080071

.field public static final abc_text_size_menu_material:I = 0x7f080073

.field public static final abc_text_size_small_material:I = 0x7f080074

.field public static final abc_text_size_subhead_material:I = 0x7f080075

.field public static final abc_text_size_subtitle_material_toolbar:I = 0x7f080004

.field public static final abc_text_size_title_material:I = 0x7f080076

.field public static final abc_text_size_title_material_toolbar:I = 0x7f080005

.field public static final action_button_padding:I = 0x7f08007c

.field public static final action_button_width:I = 0x7f08007d

.field public static final default_hint_arrow_height:I = 0x7f0800e1

.field public static final default_hint_arrow_margin:I = 0x7f0800e2

.field public static final default_hint_arrow_width:I = 0x7f0800e3

.field public static final default_hint_margin:I = 0x7f0800e4

.field public static final default_hint_padding:I = 0x7f0800e5

.field public static final disabled_alpha_material_dark:I = 0x7f080153

.field public static final disabled_alpha_material_light:I = 0x7f080154

.field public static final highlight_alpha_material_colored:I = 0x7f080171

.field public static final highlight_alpha_material_dark:I = 0x7f080172

.field public static final highlight_alpha_material_light:I = 0x7f080173

.field public static final notification_large_icon_height:I = 0x7f0801bc

.field public static final notification_large_icon_width:I = 0x7f0801bd

.field public static final notification_subtext_size:I = 0x7f0801c4

.field public static final view_hint_padding:I = 0x7f080221


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

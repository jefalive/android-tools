.class Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity$1;
.super Ljava/lang/Object;
.source "ProdutosMultilimiteActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;->aoIniciarActivity()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;

    .line 46
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity$1;->this$0:Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 10
    .param p1, "adapterView"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "i"    # I
    .param p4, "l"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/widget/AdapterView<*>;Landroid/view/View;IJ)V"
        }
    .end annotation

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity$1;->this$0:Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;->adapter:Lcom/itau/empresas/feature/credito/produtos/adapter/ProdutosMultilimiteListAdapter;

    invoke-virtual {v0, p3}, Lcom/itau/empresas/feature/credito/produtos/adapter/ProdutosMultilimiteListAdapter;->getItem(I)Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;

    move-result-object v3

    .line 50
    .local v3, "produto":Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity$1;->this$0:Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_$IntentBuilder_;

    move-result-object v0

    .line 51
    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;->getTitulo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_$IntentBuilder_;->titulo(Ljava/lang/String;)Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_$IntentBuilder_;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    .line 52
    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;->getAtributos()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_$IntentBuilder_;->atributoVOs(Ljava/util/ArrayList;)Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_$IntentBuilder_;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/produtos/activity/AtributosProdutoMultilimiteActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 54
    return-void
.end method

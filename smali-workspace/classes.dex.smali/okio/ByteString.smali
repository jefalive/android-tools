.class public Lokio/ByteString;
.super Ljava/lang/Object;
.source "ByteString.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Ljava/io/Serializable;Ljava/lang/Comparable<Lokio/ByteString;>;"
    }
.end annotation


# static fields
.field public static final EMPTY:Lokio/ByteString;

.field static final HEX_DIGITS:[C

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field final data:[B

.field transient hashCode:I

.field transient utf8:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 48
    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_14

    sput-object v0, Lokio/ByteString;->HEX_DIGITS:[C

    .line 53
    const/4 v0, 0x0

    new-array v0, v0, [B

    invoke-static {v0}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object v0

    sput-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    return-void

    nop

    :array_14
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x61s
        0x62s
        0x63s
        0x64s
        0x65s
        0x66s
    .end array-data
.end method

.method constructor <init>([B)V
    .registers 2
    .param p1, "data"    # [B

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lokio/ByteString;->data:[B

    .line 61
    return-void
.end method

.method static codePointIndexToCharIndex(Ljava/lang/String;I)I
    .registers 7
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "codePointCount"    # I

    .line 455
    const/4 v1, 0x0

    .local v1, "i":I
    const/4 v2, 0x0

    .local v2, "j":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .local v3, "length":I
    :goto_6
    if-ge v1, v3, :cond_2c

    .line 456
    if-ne v2, p1, :cond_b

    .line 457
    return v1

    .line 459
    :cond_b
    invoke-virtual {p0, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v4

    .line 460
    .local v4, "c":I
    invoke-static {v4}, Ljava/lang/Character;->isISOControl(I)Z

    move-result v0

    if-eqz v0, :cond_1d

    const/16 v0, 0xa

    if-eq v4, v0, :cond_1d

    const/16 v0, 0xd

    if-ne v4, v0, :cond_22

    :cond_1d
    const v0, 0xfffd

    if-ne v4, v0, :cond_24

    .line 462
    :cond_22
    const/4 v0, -0x1

    return v0

    .line 464
    :cond_24
    add-int/lit8 v2, v2, 0x1

    .line 455
    invoke-static {v4}, Ljava/lang/Character;->charCount(I)I

    move-result v0

    add-int/2addr v1, v0

    goto :goto_6

    .line 466
    .end local v1    # "i":I
    .end local v2    # "j":I
    .end local v3    # "length":I
    .end local v4    # "c":I
    :cond_2c
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    return v0
.end method

.method public static decodeBase64(Ljava/lang/String;)Lokio/ByteString;
    .registers 4
    .param p0, "base64"    # Ljava/lang/String;

    .line 144
    if-nez p0, :cond_a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "base64 == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 145
    :cond_a
    invoke-static {p0}, Lokio/Base64;->decode(Ljava/lang/String;)[B

    move-result-object v2

    .line 146
    .local v2, "decoded":[B
    if-eqz v2, :cond_16

    new-instance v0, Lokio/ByteString;

    invoke-direct {v0, v2}, Lokio/ByteString;-><init>([B)V

    goto :goto_17

    :cond_16
    const/4 v0, 0x0

    :goto_17
    return-object v0
.end method

.method public static encodeUtf8(Ljava/lang/String;)Lokio/ByteString;
    .registers 4
    .param p0, "s"    # Ljava/lang/String;

    .line 86
    if-nez p0, :cond_a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "s == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_a
    new-instance v2, Lokio/ByteString;

    sget-object v0, Lokio/Util;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-direct {v2, v0}, Lokio/ByteString;-><init>([B)V

    .line 88
    .local v2, "byteString":Lokio/ByteString;
    iput-object p0, v2, Lokio/ByteString;->utf8:Ljava/lang/String;

    .line 89
    return-object v2
.end method

.method public static varargs of([B)Lokio/ByteString;
    .registers 3
    .param p0, "data"    # [B

    .line 67
    if-nez p0, :cond_a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "data == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_a
    new-instance v0, Lokio/ByteString;

    invoke-virtual {p0}, [B->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    invoke-direct {v0, v1}, Lokio/ByteString;-><init>([B)V

    return-object v0
.end method

.method public static read(Ljava/io/InputStream;I)Lokio/ByteString;
    .registers 8
    .param p0, "in"    # Ljava/io/InputStream;
    .param p1, "byteCount"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 188
    if-nez p0, :cond_a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "in == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 189
    :cond_a
    if-gez p1, :cond_25

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 191
    :cond_25
    new-array v3, p1, [B

    .line 192
    .local v3, "result":[B
    const/4 v4, 0x0

    .local v4, "offset":I
    :goto_28
    if-ge v4, p1, :cond_3b

    .line 193
    sub-int v0, p1, v4

    invoke-virtual {p0, v3, v4, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v5

    .line 194
    .local v5, "read":I
    const/4 v0, -0x1

    if-ne v5, v0, :cond_39

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 192
    :cond_39
    add-int/2addr v4, v5

    goto :goto_28

    .line 196
    .end local v4    # "offset":I
    .end local v5    # "read":I
    :cond_3b
    new-instance v0, Lokio/ByteString;

    invoke-direct {v0, v3}, Lokio/ByteString;-><init>([B)V

    return-object v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .registers 7
    .param p1, "in"    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 470
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v2

    .line 471
    .local v2, "dataLength":I
    invoke-static {p1, v2}, Lokio/ByteString;->read(Ljava/io/InputStream;I)Lokio/ByteString;

    move-result-object v3

    .line 473
    .local v3, "byteString":Lokio/ByteString;
    :try_start_8
    const-class v0, Lokio/ByteString;

    const-string v1, "data"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v4

    .line 474
    .local v4, "field":Ljava/lang/reflect/Field;
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 475
    iget-object v0, v3, Lokio/ByteString;->data:[B

    invoke-virtual {v4, p0, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_19
    .catch Ljava/lang/NoSuchFieldException; {:try_start_8 .. :try_end_19} :catch_1a
    .catch Ljava/lang/IllegalAccessException; {:try_start_8 .. :try_end_19} :catch_21

    .line 480
    .end local v4    # "field":Ljava/lang/reflect/Field;
    goto :goto_28

    .line 476
    :catch_1a
    move-exception v4

    .line 477
    .local v4, "e":Ljava/lang/NoSuchFieldException;
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 478
    .end local v4    # "e":Ljava/lang/NoSuchFieldException;
    :catch_21
    move-exception v4

    .line 479
    .local v4, "e":Ljava/lang/IllegalAccessException;
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 481
    .end local v4    # "e":Ljava/lang/IllegalAccessException;
    :goto_28
    return-void
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .registers 3
    .param p1, "out"    # Ljava/io/ObjectOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 484
    iget-object v0, p0, Lokio/ByteString;->data:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 485
    iget-object v0, p0, Lokio/ByteString;->data:[B

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->write([B)V

    .line 486
    return-void
.end method


# virtual methods
.method public base64()Ljava/lang/String;
    .registers 2

    .line 105
    iget-object v0, p0, Lokio/ByteString;->data:[B

    invoke-static {v0}, Lokio/Base64;->encode([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3

    .line 47
    move-object v0, p1

    check-cast v0, Lokio/ByteString;

    invoke-virtual {p0, v0}, Lokio/ByteString;->compareTo(Lokio/ByteString;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lokio/ByteString;)I
    .registers 10
    .param p1, "byteString"    # Lokio/ByteString;

    .line 415
    invoke-virtual {p0}, Lokio/ByteString;->size()I

    move-result v2

    .line 416
    .local v2, "sizeA":I
    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result v3

    .line 417
    .local v3, "sizeB":I
    const/4 v4, 0x0

    .local v4, "i":I
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v5

    .local v5, "size":I
    :goto_d
    if-ge v4, v5, :cond_27

    .line 418
    invoke-virtual {p0, v4}, Lokio/ByteString;->getByte(I)B

    move-result v0

    and-int/lit16 v6, v0, 0xff

    .line 419
    .local v6, "byteA":I
    invoke-virtual {p1, v4}, Lokio/ByteString;->getByte(I)B

    move-result v0

    and-int/lit16 v7, v0, 0xff

    .line 420
    .local v7, "byteB":I
    if-ne v6, v7, :cond_1e

    goto :goto_24

    .line 421
    :cond_1e
    if-ge v6, v7, :cond_22

    const/4 v0, -0x1

    goto :goto_23

    :cond_22
    const/4 v0, 0x1

    :goto_23
    return v0

    .line 417
    .end local v6    # "byteA":I
    .end local v7    # "byteB":I
    :goto_24
    add-int/lit8 v4, v4, 0x1

    goto :goto_d

    .line 423
    .end local v4    # "i":I
    .end local v5    # "size":I
    :cond_27
    if-ne v2, v3, :cond_2b

    const/4 v0, 0x0

    return v0

    .line 424
    :cond_2b
    if-ge v2, v3, :cond_2f

    const/4 v0, -0x1

    goto :goto_30

    :cond_2f
    const/4 v0, 0x1

    :goto_30
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .param p1, "o"    # Ljava/lang/Object;

    .line 403
    if-ne p1, p0, :cond_4

    const/4 v0, 0x1

    return v0

    .line 404
    :cond_4
    instance-of v0, p1, Lokio/ByteString;

    if-eqz v0, :cond_26

    move-object v0, p1

    check-cast v0, Lokio/ByteString;

    .line 405
    invoke-virtual {v0}, Lokio/ByteString;->size()I

    move-result v0

    iget-object v1, p0, Lokio/ByteString;->data:[B

    array-length v1, v1

    if-ne v0, v1, :cond_26

    move-object v0, p1

    check-cast v0, Lokio/ByteString;

    iget-object v1, p0, Lokio/ByteString;->data:[B

    iget-object v2, p0, Lokio/ByteString;->data:[B

    array-length v2, v2

    .line 406
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v1, v4, v2}, Lokio/ByteString;->rangeEquals(I[BII)Z

    move-result v0

    if-eqz v0, :cond_26

    const/4 v0, 0x1

    goto :goto_27

    :cond_26
    const/4 v0, 0x0

    .line 404
    :goto_27
    return v0
.end method

.method public getByte(I)B
    .registers 3
    .param p1, "pos"    # I

    .line 282
    iget-object v0, p0, Lokio/ByteString;->data:[B

    aget-byte v0, v0, p1

    return v0
.end method

.method public hashCode()I
    .registers 3

    .line 410
    iget v1, p0, Lokio/ByteString;->hashCode:I

    .line 411
    .local v1, "result":I
    if-eqz v1, :cond_6

    move v0, v1

    goto :goto_e

    :cond_6
    iget-object v0, p0, Lokio/ByteString;->data:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    iput v0, p0, Lokio/ByteString;->hashCode:I

    :goto_e
    return v0
.end method

.method public hex()Ljava/lang/String;
    .registers 11

    .line 151
    iget-object v0, p0, Lokio/ByteString;->data:[B

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    new-array v4, v0, [C

    .line 152
    .local v4, "result":[C
    const/4 v5, 0x0

    .line 153
    .local v5, "c":I
    iget-object v6, p0, Lokio/ByteString;->data:[B

    array-length v7, v6

    const/4 v8, 0x0

    :goto_c
    if-ge v8, v7, :cond_2b

    aget-byte v9, v6, v8

    .line 154
    .local v9, "b":B
    move v0, v5

    add-int/lit8 v5, v5, 0x1

    sget-object v1, Lokio/ByteString;->HEX_DIGITS:[C

    shr-int/lit8 v2, v9, 0x4

    and-int/lit8 v2, v2, 0xf

    aget-char v1, v1, v2

    aput-char v1, v4, v0

    .line 155
    move v0, v5

    add-int/lit8 v5, v5, 0x1

    sget-object v1, Lokio/ByteString;->HEX_DIGITS:[C

    and-int/lit8 v2, v9, 0xf

    aget-char v1, v1, v2

    aput-char v1, v4, v0

    .line 153
    .end local v9    # "b":B
    add-int/lit8 v8, v8, 0x1

    goto :goto_c

    .line 157
    :cond_2b
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method

.method public rangeEquals(ILokio/ByteString;II)Z
    .registers 6
    .param p1, "offset"    # I
    .param p2, "other"    # Lokio/ByteString;
    .param p3, "otherOffset"    # I
    .param p4, "byteCount"    # I

    .line 328
    iget-object v0, p0, Lokio/ByteString;->data:[B

    invoke-virtual {p2, p3, v0, p1, p4}, Lokio/ByteString;->rangeEquals(I[BII)Z

    move-result v0

    return v0
.end method

.method public rangeEquals(I[BII)Z
    .registers 6
    .param p1, "offset"    # I
    .param p2, "other"    # [B
    .param p3, "otherOffset"    # I
    .param p4, "byteCount"    # I

    .line 337
    if-ltz p1, :cond_18

    iget-object v0, p0, Lokio/ByteString;->data:[B

    array-length v0, v0

    sub-int/2addr v0, p4

    if-gt p1, v0, :cond_18

    if-ltz p3, :cond_18

    array-length v0, p2

    sub-int/2addr v0, p4

    if-gt p3, v0, :cond_18

    iget-object v0, p0, Lokio/ByteString;->data:[B

    .line 339
    invoke-static {v0, p1, p2, p3, p4}, Lokio/Util;->arrayRangeEquals([BI[BII)Z

    move-result v0

    if-eqz v0, :cond_18

    const/4 v0, 0x1

    goto :goto_19

    :cond_18
    const/4 v0, 0x0

    .line 337
    :goto_19
    return v0
.end method

.method public size()I
    .registers 2

    .line 289
    iget-object v0, p0, Lokio/ByteString;->data:[B

    array-length v0, v0

    return v0
.end method

.method public substring(II)Lokio/ByteString;
    .registers 8
    .param p1, "beginIndex"    # I
    .param p2, "endIndex"    # I

    .line 263
    if-gez p1, :cond_a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "beginIndex < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 264
    :cond_a
    iget-object v0, p0, Lokio/ByteString;->data:[B

    array-length v0, v0

    if-le p2, v0, :cond_31

    .line 265
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "endIndex > length("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lokio/ByteString;->data:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 268
    :cond_31
    sub-int v3, p2, p1

    .line 269
    .local v3, "subLen":I
    if-gez v3, :cond_3d

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "endIndex < beginIndex"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 271
    :cond_3d
    if-nez p1, :cond_45

    iget-object v0, p0, Lokio/ByteString;->data:[B

    array-length v0, v0

    if-ne p2, v0, :cond_45

    .line 272
    return-object p0

    .line 275
    :cond_45
    new-array v4, v3, [B

    .line 276
    .local v4, "copy":[B
    iget-object v0, p0, Lokio/ByteString;->data:[B

    const/4 v1, 0x0

    invoke-static {v0, p1, v4, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 277
    new-instance v0, Lokio/ByteString;

    invoke-direct {v0, v4}, Lokio/ByteString;-><init>([B)V

    return-object v0
.end method

.method public toAsciiLowercase()Lokio/ByteString;
    .registers 6

    .line 206
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v0, p0, Lokio/ByteString;->data:[B

    array-length v0, v0

    if-ge v2, v0, :cond_43

    .line 207
    iget-object v0, p0, Lokio/ByteString;->data:[B

    aget-byte v3, v0, v2

    .line 208
    .local v3, "c":B
    const/16 v0, 0x41

    if-lt v3, v0, :cond_40

    const/16 v0, 0x5a

    if-le v3, v0, :cond_13

    goto :goto_40

    .line 212
    :cond_13
    iget-object v0, p0, Lokio/ByteString;->data:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, [B

    .line 213
    .local v4, "lowercase":[B
    move v0, v2

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v3, 0x20

    int-to-byte v1, v1

    aput-byte v1, v4, v0

    .line 214
    :goto_24
    array-length v0, v4

    if-ge v2, v0, :cond_3a

    .line 215
    aget-byte v3, v4, v2

    .line 216
    const/16 v0, 0x41

    if-lt v3, v0, :cond_37

    const/16 v0, 0x5a

    if-le v3, v0, :cond_32

    goto :goto_37

    .line 217
    :cond_32
    add-int/lit8 v0, v3, 0x20

    int-to-byte v0, v0

    aput-byte v0, v4, v2

    .line 214
    :cond_37
    :goto_37
    add-int/lit8 v2, v2, 0x1

    goto :goto_24

    .line 219
    :cond_3a
    new-instance v0, Lokio/ByteString;

    invoke-direct {v0, v4}, Lokio/ByteString;-><init>([B)V

    return-object v0

    .line 206
    .end local v3    # "c":B
    .end local v4    # "lowercase":[B
    :cond_40
    :goto_40
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 221
    .end local v2    # "i":I
    :cond_43
    return-object p0
.end method

.method public toByteArray()[B
    .registers 2

    .line 296
    iget-object v0, p0, Lokio/ByteString;->data:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 7

    .line 432
    iget-object v0, p0, Lokio/ByteString;->data:[B

    array-length v0, v0

    if-nez v0, :cond_8

    .line 433
    const-string v0, "[size=0]"

    return-object v0

    .line 436
    :cond_8
    invoke-virtual {p0}, Lokio/ByteString;->utf8()Ljava/lang/String;

    move-result-object v3

    .line 437
    .local v3, "text":Ljava/lang/String;
    const/16 v0, 0x40

    invoke-static {v3, v0}, Lokio/ByteString;->codePointIndexToCharIndex(Ljava/lang/String;I)I

    move-result v4

    .line 439
    .local v4, "i":I
    const/4 v0, -0x1

    if-ne v4, v0, :cond_6c

    .line 440
    iget-object v0, p0, Lokio/ByteString;->data:[B

    array-length v0, v0

    const/16 v1, 0x40

    if-gt v0, v1, :cond_3a

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[hex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 441
    invoke-virtual {p0}, Lokio/ByteString;->hex()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_6b

    :cond_3a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lokio/ByteString;->data:[B

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " hex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 442
    const/4 v1, 0x0

    const/16 v2, 0x40

    invoke-virtual {p0, v1, v2}, Lokio/ByteString;->substring(II)Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v1}, Lokio/ByteString;->hex()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u2026]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 440
    :goto_6b
    return-object v0

    .line 445
    :cond_6c
    const/4 v0, 0x0

    invoke-virtual {v3, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\"

    const-string v2, "\\\\"

    .line 446
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\n"

    const-string v2, "\\n"

    .line 447
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\r"

    const-string v2, "\\r"

    .line 448
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 449
    .local v5, "safeText":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v4, v0, :cond_b6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lokio/ByteString;->data:[B

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " text="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u2026]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_cf

    :cond_b6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[text="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_cf
    return-object v0
.end method

.method public utf8()Ljava/lang/String;
    .registers 5

    .line 94
    iget-object v3, p0, Lokio/ByteString;->utf8:Ljava/lang/String;

    .line 96
    .local v3, "result":Ljava/lang/String;
    if-eqz v3, :cond_6

    move-object v0, v3

    goto :goto_11

    :cond_6
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lokio/ByteString;->data:[B

    sget-object v2, Lokio/Util;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    iput-object v0, p0, Lokio/ByteString;->utf8:Ljava/lang/String;

    :goto_11
    return-object v0
.end method

.method write(Lokio/Buffer;)V
    .registers 5
    .param p1, "buffer"    # Lokio/Buffer;

    .line 319
    iget-object v0, p0, Lokio/ByteString;->data:[B

    iget-object v1, p0, Lokio/ByteString;->data:[B

    array-length v1, v1

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v1}, Lokio/Buffer;->write([BII)Lokio/Buffer;

    .line 320
    return-void
.end method

.class final enum Lorg/joda/money/format/MoneyFormatterBuilder$Singletons$2;
.super Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;
.source "MoneyFormatterBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .registers 5
    .param p3, "x0"    # Ljava/lang/String;

    .line 356
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;-><init>(Ljava/lang/String;ILjava/lang/String;Lorg/joda/money/format/MoneyFormatterBuilder$1;)V

    return-void
.end method


# virtual methods
.method public parse(Lorg/joda/money/format/MoneyParseContext;)V
    .registers 7
    .param p1, "context"    # Lorg/joda/money/format/MoneyParseContext;

    .line 363
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getIndex()I

    move-result v0

    add-int/lit8 v2, v0, 0x3

    .line 364
    .local v2, "endPos":I
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getTextLength()I

    move-result v0

    if-le v2, v0, :cond_10

    .line 365
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->setError()V

    goto :goto_27

    .line 367
    :cond_10
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getIndex()I

    move-result v0

    invoke-virtual {p1, v0, v2}, Lorg/joda/money/format/MoneyParseContext;->getTextSubstring(II)Ljava/lang/String;

    move-result-object v3

    .line 369
    .local v3, "code":Ljava/lang/String;
    :try_start_18
    invoke-static {v3}, Lorg/joda/money/CurrencyUnit;->ofNumericCode(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/joda/money/format/MoneyParseContext;->setCurrency(Lorg/joda/money/CurrencyUnit;)V

    .line 370
    invoke-virtual {p1, v2}, Lorg/joda/money/format/MoneyParseContext;->setIndex(I)V
    :try_end_22
    .catch Lorg/joda/money/IllegalCurrencyException; {:try_start_18 .. :try_end_22} :catch_23

    .line 373
    goto :goto_27

    .line 371
    :catch_23
    move-exception v4

    .line 372
    .local v4, "ex":Lorg/joda/money/IllegalCurrencyException;
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->setError()V

    .line 375
    .end local v3    # "code":Ljava/lang/String;
    .end local v4    # "ex":Lorg/joda/money/IllegalCurrencyException;
    :goto_27
    return-void
.end method

.method public print(Lorg/joda/money/format/MoneyPrintContext;Ljava/lang/Appendable;Lorg/joda/money/BigMoney;)V
    .registers 5
    .param p1, "context"    # Lorg/joda/money/format/MoneyPrintContext;
    .param p2, "appendable"    # Ljava/lang/Appendable;
    .param p3, "money"    # Lorg/joda/money/BigMoney;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 359
    invoke-virtual {p3}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/money/CurrencyUnit;->getNumeric3Code()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 360
    return-void
.end method

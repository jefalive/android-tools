.class Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment$1;
.super Ljava/lang/Object;
.source "RecargaNovoNumeroFragment.java"

# interfaces
.implements Lbr/com/itau/widgets/hintview/OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->adicionaHint(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;

    .line 193
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment$1;->this$0:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss()V
    .registers 5

    .line 196
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment$1;->this$0:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;

    # getter for: Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->fechouHintComClique:Z
    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->access$000(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 197
    return-void

    .line 199
    :cond_9
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment$1;->this$0:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;

    # getter for: Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->hintUtils:Lcom/itau/empresas/ui/util/HintUtils;
    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->access$100(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;)Lcom/itau/empresas/ui/util/HintUtils;

    move-result-object v0

    const-string v1, "CHVREC-QTD"

    .line 200
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/HintUtils;->consultarPreferenciasQuantidade(Ljava/lang/String;)I

    move-result v3

    .line 202
    .local v3, "quantidade":I
    const/4 v0, 0x1

    if-le v3, v0, :cond_25

    .line 203
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment$1;->this$0:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;

    # getter for: Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->hintUtils:Lcom/itau/empresas/ui/util/HintUtils;
    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->access$100(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;)Lcom/itau/empresas/ui/util/HintUtils;

    move-result-object v0

    const-string v1, "CHVREC-ID"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/HintUtils;->desativarHint(Ljava/lang/String;Z)V

    .line 204
    return-void

    .line 206
    :cond_25
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment$1;->this$0:Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;

    # getter for: Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->hintUtils:Lcom/itau/empresas/ui/util/HintUtils;
    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->access$100(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;)Lcom/itau/empresas/ui/util/HintUtils;

    move-result-object v0

    const-string v1, "CHVREC-QTD"

    add-int/lit8 v3, v3, 0x1

    .line 207
    invoke-virtual {v0, v1, v3}, Lcom/itau/empresas/ui/util/HintUtils;->registraAcesso(Ljava/lang/String;I)V

    .line 208
    return-void
.end method

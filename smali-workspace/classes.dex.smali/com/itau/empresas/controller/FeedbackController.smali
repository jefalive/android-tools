.class public Lcom/itau/empresas/controller/FeedbackController;
.super Lcom/itau/empresas/controller/BaseController;
.source "FeedbackController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/controller/FeedbackController$Joiner;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/itau/empresas/controller/BaseController<Lcom/itau/empresas/api/Api;>;"
    }
.end annotation


# static fields
.field private static CODIGO_APP:Ljava/lang/String;

.field private static GUID:Ljava/lang/String;


# instance fields
.field application:Lcom/itau/empresas/CustomApplication;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 21
    const-string v0, "34"

    sput-object v0, Lcom/itau/empresas/controller/FeedbackController;->CODIGO_APP:Ljava/lang/String;

    .line 22
    const-string v0, "fe0f8354-195d-42de-a883-f8ea0ff84a65"

    sput-object v0, Lcom/itau/empresas/controller/FeedbackController;->GUID:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .line 19
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .registers 1

    .line 19
    sget-object v0, Lcom/itau/empresas/controller/FeedbackController;->CODIGO_APP:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .registers 1

    .line 19
    sget-object v0, Lcom/itau/empresas/controller/FeedbackController;->GUID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/itau/empresas/controller/FeedbackController;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .param p0, "x0"    # Lcom/itau/empresas/controller/FeedbackController;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Ljava/lang/String;

    .line 19
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/itau/empresas/controller/FeedbackController;->getConteudoFeedback(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/itau/empresas/controller/FeedbackController;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/controller/FeedbackController;

    .line 19
    invoke-direct {p0}, Lcom/itau/empresas/controller/FeedbackController;->getDadosCliente()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private criarDeviceInfo(Ljava/lang/String;)Ljava/lang/String;
    .registers 14
    .param p1, "origem"    # Ljava/lang/String;

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/controller/FeedbackController;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.camera.autofocus"

    .line 64
    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    const-string v2, "T"

    goto :goto_13

    :cond_11
    const-string v2, "F"

    .line 66
    .local v2, "checkCamera":Ljava/lang/String;
    :goto_13
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 67
    .local v3, "modelo":Ljava/lang/String;
    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 69
    .local v4, "versaoSO":Ljava/lang/String;
    const-string v5, "%s"

    .line 70
    .local v5, "deviceId":Ljava/lang/String;
    const-string v6, "%s"

    .line 71
    .local v6, "imei":Ljava/lang/String;
    const-string v7, "%s"

    .line 73
    .local v7, "serial":Ljava/lang/String;
    const-string v8, "4.0.3"

    .line 74
    .local v8, "versaoApp":Ljava/lang/String;
    const-string v9, "A"

    .line 76
    .local v9, "plataforma":Ljava/lang/String;
    const/16 v0, 0x9

    new-array v10, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v3, v10, v0

    const/4 v0, 0x1

    aput-object v4, v10, v0

    const-string v0, "%s"

    const/4 v1, 0x2

    aput-object v0, v10, v1

    const-string v0, "%s"

    const/4 v1, 0x3

    aput-object v0, v10, v1

    const-string v0, "%s"

    const/4 v1, 0x4

    aput-object v0, v10, v1

    const-string v0, "4.0.3"

    const/4 v1, 0x5

    aput-object v0, v10, v1

    const-string v0, "A"

    const/4 v1, 0x6

    aput-object v0, v10, v1

    const/4 v0, 0x7

    aput-object v2, v10, v0

    const/16 v0, 0x8

    aput-object p1, v10, v0

    .line 88
    .local v10, "infos":[Ljava/lang/String;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_65
    array-length v0, v10

    if-ge v11, v0, :cond_73

    .line 89
    aget-object v0, v10, v11

    invoke-direct {p0, v0}, Lcom/itau/empresas/controller/FeedbackController;->escapaPipe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v10, v11

    .line 88
    add-int/lit8 v11, v11, 0x1

    goto :goto_65

    .line 92
    .end local v11    # "i":I
    :cond_73
    const/16 v0, 0x7c

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/FeedbackController$Joiner;->on(Ljava/lang/Character;)Lcom/itau/empresas/controller/FeedbackController$Joiner;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/itau/empresas/controller/FeedbackController$Joiner;->join([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 94
    .local v11, "result":Ljava/lang/String;
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x96

    if-le v0, v1, :cond_91

    .line 95
    const/4 v0, 0x0

    const/16 v1, 0x95

    invoke-virtual {v11, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 98
    :cond_91
    return-object v11
.end method

.method private escapaPipe(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .param p1, "s"    # Ljava/lang/String;

    .line 105
    if-nez p1, :cond_5

    .line 106
    const-string v0, ""

    return-object v0

    .line 108
    :cond_5
    const-string v0, "[|_]"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getConteudoFeedback(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 11
    .param p1, "origem"    # Ljava/lang/String;
    .param p2, "nota"    # I
    .param p3, "email"    # Ljava/lang/String;
    .param p4, "comentario"    # Ljava/lang/String;

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/controller/FeedbackController;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v4

    .line 47
    .local v4, "codigoOperador":Ljava/lang/String;
    const/4 v0, 0x3

    new-array v5, v0, [Ljava/lang/String;

    .line 48
    invoke-direct {p0, p1}, Lcom/itau/empresas/controller/FeedbackController;->criarDeviceInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, v5, v1

    .line 49
    invoke-direct {p0, p3}, Lcom/itau/empresas/controller/FeedbackController;->escapaPipe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, v5, v1

    const-string v0, "Op: %s - nota: %d - %s "

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v4, v1, v2

    .line 50
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 51
    invoke-direct {p0, p4}, Lcom/itau/empresas/controller/FeedbackController;->escapaPipe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    .line 50
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    aput-object v0, v5, v1

    .line 54
    .local v5, "s":[Ljava/lang/String;
    const/16 v0, 0x7c

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/FeedbackController$Joiner;->on(Ljava/lang/Character;)Lcom/itau/empresas/controller/FeedbackController$Joiner;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/itau/empresas/controller/FeedbackController$Joiner;->join([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getDadosCliente()Ljava/lang/String;
    .registers 4

    .line 58
    iget-object v0, p0, Lcom/itau/empresas/controller/FeedbackController;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v2

    .line 59
    .local v2, "c":Lcom/itau/empresas/api/model/ContaOperadorVO;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public enviaFeedback(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V
    .registers 13
    .param p1, "origem"    # Ljava/lang/String;
    .param p2, "idCategoria"    # I
    .param p3, "nota"    # I
    .param p4, "email"    # Ljava/lang/String;
    .param p5, "comentario"    # Ljava/lang/String;

    .line 29
    new-instance v0, Lcom/itau/empresas/controller/FeedbackController$1;

    move-object v1, p0

    move v2, p2

    move-object v3, p1

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/itau/empresas/controller/FeedbackController$1;-><init>(Lcom/itau/empresas/controller/FeedbackController;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 43
    return-void
.end method

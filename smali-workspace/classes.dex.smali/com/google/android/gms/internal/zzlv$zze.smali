.class final Lcom/google/android/gms/internal/zzlv$zze;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/zzlv;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "zze"
.end annotation


# instance fields
.field private mSize:I


# direct methods
.method private constructor <init>()V
    .registers 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/internal/zzlv$zze;->mSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/internal/zzlv$1;)V
    .registers 2

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzlv$zze;-><init>()V

    return-void
.end method


# virtual methods
.method public zza(JLjava/util/concurrent/TimeUnit;)Z
    .registers 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    move-object v6, p0

    monitor-enter v6

    :goto_c
    :try_start_c
    iget v0, p0, Lcom/google/android/gms/internal/zzlv$zze;->mSize:I
    :try_end_e
    .catchall {:try_start_c .. :try_end_e} :catchall_26

    if-nez v0, :cond_13

    monitor-exit v6

    const/4 v0, 0x1

    return v0

    :cond_13
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-gtz v0, :cond_1c

    monitor-exit v6

    const/4 v0, 0x0

    return v0

    :cond_1c
    :try_start_1c
    invoke-virtual {p0, v4, v5}, Ljava/lang/Object;->wait(J)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_22
    .catchall {:try_start_1c .. :try_end_22} :catchall_26

    move-result-wide v0

    sub-long/2addr v0, v2

    sub-long/2addr v4, v0

    goto :goto_c

    :catchall_26
    move-exception v7

    monitor-exit v6

    throw v7
.end method

.method public declared-synchronized zzoH()V
    .registers 4

    monitor-enter p0

    :try_start_1
    iget v0, p0, Lcom/google/android/gms/internal/zzlv$zze;->mSize:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzlv$zze;->mSize:I
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_9

    monitor-exit p0

    return-void

    :catchall_9
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized zzoI()V
    .registers 4

    monitor-enter p0

    :try_start_1
    iget v0, p0, Lcom/google/android/gms/internal/zzlv$zze;->mSize:I

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "too many decrements"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    iget v0, p0, Lcom/google/android/gms/internal/zzlv$zze;->mSize:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzlv$zze;->mSize:I

    iget v0, p0, Lcom/google/android/gms/internal/zzlv$zze;->mSize:I

    if-nez v0, :cond_1a

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_1a
    .catchall {:try_start_1 .. :try_end_1a} :catchall_1c

    :cond_1a
    monitor-exit p0

    return-void

    :catchall_1c
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.class public Lcom/itau/empresas/api/model/ContaContatoVO;
.super Ljava/lang/Object;
.source "ContaContatoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private agenciaContato:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia_contato"
    .end annotation
.end field

.field private canalCadastramento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "canal_cadastramento"
    .end annotation
.end field

.field private contaContato:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta_contato"
    .end annotation
.end field

.field private digitoVerificadorContato:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_verificador_contato"
    .end annotation
.end field

.field private idSistemaPagamentoBancario:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id_sistema_pagamento_bancario"
    .end annotation
.end field

.field private liberado:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "liberado"
    .end annotation
.end field

.field private moeda:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "moeda"
    .end annotation
.end field

.field private nomeBancoContato:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_banco_contato"
    .end annotation
.end field

.field private numeroBancoContato:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_banco_contato"
    .end annotation
.end field

.field private referenciaEmpresa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "referencia_empresa"
    .end annotation
.end field

.field private statusTokpag:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "status_tokpag"
    .end annotation
.end field

.field private tokpagFlad:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tokpag_flag"
    .end annotation
.end field

.field private valorLimite:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_limite"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

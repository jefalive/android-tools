.class public Lcom/itau/empresas/feature/extrato/adapter/ExtratoPagerAdapter;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;
.source "ExtratoPagerAdapter.java"


# instance fields
.field private fragmentList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Landroid/support/v4/app/Fragment;>;"
        }
    .end annotation
.end field

.field private titulosPagina:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentManager;)V
    .registers 3
    .param p1, "fm"    # Landroid/support/v4/app/FragmentManager;

    .line 15
    invoke-direct {p0, p1}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoPagerAdapter;->titulosPagina:Ljava/util/List;

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoPagerAdapter;->fragmentList:Ljava/util/List;

    .line 18
    return-void
.end method


# virtual methods
.method public adicionarFragmento(Ljava/lang/String;Landroid/support/v4/app/Fragment;)V
    .registers 4
    .param p1, "titulo"    # Ljava/lang/String;
    .param p2, "fragment"    # Landroid/support/v4/app/Fragment;

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoPagerAdapter;->titulosPagina:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoPagerAdapter;->fragmentList:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    return-void
.end method

.method public getCount()I
    .registers 2

    .line 27
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoPagerAdapter;->titulosPagina:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .registers 3
    .param p1, "position"    # I

    .line 22
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoPagerAdapter;->fragmentList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .registers 3
    .param p1, "position"    # I

    .line 32
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoPagerAdapter;->titulosPagina:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method

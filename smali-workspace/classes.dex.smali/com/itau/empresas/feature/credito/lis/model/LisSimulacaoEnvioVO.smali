.class public Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;
.super Ljava/lang/Object;
.source "LisSimulacaoEnvioVO.java"


# instance fields
.field private final agencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia"
    .end annotation
.end field

.field private final agenciaContratual:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia_contratual"
    .end annotation
.end field

.field private final codigoOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_operador"
    .end annotation
.end field

.field private final conta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta"
    .end annotation
.end field

.field private final contaContratual:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta_contratual"
    .end annotation
.end field

.field private final cpfRepresentante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf_representante"
    .end annotation
.end field

.field private final diaPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dia_pagamento"
    .end annotation
.end field

.field private final digitoConta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_conta"
    .end annotation
.end field

.field private final digitoContaContratual:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_conta_contratual"
    .end annotation
.end field

.field private final valorLimite:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_limite"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9
    .param p1, "agencia"    # Ljava/lang/String;
    .param p2, "conta"    # Ljava/lang/String;
    .param p3, "digitoConta"    # Ljava/lang/String;
    .param p4, "cpfRepresentante"    # Ljava/lang/String;
    .param p5, "valorLimite"    # Ljava/math/BigDecimal;
    .param p6, "codigoOperador"    # Ljava/lang/String;
    .param p7, "diaPagamento"    # Ljava/lang/String;

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;->agencia:Ljava/lang/String;

    .line 43
    iput-object p2, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;->conta:Ljava/lang/String;

    .line 44
    iput-object p3, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;->digitoConta:Ljava/lang/String;

    .line 45
    iput-object p4, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;->cpfRepresentante:Ljava/lang/String;

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;->agencia:Ljava/lang/String;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;->agenciaContratual:Ljava/lang/String;

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;->conta:Ljava/lang/String;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;->contaContratual:Ljava/lang/String;

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;->digitoConta:Ljava/lang/String;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;->digitoContaContratual:Ljava/lang/String;

    .line 49
    iput-object p5, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;->valorLimite:Ljava/math/BigDecimal;

    .line 50
    iput-object p6, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;->codigoOperador:Ljava/lang/String;

    .line 51
    iput-object p7, p0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;->diaPagamento:Ljava/lang/String;

    .line 52
    return-void
.end method

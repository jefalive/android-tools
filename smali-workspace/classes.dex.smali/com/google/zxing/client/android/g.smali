.class final Lcom/google/zxing/client/android/g;
.super Ljava/lang/Thread;


# instance fields
.field private final a:Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;

.field private final b:Ljava/util/Map;

.field private c:Landroid/os/Handler;

.field private final d:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method constructor <init>(Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;Ljava/util/Collection;Ljava/util/Map;Ljava/lang/String;Lcom/google/zxing/n;)V
    .registers 9

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object p1, p0, Lcom/google/zxing/client/android/g;->a:Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/zxing/client/android/g;->d:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/google/zxing/e;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/google/zxing/client/android/g;->b:Ljava/util/Map;

    if-eqz p3, :cond_1d

    iget-object v0, p0, Lcom/google/zxing/client/android/g;->b:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    :cond_1d
    if-eqz p2, :cond_25

    invoke-interface {p2}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2b

    :cond_25
    sget-object v0, Lcom/google/zxing/a;->a:Lcom/google/zxing/a;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p2

    :cond_2b
    iget-object v0, p0, Lcom/google/zxing/client/android/g;->b:Ljava/util/Map;

    sget-object v1, Lcom/google/zxing/e;->c:Lcom/google/zxing/e;

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p4, :cond_3b

    iget-object v0, p0, Lcom/google/zxing/client/android/g;->b:Ljava/util/Map;

    sget-object v1, Lcom/google/zxing/e;->e:Lcom/google/zxing/e;

    invoke-interface {v0, v1, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3b
    iget-object v0, p0, Lcom/google/zxing/client/android/g;->b:Ljava/util/Map;

    sget-object v1, Lcom/google/zxing/e;->j:Lcom/google/zxing/e;

    invoke-interface {v0, v1, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "DecodeThread"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Hints: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/zxing/client/android/g;->b:Ljava/util/Map;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method a()Landroid/os/Handler;
    .registers 3

    :try_start_0
    iget-object v0, p0, Lcom/google/zxing/client/android/g;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_5} :catch_6

    goto :goto_7

    :catch_6
    move-exception v1

    :goto_7
    iget-object v0, p0, Lcom/google/zxing/client/android/g;->c:Landroid/os/Handler;

    return-object v0
.end method

.method public run()V
    .registers 4

    invoke-static {}, Landroid/os/Looper;->prepare()V

    new-instance v0, Lcom/google/zxing/client/android/e;

    iget-object v1, p0, Lcom/google/zxing/client/android/g;->a:Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;

    iget-object v2, p0, Lcom/google/zxing/client/android/g;->b:Ljava/util/Map;

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/client/android/e;-><init>(Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;Ljava/util/Map;)V

    iput-object v0, p0, Lcom/google/zxing/client/android/g;->c:Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/zxing/client/android/g;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void
.end method

.class public final Lbr/com/itau/sdk/android/core/FlowManager;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/FlowManager$a;
    }
.end annotation


# static fields
.field private static final g:[B

.field private static h:I


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Lbr/com/itau/sdk/android/core/model/MiddlewareKeyValue;>;"
        }
    .end annotation
.end field

.field private c:Landroid/support/v4/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field private final e:Ljava/lang/Object;

.field private final f:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x37

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/FlowManager;->g:[B

    const/16 v0, 0xe

    sput v0, Lbr/com/itau/sdk/android/core/FlowManager;->h:I

    return-void

    :array_e
    .array-data 1
        0x3t
        -0x5ct
        -0x6at
        0x24t
        0x2bt
        -0x2t
        -0x5t
        0x1t
        -0x54t
        0x55t
        -0x11t
        -0x1t
        0x1t
        -0x9t
        -0x47t
        0x41t
        -0x4t
        0x9t
        -0x2t
        -0x5t
        0x3t
        -0x9t
        -0x49t
        0x53t
        -0x7t
        -0xet
        -0x5t
        0x11t
        -0x11t
        -0x47t
        0x4at
        -0x5t
        0x2t
        -0x6t
        0x9t
        -0x4t
        0x5t
        -0x27t
        0x19t
        -0x1ft
        0xdt
        0x6t
        0x8t
        -0x10t
        -0x1bt
        0x17t
        0x0t
        -0x8t
        0x0t
        0x14t
        -0x2ft
        0x1bt
        0x2t
        -0x6t
        0x9t
    .end array-data
.end method

.method constructor <init>()V
    .registers 3

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->a:Ljava/util/Map;

    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->b:Ljava/util/Map;

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->c:Landroid/support/v4/util/Pair;

    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->d:Ljava/util/Map;

    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->e:Ljava/lang/Object;

    .line 36
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 40
    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    const/4 v4, 0x0

    rsub-int/lit8 p0, p0, 0x20

    rsub-int/lit8 p1, p1, 0x37

    rsub-int/lit8 p2, p2, 0x74

    new-instance v0, Ljava/lang/String;

    sget-object v5, Lbr/com/itau/sdk/android/core/FlowManager;->g:[B

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v5, :cond_18

    move v2, p0

    move v3, p2

    :goto_13
    add-int/lit8 p1, p1, 0x1

    add-int/2addr v2, v3

    add-int/lit8 p2, v2, 0x2

    :cond_18
    int-to-byte v2, p2

    aput-byte v2, v1, v4

    move v2, v4

    add-int/lit8 v4, v4, 0x1

    if-ne v2, p0, :cond_25

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_25
    move v2, p2

    aget-byte v3, v5, p1

    goto :goto_13
.end method

.method private d()V
    .registers 4

    .line 158
    iget-object v1, p0, Lbr/com/itau/sdk/android/core/FlowManager;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 159
    :try_start_3
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_1d

    move-result v0

    if-eqz v0, :cond_d

    .line 160
    monitor-exit v1

    return-void

    .line 162
    :cond_d
    :try_start_d
    const-class v0, Lbr/com/itau/sdk/android/core/FlowManager$a;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->api(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/FlowManager$a;

    invoke-interface {v0}, Lbr/com/itau/sdk/android/core/FlowManager$a;->a()Lbr/com/itau/sdk/android/core/model/LegacyMenuDTO;

    .line 169
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/FlowManager;->e()V
    :try_end_1b
    .catchall {:try_start_d .. :try_end_1b} :catchall_1d

    .line 170
    monitor-exit v1

    goto :goto_20

    :catchall_1d
    move-exception v2

    monitor-exit v1

    throw v2

    .line 171
    :goto_20
    return-void
.end method

.method private e()V
    .registers 6

    .line 175
    :try_start_0
    const-class v0, Lbr/com/itau/sdk/android/core/c/d;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->api(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/c/d;

    invoke-interface {v0}, Lbr/com/itau/sdk/android/core/c/d;->b()Ljava/lang/Void;

    .line 176
    sget v0, Lbr/com/itau/sdk/android/core/FlowManager;->h:I

    add-int/lit8 v0, v0, -0x3

    sget-object v1, Lbr/com/itau/sdk/android/core/FlowManager;->g:[B

    const/16 v2, 0x31

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/FlowManager;->g:[B

    const/16 v3, 0x2e

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/FlowManager;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_22
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_22} :catch_23

    .line 179
    goto :goto_3a

    .line 177
    :catch_23
    move-exception v4

    .line 178
    sget-object v0, Lbr/com/itau/sdk/android/core/FlowManager;->g:[B

    const/16 v1, 0x2e

    aget-byte v0, v0, v1

    or-int/lit8 v1, v0, 0x33

    sget-object v2, Lbr/com/itau/sdk/android/core/FlowManager;->g:[B

    const/16 v3, 0x32

    aget-byte v2, v2, v3

    neg-int v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/FlowManager;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v4, v0}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 180
    :goto_3a
    return-void
.end method


# virtual methods
.method public a()Landroid/support/v4/util/Pair;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
        }
    .end annotation

    .line 112
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_b

    .line 113
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/FlowManager;->d()V

    .line 115
    :cond_b
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->c:Landroid/support/v4/util/Pair;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/model/MiddlewareKeyValue;
    .registers 3

    .line 97
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_b

    .line 98
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/FlowManager;->d()V

    .line 100
    :cond_b
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/model/MiddlewareKeyValue;

    return-object v0
.end method

.method public a(Lbr/com/itau/sdk/android/core/model/LegacyMenuDTO;)V
    .registers 7

    .line 77
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/LegacyMenuDTO;->getLinks()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_9

    .line 78
    :cond_8
    return-void

    .line 80
    :cond_9
    new-instance v0, Landroid/support/v4/util/Pair;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/LegacyMenuDTO;->getIdu()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/LegacyMenuDTO;->getOpu()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/support/v4/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->c:Landroid/support/v4/util/Pair;

    .line 81
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/LegacyMenuDTO;->getLinks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_20
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lbr/com/itau/sdk/android/core/model/MiddlewareKeyValue;

    .line 82
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/LegacyMenuDTO;->getIdu()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lbr/com/itau/sdk/android/core/model/MiddlewareKeyValue;->setId(Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->b:Ljava/util/Map;

    invoke-virtual {v4}, Lbr/com/itau/sdk/android/core/model/MiddlewareKeyValue;->getIdServ()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    goto :goto_20

    .line 85
    :cond_3e
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    .line 86
    return-void
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .registers 3

    .line 123
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public c(Ljava/lang/String;)Ljava/lang/String;
    .registers 3

    .line 134
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public c()V
    .registers 3

    .line 183
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 184
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 185
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->c:Landroid/support/v4/util/Pair;

    .line 186
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 187
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 188
    return-void
.end method

.method public d(Ljava/lang/String;)Z
    .registers 3

    .line 144
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    goto :goto_13

    :cond_12
    const/4 v0, 0x0

    :goto_13
    return v0
.end method

.method public feedFlows(Ljava/util/List;)V
    .registers 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/sdk/android/core/model/FlowDTO;>;)V"
        }
    .end annotation

    .line 51
    if-eqz p1, :cond_8

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 52
    :cond_8
    return-void

    .line 54
    :cond_9
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_d
    :goto_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_50

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lbr/com/itau/sdk/android/core/model/FlowDTO;

    .line 56
    invoke-virtual {v4}, Lbr/com/itau/sdk/android/core/model/FlowDTO;->getTarget()Ljava/lang/String;

    move-result-object v5

    .line 57
    invoke-virtual {v4}, Lbr/com/itau/sdk/android/core/model/FlowDTO;->getOp()Ljava/lang/String;

    move-result-object v6

    .line 59
    if-eqz v5, :cond_d

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 60
    goto :goto_d

    .line 62
    :cond_2b
    sget-object v0, Lbr/com/itau/sdk/android/core/FlowManager;->g:[B

    const/16 v1, 0x27

    aget-byte v0, v0, v1

    neg-int v0, v0

    sget-object v1, Lbr/com/itau/sdk/android/core/FlowManager;->g:[B

    const/16 v2, 0x2e

    aget-byte v1, v1, v2

    or-int/lit8 v2, v1, 0x15

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/FlowManager;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 63
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->d:Ljava/util/Map;

    invoke-interface {v0, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4f

    .line 65
    :cond_4a
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/FlowManager;->a:Ljava/util/Map;

    invoke-interface {v0, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    :goto_4f
    goto :goto_d

    .line 67
    :cond_50
    return-void
.end method

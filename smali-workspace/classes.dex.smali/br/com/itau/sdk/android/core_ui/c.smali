.class final synthetic Lbr/com/itau/sdk/android/core_ui/c;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field private final a:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method private constructor <init>(Ljava/util/concurrent/CountDownLatch;)V
    .registers 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbr/com/itau/sdk/android/core_ui/c;->a:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method public static a(Ljava/util/concurrent/CountDownLatch;)Landroid/content/DialogInterface$OnDismissListener;
    .registers 2

    new-instance v0, Lbr/com/itau/sdk/android/core_ui/c;

    invoke-direct {v0, p0}, Lbr/com/itau/sdk/android/core_ui/c;-><init>(Ljava/util/concurrent/CountDownLatch;)V

    return-object v0
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .registers 3
    .annotation runtime Ljava/lang/invoke/LambdaForm$Hidden;
    .end annotation

    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/c;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-static {v0, p1}, Lbr/com/itau/sdk/android/core_ui/SDKUiController;->a(Ljava/util/concurrent/CountDownLatch;Landroid/content/DialogInterface;)V

    return-void
.end method

.class public Lcom/itau/empresas/feature/itoken/fragment/ITokenNovaInstalacaoFragment;
.super Lcom/itau/empresas/ui/fragment/BaseFragment;
.source "ITokenNovaInstalacaoFragment.java"


# instance fields
.field controller:Lcom/itau/empresas/controller/ITokenController;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 22
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;-><init>()V

    return-void
.end method


# virtual methods
.method aoClicarEmOrientacoesSeguranca()V
    .registers 6

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenNovaInstalacaoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 53
    const v2, 0x7f0702df

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/itoken/fragment/ITokenNovaInstalacaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 54
    const v3, 0x7f0702a0

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/itoken/fragment/ITokenNovaInstalacaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 55
    const v4, 0x7f07031c

    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/itoken/fragment/ITokenNovaInstalacaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 57
    invoke-static {p0}, Lcom/itau/empresas/feature/itoken/ITokenOrientacoesSegurancaActivity;->intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/feature/itoken/ITokenOrientacoesSegurancaActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/itoken/ITokenOrientacoesSegurancaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 58
    return-void
.end method

.method aoClicarNoBotaoInstalar()V
    .registers 7

    .line 35
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenNovaInstalacaoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 36
    const v2, 0x7f0702df

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/itoken/fragment/ITokenNovaInstalacaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 37
    const v3, 0x7f0702a0

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/itoken/fragment/ITokenNovaInstalacaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 38
    const v4, 0x7f07030b

    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/itoken/fragment/ITokenNovaInstalacaoFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 41
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenNovaInstalacaoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    .line 43
    .local v5, "activity":Landroid/support/v4/app/FragmentActivity;
    instance-of v0, v5, Lcom/itau/empresas/ui/activity/BaseActivity;

    if-eqz v0, :cond_31

    .line 44
    move-object v0, v5

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->mostraDialogoDeProgresso()V

    .line 45
    :cond_31
    invoke-static {v5}, Lcom/itau/empresas/ui/util/SnackbarUtils;->tentaDispensarSnackbarNo(Ljava/lang/Object;)V

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenNovaInstalacaoFragment;->controller:Lcom/itau/empresas/controller/ITokenController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/ITokenController;->instalarTokenApp()V

    .line 48
    return-void
.end method

.method public aposCarregar()V
    .registers 2

    .line 30
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenNovaInstalacaoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 31
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 62
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

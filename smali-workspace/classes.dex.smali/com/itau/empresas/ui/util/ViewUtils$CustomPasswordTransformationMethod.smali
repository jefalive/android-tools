.class public Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod;
.super Landroid/text/method/PasswordTransformationMethod;
.source "ViewUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/util/ViewUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CustomPasswordTransformationMethod"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod$PassCharSequence;
    }
.end annotation


# instance fields
.field private DOT:C


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 427
    invoke-direct {p0}, Landroid/text/method/PasswordTransformationMethod;-><init>()V

    .line 429
    const/16 v0, 0x2022

    iput-char v0, p0, Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod;->DOT:C

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod;)C
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod;

    .line 427
    iget-char v0, p0, Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod;->DOT:C

    return v0
.end method


# virtual methods
.method public getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;
    .registers 4
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "view"    # Landroid/view/View;

    .line 433
    new-instance v0, Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod$PassCharSequence;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod$PassCharSequence;-><init>(Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod;Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public onFocusChanged(Landroid/view/View;Ljava/lang/CharSequence;ZILandroid/graphics/Rect;)V
    .registers 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "sourceText"    # Ljava/lang/CharSequence;
    .param p3, "focused"    # Z
    .param p4, "direction"    # I
    .param p5, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .line 440
    return-void
.end method

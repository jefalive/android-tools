.class public Lorg/joda/money/CurrencyMismatchException;
.super Ljava/lang/IllegalArgumentException;
.source "CurrencyMismatchException.java"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final firstCurrency:Lorg/joda/money/CurrencyUnit;

.field private final secondCurrency:Lorg/joda/money/CurrencyUnit;


# direct methods
.method public constructor <init>(Lorg/joda/money/CurrencyUnit;Lorg/joda/money/CurrencyUnit;)V
    .registers 5
    .param p1, "firstCurrency"    # Lorg/joda/money/CurrencyUnit;
    .param p2, "secondCurrency"    # Lorg/joda/money/CurrencyUnit;

    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Currencies differ: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    if-eqz p1, :cond_12

    invoke-virtual {p1}, Lorg/joda/money/CurrencyUnit;->getCode()Ljava/lang/String;

    move-result-object v1

    goto :goto_14

    :cond_12
    const-string v1, "null"

    :goto_14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    if-eqz p2, :cond_25

    invoke-virtual {p2}, Lorg/joda/money/CurrencyUnit;->getCode()Ljava/lang/String;

    move-result-object v1

    goto :goto_27

    :cond_25
    const-string v1, "null"

    :goto_27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 46
    iput-object p1, p0, Lorg/joda/money/CurrencyMismatchException;->firstCurrency:Lorg/joda/money/CurrencyUnit;

    .line 47
    iput-object p2, p0, Lorg/joda/money/CurrencyMismatchException;->secondCurrency:Lorg/joda/money/CurrencyUnit;

    .line 48
    return-void
.end method


# virtual methods
.method public getFirstCurrency()Lorg/joda/money/CurrencyUnit;
    .registers 2

    .line 57
    iget-object v0, p0, Lorg/joda/money/CurrencyMismatchException;->firstCurrency:Lorg/joda/money/CurrencyUnit;

    return-object v0
.end method

.method public getSecondCurrency()Lorg/joda/money/CurrencyUnit;
    .registers 2

    .line 66
    iget-object v0, p0, Lorg/joda/money/CurrencyMismatchException;->secondCurrency:Lorg/joda/money/CurrencyUnit;

    return-object v0
.end method

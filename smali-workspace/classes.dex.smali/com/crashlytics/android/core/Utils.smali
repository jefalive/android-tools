.class final Lcom/crashlytics/android/core/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# direct methods
.method private constructor <init>()V
    .registers 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static capFileCount(Ljava/io/File;Ljava/io/FilenameFilter;ILjava/util/Comparator;)V
    .registers 11
    .param p0, "directory"    # Ljava/io/File;
    .param p1, "filter"    # Ljava/io/FilenameFilter;
    .param p2, "maxAllowed"    # I
    .param p3, "fileComparator"    # Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/io/File;Ljava/io/FilenameFilter;ILjava/util/Comparator<Ljava/io/File;>;)V"
        }
    .end annotation

    .line 20
    invoke-virtual {p0, p1}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    .line 21
    .local v1, "sessionFiles":[Ljava/io/File;
    if-eqz v1, :cond_1f

    array-length v0, v1

    if-le v0, p2, :cond_1f

    .line 23
    invoke-static {v1, p3}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 24
    array-length v2, v1

    .line 25
    .local v2, "i":I
    move-object v3, v1

    .local v3, "arr$":[Ljava/io/File;
    array-length v4, v3

    .local v4, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_10
    if-ge v5, v4, :cond_1f

    aget-object v6, v3, v5

    .line 27
    .local v6, "file":Ljava/io/File;
    if-gt v2, p2, :cond_17

    .line 28
    return-void

    .line 30
    :cond_17
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 31
    add-int/lit8 v2, v2, -0x1

    .line 25
    .end local v6    # "file":Ljava/io/File;
    add-int/lit8 v5, v5, 0x1

    goto :goto_10

    .line 34
    .end local v2    # "i":I
    .end local v3    # "arr$":[Ljava/io/File;
    .end local v4    # "len$":I
    .end local v5    # "i$":I
    :cond_1f
    return-void
.end method

.class public Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;
.super Ljava/lang/Object;
.source "ContatoRecargaApelidoInputVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field apelido:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "apelido"
    .end annotation
.end field

.field estadoOperadora:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "estado_operadora"
    .end annotation
.end field

.field nomeOperadora:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_operadora"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setApelido(Ljava/lang/String;)V
    .registers 2
    .param p1, "apelido"    # Ljava/lang/String;

    .line 21
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;->apelido:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public setEstadoOperadora(Ljava/lang/String;)V
    .registers 2
    .param p1, "estadoOperadora"    # Ljava/lang/String;

    .line 25
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;->estadoOperadora:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public setNomeOperadora(Ljava/lang/String;)V
    .registers 2
    .param p1, "nomeOperadora"    # Ljava/lang/String;

    .line 33
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaApelidoInputVO;->nomeOperadora:Ljava/lang/String;

    .line 34
    return-void
.end method

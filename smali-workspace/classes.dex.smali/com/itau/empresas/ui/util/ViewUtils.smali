.class public final Lcom/itau/empresas/ui/util/ViewUtils;
.super Ljava/lang/Object;
.source "ViewUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod;
    }
.end annotation


# static fields
.field public static final CURRENCY_UNIT:Lorg/joda/money/CurrencyUnit;

.field public static final LOCALE_PT:Ljava/util/Locale;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .line 55
    const-string v0, "BRL"

    invoke-static {v0}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/ui/util/ViewUtils;->CURRENCY_UNIT:Lorg/joda/money/CurrencyUnit;

    .line 56
    new-instance v0, Ljava/util/Locale;

    const-string v1, "pt"

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/itau/empresas/ui/util/ViewUtils;->LOCALE_PT:Ljava/util/Locale;

    return-void
.end method

.method public static abrirFileExplorerIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .registers 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "mimeType"    # Ljava/lang/String;

    .line 200
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 202
    .local v2, "pm":Landroid/content/pm/PackageManager;
    new-instance v3, Landroid/content/Intent;

    const-string v0, "android.intent.action.OPEN_DOCUMENT"

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 203
    .local v3, "intentPDF":Landroid/content/Intent;
    invoke-virtual {v3, p1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 204
    const-string v0, "android.intent.category.OPENABLE"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 207
    new-instance v4, Landroid/content/Intent;

    const-string v0, "com.sec.android.app.myfiles.PICK_DATA"

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 208
    .local v4, "intentSamsung":Landroid/content/Intent;
    const-string v0, "CONTENT_TYPE"

    invoke-virtual {v4, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 209
    const-string v0, "android.intent.category.DEFAULT"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 211
    new-instance v5, Landroid/content/Intent;

    const-string v0, "android.intent.action.GET_CONTENT"

    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 212
    .local v5, "intentGeral":Landroid/content/Intent;
    invoke-virtual {v5, p1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 213
    const-string v0, "android.intent.category.OPENABLE"

    invoke-virtual {v5, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 215
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    aput-object v3, v0, v1

    const/4 v1, 0x1

    aput-object v4, v0, v1

    const/4 v1, 0x2

    aput-object v5, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    .line 217
    .local v6, "intents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_47
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_62

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/content/Intent;

    .line 218
    .local v8, "i":Landroid/content/Intent;
    const/high16 v0, 0x10000

    invoke-virtual {v2, v8, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_61

    .line 219
    return-object v8

    .line 221
    .end local v8    # "i":Landroid/content/Intent;
    :cond_61
    goto :goto_47

    .line 223
    :cond_62
    const/4 v0, 0x0

    return-object v0
.end method

.method public static ajustarNumeroAgenda(Lcom/itau/empresas/CustomApplication;Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .param p0, "app"    # Lcom/itau/empresas/CustomApplication;
    .param p1, "numero"    # Ljava/lang/String;

    .line 232
    invoke-virtual {p0}, Lcom/itau/empresas/CustomApplication;->getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v2

    .line 234
    .local v2, "dadosVO":Lcom/itau/empresas/api/model/DadosOperadorVO;
    const-string v0, "[^0-9]"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 236
    .local v3, "resultado":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xb

    if-le v0, v1, :cond_1f

    .line 237
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0xb

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_40

    .line 238
    :cond_1f
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xa

    if-ge v0, v1, :cond_40

    if-eqz v2, :cond_40

    .line 239
    invoke-virtual {v2}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getDDD()Ljava/lang/String;

    move-result-object v4

    .line 240
    .local v4, "ddd":Ljava/lang/String;
    if-eqz v4, :cond_40

    .line 241
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 245
    .end local v4    # "ddd":Ljava/lang/String;
    :cond_40
    :goto_40
    return-object v3
.end method

.method public static aoClicarBotaoSairMenu(Lcom/itau/empresas/CustomApplication;Lcom/itau/empresas/ui/activity/BaseActivity;)V
    .registers 7
    .param p0, "application"    # Lcom/itau/empresas/CustomApplication;
    .param p1, "activity"    # Lcom/itau/empresas/ui/activity/BaseActivity;

    .line 336
    invoke-virtual {p0}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "feedback"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 338
    invoke-virtual {p0}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CM-HL"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 340
    .local v3, "qtdAcesso":I
    const/4 v0, 0x5

    if-ne v3, v0, :cond_38

    .line 341
    invoke-virtual {p0}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 342
    .local v4, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v0, "feedback"

    const/4 v1, 0x0

    invoke-interface {v4, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 343
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 344
    invoke-static {}, Lcom/itau/empresas/feature/feedback/FeedbackDialog_;->builder()Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog_$FragmentBuilder_;->build()Lcom/itau/empresas/feature/feedback/FeedbackDialog;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 345
    .end local v4    # "ed":Landroid/content/SharedPreferences$Editor;
    goto :goto_4c

    .line 346
    :cond_38
    invoke-static {}, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog_;->builder()Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog_$FragmentBuilder_;

    move-result-object v0

    const-string v1, "isPoynt"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog_$FragmentBuilder_;->arg(Ljava/lang/String;Z)Lorg/androidannotations/api/builder/FragmentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog_$FragmentBuilder_;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog_$FragmentBuilder_;->build()Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 348
    .end local v3    # "qtdAcesso":I
    :goto_4c
    goto :goto_61

    .line 349
    :cond_4d
    invoke-static {}, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog_;->builder()Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog_$FragmentBuilder_;

    move-result-object v0

    const-string v1, "isPoynt"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog_$FragmentBuilder_;->arg(Ljava/lang/String;Z)Lorg/androidannotations/api/builder/FragmentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog_$FragmentBuilder_;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog_$FragmentBuilder_;->build()Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/itau/empresas/ui/dialog/ConfirmarSairDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 351
    :goto_61
    return-void
.end method

.method public static calculateHeight(Landroid/widget/ListView;)I
    .registers 7
    .param p0, "list"    # Landroid/widget/ListView;

    .line 114
    const/4 v3, 0x0

    .line 116
    .local v3, "height":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    invoke-virtual {p0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    if-ge v4, v0, :cond_28

    .line 117
    invoke-virtual {p0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v4, v1, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 118
    .local v5, "childView":Landroid/view/View;
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 119
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 118
    invoke-virtual {v5, v0, v1}, Landroid/view/View;->measure(II)V

    .line 120
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v3, v0

    .line 116
    .end local v5    # "childView":Landroid/view/View;
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 124
    .end local v4    # "i":I
    :cond_28
    invoke-virtual {p0}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/widget/ListView;->getCount()I

    move-result v1

    mul-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 126
    return v3
.end method

.method public static varargs destacaStringAlteraCor(IZLjava/lang/String;[Ljava/lang/String;)Landroid/text/Spannable;
    .registers 13
    .param p0, "cor"    # I
    .param p1, "bold"    # Z
    .param p2, "texto"    # Ljava/lang/String;
    .param p3, "textoDestacado"    # [Ljava/lang/String;

    .line 297
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 299
    .local v2, "spannable":Landroid/text/Spannable;
    move-object v3, p3

    array-length v4, v3

    const/4 v5, 0x0

    :goto_8
    if-ge v5, v4, :cond_30

    aget-object v6, v3, v5

    .line 300
    .local v6, "item":Ljava/lang/String;
    invoke-virtual {p2, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 301
    .local v7, "start":I
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    add-int v8, v7, v0

    .line 302
    .local v8, "end":I
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v0, p0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v1, 0x21

    invoke-interface {v2, v0, v7, v8, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 305
    if-eqz p1, :cond_2d

    .line 307
    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v1, 0x21

    invoke-interface {v2, v0, v7, v8, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 299
    .end local v6    # "item":Ljava/lang/String;
    .end local v7    # "start":I
    .end local v8    # "end":I
    :cond_2d
    add-int/lit8 v5, v5, 0x1

    goto :goto_8

    .line 312
    :cond_30
    return-object v2
.end method

.method public static escondeView(Landroid/view/View;I)V
    .registers 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "viewID"    # I

    .line 412
    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 414
    .local v1, "viewFound":Landroid/view/View;
    if-eqz v1, :cond_b

    .line 415
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 417
    :cond_b
    return-void
.end method

.method public static formataAdicionandoZeros(ILjava/lang/String;)Ljava/lang/String;
    .registers 6
    .param p0, "tamanhoDaStringAposFormatacao"    # I
    .param p1, "entrada"    # Ljava/lang/String;

    .line 384
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 385
    const-string v0, ""

    return-object v0

    .line 388
    :cond_9
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-eq v0, p0, :cond_15

    .line 389
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, p0, :cond_16

    .line 390
    :cond_15
    return-object p1

    .line 393
    :cond_16
    const-string v2, ""

    .line 395
    .local v2, "padding":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_19
    if-ge v3, p0, :cond_31

    .line 396
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 395
    add-int/lit8 v3, v3, 0x1

    goto :goto_19

    .line 399
    .end local v3    # "i":I
    :cond_31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 401
    .local v3, "ret":Ljava/lang/String;
    return-object v3
.end method

.method public static formataNumeroTelefonico(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "ddd"    # Ljava/lang/String;
    .param p2, "numero"    # Ljava/lang/String;

    .line 84
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x8

    if-lt v0, v1, :cond_34

    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x4

    const/4 v2, 0x0

    invoke-virtual {p2, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 86
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x4

    invoke-virtual {p2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 88
    :cond_34
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const v1, 0x7f0704e3

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getContaCompleta(Lcom/itau/empresas/CustomApplication;)Ljava/lang/String;
    .registers 4
    .param p0, "application"    # Lcom/itau/empresas/CustomApplication;

    .line 65
    invoke-virtual {p0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v2

    .line 66
    .local v2, "conta":Lcom/itau/empresas/api/model/ContaOperadorVO;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 67
    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 68
    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 66
    return-object v0
.end method

.method public static getDpisFromPixel(Landroid/content/res/Resources;F)I
    .registers 5
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "pixels"    # F

    .line 227
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v2, v0, Landroid/util/DisplayMetrics;->density:F

    .line 228
    .local v2, "scale":F
    mul-float v0, p1, v2

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static varargs gone([Landroid/view/View;)V
    .registers 6
    .param p0, "views"    # [Landroid/view/View;

    .line 249
    move-object v1, p0

    array-length v2, v1

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v2, :cond_11

    aget-object v4, v1, v3

    .line 250
    .local v4, "view":Landroid/view/View;
    if-eqz v4, :cond_e

    .line 251
    const/16 v0, 0x8

    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 249
    .end local v4    # "view":Landroid/view/View;
    :cond_e
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 254
    :cond_11
    return-void
.end method

.method public static inicializarSpinnerOrdenado(Landroid/content/Context;Landroid/widget/Spinner;Ljava/util/List;)V
    .registers 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "spinner"    # Landroid/widget/Spinner;
    .param p2, "array"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::Ljava/lang/Comparable<TT;>;>(Landroid/content/Context;Landroid/widget/Spinner;Ljava/util/List<TT;>;)V"
        }
    .end annotation

    .line 160
    invoke-static {p2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 161
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v0, 0x7f030146

    invoke-direct {v1, p0, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 163
    .local v1, "adapter":Landroid/widget/ArrayAdapter;
    const v0, 0x7f030145

    invoke-virtual {v1, v0}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 165
    invoke-virtual {p1, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 166
    return-void
.end method

.method public static redirecionaLogin(Landroid/app/Activity;)V
    .registers 3
    .param p0, "context"    # Landroid/app/Activity;

    .line 421
    invoke-static {p0}, Lcom/itau/empresas/feature/login/PreLoginActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;

    move-result-object v0

    .line 422
    const v1, 0x10008000

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;->flags(I)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;

    .line 423
    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 424
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 425
    return-void
.end method

.method public static removaPaddingsZero(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .param p0, "s"    # Ljava/lang/String;

    .line 271
    if-nez p0, :cond_4

    .line 272
    const/4 v0, 0x0

    return-object v0

    .line 275
    :cond_4
    const/4 v2, 0x0

    .line 276
    .local v2, "index":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_6
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v3, v0, :cond_19

    .line 277
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x30

    if-eq v0, v1, :cond_16

    .line 278
    move v2, v3

    .line 279
    goto :goto_19

    .line 276
    :cond_16
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 283
    .end local v3    # "i":I
    :cond_19
    :goto_19
    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static sairDoApp(Landroid/content/Context;)V
    .registers 3
    .param p0, "context"    # Landroid/content/Context;

    .line 323
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.MAIN"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 324
    .local v1, "startMain":Landroid/content/Intent;
    const-string v0, "android.intent.category.HOME"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 325
    const v0, 0x10008000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 326
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 327
    return-void
.end method

.method public static temConta(Lcom/itau/empresas/CustomApplication;)Z
    .registers 2
    .param p0, "application"    # Lcom/itau/empresas/CustomApplication;

    .line 316
    invoke-virtual {p0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    goto :goto_9

    :cond_8
    const/4 v0, 0x0

    :goto_9
    return v0
.end method

.method public static validaNumero(Ljava/lang/String;)Z
    .registers 7
    .param p0, "number"    # Ljava/lang/String;

    .line 72
    const/4 v1, 0x0

    .line 73
    .local v1, "isValid":Z
    const-string v2, "^[-+]?[0-9]*\\.?[0-9]+$"

    .line 74
    .local v2, "expression":Ljava/lang/String;
    move-object v3, p0

    .line 75
    .local v3, "inputStr":Ljava/lang/CharSequence;
    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    .line 76
    .local v4, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v4, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    .line 77
    .local v5, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 78
    const/4 v1, 0x1

    .line 80
    :cond_13
    return v1
.end method

.method public static varargs visible([Landroid/view/View;)V
    .registers 6
    .param p0, "views"    # [Landroid/view/View;

    .line 257
    move-object v1, p0

    array-length v2, v1

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v2, :cond_10

    aget-object v4, v1, v3

    .line 258
    .local v4, "view":Landroid/view/View;
    if-eqz v4, :cond_d

    .line 259
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 257
    .end local v4    # "view":Landroid/view/View;
    :cond_d
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 262
    :cond_10
    return-void
.end method

.class public final Lorg/threeten/bp/Duration;
.super Ljava/lang/Object;
.source "Duration.java"

# interfaces
.implements Lorg/threeten/bp/temporal/TemporalAmount;
.implements Ljava/lang/Comparable;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Lorg/threeten/bp/temporal/TemporalAmount;Ljava/lang/Comparable<Lorg/threeten/bp/Duration;>;Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final BI_NANOS_PER_SECOND:Ljava/math/BigInteger;

.field private static final PATTERN:Ljava/util/regex/Pattern;

.field public static final ZERO:Lorg/threeten/bp/Duration;

.field private static final serialVersionUID:J = 0x2aba9d02d1c4f832L


# instance fields
.field private final nanos:I

.field private final seconds:J


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 99
    new-instance v0, Lorg/threeten/bp/Duration;

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lorg/threeten/bp/Duration;-><init>(JI)V

    sput-object v0, Lorg/threeten/bp/Duration;->ZERO:Lorg/threeten/bp/Duration;

    .line 115
    const-wide/32 v0, 0x3b9aca00

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, Lorg/threeten/bp/Duration;->BI_NANOS_PER_SECOND:Ljava/math/BigInteger;

    .line 119
    const-string v0, "([-+]?)P(?:([-+]?[0-9]+)D)?(T(?:([-+]?[0-9]+)H)?(?:([-+]?[0-9]+)M)?(?:([-+]?[0-9]+)(?:[.,]([0-9]{0,9}))?S)?)?"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/threeten/bp/Duration;->PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method private constructor <init>(JI)V
    .registers 4
    .param p1, "seconds"    # J
    .param p3, "nanos"    # I

    .line 487
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 488
    iput-wide p1, p0, Lorg/threeten/bp/Duration;->seconds:J

    .line 489
    iput p3, p0, Lorg/threeten/bp/Duration;->nanos:I

    .line 490
    return-void
.end method

.method private static create(JI)Lorg/threeten/bp/Duration;
    .registers 7
    .param p0, "seconds"    # J
    .param p2, "nanoAdjustment"    # I

    .line 474
    int-to-long v0, p2

    or-long/2addr v0, p0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_b

    .line 475
    sget-object v0, Lorg/threeten/bp/Duration;->ZERO:Lorg/threeten/bp/Duration;

    return-object v0

    .line 477
    :cond_b
    new-instance v0, Lorg/threeten/bp/Duration;

    invoke-direct {v0, p0, p1, p2}, Lorg/threeten/bp/Duration;-><init>(JI)V

    return-object v0
.end method

.method public static ofNanos(J)Lorg/threeten/bp/Duration;
    .registers 7
    .param p0, "nanos"    # J

    .line 246
    const-wide/32 v0, 0x3b9aca00

    div-long v2, p0, v0

    .line 247
    .local v2, "secs":J
    const-wide/32 v0, 0x3b9aca00

    rem-long v0, p0, v0

    long-to-int v4, v0

    .line 248
    .local v4, "nos":I
    if-gez v4, :cond_14

    .line 249
    const v0, 0x3b9aca00

    add-int/2addr v4, v0

    .line 250
    const-wide/16 v0, 0x1

    sub-long/2addr v2, v0

    .line 252
    :cond_14
    invoke-static {v2, v3, v4}, Lorg/threeten/bp/Duration;->create(JI)Lorg/threeten/bp/Duration;

    move-result-object v0

    return-object v0
.end method

.method public static ofSeconds(J)Lorg/threeten/bp/Duration;
    .registers 3
    .param p0, "seconds"    # J

    .line 190
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lorg/threeten/bp/Duration;->create(JI)Lorg/threeten/bp/Duration;

    move-result-object v0

    return-object v0
.end method

.method public static ofSeconds(JJ)Lorg/threeten/bp/Duration;
    .registers 9
    .param p0, "seconds"    # J
    .param p2, "nanoAdjustment"    # J

    .line 213
    const-wide/32 v0, 0x3b9aca00

    invoke-static {p2, p3, v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->floorDiv(JJ)J

    move-result-wide v0

    invoke-static {p0, p1, v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->safeAdd(JJ)J

    move-result-wide v2

    .line 214
    .local v2, "secs":J
    const v0, 0x3b9aca00

    invoke-static {p2, p3, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->floorMod(JI)I

    move-result v4

    .line 215
    .local v4, "nos":I
    invoke-static {v2, v3, v4}, Lorg/threeten/bp/Duration;->create(JI)Lorg/threeten/bp/Duration;

    move-result-object v0

    return-object v0
.end method

.method static readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/Duration;
    .registers 6
    .param p0, "in"    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1260
    invoke-interface {p0}, Ljava/io/DataInput;->readLong()J

    move-result-wide v2

    .line 1261
    .local v2, "seconds":J
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v4

    .line 1262
    .local v4, "nanos":I
    int-to-long v0, v4

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/Duration;->ofSeconds(JJ)Lorg/threeten/bp/Duration;

    move-result-object v0

    return-object v0
.end method

.method private readResolve()Ljava/lang/Object;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .line 1251
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Deserialization via serialization delegate"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .registers 3

    .line 1242
    new-instance v0, Lorg/threeten/bp/Ser;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0}, Lorg/threeten/bp/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public addTo(Lorg/threeten/bp/temporal/Temporal;)Lorg/threeten/bp/temporal/Temporal;
    .registers 6
    .param p1, "temporal"    # Lorg/threeten/bp/temporal/Temporal;

    .line 1000
    iget-wide v0, p0, Lorg/threeten/bp/Duration;->seconds:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_10

    .line 1001
    iget-wide v0, p0, Lorg/threeten/bp/Duration;->seconds:J

    sget-object v2, Lorg/threeten/bp/temporal/ChronoUnit;->SECONDS:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-interface {p1, v0, v1, v2}, Lorg/threeten/bp/temporal/Temporal;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;

    move-result-object p1

    .line 1003
    :cond_10
    iget v0, p0, Lorg/threeten/bp/Duration;->nanos:I

    if-eqz v0, :cond_1d

    .line 1004
    iget v0, p0, Lorg/threeten/bp/Duration;->nanos:I

    int-to-long v0, v0

    sget-object v2, Lorg/threeten/bp/temporal/ChronoUnit;->NANOS:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-interface {p1, v0, v1, v2}, Lorg/threeten/bp/temporal/Temporal;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;

    move-result-object p1

    .line 1006
    :cond_1d
    return-object p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .param p1, "x0"    # Ljava/lang/Object;

    .line 93
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/Duration;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/Duration;->compareTo(Lorg/threeten/bp/Duration;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/threeten/bp/Duration;)I
    .registers 7
    .param p1, "otherDuration"    # Lorg/threeten/bp/Duration;

    .line 1134
    iget-wide v0, p0, Lorg/threeten/bp/Duration;->seconds:J

    iget-wide v2, p1, Lorg/threeten/bp/Duration;->seconds:J

    invoke-static {v0, v1, v2, v3}, Lorg/threeten/bp/jdk8/Jdk8Methods;->compareLongs(JJ)I

    move-result v4

    .line 1135
    .local v4, "cmp":I
    if-eqz v4, :cond_b

    .line 1136
    return v4

    .line 1138
    :cond_b
    iget v0, p0, Lorg/threeten/bp/Duration;->nanos:I

    iget v1, p1, Lorg/threeten/bp/Duration;->nanos:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .param p1, "otherDuration"    # Ljava/lang/Object;

    .line 1152
    if-ne p0, p1, :cond_4

    .line 1153
    const/4 v0, 0x1

    return v0

    .line 1155
    :cond_4
    instance-of v0, p1, Lorg/threeten/bp/Duration;

    if-eqz v0, :cond_1d

    .line 1156
    move-object v4, p1

    check-cast v4, Lorg/threeten/bp/Duration;

    .line 1157
    .local v4, "other":Lorg/threeten/bp/Duration;
    iget-wide v0, p0, Lorg/threeten/bp/Duration;->seconds:J

    iget-wide v2, v4, Lorg/threeten/bp/Duration;->seconds:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1b

    iget v0, p0, Lorg/threeten/bp/Duration;->nanos:I

    iget v1, v4, Lorg/threeten/bp/Duration;->nanos:I

    if-ne v0, v1, :cond_1b

    const/4 v0, 0x1

    goto :goto_1c

    :cond_1b
    const/4 v0, 0x0

    :goto_1c
    return v0

    .line 1160
    .end local v4    # "other":Lorg/threeten/bp/Duration;
    :cond_1d
    const/4 v0, 0x0

    return v0
.end method

.method public getSeconds()J
    .registers 3

    .line 552
    iget-wide v0, p0, Lorg/threeten/bp/Duration;->seconds:J

    return-wide v0
.end method

.method public hashCode()I
    .registers 6

    .line 1170
    iget-wide v0, p0, Lorg/threeten/bp/Duration;->seconds:J

    iget-wide v2, p0, Lorg/threeten/bp/Duration;->seconds:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    iget v1, p0, Lorg/threeten/bp/Duration;->nanos:I

    mul-int/lit8 v1, v1, 0x33

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 11

    .line 1198
    sget-object v0, Lorg/threeten/bp/Duration;->ZERO:Lorg/threeten/bp/Duration;

    if-ne p0, v0, :cond_7

    .line 1199
    const-string v0, "PT0S"

    return-object v0

    .line 1201
    :cond_7
    iget-wide v0, p0, Lorg/threeten/bp/Duration;->seconds:J

    const-wide/16 v2, 0xe10

    div-long v4, v0, v2

    .line 1202
    .local v4, "hours":J
    iget-wide v0, p0, Lorg/threeten/bp/Duration;->seconds:J

    const-wide/16 v2, 0xe10

    rem-long/2addr v0, v2

    const-wide/16 v2, 0x3c

    div-long/2addr v0, v2

    long-to-int v6, v0

    .line 1203
    .local v6, "minutes":I
    iget-wide v0, p0, Lorg/threeten/bp/Duration;->seconds:J

    const-wide/16 v2, 0x3c

    rem-long/2addr v0, v2

    long-to-int v7, v0

    .line 1204
    .local v7, "secs":I
    new-instance v8, Ljava/lang/StringBuilder;

    const/16 v0, 0x18

    invoke-direct {v8, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1205
    .local v8, "buf":Ljava/lang/StringBuilder;
    const-string v0, "PT"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1206
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-eqz v0, :cond_37

    .line 1207
    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x48

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1209
    :cond_37
    if-eqz v6, :cond_42

    .line 1210
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x4d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1212
    :cond_42
    if-nez v7, :cond_54

    iget v0, p0, Lorg/threeten/bp/Duration;->nanos:I

    if-nez v0, :cond_54

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_54

    .line 1213
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1215
    :cond_54
    if-gez v7, :cond_69

    iget v0, p0, Lorg/threeten/bp/Duration;->nanos:I

    if-lez v0, :cond_69

    .line 1216
    const/4 v0, -0x1

    if-ne v7, v0, :cond_63

    .line 1217
    const-string v0, "-0"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6c

    .line 1219
    :cond_63
    add-int/lit8 v0, v7, 0x1

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_6c

    .line 1222
    :cond_69
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1224
    :goto_6c
    iget v0, p0, Lorg/threeten/bp/Duration;->nanos:I

    if-lez v0, :cond_a7

    .line 1225
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    .line 1226
    .local v9, "pos":I
    if-gez v7, :cond_81

    .line 1227
    iget v0, p0, Lorg/threeten/bp/Duration;->nanos:I

    const v1, 0x77359400

    sub-int v0, v1, v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_8a

    .line 1229
    :cond_81
    iget v0, p0, Lorg/threeten/bp/Duration;->nanos:I

    const v1, 0x3b9aca00

    add-int/2addr v0, v1

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1231
    :goto_8a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->charAt(I)C

    move-result v0

    const/16 v1, 0x30

    if-ne v0, v1, :cond_a2

    .line 1232
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    goto :goto_8a

    .line 1234
    :cond_a2
    const/16 v0, 0x2e

    invoke-virtual {v8, v9, v0}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 1236
    .end local v9    # "pos":I
    :cond_a7
    const/16 v0, 0x53

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1237
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method writeExternal(Ljava/io/DataOutput;)V
    .registers 4
    .param p1, "out"    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1255
    iget-wide v0, p0, Lorg/threeten/bp/Duration;->seconds:J

    invoke-interface {p1, v0, v1}, Ljava/io/DataOutput;->writeLong(J)V

    .line 1256
    iget v0, p0, Lorg/threeten/bp/Duration;->nanos:I

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 1257
    return-void
.end method

.class Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$1;
.super Ljava/lang/Object;
.source "EmprestimosParceladosController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;->consultaOfertas(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;

.field final synthetic val$agencia:Ljava/lang/String;

.field final synthetic val$conta:Ljava/lang/String;

.field final synthetic val$cpfOperador:Ljava/lang/String;

.field final synthetic val$digitoConta:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .param p1, "this$0"    # Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;

    .line 26
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$1;->this$0:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;

    iput-object p2, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$1;->val$agencia:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$1;->val$conta:Ljava/lang/String;

    iput-object p4, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$1;->val$digitoConta:Ljava/lang/String;

    iput-object p5, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$1;->val$cpfOperador:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 8

    .line 29
    invoke-static {}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;->getApi()Lcom/itau/empresas/api/credito/ApiGiro;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$1;->val$agencia:Ljava/lang/String;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$1;->val$conta:Ljava/lang/String;

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$1;->val$digitoConta:Ljava/lang/String;

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController$1;->val$cpfOperador:Ljava/lang/String;

    .line 30
    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/itau/empresas/api/credito/ApiGiro;->buscaOfertaProdutosParcelados(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    move-result-object v6

    .line 32
    .local v6, "oferta":Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;
    new-instance v0, Lcom/itau/empresas/Evento$EventoEmprestimosParcelados;

    invoke-direct {v0, v6}, Lcom/itau/empresas/Evento$EventoEmprestimosParcelados;-><init>(Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;)V

    # invokes: Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;->post(Ljava/lang/Object;)V
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosController;->access$000(Ljava/lang/Object;)V

    .line 33
    return-void
.end method

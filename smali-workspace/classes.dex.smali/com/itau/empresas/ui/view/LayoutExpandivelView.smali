.class public Lcom/itau/empresas/ui/view/LayoutExpandivelView;
.super Landroid/widget/RelativeLayout;
.source "LayoutExpandivelView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field arrow:Landroid/widget/ImageView;

.field conteudoExpandivel:Landroid/widget/TextView;

.field private descricao:Ljava/lang/String;

.field textViewTitulo:Landroid/widget/TextView;

.field private titulo:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 42
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 43
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->inicio()V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->inicio()V

    .line 49
    invoke-direct {p0, p2}, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->iniciaAtributos(Landroid/util/AttributeSet;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 54
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->inicio()V

    .line 56
    invoke-direct {p0, p2}, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->iniciaAtributos(Landroid/util/AttributeSet;)V

    .line 57
    return-void
.end method

.method private iniciaAtributos(Landroid/util/AttributeSet;)V
    .registers 8
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .line 92
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/R$styleable;->LayoutExpandivelView:[I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 98
    .local v4, "a":Landroid/content/res/TypedArray;
    const/4 v0, 0x1

    :try_start_11
    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->descricao:Ljava/lang/String;

    .line 99
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->titulo:Ljava/lang/String;
    :try_end_1e
    .catchall {:try_start_11 .. :try_end_1e} :catchall_22

    .line 101
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 102
    goto :goto_27

    .line 101
    :catchall_22
    move-exception v5

    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    throw v5

    .line 103
    :goto_27
    return-void
.end method

.method private inicio()V
    .registers 2

    .line 88
    const v0, 0x7f02006e

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->setBackgroundResource(I)V

    .line 89
    return-void
.end method


# virtual methods
.method aterViews()V
    .registers 4

    .line 70
    iget-object v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->textViewTitulo:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->titulo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->conteudoExpandivel:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->descricao:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->conteudoExpandivel:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->descricao:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->arrow:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c00e1

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 75
    return-void
.end method

.method clickExpandivel()V
    .registers 3

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->conteudoExpandivel:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_10

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->conteudoExpandivel:Landroid/widget/TextView;

    const/16 v1, 0x96

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/ui/util/AnimationUtils;->collapseAnimation(Landroid/view/View;ILandroid/view/animation/Animation$AnimationListener;)V

    goto :goto_28

    .line 82
    :cond_10
    iget-object v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->conteudoExpandivel:Landroid/widget/TextView;

    const/16 v1, 0x96

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/ui/util/AnimationUtils;->expandAnimation(Landroid/view/View;ILandroid/view/animation/Animation$AnimationListener;)V

    .line 83
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->conteudoExpandivel:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils;->textToSpeech(Landroid/content/Context;Ljava/lang/String;)V

    .line 85
    :goto_28
    return-void
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .registers 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .line 116
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .registers 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .line 121
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .registers 5
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .line 107
    iget-object v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->arrow:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->arrow:Landroid/widget/ImageView;

    .line 108
    invoke-virtual {v1}, Landroid/widget/ImageView;->getRotation()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_14

    const/high16 v1, 0x43340000    # 180.0f

    goto :goto_15

    :cond_14
    const/4 v1, 0x0

    :goto_15
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->rotation(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 111
    return-void
.end method

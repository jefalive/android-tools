.class public final Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "SubMenuAdapter.java"

# interfaces
.implements Lcom/itau/empresas/feature/home/menu/adapter/ViewHolderClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter$SubMenuVH;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter<Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter$SubMenuVH;>;Lcom/itau/empresas/feature/home/menu/adapter/ViewHolderClickListener;"
    }
.end annotation


# instance fields
.field private final menuVOs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;"
        }
    .end annotation
.end field

.field private final selecionaItemMenu:Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;)V
    .registers 3
    .param p1, "menuVOs"    # Ljava/util/List;
    .param p2, "selecionaItemMenu"    # Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter;->menuVOs:Ljava/util/List;

    .line 26
    iput-object p2, p0, Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter;->selecionaItemMenu:Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;

    .line 27
    return-void
.end method


# virtual methods
.method public clicadoNaPosicao(I)V
    .registers 5
    .param p1, "posicao"    # I

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter;->menuVOs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/itau/empresas/api/model/MenuVO;

    .line 51
    .local v1, "menu":Lcom/itau/empresas/api/model/MenuVO;
    if-eqz v1, :cond_2f

    .line 53
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 55
    .local v2, "temSubMenu":Z
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2a

    .line 57
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_24

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_26

    :cond_24
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    :goto_26
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 60
    :cond_2a
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter;->selecionaItemMenu:Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;

    invoke-interface {v0, v1, v2}, Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;->menuSelecionado(Lcom/itau/empresas/api/model/MenuVO;Z)V

    .line 62
    .end local v2    # "temSubMenu":Z
    :cond_2f
    return-void
.end method

.method public getItemCount()I
    .registers 2

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter;->menuVOs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .registers 4

    .line 18
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter$SubMenuVH;

    invoke-virtual {p0, v0, p2}, Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter;->onBindViewHolder(Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter$SubMenuVH;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter$SubMenuVH;I)V
    .registers 6
    .param p1, "holder"    # Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter$SubMenuVH;
    .param p2, "position"    # I

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter;->menuVOs:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/api/model/MenuVO;

    .line 39
    .local v2, "menuVO":Lcom/itau/empresas/api/model/MenuVO;
    iget-object v0, p1, Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter$SubMenuVH;->titulo:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/MenuVO;->getNome()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .registers 4

    .line 18
    invoke-virtual {p0, p1, p2}, Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter$SubMenuVH;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter$SubMenuVH;
    .registers 7
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .line 31
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 32
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    const v2, 0x7f030112

    invoke-virtual {v0, v2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 33
    .local v3, "view":Landroid/view/View;
    new-instance v0, Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter$SubMenuVH;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v3, p0, v1}, Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter$SubMenuVH;-><init>(Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter;Landroid/view/View;Lcom/itau/empresas/feature/home/menu/adapter/ViewHolderClickListener;Lcom/itau/empresas/feature/home/menu/adapter/SubMenuAdapter$1;)V

    return-object v0
.end method

.class public final Lorg/threeten/bp/Instant;
.super Lorg/threeten/bp/jdk8/DefaultInterfaceTemporalAccessor;
.source "Instant.java"

# interfaces
.implements Lorg/threeten/bp/temporal/Temporal;
.implements Lorg/threeten/bp/temporal/TemporalAdjuster;
.implements Ljava/lang/Comparable;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/threeten/bp/Instant$2;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/threeten/bp/jdk8/DefaultInterfaceTemporalAccessor;Lorg/threeten/bp/temporal/Temporal;Lorg/threeten/bp/temporal/TemporalAdjuster;Ljava/lang/Comparable<Lorg/threeten/bp/Instant;>;Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field public static final EPOCH:Lorg/threeten/bp/Instant;

.field public static final FROM:Lorg/threeten/bp/temporal/TemporalQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/threeten/bp/temporal/TemporalQuery<Lorg/threeten/bp/Instant;>;"
        }
    .end annotation
.end field

.field public static final MAX:Lorg/threeten/bp/Instant;

.field public static final MIN:Lorg/threeten/bp/Instant;

.field private static final serialVersionUID:J = -0x93d170fdcc5dce4L


# instance fields
.field private final nanos:I

.field private final seconds:J


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 164
    new-instance v0, Lorg/threeten/bp/Instant;

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lorg/threeten/bp/Instant;-><init>(JI)V

    sput-object v0, Lorg/threeten/bp/Instant;->EPOCH:Lorg/threeten/bp/Instant;

    .line 183
    const-wide v0, -0x701cefeb9bec00L

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/threeten/bp/Instant;->ofEpochSecond(JJ)Lorg/threeten/bp/Instant;

    move-result-object v0

    sput-object v0, Lorg/threeten/bp/Instant;->MIN:Lorg/threeten/bp/Instant;

    .line 194
    const-wide v0, 0x701cd2fa9578ffL

    const-wide/32 v2, 0x3b9ac9ff

    invoke-static {v0, v1, v2, v3}, Lorg/threeten/bp/Instant;->ofEpochSecond(JJ)Lorg/threeten/bp/Instant;

    move-result-object v0

    sput-object v0, Lorg/threeten/bp/Instant;->MAX:Lorg/threeten/bp/Instant;

    .line 198
    new-instance v0, Lorg/threeten/bp/Instant$1;

    invoke-direct {v0}, Lorg/threeten/bp/Instant$1;-><init>()V

    sput-object v0, Lorg/threeten/bp/Instant;->FROM:Lorg/threeten/bp/temporal/TemporalQuery;

    return-void
.end method

.method private constructor <init>(JI)V
    .registers 4
    .param p1, "epochSecond"    # J
    .param p3, "nanos"    # I

    .line 387
    invoke-direct {p0}, Lorg/threeten/bp/jdk8/DefaultInterfaceTemporalAccessor;-><init>()V

    .line 388
    iput-wide p1, p0, Lorg/threeten/bp/Instant;->seconds:J

    .line 389
    iput p3, p0, Lorg/threeten/bp/Instant;->nanos:I

    .line 390
    return-void
.end method

.method private static create(JI)Lorg/threeten/bp/Instant;
    .registers 7
    .param p0, "seconds"    # J
    .param p2, "nanoOfSecond"    # I

    .line 370
    int-to-long v0, p2

    or-long/2addr v0, p0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_b

    .line 371
    sget-object v0, Lorg/threeten/bp/Instant;->EPOCH:Lorg/threeten/bp/Instant;

    return-object v0

    .line 373
    :cond_b
    const-wide v0, -0x701cefeb9bec00L

    cmp-long v0, p0, v0

    if-ltz v0, :cond_1d

    const-wide v0, 0x701cd2fa9578ffL

    cmp-long v0, p0, v0

    if-lez v0, :cond_25

    .line 374
    :cond_1d
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    const-string v1, "Instant exceeds minimum or maximum instant"

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 376
    :cond_25
    new-instance v0, Lorg/threeten/bp/Instant;

    invoke-direct {v0, p0, p1, p2}, Lorg/threeten/bp/Instant;-><init>(JI)V

    return-object v0
.end method

.method public static from(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/Instant;
    .registers 7
    .param p0, "temporal"    # Lorg/threeten/bp/temporal/TemporalAccessor;

    .line 336
    :try_start_0
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->INSTANT_SECONDS:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p0, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->getLong(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v3

    .line 337
    .local v3, "instantSecs":J
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->NANO_OF_SECOND:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p0, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->get(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v5

    .line 338
    .local v5, "nanoOfSecond":I
    int-to-long v0, v5

    invoke-static {v3, v4, v0, v1}, Lorg/threeten/bp/Instant;->ofEpochSecond(JJ)Lorg/threeten/bp/Instant;
    :try_end_10
    .catch Lorg/threeten/bp/DateTimeException; {:try_start_0 .. :try_end_10} :catch_12

    move-result-object v0

    return-object v0

    .line 339
    .end local v3    # "instantSecs":J
    .end local v5    # "nanoOfSecond":I
    :catch_12
    move-exception v3

    .line 340
    .local v3, "ex":Lorg/threeten/bp/DateTimeException;
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to obtain Instant from TemporalAccessor: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public static ofEpochMilli(J)Lorg/threeten/bp/Instant;
    .registers 7
    .param p0, "epochMilli"    # J

    .line 312
    const-wide/16 v0, 0x3e8

    invoke-static {p0, p1, v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->floorDiv(JJ)J

    move-result-wide v2

    .line 313
    .local v2, "secs":J
    const/16 v0, 0x3e8

    invoke-static {p0, p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->floorMod(JI)I

    move-result v4

    .line 314
    .local v4, "mos":I
    const v0, 0xf4240

    mul-int/2addr v0, v4

    invoke-static {v2, v3, v0}, Lorg/threeten/bp/Instant;->create(JI)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0
.end method

.method public static ofEpochSecond(J)Lorg/threeten/bp/Instant;
    .registers 3
    .param p0, "epochSecond"    # J

    .line 272
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lorg/threeten/bp/Instant;->create(JI)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0
.end method

.method public static ofEpochSecond(JJ)Lorg/threeten/bp/Instant;
    .registers 9
    .param p0, "epochSecond"    # J
    .param p2, "nanoAdjustment"    # J

    .line 296
    const-wide/32 v0, 0x3b9aca00

    invoke-static {p2, p3, v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->floorDiv(JJ)J

    move-result-wide v0

    invoke-static {p0, p1, v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->safeAdd(JJ)J

    move-result-wide v2

    .line 297
    .local v2, "secs":J
    const v0, 0x3b9aca00

    invoke-static {p2, p3, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->floorMod(JI)I

    move-result v4

    .line 298
    .local v4, "nos":I
    invoke-static {v2, v3, v4}, Lorg/threeten/bp/Instant;->create(JI)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0
.end method

.method private plus(JJ)Lorg/threeten/bp/Instant;
    .registers 13
    .param p1, "secondsToAdd"    # J
    .param p3, "nanosToAdd"    # J

    .line 778
    or-long v0, p1, p3

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_9

    .line 779
    return-object p0

    .line 781
    :cond_9
    iget-wide v0, p0, Lorg/threeten/bp/Instant;->seconds:J

    invoke-static {v0, v1, p1, p2}, Lorg/threeten/bp/jdk8/Jdk8Methods;->safeAdd(JJ)J

    move-result-wide v4

    .line 782
    .local v4, "epochSec":J
    const-wide/32 v0, 0x3b9aca00

    div-long v0, p3, v0

    invoke-static {v4, v5, v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->safeAdd(JJ)J

    move-result-wide v4

    .line 783
    const-wide/32 v0, 0x3b9aca00

    rem-long/2addr p3, v0

    .line 784
    iget v0, p0, Lorg/threeten/bp/Instant;->nanos:I

    int-to-long v0, v0

    add-long v6, v0, p3

    .line 785
    .local v6, "nanoAdjustment":J
    invoke-static {v4, v5, v6, v7}, Lorg/threeten/bp/Instant;->ofEpochSecond(JJ)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0
.end method

.method static readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/Instant;
    .registers 6
    .param p0, "in"    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1170
    invoke-interface {p0}, Ljava/io/DataInput;->readLong()J

    move-result-wide v2

    .line 1171
    .local v2, "seconds":J
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v4

    .line 1172
    .local v4, "nanos":I
    int-to-long v0, v4

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/Instant;->ofEpochSecond(JJ)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0
.end method

.method private readResolve()Ljava/lang/Object;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .line 1161
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Deserialization via serialization delegate"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .registers 3

    .line 1152
    new-instance v0, Lorg/threeten/bp/Ser;

    const/4 v1, 0x2

    invoke-direct {v0, v1, p0}, Lorg/threeten/bp/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public adjustInto(Lorg/threeten/bp/temporal/Temporal;)Lorg/threeten/bp/temporal/Temporal;
    .registers 6
    .param p1, "temporal"    # Lorg/threeten/bp/temporal/Temporal;

    .line 922
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->INSTANT_SECONDS:Lorg/threeten/bp/temporal/ChronoField;

    iget-wide v1, p0, Lorg/threeten/bp/Instant;->seconds:J

    invoke-interface {p1, v0, v1, v2}, Lorg/threeten/bp/temporal/Temporal;->with(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->NANO_OF_SECOND:Lorg/threeten/bp/temporal/ChronoField;

    iget v2, p0, Lorg/threeten/bp/Instant;->nanos:I

    int-to-long v2, v2

    invoke-interface {v0, v1, v2, v3}, Lorg/threeten/bp/temporal/Temporal;->with(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .param p1, "x0"    # Ljava/lang/Object;

    .line 157
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/Instant;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/Instant;->compareTo(Lorg/threeten/bp/Instant;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/threeten/bp/Instant;)I
    .registers 7
    .param p1, "otherInstant"    # Lorg/threeten/bp/Instant;

    .line 1072
    iget-wide v0, p0, Lorg/threeten/bp/Instant;->seconds:J

    iget-wide v2, p1, Lorg/threeten/bp/Instant;->seconds:J

    invoke-static {v0, v1, v2, v3}, Lorg/threeten/bp/jdk8/Jdk8Methods;->compareLongs(JJ)I

    move-result v4

    .line 1073
    .local v4, "cmp":I
    if-eqz v4, :cond_b

    .line 1074
    return v4

    .line 1076
    :cond_b
    iget v0, p0, Lorg/threeten/bp/Instant;->nanos:I

    iget v1, p1, Lorg/threeten/bp/Instant;->nanos:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .param p1, "otherInstant"    # Ljava/lang/Object;

    .line 1116
    if-ne p0, p1, :cond_4

    .line 1117
    const/4 v0, 0x1

    return v0

    .line 1119
    :cond_4
    instance-of v0, p1, Lorg/threeten/bp/Instant;

    if-eqz v0, :cond_1d

    .line 1120
    move-object v4, p1

    check-cast v4, Lorg/threeten/bp/Instant;

    .line 1121
    .local v4, "other":Lorg/threeten/bp/Instant;
    iget-wide v0, p0, Lorg/threeten/bp/Instant;->seconds:J

    iget-wide v2, v4, Lorg/threeten/bp/Instant;->seconds:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1b

    iget v0, p0, Lorg/threeten/bp/Instant;->nanos:I

    iget v1, v4, Lorg/threeten/bp/Instant;->nanos:I

    if-ne v0, v1, :cond_1b

    const/4 v0, 0x1

    goto :goto_1c

    :cond_1b
    const/4 v0, 0x0

    :goto_1c
    return v0

    .line 1124
    .end local v4    # "other":Lorg/threeten/bp/Instant;
    :cond_1d
    const/4 v0, 0x0

    return v0
.end method

.method public get(Lorg/threeten/bp/temporal/TemporalField;)I
    .registers 5
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;

    .line 487
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_3b

    .line 488
    sget-object v0, Lorg/threeten/bp/Instant$2;->$SwitchMap$org$threeten$bp$temporal$ChronoField:[I

    move-object v1, p1

    check-cast v1, Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v1}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_48

    goto :goto_22

    .line 489
    :pswitch_13
    iget v0, p0, Lorg/threeten/bp/Instant;->nanos:I

    return v0

    .line 490
    :pswitch_16
    iget v0, p0, Lorg/threeten/bp/Instant;->nanos:I

    div-int/lit16 v0, v0, 0x3e8

    return v0

    .line 491
    :pswitch_1b
    iget v0, p0, Lorg/threeten/bp/Instant;->nanos:I

    const v1, 0xf4240

    div-int/2addr v0, v1

    return v0

    .line 493
    :goto_22
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 495
    :cond_3b
    invoke-virtual {p0, p1}, Lorg/threeten/bp/Instant;->range(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalField;->getFrom(Lorg/threeten/bp/temporal/TemporalAccessor;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p1}, Lorg/threeten/bp/temporal/ValueRange;->checkValidIntValue(JLorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    return v0

    :pswitch_data_48
    .packed-switch 0x1
        :pswitch_13
        :pswitch_16
        :pswitch_1b
    .end packed-switch
.end method

.method public getEpochSecond()J
    .registers 3

    .line 545
    iget-wide v0, p0, Lorg/threeten/bp/Instant;->seconds:J

    return-wide v0
.end method

.method public getLong(Lorg/threeten/bp/temporal/TemporalField;)J
    .registers 5
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;

    .line 522
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_41

    .line 523
    sget-object v0, Lorg/threeten/bp/Instant$2;->$SwitchMap$org$threeten$bp$temporal$ChronoField:[I

    move-object v1, p1

    check-cast v1, Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v1}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_46

    goto :goto_28

    .line 524
    :pswitch_13
    iget v0, p0, Lorg/threeten/bp/Instant;->nanos:I

    int-to-long v0, v0

    return-wide v0

    .line 525
    :pswitch_17
    iget v0, p0, Lorg/threeten/bp/Instant;->nanos:I

    div-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    return-wide v0

    .line 526
    :pswitch_1d
    iget v0, p0, Lorg/threeten/bp/Instant;->nanos:I

    const v1, 0xf4240

    div-int/2addr v0, v1

    int-to-long v0, v0

    return-wide v0

    .line 527
    :pswitch_25
    iget-wide v0, p0, Lorg/threeten/bp/Instant;->seconds:J

    return-wide v0

    .line 529
    :goto_28
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 531
    :cond_41
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalField;->getFrom(Lorg/threeten/bp/temporal/TemporalAccessor;)J

    move-result-wide v0

    return-wide v0

    :pswitch_data_46
    .packed-switch 0x1
        :pswitch_13
        :pswitch_17
        :pswitch_1d
        :pswitch_25
    .end packed-switch
.end method

.method public getNano()I
    .registers 2

    .line 558
    iget v0, p0, Lorg/threeten/bp/Instant;->nanos:I

    return v0
.end method

.method public hashCode()I
    .registers 6

    .line 1134
    iget-wide v0, p0, Lorg/threeten/bp/Instant;->seconds:J

    iget-wide v2, p0, Lorg/threeten/bp/Instant;->seconds:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    iget v1, p0, Lorg/threeten/bp/Instant;->nanos:I

    mul-int/lit8 v1, v1, 0x33

    add-int/2addr v0, v1

    return v0
.end method

.method public isSupported(Lorg/threeten/bp/temporal/TemporalField;)Z
    .registers 3
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;

    .line 420
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_18

    .line 421
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->INSTANT_SECONDS:Lorg/threeten/bp/temporal/ChronoField;

    if-eq p1, v0, :cond_14

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->NANO_OF_SECOND:Lorg/threeten/bp/temporal/ChronoField;

    if-eq p1, v0, :cond_14

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->MICRO_OF_SECOND:Lorg/threeten/bp/temporal/ChronoField;

    if-eq p1, v0, :cond_14

    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->MILLI_OF_SECOND:Lorg/threeten/bp/temporal/ChronoField;

    if-ne p1, v0, :cond_16

    :cond_14
    const/4 v0, 0x1

    goto :goto_17

    :cond_16
    const/4 v0, 0x0

    :goto_17
    return v0

    .line 423
    :cond_18
    if-eqz p1, :cond_22

    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalField;->isSupportedBy(Lorg/threeten/bp/temporal/TemporalAccessor;)Z

    move-result v0

    if-eqz v0, :cond_22

    const/4 v0, 0x1

    goto :goto_23

    :cond_22
    const/4 v0, 0x0

    :goto_23
    return v0
.end method

.method public minus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/Instant;
    .registers 7
    .param p1, "amountToSubtract"    # J
    .param p3, "unit"    # Lorg/threeten/bp/temporal/TemporalUnit;

    .line 806
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p1, v0

    if-nez v0, :cond_16

    const-wide v0, 0x7fffffffffffffffL

    invoke-virtual {p0, v0, v1, p3}, Lorg/threeten/bp/Instant;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/Instant;

    move-result-object v0

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2, p3}, Lorg/threeten/bp/Instant;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/Instant;

    move-result-object v0

    goto :goto_1b

    :cond_16
    neg-long v0, p1

    invoke-virtual {p0, v0, v1, p3}, Lorg/threeten/bp/Instant;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/Instant;

    move-result-object v0

    :goto_1b
    return-object v0
.end method

.method public bridge synthetic minus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;
    .registers 5
    .param p1, "x0"    # J
    .param p3, "x1"    # Lorg/threeten/bp/temporal/TemporalUnit;

    .line 157
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/Instant;->minus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0
.end method

.method public plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/Instant;
    .registers 10
    .param p1, "amountToAdd"    # J
    .param p3, "unit"    # Lorg/threeten/bp/temporal/TemporalUnit;

    .line 707
    instance-of v0, p3, Lorg/threeten/bp/temporal/ChronoUnit;

    if-eqz v0, :cond_7b

    .line 708
    sget-object v0, Lorg/threeten/bp/Instant$2;->$SwitchMap$org$threeten$bp$temporal$ChronoUnit:[I

    move-object v1, p3

    check-cast v1, Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-virtual {v1}, Lorg/threeten/bp/temporal/ChronoUnit;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_82

    goto :goto_62

    .line 709
    :pswitch_13
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/Instant;->plusNanos(J)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0

    .line 710
    :pswitch_18
    const-wide/32 v0, 0xf4240

    div-long v0, p1, v0

    const-wide/32 v2, 0xf4240

    rem-long v2, p1, v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-direct {p0, v0, v1, v2, v3}, Lorg/threeten/bp/Instant;->plus(JJ)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0

    .line 711
    :pswitch_2a
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/Instant;->plusMillis(J)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0

    .line 712
    :pswitch_2f
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/Instant;->plusSeconds(J)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0

    .line 713
    :pswitch_34
    const/16 v0, 0x3c

    invoke-static {p1, p2, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->safeMultiply(JI)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/Instant;->plusSeconds(J)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0

    .line 714
    :pswitch_3f
    const/16 v0, 0xe10

    invoke-static {p1, p2, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->safeMultiply(JI)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/Instant;->plusSeconds(J)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0

    .line 715
    :pswitch_4a
    const v0, 0xa8c0

    invoke-static {p1, p2, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->safeMultiply(JI)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/Instant;->plusSeconds(J)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0

    .line 716
    :pswitch_56
    const v0, 0x15180

    invoke-static {p1, p2, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->safeMultiply(JI)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/Instant;->plusSeconds(J)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0

    .line 718
    :goto_62
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported unit: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 720
    :cond_7b
    invoke-interface {p3, p0, p1, p2}, Lorg/threeten/bp/temporal/TemporalUnit;->addTo(Lorg/threeten/bp/temporal/Temporal;J)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/Instant;

    return-object v0

    :pswitch_data_82
    .packed-switch 0x1
        :pswitch_13
        :pswitch_18
        :pswitch_2a
        :pswitch_2f
        :pswitch_34
        :pswitch_3f
        :pswitch_4a
        :pswitch_56
    .end packed-switch
.end method

.method public bridge synthetic plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;
    .registers 5
    .param p1, "x0"    # J
    .param p3, "x1"    # Lorg/threeten/bp/temporal/TemporalUnit;

    .line 157
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/Instant;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0
.end method

.method public plusMillis(J)Lorg/threeten/bp/Instant;
    .registers 9
    .param p1, "millisToAdd"    # J

    .line 749
    const-wide/16 v0, 0x3e8

    div-long v0, p1, v0

    const-wide/16 v2, 0x3e8

    rem-long v2, p1, v2

    const-wide/32 v4, 0xf4240

    mul-long/2addr v2, v4

    invoke-direct {p0, v0, v1, v2, v3}, Lorg/threeten/bp/Instant;->plus(JJ)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0
.end method

.method public plusNanos(J)Lorg/threeten/bp/Instant;
    .registers 5
    .param p1, "nanosToAdd"    # J

    .line 763
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1, p1, p2}, Lorg/threeten/bp/Instant;->plus(JJ)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0
.end method

.method public plusSeconds(J)Lorg/threeten/bp/Instant;
    .registers 5
    .param p1, "secondsToAdd"    # J

    .line 735
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lorg/threeten/bp/Instant;->plus(JJ)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0
.end method

.method public query(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;
    .registers 3
    .param p1, "query"    # Lorg/threeten/bp/temporal/TemporalQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:Ljava/lang/Object;>(Lorg/threeten/bp/temporal/TemporalQuery<TR;>;)TR;"
        }
    .end annotation

    .line 883
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->precision()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-ne p1, v0, :cond_9

    .line 884
    sget-object v0, Lorg/threeten/bp/temporal/ChronoUnit;->NANOS:Lorg/threeten/bp/temporal/ChronoUnit;

    return-object v0

    .line 887
    :cond_9
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->localDate()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-eq p1, v0, :cond_2d

    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->localTime()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-eq p1, v0, :cond_2d

    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->chronology()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-eq p1, v0, :cond_2d

    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->zoneId()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-eq p1, v0, :cond_2d

    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->zone()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-eq p1, v0, :cond_2d

    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->offset()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-ne p1, v0, :cond_2f

    .line 890
    :cond_2d
    const/4 v0, 0x0

    return-object v0

    .line 892
    :cond_2f
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalQuery;->queryFrom(Lorg/threeten/bp/temporal/TemporalAccessor;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public range(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;
    .registers 3
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;

    .line 458
    invoke-super {p0, p1}, Lorg/threeten/bp/jdk8/DefaultInterfaceTemporalAccessor;->range(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    return-object v0
.end method

.method public toEpochMilli()J
    .registers 6

    .line 1055
    iget-wide v0, p0, Lorg/threeten/bp/Instant;->seconds:J

    const/16 v2, 0x3e8

    invoke-static {v0, v1, v2}, Lorg/threeten/bp/jdk8/Jdk8Methods;->safeMultiply(JI)J

    move-result-wide v3

    .line 1056
    .local v3, "millis":J
    iget v0, p0, Lorg/threeten/bp/Instant;->nanos:I

    const v1, 0xf4240

    div-int/2addr v0, v1

    int-to-long v0, v0

    add-long/2addr v0, v3

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .line 1147
    sget-object v0, Lorg/threeten/bp/format/DateTimeFormatter;->ISO_INSTANT:Lorg/threeten/bp/format/DateTimeFormatter;

    invoke-virtual {v0, p0}, Lorg/threeten/bp/format/DateTimeFormatter;->format(Lorg/threeten/bp/temporal/TemporalAccessor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public with(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/Instant;
    .registers 3
    .param p1, "adjuster"    # Lorg/threeten/bp/temporal/TemporalAdjuster;

    .line 582
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalAdjuster;->adjustInto(Lorg/threeten/bp/temporal/Temporal;)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/Instant;

    return-object v0
.end method

.method public with(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/Instant;
    .registers 9
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;
    .param p2, "newValue"    # J

    .line 630
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_73

    .line 631
    move-object v3, p1

    check-cast v3, Lorg/threeten/bp/temporal/ChronoField;

    .line 632
    .local v3, "f":Lorg/threeten/bp/temporal/ChronoField;
    invoke-virtual {v3, p2, p3}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 633
    sget-object v0, Lorg/threeten/bp/Instant$2;->$SwitchMap$org$threeten$bp$temporal$ChronoField:[I

    invoke-virtual {v3}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_7a

    goto/16 :goto_5a

    .line 635
    :pswitch_17
    long-to-int v0, p2

    const v1, 0xf4240

    mul-int v4, v0, v1

    .line 636
    .local v4, "nval":I
    iget v0, p0, Lorg/threeten/bp/Instant;->nanos:I

    if-eq v4, v0, :cond_28

    iget-wide v0, p0, Lorg/threeten/bp/Instant;->seconds:J

    invoke-static {v0, v1, v4}, Lorg/threeten/bp/Instant;->create(JI)Lorg/threeten/bp/Instant;

    move-result-object v0

    goto :goto_29

    :cond_28
    move-object v0, p0

    :goto_29
    return-object v0

    .line 639
    .end local v4    # "nval":I
    :pswitch_2a
    long-to-int v0, p2

    mul-int/lit16 v4, v0, 0x3e8

    .line 640
    .local v4, "nval":I
    iget v0, p0, Lorg/threeten/bp/Instant;->nanos:I

    if-eq v4, v0, :cond_38

    iget-wide v0, p0, Lorg/threeten/bp/Instant;->seconds:J

    invoke-static {v0, v1, v4}, Lorg/threeten/bp/Instant;->create(JI)Lorg/threeten/bp/Instant;

    move-result-object v0

    goto :goto_39

    :cond_38
    move-object v0, p0

    :goto_39
    return-object v0

    .line 642
    .end local v4    # "nval":I
    :pswitch_3a
    iget v0, p0, Lorg/threeten/bp/Instant;->nanos:I

    int-to-long v0, v0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_49

    iget-wide v0, p0, Lorg/threeten/bp/Instant;->seconds:J

    long-to-int v2, p2

    invoke-static {v0, v1, v2}, Lorg/threeten/bp/Instant;->create(JI)Lorg/threeten/bp/Instant;

    move-result-object v0

    goto :goto_4a

    :cond_49
    move-object v0, p0

    :goto_4a
    return-object v0

    .line 643
    :pswitch_4b
    iget-wide v0, p0, Lorg/threeten/bp/Instant;->seconds:J

    cmp-long v0, p2, v0

    if-eqz v0, :cond_58

    iget v0, p0, Lorg/threeten/bp/Instant;->nanos:I

    invoke-static {p2, p3, v0}, Lorg/threeten/bp/Instant;->create(JI)Lorg/threeten/bp/Instant;

    move-result-object v0

    goto :goto_59

    :cond_58
    move-object v0, p0

    :goto_59
    return-object v0

    .line 645
    :goto_5a
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 647
    .end local v3    # "f":Lorg/threeten/bp/temporal/ChronoField;
    :cond_73
    invoke-interface {p1, p0, p2, p3}, Lorg/threeten/bp/temporal/TemporalField;->adjustInto(Lorg/threeten/bp/temporal/Temporal;J)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/Instant;

    return-object v0

    :pswitch_data_7a
    .packed-switch 0x1
        :pswitch_3a
        :pswitch_2a
        :pswitch_17
        :pswitch_4b
    .end packed-switch
.end method

.method public bridge synthetic with(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/temporal/Temporal;
    .registers 3
    .param p1, "x0"    # Lorg/threeten/bp/temporal/TemporalAdjuster;

    .line 157
    invoke-virtual {p0, p1}, Lorg/threeten/bp/Instant;->with(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic with(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/temporal/Temporal;
    .registers 5
    .param p1, "x0"    # Lorg/threeten/bp/temporal/TemporalField;
    .param p2, "x1"    # J

    .line 157
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/Instant;->with(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0
.end method

.method writeExternal(Ljava/io/DataOutput;)V
    .registers 4
    .param p1, "out"    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1165
    iget-wide v0, p0, Lorg/threeten/bp/Instant;->seconds:J

    invoke-interface {p1, v0, v1}, Ljava/io/DataOutput;->writeLong(J)V

    .line 1166
    iget v0, p0, Lorg/threeten/bp/Instant;->nanos:I

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 1167
    return-void
.end method

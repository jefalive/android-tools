.class public Lcom/itau/empresas/ui/util/formatacao/FormataHorario;
.super Ljava/lang/Object;
.source "FormataHorario.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static HoraMinutoSegundoParaHoraMinuto(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .param p0, "horario"    # Ljava/lang/String;

    .line 9
    const-string v0, "HH:mm:ss"

    invoke-static {v0}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/threeten/bp/LocalTime;->parse(Ljava/lang/CharSequence;Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    const-string v1, "HH:mm"

    .line 10
    invoke-static {v1}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalTime;->format(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object v0

    .line 9
    return-object v0
.end method

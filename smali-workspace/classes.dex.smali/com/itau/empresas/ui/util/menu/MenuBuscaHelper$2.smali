.class Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper$2;
.super Ljava/lang/Object;
.source "MenuBuscaHelper.java"

# interfaces
.implements Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper$CriterioBusca;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;


# direct methods
.method constructor <init>(Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;

    .line 86
    iput-object p1, p0, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper$2;->this$0:Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isCriterioValido(Lcom/itau/empresas/api/model/MenuVO;Ljava/lang/String;)Z
    .registers 4
    .param p1, "menuVO"    # Lcom/itau/empresas/api/model/MenuVO;
    .param p2, "s"    # Ljava/lang/String;

    .line 89
    if-eqz p1, :cond_a

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/MenuVO;->getNome()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    if-nez p2, :cond_c

    .line 90
    :cond_a
    const/4 v0, 0x0

    return v0

    .line 92
    :cond_c
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

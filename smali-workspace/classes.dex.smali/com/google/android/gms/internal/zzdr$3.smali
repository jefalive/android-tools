.class Lcom/google/android/gms/internal/zzdr$3;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/internal/zzdr;->zza(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic zzzP:Ljava/lang/String;

.field final synthetic zzzQ:Ljava/lang/String;

.field final synthetic zzzU:Lcom/google/android/gms/internal/zzdr;

.field final synthetic zzzV:Ljava/lang/String;

.field final synthetic zzzW:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/zzdr;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6

    iput-object p1, p0, Lcom/google/android/gms/internal/zzdr$3;->zzzU:Lcom/google/android/gms/internal/zzdr;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzdr$3;->zzzP:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/internal/zzdr$3;->zzzQ:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/internal/zzdr$3;->zzzV:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/internal/zzdr$3;->zzzW:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    const-string v0, "event"

    const-string v1, "precacheCanceled"

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "src"

    iget-object v1, p0, Lcom/google/android/gms/internal/zzdr$3;->zzzP:Ljava/lang/String;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdr$3;->zzzQ:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_22

    const-string v0, "cachedSrc"

    iget-object v1, p0, Lcom/google/android/gms/internal/zzdr$3;->zzzQ:Ljava/lang/String;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_22
    const-string v0, "type"

    iget-object v1, p0, Lcom/google/android/gms/internal/zzdr$3;->zzzU:Lcom/google/android/gms/internal/zzdr;

    iget-object v2, p0, Lcom/google/android/gms/internal/zzdr$3;->zzzV:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/internal/zzdr;->zza(Lcom/google/android/gms/internal/zzdr;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "reason"

    iget-object v1, p0, Lcom/google/android/gms/internal/zzdr$3;->zzzV:Ljava/lang/String;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzdr$3;->zzzW:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_45

    const-string v0, "message"

    iget-object v1, p0, Lcom/google/android/gms/internal/zzdr$3;->zzzW:Ljava/lang/String;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_45
    iget-object v0, p0, Lcom/google/android/gms/internal/zzdr$3;->zzzU:Lcom/google/android/gms/internal/zzdr;

    const-string v1, "onPrecacheEvent"

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/internal/zzdr;->zza(Lcom/google/android/gms/internal/zzdr;Ljava/lang/String;Ljava/util/Map;)V

    return-void
.end method

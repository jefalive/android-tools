.class final Lokio/SegmentPool;
.super Ljava/lang/Object;
.source "SegmentPool.java"


# static fields
.field static byteCount:J

.field static next:Lokio/Segment;


# direct methods
.method private constructor <init>()V
    .registers 1

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    return-void
.end method

.method static recycle(Lokio/Segment;)V
    .registers 7
    .param p0, "segment"    # Lokio/Segment;

    .line 50
    iget-object v0, p0, Lokio/Segment;->next:Lokio/Segment;

    if-nez v0, :cond_8

    iget-object v0, p0, Lokio/Segment;->prev:Lokio/Segment;

    if-eqz v0, :cond_e

    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 51
    :cond_e
    iget-boolean v0, p0, Lokio/Segment;->shared:Z

    if-eqz v0, :cond_13

    return-void

    .line 52
    :cond_13
    const-class v4, Lokio/SegmentPool;

    monitor-enter v4

    .line 53
    :try_start_16
    sget-wide v0, Lokio/SegmentPool;->byteCount:J
    :try_end_18
    .catchall {:try_start_16 .. :try_end_18} :catchall_39

    const-wide/16 v2, 0x2000

    add-long/2addr v0, v2

    const-wide/32 v2, 0x10000

    cmp-long v0, v0, v2

    if-lez v0, :cond_24

    monitor-exit v4

    return-void

    .line 54
    :cond_24
    :try_start_24
    sget-wide v0, Lokio/SegmentPool;->byteCount:J

    const-wide/16 v2, 0x2000

    add-long/2addr v0, v2

    sput-wide v0, Lokio/SegmentPool;->byteCount:J

    .line 55
    sget-object v0, Lokio/SegmentPool;->next:Lokio/Segment;

    iput-object v0, p0, Lokio/Segment;->next:Lokio/Segment;

    .line 56
    const/4 v0, 0x0

    iput v0, p0, Lokio/Segment;->limit:I

    const/4 v0, 0x0

    iput v0, p0, Lokio/Segment;->pos:I

    .line 57
    sput-object p0, Lokio/SegmentPool;->next:Lokio/Segment;
    :try_end_37
    .catchall {:try_start_24 .. :try_end_37} :catchall_39

    .line 58
    monitor-exit v4

    goto :goto_3c

    :catchall_39
    move-exception v5

    monitor-exit v4

    throw v5

    .line 59
    :goto_3c
    return-void
.end method

.method static take()Lokio/Segment;
    .registers 7

    .line 37
    const-class v4, Lokio/SegmentPool;

    monitor-enter v4

    .line 38
    :try_start_3
    sget-object v0, Lokio/SegmentPool;->next:Lokio/Segment;

    if-eqz v0, :cond_19

    .line 39
    sget-object v5, Lokio/SegmentPool;->next:Lokio/Segment;

    .line 40
    .local v5, "result":Lokio/Segment;
    iget-object v0, v5, Lokio/Segment;->next:Lokio/Segment;

    sput-object v0, Lokio/SegmentPool;->next:Lokio/Segment;

    .line 41
    const/4 v0, 0x0

    iput-object v0, v5, Lokio/Segment;->next:Lokio/Segment;

    .line 42
    sget-wide v0, Lokio/SegmentPool;->byteCount:J

    const-wide/16 v2, 0x2000

    sub-long/2addr v0, v2

    sput-wide v0, Lokio/SegmentPool;->byteCount:J
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_1b

    .line 43
    monitor-exit v4

    return-object v5

    .line 45
    .end local v5    # "result":Lokio/Segment;
    :cond_19
    monitor-exit v4

    goto :goto_1e

    :catchall_1b
    move-exception v6

    monitor-exit v4

    throw v6

    .line 46
    :goto_1e
    new-instance v0, Lokio/Segment;

    invoke-direct {v0}, Lokio/Segment;-><init>()V

    return-object v0
.end method

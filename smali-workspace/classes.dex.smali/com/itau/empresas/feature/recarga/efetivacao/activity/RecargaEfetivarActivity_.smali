.class public final Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;
.super Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;
.source "RecargaEfetivarActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 41
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;-><init>()V

    .line 45
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;

    .line 41
    invoke-super {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;

    .line 41
    invoke-super {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 63
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 64
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 65
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 66
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 67
    invoke-static {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->recargaController:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    .line 68
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->injectExtras_()V

    .line 69
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->afterInject()V

    .line 70
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 148
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 149
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_70

    .line 150
    const-string v0, "nome"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 151
    const-string v0, "nome"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->nome:Ljava/lang/String;

    .line 153
    :cond_1a
    const-string v0, "aviso"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 154
    const-string v0, "aviso"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->aviso:Ljava/lang/String;

    .line 156
    :cond_2a
    const-string v0, "resultadoSimulacaoRecargaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 157
    const-string v0, "resultadoSimulacaoRecargaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->resultadoSimulacaoRecargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;

    .line 159
    :cond_3c
    const-string v0, "novo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 160
    const-string v0, "novo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->novo:Z

    .line 162
    :cond_4c
    const-string v0, "recargaInputVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5e

    .line 163
    const-string v0, "recargaInputVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    .line 165
    :cond_5e
    const-string v0, "limite"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_70

    .line 166
    const-string v0, "limite"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->limite:Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

    .line 169
    :cond_70
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 194
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$4;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 202
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 182
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$3;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 190
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 56
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->init_(Landroid/os/Bundle;)V

    .line 57
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 59
    const v0, 0x7f03005f

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->setContentView(I)V

    .line 60
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 5
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 104
    const v0, 0x7f0e02c8

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->root:Landroid/widget/LinearLayout;

    .line 105
    const v0, 0x7f0e02d4

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->llValor:Landroid/widget/LinearLayout;

    .line 106
    const v0, 0x7f0e02d6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->llValorBonus:Landroid/widget/LinearLayout;

    .line 107
    const v0, 0x7f0e02cc

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->textoOrigemNome:Landroid/widget/TextView;

    .line 108
    const v0, 0x7f0e02cd

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->textoOrigemAgencia:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f0e02ce

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->textoOrigemConta:Landroid/widget/TextView;

    .line 110
    const v0, 0x7f0e02cf

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->textoOrigemOperador:Landroid/widget/TextView;

    .line 111
    const v0, 0x7f0e02d1

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->textoDestinoNome:Landroid/widget/TextView;

    .line 112
    const v0, 0x7f0e02d2

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->textoDestinoTelefone:Landroid/widget/TextView;

    .line 113
    const v0, 0x7f0e02d3

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->textoDestinoLocal:Landroid/widget/TextView;

    .line 114
    const v0, 0x7f0e02d8

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->textoAgContaDebitada:Landroid/widget/TextView;

    .line 115
    const v0, 0x7f0e02ca

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->textoRecargaOperadora:Landroid/widget/TextView;

    .line 116
    const v0, 0x7f0e02d5

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->textoValor:Landroid/widget/TextView;

    .line 117
    const v0, 0x7f0e02d7

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->textoValorBonus:Landroid/widget/TextView;

    .line 118
    const v0, 0x7f0e02da

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->botaoInterrogacao:Landroid/widget/ImageButton;

    .line 119
    const v0, 0x7f0e02d9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->campoIdentificacaoComprovante:Landroid/widget/TextView;

    .line 120
    const v0, 0x7f0e02b0

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/WarningView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->warning:Lcom/itau/empresas/ui/view/WarningView;

    .line 121
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 122
    const v0, 0x7f0e018f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 124
    .local v2, "view_botao_continuar":Landroid/view/View;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->botaoInterrogacao:Landroid/widget/ImageButton;

    if-eqz v0, :cond_db

    .line 125
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->botaoInterrogacao:Landroid/widget/ImageButton;

    new-instance v1, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$1;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    :cond_db
    if-eqz v2, :cond_e5

    .line 135
    new-instance v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$2;-><init>(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    :cond_e5
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->carregaElementosDaTela()V

    .line 145
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 74
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->setContentView(I)V

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 76
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 86
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->setContentView(Landroid/view/View;)V

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 88
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 80
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 82
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 173
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->setIntent(Landroid/content/Intent;)V

    .line 174
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_;->injectExtras_()V

    .line 175
    return-void
.end method

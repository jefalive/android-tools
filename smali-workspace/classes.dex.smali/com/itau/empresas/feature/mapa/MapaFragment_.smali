.class public final Lcom/itau/empresas/feature/mapa/MapaFragment_;
.super Lcom/itau/empresas/feature/mapa/MapaFragment;
.source "MapaFragment_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/mapa/MapaFragment_$FragmentBuilder_;
    }
.end annotation


# instance fields
.field private contentView_:Landroid/view/View;

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 27
    invoke-direct {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;-><init>()V

    .line 31
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method public static builder()Lcom/itau/empresas/feature/mapa/MapaFragment_$FragmentBuilder_;
    .registers 1

    .line 91
    new-instance v0, Lcom/itau/empresas/feature/mapa/MapaFragment_$FragmentBuilder_;

    invoke-direct {v0}, Lcom/itau/empresas/feature/mapa/MapaFragment_$FragmentBuilder_;-><init>()V

    return-object v0
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 80
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 81
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->application:Lcom/itau/empresas/CustomApplication;

    .line 82
    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .registers 3
    .param p1, "id"    # I

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_6

    .line 45
    const/4 v0, 0x0

    return-object v0

    .line 47
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->contentView_:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 37
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/mapa/MapaFragment_;->init_(Landroid/os/Bundle;)V

    .line 38
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/mapa/MapaFragment;->onCreate(Landroid/os/Bundle;)V

    .line 39
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 40
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 52
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/feature/mapa/MapaFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->contentView_:Landroid/view/View;

    .line 53
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_14

    .line 54
    const v0, 0x7f0300b7

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->contentView_:Landroid/view/View;

    .line 56
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->contentView_:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .registers 2

    .line 61
    invoke-super {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->onDestroyView()V

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->contentView_:Landroid/view/View;

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->mapaView:Lcom/google/android/gms/maps/MapView;

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->slidingUpPanelMapa:Lcom/sothree/slidinguppanel/SlidingUpPanelLayout;

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->textoNomeAgencia:Landroid/widget/TextView;

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->textoEnderecoAgencia:Landroid/widget/TextView;

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->textoBairroAgencia:Landroid/widget/TextView;

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->textoTelefone:Landroid/widget/TextView;

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->textoHorarioAtendimento:Landroid/widget/TextView;

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->textoServicos:Landroid/widget/TextView;

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->textoAtendimentoEmpresas:Landroid/widget/TextView;

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->imgCaixaExclusivo:Landroid/widget/ImageView;

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->imgHorarioDiferente:Landroid/widget/ImageView;

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->llIconesServicos:Landroid/widget/LinearLayout;

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->llMapaTitulo:Landroid/widget/LinearLayout;

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->linearMapDragView:Landroid/widget/LinearLayout;

    .line 77
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 115
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 116
    .local v1, "itemId_":I
    const v0, 0x7f0e06c5

    if-ne v1, v0, :cond_e

    .line 117
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment_;->menuFilter()V

    .line 118
    const/4 v0, 0x1

    return v0

    .line 120
    :cond_e
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/mapa/MapaFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 96
    const v0, 0x7f0e043e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/MapView;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->mapaView:Lcom/google/android/gms/maps/MapView;

    .line 97
    const v0, 0x7f0e043c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sothree/slidinguppanel/SlidingUpPanelLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->slidingUpPanelMapa:Lcom/sothree/slidinguppanel/SlidingUpPanelLayout;

    .line 98
    const v0, 0x7f0e0441

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->textoNomeAgencia:Landroid/widget/TextView;

    .line 99
    const v0, 0x7f0e0442

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->textoEnderecoAgencia:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0e0443

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->textoBairroAgencia:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0e0444

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->textoTelefone:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f0e0445

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->textoHorarioAtendimento:Landroid/widget/TextView;

    .line 103
    const v0, 0x7f0e0446

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->textoServicos:Landroid/widget/TextView;

    .line 104
    const v0, 0x7f0e044a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->textoAtendimentoEmpresas:Landroid/widget/TextView;

    .line 105
    const v0, 0x7f0e0448

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->imgCaixaExclusivo:Landroid/widget/ImageView;

    .line 106
    const v0, 0x7f0e0449

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->imgHorarioDiferente:Landroid/widget/ImageView;

    .line 107
    const v0, 0x7f0e0447

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->llIconesServicos:Landroid/widget/LinearLayout;

    .line 108
    const v0, 0x7f0e0440

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->llMapaTitulo:Landroid/widget/LinearLayout;

    .line 109
    const v0, 0x7f0e043d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->linearMapDragView:Landroid/widget/LinearLayout;

    .line 110
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaFragment_;->carregar()V

    .line 111
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 86
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/mapa/MapaFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 88
    return-void
.end method

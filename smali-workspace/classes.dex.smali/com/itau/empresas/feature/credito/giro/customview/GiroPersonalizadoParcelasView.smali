.class public Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;
.super Landroid/widget/RelativeLayout;
.source "GiroPersonalizadoParcelasView.java"


# instance fields
.field listaValoresPersonalizados:Landroid/widget/ListView;

.field llContainer:Landroid/widget/LinearLayout;

.field parcelasAdapter:Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 48
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 52
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 56
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    return-void
.end method

.method private recalculateHeight(Z)V
    .registers 4
    .param p1, "hasItems"    # Z

    .line 147
    if-eqz p1, :cond_f

    .line 148
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->listaValoresPersonalizados:Landroid/widget/ListView;

    invoke-static {v1}, Lcom/itau/empresas/ui/util/ViewUtils;->calculateHeight(Landroid/widget/ListView;)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_1b

    .line 150
    :cond_f
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->listaValoresPersonalizados:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 152
    :goto_1b
    return-void
.end method


# virtual methods
.method public addItem(JF)V
    .registers 8
    .param p1, "quantidade"    # J
    .param p3, "valor"    # F

    .line 126
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->parcelasAdapter:Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;

    if-eqz v0, :cond_3c

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->parcelasAdapter:Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->getItems()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3c

    .line 129
    new-instance v3, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;

    invoke-direct {v3}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;-><init>()V

    .line 130
    .local v3, "valoresEmprestimoVO":Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;
    invoke-virtual {v3, p1, p2}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->setQuantidade(J)V

    .line 131
    invoke-virtual {v3, p3}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->setValor(F)V

    .line 132
    sget-object v0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;->CARREGADO_MANUALMENTE:Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->setEnumValoresEmprestimo(Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;)V

    .line 134
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->parcelasAdapter:Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->getItems()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->parcelasAdapter:Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;

    .line 135
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 136
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->listaValoresPersonalizados:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->parcelasAdapter:Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 137
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->recalculateHeight(Z)V

    .line 139
    .end local v3    # "valoresEmprestimoVO":Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;
    :cond_3c
    return-void
.end method

.method public clearData()V
    .registers 4

    .line 96
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->listaValoresPersonalizados:Landroid/widget/ListView;

    if-eqz v0, :cond_15

    .line 98
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->listaValoresPersonalizados:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 99
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->listaValoresPersonalizados:Landroid/widget/ListView;

    const/4 v1, -0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 100
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->recalculateHeight(Z)V

    .line 102
    :cond_15
    return-void
.end method

.method public getSelectedItem()Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;
    .registers 3

    .line 114
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->listaValoresPersonalizados:Landroid/widget/ListView;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->listaValoresPersonalizados:Landroid/widget/ListView;

    .line 115
    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    if-ltz v0, :cond_1b

    .line 117
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->listaValoresPersonalizados:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->listaValoresPersonalizados:Landroid/widget/ListView;

    .line 118
    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;

    .line 117
    return-object v0

    .line 121
    :cond_1b
    const/4 v0, 0x0

    return-object v0
.end method

.method public removeSelection()V
    .registers 4

    .line 106
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->listaValoresPersonalizados:Landroid/widget/ListView;

    if-eqz v0, :cond_b

    .line 108
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->listaValoresPersonalizados:Landroid/widget/ListView;

    const/4 v1, -0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 110
    :cond_b
    return-void
.end method

.method public setData(Landroid/content/Context;Ljava/util/List;JJ)V
    .registers 16
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "parcelasEmprestimoLista"    # Ljava/util/List;
    .param p3, "quantidadeMinimaMes"    # J
    .param p5, "quantidadeMaximaMes"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;>;JJ)V"
        }
    .end annotation

    .line 72
    move-object v6, p2

    .line 76
    .local v6, "listaParcelasEmprestimo":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;>;"
    if-eqz v6, :cond_1e

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-le v0, v1, :cond_1e

    .line 78
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 80
    .local v7, "listaTemporaria":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;>;"
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_10
    const/4 v0, 0x3

    if-ge v8, v0, :cond_1d

    .line 82
    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    add-int/lit8 v8, v8, 0x1

    goto :goto_10

    .line 85
    .end local v8    # "i":I
    :cond_1d
    move-object v6, v7

    .line 88
    .end local v7    # "listaTemporaria":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;>;"
    .end local v7
    :cond_1e
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->parcelasAdapter:Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->setContext(Landroid/content/Context;)V

    .line 89
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->parcelasAdapter:Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;

    move-object v1, v6

    move-wide v2, p3

    move-wide v4, p5

    invoke-virtual/range {v0 .. v5}, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->setData(Ljava/util/List;JJ)V

    .line 90
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->listaValoresPersonalizados:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->parcelasAdapter:Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 91
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/giro/customview/GiroPersonalizadoParcelasView;->recalculateHeight(Z)V

    .line 92
    return-void
.end method

.class public Lcom/itau/empresas/feature/home/view/ICardSimplesView;
.super Landroid/widget/FrameLayout;
.source "ICardSimplesView.java"


# instance fields
.field imagemCardSimples:Lcom/itau/empresas/ui/view/TextViewIcon;

.field tituloCardSimples:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 24
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method


# virtual methods
.method public setaIcone(Ljava/lang/String;)V
    .registers 3
    .param p1, "textoFonte"    # Ljava/lang/String;

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardSimplesView;->imagemCardSimples:Lcom/itau/empresas/ui/view/TextViewIcon;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/ui/view/TextViewIcon;->setText(Ljava/lang/CharSequence;)V

    .line 37
    return-void
.end method

.method public setaTitulo(Ljava/lang/String;)V
    .registers 3
    .param p1, "tituloCard"    # Ljava/lang/String;

    .line 40
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardSimplesView;->tituloCardSimples:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    return-void
.end method

.class public Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "AtendimentoAlertasActivity.java"


# instance fields
.field private opkeys:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field root:Landroid/widget/LinearLayout;

.field textoDetalheAlerta:Landroid/widget/TextView;

.field textoTitulo:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 26
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 92
    new-instance v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method carregaElementosDeTela()V
    .registers 5

    .line 42
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 43
    .local v3, "bundle":Landroid/os/Bundle;
    invoke-virtual {v3}, Landroid/os/Bundle;->size()I

    move-result v0

    if-nez v0, :cond_20

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->root:Landroid/widget/LinearLayout;

    const v1, 0x7f0701ef

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 46
    return-void

    .line 49
    :cond_20
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->textoTitulo:Landroid/widget/TextView;

    .line 51
    const v1, 0x7f0706fd

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/itau/empresas/ui/util/AtendimentoUtil;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 50
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->textoDetalheAlerta:Landroid/widget/TextView;

    .line 56
    const v1, 0x7f07070d

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 55
    invoke-static {v1}, Lcom/itau/empresas/ui/util/AtendimentoUtil;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 54
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    return-void
.end method

.method fechaAlerta(Landroid/view/View;)V
    .registers 2
    .param p1, "view"    # Landroid/view/View;

    .line 36
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->finish()V

    .line 37
    return-void
.end method

.method public hitAnalytics()Ljava/lang/String;
    .registers 2

    .line 80
    const-string v0, ""

    return-object v0
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;

    .line 60
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 61
    return-void

    .line 63
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->escondeDialogoDeProgresso()V

    .line 64
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->root:Landroid/widget/LinearLayout;

    const v1, 0x7f0701ef

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 66
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 69
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 70
    return-void

    .line 72
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->escondeDialogoDeProgresso()V

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 75
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 85
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->opkeys:Ljava/util/List;

    if-nez v0, :cond_b

    .line 86
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->opkeys:Ljava/util/List;

    .line 88
    :cond_b
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAlertasActivity;->opkeys:Ljava/util/List;

    return-object v0
.end method

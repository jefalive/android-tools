.class public final enum Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;
.super Ljava/lang/Enum;
.source "EmprestimosParceladosView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TipoOfertaEspecial"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

.field public static final enum GIRO_AVAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

.field public static final enum GIRO_CARTOES:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

.field public static final enum GIRO_CARTOES_COM_GIRO_AVAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 83
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    const-string v1, "GIRO_AVAL"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->GIRO_AVAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    .line 84
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    const-string v1, "GIRO_CARTOES"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->GIRO_CARTOES:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    .line 85
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    const-string v1, "GIRO_CARTOES_COM_GIRO_AVAL"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->GIRO_CARTOES_COM_GIRO_AVAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    .line 82
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->GIRO_AVAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->GIRO_CARTOES:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->GIRO_CARTOES_COM_GIRO_AVAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->$VALUES:[Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 82
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 82
    const-class v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    return-object v0
.end method

.method public static values()[Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;
    .registers 1

    .line 82
    sget-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->$VALUES:[Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    invoke-virtual {v0}, [Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosView$TipoOfertaEspecial;

    return-object v0
.end method

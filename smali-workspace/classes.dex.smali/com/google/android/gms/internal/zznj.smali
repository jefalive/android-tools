.class public Lcom/google/android/gms/internal/zznj;
.super Ljava/lang/Object;


# static fields
.field private static final zzaol:Ljava/lang/reflect/Method;

.field private static final zzaom:Ljava/lang/reflect/Method;

.field private static final zzaon:Ljava/lang/reflect/Method;

.field private static final zzaoo:Ljava/lang/reflect/Method;

.field private static final zzaop:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    invoke-static {}, Lcom/google/android/gms/internal/zznj;->zzsp()Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zznj;->zzaol:Ljava/lang/reflect/Method;

    invoke-static {}, Lcom/google/android/gms/internal/zznj;->zzsq()Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zznj;->zzaom:Ljava/lang/reflect/Method;

    invoke-static {}, Lcom/google/android/gms/internal/zznj;->zzsr()Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zznj;->zzaon:Ljava/lang/reflect/Method;

    invoke-static {}, Lcom/google/android/gms/internal/zznj;->zzss()Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zznj;->zzaoo:Ljava/lang/reflect/Method;

    invoke-static {}, Lcom/google/android/gms/internal/zznj;->zzst()Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zznj;->zzaop:Ljava/lang/reflect/Method;

    return-void
.end method

.method public static zza(Landroid/os/WorkSource;)I
    .registers 4

    sget-object v0, Lcom/google/android/gms/internal/zznj;->zzaon:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_1c

    :try_start_4
    sget-object v0, Lcom/google/android/gms/internal/zznj;->zzaon:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_12} :catch_14

    move-result v0

    return v0

    :catch_14
    move-exception v2

    const-string v0, "WorkSourceUtil"

    const-string v1, "Unable to assign blame through WorkSource"

    invoke-static {v0, v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1c
    const/4 v0, 0x0

    return v0
.end method

.method public static zza(Landroid/os/WorkSource;I)Ljava/lang/String;
    .registers 7

    sget-object v0, Lcom/google/android/gms/internal/zznj;->zzaop:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_1f

    :try_start_4
    sget-object v0, Lcom/google/android/gms/internal/zznj;->zzaop:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_16} :catch_17

    return-object v0

    :catch_17
    move-exception v4

    const-string v0, "WorkSourceUtil"

    const-string v1, "Unable to assign blame through WorkSource"

    invoke-static {v0, v1, v4}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1f
    const/4 v0, 0x0

    return-object v0
.end method

.method public static zza(Landroid/os/WorkSource;ILjava/lang/String;)V
    .registers 8

    sget-object v0, Lcom/google/android/gms/internal/zznj;->zzaom:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_24

    if-nez p2, :cond_8

    const-string p2, ""

    :cond_8
    :try_start_8
    sget-object v0, Lcom/google/android/gms/internal/zznj;->zzaom:Ljava/lang/reflect/Method;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_1a} :catch_1b

    goto :goto_23

    :catch_1b
    move-exception v4

    const-string v0, "WorkSourceUtil"

    const-string v1, "Unable to assign blame through WorkSource"

    invoke-static {v0, v1, v4}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_23
    return-void

    :cond_24
    sget-object v0, Lcom/google/android/gms/internal/zznj;->zzaol:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_40

    :try_start_28
    sget-object v0, Lcom/google/android/gms/internal/zznj;->zzaol:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_37
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_37} :catch_38

    goto :goto_40

    :catch_38
    move-exception v4

    const-string v0, "WorkSourceUtil"

    const-string v1, "Unable to assign blame through WorkSource"

    invoke-static {v0, v1, v4}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_40
    :goto_40
    return-void
.end method

.method public static zzaA(Landroid/content/Context;)Z
    .registers 5

    if-nez p0, :cond_4

    const/4 v0, 0x0

    return v0

    :cond_4
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    if-nez v2, :cond_c

    const/4 v0, 0x0

    return v0

    :cond_c
    const-string v0, "android.permission.UPDATE_DEVICE_STATS"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1a

    const/4 v0, 0x1

    goto :goto_1b

    :cond_1a
    const/4 v0, 0x0

    :goto_1b
    return v0
.end method

.method public static zzb(Landroid/os/WorkSource;)Ljava/util/List;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/os/WorkSource;)Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    if-nez p0, :cond_4

    const/4 v1, 0x0

    goto :goto_8

    :cond_4
    invoke-static {p0}, Lcom/google/android/gms/internal/zznj;->zza(Landroid/os/WorkSource;)I

    move-result v1

    :goto_8
    if-nez v1, :cond_d

    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    return-object v0

    :cond_d
    const/4 v2, 0x0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    :goto_14
    if-ge v4, v1, :cond_26

    invoke-static {p0, v4}, Lcom/google/android/gms/internal/zznj;->zza(Landroid/os/WorkSource;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/internal/zzni;->zzcV(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_23

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_23
    add-int/lit8 v4, v4, 0x1

    goto :goto_14

    :cond_26
    return-object v3
.end method

.method public static zzf(ILjava/lang/String;)Landroid/os/WorkSource;
    .registers 3

    new-instance v0, Landroid/os/WorkSource;

    invoke-direct {v0}, Landroid/os/WorkSource;-><init>()V

    invoke-static {v0, p0, p1}, Lcom/google/android/gms/internal/zznj;->zza(Landroid/os/WorkSource;ILjava/lang/String;)V

    return-object v0
.end method

.method public static zzl(Landroid/content/Context;Ljava/lang/String;)Landroid/os/WorkSource;
    .registers 7

    if-eqz p0, :cond_8

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    if-nez v0, :cond_a

    :cond_8
    const/4 v0, 0x0

    return-object v0

    :cond_a
    const/4 v3, 0x0

    :try_start_b
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_13
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_b .. :try_end_13} :catch_15

    move-result-object v3

    goto :goto_30

    :catch_15
    move-exception v4

    const-string v0, "WorkSourceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not find package: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0

    :goto_30
    if-nez v3, :cond_4c

    const-string v0, "WorkSourceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not get applicationInfo from package: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0

    :cond_4c
    iget v0, v3, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-static {v0, p1}, Lcom/google/android/gms/internal/zznj;->zzf(ILjava/lang/String;)Landroid/os/WorkSource;

    move-result-object v0

    return-object v0
.end method

.method private static zzsp()Ljava/lang/reflect/Method;
    .registers 7

    const/4 v5, 0x0

    :try_start_1
    const-class v0, Landroid/os/WorkSource;

    const-string v1, "add"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_10} :catch_13

    move-result-object v0

    move-object v5, v0

    goto :goto_14

    :catch_13
    move-exception v6

    :goto_14
    return-object v5
.end method

.method private static zzsq()Ljava/lang/reflect/Method;
    .registers 7

    const/4 v5, 0x0

    invoke-static {}, Lcom/google/android/gms/internal/zzne;->zzsj()Z

    move-result v0

    if-eqz v0, :cond_1f

    :try_start_7
    const-class v0, Landroid/os/WorkSource;

    const-string v1, "add"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_1b} :catch_1e

    move-result-object v0

    move-object v5, v0

    goto :goto_1f

    :catch_1e
    move-exception v6

    :cond_1f
    :goto_1f
    return-object v5
.end method

.method private static zzsr()Ljava/lang/reflect/Method;
    .registers 5

    const/4 v3, 0x0

    :try_start_1
    const-class v0, Landroid/os/WorkSource;

    const-string v1, "size"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_b} :catch_e

    move-result-object v0

    move-object v3, v0

    goto :goto_f

    :catch_e
    move-exception v4

    :goto_f
    return-object v3
.end method

.method private static zzss()Ljava/lang/reflect/Method;
    .registers 7

    const/4 v5, 0x0

    :try_start_1
    const-class v0, Landroid/os/WorkSource;

    const-string v1, "get"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_10} :catch_13

    move-result-object v0

    move-object v5, v0

    goto :goto_14

    :catch_13
    move-exception v6

    :goto_14
    return-object v5
.end method

.method private static zzst()Ljava/lang/reflect/Method;
    .registers 7

    const/4 v5, 0x0

    invoke-static {}, Lcom/google/android/gms/internal/zzne;->zzsj()Z

    move-result v0

    if-eqz v0, :cond_1a

    :try_start_7
    const-class v0, Landroid/os/WorkSource;

    const-string v1, "getName"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_16} :catch_19

    move-result-object v0

    move-object v5, v0

    goto :goto_1a

    :catch_19
    move-exception v6

    :cond_1a
    :goto_1a
    return-object v5
.end method

.class public final Lbr/com/itau/textovoz/util/DisplayUtils;
.super Ljava/lang/Object;
.source "DisplayUtils.java"


# direct methods
.method public static getDensity(Landroid/app/Activity;)Ljava/lang/String;
    .registers 3
    .param p0, "activity"    # Landroid/app/Activity;

    .line 13
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 14
    .local v1, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 15
    iget v0, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    sparse-switch v0, :sswitch_data_28

    goto :goto_25

    .line 17
    :sswitch_16
    const-string v0, "ldpi/"

    return-object v0

    .line 19
    :sswitch_19
    const-string v0, "mdpi/"

    return-object v0

    .line 21
    :sswitch_1c
    const-string v0, "hdpi/"

    return-object v0

    .line 23
    :sswitch_1f
    const-string v0, "xhdpi/"

    return-object v0

    .line 25
    :sswitch_22
    const-string v0, "xxhdpi/"

    return-object v0

    .line 27
    :goto_25
    const-string v0, "xxxhdpi/"

    return-object v0

    :sswitch_data_28
    .sparse-switch
        0x78 -> :sswitch_16
        0xa0 -> :sswitch_19
        0xf0 -> :sswitch_1c
        0x140 -> :sswitch_1f
        0x1e0 -> :sswitch_22
    .end sparse-switch
.end method

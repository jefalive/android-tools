.class public final Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;
.super Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;
.source "AtendimentoRespostaFaqActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 39
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;-><init>()V

    .line 43
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;

    .line 39
    invoke-super {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;

    .line 39
    invoke-super {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 55
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 56
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 57
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 58
    invoke-static {p0}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->controller:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

    .line 59
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->afterInject()V

    .line 60
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 144
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$4;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 152
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 132
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$3;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 140
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 48
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->init_(Landroid/os/Bundle;)V

    .line 49
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 51
    const v0, 0x7f030021

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->setContentView(I)V

    .line 52
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 94
    const v0, 0x7f0e00d3

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->root:Landroid/widget/RelativeLayout;

    .line 95
    const v0, 0x7f0e05fa

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->textoAtendimentoPergunta:Landroid/widget/TextView;

    .line 96
    const v0, 0x7f0e05fb

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->textoAtendimentoResposta:Landroid/widget/TextView;

    .line 97
    const v0, 0x7f0e05e8

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->listaCanais:Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;

    .line 98
    const v0, 0x7f0e00d4

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->svDetalheFaq:Landroid/widget/ScrollView;

    .line 99
    const v0, 0x7f0e00d8

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/widgets/NonScrollListView;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->listaFaqsRelacionada:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    .line 100
    const v0, 0x7f0e00d7

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/CardView;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->cartaoFeedBack:Landroid/support/v7/widget/CardView;

    .line 101
    const v0, 0x7f0e05ef

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/TextViewIcon;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->textoIconCurtir:Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 102
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 103
    const v0, 0x7f0e05f0

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/TextViewIcon;

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->textoIconDescurtir:Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 104
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->textoIconCurtir:Lcom/itau/empresas/ui/view/TextViewIcon;

    if-eqz v0, :cond_7c

    .line 105
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->textoIconCurtir:Lcom/itau/empresas/ui/view/TextViewIcon;

    new-instance v1, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$1;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/TextViewIcon;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    :cond_7c
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->textoIconDescurtir:Lcom/itau/empresas/ui/view/TextViewIcon;

    if-eqz v0, :cond_8a

    .line 115
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->textoIconDescurtir:Lcom/itau/empresas/ui/view/TextViewIcon;

    new-instance v1, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$2;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/TextViewIcon;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    :cond_8a
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->carregandoElementosDeTela()V

    .line 125
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 64
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->setContentView(I)V

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 66
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 76
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->setContentView(Landroid/view/View;)V

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 78
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 70
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 72
    return-void
.end method

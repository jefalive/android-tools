.class public Lcom/itau/empresas/api/model/LancamentoVO;
.super Ljava/lang/Object;
.source "LancamentoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private agenciaLancamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia_lancamento"
    .end annotation
.end field

.field private agendado:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agendado"
    .end annotation
.end field

.field private categoriaLancamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "categoria_lancamento"
    .end annotation
.end field

.field private dataLancamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_lancamento"
    .end annotation
.end field

.field private descricaoLancamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_lancamento"
    .end annotation
.end field

.field private detalheLancamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "detalhe_lancamento"
    .end annotation
.end field

.field private festejar:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "festejar"
    .end annotation
.end field

.field private indicadorOperacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_operacao"
    .end annotation
.end field

.field private legenda:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "legenda"
    .end annotation
.end field

.field private moeda:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "moeda"
    .end annotation
.end field

.field private saldoDia:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "saldo_dia"
    .end annotation
.end field

.field private valorLancamento:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_lancamento"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCategoriaLancamento()Ljava/lang/String;
    .registers 2

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/api/model/LancamentoVO;->categoriaLancamento:Ljava/lang/String;

    return-object v0
.end method

.method public getDataLancamento()Ljava/lang/String;
    .registers 2

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/api/model/LancamentoVO;->dataLancamento:Ljava/lang/String;

    return-object v0
.end method

.method public getDescricaoLancamento()Ljava/lang/String;
    .registers 2

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/api/model/LancamentoVO;->descricaoLancamento:Ljava/lang/String;

    return-object v0
.end method

.method public getIndicadorOperacao()Ljava/lang/String;
    .registers 2

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/api/model/LancamentoVO;->indicadorOperacao:Ljava/lang/String;

    return-object v0
.end method

.method public getValorLancamento()Ljava/math/BigDecimal;
    .registers 2

    .line 57
    iget-object v0, p0, Lcom/itau/empresas/api/model/LancamentoVO;->valorLancamento:Ljava/math/BigDecimal;

    return-object v0
.end method

.class Lcom/google/android/gms/analytics/internal/zzah$zza;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/analytics/internal/zzah;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "zza"
.end annotation


# instance fields
.field private zzTe:I

.field private zzTf:Ljava/io/ByteArrayOutputStream;

.field final synthetic zzTg:Lcom/google/android/gms/analytics/internal/zzah;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/analytics/internal/zzah;)V
    .registers 3

    iput-object p1, p0, Lcom/google/android/gms/analytics/internal/zzah$zza;->zzTg:Lcom/google/android/gms/analytics/internal/zzah;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zzah$zza;->zzTf:Ljava/io/ByteArrayOutputStream;

    return-void
.end method


# virtual methods
.method public getPayload()[B
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzah$zza;->zzTf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public zzj(Lcom/google/android/gms/analytics/internal/zzab;)Z
    .registers 8

    invoke-static {p1}, Lcom/google/android/gms/common/internal/zzx;->zzz(Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, p0, Lcom/google/android/gms/analytics/internal/zzah$zza;->zzTe:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/analytics/internal/zzah$zza;->zzTg:Lcom/google/android/gms/analytics/internal/zzah;

    invoke-virtual {v1}, Lcom/google/android/gms/analytics/internal/zzah;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/analytics/internal/zzr;->zzkD()I

    move-result v1

    if-le v0, v1, :cond_15

    const/4 v0, 0x0

    return v0

    :cond_15
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzah$zza;->zzTg:Lcom/google/android/gms/analytics/internal/zzah;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/analytics/internal/zzah;->zza(Lcom/google/android/gms/analytics/internal/zzab;Z)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2b

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzah$zza;->zzTg:Lcom/google/android/gms/analytics/internal/zzah;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzah;->zzjm()Lcom/google/android/gms/analytics/internal/zzaf;

    move-result-object v0

    const-string v1, "Error formatting hit"

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/analytics/internal/zzaf;->zza(Lcom/google/android/gms/analytics/internal/zzab;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0

    :cond_2b
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    array-length v4, v3

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzah$zza;->zzTg:Lcom/google/android/gms/analytics/internal/zzah;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzah;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkv()I

    move-result v0

    if-le v4, v0, :cond_49

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzah$zza;->zzTg:Lcom/google/android/gms/analytics/internal/zzah;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzah;->zzjm()Lcom/google/android/gms/analytics/internal/zzaf;

    move-result-object v0

    const-string v1, "Hit size exceeds the maximum size limit"

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/analytics/internal/zzaf;->zza(Lcom/google/android/gms/analytics/internal/zzab;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0

    :cond_49
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzah$zza;->zzTf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v0

    if-lez v0, :cond_53

    add-int/lit8 v4, v4, 0x1

    :cond_53
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzah$zza;->zzTf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v0

    add-int/2addr v0, v4

    iget-object v1, p0, Lcom/google/android/gms/analytics/internal/zzah$zza;->zzTg:Lcom/google/android/gms/analytics/internal/zzah;

    invoke-virtual {v1}, Lcom/google/android/gms/analytics/internal/zzah;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/analytics/internal/zzr;->zzkx()I

    move-result v1

    if-le v0, v1, :cond_68

    const/4 v0, 0x0

    return v0

    :cond_68
    :try_start_68
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzah$zza;->zzTf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v0

    if-lez v0, :cond_79

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzah$zza;->zzTf:Ljava/io/ByteArrayOutputStream;

    invoke-static {}, Lcom/google/android/gms/analytics/internal/zzah;->zzlD()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    :cond_79
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzah$zza;->zzTf:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_7e
    .catch Ljava/io/IOException; {:try_start_68 .. :try_end_7e} :catch_7f

    goto :goto_89

    :catch_7f
    move-exception v5

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzah$zza;->zzTg:Lcom/google/android/gms/analytics/internal/zzah;

    const-string v1, "Failed to write payload when batching hits"

    invoke-virtual {v0, v1, v5}, Lcom/google/android/gms/analytics/internal/zzah;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v0, 0x1

    return v0

    :goto_89
    iget v0, p0, Lcom/google/android/gms/analytics/internal/zzah$zza;->zzTe:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/analytics/internal/zzah$zza;->zzTe:I

    const/4 v0, 0x1

    return v0
.end method

.method public zzlE()I
    .registers 2

    iget v0, p0, Lcom/google/android/gms/analytics/internal/zzah$zza;->zzTe:I

    return v0
.end method

.class public Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;
.super Landroid/support/v4/app/Fragment;
.source "FeedbackSearchFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$FeedbackOptionListener;
    }
.end annotation


# static fields
.field private static ARG_SEGMENT_KEY:Ljava/lang/String;


# instance fields
.field private btnNo:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

.field private btnYes:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

.field private linearFeedbackCollaboration:Landroid/widget/LinearLayout;

.field private linearFeedbackYesNoContainer:Landroid/widget/LinearLayout;

.field private listener:Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$FeedbackOptionListener;

.field private messageTextView:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

.field private segment:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 22
    const-string v0, "segment"

    sput-object v0, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->ARG_SEGMENT_KEY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;)Landroid/widget/LinearLayout;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;

    .line 21
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->linearFeedbackYesNoContainer:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$100(Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;)Landroid/widget/LinearLayout;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;

    .line 21
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->linearFeedbackCollaboration:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$200(Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;)Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$FeedbackOptionListener;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;

    .line 21
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->listener:Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$FeedbackOptionListener;

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;)Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;
    .registers 4
    .param p0, "segment"    # Ljava/lang/String;

    .line 46
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 47
    .local v1, "args":Landroid/os/Bundle;
    sget-object v0, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->ARG_SEGMENT_KEY:Ljava/lang/String;

    invoke-virtual {v1, v0, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    new-instance v2, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;

    invoke-direct {v2}, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;-><init>()V

    .line 50
    .local v2, "fragment":Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;
    invoke-virtual {v2, v1}, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->setArguments(Landroid/os/Bundle;)V

    .line 52
    return-object v2
.end method


# virtual methods
.method public onAttach(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 108
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 109
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 57
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 59
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->ARG_SEGMENT_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->segment:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 65
    sget v0, Lbr/com/itau/textovoz/R$layout;->fragment_search_feedback:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 67
    .local v3, "v":Landroid/view/View;
    sget v0, Lbr/com/itau/textovoz/R$id;->view_linear_feedback:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->linearFeedbackYesNoContainer:Landroid/widget/LinearLayout;

    .line 68
    sget v0, Lbr/com/itau/textovoz/R$id;->linear_feedback_collaboration:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->linearFeedbackCollaboration:Landroid/widget/LinearLayout;

    .line 70
    sget v0, Lbr/com/itau/textovoz/R$id;->btn_feedback_yes:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->btnYes:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    .line 71
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->btnYes:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    new-instance v1, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$1;

    invoke-direct {v1, p0}, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$1;-><init>(Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;)V

    invoke-virtual {v0, v1}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    sget v0, Lbr/com/itau/textovoz/R$id;->btn_feedback_no:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->btnNo:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    .line 84
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->btnNo:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    new-instance v1, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$2;

    invoke-direct {v1, p0}, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$2;-><init>(Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;)V

    invoke-virtual {v0, v1}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    sget v0, Lbr/com/itau/textovoz/R$id;->tv_custom_color_feedback:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->messageTextView:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    .line 97
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->segment:Ljava/lang/String;

    invoke-static {v0}, Lbr/com/itau/textovoz/util/CommonUtils;->getSegment(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v1, v0, :cond_66

    .line 98
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->messageTextView:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$color;->personnalite_theme_color:I

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setTextColor(I)V

    goto :goto_75

    .line 100
    :cond_66
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->messageTextView:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$color;->itau_theme_color:I

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setTextColor(I)V

    .line 103
    :goto_75
    return-object v3
.end method

.method public onDestroy()V
    .registers 1

    .line 118
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 119
    return-void
.end method

.method public onDetach()V
    .registers 1

    .line 113
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 114
    return-void
.end method

.method public setOptionListener(Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$FeedbackOptionListener;)V
    .registers 2
    .param p1, "listener"    # Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$FeedbackOptionListener;

    .line 42
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->listener:Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$FeedbackOptionListener;

    .line 43
    return-void
.end method

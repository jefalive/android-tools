.class public final Lbr/com/itau/sdk/android/core/endpoint/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbr/com/itau/sdk/android/core/endpoint/a;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Lbr/com/itau/sdk/android/core/endpoint/a<Ljava/lang/String;>;"
    }
.end annotation


# static fields
.field private static final a:Ljava/security/SecureRandom;

.field private static final j:[B

.field private static k:I


# instance fields
.field private final c:Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareCaller;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/google/gson/JsonElement;

.field private final f:I

.field private final g:I

.field private h:Ljava/lang/String;

.field private i:Lbr/com/itau/sdk/android/core/model/MiddlewareRequestDTO;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 22
    const/16 v0, 0x27

    new-array v0, v0, [B

    fill-array-data v0, :array_16

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/l;->j:[B

    const/16 v0, 0x28

    sput v0, Lbr/com/itau/sdk/android/core/endpoint/l;->k:I

    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/l;->a:Ljava/security/SecureRandom;

    return-void

    nop

    :array_16
    .array-data 1
        0x57t
        0x70t
        -0x4ft
        -0x23t
        -0x1t
        -0x11t
        0xdt
        0x6t
        -0x2t
        0x1ct
        -0x1bt
        -0x1et
        0x11t
        -0xdt
        -0x5t
        0x12t
        -0x2t
        0x2t
        -0xet
        0x3dt
        0xft
        -0x11t
        0x11t
        -0xct
        0x5t
        0x27t
        0x18t
        -0x4et
        0x4dt
        -0x49t
        -0x1t
        0x33t
        0x18t
        -0x4et
        0x2t
        0xet
        -0xdt
        0x29t
        -0x1bt
    .end array-data
.end method

.method public constructor <init>(Lbr/com/itau/sdk/android/core/model/MiddlewareKeyValue;[Ljava/lang/Object;Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareCaller;II)V
    .registers 12

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/l;->j:[B

    const/16 v1, 0x1a

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/l;->j:[B

    const/16 v2, 0x18

    aget-byte v1, v1, v2

    add-int/lit8 v2, v1, -0x5

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/l;->a(III)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->d:Ljava/lang/String;

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->e:Lcom/google/gson/JsonElement;

    .line 72
    iput-object p3, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->c:Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareCaller;

    .line 74
    iput p4, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->f:I

    .line 75
    iput p5, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->g:I

    .line 77
    sget v0, Lbr/com/itau/sdk/android/core/endpoint/l;->k:I

    ushr-int/lit8 v0, v0, 0x2

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/l;->j:[B

    const/4 v2, 0x7

    aget-byte v1, v1, v2

    const/16 v2, 0x15

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/l;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/MiddlewareKeyValue;->getId()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/MiddlewareKeyValue;->getOp()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 78
    invoke-direct {p0, p2, v4}, Lbr/com/itau/sdk/android/core/endpoint/l;->a([Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 80
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/endpoint/l;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->h:Ljava/lang/String;

    .line 81
    new-instance v0, Lbr/com/itau/sdk/android/core/model/MiddlewareRequestDTO;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->h:Ljava/lang/String;

    invoke-static {}, Lbr/com/itau/security/middlewarechannel/Utilities;->getDeviceID()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, v5}, Lbr/com/itau/sdk/android/core/model/MiddlewareRequestDTO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->i:Lbr/com/itau/sdk/android/core/model/MiddlewareRequestDTO;

    .line 82
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Object;Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareCaller;II)V
    .registers 11

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->d:Ljava/lang/String;

    .line 43
    iput-object p3, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->c:Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareCaller;

    .line 45
    iput p4, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->f:I

    .line 46
    iput p5, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->g:I

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->i:Lbr/com/itau/sdk/android/core/model/MiddlewareRequestDTO;

    .line 50
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->a:Lcom/google/gson/Gson;

    if-eqz p2, :cond_14

    move-object v1, p2

    goto :goto_19

    :cond_14
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    :goto_19
    invoke-virtual {v0, v1}, Lcom/google/gson/Gson;->toJsonTree(Ljava/lang/Object;)Lcom/google/gson/JsonElement;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->e:Lcom/google/gson/JsonElement;

    .line 52
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->e:Lcom/google/gson/JsonElement;

    invoke-virtual {v0}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/l;->j:[B

    const/4 v2, 0x4

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, 0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/l;->j:[B

    const/4 v3, 0x4

    aget-byte v2, v2, v3

    neg-int v2, v2

    or-int/lit8 v3, v2, 0x20

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/l;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_45

    .line 53
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/endpoint/l;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->h:Ljava/lang/String;

    goto :goto_68

    .line 55
    :cond_45
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->e:Lcom/google/gson/JsonElement;

    invoke-virtual {v0}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/l;->j:[B

    const/4 v2, 0x4

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, 0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/l;->j:[B

    const/4 v3, 0x4

    aget-byte v2, v2, v3

    neg-int v2, v2

    or-int/lit8 v3, v2, 0x20

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/l;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/JsonElement;->getAsString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->h:Ljava/lang/String;

    .line 57
    :goto_68
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->e:Lcom/google/gson/JsonElement;

    invoke-virtual {v0}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/l;->j:[B

    const/4 v2, 0x4

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, 0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/l;->j:[B

    const/4 v3, 0x4

    aget-byte v2, v2, v3

    neg-int v2, v2

    or-int/lit8 v3, v2, 0x20

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/l;->a(III)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/JsonObject;->addProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->e:Lcom/google/gson/JsonElement;

    invoke-virtual {v0}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/l;->j:[B

    const/16 v2, 0xb

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/l;->j:[B

    const/16 v3, 0x11

    aget-byte v2, v2, v3

    add-int/lit8 v2, v2, 0x1

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/l;->j:[B

    const/16 v4, 0xc

    aget-byte v3, v3, v4

    add-int/lit8 v3, v3, -0x1

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/l;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lbr/com/itau/security/middlewarechannel/Utilities;->getDeviceID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/JsonObject;->addProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    rsub-int/lit8 p0, p0, 0x21

    new-instance v0, Ljava/lang/String;

    add-int/lit8 p2, p2, 0x54

    const/4 v4, -0x1

    sget-object v5, Lbr/com/itau/sdk/android/core/endpoint/l;->j:[B

    add-int/lit8 p1, p1, 0x5

    new-array v1, p1, [B

    add-int/lit8 p1, p1, -0x1

    if-nez v5, :cond_16

    move v2, p0

    move v3, p2

    :goto_13
    neg-int v3, v3

    add-int p2, v2, v3

    :cond_16
    add-int/lit8 v4, v4, 0x1

    int-to-byte v2, p2

    aput-byte v2, v1, v4

    if-ne v4, p1, :cond_22

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_22
    move v2, p2

    add-int/lit8 p0, p0, 0x1

    aget-byte v3, v5, p0

    goto :goto_13
.end method

.method private a([Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;
    .registers 9

    .line 94
    if-eqz p1, :cond_5

    array-length v0, p1

    if-nez v0, :cond_6

    .line 95
    :cond_5
    return-object p2

    .line 97
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v1, 0x26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 99
    const/4 v5, 0x0

    :goto_12
    array-length v0, p1

    if-ge v5, v0, :cond_52

    .line 100
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/l;->j:[B

    const/16 v1, 0x23

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/l;->j:[B

    const/4 v2, 0x4

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, 0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/l;->j:[B

    const/16 v3, 0x9

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/l;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v1, v5, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x3d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-object v1, p1, v5

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    if-ge v5, v0, :cond_4f

    .line 103
    const/16 v0, 0x26

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 99
    :cond_4f
    add-int/lit8 v5, v5, 0x1

    goto :goto_12

    .line 106
    :cond_52
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b()Ljava/lang/String;
    .registers 2

    .line 110
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/l;->a:Ljava/security/SecureRandom;

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Lbr/com/itau/sdk/android/core/endpoint/e;
    .registers 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Lbr/com/itau/sdk/android/core/endpoint/e<Ljava/lang/String;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 86
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->i:Lbr/com/itau/sdk/android/core/model/MiddlewareRequestDTO;

    if-eqz v0, :cond_d

    sget-object v0, Lbr/com/itau/sdk/android/core/n;->a:Lcom/google/gson/Gson;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->i:Lbr/com/itau/sdk/android/core/model/MiddlewareRequestDTO;

    invoke-virtual {v0, v1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    goto :goto_15

    :cond_d
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->a:Lcom/google/gson/Gson;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->e:Lcom/google/gson/JsonElement;

    .line 87
    invoke-virtual {v0, v1}, Lcom/google/gson/Gson;->toJson(Lcom/google/gson/JsonElement;)Ljava/lang/String;

    move-result-object v7

    .line 88
    :goto_15
    invoke-static {p0, v7}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/m;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->c:Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareCaller;

    move-object v2, v7

    iget-object v3, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->d:Ljava/lang/String;

    iget-object v4, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->h:Ljava/lang/String;

    iget v5, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->f:I

    iget v6, p0, Lbr/com/itau/sdk/android/core/endpoint/l;->g:I

    invoke-interface/range {v1 .. v6}, Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareCaller;->execute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/m;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

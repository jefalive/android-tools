.class public final Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;
.super Ljava/security/SecureRandomSpi;
.source "SourceFile"


# static fields
.field private static final a:Ljava/io/File;

.field private static final b:Ljava/lang/Object;

.field private static c:Ljava/io/DataInputStream;

.field private static d:Ljava/io/OutputStream;

.field private static final f:[B

.field private static g:I


# instance fields
.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .line 184
    const/16 v0, 0x52

    new-array v0, v0, [B

    fill-array-data v0, :array_36

    sput-object v0, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->f:[B

    const/16 v0, 0x38

    sput v0, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->g:I

    new-instance v0, Ljava/io/File;

    sget-object v1, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->f:[B

    const/16 v2, 0x20

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->f:[B

    const/16 v3, 0x4d

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    sget-object v3, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->f:[B

    const/16 v4, 0x20

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->a(ISS)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->a:Ljava/io/File;

    .line 186
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->b:Ljava/lang/Object;

    return-void

    nop

    :array_36
    .array-data 1
        0x68t
        -0x71t
        0x47t
        0x3dt
        -0x48t
        -0xbt
        -0x5t
        0x50t
        -0x54t
        0xbt
        0x2t
        -0x5t
        -0x7t
        -0x7t
        0x5t
        -0x1dt
        -0xat
        -0x5t
        0x5t
        -0x1t
        0x42t
        -0x56t
        0x3t
        0x4dt
        -0x54t
        0xbt
        0x2t
        -0x5t
        0x42t
        -0x48t
        -0xet
        0x1t
        0x0t
        0x4bt
        -0x1dt
        -0xat
        -0x5t
        0x5t
        -0x1t
        0x42t
        -0x56t
        0x3t
        0x4dt
        -0x51t
        -0x3t
        0x9t
        -0xbt
        0x4ct
        -0x1dt
        -0xat
        -0x5t
        0x5t
        -0x1t
        0x42t
        -0x56t
        0x3t
        0x4dt
        -0x4ft
        0x2t
        -0x11t
        0x56t
        -0x55t
        0xct
        -0x2t
        -0x1t
        0x42t
        -0x4bt
        -0x7t
        -0x8t
        0x3t
        0x4dt
        -0x37t
        -0x3t
        -0x13t
        0x45t
        -0x48t
        0x1t
        0xft
        -0xft
        0x8t
        -0xdt
        0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 170
    invoke-direct {p0}, Ljava/security/SecureRandomSpi;-><init>()V

    return-void
.end method

.method private static a()Ljava/io/DataInputStream;
    .registers 7

    .line 259
    sget-object v5, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->b:Ljava/lang/Object;

    monitor-enter v5

    .line 260
    :try_start_3
    sget-object v0, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->c:Ljava/io/DataInputStream;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_56

    if-nez v0, :cond_52

    .line 266
    :try_start_7
    new-instance v0, Ljava/io/DataInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    sget-object v2, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->a:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    sput-object v0, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->c:Ljava/io/DataInputStream;
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_15} :catch_16
    .catchall {:try_start_7 .. :try_end_15} :catchall_56

    .line 271
    goto :goto_52

    .line 268
    :catch_16
    move-exception v6

    .line 269
    :try_start_17
    new-instance v0, Ljava/lang/SecurityException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->f:[B

    const/16 v3, 0x16

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    const/16 v3, 0x26

    const/16 v4, 0x25

    invoke-static {v2, v3, v4}, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->a(ISS)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->a:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->f:[B

    const/16 v3, 0x20

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    int-to-byte v3, v2

    or-int/lit8 v4, v3, 0x43

    int-to-byte v4, v4

    invoke-static {v2, v3, v4}, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->a(ISS)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v6}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 273
    :cond_52
    :goto_52
    sget-object v0, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->c:Ljava/io/DataInputStream;
    :try_end_54
    .catchall {:try_start_17 .. :try_end_54} :catchall_56

    monitor-exit v5

    return-object v0

    .line 274
    :catchall_56
    move-exception v6

    monitor-exit v5

    throw v6
.end method

.method private static a(ISS)Ljava/lang/String;
    .registers 9

    add-int/lit8 p1, p1, 0x20

    rsub-int/lit8 p2, p2, 0x47

    add-int/lit8 p0, p0, 0xc

    const/4 v4, 0x0

    sget-object v5, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->f:[B

    new-instance v0, Ljava/lang/String;

    new-array v1, p0, [B

    if-nez v5, :cond_17

    move v2, p0

    move v3, p2

    :goto_11
    add-int/lit8 p2, p2, 0x1

    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p1, v2, -0x2

    :cond_17
    move v2, v4

    add-int/lit8 v4, v4, 0x1

    int-to-byte v3, p1

    aput-byte v3, v1, v2

    if-ne v4, p0, :cond_28

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_28
    move v2, p1

    aget-byte v3, v5, p2

    goto :goto_11
.end method

.method private static b()Ljava/io/OutputStream;
    .registers 4

    .line 278
    sget-object v2, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->b:Ljava/lang/Object;

    monitor-enter v2

    .line 279
    :try_start_3
    sget-object v0, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->d:Ljava/io/OutputStream;

    if-nez v0, :cond_10

    .line 280
    new-instance v0, Ljava/io/FileOutputStream;

    sget-object v1, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->a:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    sput-object v0, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->d:Ljava/io/OutputStream;

    .line 282
    :cond_10
    sget-object v0, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->d:Ljava/io/OutputStream;
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_14

    monitor-exit v2

    return-object v0

    .line 283
    :catchall_14
    move-exception v3

    monitor-exit v2

    throw v3
.end method


# virtual methods
.method protected final engineGenerateSeed(I)[B
    .registers 2

    .line 253
    new-array p1, p1, [B

    .line 254
    invoke-virtual {p0, p1}, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->engineNextBytes([B)V

    .line 255
    return-object p1
.end method

.method protected final engineNextBytes([B)V
    .registers 9

    .line 232
    iget-boolean v0, p0, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->e:Z

    if-nez v0, :cond_b

    .line 234
    invoke-static {}, Lbr/com/itau/security/commons/PRNGFixes;->a()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->engineSetSeed([B)V

    .line 239
    :cond_b
    :try_start_b
    sget-object v6, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->b:Ljava/lang/Object;

    monitor-enter v6
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_e} :catch_21

    .line 240
    :try_start_e
    invoke-static {}, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->a()Ljava/io/DataInputStream;
    :try_end_11
    .catchall {:try_start_e .. :try_end_11} :catchall_14

    move-result-object v5

    .line 241
    monitor-exit v6

    goto :goto_17

    :catchall_14
    move-exception p1

    monitor-exit v6

    :try_start_16
    throw p1

    .line 242
    :goto_17
    move-object v6, v5

    monitor-enter v6
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_19} :catch_21

    .line 243
    :try_start_19
    invoke-virtual {v5, p1}, Ljava/io/DataInputStream;->readFully([B)V
    :try_end_1c
    .catchall {:try_start_19 .. :try_end_1c} :catchall_1e

    .line 244
    monitor-exit v6

    return-void

    :catchall_1e
    move-exception p1

    monitor-exit v6

    :try_start_20
    throw p1
    :try_end_21
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_21} :catch_21

    .line 245
    :catch_21
    move-exception v5

    .line 246
    new-instance v0, Ljava/lang/SecurityException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->f:[B

    const/16 v3, 0x4f

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    sget v3, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->g:I

    int-to-byte v3, v3

    const/16 v4, 0x26

    invoke-static {v2, v4, v3}, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->a(ISS)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->a:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v5}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method protected final engineSetSeed([B)V
    .registers 8

    .line 215
    :try_start_0
    sget-object v5, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->b:Ljava/lang/Object;

    monitor-enter v5
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_3} :catch_16
    .catchall {:try_start_0 .. :try_end_3} :catchall_3c

    .line 216
    :try_start_3
    invoke-static {}, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->b()Ljava/io/OutputStream;
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_9

    move-result-object v4

    .line 217
    monitor-exit v5

    goto :goto_c

    :catchall_9
    move-exception p1

    monitor-exit v5

    :try_start_b
    throw p1

    .line 218
    :goto_c
    invoke-virtual {v4, p1}, Ljava/io/OutputStream;->write([B)V

    .line 219
    invoke-virtual {v4}, Ljava/io/OutputStream;->flush()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_12} :catch_16
    .catchall {:try_start_b .. :try_end_12} :catchall_3c

    .line 226
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->e:Z

    .line 227
    return-void

    .line 220
    .line 223
    :catch_16
    :try_start_16
    const-class v0, Lbr/com/itau/security/commons/PRNGFixes;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->f:[B

    const/16 v2, 0x3e

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    const/16 v2, 0x26

    const/16 v3, 0x17

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->a(ISS)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->a:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    :try_end_38
    .catchall {:try_start_16 .. :try_end_38} :catchall_3c

    .line 226
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->e:Z

    .line 227
    return-void

    .line 226
    :catchall_3c
    move-exception p1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/security/commons/PRNGFixes$LinuxPRNGSecureRandom;->e:Z

    throw p1
.end method

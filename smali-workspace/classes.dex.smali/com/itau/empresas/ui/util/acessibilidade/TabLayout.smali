.class public Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;
.super Landroid/support/design/widget/TabLayout;
.source "TabLayout.java"


# instance fields
.field inflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 17
    invoke-direct {p0, p1}, Landroid/support/design/widget/TabLayout;-><init>(Landroid/content/Context;)V

    .line 18
    invoke-direct {p0}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->init()V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 22
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/TabLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    invoke-direct {p0}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->init()V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroid/support/design/widget/TabLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    invoke-direct {p0}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->init()V

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;Landroid/support/design/widget/TabLayout$Tab;Z)V
    .registers 3
    .param p0, "x0"    # Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;
    .param p1, "x1"    # Landroid/support/design/widget/TabLayout$Tab;
    .param p2, "x2"    # Z

    .line 12
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->aplicaAcessibilidadeTab(Landroid/support/design/widget/TabLayout$Tab;Z)V

    return-void
.end method

.method private ajusteTab(Landroid/support/design/widget/TabLayout$Tab;)V
    .registers 3
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;

    .line 72
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->aplicaFonteTab(Landroid/support/design/widget/TabLayout$Tab;)V

    .line 73
    invoke-virtual {p1}, Landroid/support/design/widget/TabLayout$Tab;->isSelected()Z

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->aplicaAcessibilidadeTab(Landroid/support/design/widget/TabLayout$Tab;Z)V

    .line 74
    return-void
.end method

.method private aplicaAcessibilidadeTab(Landroid/support/design/widget/TabLayout$Tab;Z)V
    .registers 10
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;
    .param p2, "selecionada"    # Z

    .line 108
    invoke-virtual {p1}, Landroid/support/design/widget/TabLayout$Tab;->getCustomView()Landroid/view/View;

    move-result-object v5

    .line 109
    .local v5, "customView":Landroid/view/View;
    invoke-virtual {p0}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz p2, :cond_e

    const v1, 0x7f070087

    goto :goto_11

    :cond_e
    const v1, 0x7f070084

    :goto_11
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    .line 112
    invoke-virtual {p1}, Landroid/support/design/widget/TabLayout$Tab;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 109
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 113
    .local v6, "contentDescription":Ljava/lang/String;
    if-eqz v5, :cond_25

    .line 114
    invoke-virtual {v5, v6}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_28

    .line 116
    :cond_25
    invoke-virtual {p1, v6}, Landroid/support/design/widget/TabLayout$Tab;->setContentDescription(Ljava/lang/CharSequence;)Landroid/support/design/widget/TabLayout$Tab;

    .line 119
    :goto_28
    return-void
.end method

.method private aplicaFonteTab(Landroid/support/design/widget/TabLayout$Tab;)V
    .registers 8
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;

    .line 78
    invoke-virtual {p1}, Landroid/support/design/widget/TabLayout$Tab;->getCustomView()Landroid/view/View;

    move-result-object v4

    .line 81
    .local v4, "customView":Landroid/view/View;
    if-nez v4, :cond_17

    .line 82
    iget-object v0, p0, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->inflater:Landroid/view/LayoutInflater;

    const v1, 0x7f03011a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/TextView;

    .line 83
    .local v5, "tv":Landroid/widget/TextView;
    invoke-virtual {p1, v5}, Landroid/support/design/widget/TabLayout$Tab;->setCustomView(Landroid/view/View;)Landroid/support/design/widget/TabLayout$Tab;

    goto :goto_27

    .line 85
    .end local v5    # "tv":Landroid/widget/TextView;
    :cond_17
    instance-of v0, v4, Landroid/widget/TextView;

    if-eqz v0, :cond_1f

    .line 86
    move-object v5, v4

    check-cast v5, Landroid/widget/TextView;

    .local v5, "tv":Landroid/widget/TextView;
    goto :goto_27

    .line 88
    .end local v5    # "tv":Landroid/widget/TextView;
    :cond_1f
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Tabs devem ser TextView"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    .local v5, "tv":Landroid/widget/TextView;
    :goto_27
    invoke-virtual {p1}, Landroid/support/design/widget/TabLayout$Tab;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    invoke-virtual {p1}, Landroid/support/design/widget/TabLayout$Tab;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_51

    .line 95
    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setSelected(Z)V

    .line 96
    .line 97
    invoke-virtual {p0}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 98
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const v2, 0x7f070087

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 96
    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_69

    .line 100
    .line 101
    :cond_51
    invoke-virtual {p0}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const v2, 0x7f070084

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 100
    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 105
    :goto_69
    return-void
.end method

.method private init()V
    .registers 3

    .line 32
    invoke-virtual {p0}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->inflater:Landroid/view/LayoutInflater;

    .line 34
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->setOnTabSelectedListener(Landroid/support/design/widget/TabLayout$OnTabSelectedListener;)V

    .line 35
    return-void
.end method


# virtual methods
.method public addTab(Landroid/support/design/widget/TabLayout$Tab;)V
    .registers 2
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;

    .line 49
    invoke-super {p0, p1}, Landroid/support/design/widget/TabLayout;->addTab(Landroid/support/design/widget/TabLayout$Tab;)V

    .line 50
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->ajusteTab(Landroid/support/design/widget/TabLayout$Tab;)V

    .line 51
    return-void
.end method

.method public addTab(Landroid/support/design/widget/TabLayout$Tab;I)V
    .registers 3
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;
    .param p2, "position"    # I

    .line 55
    invoke-super {p0, p1, p2}, Landroid/support/design/widget/TabLayout;->addTab(Landroid/support/design/widget/TabLayout$Tab;I)V

    .line 56
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->ajusteTab(Landroid/support/design/widget/TabLayout$Tab;)V

    .line 57
    return-void
.end method

.method public addTab(Landroid/support/design/widget/TabLayout$Tab;IZ)V
    .registers 4
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;
    .param p2, "position"    # I
    .param p3, "setSelected"    # Z

    .line 67
    invoke-super {p0, p1, p2, p3}, Landroid/support/design/widget/TabLayout;->addTab(Landroid/support/design/widget/TabLayout$Tab;IZ)V

    .line 68
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->ajusteTab(Landroid/support/design/widget/TabLayout$Tab;)V

    .line 69
    return-void
.end method

.method public addTab(Landroid/support/design/widget/TabLayout$Tab;Z)V
    .registers 3
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;
    .param p2, "setSelected"    # Z

    .line 61
    invoke-super {p0, p1, p2}, Landroid/support/design/widget/TabLayout;->addTab(Landroid/support/design/widget/TabLayout$Tab;Z)V

    .line 62
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->ajusteTab(Landroid/support/design/widget/TabLayout$Tab;)V

    .line 63
    return-void
.end method

.method public setOnTabSelectedListener(Landroid/support/design/widget/TabLayout$OnTabSelectedListener;)V
    .registers 3
    .param p1, "onTabSelectedListener"    # Landroid/support/design/widget/TabLayout$OnTabSelectedListener;

    .line 123
    new-instance v0, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout$1;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout$1;-><init>(Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;Landroid/support/design/widget/TabLayout$OnTabSelectedListener;)V

    .line 150
    .local v0, "listener":Landroid/support/design/widget/TabLayout$OnTabSelectedListener;
    invoke-super {p0, v0}, Landroid/support/design/widget/TabLayout;->setOnTabSelectedListener(Landroid/support/design/widget/TabLayout$OnTabSelectedListener;)V

    .line 151
    return-void
.end method

.method public setupWithViewPager(Landroid/support/v4/view/ViewPager;)V
    .registers 5
    .param p1, "viewPager"    # Landroid/support/v4/view/ViewPager;

    .line 39
    invoke-super {p0, p1}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 40
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_4
    invoke-virtual {p0}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->getTabCount()I

    move-result v0

    if-ge v1, v0, :cond_14

    .line 41
    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->getTabAt(I)Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v2

    .line 42
    .local v2, "tab":Landroid/support/design/widget/TabLayout$Tab;
    invoke-direct {p0, v2}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->ajusteTab(Landroid/support/design/widget/TabLayout$Tab;)V

    .line 40
    .end local v2    # "tab":Landroid/support/design/widget/TabLayout$Tab;
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 44
    .end local v1    # "i":I
    :cond_14
    return-void
.end method

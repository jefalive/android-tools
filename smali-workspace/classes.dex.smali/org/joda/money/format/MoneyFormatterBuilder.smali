.class public final Lorg/joda/money/format/MoneyFormatterBuilder;
.super Ljava/lang/Object;
.source "MoneyFormatterBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/joda/money/format/MoneyFormatterBuilder$1;,
        Lorg/joda/money/format/MoneyFormatterBuilder$SingletonPrinters;,
        Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;
    }
.end annotation


# instance fields
.field private final parsers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lorg/joda/money/format/MoneyParser;>;"
        }
    .end annotation
.end field

.field private final printers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lorg/joda/money/format/MoneyPrinter;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/joda/money/format/MoneyFormatterBuilder;->printers:Ljava/util/List;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/joda/money/format/MoneyFormatterBuilder;->parsers:Ljava/util/List;

    .line 50
    return-void
.end method

.method private appendInternal(Lorg/joda/money/format/MoneyPrinter;Lorg/joda/money/format/MoneyParser;)Lorg/joda/money/format/MoneyFormatterBuilder;
    .registers 4
    .param p1, "printer"    # Lorg/joda/money/format/MoneyPrinter;
    .param p2, "parser"    # Lorg/joda/money/format/MoneyParser;

    .line 203
    iget-object v0, p0, Lorg/joda/money/format/MoneyFormatterBuilder;->printers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    iget-object v0, p0, Lorg/joda/money/format/MoneyFormatterBuilder;->parsers:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    return-object p0
.end method


# virtual methods
.method public append(Lorg/joda/money/format/MoneyFormatter;)Lorg/joda/money/format/MoneyFormatterBuilder;
    .registers 3
    .param p1, "formatter"    # Lorg/joda/money/format/MoneyFormatter;

    .line 174
    const-string v0, "MoneyFormatter must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/format/MoneyFormatter;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    invoke-virtual {p1, p0}, Lorg/joda/money/format/MoneyFormatter;->appendTo(Lorg/joda/money/format/MoneyFormatterBuilder;)V

    .line 176
    return-object p0
.end method

.method public append(Lorg/joda/money/format/MoneyPrinter;Lorg/joda/money/format/MoneyParser;)Lorg/joda/money/format/MoneyFormatterBuilder;
    .registers 4
    .param p1, "printer"    # Lorg/joda/money/format/MoneyPrinter;
    .param p2, "parser"    # Lorg/joda/money/format/MoneyParser;

    .line 189
    invoke-direct {p0, p1, p2}, Lorg/joda/money/format/MoneyFormatterBuilder;->appendInternal(Lorg/joda/money/format/MoneyPrinter;Lorg/joda/money/format/MoneyParser;)Lorg/joda/money/format/MoneyFormatterBuilder;

    move-result-object v0

    return-object v0
.end method

.method public appendAmount()Lorg/joda/money/format/MoneyFormatterBuilder;
    .registers 3

    .line 62
    new-instance v1, Lorg/joda/money/format/AmountPrinterParser;

    sget-object v0, Lorg/joda/money/format/MoneyAmountStyle;->ASCII_DECIMAL_POINT_GROUP3_COMMA:Lorg/joda/money/format/MoneyAmountStyle;

    invoke-direct {v1, v0}, Lorg/joda/money/format/AmountPrinterParser;-><init>(Lorg/joda/money/format/MoneyAmountStyle;)V

    .line 63
    .local v1, "pp":Lorg/joda/money/format/AmountPrinterParser;
    invoke-direct {p0, v1, v1}, Lorg/joda/money/format/MoneyFormatterBuilder;->appendInternal(Lorg/joda/money/format/MoneyPrinter;Lorg/joda/money/format/MoneyParser;)Lorg/joda/money/format/MoneyFormatterBuilder;

    move-result-object v0

    return-object v0
.end method

.method public appendAmount(Lorg/joda/money/format/MoneyAmountStyle;)Lorg/joda/money/format/MoneyFormatterBuilder;
    .registers 4
    .param p1, "style"    # Lorg/joda/money/format/MoneyAmountStyle;

    .line 91
    const-string v0, "MoneyAmountStyle must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/format/MoneyFormatter;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    new-instance v1, Lorg/joda/money/format/AmountPrinterParser;

    invoke-direct {v1, p1}, Lorg/joda/money/format/AmountPrinterParser;-><init>(Lorg/joda/money/format/MoneyAmountStyle;)V

    .line 93
    .local v1, "pp":Lorg/joda/money/format/AmountPrinterParser;
    invoke-direct {p0, v1, v1}, Lorg/joda/money/format/MoneyFormatterBuilder;->appendInternal(Lorg/joda/money/format/MoneyPrinter;Lorg/joda/money/format/MoneyParser;)Lorg/joda/money/format/MoneyFormatterBuilder;

    move-result-object v0

    return-object v0
.end method

.method public appendAmountLocalized()Lorg/joda/money/format/MoneyFormatterBuilder;
    .registers 3

    .line 75
    new-instance v1, Lorg/joda/money/format/AmountPrinterParser;

    sget-object v0, Lorg/joda/money/format/MoneyAmountStyle;->LOCALIZED_GROUPING:Lorg/joda/money/format/MoneyAmountStyle;

    invoke-direct {v1, v0}, Lorg/joda/money/format/AmountPrinterParser;-><init>(Lorg/joda/money/format/MoneyAmountStyle;)V

    .line 76
    .local v1, "pp":Lorg/joda/money/format/AmountPrinterParser;
    invoke-direct {p0, v1, v1}, Lorg/joda/money/format/MoneyFormatterBuilder;->appendInternal(Lorg/joda/money/format/MoneyPrinter;Lorg/joda/money/format/MoneyParser;)Lorg/joda/money/format/MoneyFormatterBuilder;

    move-result-object v0

    return-object v0
.end method

.method public appendCurrencyCode()Lorg/joda/money/format/MoneyFormatterBuilder;
    .registers 3

    .line 105
    sget-object v0, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;->CODE:Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;

    sget-object v1, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;->CODE:Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;

    invoke-direct {p0, v0, v1}, Lorg/joda/money/format/MoneyFormatterBuilder;->appendInternal(Lorg/joda/money/format/MoneyPrinter;Lorg/joda/money/format/MoneyParser;)Lorg/joda/money/format/MoneyFormatterBuilder;

    move-result-object v0

    return-object v0
.end method

.method public appendCurrencyNumeric3Code()Lorg/joda/money/format/MoneyFormatterBuilder;
    .registers 3

    .line 117
    sget-object v0, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;->NUMERIC_3_CODE:Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;

    sget-object v1, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;->NUMERIC_3_CODE:Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;

    invoke-direct {p0, v0, v1}, Lorg/joda/money/format/MoneyFormatterBuilder;->appendInternal(Lorg/joda/money/format/MoneyPrinter;Lorg/joda/money/format/MoneyParser;)Lorg/joda/money/format/MoneyFormatterBuilder;

    move-result-object v0

    return-object v0
.end method

.method public appendCurrencyNumericCode()Lorg/joda/money/format/MoneyFormatterBuilder;
    .registers 3

    .line 128
    sget-object v0, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;->NUMERIC_CODE:Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;

    sget-object v1, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;->NUMERIC_CODE:Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;

    invoke-direct {p0, v0, v1}, Lorg/joda/money/format/MoneyFormatterBuilder;->appendInternal(Lorg/joda/money/format/MoneyPrinter;Lorg/joda/money/format/MoneyParser;)Lorg/joda/money/format/MoneyFormatterBuilder;

    move-result-object v0

    return-object v0
.end method

.method public appendCurrencySymbolLocalized()Lorg/joda/money/format/MoneyFormatterBuilder;
    .registers 3

    .line 142
    sget-object v0, Lorg/joda/money/format/MoneyFormatterBuilder$SingletonPrinters;->LOCALIZED_SYMBOL:Lorg/joda/money/format/MoneyFormatterBuilder$SingletonPrinters;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/joda/money/format/MoneyFormatterBuilder;->appendInternal(Lorg/joda/money/format/MoneyPrinter;Lorg/joda/money/format/MoneyParser;)Lorg/joda/money/format/MoneyFormatterBuilder;

    move-result-object v0

    return-object v0
.end method

.method public appendLiteral(Ljava/lang/CharSequence;)Lorg/joda/money/format/MoneyFormatterBuilder;
    .registers 4
    .param p1, "literal"    # Ljava/lang/CharSequence;

    .line 155
    if-eqz p1, :cond_8

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_9

    .line 156
    :cond_8
    return-object p0

    .line 158
    :cond_9
    new-instance v1, Lorg/joda/money/format/LiteralPrinterParser;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/joda/money/format/LiteralPrinterParser;-><init>(Ljava/lang/String;)V

    .line 159
    .local v1, "pp":Lorg/joda/money/format/LiteralPrinterParser;
    invoke-direct {p0, v1, v1}, Lorg/joda/money/format/MoneyFormatterBuilder;->appendInternal(Lorg/joda/money/format/MoneyPrinter;Lorg/joda/money/format/MoneyParser;)Lorg/joda/money/format/MoneyFormatterBuilder;

    move-result-object v0

    return-object v0
.end method

.method public toFormatter()Lorg/joda/money/format/MoneyFormatter;
    .registers 2

    .line 222
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/joda/money/format/MoneyFormatterBuilder;->toFormatter(Ljava/util/Locale;)Lorg/joda/money/format/MoneyFormatter;

    move-result-object v0

    return-object v0
.end method

.method public toFormatter(Ljava/util/Locale;)Lorg/joda/money/format/MoneyFormatter;
    .registers 6
    .param p1, "locale"    # Ljava/util/Locale;

    .line 240
    const-string v0, "Locale must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/format/MoneyFormatter;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 241
    iget-object v0, p0, Lorg/joda/money/format/MoneyFormatterBuilder;->printers:Ljava/util/List;

    iget-object v1, p0, Lorg/joda/money/format/MoneyFormatterBuilder;->printers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lorg/joda/money/format/MoneyPrinter;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/joda/money/format/MoneyPrinter;

    move-object v2, v0

    check-cast v2, [Lorg/joda/money/format/MoneyPrinter;

    .line 242
    .local v2, "printersCopy":[Lorg/joda/money/format/MoneyPrinter;
    iget-object v0, p0, Lorg/joda/money/format/MoneyFormatterBuilder;->parsers:Ljava/util/List;

    iget-object v1, p0, Lorg/joda/money/format/MoneyFormatterBuilder;->parsers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lorg/joda/money/format/MoneyParser;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/joda/money/format/MoneyParser;

    move-object v3, v0

    check-cast v3, [Lorg/joda/money/format/MoneyParser;

    .line 243
    .local v3, "parsersCopy":[Lorg/joda/money/format/MoneyParser;
    new-instance v0, Lorg/joda/money/format/MoneyFormatter;

    invoke-direct {v0, p1, v2, v3}, Lorg/joda/money/format/MoneyFormatter;-><init>(Ljava/util/Locale;[Lorg/joda/money/format/MoneyPrinter;[Lorg/joda/money/format/MoneyParser;)V

    return-object v0
.end method

.class public interface abstract Lcom/itau/empresas/feature/login/PreLoginAPI;
.super Ljava/lang/Object;
.source "PreLoginAPI.java"


# virtual methods
.method public abstract buscaOperadoresAtivos(Lcom/itau/empresas/feature/login/model/PreLoginVO;)Ljava/util/List;
    .param p1    # Lcom/itau/empresas/feature/login/model/PreLoginVO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = ""
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/PreLogin;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/itau/empresas/feature/login/model/PreLoginVO;)Ljava/util/List<Lcom/itau/empresas/api/model/OperadorVO;>;"
        }
    .end annotation
.end method

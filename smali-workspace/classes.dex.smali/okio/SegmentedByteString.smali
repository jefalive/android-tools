.class final Lokio/SegmentedByteString;
.super Lokio/ByteString;
.source "SegmentedByteString.java"


# instance fields
.field final transient directory:[I

.field final transient segments:[[B


# direct methods
.method constructor <init>(Lokio/Buffer;I)V
    .registers 12
    .param p1, "buffer"    # Lokio/Buffer;
    .param p2, "byteCount"    # I

    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lokio/ByteString;-><init>([B)V

    .line 57
    iget-wide v0, p1, Lokio/Buffer;->size:J

    int-to-long v4, p2

    const-wide/16 v2, 0x0

    invoke-static/range {v0 .. v5}, Lokio/Util;->checkOffsetAndCount(JJJ)V

    .line 60
    const/4 v6, 0x0

    .line 61
    .local v6, "offset":I
    const/4 v7, 0x0

    .line 62
    .local v7, "segmentCount":I
    iget-object v8, p1, Lokio/Buffer;->head:Lokio/Segment;

    .local v8, "s":Lokio/Segment;
    :goto_10
    if-ge v6, p2, :cond_2b

    .line 63
    iget v0, v8, Lokio/Segment;->limit:I

    iget v1, v8, Lokio/Segment;->pos:I

    if-ne v0, v1, :cond_20

    .line 64
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "s.limit == s.pos"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 66
    :cond_20
    iget v0, v8, Lokio/Segment;->limit:I

    iget v1, v8, Lokio/Segment;->pos:I

    sub-int/2addr v0, v1

    add-int/2addr v6, v0

    .line 67
    add-int/lit8 v7, v7, 0x1

    .line 62
    iget-object v8, v8, Lokio/Segment;->next:Lokio/Segment;

    goto :goto_10

    .line 71
    .end local v8    # "s":Lokio/Segment;
    :cond_2b
    new-array v0, v7, [[B

    iput-object v0, p0, Lokio/SegmentedByteString;->segments:[[B

    .line 72
    mul-int/lit8 v0, v7, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lokio/SegmentedByteString;->directory:[I

    .line 73
    const/4 v6, 0x0

    .line 74
    const/4 v7, 0x0

    .line 75
    iget-object v8, p1, Lokio/Buffer;->head:Lokio/Segment;

    .local v8, "s":Lokio/Segment;
    :goto_39
    if-ge v6, p2, :cond_60

    .line 76
    iget-object v0, p0, Lokio/SegmentedByteString;->segments:[[B

    iget-object v1, v8, Lokio/Segment;->data:[B

    aput-object v1, v0, v7

    .line 77
    iget v0, v8, Lokio/Segment;->limit:I

    iget v1, v8, Lokio/Segment;->pos:I

    sub-int/2addr v0, v1

    add-int/2addr v6, v0

    .line 78
    if-le v6, p2, :cond_4a

    .line 79
    move v6, p2

    .line 81
    :cond_4a
    iget-object v0, p0, Lokio/SegmentedByteString;->directory:[I

    aput v6, v0, v7

    .line 82
    iget-object v0, p0, Lokio/SegmentedByteString;->directory:[I

    iget-object v1, p0, Lokio/SegmentedByteString;->segments:[[B

    array-length v1, v1

    add-int/2addr v1, v7

    iget v2, v8, Lokio/Segment;->pos:I

    aput v2, v0, v1

    .line 83
    const/4 v0, 0x1

    iput-boolean v0, v8, Lokio/Segment;->shared:Z

    .line 84
    add-int/lit8 v7, v7, 0x1

    .line 75
    iget-object v8, v8, Lokio/Segment;->next:Lokio/Segment;

    goto :goto_39

    .line 86
    .end local v8    # "s":Lokio/Segment;
    :cond_60
    return-void
.end method

.method private segment(I)I
    .registers 7
    .param p1, "pos"    # I

    .line 139
    iget-object v0, p0, Lokio/SegmentedByteString;->directory:[I

    iget-object v1, p0, Lokio/SegmentedByteString;->segments:[[B

    array-length v1, v1

    add-int/lit8 v2, p1, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v2}, Ljava/util/Arrays;->binarySearch([IIII)I

    move-result v4

    .line 140
    .local v4, "i":I
    if-ltz v4, :cond_10

    move v0, v4

    goto :goto_12

    :cond_10
    xor-int/lit8 v0, v4, -0x1

    :goto_12
    return v0
.end method

.method private toByteString()Lokio/ByteString;
    .registers 3

    .line 240
    new-instance v0, Lokio/ByteString;

    invoke-virtual {p0}, Lokio/SegmentedByteString;->toByteArray()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lokio/ByteString;-><init>([B)V

    return-object v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .registers 2

    .line 279
    invoke-direct {p0}, Lokio/SegmentedByteString;->toByteString()Lokio/ByteString;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public base64()Ljava/lang/String;
    .registers 2

    .line 93
    invoke-direct {p0}, Lokio/SegmentedByteString;->toByteString()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->base64()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .param p1, "o"    # Ljava/lang/Object;

    .line 248
    if-ne p1, p0, :cond_4

    const/4 v0, 0x1

    return v0

    .line 249
    :cond_4
    instance-of v0, p1, Lokio/ByteString;

    if-eqz v0, :cond_26

    move-object v0, p1

    check-cast v0, Lokio/ByteString;

    .line 250
    invoke-virtual {v0}, Lokio/ByteString;->size()I

    move-result v0

    invoke-virtual {p0}, Lokio/SegmentedByteString;->size()I

    move-result v1

    if-ne v0, v1, :cond_26

    move-object v0, p1

    check-cast v0, Lokio/ByteString;

    .line 251
    invoke-virtual {p0}, Lokio/SegmentedByteString;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v0, v3, v1}, Lokio/SegmentedByteString;->rangeEquals(ILokio/ByteString;II)Z

    move-result v0

    if-eqz v0, :cond_26

    const/4 v0, 0x1

    goto :goto_27

    :cond_26
    const/4 v0, 0x0

    .line 249
    :goto_27
    return v0
.end method

.method public getByte(I)B
    .registers 11
    .param p1, "pos"    # I

    .line 129
    iget-object v0, p0, Lokio/SegmentedByteString;->directory:[I

    iget-object v1, p0, Lokio/SegmentedByteString;->segments:[[B

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    int-to-long v0, v0

    int-to-long v2, p1

    const-wide/16 v4, 0x1

    invoke-static/range {v0 .. v5}, Lokio/Util;->checkOffsetAndCount(JJJ)V

    .line 130
    invoke-direct {p0, p1}, Lokio/SegmentedByteString;->segment(I)I

    move-result v6

    .line 131
    .local v6, "segment":I
    if-nez v6, :cond_18

    const/4 v7, 0x0

    goto :goto_1e

    :cond_18
    iget-object v0, p0, Lokio/SegmentedByteString;->directory:[I

    add-int/lit8 v1, v6, -0x1

    aget v7, v0, v1

    .line 132
    .local v7, "segmentOffset":I
    :goto_1e
    iget-object v0, p0, Lokio/SegmentedByteString;->directory:[I

    iget-object v1, p0, Lokio/SegmentedByteString;->segments:[[B

    array-length v1, v1

    add-int/2addr v1, v6

    aget v8, v0, v1

    .line 133
    .local v8, "segmentPos":I
    iget-object v0, p0, Lokio/SegmentedByteString;->segments:[[B

    aget-object v0, v0, v6

    sub-int v1, p1, v7

    add-int/2addr v1, v8

    aget-byte v0, v0, v1

    return v0
.end method

.method public hashCode()I
    .registers 13

    .line 255
    iget v2, p0, Lokio/SegmentedByteString;->hashCode:I

    .line 256
    .local v2, "result":I
    if-eqz v2, :cond_5

    return v2

    .line 259
    :cond_5
    const/4 v2, 0x1

    .line 260
    const/4 v3, 0x0

    .line 261
    .local v3, "segmentOffset":I
    const/4 v4, 0x0

    .local v4, "s":I
    iget-object v0, p0, Lokio/SegmentedByteString;->segments:[[B

    array-length v5, v0

    .local v5, "segmentCount":I
    :goto_b
    if-ge v4, v5, :cond_2f

    .line 262
    iget-object v0, p0, Lokio/SegmentedByteString;->segments:[[B

    aget-object v6, v0, v4

    .line 263
    .local v6, "segment":[B
    iget-object v0, p0, Lokio/SegmentedByteString;->directory:[I

    add-int v1, v5, v4

    aget v7, v0, v1

    .line 264
    .local v7, "segmentPos":I
    iget-object v0, p0, Lokio/SegmentedByteString;->directory:[I

    aget v8, v0, v4

    .line 265
    .local v8, "nextSegmentOffset":I
    sub-int v9, v8, v3

    .line 266
    .local v9, "segmentSize":I
    move v10, v7

    .local v10, "i":I
    add-int v11, v7, v9

    .local v11, "limit":I
    :goto_20
    if-ge v10, v11, :cond_2b

    .line 267
    mul-int/lit8 v0, v2, 0x1f

    aget-byte v1, v6, v10

    add-int v2, v0, v1

    .line 266
    add-int/lit8 v10, v10, 0x1

    goto :goto_20

    .line 269
    .end local v10    # "i":I
    .end local v11    # "limit":I
    :cond_2b
    move v3, v8

    .line 261
    .end local v6    # "segment":[B
    .end local v7    # "segmentPos":I
    .end local v8    # "nextSegmentOffset":I
    .end local v9    # "segmentSize":I
    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    .line 271
    .end local v4    # "s":I
    .end local v5    # "segmentCount":I
    :cond_2f
    iput v2, p0, Lokio/SegmentedByteString;->hashCode:I

    return v2
.end method

.method public hex()Ljava/lang/String;
    .registers 2

    .line 97
    invoke-direct {p0}, Lokio/SegmentedByteString;->toByteString()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hex()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public rangeEquals(ILokio/ByteString;II)Z
    .registers 13
    .param p1, "offset"    # I
    .param p2, "other"    # Lokio/ByteString;
    .param p3, "otherOffset"    # I
    .param p4, "byteCount"    # I

    .line 194
    if-ltz p1, :cond_9

    invoke-virtual {p0}, Lokio/SegmentedByteString;->size()I

    move-result v0

    sub-int/2addr v0, p4

    if-le p1, v0, :cond_b

    :cond_9
    const/4 v0, 0x0

    return v0

    .line 196
    :cond_b
    invoke-direct {p0, p1}, Lokio/SegmentedByteString;->segment(I)I

    move-result v2

    .local v2, "s":I
    :goto_f
    if-lez p4, :cond_46

    .line 197
    if-nez v2, :cond_15

    const/4 v3, 0x0

    goto :goto_1b

    :cond_15
    iget-object v0, p0, Lokio/SegmentedByteString;->directory:[I

    add-int/lit8 v1, v2, -0x1

    aget v3, v0, v1

    .line 198
    .local v3, "segmentOffset":I
    :goto_1b
    iget-object v0, p0, Lokio/SegmentedByteString;->directory:[I

    aget v0, v0, v2

    sub-int v4, v0, v3

    .line 199
    .local v4, "segmentSize":I
    add-int v0, v3, v4

    sub-int/2addr v0, p1

    invoke-static {p4, v0}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 200
    .local v5, "stepSize":I
    iget-object v0, p0, Lokio/SegmentedByteString;->directory:[I

    iget-object v1, p0, Lokio/SegmentedByteString;->segments:[[B

    array-length v1, v1

    add-int/2addr v1, v2

    aget v6, v0, v1

    .line 201
    .local v6, "segmentPos":I
    sub-int v0, p1, v3

    add-int v7, v0, v6

    .line 202
    .local v7, "arrayOffset":I
    iget-object v0, p0, Lokio/SegmentedByteString;->segments:[[B

    aget-object v0, v0, v2

    invoke-virtual {p2, p3, v0, v7, v5}, Lokio/ByteString;->rangeEquals(I[BII)Z

    move-result v0

    if-nez v0, :cond_40

    const/4 v0, 0x0

    return v0

    .line 203
    :cond_40
    add-int/2addr p1, v5

    .line 204
    add-int/2addr p3, v5

    .line 205
    sub-int/2addr p4, v5

    .line 196
    .end local v3    # "segmentOffset":I
    .end local v4    # "segmentSize":I
    .end local v5    # "stepSize":I
    .end local v6    # "segmentPos":I
    .end local v7    # "arrayOffset":I
    add-int/lit8 v2, v2, 0x1

    goto :goto_f

    .line 207
    .end local v2    # "s":I
    :cond_46
    const/4 v0, 0x1

    return v0
.end method

.method public rangeEquals(I[BII)Z
    .registers 13
    .param p1, "offset"    # I
    .param p2, "other"    # [B
    .param p3, "otherOffset"    # I
    .param p4, "byteCount"    # I

    .line 211
    if-ltz p1, :cond_f

    invoke-virtual {p0}, Lokio/SegmentedByteString;->size()I

    move-result v0

    sub-int/2addr v0, p4

    if-gt p1, v0, :cond_f

    if-ltz p3, :cond_f

    array-length v0, p2

    sub-int/2addr v0, p4

    if-le p3, v0, :cond_11

    .line 213
    :cond_f
    const/4 v0, 0x0

    return v0

    .line 216
    :cond_11
    invoke-direct {p0, p1}, Lokio/SegmentedByteString;->segment(I)I

    move-result v2

    .local v2, "s":I
    :goto_15
    if-lez p4, :cond_4c

    .line 217
    if-nez v2, :cond_1b

    const/4 v3, 0x0

    goto :goto_21

    :cond_1b
    iget-object v0, p0, Lokio/SegmentedByteString;->directory:[I

    add-int/lit8 v1, v2, -0x1

    aget v3, v0, v1

    .line 218
    .local v3, "segmentOffset":I
    :goto_21
    iget-object v0, p0, Lokio/SegmentedByteString;->directory:[I

    aget v0, v0, v2

    sub-int v4, v0, v3

    .line 219
    .local v4, "segmentSize":I
    add-int v0, v3, v4

    sub-int/2addr v0, p1

    invoke-static {p4, v0}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 220
    .local v5, "stepSize":I
    iget-object v0, p0, Lokio/SegmentedByteString;->directory:[I

    iget-object v1, p0, Lokio/SegmentedByteString;->segments:[[B

    array-length v1, v1

    add-int/2addr v1, v2

    aget v6, v0, v1

    .line 221
    .local v6, "segmentPos":I
    sub-int v0, p1, v3

    add-int v7, v0, v6

    .line 222
    .local v7, "arrayOffset":I
    iget-object v0, p0, Lokio/SegmentedByteString;->segments:[[B

    aget-object v0, v0, v2

    invoke-static {v0, v7, p2, p3, v5}, Lokio/Util;->arrayRangeEquals([BI[BII)Z

    move-result v0

    if-nez v0, :cond_46

    const/4 v0, 0x0

    return v0

    .line 223
    :cond_46
    add-int/2addr p1, v5

    .line 224
    add-int/2addr p3, v5

    .line 225
    sub-int/2addr p4, v5

    .line 216
    .end local v3    # "segmentOffset":I
    .end local v4    # "segmentSize":I
    .end local v5    # "stepSize":I
    .end local v6    # "segmentPos":I
    .end local v7    # "arrayOffset":I
    add-int/lit8 v2, v2, 0x1

    goto :goto_15

    .line 227
    .end local v2    # "s":I
    :cond_4c
    const/4 v0, 0x1

    return v0
.end method

.method public size()I
    .registers 4

    .line 144
    iget-object v0, p0, Lokio/SegmentedByteString;->directory:[I

    iget-object v1, p0, Lokio/SegmentedByteString;->segments:[[B

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    return v0
.end method

.method public substring(II)Lokio/ByteString;
    .registers 4
    .param p1, "beginIndex"    # I
    .param p2, "endIndex"    # I

    .line 125
    invoke-direct {p0}, Lokio/SegmentedByteString;->toByteString()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lokio/ByteString;->substring(II)Lokio/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public toAsciiLowercase()Lokio/ByteString;
    .registers 2

    .line 101
    invoke-direct {p0}, Lokio/SegmentedByteString;->toByteString()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->toAsciiLowercase()Lokio/ByteString;

    move-result-object v0

    return-object v0
.end method

.method public toByteArray()[B
    .registers 10

    .line 148
    iget-object v0, p0, Lokio/SegmentedByteString;->directory:[I

    iget-object v1, p0, Lokio/SegmentedByteString;->segments:[[B

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    new-array v3, v0, [B

    .line 149
    .local v3, "result":[B
    const/4 v4, 0x0

    .line 150
    .local v4, "segmentOffset":I
    const/4 v5, 0x0

    .local v5, "s":I
    iget-object v0, p0, Lokio/SegmentedByteString;->segments:[[B

    array-length v6, v0

    .local v6, "segmentCount":I
    :goto_10
    if-ge v5, v6, :cond_29

    .line 151
    iget-object v0, p0, Lokio/SegmentedByteString;->directory:[I

    add-int v1, v6, v5

    aget v7, v0, v1

    .line 152
    .local v7, "segmentPos":I
    iget-object v0, p0, Lokio/SegmentedByteString;->directory:[I

    aget v8, v0, v5

    .line 153
    .local v8, "nextSegmentOffset":I
    iget-object v0, p0, Lokio/SegmentedByteString;->segments:[[B

    aget-object v0, v0, v5

    sub-int v1, v8, v4

    invoke-static {v0, v7, v3, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 155
    move v4, v8

    .line 150
    .end local v7    # "segmentPos":I
    .end local v8    # "nextSegmentOffset":I
    add-int/lit8 v5, v5, 0x1

    goto :goto_10

    .line 157
    .end local v5    # "s":I
    .end local v6    # "segmentCount":I
    :cond_29
    return-object v3
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .line 275
    invoke-direct {p0}, Lokio/SegmentedByteString;->toByteString()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public utf8()Ljava/lang/String;
    .registers 2

    .line 89
    invoke-direct {p0}, Lokio/SegmentedByteString;->toByteString()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->utf8()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method write(Lokio/Buffer;)V
    .registers 12
    .param p1, "buffer"    # Lokio/Buffer;

    .line 176
    const/4 v4, 0x0

    .line 177
    .local v4, "segmentOffset":I
    const/4 v5, 0x0

    .local v5, "s":I
    iget-object v0, p0, Lokio/SegmentedByteString;->segments:[[B

    array-length v6, v0

    .local v6, "segmentCount":I
    :goto_5
    if-ge v5, v6, :cond_33

    .line 178
    iget-object v0, p0, Lokio/SegmentedByteString;->directory:[I

    add-int v1, v6, v5

    aget v7, v0, v1

    .line 179
    .local v7, "segmentPos":I
    iget-object v0, p0, Lokio/SegmentedByteString;->directory:[I

    aget v8, v0, v5

    .line 180
    .local v8, "nextSegmentOffset":I
    new-instance v9, Lokio/Segment;

    iget-object v0, p0, Lokio/SegmentedByteString;->segments:[[B

    aget-object v0, v0, v5

    add-int v1, v7, v8

    sub-int/2addr v1, v4

    invoke-direct {v9, v0, v7, v1}, Lokio/Segment;-><init>([BII)V

    .line 182
    .local v9, "segment":Lokio/Segment;
    iget-object v0, p1, Lokio/Buffer;->head:Lokio/Segment;

    if-nez v0, :cond_28

    .line 183
    iput-object v9, v9, Lokio/Segment;->prev:Lokio/Segment;

    iput-object v9, v9, Lokio/Segment;->next:Lokio/Segment;

    iput-object v9, p1, Lokio/Buffer;->head:Lokio/Segment;

    goto :goto_2f

    .line 185
    :cond_28
    iget-object v0, p1, Lokio/Buffer;->head:Lokio/Segment;

    iget-object v0, v0, Lokio/Segment;->prev:Lokio/Segment;

    invoke-virtual {v0, v9}, Lokio/Segment;->push(Lokio/Segment;)Lokio/Segment;

    .line 187
    :goto_2f
    move v4, v8

    .line 177
    .end local v7    # "segmentPos":I
    .end local v8    # "nextSegmentOffset":I
    .end local v9    # "segment":Lokio/Segment;
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 189
    .end local v5    # "s":I
    .end local v6    # "segmentCount":I
    :cond_33
    iget-wide v0, p1, Lokio/Buffer;->size:J

    int-to-long v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p1, Lokio/Buffer;->size:J

    .line 190
    return-void
.end method

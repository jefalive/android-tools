.class public Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;
.super Ljava/lang/Object;
.source "MenuBuscaHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper$CriterioBusca;
    }
.end annotation


# static fields
.field private static singleton:Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;


# instance fields
.field private porChave:Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper$CriterioBusca;

.field private porNome:Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper$CriterioBusca;


# direct methods
.method private constructor <init>()V
    .registers 2

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper$1;-><init>(Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;)V

    iput-object v0, p0, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;->porNome:Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper$CriterioBusca;

    .line 86
    new-instance v0, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper$2;-><init>(Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;)V

    iput-object v0, p0, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;->porChave:Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper$CriterioBusca;

    .line 18
    return-void
.end method

.method private busca(Lcom/itau/empresas/api/model/MenuVO;Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper$CriterioBusca;)Lcom/itau/empresas/api/model/MenuVO;
    .registers 9
    .param p1, "menuVO"    # Lcom/itau/empresas/api/model/MenuVO;
    .param p2, "palavra"    # Ljava/lang/String;
    .param p3, "criterio"    # Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper$CriterioBusca;

    .line 29
    if-nez p1, :cond_4

    .line 30
    const/4 v0, 0x0

    return-object v0

    .line 34
    :cond_4
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 36
    .local v1, "visited":Ljava/util/Set;, "Ljava/util/Set<Lcom/itau/empresas/api/model/MenuVO;>;"
    new-instance v2, Ljava/util/Stack;

    invoke-direct {v2}, Ljava/util/Stack;-><init>()V

    .line 37
    .local v2, "pilha":Ljava/util/Stack;, "Ljava/util/Stack<Lcom/itau/empresas/api/model/MenuVO;>;"
    invoke-virtual {v2, p1}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 38
    :goto_11
    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_35

    .line 39
    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/itau/empresas/api/model/MenuVO;

    .line 40
    .local v3, "m":Lcom/itau/empresas/api/model/MenuVO;
    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 42
    invoke-interface {p3, v3, p2}, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper$CriterioBusca;->isCriterioValido(Lcom/itau/empresas/api/model/MenuVO;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_28

    .line 43
    return-object v3

    .line 46
    :cond_28
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v4

    .line 47
    .local v4, "filhos":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;"
    if-eqz v4, :cond_34

    .line 48
    invoke-interface {v4, v1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 49
    invoke-virtual {v2, v4}, Ljava/util/Stack;->addAll(Ljava/util/Collection;)Z

    .line 51
    .end local v3    # "m":Lcom/itau/empresas/api/model/MenuVO;
    .end local v4    # "filhos":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;"
    .end local v4
    :cond_34
    goto :goto_11

    .line 53
    :cond_35
    const/4 v0, 0x0

    return-object v0
.end method

.method public static declared-synchronized getInstance()Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;
    .registers 3

    const-class v1, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;

    monitor-enter v1

    .line 21
    :try_start_3
    sget-object v0, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;->singleton:Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;

    if-nez v0, :cond_e

    .line 22
    new-instance v0, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;

    invoke-direct {v0}, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;-><init>()V

    sput-object v0, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;->singleton:Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;

    .line 25
    :cond_e
    sget-object v0, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;->singleton:Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_12

    monitor-exit v1

    return-object v0

    :catchall_12
    move-exception v2

    monitor-exit v1

    throw v2
.end method


# virtual methods
.method public buscaPorChave(Lcom/itau/empresas/api/model/MenuVO;Ljava/lang/String;)Lcom/itau/empresas/api/model/MenuVO;
    .registers 4
    .param p1, "menuVO"    # Lcom/itau/empresas/api/model/MenuVO;
    .param p2, "chave"    # Ljava/lang/String;

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;->porChave:Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper$CriterioBusca;

    invoke-direct {p0, p1, p2, v0}, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;->busca(Lcom/itau/empresas/api/model/MenuVO;Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper$CriterioBusca;)Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v0

    return-object v0
.end method

.method public buscaPorNome(Lcom/itau/empresas/api/model/MenuVO;Ljava/lang/String;)Lcom/itau/empresas/api/model/MenuVO;
    .registers 4
    .param p1, "menuVO"    # Lcom/itau/empresas/api/model/MenuVO;
    .param p2, "nome"    # Ljava/lang/String;

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;->porNome:Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper$CriterioBusca;

    invoke-direct {p0, p1, p2, v0}, Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper;->busca(Lcom/itau/empresas/api/model/MenuVO;Ljava/lang/String;Lcom/itau/empresas/ui/util/menu/MenuBuscaHelper$CriterioBusca;)Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v0

    return-object v0
.end method

.class public Lcom/google/android/gms/internal/zzcm;
.super Lcom/google/android/gms/ads/formats/NativeAppInstallAd;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# instance fields
.field private final zzyM:Lcom/google/android/gms/internal/zzcl;

.field private final zzyN:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/google/android/gms/ads/formats/NativeAd$Image;>;"
        }
    .end annotation
.end field

.field private final zzyO:Lcom/google/android/gms/internal/zzci;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/zzcl;)V
    .registers 8

    invoke-direct {p0}, Lcom/google/android/gms/ads/formats/NativeAppInstallAd;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzcm;->zzyN:Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/gms/internal/zzcm;->zzyM:Lcom/google/android/gms/internal/zzcl;

    :try_start_c
    iget-object v0, p0, Lcom/google/android/gms/internal/zzcm;->zzyM:Lcom/google/android/gms/internal/zzcl;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzcl;->getImages()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_33

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_18
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_33

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/gms/internal/zzcm;->zzc(Ljava/lang/Object;)Lcom/google/android/gms/internal/zzch;

    move-result-object v5

    if-eqz v5, :cond_32

    iget-object v0, p0, Lcom/google/android/gms/internal/zzcm;->zzyN:Ljava/util/List;

    new-instance v1, Lcom/google/android/gms/internal/zzci;

    invoke-direct {v1, v5}, Lcom/google/android/gms/internal/zzci;-><init>(Lcom/google/android/gms/internal/zzch;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_32
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_32} :catch_34

    :cond_32
    goto :goto_18

    :cond_33
    goto :goto_3a

    :catch_34
    move-exception v2

    const-string v0, "Failed to get image."

    invoke-static {v0, v2}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_3a
    const/4 v2, 0x0

    :try_start_3b
    iget-object v0, p0, Lcom/google/android/gms/internal/zzcm;->zzyM:Lcom/google/android/gms/internal/zzcl;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzcl;->zzdK()Lcom/google/android/gms/internal/zzch;

    move-result-object v3

    if-eqz v3, :cond_49

    new-instance v0, Lcom/google/android/gms/internal/zzci;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzci;-><init>(Lcom/google/android/gms/internal/zzch;)V
    :try_end_48
    .catch Landroid/os/RemoteException; {:try_start_3b .. :try_end_48} :catch_4a

    move-object v2, v0

    :cond_49
    goto :goto_50

    :catch_4a
    move-exception v3

    const-string v0, "Failed to get icon."

    invoke-static {v0, v3}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_50
    iput-object v2, p0, Lcom/google/android/gms/internal/zzcm;->zzyO:Lcom/google/android/gms/internal/zzci;

    return-void
.end method


# virtual methods
.method public getBody()Ljava/lang/CharSequence;
    .registers 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/zzcm;->zzyM:Lcom/google/android/gms/internal/zzcl;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzcl;->getBody()Ljava/lang/String;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    move-result-object v0

    return-object v0

    :catch_7
    move-exception v1

    const-string v0, "Failed to get body."

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public getCallToAction()Ljava/lang/CharSequence;
    .registers 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/zzcm;->zzyM:Lcom/google/android/gms/internal/zzcl;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzcl;->getCallToAction()Ljava/lang/String;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    move-result-object v0

    return-object v0

    :catch_7
    move-exception v1

    const-string v0, "Failed to get call to action."

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public getHeadline()Ljava/lang/CharSequence;
    .registers 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/zzcm;->zzyM:Lcom/google/android/gms/internal/zzcl;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzcl;->getHeadline()Ljava/lang/String;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    move-result-object v0

    return-object v0

    :catch_7
    move-exception v1

    const-string v0, "Failed to get headline."

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public getIcon()Lcom/google/android/gms/ads/formats/NativeAd$Image;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzcm;->zzyO:Lcom/google/android/gms/internal/zzci;

    return-object v0
.end method

.method public getImages()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/google/android/gms/ads/formats/NativeAd$Image;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/internal/zzcm;->zzyN:Ljava/util/List;

    return-object v0
.end method

.method public getPrice()Ljava/lang/CharSequence;
    .registers 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/zzcm;->zzyM:Lcom/google/android/gms/internal/zzcl;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzcl;->getPrice()Ljava/lang/String;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    move-result-object v0

    return-object v0

    :catch_7
    move-exception v1

    const-string v0, "Failed to get price."

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public getStarRating()Ljava/lang/Double;
    .registers 5

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/zzcm;->zzyM:Lcom/google/android/gms/internal/zzcl;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzcl;->getStarRating()D

    move-result-wide v2

    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    cmpl-double v0, v2, v0

    if-nez v0, :cond_e

    const/4 v0, 0x0

    goto :goto_12

    :cond_e
    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_11} :catch_13

    move-result-object v0

    :goto_12
    return-object v0

    :catch_13
    move-exception v2

    const-string v0, "Failed to get star rating."

    invoke-static {v0, v2}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method public getStore()Ljava/lang/CharSequence;
    .registers 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/zzcm;->zzyM:Lcom/google/android/gms/internal/zzcl;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzcl;->getStore()Ljava/lang/String;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    move-result-object v0

    return-object v0

    :catch_7
    move-exception v1

    const-string v0, "Failed to get store"

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    return-object v0
.end method

.method protected synthetic zzaH()Ljava/lang/Object;
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzcm;->zzdL()Lcom/google/android/gms/dynamic/zzd;

    move-result-object v0

    return-object v0
.end method

.method zzc(Ljava/lang/Object;)Lcom/google/android/gms/internal/zzch;
    .registers 3

    instance-of v0, p1, Landroid/os/IBinder;

    if-eqz v0, :cond_c

    move-object v0, p1

    check-cast v0, Landroid/os/IBinder;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzch$zza;->zzt(Landroid/os/IBinder;)Lcom/google/android/gms/internal/zzch;

    move-result-object v0

    return-object v0

    :cond_c
    const/4 v0, 0x0

    return-object v0
.end method

.method protected zzdL()Lcom/google/android/gms/dynamic/zzd;
    .registers 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/zzcm;->zzyM:Lcom/google/android/gms/internal/zzcl;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzcl;->zzdL()Lcom/google/android/gms/dynamic/zzd;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_7

    move-result-object v0

    return-object v0

    :catch_7
    move-exception v1

    const-string v0, "Failed to retrieve native ad engine."

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    return-object v0
.end method

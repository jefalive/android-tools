.class public final enum Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;
.super Ljava/lang/Enum;
.source "EmprestimosParceladosDetalhadoView.java"

# interfaces
.implements Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Estados"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;>;Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

.field public static final enum COMPROVANTE_EFETIVACAO:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

.field public static final enum CONTRATADO_DETALHE:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

.field public static final enum SIMULACAO_OFERTA_ESPECIAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

.field public static final enum SIMULACAO_OFERTA_PERSONALIZADA:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 305
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    const-string v1, "SIMULACAO_OFERTA_ESPECIAL"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;->SIMULACAO_OFERTA_ESPECIAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    new-instance v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    const-string v1, "SIMULACAO_OFERTA_PERSONALIZADA"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;->SIMULACAO_OFERTA_PERSONALIZADA:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    new-instance v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    const-string v1, "COMPROVANTE_EFETIVACAO"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;->COMPROVANTE_EFETIVACAO:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    .line 306
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    const-string v1, "CONTRATADO_DETALHE"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;->CONTRATADO_DETALHE:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    .line 304
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;->SIMULACAO_OFERTA_ESPECIAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;->SIMULACAO_OFERTA_PERSONALIZADA:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;->COMPROVANTE_EFETIVACAO:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;->CONTRATADO_DETALHE:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;->$VALUES:[Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 304
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 304
    const-class v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    return-object v0
.end method

.method public static values()[Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;
    .registers 1

    .line 304
    sget-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;->$VALUES:[Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    invoke-virtual {v0}, [Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    return-object v0
.end method

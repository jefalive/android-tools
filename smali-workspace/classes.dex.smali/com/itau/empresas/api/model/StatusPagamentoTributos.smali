.class public Lcom/itau/empresas/api/model/StatusPagamentoTributos;
.super Ljava/lang/Object;
.source "StatusPagamentoTributos.java"


# instance fields
.field private colorBackGround:I

.field private colorMessage:I

.field private icone:I

.field private mensagemStatus:Ljava/lang/String;

.field private tituloPagamento:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;IIILjava/lang/String;)V
    .registers 6
    .param p1, "mensagemStatus"    # Ljava/lang/String;
    .param p2, "colorBackGround"    # I
    .param p3, "colorMessage"    # I
    .param p4, "icone"    # I
    .param p5, "tituloPagamento"    # Ljava/lang/String;

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/itau/empresas/api/model/StatusPagamentoTributos;->mensagemStatus:Ljava/lang/String;

    .line 15
    iput p2, p0, Lcom/itau/empresas/api/model/StatusPagamentoTributos;->colorBackGround:I

    .line 16
    iput p3, p0, Lcom/itau/empresas/api/model/StatusPagamentoTributos;->colorMessage:I

    .line 17
    iput p4, p0, Lcom/itau/empresas/api/model/StatusPagamentoTributos;->icone:I

    .line 18
    iput-object p5, p0, Lcom/itau/empresas/api/model/StatusPagamentoTributos;->tituloPagamento:Ljava/lang/String;

    .line 19
    return-void
.end method


# virtual methods
.method public getColorBackGround()I
    .registers 2

    .line 26
    iget v0, p0, Lcom/itau/empresas/api/model/StatusPagamentoTributos;->colorBackGround:I

    return v0
.end method

.method public getColorMessage()I
    .registers 2

    .line 30
    iget v0, p0, Lcom/itau/empresas/api/model/StatusPagamentoTributos;->colorMessage:I

    return v0
.end method

.method public getIcone()I
    .registers 2

    .line 34
    iget v0, p0, Lcom/itau/empresas/api/model/StatusPagamentoTributos;->icone:I

    return v0
.end method

.method public getMensagemStatus()Ljava/lang/String;
    .registers 2

    .line 22
    iget-object v0, p0, Lcom/itau/empresas/api/model/StatusPagamentoTributos;->mensagemStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getTituloPagamento()Ljava/lang/String;
    .registers 2

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/api/model/StatusPagamentoTributos;->tituloPagamento:Ljava/lang/String;

    return-object v0
.end method

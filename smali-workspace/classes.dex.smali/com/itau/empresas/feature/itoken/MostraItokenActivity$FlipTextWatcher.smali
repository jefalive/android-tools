.class Lcom/itau/empresas/feature/itoken/MostraItokenActivity$FlipTextWatcher;
.super Ljava/lang/Object;
.source "MostraItokenActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/itoken/MostraItokenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FlipTextWatcher"
.end annotation


# instance fields
.field private final textView:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/widget/TextView;)V
    .registers 2
    .param p1, "textView"    # Landroid/widget/TextView;

    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159
    iput-object p1, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity$FlipTextWatcher;->textView:Landroid/widget/TextView;

    .line 160
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 2
    .param p1, "s"    # Landroid/text/Editable;

    .line 173
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .line 164
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 7
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .line 167
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_d

    .line 168
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity$FlipTextWatcher;->textView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity$FlipTextWatcher;->textView:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/AnimationUtils;->flipViewAnimation(Landroid/view/View;Landroid/view/View;)V

    .line 169
    :cond_d
    return-void
.end method

.class public Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO$LisConsultaRespostaVO;
.super Ljava/lang/Object;
.source "LisConsultaDetalheRespostaVO.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LisConsultaRespostaVO"
.end annotation


# instance fields
.field private final list:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "produtos_rotativos"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;>;"
        }
    .end annotation
.end field


# virtual methods
.method public getList()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;>;"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO$LisConsultaRespostaVO;->list:Ljava/util/List;

    return-object v0
.end method

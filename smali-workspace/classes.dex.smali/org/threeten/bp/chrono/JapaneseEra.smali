.class public final Lorg/threeten/bp/chrono/JapaneseEra;
.super Lorg/threeten/bp/jdk8/DefaultInterfaceEra;
.source "JapaneseEra.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final ERA_CONFIG:[Lsun/util/calendar/Era;

.field public static final HEISEI:Lorg/threeten/bp/chrono/JapaneseEra;

.field private static final KNOWN_ERAS:[Lorg/threeten/bp/chrono/JapaneseEra;

.field public static final MEIJI:Lorg/threeten/bp/chrono/JapaneseEra;

.field private static final N_ERA_CONSTANTS:I

.field public static final SHOWA:Lorg/threeten/bp/chrono/JapaneseEra;

.field public static final TAISHO:Lorg/threeten/bp/chrono/JapaneseEra;

.field private static final serialVersionUID:J = 0x145a0d680453ed8aL


# instance fields
.field private final eraValue:I

.field private final transient since:Lorg/threeten/bp/LocalDate;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    .line 80
    new-instance v0, Lorg/threeten/bp/chrono/JapaneseEra;

    const/16 v1, 0x74c

    const/16 v2, 0x9

    const/16 v3, 0x8

    invoke-static {v1, v2, v3}, Lorg/threeten/bp/LocalDate;->of(III)Lorg/threeten/bp/LocalDate;

    move-result-object v1

    const/4 v2, -0x1

    invoke-direct {v0, v2, v1}, Lorg/threeten/bp/chrono/JapaneseEra;-><init>(ILorg/threeten/bp/LocalDate;)V

    sput-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->MEIJI:Lorg/threeten/bp/chrono/JapaneseEra;

    .line 85
    new-instance v0, Lorg/threeten/bp/chrono/JapaneseEra;

    const/16 v1, 0x778

    const/4 v2, 0x7

    const/16 v3, 0x1e

    invoke-static {v1, v2, v3}, Lorg/threeten/bp/LocalDate;->of(III)Lorg/threeten/bp/LocalDate;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1}, Lorg/threeten/bp/chrono/JapaneseEra;-><init>(ILorg/threeten/bp/LocalDate;)V

    sput-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->TAISHO:Lorg/threeten/bp/chrono/JapaneseEra;

    .line 90
    new-instance v0, Lorg/threeten/bp/chrono/JapaneseEra;

    const/16 v1, 0x786

    const/16 v2, 0xc

    const/16 v3, 0x19

    invoke-static {v1, v2, v3}, Lorg/threeten/bp/LocalDate;->of(III)Lorg/threeten/bp/LocalDate;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v2, v1}, Lorg/threeten/bp/chrono/JapaneseEra;-><init>(ILorg/threeten/bp/LocalDate;)V

    sput-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->SHOWA:Lorg/threeten/bp/chrono/JapaneseEra;

    .line 95
    new-instance v0, Lorg/threeten/bp/chrono/JapaneseEra;

    const/16 v1, 0x7c5

    const/4 v2, 0x1

    const/16 v3, 0x8

    invoke-static {v1, v2, v3}, Lorg/threeten/bp/LocalDate;->of(III)Lorg/threeten/bp/LocalDate;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v2, v1}, Lorg/threeten/bp/chrono/JapaneseEra;-><init>(ILorg/threeten/bp/LocalDate;)V

    sput-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->HEISEI:Lorg/threeten/bp/chrono/JapaneseEra;

    .line 99
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->HEISEI:Lorg/threeten/bp/chrono/JapaneseEra;

    invoke-virtual {v0}, Lorg/threeten/bp/chrono/JapaneseEra;->getValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    sput v0, Lorg/threeten/bp/chrono/JapaneseEra;->N_ERA_CONSTANTS:I

    .line 110
    const-string v0, "japanese"

    invoke-static {v0}, Lsun/util/calendar/CalendarSystem;->forName(Ljava/lang/String;)Lsun/util/calendar/CalendarSystem;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lsun/util/calendar/LocalGregorianCalendar;

    .line 111
    .local v4, "jcal":Lsun/util/calendar/LocalGregorianCalendar;
    invoke-virtual {v4}, Lsun/util/calendar/LocalGregorianCalendar;->getEras()[Lsun/util/calendar/Era;

    move-result-object v0

    sput-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->ERA_CONFIG:[Lsun/util/calendar/Era;

    .line 113
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->ERA_CONFIG:[Lsun/util/calendar/Era;

    array-length v0, v0

    new-array v0, v0, [Lorg/threeten/bp/chrono/JapaneseEra;

    sput-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->KNOWN_ERAS:[Lorg/threeten/bp/chrono/JapaneseEra;

    .line 114
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->KNOWN_ERAS:[Lorg/threeten/bp/chrono/JapaneseEra;

    sget-object v1, Lorg/threeten/bp/chrono/JapaneseEra;->MEIJI:Lorg/threeten/bp/chrono/JapaneseEra;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 115
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->KNOWN_ERAS:[Lorg/threeten/bp/chrono/JapaneseEra;

    sget-object v1, Lorg/threeten/bp/chrono/JapaneseEra;->TAISHO:Lorg/threeten/bp/chrono/JapaneseEra;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 116
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->KNOWN_ERAS:[Lorg/threeten/bp/chrono/JapaneseEra;

    sget-object v1, Lorg/threeten/bp/chrono/JapaneseEra;->SHOWA:Lorg/threeten/bp/chrono/JapaneseEra;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 117
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->KNOWN_ERAS:[Lorg/threeten/bp/chrono/JapaneseEra;

    sget-object v1, Lorg/threeten/bp/chrono/JapaneseEra;->HEISEI:Lorg/threeten/bp/chrono/JapaneseEra;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 118
    sget v5, Lorg/threeten/bp/chrono/JapaneseEra;->N_ERA_CONSTANTS:I

    .local v5, "i":I
    :goto_86
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->ERA_CONFIG:[Lsun/util/calendar/Era;

    array-length v0, v0

    if-ge v5, v0, :cond_b1

    .line 119
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->ERA_CONFIG:[Lsun/util/calendar/Era;

    aget-object v0, v0, v5

    invoke-virtual {v0}, Lsun/util/calendar/Era;->getSinceDate()Lsun/util/calendar/CalendarDate;

    move-result-object v6

    .line 120
    .local v6, "date":Lsun/util/calendar/CalendarDate;
    invoke-virtual {v6}, Lsun/util/calendar/CalendarDate;->getYear()I

    move-result v0

    invoke-virtual {v6}, Lsun/util/calendar/CalendarDate;->getMonth()I

    move-result v1

    invoke-virtual {v6}, Lsun/util/calendar/CalendarDate;->getDayOfMonth()I

    move-result v2

    invoke-static {v0, v1, v2}, Lorg/threeten/bp/LocalDate;->of(III)Lorg/threeten/bp/LocalDate;

    move-result-object v7

    .line 121
    .local v7, "isoDate":Lorg/threeten/bp/LocalDate;
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->KNOWN_ERAS:[Lorg/threeten/bp/chrono/JapaneseEra;

    new-instance v1, Lorg/threeten/bp/chrono/JapaneseEra;

    add-int/lit8 v2, v5, -0x2

    invoke-direct {v1, v2, v7}, Lorg/threeten/bp/chrono/JapaneseEra;-><init>(ILorg/threeten/bp/LocalDate;)V

    aput-object v1, v0, v5

    .line 118
    .end local v6    # "date":Lsun/util/calendar/CalendarDate;
    .end local v7    # "isoDate":Lorg/threeten/bp/LocalDate;
    add-int/lit8 v5, v5, 0x1

    goto :goto_86

    .line 123
    .end local v4    # "jcal":Lsun/util/calendar/LocalGregorianCalendar;
    .end local v5    # "i":I
    :cond_b1
    return-void
.end method

.method private constructor <init>(ILorg/threeten/bp/LocalDate;)V
    .registers 3
    .param p1, "eraValue"    # I
    .param p2, "since"    # Lorg/threeten/bp/LocalDate;

    .line 140
    invoke-direct {p0}, Lorg/threeten/bp/jdk8/DefaultInterfaceEra;-><init>()V

    .line 141
    iput p1, p0, Lorg/threeten/bp/chrono/JapaneseEra;->eraValue:I

    .line 142
    iput-object p2, p0, Lorg/threeten/bp/chrono/JapaneseEra;->since:Lorg/threeten/bp/LocalDate;

    .line 143
    return-void
.end method

.method static from(Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/chrono/JapaneseEra;
    .registers 6
    .param p0, "date"    # Lorg/threeten/bp/LocalDate;

    .line 236
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->MEIJI:Lorg/threeten/bp/chrono/JapaneseEra;

    iget-object v0, v0, Lorg/threeten/bp/chrono/JapaneseEra;->since:Lorg/threeten/bp/LocalDate;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->isBefore(Lorg/threeten/bp/chrono/ChronoLocalDate;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 237
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Date too early: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 239
    :cond_23
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->KNOWN_ERAS:[Lorg/threeten/bp/chrono/JapaneseEra;

    array-length v0, v0

    add-int/lit8 v3, v0, -0x1

    .local v3, "i":I
    :goto_28
    if-ltz v3, :cond_3a

    .line 240
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->KNOWN_ERAS:[Lorg/threeten/bp/chrono/JapaneseEra;

    aget-object v4, v0, v3

    .line 241
    .local v4, "era":Lorg/threeten/bp/chrono/JapaneseEra;
    iget-object v0, v4, Lorg/threeten/bp/chrono/JapaneseEra;->since:Lorg/threeten/bp/LocalDate;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->compareTo(Lorg/threeten/bp/chrono/ChronoLocalDate;)I

    move-result v0

    if-ltz v0, :cond_37

    .line 242
    return-object v4

    .line 239
    .end local v4    # "era":Lorg/threeten/bp/chrono/JapaneseEra;
    :cond_37
    add-int/lit8 v3, v3, -0x1

    goto :goto_28

    .line 245
    .end local v3    # "i":I
    :cond_3a
    const/4 v0, 0x0

    return-object v0
.end method

.method public static of(I)Lorg/threeten/bp/chrono/JapaneseEra;
    .registers 3
    .param p0, "japaneseEra"    # I

    .line 187
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->MEIJI:Lorg/threeten/bp/chrono/JapaneseEra;

    iget v0, v0, Lorg/threeten/bp/chrono/JapaneseEra;->eraValue:I

    if-lt p0, v0, :cond_f

    add-int/lit8 v0, p0, 0x2

    add-int/lit8 v0, v0, -0x1

    sget-object v1, Lorg/threeten/bp/chrono/JapaneseEra;->KNOWN_ERAS:[Lorg/threeten/bp/chrono/JapaneseEra;

    array-length v1, v1

    if-lt v0, v1, :cond_17

    .line 188
    :cond_f
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    const-string v1, "japaneseEra is invalid"

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 190
    :cond_17
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->KNOWN_ERAS:[Lorg/threeten/bp/chrono/JapaneseEra;

    invoke-static {p0}, Lorg/threeten/bp/chrono/JapaneseEra;->ordinal(I)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method private static ordinal(I)I
    .registers 3
    .param p0, "eraValue"    # I

    .line 277
    add-int/lit8 v0, p0, 0x2

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method static readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/chrono/JapaneseEra;
    .registers 3
    .param p0, "in"    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 353
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v1

    .line 354
    .local v1, "eraValue":B
    invoke-static {v1}, Lorg/threeten/bp/chrono/JapaneseEra;->of(I)Lorg/threeten/bp/chrono/JapaneseEra;

    move-result-object v0

    return-object v0
.end method

.method private readResolve()Ljava/lang/Object;
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .line 155
    :try_start_0
    iget v0, p0, Lorg/threeten/bp/chrono/JapaneseEra;->eraValue:I

    invoke-static {v0}, Lorg/threeten/bp/chrono/JapaneseEra;->of(I)Lorg/threeten/bp/chrono/JapaneseEra;
    :try_end_5
    .catch Lorg/threeten/bp/DateTimeException; {:try_start_0 .. :try_end_5} :catch_7

    move-result-object v0

    return-object v0

    .line 156
    :catch_7
    move-exception v1

    .line 157
    .local v1, "e":Lorg/threeten/bp/DateTimeException;
    new-instance v2, Ljava/io/InvalidObjectException;

    const-string v0, "Invalid era"

    invoke-direct {v2, v0}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    .line 158
    .local v2, "ex":Ljava/io/InvalidObjectException;
    invoke-virtual {v2, v1}, Ljava/io/InvalidObjectException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 159
    throw v2
.end method

.method public static values()[Lorg/threeten/bp/chrono/JapaneseEra;
    .registers 2

    .line 225
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->KNOWN_ERAS:[Lorg/threeten/bp/chrono/JapaneseEra;

    sget-object v1, Lorg/threeten/bp/chrono/JapaneseEra;->KNOWN_ERAS:[Lorg/threeten/bp/chrono/JapaneseEra;

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/threeten/bp/chrono/JapaneseEra;

    return-object v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .registers 3

    .line 345
    new-instance v0, Lorg/threeten/bp/chrono/Ser;

    const/4 v1, 0x2

    invoke-direct {v0, v1, p0}, Lorg/threeten/bp/chrono/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method endDate()Lorg/threeten/bp/LocalDate;
    .registers 6

    .line 293
    iget v0, p0, Lorg/threeten/bp/chrono/JapaneseEra;->eraValue:I

    invoke-static {v0}, Lorg/threeten/bp/chrono/JapaneseEra;->ordinal(I)I

    move-result v3

    .line 294
    .local v3, "ordinal":I
    invoke-static {}, Lorg/threeten/bp/chrono/JapaneseEra;->values()[Lorg/threeten/bp/chrono/JapaneseEra;

    move-result-object v4

    .line 295
    .local v4, "eras":[Lorg/threeten/bp/chrono/JapaneseEra;
    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    if-lt v3, v0, :cond_12

    .line 296
    sget-object v0, Lorg/threeten/bp/LocalDate;->MAX:Lorg/threeten/bp/LocalDate;

    return-object v0

    .line 298
    :cond_12
    add-int/lit8 v0, v3, 0x1

    aget-object v0, v4, v0

    invoke-virtual {v0}, Lorg/threeten/bp/chrono/JapaneseEra;->startDate()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/LocalDate;->minusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method getName()Ljava/lang/String;
    .registers 3

    .line 334
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/JapaneseEra;->getValue()I

    move-result v0

    invoke-static {v0}, Lorg/threeten/bp/chrono/JapaneseEra;->ordinal(I)I

    move-result v1

    .line 335
    .local v1, "index":I
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseEra;->ERA_CONFIG:[Lsun/util/calendar/Era;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lsun/util/calendar/Era;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValue()I
    .registers 2

    .line 313
    iget v0, p0, Lorg/threeten/bp/chrono/JapaneseEra;->eraValue:I

    return v0
.end method

.method public range(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;
    .registers 4
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;

    .line 318
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->ERA:Lorg/threeten/bp/temporal/ChronoField;

    if-ne p1, v0, :cond_d

    .line 319
    sget-object v0, Lorg/threeten/bp/chrono/JapaneseChronology;->INSTANCE:Lorg/threeten/bp/chrono/JapaneseChronology;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->ERA:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/chrono/JapaneseChronology;->range(Lorg/threeten/bp/temporal/ChronoField;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    return-object v0

    .line 321
    :cond_d
    invoke-super {p0, p1}, Lorg/threeten/bp/jdk8/DefaultInterfaceEra;->range(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    return-object v0
.end method

.method startDate()Lorg/threeten/bp/LocalDate;
    .registers 2

    .line 285
    iget-object v0, p0, Lorg/threeten/bp/chrono/JapaneseEra;->since:Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .line 340
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/JapaneseEra;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method writeExternal(Ljava/io/DataOutput;)V
    .registers 3
    .param p1, "out"    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 349
    invoke-virtual {p0}, Lorg/threeten/bp/chrono/JapaneseEra;->getValue()I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 350
    return-void
.end method

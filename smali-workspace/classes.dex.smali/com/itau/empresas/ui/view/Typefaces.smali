.class public final Lcom/itau/empresas/ui/view/Typefaces;
.super Ljava/lang/Object;
.source "Typefaces.java"


# static fields
.field private static fontCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/String;Landroid/graphics/Typeface;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 12
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/itau/empresas/ui/view/Typefaces;->fontCache:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getTypeface(Ljava/lang/String;Landroid/content/Context;)Landroid/graphics/Typeface;
    .registers 5
    .param p0, "fontname"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .line 15
    sget-object v0, Lcom/itau/empresas/ui/view/Typefaces;->fontCache:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/graphics/Typeface;

    .line 17
    .local v1, "typeface":Landroid/graphics/Typeface;
    if-nez v1, :cond_1f

    .line 19
    :try_start_b
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-static {v0, p0}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;
    :try_end_12
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_12} :catch_14

    move-result-object v1

    .line 23
    goto :goto_1a

    .line 20
    :catch_14
    move-exception v2

    .line 21
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 22
    const/4 v0, 0x0

    return-object v0

    .line 25
    .end local v2    # "e":Ljava/lang/Exception;
    :goto_1a
    sget-object v0, Lcom/itau/empresas/ui/view/Typefaces;->fontCache:Ljava/util/HashMap;

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    :cond_1f
    return-object v1
.end method

.class Lcom/itau/empresas/feature/credito/lis/LisController$1;
.super Ljava/lang/Object;
.source "LisController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/credito/lis/LisController;->buscaOfertaResumo(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/credito/lis/LisController;

.field final synthetic val$cpfRepresentante:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/credito/lis/LisController;Ljava/lang/String;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/credito/lis/LisController;

    .line 27
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/lis/LisController$1;->this$0:Lcom/itau/empresas/feature/credito/lis/LisController;

    iput-object p2, p0, Lcom/itau/empresas/feature/credito/lis/LisController$1;->val$cpfRepresentante:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 8

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/LisController$1;->this$0:Lcom/itau/empresas/feature/credito/lis/LisController;

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/lis/LisController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v6

    .line 31
    .local v6, "contaSelecionada":Lcom/itau/empresas/api/model/ContaOperadorVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/LisController$1;->this$0:Lcom/itau/empresas/feature/credito/lis/LisController;

    # invokes: Lcom/itau/empresas/feature/credito/lis/LisController;->api()Ljava/lang/Object;
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/lis/LisController;->access$000(Lcom/itau/empresas/feature/credito/lis/LisController;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    invoke-virtual {v6}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v1

    .line 32
    invoke-virtual {v6}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x7

    invoke-static {v3, v2}, Lcom/itau/empresas/ui/util/ViewUtils;->formataAdicionandoZeros(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 33
    invoke-virtual {v6}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/lis/LisController$1;->val$cpfRepresentante:Ljava/lang/String;

    const-string v5, "cheque_especial"

    .line 31
    invoke-interface/range {v0 .. v5}, Lcom/itau/empresas/api/Api;->buscaOfertaResumo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/feature/credito/lis/model/OfertaResumoLisVO;

    move-result-object v0

    # invokes: Lcom/itau/empresas/feature/credito/lis/LisController;->post(Ljava/lang/Object;)V
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/lis/LisController;->access$100(Ljava/lang/Object;)V

    .line 36
    return-void
.end method

.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;
.source "PieChart.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;>;"
    }
.end annotation


# instance fields
.field private mAbsoluteAngles:[F

.field private mCenterText:Ljava/lang/String;

.field private mCenterTextRadiusPercent:F

.field private mCenterTextWordWrapEnabled:Z

.field private mCircleBox:Landroid/graphics/RectF;

.field private mDrawAngles:[F

.field private mDrawCenterText:Z

.field private mDrawHole:Z

.field private mDrawRoundedSlices:Z

.field private mDrawXLabels:Z

.field private mHoleRadiusPercent:F

.field private mTransparentCircleRadiusPercent:F

.field private mUsePercentValues:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 78
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;-><init>(Landroid/content/Context;)V

    .line 33
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCircleBox:Landroid/graphics/RectF;

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawXLabels:Z

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawHole:Z

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mUsePercentValues:Z

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawRoundedSlices:Z

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCenterText:Ljava/lang/String;

    .line 63
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mHoleRadiusPercent:F

    .line 68
    const/high16 v0, 0x425c0000    # 55.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mTransparentCircleRadiusPercent:F

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawCenterText:Z

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCenterTextWordWrapEnabled:Z

    .line 75
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCenterTextRadiusPercent:F

    .line 79
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 82
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCircleBox:Landroid/graphics/RectF;

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawXLabels:Z

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawHole:Z

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mUsePercentValues:Z

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawRoundedSlices:Z

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCenterText:Ljava/lang/String;

    .line 63
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mHoleRadiusPercent:F

    .line 68
    const/high16 v0, 0x425c0000    # 55.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mTransparentCircleRadiusPercent:F

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawCenterText:Z

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCenterTextWordWrapEnabled:Z

    .line 75
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCenterTextRadiusPercent:F

    .line 83
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .line 86
    invoke-direct {p0, p1, p2, p3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCircleBox:Landroid/graphics/RectF;

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawXLabels:Z

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawHole:Z

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mUsePercentValues:Z

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawRoundedSlices:Z

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCenterText:Ljava/lang/String;

    .line 63
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mHoleRadiusPercent:F

    .line 68
    const/high16 v0, 0x425c0000    # 55.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mTransparentCircleRadiusPercent:F

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawCenterText:Z

    .line 73
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCenterTextWordWrapEnabled:Z

    .line 75
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCenterTextRadiusPercent:F

    .line 87
    return-void
.end method

.method private calcAngle(F)F
    .registers 4
    .param p1, "value"    # F

    .line 214
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->getYValueSum()F

    move-result v0

    div-float v0, p1, v0

    const/high16 v1, 0x43b40000    # 360.0f

    mul-float/2addr v0, v1

    return v0
.end method

.method private calcAngles()V
    .registers 10

    .line 155
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->getYValCount()I

    move-result v0

    new-array v0, v0, [F

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawAngles:[F

    .line 156
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->getYValCount()I

    move-result v0

    new-array v0, v0, [F

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mAbsoluteAngles:[F

    .line 158
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->getDataSets()Ljava/util/List;

    move-result-object v3

    .line 160
    .local v3, "dataSets":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;>;"
    const/4 v4, 0x0

    .line 162
    .local v4, "cnt":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_22
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->getDataSetCount()I

    move-result v0

    if-ge v5, v0, :cond_76

    .line 164
    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;

    .line 165
    .local v6, "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;
    invoke-virtual {v6}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->getYVals()Ljava/util/List;

    move-result-object v7

    .line 167
    .local v7, "entries":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_38
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-ge v8, v0, :cond_73

    .line 169
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawAngles:[F

    invoke-interface {v7, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-direct {p0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->calcAngle(F)F

    move-result v1

    aput v1, v0, v4

    .line 171
    if-nez v4, :cond_5f

    .line 172
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mAbsoluteAngles:[F

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawAngles:[F

    aget v1, v1, v4

    aput v1, v0, v4

    goto :goto_6e

    .line 174
    :cond_5f
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mAbsoluteAngles:[F

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mAbsoluteAngles:[F

    add-int/lit8 v2, v4, -0x1

    aget v1, v1, v2

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawAngles:[F

    aget v2, v2, v4

    add-float/2addr v1, v2

    aput v1, v0, v4

    .line 177
    :goto_6e
    add-int/lit8 v4, v4, 0x1

    .line 167
    add-int/lit8 v8, v8, 0x1

    goto :goto_38

    .line 162
    .end local v6    # "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;
    .end local v7    # "entries":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
    .end local v7
    .end local v8    # "j":I
    :cond_73
    add-int/lit8 v5, v5, 0x1

    goto :goto_22

    .line 181
    .end local v5    # "i":I
    :cond_76
    return-void
.end method


# virtual methods
.method protected calcMinMax()V
    .registers 1

    .line 145
    invoke-super {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->calcMinMax()V

    .line 147
    invoke-direct {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->calcAngles()V

    .line 148
    return-void
.end method

.method public calculateOffsets()V
    .registers 9

    .line 126
    invoke-super {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->calculateOffsets()V

    .line 129
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDataNotSet:Z

    if-eqz v0, :cond_8

    .line 130
    return-void

    .line 132
    :cond_8
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getDiameter()F

    move-result v5

    .line 133
    .local v5, "diameter":F
    const/high16 v0, 0x40000000    # 2.0f

    div-float v6, v5, v0

    .line 135
    .local v6, "boxSize":F
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getCenterOffsets()Landroid/graphics/PointF;

    move-result-object v7

    .line 139
    .local v7, "c":Landroid/graphics/PointF;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCircleBox:Landroid/graphics/RectF;

    iget v1, v7, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v6

    iget v2, v7, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v6

    iget v3, v7, Landroid/graphics/PointF;->x:F

    add-float/2addr v3, v6

    iget v4, v7, Landroid/graphics/PointF;->y:F

    add-float/2addr v4, v6

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 141
    return-void
.end method

.method public getAbsoluteAngles()[F
    .registers 2

    .line 267
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mAbsoluteAngles:[F

    return-object v0
.end method

.method public getCenterCircleBox()Landroid/graphics/PointF;
    .registers 4

    .line 399
    new-instance v0, Landroid/graphics/PointF;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCircleBox:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCircleBox:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0
.end method

.method public getCenterText()Ljava/lang/String;
    .registers 2

    .line 344
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCenterText:Ljava/lang/String;

    return-object v0
.end method

.method public getCenterTextRadiusPercent()F
    .registers 2

    .line 554
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCenterTextRadiusPercent:F

    return v0
.end method

.method public getCircleBox()Landroid/graphics/RectF;
    .registers 2

    .line 390
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCircleBox:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getDataSetIndexForIndex(I)I
    .registers 5
    .param p1, "xIndex"    # I

    .line 239
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieData;->getDataSets()Ljava/util/List;

    move-result-object v1

    .line 241
    .local v1, "dataSets":Ljava/util/List;, "Ljava/util/List<+Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<+Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_9
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1f

    .line 242
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    invoke-virtual {v0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getEntryForXIndex(I)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 243
    return v2

    .line 241
    :cond_1c
    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    .line 246
    .end local v2    # "i":I
    :cond_1f
    const/4 v0, -0x1

    return v0
.end method

.method public getDrawAngles()[F
    .registers 2

    .line 257
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawAngles:[F

    return-object v0
.end method

.method public getHoleRadius()F
    .registers 2

    .line 452
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mHoleRadiusPercent:F

    return v0
.end method

.method public getIndexForAngle(F)I
    .registers 5
    .param p1, "angle"    # F

    .line 221
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->getRotationAngle()F

    move-result v0

    sub-float v0, p1, v0

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getNormalizedAngle(F)F

    move-result v1

    .line 223
    .local v1, "a":F
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_b
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mAbsoluteAngles:[F

    array-length v0, v0

    if-ge v2, v0, :cond_1c

    .line 224
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mAbsoluteAngles:[F

    aget v0, v0, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_19

    .line 225
    return v2

    .line 223
    :cond_19
    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    .line 228
    .end local v2    # "i":I
    :cond_1c
    const/4 v0, -0x1

    return v0
.end method

.method protected getMarkerPosition(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)[F
    .registers 4
    .param p1, "e"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    .param p2, "highlight"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    .line 121
    const/4 v0, 0x0

    new-array v0, v0, [F

    return-object v0
.end method

.method public getRadius()F
    .registers 4

    .line 378
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCircleBox:Landroid/graphics/RectF;

    if-nez v0, :cond_6

    .line 379
    const/4 v0, 0x0

    return v0

    .line 381
    :cond_6
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCircleBox:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCircleBox:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method protected getRequiredBaseOffset()F
    .registers 2

    .line 373
    const/4 v0, 0x0

    return v0
.end method

.method protected getRequiredBottomOffset()F
    .registers 3

    .line 368
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mLegendRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->getLabelPaint()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Paint;->getTextSize()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    return v0
.end method

.method public getTransparentCircleRadius()F
    .registers 2

    .line 472
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mTransparentCircleRadiusPercent:F

    return v0
.end method

.method protected init()V
    .registers 4

    .line 91
    invoke-super {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->init()V

    .line 93
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mAnimator:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-direct {v0, p0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    .line 94
    return-void
.end method

.method public isCenterTextWordWrapEnabled()Z
    .registers 2

    .line 538
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCenterTextWordWrapEnabled:Z

    return v0
.end method

.method public isDrawCenterTextEnabled()Z
    .registers 2

    .line 363
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawCenterText:Z

    return v0
.end method

.method public isDrawHoleEnabled()Z
    .registers 2

    .line 325
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawHole:Z

    return v0
.end method

.method public isDrawRoundedSlicesEnabled()Z
    .registers 2

    .line 500
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawRoundedSlices:Z

    return v0
.end method

.method public isDrawSliceTextEnabled()Z
    .registers 2

    .line 490
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawXLabels:Z

    return v0
.end method

.method public isHoleTransparent()Z
    .registers 2

    .line 306
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->getPaintHole()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Paint;->getXfermode()Landroid/graphics/Xfermode;

    move-result-object v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    goto :goto_11

    :cond_10
    const/4 v0, 0x0

    :goto_11
    return v0
.end method

.method public isUsePercentValuesEnabled()Z
    .registers 2

    .line 520
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mUsePercentValues:Z

    return v0
.end method

.method public needsHighlight(II)Z
    .registers 5
    .param p1, "xIndex"    # I
    .param p2, "dataSetIndex"    # I

    .line 194
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->valuesToHighlight()Z

    move-result v0

    if-eqz v0, :cond_8

    if-gez p2, :cond_a

    .line 195
    :cond_8
    const/4 v0, 0x0

    return v0

    .line 197
    :cond_a
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_b
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mIndicesToHightlight:[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    array-length v0, v0

    if-ge v1, v0, :cond_29

    .line 200
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mIndicesToHightlight:[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->getXIndex()I

    move-result v0

    if-ne v0, p1, :cond_26

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mIndicesToHightlight:[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    aget-object v0, v0, v1

    .line 201
    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->getDataSetIndex()I

    move-result v0

    if-ne v0, p2, :cond_26

    .line 202
    const/4 v0, 0x1

    return v0

    .line 197
    :cond_26
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    .line 204
    .end local v1    # "i":I
    :cond_29
    const/4 v0, 0x0

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 98
    invoke-super {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->onDraw(Landroid/graphics/Canvas;)V

    .line 100
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDataNotSet:Z

    if-eqz v0, :cond_8

    .line 101
    return-void

    .line 103
    :cond_8
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    invoke-virtual {v0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->drawData(Landroid/graphics/Canvas;)V

    .line 105
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->valuesToHighlight()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 106
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mIndicesToHightlight:[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    invoke-virtual {v0, p1, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->drawHighlighted(Landroid/graphics/Canvas;[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)V

    .line 108
    :cond_1a
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    invoke-virtual {v0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->drawExtras(Landroid/graphics/Canvas;)V

    .line 110
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    invoke-virtual {v0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;->drawValues(Landroid/graphics/Canvas;)V

    .line 112
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mLegendRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;

    invoke-virtual {v0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->renderLegend(Landroid/graphics/Canvas;)V

    .line 114
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->drawDescription(Landroid/graphics/Canvas;)V

    .line 117
    return-void
.end method

.method public setCenterText(Ljava/lang/String;)V
    .registers 2
    .param p1, "text"    # Ljava/lang/String;

    .line 335
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCenterText:Ljava/lang/String;

    .line 336
    return-void
.end method

.method public setCenterTextColor(I)V
    .registers 3
    .param p1, "color"    # I

    .line 434
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->getPaintCenterText()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    .line 435
    return-void
.end method

.method public setCenterTextRadiusPercent(F)V
    .registers 2
    .param p1, "percent"    # F

    .line 546
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCenterTextRadiusPercent:F

    .line 547
    return-void
.end method

.method public setCenterTextSize(F)V
    .registers 4
    .param p1, "sizeDp"    # F

    .line 416
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->getPaintCenterText()Landroid/text/TextPaint;

    move-result-object v0

    .line 417
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v1

    .line 416
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 418
    return-void
.end method

.method public setCenterTextSizePixels(F)V
    .registers 3
    .param p1, "sizePixels"    # F

    .line 425
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->getPaintCenterText()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 426
    return-void
.end method

.method public setCenterTextTypeface(Landroid/graphics/Typeface;)V
    .registers 3
    .param p1, "t"    # Landroid/graphics/Typeface;

    .line 408
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->getPaintCenterText()Landroid/text/TextPaint;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 409
    return-void
.end method

.method public setCenterTextWordWrapEnabled(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 529
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mCenterTextWordWrapEnabled:Z

    .line 530
    return-void
.end method

.method public setDrawCenterText(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 354
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawCenterText:Z

    .line 355
    return-void
.end method

.method public setDrawHoleEnabled(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 315
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawHole:Z

    .line 316
    return-void
.end method

.method public setDrawSliceText(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 481
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mDrawXLabels:Z

    .line 482
    return-void
.end method

.method public setHoleColor(I)V
    .registers 4
    .param p1, "color"    # I

    .line 278
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->getPaintHole()Landroid/graphics/Paint;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 279
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->getPaintHole()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 280
    return-void
.end method

.method public setHoleColorTransparent(Z)V
    .registers 5
    .param p1, "enable"    # Z

    .line 290
    if-eqz p1, :cond_21

    .line 291
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->getPaintHole()Landroid/graphics/Paint;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 292
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->getPaintHole()Landroid/graphics/Paint;

    move-result-object v0

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    goto :goto_2d

    .line 295
    :cond_21
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->getPaintHole()Landroid/graphics/Paint;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 297
    :goto_2d
    return-void
.end method

.method public setHoleRadius(F)V
    .registers 2
    .param p1, "percent"    # F

    .line 443
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mHoleRadiusPercent:F

    .line 444
    return-void
.end method

.method public setTransparentCircleColor(I)V
    .registers 3
    .param p1, "color"    # I

    .line 456
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/DataRenderer;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/PieChartRenderer;->getPaintTransparentCircle()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 457
    return-void
.end method

.method public setTransparentCircleRadius(F)V
    .registers 2
    .param p1, "percent"    # F

    .line 468
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mTransparentCircleRadiusPercent:F

    .line 469
    return-void
.end method

.method public setUsePercentValues(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 511
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;->mUsePercentValues:Z

    .line 512
    return-void
.end method

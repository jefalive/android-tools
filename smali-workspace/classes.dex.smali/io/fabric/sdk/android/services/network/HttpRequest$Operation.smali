.class public abstract Lio/fabric/sdk/android/services/network/HttpRequest$Operation;
.super Ljava/lang/Object;
.source "HttpRequest.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/fabric/sdk/android/services/network/HttpRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40c
    name = "Operation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:Ljava/lang/Object;>Ljava/lang/Object;Ljava/util/concurrent/Callable<TV;>;"
    }
.end annotation


# direct methods
.method protected constructor <init>()V
    .registers 1

    .line 569
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call()Ljava/lang/Object;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/fabric/sdk/android/services/network/HttpRequest$HttpRequestException;
        }
    .end annotation

    .line 588
    const/4 v1, 0x0

    .line 590
    .local v1, "thrown":Z
    :try_start_1
    invoke-virtual {p0}, Lio/fabric/sdk/android/services/network/HttpRequest$Operation;->run()Ljava/lang/Object;
    :try_end_4
    .catch Lio/fabric/sdk/android/services/network/HttpRequest$HttpRequestException; {:try_start_1 .. :try_end_4} :catch_13
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_4} :catch_16
    .catchall {:try_start_1 .. :try_end_4} :catchall_1e

    move-result-object v2

    .line 599
    :try_start_5
    invoke-virtual {p0}, Lio/fabric/sdk/android/services/network/HttpRequest$Operation;->done()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_8} :catch_9

    .line 603
    goto :goto_12

    .line 600
    :catch_9
    move-exception v3

    .line 601
    .local v3, "e":Ljava/io/IOException;
    if-nez v1, :cond_12

    .line 602
    new-instance v0, Lio/fabric/sdk/android/services/network/HttpRequest$HttpRequestException;

    invoke-direct {v0, v3}, Lio/fabric/sdk/android/services/network/HttpRequest$HttpRequestException;-><init>(Ljava/io/IOException;)V

    throw v0

    .line 603
    .end local v3    # "e":Ljava/io/IOException;
    :cond_12
    :goto_12
    return-object v2

    .line 591
    :catch_13
    move-exception v2

    .line 592
    .local v2, "e":Lio/fabric/sdk/android/services/network/HttpRequest$HttpRequestException;
    const/4 v1, 0x1

    .line 593
    :try_start_15
    throw v2

    .line 594
    .end local v2    # "e":Lio/fabric/sdk/android/services/network/HttpRequest$HttpRequestException;
    :catch_16
    move-exception v2

    .line 595
    .local v2, "e":Ljava/io/IOException;
    const/4 v1, 0x1

    .line 596
    new-instance v0, Lio/fabric/sdk/android/services/network/HttpRequest$HttpRequestException;

    invoke-direct {v0, v2}, Lio/fabric/sdk/android/services/network/HttpRequest$HttpRequestException;-><init>(Ljava/io/IOException;)V

    throw v0
    :try_end_1e
    .catchall {:try_start_15 .. :try_end_1e} :catchall_1e

    .line 598
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_1e
    move-exception v4

    .line 599
    :try_start_1f
    invoke-virtual {p0}, Lio/fabric/sdk/android/services/network/HttpRequest$Operation;->done()V
    :try_end_22
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_22} :catch_23

    .line 603
    goto :goto_2c

    .line 600
    :catch_23
    move-exception v5

    .line 601
    .local v5, "e":Ljava/io/IOException;
    if-nez v1, :cond_2c

    .line 602
    new-instance v0, Lio/fabric/sdk/android/services/network/HttpRequest$HttpRequestException;

    invoke-direct {v0, v5}, Lio/fabric/sdk/android/services/network/HttpRequest$HttpRequestException;-><init>(Ljava/io/IOException;)V

    throw v0

    .line 603
    .end local v5    # "e":Ljava/io/IOException;
    :cond_2c
    :goto_2c
    throw v4
.end method

.method protected abstract done()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected abstract run()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lio/fabric/sdk/android/services/network/HttpRequest$HttpRequestException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

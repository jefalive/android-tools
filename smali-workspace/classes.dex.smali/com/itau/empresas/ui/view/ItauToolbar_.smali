.class public final Lcom/itau/empresas/ui/view/ItauToolbar_;
.super Lcom/itau/empresas/ui/view/ItauToolbar;
.source "ItauToolbar_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 38
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/ItauToolbar;-><init>(Landroid/content/Context;)V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->alreadyInflated_:Z

    .line 35
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 39
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/ItauToolbar_;->init_()V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 43
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/view/ItauToolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->alreadyInflated_:Z

    .line 35
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 44
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/ItauToolbar_;->init_()V

    .line 45
    return-void
.end method

.method private init_()V
    .registers 3

    .line 70
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 71
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 72
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 73
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 61
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->alreadyInflated_:Z

    .line 63
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ItauToolbar_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030174

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/ui/view/ItauToolbar_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 64
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 66
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/ui/view/ItauToolbar;->onFinishInflate()V

    .line 67
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 83
    const v0, 0x7f0e0653

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/CollapsingToolbarLayout;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->mCollapsingToolbar:Landroid/support/design/widget/CollapsingToolbarLayout;

    .line 84
    const v0, 0x7f0e035b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->mRelativePai:Landroid/widget/RelativeLayout;

    .line 85
    const v0, 0x7f0e0654

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/TextViewIcon;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->mSetaCima:Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 86
    const v0, 0x7f0e0354

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/TextViewIcon;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->mImagemLogo:Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 87
    const v0, 0x7f0e0655

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->mRelativeFilho:Landroid/widget/RelativeLayout;

    .line 88
    const v0, 0x7f0e00c9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->mTitulo:Landroid/widget/TextView;

    .line 89
    const v0, 0x7f0e0656

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/TextViewIcon;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->mSetaBaixo:Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 90
    const v0, 0x7f0e035d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->mSubtitulo1:Landroid/widget/TextView;

    .line 91
    const v0, 0x7f0e035e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->mSubtitulo2:Landroid/widget/TextView;

    .line 92
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->mSetaCima:Lcom/itau/empresas/ui/view/TextViewIcon;

    if-eqz v0, :cond_71

    .line 93
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->mSetaCima:Lcom/itau/empresas/ui/view/TextViewIcon;

    new-instance v1, Lcom/itau/empresas/ui/view/ItauToolbar_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/ItauToolbar_$1;-><init>(Lcom/itau/empresas/ui/view/ItauToolbar_;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/TextViewIcon;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    :cond_71
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->mRelativeFilho:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_7f

    .line 103
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->mRelativeFilho:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/itau/empresas/ui/view/ItauToolbar_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/ItauToolbar_$2;-><init>(Lcom/itau/empresas/ui/view/ItauToolbar_;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    :cond_7f
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->mCollapsingToolbar:Landroid/support/design/widget/CollapsingToolbarLayout;

    if-eqz v0, :cond_8d

    .line 113
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar_;->mCollapsingToolbar:Landroid/support/design/widget/CollapsingToolbarLayout;

    new-instance v1, Lcom/itau/empresas/ui/view/ItauToolbar_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/ItauToolbar_$3;-><init>(Lcom/itau/empresas/ui/view/ItauToolbar_;)V

    invoke-virtual {v0, v1}, Landroid/support/design/widget/CollapsingToolbarLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    :cond_8d
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ItauToolbar_;->afterViews()V

    .line 123
    return-void
.end method

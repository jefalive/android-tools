.class public final Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;
.super Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;
.source "SeletorDataPagamentoView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 37
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;-><init>(Landroid/content/Context;)V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->alreadyInflated_:Z

    .line 34
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 38
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->init_()V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 42
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->alreadyInflated_:Z

    .line 34
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 43
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->init_()V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->alreadyInflated_:Z

    .line 34
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 48
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->init_()V

    .line 49
    return-void
.end method

.method private init_()V
    .registers 3

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 75
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 76
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 77
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 65
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->alreadyInflated_:Z

    .line 67
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030182

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 70
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->onFinishInflate()V

    .line 71
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 93
    const v0, 0x7f0e0684

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->botaoMenos:Landroid/widget/Button;

    .line 94
    const v0, 0x7f0e0686

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->botaoMais:Landroid/widget/Button;

    .line 95
    const v0, 0x7f0e0685

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->diaSelecionado:Landroid/widget/EditText;

    .line 96
    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->botaoMais:Landroid/widget/Button;

    if-eqz v0, :cond_2f

    .line 97
    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->botaoMais:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_$1;-><init>(Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    :cond_2f
    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->botaoMenos:Landroid/widget/Button;

    if-eqz v0, :cond_3d

    .line 107
    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->botaoMenos:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_$2;-><init>(Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    :cond_3d
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView_;->aoCarregarTela()V

    .line 117
    return-void
.end method

.class public Landroid/support/v4/widget/ListViewAutoScrollHelper;
.super Landroid/support/v4/widget/AutoScrollHelper;
.source "ListViewAutoScrollHelper.java"


# instance fields
.field private final mTarget:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(Landroid/widget/ListView;)V
    .registers 2
    .param p1, "target"    # Landroid/widget/ListView;

    .line 30
    invoke-direct {p0, p1}, Landroid/support/v4/widget/AutoScrollHelper;-><init>(Landroid/view/View;)V

    .line 32
    iput-object p1, p0, Landroid/support/v4/widget/ListViewAutoScrollHelper;->mTarget:Landroid/widget/ListView;

    .line 33
    return-void
.end method


# virtual methods
.method public canTargetScrollHorizontally(I)Z
    .registers 3
    .param p1, "direction"    # I

    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public canTargetScrollVertically(I)Z
    .registers 10
    .param p1, "direction"    # I

    .line 48
    iget-object v2, p0, Landroid/support/v4/widget/ListViewAutoScrollHelper;->mTarget:Landroid/widget/ListView;

    .line 49
    .local v2, "target":Landroid/widget/ListView;
    invoke-virtual {v2}, Landroid/widget/ListView;->getCount()I

    move-result v3

    .line 50
    .local v3, "itemCount":I
    if-nez v3, :cond_a

    .line 51
    const/4 v0, 0x0

    return v0

    .line 54
    :cond_a
    invoke-virtual {v2}, Landroid/widget/ListView;->getChildCount()I

    move-result v4

    .line 55
    .local v4, "childCount":I
    invoke-virtual {v2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v5

    .line 56
    .local v5, "firstPosition":I
    add-int v6, v5, v4

    .line 58
    .local v6, "lastPosition":I
    if-lez p1, :cond_2b

    .line 60
    if-lt v6, v3, :cond_3f

    .line 61
    add-int/lit8 v0, v4, -0x1

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 62
    .local v7, "lastView":Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {v2}, Landroid/widget/ListView;->getHeight()I

    move-result v1

    if-gt v0, v1, :cond_2a

    .line 63
    const/4 v0, 0x0

    return v0

    .line 65
    .end local v7    # "lastView":Landroid/view/View;
    :cond_2a
    goto :goto_3f

    .line 66
    :cond_2b
    if-gez p1, :cond_3d

    .line 68
    if-gtz v5, :cond_3f

    .line 69
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 70
    .local v7, "firstView":Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v0

    if-ltz v0, :cond_3c

    .line 71
    const/4 v0, 0x0

    return v0

    .line 73
    .end local v7    # "firstView":Landroid/view/View;
    :cond_3c
    goto :goto_3f

    .line 77
    :cond_3d
    const/4 v0, 0x0

    return v0

    .line 80
    :cond_3f
    :goto_3f
    const/4 v0, 0x1

    return v0
.end method

.method public scrollTargetBy(II)V
    .registers 4
    .param p1, "deltaX"    # I
    .param p2, "deltaY"    # I

    .line 37
    iget-object v0, p0, Landroid/support/v4/widget/ListViewAutoScrollHelper;->mTarget:Landroid/widget/ListView;

    invoke-static {v0, p2}, Landroid/support/v4/widget/ListViewCompat;->scrollListBy(Landroid/widget/ListView;I)V

    .line 38
    return-void
.end method

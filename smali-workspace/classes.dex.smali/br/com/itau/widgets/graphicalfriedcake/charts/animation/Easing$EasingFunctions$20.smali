.class final Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions$20;
.super Ljava/lang/Object;
.source "Easing.java"

# interfaces
.implements Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .line 418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .registers 10
    .param p1, "input"    # F

    .line 443
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_7

    .line 445
    const/4 v0, 0x0

    return v0

    .line 448
    :cond_7
    move v5, p1

    .line 449
    .local v5, "position":F
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, v5, v0

    if-nez v0, :cond_11

    .line 451
    const/high16 v0, 0x3f800000    # 1.0f

    return v0

    .line 454
    :cond_11
    const v6, 0x3e99999a    # 0.3f

    .line 455
    .local v6, "p":F
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v1}, Ljava/lang/Math;->asin(D)D

    move-result-wide v0

    double-to-float v0, v0

    const v1, 0x3d4391d1

    mul-float v7, v1, v0

    .line 456
    .local v7, "s":F
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v0, v5, v0

    move v5, v0

    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v0, v1

    float-to-double v0, v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    sub-float v1, v5, v7

    float-to-double v1, v1

    const-wide v3, 0x401921fb54442d18L    # 6.283185307179586

    mul-double/2addr v1, v3

    const-wide v3, 0x3fd3333340000000L    # 0.30000001192092896

    div-double/2addr v1, v3

    .line 458
    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v1

    double-to-float v1, v1

    mul-float/2addr v0, v1

    neg-float v0, v0

    return v0
.end method

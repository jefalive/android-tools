.class Lcom/itau/empresas/controller/PagamentoDarfController$3;
.super Ljava/lang/Object;
.source "PagamentoDarfController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/controller/PagamentoDarfController;->incluiDarf(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/controller/PagamentoDarfController;

.field final synthetic val$darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;


# direct methods
.method constructor <init>(Lcom/itau/empresas/controller/PagamentoDarfController;Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/controller/PagamentoDarfController;

    .line 83
    iput-object p1, p0, Lcom/itau/empresas/controller/PagamentoDarfController$3;->this$0:Lcom/itau/empresas/controller/PagamentoDarfController;

    iput-object p2, p0, Lcom/itau/empresas/controller/PagamentoDarfController$3;->val$darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 3

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoDarfController$3;->this$0:Lcom/itau/empresas/controller/PagamentoDarfController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/PagamentoDarfController;->api()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/controller/PagamentoDarfController$3;->val$darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    invoke-interface {v0, v1}, Lcom/itau/empresas/api/Api;->simularDarf(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoDarfController$3;->this$0:Lcom/itau/empresas/controller/PagamentoDarfController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/PagamentoDarfController;->api()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    invoke-interface {v0}, Lcom/itau/empresas/api/Api;->incluiDarf()Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/BaseController;->post(Ljava/lang/Object;)V

    .line 89
    return-void
.end method

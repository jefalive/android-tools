.class final Lokio/Segment;
.super Ljava/lang/Object;
.source "Segment.java"


# instance fields
.field final data:[B

.field limit:I

.field next:Lokio/Segment;

.field owner:Z

.field pos:I

.field prev:Lokio/Segment;

.field shared:Z


# direct methods
.method constructor <init>()V
    .registers 2

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const/16 v0, 0x2000

    new-array v0, v0, [B

    iput-object v0, p0, Lokio/Segment;->data:[B

    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lokio/Segment;->owner:Z

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lokio/Segment;->shared:Z

    .line 64
    return-void
.end method

.method constructor <init>(Lokio/Segment;)V
    .registers 5
    .param p1, "shareFrom"    # Lokio/Segment;

    .line 67
    iget-object v0, p1, Lokio/Segment;->data:[B

    iget v1, p1, Lokio/Segment;->pos:I

    iget v2, p1, Lokio/Segment;->limit:I

    invoke-direct {p0, v0, v1, v2}, Lokio/Segment;-><init>([BII)V

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p1, Lokio/Segment;->shared:Z

    .line 69
    return-void
.end method

.method constructor <init>([BII)V
    .registers 5
    .param p1, "data"    # [B
    .param p2, "pos"    # I
    .param p3, "limit"    # I

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Lokio/Segment;->data:[B

    .line 73
    iput p2, p0, Lokio/Segment;->pos:I

    .line 74
    iput p3, p0, Lokio/Segment;->limit:I

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lokio/Segment;->owner:Z

    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, Lokio/Segment;->shared:Z

    .line 77
    return-void
.end method


# virtual methods
.method public compact()V
    .registers 5

    .line 139
    iget-object v0, p0, Lokio/Segment;->prev:Lokio/Segment;

    if-ne v0, p0, :cond_a

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 140
    :cond_a
    iget-object v0, p0, Lokio/Segment;->prev:Lokio/Segment;

    iget-boolean v0, v0, Lokio/Segment;->owner:Z

    if-nez v0, :cond_11

    return-void

    .line 141
    :cond_11
    iget v0, p0, Lokio/Segment;->limit:I

    iget v1, p0, Lokio/Segment;->pos:I

    sub-int v2, v0, v1

    .line 142
    .local v2, "byteCount":I
    iget-object v0, p0, Lokio/Segment;->prev:Lokio/Segment;

    iget v0, v0, Lokio/Segment;->limit:I

    rsub-int v0, v0, 0x2000

    iget-object v1, p0, Lokio/Segment;->prev:Lokio/Segment;

    iget-boolean v1, v1, Lokio/Segment;->shared:Z

    if-eqz v1, :cond_25

    const/4 v1, 0x0

    goto :goto_29

    :cond_25
    iget-object v1, p0, Lokio/Segment;->prev:Lokio/Segment;

    iget v1, v1, Lokio/Segment;->pos:I

    :goto_29
    add-int v3, v0, v1

    .line 143
    .local v3, "availableByteCount":I
    if-le v2, v3, :cond_2e

    return-void

    .line 144
    :cond_2e
    iget-object v0, p0, Lokio/Segment;->prev:Lokio/Segment;

    invoke-virtual {p0, v0, v2}, Lokio/Segment;->writeTo(Lokio/Segment;I)V

    .line 145
    invoke-virtual {p0}, Lokio/Segment;->pop()Lokio/Segment;

    .line 146
    invoke-static {p0}, Lokio/SegmentPool;->recycle(Lokio/Segment;)V

    .line 147
    return-void
.end method

.method public pop()Lokio/Segment;
    .registers 4

    .line 84
    iget-object v0, p0, Lokio/Segment;->next:Lokio/Segment;

    if-eq v0, p0, :cond_7

    iget-object v2, p0, Lokio/Segment;->next:Lokio/Segment;

    goto :goto_8

    :cond_7
    const/4 v2, 0x0

    .line 85
    .local v2, "result":Lokio/Segment;
    :goto_8
    iget-object v0, p0, Lokio/Segment;->prev:Lokio/Segment;

    iget-object v1, p0, Lokio/Segment;->next:Lokio/Segment;

    iput-object v1, v0, Lokio/Segment;->next:Lokio/Segment;

    .line 86
    iget-object v0, p0, Lokio/Segment;->next:Lokio/Segment;

    iget-object v1, p0, Lokio/Segment;->prev:Lokio/Segment;

    iput-object v1, v0, Lokio/Segment;->prev:Lokio/Segment;

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lokio/Segment;->next:Lokio/Segment;

    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lokio/Segment;->prev:Lokio/Segment;

    .line 89
    return-object v2
.end method

.method public push(Lokio/Segment;)Lokio/Segment;
    .registers 3
    .param p1, "segment"    # Lokio/Segment;

    .line 97
    iput-object p0, p1, Lokio/Segment;->prev:Lokio/Segment;

    .line 98
    iget-object v0, p0, Lokio/Segment;->next:Lokio/Segment;

    iput-object v0, p1, Lokio/Segment;->next:Lokio/Segment;

    .line 99
    iget-object v0, p0, Lokio/Segment;->next:Lokio/Segment;

    iput-object p1, v0, Lokio/Segment;->prev:Lokio/Segment;

    .line 100
    iput-object p1, p0, Lokio/Segment;->next:Lokio/Segment;

    .line 101
    return-object p1
.end method

.method public split(I)Lokio/Segment;
    .registers 7
    .param p1, "byteCount"    # I

    .line 113
    if-lez p1, :cond_9

    iget v0, p0, Lokio/Segment;->limit:I

    iget v1, p0, Lokio/Segment;->pos:I

    sub-int/2addr v0, v1

    if-le p1, v0, :cond_f

    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 121
    :cond_f
    const/16 v0, 0x400

    if-lt p1, v0, :cond_19

    .line 122
    new-instance v4, Lokio/Segment;

    invoke-direct {v4, p0}, Lokio/Segment;-><init>(Lokio/Segment;)V

    .local v4, "prefix":Lokio/Segment;
    goto :goto_27

    .line 124
    .end local v4    # "prefix":Lokio/Segment;
    :cond_19
    invoke-static {}, Lokio/SegmentPool;->take()Lokio/Segment;

    move-result-object v4

    .line 125
    .local v4, "prefix":Lokio/Segment;
    iget-object v0, p0, Lokio/Segment;->data:[B

    iget v1, p0, Lokio/Segment;->pos:I

    iget-object v2, v4, Lokio/Segment;->data:[B

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 128
    :goto_27
    iget v0, v4, Lokio/Segment;->pos:I

    add-int/2addr v0, p1

    iput v0, v4, Lokio/Segment;->limit:I

    .line 129
    iget v0, p0, Lokio/Segment;->pos:I

    add-int/2addr v0, p1

    iput v0, p0, Lokio/Segment;->pos:I

    .line 130
    iget-object v0, p0, Lokio/Segment;->prev:Lokio/Segment;

    invoke-virtual {v0, v4}, Lokio/Segment;->push(Lokio/Segment;)Lokio/Segment;

    .line 131
    return-object v4
.end method

.method public writeTo(Lokio/Segment;I)V
    .registers 8
    .param p1, "sink"    # Lokio/Segment;
    .param p2, "byteCount"    # I

    .line 151
    iget-boolean v0, p1, Lokio/Segment;->owner:Z

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 152
    :cond_a
    iget v0, p1, Lokio/Segment;->limit:I

    add-int/2addr v0, p2

    const/16 v1, 0x2000

    if-le v0, v1, :cond_44

    .line 154
    iget-boolean v0, p1, Lokio/Segment;->shared:Z

    if-eqz v0, :cond_1b

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 155
    :cond_1b
    iget v0, p1, Lokio/Segment;->limit:I

    add-int/2addr v0, p2

    iget v1, p1, Lokio/Segment;->pos:I

    sub-int/2addr v0, v1

    const/16 v1, 0x2000

    if-le v0, v1, :cond_2b

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 156
    :cond_2b
    iget-object v0, p1, Lokio/Segment;->data:[B

    iget v1, p1, Lokio/Segment;->pos:I

    iget-object v2, p1, Lokio/Segment;->data:[B

    iget v3, p1, Lokio/Segment;->limit:I

    iget v4, p1, Lokio/Segment;->pos:I

    sub-int/2addr v3, v4

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 157
    iget v0, p1, Lokio/Segment;->limit:I

    iget v1, p1, Lokio/Segment;->pos:I

    sub-int/2addr v0, v1

    iput v0, p1, Lokio/Segment;->limit:I

    .line 158
    const/4 v0, 0x0

    iput v0, p1, Lokio/Segment;->pos:I

    .line 161
    :cond_44
    iget-object v0, p0, Lokio/Segment;->data:[B

    iget v1, p0, Lokio/Segment;->pos:I

    iget-object v2, p1, Lokio/Segment;->data:[B

    iget v3, p1, Lokio/Segment;->limit:I

    invoke-static {v0, v1, v2, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 162
    iget v0, p1, Lokio/Segment;->limit:I

    add-int/2addr v0, p2

    iput v0, p1, Lokio/Segment;->limit:I

    .line 163
    iget v0, p0, Lokio/Segment;->pos:I

    add-int/2addr v0, p2

    iput v0, p0, Lokio/Segment;->pos:I

    .line 164
    return-void
.end method

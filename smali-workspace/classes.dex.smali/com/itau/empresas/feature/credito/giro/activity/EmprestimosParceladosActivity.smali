.class public Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "EmprestimosParceladosActivity.java"

# interfaces
.implements Landroid/support/design/widget/TabLayout$OnTabSelectedListener;
.implements Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity$SectionsPagerAdapter;
    }
.end annotation


# instance fields
.field creditoSimplificadoTab:Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;

.field creditoSimplificadoViewpager:Lcom/itau/empresas/ui/util/CustomViewPager;

.field private fragmentContratados:Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;

.field private fragmentCreditoSimplificado:Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;

.field ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

.field private ofertaProdutosParceladosSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

.field root:Landroid/widget/LinearLayout;

.field simulacaoController:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;

.field simulacaoPropostaController:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController;

.field private simulacaoPropostaPersonalizada:Z

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field private topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 44
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;)Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->fragmentCreditoSimplificado:Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;

    return-object v0
.end method

.method static synthetic access$002(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;)Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;
    .param p1, "x1"    # Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;

    .line 44
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->fragmentCreditoSimplificado:Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;

    return-object p1
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;)Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->fragmentContratados:Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;

    return-object v0
.end method

.method static synthetic access$102(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;)Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;
    .param p1, "x1"    # Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;

    .line 44
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->fragmentContratados:Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;

    return-object p1
.end method

.method private buscaSimulacao()V
    .registers 3

    .line 210
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->dismissSnackbar()V

    .line 212
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->mostraDialogoDeProgresso()V

    .line 214
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->simulacaoController:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;->consultaSimulacao(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)V

    .line 215
    return-void
.end method

.method private buscaSimulacaoProposta()V
    .registers 3

    .line 218
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->dismissSnackbar()V

    .line 220
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->mostraDialogoDeProgresso()V

    .line 222
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->simulacaoPropostaController:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController;->consultarSimulacaoProposta(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)V

    .line 223
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 77
    const v1, 0x7f0701ca

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 80
    return-void
.end method

.method private setupViewPager()V
    .registers 4

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->creditoSimplificadoViewpager:Lcom/itau/empresas/ui/util/CustomViewPager;

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity$SectionsPagerAdapter;

    .line 84
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity$SectionsPagerAdapter;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;Landroid/support/v4/app/FragmentManager;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/CustomViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 85
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->creditoSimplificadoTab:Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->creditoSimplificadoViewpager:Lcom/itau/empresas/ui/util/CustomViewPager;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 86
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->creditoSimplificadoTab:Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;

    invoke-virtual {v0, p0}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->setOnTabSelectedListener(Landroid/support/design/widget/TabLayout$OnTabSelectedListener;)V

    .line 87
    return-void
.end method

.method private validarRespostaDaSimulacaoProposta(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;)Z
    .registers 5
    .param p1, "propostaSaidaVO"    # Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 264
    const/4 v2, 0x1

    .line 266
    .local v2, "valido":Z
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->simulacaoPropostaPersonalizada:Z

    if-eqz v0, :cond_21

    .line 267
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getValoresEmprestimos()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 268
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getValoresEmprestimos()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_21

    .line 270
    :cond_15
    const v0, 0x7f070622

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->showSnackbar(Ljava/lang/CharSequence;I)V

    .line 274
    const/4 v2, 0x0

    .line 277
    :cond_21
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->simulacaoPropostaPersonalizada:Z

    if-nez v0, :cond_49

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 278
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getDataPrimeiroPagamento()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_49

    .line 279
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getParcelasEmprestimoVO()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_3d

    .line 280
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getParcelasEmprestimoVO()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_49

    .line 282
    :cond_3d
    const v0, 0x7f070622

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->showSnackbar(Ljava/lang/CharSequence;I)V

    .line 286
    const/4 v2, 0x0

    .line 289
    :cond_49
    return v2
.end method


# virtual methods
.method aoCarregarElementosDeTela()V
    .registers 3

    .line 69
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->constroiToolbar()V

    .line 70
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->setupViewPager()V

    .line 71
    new-instance v0, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->root:Landroid/widget/LinearLayout;

    invoke-direct {v0, v1}, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;

    .line 72
    return-void
.end method

.method public dismissSnackbar()V
    .registers 2

    .line 293
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;->dismissSnackbar()V

    .line 294
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;

    .line 118
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 119
    return-void

    .line 122
    :cond_7
    const-string v0, "ItauEmpresas"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Erro na Chamanda: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 123
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->escondeDialogoDeProgresso()V

    .line 124
    const v0, 0x7f0701f2

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->showSnackbar(Ljava/lang/CharSequence;I)V

    .line 126
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 6
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 131
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v3

    .line 132
    .local v3, "exception":Lbr/com/itau/sdk/android/core/exception/BackendException;
    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ofertasCreditosParcelados"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 133
    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getStatusCode()I

    move-result v0

    const/16 v1, 0x1a6

    if-ne v0, v1, :cond_4f

    .line 134
    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4f

    .line 135
    const v0, 0x7f070531

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 136
    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4f

    .line 138
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->fragmentCreditoSimplificado:Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;

    .line 139
    const v1, 0x7f07055f

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/fragment/CreditoSimplificadoFragment;->mostraEstadoVazio(Ljava/lang/String;)V

    .line 142
    :cond_4f
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_56

    .line 143
    return-void

    .line 146
    :cond_56
    const-string v0, "ItauEmpresas"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Erro na Chamanda: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 147
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->escondeDialogoDeProgresso()V

    .line 148
    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->showSnackbar(Ljava/lang/CharSequence;I)V

    .line 150
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoBuscaSimulacaoEspecialOuPersonalizada;)V
    .registers 5
    .param p1, "eventoBuscaSimulacaoEspecialOuPersonalizada"    # Lcom/itau/empresas/Evento$EventoBuscaSimulacaoEspecialOuPersonalizada;

    .line 161
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->dismissSnackbar()V

    .line 163
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->mostraDialogoDeProgresso()V

    .line 165
    .line 166
    invoke-virtual {p1}, Lcom/itau/empresas/Evento$EventoBuscaSimulacaoEspecialOuPersonalizada;->getPersonalizado()Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->simulacaoPropostaPersonalizada:Z

    .line 168
    .line 169
    invoke-virtual {p1}, Lcom/itau/empresas/Evento$EventoBuscaSimulacaoEspecialOuPersonalizada;->getOfertaProdutosParceladosSaidaVO()Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaProdutosParceladosSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    .line 171
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->simulacaoPropostaPersonalizada:Z

    if-eqz v0, :cond_63

    .line 173
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-direct {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 174
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaProdutosParceladosSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->ajustaDados(Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;Z)V

    .line 175
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 177
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getConta()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x7

    invoke-static {v2, v1}, Lcom/itau/empresas/ui/util/ViewUtils;->formataAdicionandoZeros(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 176
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setConta(Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaProdutosParceladosSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    .line 179
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getPrazoMinimoContratacao()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 178
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setPrazoMinimoContratacao(Ljava/lang/String;)V

    .line 180
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaProdutosParceladosSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    .line 181
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getPrazoMaximoContratacao()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 180
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setPrazoMaximoContratacao(Ljava/lang/String;)V

    .line 182
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setValorEmprestimo(Ljava/math/BigDecimal;)V

    .line 183
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setDataPrimeiroPagamento(Ljava/lang/String;)V

    .line 185
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->buscaSimulacaoProposta()V

    goto :goto_c1

    .line 189
    :cond_63
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-direct {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 190
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaProdutosParceladosSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->ajustaDados(Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;Z)V

    .line 191
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 193
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getConta()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x7

    invoke-static {v2, v1}, Lcom/itau/empresas/ui/util/ViewUtils;->formataAdicionandoZeros(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 192
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setConta(Ljava/lang/String;)V

    .line 194
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaProdutosParceladosSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    .line 195
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getPrazoMinimoContratacao()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 194
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setPrazoMinimoContratacao(Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaProdutosParceladosSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    .line 197
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getPrazoMaximoContratacao()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 196
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setPrazoMaximoContratacao(Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaProdutosParceladosSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    .line 199
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getOfertaEspecialProdutosParceladosVO()Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;

    move-result-object v1

    .line 200
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getValorOferta()Ljava/math/BigDecimal;

    move-result-object v1

    .line 198
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setValorEmprestimo(Ljava/math/BigDecimal;)V

    .line 201
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaProdutosParceladosSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    .line 202
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getOfertaEspecialProdutosParceladosVO()Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;

    move-result-object v1

    .line 203
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getDataPrimeiroVencimento()Ljava/lang/String;

    move-result-object v1

    .line 201
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setDataPrimeiroPagamento(Ljava/lang/String;)V

    .line 205
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->buscaSimulacao()V

    .line 207
    :goto_c1
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;)V
    .registers 5
    .param p1, "propostaSaidaVO"    # Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 249
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->escondeDialogoDeProgresso()V

    .line 251
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->validarRespostaDaSimulacaoProposta(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;)Z

    move-result v2

    .line 253
    .local v2, "retornoValido":Z
    if-eqz v2, :cond_1e

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaProdutosParceladosSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    if-eqz v0, :cond_1e

    .line 255
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_$IntentBuilder_;

    move-result-object v0

    .line 256
    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_$IntentBuilder_;->produtosParceladosSimulacaoPropostaSaidaVO(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 257
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_$IntentBuilder_;->ofertaEspecialAceita(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_$IntentBuilder_;

    move-result-object v0

    .line 258
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 260
    :cond_1e
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;)V
    .registers 4
    .param p1, "simulacaoSaidaVO"    # Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    .line 232
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->escondeDialogoDeProgresso()V

    .line 234
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 235
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;->ofertaEspecialAceitaVO(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;

    move-result-object v0

    .line 236
    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;->simulacaoSaidaVO(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;

    move-result-object v0

    .line 237
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;->fluxoPersonalizado(Z)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;

    move-result-object v0

    .line 238
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getDadosSeguro()Lcom/itau/empresas/feature/credito/giro/model/SeguroProdutosParceladoSaidaVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/SeguroProdutosParceladoSaidaVO;->getIndicadorSeguro()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;->exibirSimulacao(Z)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;

    move-result-object v0

    .line 239
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 240
    return-void
.end method

.method public onTabReselected(Landroid/support/design/widget/TabLayout$Tab;)V
    .registers 2
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;

    .line 114
    return-void
.end method

.method public onTabSelected(Landroid/support/design/widget/TabLayout$Tab;)V
    .registers 4
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;

    .line 100
    invoke-virtual {p1}, Landroid/support/design/widget/TabLayout$Tab;->getPosition()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_10

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->fragmentContratados:Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;

    if-eqz v0, :cond_10

    .line 102
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->fragmentContratados:Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/fragment/EmprestimosParceladosContratadosFragment;->carregarElementosDeTela()V

    .line 104
    :cond_10
    return-void
.end method

.method public onTabUnselected(Landroid/support/design/widget/TabLayout$Tab;)V
    .registers 2
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;

    .line 109
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 91
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "ofertasCreditosParcelados"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "creditoParceladoPersonalizadoSimular"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "comprovantesReemissao"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "comprovanteIdReemissao"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "creditoParceladoSimular"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public showSnackbar(Ljava/lang/CharSequence;I)V
    .registers 4
    .param p1, "texto"    # Ljava/lang/CharSequence;
    .param p2, "duracao"    # I

    .line 297
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;

    invoke-virtual {v0, p1, p2}, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;->showSnackbar(Ljava/lang/CharSequence;I)V

    .line 298
    return-void
.end method

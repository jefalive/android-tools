.class public Lcom/itau/empresas/feature/credito/lis/model/LisConsultaComprovanteIdentificadorVO;
.super Ljava/lang/Object;
.source "LisConsultaComprovanteIdentificadorVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private camposComprovante:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "campos_comprovante"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;>;"
        }
    .end annotation
.end field

.field private idComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id_comprovante"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;
.super Ljava/lang/Object;
.source "DetalhesAutorizacaoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private agencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia"
    .end annotation
.end field

.field private autenticacaoComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "autenticacao_comprovante"
    .end annotation
.end field

.field private cnpj:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cnpj"
    .end annotation
.end field

.field private cnpjFavorecido:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cnpj_favorecido"
    .end annotation
.end field

.field private codigoMotivo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_motivo"
    .end annotation
.end field

.field private codigoPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_pagamento"
    .end annotation
.end field

.field private codigoReceita:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_receita"
    .end annotation
.end field

.field private codigoServico:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_servico"
    .end annotation
.end field

.field private competencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "competencia"
    .end annotation
.end field

.field private conta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta"
    .end annotation
.end field

.field private dataApuracao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_apuracao"
    .end annotation
.end field

.field private dataPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_pagamento"
    .end annotation
.end field

.field private dataVencimento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_vencimento"
    .end annotation
.end field

.field private descricaoMotivo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_motivo"
    .end annotation
.end field

.field private descricaoPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_pagamento"
    .end annotation
.end field

.field private identificacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "identificacao"
    .end annotation
.end field

.field private identificacaoComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "identificacao_comprovante"
    .end annotation
.end field

.field private indicadorComprovante:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_comprovante"
    .end annotation
.end field

.field private indicadorMotivo:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_motivo"
    .end annotation
.end field

.field private indicadorTipoPessoa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_tipo_pessoa"
    .end annotation
.end field

.field private layout:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "layout"
    .end annotation
.end field

.field private listaEtapas:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "lista_etapas"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;>;"
        }
    .end annotation
.end field

.field private modalidadePagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "modalidade_pagamento"
    .end annotation
.end field

.field private nomeContribuinte:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_contribuinte"
    .end annotation
.end field

.field private nomeEmpresa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_empresa"
    .end annotation
.end field

.field private numeroCarrinho:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_carrinho"
    .end annotation
.end field

.field private numeroLote:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_lote"
    .end annotation
.end field

.field private numeroPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_pagamento"
    .end annotation
.end field

.field private numeroReferencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_referencia"
    .end annotation
.end field

.field private percentual:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "percentual"
    .end annotation
.end field

.field private seuNumero:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "seu_numero"
    .end annotation
.end field

.field private situacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "situacao"
    .end annotation
.end field

.field private tipoPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_pagamento"
    .end annotation
.end field

.field private valorInss:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_inss"
    .end annotation
.end field

.field private valorJuros:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_juros"
    .end annotation
.end field

.field private valorMulta:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_multa"
    .end annotation
.end field

.field private valorOutrasEntidades:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_outros"
    .end annotation
.end field

.field private valorPagamento:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_pagamento"
    .end annotation
.end field

.field private valorPrincipal:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_principal"
    .end annotation
.end field

.field private valorReceitaBruta:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_receita_bruta"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAutenticacaoComprovante()Ljava/lang/String;
    .registers 2

    .line 405
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->autenticacaoComprovante:Ljava/lang/String;

    return-object v0
.end method

.method public getCnpj()Ljava/lang/String;
    .registers 2

    .line 181
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->cnpj:Ljava/lang/String;

    return-object v0
.end method

.method public getCodigoMotivo()Ljava/lang/String;
    .registers 2

    .line 277
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->codigoMotivo:Ljava/lang/String;

    return-object v0
.end method

.method public getCodigoPagamento()Ljava/lang/String;
    .registers 2

    .line 429
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->codigoPagamento:Ljava/lang/String;

    return-object v0
.end method

.method public getCodigoReceita()Ljava/lang/String;
    .registers 2

    .line 317
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->codigoReceita:Ljava/lang/String;

    return-object v0
.end method

.method public getCompetencia()Ljava/lang/String;
    .registers 2

    .line 149
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->competencia:Ljava/lang/String;

    return-object v0
.end method

.method public getDataPagamento()Ljava/lang/String;
    .registers 2

    .line 229
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->dataPagamento:Ljava/lang/String;

    return-object v0
.end method

.method public getDataVencimento()Ljava/lang/String;
    .registers 2

    .line 325
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->dataVencimento:Ljava/lang/String;

    return-object v0
.end method

.method public getDescricaoMotivo()Ljava/lang/String;
    .registers 2

    .line 285
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->descricaoMotivo:Ljava/lang/String;

    return-object v0
.end method

.method public getDescricaoPagamento()Ljava/lang/String;
    .registers 2

    .line 261
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->descricaoPagamento:Ljava/lang/String;

    return-object v0
.end method

.method public getIdentificacao()Ljava/lang/String;
    .registers 2

    .line 141
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->identificacao:Ljava/lang/String;

    return-object v0
.end method

.method public getIdentificacaoComprovante()Ljava/lang/String;
    .registers 2

    .line 413
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->identificacaoComprovante:Ljava/lang/String;

    return-object v0
.end method

.method public getListaEtapas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;>;"
        }
    .end annotation

    .line 421
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->listaEtapas:Ljava/util/List;

    return-object v0
.end method

.method public getNomeContribuinte()Ljava/lang/String;
    .registers 2

    .line 373
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->nomeContribuinte:Ljava/lang/String;

    return-object v0
.end method

.method public getNumeroReferencia()Ljava/lang/String;
    .registers 2

    .line 293
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->numeroReferencia:Ljava/lang/String;

    return-object v0
.end method

.method public getSeuNumero()Ljava/lang/String;
    .registers 2

    .line 221
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->seuNumero:Ljava/lang/String;

    return-object v0
.end method

.method public getSituacao()Ljava/lang/String;
    .registers 2

    .line 389
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->situacao:Ljava/lang/String;

    return-object v0
.end method

.method public getValorInss()Ljava/math/BigDecimal;
    .registers 2

    .line 133
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->valorInss:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getValorJuros()Ljava/math/BigDecimal;
    .registers 2

    .line 349
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->valorJuros:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getValorMulta()Ljava/math/BigDecimal;
    .registers 2

    .line 341
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->valorMulta:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getValorOutrasEntidades()Ljava/math/BigDecimal;
    .registers 2

    .line 125
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->valorOutrasEntidades:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getValorPagamento()Ljava/math/BigDecimal;
    .registers 2

    .line 237
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->valorPagamento:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getValorPrincipal()Ljava/math/BigDecimal;
    .registers 2

    .line 333
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->valorPrincipal:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public isIndicadorComprovante()Z
    .registers 2

    .line 269
    iget-boolean v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->indicadorComprovante:Z

    return v0
.end method

.class public Lcom/google/android/gms/internal/zzig;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/zzig$zza;
    }
.end annotation


# instance fields
.field private zzJu:Z

.field private final zzLf:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList<Lcom/google/android/gms/internal/zzig$zza;>;"
        }
    .end annotation
.end field

.field private final zzLg:Ljava/lang/String;

.field private final zzLh:Ljava/lang/String;

.field private zzLi:J

.field private zzLj:J

.field private zzLk:J

.field private zzLl:J

.field private zzLm:J

.field private zzLn:J

.field private final zzpV:Ljava/lang/Object;

.field private final zzqV:Lcom/google/android/gms/internal/zzih;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/zzih;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzig;->zzpV:Ljava/lang/Object;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzig;->zzLi:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzig;->zzLj:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzig;->zzJu:Z

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzig;->zzLk:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzig;->zzLl:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzig;->zzLm:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzig;->zzLn:J

    iput-object p1, p0, Lcom/google/android/gms/internal/zzig;->zzqV:Lcom/google/android/gms/internal/zzih;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzig;->zzLg:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/internal/zzig;->zzLh:Ljava/lang/String;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzig;->zzLf:Ljava/util/LinkedList;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbF()Lcom/google/android/gms/internal/zzih;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/internal/zzig;-><init>(Lcom/google/android/gms/internal/zzih;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public toBundle()Landroid/os/Bundle;
    .registers 10

    iget-object v3, p0, Lcom/google/android/gms/internal/zzig;->zzpV:Ljava/lang/Object;

    monitor-enter v3

    :try_start_3
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v0, "seq_num"

    iget-object v1, p0, Lcom/google/android/gms/internal/zzig;->zzLg:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "slotid"

    iget-object v1, p0, Lcom/google/android/gms/internal/zzig;->zzLh:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ismediation"

    iget-boolean v1, p0, Lcom/google/android/gms/internal/zzig;->zzJu:Z

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "treq"

    iget-wide v1, p0, Lcom/google/android/gms/internal/zzig;->zzLm:J

    invoke-virtual {v4, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "tresponse"

    iget-wide v1, p0, Lcom/google/android/gms/internal/zzig;->zzLn:J

    invoke-virtual {v4, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "timp"

    iget-wide v1, p0, Lcom/google/android/gms/internal/zzig;->zzLj:J

    invoke-virtual {v4, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "tload"

    iget-wide v1, p0, Lcom/google/android/gms/internal/zzig;->zzLk:J

    invoke-virtual {v4, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "pcc"

    iget-wide v1, p0, Lcom/google/android/gms/internal/zzig;->zzLl:J

    invoke-virtual {v4, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "tfetch"

    iget-wide v1, p0, Lcom/google/android/gms/internal/zzig;->zzLi:J

    invoke-virtual {v4, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzig;->zzLf:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_52
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_67

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/android/gms/internal/zzig$zza;

    invoke-virtual {v7}, Lcom/google/android/gms/internal/zzig$zza;->toBundle()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_52

    :cond_67
    const-string v0, "tclick"

    invoke-virtual {v4, v0, v5}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V
    :try_end_6c
    .catchall {:try_start_3 .. :try_end_6c} :catchall_6e

    monitor-exit v3

    return-object v4

    :catchall_6e
    move-exception v8

    monitor-exit v3

    throw v8
.end method

.method public zzA(Z)V
    .registers 8

    iget-object v4, p0, Lcom/google/android/gms/internal/zzig;->zzpV:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    iget-wide v0, p0, Lcom/google/android/gms/internal/zzig;->zzLn:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_12

    iput-boolean p1, p0, Lcom/google/android/gms/internal/zzig;->zzJu:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/zzig;->zzqV:Lcom/google/android/gms/internal/zzih;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/zzih;->zza(Lcom/google/android/gms/internal/zzig;)V
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_14

    :cond_12
    monitor-exit v4

    goto :goto_17

    :catchall_14
    move-exception v5

    monitor-exit v4

    throw v5

    :goto_17
    return-void
.end method

.method public zzgS()V
    .registers 7

    iget-object v4, p0, Lcom/google/android/gms/internal/zzig;->zzpV:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    iget-wide v0, p0, Lcom/google/android/gms/internal/zzig;->zzLn:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1e

    iget-wide v0, p0, Lcom/google/android/gms/internal/zzig;->zzLj:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_1e

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzig;->zzLj:J

    iget-object v0, p0, Lcom/google/android/gms/internal/zzig;->zzqV:Lcom/google/android/gms/internal/zzih;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/zzih;->zza(Lcom/google/android/gms/internal/zzig;)V

    :cond_1e
    iget-object v0, p0, Lcom/google/android/gms/internal/zzig;->zzqV:Lcom/google/android/gms/internal/zzih;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzih;->zzha()Lcom/google/android/gms/internal/zzii;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzii;->zzgS()V
    :try_end_27
    .catchall {:try_start_3 .. :try_end_27} :catchall_29

    monitor-exit v4

    goto :goto_2c

    :catchall_29
    move-exception v5

    monitor-exit v4

    throw v5

    :goto_2c
    return-void
.end method

.method public zzgT()V
    .registers 8

    iget-object v4, p0, Lcom/google/android/gms/internal/zzig;->zzpV:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    iget-wide v0, p0, Lcom/google/android/gms/internal/zzig;->zzLn:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2d

    new-instance v5, Lcom/google/android/gms/internal/zzig$zza;

    invoke-direct {v5}, Lcom/google/android/gms/internal/zzig$zza;-><init>()V

    invoke-virtual {v5}, Lcom/google/android/gms/internal/zzig$zza;->zzgX()V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzig;->zzLf:Ljava/util/LinkedList;

    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-wide v0, p0, Lcom/google/android/gms/internal/zzig;->zzLl:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzig;->zzLl:J

    iget-object v0, p0, Lcom/google/android/gms/internal/zzig;->zzqV:Lcom/google/android/gms/internal/zzih;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzih;->zzha()Lcom/google/android/gms/internal/zzii;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzii;->zzgT()V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzig;->zzqV:Lcom/google/android/gms/internal/zzih;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/zzih;->zza(Lcom/google/android/gms/internal/zzig;)V
    :try_end_2d
    .catchall {:try_start_3 .. :try_end_2d} :catchall_2f

    :cond_2d
    monitor-exit v4

    goto :goto_32

    :catchall_2f
    move-exception v6

    monitor-exit v4

    throw v6

    :goto_32
    return-void
.end method

.method public zzgU()V
    .registers 8

    iget-object v4, p0, Lcom/google/android/gms/internal/zzig;->zzpV:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    iget-wide v0, p0, Lcom/google/android/gms/internal/zzig;->zzLn:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2e

    iget-object v0, p0, Lcom/google/android/gms/internal/zzig;->zzLf:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2e

    iget-object v0, p0, Lcom/google/android/gms/internal/zzig;->zzLf:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/gms/internal/zzig$zza;

    invoke-virtual {v5}, Lcom/google/android/gms/internal/zzig$zza;->zzgV()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_2e

    invoke-virtual {v5}, Lcom/google/android/gms/internal/zzig$zza;->zzgW()V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzig;->zzqV:Lcom/google/android/gms/internal/zzih;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/zzih;->zza(Lcom/google/android/gms/internal/zzig;)V
    :try_end_2e
    .catchall {:try_start_3 .. :try_end_2e} :catchall_30

    :cond_2e
    monitor-exit v4

    goto :goto_33

    :catchall_30
    move-exception v6

    monitor-exit v4

    throw v6

    :goto_33
    return-void
.end method

.method public zzk(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;)V
    .registers 7

    iget-object v3, p0, Lcom/google/android/gms/internal/zzig;->zzpV:Ljava/lang/Object;

    monitor-enter v3

    :try_start_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzig;->zzLm:J

    iget-object v0, p0, Lcom/google/android/gms/internal/zzig;->zzqV:Lcom/google/android/gms/internal/zzih;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzih;->zzha()Lcom/google/android/gms/internal/zzii;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/gms/internal/zzig;->zzLm:J

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/gms/internal/zzii;->zzb(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;J)V
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_16

    monitor-exit v3

    goto :goto_19

    :catchall_16
    move-exception v4

    monitor-exit v3

    throw v4

    :goto_19
    return-void
.end method

.method public zzl(J)V
    .registers 9

    iget-object v4, p0, Lcom/google/android/gms/internal/zzig;->zzpV:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    iput-wide p1, p0, Lcom/google/android/gms/internal/zzig;->zzLn:J

    iget-wide v0, p0, Lcom/google/android/gms/internal/zzig;->zzLn:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/gms/internal/zzig;->zzqV:Lcom/google/android/gms/internal/zzih;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/zzih;->zza(Lcom/google/android/gms/internal/zzig;)V
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_14

    :cond_12
    monitor-exit v4

    goto :goto_17

    :catchall_14
    move-exception v5

    monitor-exit v4

    throw v5

    :goto_17
    return-void
.end method

.method public zzm(J)V
    .registers 9

    iget-object v4, p0, Lcom/google/android/gms/internal/zzig;->zzpV:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    iget-wide v0, p0, Lcom/google/android/gms/internal/zzig;->zzLn:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_12

    iput-wide p1, p0, Lcom/google/android/gms/internal/zzig;->zzLi:J

    iget-object v0, p0, Lcom/google/android/gms/internal/zzig;->zzqV:Lcom/google/android/gms/internal/zzih;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/zzih;->zza(Lcom/google/android/gms/internal/zzig;)V
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_14

    :cond_12
    monitor-exit v4

    goto :goto_17

    :catchall_14
    move-exception v5

    monitor-exit v4

    throw v5

    :goto_17
    return-void
.end method

.method public zzz(Z)V
    .registers 8

    iget-object v4, p0, Lcom/google/android/gms/internal/zzig;->zzpV:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    iget-wide v0, p0, Lcom/google/android/gms/internal/zzig;->zzLn:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1c

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzig;->zzLk:J

    if-nez p1, :cond_1c

    iget-wide v0, p0, Lcom/google/android/gms/internal/zzig;->zzLk:J

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzig;->zzLj:J

    iget-object v0, p0, Lcom/google/android/gms/internal/zzig;->zzqV:Lcom/google/android/gms/internal/zzih;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/zzih;->zza(Lcom/google/android/gms/internal/zzig;)V
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1e

    :cond_1c
    monitor-exit v4

    goto :goto_21

    :catchall_1e
    move-exception v5

    monitor-exit v4

    throw v5

    :goto_21
    return-void
.end method

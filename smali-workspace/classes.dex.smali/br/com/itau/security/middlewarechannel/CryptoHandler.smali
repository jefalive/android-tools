.class public final Lbr/com/itau/security/middlewarechannel/CryptoHandler;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final d:[B

.field private static e:I


# instance fields
.field private final a:Lbr/com/itau/security/middlewarechannel/RequestProperties;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x59

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->d:[B

    const/16 v0, 0x49

    sput v0, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->e:I

    return-void

    :array_e
    .array-data 1
        0x72t
        -0x1ct
        0x4dt
        0x63t
        0x4t
        -0x12t
        0x10t
        0x0t
        -0x10t
        0xet
        0x7t
        -0x1t
        0x1dt
        0x6t
        0x25t
        -0x4ct
        -0x7t
        0x3t
        0x0t
        0x55t
        -0x41t
        -0x2t
        0x46t
        -0x52t
        0xft
        -0xet
        0x1t
        0x10t
        0x2t
        0x45t
        -0x56t
        0xft
        -0xat
        0xdt
        0x49t
        -0x34t
        -0x1et
        0xct
        -0x2t
        0x4t
        -0xat
        0xct
        0x5t
        -0xdt
        0x46t
        -0x38t
        0x3t
        -0xet
        0x31t
        -0x20t
        -0x10t
        0xet
        0x7t
        -0x1t
        0x1dt
        0x6t
        0x1dt
        0x0t
        -0x2bt
        -0x5t
        0xat
        0x9t
        0x45t
        -0x4dt
        0x0t
        -0x4t
        0x55t
        -0x42t
        -0xbt
        0x3t
        -0x2t
        0x5t
        0x8t
        -0xet
        0x10t
        0x46t
        -0x51t
        0xet
        -0xbt
        -0x3t
        0x11t
        -0xdt
        0x0t
        0x5t
        -0x2t
        -0x1t
        0x8t
        -0x11t
        0xct
    .end array-data
.end method

.method public constructor <init>(Lbr/com/itau/security/middlewarechannel/RequestProperties;)V
    .registers 3

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->a:Lbr/com/itau/security/middlewarechannel/RequestProperties;

    .line 19
    invoke-virtual {p1}, Lbr/com/itau/security/middlewarechannel/RequestProperties;->getUserID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->b:Ljava/lang/String;

    .line 20
    invoke-static {}, Lbr/com/itau/security/middlewarechannel/Utilities;->getDeviceID()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->c:Ljava/lang/String;

    .line 21
    return-void
.end method

.method private a()Ljava/lang/String;
    .registers 12

    .line 69
    .line 1098
    iget-object v0, p0, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->a:Lbr/com/itau/security/middlewarechannel/RequestProperties;

    invoke-virtual {v0}, Lbr/com/itau/security/middlewarechannel/RequestProperties;->getJson()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lbr/com/itau/security/middlewarechannel/Utilities;->getDeviceID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_25

    .line 1099
    new-instance v0, Lbr/com/itau/security/commons/exception/CommonSecurityException;

    sget-object v1, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->d:[B

    const/16 v2, 0x11

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    const/16 v2, 0x21

    const/16 v3, 0x18

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->a(BSB)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/security/commons/exception/CommonSecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_25
    invoke-static {}, Lsc/d;->a()Ljava/lang/String;

    move-result-object v6

    .line 72
    invoke-static {}, Lsc/d;->b()Ljava/lang/String;

    move-result-object v7

    .line 73
    invoke-static {v7}, Lsc/d;->a(Ljava/lang/String;)I

    move-result v8

    .line 74
    invoke-static {v7}, Lsc/d;->b(Ljava/lang/String;)I

    move-result v9

    .line 75
    invoke-static {v7, v8}, Lsc/d;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 76
    iget-object v0, p0, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->a:Lbr/com/itau/security/middlewarechannel/RequestProperties;

    invoke-virtual {v0}, Lbr/com/itau/security/middlewarechannel/RequestProperties;->getJson()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6, v9}, Lsc/d;->a(Ljava/lang/String;Ljava/lang/String;I)[B

    move-result-object v10

    .line 77
    invoke-static {v6, v8, v9}, Lsc/d;->a(Ljava/lang/String;Ljava/lang/String;I)[B

    move-result-object v6

    .line 79
    new-instance v0, Lbr/com/itau/security/commons/json/JsonHelper;

    invoke-direct {v0}, Lbr/com/itau/security/commons/json/JsonHelper;-><init>()V

    sget-object v1, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->d:[B

    const/4 v2, 0x7

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    or-int/lit8 v2, v1, 0x21

    int-to-byte v2, v2

    sget-object v3, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->d:[B

    const/4 v4, 0x7

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->a(BSB)Ljava/lang/String;

    move-result-object v1

    .line 80
    invoke-virtual {v0, v1, v7}, Lbr/com/itau/security/commons/json/JsonHelper;->put(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/security/commons/json/JsonHelper;

    move-result-object v0

    sget-object v1, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->d:[B

    const/16 v2, 0x17

    aget-byte v1, v1, v2

    neg-int v1, v1

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->d:[B

    const/16 v3, 0xe

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    sget-object v3, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->d:[B

    const/4 v4, 0x7

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->a(BSB)Ljava/lang/String;

    move-result-object v1

    .line 81
    invoke-static {v10}, Lbr/com/itau/security/commons/binary/ByteUtils;->b64Encode([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/security/commons/json/JsonHelper;->put(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/security/commons/json/JsonHelper;

    move-result-object v0

    sget v1, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->e:I

    or-int/lit8 v1, v1, 0x6

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->d:[B

    const/16 v3, 0x30

    aget-byte v2, v2, v3

    add-int/lit8 v2, v2, -0x1

    int-to-byte v2, v2

    sget-object v3, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->d:[B

    const/4 v4, 0x7

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->a(BSB)Ljava/lang/String;

    move-result-object v1

    .line 82
    invoke-static {v6}, Lbr/com/itau/security/commons/binary/ByteUtils;->b64Encode([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/security/commons/json/JsonHelper;->put(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/security/commons/json/JsonHelper;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Lbr/com/itau/security/commons/json/JsonHelper;->toString()Ljava/lang/String;

    move-result-object v6

    .line 85
    iget-object v0, p0, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->a:Lbr/com/itau/security/middlewarechannel/RequestProperties;

    invoke-virtual {v0}, Lbr/com/itau/security/middlewarechannel/RequestProperties;->getOperation()Ljava/lang/String;

    move-result-object v7

    .line 86
    iget-object v0, p0, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->a:Lbr/com/itau/security/middlewarechannel/RequestProperties;

    invoke-virtual {v0}, Lbr/com/itau/security/middlewarechannel/RequestProperties;->getUserID()Ljava/lang/String;

    move-result-object v8

    .line 87
    iget-object v0, p0, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->a:Lbr/com/itau/security/middlewarechannel/RequestProperties;

    invoke-virtual {v0}, Lbr/com/itau/security/middlewarechannel/RequestProperties;->getUserAgent()Ljava/lang/String;

    move-result-object v9

    .line 88
    iget-object v0, p0, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->a:Lbr/com/itau/security/middlewarechannel/RequestProperties;

    invoke-virtual {v0}, Lbr/com/itau/security/middlewarechannel/RequestProperties;->getAuthToken()Ljava/lang/String;

    move-result-object v10

    .line 91
    :try_start_c7
    new-instance v0, Lbr/com/itau/security/middlewarechannel/RequestProperties;

    move-object v1, v6

    move-object v2, v7

    move-object v3, v8

    move-object v4, v9

    move-object v5, v10

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/security/middlewarechannel/RequestProperties;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lsc/b;->a(Lbr/com/itau/security/middlewarechannel/RequestProperties;)Ljava/lang/String;
    :try_end_d4
    .catch Ljava/io/IOException; {:try_start_c7 .. :try_end_d4} :catch_d6

    move-result-object v0

    return-object v0

    .line 92
    :catch_d6
    move-exception v6

    .line 93
    new-instance v0, Lbr/com/itau/security/commons/exception/CommonSecurityException;

    sget-object v1, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->d:[B

    const/4 v2, 0x7

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->d:[B

    const/16 v3, 0x44

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    const/16 v3, 0x36

    invoke-static {v3, v1, v2}, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->a(BSB)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v6}, Lbr/com/itau/security/commons/exception/CommonSecurityException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v0
.end method

.method private static a(BSB)Ljava/lang/String;
    .registers 9

    sget-object v4, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->d:[B

    add-int/lit8 p0, p0, 0x4

    add-int/lit8 p1, p1, 0x43

    mul-int/lit8 p2, p2, 0x2

    add-int/lit8 p2, p2, 0x4

    const/4 v5, 0x0

    new-instance v0, Ljava/lang/String;

    new-array v1, p2, [B

    if-nez v4, :cond_19

    move v2, p2

    move v3, p1

    :goto_13
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p1, v2, 0x1

    add-int/lit8 p0, p0, 0x1

    :cond_19
    move v2, v5

    add-int/lit8 v5, v5, 0x1

    int-to-byte v3, p1

    aput-byte v3, v1, v2

    if-ne v5, p2, :cond_2a

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2a
    move v2, p1

    aget-byte v3, v4, p0

    goto :goto_13
.end method


# virtual methods
.method public final getData()Ljava/lang/String;
    .registers 2

    .line 33
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->getData(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getData(Ljava/lang/String;)Ljava/lang/String;
    .registers 13

    .line 43
    invoke-direct {p0}, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->a()Ljava/lang/String;

    move-result-object v5

    move-object v6, p1

    move-object p1, p0

    .line 1048
    new-instance v0, Lbr/com/itau/security/commons/json/JsonHelper;

    invoke-direct {v0, v5}, Lbr/com/itau/security/commons/json/JsonHelper;-><init>(Ljava/lang/String;)V

    .line 1049
    move-object v5, v0

    sget v1, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->e:I

    or-int/lit8 v1, v1, 0x6

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->d:[B

    const/16 v3, 0x30

    aget-byte v2, v2, v3

    add-int/lit8 v2, v2, -0x1

    int-to-byte v2, v2

    sget-object v3, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->d:[B

    const/4 v4, 0x7

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->a(BSB)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/security/commons/json/JsonHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1050
    sget-object v0, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->d:[B

    const/16 v1, 0x17

    aget-byte v0, v0, v1

    neg-int v0, v0

    int-to-byte v0, v0

    sget-object v1, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->d:[B

    const/16 v2, 0xe

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->d:[B

    const/4 v3, 0x7

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->a(BSB)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lbr/com/itau/security/commons/json/JsonHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1051
    sget-object v0, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->d:[B

    const/4 v1, 0x7

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    or-int/lit8 v1, v0, 0x21

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->d:[B

    const/4 v3, 0x7

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->a(BSB)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lbr/com/itau/security/commons/json/JsonHelper;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1053
    invoke-static {v5}, Lsc/d;->a(Ljava/lang/String;)I

    move-result v9

    .line 1054
    invoke-static {v5}, Lsc/d;->b(Ljava/lang/String;)I

    move-result v10

    .line 1056
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p1, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lbr/com/itau/security/middlewarechannel/CryptoHandler;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1057
    invoke-static {v0, v9}, Lsc/d;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 1058
    invoke-static {v7}, Lbr/com/itau/security/commons/binary/ByteUtils;->b64Decode(Ljava/lang/String;)[B

    move-result-object v5

    .line 1059
    invoke-static {v8}, Lbr/com/itau/security/commons/binary/ByteUtils;->b64Decode(Ljava/lang/String;)[B

    move-result-object v7

    .line 1061
    invoke-static {v5, p1, v10}, Lsc/d;->a([BLjava/lang/String;I)[B

    move-result-object p1

    .line 1062
    const/4 v0, 0x2

    invoke-static {v0, v7, p1, v10}, Lsc/d;->a(I[B[BI)[B

    move-result-object v0

    .line 1064
    invoke-static {v0, v6}, Lbr/com/itau/security/commons/binary/ByteUtils;->to([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 43
    return-object v0
.end method

.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;
.source "YAxisRendererRadarChart.java"


# instance fields
.field private mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;


# direct methods
.method public constructor <init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;)V
    .registers 5
    .param p1, "viewPortHandler"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;
    .param p2, "yAxis"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;
    .param p3, "chart"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;)V

    .line 27
    iput-object p3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    .line 28
    return-void
.end method


# virtual methods
.method public computeAxis(FF)V
    .registers 3
    .param p1, "yMin"    # F
    .param p2, "yMax"    # F

    .line 33
    invoke-virtual {p0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->computeAxisValues(FF)V

    .line 34
    return-void
.end method

.method protected computeAxisValues(FF)V
    .registers 27
    .param p1, "min"    # F
    .param p2, "max"    # F

    .line 39
    move/from16 v4, p1

    .line 40
    .local v4, "yMin":F
    move/from16 v5, p2

    .line 43
    .local v5, "yMax":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getLabelCount()I

    move-result v6

    .line 44
    .local v6, "labelCount":I
    sub-float v0, v5, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v7, v0

    .line 47
    .local v7, "range":D
    if-eqz v6, :cond_1b

    const-wide/16 v0, 0x0

    cmpg-double v0, v7, v0

    if-gtz v0, :cond_2c

    .line 48
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    const/4 v1, 0x0

    new-array v1, v1, [F

    iput-object v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    .line 49
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    const/4 v1, 0x0

    iput v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntryCount:I

    .line 50
    return-void

    .line 54
    :cond_2c
    int-to-double v0, v6

    div-double v9, v7, v0

    .line 55
    .local v9, "rawInterval":D
    invoke-static {v9, v10}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->roundToNextSignificant(D)F

    move-result v0

    float-to-double v11, v0

    .line 56
    .local v11, "interval":D
    invoke-static {v11, v12}, Ljava/lang/Math;->log10(D)D

    move-result-wide v0

    double-to-int v0, v0

    int-to-double v0, v0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v13

    .line 57
    .local v13, "intervalMagnitude":D
    div-double v0, v11, v13

    double-to-int v15, v0

    .line 58
    .local v15, "intervalSigDigit":I
    const/4 v0, 0x5

    if-le v15, v0, :cond_4d

    .line 61
    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    mul-double/2addr v0, v13

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v11

    .line 66
    :cond_4d
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->isShowOnlyMinMaxEnabled()Z

    move-result v0

    if-eqz v0, :cond_7b

    .line 69
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    const/4 v1, 0x2

    iput v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntryCount:I

    .line 70
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    const/4 v1, 0x2

    new-array v1, v1, [F

    iput-object v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    .line 71
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    const/4 v1, 0x0

    aput v4, v0, v1

    .line 72
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    const/4 v1, 0x1

    aput v5, v0, v1

    goto/16 :goto_ea

    .line 78
    :cond_7b
    float-to-double v0, v4

    div-double/2addr v0, v11

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    mul-double v16, v0, v11

    .line 81
    .local v16, "first":D
    const-wide/16 v0, 0x0

    cmpl-double v0, v16, v0

    if-nez v0, :cond_8b

    .line 82
    const-wide/16 v16, 0x0

    .line 85
    :cond_8b
    float-to-double v0, v5

    div-double/2addr v0, v11

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    mul-double/2addr v0, v11

    invoke-static {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->nextUp(D)D

    move-result-wide v18

    .line 90
    .local v18, "last":D
    const/16 v23, 0x0

    .line 91
    .local v23, "n":I
    move-wide/from16 v20, v16

    .local v20, "f":D
    :goto_9a
    cmpg-double v0, v20, v18

    if-gtz v0, :cond_a3

    .line 92
    add-int/lit8 v23, v23, 0x1

    .line 91
    add-double v20, v20, v11

    goto :goto_9a

    .line 96
    :cond_a3
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getAxisMaxValue()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_b3

    .line 97
    add-int/lit8 v23, v23, 0x1

    .line 99
    :cond_b3
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    move/from16 v1, v23

    iput v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntryCount:I

    .line 102
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    array-length v0, v0

    move/from16 v1, v23

    if-ge v0, v1, :cond_d0

    .line 104
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    move/from16 v1, v23

    new-array v1, v1, [F

    iput-object v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    .line 108
    :cond_d0
    move-wide/from16 v20, v16

    const/16 v22, 0x0

    .local v22, "i":I
    :goto_d4
    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_ea

    .line 109
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    move-wide/from16 v1, v20

    double-to-float v1, v1

    aput v1, v0, v22

    .line 108
    add-double v20, v20, v11

    add-int/lit8 v22, v22, 0x1

    goto :goto_d4

    .line 114
    .end local v16    # "first":D
    .end local v18    # "last":D
    .end local v20    # "f":D
    .end local v22    # "i":I
    .end local v23    # "n":I
    :cond_ea
    :goto_ea
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpg-double v0, v11, v0

    if-gez v0, :cond_101

    .line 115
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-static {v11, v12}, Ljava/lang/Math;->log10(D)D

    move-result-wide v1

    neg-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v1, v1

    iput v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mDecimals:I

    goto :goto_108

    .line 117
    :cond_101
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    const/4 v1, 0x0

    iput v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mDecimals:I

    .line 121
    :goto_108
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    move-object/from16 v2, p0

    iget-object v2, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget v2, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntryCount:I

    add-int/lit8 v2, v2, -0x1

    aget v1, v1, v2

    iput v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisMaximum:F

    .line 122
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisMaximum:F

    move-object/from16 v2, p0

    iget-object v2, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget v2, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisMinimum:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iput v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisRange:F

    .line 123
    return-void
.end method

.method public renderAxisLabels(Landroid/graphics/Canvas;)V
    .registers 12
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 130
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->isDrawLabelsEnabled()Z

    move-result v0

    if-nez v0, :cond_11

    .line 131
    :cond_10
    return-void

    .line 134
    :cond_11
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mAxisLabelPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 135
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mAxisLabelPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getTextSize()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 136
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mAxisLabelPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 139
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getCenterOffsets()Landroid/graphics/PointF;

    move-result-object v3

    .line 140
    .local v3, "center":Landroid/graphics/PointF;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getFactor()F

    move-result v4

    .line 143
    .local v4, "factor":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget v5, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntryCount:I

    .line 146
    .local v5, "labelCount":I
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_43
    if-ge v6, v5, :cond_7e

    .line 149
    add-int/lit8 v0, v5, -0x1

    if-ne v6, v0, :cond_52

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->isDrawTopYLabelEntryEnabled()Z

    move-result v0

    if-nez v0, :cond_52

    .line 150
    goto :goto_7e

    .line 153
    :cond_52
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    aget v0, v0, v6

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mAxisMinimum:F

    sub-float/2addr v0, v1

    mul-float v7, v0, v4

    .line 156
    .local v7, "r":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getRotationAngle()F

    move-result v0

    invoke-static {v3, v7, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getPosition(Landroid/graphics/PointF;FF)Landroid/graphics/PointF;

    move-result-object v8

    .line 159
    .local v8, "p":Landroid/graphics/PointF;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0, v6}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getFormattedLabel(I)Ljava/lang/String;

    move-result-object v9

    .line 162
    .local v9, "label":Ljava/lang/String;
    iget v0, v8, Landroid/graphics/PointF;->x:F

    const/high16 v1, 0x41200000    # 10.0f

    add-float/2addr v0, v1

    iget v1, v8, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mAxisLabelPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v9, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 146
    .end local v7    # "r":F
    .end local v8    # "p":Landroid/graphics/PointF;
    .end local v9    # "label":Ljava/lang/String;
    add-int/lit8 v6, v6, 0x1

    goto :goto_43

    .line 164
    .end local v6    # "j":I
    :cond_7e
    :goto_7e
    return-void
.end method

.method public renderLimitLines(Landroid/graphics/Canvas;)V
    .registers 14
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 171
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getLimitLines()Ljava/util/List;

    move-result-object v2

    .line 174
    .local v2, "limitLines":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;>;"
    if-nez v2, :cond_9

    .line 175
    return-void

    .line 178
    :cond_9
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getSliceAngle()F

    move-result v3

    .line 183
    .local v3, "sliceangle":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getFactor()F

    move-result v4

    .line 186
    .local v4, "factor":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getCenterOffsets()Landroid/graphics/PointF;

    move-result-object v5

    .line 189
    .local v5, "center":Landroid/graphics/PointF;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1c
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_92

    .line 192
    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;

    .line 195
    .local v7, "l":Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getLineColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 196
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getDashPathEffect()Landroid/graphics/DashPathEffect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 197
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getLineWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 200
    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getLimit()F

    move-result v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getYChartMin()F

    move-result v1

    sub-float/2addr v0, v1

    mul-float v8, v0, v4

    .line 203
    .local v8, "r":F
    new-instance v9, Landroid/graphics/Path;

    invoke-direct {v9}, Landroid/graphics/Path;-><init>()V

    .line 206
    .local v9, "limitPath":Landroid/graphics/Path;
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_57
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getData()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;->getXValCount()I

    move-result v0

    if-ge v10, v0, :cond_86

    .line 209
    int-to-float v0, v10

    mul-float/2addr v0, v3

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getRotationAngle()F

    move-result v1

    add-float/2addr v0, v1

    invoke-static {v5, v8, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getPosition(Landroid/graphics/PointF;FF)Landroid/graphics/PointF;

    move-result-object v11

    .line 212
    .local v11, "p":Landroid/graphics/PointF;
    if-nez v10, :cond_7c

    .line 213
    iget v0, v11, Landroid/graphics/PointF;->x:F

    iget v1, v11, Landroid/graphics/PointF;->y:F

    invoke-virtual {v9, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    goto :goto_83

    .line 215
    :cond_7c
    iget v0, v11, Landroid/graphics/PointF;->x:F

    iget v1, v11, Landroid/graphics/PointF;->y:F

    invoke-virtual {v9, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 206
    .end local v11    # "p":Landroid/graphics/PointF;
    :goto_83
    add-int/lit8 v10, v10, 0x1

    goto :goto_57

    .line 219
    .end local v10    # "j":I
    :cond_86
    invoke-virtual {v9}, Landroid/graphics/Path;->close()V

    .line 222
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRendererRadarChart;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v9, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 189
    .end local v7    # "l":Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;
    .end local v8    # "r":F
    .end local v9    # "limitPath":Landroid/graphics/Path;
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1c

    .line 224
    .end local v6    # "i":I
    :cond_92
    return-void
.end method

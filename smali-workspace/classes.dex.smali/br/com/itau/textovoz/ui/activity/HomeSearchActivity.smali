.class public Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;
.super Lbr/com/itau/textovoz/ui/activity/BaseActivity;
.source "HomeSearchActivity.java"

# interfaces
.implements Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuListListener;
.implements Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqListListener;
.implements Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$FeedbackOptionListener;
.implements Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment$AttendanceListener;
.implements Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch$OnPushMessageListener;
.implements Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$OnBalanceAccountHeaderContainer;


# static fields
.field static fm:Landroid/support/v4/app/FragmentManager;

.field static mListener:Lbr/com/itau/textovoz/SearchListenerBalance;


# instance fields
.field accessibilityDelegate:Landroid/support/v4/view/AccessibilityDelegateCompat;

.field private account:Ljava/lang/String;

.field private accountDAC:Ljava/lang/String;

.field private agency:Ljava/lang/String;

.field private flagAttendance:Z

.field private flagCard:Ljava/lang/String;

.field private flagFaq:Z

.field private flagMenu:Z

.field private flagOverflowMessage:Z

.field private flagReceipts:Z

.field private flagTransaction:Ljava/lang/String;

.field private flagVoiceSearch:Z

.field private isExibeFaq:Z

.field private isReturnVoice:Z

.field private linearFragmentContainer:Landroid/widget/LinearLayout;

.field private menu:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
        }
    .end annotation
.end field

.field private nestedScrollView:Landroid/support/v4/widget/NestedScrollView;

.field private networkHandlerSearch:Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;

.field private final onClickListener:Landroid/view/View$OnClickListener;

.field private final onScrollChangeListener:Landroid/support/v4/widget/NestedScrollView$OnScrollChangeListener;

.field private operator:Ljava/lang/String;

.field private relativeSearchLoadingContainer:Landroid/widget/RelativeLayout;

.field private searchContext:Lbr/com/itau/textovoz/util/SearchContext;

.field private searchTextTimer:Landroid/os/CountDownTimer;

.field private searchView:Landroid/support/v7/widget/SearchView;

.field searchViewOnQueryTextListener:Landroid/support/v7/widget/SearchView$OnQueryTextListener;

.field private segment:Ljava/lang/String;

.field private textSearch:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 71
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/BaseActivity;-><init>()V

    .line 324
    new-instance v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;

    invoke-direct {v0, p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;-><init>(Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;)V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchViewOnQueryTextListener:Landroid/support/v7/widget/SearchView$OnQueryTextListener;

    .line 363
    new-instance v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$2;

    invoke-direct {v0, p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$2;-><init>(Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;)V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->onClickListener:Landroid/view/View$OnClickListener;

    .line 409
    new-instance v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$4;

    invoke-direct {v0, p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$4;-><init>(Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;)V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->onScrollChangeListener:Landroid/support/v4/widget/NestedScrollView$OnScrollChangeListener;

    .line 838
    new-instance v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$6;

    invoke-direct {v0, p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$6;-><init>(Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;)V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->accessibilityDelegate:Landroid/support/v4/view/AccessibilityDelegateCompat;

    return-void
.end method

.method static synthetic access$000(Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;)Landroid/os/CountDownTimer;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    .line 71
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchTextTimer:Landroid/os/CountDownTimer;

    return-object v0
.end method

.method static synthetic access$002(Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;Landroid/os/CountDownTimer;)Landroid/os/CountDownTimer;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;
    .param p1, "x1"    # Landroid/os/CountDownTimer;

    .line 71
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchTextTimer:Landroid/os/CountDownTimer;

    return-object p1
.end method

.method static synthetic access$100(Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;Ljava/lang/String;)V
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .line 71
    invoke-direct {p0, p1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->showRationaleMessage(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;)V
    .registers 1
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    .line 71
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->openSearchVoice()V

    return-void
.end method

.method static synthetic access$300(Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;)Landroid/support/v7/widget/SearchView;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    .line 71
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    return-object v0
.end method

.method private addAttendanceToUI()V
    .registers 6

    .line 693
    iget-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->flagAttendance:Z

    if-nez v0, :cond_1d

    .line 694
    new-instance v3, Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment;

    invoke-direct {v3}, Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment;-><init>()V

    .line 695
    .local v3, "attendanceSearchFragment":Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment;
    invoke-virtual {v3, p0}, Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment;->setAttendanceListener(Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment$AttendanceListener;)V

    .line 697
    .line 698
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$id;->id_attendance_search_fragment:I

    sget-object v2, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->fm:Landroid/support/v4/app/FragmentManager;

    invoke-direct {p0, v0, v1, v3, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setupFragmentContainer(Landroid/view/LayoutInflater;ILandroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentManager;)Landroid/view/View;

    move-result-object v4

    .line 702
    .local v4, "container":Landroid/view/View;
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->linearFragmentContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 704
    .end local v3    # "attendanceSearchFragment":Lbr/com/itau/textovoz/ui/fragment/AttendanceSearchFragment;
    .end local v4    # "container":Landroid/view/View;
    :cond_1d
    return-void
.end method

.method private addCardsToUI(Ljava/util/ArrayList;)V
    .registers 7
    .param p1, "cards"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Cards;>;)V"
        }
    .end annotation

    .line 668
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1c

    .line 669
    new-instance v3, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;

    invoke-direct {v3}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;-><init>()V

    .line 671
    .line 672
    .local v3, "cardFragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$id;->id_cards_search_list_fragment:I

    sget-object v2, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->fm:Landroid/support/v4/app/FragmentManager;

    invoke-direct {p0, v0, v1, v3, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setupFragmentContainer(Landroid/view/LayoutInflater;ILandroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentManager;)Landroid/view/View;

    move-result-object v4

    .line 676
    .local v4, "container":Landroid/view/View;
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->linearFragmentContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 678
    .end local v3    # "cardFragment":Landroid/support/v4/app/Fragment;
    .end local v4    # "container":Landroid/view/View;
    :cond_1c
    return-void
.end method

.method private addEmptySearchToUI()V
    .registers 6

    .line 616
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->textSearch:Ljava/lang/String;

    invoke-static {v0}, Lbr/com/itau/textovoz/ui/fragment/EmptySearchFragment;->newInstance(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    .line 618
    .line 619
    .local v3, "emptySearchFragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$id;->id_empty_search_fragment:I

    sget-object v2, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->fm:Landroid/support/v4/app/FragmentManager;

    invoke-direct {p0, v0, v1, v3, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setupFragmentContainer(Landroid/view/LayoutInflater;ILandroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentManager;)Landroid/view/View;

    move-result-object v4

    .line 623
    .local v4, "container":Landroid/view/View;
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->linearFragmentContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 624
    return-void
.end method

.method private addFaqsToUI(Ljava/util/ArrayList;)V
    .registers 7
    .param p1, "faqs"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Faq;>;)V"
        }
    .end annotation

    .line 627
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_22

    .line 628
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->segment:Ljava/lang/String;

    iget-boolean v1, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->isExibeFaq:Z

    invoke-static {p1, v0, v1}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->newInstance(Ljava/util/ArrayList;Ljava/lang/String;Z)Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;

    move-result-object v3

    .line 629
    .local v3, "faqFragment":Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;
    invoke-virtual {v3, p0}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->setFaqListListener(Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqListListener;)V

    .line 631
    .line 632
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$id;->id_faq_list_fragment:I

    sget-object v2, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->fm:Landroid/support/v4/app/FragmentManager;

    invoke-direct {p0, v0, v1, v3, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setupFragmentContainer(Landroid/view/LayoutInflater;ILandroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentManager;)Landroid/view/View;

    move-result-object v4

    .line 636
    .local v4, "container":Landroid/view/View;
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->linearFragmentContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 638
    .end local v3    # "faqFragment":Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;
    .end local v4    # "container":Landroid/view/View;
    :cond_22
    return-void
.end method

.method private addFeedbackToUI()V
    .registers 6

    .line 681
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->segment:Ljava/lang/String;

    invoke-static {v0}, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->newInstance(Ljava/lang/String;)Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;

    move-result-object v3

    .line 682
    .local v3, "feedbackSearchFragment":Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;
    invoke-virtual {v3, p0}, Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment;->setOptionListener(Lbr/com/itau/textovoz/ui/fragment/FeedbackSearchFragment$FeedbackOptionListener;)V

    .line 684
    .line 685
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$id;->id_feedback_search_fragment:I

    sget-object v2, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->fm:Landroid/support/v4/app/FragmentManager;

    invoke-direct {p0, v0, v1, v3, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setupFragmentContainer(Landroid/view/LayoutInflater;ILandroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentManager;)Landroid/view/View;

    move-result-object v4

    .line 689
    .local v4, "container":Landroid/view/View;
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->linearFragmentContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 690
    return-void
.end method

.method private addMenusToUI(Ljava/util/ArrayList;)V
    .registers 7
    .param p1, "menus"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;)V"
        }
    .end annotation

    .line 641
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1e

    .line 642
    invoke-static {p1}, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;->newInstance(Ljava/util/ArrayList;)Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;

    move-result-object v3

    .line 643
    .local v3, "menuFragment":Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;
    invoke-virtual {v3, p0}, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;->setMenuListListener(Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuListListener;)V

    .line 645
    .line 646
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$id;->id_menu_list_fragment:I

    sget-object v2, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->fm:Landroid/support/v4/app/FragmentManager;

    invoke-direct {p0, v0, v1, v3, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setupFragmentContainer(Landroid/view/LayoutInflater;ILandroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentManager;)Landroid/view/View;

    move-result-object v4

    .line 650
    .local v4, "container":Landroid/view/View;
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->linearFragmentContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 652
    .end local v3    # "menuFragment":Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;
    .end local v4    # "container":Landroid/view/View;
    :cond_1e
    return-void
.end method

.method private addOveflowMessageToUI(Ljava/util/ArrayList;)V
    .registers 10
    .param p1, "overflowMessages"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/OverflowMessage;>;)V"
        }
    .end annotation

    .line 708
    if-eqz p1, :cond_5d

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5d

    .line 710
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lbr/com/itau/textovoz/model/OverflowMessage;

    .line 711
    .local v3, "om":Lbr/com/itau/textovoz/model/OverflowMessage;
    invoke-virtual {v3}, Lbr/com/itau/textovoz/model/OverflowMessage;->getText()Ljava/lang/String;

    move-result-object v4

    .line 713
    .local v4, "message":Ljava/lang/String;
    sget v0, Lbr/com/itau/textovoz/R$string;->ga_categoria_busca:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v2, Lbr/com/itau/textovoz/R$string;->ga_acao_transbordo:I

    .line 714
    invoke-virtual {p0, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Lbr/com/itau/textovoz/model/OverflowMessage;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->textSearch:Ljava/lang/String;

    .line 713
    invoke-virtual {p0, v0, v1, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setEventTracker(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    .line 717
    .local v5, "inflater":Landroid/view/LayoutInflater;
    sget v0, Lbr/com/itau/textovoz/R$layout;->view_content_overflow_message:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v5, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 719
    .local v6, "v":Landroid/view/View;
    sget v0, Lbr/com/itau/textovoz/R$id;->oveflow_message_textView:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    .line 720
    .local v7, "messageTextView":Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;
    invoke-virtual {v7, v4}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setText(Ljava/lang/CharSequence;)V

    .line 722
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->linearFragmentContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 724
    .end local v3    # "om":Lbr/com/itau/textovoz/model/OverflowMessage;
    .end local v4    # "message":Ljava/lang/String;
    .end local v5    # "inflater":Landroid/view/LayoutInflater;
    .end local v6    # "v":Landroid/view/View;
    .end local v7    # "messageTextView":Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;
    :cond_5d
    return-void
.end method

.method private addReceiptsToUI(Ljava/util/ArrayList;)V
    .registers 7
    .param p1, "receipts"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Receipt;>;)V"
        }
    .end annotation

    .line 655
    if-eqz p1, :cond_1d

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1d

    .line 656
    invoke-static {p1}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->newInstance(Ljava/util/ArrayList;)Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;

    move-result-object v3

    .line 658
    .line 659
    .local v3, "receiptsFragment":Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$id;->id_receipts_search_fragment:I

    sget-object v2, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->fm:Landroid/support/v4/app/FragmentManager;

    invoke-direct {p0, v0, v1, v3, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setupFragmentContainer(Landroid/view/LayoutInflater;ILandroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentManager;)Landroid/view/View;

    move-result-object v4

    .line 663
    .local v4, "container":Landroid/view/View;
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->linearFragmentContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 665
    .end local v3    # "receiptsFragment":Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;
    .end local v4    # "container":Landroid/view/View;
    :cond_1d
    return-void
.end method

.method private cleanFragments()V
    .registers 2

    .line 187
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->linearFragmentContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_d

    .line 188
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->linearFragmentContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 190
    :cond_d
    return-void
.end method

.method private fetchList()V
    .registers 2

    .line 179
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->cleanFragments()V

    .line 180
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->showLoadingDialog()V

    .line 181
    iget-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->isReturnVoice:Z

    if-nez v0, :cond_f

    .line 182
    const-string v0, ""

    invoke-direct {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchAPI(Ljava/lang/String;)V

    .line 184
    :cond_f
    return-void
.end method

.method private getMenus(Lbr/com/itau/textovoz/model/response/SearchResponse;)Ljava/util/ArrayList;
    .registers 6
    .param p1, "searchResponse"    # Lbr/com/itau/textovoz/model/response/SearchResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lbr/com/itau/textovoz/model/response/SearchResponse;)Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
        }
    .end annotation

    .line 788
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 790
    .local v1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/response/SearchResponse;->getMenus()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_3a

    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/response/SearchResponse;->getMenus()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3a

    .line 791
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->operator:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_33

    .line 792
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/response/SearchResponse;->getMenus()Ljava/util/ArrayList;

    move-result-object v2

    .line 793
    .local v2, "menuListAPI":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->menu:Ljava/util/ArrayList;

    invoke-direct {p0, v2, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->returnDuplicates(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    .line 794
    .local v3, "returnDuplicates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
    if-eqz v3, :cond_32

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_32

    .line 795
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 797
    .end local v2    # "menuListAPI":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
    .end local v2
    .end local v3    # "returnDuplicates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
    .end local v3
    :cond_32
    goto :goto_3a

    .line 798
    :cond_33
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/response/SearchResponse;->getMenus()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 801
    :cond_3a
    :goto_3a
    return-object v1
.end method

.method private initController()V
    .registers 2

    .line 217
    invoke-static {}, Lbr/com/itau/textovoz/SearchItau;->init()V

    .line 218
    invoke-static {}, Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;->getInstance()Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->networkHandlerSearch:Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;

    .line 219
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->fetchList()V

    .line 220
    return-void
.end method

.method public static isErrorBalance()V
    .registers 3

    .line 825
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->fm:Landroid/support/v4/app/FragmentManager;

    sget v1, Lbr/com/itau/textovoz/R$id;->id_cards_search_list_fragment:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;

    .line 826
    .local v2, "fragment":Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;
    if-eqz v2, :cond_10

    .line 827
    invoke-virtual {v2}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->showError()V

    .line 829
    :cond_10
    return-void
.end method

.method private openFaq(Lbr/com/itau/textovoz/model/Faq;)V
    .registers 5
    .param p1, "faq"    # Lbr/com/itau/textovoz/model/Faq;

    .line 751
    invoke-static {p0}, Lbr/com/itau/textovoz/util/ViewUtils;->hideSoftKeyboard(Landroid/app/Activity;)V

    .line 753
    new-instance v2, Landroid/content/Intent;

    const-class v0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;

    invoke-direct {v2, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 754
    .local v2, "intent":Landroid/content/Intent;
    const-string v0, "type"

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->segment:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 755
    const-string v0, "product"

    sget v1, Lbr/com/itau/textovoz/R$string;->attendance:I

    invoke-virtual {p0, v1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 756
    const-string v0, "faq"

    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Faq;->getQuestion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 757
    const-string v0, "answer"

    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Faq;->getAnswer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 758
    const-string v0, "operator"

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->operator:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 759
    const-string v0, "links"

    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Faq;->getLinks()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 760
    const-string v0, "channel"

    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Faq;->getChannels()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 761
    const/16 v0, 0x64

    invoke-virtual {p0, v2, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 762
    return-void
.end method

.method private openSearchVoice()V
    .registers 4

    .line 419
    new-instance v2, Landroid/content/Intent;

    const-class v0, Lbr/com/itau/textovoz/ui/activity/SearchVoiceActivity;

    invoke-direct {v2, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 420
    .local v2, "intent":Landroid/content/Intent;
    const-string v0, "type"

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->segment:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 421
    const/16 v0, 0x1da6

    invoke-virtual {p0, v2, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 422
    sget v0, Lbr/com/itau/textovoz/R$anim;->push_down_in:I

    sget v1, Lbr/com/itau/textovoz/R$anim;->push_down_out:I

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->overridePendingTransition(II)V

    .line 423
    return-void
.end method

.method private returnDuplicates(Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .registers 8
    .param p1, "menuListAPI"    # Ljava/util/ArrayList;
    .param p2, "menuListComp"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;)Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
        }
    .end annotation

    .line 774
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 776
    .local v2, "retList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_6
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_3b

    .line 777
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_d
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_38

    .line 778
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/textovoz/model/Menu;

    invoke-virtual {v0}, Lbr/com/itau/textovoz/model/Menu;->getOpKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbr/com/itau/textovoz/model/Menu;

    invoke-virtual {v1}, Lbr/com/itau/textovoz/model/Menu;->getOpKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 779
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 780
    goto :goto_38

    .line 777
    :cond_35
    add-int/lit8 v4, v4, 0x1

    goto :goto_d

    .line 776
    .end local v4    # "j":I
    :cond_38
    :goto_38
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 784
    .end local v3    # "i":I
    :cond_3b
    return-object v2
.end method

.method private returnFaqLink([Ljava/lang/String;)V
    .registers 4
    .param p1, "faqLink"    # [Ljava/lang/String;

    .line 743
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 744
    .local v1, "intent":Landroid/content/Intent;
    sget v0, Lbr/com/itau/textovoz/R$string;->link_result:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 745
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setResult(ILandroid/content/Intent;)V

    .line 746
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->finish()V

    .line 747
    return-void
.end method

.method private searchAPI(Ljava/lang/String;)V
    .registers 21
    .param p1, "searchTerm"    # Ljava/lang/String;

    .line 194
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->operator:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 195
    const-string v18, "34cfc64e-2281-4645-9d31-f67d9ab2618c"

    .local v18, "key":Ljava/lang/String;
    goto :goto_f

    .line 197
    .end local v18    # "key":Ljava/lang/String;
    :cond_d
    const-string v18, "38d6287a-9f93-4e70-8d03-dac48cdb5a09"

    .line 200
    .local v18, "key":Ljava/lang/String;
    :goto_f
    invoke-static {}, Lbr/com/itau/textovoz/SearchItau;->getApi()Lbr/com/itau/textovoz/controller/SearchController;

    move-result-object v1

    move-object/from16 v2, p1

    move-object/from16 v0, p0

    iget-object v3, v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->agency:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->accountDAC:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->operator:Ljava/lang/String;

    move-object/from16 v7, v18

    .line 201
    invoke-static {}, Lbr/com/itau/textovoz/util/ApiUtils;->getVersionOS()Ljava/lang/String;

    move-result-object v8

    invoke-static/range {p0 .. p0}, Lbr/com/itau/textovoz/util/ApiUtils;->getPlatform(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    invoke-static {}, Lbr/com/itau/textovoz/util/ApiUtils;->getModel()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->flagVoiceSearch:Z

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->flagMenu:Z

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->flagFaq:Z

    move-object/from16 v0, p0

    iget-object v14, v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->flagCard:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->flagTransaction:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->flagOverflowMessage:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->flagReceipts:Z

    move/from16 v17, v0

    .line 200
    invoke-virtual/range {v1 .. v17}, Lbr/com/itau/textovoz/controller/SearchController;->doGetSearch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;Ljava/lang/String;ZZ)V

    .line 203
    return-void
.end method

.method private setTextSearchView(Ljava/lang/String;)V
    .registers 4
    .param p1, "textSearchView"    # Ljava/lang/String;

    .line 506
    sget v0, Lbr/com/itau/textovoz/R$string;->ga_categoria_busca:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$string;->ga_termo_buscado:I

    invoke-virtual {p0, v1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setEventTracker(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->isReturnVoice:Z

    .line 510
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->flagVoiceSearch:Z

    .line 512
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    new-instance v1, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$5;

    invoke-direct {v1, p0, p1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$5;-><init>(Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->post(Ljava/lang/Runnable;)Z

    .line 519
    return-void
.end method

.method private setToolbar()V
    .registers 6

    .line 223
    sget v0, Lbr/com/itau/textovoz/R$id;->toolbar_library:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/support/v7/widget/Toolbar;

    .line 224
    .local v2, "toolbar":Landroid/support/v7/widget/Toolbar;
    invoke-virtual {p0, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 226
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v3

    .line 227
    .local v3, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz v3, :cond_40

    .line 228
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 229
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/support/v7/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 233
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->segment:Ljava/lang/String;

    invoke-static {v0}, Lbr/com/itau/textovoz/util/CommonUtils;->getSegment(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v1, v0, :cond_2a

    .line 234
    sget v0, Lbr/com/itau/textovoz/R$xml;->arrow_person:I

    invoke-static {p0, v0}, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->inflate(Landroid/content/Context;I)Lbr/com/itau/textovoz/ui/view/FontIconDrawable;

    move-result-object v4

    .local v4, "fontIconDrawable":Lbr/com/itau/textovoz/ui/view/FontIconDrawable;
    goto :goto_30

    .line 238
    .end local v4    # "fontIconDrawable":Lbr/com/itau/textovoz/ui/view/FontIconDrawable;
    :cond_2a
    sget v0, Lbr/com/itau/textovoz/R$xml;->arrow_itau:I

    invoke-static {p0, v0}, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->inflate(Landroid/content/Context;I)Lbr/com/itau/textovoz/ui/view/FontIconDrawable;

    move-result-object v4

    .line 242
    .local v4, "fontIconDrawable":Lbr/com/itau/textovoz/ui/view/FontIconDrawable;
    :goto_30
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$string;->content_description_back:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/support/v7/app/ActionBar;->setHomeActionContentDescription(Ljava/lang/CharSequence;)V

    .line 244
    invoke-virtual {v3, v4}, Landroid/support/v7/app/ActionBar;->setHomeAsUpIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 246
    .end local v4    # "fontIconDrawable":Lbr/com/itau/textovoz/ui/view/FontIconDrawable;
    :cond_40
    return-void
.end method

.method public static setValueBalance(Ljava/lang/Double;)V
    .registers 4
    .param p0, "balance"    # Ljava/lang/Double;

    .line 817
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->fm:Landroid/support/v4/app/FragmentManager;

    sget v1, Lbr/com/itau/textovoz/R$id;->id_cards_search_list_fragment:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;

    .line 819
    .local v2, "fragment":Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;
    if-eqz v2, :cond_10

    .line 820
    invoke-virtual {v2, p0}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->showContent(Ljava/lang/Double;)V

    .line 822
    :cond_10
    return-void
.end method

.method private setViews()V
    .registers 6

    .line 158
    sget v0, Lbr/com/itau/textovoz/R$id;->home_linear_list_container:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->linearFragmentContainer:Landroid/widget/LinearLayout;

    .line 159
    sget v0, Lbr/com/itau/textovoz/R$id;->relative_search_loading_container:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->relativeSearchLoadingContainer:Landroid/widget/RelativeLayout;

    .line 160
    sget v0, Lbr/com/itau/textovoz/R$id;->home_scroll_view:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/NestedScrollView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->nestedScrollView:Landroid/support/v4/widget/NestedScrollView;

    .line 161
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->nestedScrollView:Landroid/support/v4/widget/NestedScrollView;

    if-eqz v0, :cond_29

    .line 162
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->nestedScrollView:Landroid/support/v4/widget/NestedScrollView;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->onScrollChangeListener:Landroid/support/v4/widget/NestedScrollView$OnScrollChangeListener;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/NestedScrollView;->setOnScrollChangeListener(Landroid/support/v4/widget/NestedScrollView$OnScrollChangeListener;)V

    .line 165
    :cond_29
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->relativeSearchLoadingContainer:Landroid/widget/RelativeLayout;

    sget v1, Lbr/com/itau/textovoz/R$id;->progress_search_loading:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/ProgressBar;

    .line 168
    .local v4, "progressBar":Landroid/widget/ProgressBar;
    if-eqz v4, :cond_6e

    .line 169
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->segment:Ljava/lang/String;

    invoke-static {v0}, Lbr/com/itau/textovoz/util/CommonUtils;->getSegment(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v1, v0, :cond_57

    .line 170
    invoke-virtual {v4}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$color;->personnalite_theme_color:I

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/support/v4/content/res/ResourcesCompat;->getColor(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_6e

    .line 172
    :cond_57
    invoke-virtual {v4}, Landroid/widget/ProgressBar;->getIndeterminateDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$color;->itau_theme_color:I

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/support/v4/content/res/ResourcesCompat;->getColor(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 176
    :cond_6e
    :goto_6e
    return-void
.end method

.method private setupFragmentContainer(Landroid/view/LayoutInflater;ILandroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentManager;)Landroid/view/View;
    .registers 9
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "type"    # I
    .param p3, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p4, "fm"    # Landroid/support/v4/app/FragmentManager;

    .line 727
    sget v0, Lbr/com/itau/textovoz/R$layout;->generic_fragment_container:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/FrameLayout;

    .line 728
    .local v3, "fl":Landroid/widget/FrameLayout;
    invoke-virtual {v3, p2}, Landroid/widget/FrameLayout;->setId(I)V

    .line 730
    invoke-virtual {p4}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 731
    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getId()I

    move-result v1

    invoke-virtual {v0, v1, p3}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 732
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 734
    return-object v3
.end method

.method private showMessageError(Ljava/lang/String;Z)V
    .registers 6
    .param p1, "messageText"    # Ljava/lang/String;
    .param p2, "closeAppLibrary"    # Z

    .line 497
    new-instance v2, Landroid/content/Intent;

    const-class v0, Lbr/com/itau/textovoz/ui/activity/ErrorSystemActivity;

    invoke-direct {v2, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 498
    .local v2, "intent":Landroid/content/Intent;
    const/high16 v0, 0x4000000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 499
    const-string v0, "type"

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->segment:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 500
    const-string v0, "closeAppLibrary"

    invoke-virtual {v2, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 501
    const-string v0, "messageText"

    invoke-virtual {v2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 502
    const/16 v0, 0x1da7

    invoke-virtual {p0, v2, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 503
    return-void
.end method

.method private showRationaleMessage(Ljava/lang/String;)V
    .registers 9
    .param p1, "text"    # Ljava/lang/String;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .line 384
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v0, p1, v1}, Landroid/support/design/widget/Snackbar;->make(Landroid/view/View;Ljava/lang/CharSequence;I)Landroid/support/design/widget/Snackbar;

    move-result-object v2

    .line 385
    .local v2, "snackbar":Landroid/support/design/widget/Snackbar;
    new-instance v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$3;

    invoke-direct {v0, p0, p1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$3;-><init>(Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Landroid/support/design/widget/Snackbar;->setCallback(Landroid/support/design/widget/Snackbar$Callback;)Landroid/support/design/widget/Snackbar;

    .line 397
    invoke-virtual {v2}, Landroid/support/design/widget/Snackbar;->getView()Landroid/view/View;

    move-result-object v3

    .line 398
    .local v3, "snackBarView":Landroid/view/View;
    sget v0, Lbr/com/itau/textovoz/R$id;->snackbar_text:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/TextView;

    .line 399
    .local v4, "tvSnack":Landroid/widget/TextView;
    sget v0, Lbr/com/itau/textovoz/R$id;->snackbar_action:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/TextView;

    .line 400
    .local v5, "tvSnackAction":Landroid/widget/TextView;
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "fonts/Roboto-Regular.ttf"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v6

    .line 401
    .local v6, "arialTypeFace":Landroid/graphics/Typeface;
    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 402
    const/4 v0, 0x5

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 403
    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 405
    invoke-virtual {v2}, Landroid/support/design/widget/Snackbar;->show()V

    .line 407
    return-void
.end method


# virtual methods
.method public hideLoadingDialog()V
    .registers 3

    .line 263
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->relativeSearchLoadingContainer:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 264
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 7
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .line 465
    invoke-super {p0, p1, p2, p3}, Lbr/com/itau/textovoz/ui/activity/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 467
    const/16 v0, 0x1da6

    if-ne p1, v0, :cond_3d

    .line 468
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1c

    .line 469
    const-string v0, "return_voice"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 470
    .local v2, "matches":Ljava/lang/String;
    if-eqz v2, :cond_1b

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1b

    .line 471
    invoke-direct {p0, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setTextSearchView(Ljava/lang/String;)V

    .line 473
    .end local v2    # "matches":Ljava/lang/String;
    :cond_1b
    goto :goto_5e

    :cond_1c
    if-nez p2, :cond_5e

    .line 474
    if-eqz p3, :cond_5e

    .line 475
    const-string v0, "return_voice"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 476
    .local v2, "error":Ljava/lang/String;
    if-eqz v2, :cond_3c

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3c

    .line 477
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$string;->error_system_search_voice:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->showMessageError(Ljava/lang/String;Z)V

    .line 480
    .end local v2    # "error":Ljava/lang/String;
    :cond_3c
    goto :goto_5e

    .line 482
    :cond_3d
    const/16 v0, 0x1da7

    if-ne p1, v0, :cond_48

    .line 483
    const/4 v0, -0x1

    if-ne p2, v0, :cond_5e

    .line 484
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->finish()V

    goto :goto_5e

    .line 486
    :cond_48
    const/16 v0, 0x64

    if-ne p1, v0, :cond_5e

    .line 487
    const/4 v0, -0x1

    if-ne p2, v0, :cond_5e

    .line 488
    sget v0, Lbr/com/itau/textovoz/R$string;->link_result:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 489
    .local v2, "strings":[Ljava/lang/String;
    if-eqz v2, :cond_5e

    .line 490
    invoke-direct {p0, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->returnFaqLink([Ljava/lang/String;)V

    .line 494
    .end local v2    # "strings":[Ljava/lang/String;
    :cond_5e
    :goto_5e
    return-void
.end method

.method public onBackPressed()V
    .registers 3

    .line 766
    invoke-static {p0}, Lbr/com/itau/textovoz/util/ViewUtils;->hideSoftKeyboard(Landroid/app/Activity;)V

    .line 767
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 768
    .local v1, "intent":Landroid/content/Intent;
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setResult(ILandroid/content/Intent;)V

    .line 769
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->finish()V

    .line 770
    return-void
.end method

.method public onBalanceAccountHeaderSelected()V
    .registers 4

    .line 806
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 807
    .local v2, "intent":Landroid/content/Intent;
    sget v0, Lbr/com/itau/textovoz/R$string;->extract_intent:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 808
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setResult(ILandroid/content/Intent;)V

    .line 809
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->finish()V

    .line 810
    return-void
.end method

.method public onBalanceAccountViewBalance()V
    .registers 2

    .line 833
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->mListener:Lbr/com/itau/textovoz/SearchListenerBalance;

    if-eqz v0, :cond_9

    .line 834
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->mListener:Lbr/com/itau/textovoz/SearchListenerBalance;

    invoke-interface {v0}, Lbr/com/itau/textovoz/SearchListenerBalance;->onBalanceView()V

    .line 836
    :cond_9
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 114
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v2, Lbr/com/itau/textovoz/R$string;->fonts:I

    invoke-virtual {p0, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$string;->font_name_ttf_itau:I

    invoke-virtual {p0, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbr/com/itau/textovoz/ui/view/FontIconTypefaceHolder;->init(Landroid/content/res/AssetManager;Ljava/lang/String;)V

    .line 116
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 117
    .local v3, "extras":Landroid/os/Bundle;
    if-eqz v3, :cond_e5

    .line 118
    sget v0, Lbr/com/itau/textovoz/R$string;->menu_extra:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->menu:Ljava/util/ArrayList;

    .line 119
    sget v0, Lbr/com/itau/textovoz/R$string;->agency_extra:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->agency:Ljava/lang/String;

    .line 120
    sget v0, Lbr/com/itau/textovoz/R$string;->account_extra:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->account:Ljava/lang/String;

    .line 121
    sget v0, Lbr/com/itau/textovoz/R$string;->accountDAC_extra:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->accountDAC:Ljava/lang/String;

    .line 122
    sget v0, Lbr/com/itau/textovoz/R$string;->operator_extra:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->operator:Ljava/lang/String;

    .line 123
    sget v0, Lbr/com/itau/textovoz/R$string;->type_extra:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->segment:Ljava/lang/String;

    .line 124
    sget v0, Lbr/com/itau/textovoz/R$string;->context_extra:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lbr/com/itau/textovoz/util/SearchContext;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchContext:Lbr/com/itau/textovoz/util/SearchContext;

    .line 125
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchContext:Lbr/com/itau/textovoz/util/SearchContext;

    if-nez v0, :cond_8c

    .line 126
    sget-object v0, Lbr/com/itau/textovoz/util/SearchContext;->DEFAULT:Lbr/com/itau/textovoz/util/SearchContext;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchContext:Lbr/com/itau/textovoz/util/SearchContext;

    .line 129
    :cond_8c
    sget v0, Lbr/com/itau/textovoz/R$string;->flag_menu_extra:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->flagMenu:Z

    .line 130
    sget v0, Lbr/com/itau/textovoz/R$string;->flag_faq_extra:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->flagFaq:Z

    .line 131
    sget v0, Lbr/com/itau/textovoz/R$string;->flag_card_extra:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->flagCard:Ljava/lang/String;

    .line 132
    sget v0, Lbr/com/itau/textovoz/R$string;->flag_trans_extra:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->flagTransaction:Ljava/lang/String;

    .line 133
    sget v0, Lbr/com/itau/textovoz/R$string;->flag_attendance_extra:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->flagAttendance:Z

    .line 134
    sget v0, Lbr/com/itau/textovoz/R$string;->flag_overflow_message_extra:I

    .line 135
    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->flagOverflowMessage:Z

    .line 136
    sget v0, Lbr/com/itau/textovoz/R$string;->flag_receipts_extra:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->flagReceipts:Z

    .line 139
    :cond_e5
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->segment:Ljava/lang/String;

    invoke-static {v0}, Lbr/com/itau/textovoz/util/CommonUtils;->getSegment(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v1, v0, :cond_f4

    .line 140
    sget v0, Lbr/com/itau/textovoz/R$style;->PersonnaliteBaseTheme:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setTheme(I)V

    goto :goto_f9

    .line 142
    :cond_f4
    sget v0, Lbr/com/itau/textovoz/R$style;->ItauBaseTheme:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setTheme(I)V

    .line 145
    :goto_f9
    invoke-super {p0, p1}, Lbr/com/itau/textovoz/ui/activity/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 147
    sget v0, Lbr/com/itau/textovoz/R$layout;->activity_home_search_:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setContentView(I)V

    .line 149
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    sput-object v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->fm:Landroid/support/v4/app/FragmentManager;

    .line 151
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setToolbar()V

    .line 152
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setViews()V

    .line 153
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->initController()V

    .line 155
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 15
    .param p1, "menu"    # Landroid/view/Menu;

    .line 268
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v3

    .line 269
    .local v3, "menuInflater":Landroid/view/MenuInflater;
    sget v0, Lbr/com/itau/textovoz/R$menu;->search:I

    invoke-virtual {v3, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 271
    const-string v0, "search"

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/app/SearchManager;

    .line 272
    .local v4, "searchManager":Landroid/app/SearchManager;
    sget v0, Lbr/com/itau/textovoz/R$id;->menu_search:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 273
    .local v5, "searchMenuItem":Landroid/view/MenuItem;
    invoke-static {v5}, Landroid/support/v4/view/MenuItemCompat;->getActionView(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SearchView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    .line 275
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_dd

    .line 277
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    sget v1, Lbr/com/itau/textovoz/R$id;->search_mag_icon:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/ImageView;

    .line 278
    .local v6, "magImage":Landroid/widget/ImageView;
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    .line 279
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setMaxWidth(I)V

    .line 280
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 282
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 283
    .line 284
    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/widget/LinearLayout$LayoutParams;

    .line 285
    .local v7, "layouts":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v0, 0x0

    iput v0, v7, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 286
    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 288
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    sget v1, Lbr/com/itau/textovoz/R$id;->search_edit_frame:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/widget/LinearLayout;

    .line 290
    .line 291
    .local v8, "searchEditFrame":Landroid/widget/LinearLayout;
    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/widget/LinearLayout$LayoutParams;

    .line 292
    .local v9, "searchEditFrameLP":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v0, 0x0

    iput v0, v9, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 293
    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 295
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    sget v1, Lbr/com/itau/textovoz/R$id;->search_voice_btn:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/widget/ImageView;

    .line 296
    .local v10, "magImageVoice":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$string;->content_description_voice_button:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 298
    sget v0, Lbr/com/itau/textovoz/R$xml;->microphone:I

    invoke-static {p0, v0}, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->inflate(Landroid/content/Context;I)Lbr/com/itau/textovoz/ui/view/FontIconDrawable;

    move-result-object v11

    .line 299
    .local v11, "fontIconDrawable":Lbr/com/itau/textovoz/ui/view/FontIconDrawable;
    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 300
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v10, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 302
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    sget v1, Lbr/com/itau/textovoz/R$id;->search_src_text:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Landroid/widget/EditText;

    .line 303
    .local v12, "searchEditText":Landroid/widget/EditText;
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {v12, v0}, Landroid/widget/EditText;->setAlpha(F)V

    .line 305
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchViewOnQueryTextListener:Landroid/support/v7/widget/SearchView$OnQueryTextListener;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setOnQueryTextListener(Landroid/support/v7/widget/SearchView$OnQueryTextListener;)V

    .line 306
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    .line 307
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setIconified(Z)V

    .line 308
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    sget v1, Lbr/com/itau/textovoz/R$string;->help_search:I

    invoke-virtual {p0, v1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 309
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setIconifiedByDefault(Z)V

    .line 310
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    const v1, 0x186a0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setMaxWidth(I)V

    .line 311
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->clearFocus()V

    .line 312
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->requestFocusFromTouch()Z

    .line 315
    .end local v6    # "magImage":Landroid/widget/ImageView;
    .end local v7    # "layouts":Landroid/widget/LinearLayout$LayoutParams;
    .end local v8    # "searchEditFrame":Landroid/widget/LinearLayout;
    .end local v9    # "searchEditFrameLP":Landroid/widget/LinearLayout$LayoutParams;
    .end local v10    # "magImageVoice":Landroid/widget/ImageView;
    .end local v11    # "fontIconDrawable":Lbr/com/itau/textovoz/ui/view/FontIconDrawable;
    .end local v12    # "searchEditText":Landroid/widget/EditText;
    :cond_dd
    invoke-super {p0, p1}, Lbr/com/itau/textovoz/ui/activity/BaseActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onFaqItemClicked(Lbr/com/itau/textovoz/model/Faq;)V
    .registers 5
    .param p1, "faq"    # Lbr/com/itau/textovoz/model/Faq;

    .line 860
    sget v0, Lbr/com/itau/textovoz/R$string;->ga_categoria_busca:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v2, Lbr/com/itau/textovoz/R$string;->ga_acao_faq:I

    invoke-virtual {p0, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Faq;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v2}, Landroid/support/v7/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setEventTracker(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 861
    invoke-direct {p0, p1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->openFaq(Lbr/com/itau/textovoz/model/Faq;)V

    .line 862
    return-void
.end method

.method public onFeedbackNoClicked()V
    .registers 4

    .line 872
    sget v0, Lbr/com/itau/textovoz/R$string;->ga_categoria_busca:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$string;->ga_feedback_no:I

    invoke-virtual {p0, v1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    .line 873
    invoke-virtual {v2}, Landroid/support/v7/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 872
    invoke-virtual {p0, v0, v1, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setEventTracker(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 874
    return-void
.end method

.method public onFeedbackYesClicked()V
    .registers 4

    .line 866
    sget v0, Lbr/com/itau/textovoz/R$string;->ga_categoria_busca:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$string;->ga_feedback_yes:I

    invoke-virtual {p0, v1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    .line 867
    invoke-virtual {v2}, Landroid/support/v7/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 866
    invoke-virtual {p0, v0, v1, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setEventTracker(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 868
    return-void
.end method

.method public onMenuItemClicked(Lbr/com/itau/textovoz/model/Menu;)V
    .registers 6
    .param p1, "menu"    # Lbr/com/itau/textovoz/model/Menu;

    .line 849
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 850
    .local v3, "i":Landroid/content/Intent;
    sget v0, Lbr/com/itau/textovoz/R$string;->menu_intent:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 851
    sget v0, Lbr/com/itau/textovoz/R$string;->ga_categoria_busca:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v2, Lbr/com/itau/textovoz/R$string;->ga_acao_rota:I

    invoke-virtual {p0, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 852
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Menu;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v2}, Landroid/support/v7/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 851
    invoke-virtual {p0, v0, v1, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setEventTracker(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v3}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setResult(ILandroid/content/Intent;)V

    .line 855
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->finish()V

    .line 856
    return-void
.end method

.method public onNeedHelpClicked()V
    .registers 5

    .line 878
    sget v0, Lbr/com/itau/textovoz/R$string;->ga_categoria_busca:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$string;->ga_acao_atendimento:I

    invoke-virtual {p0, v1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    .line 879
    invoke-virtual {v2}, Landroid/support/v7/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 878
    invoke-virtual {p0, v0, v1, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setEventTracker(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 880
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 881
    .local v3, "intent":Landroid/content/Intent;
    sget v0, Lbr/com/itau/textovoz/R$string;->attendance_intent:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 882
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v3}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setResult(ILandroid/content/Intent;)V

    .line 883
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->finish()V

    .line 884
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 207
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_12

    goto :goto_d

    .line 209
    :sswitch_8
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->onBackPressed()V

    .line 210
    const/4 v0, 0x1

    return v0

    .line 212
    :goto_d
    invoke-super {p0, p1}, Lbr/com/itau/textovoz/ui/activity/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :sswitch_data_12
    .sparse-switch
        0x102002c -> :sswitch_8
    .end sparse-switch
.end method

.method protected onPause()V
    .registers 3

    .line 459
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->networkHandlerSearch:Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;->setHandler(Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch$OnPushMessageListener;)V

    .line 460
    invoke-super {p0}, Lbr/com/itau/textovoz/ui/activity/BaseActivity;->onStop()V

    .line 461
    return-void
.end method

.method public onPushMessageListSuccessful(Lbr/com/itau/textovoz/model/response/SearchResponse;)V
    .registers 15
    .param p1, "apiResponse"    # Lbr/com/itau/textovoz/model/response/SearchResponse;

    .line 524
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/response/SearchResponse;->getClassification()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_18

    .line 525
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/response/SearchResponse;->getClassification()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DUVIDA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    const/4 v3, 0x1

    goto :goto_19

    :cond_18
    const/4 v3, 0x0

    .line 527
    .local v3, "isClassification":Z
    :goto_19
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/response/SearchResponse;->getTransactions()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2b

    .line 528
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/response/SearchResponse;->getTransactions()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2b

    const/4 v4, 0x1

    goto :goto_2c

    :cond_2b
    const/4 v4, 0x0

    .line 530
    .local v4, "isTransactions":Z
    :goto_2c
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/response/SearchResponse;->isExibeFaq()Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->isExibeFaq:Z

    .line 532
    invoke-direct {p0, p1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getMenus(Lbr/com/itau/textovoz/model/response/SearchResponse;)Ljava/util/ArrayList;

    move-result-object v5

    .line 533
    .local v5, "menus":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/response/SearchResponse;->getFaqs()Ljava/util/ArrayList;

    move-result-object v6

    .line 534
    .local v6, "faqs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Faq;>;"
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/response/SearchResponse;->getCards()Ljava/util/ArrayList;

    move-result-object v7

    .line 535
    .local v7, "cards":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Cards;>;"
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/response/SearchResponse;->getOverflowMessages()Ljava/util/ArrayList;

    move-result-object v8

    .line 536
    .local v8, "overflowMessages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/OverflowMessage;>;"
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/response/SearchResponse;->getReceipts()Ljava/util/ArrayList;

    move-result-object v9

    .line 538
    .local v9, "receipts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Receipt;>;"
    if-eqz v4, :cond_c2

    iget-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->isReturnVoice:Z

    if-eqz v0, :cond_c2

    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchContext:Lbr/com/itau/textovoz/util/SearchContext;

    sget-object v1, Lbr/com/itau/textovoz/util/SearchContext;->DEFAULT:Lbr/com/itau/textovoz/util/SearchContext;

    if-ne v0, v1, :cond_c2

    .line 539
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->cleanFragments()V

    .line 540
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->isReturnVoice:Z

    .line 541
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/response/SearchResponse;->getTransactions()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lbr/com/itau/textovoz/model/Transaction;

    .line 543
    .local v10, "transaction":Lbr/com/itau/textovoz/model/Transaction;
    new-instance v11, Lbr/com/itau/textovoz/model/Menu;

    invoke-direct {v11}, Lbr/com/itau/textovoz/model/Menu;-><init>()V

    .line 544
    .local v11, "menu":Lbr/com/itau/textovoz/model/Menu;
    invoke-virtual {v10}, Lbr/com/itau/textovoz/model/Transaction;->getKeyMobile()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Lbr/com/itau/textovoz/model/Menu;->setOpKey(Ljava/lang/String;)V

    .line 545
    invoke-virtual {v10}, Lbr/com/itau/textovoz/model/Transaction;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Lbr/com/itau/textovoz/model/Menu;->setName(Ljava/lang/String;)V

    .line 547
    new-instance v12, Landroid/content/Intent;

    invoke-direct {v12}, Landroid/content/Intent;-><init>()V

    .line 548
    .local v12, "intent":Landroid/content/Intent;
    sget v0, Lbr/com/itau/textovoz/R$string;->menu_intent:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12, v0, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 549
    sget v0, Lbr/com/itau/textovoz/R$string;->ga_categoria_busca:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v2, Lbr/com/itau/textovoz/R$string;->ga_acao_transacao:I

    invoke-virtual {p0, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v11}, Lbr/com/itau/textovoz/model/Menu;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v2}, Landroid/support/v7/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setEventTracker(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v12}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setResult(ILandroid/content/Intent;)V

    .line 552
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->finish()V

    .line 553
    .end local v10    # "transaction":Lbr/com/itau/textovoz/model/Transaction;
    .end local v11    # "menu":Lbr/com/itau/textovoz/model/Menu;
    .end local v12    # "intent":Landroid/content/Intent;
    goto/16 :goto_168

    .line 554
    :cond_c2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->isReturnVoice:Z

    .line 555
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/response/SearchResponse;->getCards()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_ef

    .line 556
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_ef

    .line 557
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_ef

    .line 558
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_ef

    .line 560
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->hideLoadingDialog()V

    .line 562
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->cleanFragments()V

    .line 563
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addEmptySearchToUI()V

    .line 564
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addAttendanceToUI()V

    goto/16 :goto_168

    .line 567
    :cond_ef
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->cleanFragments()V

    .line 568
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->hideLoadingDialog()V

    .line 570
    iget-boolean v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->isExibeFaq:Z

    if-eqz v0, :cond_159

    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->textSearch:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_159

    .line 571
    sget-object v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$7;->$SwitchMap$br$com$itau$textovoz$util$SearchContext:[I

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchContext:Lbr/com/itau/textovoz/util/SearchContext;

    invoke-virtual {v1}, Lbr/com/itau/textovoz/util/SearchContext;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_16a

    goto :goto_143

    .line 573
    :pswitch_10f
    invoke-direct {p0, v6}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addFaqsToUI(Ljava/util/ArrayList;)V

    .line 574
    invoke-direct {p0, v5}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addMenusToUI(Ljava/util/ArrayList;)V

    .line 575
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addFeedbackToUI()V

    .line 576
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addAttendanceToUI()V

    .line 577
    goto/16 :goto_168

    .line 579
    :pswitch_11d
    invoke-direct {p0, v5}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addMenusToUI(Ljava/util/ArrayList;)V

    .line 580
    invoke-direct {p0, v7}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addCardsToUI(Ljava/util/ArrayList;)V

    .line 581
    invoke-direct {p0, v6}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addFaqsToUI(Ljava/util/ArrayList;)V

    .line 582
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addFeedbackToUI()V

    .line 583
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addAttendanceToUI()V

    .line 584
    goto :goto_168

    .line 586
    :pswitch_12d
    invoke-direct {p0, v9}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addReceiptsToUI(Ljava/util/ArrayList;)V

    .line 587
    invoke-direct {p0, v7}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addCardsToUI(Ljava/util/ArrayList;)V

    .line 588
    invoke-direct {p0, v8}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addOveflowMessageToUI(Ljava/util/ArrayList;)V

    .line 589
    invoke-direct {p0, v5}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addMenusToUI(Ljava/util/ArrayList;)V

    .line 590
    invoke-direct {p0, v6}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addFaqsToUI(Ljava/util/ArrayList;)V

    .line 591
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addFeedbackToUI()V

    .line 592
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addAttendanceToUI()V

    .line 593
    goto :goto_168

    .line 595
    :goto_143
    invoke-direct {p0, v9}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addReceiptsToUI(Ljava/util/ArrayList;)V

    .line 596
    invoke-direct {p0, v7}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addCardsToUI(Ljava/util/ArrayList;)V

    .line 597
    invoke-direct {p0, v8}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addOveflowMessageToUI(Ljava/util/ArrayList;)V

    .line 598
    invoke-direct {p0, v5}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addMenusToUI(Ljava/util/ArrayList;)V

    .line 599
    invoke-direct {p0, v6}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addFaqsToUI(Ljava/util/ArrayList;)V

    .line 600
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addFeedbackToUI()V

    .line 601
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addAttendanceToUI()V

    .line 602
    goto :goto_168

    .line 604
    :cond_159
    if-eqz v3, :cond_162

    .line 605
    invoke-direct {p0, v6}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addFaqsToUI(Ljava/util/ArrayList;)V

    .line 606
    invoke-direct {p0, v5}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addMenusToUI(Ljava/util/ArrayList;)V

    goto :goto_168

    .line 608
    :cond_162
    invoke-direct {p0, v5}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addMenusToUI(Ljava/util/ArrayList;)V

    .line 609
    invoke-direct {p0, v6}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->addFaqsToUI(Ljava/util/ArrayList;)V

    .line 613
    :goto_168
    return-void

    nop

    :pswitch_data_16a
    .packed-switch 0x1
        :pswitch_10f
        :pswitch_11d
        :pswitch_12d
    .end packed-switch
.end method

.method public onPushMessagesListFailure()V
    .registers 3

    .line 739
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$string;->error_system_search:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->showMessageError(Ljava/lang/String;Z)V

    .line 740
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .registers 6
    .param p1, "requestCode"    # I
    .param p2, "permissions"    # [Ljava/lang/String;
    .param p3, "grantResults"    # [I

    .line 427
    invoke-super {p0, p1, p2, p3}, Lbr/com/itau/textovoz/ui/activity/BaseActivity;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V

    .line 429
    const/4 v0, 0x6

    if-ne p1, v0, :cond_1d

    .line 430
    invoke-static {p3}, Lbr/com/itau/textovoz/util/PermissionUtils;->verifyPermissions([I)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 431
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$string;->error_permission_deneid:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->showRationaleMessage(Ljava/lang/String;)V

    goto :goto_1d

    .line 433
    :cond_1a
    invoke-direct {p0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->openSearchVoice()V

    .line 436
    :cond_1d
    :goto_1d
    return-void
.end method

.method protected onRestart()V
    .registers 2

    .line 440
    invoke-super {p0}, Lbr/com/itau/textovoz/ui/activity/BaseActivity;->onRestart()V

    .line 442
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    if-eqz v0, :cond_c

    .line 443
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {v0}, Landroid/support/v7/widget/SearchView;->clearFocus()V

    .line 446
    :cond_c
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->nestedScrollView:Landroid/support/v4/widget/NestedScrollView;

    if-eqz v0, :cond_15

    .line 447
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->nestedScrollView:Landroid/support/v4/widget/NestedScrollView;

    invoke-virtual {v0}, Landroid/support/v4/widget/NestedScrollView;->requestFocus()Z

    .line 449
    :cond_15
    return-void
.end method

.method protected onResume()V
    .registers 2

    .line 453
    invoke-super {p0}, Lbr/com/itau/textovoz/ui/activity/BaseActivity;->onResume()V

    .line 454
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->networkHandlerSearch:Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;

    invoke-virtual {v0, p0}, Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch;->setHandler(Lbr/com/itau/textovoz/api/handler/NetworkHandlerSearch$OnPushMessageListener;)V

    .line 455
    return-void
.end method

.method public search(Ljava/lang/String;)V
    .registers 2
    .param p1, "query"    # Ljava/lang/String;

    .line 319
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->textSearch:Ljava/lang/String;

    .line 320
    invoke-direct {p0, p1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchAPI(Ljava/lang/String;)V

    .line 321
    return-void
.end method

.method public showLoadingDialog()V
    .registers 3

    .line 257
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->relativeSearchLoadingContainer:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 258
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->relativeSearchLoadingContainer:Landroid/widget/RelativeLayout;

    sget v1, Lbr/com/itau/textovoz/R$string;->progressbar_description_wait:I

    invoke-virtual {p0, v1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbr/com/itau/textovoz/util/AccessibilityUtils;->setRequestFocus(Landroid/view/View;Ljava/lang/String;)V

    .line 260
    return-void
.end method

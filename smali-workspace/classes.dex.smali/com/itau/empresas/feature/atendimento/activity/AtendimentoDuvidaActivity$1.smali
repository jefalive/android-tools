.class Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity$1;
.super Ljava/lang/Object;
.source "AtendimentoDuvidaActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->configuraClick(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;

.field final synthetic val$faqs:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;Ljava/util/ArrayList;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;

    .line 104
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity$1;->this$0:Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;

    iput-object p2, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity$1;->val$faqs:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 12
    .param p1, "adapterView"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "l"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/widget/AdapterView<*>;Landroid/view/View;IJ)V"
        }
    .end annotation

    .line 108
    add-int/lit8 v5, p3, -0x1

    .line 110
    .local v5, "item":I
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity$1;->this$0:Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;

    # getter for: Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->application:Lcom/itau/empresas/CustomApplication;
    invoke-static {v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->access$000(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;)Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    iget-object v2, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity$1;->this$0:Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;

    .line 111
    const v3, 0x7f0702db

    invoke-virtual {v2, v3}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity$1;->this$0:Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;

    .line 112
    const v4, 0x7f0702ad

    invoke-virtual {v3, v4}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity$1;->val$faqs:Ljava/util/ArrayList;

    .line 113
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;

    invoke-virtual {v4}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->getPergunta()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 115
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;

    move-result-object v0

    const-string v1, "FAQ"

    iget-object v2, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity$1;->val$faqs:Ljava/util/ArrayList;

    .line 117
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/Serializable;

    .line 116
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;->extra(Ljava/lang/String;Ljava/io/Serializable;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;

    .line 117
    invoke-virtual {v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 118
    return-void
.end method

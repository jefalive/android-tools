.class public Lcom/itau/empresas/ui/view/ButtonIcon;
.super Landroid/widget/Button;
.source "ButtonIcon.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 12
    invoke-direct {p0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 13
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/ButtonIcon;->applyCustomFont(Landroid/content/Context;)V

    .line 14
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 17
    invoke-direct {p0, p1, p2}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/ButtonIcon;->applyCustomFont(Landroid/content/Context;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/ButtonIcon;->applyCustomFont(Landroid/content/Context;)V

    .line 25
    return-void
.end method

.method private applyCustomFont(Landroid/content/Context;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;

    .line 28
    const-string v0, "fonts/itaufonts_master_24px_v1.ttf"

    invoke-static {v0, p1}, Lcom/itau/empresas/ui/view/Typefaces;->getTypeface(Ljava/lang/String;Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v1

    .line 29
    .local v1, "customFont":Landroid/graphics/Typeface;
    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/view/ButtonIcon;->setTypeface(Landroid/graphics/Typeface;)V

    .line 30
    return-void
.end method

.class public abstract Lbr/com/itau/textovoz/ui/activity/BaseActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "BaseActivity.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 9
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onStart()V
    .registers 1

    .line 13
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onStart()V

    .line 14
    return-void
.end method

.method public setEventTracker(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 12
    .param p1, "categoria"    # Ljava/lang/String;
    .param p2, "acao"    # Ljava/lang/String;
    .param p3, "label"    # Ljava/lang/String;

    .line 18
    const/4 v5, 0x0

    .line 19
    .local v5, "method":Ljava/lang/reflect/Method;
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/BaseActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/activity/BaseActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 22
    .local v6, "customApplication":Ljava/lang/Object;
    :try_start_11
    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "setEventTracker"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x2

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_2c
    .catch Ljava/lang/NoSuchMethodException; {:try_start_11 .. :try_end_2c} :catch_2f

    move-result-object v0

    move-object v5, v0

    .line 25
    goto :goto_33

    .line 23
    :catch_2f
    move-exception v7

    .line 24
    .local v7, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v7}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    .line 27
    .end local v7    # "e":Ljava/lang/NoSuchMethodException;
    :goto_33
    if-eqz v5, :cond_50

    .line 29
    const/4 v0, 0x3

    :try_start_36
    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    aput-object p3, v0, v1

    invoke-virtual {v5, v6, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    const-string v0, "TesteGA"

    const-string v1, "fromInvocation"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4b
    .catch Ljava/lang/IllegalAccessException; {:try_start_36 .. :try_end_4b} :catch_4c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_36 .. :try_end_4b} :catch_4c
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_36 .. :try_end_4b} :catch_4c

    .line 34
    goto :goto_50

    .line 32
    :catch_4c
    move-exception v7

    .line 33
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    .line 36
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_50
    :goto_50
    return-void
.end method

.class public final Lcom/google/android/gms/maps/internal/zza;
.super Ljava/lang/Object;


# direct methods
.method public static zza(B)Ljava/lang/Boolean;
    .registers 2

    sparse-switch p0, :sswitch_data_c

    goto :goto_a

    :sswitch_4
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0

    :sswitch_7
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object v0

    :goto_a
    const/4 v0, 0x0

    return-object v0

    :sswitch_data_c
    .sparse-switch
        0x0 -> :sswitch_7
        0x1 -> :sswitch_4
    .end sparse-switch
.end method

.method public static zze(Ljava/lang/Boolean;)B
    .registers 2

    if-eqz p0, :cond_c

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    goto :goto_d

    :cond_a
    const/4 v0, 0x0

    goto :goto_d

    :cond_c
    const/4 v0, -0x1

    :goto_d
    return v0
.end method

.class public Landroid/support/transition/TransitionManager;
.super Ljava/lang/Object;
.source "TransitionManager.java"


# static fields
.field private static sImpl:Landroid/support/transition/TransitionManagerStaticsImpl;


# instance fields
.field private mImpl:Landroid/support/transition/TransitionManagerImpl;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    .line 42
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_e

    .line 43
    new-instance v0, Landroid/support/transition/TransitionManagerStaticsIcs;

    invoke-direct {v0}, Landroid/support/transition/TransitionManagerStaticsIcs;-><init>()V

    sput-object v0, Landroid/support/transition/TransitionManager;->sImpl:Landroid/support/transition/TransitionManagerStaticsImpl;

    goto :goto_15

    .line 45
    :cond_e
    new-instance v0, Landroid/support/transition/TransitionManagerStaticsKitKat;

    invoke-direct {v0}, Landroid/support/transition/TransitionManagerStaticsKitKat;-><init>()V

    sput-object v0, Landroid/support/transition/TransitionManager;->sImpl:Landroid/support/transition/TransitionManagerStaticsImpl;

    .line 47
    :goto_15
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_11

    .line 53
    new-instance v0, Landroid/support/transition/TransitionManagerIcs;

    invoke-direct {v0}, Landroid/support/transition/TransitionManagerIcs;-><init>()V

    iput-object v0, p0, Landroid/support/transition/TransitionManager;->mImpl:Landroid/support/transition/TransitionManagerImpl;

    goto :goto_18

    .line 55
    :cond_11
    new-instance v0, Landroid/support/transition/TransitionManagerKitKat;

    invoke-direct {v0}, Landroid/support/transition/TransitionManagerKitKat;-><init>()V

    iput-object v0, p0, Landroid/support/transition/TransitionManager;->mImpl:Landroid/support/transition/TransitionManagerImpl;

    .line 57
    :goto_18
    return-void
.end method

.method public static beginDelayedTransition(Landroid/view/ViewGroup;Landroid/support/transition/Transition;)V
    .registers 4
    .param p0, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p1, "transition"    # Landroid/support/transition/Transition;

    .line 125
    sget-object v0, Landroid/support/transition/TransitionManager;->sImpl:Landroid/support/transition/TransitionManagerStaticsImpl;

    if-nez p1, :cond_6

    const/4 v1, 0x0

    goto :goto_8

    :cond_6
    iget-object v1, p1, Landroid/support/transition/Transition;->mImpl:Landroid/support/transition/TransitionImpl;

    :goto_8
    invoke-virtual {v0, p0, v1}, Landroid/support/transition/TransitionManagerStaticsImpl;->beginDelayedTransition(Landroid/view/ViewGroup;Landroid/support/transition/TransitionImpl;)V

    .line 126
    return-void
.end method

.class Lcom/google/android/gms/internal/zzeg$2;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/zzji$zzc;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/internal/zzeg;->zzeq()Lcom/google/android/gms/internal/zzeg$zze;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Lcom/google/android/gms/internal/zzji$zzc<Lcom/google/android/gms/internal/zzed;>;"
    }
.end annotation


# instance fields
.field final synthetic zzBe:Lcom/google/android/gms/internal/zzeg;

.field final synthetic zzBl:Lcom/google/android/gms/internal/zzeg$zze;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/zzeg;Lcom/google/android/gms/internal/zzeg$zze;)V
    .registers 3

    iput-object p1, p0, Lcom/google/android/gms/internal/zzeg$2;->zzBe:Lcom/google/android/gms/internal/zzeg;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzeg$2;->zzBl:Lcom/google/android/gms/internal/zzeg$zze;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public zza(Lcom/google/android/gms/internal/zzed;)V
    .registers 6

    iget-object v0, p0, Lcom/google/android/gms/internal/zzeg$2;->zzBe:Lcom/google/android/gms/internal/zzeg;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzeg;->zzc(Lcom/google/android/gms/internal/zzeg;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    :try_start_7
    iget-object v0, p0, Lcom/google/android/gms/internal/zzeg$2;->zzBe:Lcom/google/android/gms/internal/zzeg;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzeg;->zza(Lcom/google/android/gms/internal/zzeg;I)I

    iget-object v0, p0, Lcom/google/android/gms/internal/zzeg$2;->zzBe:Lcom/google/android/gms/internal/zzeg;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzeg;->zzg(Lcom/google/android/gms/internal/zzeg;)Lcom/google/android/gms/internal/zzeg$zze;

    move-result-object v0

    if-eqz v0, :cond_2d

    iget-object v0, p0, Lcom/google/android/gms/internal/zzeg$2;->zzBl:Lcom/google/android/gms/internal/zzeg$zze;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzeg$2;->zzBe:Lcom/google/android/gms/internal/zzeg;

    invoke-static {v1}, Lcom/google/android/gms/internal/zzeg;->zzg(Lcom/google/android/gms/internal/zzeg;)Lcom/google/android/gms/internal/zzeg$zze;

    move-result-object v1

    if-eq v0, v1, :cond_2d

    const-string v0, "New JS engine is loaded, marking previous one as destroyable."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzeg$2;->zzBe:Lcom/google/android/gms/internal/zzeg;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzeg;->zzg(Lcom/google/android/gms/internal/zzeg;)Lcom/google/android/gms/internal/zzeg$zze;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzeg$zze;->zzeu()V

    :cond_2d
    iget-object v0, p0, Lcom/google/android/gms/internal/zzeg$2;->zzBe:Lcom/google/android/gms/internal/zzeg;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzeg$2;->zzBl:Lcom/google/android/gms/internal/zzeg$zze;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzeg;->zza(Lcom/google/android/gms/internal/zzeg;Lcom/google/android/gms/internal/zzeg$zze;)Lcom/google/android/gms/internal/zzeg$zze;
    :try_end_34
    .catchall {:try_start_7 .. :try_end_34} :catchall_36

    monitor-exit v2

    goto :goto_39

    :catchall_36
    move-exception v3

    monitor-exit v2

    throw v3

    :goto_39
    return-void
.end method

.method public synthetic zze(Ljava/lang/Object;)V
    .registers 3

    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/internal/zzed;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/zzeg$2;->zza(Lcom/google/android/gms/internal/zzed;)V

    return-void
.end method

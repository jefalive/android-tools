.class final Lorg/threeten/bp/Month$1;
.super Ljava/lang/Object;
.source "Month.java"

# interfaces
.implements Lorg/threeten/bp/temporal/TemporalQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/threeten/bp/Month;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Lorg/threeten/bp/temporal/TemporalQuery<Lorg/threeten/bp/Month;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic queryFrom(Lorg/threeten/bp/temporal/TemporalAccessor;)Ljava/lang/Object;
    .registers 3
    .param p1, "x0"    # Lorg/threeten/bp/temporal/TemporalAccessor;

    .line 140
    invoke-virtual {p0, p1}, Lorg/threeten/bp/Month$1;->queryFrom(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/Month;

    move-result-object v0

    return-object v0
.end method

.method public queryFrom(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/Month;
    .registers 3
    .param p1, "temporal"    # Lorg/threeten/bp/temporal/TemporalAccessor;

    .line 143
    invoke-static {p1}, Lorg/threeten/bp/Month;->from(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/Month;

    move-result-object v0

    return-object v0
.end method

.class Lbr/com/itau/topsnackbar/ViewManagerFactory;
.super Ljava/lang/Object;
.source "ViewManagerFactory.java"


# direct methods
.method static createManager(Landroid/view/ViewGroup;)Lbr/com/itau/topsnackbar/ViewManager;
    .registers 4
    .param p0, "parent"    # Landroid/view/ViewGroup;

    .line 17
    instance-of v0, p0, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1c

    .line 18
    move-object v2, p0

    check-cast v2, Landroid/widget/LinearLayout;

    .line 19
    .local v2, "linearLayout":Landroid/widget/LinearLayout;
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getOrientation()I

    move-result v0

    if-nez v0, :cond_16

    .line 20
    invoke-static {p0}, Lbr/com/itau/topsnackbar/ViewManagerFactory;->findSuitableParent(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object p0

    .line 21
    invoke-static {p0}, Lbr/com/itau/topsnackbar/ViewManagerFactory;->createManager(Landroid/view/ViewGroup;)Lbr/com/itau/topsnackbar/ViewManager;

    move-result-object v0

    return-object v0

    .line 23
    :cond_16
    new-instance v0, Lbr/com/itau/topsnackbar/LinearLayoutManager;

    invoke-direct {v0, v2}, Lbr/com/itau/topsnackbar/LinearLayoutManager;-><init>(Landroid/widget/LinearLayout;)V

    return-object v0

    .line 25
    .end local v2    # "linearLayout":Landroid/widget/LinearLayout;
    :cond_1c
    instance-of v0, p0, Landroid/support/constraint/ConstraintLayout;

    if-eqz v0, :cond_29

    .line 26
    new-instance v0, Lbr/com/itau/topsnackbar/ConstraintLayoutManager;

    move-object v1, p0

    check-cast v1, Landroid/support/constraint/ConstraintLayout;

    invoke-direct {v0, v1}, Lbr/com/itau/topsnackbar/ConstraintLayoutManager;-><init>(Landroid/support/constraint/ConstraintLayout;)V

    return-object v0

    .line 28
    :cond_29
    instance-of v0, p0, Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_36

    .line 29
    new-instance v0, Lbr/com/itau/topsnackbar/RelativeLayoutManager;

    move-object v1, p0

    check-cast v1, Landroid/widget/RelativeLayout;

    invoke-direct {v0, v1}, Lbr/com/itau/topsnackbar/RelativeLayoutManager;-><init>(Landroid/widget/RelativeLayout;)V

    return-object v0

    .line 30
    :cond_36
    new-instance v0, Lbr/com/itau/topsnackbar/DefaultManager;

    invoke-direct {v0, p0}, Lbr/com/itau/topsnackbar/DefaultManager;-><init>(Landroid/view/ViewGroup;)V

    return-object v0
.end method

.method private static findSuitableParent(Landroid/view/View;)Landroid/view/ViewGroup;
    .registers 5
    .param p0, "view"    # Landroid/view/View;

    .line 34
    const/4 v2, 0x0

    .line 36
    .local v2, "fallback":Landroid/view/ViewGroup;
    :cond_1
    if-nez p0, :cond_4

    goto :goto_3b

    .line 37
    :cond_4
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CoordinatorLayout"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 39
    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0

    .line 41
    :cond_18
    instance-of v0, p0, Landroid/widget/FrameLayout;

    if-eqz v0, :cond_2c

    .line 42
    invoke-virtual {p0}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x1020002

    if-ne v0, v1, :cond_29

    .line 43
    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0

    .line 45
    :cond_29
    move-object v2, p0

    check-cast v2, Landroid/view/ViewGroup;

    .line 49
    :cond_2c
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    .line 50
    .local v3, "parent":Landroid/view/ViewParent;
    instance-of v0, v3, Landroid/view/View;

    if-eqz v0, :cond_38

    move-object p0, v3

    check-cast p0, Landroid/view/View;

    goto :goto_39

    :cond_38
    const/4 p0, 0x0

    .line 51
    .end local v3    # "parent":Landroid/view/ViewParent;
    :goto_39
    if-nez p0, :cond_1

    .line 53
    :goto_3b
    return-object v2
.end method

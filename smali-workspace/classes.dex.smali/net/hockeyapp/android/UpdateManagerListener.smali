.class public abstract Lnet/hockeyapp/android/UpdateManagerListener;
.super Lnet/hockeyapp/android/StringListener;
.source "UpdateManagerListener.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 41
    invoke-direct {p0}, Lnet/hockeyapp/android/StringListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onNoUpdateAvailable()V
    .registers 1

    .line 65
    return-void
.end method

.method public onUpdateAvailable()V
    .registers 1

    .line 72
    return-void
.end method

.method public onUpdateAvailable(Lorg/json/JSONArray;Ljava/lang/String;)V
    .registers 3
    .param p1, "data"    # Lorg/json/JSONArray;
    .param p2, "url"    # Ljava/lang/String;

    .line 80
    invoke-virtual {p0}, Lnet/hockeyapp/android/UpdateManagerListener;->onUpdateAvailable()V

    .line 81
    return-void
.end method

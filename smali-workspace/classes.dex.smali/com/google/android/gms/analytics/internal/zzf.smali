.class public Lcom/google/android/gms/analytics/internal/zzf;
.super Ljava/lang/Object;


# static fields
.field private static zzQn:Lcom/google/android/gms/analytics/internal/zzf;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final zzQA:Lcom/google/android/gms/analytics/internal/zzu;

.field private final zzQo:Landroid/content/Context;

.field private final zzQp:Lcom/google/android/gms/analytics/internal/zzr;

.field private final zzQq:Lcom/google/android/gms/analytics/internal/zzaf;

.field private final zzQr:Lcom/google/android/gms/measurement/zzg;

.field private final zzQs:Lcom/google/android/gms/analytics/internal/zzb;

.field private final zzQt:Lcom/google/android/gms/analytics/internal/zzv;

.field private final zzQu:Lcom/google/android/gms/analytics/internal/zzan;

.field private final zzQv:Lcom/google/android/gms/analytics/internal/zzai;

.field private final zzQw:Lcom/google/android/gms/analytics/GoogleAnalytics;

.field private final zzQx:Lcom/google/android/gms/analytics/internal/zzn;

.field private final zzQy:Lcom/google/android/gms/analytics/internal/zza;

.field private final zzQz:Lcom/google/android/gms/analytics/internal/zzk;

.field private final zzqW:Lcom/google/android/gms/internal/zzmq;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/analytics/internal/zzg;)V
    .registers 18

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/analytics/internal/zzg;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v0, "Application context can\'t be null"

    invoke-static {v3, v0}, Lcom/google/android/gms/common/internal/zzx;->zzb(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    instance-of v0, v3, Landroid/app/Application;

    const-string v1, "getApplicationContext didn\'t return the application"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->zzb(ZLjava/lang/Object;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/analytics/internal/zzg;->zzjx()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/common/internal/zzx;->zzz(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/gms/analytics/internal/zzf;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/gms/analytics/internal/zzf;->zzQo:Landroid/content/Context;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/internal/zzg;->zzh(Lcom/google/android/gms/analytics/internal/zzf;)Lcom/google/android/gms/internal/zzmq;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/gms/analytics/internal/zzf;->zzqW:Lcom/google/android/gms/internal/zzmq;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/internal/zzg;->zzg(Lcom/google/android/gms/analytics/internal/zzf;)Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/gms/analytics/internal/zzf;->zzQp:Lcom/google/android/gms/analytics/internal/zzr;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/internal/zzg;->zzf(Lcom/google/android/gms/analytics/internal/zzf;)Lcom/google/android/gms/analytics/internal/zzaf;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/analytics/internal/zzaf;->zza()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/gms/analytics/internal/zzf;->zzQq:Lcom/google/android/gms/analytics/internal/zzaf;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzf;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkr()Z

    move-result v0

    if-eqz v0, :cond_76

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzf;->zzjm()Lcom/google/android/gms/analytics/internal/zzaf;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Google Analytics "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/analytics/internal/zze;->VERSION:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is starting up."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/internal/zzaf;->zzbf(Ljava/lang/String;)V

    goto :goto_aa

    :cond_76
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzf;->zzjm()Lcom/google/android/gms/analytics/internal/zzaf;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Google Analytics "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/analytics/internal/zze;->VERSION:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is starting up. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "To enable debug logging on a device run:\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  adb shell setprop log.tag.GAv4 DEBUG\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  adb logcat -s GAv4"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/internal/zzaf;->zzbf(Ljava/lang/String;)V

    :goto_aa
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/internal/zzg;->zzq(Lcom/google/android/gms/analytics/internal/zzf;)Lcom/google/android/gms/analytics/internal/zzai;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/analytics/internal/zzai;->zza()V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/gms/analytics/internal/zzf;->zzQv:Lcom/google/android/gms/analytics/internal/zzai;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/internal/zzg;->zze(Lcom/google/android/gms/analytics/internal/zzf;)Lcom/google/android/gms/analytics/internal/zzan;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/analytics/internal/zzan;->zza()V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/gms/analytics/internal/zzf;->zzQu:Lcom/google/android/gms/analytics/internal/zzan;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/internal/zzg;->zzl(Lcom/google/android/gms/analytics/internal/zzf;)Lcom/google/android/gms/analytics/internal/zzb;

    move-result-object v8

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/internal/zzg;->zzd(Lcom/google/android/gms/analytics/internal/zzf;)Lcom/google/android/gms/analytics/internal/zzn;

    move-result-object v9

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/internal/zzg;->zzc(Lcom/google/android/gms/analytics/internal/zzf;)Lcom/google/android/gms/analytics/internal/zza;

    move-result-object v10

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/internal/zzg;->zzb(Lcom/google/android/gms/analytics/internal/zzf;)Lcom/google/android/gms/analytics/internal/zzk;

    move-result-object v11

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/internal/zzg;->zza(Lcom/google/android/gms/analytics/internal/zzf;)Lcom/google/android/gms/analytics/internal/zzu;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/gms/analytics/internal/zzg;->zzab(Landroid/content/Context;)Lcom/google/android/gms/measurement/zzg;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzf;->zzjw()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    invoke-virtual {v13, v0}, Lcom/google/android/gms/measurement/zzg;->zza(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/gms/analytics/internal/zzf;->zzQr:Lcom/google/android/gms/measurement/zzg;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/internal/zzg;->zzi(Lcom/google/android/gms/analytics/internal/zzf;)Lcom/google/android/gms/analytics/GoogleAnalytics;

    move-result-object v14

    invoke-virtual {v9}, Lcom/google/android/gms/analytics/internal/zzn;->zza()V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/gms/analytics/internal/zzf;->zzQx:Lcom/google/android/gms/analytics/internal/zzn;

    invoke-virtual {v10}, Lcom/google/android/gms/analytics/internal/zza;->zza()V

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/google/android/gms/analytics/internal/zzf;->zzQy:Lcom/google/android/gms/analytics/internal/zza;

    invoke-virtual {v11}, Lcom/google/android/gms/analytics/internal/zzk;->zza()V

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/gms/analytics/internal/zzf;->zzQz:Lcom/google/android/gms/analytics/internal/zzk;

    invoke-virtual {v12}, Lcom/google/android/gms/analytics/internal/zzu;->zza()V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/gms/analytics/internal/zzf;->zzQA:Lcom/google/android/gms/analytics/internal/zzu;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/internal/zzg;->zzp(Lcom/google/android/gms/analytics/internal/zzf;)Lcom/google/android/gms/analytics/internal/zzv;

    move-result-object v15

    invoke-virtual {v15}, Lcom/google/android/gms/analytics/internal/zzv;->zza()V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/gms/analytics/internal/zzf;->zzQt:Lcom/google/android/gms/analytics/internal/zzv;

    invoke-virtual {v8}, Lcom/google/android/gms/analytics/internal/zzb;->zza()V

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/gms/analytics/internal/zzf;->zzQs:Lcom/google/android/gms/analytics/internal/zzb;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzf;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkr()Z

    move-result v0

    if-eqz v0, :cond_150

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzf;->zzjm()Lcom/google/android/gms/analytics/internal/zzaf;

    move-result-object v0

    const-string v1, "Device AnalyticsService version"

    sget-object v2, Lcom/google/android/gms/analytics/internal/zze;->VERSION:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/analytics/internal/zzaf;->zzb(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_150
    invoke-virtual {v14}, Lcom/google/android/gms/analytics/GoogleAnalytics;->zza()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/gms/analytics/internal/zzf;->zzQw:Lcom/google/android/gms/analytics/GoogleAnalytics;

    invoke-virtual {v8}, Lcom/google/android/gms/analytics/internal/zzb;->start()V

    return-void
.end method

.method private zza(Lcom/google/android/gms/analytics/internal/zzd;)V
    .registers 4

    const-string v0, "Analytics service not created/initialized"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/zzx;->zzb(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzd;->isInitialized()Z

    move-result v0

    const-string v1, "Analytics service not initialized"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->zzb(ZLjava/lang/Object;)V

    return-void
.end method

.method public static zzaa(Landroid/content/Context;)Lcom/google/android/gms/analytics/internal/zzf;
    .registers 17

    invoke-static/range {p0 .. p0}, Lcom/google/android/gms/common/internal/zzx;->zzz(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/analytics/internal/zzf;->zzQn:Lcom/google/android/gms/analytics/internal/zzf;

    if-nez v0, :cond_55

    const-class v4, Lcom/google/android/gms/analytics/internal/zzf;

    monitor-enter v4

    :try_start_a
    sget-object v0, Lcom/google/android/gms/analytics/internal/zzf;->zzQn:Lcom/google/android/gms/analytics/internal/zzf;

    if-nez v0, :cond_50

    invoke-static {}, Lcom/google/android/gms/internal/zzmt;->zzsc()Lcom/google/android/gms/internal/zzmq;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/gms/internal/zzmq;->elapsedRealtime()J

    move-result-wide v6

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    new-instance v9, Lcom/google/android/gms/analytics/internal/zzg;

    invoke-direct {v9, v8}, Lcom/google/android/gms/analytics/internal/zzg;-><init>(Landroid/content/Context;)V

    new-instance v10, Lcom/google/android/gms/analytics/internal/zzf;

    invoke-direct {v10, v9}, Lcom/google/android/gms/analytics/internal/zzf;-><init>(Lcom/google/android/gms/analytics/internal/zzg;)V

    sput-object v10, Lcom/google/android/gms/analytics/internal/zzf;->zzQn:Lcom/google/android/gms/analytics/internal/zzf;

    invoke-static {}, Lcom/google/android/gms/analytics/GoogleAnalytics;->zziF()V

    invoke-interface {v5}, Lcom/google/android/gms/internal/zzmq;->elapsedRealtime()J

    move-result-wide v0

    sub-long v11, v0, v6

    sget-object v0, Lcom/google/android/gms/analytics/internal/zzy;->zzSz:Lcom/google/android/gms/analytics/internal/zzy$zza;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzy$zza;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v13

    cmp-long v0, v11, v13

    if-lez v0, :cond_50

    invoke-virtual {v10}, Lcom/google/android/gms/analytics/internal/zzf;->zzjm()Lcom/google/android/gms/analytics/internal/zzaf;

    move-result-object v0

    const-string v1, "Slow initialization (ms)"

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/analytics/internal/zzaf;->zzc(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_50
    .catchall {:try_start_a .. :try_end_50} :catchall_52

    :cond_50
    monitor-exit v4

    goto :goto_55

    :catchall_52
    move-exception v15

    monitor-exit v4

    throw v15

    :cond_55
    :goto_55
    sget-object v0, Lcom/google/android/gms/analytics/internal/zzf;->zzQn:Lcom/google/android/gms/analytics/internal/zzf;

    return-object v0
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public zziH()Lcom/google/android/gms/analytics/internal/zzb;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQs:Lcom/google/android/gms/analytics/internal/zzb;

    invoke-direct {p0, v0}, Lcom/google/android/gms/analytics/internal/zzf;->zza(Lcom/google/android/gms/analytics/internal/zzd;)V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQs:Lcom/google/android/gms/analytics/internal/zzb;

    return-object v0
.end method

.method public zziI()Lcom/google/android/gms/analytics/internal/zzan;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQu:Lcom/google/android/gms/analytics/internal/zzan;

    invoke-direct {p0, v0}, Lcom/google/android/gms/analytics/internal/zzf;->zza(Lcom/google/android/gms/analytics/internal/zzd;)V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQu:Lcom/google/android/gms/analytics/internal/zzan;

    return-object v0
.end method

.method public zzjA()Lcom/google/android/gms/analytics/internal/zzai;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQv:Lcom/google/android/gms/analytics/internal/zzai;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQv:Lcom/google/android/gms/analytics/internal/zzai;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzai;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_e

    :cond_c
    const/4 v0, 0x0

    return-object v0

    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQv:Lcom/google/android/gms/analytics/internal/zzai;

    return-object v0
.end method

.method public zzjB()Lcom/google/android/gms/analytics/internal/zza;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQy:Lcom/google/android/gms/analytics/internal/zza;

    invoke-direct {p0, v0}, Lcom/google/android/gms/analytics/internal/zzf;->zza(Lcom/google/android/gms/analytics/internal/zzd;)V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQy:Lcom/google/android/gms/analytics/internal/zza;

    return-object v0
.end method

.method public zzjC()Lcom/google/android/gms/analytics/internal/zzn;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQx:Lcom/google/android/gms/analytics/internal/zzn;

    invoke-direct {p0, v0}, Lcom/google/android/gms/analytics/internal/zzf;->zza(Lcom/google/android/gms/analytics/internal/zzd;)V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQx:Lcom/google/android/gms/analytics/internal/zzn;

    return-object v0
.end method

.method public zzjk()V
    .registers 1

    invoke-static {}, Lcom/google/android/gms/measurement/zzg;->zzjk()V

    return-void
.end method

.method public zzjl()Lcom/google/android/gms/internal/zzmq;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzqW:Lcom/google/android/gms/internal/zzmq;

    return-object v0
.end method

.method public zzjm()Lcom/google/android/gms/analytics/internal/zzaf;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQq:Lcom/google/android/gms/analytics/internal/zzaf;

    invoke-direct {p0, v0}, Lcom/google/android/gms/analytics/internal/zzf;->zza(Lcom/google/android/gms/analytics/internal/zzd;)V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQq:Lcom/google/android/gms/analytics/internal/zzaf;

    return-object v0
.end method

.method public zzjn()Lcom/google/android/gms/analytics/internal/zzr;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQp:Lcom/google/android/gms/analytics/internal/zzr;

    return-object v0
.end method

.method public zzjo()Lcom/google/android/gms/measurement/zzg;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQr:Lcom/google/android/gms/measurement/zzg;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/zzx;->zzz(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQr:Lcom/google/android/gms/measurement/zzg;

    return-object v0
.end method

.method public zzjp()Lcom/google/android/gms/analytics/internal/zzv;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQt:Lcom/google/android/gms/analytics/internal/zzv;

    invoke-direct {p0, v0}, Lcom/google/android/gms/analytics/internal/zzf;->zza(Lcom/google/android/gms/analytics/internal/zzd;)V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQt:Lcom/google/android/gms/analytics/internal/zzv;

    return-object v0
.end method

.method public zzjq()Lcom/google/android/gms/analytics/internal/zzai;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQv:Lcom/google/android/gms/analytics/internal/zzai;

    invoke-direct {p0, v0}, Lcom/google/android/gms/analytics/internal/zzf;->zza(Lcom/google/android/gms/analytics/internal/zzd;)V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQv:Lcom/google/android/gms/analytics/internal/zzai;

    return-object v0
.end method

.method public zzjt()Lcom/google/android/gms/analytics/internal/zzk;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQz:Lcom/google/android/gms/analytics/internal/zzk;

    invoke-direct {p0, v0}, Lcom/google/android/gms/analytics/internal/zzf;->zza(Lcom/google/android/gms/analytics/internal/zzd;)V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQz:Lcom/google/android/gms/analytics/internal/zzk;

    return-object v0
.end method

.method public zzju()Lcom/google/android/gms/analytics/internal/zzu;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQA:Lcom/google/android/gms/analytics/internal/zzu;

    return-object v0
.end method

.method protected zzjw()Ljava/lang/Thread$UncaughtExceptionHandler;
    .registers 2

    new-instance v0, Lcom/google/android/gms/analytics/internal/zzf$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/analytics/internal/zzf$1;-><init>(Lcom/google/android/gms/analytics/internal/zzf;)V

    return-object v0
.end method

.method public zzjx()Landroid/content/Context;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQo:Landroid/content/Context;

    return-object v0
.end method

.method public zzjy()Lcom/google/android/gms/analytics/internal/zzaf;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQq:Lcom/google/android/gms/analytics/internal/zzaf;

    return-object v0
.end method

.method public zzjz()Lcom/google/android/gms/analytics/GoogleAnalytics;
    .registers 3

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQw:Lcom/google/android/gms/analytics/GoogleAnalytics;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/zzx;->zzz(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQw:Lcom/google/android/gms/analytics/GoogleAnalytics;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/GoogleAnalytics;->isInitialized()Z

    move-result v0

    const-string v1, "Analytics instance not initialized"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->zzb(ZLjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzf;->zzQw:Lcom/google/android/gms/analytics/GoogleAnalytics;

    return-object v0
.end method

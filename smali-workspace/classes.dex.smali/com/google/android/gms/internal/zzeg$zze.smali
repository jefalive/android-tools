.class public Lcom/google/android/gms/internal/zzeg$zze;
.super Lcom/google/android/gms/internal/zzjj;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/zzeg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "zze"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/zzjj<Lcom/google/android/gms/internal/zzed;>;"
    }
.end annotation


# instance fields
.field private zzBa:Lcom/google/android/gms/internal/zzeg$zzb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/zzeg$zzb<Lcom/google/android/gms/internal/zzed;>;"
        }
    .end annotation
.end field

.field private zzBr:Z

.field private zzBs:I

.field private final zzpV:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/zzeg$zzb;)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/google/android/gms/internal/zzeg$zzb<Lcom/google/android/gms/internal/zzed;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzjj;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzeg$zze;->zzpV:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/gms/internal/zzeg$zze;->zzBa:Lcom/google/android/gms/internal/zzeg$zzb;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzeg$zze;->zzBr:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/internal/zzeg$zze;->zzBs:I

    return-void
.end method

.method static synthetic zza(Lcom/google/android/gms/internal/zzeg$zze;)Lcom/google/android/gms/internal/zzeg$zzb;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzeg$zze;->zzBa:Lcom/google/android/gms/internal/zzeg$zzb;

    return-object v0
.end method


# virtual methods
.method public zzes()Lcom/google/android/gms/internal/zzeg$zzd;
    .registers 6

    new-instance v2, Lcom/google/android/gms/internal/zzeg$zzd;

    invoke-direct {v2, p0}, Lcom/google/android/gms/internal/zzeg$zzd;-><init>(Lcom/google/android/gms/internal/zzeg$zze;)V

    iget-object v3, p0, Lcom/google/android/gms/internal/zzeg$zze;->zzpV:Ljava/lang/Object;

    monitor-enter v3

    :try_start_8
    new-instance v0, Lcom/google/android/gms/internal/zzeg$zze$1;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/internal/zzeg$zze$1;-><init>(Lcom/google/android/gms/internal/zzeg$zze;Lcom/google/android/gms/internal/zzeg$zzd;)V

    new-instance v1, Lcom/google/android/gms/internal/zzeg$zze$2;

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/internal/zzeg$zze$2;-><init>(Lcom/google/android/gms/internal/zzeg$zze;Lcom/google/android/gms/internal/zzeg$zzd;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/zzeg$zze;->zza(Lcom/google/android/gms/internal/zzji$zzc;Lcom/google/android/gms/internal/zzji$zza;)V

    iget v0, p0, Lcom/google/android/gms/internal/zzeg$zze;->zzBs:I

    if-ltz v0, :cond_1b

    const/4 v0, 0x1

    goto :goto_1c

    :cond_1b
    const/4 v0, 0x0

    :goto_1c
    invoke-static {v0}, Lcom/google/android/gms/common/internal/zzx;->zzab(Z)V

    iget v0, p0, Lcom/google/android/gms/internal/zzeg$zze;->zzBs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzeg$zze;->zzBs:I
    :try_end_25
    .catchall {:try_start_8 .. :try_end_25} :catchall_27

    monitor-exit v3

    goto :goto_2a

    :catchall_27
    move-exception v4

    monitor-exit v3

    throw v4

    :goto_2a
    return-object v2
.end method

.method protected zzet()V
    .registers 5

    iget-object v2, p0, Lcom/google/android/gms/internal/zzeg$zze;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget v0, p0, Lcom/google/android/gms/internal/zzeg$zze;->zzBs:I

    const/4 v1, 0x1

    if-lt v0, v1, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    invoke-static {v0}, Lcom/google/android/gms/common/internal/zzx;->zzab(Z)V

    const-string v0, "Releasing 1 reference for JS Engine"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/internal/zzeg$zze;->zzBs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzeg$zze;->zzBs:I

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzeg$zze;->zzev()V
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1e

    monitor-exit v2

    goto :goto_21

    :catchall_1e
    move-exception v3

    monitor-exit v2

    throw v3

    :goto_21
    return-void
.end method

.method public zzeu()V
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzeg$zze;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget v0, p0, Lcom/google/android/gms/internal/zzeg$zze;->zzBs:I

    if-ltz v0, :cond_9

    const/4 v0, 0x1

    goto :goto_a

    :cond_9
    const/4 v0, 0x0

    :goto_a
    invoke-static {v0}, Lcom/google/android/gms/common/internal/zzx;->zzab(Z)V

    const-string v0, "Releasing root reference. JS Engine will be destroyed once other references are released."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzeg$zze;->zzBr:Z

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzeg$zze;->zzev()V
    :try_end_18
    .catchall {:try_start_3 .. :try_end_18} :catchall_1a

    monitor-exit v1

    goto :goto_1d

    :catchall_1a
    move-exception v2

    monitor-exit v1

    throw v2

    :goto_1d
    return-void
.end method

.method protected zzev()V
    .registers 5

    iget-object v2, p0, Lcom/google/android/gms/internal/zzeg$zze;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget v0, p0, Lcom/google/android/gms/internal/zzeg$zze;->zzBs:I

    if-ltz v0, :cond_9

    const/4 v0, 0x1

    goto :goto_a

    :cond_9
    const/4 v0, 0x0

    :goto_a
    invoke-static {v0}, Lcom/google/android/gms/common/internal/zzx;->zzab(Z)V

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzeg$zze;->zzBr:Z

    if-eqz v0, :cond_28

    iget v0, p0, Lcom/google/android/gms/internal/zzeg$zze;->zzBs:I

    if-nez v0, :cond_28

    const-string v0, "No reference is left (including root). Cleaning up engine."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/gms/internal/zzeg$zze$3;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/zzeg$zze$3;-><init>(Lcom/google/android/gms/internal/zzeg$zze;)V

    new-instance v1, Lcom/google/android/gms/internal/zzji$zzb;

    invoke-direct {v1}, Lcom/google/android/gms/internal/zzji$zzb;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/zzeg$zze;->zza(Lcom/google/android/gms/internal/zzji$zzc;Lcom/google/android/gms/internal/zzji$zza;)V

    goto :goto_2d

    :cond_28
    const-string v0, "There are still references to the engine. Not destroying."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V
    :try_end_2d
    .catchall {:try_start_3 .. :try_end_2d} :catchall_2f

    :goto_2d
    monitor-exit v2

    goto :goto_32

    :catchall_2f
    move-exception v3

    monitor-exit v2

    throw v3

    :goto_32
    return-void
.end method

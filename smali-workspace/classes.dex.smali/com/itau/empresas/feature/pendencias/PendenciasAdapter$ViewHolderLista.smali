.class Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderLista;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "PendenciasAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewHolderLista"
.end annotation


# instance fields
.field nomePendencia:Landroid/widget/TextView;

.field parentView:Landroid/view/View;

.field quantidadePendencia:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .registers 3
    .param p1, "itemView"    # Landroid/view/View;

    .line 154
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 155
    iput-object p1, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderLista;->parentView:Landroid/view/View;

    .line 156
    const v0, 0x7f0e0558

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderLista;->nomePendencia:Landroid/widget/TextView;

    .line 157
    const v0, 0x7f0e0559

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderLista;->quantidadePendencia:Landroid/widget/TextView;

    .line 159
    return-void
.end method

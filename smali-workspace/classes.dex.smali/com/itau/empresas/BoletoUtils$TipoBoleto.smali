.class public final enum Lcom/itau/empresas/BoletoUtils$TipoBoleto;
.super Ljava/lang/Enum;
.source "BoletoUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/BoletoUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TipoBoleto"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/itau/empresas/BoletoUtils$TipoBoleto;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/itau/empresas/BoletoUtils$TipoBoleto;

.field public static final enum CONCESSIONARIA:Lcom/itau/empresas/BoletoUtils$TipoBoleto;

.field public static final enum NORMAL:Lcom/itau/empresas/BoletoUtils$TipoBoleto;


# instance fields
.field private final pattern:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 26
    new-instance v0, Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    const-string v1, "NORMAL"

    const-string v2, "(\\d{5})[.](\\d{5})\\s(\\d{5})[.](\\d{6})\\s(\\d{5})[.](\\d{6})\\s(\\d)\\s(\\d{14})"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/itau/empresas/BoletoUtils$TipoBoleto;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->NORMAL:Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    .line 27
    new-instance v0, Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    const-string v1, "CONCESSIONARIA"

    const-string v2, "(\\d{12})\\s(\\d{12})\\s(\\d{12})\\s(\\d{12})"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3, v2}, Lcom/itau/empresas/BoletoUtils$TipoBoleto;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->CONCESSIONARIA:Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    .line 25
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    sget-object v1, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->NORMAL:Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->CONCESSIONARIA:Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->$VALUES:[Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .registers 5
    .param p3, "pattern"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32
    invoke-static {p3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->pattern:Ljava/util/regex/Pattern;

    .line 33
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/itau/empresas/BoletoUtils$TipoBoleto;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 25
    const-class v0, Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    return-object v0
.end method

.method public static values()[Lcom/itau/empresas/BoletoUtils$TipoBoleto;
    .registers 1

    .line 25
    sget-object v0, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->$VALUES:[Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    invoke-virtual {v0}, [Lcom/itau/empresas/BoletoUtils$TipoBoleto;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/itau/empresas/BoletoUtils$TipoBoleto;

    return-object v0
.end method


# virtual methods
.method public extraiBloco(Ljava/lang/String;I)Ljava/lang/String;
    .registers 5
    .param p1, "codigoBarra"    # Ljava/lang/String;
    .param p2, "i"    # I

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->pattern:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 45
    .local v1, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 46
    invoke-virtual {v1, p2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 48
    :cond_11
    const-string v0, ""

    return-object v0
.end method

.method public getPattern()Ljava/util/regex/Pattern;
    .registers 2

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->pattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public isTipo(Ljava/lang/String;)Z
    .registers 3
    .param p1, "codigoBarra"    # Ljava/lang/String;

    .line 40
    iget-object v0, p0, Lcom/itau/empresas/BoletoUtils$TipoBoleto;->pattern:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method

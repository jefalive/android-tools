.class final Lcom/adobe/mobile/RequestHandler;
.super Ljava/lang/Object;
.source "RequestHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adobe/mobile/RequestHandler$HeaderCallback;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static requestConnect(Ljava/lang/String;)Ljava/net/HttpURLConnection;
    .registers 6
    .param p0, "url"    # Ljava/lang/String;

    .line 392
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_b} :catch_c

    return-object v0

    .line 393
    :catch_c
    move-exception v4

    .line 394
    .local v4, "e":Ljava/lang/Exception;
    const-string v0, "Adobe Mobile - Exception opening URL(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 396
    .end local v4    # "e":Ljava/lang/Exception;
    const/4 v0, 0x0

    return-object v0
.end method

.method protected static retrieveAnalyticsRequestData(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;ILjava/lang/String;)[B
    .registers 17
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "postBody"    # Ljava/lang/String;
    .param p2, "headers"    # Ljava/util/Map;
    .param p3, "timeout"    # I
    .param p4, "logPrefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;ILjava/lang/String;)[B"
        }
    .end annotation

    .line 240
    if-nez p0, :cond_4

    .line 241
    const/4 v0, 0x0

    return-object v0

    .line 244
    :cond_4
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->isWearableApp()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 245
    move-object/from16 v0, p4

    invoke-static {p0, p1, p3, v0}, Lcom/adobe/mobile/WearableFunctionBridge;->retrieveAnalyticsRequestData(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)[B

    move-result-object v0

    return-object v0

    .line 248
    :cond_11
    invoke-static {p0}, Lcom/adobe/mobile/RequestHandler;->requestConnect(Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v4

    .line 250
    .local v4, "connection":Ljava/net/HttpURLConnection;
    if-nez v4, :cond_19

    .line 251
    const/4 v0, 0x0

    return-object v0

    .line 256
    :cond_19
    :try_start_19
    invoke-virtual {v4, p3}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 257
    invoke-virtual {v4, p3}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 258
    const-string v0, "POST"

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 259
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getSSL()Z

    move-result v0

    if-nez v0, :cond_35

    .line 260
    const-string v0, "connection"

    const-string v1, "close"

    invoke-virtual {v4, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    :cond_35
    const-string v0, "UTF-8"

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v5

    .line 264
    .local v5, "outputBytes":[B
    array-length v0, v5

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 265
    const-string v0, "Content-Type"

    const-string v1, "application/x-www-form-urlencoded"

    invoke-virtual {v4, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    if-eqz p2, :cond_6d

    .line 269
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_50
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/util/Map$Entry;

    .line 270
    .local v7, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    .end local v7    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v7
    goto :goto_50

    .line 275
    :cond_6d
    new-instance v6, Ljava/io/BufferedOutputStream;

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    invoke-direct {v6, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 276
    .local v6, "postBodyStream":Ljava/io/OutputStream;
    invoke-virtual {v6, v5}, Ljava/io/OutputStream;->write([B)V

    .line 277
    invoke-virtual {v6}, Ljava/io/OutputStream;->close()V

    .line 279
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    .line 280
    .local v7, "inputStream":Ljava/io/InputStream;
    const/16 v0, 0x400

    new-array v8, v0, [B

    .line 281
    .local v8, "buffer":[B
    new-instance v9, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v9}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 283
    .local v9, "baos":Ljava/io/ByteArrayOutputStream;
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getSSL()Z

    move-result v0

    if-nez v0, :cond_9b

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_a8

    .line 286
    :cond_9b
    :goto_9b
    invoke-virtual {v7, v8}, Ljava/io/InputStream;->read([B)I

    move-result v10

    .line 287
    .local v10, "len":I
    const/4 v0, -0x1

    if-ne v10, v0, :cond_a3

    .line 288
    goto :goto_a8

    .line 290
    :cond_a3
    const/4 v0, 0x0

    invoke-virtual {v9, v8, v0, v10}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 291
    .end local v10    # "len":I
    goto :goto_9b

    .line 294
    :cond_a8
    :goto_a8
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    .line 296
    const-string v0, "%s - Request Sent(%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p4, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 299
    invoke-virtual {v9}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_bc
    .catch Ljava/net/SocketTimeoutException; {:try_start_19 .. :try_end_bc} :catch_cb
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_bc} :catch_e9
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_bc} :catch_10b
    .catch Ljava/lang/Error; {:try_start_19 .. :try_end_bc} :catch_12f
    .catchall {:try_start_19 .. :try_end_bc} :catchall_153

    move-result-object v10

    .line 313
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getSSL()Z

    move-result v0

    if-nez v0, :cond_ca

    .line 314
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_ca
    return-object v10

    .line 300
    .end local v5    # "outputBytes":[B
    .end local v6    # "postBodyStream":Ljava/io/OutputStream;
    .end local v7    # "inputStream":Ljava/io/InputStream;
    .end local v8    # "buffer":[B
    .end local v9    # "baos":Ljava/io/ByteArrayOutputStream;
    :catch_cb
    move-exception v5

    .line 301
    .local v5, "e":Ljava/net/SocketTimeoutException;
    const-string v0, "%s - Timed out sending request(%s)"

    const/4 v1, 0x2

    :try_start_cf
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p4, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_da
    .catchall {:try_start_cf .. :try_end_da} :catchall_153

    .line 302
    const/4 v6, 0x0

    .line 313
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getSSL()Z

    move-result v0

    if-nez v0, :cond_e8

    .line 314
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_e8
    return-object v6

    .line 303
    .end local v5    # "e":Ljava/net/SocketTimeoutException;
    :catch_e9
    move-exception v5

    .line 304
    .local v5, "e":Ljava/io/IOException;
    const-string v0, "%s - IOException while sending request, may retry(%s)"

    const/4 v1, 0x2

    :try_start_ed
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p4, v1, v2

    invoke-virtual {v5}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_fc
    .catchall {:try_start_ed .. :try_end_fc} :catchall_153

    .line 305
    const/4 v6, 0x0

    .line 313
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getSSL()Z

    move-result v0

    if-nez v0, :cond_10a

    .line 314
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_10a
    return-object v6

    .line 306
    .end local v5    # "e":Ljava/io/IOException;
    :catch_10b
    move-exception v5

    .line 307
    .local v5, "e":Ljava/lang/Exception;
    const-string v0, "%s - Exception while attempting to send hit, will not retry(%s)"

    const/4 v1, 0x2

    :try_start_10f
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p4, v1, v2

    invoke-virtual {v5}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 308
    const/4 v0, 0x0

    new-array v6, v0, [B
    :try_end_121
    .catchall {:try_start_10f .. :try_end_121} :catchall_153

    .line 313
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getSSL()Z

    move-result v0

    if-nez v0, :cond_12e

    .line 314
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_12e
    return-object v6

    .line 309
    .end local v5    # "e":Ljava/lang/Exception;
    :catch_12f
    move-exception v5

    .line 310
    .local v5, "e":Ljava/lang/Error;
    const-string v0, "%s - Exception while attempting to send hit, will not retry(%s)"

    const/4 v1, 0x2

    :try_start_133
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p4, v1, v2

    invoke-virtual {v5}, Ljava/lang/Error;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 311
    const/4 v0, 0x0

    new-array v6, v0, [B
    :try_end_145
    .catchall {:try_start_133 .. :try_end_145} :catchall_153

    .line 313
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getSSL()Z

    move-result v0

    if-nez v0, :cond_152

    .line 314
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_152
    return-object v6

    .line 313
    .end local v5    # "e":Ljava/lang/Error;
    :catchall_153
    move-exception v11

    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getSSL()Z

    move-result v0

    if-nez v0, :cond_161

    .line 314
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_161
    throw v11
.end method

.method protected static retrieveData(Ljava/lang/String;ILjava/lang/String;Ljava/util/concurrent/Callable;Lcom/adobe/mobile/RequestHandler$HeaderCallback;)[B
    .registers 23
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "readTimeout"    # I
    .param p2, "source"    # Ljava/lang/String;
    .param p3, "requestHeaderProvider"    # Ljava/util/concurrent/Callable;
    .param p4, "responseHeaderReceiver"    # Lcom/adobe/mobile/RequestHandler$HeaderCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;ILjava/lang/String;Ljava/util/concurrent/Callable<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;Lcom/adobe/mobile/RequestHandler$HeaderCallback;)[B"
        }
    .end annotation

    .line 79
    const/4 v4, 0x0

    .line 80
    .local v4, "connection":Ljava/net/HttpURLConnection;
    const/4 v5, 0x0

    .line 84
    .local v5, "inStream":Ljava/io/InputStream;
    move-object/from16 v9, p0

    .line 85
    .local v9, "_url":Ljava/lang/String;
    const/4 v10, 0x0

    .line 86
    .local v10, "responseCode":I
    const/4 v11, 0x0

    .line 91
    .local v11, "redirectCount":I
    :goto_6
    const/16 v0, 0x15

    if-le v11, v0, :cond_21

    .line 92
    const-string v0, "%s - Too many redirects for (%s) - %d"

    const/4 v1, 0x3

    :try_start_d
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    goto/16 :goto_a8

    .line 97
    :cond_21
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, v9}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v4, v0

    .line 98
    const/16 v0, 0x7d0

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 99
    move/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 100
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 103
    const-string v0, "Accept-Language"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getDefaultAcceptLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const-string v0, "User-Agent"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getDefaultUserAgent()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    if-eqz p3, :cond_7d

    .line 108
    invoke-interface/range {p3 .. p3}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Ljava/util/Map;

    .line 110
    .local v12, "requestHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v12, :cond_7d

    .line 111
    invoke-interface {v12}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_60
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7d

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Ljava/util/Map$Entry;

    .line 112
    .local v14, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v14}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v14}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    .end local v14    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v14
    goto :goto_60

    .line 118
    .end local v12    # "requestHeaders":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v12
    :cond_7d
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v10

    .line 121
    if-eqz p4, :cond_8c

    .line 122
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v0

    move-object/from16 v1, p4

    invoke-interface {v1, v0}, Lcom/adobe/mobile/RequestHandler$HeaderCallback;->call(Ljava/util/Map;)V

    .line 126
    :cond_8c
    sparse-switch v10, :sswitch_data_1b0

    goto :goto_a8

    .line 129
    :sswitch_90
    add-int/lit8 v11, v11, 0x1

    .line 131
    const-string v0, "Location"

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 132
    .local v8, "location":Ljava/lang/String;
    new-instance v6, Ljava/net/URL;

    invoke-direct {v6, v9}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 133
    .local v6, "base":Ljava/net/URL;
    new-instance v7, Ljava/net/URL;

    invoke-direct {v7, v6, v8}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V

    .line 134
    .local v7, "next":Ljava/net/URL;
    invoke-virtual {v7}, Ljava/net/URL;->toExternalForm()Ljava/lang/String;

    move-result-object v9

    .line 135
    goto/16 :goto_6

    .line 138
    .line 142
    .end local v6    # "base":Ljava/net/URL;
    .end local v7    # "next":Ljava/net/URL;
    .end local v8    # "location":Ljava/lang/String;
    :goto_a8
    const/16 v0, 0xc8

    if-ne v10, v0, :cond_ed

    .line 143
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    move-object v5, v0

    .line 145
    const/16 v0, 0x400

    new-array v12, v0, [B

    .line 147
    .local v12, "buffer":[B
    new-instance v13, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v13}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 151
    .local v13, "baos":Ljava/io/ByteArrayOutputStream;
    :goto_ba
    invoke-virtual {v5, v12}, Ljava/io/InputStream;->read([B)I

    move-result v14

    .line 152
    .local v14, "len":I
    const/4 v0, -0x1

    if-ne v14, v0, :cond_c2

    .line 153
    goto :goto_c7

    .line 155
    :cond_c2
    const/4 v0, 0x0

    invoke-virtual {v13, v12, v0, v14}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 156
    .end local v14    # "len":I
    goto :goto_ba

    .line 159
    :goto_c7
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 162
    invoke-virtual {v13}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_cd
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_cd} :catch_f4
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_cd} :catch_127
    .catch Ljava/lang/Error; {:try_start_d .. :try_end_cd} :catch_15a
    .catchall {:try_start_d .. :try_end_cd} :catchall_18d

    move-result-object v14

    .line 174
    if-eqz v4, :cond_d3

    .line 175
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 178
    :cond_d3
    if-eqz v5, :cond_ec

    .line 180
    :try_start_d5
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_d8
    .catch Ljava/io/IOException; {:try_start_d5 .. :try_end_d8} :catch_d9

    .line 183
    goto :goto_ec

    .line 181
    :catch_d9
    move-exception v15

    .line 182
    .local v15, "ex":Ljava/io/IOException;
    const-string v0, "%s - Unable to close stream (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {v15}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 183
    .end local v15    # "ex":Ljava/io/IOException;
    :cond_ec
    :goto_ec
    return-object v14

    .line 174
    .end local v9    # "_url":Ljava/lang/String;
    .end local v10    # "responseCode":I
    .end local v11    # "redirectCount":I
    .end local v12    # "buffer":[B
    .end local v13    # "baos":Ljava/io/ByteArrayOutputStream;
    :cond_ed
    if-eqz v4, :cond_f2

    .line 175
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 178
    :cond_f2
    goto/16 :goto_1ad

    .line 164
    .line 180
    .line 181
    .line 182
    .line 183
    .line 183
    .local v6, "ex":Ljava/io/IOException;
    .end local v6    # "ex":Ljava/io/IOException;
    :catch_f4
    move-exception v6

    .line 165
    .local v6, "ex":Ljava/io/IOException;
    const-string v0, "%s - IOException while sending request (%s)"

    const/4 v1, 0x2

    :try_start_f8
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {v6}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_107
    .catchall {:try_start_f8 .. :try_end_107} :catchall_18d

    .line 166
    const/4 v7, 0x0

    .line 174
    if-eqz v4, :cond_10d

    .line 175
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 178
    :cond_10d
    if-eqz v5, :cond_126

    .line 180
    :try_start_10f
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_112
    .catch Ljava/io/IOException; {:try_start_10f .. :try_end_112} :catch_113

    .line 183
    goto :goto_126

    .line 181
    :catch_113
    move-exception v8

    .line 182
    .local v8, "ex":Ljava/io/IOException;
    const-string v0, "%s - Unable to close stream (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {v8}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 183
    .end local v8    # "ex":Ljava/io/IOException;
    :cond_126
    :goto_126
    return-object v7

    .line 167
    .end local v6    # "ex":Ljava/io/IOException;
    :catch_127
    move-exception v6

    .line 168
    .local v6, "ex":Ljava/lang/Exception;
    const-string v0, "%s - Exception while sending request (%s)"

    const/4 v1, 0x2

    :try_start_12b
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {v6}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_13a
    .catchall {:try_start_12b .. :try_end_13a} :catchall_18d

    .line 169
    const/4 v7, 0x0

    .line 174
    if-eqz v4, :cond_140

    .line 175
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 178
    :cond_140
    if-eqz v5, :cond_159

    .line 180
    :try_start_142
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_145
    .catch Ljava/io/IOException; {:try_start_142 .. :try_end_145} :catch_146

    .line 183
    goto :goto_159

    .line 181
    :catch_146
    move-exception v8

    .line 182
    .local v8, "ex":Ljava/io/IOException;
    const-string v0, "%s - Unable to close stream (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {v8}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 183
    .end local v8    # "ex":Ljava/io/IOException;
    :cond_159
    :goto_159
    return-object v7

    .line 170
    .end local v6    # "ex":Ljava/lang/Exception;
    :catch_15a
    move-exception v6

    .line 171
    .local v6, "er":Ljava/lang/Error;
    const-string v0, "%s - Unexpected error while sending request (%s)"

    const/4 v1, 0x2

    :try_start_15e
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {v6}, Ljava/lang/Error;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_16d
    .catchall {:try_start_15e .. :try_end_16d} :catchall_18d

    .line 172
    const/4 v7, 0x0

    .line 174
    if-eqz v4, :cond_173

    .line 175
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 178
    :cond_173
    if-eqz v5, :cond_18c

    .line 180
    :try_start_175
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_178
    .catch Ljava/io/IOException; {:try_start_175 .. :try_end_178} :catch_179

    .line 183
    goto :goto_18c

    .line 181
    :catch_179
    move-exception v8

    .line 182
    .local v8, "ex":Ljava/io/IOException;
    const-string v0, "%s - Unable to close stream (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual {v8}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 183
    .end local v8    # "ex":Ljava/io/IOException;
    :cond_18c
    :goto_18c
    return-object v7

    .line 174
    .end local v6    # "er":Ljava/lang/Error;
    :catchall_18d
    move-exception v16

    if-eqz v4, :cond_193

    .line 175
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 178
    :cond_193
    if-eqz v5, :cond_1ac

    .line 180
    :try_start_195
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_198
    .catch Ljava/io/IOException; {:try_start_195 .. :try_end_198} :catch_199

    .line 183
    goto :goto_1ac

    .line 181
    :catch_199
    move-exception v17

    .line 182
    .local v17, "ex":Ljava/io/IOException;
    const-string v0, "%s - Unable to close stream (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 183
    .end local v17    # "ex":Ljava/io/IOException;
    :cond_1ac
    :goto_1ac
    throw v16

    .line 187
    :goto_1ad
    const/4 v0, 0x0

    return-object v0

    nop

    :sswitch_data_1b0
    .sparse-switch
        0x12d -> :sswitch_90
        0x12e -> :sswitch_90
    .end sparse-switch
.end method

.method protected static retrieveData(Ljava/lang/String;Ljava/util/Map;ILjava/lang/String;)[B
    .registers 7
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headers"    # Ljava/util/Map;
    .param p2, "readTimeout"    # I
    .param p3, "source"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;ILjava/lang/String;)[B"
        }
    .end annotation

    .line 54
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->isWearableApp()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 55
    invoke-static {p0, p2}, Lcom/adobe/mobile/WearableFunctionBridge;->retrieveData(Ljava/lang/String;I)[B

    move-result-object v0

    return-object v0

    .line 58
    :cond_b
    new-instance v2, Lcom/adobe/mobile/RequestHandler$1;

    invoke-direct {v2, p1}, Lcom/adobe/mobile/RequestHandler$1;-><init>(Ljava/util/Map;)V

    .line 64
    .local v2, "headersCallback":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    if-eqz p1, :cond_14

    move-object v0, v2

    goto :goto_15

    :cond_14
    const/4 v0, 0x0

    :goto_15
    const/4 v1, 0x0

    invoke-static {p0, p2, p3, v0, v1}, Lcom/adobe/mobile/RequestHandler;->retrieveData(Ljava/lang/String;ILjava/lang/String;Ljava/util/concurrent/Callable;Lcom/adobe/mobile/RequestHandler$HeaderCallback;)[B

    move-result-object v0

    return-object v0
.end method

.method protected static sendGenericRequest(Ljava/lang/String;Ljava/util/Map;ILjava/lang/String;)V
    .registers 12
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "headers"    # Ljava/util/Map;
    .param p2, "timeout"    # I
    .param p3, "source"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;ILjava/lang/String;)V"
        }
    .end annotation

    .line 192
    if-nez p0, :cond_3

    .line 193
    return-void

    .line 196
    :cond_3
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->isWearableApp()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 197
    invoke-static {p0, p2, p3}, Lcom/adobe/mobile/WearableFunctionBridge;->sendGenericRequest(Ljava/lang/String;ILjava/lang/String;)V

    .line 198
    return-void

    .line 203
    :cond_d
    :try_start_d
    invoke-static {p0}, Lcom/adobe/mobile/RequestHandler;->requestConnect(Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v4

    .line 204
    .local v4, "connection":Ljava/net/HttpURLConnection;
    if-eqz v4, :cond_66

    .line 205
    invoke-virtual {v4, p2}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 206
    invoke-virtual {v4, p2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 208
    if-eqz p1, :cond_4b

    .line 210
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_23
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/util/Map$Entry;

    .line 211
    .local v6, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/lang/String;

    .line 212
    .local v7, "value":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4a

    .line 213
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0, v7}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    .end local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v6
    .end local v7    # "value":Ljava/lang/String;
    :cond_4a
    goto :goto_23

    .line 218
    :cond_4b
    const-string v0, "%s - Request Sent(%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 220
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I

    .line 222
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 223
    .local v5, "stream":Ljava/io/InputStream;
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 224
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_66
    .catch Ljava/net/SocketTimeoutException; {:try_start_d .. :try_end_66} :catch_67
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_66} :catch_77
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_66} :catch_8b
    .catch Ljava/lang/Error; {:try_start_d .. :try_end_66} :catch_9f

    .line 234
    .end local v4    # "connection":Ljava/net/HttpURLConnection;
    .end local v5    # "stream":Ljava/io/InputStream;
    :cond_66
    goto :goto_b2

    .line 226
    :catch_67
    move-exception v4

    .line 227
    .local v4, "e":Ljava/net/SocketTimeoutException;
    const-string v0, "%s - Timed out sending request(%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 234
    .end local v4    # "e":Ljava/net/SocketTimeoutException;
    goto :goto_b2

    .line 228
    :catch_77
    move-exception v4

    .line 229
    .local v4, "e":Ljava/io/IOException;
    const-string v0, "%s - IOException while sending request, may retry(%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    invoke-virtual {v4}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 234
    .end local v4    # "e":Ljava/io/IOException;
    goto :goto_b2

    .line 230
    :catch_8b
    move-exception v4

    .line 231
    .local v4, "e":Ljava/lang/Exception;
    const-string v0, "%s - Exception while attempting to send hit, will not retry(%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    invoke-virtual {v4}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 234
    .end local v4    # "e":Ljava/lang/Exception;
    goto :goto_b2

    .line 232
    :catch_9f
    move-exception v4

    .line 233
    .local v4, "e":Ljava/lang/Error;
    const-string v0, "%s - Exception while attempting to send hit, will not retry(%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    invoke-virtual {v4}, Ljava/lang/Error;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 235
    .end local v4    # "e":Ljava/lang/Error;
    :goto_b2
    return-void
.end method

.method protected static sendThirdPartyRequest(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;ILjava/lang/String;Ljava/lang/String;)Z
    .registers 15
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "postBody"    # Ljava/lang/String;
    .param p2, "headers"    # Ljava/util/Map;
    .param p3, "timeout"    # I
    .param p4, "postType"    # Ljava/lang/String;
    .param p5, "logPrefix"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;ILjava/lang/String;Ljava/lang/String;)Z"
        }
    .end annotation

    .line 321
    if-nez p0, :cond_4

    .line 322
    const/4 v0, 0x0

    return v0

    .line 325
    :cond_4
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->isWearableApp()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 326
    invoke-static {p0, p1, p3, p4, p5}, Lcom/adobe/mobile/WearableFunctionBridge;->sendThirdPartyRequest(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 329
    :cond_f
    invoke-static {p0}, Lcom/adobe/mobile/RequestHandler;->requestConnect(Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v4

    .line 331
    .local v4, "connection":Ljava/net/HttpURLConnection;
    if-nez v4, :cond_17

    .line 332
    const/4 v0, 0x0

    return v0

    .line 337
    :cond_17
    :try_start_17
    invoke-virtual {v4, p3}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 338
    invoke-virtual {v4, p3}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 339
    const-string v0, "GET"

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 342
    if-eqz p2, :cond_49

    .line 343
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2c
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_49

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/util/Map$Entry;

    .line 344
    .local v6, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    .end local v6    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v6
    goto :goto_2c

    .line 349
    :cond_49
    if-eqz p1, :cond_80

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_80

    .line 350
    const-string v0, "POST"

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 352
    if-eqz p4, :cond_60

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_60

    move-object v5, p4

    goto :goto_62

    :cond_60
    const-string v5, "application/x-www-form-urlencoded"

    .line 354
    .local v5, "contentType":Ljava/lang/String;
    :goto_62
    const-string v0, "UTF-8"

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    .line 355
    .local v6, "outputBytes":[B
    array-length v0, v6

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 356
    const-string v0, "Content-Type"

    invoke-virtual {v4, v0, v5}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    .line 360
    .local v7, "connectionOutputStream":Ljava/io/OutputStream;
    new-instance v8, Ljava/io/BufferedOutputStream;

    invoke-direct {v8, v7}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 361
    .local v8, "postBodyStream":Ljava/io/OutputStream;
    invoke-virtual {v8, v6}, Ljava/io/OutputStream;->write([B)V

    .line 362
    invoke-virtual {v8}, Ljava/io/OutputStream;->close()V

    .line 366
    .end local v5    # "contentType":Ljava/lang/String;
    .end local v6    # "outputBytes":[B
    .end local v7    # "connectionOutputStream":Ljava/io/OutputStream;
    .end local v8    # "postBodyStream":Ljava/io/OutputStream;
    :cond_80
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 367
    .local v5, "inputStream":Ljava/io/InputStream;
    const/16 v0, 0xa

    new-array v6, v0, [B

    .line 368
    .local v6, "b":[B
    :goto_88
    invoke-virtual {v5, v6}, Ljava/io/InputStream;->read([B)I

    move-result v0

    if-lez v0, :cond_8f

    goto :goto_88

    .line 369
    :cond_8f
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 371
    const-string v0, "%s - Successfully forwarded hit (%s body: %s type: %s)"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p5, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const/4 v2, 0x2

    aput-object p1, v1, v2

    const/4 v2, 0x3

    aput-object p4, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_a6
    .catch Ljava/net/SocketTimeoutException; {:try_start_17 .. :try_end_a6} :catch_a7
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_a6} :catch_b8
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_a6} :catch_cc
    .catch Ljava/lang/Error; {:try_start_17 .. :try_end_a6} :catch_e0

    .line 385
    .end local v5    # "inputStream":Ljava/io/InputStream;
    .end local v6    # "b":[B
    goto :goto_f3

    .line 373
    :catch_a7
    move-exception v5

    .line 374
    .local v5, "e":Ljava/net/SocketTimeoutException;
    const-string v0, "%s - Timed out sending request (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p5, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 375
    const/4 v0, 0x0

    return v0

    .line 377
    .end local v5    # "e":Ljava/net/SocketTimeoutException;
    :catch_b8
    move-exception v5

    .line 378
    .local v5, "e":Ljava/io/IOException;
    const-string v0, "%s - IOException while sending request, will not retry (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p5, v1, v2

    invoke-virtual {v5}, Ljava/io/IOException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 385
    .end local v5    # "e":Ljava/io/IOException;
    goto :goto_f3

    .line 380
    :catch_cc
    move-exception v5

    .line 381
    .local v5, "e":Ljava/lang/Exception;
    const-string v0, "%s - Exception while attempting to send hit, will not retry (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p5, v1, v2

    invoke-virtual {v5}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 385
    .end local v5    # "e":Ljava/lang/Exception;
    goto :goto_f3

    .line 383
    :catch_e0
    move-exception v5

    .line 384
    .local v5, "e":Ljava/lang/Error;
    const-string v0, "%s - Exception while attempting to send hit, will not retry (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p5, v1, v2

    invoke-virtual {v5}, Ljava/lang/Error;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 387
    .end local v5    # "e":Ljava/lang/Error;
    :goto_f3
    const/4 v0, 0x1

    return v0
.end method

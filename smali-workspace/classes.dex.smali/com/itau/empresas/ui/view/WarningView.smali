.class public Lcom/itau/empresas/ui/view/WarningView;
.super Landroid/widget/LinearLayout;
.source "WarningView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/view/WarningView$Estado;
    }
.end annotation


# instance fields
.field protected corCinza:I

.field protected corVerde:I

.field protected imgWarning:Landroid/widget/ImageView;

.field protected llViewWarning:Landroid/widget/LinearLayout;

.field protected textoWarning:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 32
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 40
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    return-void
.end method


# virtual methods
.method public setBackgroundColor(I)V
    .registers 3
    .param p1, "color"    # I

    .line 70
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WarningView;->llViewWarning:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 71
    return-void
.end method

.method public setMensagem(Lcom/itau/empresas/ui/view/WarningView$Estado;I)V
    .registers 4
    .param p1, "estado"    # Lcom/itau/empresas/ui/view/WarningView$Estado;
    .param p2, "resMensagem"    # I

    .line 49
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/WarningView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/itau/empresas/ui/view/WarningView;->setMensagem(Lcom/itau/empresas/ui/view/WarningView$Estado;Ljava/lang/String;)V

    .line 50
    return-void
.end method

.method public setMensagem(Lcom/itau/empresas/ui/view/WarningView$Estado;Ljava/lang/String;)V
    .registers 5
    .param p1, "estado"    # Lcom/itau/empresas/ui/view/WarningView$Estado;
    .param p2, "mensagem"    # Ljava/lang/String;

    .line 53
    sget-object v0, Lcom/itau/empresas/ui/view/WarningView$1;->$SwitchMap$com$itau$empresas$ui$view$WarningView$Estado:[I

    invoke-virtual {p1}, Lcom/itau/empresas/ui/view/WarningView$Estado;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_40

    goto :goto_3a

    .line 55
    :pswitch_c
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WarningView;->imgWarning:Landroid/widget/ImageView;

    const v1, 0x7f02011c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WarningView;->llViewWarning:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/itau/empresas/ui/view/WarningView;->corVerde:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 57
    goto :goto_3a

    .line 59
    :pswitch_1c
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WarningView;->imgWarning:Landroid/widget/ImageView;

    const v1, 0x7f020107

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 60
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WarningView;->llViewWarning:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/itau/empresas/ui/view/WarningView;->corCinza:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 62
    :pswitch_2b
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WarningView;->imgWarning:Landroid/widget/ImageView;

    const v1, 0x7f020129

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WarningView;->llViewWarning:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/itau/empresas/ui/view/WarningView;->corCinza:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 66
    :goto_3a
    iget-object v0, p0, Lcom/itau/empresas/ui/view/WarningView;->textoWarning:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    return-void

    :pswitch_data_40
    .packed-switch 0x1
        :pswitch_c
        :pswitch_1c
        :pswitch_2b
    .end packed-switch
.end method

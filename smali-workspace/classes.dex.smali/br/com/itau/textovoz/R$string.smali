.class public final Lbr/com/itau/textovoz/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f070000

.field public static final abc_action_bar_home_description_format:I = 0x7f070001

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f070002

.field public static final abc_action_bar_up_description:I = 0x7f070003

.field public static final abc_action_menu_overflow_description:I = 0x7f070004

.field public static final abc_action_mode_done:I = 0x7f070005

.field public static final abc_activity_chooser_view_see_all:I = 0x7f070006

.field public static final abc_activitychooserview_choose_application:I = 0x7f070007

.field public static final abc_capital_off:I = 0x7f070008

.field public static final abc_capital_on:I = 0x7f070009

.field public static final abc_font_family_body_1_material:I = 0x7f07006f

.field public static final abc_font_family_body_2_material:I = 0x7f070070

.field public static final abc_font_family_button_material:I = 0x7f070071

.field public static final abc_font_family_caption_material:I = 0x7f070072

.field public static final abc_font_family_display_1_material:I = 0x7f070073

.field public static final abc_font_family_display_2_material:I = 0x7f070074

.field public static final abc_font_family_display_3_material:I = 0x7f070075

.field public static final abc_font_family_display_4_material:I = 0x7f070076

.field public static final abc_font_family_headline_material:I = 0x7f070077

.field public static final abc_font_family_menu_material:I = 0x7f070078

.field public static final abc_font_family_subhead_material:I = 0x7f070079

.field public static final abc_font_family_title_material:I = 0x7f07007a

.field public static final abc_search_hint:I = 0x7f07000a

.field public static final abc_searchview_description_clear:I = 0x7f07000b

.field public static final abc_searchview_description_query:I = 0x7f07000c

.field public static final abc_searchview_description_search:I = 0x7f07000d

.field public static final abc_searchview_description_submit:I = 0x7f07000e

.field public static final abc_searchview_description_voice:I = 0x7f07000f

.field public static final abc_shareactionprovider_share_with:I = 0x7f070010

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f070011

.field public static final abc_toolbar_collapse_description:I = 0x7f070012

.field public static final accountDAC_extra:I = 0x7f07007e

.field public static final account_extra:I = 0x7f07007f

.field public static final agency_extra:I = 0x7f070106

.field public static final android:I = 0x7f07010f

.field public static final answer_bundle_item:I = 0x7f070110

.field public static final app_name:I = 0x7f07011b

.field public static final appbar_scrolling_view_behavior:I = 0x7f07011c

.field public static final application_action_open:I = 0x7f07011d

.field public static final attendance:I = 0x7f070124

.field public static final attendance_filter:I = 0x7f070125

.field public static final attendance_intent:I = 0x7f070126

.field public static final available_balance:I = 0x7f070130

.field public static final balance:I = 0x7f070133

.field public static final bottom_sheet_behavior:I = 0x7f07015b

.field public static final call_for_us:I = 0x7f070160

.field public static final channel_bundle_item:I = 0x7f070182

.field public static final character_counter_pattern:I = 0x7f070183

.field public static final close:I = 0x7f070186

.field public static final close_message:I = 0x7f070187

.field public static final content_description_back:I = 0x7f0701af

.field public static final content_description_card:I = 0x7f0701b0

.field public static final content_description_close:I = 0x7f0701b1

.field public static final content_description_close_voice:I = 0x7f0701b2

.field public static final content_description_error:I = 0x7f0701b4

.field public static final content_description_voice:I = 0x7f0701b5

.field public static final content_description_voice_button:I = 0x7f0701b6

.field public static final context_extra:I = 0x7f0701b7

.field public static final currency:I = 0x7f0701cb

.field public static final description_card_feedback:I = 0x7f0701db

.field public static final description_card_item_not_found:I = 0x7f0701dc

.field public static final description_voice_button_image:I = 0x7f0701dd

.field public static final error_audio:I = 0x7f0701f5

.field public static final error_client:I = 0x7f0701f6

.field public static final error_insufficient_permissions:I = 0x7f0701f7

.field public static final error_network:I = 0x7f0701f8

.field public static final error_network_timeout:I = 0x7f0701f9

.field public static final error_no_match:I = 0x7f0701fa

.field public static final error_permission_deneid:I = 0x7f0701fb

.field public static final error_recognizer_busy:I = 0x7f0701fc

.field public static final error_server:I = 0x7f0701fd

.field public static final error_speech_timeout:I = 0x7f0701fe

.field public static final error_system_hello:I = 0x7f0701ff

.field public static final error_system_not:I = 0x7f070200

.field public static final error_system_search:I = 0x7f070201

.field public static final error_system_search_voice:I = 0x7f070202

.field public static final error_system_try:I = 0x7f070203

.field public static final error_system_unavailable:I = 0x7f070204

.field public static final expand_card_feedback:I = 0x7f070207

.field public static final expand_card_feedback_show:I = 0x7f070208

.field public static final extract_intent:I = 0x7f070209

.field public static final faq_bundle_item:I = 0x7f070210

.field public static final faq_intent:I = 0x7f070211

.field public static final feed_back:I = 0x7f070214

.field public static final feed_not:I = 0x7f070215

.field public static final feed_yes:I = 0x7f070216

.field public static final feedback_no:I = 0x7f07021a

.field public static final feedback_respost:I = 0x7f07021c

.field public static final feedback_title:I = 0x7f07021d

.field public static final feedback_yes:I = 0x7f07021e

.field public static final flag_attendance_extra:I = 0x7f070239

.field public static final flag_card_extra:I = 0x7f07023a

.field public static final flag_faq_extra:I = 0x7f07023b

.field public static final flag_menu_extra:I = 0x7f07023c

.field public static final flag_overflow_message_extra:I = 0x7f07023d

.field public static final flag_receipts_extra:I = 0x7f07023e

.field public static final flag_trans_extra:I = 0x7f07023f

.field public static final flag_voice_extra:I = 0x7f070240

.field public static final font_arial:I = 0x7f070241

.field public static final font_arial_bold:I = 0x7f070242

.field public static final font_name_ttf_itau:I = 0x7f070246

.field public static final font_name_ttf_itau_name:I = 0x7f070247

.field public static final font_not_found:I = 0x7f070248

.field public static final font_roboto_bold:I = 0x7f070249

.field public static final font_roboto_regular:I = 0x7f07024a

.field public static final fonts:I = 0x7f07028e

.field public static final ga_acao_atendimento:I = 0x7f070298

.field public static final ga_acao_card:I = 0x7f07029b

.field public static final ga_acao_card_saldo:I = 0x7f07029d

.field public static final ga_acao_card_saldo_extrato:I = 0x7f07029e

.field public static final ga_acao_enter:I = 0x7f0702a7

.field public static final ga_acao_faq:I = 0x7f0702ad

.field public static final ga_acao_parou_digitar:I = 0x7f0702ba

.field public static final ga_acao_rota:I = 0x7f0702c0

.field public static final ga_acao_transacao:I = 0x7f0702c9

.field public static final ga_acao_transbordo:I = 0x7f0702ca

.field public static final ga_categoria_busca:I = 0x7f0702d9

.field public static final ga_csi_feedback:I = 0x7f0702ed

.field public static final ga_faq:I = 0x7f0702f3

.field public static final ga_feedback_no:I = 0x7f0702f4

.field public static final ga_feedback_yes:I = 0x7f0702f5

.field public static final ga_termo_buscado:I = 0x7f070338

.field public static final help_improve:I = 0x7f070348

.field public static final help_search:I = 0x7f070349

.field public static final help_voice_balance:I = 0x7f07034a

.field public static final help_voice_difference:I = 0x7f07034b

.field public static final help_voice_limit:I = 0x7f07034c

.field public static final help_voice_suggestions:I = 0x7f07034d

.field public static final help_voice_transfer:I = 0x7f07034e

.field public static final ic_arrow:I = 0x7f070365

.field public static final ic_attendance:I = 0x7f070366

.field public static final ic_chat:I = 0x7f070367

.field public static final ic_close:I = 0x7f070368

.field public static final ic_glass:I = 0x7f070369

.field public static final ic_microphone:I = 0x7f07036a

.field public static final ic_overflow_message:I = 0x7f07036b

.field public static final itausdkcore_first_token:I = 0x7f070395

.field public static final itausdkcore_message_sms_time:I = 0x7f070397

.field public static final itausdkcore_second_token:I = 0x7f070398

.field public static final itausdkcore_token_number:I = 0x7f07039c

.field public static final itausdkcore_unlock_warning_app:I = 0x7f07039d

.field public static final itausdkcore_unlock_warning_chaveiro:I = 0x7f07039e

.field public static final link_result:I = 0x7f0703fe

.field public static final links_bundle_item:I = 0x7f0703ff

.field public static final menu_extra:I = 0x7f070474

.field public static final menu_intent:I = 0x7f070475

.field public static final menu_map_search:I = 0x7f070476

.field public static final more_items_receipt:I = 0x7f070477

.field public static final need_help_title:I = 0x7f07047a

.field public static final need_help_title_chat:I = 0x7f07047b

.field public static final need_help_title_csi:I = 0x7f07047c

.field public static final now_not:I = 0x7f070482

.field public static final operator_extra:I = 0x7f07048d

.field public static final other_reason:I = 0x7f07048e

.field public static final overflow_view_text:I = 0x7f07048f

.field public static final product_bundle_item:I = 0x7f0704b6

.field public static final progressbar_description_wait:I = 0x7f0704b7

.field public static final query_common:I = 0x7f0704b9

.field public static final receipts_fragment_header:I = 0x7f0704bd

.field public static final reply_confused:I = 0x7f0704c0

.field public static final reply_no_match:I = 0x7f0704c1

.field public static final search_check:I = 0x7f0704c5

.field public static final search_empty_glass:I = 0x7f0704c6

.field public static final search_empty_quick:I = 0x7f0704c7

.field public static final search_menu_title:I = 0x7f07004d

.field public static final search_text_voice:I = 0x7f0704c8

.field public static final see_card_extract:I = 0x7f0704c9

.field public static final send_card_feedback:I = 0x7f0704ca

.field public static final signal:I = 0x7f0704d1

.field public static final status_bar_notification_info_overflow:I = 0x7f07004e

.field public static final term_default:I = 0x7f0704e4

.field public static final text_attendance:I = 0x7f0704e5

.field public static final text_relational_permission:I = 0x7f0704e8

.field public static final thank_you:I = 0x7f070692

.field public static final this_information_good:I = 0x7f070693

.field public static final title_card_feedback:I = 0x7f070694

.field public static final title_card_simple_balance:I = 0x7f070695

.field public static final type_extra:I = 0x7f0706ca

.field public static final view_item_search_freq:I = 0x7f0706d5

.field public static final view_item_search_suggested:I = 0x7f0706d6

.field public static final voice_search_error_max_chars:I = 0x7f0706d7

.field public static final voice_search_try:I = 0x7f0706d8

.field public static final voice_search_voice:I = 0x7f0706d9


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 1460
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

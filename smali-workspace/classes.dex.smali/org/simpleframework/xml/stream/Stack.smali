.class Lorg/simpleframework/xml/stream/Stack;
.super Ljava/util/ArrayList;
.source "Stack.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:Ljava/lang/Object;>Ljava/util/ArrayList<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(I)V
    .registers 2
    .param p1, "size"    # I

    .line 44
    invoke-direct {p0, p1}, Ljava/util/ArrayList;-><init>(I)V

    .line 45
    return-void
.end method


# virtual methods
.method public bottom()Ljava/lang/Object;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 87
    invoke-virtual {p0}, Lorg/simpleframework/xml/stream/Stack;->size()I

    move-result v1

    .line 89
    .local v1, "size":I
    if-gtz v1, :cond_8

    .line 90
    const/4 v0, 0x0

    return-object v0

    .line 92
    :cond_8
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/simpleframework/xml/stream/Stack;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public pop()Ljava/lang/Object;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 55
    invoke-virtual {p0}, Lorg/simpleframework/xml/stream/Stack;->size()I

    move-result v1

    .line 57
    .local v1, "size":I
    if-gtz v1, :cond_8

    .line 58
    const/4 v0, 0x0

    return-object v0

    .line 60
    :cond_8
    add-int/lit8 v0, v1, -0x1

    invoke-virtual {p0, v0}, Lorg/simpleframework/xml/stream/Stack;->remove(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public push(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3
    .param p1, "value"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TT;"
        }
    .end annotation

    .line 105
    invoke-virtual {p0, p1}, Lorg/simpleframework/xml/stream/Stack;->add(Ljava/lang/Object;)Z

    .line 106
    return-object p1
.end method

.method public top()Ljava/lang/Object;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 71
    invoke-virtual {p0}, Lorg/simpleframework/xml/stream/Stack;->size()I

    move-result v1

    .line 73
    .local v1, "size":I
    if-gtz v1, :cond_8

    .line 74
    const/4 v0, 0x0

    return-object v0

    .line 76
    :cond_8
    add-int/lit8 v0, v1, -0x1

    invoke-virtual {p0, v0}, Lorg/simpleframework/xml/stream/Stack;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

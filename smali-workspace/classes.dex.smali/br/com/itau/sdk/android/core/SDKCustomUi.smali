.class public interface abstract Lbr/com/itau/sdk/android/core/SDKCustomUi;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract dimisssDialog()V
.end method

.method public abstract getEletronicPasswordView()Lbr/com/itau/sdk/android/core/login/EletronicPassword;
.end method

.method public abstract getThemeManager()Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;
.end method

.method public abstract initPasswordFlow(Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$OnTokenAction;Ljava/lang/Integer;)V
.end method

.method public abstract initTokenFlow(Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$OnTokenAction;Lbr/com/itau/sdk/android/core/model/UiModelDTO;Ljava/lang/String;)V
.end method

.method public abstract onError(Ljava/lang/String;)V
.end method

.method public abstract showVersionDialog(Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/util/concurrent/CountDownLatch;)V
.end method

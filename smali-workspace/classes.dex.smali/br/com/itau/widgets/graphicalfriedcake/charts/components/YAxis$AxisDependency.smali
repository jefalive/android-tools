.class public final enum Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;
.super Ljava/lang/Enum;
.source "YAxis.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AxisDependency"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

.field public static final enum AxisDependency:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

.field public static final enum LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

.field public static final enum RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 90
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    const-string v1, "LEFT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    const-string v1, "AxisDependency"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->AxisDependency:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    const-string v1, "RIGHT"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    .line 89
    const/4 v0, 0x3

    new-array v0, v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->AxisDependency:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->$VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 89
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 89
    const-class v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;
    .registers 1

    .line 89
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->$VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    invoke-virtual {v0}, [Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    return-object v0
.end method

.class public abstract Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineScatterCandleRadarDataSet;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleDataSet;
.source "LineScatterCandleRadarDataSet.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleDataSet<TT;>;"
    }
.end annotation


# instance fields
.field protected mDrawHorizontalHighlightIndicator:Z

.field protected mDrawVerticalHighlightIndicator:Z

.field protected mHighlightLineWidth:F


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;)V
    .registers 4
    .param p1, "yVals"    # Ljava/util/List;
    .param p2, "label"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<TT;>;Ljava/lang/String;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 15
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineScatterCandleRadarDataSet;->mDrawVerticalHighlightIndicator:Z

    .line 16
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineScatterCandleRadarDataSet;->mDrawHorizontalHighlightIndicator:Z

    .line 20
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineScatterCandleRadarDataSet;->mHighlightLineWidth:F

    .line 25
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineScatterCandleRadarDataSet;->mHighlightLineWidth:F

    .line 26
    return-void
.end method


# virtual methods
.method public getHighlightLineWidth()F
    .registers 2

    .line 77
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineScatterCandleRadarDataSet;->mHighlightLineWidth:F

    return v0
.end method

.method public isHorizontalHighlightIndicatorEnabled()Z
    .registers 2

    .line 63
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineScatterCandleRadarDataSet;->mDrawHorizontalHighlightIndicator:Z

    return v0
.end method

.method public isVerticalHighlightIndicatorEnabled()Z
    .registers 2

    .line 58
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineScatterCandleRadarDataSet;->mDrawVerticalHighlightIndicator:Z

    return v0
.end method

.method public setDrawHighlightIndicators(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 52
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineScatterCandleRadarDataSet;->setDrawVerticalHighlightIndicator(Z)V

    .line 53
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineScatterCandleRadarDataSet;->setDrawHorizontalHighlightIndicator(Z)V

    .line 54
    return-void
.end method

.method public setDrawHorizontalHighlightIndicator(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 34
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineScatterCandleRadarDataSet;->mDrawHorizontalHighlightIndicator:Z

    .line 35
    return-void
.end method

.method public setDrawVerticalHighlightIndicator(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 43
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineScatterCandleRadarDataSet;->mDrawVerticalHighlightIndicator:Z

    .line 44
    return-void
.end method

.method public setHighlightLineWidth(F)V
    .registers 3
    .param p1, "width"    # F

    .line 72
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/LineScatterCandleRadarDataSet;->mHighlightLineWidth:F

    .line 73
    return-void
.end method

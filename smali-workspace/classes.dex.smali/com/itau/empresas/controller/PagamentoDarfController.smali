.class public Lcom/itau/empresas/controller/PagamentoDarfController;
.super Lcom/itau/empresas/controller/BaseController;
.source "PagamentoDarfController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/itau/empresas/controller/BaseController<Lcom/itau/empresas/api/Api;>;"
    }
.end annotation


# instance fields
.field app:Lcom/itau/empresas/CustomApplication;

.field validaEditText:Lcom/itau/empresas/ui/util/validacao/ValidaEditText;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 32
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/controller/PagamentoDarfController;Lcom/google/gson/JsonObject;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/controller/PagamentoDarfController;
    .param p1, "x1"    # Lcom/google/gson/JsonObject;

    .line 32
    invoke-direct {p0, p1}, Lcom/itau/empresas/controller/PagamentoDarfController;->parseJson(Lcom/google/gson/JsonObject;)V

    return-void
.end method

.method private parseJson(Lcom/google/gson/JsonObject;)V
    .registers 4
    .param p1, "jsonStr"    # Lcom/google/gson/JsonObject;

    .line 104
    const-string v0, "duplicado"

    invoke-virtual {p1, v0}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    const-string v0, "duplicado"

    invoke-virtual {p1, v0}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/JsonElement;->getAsBoolean()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 105
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    new-instance v1, Lcom/itau/empresas/controller/PagamentoDarfController$5;

    invoke-direct {v1, p0}, Lcom/itau/empresas/controller/PagamentoDarfController$5;-><init>(Lcom/itau/empresas/controller/PagamentoDarfController;)V

    .line 106
    invoke-virtual {v1}, Lcom/itau/empresas/controller/PagamentoDarfController$5;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 105
    invoke-virtual {v0, p1, v1}, Lcom/google/gson/Gson;->fromJson(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/PagamentoDarfController;->post(Ljava/lang/Object;)V

    goto :goto_3f

    .line 108
    :cond_2a
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    new-instance v1, Lcom/itau/empresas/controller/PagamentoDarfController$6;

    invoke-direct {v1, p0}, Lcom/itau/empresas/controller/PagamentoDarfController$6;-><init>(Lcom/itau/empresas/controller/PagamentoDarfController;)V

    .line 109
    invoke-virtual {v1}, Lcom/itau/empresas/controller/PagamentoDarfController$6;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 108
    invoke-virtual {v0, p1, v1}, Lcom/google/gson/Gson;->fromJson(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/PagamentoDarfController;->post(Ljava/lang/Object;)V

    .line 111
    :goto_3f
    return-void
.end method


# virtual methods
.method public autorizaDarf()V
    .registers 2

    .line 114
    new-instance v0, Lcom/itau/empresas/controller/PagamentoDarfController$7;

    invoke-direct {v0, p0}, Lcom/itau/empresas/controller/PagamentoDarfController$7;-><init>(Lcom/itau/empresas/controller/PagamentoDarfController;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 120
    return-void
.end method

.method public consultaAlcadas(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "conta"    # Ljava/lang/String;
    .param p2, "operador"    # Ljava/lang/String;

    .line 145
    new-instance v0, Lcom/itau/empresas/controller/PagamentoDarfController$10;

    invoke-direct {v0, p0, p2, p1}, Lcom/itau/empresas/controller/PagamentoDarfController$10;-><init>(Lcom/itau/empresas/controller/PagamentoDarfController;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 151
    return-void
.end method

.method public consultaCnpjPagamentos()V
    .registers 2

    .line 65
    new-instance v0, Lcom/itau/empresas/controller/PagamentoDarfController$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/controller/PagamentoDarfController$2;-><init>(Lcom/itau/empresas/controller/PagamentoDarfController;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 74
    return-void
.end method

.method public consultaDadosPerfilLogado()Lcom/itau/empresas/api/model/PerfilOperadorVO;
    .registers 4

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoDarfController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getPerfilLogado()Lcom/itau/empresas/api/model/PerfilOperadorVO;

    move-result-object v1

    .line 57
    .local v1, "perfilOperadorVO":Lcom/itau/empresas/api/model/PerfilOperadorVO;
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoDarfController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v2

    .line 58
    .local v2, "dadosOperador":Lcom/itau/empresas/api/model/DadosOperadorVO;
    if-eqz v2, :cond_15

    .line 59
    invoke-virtual {v2}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getNomeEmpresa()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->setNomeEmpresa(Ljava/lang/String;)V

    .line 61
    :cond_15
    return-object v1
.end method

.method public consultaDetalhesPagamento(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 13
    .param p1, "codigoOperador"    # Ljava/lang/String;
    .param p2, "identificacaoFuncionalidade"    # Ljava/lang/String;
    .param p3, "numeroCarrinho"    # Ljava/lang/String;
    .param p4, "numeroLote"    # Ljava/lang/String;
    .param p5, "identificacaoPagamento"    # Ljava/lang/String;

    .line 134
    new-instance v0, Lcom/itau/empresas/controller/PagamentoDarfController$9;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/itau/empresas/controller/PagamentoDarfController$9;-><init>(Lcom/itau/empresas/controller/PagamentoDarfController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 142
    return-void
.end method

.method public consultaLimitesHorario(Ljava/lang/String;)V
    .registers 3
    .param p1, "tipoOperacaoSispag"    # Ljava/lang/String;

    .line 47
    new-instance v0, Lcom/itau/empresas/controller/PagamentoDarfController$1;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/controller/PagamentoDarfController$1;-><init>(Lcom/itau/empresas/controller/PagamentoDarfController;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 53
    return-void
.end method

.method public dataSelecionadaEhMenorQueAtual(Ljava/lang/String;)Z
    .registers 3
    .param p1, "dataSelecionada"    # Ljava/lang/String;

    .line 155
    invoke-static {p1}, Lcom/itau/empresas/ui/util/validacao/ValidaData;->dataSelecionadaEhMenorQueAtual(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public incluiComDuplicidade()V
    .registers 2

    .line 123
    new-instance v0, Lcom/itau/empresas/controller/PagamentoDarfController$8;

    invoke-direct {v0, p0}, Lcom/itau/empresas/controller/PagamentoDarfController$8;-><init>(Lcom/itau/empresas/controller/PagamentoDarfController;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 129
    return-void
.end method

.method public incluiDarf(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;)V
    .registers 3
    .param p1, "darfInclusaoRequestVO"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 83
    new-instance v0, Lcom/itau/empresas/controller/PagamentoDarfController$3;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/controller/PagamentoDarfController$3;-><init>(Lcom/itau/empresas/controller/PagamentoDarfController;Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 91
    return-void
.end method

.method public simularAutorizacaoDarf(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;)V
    .registers 3
    .param p1, "darfInclusaoRequestVO"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 94
    new-instance v0, Lcom/itau/empresas/controller/PagamentoDarfController$4;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/controller/PagamentoDarfController$4;-><init>(Lcom/itau/empresas/controller/PagamentoDarfController;Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 101
    return-void
.end method

.method public validaCampoObrigatorio(Landroid/widget/EditText;Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;)V
    .registers 4
    .param p1, "editText"    # Landroid/widget/EditText;
    .param p2, "listener"    # Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;

    .line 165
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoDarfController;->validaEditText:Lcom/itau/empresas/ui/util/validacao/ValidaEditText;

    invoke-virtual {v0, p2}, Lcom/itau/empresas/ui/util/validacao/ValidaEditText;->setListener(Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;)V

    .line 166
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoDarfController;->validaEditText:Lcom/itau/empresas/ui/util/validacao/ValidaEditText;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/ui/util/validacao/ValidaEditText;->validaTextoVazio(Landroid/widget/EditText;)V

    .line 167
    return-void
.end method

.method public validaTextoObrigatório(Ljava/util/List;Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;)V
    .registers 4
    .param p1, "editTextList"    # Ljava/util/List;
    .param p2, "listener"    # Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Landroid/widget/TextView;>;Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;)V"
        }
    .end annotation

    .line 160
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoDarfController;->validaEditText:Lcom/itau/empresas/ui/util/validacao/ValidaEditText;

    invoke-virtual {v0, p2}, Lcom/itau/empresas/ui/util/validacao/ValidaEditText;->setListener(Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;)V

    .line 161
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoDarfController;->validaEditText:Lcom/itau/empresas/ui/util/validacao/ValidaEditText;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/ui/util/validacao/ValidaEditText;->validaTextoVazio(Ljava/util/List;)V

    .line 162
    return-void
.end method

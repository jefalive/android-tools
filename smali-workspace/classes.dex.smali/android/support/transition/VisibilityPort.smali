.class abstract Landroid/support/transition/VisibilityPort;
.super Landroid/support/transition/TransitionPort;
.source "VisibilityPort.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/transition/VisibilityPort$VisibilityInfo;
    }
.end annotation


# static fields
.field private static final sTransitionProperties:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 40
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android:visibility:visibility"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "android:visibility:parent"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Landroid/support/transition/VisibilityPort;->sTransitionProperties:[Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .line 34
    invoke-direct {p0}, Landroid/support/transition/TransitionPort;-><init>()V

    .line 216
    return-void
.end method

.method private captureValues(Landroid/support/transition/TransitionValues;)V
    .registers 6
    .param p1, "transitionValues"    # Landroid/support/transition/TransitionValues;

    .line 51
    iget-object v0, p1, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    .line 52
    .local v3, "visibility":I
    iget-object v0, p1, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:visibility:visibility"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    iget-object v0, p1, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:visibility:parent"

    iget-object v2, p1, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    return-void
.end method

.method private getVisibilityChangeInfo(Landroid/support/transition/TransitionValues;Landroid/support/transition/TransitionValues;)Landroid/support/transition/VisibilityPort$VisibilityInfo;
    .registers 6
    .param p1, "startValues"    # Landroid/support/transition/TransitionValues;
    .param p2, "endValues"    # Landroid/support/transition/TransitionValues;

    .line 93
    new-instance v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;

    invoke-direct {v2}, Landroid/support/transition/VisibilityPort$VisibilityInfo;-><init>()V

    .line 94
    .local v2, "visInfo":Landroid/support/transition/VisibilityPort$VisibilityInfo;
    const/4 v0, 0x0

    iput-boolean v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->visibilityChange:Z

    .line 95
    const/4 v0, 0x0

    iput-boolean v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->fadeIn:Z

    .line 96
    if-eqz p1, :cond_2a

    .line 97
    iget-object v0, p1, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:visibility:visibility"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->startVisibility:I

    .line 98
    iget-object v0, p1, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:visibility:parent"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->startParent:Landroid/view/ViewGroup;

    goto :goto_30

    .line 100
    :cond_2a
    const/4 v0, -0x1

    iput v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->startVisibility:I

    .line 101
    const/4 v0, 0x0

    iput-object v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->startParent:Landroid/view/ViewGroup;

    .line 103
    :goto_30
    if-eqz p2, :cond_4f

    .line 104
    iget-object v0, p2, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:visibility:visibility"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->endVisibility:I

    .line 105
    iget-object v0, p2, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:visibility:parent"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->endParent:Landroid/view/ViewGroup;

    goto :goto_55

    .line 107
    :cond_4f
    const/4 v0, -0x1

    iput v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->endVisibility:I

    .line 108
    const/4 v0, 0x0

    iput-object v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->endParent:Landroid/view/ViewGroup;

    .line 110
    :goto_55
    if-eqz p1, :cond_9d

    if-eqz p2, :cond_9d

    .line 111
    iget v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->startVisibility:I

    iget v1, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->endVisibility:I

    if-ne v0, v1, :cond_66

    iget-object v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->startParent:Landroid/view/ViewGroup;

    iget-object v1, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->endParent:Landroid/view/ViewGroup;

    if-ne v0, v1, :cond_66

    .line 113
    return-object v2

    .line 115
    :cond_66
    iget v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->startVisibility:I

    iget v1, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->endVisibility:I

    if-eq v0, v1, :cond_82

    .line 116
    iget v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->startVisibility:I

    if-nez v0, :cond_77

    .line 117
    const/4 v0, 0x0

    iput-boolean v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->fadeIn:Z

    .line 118
    const/4 v0, 0x1

    iput-boolean v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->visibilityChange:Z

    goto :goto_9d

    .line 119
    :cond_77
    iget v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->endVisibility:I

    if-nez v0, :cond_9d

    .line 120
    const/4 v0, 0x1

    iput-boolean v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->fadeIn:Z

    .line 121
    const/4 v0, 0x1

    iput-boolean v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->visibilityChange:Z

    goto :goto_9d

    .line 124
    :cond_82
    iget-object v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->startParent:Landroid/view/ViewGroup;

    iget-object v1, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->endParent:Landroid/view/ViewGroup;

    if-eq v0, v1, :cond_9d

    .line 125
    iget-object v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->endParent:Landroid/view/ViewGroup;

    if-nez v0, :cond_93

    .line 126
    const/4 v0, 0x0

    iput-boolean v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->fadeIn:Z

    .line 127
    const/4 v0, 0x1

    iput-boolean v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->visibilityChange:Z

    goto :goto_9d

    .line 128
    :cond_93
    iget-object v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->startParent:Landroid/view/ViewGroup;

    if-nez v0, :cond_9d

    .line 129
    const/4 v0, 0x1

    iput-boolean v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->fadeIn:Z

    .line 130
    const/4 v0, 0x1

    iput-boolean v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->visibilityChange:Z

    .line 135
    :cond_9d
    :goto_9d
    if-nez p1, :cond_a6

    .line 136
    const/4 v0, 0x1

    iput-boolean v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->fadeIn:Z

    .line 137
    const/4 v0, 0x1

    iput-boolean v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->visibilityChange:Z

    goto :goto_ae

    .line 138
    :cond_a6
    if-nez p2, :cond_ae

    .line 139
    const/4 v0, 0x0

    iput-boolean v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->fadeIn:Z

    .line 140
    const/4 v0, 0x1

    iput-boolean v0, v2, Landroid/support/transition/VisibilityPort$VisibilityInfo;->visibilityChange:Z

    .line 142
    :cond_ae
    :goto_ae
    return-object v2
.end method


# virtual methods
.method public captureEndValues(Landroid/support/transition/TransitionValues;)V
    .registers 2
    .param p1, "transitionValues"    # Landroid/support/transition/TransitionValues;

    .line 63
    invoke-direct {p0, p1}, Landroid/support/transition/VisibilityPort;->captureValues(Landroid/support/transition/TransitionValues;)V

    .line 64
    return-void
.end method

.method public captureStartValues(Landroid/support/transition/TransitionValues;)V
    .registers 2
    .param p1, "transitionValues"    # Landroid/support/transition/TransitionValues;

    .line 58
    invoke-direct {p0, p1}, Landroid/support/transition/VisibilityPort;->captureValues(Landroid/support/transition/TransitionValues;)V

    .line 59
    return-void
.end method

.method public createAnimator(Landroid/view/ViewGroup;Landroid/support/transition/TransitionValues;Landroid/support/transition/TransitionValues;)Landroid/animation/Animator;
    .registers 16
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/support/transition/TransitionValues;
    .param p3, "endValues"    # Landroid/support/transition/TransitionValues;

    .line 148
    invoke-direct {p0, p2, p3}, Landroid/support/transition/VisibilityPort;->getVisibilityChangeInfo(Landroid/support/transition/TransitionValues;Landroid/support/transition/TransitionValues;)Landroid/support/transition/VisibilityPort$VisibilityInfo;

    move-result-object v6

    .line 149
    .local v6, "visInfo":Landroid/support/transition/VisibilityPort$VisibilityInfo;
    iget-boolean v0, v6, Landroid/support/transition/VisibilityPort$VisibilityInfo;->visibilityChange:Z

    if-eqz v0, :cond_6e

    .line 152
    const/4 v7, 0x0

    .line 153
    .local v7, "isTarget":Z
    iget-object v0, p0, Landroid/support/transition/VisibilityPort;->mTargets:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_19

    iget-object v0, p0, Landroid/support/transition/VisibilityPort;->mTargetIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_46

    .line 154
    :cond_19
    if-eqz p2, :cond_1e

    iget-object v8, p2, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    goto :goto_1f

    :cond_1e
    const/4 v8, 0x0

    .line 155
    .local v8, "startView":Landroid/view/View;
    :goto_1f
    if-eqz p3, :cond_24

    iget-object v9, p3, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    goto :goto_25

    :cond_24
    const/4 v9, 0x0

    .line 156
    .local v9, "endView":Landroid/view/View;
    :goto_25
    if-eqz v8, :cond_2c

    invoke-virtual {v8}, Landroid/view/View;->getId()I

    move-result v10

    goto :goto_2d

    :cond_2c
    const/4 v10, -0x1

    .line 157
    .local v10, "startId":I
    :goto_2d
    if-eqz v9, :cond_34

    invoke-virtual {v9}, Landroid/view/View;->getId()I

    move-result v11

    goto :goto_35

    :cond_34
    const/4 v11, -0x1

    .line 158
    .local v11, "endId":I
    :goto_35
    int-to-long v0, v10

    invoke-virtual {p0, v8, v0, v1}, Landroid/support/transition/VisibilityPort;->isValidTarget(Landroid/view/View;J)Z

    move-result v0

    if-nez v0, :cond_43

    int-to-long v0, v11

    invoke-virtual {p0, v9, v0, v1}, Landroid/support/transition/VisibilityPort;->isValidTarget(Landroid/view/View;J)Z

    move-result v0

    if-eqz v0, :cond_45

    :cond_43
    const/4 v7, 0x1

    goto :goto_46

    :cond_45
    const/4 v7, 0x0

    .line 160
    .end local v8    # "startView":Landroid/view/View;
    .end local v9    # "endView":Landroid/view/View;
    .end local v10    # "startId":I
    .end local v11    # "endId":I
    :cond_46
    :goto_46
    if-nez v7, :cond_50

    iget-object v0, v6, Landroid/support/transition/VisibilityPort$VisibilityInfo;->startParent:Landroid/view/ViewGroup;

    if-nez v0, :cond_50

    iget-object v0, v6, Landroid/support/transition/VisibilityPort$VisibilityInfo;->endParent:Landroid/view/ViewGroup;

    if-eqz v0, :cond_6e

    .line 161
    :cond_50
    iget-boolean v0, v6, Landroid/support/transition/VisibilityPort$VisibilityInfo;->fadeIn:Z

    if-eqz v0, :cond_61

    .line 162
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    iget v3, v6, Landroid/support/transition/VisibilityPort$VisibilityInfo;->startVisibility:I

    move-object v4, p3

    iget v5, v6, Landroid/support/transition/VisibilityPort$VisibilityInfo;->endVisibility:I

    invoke-virtual/range {v0 .. v5}, Landroid/support/transition/VisibilityPort;->onAppear(Landroid/view/ViewGroup;Landroid/support/transition/TransitionValues;ILandroid/support/transition/TransitionValues;I)Landroid/animation/Animator;

    move-result-object v0

    return-object v0

    .line 165
    :cond_61
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    iget v3, v6, Landroid/support/transition/VisibilityPort$VisibilityInfo;->startVisibility:I

    move-object v4, p3

    iget v5, v6, Landroid/support/transition/VisibilityPort$VisibilityInfo;->endVisibility:I

    invoke-virtual/range {v0 .. v5}, Landroid/support/transition/VisibilityPort;->onDisappear(Landroid/view/ViewGroup;Landroid/support/transition/TransitionValues;ILandroid/support/transition/TransitionValues;I)Landroid/animation/Animator;

    move-result-object v0

    return-object v0

    .line 171
    .end local v7    # "isTarget":Z
    :cond_6e
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTransitionProperties()[Ljava/lang/String;
    .registers 2

    .line 47
    sget-object v0, Landroid/support/transition/VisibilityPort;->sTransitionProperties:[Ljava/lang/String;

    return-object v0
.end method

.method public isVisible(Landroid/support/transition/TransitionValues;)Z
    .registers 6
    .param p1, "values"    # Landroid/support/transition/TransitionValues;

    .line 82
    if-nez p1, :cond_4

    .line 83
    const/4 v0, 0x0

    return v0

    .line 85
    :cond_4
    iget-object v0, p1, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:visibility:visibility"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 86
    .local v2, "visibility":I
    iget-object v0, p1, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:visibility:parent"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/view/View;

    .line 88
    .local v3, "parent":Landroid/view/View;
    if-nez v2, :cond_23

    if-eqz v3, :cond_23

    const/4 v0, 0x1

    goto :goto_24

    :cond_23
    const/4 v0, 0x0

    :goto_24
    return v0
.end method

.method public onAppear(Landroid/view/ViewGroup;Landroid/support/transition/TransitionValues;ILandroid/support/transition/TransitionValues;I)Landroid/animation/Animator;
    .registers 7
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/support/transition/TransitionValues;
    .param p3, "startVisibility"    # I
    .param p4, "endValues"    # Landroid/support/transition/TransitionValues;
    .param p5, "endVisibility"    # I

    .line 192
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDisappear(Landroid/view/ViewGroup;Landroid/support/transition/TransitionValues;ILandroid/support/transition/TransitionValues;I)Landroid/animation/Animator;
    .registers 7
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/support/transition/TransitionValues;
    .param p3, "startVisibility"    # I
    .param p4, "endValues"    # Landroid/support/transition/TransitionValues;
    .param p5, "endVisibility"    # I

    .line 213
    const/4 v0, 0x0

    return-object v0
.end method

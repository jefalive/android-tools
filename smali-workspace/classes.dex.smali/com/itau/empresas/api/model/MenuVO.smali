.class public Lcom/itau/empresas/api/model/MenuVO;
.super Ljava/lang/Object;
.source "MenuVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private chaveMobile:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "chaveMobile"
    .end annotation
.end field

.field private filhos:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "filhos"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;"
        }
    .end annotation
.end field

.field private icone:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "icone"
    .end annotation
.end field

.field private idUniversal:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "idUniversal"
    .end annotation
.end field

.field private nome:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome"
    .end annotation
.end field

.field private pai:Lcom/itau/empresas/api/model/MenuVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "pai"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getChaveMobile()Ljava/lang/String;
    .registers 2

    .line 45
    iget-object v0, p0, Lcom/itau/empresas/api/model/MenuVO;->chaveMobile:Ljava/lang/String;

    return-object v0
.end method

.method public getFilhos()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;"
        }
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/itau/empresas/api/model/MenuVO;->filhos:Ljava/util/List;

    if-nez v0, :cond_a

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0

    .line 64
    :cond_a
    iget-object v0, p0, Lcom/itau/empresas/api/model/MenuVO;->filhos:Ljava/util/List;

    return-object v0
.end method

.method public getIcone()Ljava/lang/String;
    .registers 2

    .line 53
    iget-object v0, p0, Lcom/itau/empresas/api/model/MenuVO;->icone:Ljava/lang/String;

    return-object v0
.end method

.method public getNome()Ljava/lang/String;
    .registers 2

    .line 37
    iget-object v0, p0, Lcom/itau/empresas/api/model/MenuVO;->nome:Ljava/lang/String;

    return-object v0
.end method

.method public setChaveMobile(Ljava/lang/String;)V
    .registers 2
    .param p1, "chaveMobile"    # Ljava/lang/String;

    .line 49
    iput-object p1, p0, Lcom/itau/empresas/api/model/MenuVO;->chaveMobile:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public setFilhos(Ljava/util/List;)V
    .registers 2
    .param p1, "filhos"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;)V"
        }
    .end annotation

    .line 68
    iput-object p1, p0, Lcom/itau/empresas/api/model/MenuVO;->filhos:Ljava/util/List;

    .line 69
    return-void
.end method

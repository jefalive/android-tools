.class public Lcom/itau/empresas/feature/credito/produtos/model/ProdutosRotativosVO;
.super Ljava/lang/Object;
.source "ProdutosRotativosVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private agencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia"
    .end annotation
.end field

.field private codigoBanco:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_banco"
    .end annotation
.end field

.field private codigoEmpresa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_empresa"
    .end annotation
.end field

.field private codigoProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_produto"
    .end annotation
.end field

.field private conta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta"
    .end annotation
.end field

.field private limiteContratado:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "limite_contratado"
    .end annotation
.end field

.field private limiteDisponivel:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "limite_disponivel"
    .end annotation
.end field

.field private limitePreAprovado:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "limite_pre_aprovado"
    .end annotation
.end field

.field private limiteUtilizado:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "limite_utilizado"
    .end annotation
.end field

.field private nomeProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_produto"
    .end annotation
.end field

.field private valorMaximo:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_maximo"
    .end annotation
.end field

.field private valorMinimo:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_minimo"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getLimiteDisponivel()Ljava/lang/Float;
    .registers 2

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/model/ProdutosRotativosVO;->limiteDisponivel:Ljava/lang/Float;

    return-object v0
.end method

.method public getNomeProduto()Ljava/lang/String;
    .registers 2

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/model/ProdutosRotativosVO;->nomeProduto:Ljava/lang/String;

    return-object v0
.end method

.class public Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;
.super Ljava/lang/Object;
.source "ListaPendenciasVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private listaPendencias:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "lista"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/pendencias/PendenciaVO;>;"
        }
    .end annotation
.end field

.field private totalPendencias:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "total_pendencias"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getListaPendencias()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/feature/pendencias/PendenciaVO;>;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;->listaPendencias:Ljava/util/List;

    return-object v0
.end method

.method public getTotalPendencias()I
    .registers 2

    .line 17
    iget v0, p0, Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;->totalPendencias:I

    return v0
.end method

.method public setTotalPendencias(I)V
    .registers 2
    .param p1, "totalPendencias"    # I

    .line 29
    iput p1, p0, Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;->totalPendencias:I

    .line 30
    return-void
.end method

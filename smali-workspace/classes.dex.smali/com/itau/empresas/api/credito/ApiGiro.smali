.class public interface abstract Lcom/itau/empresas/api/credito/ApiGiro;
.super Ljava/lang/Object;
.source "ApiGiro.java"


# virtual methods
.method public abstract buscaEmprestimosParceladosContratadoComprovante(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Path;
            value = "id_comprovante"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "agencia"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "conta"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "digito_conta"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "comprovanteIdReemissao"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation
.end method

.method public abstract buscaEmprestimosParceladosContratados(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "agencia"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "conta"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "digito_verificador_conta"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "data_inicio"
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "data_fim"
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "tipo"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "comprovantesReemissao"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosIdentificadorVO;>;"
        }
    .end annotation
.end method

.method public abstract buscaOfertaProdutosParcelados(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "agencia"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "conta"
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "digito_conta"
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "cpf_operador"
        .end annotation
    .end param
    .param p5    # Z
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Query;
            value = "reduzido"
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/GET;
        opKey = "ofertasCreditosParcelados"
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation
.end method

.method public abstract buscaSimulacaoEmprestimosParcelados(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;
    .param p1    # Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "creditoParceladoSimular"
    .end annotation
.end method

.method public abstract consultarProdutosParceladosSimulacaoProposta(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;
    .param p1    # Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "creditoParceladoPersonalizadoSimular"
    .end annotation
.end method

.method public abstract contrataEmprestimosParcelados(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;)Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;
    .param p1    # Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;
        .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Body;
        .end annotation
    .end param
    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/Headers;
        value = {
            "Accept: application/vnd.itau.v1+json",
            "itau-chave: 38d6287a-9f93-4e70-8d03-dac48cdb5a09"
        }
    .end annotation

    .annotation runtime Lbr/com/itau/sdk/android/core/endpoint/http/POST;
        opKey = "creditoParceladoEfetivar"
    .end annotation
.end method

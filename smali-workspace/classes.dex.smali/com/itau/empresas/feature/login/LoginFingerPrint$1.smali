.class Lcom/itau/empresas/feature/login/LoginFingerPrint$1;
.super Ljava/lang/Object;
.source "LoginFingerPrint.java"

# interfaces
.implements Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/login/LoginFingerPrint;->autenticarFingerPrint()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/login/LoginFingerPrint;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/login/LoginFingerPrint;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/login/LoginFingerPrint;

    .line 65
    iput-object p1, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint$1;->this$0:Lcom/itau/empresas/feature/login/LoginFingerPrint;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;)V
    .registers 4
    .param p1, "exception"    # Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;

    .line 75
    const-string v0, "STATUS_USER_CANCELLED"

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1f

    const-string v0, "Fingerprint operation canceled."

    .line 77
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1f

    .line 79
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 81
    :cond_1f
    return-void
.end method

.method public onResult(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;)V
    .registers 3
    .param p1, "result"    # Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    .line 68
    sget-object v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->AUTHENTICATED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    if-ne p1, v0, :cond_9

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginFingerPrint$1;->this$0:Lcom/itau/empresas/feature/login/LoginFingerPrint;

    # invokes: Lcom/itau/empresas/feature/login/LoginFingerPrint;->loginComFingerprint()V
    invoke-static {v0}, Lcom/itau/empresas/feature/login/LoginFingerPrint;->access$000(Lcom/itau/empresas/feature/login/LoginFingerPrint;)V

    .line 71
    :cond_9
    return-void
.end method

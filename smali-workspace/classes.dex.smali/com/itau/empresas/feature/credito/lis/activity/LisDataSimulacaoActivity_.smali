.class public final Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;
.super Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;
.source "LisDataSimulacaoActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 41
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;-><init>()V

    .line 45
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;

    .line 41
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;

    .line 41
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 60
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 61
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 62
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 63
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/lis/LisController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/LisController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->controller:Lcom/itau/empresas/feature/credito/lis/LisController;

    .line 64
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->injectExtras_()V

    .line 65
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->afterInject()V

    .line 66
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 158
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 159
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_3e

    .line 160
    const-string v0, "valorSelecionado"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 161
    const-string v0, "valorSelecionado"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/math/BigDecimal;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->valorSelecionado:Ljava/math/BigDecimal;

    .line 163
    :cond_1c
    const-string v0, "possuiProdutoLisAdicional"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 164
    const-string v0, "possuiProdutoLisAdicional"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->possuiProdutoLisAdicional:Z

    .line 166
    :cond_2c
    const-string v0, "ofertaContratacaoLisVo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 167
    const-string v0, "ofertaContratacaoLisVo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    .line 170
    :cond_3e
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 87
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 195
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$6;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$6;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 203
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 183
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$5;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$5;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 191
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 53
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->init_(Landroid/os/Bundle;)V

    .line 54
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 56
    const v0, 0x7f030049

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->setContentView(I)V

    .line 57
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 6
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 100
    const v0, 0x7f0e020e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->root:Landroid/widget/LinearLayout;

    .line 101
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 102
    const v0, 0x7f0e0211

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->lisCardValorView:Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;

    .line 103
    const v0, 0x7f0e021c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->checkBoxLisAdicional:Landroid/widget/CheckBox;

    .line 104
    const v0, 0x7f0e021d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->textoLisAdicional:Landroid/widget/TextView;

    .line 105
    const v0, 0x7f0e0204

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->textoLisExclamacao:Landroid/widget/TextView;

    .line 106
    const v0, 0x7f0e021a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->textoLisCondicoesGeraisPdf:Landroid/widget/TextView;

    .line 107
    const v0, 0x7f0e0213

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->seletorDataPagamentoView:Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;

    .line 108
    const v0, 0x7f0e0215

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->linearLayoutLisAdicional:Landroid/widget/LinearLayout;

    .line 109
    const v0, 0x7f0e0214

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->warningData:Landroid/view/View;

    .line 110
    const v0, 0x7f0e0217

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->rlChequeEspecialAdicional:Landroid/view/View;

    .line 111
    const v0, 0x7f0e018f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 112
    .local v2, "view_botao_continuar":Landroid/view/View;
    const v0, 0x7f0e021b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 114
    .local v3, "view_rl_cb_lis_adicional":Landroid/view/View;
    if-eqz v2, :cond_8d

    .line 115
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$1;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    :cond_8d
    if-eqz v3, :cond_97

    .line 125
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$2;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    :cond_97
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->lisCardValorView:Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;

    if-eqz v0, :cond_a5

    .line 135
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->lisCardValorView:Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$3;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    :cond_a5
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->rlChequeEspecialAdicional:Landroid/view/View;

    if-eqz v0, :cond_b3

    .line 145
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->rlChequeEspecialAdicional:Landroid/view/View;

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$4;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 154
    :cond_b3
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->afterViews()V

    .line 155
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 70
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->setContentView(I)V

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 72
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 82
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->setContentView(Landroid/view/View;)V

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 84
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 76
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 78
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 174
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->setIntent(Landroid/content/Intent;)V

    .line 175
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_;->injectExtras_()V

    .line 176
    return-void
.end method

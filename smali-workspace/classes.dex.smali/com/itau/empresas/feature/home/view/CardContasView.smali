.class public Lcom/itau/empresas/feature/home/view/CardContasView;
.super Lcom/itau/empresas/feature/home/view/ICardView;
.source "CardContasView.java"

# interfaces
.implements Lcom/itau/empresas/api/ApiConsumidor;


# instance fields
.field application:Lcom/itau/empresas/CustomApplication;

.field containerLis:Landroid/widget/LinearLayout;

.field containerLisAdicional:Landroid/widget/LinearLayout;

.field containerLisUtilizado:Landroid/widget/RelativeLayout;

.field containerTotalLis:Landroid/widget/LinearLayout;

.field controller:Lcom/itau/empresas/controller/ExtratoController;

.field private limiteTotal:Ljava/lang/Double;

.field private lis:Ljava/lang/Double;

.field private lisAdicional:Ljava/lang/Double;

.field private lisUtilizado:Ljava/lang/Double;

.field private lisVO:Lcom/itau/empresas/api/model/LisVO;

.field private saldoDisponivel:Ljava/lang/Double;

.field private saldoVO:Lcom/itau/empresas/api/model/SaldoVO;

.field separadorSaldo:Landroid/view/View;

.field slideLisUtilizado:Landroid/widget/SeekBar;

.field textoErro:Landroid/widget/TextView;

.field textoLisTotal:Landroid/widget/TextView;

.field textoTituloLimite:Landroid/widget/TextView;

.field textoTituloSaldoLimite:Landroid/widget/TextView;

.field textoValorAdicional:Landroid/widget/TextView;

.field textoValorComLimite:Landroid/widget/TextView;

.field textoValorLis:Landroid/widget/TextView;

.field textoValorSaldo:Landroid/widget/TextView;

.field textoValorUtilizado:Landroid/widget/TextView;

.field private totalComLimite:Ljava/lang/Double;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;

    .line 72
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/view/ICardView;-><init>(Landroid/content/Context;)V

    .line 62
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->saldoDisponivel:Ljava/lang/Double;

    .line 63
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lis:Ljava/lang/Double;

    .line 64
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lisAdicional:Ljava/lang/Double;

    .line 65
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lisUtilizado:Ljava/lang/Double;

    .line 66
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->limiteTotal:Ljava/lang/Double;

    .line 67
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->totalComLimite:Ljava/lang/Double;

    .line 73
    return-void
.end method

.method private ajustaVisibilidadeDosItens()V
    .registers 5

    .line 273
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->separadorSaldo:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 274
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->containerTotalLis:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 275
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lisUtilizado:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_3d

    .line 276
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->slideLisUtilizado:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->limiteTotal:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 277
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->slideLisUtilizado:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lisUtilizado:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 278
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->containerLisUtilizado:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 279
    const-string v0, "consultar limite"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/home/view/CardContasView;->setTextoVerMaisDireito(Ljava/lang/String;Z)V

    goto :goto_44

    .line 281
    :cond_3d
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->containerLisUtilizado:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 283
    :goto_44
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lisAdicional:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_58

    .line 284
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->containerLisAdicional:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_6a

    .line 286
    :cond_58
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->containerLisAdicional:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 287
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->separadorSaldo:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 288
    const-string v0, "consultar limite"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/home/view/CardContasView;->setTextoVerMaisDireito(Ljava/lang/String;Z)V

    .line 290
    :goto_6a
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lis:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_7e

    .line 291
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->containerLis:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_96

    .line 293
    :cond_7e
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->containerLis:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 294
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->containerTotalLis:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 295
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->separadorSaldo:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 296
    const-string v0, "consultar limite"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/home/view/CardContasView;->setTextoVerMaisDireito(Ljava/lang/String;Z)V

    .line 298
    :goto_96
    return-void
.end method

.method private desregistraEventBus()V
    .registers 2

    .line 128
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->isRegistered(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 129
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->unregister(Ljava/lang/Object;)V

    .line 131
    :cond_11
    return-void
.end method

.method private insereValoresLis(Lcom/itau/empresas/api/model/LisVO;)V
    .registers 6
    .param p1, "retornoLis"    # Lcom/itau/empresas/api/model/LisVO;

    .line 234
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->saldoVO:Lcom/itau/empresas/api/model/SaldoVO;

    if-eqz v0, :cond_6

    if-nez p1, :cond_7

    .line 235
    :cond_6
    return-void

    .line 238
    :cond_7
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteLis()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1b

    .line 239
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteLis()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lis:Ljava/lang/Double;

    .line 242
    :cond_1b
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->saldoVO:Lcom/itau/empresas/api/model/SaldoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/SaldoVO;->getValorLimiteUtilizado()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_33

    .line 243
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->saldoVO:Lcom/itau/empresas/api/model/SaldoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/SaldoVO;->getValorLimiteUtilizado()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lisUtilizado:Ljava/lang/Double;

    .line 246
    :cond_33
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteAdicional()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_47

    .line 247
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteAdicional()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lisAdicional:Ljava/lang/Double;

    .line 250
    :cond_47
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteTotal()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5b

    .line 251
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteTotal()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->limiteTotal:Ljava/lang/Double;

    .line 254
    :cond_5b
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->saldoVO:Lcom/itau/empresas/api/model/SaldoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/SaldoVO;->getDisponivelSaque()Ljava/lang/Double;

    move-result-object v0

    if-eqz v0, :cond_6b

    .line 255
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->saldoVO:Lcom/itau/empresas/api/model/SaldoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/SaldoVO;->getDisponivelSaque()Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->totalComLimite:Ljava/lang/Double;

    .line 257
    :cond_6b
    return-void
.end method

.method private insereValoresMultiLimite(Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;)V
    .registers 4
    .param p1, "retornoMultiLimite"    # Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;

    .line 185
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->saldoVO:Lcom/itau/empresas/api/model/SaldoVO;

    if-eqz v0, :cond_6

    if-nez p1, :cond_7

    .line 186
    :cond_6
    return-void

    .line 189
    :cond_7
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;->getLimiteTotalConcedido()Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->doubleValue()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lis:Ljava/lang/Double;

    .line 191
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;->getLimiteTotalUtilizado()Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lisUtilizado:Ljava/lang/Double;

    .line 193
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;->getLimiteTotal()Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->limiteTotal:Ljava/lang/Double;

    .line 195
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->saldoVO:Lcom/itau/empresas/api/model/SaldoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/SaldoVO;->getDisponivelSaque()Ljava/lang/Double;

    move-result-object v0

    if-eqz v0, :cond_43

    .line 196
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->saldoVO:Lcom/itau/empresas/api/model/SaldoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/SaldoVO;->getDisponivelSaque()Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->totalComLimite:Ljava/lang/Double;

    .line 198
    :cond_43
    return-void
.end method

.method private trataMensagemErroSaldo(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 6
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 331
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 332
    return-void

    .line 335
    :cond_7
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v2

    .line 336
    .local v2, "errorDTO":Lbr/com/itau/sdk/android/core/model/ErrorDTO;
    if-eqz v2, :cond_16

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v3

    goto :goto_21

    .line 337
    :cond_16
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0701f2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 339
    .local v3, "mensagem":Ljava/lang/String;
    :goto_21
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->textoErro:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 340
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->textoErro:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 341
    return-void
.end method

.method private verificaEstouroLimite()V
    .registers 5

    .line 260
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lisUtilizado:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iget-object v2, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->limiteTotal:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-lez v0, :cond_14

    .line 261
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->limiteTotal:Ljava/lang/Double;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lisUtilizado:Ljava/lang/Double;

    .line 263
    :cond_14
    return-void
.end method

.method private zerarValores()V
    .registers 3

    .line 266
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lis:Ljava/lang/Double;

    .line 267
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lisAdicional:Ljava/lang/Double;

    .line 268
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lisUtilizado:Ljava/lang/Double;

    .line 269
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->limiteTotal:Ljava/lang/Double;

    .line 270
    return-void
.end method


# virtual methods
.method protected abrirCard()V
    .registers 1

    .line 106
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->expandirCard()V

    .line 107
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->retiraDivisor()V

    .line 108
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->ajustaVisibilidadeDosItens()V

    .line 109
    return-void
.end method

.method afterViews()V
    .registers 3

    .line 87
    invoke-static {p0}, Lcom/itau/empresas/ui/util/EventBusUtils;->registraEventBus(Landroid/view/View;)V

    .line 88
    const-string v0, "ver extrato"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/home/view/CardContasView;->setTextoVerMaisEsquerdo(Ljava/lang/String;Z)V

    .line 89
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->preferenciasTAG()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/view/CardContasView;->buscaEstadoWidget(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 90
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->fazerRequest()V

    .line 92
    :cond_16
    return-void
.end method

.method public direcionarDetalhesLimite()V
    .registers 4

    .line 120
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    if-eqz v0, :cond_1a

    .line 121
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_$IntentBuilder_;

    move-result-object v0

    const-string v1, "lisVO"

    iget-object v2, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_$IntentBuilder_;->extra(Ljava/lang/String;Ljava/io/Serializable;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_$IntentBuilder_;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    goto :goto_25

    .line 123
    :cond_1a
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 125
    :goto_25
    return-void
.end method

.method public direcionarExtrato()V
    .registers 4

    .line 113
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;

    move-result-object v0

    const-string v1, "EXTRA_EXIBIR_EXTRATO"

    .line 114
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;->extra(Ljava/lang/String;Z)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 115
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->desregistraEventBus()V

    .line 116
    return-void
.end method

.method protected fazerRequest()V
    .registers 2

    .line 100
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/view/CardContasView;->setCarregando(Z)V

    .line 101
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->controller:Lcom/itau/empresas/controller/ExtratoController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/ExtratoController;->consultaSaldo()V

    .line 102
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->controller:Lcom/itau/empresas/controller/ExtratoController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/ExtratoController;->consultaMultiLimite()V

    .line 103
    return-void
.end method

.method protected layoutResId()I
    .registers 2

    .line 77
    const v0, 0x7f030156

    return v0
.end method

.method protected onDetachedFromWindow()V
    .registers 1

    .line 139
    invoke-super {p0}, Lcom/itau/empresas/feature/home/view/ICardView;->onDetachedFromWindow()V

    .line 140
    invoke-static {p0}, Lcom/itau/empresas/ui/util/EventBusUtils;->registraEventBus(Landroid/view/View;)V

    .line 141
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->desregistraEventBus()V

    .line 142
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 7
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 301
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 302
    return-void

    .line 305
    :cond_7
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/view/CardContasView;->setCarregando(Z)V

    .line 306
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    .line 307
    .local v2, "opKey":Ljava/lang/String;
    move-object v3, v2

    const/4 v4, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_68

    goto :goto_3a

    :sswitch_1d
    const-string v0, "multiLimites"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3a

    const/4 v4, 0x0

    goto :goto_3a

    :sswitch_27
    const-string v0, "lis"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3a

    const/4 v4, 0x1

    goto :goto_3a

    :sswitch_31
    const-string v0, "saldo"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3a

    const/4 v4, 0x2

    :cond_3a
    :goto_3a
    packed-switch v4, :pswitch_data_76

    goto :goto_57

    .line 310
    :pswitch_3e
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/view/CardContasView;->setCarregando(Z)V

    .line 311
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->controller:Lcom/itau/empresas/controller/ExtratoController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/ExtratoController;->consultaLis()V

    .line 312
    goto :goto_67

    .line 315
    :pswitch_48
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/view/CardContasView;->setCarregando(Z)V

    .line 316
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->zerarValores()V

    .line 317
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->abrirCard()V

    .line 318
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/view/CardContasView;->setCarregando(Z)V

    .line 319
    goto :goto_67

    .line 322
    :goto_57
    :pswitch_57
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->textoValorSaldo:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 323
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->zerarValores()V

    .line 324
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->ajustaVisibilidadeDosItens()V

    .line 325
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/view/CardContasView;->trataMensagemErroSaldo(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V

    .line 328
    :goto_67
    return-void

    :sswitch_data_68
    .sparse-switch
        -0x65c589d0 -> :sswitch_1d
        0x1a296 -> :sswitch_27
        0x68248e9 -> :sswitch_31
    .end sparse-switch

    :pswitch_data_76
    .packed-switch 0x0
        :pswitch_3e
        :pswitch_48
        :pswitch_57
    .end packed-switch
.end method

.method public onEventMainThread(Lcom/itau/empresas/api/model/LisVO;)V
    .registers 6
    .param p1, "retornoLis"    # Lcom/itau/empresas/api/model/LisVO;

    .line 201
    iput-object p1, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    .line 202
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/view/CardContasView;->setCarregando(Z)V

    .line 203
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->zerarValores()V

    .line 205
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->textoTituloLimite:Landroid/widget/TextView;

    .line 206
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07017a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->textoTituloSaldoLimite:Landroid/widget/TextView;

    .line 208
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07017c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/view/CardContasView;->insereValoresLis(Lcom/itau/empresas/api/model/LisVO;)V

    .line 212
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->verificaEstouroLimite()V

    .line 214
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->textoValorLis:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lis:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(DZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 216
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->textoValorUtilizado:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lisUtilizado:Ljava/lang/Double;

    .line 217
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(DZ)Ljava/lang/String;

    move-result-object v1

    .line 216
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->textoValorAdicional:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lisAdicional:Ljava/lang/Double;

    .line 220
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(DZ)Ljava/lang/String;

    move-result-object v1

    .line 219
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->textoLisTotal:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->limiteTotal:Ljava/lang/Double;

    .line 223
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(DZ)Ljava/lang/String;

    move-result-object v1

    .line 222
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 225
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->textoValorComLimite:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->totalComLimite:Ljava/lang/Double;

    .line 226
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(DZ)Ljava/lang/String;

    move-result-object v1

    .line 225
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/view/CardContasView;->setCarregando(Z)V

    .line 229
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->abrirCard()V

    .line 230
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->ajustaVisibilidadeDosItens()V

    .line 231
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/api/model/SaldoVO;)V
    .registers 6
    .param p1, "retornoSaldo"    # Lcom/itau/empresas/api/model/SaldoVO;

    .line 145
    iput-object p1, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->saldoVO:Lcom/itau/empresas/api/model/SaldoVO;

    .line 146
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->textoValorSaldo:Landroid/widget/TextView;

    .line 147
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/SaldoVO;->getSaldoDisponivelSaque()Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 146
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/SaldoVO;->getSaldoDisponivelSaque()Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->saldoDisponivel:Ljava/lang/Double;

    .line 151
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->saldoDisponivel:Ljava/lang/Double;

    invoke-static {v0, v1}, Lcom/itau/empresas/feature/saldo/SaldoUtils;->getCorSaldo(Landroid/content/Context;Ljava/lang/Double;)I

    move-result v3

    .line 153
    .local v3, "cor":I
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->textoValorSaldo:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 155
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->abrirCard()V

    .line 156
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;)V
    .registers 6
    .param p1, "retornoMultiLimite"    # Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;

    .line 159
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/view/CardContasView;->setCarregando(Z)V

    .line 160
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->zerarValores()V

    .line 162
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->textoTituloLimite:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07017f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->textoTituloSaldoLimite:Landroid/widget/TextView;

    .line 164
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07017d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/view/CardContasView;->insereValoresMultiLimite(Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;)V

    .line 167
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->verificaEstouroLimite()V

    .line 169
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->textoValorLis:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lis:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(DZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->textoValorUtilizado:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->lisUtilizado:Ljava/lang/Double;

    .line 172
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(DZ)Ljava/lang/String;

    move-result-object v1

    .line 171
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->textoValorComLimite:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->totalComLimite:Ljava/lang/Double;

    .line 175
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(DZ)Ljava/lang/String;

    move-result-object v1

    .line 174
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/home/view/CardContasView;->setCarregando(Z)V

    .line 178
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->abrirCard()V

    .line 179
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->ajustaVisibilidadeDosItens()V

    .line 181
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 135
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "saldo"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "multiLimites"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "lis"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected preferenciasTAG()Ljava/lang/String;
    .registers 2

    .line 82
    const-string v0, "card_contas"

    return-object v0
.end method

.method public setTextoTituloCard(Ljava/lang/String;)V
    .registers 3
    .param p1, "titulo"    # Ljava/lang/String;

    .line 95
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView;->textoTituloCard:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    return-void
.end method

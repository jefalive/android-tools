.class Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder$1;
.super Ljava/lang/Object;
.source "AreaUsuarioSelecaoContaAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;-><init>(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;

.field final synthetic val$this$0:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;


# direct methods
.method constructor <init>(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;)V
    .registers 3
    .param p1, "this$1"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;

    .line 175
    iput-object p1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder$1;->this$1:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;

    iput-object p2, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder$1;->val$this$0:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 5
    .param p1, "view"    # Landroid/view/View;

    .line 177
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder$1;->this$1:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;

    iget-object v0, v0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->this$0:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

    # getter for: Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->onClickListener:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$OnClick;
    invoke-static {v0}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->access$600(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;)Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$OnClick;

    move-result-object v0

    if-eqz v0, :cond_29

    .line 178
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder$1;->this$1:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;

    iget-object v0, v0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->this$0:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

    # getter for: Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->onClickListener:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$OnClick;
    invoke-static {v0}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->access$600(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;)Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$OnClick;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder$1;->this$1:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;

    iget-object v1, v1, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->this$0:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

    # getter for: Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->listaContas:Ljava/util/List;
    invoke-static {v1}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;->access$700(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder$1;->this$1:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;

    invoke-virtual {v2}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->getAdapterPosition()I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/api/model/ContaOperadorVO;

    invoke-interface {v0, v1}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$OnClick;->aoClicarNaConta(Lcom/itau/empresas/api/model/ContaOperadorVO;)V

    .line 180
    :cond_29
    return-void
.end method

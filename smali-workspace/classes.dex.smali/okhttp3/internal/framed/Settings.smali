.class public final Lokhttp3/internal/framed/Settings;
.super Ljava/lang/Object;
.source "Settings.java"


# instance fields
.field private persistValue:I

.field private persisted:I

.field private set:I

.field private final values:[I


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const/16 v0, 0xa

    new-array v0, v0, [I

    iput-object v0, p0, Lokhttp3/internal/framed/Settings;->values:[I

    return-void
.end method


# virtual methods
.method clear()V
    .registers 3

    .line 85
    const/4 v0, 0x0

    iput v0, p0, Lokhttp3/internal/framed/Settings;->persisted:I

    const/4 v0, 0x0

    iput v0, p0, Lokhttp3/internal/framed/Settings;->persistValue:I

    const/4 v0, 0x0

    iput v0, p0, Lokhttp3/internal/framed/Settings;->set:I

    .line 86
    iget-object v0, p0, Lokhttp3/internal/framed/Settings;->values:[I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 87
    return-void
.end method

.method flags(I)I
    .registers 4
    .param p1, "id"    # I

    .line 124
    const/4 v1, 0x0

    .line 125
    .local v1, "result":I
    invoke-virtual {p0, p1}, Lokhttp3/internal/framed/Settings;->isPersisted(I)Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v1, 0x2

    .line 126
    :cond_8
    invoke-virtual {p0, p1}, Lokhttp3/internal/framed/Settings;->persistValue(I)Z

    move-result v0

    if-eqz v0, :cond_10

    or-int/lit8 v1, v1, 0x1

    .line 127
    :cond_10
    return v1
.end method

.method get(I)I
    .registers 3
    .param p1, "id"    # I

    .line 119
    iget-object v0, p0, Lokhttp3/internal/framed/Settings;->values:[I

    aget v0, v0, p1

    return v0
.end method

.method getHeaderTableSize()I
    .registers 4

    .line 143
    const/4 v2, 0x2

    .line 144
    .local v2, "bit":I
    iget v0, p0, Lokhttp3/internal/framed/Settings;->set:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_d

    iget-object v0, p0, Lokhttp3/internal/framed/Settings;->values:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    goto :goto_e

    :cond_d
    const/4 v0, -0x1

    :goto_e
    return v0
.end method

.method getInitialWindowSize(I)I
    .registers 5
    .param p1, "defaultValue"    # I

    .line 197
    const/16 v2, 0x80

    .line 198
    .local v2, "bit":I
    iget v0, p0, Lokhttp3/internal/framed/Settings;->set:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_e

    iget-object v0, p0, Lokhttp3/internal/framed/Settings;->values:[I

    const/4 v1, 0x7

    aget v0, v0, v1

    goto :goto_f

    :cond_e
    move v0, p1

    :goto_f
    return v0
.end method

.method getMaxConcurrentStreams(I)I
    .registers 5
    .param p1, "defaultValue"    # I

    .line 168
    const/16 v2, 0x10

    .line 169
    .local v2, "bit":I
    iget v0, p0, Lokhttp3/internal/framed/Settings;->set:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_e

    iget-object v0, p0, Lokhttp3/internal/framed/Settings;->values:[I

    const/4 v1, 0x4

    aget v0, v0, v1

    goto :goto_f

    :cond_e
    move v0, p1

    :goto_f
    return v0
.end method

.method getMaxFrameSize(I)I
    .registers 5
    .param p1, "defaultValue"    # I

    .line 180
    const/16 v2, 0x20

    .line 181
    .local v2, "bit":I
    iget v0, p0, Lokhttp3/internal/framed/Settings;->set:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_e

    iget-object v0, p0, Lokhttp3/internal/framed/Settings;->values:[I

    const/4 v1, 0x5

    aget v0, v0, v1

    goto :goto_f

    :cond_e
    move v0, p1

    :goto_f
    return v0
.end method

.method isPersisted(I)Z
    .registers 4
    .param p1, "id"    # I

    .line 225
    const/4 v0, 0x1

    shl-int v1, v0, p1

    .line 226
    .local v1, "bit":I
    iget v0, p0, Lokhttp3/internal/framed/Settings;->persisted:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method isSet(I)Z
    .registers 4
    .param p1, "id"    # I

    .line 113
    const/4 v0, 0x1

    shl-int v1, v0, p1

    .line 114
    .local v1, "bit":I
    iget v0, p0, Lokhttp3/internal/framed/Settings;->set:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method merge(Lokhttp3/internal/framed/Settings;)V
    .registers 5
    .param p1, "other"    # Lokhttp3/internal/framed/Settings;

    .line 234
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    const/16 v0, 0xa

    if-ge v2, v0, :cond_1a

    .line 235
    invoke-virtual {p1, v2}, Lokhttp3/internal/framed/Settings;->isSet(I)Z

    move-result v0

    if-nez v0, :cond_c

    goto :goto_17

    .line 236
    :cond_c
    invoke-virtual {p1, v2}, Lokhttp3/internal/framed/Settings;->flags(I)I

    move-result v0

    invoke-virtual {p1, v2}, Lokhttp3/internal/framed/Settings;->get(I)I

    move-result v1

    invoke-virtual {p0, v2, v0, v1}, Lokhttp3/internal/framed/Settings;->set(III)Lokhttp3/internal/framed/Settings;

    .line 234
    :goto_17
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 238
    .end local v2    # "i":I
    :cond_1a
    return-void
.end method

.method persistValue(I)Z
    .registers 4
    .param p1, "id"    # I

    .line 219
    const/4 v0, 0x1

    shl-int v1, v0, p1

    .line 220
    .local v1, "bit":I
    iget v0, p0, Lokhttp3/internal/framed/Settings;->persistValue:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method set(III)Lokhttp3/internal/framed/Settings;
    .registers 7
    .param p1, "id"    # I
    .param p2, "idFlags"    # I
    .param p3, "value"    # I

    .line 90
    iget-object v0, p0, Lokhttp3/internal/framed/Settings;->values:[I

    array-length v0, v0

    if-lt p1, v0, :cond_6

    .line 91
    return-object p0

    .line 94
    :cond_6
    const/4 v0, 0x1

    shl-int v2, v0, p1

    .line 95
    .local v2, "bit":I
    iget v0, p0, Lokhttp3/internal/framed/Settings;->set:I

    or-int/2addr v0, v2

    iput v0, p0, Lokhttp3/internal/framed/Settings;->set:I

    .line 96
    and-int/lit8 v0, p2, 0x1

    if-eqz v0, :cond_18

    .line 97
    iget v0, p0, Lokhttp3/internal/framed/Settings;->persistValue:I

    or-int/2addr v0, v2

    iput v0, p0, Lokhttp3/internal/framed/Settings;->persistValue:I

    goto :goto_1f

    .line 99
    :cond_18
    iget v0, p0, Lokhttp3/internal/framed/Settings;->persistValue:I

    xor-int/lit8 v1, v2, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lokhttp3/internal/framed/Settings;->persistValue:I

    .line 101
    :goto_1f
    and-int/lit8 v0, p2, 0x2

    if-eqz v0, :cond_29

    .line 102
    iget v0, p0, Lokhttp3/internal/framed/Settings;->persisted:I

    or-int/2addr v0, v2

    iput v0, p0, Lokhttp3/internal/framed/Settings;->persisted:I

    goto :goto_30

    .line 104
    :cond_29
    iget v0, p0, Lokhttp3/internal/framed/Settings;->persisted:I

    xor-int/lit8 v1, v2, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lokhttp3/internal/framed/Settings;->persisted:I

    .line 107
    :goto_30
    iget-object v0, p0, Lokhttp3/internal/framed/Settings;->values:[I

    aput p3, v0, p1

    .line 108
    return-object p0
.end method

.method size()I
    .registers 2

    .line 132
    iget v0, p0, Lokhttp3/internal/framed/Settings;->set:I

    invoke-static {v0}, Ljava/lang/Integer;->bitCount(I)I

    move-result v0

    return v0
.end method

.class public Lbr/com/itau/sdk/android/core/model/RemoteBundleDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private data:Lcom/google/gson/JsonObject;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data"
    .end annotation
.end field

.field private hash:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "hash"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getData()Lcom/google/gson/JsonObject;
    .registers 2

    .line 21
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/RemoteBundleDTO;->data:Lcom/google/gson/JsonObject;

    return-object v0
.end method

.method public getHash()Ljava/lang/String;
    .registers 2

    .line 17
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/RemoteBundleDTO;->hash:Ljava/lang/String;

    return-object v0
.end method

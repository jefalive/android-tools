.class public Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;
.super Landroid/widget/RelativeLayout;
.source "LisComprovanteDetalheView.java"


# instance fields
.field assinaturaEletronica:Landroid/widget/LinearLayout;

.field private comprovante:Lcom/itau/empresas/api/model/ComprovanteServiceVO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/itau/empresas/api/model/ComprovanteServiceVO<Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;>;"
        }
    .end annotation
.end field

.field comprovanteDadosConta:Landroid/widget/LinearLayout;

.field comprovanteValores:Landroid/widget/LinearLayout;

.field private context:Landroid/content/Context;

.field private customApplication:Lcom/itau/empresas/CustomApplication;

.field dadosControle:Landroid/widget/LinearLayout;

.field devedorSolidario:Landroid/widget/LinearLayout;

.field lisComprovanteDetalhe:Landroid/view/View;

.field private respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

.field svLisComprovanteDetalhe:Landroid/widget/ScrollView;

.field private tipoComprovante:Ljava/lang/String;

.field toolbar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 58
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 62
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 66
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 67
    return-void
.end method

.method private constroiAssinaturaEletronica()V
    .registers 3

    .line 108
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->assinaturaEletronica:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->vinculaAssinaturaEletronica()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->inicializarConstrutor(Landroid/widget/LinearLayout;Ljava/util/List;)V

    .line 109
    return-void
.end method

.method private constroiComrpovanteValores()V
    .registers 3

    .line 104
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->comprovanteValores:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->getComprovanteLis()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->inicializarConstrutor(Landroid/widget/LinearLayout;Ljava/util/List;)V

    .line 105
    return-void
.end method

.method private constroiDadosConta()V
    .registers 3

    .line 122
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->comprovanteDadosConta:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->vinculaDadosConta()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->inicializarConstrutor(Landroid/widget/LinearLayout;Ljava/util/List;)V

    .line 123
    return-void
.end method

.method private constroiDadosControle()V
    .registers 3

    .line 117
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->dadosControle:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->vinculaDadosControle()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->inicializarConstrutor(Landroid/widget/LinearLayout;Ljava/util/List;)V

    .line 118
    return-void
.end method

.method private constroiDevedorSolidario()V
    .registers 3

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->devedorSolidario:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->vinculaDevedorSolidario()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->inicializarConstrutor(Landroid/widget/LinearLayout;Ljava/util/List;)V

    .line 113
    return-void
.end method

.method private getComprovanteLis()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
        }
    .end annotation

    .line 140
    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->tipoComprovante:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v0

    packed-switch v0, :pswitch_data_42

    goto :goto_28

    :pswitch_b
    const-string v0, "L01"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_28

    const/4 v2, 0x0

    goto :goto_28

    :pswitch_15
    const-string v0, "L02"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_28

    const/4 v2, 0x1

    goto :goto_28

    :pswitch_1f
    const-string v0, "L03"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_28

    const/4 v2, 0x2

    :cond_28
    :goto_28
    packed-switch v2, :pswitch_data_4c

    goto :goto_3b

    .line 142
    :pswitch_2c
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->vinculaLis()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 144
    :pswitch_31
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->vinculaRecebiveis()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 146
    :pswitch_36
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->vinculaAdicional()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 148
    :goto_3b
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0

    nop

    :pswitch_data_42
    .packed-switch 0x1234d
        :pswitch_b
        :pswitch_15
        :pswitch_1f
    .end packed-switch

    :pswitch_data_4c
    .packed-switch 0x0
        :pswitch_2c
        :pswitch_31
        :pswitch_36
    .end packed-switch
.end method

.method private inicializarConstrutor(Landroid/widget/LinearLayout;Ljava/util/List;)V
    .registers 6
    .param p1, "target"    # Landroid/widget/LinearLayout;
    .param p2, "comprovanteItem"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/widget/LinearLayout;Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;)V"
        }
    .end annotation

    .line 128
    :try_start_0
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    .line 129
    invoke-static {v0}, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;->builder(Landroid/content/Context;)Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;

    move-result-object v0

    .line 130
    invoke-virtual {v0, p2}, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;->itensComprovanteList(Ljava/util/List;)Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;

    move-result-object v0

    .line 131
    invoke-virtual {v0, p1}, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;->targetLayout(Landroid/widget/LinearLayout;)Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;

    move-result-object v0

    .line 132
    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/comprovante/GeradoritensComprovanteBuilder;->build()V
    :try_end_11
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_11} :catch_12

    .line 135
    goto :goto_1c

    .line 133
    :catch_12
    move-exception v2

    .line 134
    .local v2, "e":Ljava/lang/IllegalAccessException;
    const-string v0, "initBuilder"

    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lbr/com/itau/textovoz/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 136
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :goto_1c
    return-void
.end method

.method private vinculaAdicional()Ljava/util/List;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
        }
    .end annotation

    .line 211
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->vinculaLis()Ljava/util/List;

    move-result-object v5

    .line 213
    .local v5, "item":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705d5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    .line 214
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getValorLimiteAval()F

    move-result v3

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    .line 215
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getValorLimiteAvalPorcentagem()F

    move-result v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemSimples(F)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 213
    const v3, 0x7f0705cb

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v5, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 216
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705d4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    .line 217
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getValorLimiteAdicional()Ljava/lang/String;

    move-result-object v1

    .line 216
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v5, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 219
    return-object v5
.end method

.method private vinculaAssinaturaEletronica()Ljava/util/List;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
        }
    .end annotation

    .line 198
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 200
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705cd

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getDadosAssinaturaEletronicaNome()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 201
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705c5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CPF:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getDadosAssinaturaEletronicaCpf()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 202
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705c6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    .line 203
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getDadosAssinaturaEletronicaData()Ljava/lang/String;

    move-result-object v1

    .line 202
    invoke-static {v1}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705be

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->comprovante:Lcom/itau/empresas/api/model/ComprovanteServiceVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ComprovanteServiceVO;->getNomeCanal()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206
    return-object v3
.end method

.method private vinculaDadosConta()Ljava/util/List;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
        }
    .end annotation

    .line 249
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 250
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705ce

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->comprovante:Lcom/itau/empresas/api/model/ComprovanteServiceVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ComprovanteServiceVO;->getNomeCliente()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 251
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705bb

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->customApplication:Lcom/itau/empresas/CustomApplication;

    .line 252
    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->customApplication:Lcom/itau/empresas/CustomApplication;

    .line 253
    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 251
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705c0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    .line 255
    invoke-virtual {v2}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getCnpj()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 254
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 257
    return-object v3
.end method

.method private vinculaDadosControle()Ljava/util/List;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
        }
    .end annotation

    .line 237
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 239
    .local v5, "list":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705bc

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->comprovante:Lcom/itau/empresas/api/model/ComprovanteServiceVO;

    .line 240
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/ComprovanteServiceVO;->getDataRealTransacao()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->comprovante:Lcom/itau/empresas/api/model/ComprovanteServiceVO;

    .line 241
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/ComprovanteServiceVO;->getHoraRealTransacao()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataHorario(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->comprovante:Lcom/itau/empresas/api/model/ComprovanteServiceVO;

    .line 242
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/ComprovanteServiceVO;->getNomeCanal()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v2, v4

    .line 240
    const v3, 0x7f0705c9

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 239
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705bd

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->comprovante:Lcom/itau/empresas/api/model/ComprovanteServiceVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ComprovanteServiceVO;->getAutenticacaoDigital()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    return-object v5
.end method

.method private vinculaDevedorSolidario()Ljava/util/List;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
        }
    .end annotation

    .line 224
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 226
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705cd

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getDadosDevedorSolidarioNome()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 227
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705c5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CPF:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getDadosDevedorSolidarioCpf()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705c6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    .line 229
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getDadosDevedorSolidarioData()Ljava/lang/String;

    move-result-object v1

    .line 228
    invoke-static {v1}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 230
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705be

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->comprovante:Lcom/itau/empresas/api/model/ComprovanteServiceVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ComprovanteServiceVO;->getNomeCanal()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    return-object v3
.end method

.method private vinculaLis()Ljava/util/List;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
        }
    .end annotation

    .line 153
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 155
    .local v5, "list":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705d0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getProduto()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705d3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    .line 157
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getValorLimite()F

    move-result v3

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    .line 158
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getValorLimitePorcentagem()F

    move-result v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemSimples(F)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 156
    const v3, 0x7f0705cb

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705c8

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getDiaPagamentoEncargos()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705d7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    .line 161
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getValorTarifaContratacao()F

    move-result v3

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    .line 162
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getValorTarifaContratacaoPercentual()F

    move-result v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemSimples(F)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 160
    const v3, 0x7f0705cb

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    .line 163
    const v3, 0x7f0705d8

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 160
    invoke-static {v0, v1, v2}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperObservationImpl;->load(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705d2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    .line 165
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getValorIof()F

    move-result v3

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    .line 166
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getValorIofPercentual()F

    move-result v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemSimples(F)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 164
    const v3, 0x7f0705cb

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705d9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->comprovante:Lcom/itau/empresas/api/model/ComprovanteServiceVO;

    .line 168
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/ComprovanteServiceVO;->getValorTransacao()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    .line 169
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getValorTotalOperacaoPercentual()F

    move-result v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemSimples(F)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 167
    const v3, 0x7f0705cb

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705d1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    .line 171
    invoke-virtual {v2}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getTaxaJurosMes()F

    move-result v2

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getTaxaJurosAno()F

    move-result v3

    .line 170
    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemTaxaMesEAno(Landroid/content/Context;FF)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705bf

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    .line 173
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getCetMes()F

    move-result v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemSimples(F)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    .line 174
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getCetAno()F

    move-result v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemSimples(F)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 172
    const v3, 0x7f0705ca

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 175
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705cf

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getPeriodicidadeCapitalizacao()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705c7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->comprovante:Lcom/itau/empresas/api/model/ComprovanteServiceVO;

    .line 177
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ComprovanteServiceVO;->getDataEfetivaTransacao()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 176
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    return-object v5
.end method

.method private vinculaRecebiveis()Ljava/util/List;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
        }
    .end annotation

    .line 184
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->vinculaLis()Ljava/util/List;

    move-result-object v5

    .line 186
    .local v5, "item":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705d5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    .line 187
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getValorLimiteAval()F

    move-result v3

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    .line 188
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->getValorLimiteAvalPorcentagem()F

    move-result v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemSimples(F)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 186
    const v3, 0x7f0705cb

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v5, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 189
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705c4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v2, 0x7f0705cc

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v5, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 190
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705d6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v2, 0x7f0705cc

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v5, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 191
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v1, 0x7f0705c1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    const v2, 0x7f0705cc

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;->load(Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v5, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 193
    return-object v5
.end method


# virtual methods
.method public setData(Landroid/content/Context;Lcom/itau/empresas/CustomApplication;Ljava/lang/String;Lcom/itau/empresas/api/model/ComprovanteServiceVO;)V
    .registers 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "customApplication"    # Lcom/itau/empresas/CustomApplication;
    .param p3, "tipoComprovante"    # Ljava/lang/String;
    .param p4, "comprovante"    # Lcom/itau/empresas/api/model/ComprovanteServiceVO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;Lcom/itau/empresas/CustomApplication;Ljava/lang/String;Lcom/itau/empresas/api/model/ComprovanteServiceVO<Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;>;)V"
        }
    .end annotation

    .line 83
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->context:Landroid/content/Context;

    .line 84
    iput-object p2, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->customApplication:Lcom/itau/empresas/CustomApplication;

    .line 85
    iput-object p4, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->comprovante:Lcom/itau/empresas/api/model/ComprovanteServiceVO;

    .line 86
    iput-object p3, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->tipoComprovante:Ljava/lang/String;

    .line 87
    invoke-virtual {p4}, Lcom/itau/empresas/api/model/ComprovanteServiceVO;->getComprovantes()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    .line 89
    if-eqz p3, :cond_2a

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->respostaVO:Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;

    if-eqz v0, :cond_2a

    .line 91
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->constroiComrpovanteValores()V

    .line 92
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->constroiAssinaturaEletronica()V

    .line 93
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->constroiDevedorSolidario()V

    .line 94
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->constroiDadosControle()V

    .line 95
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->constroiDadosConta()V

    .line 97
    :cond_2a
    return-void
.end method

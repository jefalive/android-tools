.class public final Lorg/threeten/bp/LocalDate;
.super Lorg/threeten/bp/chrono/ChronoLocalDate;
.source "LocalDate.java"

# interfaces
.implements Lorg/threeten/bp/temporal/Temporal;
.implements Lorg/threeten/bp/temporal/TemporalAdjuster;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/threeten/bp/LocalDate$2;
    }
.end annotation


# static fields
.field public static final FROM:Lorg/threeten/bp/temporal/TemporalQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/threeten/bp/temporal/TemporalQuery<Lorg/threeten/bp/LocalDate;>;"
        }
    .end annotation
.end field

.field public static final MAX:Lorg/threeten/bp/LocalDate;

.field public static final MIN:Lorg/threeten/bp/LocalDate;

.field private static final serialVersionUID:J = 0x28d617b1d8f33f1eL


# instance fields
.field private final day:S

.field private final month:S

.field private final year:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 108
    const v0, -0x3b9ac9ff

    const/4 v1, 0x1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lorg/threeten/bp/LocalDate;->of(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    sput-object v0, Lorg/threeten/bp/LocalDate;->MIN:Lorg/threeten/bp/LocalDate;

    .line 113
    const v0, 0x3b9ac9ff

    const/16 v1, 0xc

    const/16 v2, 0x1f

    invoke-static {v0, v1, v2}, Lorg/threeten/bp/LocalDate;->of(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    sput-object v0, Lorg/threeten/bp/LocalDate;->MAX:Lorg/threeten/bp/LocalDate;

    .line 117
    new-instance v0, Lorg/threeten/bp/LocalDate$1;

    invoke-direct {v0}, Lorg/threeten/bp/LocalDate$1;-><init>()V

    sput-object v0, Lorg/threeten/bp/LocalDate;->FROM:Lorg/threeten/bp/temporal/TemporalQuery;

    return-void
.end method

.method private constructor <init>(III)V
    .registers 5
    .param p1, "year"    # I
    .param p2, "month"    # I
    .param p3, "dayOfMonth"    # I

    .line 421
    invoke-direct {p0}, Lorg/threeten/bp/chrono/ChronoLocalDate;-><init>()V

    .line 422
    iput p1, p0, Lorg/threeten/bp/LocalDate;->year:I

    .line 423
    int-to-short v0, p2

    iput-short v0, p0, Lorg/threeten/bp/LocalDate;->month:S

    .line 424
    int-to-short v0, p3

    iput-short v0, p0, Lorg/threeten/bp/LocalDate;->day:S

    .line 425
    return-void
.end method

.method private static create(ILorg/threeten/bp/Month;I)Lorg/threeten/bp/LocalDate;
    .registers 6
    .param p0, "year"    # I
    .param p1, "month"    # Lorg/threeten/bp/Month;
    .param p2, "dayOfMonth"    # I

    .line 381
    const/16 v0, 0x1c

    if-le p2, v0, :cond_61

    sget-object v0, Lorg/threeten/bp/chrono/IsoChronology;->INSTANCE:Lorg/threeten/bp/chrono/IsoChronology;

    int-to-long v1, p0

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/chrono/IsoChronology;->isLeapYear(J)Z

    move-result v0

    invoke-virtual {p1, v0}, Lorg/threeten/bp/Month;->length(Z)I

    move-result v0

    if-le p2, v0, :cond_61

    .line 382
    const/16 v0, 0x1d

    if-ne p2, v0, :cond_34

    .line 383
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid date \'February 29\' as \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' is not a leap year"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 385
    :cond_34
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid date \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/threeten/bp/Month;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 388
    :cond_61
    new-instance v0, Lorg/threeten/bp/LocalDate;

    invoke-virtual {p1}, Lorg/threeten/bp/Month;->getValue()I

    move-result v1

    invoke-direct {v0, p0, v1, p2}, Lorg/threeten/bp/LocalDate;-><init>(III)V

    return-object v0
.end method

.method public static from(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/LocalDate;
    .registers 5
    .param p0, "temporal"    # Lorg/threeten/bp/temporal/TemporalAccessor;

    .line 332
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->localDate()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    invoke-interface {p0, v0}, Lorg/threeten/bp/temporal/TemporalAccessor;->query(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lorg/threeten/bp/LocalDate;

    .line 333
    .local v3, "date":Lorg/threeten/bp/LocalDate;
    if-nez v3, :cond_38

    .line 334
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to obtain LocalDate from TemporalAccessor: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 337
    :cond_38
    return-object v3
.end method

.method private get0(Lorg/threeten/bp/temporal/TemporalField;)I
    .registers 5
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;

    .line 579
    sget-object v0, Lorg/threeten/bp/LocalDate$2;->$SwitchMap$org$threeten$bp$temporal$ChronoField:[I

    move-object v1, p1

    check-cast v1, Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v1}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_b0

    goto/16 :goto_97

    .line 580
    :pswitch_10
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getDayOfWeek()Lorg/threeten/bp/DayOfWeek;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/DayOfWeek;->getValue()I

    move-result v0

    return v0

    .line 581
    :pswitch_19
    iget-short v0, p0, Lorg/threeten/bp/LocalDate;->day:S

    add-int/lit8 v0, v0, -0x1

    rem-int/lit8 v0, v0, 0x7

    add-int/lit8 v0, v0, 0x1

    return v0

    .line 582
    :pswitch_22
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getDayOfYear()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    rem-int/lit8 v0, v0, 0x7

    add-int/lit8 v0, v0, 0x1

    return v0

    .line 583
    :pswitch_2d
    iget-short v0, p0, Lorg/threeten/bp/LocalDate;->day:S

    return v0

    .line 584
    :pswitch_30
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getDayOfYear()I

    move-result v0

    return v0

    .line 585
    :pswitch_35
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Field too large for an int: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 586
    :pswitch_4e
    iget-short v0, p0, Lorg/threeten/bp/LocalDate;->day:S

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v0, v0, 0x7

    add-int/lit8 v0, v0, 0x1

    return v0

    .line 587
    :pswitch_57
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getDayOfYear()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v0, v0, 0x7

    add-int/lit8 v0, v0, 0x1

    return v0

    .line 588
    :pswitch_62
    iget-short v0, p0, Lorg/threeten/bp/LocalDate;->month:S

    return v0

    .line 589
    :pswitch_65
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Field too large for an int: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 590
    :pswitch_7e
    iget v0, p0, Lorg/threeten/bp/LocalDate;->year:I

    const/4 v1, 0x1

    if-lt v0, v1, :cond_86

    iget v0, p0, Lorg/threeten/bp/LocalDate;->year:I

    goto :goto_8a

    :cond_86
    iget v0, p0, Lorg/threeten/bp/LocalDate;->year:I

    rsub-int/lit8 v0, v0, 0x1

    :goto_8a
    return v0

    .line 591
    :pswitch_8b
    iget v0, p0, Lorg/threeten/bp/LocalDate;->year:I

    return v0

    .line 592
    :pswitch_8e
    iget v0, p0, Lorg/threeten/bp/LocalDate;->year:I

    const/4 v1, 0x1

    if-lt v0, v1, :cond_95

    const/4 v0, 0x1

    goto :goto_96

    :cond_95
    const/4 v0, 0x0

    :goto_96
    return v0

    .line 594
    :goto_97
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_data_b0
    .packed-switch 0x1
        :pswitch_2d
        :pswitch_30
        :pswitch_4e
        :pswitch_7e
        :pswitch_10
        :pswitch_19
        :pswitch_22
        :pswitch_35
        :pswitch_57
        :pswitch_62
        :pswitch_65
        :pswitch_8b
        :pswitch_8e
    .end packed-switch
.end method

.method private getProlepticMonth()J
    .registers 5

    .line 598
    iget v0, p0, Lorg/threeten/bp/LocalDate;->year:I

    int-to-long v0, v0

    const-wide/16 v2, 0xc

    mul-long/2addr v0, v2

    iget-short v2, p0, Lorg/threeten/bp/LocalDate;->month:S

    add-int/lit8 v2, v2, -0x1

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public static now()Lorg/threeten/bp/LocalDate;
    .registers 1

    .line 165
    invoke-static {}, Lorg/threeten/bp/Clock;->systemDefaultZone()Lorg/threeten/bp/Clock;

    move-result-object v0

    invoke-static {v0}, Lorg/threeten/bp/LocalDate;->now(Lorg/threeten/bp/Clock;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public static now(Lorg/threeten/bp/Clock;)Lorg/threeten/bp/LocalDate;
    .registers 11
    .param p0, "clock"    # Lorg/threeten/bp/Clock;

    .line 195
    const-string v0, "clock"

    invoke-static {p0, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 196
    invoke-virtual {p0}, Lorg/threeten/bp/Clock;->instant()Lorg/threeten/bp/Instant;

    move-result-object v4

    .line 197
    .local v4, "now":Lorg/threeten/bp/Instant;
    invoke-virtual {p0}, Lorg/threeten/bp/Clock;->getZone()Lorg/threeten/bp/ZoneId;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/ZoneId;->getRules()Lorg/threeten/bp/zone/ZoneRules;

    move-result-object v0

    invoke-virtual {v0, v4}, Lorg/threeten/bp/zone/ZoneRules;->getOffset(Lorg/threeten/bp/Instant;)Lorg/threeten/bp/ZoneOffset;

    move-result-object v5

    .line 198
    .local v5, "offset":Lorg/threeten/bp/ZoneOffset;
    invoke-virtual {v4}, Lorg/threeten/bp/Instant;->getEpochSecond()J

    move-result-wide v0

    invoke-virtual {v5}, Lorg/threeten/bp/ZoneOffset;->getTotalSeconds()I

    move-result v2

    int-to-long v2, v2

    add-long v6, v0, v2

    .line 199
    .local v6, "epochSec":J
    const-wide/32 v0, 0x15180

    invoke-static {v6, v7, v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->floorDiv(JJ)J

    move-result-wide v8

    .line 200
    .local v8, "epochDay":J
    invoke-static {v8, v9}, Lorg/threeten/bp/LocalDate;->ofEpochDay(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public static of(III)Lorg/threeten/bp/LocalDate;
    .registers 6
    .param p0, "year"    # I
    .param p1, "month"    # I
    .param p2, "dayOfMonth"    # I

    .line 236
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->YEAR:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p0

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 237
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->MONTH_OF_YEAR:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 238
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->DAY_OF_MONTH:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p2

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 239
    invoke-static {p1}, Lorg/threeten/bp/Month;->of(I)Lorg/threeten/bp/Month;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lorg/threeten/bp/LocalDate;->create(ILorg/threeten/bp/Month;I)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public static of(ILorg/threeten/bp/Month;I)Lorg/threeten/bp/LocalDate;
    .registers 6
    .param p0, "year"    # I
    .param p1, "month"    # Lorg/threeten/bp/Month;
    .param p2, "dayOfMonth"    # I

    .line 217
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->YEAR:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p0

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 218
    const-string v0, "month"

    invoke-static {p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 219
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->DAY_OF_MONTH:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p2

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 220
    invoke-static {p0, p1, p2}, Lorg/threeten/bp/LocalDate;->create(ILorg/threeten/bp/Month;I)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public static ofEpochDay(J)Lorg/threeten/bp/LocalDate;
    .registers 19
    .param p0, "epochDay"    # J

    .line 282
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->EPOCH_DAY:Lorg/threeten/bp/temporal/ChronoField;

    move-wide/from16 v1, p0

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 283
    const-wide/32 v0, 0xafaa8

    add-long v4, p0, v0

    .line 285
    .local v4, "zeroDay":J
    const-wide/16 v0, 0x3c

    sub-long/2addr v4, v0

    .line 286
    const-wide/16 v6, 0x0

    .line 287
    .local v6, "adjust":J
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-gez v0, :cond_2c

    .line 289
    const-wide/16 v0, 0x1

    add-long/2addr v0, v4

    const-wide/32 v2, 0x23ab1

    div-long/2addr v0, v2

    const-wide/16 v2, 0x1

    sub-long v8, v0, v2

    .line 290
    .local v8, "adjustCycles":J
    const-wide/16 v0, 0x190

    mul-long v6, v8, v0

    .line 291
    neg-long v0, v8

    const-wide/32 v2, 0x23ab1

    mul-long/2addr v0, v2

    add-long/2addr v4, v0

    .line 293
    .end local v8    # "adjustCycles":J
    :cond_2c
    const-wide/16 v0, 0x190

    mul-long/2addr v0, v4

    const-wide/16 v2, 0x24f

    add-long/2addr v0, v2

    const-wide/32 v2, 0x23ab1

    div-long v8, v0, v2

    .line 294
    .local v8, "yearEst":J
    const-wide/16 v0, 0x16d

    mul-long/2addr v0, v8

    const-wide/16 v2, 0x4

    div-long v2, v8, v2

    add-long/2addr v0, v2

    const-wide/16 v2, 0x64

    div-long v2, v8, v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x190

    div-long v2, v8, v2

    add-long/2addr v0, v2

    sub-long v10, v4, v0

    .line 295
    .local v10, "doyEst":J
    const-wide/16 v0, 0x0

    cmp-long v0, v10, v0

    if-gez v0, :cond_68

    .line 297
    const-wide/16 v0, 0x1

    sub-long/2addr v8, v0

    .line 298
    const-wide/16 v0, 0x16d

    mul-long/2addr v0, v8

    const-wide/16 v2, 0x4

    div-long v2, v8, v2

    add-long/2addr v0, v2

    const-wide/16 v2, 0x64

    div-long v2, v8, v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x190

    div-long v2, v8, v2

    add-long/2addr v0, v2

    sub-long v10, v4, v0

    .line 300
    :cond_68
    add-long/2addr v8, v6

    .line 301
    long-to-int v12, v10

    .line 304
    .local v12, "marchDoy0":I
    mul-int/lit8 v0, v12, 0x5

    add-int/lit8 v0, v0, 0x2

    div-int/lit16 v13, v0, 0x99

    .line 305
    .local v13, "marchMonth0":I
    add-int/lit8 v0, v13, 0x2

    rem-int/lit8 v0, v0, 0xc

    add-int/lit8 v14, v0, 0x1

    .line 306
    .local v14, "month":I
    mul-int/lit16 v0, v13, 0x132

    add-int/lit8 v0, v0, 0x5

    div-int/lit8 v0, v0, 0xa

    sub-int v0, v12, v0

    add-int/lit8 v15, v0, 0x1

    .line 307
    .local v15, "dom":I
    div-int/lit8 v0, v13, 0xa

    int-to-long v0, v0

    add-long/2addr v8, v0

    .line 310
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->YEAR:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v0, v8, v9}, Lorg/threeten/bp/temporal/ChronoField;->checkValidIntValue(J)I

    move-result v16

    .line 311
    .local v16, "year":I
    new-instance v0, Lorg/threeten/bp/LocalDate;

    move/from16 v1, v16

    invoke-direct {v0, v1, v14, v15}, Lorg/threeten/bp/LocalDate;-><init>(III)V

    return-object v0
.end method

.method public static ofYearDay(II)Lorg/threeten/bp/LocalDate;
    .registers 9
    .param p0, "year"    # I
    .param p1, "dayOfYear"    # I

    .line 255
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->YEAR:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p0

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 256
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->DAY_OF_YEAR:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 257
    sget-object v0, Lorg/threeten/bp/chrono/IsoChronology;->INSTANCE:Lorg/threeten/bp/chrono/IsoChronology;

    int-to-long v1, p0

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/chrono/IsoChronology;->isLeapYear(J)Z

    move-result v3

    .line 258
    .local v3, "leap":Z
    const/16 v0, 0x16e

    if-ne p1, v0, :cond_38

    if-nez v3, :cond_38

    .line 259
    new-instance v0, Lorg/threeten/bp/DateTimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid date \'DayOfYear 366\' as \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' is not a leap year"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/DateTimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 261
    :cond_38
    add-int/lit8 v0, p1, -0x1

    div-int/lit8 v0, v0, 0x1f

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lorg/threeten/bp/Month;->of(I)Lorg/threeten/bp/Month;

    move-result-object v4

    .line 262
    .local v4, "moy":Lorg/threeten/bp/Month;
    invoke-virtual {v4, v3}, Lorg/threeten/bp/Month;->firstDayOfYear(Z)I

    move-result v0

    invoke-virtual {v4, v3}, Lorg/threeten/bp/Month;->length(Z)I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v5, v0, -0x1

    .line 263
    .local v5, "monthEnd":I
    if-le p1, v5, :cond_55

    .line 264
    const-wide/16 v0, 0x1

    invoke-virtual {v4, v0, v1}, Lorg/threeten/bp/Month;->plus(J)Lorg/threeten/bp/Month;

    move-result-object v4

    .line 266
    :cond_55
    invoke-virtual {v4, v3}, Lorg/threeten/bp/Month;->firstDayOfYear(Z)I

    move-result v0

    sub-int v0, p1, v0

    add-int/lit8 v6, v0, 0x1

    .line 267
    .local v6, "dom":I
    invoke-static {p0, v4, v6}, Lorg/threeten/bp/LocalDate;->create(ILorg/threeten/bp/Month;I)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public static parse(Ljava/lang/CharSequence;)Lorg/threeten/bp/LocalDate;
    .registers 2
    .param p0, "text"    # Ljava/lang/CharSequence;

    .line 352
    sget-object v0, Lorg/threeten/bp/format/DateTimeFormatter;->ISO_LOCAL_DATE:Lorg/threeten/bp/format/DateTimeFormatter;

    invoke-static {p0, v0}, Lorg/threeten/bp/LocalDate;->parse(Ljava/lang/CharSequence;Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public static parse(Ljava/lang/CharSequence;Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/LocalDate;
    .registers 3
    .param p0, "text"    # Ljava/lang/CharSequence;
    .param p1, "formatter"    # Lorg/threeten/bp/format/DateTimeFormatter;

    .line 366
    const-string v0, "formatter"

    invoke-static {p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 367
    sget-object v0, Lorg/threeten/bp/LocalDate;->FROM:Lorg/threeten/bp/temporal/TemporalQuery;

    invoke-virtual {p1, p0, v0}, Lorg/threeten/bp/format/DateTimeFormatter;->parse(Ljava/lang/CharSequence;Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method static readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/LocalDate;
    .registers 5
    .param p0, "in"    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1883
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v1

    .line 1884
    .local v1, "year":I
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v2

    .line 1885
    .local v2, "month":I
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v3

    .line 1886
    .local v3, "dayOfMonth":I
    invoke-static {v1, v2, v3}, Lorg/threeten/bp/LocalDate;->of(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method private readResolve()Ljava/lang/Object;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .line 1873
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Deserialization via serialization delegate"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static resolvePreviousValid(III)Lorg/threeten/bp/LocalDate;
    .registers 6
    .param p0, "year"    # I
    .param p1, "month"    # I
    .param p2, "day"    # I

    .line 400
    packed-switch p1, :pswitch_data_22

    goto :goto_1d

    .line 402
    :pswitch_4
    sget-object v0, Lorg/threeten/bp/chrono/IsoChronology;->INSTANCE:Lorg/threeten/bp/chrono/IsoChronology;

    int-to-long v1, p0

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/chrono/IsoChronology;->isLeapYear(J)Z

    move-result v0

    if-eqz v0, :cond_10

    const/16 v0, 0x1d

    goto :goto_12

    :cond_10
    const/16 v0, 0x1c

    :goto_12
    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result p2

    .line 403
    goto :goto_1d

    .line 408
    :pswitch_17
    const/16 v0, 0x1e

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result p2

    .line 411
    :goto_1d
    :pswitch_1d
    invoke-static {p0, p1, p2}, Lorg/threeten/bp/LocalDate;->of(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0

    :pswitch_data_22
    .packed-switch 0x2
        :pswitch_4
        :pswitch_1d
        :pswitch_17
        :pswitch_1d
        :pswitch_17
        :pswitch_1d
        :pswitch_1d
        :pswitch_17
        :pswitch_1d
        :pswitch_17
    .end packed-switch
.end method

.method private writeReplace()Ljava/lang/Object;
    .registers 3

    .line 1864
    new-instance v0, Lorg/threeten/bp/Ser;

    const/4 v1, 0x3

    invoke-direct {v0, v1, p0}, Lorg/threeten/bp/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method public adjustInto(Lorg/threeten/bp/temporal/Temporal;)Lorg/threeten/bp/temporal/Temporal;
    .registers 3
    .param p1, "temporal"    # Lorg/threeten/bp/temporal/Temporal;

    .line 1369
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoLocalDate;->adjustInto(Lorg/threeten/bp/temporal/Temporal;)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    return-object v0
.end method

.method public atStartOfDay()Lorg/threeten/bp/LocalDateTime;
    .registers 2

    .line 1590
    sget-object v0, Lorg/threeten/bp/LocalTime;->MIDNIGHT:Lorg/threeten/bp/LocalTime;

    invoke-static {p0, v0}, Lorg/threeten/bp/LocalDateTime;->of(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    return-object v0
.end method

.method public atTime(Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/LocalDateTime;
    .registers 3
    .param p1, "time"    # Lorg/threeten/bp/LocalTime;

    .line 1508
    invoke-static {p0, p1}, Lorg/threeten/bp/LocalDateTime;->of(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic atTime(Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/chrono/ChronoLocalDateTime;
    .registers 3
    .param p1, "x0"    # Lorg/threeten/bp/LocalTime;

    .line 100
    invoke-virtual {p0, p1}, Lorg/threeten/bp/LocalDate;->atTime(Lorg/threeten/bp/LocalTime;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .param p1, "x0"    # Ljava/lang/Object;

    .line 100
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/chrono/ChronoLocalDate;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->compareTo(Lorg/threeten/bp/chrono/ChronoLocalDate;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/threeten/bp/chrono/ChronoLocalDate;)I
    .registers 3
    .param p1, "other"    # Lorg/threeten/bp/chrono/ChronoLocalDate;

    .line 1672
    instance-of v0, p1, Lorg/threeten/bp/LocalDate;

    if-eqz v0, :cond_c

    .line 1673
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/LocalDate;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->compareTo0(Lorg/threeten/bp/LocalDate;)I

    move-result v0

    return v0

    .line 1675
    :cond_c
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoLocalDate;->compareTo(Lorg/threeten/bp/chrono/ChronoLocalDate;)I

    move-result v0

    return v0
.end method

.method compareTo0(Lorg/threeten/bp/LocalDate;)I
    .registers 5
    .param p1, "otherDate"    # Lorg/threeten/bp/LocalDate;

    .line 1679
    iget v0, p0, Lorg/threeten/bp/LocalDate;->year:I

    iget v1, p1, Lorg/threeten/bp/LocalDate;->year:I

    sub-int v2, v0, v1

    .line 1680
    .local v2, "cmp":I
    if-nez v2, :cond_16

    .line 1681
    iget-short v0, p0, Lorg/threeten/bp/LocalDate;->month:S

    iget-short v1, p1, Lorg/threeten/bp/LocalDate;->month:S

    sub-int v2, v0, v1

    .line 1682
    if-nez v2, :cond_16

    .line 1683
    iget-short v0, p0, Lorg/threeten/bp/LocalDate;->day:S

    iget-short v1, p1, Lorg/threeten/bp/LocalDate;->day:S

    sub-int v2, v0, v1

    .line 1686
    :cond_16
    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .param p1, "obj"    # Ljava/lang/Object;

    .line 1791
    if-ne p0, p1, :cond_4

    .line 1792
    const/4 v0, 0x1

    return v0

    .line 1794
    :cond_4
    instance-of v0, p1, Lorg/threeten/bp/LocalDate;

    if-eqz v0, :cond_15

    .line 1795
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/LocalDate;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->compareTo0(Lorg/threeten/bp/LocalDate;)I

    move-result v0

    if-nez v0, :cond_13

    const/4 v0, 0x1

    goto :goto_14

    :cond_13
    const/4 v0, 0x0

    :goto_14
    return v0

    .line 1797
    :cond_15
    const/4 v0, 0x0

    return v0
.end method

.method public format(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;
    .registers 3
    .param p1, "formatter"    # Lorg/threeten/bp/format/DateTimeFormatter;

    .line 1859
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoLocalDate;->format(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get(Lorg/threeten/bp/temporal/TemporalField;)I
    .registers 3
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;

    .line 536
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_9

    .line 537
    invoke-direct {p0, p1}, Lorg/threeten/bp/LocalDate;->get0(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    return v0

    .line 539
    :cond_9
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoLocalDate;->get(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic getChronology()Lorg/threeten/bp/chrono/Chronology;
    .registers 2

    .line 100
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getChronology()Lorg/threeten/bp/chrono/IsoChronology;

    move-result-object v0

    return-object v0
.end method

.method public getChronology()Lorg/threeten/bp/chrono/IsoChronology;
    .registers 2

    .line 614
    sget-object v0, Lorg/threeten/bp/chrono/IsoChronology;->INSTANCE:Lorg/threeten/bp/chrono/IsoChronology;

    return-object v0
.end method

.method public getDayOfMonth()I
    .registers 2

    .line 691
    iget-short v0, p0, Lorg/threeten/bp/LocalDate;->day:S

    return v0
.end method

.method public getDayOfWeek()Lorg/threeten/bp/DayOfWeek;
    .registers 6

    .line 719
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->toEpochDay()J

    move-result-wide v0

    const-wide/16 v2, 0x3

    add-long/2addr v0, v2

    const/4 v2, 0x7

    invoke-static {v0, v1, v2}, Lorg/threeten/bp/jdk8/Jdk8Methods;->floorMod(JI)I

    move-result v4

    .line 720
    .local v4, "dow0":I
    add-int/lit8 v0, v4, 0x1

    invoke-static {v0}, Lorg/threeten/bp/DayOfWeek;->of(I)Lorg/threeten/bp/DayOfWeek;

    move-result-object v0

    return-object v0
.end method

.method public getDayOfYear()I
    .registers 3

    .line 702
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getMonth()Lorg/threeten/bp/Month;

    move-result-object v0

    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->isLeapYear()Z

    move-result v1

    invoke-virtual {v0, v1}, Lorg/threeten/bp/Month;->firstDayOfYear(Z)I

    move-result v0

    iget-short v1, p0, Lorg/threeten/bp/LocalDate;->day:S

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getEra()Lorg/threeten/bp/chrono/Era;
    .registers 2

    .line 637
    invoke-super {p0}, Lorg/threeten/bp/chrono/ChronoLocalDate;->getEra()Lorg/threeten/bp/chrono/Era;

    move-result-object v0

    return-object v0
.end method

.method public getLong(Lorg/threeten/bp/temporal/TemporalField;)J
    .registers 4
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;

    .line 566
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_1c

    .line 567
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->EPOCH_DAY:Lorg/threeten/bp/temporal/ChronoField;

    if-ne p1, v0, :cond_d

    .line 568
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->toEpochDay()J

    move-result-wide v0

    return-wide v0

    .line 570
    :cond_d
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->PROLEPTIC_MONTH:Lorg/threeten/bp/temporal/ChronoField;

    if-ne p1, v0, :cond_16

    .line 571
    invoke-direct {p0}, Lorg/threeten/bp/LocalDate;->getProlepticMonth()J

    move-result-wide v0

    return-wide v0

    .line 573
    :cond_16
    invoke-direct {p0, p1}, Lorg/threeten/bp/LocalDate;->get0(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v0

    int-to-long v0, v0

    return-wide v0

    .line 575
    :cond_1c
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalField;->getFrom(Lorg/threeten/bp/temporal/TemporalAccessor;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getMonth()Lorg/threeten/bp/Month;
    .registers 2

    .line 680
    iget-short v0, p0, Lorg/threeten/bp/LocalDate;->month:S

    invoke-static {v0}, Lorg/threeten/bp/Month;->of(I)Lorg/threeten/bp/Month;

    move-result-object v0

    return-object v0
.end method

.method public getMonthValue()I
    .registers 2

    .line 665
    iget-short v0, p0, Lorg/threeten/bp/LocalDate;->month:S

    return v0
.end method

.method public getYear()I
    .registers 2

    .line 651
    iget v0, p0, Lorg/threeten/bp/LocalDate;->year:I

    return v0
.end method

.method public hashCode()I
    .registers 7

    .line 1807
    iget v3, p0, Lorg/threeten/bp/LocalDate;->year:I

    .line 1808
    .local v3, "yearValue":I
    iget-short v4, p0, Lorg/threeten/bp/LocalDate;->month:S

    .line 1809
    .local v4, "monthValue":I
    iget-short v5, p0, Lorg/threeten/bp/LocalDate;->day:S

    .line 1810
    .local v5, "dayValue":I
    and-int/lit16 v0, v3, -0x800

    shl-int/lit8 v1, v3, 0xb

    shl-int/lit8 v2, v4, 0x6

    add-int/2addr v1, v2

    add-int/2addr v1, v5

    xor-int/2addr v0, v1

    return v0
.end method

.method public isAfter(Lorg/threeten/bp/chrono/ChronoLocalDate;)Z
    .registers 3
    .param p1, "other"    # Lorg/threeten/bp/chrono/ChronoLocalDate;

    .line 1712
    instance-of v0, p1, Lorg/threeten/bp/LocalDate;

    if-eqz v0, :cond_11

    .line 1713
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/LocalDate;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->compareTo0(Lorg/threeten/bp/LocalDate;)I

    move-result v0

    if-lez v0, :cond_f

    const/4 v0, 0x1

    goto :goto_10

    :cond_f
    const/4 v0, 0x0

    :goto_10
    return v0

    .line 1715
    :cond_11
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoLocalDate;->isAfter(Lorg/threeten/bp/chrono/ChronoLocalDate;)Z

    move-result v0

    return v0
.end method

.method public isBefore(Lorg/threeten/bp/chrono/ChronoLocalDate;)Z
    .registers 3
    .param p1, "other"    # Lorg/threeten/bp/chrono/ChronoLocalDate;

    .line 1741
    instance-of v0, p1, Lorg/threeten/bp/LocalDate;

    if-eqz v0, :cond_11

    .line 1742
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/LocalDate;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->compareTo0(Lorg/threeten/bp/LocalDate;)I

    move-result v0

    if-gez v0, :cond_f

    const/4 v0, 0x1

    goto :goto_10

    :cond_f
    const/4 v0, 0x0

    :goto_10
    return v0

    .line 1744
    :cond_11
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoLocalDate;->isBefore(Lorg/threeten/bp/chrono/ChronoLocalDate;)Z

    move-result v0

    return v0
.end method

.method public isLeapYear()Z
    .registers 4

    .line 744
    sget-object v0, Lorg/threeten/bp/chrono/IsoChronology;->INSTANCE:Lorg/threeten/bp/chrono/IsoChronology;

    iget v1, p0, Lorg/threeten/bp/LocalDate;->year:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/chrono/IsoChronology;->isLeapYear(J)Z

    move-result v0

    return v0
.end method

.method public isSupported(Lorg/threeten/bp/temporal/TemporalField;)Z
    .registers 3
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;

    .line 466
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoLocalDate;->isSupported(Lorg/threeten/bp/temporal/TemporalField;)Z

    move-result v0

    return v0
.end method

.method public lengthOfMonth()I
    .registers 2

    .line 757
    iget-short v0, p0, Lorg/threeten/bp/LocalDate;->month:S

    packed-switch v0, :pswitch_data_18

    goto :goto_15

    .line 759
    :pswitch_6
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->isLeapYear()Z

    move-result v0

    if-eqz v0, :cond_f

    const/16 v0, 0x1d

    goto :goto_11

    :cond_f
    const/16 v0, 0x1c

    :goto_11
    return v0

    .line 764
    :pswitch_12
    const/16 v0, 0x1e

    return v0

    .line 766
    :goto_15
    :pswitch_15
    const/16 v0, 0x1f

    return v0

    :pswitch_data_18
    .packed-switch 0x2
        :pswitch_6
        :pswitch_15
        :pswitch_12
        :pswitch_15
        :pswitch_12
        :pswitch_15
        :pswitch_15
        :pswitch_12
        :pswitch_15
        :pswitch_12
    .end packed-switch
.end method

.method public lengthOfYear()I
    .registers 2

    .line 779
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->isLeapYear()Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0x16e

    goto :goto_b

    :cond_9
    const/16 v0, 0x16d

    :goto_b
    return v0
.end method

.method public minus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/LocalDate;
    .registers 7
    .param p1, "amountToSubtract"    # J
    .param p3, "unit"    # Lorg/threeten/bp/temporal/TemporalUnit;

    .line 1225
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p1, v0

    if-nez v0, :cond_16

    const-wide v0, 0x7fffffffffffffffL

    invoke-virtual {p0, v0, v1, p3}, Lorg/threeten/bp/LocalDate;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2, p3}, Lorg/threeten/bp/LocalDate;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    goto :goto_1b

    :cond_16
    neg-long v0, p1

    invoke-virtual {p0, v0, v1, p3}, Lorg/threeten/bp/LocalDate;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    :goto_1b
    return-object v0
.end method

.method public bridge synthetic minus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .registers 5
    .param p1, "x0"    # J
    .param p3, "x1"    # Lorg/threeten/bp/temporal/TemporalUnit;

    .line 100
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/LocalDate;->minus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic minus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;
    .registers 5
    .param p1, "x0"    # J
    .param p3, "x1"    # Lorg/threeten/bp/temporal/TemporalUnit;

    .line 100
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/LocalDate;->minus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public minusDays(J)Lorg/threeten/bp/LocalDate;
    .registers 6
    .param p1, "daysToSubtract"    # J

    .line 1312
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p1, v0

    if-nez v0, :cond_16

    const-wide v0, 0x7fffffffffffffffL

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalDate;->plusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/LocalDate;->plusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    goto :goto_1b

    :cond_16
    neg-long v0, p1

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalDate;->plusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    :goto_1b
    return-object v0
.end method

.method public minusYears(J)Lorg/threeten/bp/LocalDate;
    .registers 6
    .param p1, "yearsToSubtract"    # J

    .line 1250
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p1, v0

    if-nez v0, :cond_16

    const-wide v0, 0x7fffffffffffffffL

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalDate;->plusYears(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/LocalDate;->plusYears(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    goto :goto_1b

    :cond_16
    neg-long v0, p1

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalDate;->plusYears(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    :goto_1b
    return-object v0
.end method

.method public plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/LocalDate;
    .registers 8
    .param p1, "amountToAdd"    # J
    .param p3, "unit"    # Lorg/threeten/bp/temporal/TemporalUnit;

    .line 1067
    instance-of v0, p3, Lorg/threeten/bp/temporal/ChronoUnit;

    if-eqz v0, :cond_72

    .line 1068
    move-object v3, p3

    check-cast v3, Lorg/threeten/bp/temporal/ChronoUnit;

    .line 1069
    .local v3, "f":Lorg/threeten/bp/temporal/ChronoUnit;
    sget-object v0, Lorg/threeten/bp/LocalDate$2;->$SwitchMap$org$threeten$bp$temporal$ChronoUnit:[I

    invoke-virtual {v3}, Lorg/threeten/bp/temporal/ChronoUnit;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_7a

    goto :goto_59

    .line 1070
    :pswitch_13
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/LocalDate;->plusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0

    .line 1071
    :pswitch_18
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/LocalDate;->plusWeeks(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0

    .line 1072
    :pswitch_1d
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/LocalDate;->plusMonths(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0

    .line 1073
    :pswitch_22
    invoke-virtual {p0, p1, p2}, Lorg/threeten/bp/LocalDate;->plusYears(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0

    .line 1074
    :pswitch_27
    const/16 v0, 0xa

    invoke-static {p1, p2, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->safeMultiply(JI)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalDate;->plusYears(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0

    .line 1075
    :pswitch_32
    const/16 v0, 0x64

    invoke-static {p1, p2, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->safeMultiply(JI)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalDate;->plusYears(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0

    .line 1076
    :pswitch_3d
    const/16 v0, 0x3e8

    invoke-static {p1, p2, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->safeMultiply(JI)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalDate;->plusYears(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0

    .line 1077
    :pswitch_48
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->ERA:Lorg/threeten/bp/temporal/ChronoField;

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->ERA:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v1}, Lorg/threeten/bp/LocalDate;->getLong(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v1

    invoke-static {v1, v2, p1, p2}, Lorg/threeten/bp/jdk8/Jdk8Methods;->safeAdd(JJ)J

    move-result-wide v1

    invoke-virtual {p0, v0, v1, v2}, Lorg/threeten/bp/LocalDate;->with(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0

    .line 1079
    :goto_59
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported unit: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1081
    .end local v3    # "f":Lorg/threeten/bp/temporal/ChronoUnit;
    :cond_72
    invoke-interface {p3, p0, p1, p2}, Lorg/threeten/bp/temporal/TemporalUnit;->addTo(Lorg/threeten/bp/temporal/Temporal;J)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/LocalDate;

    return-object v0

    nop

    :pswitch_data_7a
    .packed-switch 0x1
        :pswitch_13
        :pswitch_18
        :pswitch_1d
        :pswitch_22
        :pswitch_27
        :pswitch_32
        :pswitch_3d
        :pswitch_48
    .end packed-switch
.end method

.method public plus(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/LocalDate;
    .registers 3
    .param p1, "amount"    # Lorg/threeten/bp/temporal/TemporalAmount;

    .line 1047
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalAmount;->addTo(Lorg/threeten/bp/temporal/Temporal;)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public bridge synthetic plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .registers 5
    .param p1, "x0"    # J
    .param p3, "x1"    # Lorg/threeten/bp/temporal/TemporalUnit;

    .line 100
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/LocalDate;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic plus(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .registers 3
    .param p1, "x0"    # Lorg/threeten/bp/temporal/TemporalAmount;

    .line 100
    invoke-virtual {p0, p1}, Lorg/threeten/bp/LocalDate;->plus(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;
    .registers 5
    .param p1, "x0"    # J
    .param p3, "x1"    # Lorg/threeten/bp/temporal/TemporalUnit;

    .line 100
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/LocalDate;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic plus(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/temporal/Temporal;
    .registers 3
    .param p1, "x0"    # Lorg/threeten/bp/temporal/TemporalAmount;

    .line 100
    invoke-virtual {p0, p1}, Lorg/threeten/bp/LocalDate;->plus(Lorg/threeten/bp/temporal/TemporalAmount;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public plusDays(J)Lorg/threeten/bp/LocalDate;
    .registers 7
    .param p1, "daysToAdd"    # J

    .line 1179
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_7

    .line 1180
    return-object p0

    .line 1182
    :cond_7
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->toEpochDay()J

    move-result-wide v0

    invoke-static {v0, v1, p1, p2}, Lorg/threeten/bp/jdk8/Jdk8Methods;->safeAdd(JJ)J

    move-result-wide v2

    .line 1183
    .local v2, "mjDay":J
    invoke-static {v2, v3}, Lorg/threeten/bp/LocalDate;->ofEpochDay(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public plusMonths(J)Lorg/threeten/bp/LocalDate;
    .registers 13
    .param p1, "monthsToAdd"    # J

    .line 1134
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_7

    .line 1135
    return-object p0

    .line 1137
    :cond_7
    iget v0, p0, Lorg/threeten/bp/LocalDate;->year:I

    int-to-long v0, v0

    const-wide/16 v2, 0xc

    mul-long/2addr v0, v2

    iget-short v2, p0, Lorg/threeten/bp/LocalDate;->month:S

    add-int/lit8 v2, v2, -0x1

    int-to-long v2, v2

    add-long v4, v0, v2

    .line 1138
    .local v4, "monthCount":J
    add-long v6, v4, p1

    .line 1139
    .local v6, "calcMonths":J
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->YEAR:Lorg/threeten/bp/temporal/ChronoField;

    const-wide/16 v1, 0xc

    invoke-static {v6, v7, v1, v2}, Lorg/threeten/bp/jdk8/Jdk8Methods;->floorDiv(JJ)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidIntValue(J)I

    move-result v8

    .line 1140
    .local v8, "newYear":I
    const/16 v0, 0xc

    invoke-static {v6, v7, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->floorMod(JI)I

    move-result v0

    add-int/lit8 v9, v0, 0x1

    .line 1141
    .local v9, "newMonth":I
    iget-short v0, p0, Lorg/threeten/bp/LocalDate;->day:S

    invoke-static {v8, v9, v0}, Lorg/threeten/bp/LocalDate;->resolvePreviousValid(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public plusWeeks(J)Lorg/threeten/bp/LocalDate;
    .registers 5
    .param p1, "weeksToAdd"    # J

    .line 1160
    const/4 v0, 0x7

    invoke-static {p1, p2, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->safeMultiply(JI)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalDate;->plusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public plusYears(J)Lorg/threeten/bp/LocalDate;
    .registers 7
    .param p1, "yearsToAdd"    # J

    .line 1106
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_7

    .line 1107
    return-object p0

    .line 1109
    :cond_7
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->YEAR:Lorg/threeten/bp/temporal/ChronoField;

    iget v1, p0, Lorg/threeten/bp/LocalDate;->year:I

    int-to-long v1, v1

    add-long/2addr v1, p1

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidIntValue(J)I

    move-result v3

    .line 1110
    .local v3, "newYear":I
    iget-short v0, p0, Lorg/threeten/bp/LocalDate;->month:S

    iget-short v1, p0, Lorg/threeten/bp/LocalDate;->day:S

    invoke-static {v3, v0, v1}, Lorg/threeten/bp/LocalDate;->resolvePreviousValid(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public query(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;
    .registers 3
    .param p1, "query"    # Lorg/threeten/bp/temporal/TemporalQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:Ljava/lang/Object;>(Lorg/threeten/bp/temporal/TemporalQuery<TR;>;)TR;"
        }
    .end annotation

    .line 1337
    invoke-static {}, Lorg/threeten/bp/temporal/TemporalQueries;->localDate()Lorg/threeten/bp/temporal/TemporalQuery;

    move-result-object v0

    if-ne p1, v0, :cond_7

    .line 1338
    return-object p0

    .line 1340
    :cond_7
    invoke-super {p0, p1}, Lorg/threeten/bp/chrono/ChronoLocalDate;->query(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public range(Lorg/threeten/bp/temporal/TemporalField;)Lorg/threeten/bp/temporal/ValueRange;
    .registers 7
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;

    .line 493
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_83

    .line 494
    move-object v4, p1

    check-cast v4, Lorg/threeten/bp/temporal/ChronoField;

    .line 495
    .local v4, "f":Lorg/threeten/bp/temporal/ChronoField;
    invoke-virtual {v4}, Lorg/threeten/bp/temporal/ChronoField;->isDateBased()Z

    move-result v0

    if-eqz v0, :cond_6a

    .line 496
    sget-object v0, Lorg/threeten/bp/LocalDate$2;->$SwitchMap$org$threeten$bp$temporal$ChronoField:[I

    invoke-virtual {v4}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_88

    goto :goto_65

    .line 497
    :pswitch_19
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->lengthOfMonth()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x1

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/temporal/ValueRange;->of(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    return-object v0

    .line 498
    :pswitch_25
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->lengthOfYear()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x1

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/temporal/ValueRange;->of(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    return-object v0

    .line 499
    :pswitch_31
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getMonth()Lorg/threeten/bp/Month;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/Month;->FEBRUARY:Lorg/threeten/bp/Month;

    if-ne v0, v1, :cond_42

    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->isLeapYear()Z

    move-result v0

    if-nez v0, :cond_42

    const-wide/16 v0, 0x4

    goto :goto_44

    :cond_42
    const-wide/16 v0, 0x5

    :goto_44
    const-wide/16 v2, 0x1

    invoke-static {v2, v3, v0, v1}, Lorg/threeten/bp/temporal/ValueRange;->of(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    return-object v0

    .line 501
    :pswitch_4b
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v0

    if-gtz v0, :cond_5b

    const-wide/16 v0, 0x1

    const-wide/32 v2, 0x3b9aca00

    invoke-static {v0, v1, v2, v3}, Lorg/threeten/bp/temporal/ValueRange;->of(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    goto :goto_64

    :cond_5b
    const-wide/16 v0, 0x1

    const-wide/32 v2, 0x3b9ac9ff

    invoke-static {v0, v1, v2, v3}, Lorg/threeten/bp/temporal/ValueRange;->of(JJ)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    :goto_64
    return-object v0

    .line 503
    :goto_65
    invoke-interface {p1}, Lorg/threeten/bp/temporal/TemporalField;->range()Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    return-object v0

    .line 505
    :cond_6a
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 507
    .end local v4    # "f":Lorg/threeten/bp/temporal/ChronoField;
    :cond_83
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalField;->rangeRefinedBy(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/temporal/ValueRange;

    move-result-object v0

    return-object v0

    :pswitch_data_88
    .packed-switch 0x1
        :pswitch_19
        :pswitch_25
        :pswitch_31
        :pswitch_4b
    .end packed-switch
.end method

.method public toEpochDay()J
    .registers 13

    .line 1635
    iget v0, p0, Lorg/threeten/bp/LocalDate;->year:I

    int-to-long v6, v0

    .line 1636
    .local v6, "y":J
    iget-short v0, p0, Lorg/threeten/bp/LocalDate;->month:S

    int-to-long v8, v0

    .line 1637
    .local v8, "m":J
    const-wide/16 v10, 0x0

    .line 1638
    .local v10, "total":J
    const-wide/16 v0, 0x16d

    mul-long/2addr v0, v6

    add-long/2addr v10, v0

    .line 1639
    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-ltz v0, :cond_28

    .line 1640
    const-wide/16 v0, 0x3

    add-long/2addr v0, v6

    const-wide/16 v2, 0x4

    div-long/2addr v0, v2

    const-wide/16 v2, 0x63

    add-long/2addr v2, v6

    const-wide/16 v4, 0x64

    div-long/2addr v2, v4

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x18f

    add-long/2addr v2, v6

    const-wide/16 v4, 0x190

    div-long/2addr v2, v4

    add-long/2addr v0, v2

    add-long/2addr v10, v0

    goto :goto_37

    .line 1642
    :cond_28
    const-wide/16 v0, -0x4

    div-long v0, v6, v0

    const-wide/16 v2, -0x64

    div-long v2, v6, v2

    sub-long/2addr v0, v2

    const-wide/16 v2, -0x190

    div-long v2, v6, v2

    add-long/2addr v0, v2

    sub-long/2addr v10, v0

    .line 1644
    :goto_37
    const-wide/16 v0, 0x16f

    mul-long/2addr v0, v8

    const-wide/16 v2, 0x16a

    sub-long/2addr v0, v2

    const-wide/16 v2, 0xc

    div-long/2addr v0, v2

    add-long/2addr v10, v0

    .line 1645
    iget-short v0, p0, Lorg/threeten/bp/LocalDate;->day:S

    add-int/lit8 v0, v0, -0x1

    int-to-long v0, v0

    add-long/2addr v10, v0

    .line 1646
    const-wide/16 v0, 0x2

    cmp-long v0, v8, v0

    if-lez v0, :cond_59

    .line 1647
    const-wide/16 v0, 0x1

    sub-long/2addr v10, v0

    .line 1648
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->isLeapYear()Z

    move-result v0

    if-nez v0, :cond_59

    .line 1649
    const-wide/16 v0, 0x1

    sub-long/2addr v10, v0

    .line 1652
    :cond_59
    const-wide/32 v0, 0xafaa8

    sub-long v0, v10, v0

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .registers 8

    .line 1823
    iget v2, p0, Lorg/threeten/bp/LocalDate;->year:I

    .line 1824
    .local v2, "yearValue":I
    iget-short v3, p0, Lorg/threeten/bp/LocalDate;->month:S

    .line 1825
    .local v3, "monthValue":I
    iget-short v4, p0, Lorg/threeten/bp/LocalDate;->day:S

    .line 1826
    .local v4, "dayValue":I
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v5

    .line 1827
    .local v5, "absYear":I
    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v0, 0xa

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1828
    .local v6, "buf":Ljava/lang/StringBuilder;
    const/16 v0, 0x3e8

    if-ge v5, v0, :cond_2d

    .line 1829
    if-gez v2, :cond_22

    .line 1830
    add-int/lit16 v0, v2, -0x2710

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    goto :goto_39

    .line 1832
    :cond_22
    add-int/lit16 v0, v2, 0x2710

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    goto :goto_39

    .line 1835
    :cond_2d
    const/16 v0, 0x270f

    if-le v2, v0, :cond_36

    .line 1836
    const/16 v0, 0x2b

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1838
    :cond_36
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1840
    :goto_39
    const/16 v0, 0xa

    if-ge v3, v0, :cond_40

    const-string v0, "-0"

    goto :goto_42

    :cond_40
    const-string v0, "-"

    :goto_42
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0xa

    if-ge v4, v1, :cond_51

    const-string v1, "-0"

    goto :goto_53

    :cond_51
    const-string v1, "-"

    :goto_53
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public until(Lorg/threeten/bp/chrono/ChronoLocalDate;)Lorg/threeten/bp/Period;
    .registers 13
    .param p1, "endDate"    # Lorg/threeten/bp/chrono/ChronoLocalDate;

    .line 1480
    invoke-static {p1}, Lorg/threeten/bp/LocalDate;->from(Lorg/threeten/bp/temporal/TemporalAccessor;)Lorg/threeten/bp/LocalDate;

    move-result-object v4

    .line 1481
    .local v4, "end":Lorg/threeten/bp/LocalDate;
    invoke-direct {v4}, Lorg/threeten/bp/LocalDate;->getProlepticMonth()J

    move-result-wide v0

    invoke-direct {p0}, Lorg/threeten/bp/LocalDate;->getProlepticMonth()J

    move-result-wide v2

    sub-long v5, v0, v2

    .line 1482
    .local v5, "totalMonths":J
    iget-short v0, v4, Lorg/threeten/bp/LocalDate;->day:S

    iget-short v1, p0, Lorg/threeten/bp/LocalDate;->day:S

    sub-int v7, v0, v1

    .line 1483
    .local v7, "days":I
    const-wide/16 v0, 0x0

    cmp-long v0, v5, v0

    if-lez v0, :cond_2e

    if-gez v7, :cond_2e

    .line 1484
    const-wide/16 v0, 0x1

    sub-long/2addr v5, v0

    .line 1485
    invoke-virtual {p0, v5, v6}, Lorg/threeten/bp/LocalDate;->plusMonths(J)Lorg/threeten/bp/LocalDate;

    move-result-object v8

    .line 1486
    .local v8, "calcDate":Lorg/threeten/bp/LocalDate;
    invoke-virtual {v4}, Lorg/threeten/bp/LocalDate;->toEpochDay()J

    move-result-wide v0

    invoke-virtual {v8}, Lorg/threeten/bp/LocalDate;->toEpochDay()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v7, v0

    .line 1487
    .end local v8    # "calcDate":Lorg/threeten/bp/LocalDate;
    goto :goto_3e

    :cond_2e
    const-wide/16 v0, 0x0

    cmp-long v0, v5, v0

    if-gez v0, :cond_3e

    if-lez v7, :cond_3e

    .line 1488
    const-wide/16 v0, 0x1

    add-long/2addr v5, v0

    .line 1489
    invoke-virtual {v4}, Lorg/threeten/bp/LocalDate;->lengthOfMonth()I

    move-result v0

    sub-int/2addr v7, v0

    .line 1491
    :cond_3e
    :goto_3e
    const-wide/16 v0, 0xc

    div-long v8, v5, v0

    .line 1492
    .local v8, "years":J
    const-wide/16 v0, 0xc

    rem-long v0, v5, v0

    long-to-int v10, v0

    .line 1493
    .local v10, "months":I
    invoke-static {v8, v9}, Lorg/threeten/bp/jdk8/Jdk8Methods;->safeToInt(J)I

    move-result v0

    invoke-static {v0, v10, v7}, Lorg/threeten/bp/Period;->of(III)Lorg/threeten/bp/Period;

    move-result-object v0

    return-object v0
.end method

.method public with(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/LocalDate;
    .registers 3
    .param p1, "adjuster"    # Lorg/threeten/bp/temporal/TemporalAdjuster;

    .line 821
    instance-of v0, p1, Lorg/threeten/bp/LocalDate;

    if-eqz v0, :cond_8

    .line 822
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/LocalDate;

    return-object v0

    .line 824
    :cond_8
    invoke-interface {p1, p0}, Lorg/threeten/bp/temporal/TemporalAdjuster;->adjustInto(Lorg/threeten/bp/temporal/Temporal;)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/LocalDate;

    return-object v0
.end method

.method public with(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/LocalDate;
    .registers 8
    .param p1, "field"    # Lorg/threeten/bp/temporal/TemporalField;
    .param p2, "newValue"    # J

    .line 932
    instance-of v0, p1, Lorg/threeten/bp/temporal/ChronoField;

    if-eqz v0, :cond_c3

    .line 933
    move-object v3, p1

    check-cast v3, Lorg/threeten/bp/temporal/ChronoField;

    .line 934
    .local v3, "f":Lorg/threeten/bp/temporal/ChronoField;
    invoke-virtual {v3, p2, p3}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 935
    sget-object v0, Lorg/threeten/bp/LocalDate$2;->$SwitchMap$org$threeten$bp$temporal$ChronoField:[I

    invoke-virtual {v3}, Lorg/threeten/bp/temporal/ChronoField;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_ca

    goto/16 :goto_aa

    .line 936
    :pswitch_17
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getDayOfWeek()Lorg/threeten/bp/DayOfWeek;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/DayOfWeek;->getValue()I

    move-result v0

    int-to-long v0, v0

    sub-long v0, p2, v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalDate;->plusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0

    .line 937
    :pswitch_27
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->ALIGNED_DAY_OF_WEEK_IN_MONTH:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->getLong(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    sub-long v0, p2, v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalDate;->plusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0

    .line 938
    :pswitch_34
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->ALIGNED_DAY_OF_WEEK_IN_YEAR:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->getLong(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    sub-long v0, p2, v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalDate;->plusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0

    .line 939
    :pswitch_41
    long-to-int v0, p2

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->withDayOfMonth(I)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0

    .line 940
    :pswitch_47
    long-to-int v0, p2

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->withDayOfYear(I)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0

    .line 941
    :pswitch_4d
    invoke-static {p2, p3}, Lorg/threeten/bp/LocalDate;->ofEpochDay(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0

    .line 942
    :pswitch_52
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->ALIGNED_WEEK_OF_MONTH:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->getLong(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    sub-long v0, p2, v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalDate;->plusWeeks(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0

    .line 943
    :pswitch_5f
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->ALIGNED_WEEK_OF_YEAR:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->getLong(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    sub-long v0, p2, v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalDate;->plusWeeks(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0

    .line 944
    :pswitch_6c
    long-to-int v0, p2

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->withMonth(I)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0

    .line 945
    :pswitch_72
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->PROLEPTIC_MONTH:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->getLong(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    sub-long v0, p2, v0

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/LocalDate;->plusMonths(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0

    .line 946
    :pswitch_7f
    iget v0, p0, Lorg/threeten/bp/LocalDate;->year:I

    const/4 v1, 0x1

    if-lt v0, v1, :cond_86

    move-wide v0, p2

    goto :goto_89

    :cond_86
    const-wide/16 v0, 0x1

    sub-long/2addr v0, p2

    :goto_89
    long-to-int v0, v0

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->withYear(I)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0

    .line 947
    :pswitch_8f
    long-to-int v0, p2

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->withYear(I)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0

    .line 948
    :pswitch_95
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->ERA:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->getLong(Lorg/threeten/bp/temporal/TemporalField;)J

    move-result-wide v0

    cmp-long v0, v0, p2

    if-nez v0, :cond_a1

    move-object v0, p0

    goto :goto_a9

    :cond_a1
    iget v0, p0, Lorg/threeten/bp/LocalDate;->year:I

    rsub-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lorg/threeten/bp/LocalDate;->withYear(I)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    :goto_a9
    return-object v0

    .line 950
    :goto_aa
    new-instance v0, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported field: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/threeten/bp/temporal/UnsupportedTemporalTypeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 952
    .end local v3    # "f":Lorg/threeten/bp/temporal/ChronoField;
    :cond_c3
    invoke-interface {p1, p0, p2, p3}, Lorg/threeten/bp/temporal/TemporalField;->adjustInto(Lorg/threeten/bp/temporal/Temporal;J)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    check-cast v0, Lorg/threeten/bp/LocalDate;

    return-object v0

    :pswitch_data_ca
    .packed-switch 0x1
        :pswitch_41
        :pswitch_47
        :pswitch_52
        :pswitch_7f
        :pswitch_17
        :pswitch_27
        :pswitch_34
        :pswitch_4d
        :pswitch_5f
        :pswitch_6c
        :pswitch_72
        :pswitch_8f
        :pswitch_95
    .end packed-switch
.end method

.method public bridge synthetic with(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .registers 3
    .param p1, "x0"    # Lorg/threeten/bp/temporal/TemporalAdjuster;

    .line 100
    invoke-virtual {p0, p1}, Lorg/threeten/bp/LocalDate;->with(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic with(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/chrono/ChronoLocalDate;
    .registers 5
    .param p1, "x0"    # Lorg/threeten/bp/temporal/TemporalField;
    .param p2, "x1"    # J

    .line 100
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/LocalDate;->with(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic with(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/temporal/Temporal;
    .registers 3
    .param p1, "x0"    # Lorg/threeten/bp/temporal/TemporalAdjuster;

    .line 100
    invoke-virtual {p0, p1}, Lorg/threeten/bp/LocalDate;->with(Lorg/threeten/bp/temporal/TemporalAdjuster;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic with(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/temporal/Temporal;
    .registers 5
    .param p1, "x0"    # Lorg/threeten/bp/temporal/TemporalField;
    .param p2, "x1"    # J

    .line 100
    invoke-virtual {p0, p1, p2, p3}, Lorg/threeten/bp/LocalDate;->with(Lorg/threeten/bp/temporal/TemporalField;J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public withDayOfMonth(I)Lorg/threeten/bp/LocalDate;
    .registers 4
    .param p1, "dayOfMonth"    # I

    .line 1004
    iget-short v0, p0, Lorg/threeten/bp/LocalDate;->day:S

    if-ne v0, p1, :cond_5

    .line 1005
    return-object p0

    .line 1007
    :cond_5
    iget v0, p0, Lorg/threeten/bp/LocalDate;->year:I

    iget-short v1, p0, Lorg/threeten/bp/LocalDate;->month:S

    invoke-static {v0, v1, p1}, Lorg/threeten/bp/LocalDate;->of(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public withDayOfYear(I)Lorg/threeten/bp/LocalDate;
    .registers 3
    .param p1, "dayOfYear"    # I

    .line 1022
    invoke-virtual {p0}, Lorg/threeten/bp/LocalDate;->getDayOfYear()I

    move-result v0

    if-ne v0, p1, :cond_7

    .line 1023
    return-object p0

    .line 1025
    :cond_7
    iget v0, p0, Lorg/threeten/bp/LocalDate;->year:I

    invoke-static {v0, p1}, Lorg/threeten/bp/LocalDate;->ofYearDay(II)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public withMonth(I)Lorg/threeten/bp/LocalDate;
    .registers 5
    .param p1, "month"    # I

    .line 985
    iget-short v0, p0, Lorg/threeten/bp/LocalDate;->month:S

    if-ne v0, p1, :cond_5

    .line 986
    return-object p0

    .line 988
    :cond_5
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->MONTH_OF_YEAR:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 989
    iget v0, p0, Lorg/threeten/bp/LocalDate;->year:I

    iget-short v1, p0, Lorg/threeten/bp/LocalDate;->day:S

    invoke-static {v0, p1, v1}, Lorg/threeten/bp/LocalDate;->resolvePreviousValid(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public withYear(I)Lorg/threeten/bp/LocalDate;
    .registers 5
    .param p1, "year"    # I

    .line 967
    iget v0, p0, Lorg/threeten/bp/LocalDate;->year:I

    if-ne v0, p1, :cond_5

    .line 968
    return-object p0

    .line 970
    :cond_5
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->YEAR:Lorg/threeten/bp/temporal/ChronoField;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/temporal/ChronoField;->checkValidValue(J)J

    .line 971
    iget-short v0, p0, Lorg/threeten/bp/LocalDate;->month:S

    iget-short v1, p0, Lorg/threeten/bp/LocalDate;->day:S

    invoke-static {p1, v0, v1}, Lorg/threeten/bp/LocalDate;->resolvePreviousValid(III)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method writeExternal(Ljava/io/DataOutput;)V
    .registers 3
    .param p1, "out"    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1877
    iget v0, p0, Lorg/threeten/bp/LocalDate;->year:I

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 1878
    iget-short v0, p0, Lorg/threeten/bp/LocalDate;->month:S

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 1879
    iget-short v0, p0, Lorg/threeten/bp/LocalDate;->day:S

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 1880
    return-void
.end method

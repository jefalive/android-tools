.class Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy$6;
.super Ljava/lang/Object;
.source "PagamentoGpsActiviy.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->mostraDialogDuplicidade()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;

.field final synthetic val$dialog:Lcom/itau/empresas/ui/dialog/CustomAlertDialog;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;

    .line 457
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy$6;->this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;

    iput-object p2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy$6;->val$dialog:Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;

    .line 460
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy$6;->this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;

    const/4 v1, 0x1

    # invokes: Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->incluiGps(Z)V
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->access$200(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;Z)V

    .line 461
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy$6;->this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;

    iget-object v0, v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy;->controller:Lcom/itau/empresas/controller/PagamentoGPSController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/PagamentoGPSController;->incluiComDuplicidade()V

    .line 462
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoGpsActiviy$6;->val$dialog:Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->dismiss()V

    .line 463
    return-void
.end method

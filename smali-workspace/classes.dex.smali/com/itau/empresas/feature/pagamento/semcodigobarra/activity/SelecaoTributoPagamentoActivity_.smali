.class public final Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;
.super Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;
.source "SelecaoTributoPagamentoActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 34
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;-><init>()V

    .line 38
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;

    .line 34
    invoke-super {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;

    .line 34
    invoke-super {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 50
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 51
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 52
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 53
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;->app:Lcom/itau/empresas/CustomApplication;

    .line 54
    invoke-static {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/TributosAdapter_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/TributosAdapter_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;->adapter:Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/TributosAdapter;

    .line 55
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;->afterInject()V

    .line 56
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 122
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_$3;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 130
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 110
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_$2;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 118
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 42
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 43
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;->init_(Landroid/os/Bundle;)V

    .line 44
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 46
    const v0, 0x7f030065

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;->setContentView(I)V

    .line 47
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 90
    const v0, 0x7f0e030c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;->listaTributos:Landroid/widget/ListView;

    .line 91
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 92
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;->listaTributos:Landroid/widget/ListView;

    if-eqz v0, :cond_24

    .line 93
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;->listaTributos:Landroid/widget/ListView;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_$1;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 102
    :cond_24
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;->carregaElemetosTela()V

    .line 103
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 60
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->setContentView(I)V

    .line 61
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 62
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 72
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->setContentView(Landroid/view/View;)V

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 74
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 66
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/SelecaoTributoPagamentoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 68
    return-void
.end method

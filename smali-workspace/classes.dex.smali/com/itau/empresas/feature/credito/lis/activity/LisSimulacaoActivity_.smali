.class public final Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;
.super Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;
.source "LisSimulacaoActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 40
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;-><init>()V

    .line 44
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;

    .line 40
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;

    .line 40
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 59
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 60
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 61
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 62
    .local v1, "resources_":Landroid/content/res/Resources;
    const v0, 0x7f07042b

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->textoAlterarCorSpanConsultar:Ljava/lang/String;

    .line 63
    const v0, 0x7f07042a

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->textoAlterarCorSpanCondicoesGerais:Ljava/lang/String;

    .line 64
    const v0, 0x7f0703fd

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->condicoesGeraisPDF:Ljava/lang/String;

    .line 65
    const v0, 0x7f0703fc

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->condicoesGeraisAdicionalPDF:Ljava/lang/String;

    .line 66
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 67
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/lis/LisController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/LisController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->controller:Lcom/itau/empresas/feature/credito/lis/LisController;

    .line 68
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->injectExtras_()V

    .line 70
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->afterInject()V

    .line 71
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 172
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 173
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_3e

    .line 174
    const-string v0, "lisAdicional"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 175
    const-string v0, "lisAdicional"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->lisAdicional:Z

    .line 177
    :cond_1a
    const-string v0, "simulacaoRespostaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 178
    const-string v0, "simulacaoRespostaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->simulacaoRespostaVO:Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;

    .line 180
    :cond_2c
    const-string v0, "ofertaContratacaoLisVo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 181
    const-string v0, "ofertaContratacaoLisVo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    .line 184
    :cond_3e
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 92
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 209
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$6;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$6;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 217
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 197
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$5;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$5;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 205
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 52
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->init_(Landroid/os/Bundle;)V

    .line 53
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 55
    const v0, 0x7f03004d

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->setContentView(I)V

    .line 56
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 6
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 105
    const v0, 0x7f0e0243

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->root:Landroid/widget/LinearLayout;

    .line 106
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 107
    const v0, 0x7f0e0246

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->textoDescricaoValor:Landroid/widget/TextView;

    .line 108
    const v0, 0x7f0e0247

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->textoValorEscolhido:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f0e024a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->lisSimulacaoValorAdicional:Landroid/widget/TextView;

    .line 110
    const v0, 0x7f0e0248

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->textoValorPreAprovado:Landroid/widget/TextView;

    .line 111
    const v0, 0x7f0e0249

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->limiteAdicional:Landroid/widget/LinearLayout;

    .line 112
    const v0, 0x7f0e024b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->descricaoDataVencimento:Landroid/widget/TextView;

    .line 113
    const v0, 0x7f0e024c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->textoDiaVencimento:Landroid/widget/TextView;

    .line 114
    const v0, 0x7f0e0224

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->textoJurosMes:Landroid/widget/TextView;

    .line 115
    const v0, 0x7f0e024d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->textoTarifaContratacao:Landroid/widget/TextView;

    .line 116
    const v0, 0x7f0e024e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->textoIOF:Landroid/widget/TextView;

    .line 117
    const v0, 0x7f0e024f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->textoTotalOperacao:Landroid/widget/TextView;

    .line 118
    const v0, 0x7f0e0250

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->textoCustoEfetivoTotal:Landroid/widget/TextView;

    .line 119
    const v0, 0x7f0e0251

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->textoPeriodicidade:Landroid/widget/TextView;

    .line 120
    const v0, 0x7f0e01e7

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->textoDataVencimento:Landroid/widget/TextView;

    .line 121
    const v0, 0x7f0e0253

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->textoLisCondicoesGerais:Landroid/widget/TextView;

    .line 122
    const v0, 0x7f0e0255

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->textviewLisSimualcaoCardCondicoesGerais:Landroid/widget/TextView;

    .line 123
    const v0, 0x7f0e0256

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->botaoContratar:Landroid/widget/Button;

    .line 124
    const v0, 0x7f0e0220

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->rlLisEscolhaValor:Landroid/view/View;

    .line 125
    const v0, 0x7f0e0252

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 126
    .local v2, "view_botao_reiniciar":Landroid/view/View;
    const v0, 0x7f0e0254

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 128
    .local v3, "view_card_view_lis_simulacao_descricao":Landroid/view/View;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->textoLisCondicoesGerais:Landroid/widget/TextView;

    if-eqz v0, :cond_f6

    .line 129
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->textoLisCondicoesGerais:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$1;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    :cond_f6
    if-eqz v2, :cond_100

    .line 139
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$2;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    :cond_100
    if-eqz v3, :cond_10a

    .line 149
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$3;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$3;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 158
    :cond_10a
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->botaoContratar:Landroid/widget/Button;

    if-eqz v0, :cond_118

    .line 159
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->botaoContratar:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$4;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    :cond_118
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->aoCarregarTela()V

    .line 169
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 75
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->setContentView(I)V

    .line 76
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 77
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 87
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->setContentView(Landroid/view/View;)V

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 89
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 81
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 82
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 83
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 188
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity;->setIntent(Landroid/content/Intent;)V

    .line 189
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->injectExtras_()V

    .line 190
    return-void
.end method

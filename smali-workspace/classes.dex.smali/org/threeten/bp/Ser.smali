.class final Lorg/threeten/bp/Ser;
.super Ljava/lang/Object;
.source "Ser.java"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = -0x6aa27b45e4ddb74eL


# instance fields
.field private object:Ljava/lang/Object;

.field private type:B


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    return-void
.end method

.method constructor <init>(BLjava/lang/Object;)V
    .registers 3
    .param p1, "type"    # B
    .param p2, "object"    # Ljava/lang/Object;

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iput-byte p1, p0, Lorg/threeten/bp/Ser;->type:B

    .line 106
    iput-object p2, p0, Lorg/threeten/bp/Ser;->object:Ljava/lang/Object;

    .line 107
    return-void
.end method

.method static read(Ljava/io/DataInput;)Ljava/lang/Object;
    .registers 3
    .param p0, "in"    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 178
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v1

    .line 179
    .local v1, "type":B
    invoke-static {v1, p0}, Lorg/threeten/bp/Ser;->readInternal(BLjava/io/DataInput;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private static readInternal(BLjava/io/DataInput;)Ljava/lang/Object;
    .registers 4
    .param p0, "type"    # B
    .param p1, "in"    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 183
    sparse-switch p0, :sswitch_data_4e

    goto/16 :goto_46

    .line 184
    :sswitch_5
    invoke-static {p1}, Lorg/threeten/bp/Duration;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/Duration;

    move-result-object v0

    return-object v0

    .line 185
    :sswitch_a
    invoke-static {p1}, Lorg/threeten/bp/Instant;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/Instant;

    move-result-object v0

    return-object v0

    .line 186
    :sswitch_f
    invoke-static {p1}, Lorg/threeten/bp/LocalDate;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0

    .line 187
    :sswitch_14
    invoke-static {p1}, Lorg/threeten/bp/LocalDateTime;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    return-object v0

    .line 188
    :sswitch_19
    invoke-static {p1}, Lorg/threeten/bp/LocalTime;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    return-object v0

    .line 189
    :sswitch_1e
    invoke-static {p1}, Lorg/threeten/bp/MonthDay;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/MonthDay;

    move-result-object v0

    return-object v0

    .line 190
    :sswitch_23
    invoke-static {p1}, Lorg/threeten/bp/OffsetDateTime;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/OffsetDateTime;

    move-result-object v0

    return-object v0

    .line 191
    :sswitch_28
    invoke-static {p1}, Lorg/threeten/bp/OffsetTime;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/OffsetTime;

    move-result-object v0

    return-object v0

    .line 192
    :sswitch_2d
    invoke-static {p1}, Lorg/threeten/bp/Year;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/Year;

    move-result-object v0

    return-object v0

    .line 193
    :sswitch_32
    invoke-static {p1}, Lorg/threeten/bp/YearMonth;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/YearMonth;

    move-result-object v0

    return-object v0

    .line 194
    :sswitch_37
    invoke-static {p1}, Lorg/threeten/bp/ZonedDateTime;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    return-object v0

    .line 195
    :sswitch_3c
    invoke-static {p1}, Lorg/threeten/bp/ZoneOffset;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    return-object v0

    .line 196
    :sswitch_41
    invoke-static {p1}, Lorg/threeten/bp/ZoneRegion;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/ZoneId;

    move-result-object v0

    return-object v0

    .line 198
    :goto_46
    new-instance v0, Ljava/io/StreamCorruptedException;

    const-string v1, "Unknown serialized type"

    invoke-direct {v0, v1}, Ljava/io/StreamCorruptedException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_data_4e
    .sparse-switch
        0x1 -> :sswitch_5
        0x2 -> :sswitch_a
        0x3 -> :sswitch_f
        0x4 -> :sswitch_14
        0x5 -> :sswitch_19
        0x6 -> :sswitch_37
        0x7 -> :sswitch_41
        0x8 -> :sswitch_3c
        0x40 -> :sswitch_1e
        0x42 -> :sswitch_28
        0x43 -> :sswitch_2d
        0x44 -> :sswitch_32
        0x45 -> :sswitch_23
    .end sparse-switch
.end method

.method private readResolve()Ljava/lang/Object;
    .registers 2

    .line 208
    iget-object v0, p0, Lorg/threeten/bp/Ser;->object:Ljava/lang/Object;

    return-object v0
.end method

.method static writeInternal(BLjava/lang/Object;Ljava/io/DataOutput;)V
    .registers 5
    .param p0, "type"    # B
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "out"    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 120
    invoke-interface {p2, p0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 121
    sparse-switch p0, :sswitch_data_6e

    goto/16 :goto_65

    .line 123
    :sswitch_8
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/Duration;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/Duration;->writeExternal(Ljava/io/DataOutput;)V

    .line 124
    goto/16 :goto_6d

    .line 126
    :sswitch_10
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/Instant;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/Instant;->writeExternal(Ljava/io/DataOutput;)V

    .line 127
    goto/16 :goto_6d

    .line 129
    :sswitch_18
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/LocalDate;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/LocalDate;->writeExternal(Ljava/io/DataOutput;)V

    .line 130
    goto :goto_6d

    .line 132
    :sswitch_1f
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/LocalDateTime;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/LocalDateTime;->writeExternal(Ljava/io/DataOutput;)V

    .line 133
    goto :goto_6d

    .line 135
    :sswitch_26
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/LocalTime;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/LocalTime;->writeExternal(Ljava/io/DataOutput;)V

    .line 136
    goto :goto_6d

    .line 138
    :sswitch_2d
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/MonthDay;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/MonthDay;->writeExternal(Ljava/io/DataOutput;)V

    .line 139
    goto :goto_6d

    .line 141
    :sswitch_34
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/OffsetDateTime;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/OffsetDateTime;->writeExternal(Ljava/io/DataOutput;)V

    .line 142
    goto :goto_6d

    .line 144
    :sswitch_3b
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/OffsetTime;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/OffsetTime;->writeExternal(Ljava/io/DataOutput;)V

    .line 145
    goto :goto_6d

    .line 147
    :sswitch_42
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/YearMonth;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/YearMonth;->writeExternal(Ljava/io/DataOutput;)V

    .line 148
    goto :goto_6d

    .line 150
    :sswitch_49
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/Year;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/Year;->writeExternal(Ljava/io/DataOutput;)V

    .line 151
    goto :goto_6d

    .line 153
    :sswitch_50
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/ZoneRegion;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/ZoneRegion;->writeExternal(Ljava/io/DataOutput;)V

    .line 154
    goto :goto_6d

    .line 156
    :sswitch_57
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/ZoneOffset;->writeExternal(Ljava/io/DataOutput;)V

    .line 157
    goto :goto_6d

    .line 159
    :sswitch_5e
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/ZonedDateTime;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/ZonedDateTime;->writeExternal(Ljava/io/DataOutput;)V

    .line 160
    goto :goto_6d

    .line 162
    :goto_65
    new-instance v0, Ljava/io/InvalidClassException;

    const-string v1, "Unknown serialized type"

    invoke-direct {v0, v1}, Ljava/io/InvalidClassException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 164
    :goto_6d
    return-void

    :sswitch_data_6e
    .sparse-switch
        0x1 -> :sswitch_8
        0x2 -> :sswitch_10
        0x3 -> :sswitch_18
        0x4 -> :sswitch_1f
        0x5 -> :sswitch_26
        0x6 -> :sswitch_5e
        0x7 -> :sswitch_50
        0x8 -> :sswitch_57
        0x40 -> :sswitch_2d
        0x42 -> :sswitch_3b
        0x43 -> :sswitch_49
        0x44 -> :sswitch_42
        0x45 -> :sswitch_34
    .end sparse-switch
.end method


# virtual methods
.method public readExternal(Ljava/io/ObjectInput;)V
    .registers 3
    .param p1, "in"    # Ljava/io/ObjectInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 173
    invoke-interface {p1}, Ljava/io/ObjectInput;->readByte()B

    move-result v0

    iput-byte v0, p0, Lorg/threeten/bp/Ser;->type:B

    .line 174
    iget-byte v0, p0, Lorg/threeten/bp/Ser;->type:B

    invoke-static {v0, p1}, Lorg/threeten/bp/Ser;->readInternal(BLjava/io/DataInput;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/Ser;->object:Ljava/lang/Object;

    .line 175
    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .registers 4
    .param p1, "out"    # Ljava/io/ObjectOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 116
    iget-byte v0, p0, Lorg/threeten/bp/Ser;->type:B

    iget-object v1, p0, Lorg/threeten/bp/Ser;->object:Ljava/lang/Object;

    invoke-static {v0, v1, p1}, Lorg/threeten/bp/Ser;->writeInternal(BLjava/lang/Object;Ljava/io/DataOutput;)V

    .line 117
    return-void
.end method

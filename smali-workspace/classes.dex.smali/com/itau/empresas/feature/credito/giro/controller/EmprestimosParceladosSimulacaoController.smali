.class public Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;
.super Lcom/itau/empresas/controller/BaseController;
.source "EmprestimosParceladosSimulacaoController.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 16
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method

.method static synthetic access$000(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 16
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$100(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 16
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;->post(Ljava/lang/Object;)V

    return-void
.end method

.method public static getApi()Lcom/itau/empresas/api/credito/ApiGiro;
    .registers 1

    .line 19
    const-class v0, Lcom/itau/empresas/api/credito/ApiGiro;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->api(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/credito/ApiGiro;

    return-object v0
.end method


# virtual methods
.method public consultaSimulacao(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)V
    .registers 3
    .param p1, "ofertaAceita"    # Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 24
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setOfertaEspecial(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;)V

    .line 25
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setOfertaProdutosParceladosSaidaVO(Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;)V

    .line 27
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController$1;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController$1;-><init>(Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 34
    return-void
.end method

.method public contrataEmprestimoSimulado(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;)V
    .registers 5
    .param p1, "simulacao"    # Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    .line 40
    .line 41
    :try_start_0
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->clone()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    .line 44
    .local v2, "simulacaoAtualizada":Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getValorTarifaContratacao()F

    move-result v0

    invoke-virtual {v2, v0}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->setValorTarifa(F)V

    .line 45
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getTaxaJurosMes()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->setTaxaJurosMensal(Ljava/lang/String;)V

    .line 46
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getTaxaJurosAno()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->setTaxaJurosAnual(Ljava/lang/String;)V

    .line 47
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->setTaxaJurosMes(Ljava/lang/String;)V

    .line 49
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController$2;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController$2;-><init>(Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V
    :try_end_28
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_28} :catch_29

    .line 58
    .end local v2    # "simulacaoAtualizada":Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;
    goto :goto_39

    .line 56
    :catch_29
    move-exception v2

    .line 57
    .local v2, "e":Ljava/lang/CloneNotSupportedException;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/CloneNotSupportedException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lbr/com/itau/textovoz/util/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 60
    .end local v2    # "e":Ljava/lang/CloneNotSupportedException;
    :goto_39
    return-void
.end method

.class public final Lcom/itau/empresas/feature/mapa/MapaActivity_;
.super Lcom/itau/empresas/feature/mapa/MapaActivity;
.source "MapaActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/mapa/MapaActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 39
    invoke-direct {p0}, Lcom/itau/empresas/feature/mapa/MapaActivity;-><init>()V

    .line 43
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/mapa/MapaActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/mapa/MapaActivity_;

    .line 39
    invoke-super {p0}, Lcom/itau/empresas/feature/mapa/MapaActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/mapa/MapaActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/mapa/MapaActivity_;

    .line 39
    invoke-super {p0}, Lcom/itau/empresas/feature/mapa/MapaActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 55
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 56
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 57
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 58
    invoke-static {p0}, Lcom/itau/empresas/controller/MapasController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/controller/MapasController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->controller:Lcom/itau/empresas/controller/MapasController;

    .line 59
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/mapa/MapaActivity_;->restoreSavedInstanceState_(Landroid/os/Bundle;)V

    .line 60
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaActivity_;->afterInject()V

    .line 61
    return-void
.end method

.method private restoreSavedInstanceState_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 155
    if-nez p1, :cond_3

    .line 156
    return-void

    .line 158
    :cond_3
    const-string v0, "lastPosition"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/LatLng;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->lastPosition:Lcom/google/android/gms/maps/model/LatLng;

    .line 159
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 178
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/mapa/MapaActivity_$6;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/mapa/MapaActivity_$6;-><init>(Lcom/itau/empresas/feature/mapa/MapaActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 186
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 166
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/mapa/MapaActivity_$5;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/mapa/MapaActivity_$5;-><init>(Lcom/itau/empresas/feature/mapa/MapaActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 174
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 48
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/mapa/MapaActivity_;->init_(Landroid/os/Bundle;)V

    .line 49
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/mapa/MapaActivity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 51
    const v0, 0x7f030051

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/mapa/MapaActivity_;->setContentView(I)V

    .line 52
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "bundle_"    # Landroid/os/Bundle;

    .line 150
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/mapa/MapaActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 151
    const-string v0, "lastPosition"

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->lastPosition:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 152
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 95
    const v0, 0x7f0e0269

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->root:Landroid/widget/RelativeLayout;

    .line 96
    const v0, 0x7f0e025a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 97
    const v0, 0x7f0e05c4

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->tituloToolbar:Landroid/widget/TextView;

    .line 98
    const v0, 0x7f0e026a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->tabsMap:Landroid/support/design/widget/TabLayout;

    .line 99
    const v0, 0x7f0e026b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->pagerMap:Landroid/support/v4/view/ViewPager;

    .line 100
    const v0, 0x7f0e026e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SwitchCompat;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->switchFiltroItau:Landroid/support/v7/widget/SwitchCompat;

    .line 101
    const v0, 0x7f0e0270

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SwitchCompat;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->switchFiltroPersonnalite:Landroid/support/v7/widget/SwitchCompat;

    .line 102
    const v0, 0x7f0e0272

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SwitchCompat;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->switchFiltroEmpresas:Landroid/support/v7/widget/SwitchCompat;

    .line 103
    const v0, 0x7f0e0274

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/SwitchCompat;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->switchFiltroCaixaEletronico:Landroid/support/v7/widget/SwitchCompat;

    .line 104
    const v0, 0x7f0e0268

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->drawerLayoutFilter:Landroid/support/v4/widget/DrawerLayout;

    .line 105
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->switchFiltroItau:Landroid/support/v7/widget/SwitchCompat;

    if-eqz v0, :cond_7c

    .line 106
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->switchFiltroItau:Landroid/support/v7/widget/SwitchCompat;

    new-instance v1, Lcom/itau/empresas/feature/mapa/MapaActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/mapa/MapaActivity_$1;-><init>(Lcom/itau/empresas/feature/mapa/MapaActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 115
    :cond_7c
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->switchFiltroPersonnalite:Landroid/support/v7/widget/SwitchCompat;

    if-eqz v0, :cond_8a

    .line 116
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->switchFiltroPersonnalite:Landroid/support/v7/widget/SwitchCompat;

    new-instance v1, Lcom/itau/empresas/feature/mapa/MapaActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/mapa/MapaActivity_$2;-><init>(Lcom/itau/empresas/feature/mapa/MapaActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 125
    :cond_8a
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->switchFiltroEmpresas:Landroid/support/v7/widget/SwitchCompat;

    if-eqz v0, :cond_98

    .line 126
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->switchFiltroEmpresas:Landroid/support/v7/widget/SwitchCompat;

    new-instance v1, Lcom/itau/empresas/feature/mapa/MapaActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/mapa/MapaActivity_$3;-><init>(Lcom/itau/empresas/feature/mapa/MapaActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 135
    :cond_98
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->switchFiltroCaixaEletronico:Landroid/support/v7/widget/SwitchCompat;

    if-eqz v0, :cond_a6

    .line 136
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->switchFiltroCaixaEletronico:Landroid/support/v7/widget/SwitchCompat;

    new-instance v1, Lcom/itau/empresas/feature/mapa/MapaActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/mapa/MapaActivity_$4;-><init>(Lcom/itau/empresas/feature/mapa/MapaActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 145
    :cond_a6
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaActivity_;->afterViews()V

    .line 146
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 65
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/mapa/MapaActivity;->setContentView(I)V

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 67
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 77
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/mapa/MapaActivity;->setContentView(Landroid/view/View;)V

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 79
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 71
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/mapa/MapaActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 73
    return-void
.end method

.class Lio/fabric/sdk/android/services/settings/DefaultCachedSettingsIo;
.super Ljava/lang/Object;
.source "DefaultCachedSettingsIo.java"

# interfaces
.implements Lio/fabric/sdk/android/services/settings/CachedSettingsIo;


# instance fields
.field private final kit:Lio/fabric/sdk/android/Kit;


# direct methods
.method public constructor <init>(Lio/fabric/sdk/android/Kit;)V
    .registers 2
    .param p1, "kit"    # Lio/fabric/sdk/android/Kit;

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lio/fabric/sdk/android/services/settings/DefaultCachedSettingsIo;->kit:Lio/fabric/sdk/android/Kit;

    .line 42
    return-void
.end method


# virtual methods
.method public readCachedSettings()Lorg/json/JSONObject;
    .registers 9

    .line 46
    invoke-static {}, Lio/fabric/sdk/android/Fabric;->getLogger()Lio/fabric/sdk/android/Logger;

    move-result-object v0

    const-string v1, "Fabric"

    const-string v2, "Reading cached settings..."

    invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const/4 v3, 0x0

    .line 49
    .local v3, "fis":Ljava/io/FileInputStream;
    const/4 v4, 0x0

    .line 52
    .local v4, "toReturn":Lorg/json/JSONObject;
    :try_start_d
    new-instance v5, Ljava/io/File;

    new-instance v0, Lio/fabric/sdk/android/services/persistence/FileStoreImpl;

    iget-object v1, p0, Lio/fabric/sdk/android/services/settings/DefaultCachedSettingsIo;->kit:Lio/fabric/sdk/android/Kit;

    invoke-direct {v0, v1}, Lio/fabric/sdk/android/services/persistence/FileStoreImpl;-><init>(Lio/fabric/sdk/android/Kit;)V

    invoke-virtual {v0}, Lio/fabric/sdk/android/services/persistence/FileStoreImpl;->getFilesDir()Ljava/io/File;

    move-result-object v0

    const-string v1, "com.crashlytics.settings.json"

    invoke-direct {v5, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 55
    .local v5, "settingsFile":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_36

    .line 56
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move-object v3, v0

    .line 57
    invoke-static {v3}, Lio/fabric/sdk/android/services/common/CommonUtils;->streamToString(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v6

    .line 59
    .local v6, "settingsStr":Ljava/lang/String;
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    move-object v4, v0

    .line 60
    .end local v6    # "settingsStr":Ljava/lang/String;
    goto :goto_41

    .line 61
    :cond_36
    invoke-static {}, Lio/fabric/sdk/android/Fabric;->getLogger()Lio/fabric/sdk/android/Logger;

    move-result-object v0

    const-string v1, "Fabric"

    const-string v2, "No cached settings found."

    invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_41
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_41} :catch_47
    .catchall {:try_start_d .. :try_end_41} :catchall_59

    .line 66
    .end local v5    # "settingsFile":Ljava/io/File;
    :goto_41
    const-string v0, "Error while closing settings cache file."

    invoke-static {v3, v0}, Lio/fabric/sdk/android/services/common/CommonUtils;->closeOrLog(Ljava/io/Closeable;Ljava/lang/String;)V

    .line 67
    goto :goto_60

    .line 63
    :catch_47
    move-exception v5

    .line 64
    .local v5, "e":Ljava/lang/Exception;
    :try_start_48
    invoke-static {}, Lio/fabric/sdk/android/Fabric;->getLogger()Lio/fabric/sdk/android/Logger;

    move-result-object v0

    const-string v1, "Fabric"

    const-string v2, "Failed to fetch cached settings"

    invoke-interface {v0, v1, v2, v5}, Lio/fabric/sdk/android/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_53
    .catchall {:try_start_48 .. :try_end_53} :catchall_59

    .line 66
    .end local v5    # "e":Ljava/lang/Exception;
    const-string v0, "Error while closing settings cache file."

    invoke-static {v3, v0}, Lio/fabric/sdk/android/services/common/CommonUtils;->closeOrLog(Ljava/io/Closeable;Ljava/lang/String;)V

    .line 67
    goto :goto_60

    .line 66
    :catchall_59
    move-exception v7

    const-string v0, "Error while closing settings cache file."

    invoke-static {v3, v0}, Lio/fabric/sdk/android/services/common/CommonUtils;->closeOrLog(Ljava/io/Closeable;Ljava/lang/String;)V

    throw v7

    .line 69
    :goto_60
    return-object v4
.end method

.method public writeCachedSettings(JLorg/json/JSONObject;)V
    .registers 11
    .param p1, "expiresAtMillis"    # J
    .param p3, "settingsJson"    # Lorg/json/JSONObject;

    .line 74
    invoke-static {}, Lio/fabric/sdk/android/Fabric;->getLogger()Lio/fabric/sdk/android/Logger;

    move-result-object v0

    const-string v1, "Fabric"

    const-string v2, "Writing settings to cache file..."

    invoke-interface {v0, v1, v2}, Lio/fabric/sdk/android/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    if-eqz p3, :cond_54

    .line 77
    const/4 v4, 0x0

    .line 80
    .local v4, "writer":Ljava/io/FileWriter;
    const-string v0, "expires_at"

    :try_start_10
    invoke-virtual {p3, v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 82
    new-instance v0, Ljava/io/FileWriter;

    new-instance v1, Ljava/io/File;

    new-instance v2, Lio/fabric/sdk/android/services/persistence/FileStoreImpl;

    iget-object v3, p0, Lio/fabric/sdk/android/services/settings/DefaultCachedSettingsIo;->kit:Lio/fabric/sdk/android/Kit;

    invoke-direct {v2, v3}, Lio/fabric/sdk/android/services/persistence/FileStoreImpl;-><init>(Lio/fabric/sdk/android/Kit;)V

    invoke-virtual {v2}, Lio/fabric/sdk/android/services/persistence/FileStoreImpl;->getFilesDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "com.crashlytics.settings.json"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    move-object v4, v0

    .line 84
    invoke-virtual {p3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 85
    invoke-virtual {v4}, Ljava/io/FileWriter;->flush()V
    :try_end_35
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_35} :catch_3b
    .catchall {:try_start_10 .. :try_end_35} :catchall_4d

    .line 89
    const-string v0, "Failed to close settings writer."

    invoke-static {v4, v0}, Lio/fabric/sdk/android/services/common/CommonUtils;->closeOrLog(Ljava/io/Closeable;Ljava/lang/String;)V

    .line 90
    goto :goto_54

    .line 86
    :catch_3b
    move-exception v5

    .line 87
    .local v5, "e":Ljava/lang/Exception;
    :try_start_3c
    invoke-static {}, Lio/fabric/sdk/android/Fabric;->getLogger()Lio/fabric/sdk/android/Logger;

    move-result-object v0

    const-string v1, "Fabric"

    const-string v2, "Failed to cache settings"

    invoke-interface {v0, v1, v2, v5}, Lio/fabric/sdk/android/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_47
    .catchall {:try_start_3c .. :try_end_47} :catchall_4d

    .line 89
    .end local v5    # "e":Ljava/lang/Exception;
    const-string v0, "Failed to close settings writer."

    invoke-static {v4, v0}, Lio/fabric/sdk/android/services/common/CommonUtils;->closeOrLog(Ljava/io/Closeable;Ljava/lang/String;)V

    .line 90
    goto :goto_54

    .line 89
    :catchall_4d
    move-exception v6

    const-string v0, "Failed to close settings writer."

    invoke-static {v4, v0}, Lio/fabric/sdk/android/services/common/CommonUtils;->closeOrLog(Ljava/io/Closeable;Ljava/lang/String;)V

    throw v6

    .line 92
    .end local v4    # "writer":Ljava/io/FileWriter;
    :cond_54
    :goto_54
    return-void
.end method

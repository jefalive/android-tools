.class public Lcom/google/android/gms/internal/zzik;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# instance fields
.field private zzLJ:I

.field private zzLK:I

.field private final zzLh:Ljava/lang/String;

.field private final zzpV:Ljava/lang/Object;

.field private final zzqV:Lcom/google/android/gms/internal/zzih;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/zzih;Ljava/lang/String;)V
    .registers 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzik;->zzpV:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/gms/internal/zzik;->zzqV:Lcom/google/android/gms/internal/zzih;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzik;->zzLh:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 3

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbF()Lcom/google/android/gms/internal/zzih;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/internal/zzik;-><init>(Lcom/google/android/gms/internal/zzih;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public toBundle()Landroid/os/Bundle;
    .registers 6

    iget-object v2, p0, Lcom/google/android/gms/internal/zzik;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v0, "pmnli"

    iget v1, p0, Lcom/google/android/gms/internal/zzik;->zzLJ:I

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "pmnll"

    iget v1, p0, Lcom/google/android/gms/internal/zzik;->zzLK:I

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_18

    monitor-exit v2

    return-object v3

    :catchall_18
    move-exception v4

    monitor-exit v2

    throw v4
.end method

.method public zzg(II)V
    .registers 7

    iget-object v2, p0, Lcom/google/android/gms/internal/zzik;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iput p1, p0, Lcom/google/android/gms/internal/zzik;->zzLJ:I

    iput p2, p0, Lcom/google/android/gms/internal/zzik;->zzLK:I

    iget-object v0, p0, Lcom/google/android/gms/internal/zzik;->zzqV:Lcom/google/android/gms/internal/zzih;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzik;->zzLh:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/gms/internal/zzih;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzik;)V
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_10

    monitor-exit v2

    goto :goto_13

    :catchall_10
    move-exception v3

    monitor-exit v2

    throw v3

    :goto_13
    return-void
.end method

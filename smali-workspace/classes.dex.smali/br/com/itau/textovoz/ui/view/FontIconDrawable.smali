.class public Lbr/com/itau/textovoz/ui/view/FontIconDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "FontIconDrawable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/textovoz/ui/view/FontIconDrawable$MethodGetLayoutDirection;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private autoMirrored:Z

.field private boundsChanged:Z

.field private needMirroring:Z

.field private paint:Landroid/text/TextPaint;

.field private rect:Landroid/graphics/Rect;

.field private text:Ljava/lang/String;

.field private textColor:Landroid/content/res/ColorStateList;

.field private textSize:F


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 36
    const-class v0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 3

    .line 81
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 82
    const-string v0, ""

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->text:Ljava/lang/String;

    .line 83
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->paint:Landroid/text/TextPaint;

    .line 84
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->rect:Landroid/graphics/Rect;

    .line 86
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->paint:Landroid/text/TextPaint;

    invoke-static {}, Lbr/com/itau/textovoz/ui/view/FontIconTypefaceHolder;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 87
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->paint:Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    .line 88
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->paint:Landroid/text/TextPaint;

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 89
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .registers 1

    .line 35
    sget-object v0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method private static fitRect(Landroid/graphics/Rect;I)V
    .registers 8
    .param p0, "rect"    # Landroid/graphics/Rect;
    .param p1, "size"    # I

    .line 154
    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 155
    .local v2, "w":I
    if-le p1, v2, :cond_18

    .line 156
    sub-int v0, p1, v2

    div-int/lit8 v3, v0, 0x2

    .line 157
    .local v3, "d1":I
    sub-int v0, p1, v2

    sub-int v4, v0, v3

    .line 158
    .local v4, "d2":I
    iget v0, p0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v3

    iput v0, p0, Landroid/graphics/Rect;->left:I

    .line 159
    iget v0, p0, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v4

    iput v0, p0, Landroid/graphics/Rect;->right:I

    .line 162
    .end local v3    # "d1":I
    .end local v4    # "d2":I
    :cond_18
    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v3

    .line 163
    .local v3, "h":I
    if-le p1, v3, :cond_30

    .line 164
    sub-int v0, p1, v3

    div-int/lit8 v4, v0, 0x2

    .line 165
    .local v4, "d1":I
    sub-int v0, p1, v3

    sub-int v5, v0, v4

    .line 166
    .local v5, "d2":I
    iget v0, p0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v4

    iput v0, p0, Landroid/graphics/Rect;->top:I

    .line 167
    iget v0, p0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v5

    iput v0, p0, Landroid/graphics/Rect;->bottom:I

    .line 169
    .end local v4    # "d1":I
    .end local v5    # "d2":I
    :cond_30
    return-void
.end method

.method public static inflate(Landroid/content/Context;I)Lbr/com/itau/textovoz/ui/view/FontIconDrawable;
    .registers 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "xmlId"    # I

    .line 50
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v2

    .line 51
    .local v2, "parser":Landroid/content/res/XmlResourceParser;
    if-nez v2, :cond_10

    .line 52
    new-instance v0, Landroid/view/InflateException;

    invoke-direct {v0}, Landroid/view/InflateException;-><init>()V

    throw v0

    .line 57
    :cond_10
    :goto_10
    :try_start_10
    invoke-interface {v2}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v0

    move v3, v0

    .local v3, "type":I
    const/4 v1, 0x1

    if-eq v0, v1, :cond_3b

    .line 58
    invoke-interface {v2}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v4

    .line 60
    .local v4, "name":Ljava/lang/String;
    const/4 v0, 0x2

    if-eq v3, v0, :cond_20

    .line 61
    goto :goto_10

    .line 64
    :cond_20
    const-string v0, "font-icon"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 65
    new-instance v5, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;

    invoke-direct {v5}, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;-><init>()V

    .line 66
    .local v5, "result":Lbr/com/itau/textovoz/ui/view/FontIconDrawable;
    invoke-static {v2}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v0

    invoke-virtual {v5, p0, v0}, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->inflate(Landroid/content/Context;Landroid/util/AttributeSet;)V
    :try_end_34
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_10 .. :try_end_34} :catch_3c
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_34} :catch_43

    .line 67
    return-object v5

    .line 69
    .end local v5    # "result":Lbr/com/itau/textovoz/ui/view/FontIconDrawable;
    :cond_35
    :try_start_35
    new-instance v0, Landroid/view/InflateException;

    invoke-direct {v0, v4}, Landroid/view/InflateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3b
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_35 .. :try_end_3b} :catch_3c
    .catch Ljava/io/IOException; {:try_start_35 .. :try_end_3b} :catch_43

    .line 76
    .end local v3    # "type":I
    .end local v4    # "name":Ljava/lang/String;
    :cond_3b
    goto :goto_4a

    .line 72
    :catch_3c
    move-exception v3

    .line 73
    .local v3, "ex":Lorg/xmlpull/v1/XmlPullParserException;
    new-instance v0, Landroid/view/InflateException;

    invoke-direct {v0, v3}, Landroid/view/InflateException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 74
    .end local v3    # "ex":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_43
    move-exception v3

    .line 75
    .local v3, "ex":Ljava/io/IOException;
    new-instance v0, Landroid/view/InflateException;

    invoke-direct {v0, v3}, Landroid/view/InflateException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 78
    .end local v3    # "ex":Ljava/io/IOException;
    :goto_4a
    new-instance v0, Landroid/view/InflateException;

    invoke-direct {v0}, Landroid/view/InflateException;-><init>()V

    throw v0
.end method

.method private updatePaint(ZZ)V
    .registers 12
    .param p1, "updateBounds"    # Z
    .param p2, "forcedInvalidate"    # Z

    .line 128
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->getState()[I

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->updatePaintColor([I)Z

    move-result v5

    .line 130
    .local v5, "colorChanged":Z
    const/4 v6, 0x0

    .line 132
    .local v6, "textSizeChanged":Z
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->paint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getTextSize()F

    move-result v7

    .line 133
    .local v7, "oldTextSize":F
    iget v8, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->textSize:F

    .line 135
    .local v8, "newTextSize":F
    invoke-static {v7, v8}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    if-eqz v0, :cond_1d

    .line 136
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->paint:Landroid/text/TextPaint;

    invoke-virtual {v0, v8}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 137
    const/4 v6, 0x1

    .line 140
    :cond_1d
    if-nez v6, :cond_21

    if-eqz p1, :cond_41

    .line 141
    :cond_21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->boundsChanged:Z

    .line 143
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->paint:Landroid/text/TextPaint;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->text:Ljava/lang/String;

    iget-object v2, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->text:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->rect:Landroid/graphics/Rect;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 144
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->rect:Landroid/graphics/Rect;

    iget v1, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->textSize:F

    float-to-int v1, v1

    invoke-static {v0, v1}, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->fitRect(Landroid/graphics/Rect;I)V

    .line 145
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->rect:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 148
    :cond_41
    if-nez v5, :cond_49

    if-nez p2, :cond_49

    iget-boolean v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->boundsChanged:Z

    if-eqz v0, :cond_4c

    .line 149
    :cond_49
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->invalidateSelf()V

    .line 151
    :cond_4c
    return-void
.end method

.method private updatePaintColor([I)Z
    .registers 6
    .param p1, "state"    # [I

    .line 172
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->paint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getColor()I

    move-result v2

    .line 173
    .local v2, "oldColor":I
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->textColor:Landroid/content/res/ColorStateList;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->textColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v3

    .line 175
    .local v3, "newColor":I
    if-eq v2, v3, :cond_1b

    .line 176
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->paint:Landroid/text/TextPaint;

    invoke-virtual {v0, v3}, Landroid/text/TextPaint;->setColor(I)V

    .line 177
    const/4 v0, 0x1

    return v0

    .line 180
    :cond_1b
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .registers 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 312
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->needMirroring()Z

    move-result v4

    .line 314
    .local v4, "needMirroring":Z
    if-eqz v4, :cond_1e

    .line 315
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 316
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->rect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->rect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 317
    const/high16 v0, -0x40800000    # -1.0f

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 320
    :cond_1e
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->text:Ljava/lang/String;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->rect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    neg-int v1, v1

    int-to-float v1, v1

    iget-object v2, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->rect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    neg-int v2, v2

    int-to-float v2, v2

    iget-object v3, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->paint:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 322
    if-eqz v4, :cond_36

    .line 323
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 325
    :cond_36
    return-void
.end method

.method public getIntrinsicHeight()I
    .registers 2

    .line 296
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->rect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    return v0
.end method

.method public getIntrinsicWidth()I
    .registers 2

    .line 291
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->rect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    return v0
.end method

.method public getOpacity()I
    .registers 2

    .line 282
    const/4 v0, -0x3

    return v0
.end method

.method public inflate(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .line 93
    sget-object v0, Lbr/com/itau/textovoz/R$styleable;->FontIconDrawable:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 94
    .local v2, "a":Landroid/content/res/TypedArray;
    if-nez v2, :cond_14

    .line 95
    const-class v0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "inflate failed: r.obtainAttributes() returns null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    return-void

    .line 102
    :cond_14
    :try_start_14
    sget v0, Lbr/com/itau/textovoz/R$styleable;->FontIconDrawable_text:I

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->text:Ljava/lang/String;

    .line 103
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->text:Ljava/lang/String;

    if-nez v0, :cond_24

    .line 104
    const-string v0, ""

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->text:Ljava/lang/String;

    .line 107
    :cond_24
    sget v0, Lbr/com/itau/textovoz/R$styleable;->FontIconDrawable_textColor:I

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->textColor:Landroid/content/res/ColorStateList;

    .line 108
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->textColor:Landroid/content/res/ColorStateList;

    if-nez v0, :cond_37

    .line 109
    const/4 v0, 0x0

    invoke-static {v0}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->textColor:Landroid/content/res/ColorStateList;

    .line 112
    :cond_37
    sget v0, Lbr/com/itau/textovoz/R$styleable;->FontIconDrawable_textSize:I

    const/high16 v1, 0x41100000    # 9.0f

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->textSize:F

    .line 114
    sget v0, Lbr/com/itau/textovoz/R$styleable;->FontIconDrawable_autoMirrored:I

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->autoMirrored:Z

    .line 115
    sget v0, Lbr/com/itau/textovoz/R$styleable;->FontIconDrawable_needMirroring:I

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->needMirroring:Z
    :try_end_53
    .catchall {:try_start_14 .. :try_end_53} :catchall_57

    .line 117
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 118
    goto :goto_5c

    .line 117
    :catchall_57
    move-exception v3

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    throw v3

    .line 120
    :goto_5c
    const/4 v0, 0x1

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->updatePaint(ZZ)V

    .line 121
    return-void
.end method

.method public isAutoMirrored()Z
    .registers 2

    .line 250
    iget-boolean v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->autoMirrored:Z

    return v0
.end method

.method public isNeedMirroring()Z
    .registers 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 259
    iget-boolean v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->needMirroring:Z

    return v0
.end method

.method public isStateful()Z
    .registers 2

    .line 185
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->textColor:Landroid/content/res/ColorStateList;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v0

    return v0
.end method

.method protected needMirroring()Z
    .registers 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .line 302
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_b

    .line 304
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->isNeedMirroring()Z

    move-result v0

    return v0

    .line 306
    :cond_b
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->isAutoMirrored()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-static {p0}, Lbr/com/itau/textovoz/ui/view/FontIconDrawable$MethodGetLayoutDirection;->invoke(Landroid/graphics/drawable/Drawable;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1a

    const/4 v0, 0x1

    goto :goto_1b

    :cond_1a
    const/4 v0, 0x0

    :goto_1b
    return v0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .registers 3
    .param p1, "bounds"    # Landroid/graphics/Rect;

    .line 286
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->boundsChanged:Z

    .line 287
    return-void
.end method

.method protected onStateChange([I)Z
    .registers 3
    .param p1, "state"    # [I

    .line 190
    invoke-direct {p0, p1}, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->updatePaintColor([I)Z

    move-result v0

    return v0
.end method

.method public setAlpha(I)V
    .registers 3
    .param p1, "alpha"    # I

    .line 270
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->paint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 271
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->invalidateSelf()V

    .line 272
    return-void
.end method

.method public setAutoMirrored(Z)V
    .registers 2
    .param p1, "autoMirrored"    # Z

    .line 254
    iput-boolean p1, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->autoMirrored:Z

    .line 255
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .registers 3
    .param p1, "cf"    # Landroid/graphics/ColorFilter;

    .line 276
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->paint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 277
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->invalidateSelf()V

    .line 278
    return-void
.end method

.class public final Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;
.super Ljava/lang/Object;
.source "FSize.java"


# instance fields
.field public final height:F

.field public final width:F


# direct methods
.method public constructor <init>(FF)V
    .registers 3
    .param p1, "width"    # F
    .param p2, "height"    # F

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;->width:F

    .line 17
    iput p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;->height:F

    .line 18
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1, "obj"    # Ljava/lang/Object;

    .line 25
    if-nez p1, :cond_4

    .line 26
    const/4 v0, 0x0

    return v0

    .line 28
    :cond_4
    if-ne p0, p1, :cond_8

    .line 29
    const/4 v0, 0x1

    return v0

    .line 31
    :cond_8
    instance-of v0, p1, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    if-eqz v0, :cond_23

    .line 32
    move-object v2, p1

    check-cast v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    .line 33
    .local v2, "other":Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;->width:F

    iget v1, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;->width:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_21

    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;->height:F

    iget v1, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;->height:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_21

    const/4 v0, 0x1

    goto :goto_22

    :cond_21
    const/4 v0, 0x0

    :goto_22
    return v0

    .line 35
    .end local v2    # "other":Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;
    :cond_23
    const/4 v0, 0x0

    return v0
.end method

.method public hashCode()I
    .registers 3

    .line 50
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;->width:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;->height:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;->width:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;->height:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public final Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView_;
.super Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView;
.source "ProdutoMultilimiteView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 34
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView;-><init>(Landroid/content/Context;)V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView_;->alreadyInflated_:Z

    .line 31
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 35
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView_;->init_()V

    .line 36
    return-void
.end method

.method public static build(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 39
    new-instance v0, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView_;-><init>(Landroid/content/Context;)V

    .line 40
    .local v0, "instance":Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView_;
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView_;->onFinishInflate()V

    .line 41
    return-object v0
.end method

.method private init_()V
    .registers 3

    .line 61
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 62
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 63
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 64
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 52
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView_;->alreadyInflated_:Z

    .line 54
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03010f

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 57
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView;->onFinishInflate()V

    .line 58
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 68
    const v0, 0x7f0e0546

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView_;->textoTipo:Landroid/widget/TextView;

    .line 69
    const v0, 0x7f0e0547

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/view/ProdutoMultilimiteView_;->imagemSetaDireita:Landroid/widget/ImageView;

    .line 70
    return-void
.end method

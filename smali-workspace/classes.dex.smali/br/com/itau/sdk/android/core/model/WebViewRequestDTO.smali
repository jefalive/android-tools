.class public final Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;
.super Lbr/com/itau/sdk/android/core/model/RequestDTO;
.source "SourceFile"


# instance fields
.field private op:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "op"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 6
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/model/RequestDTO;-><init>()V

    return-void
.end method


# virtual methods
.method public copy(Lbr/com/itau/sdk/android/core/model/RequestDTO;)V
    .registers 3

    .line 24
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;->getBody()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbr/com/itau/sdk/android/core/model/RequestDTO;->setBody(Ljava/lang/Object;)V

    .line 25
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;->getHeader()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbr/com/itau/sdk/android/core/model/RequestDTO;->addAllHeader(Ljava/util/List;)V

    .line 26
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;->getPath()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbr/com/itau/sdk/android/core/model/RequestDTO;->addAllPath(Ljava/util/List;)V

    .line 27
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;->getQuery()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbr/com/itau/sdk/android/core/model/RequestDTO;->addAllQuery(Ljava/util/List;)V

    .line 28
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;->getMethod()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbr/com/itau/sdk/android/core/model/RequestDTO;->setMethod(Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method public getOp()Ljava/lang/String;
    .registers 2

    .line 12
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;->op:Ljava/lang/String;

    return-object v0
.end method

.method public setMethod(Ljava/lang/String;)V
    .registers 2

    .line 20
    invoke-super {p0, p1}, Lbr/com/itau/sdk/android/core/model/RequestDTO;->setMethod(Ljava/lang/String;)V

    .line 21
    return-void
.end method

.method public setOp(Ljava/lang/String;)V
    .registers 2

    .line 16
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;->op:Ljava/lang/String;

    .line 17
    return-void
.end method

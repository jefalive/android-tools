.class public Lcom/itau/empresas/AlarmeUtils;
.super Ljava/lang/Object;
.source "AlarmeUtils.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static agendarAlarmeComRepeticao(Landroid/content/Context;IJJ)V
    .registers 16
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "requestCode"    # I
    .param p2, "time"    # J
    .param p4, "intervalo"    # J

    .line 40
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/app/AlarmManager;

    .line 41
    .local v7, "alarmeManager":Landroid/app/AlarmManager;
    new-instance v8, Landroid/content/Intent;

    const-class v0, Lcom/itau/empresas/receiver/AlarmeReceiver_;

    invoke-direct {v8, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 43
    .local v8, "alarmeIntent":Landroid/content/Intent;
    const-string v0, "alarmeNotification"

    invoke-virtual {v8, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 45
    .line 46
    const/high16 v0, 0x8000000

    invoke-static {p0, p1, v8, v0}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    .line 48
    .local v9, "alarmePendingIntent":Landroid/app/PendingIntent;
    move-object v0, v7

    .line 50
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    add-long v2, v1, p2

    move-wide v4, p4

    move-object v6, v9

    .line 49
    const/4 v1, 0x2

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 52
    return-void
.end method

.method public static agendarAlarmeUnico(Landroid/content/Context;IJ)V
    .registers 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "requestCode"    # I
    .param p2, "time"    # J

    .line 56
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/app/AlarmManager;

    .line 57
    .local v3, "alarmeManager":Landroid/app/AlarmManager;
    new-instance v4, Landroid/content/Intent;

    const-class v0, Lcom/itau/empresas/receiver/AlarmeReceiver_;

    invoke-direct {v4, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 59
    .local v4, "alarmeIntent":Landroid/content/Intent;
    const-string v0, "alarmeNotification"

    invoke-virtual {v4, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 61
    const/high16 v0, 0x8000000

    invoke-static {p0, p1, v4, v0}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 64
    .line 65
    .local v5, "alarmePendingIntent":Landroid/app/PendingIntent;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    add-long/2addr v0, p2

    const/4 v2, 0x2

    invoke-virtual {v3, v2, v0, v1, v5}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 67
    return-void
.end method

.method public static buscarDatetimeAlarme(Ljava/lang/String;Lcom/itau/empresas/CustomApplication;)J
    .registers 5
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "application"    # Lcom/itau/empresas/CustomApplication;

    .line 113
    invoke-virtual {p1}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-wide/16 v1, -0x1

    invoke-interface {v0, p0, v1, v2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static cancelarAlarme(Landroid/content/Context;I)V
    .registers 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "requestCode"    # I

    .line 77
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/AlarmManager;

    .line 78
    .local v1, "alarmeManager":Landroid/app/AlarmManager;
    new-instance v2, Landroid/content/Intent;

    const-class v0, Lcom/itau/empresas/receiver/AlarmeReceiver_;

    invoke-direct {v2, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 83
    .local v2, "alarmeIntent":Landroid/content/Intent;
    const-string v0, "alarmeNotification"

    invoke-virtual {v2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 86
    .line 87
    const/high16 v0, 0x8000000

    invoke-static {p0, p1, v2, v0}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 89
    .local v3, "alarmePendingIntent":Landroid/app/PendingIntent;
    invoke-virtual {v1, v3}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 90
    return-void
.end method

.method public static reagendarAlarme(Landroid/content/Context;IJJ)V
    .registers 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "requestCode"    # I
    .param p2, "time"    # J
    .param p4, "intervalo"    # J

    .line 71
    invoke-static {p0, p1}, Lcom/itau/empresas/AlarmeUtils;->cancelarAlarme(Landroid/content/Context;I)V

    .line 72
    invoke-static/range {p0 .. p5}, Lcom/itau/empresas/AlarmeUtils;->agendarAlarmeComRepeticao(Landroid/content/Context;IJJ)V

    .line 73
    return-void
.end method

.method public static salvaLogoutUsuario(Lcom/itau/empresas/CustomApplication;)V
    .registers 4
    .param p0, "application"    # Lcom/itau/empresas/CustomApplication;

    .line 101
    invoke-virtual {p0}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 103
    .local v2, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v0, "usuario_fez_logout"

    const/4 v1, 0x1

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 104
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 105
    return-void
.end method

.method public static salvarDatetimeAlarme(Ljava/lang/String;JLcom/itau/empresas/CustomApplication;)V
    .registers 6
    .param p0, "dataKey"    # Ljava/lang/String;
    .param p1, "datetime"    # J
    .param p3, "application"    # Lcom/itau/empresas/CustomApplication;

    .line 94
    invoke-virtual {p3}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 96
    .local v1, "ed":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v1, p0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 97
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 98
    return-void
.end method

.method public static usuarioEfetuouLogout(Lcom/itau/empresas/CustomApplication;)Z
    .registers 4
    .param p0, "application"    # Lcom/itau/empresas/CustomApplication;

    .line 109
    invoke-virtual {p0}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "usuario_fez_logout"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static zerarDatetimeAlarme(Ljava/lang/String;Lcom/itau/empresas/CustomApplication;)V
    .registers 4
    .param p0, "key"    # Ljava/lang/String;
    .param p1, "application"    # Lcom/itau/empresas/CustomApplication;

    .line 117
    const-wide/16 v0, 0x0

    invoke-static {p0, v0, v1, p1}, Lcom/itau/empresas/AlarmeUtils;->salvarDatetimeAlarme(Ljava/lang/String;JLcom/itau/empresas/CustomApplication;)V

    .line 118
    return-void
.end method

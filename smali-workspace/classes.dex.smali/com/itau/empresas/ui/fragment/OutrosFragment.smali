.class public Lcom/itau/empresas/ui/fragment/OutrosFragment;
.super Lcom/itau/empresas/ui/fragment/BaseFragment;
.source "OutrosFragment.java"


# instance fields
.field linhaOutrosAplicativos:Landroid/view/View;

.field linhaOutrosBeta:Landroid/view/View;

.field linhaOutrosContato:Landroid/view/View;

.field rlOutrosAplicativos:Landroid/widget/RelativeLayout;

.field rlOutrosBeta:Landroid/widget/RelativeLayout;

.field rlOutrosContato:Landroid/widget/RelativeLayout;

.field startedFrom:Ljava/lang/String;

.field textoAplicativo:Landroid/widget/TextView;

.field textoAvaliar:Landroid/widget/TextView;

.field textoDataAtualizacao:Landroid/widget/TextView;

.field textoRecomendar:Landroid/widget/TextView;

.field textoSeguranca:Landroid/widget/TextView;

.field textoTermosDeUso:Landroid/widget/TextView;

.field textoVersao:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 31
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;-><init>()V

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment;->startedFrom:Ljava/lang/String;

    return-void
.end method

.method private eventoAnalytics(Landroid/widget/TextView;)V
    .registers 4
    .param p1, "tv"    # Landroid/widget/TextView;

    .line 181
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/GenericaActivity;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/activity/GenericaActivity;->analyticsHit(Ljava/lang/String;)V

    .line 182
    return-void
.end method

.method private isLogado()Z
    .registers 2

    .line 192
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getMenuVO()Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method private mostraItensAreaNaoLogada()V
    .registers 3

    .line 201
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment;->linhaOutrosContato:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 202
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment;->linhaOutrosBeta:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 203
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment;->linhaOutrosAplicativos:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 205
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment;->rlOutrosContato:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 206
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment;->rlOutrosAplicativos:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 207
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment;->rlOutrosBeta:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 208
    return-void
.end method


# virtual methods
.method aoClicarEmAplicativos()V
    .registers 4

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment;->textoAplicativo:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->eventoAnalytics(Landroid/widget/TextView;)V

    .line 69
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->isLogado()Z

    move-result v0

    if-nez v0, :cond_23

    .line 70
    const v0, 0x7f0700d5

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 71
    const v1, 0x7f0700ca

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 72
    const v2, 0x7f0700c4

    invoke-virtual {p0, v2}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 70
    invoke-virtual {p0, v0, v1, v2}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->eventoAdobe(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :cond_23
    invoke-static {p0}, Lcom/itau/empresas/ui/activity/AplicativosActivity;->intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/ui/activity/AplicativosActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/AplicativosActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 76
    return-void
.end method

.method aoClicarEmAvaliar()V
    .registers 11

    .line 117
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment;->textoAvaliar:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->eventoAnalytics(Landroid/widget/TextView;)V

    .line 121
    :try_start_5
    new-instance v4, Landroid/content/Intent;

    const-string v0, "android.intent.action.SEND"

    invoke-direct {v4, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 122
    .local v4, "intent":Landroid/content/Intent;
    const-string v0, "text/plain"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 123
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 124
    .local v5, "pm":Landroid/content/pm/PackageManager;
    const/4 v0, 0x0

    invoke-virtual {v5, v4, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    .line 125
    .local v6, "matches":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v7, 0x0

    .line 126
    .local v7, "best":Landroid/content/pm/ResolveInfo;
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_23
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9f

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/content/pm/ResolveInfo;

    .line 127
    .local v9, "info":Landroid/content/pm/ResolveInfo;
    iget-object v0, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    const-string v1, ".gm"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9c

    iget-object v0, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 128
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "gmail"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9c

    iget-object v0, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 129
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mail"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9c

    iget-object v0, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 130
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "inbox"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9c

    iget-object v0, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 131
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "outlook"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9c

    iget-object v0, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 132
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "wl"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9c

    iget-object v0, v9, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 133
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "email"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9d

    .line 135
    :cond_9c
    move-object v7, v9

    .line 137
    .end local v9    # "info":Landroid/content/pm/ResolveInfo;
    :cond_9d
    goto/16 :goto_23

    .line 138
    :cond_9f
    if-eqz v7, :cond_c4

    .line 139
    iget-object v0, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v1, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 140
    const-string v0, "android.intent.extra.EMAIL"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "atendimento.appempresas@itau-unibanco.com.br"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 142
    const-string v0, "android.intent.extra.SUBJECT"

    const-string v1, "Feedback do App Ita\u00fa Empresas - app vers\u00e3o 4.0.3"

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    invoke-virtual {p0, v4}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_df

    .line 146
    :cond_c4
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 147
    const v1, 0x7f07046d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 148
    const v1, 0x7f07048c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 149
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;
    :try_end_df
    .catch Landroid/content/ActivityNotFoundException; {:try_start_5 .. :try_end_df} :catch_e0

    .line 156
    .end local v4    # "intent":Landroid/content/Intent;
    .end local v5    # "pm":Landroid/content/pm/PackageManager;
    .end local v6    # "matches":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v6
    .end local v7    # "best":Landroid/content/pm/ResolveInfo;
    :goto_df
    goto :goto_fc

    .line 151
    :catch_e0
    move-exception v4

    .line 152
    .local v4, "anfe":Landroid/content/ActivityNotFoundException;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 153
    const v1, 0x7f07046d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 154
    const v1, 0x7f07048c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 155
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 157
    .end local v4    # "anfe":Landroid/content/ActivityNotFoundException;
    :goto_fc
    return-void
.end method

.method aoClicarEmBeta()V
    .registers 3

    .line 171
    invoke-static {p0}, Lcom/itau/empresas/ui/activity/BetaActivity;->intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/ui/activity/BetaActivity_$IntentBuilder_;

    move-result-object v0

    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->isLogado()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/activity/BetaActivity_$IntentBuilder_;->isLogado(Z)Lcom/itau/empresas/ui/activity/BetaActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BetaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 172
    return-void
.end method

.method aoClicarEmContato()V
    .registers 4

    .line 161
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->isLogado()Z

    move-result v0

    if-nez v0, :cond_1e

    .line 162
    const v0, 0x7f0700dc

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 163
    const v1, 0x7f0700ca

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 164
    const v2, 0x7f0700c4

    invoke-virtual {p0, v2}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 162
    invoke-virtual {p0, v0, v1, v2}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->eventoAdobe(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :cond_1e
    invoke-static {p0}, Lcom/itau/empresas/ui/activity/ContatosActivity;->intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/ui/activity/ContatosActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/ContatosActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 167
    return-void
.end method

.method aoClicarEmMapa()V
    .registers 2

    .line 176
    invoke-static {p0}, Lcom/itau/empresas/feature/mapa/MapaActivity;->intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/feature/mapa/MapaActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/mapa/MapaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 177
    return-void
.end method

.method aoClicarEmRecomendar()V
    .registers 4

    .line 93
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment;->textoRecomendar:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->eventoAnalytics(Landroid/widget/TextView;)V

    .line 94
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 95
    .local v2, "sendIntent":Landroid/content/Intent;
    const-string v0, "android.intent.action.SEND"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 96
    const-string v0, "android.intent.extra.TEXT"

    .line 97
    const v1, 0x7f07056f

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 96
    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 98
    const-string v0, "text/plain"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    invoke-virtual {p0, v2}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->startActivity(Landroid/content/Intent;)V

    .line 100
    return-void
.end method

.method aoClicarEmSeguranca()V
    .registers 4

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment;->textoSeguranca:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->eventoAnalytics(Landroid/widget/TextView;)V

    .line 82
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->isLogado()Z

    move-result v0

    if-nez v0, :cond_23

    .line 83
    const v0, 0x7f0700d7

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 84
    const v1, 0x7f0700ca

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 85
    const v2, 0x7f0700c4

    invoke-virtual {p0, v2}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 83
    invoke-virtual {p0, v0, v1, v2}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->eventoAdobe(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    :cond_23
    invoke-static {p0}, Lcom/itau/empresas/ui/activity/SegurancaActivity;->intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/ui/activity/SegurancaActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/SegurancaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 89
    return-void
.end method

.method aoClicarEmTermosDeUso()V
    .registers 4

    .line 104
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment;->textoTermosDeUso:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->eventoAnalytics(Landroid/widget/TextView;)V

    .line 106
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->isLogado()Z

    move-result v0

    if-nez v0, :cond_23

    .line 107
    const v0, 0x7f0700d8

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 108
    const v1, 0x7f0700ca

    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 109
    const v2, 0x7f0700c4

    invoke-virtual {p0, v2}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 107
    invoke-virtual {p0, v0, v1, v2}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->eventoAdobe(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :cond_23
    invoke-static {p0}, Lcom/itau/empresas/ui/activity/TermosDeUsoActivity;->intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/ui/activity/TermosDeUsoActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/TermosDeUsoActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 113
    return-void
.end method

.method aoIniciarActivity()V
    .registers 5

    .line 54
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->setTitle()V

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment;->textoVersao:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment;->textoVersao:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "4.0.3"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment;->textoDataAtualizacao:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment;->textoDataAtualizacao:Landroid/widget/TextView;

    .line 57
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 58
    const v3, 0x7f0701cd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 56
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    const-string v0, "outros"

    iget-object v1, p0, Lcom/itau/empresas/ui/fragment/OutrosFragment;->startedFrom:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_61

    .line 61
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->mostraItensAreaNaoLogada()V

    .line 63
    :cond_61
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 197
    const/4 v0, 0x0

    return-object v0
.end method

.method public setTitle()V
    .registers 3

    .line 185
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/OutrosFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 186
    .local v1, "activity":Landroid/app/Activity;
    if-eqz v1, :cond_c

    .line 187
    const v0, 0x7f070576

    invoke-virtual {v1, v0}, Landroid/app/Activity;->setTitle(I)V

    .line 189
    :cond_c
    return-void
.end method

.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/AxisRenderer;
.source "YAxisRenderer.java"


# instance fields
.field protected mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;


# direct methods
.method public constructor <init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;)V
    .registers 6
    .param p1, "viewPortHandler"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;
    .param p2, "yAxis"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;
    .param p3, "trans"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;

    .line 24
    invoke-direct {p0, p1, p3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/AxisRenderer;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;)V

    .line 26
    iput-object p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    .line 28
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 29
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41200000    # 10.0f

    invoke-static {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 30
    return-void
.end method


# virtual methods
.method public computeAxis(FF)V
    .registers 8
    .param p1, "yMin"    # F
    .param p2, "yMax"    # F

    .line 42
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentWidth()F

    move-result v0

    const/high16 v1, 0x41200000    # 10.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4d

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->isFullyZoomedOutY()Z

    move-result v0

    if-nez v0, :cond_4d

    .line 44
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mTrans:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentLeft()F

    move-result v1

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 45
    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentTop()F

    move-result v2

    .line 44
    invoke-virtual {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->getValuesByTouchPoint(FF)Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/PointD;

    move-result-object v3

    .line 46
    .local v3, "p1":Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/PointD;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mTrans:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentLeft()F

    move-result v1

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 47
    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentBottom()F

    move-result v2

    .line 46
    invoke-virtual {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->getValuesByTouchPoint(FF)Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/PointD;

    move-result-object v4

    .line 49
    .local v4, "p2":Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/PointD;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->isInverted()Z

    move-result v0

    if-nez v0, :cond_47

    .line 50
    iget-wide v0, v4, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/PointD;->y:D

    double-to-float p1, v0

    .line 51
    iget-wide v0, v3, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/PointD;->y:D

    double-to-float p2, v0

    goto :goto_4d

    .line 54
    :cond_47
    iget-wide v0, v3, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/PointD;->y:D

    double-to-float p1, v0

    .line 55
    iget-wide v0, v4, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/PointD;->y:D

    double-to-float p2, v0

    .line 59
    .end local v3    # "p1":Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/PointD;
    .end local v4    # "p2":Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/PointD;
    :cond_4d
    :goto_4d
    invoke-virtual {p0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->computeAxisValues(FF)V

    .line 60
    return-void
.end method

.method protected computeAxisValues(FF)V
    .registers 27
    .param p1, "min"    # F
    .param p2, "max"    # F

    .line 71
    move/from16 v4, p1

    .line 72
    .local v4, "yMin":F
    move/from16 v5, p2

    .line 74
    .local v5, "yMax":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getLabelCount()I

    move-result v6

    .line 75
    .local v6, "labelCount":I
    sub-float v0, v5, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v7, v0

    .line 77
    .local v7, "range":D
    if-eqz v6, :cond_1b

    const-wide/16 v0, 0x0

    cmpg-double v0, v7, v0

    if-gtz v0, :cond_2c

    .line 78
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    const/4 v1, 0x0

    new-array v1, v1, [F

    iput-object v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    .line 79
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    const/4 v1, 0x0

    iput v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntryCount:I

    .line 80
    return-void

    .line 83
    :cond_2c
    int-to-double v0, v6

    div-double v9, v7, v0

    .line 84
    .local v9, "rawInterval":D
    invoke-static {v9, v10}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->roundToNextSignificant(D)F

    move-result v0

    float-to-double v11, v0

    .line 85
    .local v11, "interval":D
    invoke-static {v11, v12}, Ljava/lang/Math;->log10(D)D

    move-result-wide v0

    double-to-int v0, v0

    int-to-double v0, v0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v13

    .line 86
    .local v13, "intervalMagnitude":D
    div-double v0, v11, v13

    double-to-int v15, v0

    .line 87
    .local v15, "intervalSigDigit":I
    const/4 v0, 0x5

    if-le v15, v0, :cond_4d

    .line 90
    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    mul-double/2addr v0, v13

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v11

    .line 94
    :cond_4d
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->isShowOnlyMinMaxEnabled()Z

    move-result v0

    if-eqz v0, :cond_7b

    .line 96
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    const/4 v1, 0x2

    iput v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntryCount:I

    .line 97
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    const/4 v1, 0x2

    new-array v1, v1, [F

    iput-object v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    .line 98
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    const/4 v1, 0x0

    aput v4, v0, v1

    .line 99
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    const/4 v1, 0x1

    aput v5, v0, v1

    goto/16 :goto_d2

    .line 103
    :cond_7b
    float-to-double v0, v4

    div-double/2addr v0, v11

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    mul-double v16, v0, v11

    .line 104
    .local v16, "first":D
    float-to-double v0, v5

    div-double/2addr v0, v11

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    mul-double/2addr v0, v11

    invoke-static {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->nextUp(D)D

    move-result-wide v18

    .line 108
    .local v18, "last":D
    const/16 v23, 0x0

    .line 109
    .local v23, "n":I
    move-wide/from16 v20, v16

    .local v20, "f":D
    :goto_92
    cmpg-double v0, v20, v18

    if-gtz v0, :cond_9b

    .line 110
    add-int/lit8 v23, v23, 0x1

    .line 109
    add-double v20, v20, v11

    goto :goto_92

    .line 113
    :cond_9b
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    move/from16 v1, v23

    iput v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntryCount:I

    .line 115
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    array-length v0, v0

    move/from16 v1, v23

    if-ge v0, v1, :cond_b8

    .line 117
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    move/from16 v1, v23

    new-array v1, v1, [F

    iput-object v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    .line 120
    :cond_b8
    move-wide/from16 v20, v16

    const/16 v22, 0x0

    .local v22, "i":I
    :goto_bc
    move/from16 v0, v22

    move/from16 v1, v23

    if-ge v0, v1, :cond_d2

    .line 121
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    move-wide/from16 v1, v20

    double-to-float v1, v1

    aput v1, v0, v22

    .line 120
    add-double v20, v20, v11

    add-int/lit8 v22, v22, 0x1

    goto :goto_bc

    .line 125
    .end local v16    # "first":D
    .end local v18    # "last":D
    .end local v20    # "f":D
    .end local v22    # "i":I
    .end local v23    # "n":I
    :cond_d2
    :goto_d2
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpg-double v0, v11, v0

    if-gez v0, :cond_e9

    .line 126
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-static {v11, v12}, Ljava/lang/Math;->log10(D)D

    move-result-wide v1

    neg-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v1, v1

    iput v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mDecimals:I

    goto :goto_f0

    .line 128
    :cond_e9
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    const/4 v1, 0x0

    iput v1, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mDecimals:I

    .line 130
    :goto_f0
    return-void
.end method

.method protected drawYLabels(Landroid/graphics/Canvas;F[FF)V
    .registers 9
    .param p1, "c"    # Landroid/graphics/Canvas;
    .param p2, "fixedPosition"    # F
    .param p3, "positions"    # [F
    .param p4, "offset"    # F

    .line 217
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntryCount:I

    if-ge v2, v0, :cond_2d

    .line 219
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getFormattedLabel(I)Ljava/lang/String;

    move-result-object v3

    .line 221
    .local v3, "text":Ljava/lang/String;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->isDrawTopYLabelEntryEnabled()Z

    move-result v0

    if-nez v0, :cond_1e

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntryCount:I

    add-int/lit8 v0, v0, -0x1

    if-lt v2, v0, :cond_1e

    .line 222
    return-void

    .line 224
    :cond_1e
    mul-int/lit8 v0, v2, 0x2

    add-int/lit8 v0, v0, 0x1

    aget v0, p3, v0

    add-float/2addr v0, p4

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, p2, v0, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 217
    .end local v3    # "text":Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 226
    .end local v2    # "i":I
    :cond_2d
    return-void
.end method

.method public renderAxisLabels(Landroid/graphics/Canvas;)V
    .registers 11
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 138
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->isDrawLabelsEnabled()Z

    move-result v0

    if-nez v0, :cond_11

    .line 139
    :cond_10
    return-void

    .line 141
    :cond_11
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntryCount:I

    mul-int/lit8 v0, v0, 0x2

    new-array v3, v0, [F

    .line 143
    .local v3, "positions":[F
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1a
    array-length v0, v3

    if-ge v4, v0, :cond_2c

    .line 147
    add-int/lit8 v0, v4, 0x1

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    div-int/lit8 v2, v4, 0x2

    aget v1, v1, v2

    aput v1, v3, v0

    .line 143
    add-int/lit8 v4, v4, 0x2

    goto :goto_1a

    .line 150
    .end local v4    # "i":I
    :cond_2c
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mTrans:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;

    invoke-virtual {v0, v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->pointValuesToPixel([F)V

    .line 152
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 153
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getTextSize()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 154
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 156
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getXOffset()F

    move-result v4

    .line 157
    .local v4, "xoffset":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    const-string v1, "A"

    invoke-static {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->calcTextHeight(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40200000    # 2.5f

    div-float/2addr v0, v1

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getYOffset()F

    move-result v1

    add-float v5, v0, v1

    .line 159
    .local v5, "yoffset":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getAxisDependency()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    move-result-object v6

    .line 160
    .local v6, "dependency":Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getLabelPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    move-result-object v7

    .line 162
    .local v7, "labelPosition":Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;
    const/4 v8, 0x0

    .line 164
    .local v8, "xPos":F
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    if-ne v6, v0, :cond_a1

    .line 166
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;->OUTSIDE_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    if-ne v7, v0, :cond_91

    .line 167
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 168
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->offsetLeft()F

    move-result v0

    sub-float v8, v0, v4

    goto :goto_c4

    .line 170
    :cond_91
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 171
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->offsetLeft()F

    move-result v0

    add-float v8, v0, v4

    goto :goto_c4

    .line 176
    :cond_a1
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;->OUTSIDE_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$YAxisLabelPosition;

    if-ne v7, v0, :cond_b5

    .line 177
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 178
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentRight()F

    move-result v0

    add-float v8, v0, v4

    goto :goto_c4

    .line 180
    :cond_b5
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mAxisLabelPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 181
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentRight()F

    move-result v0

    sub-float v8, v0, v4

    .line 185
    :goto_c4
    invoke-virtual {p0, p1, v8, v3, v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->drawYLabels(Landroid/graphics/Canvas;F[FF)V

    .line 186
    return-void
.end method

.method public renderAxisLine(Landroid/graphics/Canvas;)V
    .registers 8
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 191
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->isDrawAxisLineEnabled()Z

    move-result v0

    if-nez v0, :cond_11

    .line 192
    :cond_10
    return-void

    .line 194
    :cond_11
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mAxisLinePaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getAxisLineColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 195
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mAxisLinePaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getAxisLineWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 197
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getAxisDependency()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;->LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;

    if-ne v0, v1, :cond_50

    .line 198
    move-object v0, p1

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentLeft()F

    move-result v1

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 199
    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentTop()F

    move-result v2

    iget-object v3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentLeft()F

    move-result v3

    iget-object v4, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 200
    invoke-virtual {v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentBottom()F

    move-result v4

    iget-object v5, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mAxisLinePaint:Landroid/graphics/Paint;

    .line 198
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_6e

    .line 202
    :cond_50
    move-object v0, p1

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentRight()F

    move-result v1

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 203
    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentTop()F

    move-result v2

    iget-object v3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentRight()F

    move-result v3

    iget-object v4, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 204
    invoke-virtual {v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentBottom()F

    move-result v4

    iget-object v5, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mAxisLinePaint:Landroid/graphics/Paint;

    .line 202
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 206
    :goto_6e
    return-void
.end method

.method public renderGridLines(Landroid/graphics/Canvas;)V
    .registers 7
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 231
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->isDrawGridLinesEnabled()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_11

    .line 232
    :cond_10
    return-void

    .line 235
    :cond_11
    const/4 v0, 0x2

    new-array v2, v0, [F

    .line 237
    .local v2, "position":[F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mGridPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getGridColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 238
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mGridPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getGridLineWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 239
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mGridPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getGridDashPathEffect()Landroid/graphics/DashPathEffect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 241
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 244
    .local v3, "gridLinePath":Landroid/graphics/Path;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_3b
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntryCount:I

    if-ge v4, v0, :cond_72

    .line 246
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    aget v0, v0, v4

    const/4 v1, 0x1

    aput v0, v2, v1

    .line 247
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mTrans:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;

    invoke-virtual {v0, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->pointValuesToPixel([F)V

    .line 249
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->offsetLeft()F

    move-result v0

    const/4 v1, 0x1

    aget v1, v2, v1

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 250
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentRight()F

    move-result v0

    const/4 v1, 0x1

    aget v1, v2, v1

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 254
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mGridPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 256
    invoke-virtual {v3}, Landroid/graphics/Path;->reset()V

    .line 244
    add-int/lit8 v4, v4, 0x1

    goto :goto_3b

    .line 258
    .end local v4    # "i":I
    :cond_72
    return-void
.end method

.method public renderLimitLines(Landroid/graphics/Canvas;)V
    .registers 13
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 268
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mYAxis:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->getLimitLines()Ljava/util/List;

    move-result-object v3

    .line 270
    .local v3, "limitLines":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;>;"
    if-eqz v3, :cond_e

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_f

    .line 271
    :cond_e
    return-void

    .line 273
    :cond_f
    const/4 v0, 0x2

    new-array v4, v0, [F

    .line 274
    .local v4, "pts":[F
    new-instance v5, Landroid/graphics/Path;

    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 276
    .local v5, "limitLinePath":Landroid/graphics/Path;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_18
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_fa

    .line 278
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;

    .line 280
    .local v7, "l":Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 281
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getLineColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 282
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getLineWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 283
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getDashPathEffect()Landroid/graphics/DashPathEffect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 285
    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getLimit()F

    move-result v0

    const/4 v1, 0x1

    aput v0, v4, v1

    .line 287
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mTrans:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;

    invoke-virtual {v0, v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;->pointValuesToPixel([F)V

    .line 289
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentLeft()F

    move-result v0

    const/4 v1, 0x1

    aget v1, v4, v1

    invoke-virtual {v5, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 290
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentRight()F

    move-result v0

    const/4 v1, 0x1

    aget v1, v4, v1

    invoke-virtual {v5, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 292
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 293
    invoke-virtual {v5}, Landroid/graphics/Path;->reset()V

    .line 296
    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getLabel()Ljava/lang/String;

    move-result-object v8

    .line 299
    .local v8, "label":Ljava/lang/String;
    if-eqz v8, :cond_f6

    const-string v0, ""

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f6

    .line 301
    const/high16 v0, 0x40800000    # 4.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v9

    .line 302
    .local v9, "xOffset":F
    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getLineWidth()F

    move-result v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-static {v1, v8}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->calcTextHeight(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float v10, v0, v1

    .line 305
    .local v10, "yOffset":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getTextStyle()Landroid/graphics/Paint$Style;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 306
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 307
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 308
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 309
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getTextSize()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 311
    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;->getLabelPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;->POS_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    if-ne v0, v1, :cond_df

    .line 313
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 314
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentRight()F

    move-result v0

    sub-float/2addr v0, v9

    const/4 v1, 0x1

    aget v1, v4, v1

    sub-float/2addr v1, v10

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_f6

    .line 319
    :cond_df
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 320
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->offsetLeft()F

    move-result v0

    add-float/2addr v0, v9

    const/4 v1, 0x1

    aget v1, v4, v1

    sub-float/2addr v1, v10

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/YAxisRenderer;->mLimitLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 276
    .end local v7    # "l":Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;
    .end local v8    # "label":Ljava/lang/String;
    .end local v9    # "xOffset":F
    .end local v10    # "yOffset":F
    :cond_f6
    :goto_f6
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_18

    .line 326
    .end local v6    # "i":I
    :cond_fa
    return-void
.end method

.class public Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "GiroDataActivity.java"

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;


# instance fields
.field campoData:Landroid/widget/TextView;

.field campoValorEmprestimo:Landroid/widget/EditText;

.field private final maxPickerDate:Ljava/util/Calendar;

.field private final minPickerDate:Ljava/util/Calendar;

.field ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

.field private quantidadeMaxDiasPrimeiroPagamento:I

.field private quantidadeMinDiasPrimeiroPagamento:I

.field root:Landroid/widget/RelativeLayout;

.field simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

.field simulacaoPropostasController:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController;

.field textoAviso:Landroid/widget/TextView;

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field private topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 49
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    .line 70
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->minPickerDate:Ljava/util/Calendar;

    .line 71
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->maxPickerDate:Ljava/util/Calendar;

    return-void
.end method

.method static synthetic access$000(Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    .line 49
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->convertaDatePickerParaString(Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private buscaSimulacaoPersonalizada()V
    .registers 3

    .line 126
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->constroiToolbar()V

    .line 127
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->dismissSnackbar()V

    .line 128
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->mostraDialogoDeProgresso()V

    .line 129
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->simulacaoPropostasController:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController;->consultarSimulacaoProposta(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)V

    .line 130
    return-void
.end method

.method private carregarDadosNaTela()V
    .registers 11

    .line 156
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v8

    .line 157
    .local v8, "dataAtual":Ljava/util/Date;
    const/4 v9, 0x0

    .line 159
    .local v9, "dataVencimentoAPI":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getDatasPrimeiroPagamento()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2e

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 160
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getDatasPrimeiroPagamento()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2e

    .line 162
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getDatasPrimeiroPagamento()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/DataPrimeiroPagamentoVO;

    .line 163
    invoke-virtual {v0}, Lcom/itau/empresas/api/model/DataPrimeiroPagamentoVO;->getDatasPrimeiroPagamento()Ljava/lang/String;

    move-result-object v9

    .line 166
    :cond_2e
    invoke-static {v8}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->printData(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    .line 168
    .local v4, "dataAtualFormatada":Ljava/lang/String;
    if-nez v9, :cond_43

    .line 170
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 171
    invoke-static {v8}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->printDataComBarra(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setDataPrimeiroPagamento(Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->campoData:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_51

    .line 177
    :cond_43
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-virtual {v0, v9}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setDataPrimeiroPagamento(Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->campoData:Landroid/widget/TextView;

    invoke-static {v9}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    :goto_51
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->campoValorEmprestimo:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 184
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getValorEmprestimo()Ljava/math/BigDecimal;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v1

    .line 183
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 186
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 187
    .local v7, "calendarFuturo":Ljava/util/Calendar;
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->quantidadeMaxDiasPrimeiroPagamento:I

    const/4 v1, 0x5

    invoke-virtual {v7, v1, v0}, Ljava/util/Calendar;->add(II)V

    .line 188
    invoke-virtual {v7}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->printData(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 190
    .local v5, "dataMaxima":Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 191
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->quantidadeMinDiasPrimeiroPagamento:I

    const/4 v1, 0x5

    invoke-virtual {v7, v1, v0}, Ljava/util/Calendar;->add(II)V

    .line 192
    invoke-virtual {v7}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->printData(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    .line 194
    .local v6, "dataMinima":Ljava/lang/String;
    const v0, 0x7f070562

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v6, v1, v2

    const/4 v2, 0x1

    aput-object v5, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 197
    .local v3, "aviso":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->textoAviso:Landroid/widget/TextView;

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 134
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getDadosSeguroVO()Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;

    move-result-object v0

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 135
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->getDadosSeguroVO()Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;

    move-result-object v0

    .line 136
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;->isIndicadorSeguro()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 137
    const v1, 0x7f070699

    .local v1, "id":I
    goto :goto_1b

    .line 140
    .end local v1    # "id":I
    :cond_18
    const v1, 0x7f07069a

    .line 143
    .local v1, "id":I
    :goto_1b
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 145
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 146
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 147
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 148
    return-void
.end method

.method private static convertaDatePickerParaString(Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;)Ljava/lang/String;
    .registers 7
    .param p0, "datePickerDialog"    # Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    .line 203
    invoke-virtual {p0}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->getSelectedDay()Lcom/wdullaer/materialdatetimepicker/date/MonthAdapter$CalendarDay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/wdullaer/materialdatetimepicker/date/MonthAdapter$CalendarDay;->getDay()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 204
    .local v3, "dia":Ljava/lang/Integer;
    invoke-virtual {p0}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->getSelectedDay()Lcom/wdullaer/materialdatetimepicker/date/MonthAdapter$CalendarDay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/wdullaer/materialdatetimepicker/date/MonthAdapter$CalendarDay;->getMonth()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 205
    .local v4, "mes":Ljava/lang/Integer;
    invoke-virtual {p0}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->getSelectedDay()Lcom/wdullaer/materialdatetimepicker/date/MonthAdapter$CalendarDay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/wdullaer/materialdatetimepicker/date/MonthAdapter$CalendarDay;->getYear()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 207
    .local v5, "ano":Ljava/lang/Integer;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0xa

    if-ge v1, v2, :cond_4b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_4f

    :cond_4b
    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 208
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0xa

    if-ge v1, v2, :cond_79

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_7d

    :cond_79
    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_7d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 209
    invoke-virtual {v5}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 207
    return-object v0
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 316
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method aoCarregarTela()V
    .registers 4

    .line 81
    new-instance v0, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->root:Landroid/widget/RelativeLayout;

    invoke-direct {v0, v1}, Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;-><init>(Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;

    .line 82
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->constroiToolbar()V

    .line 84
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    if-eqz v0, :cond_22

    .line 86
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 87
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getQuantidadeMaximaDiasPrimeiroPagamento()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->quantidadeMaxDiasPrimeiroPagamento:I

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->simulacaoPropostaSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 89
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;->getQuantidadeMinimaDiasPrimeiroPagamento()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->quantidadeMinDiasPrimeiroPagamento:I

    .line 92
    :cond_22
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->minPickerDate:Ljava/util/Calendar;

    iget v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->quantidadeMinDiasPrimeiroPagamento:I

    const/4 v2, 0x5

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    .line 93
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->maxPickerDate:Ljava/util/Calendar;

    iget v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->quantidadeMaxDiasPrimeiroPagamento:I

    const/4 v2, 0x5

    invoke-virtual {v0, v2, v1}, Ljava/util/Calendar;->add(II)V

    .line 95
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->carregarDadosNaTela()V

    .line 97
    return-void
.end method

.method aoClicarNoBotaoContinuar()V
    .registers 1

    .line 119
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->buscaSimulacaoPersonalizada()V

    .line 120
    return-void
.end method

.method aoClicarNoBotaoData()V
    .registers 7

    .line 100
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->campoData:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->minPickerDate:Ljava/util/Calendar;

    .line 101
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->minPickerDate:Ljava/util/Calendar;

    .line 102
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->minPickerDate:Ljava/util/Calendar;

    .line 103
    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 101
    invoke-static {p0, v0, v1, v2, v3}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->mostraDialogoData(Lcom/itau/empresas/ui/activity/BaseActivity;Landroid/widget/TextView;III)Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    move-result-object v5

    .line 104
    .local v5, "dpdPorInicioMes":Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->minPickerDate:Ljava/util/Calendar;

    invoke-virtual {v5, v0}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->setMinDate(Ljava/util/Calendar;)V

    .line 105
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->maxPickerDate:Ljava/util/Calendar;

    invoke-virtual {v5, v0}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->setMaxDate(Ljava/util/Calendar;)V

    .line 106
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity$1;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;)V

    invoke-virtual {v5, v0}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->setOnDateSetListener(Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog$OnDateSetListener;)V

    .line 116
    return-void
.end method

.method public dismissSnackbar()V
    .registers 2

    .line 308
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;

    invoke-interface {v0}, Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;->dismissSnackbar()V

    .line 309
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 2
    .param p1, "v"    # Landroid/view/View;

    .line 304
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->onBackPressed()V

    .line 305
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .registers 7
    .param p1, "view"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .line 225
    invoke-virtual {p1}, Landroid/widget/TextView;->getId()I

    move-result v0

    const v1, 0x7f0e0196

    if-ne v0, v1, :cond_45

    const/4 v0, 0x6

    if-eq p2, v0, :cond_e

    if-nez p2, :cond_45

    .line 228
    :cond_e
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->campoValorEmprestimo:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_40

    .line 230
    const-string v0, "[R$,.]"

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->campoValorEmprestimo:Landroid/widget/EditText;

    .line 234
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 233
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/MascaraValorMonetario;->removeMascara(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 231
    invoke-static {v0}, Lcom/itau/empresas/ui/util/MascaraValorMonetario;->formataStringValorMonetarioParaDecimal(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 236
    .local v2, "valorDigitado":Ljava/math/BigDecimal;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->campoValorEmprestimo:Landroid/widget/EditText;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 238
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    invoke-virtual {v0, v2}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->setValorEmprestimo(Ljava/math/BigDecimal;)V

    .line 242
    .end local v2    # "valorDigitado":Ljava/math/BigDecimal;
    :cond_40
    invoke-static {p0}, Lcom/itau/empresas/ui/util/TecladoUtils;->esconderTeclado(Landroid/app/Activity;)V

    .line 244
    const/4 v0, 0x1

    return v0

    .line 248
    :cond_45
    const/4 v0, 0x0

    return v0
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;

    .line 273
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 274
    return-void

    .line 277
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->escondeDialogoDeProgresso()V

    .line 279
    const-string v0, "ItauEmpresas"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Erro na Chamanda: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 280
    const v0, 0x7f0701f2

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->showSnackbar(Ljava/lang/CharSequence;I)V

    .line 282
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 6
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 290
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 291
    return-void

    .line 294
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->escondeDialogoDeProgresso()V

    .line 296
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v3

    .line 297
    .local v3, "exception":Lbr/com/itau/sdk/android/core/exception/BackendException;
    const-string v0, "ItauEmpresas"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Erro na Chamanda: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 298
    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->showSnackbar(Ljava/lang/CharSequence;I)V

    .line 301
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;)V
    .registers 4
    .param p1, "propostaSaidaVO"    # Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 257
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->escondeDialogoDeProgresso()V

    .line 259
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->ofertaEspecialAceita:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 260
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_$IntentBuilder_;->ofertaEspecialAceita(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_$IntentBuilder_;

    move-result-object v0

    .line 261
    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_$IntentBuilder_;->simulacaoPropostaSaidaVO(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;)Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_$IntentBuilder_;

    move-result-object v0

    .line 262
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_$IntentBuilder_;->modoPersonalizado(Z)Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_$IntentBuilder_;

    move-result-object v0

    .line 263
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/activity/GiroSimulacaoPropostaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 264
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 214
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "creditoParceladoPersonalizadoSimular"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public showSnackbar(Ljava/lang/CharSequence;I)V
    .registers 4
    .param p1, "texto"    # Ljava/lang/CharSequence;
    .param p2, "duracao"    # I

    .line 312
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroDataActivity;->topSnackbar:Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;

    invoke-interface {v0, p1, p2}, Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;->showSnackbar(Ljava/lang/CharSequence;I)V

    .line 313
    return-void
.end method

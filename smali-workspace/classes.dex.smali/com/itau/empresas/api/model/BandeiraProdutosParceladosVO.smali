.class public Lcom/itau/empresas/api/model/BandeiraProdutosParceladosVO;
.super Ljava/lang/Object;
.source "BandeiraProdutosParceladosVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private codigoBandeiraCartao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_bandeira_cartao"
    .end annotation
.end field

.field private descricaoBandeiraCartao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_bandeira_cartao"
    .end annotation
.end field

.field private hasIndicadorTrava:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_trava"
    .end annotation
.end field

.field private situacaoTrava:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "situacao_trava"
    .end annotation
.end field

.field private valorTotal:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_total"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

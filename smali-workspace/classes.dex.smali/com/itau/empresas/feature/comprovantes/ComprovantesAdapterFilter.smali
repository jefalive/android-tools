.class Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterFilter;
.super Landroid/widget/Filter;
.source "ComprovantesAdapterFilter.java"


# instance fields
.field private final comprovantesAdapter:Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;

.field private final context:Landroid/content/Context;

.field private listaComprovantes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;>;"
        }
    .end annotation
.end field

.field private final listaFiltrada:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;Ljava/util/List;Landroid/content/Context;Ljava/util/List;)V
    .registers 5
    .param p1, "comprovantesAdapter"    # Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;
    .param p2, "listaFiltrada"    # Ljava/util/List;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "listaComprovantes"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;Ljava/util/List<Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;>;Landroid/content/Context;Ljava/util/List<Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;>;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    .line 26
    iput-object p2, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterFilter;->listaFiltrada:Ljava/util/List;

    .line 27
    iput-object p3, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterFilter;->context:Landroid/content/Context;

    .line 28
    iput-object p1, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterFilter;->comprovantesAdapter:Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;

    .line 29
    iput-object p4, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterFilter;->listaComprovantes:Ljava/util/List;

    .line 30
    return-void
.end method

.method private fazFiltragem(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;Ljava/util/List;)V
    .registers 6
    .param p1, "busca"    # Ljava/lang/CharSequence;
    .param p2, "resultado"    # Landroid/widget/Filter$FilterResults;
    .param p3, "novaLista"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;Ljava/util/List<Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;>;)V"
        }
    .end annotation

    .line 55
    if-eqz p1, :cond_8

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_11

    .line 56
    :cond_8
    iput-object p3, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 57
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    goto :goto_21

    .line 59
    :cond_11
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 60
    .local v1, "listaComprovantesFiltrada":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;>;"
    invoke-direct {p0, p1, v1}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterFilter;->populaListraComprovanteFitlrada(Ljava/lang/CharSequence;Ljava/util/List;)V

    .line 61
    iput-object v1, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 62
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    .line 64
    .end local v1    # "listaComprovantesFiltrada":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;>;"
    .end local v1
    :goto_21
    return-void
.end method

.method private populaListraComprovanteFitlrada(Ljava/lang/CharSequence;Ljava/util/List;)V
    .registers 7
    .param p1, "busca"    # Ljava/lang/CharSequence;
    .param p2, "listaComprovantesFiltrada"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/CharSequence;Ljava/util/List<Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;>;)V"
        }
    .end annotation

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterFilter;->listaFiltrada:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    .line 69
    .local v3, "c":Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;
    invoke-virtual {v3}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getNomeOperacao()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 70
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3b

    .line 71
    invoke-virtual {v3}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getLiteralIdentificacaoComprovante()Ljava/lang/String;

    move-result-object v0

    .line 72
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 73
    :cond_3b
    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    .end local v3    # "c":Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;
    :cond_3e
    goto :goto_6

    .line 76
    :cond_3f
    return-void
.end method

.method private setaTextoSemComprovante(Landroid/widget/Filter$FilterResults;Landroid/widget/TextView;)V
    .registers 5
    .param p1, "resultado"    # Landroid/widget/Filter$FilterResults;
    .param p2, "textoSemComprovantes"    # Landroid/widget/TextView;

    .line 80
    iget v0, p1, Landroid/widget/Filter$FilterResults;->count:I

    if-lez v0, :cond_a

    .line 81
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1a

    .line 83
    :cond_a
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 84
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterFilter;->context:Landroid/content/Context;

    .line 85
    const v1, 0x7f070196

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    :goto_1a
    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .registers 4
    .param p1, "busca"    # Ljava/lang/CharSequence;

    .line 34
    new-instance v0, Landroid/widget/Filter$FilterResults;

    invoke-direct {v0}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 35
    .local v0, "resultado":Landroid/widget/Filter$FilterResults;
    iget-object v1, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterFilter;->listaFiltrada:Ljava/util/List;

    .line 36
    .local v1, "novaLista":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;>;"
    invoke-direct {p0, p1, v0, v1}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterFilter;->fazFiltragem(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;Ljava/util/List;)V

    .line 37
    return-object v0
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .registers 6
    .param p1, "constraint"    # Ljava/lang/CharSequence;
    .param p2, "resultado"    # Landroid/widget/Filter$FilterResults;

    .line 43
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterFilter;->context:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 44
    const v1, 0x7f0e00f5

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/TextView;

    .line 46
    .local v2, "textoSemComprovantes":Landroid/widget/TextView;
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterFilter;->listaComprovantes:Ljava/util/List;

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterFilter;->comprovantesAdapter:Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->ordenar()V

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterFilter;->comprovantesAdapter:Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->notifyDataSetChanged()V

    .line 50
    invoke-direct {p0, p2, v2}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterFilter;->setaTextoSemComprovante(Landroid/widget/Filter$FilterResults;Landroid/widget/TextView;)V

    .line 51
    return-void
.end method

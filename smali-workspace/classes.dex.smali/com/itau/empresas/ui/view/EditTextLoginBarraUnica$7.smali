.class Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$7;
.super Ljava/lang/Object;
.source "EditTextLoginBarraUnica.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->aposCarregar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;


# direct methods
.method constructor <init>(Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    .line 184
    iput-object p1, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$7;->this$0:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 4
    .param p1, "s"    # Landroid/text/Editable;

    .line 197
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_19

    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$7;->this$0:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    # getter for: Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->listener:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;
    invoke-static {v0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->access$000(Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;)Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 198
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$7;->this$0:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    # getter for: Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->listener:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;
    invoke-static {v0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->access$000(Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;)Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;->preencheuAgenciaContaOuOperador()V

    .line 200
    :cond_19
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .line 188
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .line 193
    return-void
.end method

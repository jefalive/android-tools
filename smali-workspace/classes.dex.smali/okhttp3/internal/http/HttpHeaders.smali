.class public final Lokhttp3/internal/http/HttpHeaders;
.super Ljava/lang/Object;
.source "HttpHeaders.java"


# direct methods
.method public static contentLength(Lokhttp3/Headers;)J
    .registers 3
    .param p0, "headers"    # Lokhttp3/Headers;

    .line 46
    const-string v0, "Content-Length"

    invoke-virtual {p0, v0}, Lokhttp3/Headers;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lokhttp3/internal/http/HttpHeaders;->stringToLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static contentLength(Lokhttp3/Response;)J
    .registers 3
    .param p0, "response"    # Lokhttp3/Response;

    .line 42
    invoke-virtual {p0}, Lokhttp3/Response;->headers()Lokhttp3/Headers;

    move-result-object v0

    invoke-static {v0}, Lokhttp3/internal/http/HttpHeaders;->contentLength(Lokhttp3/Headers;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static hasBody(Lokhttp3/Response;)Z
    .registers 6
    .param p0, "response"    # Lokhttp3/Response;

    .line 193
    invoke-virtual {p0}, Lokhttp3/Response;->request()Lokhttp3/Request;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Request;->method()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HEAD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 194
    const/4 v0, 0x0

    return v0

    .line 197
    :cond_12
    invoke-virtual {p0}, Lokhttp3/Response;->code()I

    move-result v4

    .line 198
    .local v4, "responseCode":I
    const/16 v0, 0x64

    if-lt v4, v0, :cond_1e

    const/16 v0, 0xc8

    if-lt v4, v0, :cond_28

    :cond_1e
    const/16 v0, 0xcc

    if-eq v4, v0, :cond_28

    const/16 v0, 0x130

    if-eq v4, v0, :cond_28

    .line 201
    const/4 v0, 0x1

    return v0

    .line 206
    :cond_28
    invoke-static {p0}, Lokhttp3/internal/http/HttpHeaders;->contentLength(Lokhttp3/Response;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_40

    const-string v0, "chunked"

    const-string v1, "Transfer-Encoding"

    .line 207
    invoke-virtual {p0, v1}, Lokhttp3/Response;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 208
    :cond_40
    const/4 v0, 0x1

    return v0

    .line 211
    :cond_42
    const/4 v0, 0x0

    return v0
.end method

.method public static parseSeconds(Ljava/lang/String;I)I
    .registers 6
    .param p0, "value"    # Ljava/lang/String;
    .param p1, "defaultValue"    # I

    .line 247
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_3} :catch_19

    move-result-wide v2

    .line 248
    .local v2, "seconds":J
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, v2, v0

    if-lez v0, :cond_f

    .line 249
    const v0, 0x7fffffff

    return v0

    .line 250
    :cond_f
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-gez v0, :cond_17

    .line 251
    const/4 v0, 0x0

    return v0

    .line 253
    :cond_17
    long-to-int v0, v2

    return v0

    .line 255
    .end local v2    # "seconds":J
    :catch_19
    move-exception v2

    .line 256
    .local v2, "e":Ljava/lang/NumberFormatException;
    return p1
.end method

.method public static receiveHeaders(Lokhttp3/CookieJar;Lokhttp3/HttpUrl;Lokhttp3/Headers;)V
    .registers 5
    .param p0, "cookieJar"    # Lokhttp3/CookieJar;
    .param p1, "url"    # Lokhttp3/HttpUrl;
    .param p2, "headers"    # Lokhttp3/Headers;

    .line 182
    sget-object v0, Lokhttp3/CookieJar;->NO_COOKIES:Lokhttp3/CookieJar;

    if-ne p0, v0, :cond_5

    return-void

    .line 184
    :cond_5
    invoke-static {p1, p2}, Lokhttp3/Cookie;->parseAll(Lokhttp3/HttpUrl;Lokhttp3/Headers;)Ljava/util/List;

    move-result-object v1

    .line 185
    .local v1, "cookies":Ljava/util/List;, "Ljava/util/List<Lokhttp3/Cookie;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_10

    return-void

    .line 187
    :cond_10
    invoke-interface {p0, p1, v1}, Lokhttp3/CookieJar;->saveFromResponse(Lokhttp3/HttpUrl;Ljava/util/List;)V

    .line 188
    return-void
.end method

.method public static skipUntil(Ljava/lang/String;ILjava/lang/String;)I
    .registers 5
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "pos"    # I
    .param p2, "characters"    # Ljava/lang/String;

    .line 219
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge p1, v0, :cond_15

    .line 220
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_12

    .line 221
    goto :goto_15

    .line 219
    :cond_12
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 224
    :cond_15
    :goto_15
    return p1
.end method

.method public static skipWhitespace(Ljava/lang/String;I)I
    .registers 4
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "pos"    # I

    .line 232
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge p1, v0, :cond_16

    .line 233
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 234
    .local v1, "c":C
    const/16 v0, 0x20

    if-eq v1, v0, :cond_13

    const/16 v0, 0x9

    if-eq v1, v0, :cond_13

    .line 235
    goto :goto_16

    .line 232
    .end local v1    # "c":C
    :cond_13
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 238
    :cond_16
    :goto_16
    return p1
.end method

.method private static stringToLong(Ljava/lang/String;)J
    .registers 4
    .param p0, "s"    # Ljava/lang/String;

    .line 50
    if-nez p0, :cond_5

    const-wide/16 v0, -0x1

    return-wide v0

    .line 52
    :cond_5
    :try_start_5
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_8
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_8} :catch_a

    move-result-wide v0

    return-wide v0

    .line 53
    :catch_a
    move-exception v2

    .line 54
    .local v2, "e":Ljava/lang/NumberFormatException;
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.class public final Lcom/itau/empresas/feature/credito/giro/adapter/EmprestimosParceladosContratadosAdapter_;
.super Lcom/itau/empresas/feature/credito/giro/adapter/EmprestimosParceladosContratadosAdapter;
.source "EmprestimosParceladosContratadosAdapter_.java"


# instance fields
.field private context_:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 19
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/adapter/EmprestimosParceladosContratadosAdapter;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/adapter/EmprestimosParceladosContratadosAdapter_;->context_:Landroid/content/Context;

    .line 21
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/adapter/EmprestimosParceladosContratadosAdapter_;->init_()V

    .line 22
    return-void
.end method

.method public static getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/adapter/EmprestimosParceladosContratadosAdapter_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 25
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/adapter/EmprestimosParceladosContratadosAdapter_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/giro/adapter/EmprestimosParceladosContratadosAdapter_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private init_()V
    .registers 4

    .line 29
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/EmprestimosParceladosContratadosAdapter_;->context_:Landroid/content/Context;

    instance-of v0, v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    if-eqz v0, :cond_d

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/EmprestimosParceladosContratadosAdapter_;->context_:Landroid/content/Context;

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/EmprestimosParceladosContratadosAdapter_;->activity:Lcom/itau/empresas/ui/activity/BaseActivity;

    goto :goto_35

    .line 32
    :cond_d
    const-string v0, "EmprestimosParceladosCo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Due to Context class "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/adapter/EmprestimosParceladosContratadosAdapter_;->context_:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", the @RootContext BaseActivity won\'t be populated"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    :goto_35
    return-void
.end method

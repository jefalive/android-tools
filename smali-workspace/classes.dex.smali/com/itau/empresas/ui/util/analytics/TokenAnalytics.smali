.class public Lcom/itau/empresas/ui/util/analytics/TokenAnalytics;
.super Ljava/lang/Object;
.source "TokenAnalytics.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/SDKTracker;


# instance fields
.field private application:Lcom/itau/empresas/CustomApplication;

.field private tracker:Lcom/itau/empresas/ui/util/analytics/TrackerEvento;


# direct methods
.method public constructor <init>(Lcom/itau/empresas/CustomApplication;Lcom/itau/empresas/ui/util/analytics/TrackerEvento;)V
    .registers 3
    .param p1, "application"    # Lcom/itau/empresas/CustomApplication;
    .param p2, "tracker"    # Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/itau/empresas/ui/util/analytics/TokenAnalytics;->application:Lcom/itau/empresas/CustomApplication;

    .line 16
    iput-object p2, p0, Lcom/itau/empresas/ui/util/analytics/TokenAnalytics;->tracker:Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    .line 17
    return-void
.end method

.method private analytics(I)V
    .registers 3
    .param p1, "acao"    # I

    .line 20
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lcom/itau/empresas/ui/util/analytics/TokenAnalytics;->analytics(II)V

    .line 21
    return-void
.end method

.method private analytics(II)V
    .registers 7
    .param p1, "acao"    # I
    .param p2, "marcador"    # I

    .line 24
    iget-object v0, p0, Lcom/itau/empresas/ui/util/analytics/TokenAnalytics;->tracker:Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/analytics/TokenAnalytics;->application:Lcom/itau/empresas/CustomApplication;

    new-instance v2, Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;

    const v3, 0x7f0702ea

    invoke-direct {v2, v3, p1, p2}, Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;-><init>(III)V

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Landroid/content/Context;Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;)V

    .line 26
    return-void
.end method

.method private analytics(Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;)V
    .registers 4
    .param p1, "evento"    # Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    .line 29
    sget-object v0, Lcom/itau/empresas/ui/util/analytics/TokenAnalytics$1;->$SwitchMap$br$com$itau$sdk$android$core$token$TokenEvent$Action:[I

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_5e

    goto/16 :goto_5d

    .line 31
    :pswitch_d
    const v0, 0x7f0702a3

    const v1, 0x7f0702fc

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/ui/util/analytics/TokenAnalytics;->analytics(II)V

    .line 32
    goto :goto_5d

    .line 34
    :pswitch_17
    const v0, 0x7f0702c5

    const v1, 0x7f0702fc

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/ui/util/analytics/TokenAnalytics;->analytics(II)V

    .line 35
    goto :goto_5d

    .line 37
    :pswitch_21
    const v0, 0x7f0702a3

    const v1, 0x7f070300

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/ui/util/analytics/TokenAnalytics;->analytics(II)V

    .line 38
    goto :goto_5d

    .line 40
    :pswitch_2b
    const v0, 0x7f0702c5

    const v1, 0x7f070300

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/ui/util/analytics/TokenAnalytics;->analytics(II)V

    .line 41
    goto :goto_5d

    .line 43
    :pswitch_35
    const v0, 0x7f0702a3

    const v1, 0x7f07031f

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/ui/util/analytics/TokenAnalytics;->analytics(II)V

    .line 44
    goto :goto_5d

    .line 46
    :pswitch_3f
    const v0, 0x7f0702c5

    const v1, 0x7f07031f

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/ui/util/analytics/TokenAnalytics;->analytics(II)V

    .line 47
    goto :goto_5d

    .line 49
    :pswitch_49
    const v0, 0x7f0702d1

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/util/analytics/TokenAnalytics;->analytics(I)V

    .line 50
    goto :goto_5d

    .line 52
    :pswitch_50
    const v0, 0x7f0702bf

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/util/analytics/TokenAnalytics;->analytics(I)V

    .line 53
    goto :goto_5d

    .line 55
    :pswitch_57
    const v0, 0x7f0702a3

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/util/analytics/TokenAnalytics;->analytics(I)V

    .line 58
    :goto_5d
    return-void

    :pswitch_data_5e
    .packed-switch 0x1
        :pswitch_d
        :pswitch_17
        :pswitch_21
        :pswitch_2b
        :pswitch_35
        :pswitch_3f
        :pswitch_49
        :pswitch_50
        :pswitch_57
    .end packed-switch
.end method


# virtual methods
.method public onEnterScreen(Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;)V
    .registers 6
    .param p1, "screen"    # Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/ui/util/analytics/TokenAnalytics;->tracker:Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoScreenView;

    iget-object v2, p0, Lcom/itau/empresas/ui/util/analytics/TokenAnalytics;->application:Lcom/itau/empresas/CustomApplication;

    .line 69
    const v3, 0x7f0702ea

    invoke-virtual {v2, v3}, Lcom/itau/empresas/CustomApplication;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/itau/empresas/ui/util/analytics/EventoScreenView;-><init>(Ljava/lang/String;)V

    .line 68
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarScreenView(Lcom/itau/empresas/ui/util/analytics/EventoScreenView;)V

    .line 70
    const v0, 0x7f0702a8

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/util/analytics/TokenAnalytics;->analytics(I)V

    .line 72
    return-void
.end method

.method public onLeaveScreen(Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;)V
    .registers 3
    .param p1, "screen"    # Lbr/com/itau/sdk/android/core/token/TokenEvent$Screen;

    .line 76
    const v0, 0x7f0702c1

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/util/analytics/TokenAnalytics;->analytics(I)V

    .line 77
    return-void
.end method

.method public onTrackingEvent(Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;)V
    .registers 3
    .param p1, "category"    # Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;
    .param p2, "action"    # Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    .line 62
    invoke-direct {p0, p2}, Lcom/itau/empresas/ui/util/analytics/TokenAnalytics;->analytics(Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;)V

    .line 63
    return-void
.end method

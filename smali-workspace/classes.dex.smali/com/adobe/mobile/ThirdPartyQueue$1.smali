.class Lcom/adobe/mobile/ThirdPartyQueue$1;
.super Ljava/lang/Object;
.source "ThirdPartyQueue.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/adobe/mobile/ThirdPartyQueue;->workerThread()Ljava/lang/Runnable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/adobe/mobile/ThirdPartyQueue;

.field final synthetic val$worker:Lcom/adobe/mobile/ThirdPartyQueue;


# direct methods
.method constructor <init>(Lcom/adobe/mobile/ThirdPartyQueue;Lcom/adobe/mobile/ThirdPartyQueue;)V
    .registers 3
    .param p1, "this$0"    # Lcom/adobe/mobile/ThirdPartyQueue;

    .line 217
    iput-object p1, p0, Lcom/adobe/mobile/ThirdPartyQueue$1;->this$0:Lcom/adobe/mobile/ThirdPartyQueue;

    iput-object p2, p0, Lcom/adobe/mobile/ThirdPartyQueue$1;->val$worker:Lcom/adobe/mobile/ThirdPartyQueue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 13

    .line 222
    const/16 v0, 0xa

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 224
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getOfflineTrackingEnabled()Z

    move-result v6

    .line 227
    .local v6, "offlineEnabled":Z
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 229
    .local v7, "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "Accept-Language"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getDefaultAcceptLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v7, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    const-string v0, "User-Agent"

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getDefaultUserAgent()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v7, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    :goto_24
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getPrivacyStatus()Lcom/adobe/mobile/MobilePrivacyStatus;

    move-result-object v0

    sget-object v1, Lcom/adobe/mobile/MobilePrivacyStatus;->MOBILE_PRIVACY_STATUS_OPT_IN:Lcom/adobe/mobile/MobilePrivacyStatus;

    if-ne v0, v1, :cond_12f

    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->networkConnectivity()Z

    move-result v0

    if-eqz v0, :cond_12f

    .line 235
    iget-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue$1;->val$worker:Lcom/adobe/mobile/ThirdPartyQueue;

    invoke-virtual {v0}, Lcom/adobe/mobile/ThirdPartyQueue;->selectOldestHit()Lcom/adobe/mobile/AbstractHitDatabase$Hit;

    move-result-object v8

    .line 238
    .local v8, "hit":Lcom/adobe/mobile/AbstractHitDatabase$Hit;
    if-eqz v8, :cond_12f

    iget-object v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->urlFragment:Ljava/lang/String;

    if-nez v0, :cond_48

    .line 239
    goto/16 :goto_12f

    .line 243
    :cond_48
    if-nez v6, :cond_67

    iget-wide v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->timestamp:J

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getTimeSince1970()J

    move-result-wide v2

    const-wide/16 v4, 0x3c

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_67

    .line 245
    :try_start_57
    iget-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue$1;->val$worker:Lcom/adobe/mobile/ThirdPartyQueue;

    iget-object v1, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->identifier:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/adobe/mobile/ThirdPartyQueue;->deleteHit(Ljava/lang/String;)V
    :try_end_5e
    .catch Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException; {:try_start_57 .. :try_end_5e} :catch_5f

    .line 250
    goto :goto_24

    .line 247
    :catch_5f
    move-exception v9

    .line 248
    .local v9, "ex":Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException;
    iget-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue$1;->val$worker:Lcom/adobe/mobile/ThirdPartyQueue;

    invoke-virtual {v0, v9}, Lcom/adobe/mobile/ThirdPartyQueue;->resetDatabase(Ljava/lang/Exception;)V

    .line 249
    goto/16 :goto_12f

    .line 255
    .end local v9    # "ex":Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException;
    :cond_67
    iget-object v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->postBody:Ljava/lang/String;

    if-eqz v0, :cond_6e

    iget-object v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->postBody:Ljava/lang/String;

    goto :goto_70

    :cond_6e
    const-string v0, ""

    :goto_70
    iput-object v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->postBody:Ljava/lang/String;

    .line 256
    iget-object v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->postType:Ljava/lang/String;

    if-eqz v0, :cond_79

    iget-object v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->postType:Ljava/lang/String;

    goto :goto_7b

    :cond_79
    const-string v0, ""

    :goto_7b
    iput-object v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->postType:Ljava/lang/String;

    .line 259
    iget v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->timeout:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_85

    const/16 v0, 0x7d0

    goto :goto_89

    :cond_85
    iget v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->timeout:I

    mul-int/lit16 v0, v0, 0x3e8

    :goto_89
    iput v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->timeout:I

    .line 264
    iget-object v0, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->urlFragment:Ljava/lang/String;

    iget-object v1, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->postBody:Ljava/lang/String;

    move-object v2, v7

    iget v3, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->timeout:I

    iget-object v4, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->postType:Ljava/lang/String;

    iget-object v5, p0, Lcom/adobe/mobile/ThirdPartyQueue$1;->this$0:Lcom/adobe/mobile/ThirdPartyQueue;

    iget-object v5, v5, Lcom/adobe/mobile/ThirdPartyQueue;->logPrefix:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/adobe/mobile/RequestHandler;->sendThirdPartyRequest(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;ILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b6

    .line 268
    :try_start_9e
    iget-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue$1;->val$worker:Lcom/adobe/mobile/ThirdPartyQueue;

    iget-object v1, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->identifier:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/adobe/mobile/ThirdPartyQueue;->deleteHit(Ljava/lang/String;)V
    :try_end_a5
    .catch Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException; {:try_start_9e .. :try_end_a5} :catch_a6

    .line 273
    goto :goto_ae

    .line 270
    :catch_a6
    move-exception v11

    .line 271
    .local v11, "ex":Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException;
    iget-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue$1;->val$worker:Lcom/adobe/mobile/ThirdPartyQueue;

    invoke-virtual {v0, v11}, Lcom/adobe/mobile/ThirdPartyQueue;->resetDatabase(Ljava/lang/Exception;)V

    .line 272
    goto/16 :goto_12f

    .line 275
    .end local v11    # "ex":Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException;
    :goto_ae
    iget-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue$1;->val$worker:Lcom/adobe/mobile/ThirdPartyQueue;

    iget-wide v1, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->timestamp:J

    iput-wide v1, v0, Lcom/adobe/mobile/ThirdPartyQueue;->lastHitTimestamp:J

    .line 276
    goto/16 :goto_24

    .line 281
    :cond_b6
    const-string v0, "%s - Unable to forward hit (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/ThirdPartyQueue$1;->this$0:Lcom/adobe/mobile/ThirdPartyQueue;

    iget-object v2, v2, Lcom/adobe/mobile/ThirdPartyQueue;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->urlFragment:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 283
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getOfflineTrackingEnabled()Z

    move-result v0

    if-eqz v0, :cond_ed

    .line 286
    const-wide/16 v9, 0x1e

    .line 287
    .local v9, "delay":J
    const-string v0, "%s - Network error, imposing internal cooldown (%d seconds)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/ThirdPartyQueue$1;->this$0:Lcom/adobe/mobile/ThirdPartyQueue;

    iget-object v2, v2, Lcom/adobe/mobile/ThirdPartyQueue;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_fd

    .line 292
    .end local v9    # "delay":J
    :cond_ed
    :try_start_ed
    iget-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue$1;->val$worker:Lcom/adobe/mobile/ThirdPartyQueue;

    iget-object v1, v8, Lcom/adobe/mobile/AbstractHitDatabase$Hit;->identifier:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/adobe/mobile/ThirdPartyQueue;->deleteHit(Ljava/lang/String;)V
    :try_end_f4
    .catch Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException; {:try_start_ed .. :try_end_f4} :catch_f6

    .line 297
    goto/16 :goto_24

    .line 294
    :catch_f6
    move-exception v11

    .line 295
    .local v11, "ex":Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException;
    iget-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue$1;->val$worker:Lcom/adobe/mobile/ThirdPartyQueue;

    invoke-virtual {v0, v11}, Lcom/adobe/mobile/ThirdPartyQueue;->resetDatabase(Ljava/lang/Exception;)V

    .line 296
    goto :goto_12f

    .line 305
    .local v9, "delay":J
    .end local v11    # "ex":Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException;
    :goto_fd
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_fe
    int-to-long v0, v11

    cmp-long v0, v0, v9

    if-gez v0, :cond_115

    :try_start_103
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->networkConnectivity()Z

    move-result v0

    if-eqz v0, :cond_115

    .line 306
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_112
    .catch Ljava/lang/Exception; {:try_start_103 .. :try_end_112} :catch_116

    .line 305
    add-int/lit8 v11, v11, 0x1

    goto :goto_fe

    .line 311
    .end local v11    # "i":I
    :cond_115
    goto :goto_12d

    .line 309
    :catch_116
    move-exception v11

    .line 310
    .local v11, "e":Ljava/lang/Exception;
    const-string v0, "%s - Background Thread Interrupted (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/ThirdPartyQueue$1;->this$0:Lcom/adobe/mobile/ThirdPartyQueue;

    iget-object v2, v2, Lcom/adobe/mobile/ThirdPartyQueue;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v11}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 312
    .end local v8    # "hit":Lcom/adobe/mobile/AbstractHitDatabase$Hit;
    .end local v9    # "delay":J
    .end local v11    # "e":Ljava/lang/Exception;
    :goto_12d
    goto/16 :goto_24

    .line 315
    :cond_12f
    :goto_12f
    iget-object v0, p0, Lcom/adobe/mobile/ThirdPartyQueue$1;->val$worker:Lcom/adobe/mobile/ThirdPartyQueue;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/adobe/mobile/ThirdPartyQueue;->bgThreadActive:Z

    .line 316
    return-void
.end method

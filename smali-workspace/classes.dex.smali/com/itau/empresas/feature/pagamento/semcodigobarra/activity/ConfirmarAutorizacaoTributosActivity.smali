.class public Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "ConfirmarAutorizacaoTributosActivity.java"


# instance fields
.field app:Lcom/itau/empresas/CustomApplication;

.field private autorizacaoResponseVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/AutorizacaoResponseVO;

.field botaoAutorizarPagamentoTributos:Landroid/widget/TextView;

.field campoAgenciaConta:Landroid/widget/TextView;

.field campoCnpjPagador:Landroid/widget/TextView;

.field campoNomeDaEmpresa:Landroid/widget/TextView;

.field dadosConfirmacaoTributos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosConfirmacaoTributo;>;"
        }
    .end annotation
.end field

.field darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

.field gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

.field private inclusaoTributoResponseVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

.field llConfirmacaoPagamento:Landroid/widget/LinearLayout;

.field pagamentoDarfController:Lcom/itau/empresas/controller/PagamentoDarfController;

.field pagamentoGPSController:Lcom/itau/empresas/controller/PagamentoGPSController;

.field root:Landroid/widget/LinearLayout;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 49
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private abreTelaSucesso(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;)V
    .registers 4
    .param p1, "detalhesAutorizacaoVO"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    .line 213
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->escondeDialogoDeProgresso()V

    .line 214
    invoke-static {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$IntentBuilder_;

    move-result-object v0

    .line 215
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->builDadosPagamento(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$IntentBuilder_;->dadosPagamentoTributo(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$IntentBuilder_;

    move-result-object v0

    .line 216
    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 217
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->finish()V

    .line 218
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;

    .line 49
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->autorizarTributo()V

    return-void
.end method

.method private autorizaTributo()V
    .registers 2

    .line 154
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    if-eqz v0, :cond_a

    .line 155
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->pagamentoGPSController:Lcom/itau/empresas/controller/PagamentoGPSController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/PagamentoGPSController;->autorizarGPS()V

    goto :goto_f

    .line 158
    :cond_a
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->pagamentoDarfController:Lcom/itau/empresas/controller/PagamentoDarfController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/PagamentoDarfController;->autorizaDarf()V

    .line 161
    :goto_f
    return-void
.end method

.method private autorizarDarf(Z)V
    .registers 4
    .param p1, "duplicidade"    # Z

    .line 252
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->setDuplicidade(Z)V

    .line 253
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->pagamentoDarfController:Lcom/itau/empresas/controller/PagamentoDarfController;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/controller/PagamentoDarfController;->simularAutorizacaoDarf(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;)V

    .line 254
    return-void
.end method

.method private autorizarGPS(Z)V
    .registers 4
    .param p1, "duplicidade"    # Z

    .line 257
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->setDuplicidade(Ljava/lang/Boolean;)V

    .line 258
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->pagamentoGPSController:Lcom/itau/empresas/controller/PagamentoGPSController;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/controller/PagamentoGPSController;->simularAutorizacaoGPS(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;)V

    .line 260
    return-void
.end method

.method private autorizarTributo()V
    .registers 2

    .line 263
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    if-eqz v0, :cond_9

    .line 264
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->autorizarGPS(Z)V

    goto :goto_d

    .line 267
    :cond_9
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->autorizarDarf(Z)V

    .line 269
    :goto_d
    return-void
.end method

.method private builDadosPagamento(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;
    .registers 4
    .param p1, "detalhesAutorizacaoVO"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    .line 221
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;

    invoke-direct {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;-><init>()V

    .line 222
    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->comDetelhesAutorizacao(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;

    move-result-object v0

    .line 223
    const v1, 0x7f07049f

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->comMensagem(Ljava/lang/String;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;

    move-result-object v0

    .line 224
    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->build()Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    move-result-object v0

    .line 221
    return-object v0
.end method

.method private buildAdpaterComprovantes()V
    .registers 4

    .line 205
    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/ConfirmacaoTributosAdapter;

    invoke-direct {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/ConfirmacaoTributosAdapter;-><init>()V

    .line 206
    .local v1, "adapter":Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/ConfirmacaoTributosAdapter;
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->dadosConfirmacaoTributos:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/ConfirmacaoTributosAdapter;->setList(Ljava/util/List;)V

    .line 207
    new-instance v2, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;

    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->llConfirmacaoPagamento:Landroid/widget/LinearLayout;

    invoke-direct {v2, v0, v1}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;-><init>(Landroid/widget/LinearLayout;Landroid/widget/Adapter;)V

    .line 209
    .local v2, "layoutWrapper":Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;
    invoke-virtual {v2}, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->init()V

    .line 210
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 94
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 96
    const v1, 0x7f0706a0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 99
    return-void
.end method

.method private consultaDadosCliente()V
    .registers 2

    .line 116
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->pagamentoDarfController:Lcom/itau/empresas/controller/PagamentoDarfController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/PagamentoDarfController;->consultaDadosPerfilLogado()Lcom/itau/empresas/api/model/PerfilOperadorVO;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->atualizaDadosPerfilLogado(Lcom/itau/empresas/api/model/PerfilOperadorVO;)V

    .line 117
    return-void
.end method

.method private consultaDetalhesPagamento()V
    .registers 8

    .line 197
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->mostraDialogoDeProgresso()V

    .line 198
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->pagamentoDarfController:Lcom/itau/empresas/controller/PagamentoDarfController;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    const-string v2, "03"

    iget-object v3, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->autorizacaoResponseVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/AutorizacaoResponseVO;

    .line 199
    invoke-virtual {v3}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/AutorizacaoResponseVO;->getNumeroCarrinho()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->autorizacaoResponseVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/AutorizacaoResponseVO;

    .line 200
    invoke-virtual {v4}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/AutorizacaoResponseVO;->getAutorizacoesList()Ljava/util/List;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/itau/empresas/api/model/AutorizacoesVO;

    invoke-virtual {v4}, Lcom/itau/empresas/api/model/AutorizacoesVO;->getNumeroLote()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->autorizacaoResponseVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/AutorizacaoResponseVO;

    .line 201
    invoke-virtual {v5}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/AutorizacaoResponseVO;->getAutorizacoesList()Ljava/util/List;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/itau/empresas/api/model/AutorizacoesVO;

    invoke-virtual {v5}, Lcom/itau/empresas/api/model/AutorizacoesVO;->getNumeroPagamento()Ljava/lang/String;

    move-result-object v5

    .line 198
    invoke-virtual/range {v0 .. v5}, Lcom/itau/empresas/controller/PagamentoDarfController;->consultaDetalhesPagamento(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    return-void
.end method

.method private incluiOuAutorizaTributo()V
    .registers 2

    .line 126
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    if-eqz v0, :cond_9

    .line 127
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->autorizarGPS(Z)V

    goto :goto_d

    .line 130
    :cond_9
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->autorizarDarf(Z)V

    .line 132
    :goto_d
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 272
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private mostraDialogDuplicidade()V
    .registers 4

    .line 228
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->DOIS_BOTOES:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 229
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 230
    const v1, 0x7f0701a9

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 231
    const v1, 0x7f070176

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoPositivo(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 232
    const v1, 0x7f07013f

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNegativo(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 233
    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v2

    .line 234
    .local v2, "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setBoldBotaoPositivo(Z)V

    .line 235
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity$1;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity$1;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNegativoListener(Landroid/view/View$OnClickListener;)V

    .line 242
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity$2;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity$2;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setPositivoListener(Landroid/view/View$OnClickListener;)V

    .line 248
    invoke-virtual {v2, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 249
    return-void
.end method


# virtual methods
.method public atualizaDadosPerfilLogado(Lcom/itau/empresas/api/model/PerfilOperadorVO;)V
    .registers 5
    .param p1, "perfilOperadorVO"    # Lcom/itau/empresas/api/model/PerfilOperadorVO;

    .line 102
    if-eqz p1, :cond_5d

    .line 103
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->campoNomeDaEmpresa:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->getNomeEmpresa()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_17

    .line 105
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->getNomeEmpresa()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    goto :goto_1d

    :cond_17
    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 106
    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getNomeEmpresa()Ljava/lang/String;

    move-result-object v1

    .line 103
    :goto_1d
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->campoAgenciaConta:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_56

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->app:Lcom/itau/empresas/CustomApplication;

    .line 108
    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->app:Lcom/itau/empresas/CustomApplication;

    .line 109
    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto :goto_58

    :cond_56
    const-string v1, ""

    :goto_58
    check-cast v1, Ljava/lang/CharSequence;

    .line 107
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    :cond_5d
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->campoCnpjPagador:Landroid/widget/TextView;

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;->CPF_CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    if-eqz v2, :cond_6c

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->darfInclusaoRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;

    .line 112
    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DarfInclusaoRequestVO;->getCnpjEmpresa()Ljava/lang/String;

    move-result-object v2

    goto :goto_72

    :cond_6c
    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;->getCnpj()Ljava/lang/String;

    move-result-object v2

    .line 111
    :goto_72
    invoke-interface {v1, v2}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    return-void
.end method

.method public clickAutorizaDarf()V
    .registers 1

    .line 121
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->mostraDialogoDeProgresso()V

    .line 122
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->incluiOuAutorizaTributo()V

    .line 123
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/exception/BackendException;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/exception/BackendException;

    .line 176
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 177
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 178
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;

    .line 185
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getCampos()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_42

    .line 186
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getCampos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_42

    .line 187
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getCampos()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbr/com/itau/sdk/android/core/model/CampoErroDTO;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/CampoErroDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 188
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    goto :goto_6a

    .line 190
    :cond_42
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6a

    .line 191
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 192
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 194
    :cond_6a
    :goto_6a
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 164
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getCampos()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_42

    .line 165
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getCampos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_42

    .line 166
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getCampos()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbr/com/itau/sdk/android/core/model/CampoErroDTO;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/CampoErroDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 167
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    goto :goto_6a

    .line 169
    :cond_42
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6a

    .line 170
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 171
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 173
    :cond_6a
    :goto_6a
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/AutorizacaoResponseVO;)V
    .registers 2
    .param p1, "autorizacoesVO"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/AutorizacaoResponseVO;

    .line 135
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->escondeDialogoDeProgresso()V

    .line 136
    if-eqz p1, :cond_a

    .line 137
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->autorizacaoResponseVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/AutorizacaoResponseVO;

    .line 138
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->consultaDetalhesPagamento()V

    .line 140
    :cond_a
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;)V
    .registers 2
    .param p1, "detalhesAutorizacaoVO"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    .line 181
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->abreTelaSucesso(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;)V

    .line 182
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;)V
    .registers 3
    .param p1, "inclusaoTributoResponseVO"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    .line 143
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->escondeDialogoDeProgresso()V

    .line 144
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->inclusaoTributoResponseVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    .line 145
    invoke-virtual {p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;->isDuplicado()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 146
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->mostraDialogDuplicidade()V

    goto :goto_12

    .line 149
    :cond_f
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->autorizaTributo()V

    .line 151
    :goto_12
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 73
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "detalhePagamentoAutorizado"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "pagamentoTributoDarfPJIncluirEAutorizarSimular"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "pagamentoTributoDarfPJIncluirEAutorizarEfetivar"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "detalhePagamentoAutorizado"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "pagamentoTributoGpsPJIncluirSimular"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "pagamentoTributoGpsPJIncluirEfetivar"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "pagamentoTributoGpsPJIncluirEAutorizarSimular"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "pagamentoTributoGpsPJIncluirEAutorizarEfetivar"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public updateView()V
    .registers 4

    .line 85
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->constroiToolbar()V

    .line 86
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->consultaDadosCliente()V

    .line 87
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->buildAdpaterComprovantes()V

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->botaoAutorizarPagamentoTributos:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ConfirmarAutorizacaoTributosActivity;->botaoAutorizarPagamentoTributos:Landroid/widget/TextView;

    .line 89
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "confirmar"

    .line 90
    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 88
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    return-void
.end method

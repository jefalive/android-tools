.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
.source "BarEntry.java"


# instance fields
.field private mNegativeSum:F

.field private mPositiveSum:F

.field private mVals:[F


# direct methods
.method public constructor <init>(FI)V
    .registers 3
    .param p1, "val"    # F
    .param p2, "xIndex"    # I

    .line 47
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;-><init>(FI)V

    .line 48
    return-void
.end method

.method public constructor <init>(FILjava/lang/Object;)V
    .registers 4
    .param p1, "val"    # F
    .param p2, "xIndex"    # I
    .param p3, "data"    # Ljava/lang/Object;

    .line 76
    invoke-direct {p0, p1, p2, p3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;-><init>(FILjava/lang/Object;)V

    .line 77
    return-void
.end method

.method public constructor <init>([FI)V
    .registers 4
    .param p1, "vals"    # [F
    .param p2, "xIndex"    # I

    .line 32
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->calcSum([F)F

    move-result v0

    invoke-direct {p0, v0, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;-><init>(FI)V

    .line 35
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->mVals:[F

    .line 36
    invoke-direct {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->calcPosNegSum()V

    .line 37
    return-void
.end method

.method public constructor <init>([FILjava/lang/String;)V
    .registers 5
    .param p1, "vals"    # [F
    .param p2, "xIndex"    # I
    .param p3, "label"    # Ljava/lang/String;

    .line 60
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->calcSum([F)F

    move-result v0

    invoke-direct {p0, v0, p2, p3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;-><init>(FILjava/lang/Object;)V

    .line 63
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->mVals:[F

    .line 64
    invoke-direct {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->calcPosNegSum()V

    .line 65
    return-void
.end method

.method private calcPosNegSum()V
    .registers 8

    .line 163
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->mVals:[F

    if-nez v0, :cond_b

    .line 164
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->mNegativeSum:F

    .line 165
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->mPositiveSum:F

    .line 166
    return-void

    .line 170
    :cond_b
    const/4 v1, 0x0

    .line 171
    .local v1, "sumNeg":F
    const/4 v2, 0x0

    .line 174
    .local v2, "sumPos":F
    iget-object v3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->mVals:[F

    array-length v4, v3

    const/4 v5, 0x0

    :goto_11
    if-ge v5, v4, :cond_24

    aget v6, v3, v5

    .line 175
    .local v6, "f":F
    const/4 v0, 0x0

    cmpg-float v0, v6, v0

    if-gtz v0, :cond_20

    .line 176
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v0

    add-float/2addr v1, v0

    goto :goto_21

    .line 178
    :cond_20
    add-float/2addr v2, v6

    .line 174
    .end local v6    # "f":F
    :goto_21
    add-int/lit8 v5, v5, 0x1

    goto :goto_11

    .line 182
    :cond_24
    iput v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->mNegativeSum:F

    .line 183
    iput v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->mPositiveSum:F

    .line 184
    return-void
.end method

.method private static calcSum([F)F
    .registers 7
    .param p0, "vals"    # [F

    .line 198
    if-nez p0, :cond_4

    .line 199
    const/4 v0, 0x0

    return v0

    .line 202
    :cond_4
    const/4 v1, 0x0

    .line 205
    .local v1, "sum":F
    move-object v2, p0

    array-length v3, v2

    const/4 v4, 0x0

    :goto_8
    if-ge v4, v3, :cond_10

    aget v5, v2, v4

    .line 206
    .local v5, "f":F
    add-float/2addr v1, v5

    .line 205
    .end local v5    # "f":F
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    .line 209
    :cond_10
    return v1
.end method


# virtual methods
.method public copy()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;
    .registers 5

    .line 86
    new-instance v3, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->getVal()F

    move-result v0

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->getXIndex()I

    move-result v1

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->getData()Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v3, v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;-><init>(FILjava/lang/Object;)V

    .line 87
    .local v3, "copied":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->mVals:[F

    invoke-virtual {v3, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->setVals([F)V

    .line 88
    return-object v3
.end method

.method public bridge synthetic copy()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    .registers 2

    .line 9
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->copy()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;

    move-result-object v0

    return-object v0
.end method

.method public getBelowSum(I)F
    .registers 6
    .param p1, "stackIndex"    # I

    .line 127
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->mVals:[F

    if-nez v0, :cond_6

    .line 128
    const/4 v0, 0x0

    return v0

    .line 130
    :cond_6
    const/4 v2, 0x0

    .line 131
    .local v2, "remainder":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->mVals:[F

    array-length v0, v0

    add-int/lit8 v3, v0, -0x1

    .line 133
    .local v3, "index":I
    :goto_c
    if-le v3, p1, :cond_18

    if-ltz v3, :cond_18

    .line 134
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->mVals:[F

    aget v0, v0, v3

    add-float/2addr v2, v0

    .line 135
    add-int/lit8 v3, v3, -0x1

    goto :goto_c

    .line 138
    :cond_18
    return v2
.end method

.method public getNegativeSum()F
    .registers 2

    .line 156
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->mNegativeSum:F

    return v0
.end method

.method public getPositiveSum()F
    .registers 2

    .line 147
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->mPositiveSum:F

    return v0
.end method

.method public getVal()F
    .registers 2

    .line 121
    invoke-super {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    return v0
.end method

.method public getVals()[F
    .registers 2

    .line 99
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->mVals:[F

    return-object v0
.end method

.method public setVals([F)V
    .registers 3
    .param p1, "vals"    # [F

    .line 109
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->calcSum([F)F

    move-result v0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->setVal(F)V

    .line 110
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->mVals:[F

    .line 111
    invoke-direct {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarEntry;->calcPosNegSum()V

    .line 112
    return-void
.end method

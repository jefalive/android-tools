.class public Lbr/com/itau/sdk/android/core/login/model/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/login/model/a$a;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "identificador_cliente"
    .end annotation
.end field

.field private b:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "chave_acesso"
    .end annotation
.end field

.field private c:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "remover_chave_acesso"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/login/model/a;
    .registers 2

    .line 31
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/login/model/a;->a:Ljava/lang/String;

    .line 32
    return-object p0
.end method

.method public a()Ljava/lang/String;
    .registers 2

    .line 19
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/login/model/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/login/model/a;
    .registers 2

    .line 36
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/login/model/a;->b:Ljava/lang/String;

    .line 37
    return-object p0
.end method

.method public b()Ljava/lang/String;
    .registers 2

    .line 23
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/login/model/a;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .registers 2

    .line 27
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/login/model/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Lbr/com/itau/sdk/android/core/login/model/a$a;
    .registers 2

    .line 41
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/login/model/a;->c:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 42
    sget-object v0, Lbr/com/itau/sdk/android/core/login/model/a$a;->b:Lbr/com/itau/sdk/android/core/login/model/a$a;

    return-object v0

    .line 44
    :cond_7
    sget-object v0, Lbr/com/itau/sdk/android/core/login/model/a$a;->c:Lbr/com/itau/sdk/android/core/login/model/a$a;

    return-object v0
.end method

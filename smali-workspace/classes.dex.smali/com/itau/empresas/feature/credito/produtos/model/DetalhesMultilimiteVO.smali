.class public Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteVO;
.super Ljava/lang/Object;
.source "DetalhesMultilimiteVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private produtos:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "produtos"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getProdutos()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;>;"
        }
    .end annotation

    .line 14
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteVO;->produtos:Ljava/util/List;

    return-object v0
.end method

.method public setProdutos(Ljava/util/List;)V
    .registers 2
    .param p1, "produtos"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteProdutoVO;>;)V"
        }
    .end annotation

    .line 18
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteVO;->produtos:Ljava/util/List;

    .line 19
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 23
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DetalhesMultilimiteVO{produtos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteVO;->produtos:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

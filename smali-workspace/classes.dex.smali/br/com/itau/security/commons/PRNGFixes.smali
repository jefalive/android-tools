.class public final Lbr/com/itau/security/commons/PRNGFixes;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final c:[B

.field private static final d:[B

.field private static e:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 44
    const/16 v0, 0x173

    new-array v0, v0, [B

    fill-array-data v0, :array_14

    sput-object v0, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v0, 0x2a

    sput v0, Lbr/com/itau/security/commons/PRNGFixes;->e:I

    invoke-static {}, Lbr/com/itau/security/commons/PRNGFixes;->f()[B

    move-result-object v0

    sput-object v0, Lbr/com/itau/security/commons/PRNGFixes;->c:[B

    .line 43
    return-void

    :array_14
    .array-data 1
        0x71t
        -0x65t
        0x51t
        -0x1at
        0xdt
        -0xet
        0x8t
        0x7t
        -0xct
        -0x13t
        0x1t
        -0x13t
        0x2t
        0xct
        0x12t
        -0x10t
        -0xet
        0x9t
        -0xct
        0x1t
        0x3et
        -0x26t
        0xat
        0x6t
        0xft
        -0x20t
        -0x3t
        0x3t
        0x6t
        0xat
        0x6t
        0xft
        -0x20t
        -0x3t
        0x3t
        0x6t
        0x26t
        -0x4ft
        -0x2t
        -0x6t
        0x53t
        -0x42t
        -0x16t
        0x14t
        -0x9t
        -0x4t
        0xat
        -0x2t
        -0xbt
        0x6t
        -0x13t
        0x1t
        -0x13t
        0x2t
        0xct
        0x12t
        -0x10t
        -0xet
        0x9t
        -0xct
        0x1t
        0x3et
        -0x3at
        0x1t
        -0x10t
        0x2at
        -0x26t
        -0x6t
        -0x2t
        0x12t
        -0xet
        0xat
        -0x3t
        0x3ct
        0x5t
        -0x32t
        0xat
        0x6t
        0xft
        -0x20t
        -0x3t
        0x3t
        0x6t
        0x24t
        -0x8t
        0x8t
        -0x43t
        0x0t
        -0x3t
        -0x9t
        0x5t
        0x0t
        0x43t
        -0x43t
        -0x18t
        0x58t
        -0x58t
        0x4t
        0x2t
        0x0t
        0x6t
        0x46t
        -0x31t
        -0x23t
        0x2t
        -0x8t
        0xct
        0x4t
        -0x2t
        -0xet
        0x37t
        0x19t
        0xat
        0x6t
        0xft
        -0x20t
        -0x3t
        0x3t
        0x6t
        0x10t
        -0xet
        0x9t
        -0x1ct
        -0xet
        -0x4t
        0xdt
        -0x4t
        0x4t
        -0x8t
        -0x4t
        -0x4t
        0x6t
        0x0t
        0xdt
        0x18t
        -0xct
        0x8t
        -0x13t
        0x56t
        -0x34t
        -0x13t
        0x1t
        -0x13t
        0x2t
        0xct
        0x12t
        -0x10t
        -0xet
        0x9t
        -0xct
        0x1t
        0x44t
        -0x2t
        0x8t
        -0x43t
        0x0t
        -0x3t
        -0x9t
        0x5t
        0x0t
        0x43t
        -0x43t
        -0x18t
        0x58t
        -0x58t
        0x4t
        0x2t
        0x0t
        0x6t
        0x46t
        -0x31t
        -0x23t
        0x2t
        -0x8t
        0xct
        0x4t
        -0x2t
        -0xet
        0x37t
        0x19t
        -0x4t
        0xat
        0x38t
        -0x34t
        -0x10t
        0xet
        -0x3t
        -0x6t
        0x2t
        0x36t
        -0x3bt
        0x6t
        -0x12t
        0x4t
        -0x3t
        0x0t
        -0xct
        0x4at
        -0x4bt
        0x9t
        0x8t
        -0x10t
        0x45t
        -0x43t
        -0x3t
        0x2t
        -0x8t
        0xct
        0x4t
        -0x2t
        -0xet
        0x43t
        -0x3dt
        -0xat
        -0x1t
        0xdt
        0x36t
        -0x21t
        -0x14t
        -0x14t
        0xat
        -0xet
        0x10t
        0x21t
        -0x30t
        -0x8t
        0x8t
        -0x5t
        0x4t
        -0x1ct
        -0x9t
        -0x4t
        0x6t
        0x0t
        0x43t
        -0x55t
        0x4t
        0x4et
        -0x54t
        0xdt
        -0x1t
        0x0t
        0x43t
        -0x30t
        -0x22t
        0xat
        -0xat
        0x1at
        -0x1t
        0x6t
        0x2bt
        -0x31t
        -0x3t
        0x3t
        0x6t
        0x0t
        0xdt
        0x18t
        -0xct
        0x17t
        -0x46t
        -0xat
        0xat
        -0xdt
        0xat
        -0x6t
        -0x6t
        0x6t
        0x46t
        -0x4ft
        -0x2t
        -0x6t
        0x53t
        -0x54t
        -0x3t
        0x4t
        -0x1t
        0x0t
        -0x4t
        -0x3t
        0xet
        0x0t
        0x10t
        -0xet
        0x9t
        -0x1ct
        -0x15t
        0xdt
        -0x1t
        0x0t
        -0x36t
        -0x2t
        -0x12t
        0x46t
        -0x47t
        0x2t
        0x10t
        -0xet
        0x9t
        -0xct
        0x1t
        -0x1ct
        -0x9t
        -0x4t
        0x6t
        0x0t
        0x43t
        -0x55t
        0x4t
        0x4et
        -0x48t
        0x1t
        -0xat
        0x8t
        -0xet
        0x10t
        -0x14t
        0xet
        0x44t
        -0x54t
        0xdt
        -0x1t
        0x0t
        -0x1at
        0x8t
        -0x14t
        0x7t
        0xat
        0x1t
        -0x12t
        0xet
        0x0t
        0x43t
        -0x4ft
        -0x8t
        0x7t
        0xat
        -0x4t
        -0xet
        0x51t
        -0x50t
        0x8t
        0x45t
        -0x43t
        -0x18t
        0x4t
        0xet
        -0xft
        0x52t
        -0x53t
        0xct
        0x3t
        -0x4t
        0x43t
        -0x47t
        -0xdt
        0x2t
        0x1t
        0x4ct
        -0x2dt
        -0x1et
        -0x6t
        -0x8t
        -0x4t
        0x57t
        -0x31t
        -0x3t
        0x3t
        0x6t
        0xct
        0x19t
    .end array-data
.end method

.method private constructor <init>()V
    .registers 1

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    return-void
.end method

.method private static a(BSS)Ljava/lang/String;
    .registers 9

    add-int/lit8 p0, p0, 0x5

    rsub-int/lit8 p2, p2, 0x6f

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/String;

    sget-object v5, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    rsub-int p1, p1, 0x143

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v5, :cond_19

    move v2, p0

    move v3, p1

    :goto_13
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p2, v2, -0x1

    add-int/lit8 p1, p1, 0x1

    :cond_19
    int-to-byte v2, p2

    aput-byte v2, v1, v4

    if-ne v4, p0, :cond_27

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_27
    move v2, p2

    aget-byte v3, v5, p1

    add-int/lit8 v4, v4, 0x1

    goto :goto_13
.end method

.method static synthetic a()[B
    .registers 1

    .line 39
    invoke-static {}, Lbr/com/itau/security/commons/PRNGFixes;->d()[B

    move-result-object v0

    return-object v0
.end method

.method public static apply()V
    .registers 7

    .line 1069
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_c

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-le v0, v1, :cond_e

    .line 1072
    :cond_c
    goto/16 :goto_108

    .line 1077
    :cond_e
    :try_start_e
    sget-object v0, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v1, 0x167

    aget-byte v0, v0, v1

    neg-int v0, v0

    int-to-byte v0, v0

    sget-object v1, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v2, 0x57

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    const/16 v2, 0x8f

    invoke-static {v0, v2, v1}, Lbr/com/itau/security/commons/PRNGFixes;->a(BSS)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sget-object v1, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v2, 0x61

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget v2, Lbr/com/itau/security/commons/PRNGFixes;->e:I

    add-int/lit8 v2, v2, -0x1

    int-to-short v2, v2

    const/16 v3, 0x1d

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/commons/PRNGFixes;->a(BSS)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, [B

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 1078
    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 1079
    invoke-static {}, Lbr/com/itau/security/commons/PRNGFixes;->d()[B

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 1082
    sget-object v0, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v1, 0x167

    aget-byte v0, v0, v1

    neg-int v0, v0

    int-to-byte v0, v0

    sget-object v1, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v2, 0x57

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    const/16 v2, 0x8f

    invoke-static {v0, v2, v1}, Lbr/com/itau/security/commons/PRNGFixes;->a(BSS)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sget-object v1, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v2, 0x11

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    const/16 v2, 0xcc

    const/16 v3, 0x1d

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/commons/PRNGFixes;->a(BSS)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 1084
    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    sget-object v2, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/4 v3, 0x7

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    sget-object v3, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v4, 0xdf

    aget-byte v3, v3, v4

    int-to-short v3, v3

    const/16 v4, 0x40

    invoke-static {v2, v3, v4}, Lbr/com/itau/security/commons/PRNGFixes;->a(BSS)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 1085
    const/16 v2, 0x400

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1082
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1086
    move v6, v0

    const/16 v1, 0x400

    if-eq v0, v1, :cond_ec

    .line 1087
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v2, Lbr/com/itau/security/commons/PRNGFixes;->e:I

    add-int/lit8 v2, v2, 0x2

    int-to-byte v2, v2

    sget-object v3, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v4, 0x57

    aget-byte v3, v3, v4

    int-to-short v3, v3

    sget-object v4, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v5, 0xf7

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v2, v3, v4}, Lbr/com/itau/security/commons/PRNGFixes;->a(BSS)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_ec
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_ec} :catch_ed

    .line 1093
    :cond_ec
    goto :goto_108

    .line 1091
    :catch_ed
    move-exception v6

    .line 1092
    new-instance v0, Ljava/lang/SecurityException;

    sget-object v1, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v2, 0x2a

    aget-byte v1, v1, v2

    neg-int v1, v1

    int-to-byte v1, v1

    or-int/lit8 v2, v1, 0x48

    int-to-short v2, v2

    sget v3, Lbr/com/itau/security/commons/PRNGFixes;->e:I

    add-int/lit8 v3, v3, -0x1

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/commons/PRNGFixes;->a(BSS)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v6}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 1105
    :goto_108
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-gt v0, v1, :cond_20a

    .line 1112
    sget-object v0, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v1, 0x77

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    or-int/lit16 v1, v0, 0x12a

    int-to-short v1, v1

    sget-object v2, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v3, 0x7a

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/commons/PRNGFixes;->a(BSS)Ljava/lang/String;

    move-result-object v0

    .line 1113
    invoke-static {v0}, Ljava/security/Security;->getProviders(Ljava/lang/String;)[Ljava/security/Provider;

    move-result-object v6

    .line 1114
    if-eqz v6, :cond_13c

    array-length v0, v6

    if-lez v0, :cond_13c

    const-class v0, Lcc/a;

    const/4 v1, 0x0

    aget-object v1, v6, v1

    .line 1117
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 1116
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_145

    .line 1118
    :cond_13c
    new-instance v0, Lcc/a;

    invoke-direct {v0}, Lcc/a;-><init>()V

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/security/Security;->insertProviderAt(Ljava/security/Provider;I)I

    .line 1124
    :cond_145
    new-instance v6, Ljava/security/SecureRandom;

    invoke-direct {v6}, Ljava/security/SecureRandom;-><init>()V

    .line 1125
    const-class v0, Lcc/a;

    .line 1126
    invoke-virtual {v6}, Ljava/security/SecureRandom;->getProvider()Ljava/security/Provider;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 1125
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_18c

    .line 1127
    new-instance v0, Ljava/lang/SecurityException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v2, Lbr/com/itau/security/commons/PRNGFixes;->e:I

    add-int/lit8 v2, v2, -0x2

    int-to-byte v2, v2

    or-int/lit16 v3, v2, 0x93

    int-to-short v3, v3

    sget-object v4, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v5, 0xa

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v2, v3, v4}, Lbr/com/itau/security/commons/PRNGFixes;->a(BSS)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1129
    invoke-virtual {v6}, Ljava/security/SecureRandom;->getProvider()Ljava/security/Provider;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1134
    :cond_18c
    :try_start_18c
    sget-object v0, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v1, 0x1b

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    or-int/lit16 v1, v0, 0xd0

    int-to-short v1, v1

    sget-object v2, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v3, 0x7a

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/commons/PRNGFixes;->a(BSS)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;)Ljava/security/SecureRandom;
    :try_end_1a5
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_18c .. :try_end_1a5} :catch_1a7

    move-result-object v6

    .line 1137
    goto :goto_1c5

    .line 1135
    :catch_1a7
    move-exception v6

    .line 1136
    new-instance v0, Ljava/lang/SecurityException;

    sget-object v1, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v2, 0xe

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v3, 0x7a

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    const/16 v3, 0x126

    invoke-static {v1, v3, v2}, Lbr/com/itau/security/commons/PRNGFixes;->a(BSS)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v6}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 1138
    :goto_1c5
    const-class v0, Lcc/a;

    .line 1139
    invoke-virtual {v6}, Ljava/security/SecureRandom;->getProvider()Ljava/security/Provider;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 1138
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_20a

    .line 1140
    new-instance v0, Ljava/lang/SecurityException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v3, 0x3e

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    sget-object v3, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v4, 0x7a

    aget-byte v3, v3, v4

    neg-int v3, v3

    int-to-byte v3, v3

    const/16 v4, 0x111

    invoke-static {v2, v4, v3}, Lbr/com/itau/security/commons/PRNGFixes;->a(BSS)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1142
    invoke-virtual {v6}, Ljava/security/SecureRandom;->getProvider()Ljava/security/Provider;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_20a
    return-void
.end method

.method private static d()[B
    .registers 7

    .line 293
    :try_start_0
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 294
    new-instance v6, Ljava/io/DataOutputStream;

    invoke-direct {v6, v5}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 296
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {v6, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 297
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    invoke-virtual {v6, v0, v1}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 298
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-virtual {v6, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 299
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    invoke-virtual {v6, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 300
    sget-object v0, Lbr/com/itau/security/commons/PRNGFixes;->c:[B

    invoke-virtual {v6, v0}, Ljava/io/DataOutputStream;->write([B)V

    .line 301
    invoke-virtual {v6}, Ljava/io/DataOutputStream;->close()V

    .line 302
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_31
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_31} :catch_33

    move-result-object v0

    return-object v0

    .line 303
    :catch_33
    move-exception v5

    .line 304
    new-instance v0, Ljava/lang/SecurityException;

    sget-object v1, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v2, 0xe

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v3, 0x2a

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-short v2, v2

    sget v3, Lbr/com/itau/security/commons/PRNGFixes;->e:I

    add-int/lit8 v3, v3, -0x1

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/commons/PRNGFixes;->a(BSS)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v5}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method private static e()Ljava/lang/String;
    .registers 5

    .line 317
    :try_start_0
    const-class v0, Landroid/os/Build;

    sget-object v1, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v2, 0xa

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    or-int/lit16 v2, v1, 0x13e

    int-to-short v2, v2

    sget-object v3, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v4, 0x7a

    aget-byte v3, v3, v4

    neg-int v3, v3

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/commons/PRNGFixes;->a(BSS)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_23} :catch_24

    return-object v0

    .line 318
    .line 319
    :catch_24
    const/4 v0, 0x0

    return-object v0
.end method

.method private static f()[B
    .registers 7

    .line 324
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 325
    sget-object v6, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    .line 326
    if-eqz v6, :cond_c

    .line 327
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 329
    :cond_c
    invoke-static {}, Lbr/com/itau/security/commons/PRNGFixes;->e()Ljava/lang/String;

    move-result-object v6

    .line 330
    if-eqz v6, :cond_15

    .line 331
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 334
    :cond_15
    :try_start_15
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v2, 0x57

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    or-int/lit16 v2, v1, 0xbf

    int-to-short v2, v2

    sget-object v3, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v4, 0xf7

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/commons/PRNGFixes;->a(BSS)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_31
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_15 .. :try_end_31} :catch_33

    move-result-object v0

    return-object v0

    .line 335
    .line 336
    :catch_33
    new-instance v0, Ljava/lang/RuntimeException;

    sget-object v1, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v2, 0x103

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v3, 0x97

    aget-byte v2, v2, v3

    int-to-short v2, v2

    sget-object v3, Lbr/com/itau/security/commons/PRNGFixes;->d:[B

    const/16 v4, 0xf7

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/security/commons/PRNGFixes;->a(BSS)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

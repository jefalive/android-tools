.class public Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperObservationImpl;
.super Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;
.source "ComprovanteLabelWrapperObservationImpl.java"


# instance fields
.field public final observation:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "observation"    # Ljava/lang/String;

    .line 9
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    iput-object p3, p0, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperObservationImpl;->observation:Ljava/lang/String;

    .line 11
    return-void
.end method

.method public static load(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;
    .registers 4
    .param p0, "label"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "observation"    # Ljava/lang/String;

    .line 18
    new-instance v0, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperObservationImpl;

    invoke-direct {v0, p0, p1, p2}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperObservationImpl;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getObservation()Ljava/lang/String;
    .registers 2

    .line 14
    iget-object v0, p0, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperObservationImpl;->observation:Ljava/lang/String;

    return-object v0
.end method

.class Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$3;
.super Ljava/lang/Object;
.source "FaqSearchActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->showLinks(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;

.field final synthetic val$linkItem:Lbr/com/itau/textovoz/model/Link;


# direct methods
.method constructor <init>(Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;Lbr/com/itau/textovoz/model/Link;)V
    .registers 3
    .param p1, "this$0"    # Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;

    .line 163
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$3;->this$0:Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;

    iput-object p2, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$3;->val$linkItem:Lbr/com/itau/textovoz/model/Link;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 5
    .param p1, "v"    # Landroid/view/View;

    .line 167
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$3;->this$0:Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;

    # getter for: Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->operator:Ljava/lang/String;
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->access$200(Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 168
    # getter for: Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->menuOptions:Ljava/util/HashMap;
    invoke-static {}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->access$300()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, [Ljava/lang/String;

    .local v2, "back":[Ljava/lang/String;
    goto :goto_2c

    .line 170
    .end local v2    # "back":[Ljava/lang/String;
    :cond_20
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$3;->val$linkItem:Lbr/com/itau/textovoz/model/Link;

    invoke-virtual {v0}, Lbr/com/itau/textovoz/model/Link;->get_ref()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, v2, v1

    .line 173
    .local v2, "back":[Ljava/lang/String;
    :goto_2c
    if-eqz v2, :cond_33

    .line 174
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$3;->this$0:Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;

    # invokes: Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->resultFaqLink([Ljava/lang/String;)V
    invoke-static {v0, v2}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->access$400(Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;[Ljava/lang/String;)V

    .line 176
    :cond_33
    return-void
.end method

.class public final enum Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;
.super Ljava/lang/Enum;
.source "RecargaEfetivarActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ErrorLimite"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

.field public static final enum CHEQUE_ESPECIAL:Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

.field public static final enum LDP:Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 456
    new-instance v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

    const-string v1, "CHEQUE_ESPECIAL"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;->CHEQUE_ESPECIAL:Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

    new-instance v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

    const-string v1, "LDP"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;->LDP:Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

    .line 455
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

    sget-object v1, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;->CHEQUE_ESPECIAL:Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;->LDP:Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;->$VALUES:[Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 455
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 455
    const-class v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

    return-object v0
.end method

.method public static values()[Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;
    .registers 1

    .line 455
    sget-object v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;->$VALUES:[Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

    invoke-virtual {v0}, [Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

    return-object v0
.end method

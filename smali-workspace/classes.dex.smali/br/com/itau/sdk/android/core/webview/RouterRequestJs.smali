.class public Lbr/com/itau/sdk/android/core/webview/RouterRequestJs;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/webview/RouterRequestJs$RouterRequestResult;
    }
.end annotation


# static fields
.field private static final $:[B

.field private static $$:I


# instance fields
.field private result:Lbr/com/itau/sdk/android/core/webview/RouterRequestJs$RouterRequestResult;


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    new-instance v0, Ljava/lang/String;

    mul-int/lit8 p0, p0, 0x4

    rsub-int/lit8 p0, p0, 0xf

    mul-int/lit8 p1, p1, 0x4

    rsub-int/lit8 p1, p1, 0x4

    sget-object v5, Lbr/com/itau/sdk/android/core/webview/RouterRequestJs;->$:[B

    mul-int/lit8 p2, p2, 0x2

    rsub-int/lit8 p2, p2, 0x52

    const/4 v4, 0x0

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v5, :cond_1d

    move v2, p0

    move v3, p2

    :goto_19
    add-int p2, v2, v3

    add-int/lit8 p1, p1, 0x1

    :cond_1d
    int-to-byte v2, p2

    aput-byte v2, v1, v4

    move v2, v4

    add-int/lit8 v4, v4, 0x1

    if-ne v2, p0, :cond_2a

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_2a
    move v2, p2

    aget-byte v3, v5, p1

    goto :goto_19
.end method

.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x12

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/webview/RouterRequestJs;->$:[B

    const/16 v0, 0x36

    sput v0, Lbr/com/itau/sdk/android/core/webview/RouterRequestJs;->$$:I

    return-void

    :array_e
    .array-data 1
        0x60t
        -0x29t
        0x11t
        -0x6bt
        0x1dt
        0x6t
        -0x1t
        -0xft
        0xdt
        -0x20t
        0x13t
        0xct
        0x4t
        -0x10t
        0xet
        0x1t
        -0x2at
        0x29t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$lambda$0(Lbr/com/itau/sdk/android/core/webview/RouterRequestJs;Lbr/com/itau/sdk/android/core/endpoint/http/a/c;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4

    invoke-direct {p0, p1, p2, p3}, Lbr/com/itau/sdk/android/core/webview/RouterRequestJs;->lambda$routerRequest$0(Lbr/com/itau/sdk/android/core/endpoint/http/a/c;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private synthetic lambda$routerRequest$0(Lbr/com/itau/sdk/android/core/endpoint/http/a/c;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8

    .line 32
    :try_start_0
    invoke-static {p2}, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->resolveRequest(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;

    move-result-object v0

    invoke-interface {p1, v0}, Lbr/com/itau/sdk/android/core/endpoint/http/a/c;->a(Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;)Lcom/google/gson/JsonElement;

    move-result-object v2

    .line 33
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/RouterRequestJs;->result:Lbr/com/itau/sdk/android/core/webview/RouterRequestJs$RouterRequestResult;

    if-eqz v2, :cond_11

    invoke-virtual {v2}, Lcom/google/gson/JsonElement;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_13

    :cond_11
    const-string v1, ""

    :goto_13
    invoke-interface {v0, p2, v1, p3}, Lbr/com/itau/sdk/android/core/webview/RouterRequestJs$RouterRequestResult;->onSuccess(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_16
    .catch Lbr/com/itau/sdk/android/core/exception/BackendException; {:try_start_0 .. :try_end_16} :catch_17
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_16} :catch_2f

    .line 39
    goto :goto_3d

    .line 34
    :catch_17
    move-exception v2

    .line 35
    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->excludeFieldsWithoutExposeAnnotation()Lcom/google/gson/GsonBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v3

    .line 36
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/RouterRequestJs;->result:Lbr/com/itau/sdk/android/core/webview/RouterRequestJs$RouterRequestResult;

    invoke-virtual {v3, v2}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p2, v1, p3}, Lbr/com/itau/sdk/android/core/webview/RouterRequestJs$RouterRequestResult;->onError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    goto :goto_3d

    .line 37
    :catch_2f
    move-exception v2

    .line 38
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/RouterRequestJs;->result:Lbr/com/itau/sdk/android/core/webview/RouterRequestJs$RouterRequestResult;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbr/com/itau/sdk/android/core/exception/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p2, v1, p3}, Lbr/com/itau/sdk/android/core/webview/RouterRequestJs$RouterRequestResult;->onError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    :goto_3d
    return-void
.end method

.method public static newInstance(Lbr/com/itau/sdk/android/core/webview/RouterRequestJs$RouterRequestResult;)Lbr/com/itau/sdk/android/core/webview/RouterRequestJs;
    .registers 2

    .line 20
    new-instance v0, Lbr/com/itau/sdk/android/core/webview/RouterRequestJs;

    invoke-direct {v0}, Lbr/com/itau/sdk/android/core/webview/RouterRequestJs;-><init>()V

    .line 21
    iput-object p0, v0, Lbr/com/itau/sdk/android/core/webview/RouterRequestJs;->result:Lbr/com/itau/sdk/android/core/webview/RouterRequestJs$RouterRequestResult;

    .line 22
    return-object v0
.end method


# virtual methods
.method public find(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .line 46
    invoke-static {}, Lbr/com/itau/sdk/android/core/webview/MemoryStorageWebView;->getInstance()Lbr/com/itau/sdk/android/core/webview/MemoryStorageWebView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbr/com/itau/sdk/android/core/webview/MemoryStorageWebView;->findItem(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 47
    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_11

    :cond_f
    const-string v0, ""

    :goto_11
    return-object v0
.end method

.method public routerRequest(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .line 28
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/RouterRequestJs;->$:[B

    const/16 v1, 0xf

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0, v0, v0}, Lbr/com/itau/sdk/android/core/webview/RouterRequestJs;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/a/c;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->api(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/sdk/android/core/endpoint/http/a/c;

    .line 30
    invoke-static {p0, v2, p1, p2}, Lbr/com/itau/sdk/android/core/webview/RouterRequestJs$$Lambda$1;->lambdaFactory$(Lbr/com/itau/sdk/android/core/webview/RouterRequestJs;Lbr/com/itau/sdk/android/core/endpoint/http/a/c;Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/type/BackendCall;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 41
    return-void
.end method

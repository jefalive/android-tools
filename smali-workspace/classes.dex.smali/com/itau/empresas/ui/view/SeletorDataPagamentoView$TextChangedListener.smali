.class Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$TextChangedListener;
.super Ljava/lang/Object;
.source "SeletorDataPagamentoView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TextChangedListener"
.end annotation


# instance fields
.field private interno:Z

.field final synthetic this$0:Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;


# direct methods
.method private constructor <init>(Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;)V
    .registers 2

    .line 114
    iput-object p1, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$TextChangedListener;->this$0:Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$1;)V
    .registers 3
    .param p1, "x0"    # Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;
    .param p2, "x1"    # Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$1;

    .line 114
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$TextChangedListener;-><init>(Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 5
    .param p1, "s"    # Landroid/text/Editable;

    .line 130
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$TextChangedListener;->interno:Z

    if-eqz v0, :cond_d

    .line 131
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$TextChangedListener;->interno:Z

    .line 132
    return-void

    .line 135
    :cond_d
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 136
    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$TextChangedListener;->this$0:Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->getValorMinimo()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .local v2, "dia":I
    goto :goto_2a

    .line 138
    .end local v2    # "dia":I
    :cond_22
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 140
    .local v2, "dia":I
    :goto_2a
    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$TextChangedListener;->this$0:Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->getValorMinimo()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v2, v0, :cond_54

    .line 141
    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$TextChangedListener;->this$0:Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;

    iget-object v0, v0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->diaSelecionado:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$TextChangedListener;->this$0:Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;

    invoke-virtual {v1}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->getValorMinimo()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$TextChangedListener;->this$0:Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;

    # invokes: Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->mostraHintError()V
    invoke-static {v0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->access$100(Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;)V

    .line 143
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$TextChangedListener;->interno:Z

    .line 145
    :cond_54
    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$TextChangedListener;->this$0:Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->getValorMaximo()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le v2, v0, :cond_7e

    .line 146
    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$TextChangedListener;->this$0:Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;

    iget-object v0, v0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->diaSelecionado:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$TextChangedListener;->this$0:Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;

    invoke-virtual {v1}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->getValorMaximo()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 147
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$TextChangedListener;->interno:Z

    .line 148
    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$TextChangedListener;->this$0:Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;

    # invokes: Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->mostraHintError()V
    invoke-static {v0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->access$100(Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;)V

    .line 151
    :cond_7e
    iget-object v0, p0, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView$TextChangedListener;->this$0:Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;

    # invokes: Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->configuraAcessibilidade()V
    invoke-static {v0}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->access$200(Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;)V

    .line 152
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .line 121
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .line 126
    return-void
.end method

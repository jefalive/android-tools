.class public Lcom/itau/empresas/ui/dialog/DialogSucesso;
.super Landroid/app/DialogFragment;
.source "DialogSucesso.java"

# interfaces
.implements Lcom/itau/empresas/ui/view/CheckSucesso$ListenerAnimacao;


# instance fields
.field private button:Lcom/itau/empresas/ui/view/CheckSucesso;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 16
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public finish()V
    .registers 1

    .line 57
    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/DialogSucesso;->dismiss()V

    .line 58
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 38
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v1

    .line 39
    .local v1, "dialog":Landroid/app/Dialog;
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 41
    return-object v1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 25
    const v0, 0x7f03008b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 26
    .local v2, "view":Landroid/view/View;
    const v0, 0x7f0e03a8

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/CheckSucesso;

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/DialogSucesso;->button:Lcom/itau/empresas/ui/view/CheckSucesso;

    .line 27
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/DialogSucesso;->button:Lcom/itau/empresas/ui/view/CheckSucesso;

    invoke-virtual {v0, p0}, Lcom/itau/empresas/ui/view/CheckSucesso;->setListenerAnimacao(Lcom/itau/empresas/ui/view/CheckSucesso$ListenerAnimacao;)V

    .line 28
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/DialogSucesso;->button:Lcom/itau/empresas/ui/view/CheckSucesso;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/CheckSucesso;->startAnimating()V

    .line 29
    return-object v2
.end method

.method public showDialog(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/DialogSucesso;
    .registers 4
    .param p1, "activity"    # Landroid/support/v7/app/AppCompatActivity;

    .line 46
    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_27

    .line 47
    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_27

    .line 48
    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/ui/dialog/DialogSucesso;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 52
    :cond_27
    return-object p0
.end method

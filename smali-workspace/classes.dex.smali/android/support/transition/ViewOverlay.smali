.class Landroid/support/transition/ViewOverlay;
.super Ljava/lang/Object;
.source "ViewOverlay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/transition/ViewOverlay$OverlayViewGroup;
    }
.end annotation


# instance fields
.field protected mOverlayViewGroup:Landroid/support/transition/ViewOverlay$OverlayViewGroup;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/view/View;)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "hostView"    # Landroid/view/ViewGroup;
    .param p3, "requestingView"    # Landroid/view/View;

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Landroid/support/transition/ViewOverlay$OverlayViewGroup;

    invoke-direct {v0, p1, p2, p3, p0}, Landroid/support/transition/ViewOverlay$OverlayViewGroup;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/view/View;Landroid/support/transition/ViewOverlay;)V

    iput-object v0, p0, Landroid/support/transition/ViewOverlay;->mOverlayViewGroup:Landroid/support/transition/ViewOverlay$OverlayViewGroup;

    .line 48
    return-void
.end method

.method public static createFrom(Landroid/view/View;)Landroid/support/transition/ViewOverlay;
    .registers 7
    .param p0, "view"    # Landroid/view/View;

    .line 64
    invoke-static {p0}, Landroid/support/transition/ViewOverlay;->getContentView(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v2

    .line 65
    .local v2, "contentView":Landroid/view/ViewGroup;
    if-eqz v2, :cond_28

    .line 66
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    .line 67
    .local v3, "numChildren":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_b
    if-ge v4, v3, :cond_1e

    .line 68
    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 69
    .local v5, "child":Landroid/view/View;
    instance-of v0, v5, Landroid/support/transition/ViewOverlay$OverlayViewGroup;

    if-eqz v0, :cond_1b

    .line 70
    move-object v0, v5

    check-cast v0, Landroid/support/transition/ViewOverlay$OverlayViewGroup;

    iget-object v0, v0, Landroid/support/transition/ViewOverlay$OverlayViewGroup;->mViewOverlay:Landroid/support/transition/ViewOverlay;

    return-object v0

    .line 67
    .end local v5    # "child":Landroid/view/View;
    :cond_1b
    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    .line 73
    .end local v4    # "i":I
    :cond_1e
    new-instance v0, Landroid/support/transition/ViewGroupOverlay;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v2, p0}, Landroid/support/transition/ViewGroupOverlay;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Landroid/view/View;)V

    return-object v0

    .line 75
    .end local v3    # "numChildren":I
    :cond_28
    const/4 v0, 0x0

    return-object v0
.end method

.method static getContentView(Landroid/view/View;)Landroid/view/ViewGroup;
    .registers 4
    .param p0, "view"    # Landroid/view/View;

    .line 51
    move-object v2, p0

    .line 52
    .local v2, "parent":Landroid/view/View;
    :cond_1
    :goto_1
    if-eqz v2, :cond_24

    .line 53
    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x1020002

    if-ne v0, v1, :cond_14

    instance-of v0, v2, Landroid/view/ViewGroup;

    if-eqz v0, :cond_14

    .line 54
    move-object v0, v2

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0

    .line 56
    :cond_14
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 57
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/ViewGroup;

    goto :goto_1

    .line 60
    :cond_24
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public add(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .line 96
    iget-object v0, p0, Landroid/support/transition/ViewOverlay;->mOverlayViewGroup:Landroid/support/transition/ViewOverlay$OverlayViewGroup;

    invoke-virtual {v0, p1}, Landroid/support/transition/ViewOverlay$OverlayViewGroup;->add(Landroid/graphics/drawable/Drawable;)V

    .line 97
    return-void
.end method

.method public remove(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .line 106
    iget-object v0, p0, Landroid/support/transition/ViewOverlay;->mOverlayViewGroup:Landroid/support/transition/ViewOverlay$OverlayViewGroup;

    invoke-virtual {v0, p1}, Landroid/support/transition/ViewOverlay$OverlayViewGroup;->remove(Landroid/graphics/drawable/Drawable;)V

    .line 107
    return-void
.end method

.class public Lcom/itau/empresas/api/model/ContasAPagarVO;
.super Ljava/lang/Object;
.source "ContasAPagarVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private contasAlcada:Lcom/itau/empresas/api/model/ContasAlcada;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "contas"
    .end annotation
.end field

.field private contasModulosVO:Lcom/itau/empresas/api/model/ContasModulosVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "contas_modulo"
    .end annotation
.end field

.field private obrigatorioVistadorVO:Lcom/itau/empresas/api/model/ObrigatorioVistadorVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "obrigatorio_vistador"
    .end annotation
.end field

.field private salarioAlcadaVO:Lcom/itau/empresas/api/model/SalarioAlcadaVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "salario"
    .end annotation
.end field

.field private salarioModuloVO:Lcom/itau/empresas/api/model/SalarioModuloVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "salario_modulo"
    .end annotation
.end field

.field private tributosAlcadaVO:Lcom/itau/empresas/api/model/TributosAlcadaVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tributos"
    .end annotation
.end field

.field private tributosModuloVO:Lcom/itau/empresas/api/model/TributosModuloVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tributos_modulo"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getObrigatorioVistadorVO()Lcom/itau/empresas/api/model/ObrigatorioVistadorVO;
    .registers 2

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/api/model/ContasAPagarVO;->obrigatorioVistadorVO:Lcom/itau/empresas/api/model/ObrigatorioVistadorVO;

    return-object v0
.end method

.method public getTributosAlcadaVO()Lcom/itau/empresas/api/model/TributosAlcadaVO;
    .registers 2

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/api/model/ContasAPagarVO;->tributosAlcadaVO:Lcom/itau/empresas/api/model/TributosAlcadaVO;

    return-object v0
.end method

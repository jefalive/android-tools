.class public Lcom/google/android/gms/internal/zzgr;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/zzgr$zza;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public zza(Landroid/content/Context;Lcom/google/android/gms/ads/internal/zza;Lcom/google/android/gms/internal/zzif$zza;Lcom/google/android/gms/internal/zzan;Lcom/google/android/gms/internal/zzjp;Lcom/google/android/gms/internal/zzex;Lcom/google/android/gms/internal/zzgr$zza;Lcom/google/android/gms/internal/zzcb;)Lcom/google/android/gms/internal/zzit;
    .registers 18

    iget-object v8, p3, Lcom/google/android/gms/internal/zzif$zza;->zzLe:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-boolean v0, v8, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->zzHT:Z

    if-eqz v0, :cond_16

    new-instance v0, Lcom/google/android/gms/internal/zzgu;

    move-object v1, p1

    move-object v2, p3

    move-object v3, p6

    move-object/from16 v4, p7

    move-object/from16 v5, p8

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/zzgu;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/zzif$zza;Lcom/google/android/gms/internal/zzex;Lcom/google/android/gms/internal/zzgr$zza;Lcom/google/android/gms/internal/zzcb;Lcom/google/android/gms/internal/zzjp;)V

    move-object v7, v0

    goto/16 :goto_9c

    :cond_16
    iget-boolean v0, v8, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->zzuk:Z

    if-eqz v0, :cond_5f

    instance-of v0, p2, Lcom/google/android/gms/ads/internal/zzp;

    if-eqz v0, :cond_33

    new-instance v0, Lcom/google/android/gms/internal/zzgv;

    move-object v1, p1

    move-object v2, p2

    check-cast v2, Lcom/google/android/gms/ads/internal/zzp;

    new-instance v3, Lcom/google/android/gms/internal/zzee;

    invoke-direct {v3}, Lcom/google/android/gms/internal/zzee;-><init>()V

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/zzgv;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/zzp;Lcom/google/android/gms/internal/zzee;Lcom/google/android/gms/internal/zzif$zza;Lcom/google/android/gms/internal/zzan;Lcom/google/android/gms/internal/zzgr$zza;)V

    move-object v7, v0

    goto/16 :goto_9c

    :cond_33
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid NativeAdManager type. Found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p2, :cond_4b

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    goto :goto_4d

    :cond_4b
    const-string v2, "null"

    :goto_4d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; Required: NativeAdManager."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5f
    iget-boolean v0, v8, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->zzHZ:Z

    if-eqz v0, :cond_6b

    new-instance v7, Lcom/google/android/gms/internal/zzgp;

    move-object/from16 v0, p7

    invoke-direct {v7, p1, p3, p5, v0}, Lcom/google/android/gms/internal/zzgp;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/zzif$zza;Lcom/google/android/gms/internal/zzjp;Lcom/google/android/gms/internal/zzgr$zza;)V

    goto :goto_9c

    :cond_6b
    sget-object v0, Lcom/google/android/gms/internal/zzbt;->zzwu:Lcom/google/android/gms/internal/zzbp;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzbp;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_95

    invoke-static {}, Lcom/google/android/gms/internal/zzne;->zzsk()Z

    move-result v0

    if-eqz v0, :cond_95

    invoke-static {}, Lcom/google/android/gms/internal/zzne;->isAtLeastL()Z

    move-result v0

    if-nez v0, :cond_95

    invoke-interface {p5}, Lcom/google/android/gms/internal/zzjp;->zzaN()Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->zzui:Z

    if-eqz v0, :cond_95

    new-instance v7, Lcom/google/android/gms/internal/zzgt;

    move-object/from16 v0, p7

    invoke-direct {v7, p1, p3, p5, v0}, Lcom/google/android/gms/internal/zzgt;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/zzif$zza;Lcom/google/android/gms/internal/zzjp;Lcom/google/android/gms/internal/zzgr$zza;)V

    goto :goto_9c

    :cond_95
    new-instance v7, Lcom/google/android/gms/internal/zzgs;

    move-object/from16 v0, p7

    invoke-direct {v7, p1, p3, p5, v0}, Lcom/google/android/gms/internal/zzgs;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/zzif$zza;Lcom/google/android/gms/internal/zzjp;Lcom/google/android/gms/internal/zzgr$zza;)V

    :goto_9c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AdRenderer: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaI(Ljava/lang/String;)V

    invoke-interface {v7}, Lcom/google/android/gms/internal/zzit;->zzgd()Ljava/lang/Object;

    return-object v7
.end method

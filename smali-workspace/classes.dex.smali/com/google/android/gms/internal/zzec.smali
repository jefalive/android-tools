.class Lcom/google/android/gms/internal/zzec;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# instance fields
.field final zzAC:I

.field final zzpS:Ljava/lang/String;

.field final zzqH:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Ljava/lang/String;I)V
    .registers 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/zzec;->zzqH:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzec;->zzpS:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/gms/internal/zzec;->zzAC:I

    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/internal/zzea;)V
    .registers 5

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzea;->zzei()Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzea;->getAdUnitId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzea;->getNetworkType()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/internal/zzec;-><init>(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Ljava/lang/String;I)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .registers 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "\u0000"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v0, v2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_15

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Incorrect field count for QueueSeed."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_15
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    const/4 v0, 0x0

    :try_start_1a
    aget-object v0, v2, v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v4

    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {v0, v4, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzec;->zzpS:Ljava/lang/String;

    const/4 v0, 0x1

    aget-object v0, v2, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/zzec;->zzAC:I

    const/4 v0, 0x2

    aget-object v0, v2, v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v5

    array-length v0, v5

    const/4 v1, 0x0

    invoke-virtual {v3, v5, v1, v0}, Landroid/os/Parcel;->unmarshall([BII)V

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    sget-object v0, Lcom/google/android/gms/ads/internal/client/AdRequestParcel;->CREATOR:Lcom/google/android/gms/ads/internal/client/zzg;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/ads/internal/client/zzg;->zzb(Landroid/os/Parcel;)Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzec;->zzqH:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;
    :try_end_4c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1a .. :try_end_4c} :catch_50
    .catchall {:try_start_1a .. :try_end_4c} :catchall_59

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    goto :goto_5e

    :catch_50
    move-exception v4

    :try_start_51
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Malformed QueueSeed encoding."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_59
    .catchall {:try_start_51 .. :try_end_59} :catchall_59

    :catchall_59
    move-exception v6

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v6

    :goto_5e
    return-void
.end method


# virtual methods
.method zzem()Ljava/lang/String;
    .registers 11

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/internal/zzec;->zzpS:Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    const/4 v0, 0x0

    invoke-static {v3, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v4

    iget v0, p0, Lcom/google/android/gms/internal/zzec;->zzAC:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/gms/internal/zzec;->zzqH:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/ads/internal/client/AdRequestParcel;->writeToParcel(Landroid/os/Parcel;I)V

    invoke-virtual {v2}, Landroid/os/Parcel;->marshall()[B

    move-result-object v6

    const/4 v0, 0x0

    invoke-static {v6, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u0000"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u0000"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_46
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_4 .. :try_end_46} :catch_4b
    .catchall {:try_start_4 .. :try_end_46} :catchall_55

    move-result-object v8

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    return-object v8

    :catch_4b
    move-exception v3

    const-string v0, "QueueSeed encode failed because UTF-8 is not available."

    :try_start_4e
    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->e(Ljava/lang/String;)V
    :try_end_51
    .catchall {:try_start_4e .. :try_end_51} :catchall_55

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    goto :goto_5a

    :catchall_55
    move-exception v9

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v9

    :goto_5a
    const-string v0, ""

    return-object v0
.end method

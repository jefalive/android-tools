.class final Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;
.super Ljava/lang/Object;
.source "FormatadorBase.java"

# interfaces
.implements Lbr/com/concretesolutions/canarinho/formatador/Formatador;


# instance fields
.field private final formatted:Ljava/util/regex/Pattern;

.field private final formattedReplacement:Ljava/lang/String;

.field private final unformatted:Ljava/util/regex/Pattern;

.field private final unformattedReplacement:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/util/regex/Pattern;Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)V
    .registers 5
    .param p1, "formatted"    # Ljava/util/regex/Pattern;
    .param p2, "formattedReplacement"    # Ljava/lang/String;
    .param p3, "unformatted"    # Ljava/util/regex/Pattern;
    .param p4, "unformattedReplacement"    # Ljava/lang/String;

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->formatted:Ljava/util/regex/Pattern;

    .line 26
    iput-object p2, p0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->formattedReplacement:Ljava/lang/String;

    .line 27
    iput-object p3, p0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->unformatted:Ljava/util/regex/Pattern;

    .line 28
    iput-object p4, p0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->unformattedReplacement:Ljava/lang/String;

    .line 29
    return-void
.end method

.method private matchAndReplace(Ljava/util/regex/Matcher;Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .param p1, "matcher"    # Ljava/util/regex/Matcher;
    .param p2, "replacement"    # Ljava/lang/String;

    .line 81
    invoke-virtual {p1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 82
    invoke-virtual {p1, p2}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 85
    :cond_b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Valor n\u00e3o est\u00e1 formatado propriamente."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final desformata(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 48
    if-nez p1, :cond_a

    .line 49
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value may not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_a
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->unformatted:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 53
    return-object p1

    .line 56
    :cond_17
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->formatted:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 57
    .local v2, "matcher":Ljava/util/regex/Matcher;
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->unformattedReplacement:Ljava/lang/String;

    invoke-direct {p0, v2, v0}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->matchAndReplace(Ljava/util/regex/Matcher;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final estaFormatado(Ljava/lang/String;)Z
    .registers 4
    .param p1, "value"    # Ljava/lang/String;

    .line 63
    if-nez p1, :cond_a

    .line 64
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "value must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_a
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->formatted:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method

.method public final formata(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .param p1, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .line 34
    if-nez p1, :cond_a

    .line 35
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Value may not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_a
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->formatted:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 39
    return-object p1

    .line 42
    :cond_17
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->unformatted:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    iget-object v1, p0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->formattedReplacement:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->matchAndReplace(Ljava/util/regex/Matcher;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final podeSerFormatado(Ljava/lang/String;)Z
    .registers 3
    .param p1, "value"    # Ljava/lang/String;

    .line 72
    if-nez p1, :cond_4

    .line 73
    const/4 v0, 0x0

    return v0

    .line 76
    :cond_4
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->unformatted:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method

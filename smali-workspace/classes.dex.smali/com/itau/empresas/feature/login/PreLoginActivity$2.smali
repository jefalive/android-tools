.class Lcom/itau/empresas/feature/login/PreLoginActivity$2;
.super Ljava/lang/Object;
.source "PreLoginActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/login/PreLoginActivity;->executaModoMultiOperador()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/login/PreLoginActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/login/PreLoginActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/login/PreLoginActivity;

    .line 182
    iput-object p1, p0, Lcom/itau/empresas/feature/login/PreLoginActivity$2;->this$0:Lcom/itau/empresas/feature/login/PreLoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 3

    .line 185
    # getter for: Lcom/itau/empresas/feature/login/PreLoginActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/itau/empresas/feature/login/PreLoginActivity;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, ">>Modo Multi Operador"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginActivity$2;->this$0:Lcom/itau/empresas/feature/login/PreLoginActivity;

    # getter for: Lcom/itau/empresas/feature/login/PreLoginActivity;->preLoginInput:Lcom/itau/empresas/feature/login/PreLoginInput;
    invoke-static {v0}, Lcom/itau/empresas/feature/login/PreLoginActivity;->access$000(Lcom/itau/empresas/feature/login/PreLoginActivity;)Lcom/itau/empresas/feature/login/PreLoginInput;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginActivity$2;->this$0:Lcom/itau/empresas/feature/login/PreLoginActivity;

    .line 187
    # getter for: Lcom/itau/empresas/feature/login/PreLoginActivity;->strategyAtual:Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;
    invoke-static {v1}, Lcom/itau/empresas/feature/login/PreLoginActivity;->access$200(Lcom/itau/empresas/feature/login/PreLoginActivity;)Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;

    .line 188
    invoke-virtual {v1}, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaDocStrategy;->getListaOperadores()Ljava/util/List;

    move-result-object v1

    .line 186
    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/login/PreLoginInput;->modoListaOperadores(Ljava/util/List;)V

    .line 189
    return-void
.end method

.class public Lcom/itau/empresas/feature/atendimento/model/AtendimentoVO;
.super Ljava/lang/Object;
.source "AtendimentoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private faqs:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "faqs"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;>;"
        }
    .end annotation
.end field

.field private produtos:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "produtos"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoProduto;>;"
        }
    .end annotation
.end field

.field private sugestoes:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "sugestoes"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Ljava/lang/String;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFaqs()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;>;"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoVO;->faqs:Ljava/util/ArrayList;

    return-object v0
.end method

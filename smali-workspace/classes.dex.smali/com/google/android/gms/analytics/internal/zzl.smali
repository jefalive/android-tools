.class Lcom/google/android/gms/analytics/internal/zzl;
.super Lcom/google/android/gms/analytics/internal/zzd;


# instance fields
.field private mStarted:Z

.field private final zzQY:Lcom/google/android/gms/analytics/internal/zzj;

.field private final zzQZ:Lcom/google/android/gms/analytics/internal/zzah;

.field private final zzRa:Lcom/google/android/gms/analytics/internal/zzag;

.field private final zzRb:Lcom/google/android/gms/analytics/internal/zzi;

.field private zzRc:J

.field private final zzRd:Lcom/google/android/gms/analytics/internal/zzt;

.field private final zzRe:Lcom/google/android/gms/analytics/internal/zzt;

.field private final zzRf:Lcom/google/android/gms/analytics/internal/zzaj;

.field private zzRg:J

.field private zzRh:Z


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/analytics/internal/zzf;Lcom/google/android/gms/analytics/internal/zzg;)V
    .registers 5

    invoke-direct {p0, p1}, Lcom/google/android/gms/analytics/internal/zzd;-><init>(Lcom/google/android/gms/analytics/internal/zzf;)V

    invoke-static {p2}, Lcom/google/android/gms/common/internal/zzx;->zzz(Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRc:J

    invoke-virtual {p2, p1}, Lcom/google/android/gms/analytics/internal/zzg;->zzk(Lcom/google/android/gms/analytics/internal/zzf;)Lcom/google/android/gms/analytics/internal/zzag;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRa:Lcom/google/android/gms/analytics/internal/zzag;

    invoke-virtual {p2, p1}, Lcom/google/android/gms/analytics/internal/zzg;->zzm(Lcom/google/android/gms/analytics/internal/zzf;)Lcom/google/android/gms/analytics/internal/zzj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {p2, p1}, Lcom/google/android/gms/analytics/internal/zzg;->zzn(Lcom/google/android/gms/analytics/internal/zzf;)Lcom/google/android/gms/analytics/internal/zzah;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzQZ:Lcom/google/android/gms/analytics/internal/zzah;

    invoke-virtual {p2, p1}, Lcom/google/android/gms/analytics/internal/zzg;->zzo(Lcom/google/android/gms/analytics/internal/zzf;)Lcom/google/android/gms/analytics/internal/zzi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRb:Lcom/google/android/gms/analytics/internal/zzi;

    new-instance v0, Lcom/google/android/gms/analytics/internal/zzaj;

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjl()Lcom/google/android/gms/internal/zzmq;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/analytics/internal/zzaj;-><init>(Lcom/google/android/gms/internal/zzmq;)V

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRf:Lcom/google/android/gms/analytics/internal/zzaj;

    new-instance v0, Lcom/google/android/gms/analytics/internal/zzl$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/analytics/internal/zzl$1;-><init>(Lcom/google/android/gms/analytics/internal/zzl;Lcom/google/android/gms/analytics/internal/zzf;)V

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRd:Lcom/google/android/gms/analytics/internal/zzt;

    new-instance v0, Lcom/google/android/gms/analytics/internal/zzl$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/analytics/internal/zzl$2;-><init>(Lcom/google/android/gms/analytics/internal/zzl;Lcom/google/android/gms/analytics/internal/zzf;)V

    iput-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRe:Lcom/google/android/gms/analytics/internal/zzt;

    return-void
.end method

.method private zza(Lcom/google/android/gms/analytics/internal/zzh;Lcom/google/android/gms/internal/zzpr;)V
    .registers 14

    invoke-static {p1}, Lcom/google/android/gms/common/internal/zzx;->zzz(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/gms/common/internal/zzx;->zzz(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/google/android/gms/analytics/zza;

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzji()Lcom/google/android/gms/analytics/internal/zzf;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/gms/analytics/zza;-><init>(Lcom/google/android/gms/analytics/internal/zzf;)V

    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzh;->zzjE()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/analytics/zza;->zzaS(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzh;->zzjF()Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/analytics/zza;->enableAdvertisingIdCollection(Z)V

    invoke-virtual {v2}, Lcom/google/android/gms/analytics/zza;->zziy()Lcom/google/android/gms/measurement/zzc;

    move-result-object v3

    const-class v0, Lcom/google/android/gms/internal/zzke;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/measurement/zzc;->zzf(Ljava/lang/Class;)Lcom/google/android/gms/measurement/zze;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/google/android/gms/internal/zzke;

    const-string v0, "data"

    invoke-virtual {v4, v0}, Lcom/google/android/gms/internal/zzke;->zzaX(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Lcom/google/android/gms/internal/zzke;->zzI(Z)V

    invoke-virtual {v3, p2}, Lcom/google/android/gms/measurement/zzc;->zzb(Lcom/google/android/gms/measurement/zze;)V

    const-class v0, Lcom/google/android/gms/internal/zzkd;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/measurement/zzc;->zzf(Ljava/lang/Class;)Lcom/google/android/gms/measurement/zze;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/gms/internal/zzkd;

    const-class v0, Lcom/google/android/gms/internal/zzpq;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/measurement/zzc;->zzf(Ljava/lang/Class;)Lcom/google/android/gms/measurement/zze;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/gms/internal/zzpq;

    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzh;->zzn()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_54
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/util/Map$Entry;

    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Ljava/lang/String;

    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Ljava/lang/String;

    const-string v0, "an"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7b

    invoke-virtual {v6, v10}, Lcom/google/android/gms/internal/zzpq;->setAppName(Ljava/lang/String;)V

    goto :goto_ae

    :cond_7b
    const-string v0, "av"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_87

    invoke-virtual {v6, v10}, Lcom/google/android/gms/internal/zzpq;->setAppVersion(Ljava/lang/String;)V

    goto :goto_ae

    :cond_87
    const-string v0, "aid"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_93

    invoke-virtual {v6, v10}, Lcom/google/android/gms/internal/zzpq;->setAppId(Ljava/lang/String;)V

    goto :goto_ae

    :cond_93
    const-string v0, "aiid"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9f

    invoke-virtual {v6, v10}, Lcom/google/android/gms/internal/zzpq;->setAppInstallerId(Ljava/lang/String;)V

    goto :goto_ae

    :cond_9f
    const-string v0, "uid"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_ab

    invoke-virtual {v4, v10}, Lcom/google/android/gms/internal/zzke;->setUserId(Ljava/lang/String;)V

    goto :goto_ae

    :cond_ab
    invoke-virtual {v5, v9, v10}, Lcom/google/android/gms/internal/zzkd;->set(Ljava/lang/String;Ljava/lang/String;)V

    :goto_ae
    goto/16 :goto_54

    :cond_b0
    const-string v0, "Sending installation campaign to"

    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzh;->zzjE()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p2}, Lcom/google/android/gms/analytics/internal/zzl;->zzb(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjq()Lcom/google/android/gms/analytics/internal/zzai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzai;->zzlF()J

    move-result-wide v0

    invoke-virtual {v3, v0, v1}, Lcom/google/android/gms/measurement/zzc;->zzM(J)V

    invoke-virtual {v3}, Lcom/google/android/gms/measurement/zzc;->zzAy()V

    return-void
.end method

.method static synthetic zza(Lcom/google/android/gms/analytics/internal/zzl;)V
    .registers 1

    invoke-direct {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjV()V

    return-void
.end method

.method static synthetic zzb(Lcom/google/android/gms/analytics/internal/zzl;)V
    .registers 1

    invoke-direct {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjW()V

    return-void
.end method

.method private zzbk(Ljava/lang/String;)Z
    .registers 4

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_c

    const/4 v0, 0x1

    goto :goto_d

    :cond_c
    const/4 v0, 0x0

    :goto_d
    return v0
.end method

.method private zzjT()V
    .registers 7

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzji()Lcom/google/android/gms/analytics/internal/zzf;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/analytics/internal/zzf;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/analytics/AnalyticsReceiver;->zzY(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string v0, "AnalyticsReceiver is not registered or is disabled. Register the receiver for reliable dispatching on non-Google Play devices. See http://goo.gl/8Rd3yj for instructions."

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbg(Ljava/lang/String;)V

    goto :goto_1f

    :cond_14
    invoke-static {v2}, Lcom/google/android/gms/analytics/AnalyticsService;->zzZ(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_1f

    const-string v0, "AnalyticsService is not registered or is disabled. Analytics service at risk of not starting. See http://goo.gl/8Rd3yj for instructions."

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbh(Ljava/lang/String;)V

    :cond_1f
    :goto_1f
    invoke-static {v2}, Lcom/google/android/gms/analytics/CampaignTrackingReceiver;->zzY(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_2b

    const-string v0, "CampaignTrackingReceiver is not registered, not exported or is disabled. Installation campaign tracking is not possible. See http://goo.gl/8Rd3yj for instructions."

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbg(Ljava/lang/String;)V

    goto :goto_36

    :cond_2b
    invoke-static {v2}, Lcom/google/android/gms/analytics/CampaignTrackingService;->zzZ(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_36

    const-string v0, "CampaignTrackingService is not registered or is disabled. Installation campaign tracking is not possible. See http://goo.gl/8Rd3yj for instructions."

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbg(Ljava/lang/String;)V

    :cond_36
    :goto_36
    return-void
.end method

.method private zzjV()V
    .registers 2

    new-instance v0, Lcom/google/android/gms/analytics/internal/zzl$4;

    invoke-direct {v0, p0}, Lcom/google/android/gms/analytics/internal/zzl$4;-><init>(Lcom/google/android/gms/analytics/internal/zzl;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzb(Lcom/google/android/gms/analytics/internal/zzw;)V

    return-void
.end method

.method private zzjW()V
    .registers 5

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->zzjN()I

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkb()V
    :try_end_8
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_8} :catch_9

    goto :goto_f

    :catch_9
    move-exception v3

    const-string v0, "Failed to delete stale hits"

    invoke-virtual {p0, v0, v3}, Lcom/google/android/gms/analytics/internal/zzl;->zzd(Ljava/lang/String;Ljava/lang/Object;)V

    :goto_f
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRe:Lcom/google/android/gms/analytics/internal/zzt;

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/analytics/internal/zzr;->zzkT()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/analytics/internal/zzt;->zzt(J)V

    return-void
.end method

.method private zzkc()Z
    .registers 5

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRh:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    return v0

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkr()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzks()Z

    move-result v0

    if-nez v0, :cond_1c

    const/4 v0, 0x0

    return v0

    :cond_1c
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzki()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_28

    const/4 v0, 0x1

    goto :goto_29

    :cond_28
    const/4 v0, 0x0

    :goto_29
    return v0
.end method

.method private zzkd()V
    .registers 12

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjp()Lcom/google/android/gms/analytics/internal/zzv;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/analytics/internal/zzv;->zzlb()Z

    move-result v0

    if-nez v0, :cond_b

    return-void

    :cond_b
    invoke-virtual {v2}, Lcom/google/android/gms/analytics/internal/zzv;->zzbw()Z

    move-result v0

    if-nez v0, :cond_49

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjO()J

    move-result-wide v3

    const-wide/16 v0, 0x0

    cmp-long v0, v3, v0

    if-eqz v0, :cond_49

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjl()Lcom/google/android/gms/internal/zzmq;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzmq;->currentTimeMillis()J

    move-result-wide v5

    sub-long v0, v5, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v7

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkB()J

    move-result-wide v0

    cmp-long v0, v7, v0

    if-gtz v0, :cond_49

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkA()J

    move-result-wide v9

    const-string v0, "Dispatch alarm scheduled (ms)"

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/analytics/internal/zzl;->zza(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v2}, Lcom/google/android/gms/analytics/internal/zzv;->zzlc()V

    :cond_49
    return-void
.end method

.method private zzke()V
    .registers 15

    invoke-direct {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkd()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzki()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjq()Lcom/google/android/gms/analytics/internal/zzai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzai;->zzlH()J

    move-result-wide v8

    const-wide/16 v0, 0x0

    cmp-long v0, v8, v0

    if-eqz v0, :cond_39

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjl()Lcom/google/android/gms/internal/zzmq;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzmq;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v8

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v10

    sub-long v12, v4, v10

    const-wide/16 v0, 0x0

    cmp-long v0, v12, v0

    if-lez v0, :cond_2c

    move-wide v6, v12

    goto :goto_38

    :cond_2c
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzky()J

    move-result-wide v0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    :goto_38
    goto :goto_45

    :cond_39
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzky()J

    move-result-wide v0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    :goto_45
    const-string v0, "Dispatch scheduled (ms)"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/analytics/internal/zzl;->zza(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRd:Lcom/google/android/gms/analytics/internal/zzt;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzt;->zzbw()Z

    move-result v0

    if-eqz v0, :cond_69

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRd:Lcom/google/android/gms/analytics/internal/zzt;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzt;->zzkY()J

    move-result-wide v0

    add-long/2addr v0, v6

    const-wide/16 v2, 0x1

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v10

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRd:Lcom/google/android/gms/analytics/internal/zzt;

    invoke-virtual {v0, v10, v11}, Lcom/google/android/gms/analytics/internal/zzt;->zzu(J)V

    goto :goto_6e

    :cond_69
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRd:Lcom/google/android/gms/analytics/internal/zzt;

    invoke-virtual {v0, v6, v7}, Lcom/google/android/gms/analytics/internal/zzt;->zzt(J)V

    :goto_6e
    return-void
.end method

.method private zzkf()V
    .registers 1

    invoke-direct {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkg()V

    invoke-direct {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkh()V

    return-void
.end method

.method private zzkg()V
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRd:Lcom/google/android/gms/analytics/internal/zzt;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzt;->zzbw()Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v0, "All hits dispatched or no network/service. Going to power save mode"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbd(Ljava/lang/String;)V

    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRd:Lcom/google/android/gms/analytics/internal/zzt;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzt;->cancel()V

    return-void
.end method

.method private zzkh()V
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjp()Lcom/google/android/gms/analytics/internal/zzv;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/analytics/internal/zzv;->zzbw()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {v1}, Lcom/google/android/gms/analytics/internal/zzv;->cancel()V

    :cond_d
    return-void
.end method


# virtual methods
.method protected onServiceConnected()V
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjk()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkr()Z

    move-result v0

    if-nez v0, :cond_10

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjY()V

    :cond_10
    return-void
.end method

.method start()V
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjv()V

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->mStarted:Z

    if-nez v0, :cond_9

    const/4 v0, 0x1

    goto :goto_a

    :cond_9
    const/4 v0, 0x0

    :goto_a
    const-string v1, "Analytics backend already started"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->zza(ZLjava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->mStarted:Z

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkr()Z

    move-result v0

    if-nez v0, :cond_1f

    invoke-direct {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjT()V

    :cond_1f
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjo()Lcom/google/android/gms/measurement/zzg;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/analytics/internal/zzl$3;

    invoke-direct {v1, p0}, Lcom/google/android/gms/analytics/internal/zzl$3;-><init>(Lcom/google/android/gms/analytics/internal/zzl;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/zzg;->zzf(Ljava/lang/Runnable;)V

    return-void
.end method

.method public zzJ(Z)V
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkb()V

    return-void
.end method

.method public zza(Lcom/google/android/gms/analytics/internal/zzh;Z)J
    .registers 16

    invoke-static {p1}, Lcom/google/android/gms/common/internal/zzx;->zzz(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjv()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjk()V

    const-wide/16 v5, -0x1

    :try_start_b
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->beginTransaction()V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzh;->zzjD()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzh;->getClientId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/analytics/internal/zzj;->zza(JLjava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzh;->zzjD()J

    move-result-wide v1

    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzh;->getClientId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzh;->zzjE()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/gms/analytics/internal/zzj;->zza(JLjava/lang/String;Ljava/lang/String;)J

    move-result-wide v5

    if-nez p2, :cond_35

    invoke-virtual {p1, v5, v6}, Lcom/google/android/gms/analytics/internal/zzh;->zzn(J)V

    goto :goto_3b

    :cond_35
    const-wide/16 v0, 0x1

    add-long/2addr v0, v5

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/analytics/internal/zzh;->zzn(J)V

    :goto_3b
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/analytics/internal/zzj;->zzb(Lcom/google/android/gms/analytics/internal/zzh;)V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->setTransactionSuccessful()V
    :try_end_45
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b .. :try_end_45} :catch_52
    .catchall {:try_start_b .. :try_end_45} :catchall_67

    :try_start_45
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->endTransaction()V
    :try_end_4a
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_45 .. :try_end_4a} :catch_4b

    goto :goto_75

    :catch_4b
    move-exception v7

    const-string v0, "Failed to end transaction"

    invoke-virtual {p0, v0, v7}, Lcom/google/android/gms/analytics/internal/zzl;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_75

    :catch_52
    move-exception v7

    const-string v0, "Failed to update Analytics property"

    :try_start_55
    invoke-virtual {p0, v0, v7}, Lcom/google/android/gms/analytics/internal/zzl;->zze(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_58
    .catchall {:try_start_55 .. :try_end_58} :catchall_67

    const-wide/16 v8, -0x1

    :try_start_5a
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->endTransaction()V
    :try_end_5f
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_5a .. :try_end_5f} :catch_60

    goto :goto_66

    :catch_60
    move-exception v10

    const-string v0, "Failed to end transaction"

    invoke-virtual {p0, v0, v10}, Lcom/google/android/gms/analytics/internal/zzl;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    :goto_66
    return-wide v8

    :catchall_67
    move-exception v11

    :try_start_68
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->endTransaction()V
    :try_end_6d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_68 .. :try_end_6d} :catch_6e

    goto :goto_74

    :catch_6e
    move-exception v12

    const-string v0, "Failed to end transaction"

    invoke-virtual {p0, v0, v12}, Lcom/google/android/gms/analytics/internal/zzl;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    :goto_74
    throw v11

    :goto_75
    return-wide v5
.end method

.method public zza(Lcom/google/android/gms/analytics/internal/zzab;)V
    .registers 5

    invoke-static {p1}, Lcom/google/android/gms/common/internal/zzx;->zzz(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/google/android/gms/measurement/zzg;->zzjk()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjv()V

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRh:Z

    if-eqz v0, :cond_13

    const-string v0, "Hit delivery not possible. Missing network permissions. See http://goo.gl/8Rd3yj for instructions"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbe(Ljava/lang/String;)V

    goto :goto_18

    :cond_13
    const-string v0, "Delivering hit"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/analytics/internal/zzl;->zza(Ljava/lang/String;Ljava/lang/Object;)V

    :goto_18
    invoke-virtual {p0, p1}, Lcom/google/android/gms/analytics/internal/zzl;->zzf(Lcom/google/android/gms/analytics/internal/zzab;)Lcom/google/android/gms/analytics/internal/zzab;

    move-result-object p1

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjX()V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRb:Lcom/google/android/gms/analytics/internal/zzi;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/analytics/internal/zzi;->zzb(Lcom/google/android/gms/analytics/internal/zzab;)Z

    move-result v0

    if-eqz v0, :cond_2d

    const-string v0, "Hit sent to the device AnalyticsService for delivery"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbe(Ljava/lang/String;)V

    return-void

    :cond_2d
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkr()Z

    move-result v0

    if-eqz v0, :cond_41

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjm()Lcom/google/android/gms/analytics/internal/zzaf;

    move-result-object v0

    const-string v1, "Service unavailable on package side"

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/analytics/internal/zzaf;->zza(Lcom/google/android/gms/analytics/internal/zzab;Ljava/lang/String;)V

    return-void

    :cond_41
    :try_start_41
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/analytics/internal/zzj;->zzc(Lcom/google/android/gms/analytics/internal/zzab;)V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkb()V
    :try_end_49
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_41 .. :try_end_49} :catch_4a

    goto :goto_59

    :catch_4a
    move-exception v2

    const-string v0, "Delivery failed to save hit to a database"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/analytics/internal/zzl;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjm()Lcom/google/android/gms/analytics/internal/zzaf;

    move-result-object v0

    const-string v1, "deliver: failed to insert hit to database"

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/analytics/internal/zzaf;->zza(Lcom/google/android/gms/analytics/internal/zzab;Ljava/lang/String;)V

    :goto_59
    return-void
.end method

.method public zza(Lcom/google/android/gms/analytics/internal/zzw;J)V
    .registers 11

    invoke-static {}, Lcom/google/android/gms/measurement/zzg;->zzjk()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjv()V

    const-wide/16 v2, -0x1

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjq()Lcom/google/android/gms/analytics/internal/zzai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzai;->zzlH()J

    move-result-wide v4

    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-eqz v0, :cond_23

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjl()Lcom/google/android/gms/internal/zzmq;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzmq;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    :cond_23
    const-string v0, "Dispatching local hits. Elapsed time since last dispatch (ms)"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/analytics/internal/zzl;->zzb(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkr()Z

    move-result v0

    if-nez v0, :cond_39

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjX()V

    :cond_39
    :try_start_39
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjZ()Z

    move-result v6

    if-eqz v6, :cond_4c

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjo()Lcom/google/android/gms/measurement/zzg;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/analytics/internal/zzl$5;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/gms/analytics/internal/zzl$5;-><init>(Lcom/google/android/gms/analytics/internal/zzl;Lcom/google/android/gms/analytics/internal/zzw;J)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/measurement/zzg;->zzf(Ljava/lang/Runnable;)V

    goto :goto_67

    :cond_4c
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjq()Lcom/google/android/gms/analytics/internal/zzai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzai;->zzlI()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkb()V

    if-eqz p1, :cond_5c

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/google/android/gms/analytics/internal/zzw;->zzc(Ljava/lang/Throwable;)V

    :cond_5c
    iget-wide v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRg:J

    cmp-long v0, v0, p2

    if-eqz v0, :cond_67

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRa:Lcom/google/android/gms/analytics/internal/zzag;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzag;->zzlA()V
    :try_end_67
    .catch Ljava/lang/Throwable; {:try_start_39 .. :try_end_67} :catch_68

    :cond_67
    :goto_67
    goto :goto_7d

    :catch_68
    move-exception v6

    const-string v0, "Local dispatch failed"

    invoke-virtual {p0, v0, v6}, Lcom/google/android/gms/analytics/internal/zzl;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjq()Lcom/google/android/gms/analytics/internal/zzai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzai;->zzlI()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkb()V

    if-eqz p1, :cond_7d

    invoke-interface {p1, v6}, Lcom/google/android/gms/analytics/internal/zzw;->zzc(Ljava/lang/Throwable;)V

    :cond_7d
    :goto_7d
    return-void
.end method

.method public zzb(Lcom/google/android/gms/analytics/internal/zzw;)V
    .registers 4

    iget-wide v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRg:J

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/gms/analytics/internal/zzl;->zza(Lcom/google/android/gms/analytics/internal/zzw;J)V

    return-void
.end method

.method public zzbl(Ljava/lang/String;)V
    .registers 10

    invoke-static {p1}, Lcom/google/android/gms/common/internal/zzx;->zzcM(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjk()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjj()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjm()Lcom/google/android/gms/analytics/internal/zzaf;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/analytics/internal/zzam;->zza(Lcom/google/android/gms/analytics/internal/zzaf;Ljava/lang/String;)Lcom/google/android/gms/internal/zzpr;

    move-result-object v3

    if-nez v3, :cond_19

    const-string v0, "Parsing failed. Ignoring invalid campaign data"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/analytics/internal/zzl;->zzd(Ljava/lang/String;Ljava/lang/Object;)V

    return-void

    :cond_19
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjq()Lcom/google/android/gms/analytics/internal/zzai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzai;->zzlJ()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2d

    const-string v0, "Ignoring duplicate install campaign"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbg(Ljava/lang/String;)V

    return-void

    :cond_2d
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_39

    const-string v0, "Ignoring multiple install campaigns. original, new"

    invoke-virtual {p0, v0, v4, p1}, Lcom/google/android/gms/analytics/internal/zzl;->zzd(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    return-void

    :cond_39
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjq()Lcom/google/android/gms/analytics/internal/zzai;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/analytics/internal/zzai;->zzbp(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjq()Lcom/google/android/gms/analytics/internal/zzai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzai;->zzlG()Lcom/google/android/gms/analytics/internal/zzaj;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/analytics/internal/zzr;->zzkW()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/analytics/internal/zzaj;->zzv(J)Z

    move-result v0

    if-eqz v0, :cond_5c

    const-string v0, "Campaign received too late, ignoring"

    invoke-virtual {p0, v0, v3}, Lcom/google/android/gms/analytics/internal/zzl;->zzd(Ljava/lang/String;Ljava/lang/Object;)V

    return-void

    :cond_5c
    const-string v0, "Received installation campaign"

    invoke-virtual {p0, v0, v3}, Lcom/google/android/gms/analytics/internal/zzl;->zzb(Ljava/lang/String;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/analytics/internal/zzj;->zzr(J)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_6d
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7e

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/android/gms/analytics/internal/zzh;

    invoke-direct {p0, v7, v3}, Lcom/google/android/gms/analytics/internal/zzl;->zza(Lcom/google/android/gms/analytics/internal/zzh;Lcom/google/android/gms/internal/zzpr;)V

    goto :goto_6d

    :cond_7e
    return-void
.end method

.method protected zzc(Lcom/google/android/gms/analytics/internal/zzh;)V
    .registers 7

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjk()V

    const-string v0, "Sending first hit to property"

    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzh;->zzjE()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/analytics/internal/zzl;->zzb(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjq()Lcom/google/android/gms/analytics/internal/zzai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzai;->zzlG()Lcom/google/android/gms/analytics/internal/zzaj;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/analytics/internal/zzr;->zzkW()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/analytics/internal/zzaj;->zzv(J)Z

    move-result v0

    if-eqz v0, :cond_23

    return-void

    :cond_23
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjq()Lcom/google/android/gms/analytics/internal/zzai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzai;->zzlJ()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_32

    return-void

    :cond_32
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjm()Lcom/google/android/gms/analytics/internal/zzaf;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/google/android/gms/analytics/internal/zzam;->zza(Lcom/google/android/gms/analytics/internal/zzaf;Ljava/lang/String;)Lcom/google/android/gms/internal/zzpr;

    move-result-object v4

    const-string v0, "Found relevant installation campaign"

    invoke-virtual {p0, v0, v4}, Lcom/google/android/gms/analytics/internal/zzl;->zzb(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct {p0, p1, v4}, Lcom/google/android/gms/analytics/internal/zzl;->zza(Lcom/google/android/gms/analytics/internal/zzh;Lcom/google/android/gms/internal/zzpr;)V

    return-void
.end method

.method zzf(Lcom/google/android/gms/analytics/internal/zzab;)Lcom/google/android/gms/analytics/internal/zzab;
    .registers 10

    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzab;->zzlv()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    return-object p1

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjq()Lcom/google/android/gms/analytics/internal/zzai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzai;->zzlK()Lcom/google/android/gms/analytics/internal/zzai$zza;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/analytics/internal/zzai$zza;->zzlN()Landroid/util/Pair;

    move-result-object v3

    if-nez v3, :cond_1a

    return-object p1

    :cond_1a
    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object v4, v0

    check-cast v4, Ljava/lang/Long;

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object v5, v0

    check-cast v5, Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzab;->zzn()Ljava/util/Map;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    const-string v0, "_m"

    invoke-interface {v7, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0, p1, v7}, Lcom/google/android/gms/analytics/internal/zzab;->zza(Lcom/google/android/gms/analytics/internal/zzc;Lcom/google/android/gms/analytics/internal/zzab;Ljava/util/Map;)Lcom/google/android/gms/analytics/internal/zzab;

    move-result-object v0

    return-object v0
.end method

.method protected zziJ()V
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->zza()V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzQZ:Lcom/google/android/gms/analytics/internal/zzah;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzah;->zza()V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRb:Lcom/google/android/gms/analytics/internal/zzi;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzi;->zza()V

    return-void
.end method

.method public zzjO()J
    .registers 4

    invoke-static {}, Lcom/google/android/gms/measurement/zzg;->zzjk()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjv()V

    :try_start_6
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->zzjO()J
    :try_end_b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_6 .. :try_end_b} :catch_d

    move-result-wide v0

    return-wide v0

    :catch_d
    move-exception v2

    const-string v0, "Failed to get min/max hit times from local store"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/analytics/internal/zzl;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method protected zzjU()V
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjv()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjq()Lcom/google/android/gms/analytics/internal/zzai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzai;->zzlF()J

    const-string v0, "android.permission.ACCESS_NETWORK_STATE"

    invoke-direct {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbk(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1a

    const-string v0, "Missing required android.permission.ACCESS_NETWORK_STATE. Google Analytics disabled. See http://goo.gl/8Rd3yj for instructions"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbh(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkj()V

    :cond_1a
    const-string v0, "android.permission.INTERNET"

    invoke-direct {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbk(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2a

    const-string v0, "Missing required android.permission.INTERNET. Google Analytics disabled. See http://goo.gl/8Rd3yj for instructions"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbh(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkj()V

    :cond_2a
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/analytics/AnalyticsService;->zzZ(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3a

    const-string v0, "AnalyticsService registered in the app manifest and enabled"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbd(Ljava/lang/String;)V

    goto :goto_4f

    :cond_3a
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkr()Z

    move-result v0

    if-eqz v0, :cond_4a

    const-string v0, "Device AnalyticsService not registered! Hits will not be delivered reliably."

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbh(Ljava/lang/String;)V

    goto :goto_4f

    :cond_4a
    const-string v0, "AnalyticsService not registered in the app manifest. Hits might not be delivered reliably. See http://goo.gl/8Rd3yj for instructions."

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbg(Ljava/lang/String;)V

    :goto_4f
    iget-boolean v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRh:Z

    if-nez v0, :cond_68

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkr()Z

    move-result v0

    if-nez v0, :cond_68

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_68

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjX()V

    :cond_68
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkb()V

    return-void
.end method

.method protected zzjX()V
    .registers 4

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRh:Z

    if-eqz v0, :cond_5

    return-void

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkt()Z

    move-result v0

    if-nez v0, :cond_10

    return-void

    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRb:Lcom/google/android/gms/analytics/internal/zzi;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzi;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_19

    return-void

    :cond_19
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkO()J

    move-result-wide v1

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRf:Lcom/google/android/gms/analytics/internal/zzaj;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/analytics/internal/zzaj;->zzv(J)Z

    move-result v0

    if-eqz v0, :cond_48

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRf:Lcom/google/android/gms/analytics/internal/zzaj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzaj;->start()V

    const-string v0, "Connecting to service"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbd(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRb:Lcom/google/android/gms/analytics/internal/zzi;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzi;->connect()Z

    move-result v0

    if-eqz v0, :cond_48

    const-string v0, "Connected to service"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbd(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRf:Lcom/google/android/gms/analytics/internal/zzaj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzaj;->clear()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->onServiceConnected()V

    :cond_48
    return-void
.end method

.method public zzjY()V
    .registers 7

    invoke-static {}, Lcom/google/android/gms/measurement/zzg;->zzjk()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjv()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjj()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkt()Z

    move-result v0

    if-nez v0, :cond_18

    const-string v0, "Service client disabled. Can\'t dispatch local hits to device AnalyticsService"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbg(Ljava/lang/String;)V

    :cond_18
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRb:Lcom/google/android/gms/analytics/internal/zzi;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzi;->isConnected()Z

    move-result v0

    if-nez v0, :cond_26

    const-string v0, "Service not connected"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbd(Ljava/lang/String;)V

    return-void

    :cond_26
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2f

    return-void

    :cond_2f
    const-string v0, "Dispatching local hits to device AnalyticsService"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbd(Ljava/lang/String;)V

    :goto_34
    :try_start_34
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/analytics/internal/zzr;->zzkC()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/analytics/internal/zzj;->zzp(J)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4d

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkb()V
    :try_end_4c
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_34 .. :try_end_4c} :catch_4e

    return-void

    :cond_4d
    goto :goto_58

    :catch_4e
    move-exception v4

    const-string v0, "Failed to read hits from store"

    invoke-virtual {p0, v0, v4}, Lcom/google/android/gms/analytics/internal/zzl;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkf()V

    return-void

    :goto_58
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8a

    const/4 v0, 0x0

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/google/android/gms/analytics/internal/zzab;

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRb:Lcom/google/android/gms/analytics/internal/zzi;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/analytics/internal/zzi;->zzb(Lcom/google/android/gms/analytics/internal/zzab;)Z

    move-result v0

    if-nez v0, :cond_72

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkb()V

    return-void

    :cond_72
    invoke-interface {v3, v4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :try_start_75
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v4}, Lcom/google/android/gms/analytics/internal/zzab;->zzlq()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/analytics/internal/zzj;->zzq(J)V
    :try_end_7e
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_75 .. :try_end_7e} :catch_7f

    goto :goto_89

    :catch_7f
    move-exception v5

    const-string v0, "Failed to remove hit that was send for delivery"

    invoke-virtual {p0, v0, v5}, Lcom/google/android/gms/analytics/internal/zzl;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkf()V

    return-void

    :goto_89
    goto :goto_58

    :cond_8a
    goto :goto_34
.end method

.method protected zzjZ()Z
    .registers 19

    invoke-static {}, Lcom/google/android/gms/measurement/zzg;->zzjk()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjv()V

    const-string v0, "Dispatching a batch of local hits"

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbd(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzRb:Lcom/google/android/gms/analytics/internal/zzi;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzi;->isConnected()Z

    move-result v0

    if-nez v0, :cond_23

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkr()Z

    move-result v0

    if-nez v0, :cond_23

    const/4 v4, 0x1

    goto :goto_24

    :cond_23
    const/4 v4, 0x0

    :goto_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQZ:Lcom/google/android/gms/analytics/internal/zzah;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzah;->zzlB()Z

    move-result v0

    if-nez v0, :cond_30

    const/4 v5, 0x1

    goto :goto_31

    :cond_30
    const/4 v5, 0x0

    :goto_31
    if-eqz v4, :cond_3e

    if-eqz v5, :cond_3e

    const-string v0, "No network or service available. Will retry later"

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbd(Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0

    :cond_3e
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkC()I

    move-result v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/analytics/internal/zzr;->zzkD()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-long v6, v0

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    const-wide/16 v9, 0x0

    :goto_5a
    move-object/from16 v0, p0

    :try_start_5c
    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->beginTransaction()V

    invoke-interface {v8}, Ljava/util/List;->clear()V
    :try_end_64
    .catchall {:try_start_5c .. :try_end_64} :catchall_24c

    move-object/from16 v0, p0

    :try_start_66
    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0, v6, v7}, Lcom/google/android/gms/analytics/internal/zzj;->zzp(J)Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9a

    const-string v0, "Store is empty, nothing to dispatch"

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbd(Ljava/lang/String;)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkf()V
    :try_end_7c
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_66 .. :try_end_7c} :catch_aa
    .catchall {:try_start_66 .. :try_end_7c} :catchall_24c

    const/4 v12, 0x0

    move-object/from16 v0, p0

    :try_start_7f
    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->setTransactionSuccessful()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->endTransaction()V
    :try_end_8b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7f .. :try_end_8b} :catch_8c

    goto :goto_99

    :catch_8c
    move-exception v13

    const-string v0, "Failed to commit local dispatch transaction"

    move-object/from16 v1, p0

    invoke-virtual {v1, v0, v13}, Lcom/google/android/gms/analytics/internal/zzl;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkf()V

    const/4 v0, 0x0

    return v0

    :goto_99
    return v12

    :cond_9a
    const-string v0, "Hits loaded from store. count"

    :try_start_9c
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v2, p0

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/analytics/internal/zzl;->zza(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_a9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_9c .. :try_end_a9} :catch_aa
    .catchall {:try_start_9c .. :try_end_a9} :catchall_24c

    goto :goto_d3

    :catch_aa
    move-exception v12

    const-string v0, "Failed to read hits from persisted store"

    move-object/from16 v1, p0

    :try_start_af
    invoke-virtual {v1, v0, v12}, Lcom/google/android/gms/analytics/internal/zzl;->zzd(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkf()V
    :try_end_b5
    .catchall {:try_start_af .. :try_end_b5} :catchall_24c

    const/4 v13, 0x0

    move-object/from16 v0, p0

    :try_start_b8
    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->setTransactionSuccessful()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->endTransaction()V
    :try_end_c4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_b8 .. :try_end_c4} :catch_c5

    goto :goto_d2

    :catch_c5
    move-exception v14

    const-string v0, "Failed to commit local dispatch transaction"

    move-object/from16 v1, p0

    invoke-virtual {v1, v0, v14}, Lcom/google/android/gms/analytics/internal/zzl;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkf()V

    const/4 v0, 0x0

    return v0

    :goto_d2
    return v13

    :goto_d3
    :try_start_d3
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_d7
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_121

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/google/android/gms/analytics/internal/zzab;

    invoke-virtual {v13}, Lcom/google/android/gms/analytics/internal/zzab;->zzlq()J

    move-result-wide v0

    cmp-long v0, v0, v9

    if-nez v0, :cond_120

    const-string v0, "Database contains successfully uploaded hit"

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v3, p0

    invoke-virtual {v3, v0, v1, v2}, Lcom/google/android/gms/analytics/internal/zzl;->zzd(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkf()V
    :try_end_102
    .catchall {:try_start_d3 .. :try_end_102} :catchall_24c

    const/4 v14, 0x0

    move-object/from16 v0, p0

    :try_start_105
    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->setTransactionSuccessful()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->endTransaction()V
    :try_end_111
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_105 .. :try_end_111} :catch_112

    goto :goto_11f

    :catch_112
    move-exception v15

    const-string v0, "Failed to commit local dispatch transaction"

    move-object/from16 v1, p0

    invoke-virtual {v1, v0, v15}, Lcom/google/android/gms/analytics/internal/zzl;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkf()V

    const/4 v0, 0x0

    return v0

    :goto_11f
    return v14

    :cond_120
    goto :goto_d7

    :cond_121
    move-object/from16 v0, p0

    :try_start_123
    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzRb:Lcom/google/android/gms/analytics/internal/zzi;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzi;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1a9

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkr()Z

    move-result v0

    if-nez v0, :cond_1a9

    const-string v0, "Service connected, sending hits to the service"

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbd(Ljava/lang/String;)V

    :goto_13c
    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1a9

    const/4 v0, 0x0

    invoke-interface {v11, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/google/android/gms/analytics/internal/zzab;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzRb:Lcom/google/android/gms/analytics/internal/zzi;

    invoke-virtual {v0, v12}, Lcom/google/android/gms/analytics/internal/zzi;->zzb(Lcom/google/android/gms/analytics/internal/zzab;)Z

    move-result v0

    if-nez v0, :cond_155

    goto :goto_1a9

    :cond_155
    invoke-virtual {v12}, Lcom/google/android/gms/analytics/internal/zzab;->zzlq()J

    move-result-wide v0

    invoke-static {v9, v10, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v9

    invoke-interface {v11, v12}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    const-string v0, "Hit sent do device AnalyticsService for delivery"

    move-object/from16 v1, p0

    invoke-virtual {v1, v0, v12}, Lcom/google/android/gms/analytics/internal/zzl;->zzb(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_167
    .catchall {:try_start_123 .. :try_end_167} :catchall_24c

    move-object/from16 v0, p0

    :try_start_169
    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v12}, Lcom/google/android/gms/analytics/internal/zzab;->zzlq()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/analytics/internal/zzj;->zzq(J)V

    invoke-virtual {v12}, Lcom/google/android/gms/analytics/internal/zzab;->zzlq()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_17d
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_169 .. :try_end_17d} :catch_17e
    .catchall {:try_start_169 .. :try_end_17d} :catchall_24c

    goto :goto_1a7

    :catch_17e
    move-exception v13

    const-string v0, "Failed to remove hit that was send for delivery"

    move-object/from16 v1, p0

    :try_start_183
    invoke-virtual {v1, v0, v13}, Lcom/google/android/gms/analytics/internal/zzl;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkf()V
    :try_end_189
    .catchall {:try_start_183 .. :try_end_189} :catchall_24c

    const/4 v14, 0x0

    move-object/from16 v0, p0

    :try_start_18c
    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->setTransactionSuccessful()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->endTransaction()V
    :try_end_198
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_18c .. :try_end_198} :catch_199

    goto :goto_1a6

    :catch_199
    move-exception v15

    const-string v0, "Failed to commit local dispatch transaction"

    move-object/from16 v1, p0

    invoke-virtual {v1, v0, v15}, Lcom/google/android/gms/analytics/internal/zzl;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkf()V

    const/4 v0, 0x0

    return v0

    :goto_1a6
    return v14

    :goto_1a7
    goto/16 :goto_13c

    :cond_1a9
    :goto_1a9
    move-object/from16 v0, p0

    :try_start_1ab
    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQZ:Lcom/google/android/gms/analytics/internal/zzah;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzah;->zzlB()Z

    move-result v0

    if-eqz v0, :cond_20c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQZ:Lcom/google/android/gms/analytics/internal/zzah;

    invoke-virtual {v0, v11}, Lcom/google/android/gms/analytics/internal/zzah;->zzq(Ljava/util/List;)Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1bf
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1d5

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Ljava/lang/Long;

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v9, v10, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v9

    goto :goto_1bf

    :cond_1d5
    invoke-interface {v11, v12}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z
    :try_end_1d8
    .catchall {:try_start_1ab .. :try_end_1d8} :catchall_24c

    move-object/from16 v0, p0

    :try_start_1da
    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0, v12}, Lcom/google/android/gms/analytics/internal/zzj;->zzo(Ljava/util/List;)V

    invoke-interface {v8, v12}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_1e2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1da .. :try_end_1e2} :catch_1e3
    .catchall {:try_start_1da .. :try_end_1e2} :catchall_24c

    goto :goto_20c

    :catch_1e3
    move-exception v13

    const-string v0, "Failed to remove successfully uploaded hits"

    move-object/from16 v1, p0

    :try_start_1e8
    invoke-virtual {v1, v0, v13}, Lcom/google/android/gms/analytics/internal/zzl;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkf()V
    :try_end_1ee
    .catchall {:try_start_1e8 .. :try_end_1ee} :catchall_24c

    const/4 v14, 0x0

    move-object/from16 v0, p0

    :try_start_1f1
    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->setTransactionSuccessful()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->endTransaction()V
    :try_end_1fd
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1f1 .. :try_end_1fd} :catch_1fe

    goto :goto_20b

    :catch_1fe
    move-exception v15

    const-string v0, "Failed to commit local dispatch transaction"

    move-object/from16 v1, p0

    invoke-virtual {v1, v0, v15}, Lcom/google/android/gms/analytics/internal/zzl;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkf()V

    const/4 v0, 0x0

    return v0

    :goto_20b
    return v14

    :cond_20c
    :goto_20c
    :try_start_20c
    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z
    :try_end_20f
    .catchall {:try_start_20c .. :try_end_20f} :catchall_24c

    move-result v0

    if-eqz v0, :cond_230

    const/4 v12, 0x0

    move-object/from16 v0, p0

    :try_start_215
    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->setTransactionSuccessful()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->endTransaction()V
    :try_end_221
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_215 .. :try_end_221} :catch_222

    goto :goto_22f

    :catch_222
    move-exception v13

    const-string v0, "Failed to commit local dispatch transaction"

    move-object/from16 v1, p0

    invoke-virtual {v1, v0, v13}, Lcom/google/android/gms/analytics/internal/zzl;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkf()V

    const/4 v0, 0x0

    return v0

    :goto_22f
    return v12

    :cond_230
    move-object/from16 v0, p0

    :try_start_232
    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->setTransactionSuccessful()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->endTransaction()V
    :try_end_23e
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_232 .. :try_end_23e} :catch_23f

    goto :goto_26c

    :catch_23f
    move-exception v11

    const-string v0, "Failed to commit local dispatch transaction"

    move-object/from16 v1, p0

    invoke-virtual {v1, v0, v11}, Lcom/google/android/gms/analytics/internal/zzl;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkf()V

    const/4 v0, 0x0

    return v0

    :catchall_24c
    move-exception v16

    move-object/from16 v0, p0

    :try_start_24f
    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->setTransactionSuccessful()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->endTransaction()V
    :try_end_25b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_24f .. :try_end_25b} :catch_25c

    goto :goto_26b

    :catch_25c
    move-exception v17

    const-string v0, "Failed to commit local dispatch transaction"

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/analytics/internal/zzl;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkf()V

    const/4 v0, 0x0

    return v0

    :goto_26b
    throw v16

    :goto_26c
    goto/16 :goto_5a
.end method

.method public zzjf()V
    .registers 2

    invoke-static {}, Lcom/google/android/gms/measurement/zzg;->zzjk()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjv()V

    const-string v0, "Service disconnected"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbd(Ljava/lang/String;)V

    return-void
.end method

.method zzjh()V
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjk()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjl()Lcom/google/android/gms/internal/zzmq;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzmq;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRg:J

    return-void
.end method

.method public zzka()V
    .registers 6

    invoke-static {}, Lcom/google/android/gms/measurement/zzg;->zzjk()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjv()V

    const-string v0, "Sync dispatching local hits"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzl;->zzbe(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRg:J

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkr()Z

    move-result v0

    if-nez v0, :cond_1a

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjX()V

    :cond_1a
    :goto_1a
    :try_start_1a
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjZ()Z

    move-result v0

    if-eqz v0, :cond_21

    goto :goto_1a

    :cond_21
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjq()Lcom/google/android/gms/analytics/internal/zzai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzai;->zzlI()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkb()V

    iget-wide v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRg:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_36

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRa:Lcom/google/android/gms/analytics/internal/zzag;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzag;->zzlA()V
    :try_end_36
    .catch Ljava/lang/Throwable; {:try_start_1a .. :try_end_36} :catch_37

    :cond_36
    goto :goto_40

    :catch_37
    move-exception v4

    const-string v0, "Sync local dispatch failed"

    invoke-virtual {p0, v0, v4}, Lcom/google/android/gms/analytics/internal/zzl;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkb()V

    :goto_40
    return-void
.end method

.method public zzkb()V
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzji()Lcom/google/android/gms/analytics/internal/zzf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzf;->zzjk()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjv()V

    invoke-direct {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkc()Z

    move-result v0

    if-nez v0, :cond_19

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRa:Lcom/google/android/gms/analytics/internal/zzag;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzag;->unregister()V

    invoke-direct {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkf()V

    return-void

    :cond_19
    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzQY:Lcom/google/android/gms/analytics/internal/zzj;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzj;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2a

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRa:Lcom/google/android/gms/analytics/internal/zzag;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzag;->unregister()V

    invoke-direct {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkf()V

    return-void

    :cond_2a
    sget-object v0, Lcom/google/android/gms/analytics/internal/zzy;->zzSs:Lcom/google/android/gms/analytics/internal/zzy$zza;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzy$zza;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_44

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRa:Lcom/google/android/gms/analytics/internal/zzag;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzag;->zzly()V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRa:Lcom/google/android/gms/analytics/internal/zzag;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzag;->isConnected()Z

    move-result v1

    goto :goto_45

    :cond_44
    const/4 v1, 0x1

    :goto_45
    if-eqz v1, :cond_4b

    invoke-direct {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzke()V

    goto :goto_51

    :cond_4b
    invoke-direct {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkf()V

    invoke-direct {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkd()V

    :goto_51
    return-void
.end method

.method public zzki()J
    .registers 7

    iget-wide v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRc:J

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v0, v0, v2

    if-eqz v0, :cond_b

    iget-wide v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRc:J

    return-wide v0

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkz()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zziI()Lcom/google/android/gms/analytics/internal/zzan;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzan;->zzll()Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zziI()Lcom/google/android/gms/analytics/internal/zzan;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzan;->zzmc()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long v4, v0, v2

    :cond_2a
    return-wide v4
.end method

.method public zzkj()V
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjv()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzjk()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRh:Z

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzl;->zzRb:Lcom/google/android/gms/analytics/internal/zzi;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzi;->disconnect()V

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzl;->zzkb()V

    return-void
.end method

.class public final Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;
.super Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;
.source "ProdutosActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 35
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;-><init>()V

    .line 39
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;

    .line 35
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;

    .line 35
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 52
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 53
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 54
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 55
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;->injectExtras_()V

    .line 56
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;->afterInject()V

    .line 57
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 98
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 99
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_1c

    .line 100
    const-string v0, "multiLimiteVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 101
    const-string v0, "multiLimiteVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;->multiLimiteVO:Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;

    .line 104
    :cond_1c
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 129
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_$2;-><init>(Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 137
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 117
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_$1;-><init>(Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 125
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 45
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;->init_(Landroid/os/Bundle;)V

    .line 46
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;->onCreate(Landroid/os/Bundle;)V

    .line 47
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 48
    const v0, 0x7f03005a

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;->setContentView(I)V

    .line 49
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 91
    const v0, 0x7f0e02a1

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/util/CustomViewPager;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;->widgetProdutosViewpager:Lcom/itau/empresas/ui/util/CustomViewPager;

    .line 92
    const v0, 0x7f0e029d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;->widgetProdutosTablayout:Landroid/support/design/widget/TabLayout;

    .line 93
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 94
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;->carregandoElementosDeTela()V

    .line 95
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 61
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;->setContentView(I)V

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 63
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 73
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;->setContentView(Landroid/view/View;)V

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 75
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 67
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 69
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 108
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;->setIntent(Landroid/content/Intent;)V

    .line 109
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity_;->injectExtras_()V

    .line 110
    return-void
.end method

.class Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "MenuViewHolder.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private cardView:Landroid/support/v7/widget/CardView;

.field private iconMenu:Lcom/itau/empresas/ui/view/TextViewIcon;

.field private textMenu:Landroid/widget/TextView;

.field private viewHolderClickListener:Lcom/itau/empresas/feature/home/menu/adapter/ViewHolderClickListener;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/itau/empresas/feature/home/menu/adapter/ViewHolderClickListener;)V
    .registers 4
    .param p1, "itemView"    # Landroid/view/View;
    .param p2, "viewHolderClickListener"    # Lcom/itau/empresas/feature/home/menu/adapter/ViewHolderClickListener;

    .line 23
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 24
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 25
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 26
    const v0, 0x7f0e05dc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/CardView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;->cardView:Landroid/support/v7/widget/CardView;

    .line 27
    const v0, 0x7f0e05de

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/TextViewIcon;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;->iconMenu:Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 28
    const v0, 0x7f0e05df

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;->textMenu:Landroid/widget/TextView;

    .line 29
    iput-object p2, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;->viewHolderClickListener:Lcom/itau/empresas/feature/home/menu/adapter/ViewHolderClickListener;

    .line 31
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;

    .line 43
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;->viewHolderClickListener:Lcom/itau/empresas/feature/home/menu/adapter/ViewHolderClickListener;

    if-eqz v0, :cond_d

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;->viewHolderClickListener:Lcom/itau/empresas/feature/home/menu/adapter/ViewHolderClickListener;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;->getAdapterPosition()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/home/menu/adapter/ViewHolderClickListener;->clicadoNaPosicao(I)V

    .line 46
    :cond_d
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .line 51
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_19

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;->cardView:Landroid/support/v7/widget/CardView;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;->textMenu:Landroid/widget/TextView;

    .line 53
    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c00cd

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 52
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/CardView;->setCardBackgroundColor(I)V

    goto :goto_2b

    .line 55
    :cond_19
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;->cardView:Landroid/support/v7/widget/CardView;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;->textMenu:Landroid/widget/TextView;

    .line 56
    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c015d

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 55
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/CardView;->setCardBackgroundColor(I)V

    .line 59
    :goto_2b
    const/4 v0, 0x0

    return v0
.end method

.method setMenuIcon(Ljava/lang/String;)V
    .registers 3
    .param p1, "text"    # Ljava/lang/String;

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;->iconMenu:Lcom/itau/empresas/ui/view/TextViewIcon;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/ui/view/TextViewIcon;->setText(Ljava/lang/CharSequence;)V

    .line 39
    return-void
.end method

.method setMenuText(Ljava/lang/String;)V
    .registers 3
    .param p1, "text"    # Ljava/lang/String;

    .line 34
    iget-object v0, p0, Lcom/itau/empresas/feature/home/menu/adapter/MenuViewHolder;->textMenu:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 35
    return-void
.end method

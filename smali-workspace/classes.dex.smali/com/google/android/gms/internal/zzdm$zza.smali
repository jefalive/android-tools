.class public Lcom/google/android/gms/internal/zzdm$zza;
.super Lcom/google/android/gms/internal/zzim;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/zzdm;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "zza"
.end annotation


# instance fields
.field private final zzF:Ljava/lang/String;

.field private final zzpD:Lcom/google/android/gms/internal/zzjp;

.field private final zzzE:Ljava/lang/String;

.field private final zzzF:Ljava/lang/String;

.field private final zzzG:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/zzjp;Ljava/lang/String;)V
    .registers 4

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzim;-><init>()V

    const-string v0, "play.google.com"

    iput-object v0, p0, Lcom/google/android/gms/internal/zzdm$zza;->zzzE:Ljava/lang/String;

    const-string v0, "market"

    iput-object v0, p0, Lcom/google/android/gms/internal/zzdm$zza;->zzzF:Ljava/lang/String;

    const/16 v0, 0xa

    iput v0, p0, Lcom/google/android/gms/internal/zzdm$zza;->zzzG:I

    iput-object p1, p0, Lcom/google/android/gms/internal/zzdm$zza;->zzpD:Lcom/google/android/gms/internal/zzjp;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzdm$zza;->zzF:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onStop()V
    .registers 1

    return-void
.end method

.method public zzT(Ljava/lang/String;)Landroid/content/Intent;
    .registers 5

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v0, 0x10000000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    return-object v2
.end method

.method public zzbr()V
    .registers 14

    iget-object v4, p0, Lcom/google/android/gms/internal/zzdm$zza;->zzF:Ljava/lang/String;

    const/4 v5, 0x0

    :goto_3
    const/16 v0, 0xa

    if-ge v5, v0, :cond_ef

    add-int/lit8 v5, v5, 0x1

    :try_start_9
    new-instance v6, Ljava/net/URL;

    invoke-direct {v6, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const-string v0, "play.google.com"

    invoke-virtual {v6}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_17
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_9 .. :try_end_17} :catch_a8
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_17} :catch_c0
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_17} :catch_d8

    move-result v0

    if-eqz v0, :cond_1c

    goto/16 :goto_ef

    :cond_1c
    const-string v0, "market"

    :try_start_1e
    invoke-virtual {v6}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_25
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1e .. :try_end_25} :catch_a8
    .catch Ljava/io/IOException; {:try_start_1e .. :try_end_25} :catch_c0
    .catch Ljava/lang/RuntimeException; {:try_start_1e .. :try_end_25} :catch_d8

    move-result v0

    if-eqz v0, :cond_2a

    goto/16 :goto_ef

    :cond_2a
    :try_start_2a
    invoke-virtual {v6}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/net/HttpURLConnection;
    :try_end_31
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2a .. :try_end_31} :catch_a8
    .catch Ljava/io/IOException; {:try_start_2a .. :try_end_31} :catch_c0
    .catch Ljava/lang/RuntimeException; {:try_start_2a .. :try_end_31} :catch_d8

    :try_start_31
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbC()Lcom/google/android/gms/internal/zzir;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzdm$zza;->zzpD:Lcom/google/android/gms/internal/zzjp;

    invoke-interface {v1}, Lcom/google/android/gms/internal/zzjp;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/zzdm$zza;->zzpD:Lcom/google/android/gms/internal/zzjp;

    invoke-interface {v2}, Lcom/google/android/gms/internal/zzjp;->zzhX()Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->afmaVersion:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3, v7}, Lcom/google/android/gms/internal/zzir;->zza(Landroid/content/Context;Ljava/lang/String;ZLjava/net/HttpURLConnection;)V

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v8

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v9

    const-string v10, ""

    const/16 v0, 0x12c

    if-lt v8, v0, :cond_8d

    const/16 v0, 0x18f

    if-gt v8, v0, :cond_8d

    const/4 v11, 0x0

    const-string v0, "Location"

    invoke-interface {v9, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6c

    const-string v0, "Location"

    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Ljava/util/List;

    goto :goto_7d

    :cond_6c
    const-string v0, "location"

    invoke-interface {v9, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7d

    const-string v0, "location"

    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Ljava/util/List;

    :cond_7d
    :goto_7d
    if-eqz v11, :cond_8d

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_8d

    const/4 v0, 0x0

    invoke-interface {v11, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Ljava/lang/String;

    :cond_8d
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9c

    const-string v0, "Arrived at landing page, this ideally should not happen. Will open it in browser."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V
    :try_end_98
    .catchall {:try_start_31 .. :try_end_98} :catchall_a1

    :try_start_98
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_9b
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_98 .. :try_end_9b} :catch_a8
    .catch Ljava/io/IOException; {:try_start_98 .. :try_end_9b} :catch_c0
    .catch Ljava/lang/RuntimeException; {:try_start_98 .. :try_end_9b} :catch_d8

    goto :goto_ef

    :cond_9c
    move-object v4, v10

    :try_start_9d
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_a0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_9d .. :try_end_a0} :catch_a8
    .catch Ljava/io/IOException; {:try_start_9d .. :try_end_a0} :catch_c0
    .catch Ljava/lang/RuntimeException; {:try_start_9d .. :try_end_a0} :catch_d8

    goto :goto_a6

    :catchall_a1
    move-exception v12

    :try_start_a2
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    throw v12
    :try_end_a6
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_a2 .. :try_end_a6} :catch_a8
    .catch Ljava/io/IOException; {:try_start_a2 .. :try_end_a6} :catch_c0
    .catch Ljava/lang/RuntimeException; {:try_start_a2 .. :try_end_a6} :catch_d8

    :goto_a6
    goto/16 :goto_3

    :catch_a8
    move-exception v6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error while parsing ping URL: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6}, Lcom/google/android/gms/internal/zzin;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_ef

    :catch_c0
    move-exception v6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error while pinging URL: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6}, Lcom/google/android/gms/internal/zzin;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_ef

    :catch_d8
    move-exception v6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error while pinging URL: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v6}, Lcom/google/android/gms/internal/zzin;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_ef
    :goto_ef
    invoke-virtual {p0, v4}, Lcom/google/android/gms/internal/zzdm$zza;->zzT(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbC()Lcom/google/android/gms/internal/zzir;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzdm$zza;->zzpD:Lcom/google/android/gms/internal/zzjp;

    invoke-interface {v1}, Lcom/google/android/gms/internal/zzjp;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Lcom/google/android/gms/internal/zzir;->zzb(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

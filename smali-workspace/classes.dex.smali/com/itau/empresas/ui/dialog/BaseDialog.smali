.class public abstract Lcom/itau/empresas/ui/dialog/BaseDialog;
.super Landroid/app/DialogFragment;
.source "BaseDialog.java"


# instance fields
.field protected application:Lcom/itau/empresas/CustomApplication;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 18
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public analyticsHit(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "acao"    # Ljava/lang/String;
    .param p2, "label"    # Ljava/lang/String;

    .line 49
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/itau/empresas/ui/dialog/BaseDialog;->analyticsHit(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 50
    return-void
.end method

.method public analyticsHit(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .registers 10
    .param p1, "acao"    # Ljava/lang/String;
    .param p2, "label"    # Ljava/lang/String;
    .param p3, "valor"    # Ljava/lang/Long;

    .line 53
    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/BaseDialog;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 54
    .local v3, "activity":Landroid/app/Activity;
    if-eqz v3, :cond_26

    instance-of v0, v3, Lcom/itau/empresas/ui/activity/BaseActivity;

    if-eqz v0, :cond_26

    .line 56
    move-object v4, v3

    check-cast v4, Lcom/itau/empresas/ui/activity/BaseActivity;

    .line 57
    .local v4, "base":Lcom/itau/empresas/ui/activity/BaseActivity;
    invoke-virtual {v3}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/itau/empresas/CustomApplication;

    .line 58
    .local v5, "app":Lcom/itau/empresas/CustomApplication;
    if-eqz v5, :cond_26

    .line 59
    invoke-virtual {v5}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 61
    invoke-virtual {v4}, Lcom/itau/empresas/ui/activity/BaseActivity;->hitAnalytics()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p1, p2, p3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 60
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 64
    .end local v4    # "base":Lcom/itau/empresas/ui/activity/BaseActivity;
    .end local v5    # "app":Lcom/itau/empresas/CustomApplication;
    :cond_26
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 25
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v2

    .line 28
    .local v2, "dialog":Landroid/app/Dialog;
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 29
    return-object v2
.end method

.method public show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;
    .registers 4
    .param p1, "activity"    # Landroid/support/v7/app/AppCompatActivity;

    .line 35
    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_27

    .line 36
    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_27

    .line 38
    invoke-virtual {p1}, Landroid/support/v7/app/AppCompatActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/ui/dialog/BaseDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 41
    :cond_27
    return-object p0
.end method

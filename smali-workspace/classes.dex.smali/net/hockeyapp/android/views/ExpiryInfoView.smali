.class public Lnet/hockeyapp/android/views/ExpiryInfoView;
.super Landroid/widget/RelativeLayout;
.source "ExpiryInfoView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 48
    const-string v0, ""

    invoke-direct {p0, p1, v0}, Lnet/hockeyapp/android/views/ExpiryInfoView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "text"    # Ljava/lang/String;

    .line 52
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 54
    invoke-direct {p0, p1}, Lnet/hockeyapp/android/views/ExpiryInfoView;->loadLayoutParams(Landroid/content/Context;)V

    .line 55
    invoke-direct {p0, p1}, Lnet/hockeyapp/android/views/ExpiryInfoView;->loadShadowView(Landroid/content/Context;)V

    .line 56
    invoke-direct {p0, p1, p2}, Lnet/hockeyapp/android/views/ExpiryInfoView;->loadTextView(Landroid/content/Context;Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method private loadLayoutParams(Landroid/content/Context;)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;

    .line 60
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    const/4 v1, -0x1

    invoke-direct {v2, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 61
    .local v2, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/views/ExpiryInfoView;->setBackgroundColor(I)V

    .line 62
    invoke-virtual {p0, v2}, Lnet/hockeyapp/android/views/ExpiryInfoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 63
    return-void
.end method

.method private loadShadowView(Landroid/content/Context;)V
    .registers 8
    .param p1, "context"    # Landroid/content/Context;

    .line 67
    invoke-virtual {p0}, Lnet/hockeyapp/android/views/ExpiryInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x1

    const/high16 v2, 0x40400000    # 3.0f

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v3, v0

    .line 68
    .local v3, "height":I
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    invoke-direct {v4, v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 69
    .local v4, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v0, 0xa

    const/4 v1, -0x1

    invoke-virtual {v4, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 71
    new-instance v5, Landroid/widget/ImageView;

    invoke-direct {v5, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 72
    .local v5, "shadowView":Landroid/widget/ImageView;
    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 73
    invoke-static {}, Lnet/hockeyapp/android/utils/ViewHelper;->getGradient()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 75
    invoke-virtual {p0, v5}, Lnet/hockeyapp/android/views/ExpiryInfoView;->addView(Landroid/view/View;)V

    .line 76
    return-void
.end method

.method private loadTextView(Landroid/content/Context;Ljava/lang/String;)V
    .registers 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "text"    # Ljava/lang/String;

    .line 79
    invoke-virtual {p0}, Lnet/hockeyapp/android/views/ExpiryInfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x1

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v3, v0

    .line 81
    .local v3, "margin":I
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    const/4 v1, -0x2

    invoke-direct {v4, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 82
    .local v4, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v0, 0xd

    const/4 v1, -0x1

    invoke-virtual {v4, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 83
    invoke-virtual {v4, v3, v3, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 85
    new-instance v5, Landroid/widget/TextView;

    invoke-direct {v5, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 86
    .local v5, "textView":Landroid/widget/TextView;
    const/16 v0, 0x11

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 87
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 88
    invoke-virtual {v5, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    const/high16 v0, -0x1000000

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 91
    invoke-virtual {p0, v5}, Lnet/hockeyapp/android/views/ExpiryInfoView;->addView(Landroid/view/View;)V

    .line 92
    return-void
.end method

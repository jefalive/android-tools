.class Lorg/simpleframework/xml/core/AnnotationFactory;
.super Ljava/lang/Object;
.source "AnnotationFactory.java"


# instance fields
.field private final format:Lorg/simpleframework/xml/stream/Format;

.field private final required:Z


# direct methods
.method public constructor <init>(Lorg/simpleframework/xml/core/Detail;Lorg/simpleframework/xml/core/Support;)V
    .registers 4
    .param p1, "detail"    # Lorg/simpleframework/xml/core/Detail;
    .param p2, "support"    # Lorg/simpleframework/xml/core/Support;

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    invoke-interface {p1}, Lorg/simpleframework/xml/core/Detail;->isRequired()Z

    move-result v0

    iput-boolean v0, p0, Lorg/simpleframework/xml/core/AnnotationFactory;->required:Z

    .line 69
    invoke-virtual {p2}, Lorg/simpleframework/xml/core/Support;->getFormat()Lorg/simpleframework/xml/stream/Format;

    move-result-object v0

    iput-object v0, p0, Lorg/simpleframework/xml/core/AnnotationFactory;->format:Lorg/simpleframework/xml/stream/Format;

    .line 70
    return-void
.end method

.method private getClassLoader()Ljava/lang/ClassLoader;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 167
    const-class v0, Lorg/simpleframework/xml/core/AnnotationFactory;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    return-object v0
.end method

.method private getInstance(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
    .registers 5
    .param p1, "type"    # Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 111
    invoke-direct {p0}, Lorg/simpleframework/xml/core/AnnotationFactory;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 112
    .local v1, "loader":Ljava/lang/ClassLoader;
    invoke-virtual {p1}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v2

    .line 114
    .local v2, "entry":Ljava/lang/Class;
    invoke-virtual {p1}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_22

    .line 115
    invoke-direct {p0, v2}, Lorg/simpleframework/xml/core/AnnotationFactory;->isPrimitive(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 116
    const-class v0, Lorg/simpleframework/xml/Element;

    invoke-direct {p0, v1, v0}, Lorg/simpleframework/xml/core/AnnotationFactory;->getInstance(Ljava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    return-object v0

    .line 118
    :cond_1b
    const-class v0, Lorg/simpleframework/xml/ElementArray;

    invoke-direct {p0, v1, v0}, Lorg/simpleframework/xml/core/AnnotationFactory;->getInstance(Ljava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    return-object v0

    .line 120
    :cond_22
    invoke-direct {p0, p1}, Lorg/simpleframework/xml/core/AnnotationFactory;->isPrimitive(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-direct {p0}, Lorg/simpleframework/xml/core/AnnotationFactory;->isAttribute()Z

    move-result v0

    if-eqz v0, :cond_35

    .line 121
    const-class v0, Lorg/simpleframework/xml/Attribute;

    invoke-direct {p0, v1, v0}, Lorg/simpleframework/xml/core/AnnotationFactory;->getInstance(Ljava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    return-object v0

    .line 123
    :cond_35
    const-class v0, Lorg/simpleframework/xml/Element;

    invoke-direct {p0, v1, v0}, Lorg/simpleframework/xml/core/AnnotationFactory;->getInstance(Ljava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    return-object v0
.end method

.method private getInstance(Ljava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
    .registers 4
    .param p1, "loader"    # Ljava/lang/ClassLoader;
    .param p2, "label"    # Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 137
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/simpleframework/xml/core/AnnotationFactory;->getInstance(Ljava/lang/ClassLoader;Ljava/lang/Class;Z)Ljava/lang/annotation/Annotation;

    move-result-object v0

    return-object v0
.end method

.method private getInstance(Ljava/lang/ClassLoader;Ljava/lang/Class;Z)Ljava/lang/annotation/Annotation;
    .registers 7
    .param p1, "loader"    # Ljava/lang/ClassLoader;
    .param p2, "label"    # Ljava/lang/Class;
    .param p3, "attribute"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 152
    new-instance v1, Lorg/simpleframework/xml/core/AnnotationHandler;

    iget-boolean v0, p0, Lorg/simpleframework/xml/core/AnnotationFactory;->required:Z

    invoke-direct {v1, p2, v0, p3}, Lorg/simpleframework/xml/core/AnnotationHandler;-><init>(Ljava/lang/Class;ZZ)V

    .line 153
    .local v1, "handler":Lorg/simpleframework/xml/core/AnnotationHandler;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/Class;

    const/4 v0, 0x0

    aput-object p2, v2, v0

    .line 155
    .local v2, "list":[Ljava/lang/Class;
    invoke-static {p1, v2, v1}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/annotation/Annotation;

    return-object v0
.end method

.method private isAttribute()Z
    .registers 3

    .line 228
    iget-object v0, p0, Lorg/simpleframework/xml/core/AnnotationFactory;->format:Lorg/simpleframework/xml/stream/Format;

    invoke-virtual {v0}, Lorg/simpleframework/xml/stream/Format;->getVerbosity()Lorg/simpleframework/xml/stream/Verbosity;

    move-result-object v1

    .line 230
    .local v1, "verbosity":Lorg/simpleframework/xml/stream/Verbosity;
    if-eqz v1, :cond_10

    .line 231
    sget-object v0, Lorg/simpleframework/xml/stream/Verbosity;->LOW:Lorg/simpleframework/xml/stream/Verbosity;

    if-ne v1, v0, :cond_e

    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0

    .line 233
    :cond_10
    const/4 v0, 0x0

    return v0
.end method

.method private isPrimitive(Ljava/lang/Class;)Z
    .registers 3
    .param p1, "type"    # Ljava/lang/Class;

    .line 208
    const-class v0, Ljava/lang/Number;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 209
    const/4 v0, 0x1

    return v0

    .line 211
    :cond_a
    const-class v0, Ljava/lang/Boolean;

    if-ne p1, v0, :cond_10

    .line 212
    const/4 v0, 0x1

    return v0

    .line 214
    :cond_10
    const-class v0, Ljava/lang/Character;

    if-ne p1, v0, :cond_16

    .line 215
    const/4 v0, 0x1

    return v0

    .line 217
    :cond_16
    invoke-virtual {p1}, Ljava/lang/Class;->isPrimitive()Z

    move-result v0

    return v0
.end method

.method private isPrimitiveKey([Ljava/lang/Class;)Z
    .registers 5
    .param p1, "dependents"    # [Ljava/lang/Class;

    .line 181
    if-eqz p1, :cond_26

    array-length v0, p1

    if-lez v0, :cond_26

    .line 182
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    .line 183
    .local v1, "parent":Ljava/lang/Class;
    const/4 v0, 0x0

    aget-object v2, p1, v0

    .line 185
    .local v2, "type":Ljava/lang/Class;
    if-eqz v1, :cond_21

    .line 186
    invoke-virtual {v1}, Ljava/lang/Class;->isEnum()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 187
    const/4 v0, 0x1

    return v0

    .line 189
    :cond_19
    invoke-virtual {v2}, Ljava/lang/Class;->isEnum()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 190
    const/4 v0, 0x1

    return v0

    .line 193
    :cond_21
    invoke-direct {p0, v2}, Lorg/simpleframework/xml/core/AnnotationFactory;->isPrimitive(Ljava/lang/Class;)Z

    move-result v0

    return v0

    .line 195
    .end local v1    # "parent":Ljava/lang/Class;
    .end local v2    # "type":Ljava/lang/Class;
    :cond_26
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public getInstance(Ljava/lang/Class;[Ljava/lang/Class;)Ljava/lang/annotation/Annotation;
    .registers 6
    .param p1, "type"    # Ljava/lang/Class;
    .param p2, "dependents"    # [Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 85
    invoke-direct {p0}, Lorg/simpleframework/xml/core/AnnotationFactory;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    .line 87
    .local v2, "loader":Ljava/lang/ClassLoader;
    const-class v0, Ljava/util/Map;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 88
    invoke-direct {p0, p2}, Lorg/simpleframework/xml/core/AnnotationFactory;->isPrimitiveKey([Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-direct {p0}, Lorg/simpleframework/xml/core/AnnotationFactory;->isAttribute()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 89
    const-class v0, Lorg/simpleframework/xml/ElementMap;

    const/4 v1, 0x1

    invoke-direct {p0, v2, v0, v1}, Lorg/simpleframework/xml/core/AnnotationFactory;->getInstance(Ljava/lang/ClassLoader;Ljava/lang/Class;Z)Ljava/lang/annotation/Annotation;

    move-result-object v0

    return-object v0

    .line 91
    :cond_20
    const-class v0, Lorg/simpleframework/xml/ElementMap;

    invoke-direct {p0, v2, v0}, Lorg/simpleframework/xml/core/AnnotationFactory;->getInstance(Ljava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    return-object v0

    .line 93
    :cond_27
    const-class v0, Ljava/util/Collection;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_36

    .line 94
    const-class v0, Lorg/simpleframework/xml/ElementList;

    invoke-direct {p0, v2, v0}, Lorg/simpleframework/xml/core/AnnotationFactory;->getInstance(Ljava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    return-object v0

    .line 96
    :cond_36
    invoke-direct {p0, p1}, Lorg/simpleframework/xml/core/AnnotationFactory;->getInstance(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    return-object v0
.end method

.class public final Lcom/google/android/gms/internal/zzhj$zza;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/zzhj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "zza"
.end annotation


# instance fields
.field private zzHD:I

.field private zzHE:I

.field private zzHF:F

.field private zzJQ:I

.field private zzJR:Z

.field private zzJS:Z

.field private zzJT:Ljava/lang/String;

.field private zzJU:Ljava/lang/String;

.field private zzJV:Z

.field private zzJW:Z

.field private zzJX:Z

.field private zzJY:Z

.field private zzJZ:Ljava/lang/String;

.field private zzKa:Ljava/lang/String;

.field private zzKb:I

.field private zzKc:I

.field private zzKd:I

.field private zzKe:I

.field private zzKf:I

.field private zzKg:I

.field private zzKh:D

.field private zzKi:Z

.field private zzKj:Z

.field private zzKk:I

.field private zzKl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/zzhj$zza;->zzB(Landroid/content/Context;)V

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/internal/zzhj$zza;->zza(Landroid/content/Context;Landroid/content/pm/PackageManager;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/zzhj$zza;->zzC(Landroid/content/Context;)V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const-string v0, "geo:0,0?q=donuts"

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzhj$zza;->zza(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_1e

    const/4 v0, 0x1

    goto :goto_1f

    :cond_1e
    const/4 v0, 0x0

    :goto_1f
    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzJR:Z

    const-string v0, "http://www.google.com"

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzhj$zza;->zza(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_2b

    const/4 v0, 0x1

    goto :goto_2c

    :cond_2b
    const/4 v0, 0x0

    :goto_2c
    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzJS:Z

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzJU:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/ads/internal/client/zzn;->zzcS()Lcom/google/android/gms/ads/internal/util/client/zza;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/util/client/zza;->zzhI()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzJV:Z

    invoke-static {p1}, Lcom/google/android/gms/common/zze;->zzap(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzJW:Z

    invoke-virtual {v2}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzJZ:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/internal/zzhj$zza;->zza(Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzKa:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    if-nez v3, :cond_57

    return-void

    :cond_57
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    if-nez v4, :cond_5e

    return-void

    :cond_5e
    iget v0, v4, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzHF:F

    iget v0, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzHD:I

    iget v0, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzHE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/zzhj;)V
    .registers 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/zzhj$zza;->zzB(Landroid/content/Context;)V

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/internal/zzhj$zza;->zza(Landroid/content/Context;Landroid/content/pm/PackageManager;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/zzhj$zza;->zzC(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/zzhj$zza;->zzD(Landroid/content/Context;)V

    iget-boolean v0, p2, Lcom/google/android/gms/internal/zzhj;->zzJR:Z

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzJR:Z

    iget-boolean v0, p2, Lcom/google/android/gms/internal/zzhj;->zzJS:Z

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzJS:Z

    iget-object v0, p2, Lcom/google/android/gms/internal/zzhj;->zzJU:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzJU:Ljava/lang/String;

    iget-boolean v0, p2, Lcom/google/android/gms/internal/zzhj;->zzJV:Z

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzJV:Z

    iget-boolean v0, p2, Lcom/google/android/gms/internal/zzhj;->zzJW:Z

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzJW:Z

    iget-object v0, p2, Lcom/google/android/gms/internal/zzhj;->zzJZ:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzJZ:Ljava/lang/String;

    iget-object v0, p2, Lcom/google/android/gms/internal/zzhj;->zzKa:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzKa:Ljava/lang/String;

    iget v0, p2, Lcom/google/android/gms/internal/zzhj;->zzHF:F

    iput v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzHF:F

    iget v0, p2, Lcom/google/android/gms/internal/zzhj;->zzHD:I

    iput v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzHD:I

    iget v0, p2, Lcom/google/android/gms/internal/zzhj;->zzHE:I

    iput v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzHE:I

    return-void
.end method

.method private zzB(Landroid/content/Context;)V
    .registers 4

    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/media/AudioManager;

    invoke-virtual {v1}, Landroid/media/AudioManager;->getMode()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzJQ:I

    invoke-virtual {v1}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzJX:Z

    invoke-virtual {v1}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzJY:Z

    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzKb:I

    invoke-virtual {v1}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzKf:I

    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzKg:I

    return-void
.end method

.method private zzC(Landroid/content/Context;)V
    .registers 9

    new-instance v2, Landroid/content/IntentFilter;

    const-string v0, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v2, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v3

    if-eqz v3, :cond_35

    const-string v0, "status"

    const/4 v1, -0x1

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const-string v0, "level"

    const/4 v1, -0x1

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const-string v0, "scale"

    const/4 v1, -0x1

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    int-to-float v0, v5

    int-to-float v1, v6

    div-float/2addr v0, v1

    float-to-double v0, v0

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzKh:D

    const/4 v0, 0x2

    if-eq v4, v0, :cond_2f

    const/4 v0, 0x5

    if-ne v4, v0, :cond_31

    :cond_2f
    const/4 v0, 0x1

    goto :goto_32

    :cond_31
    const/4 v0, 0x0

    :goto_32
    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzKi:Z

    goto :goto_3c

    :cond_35
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzKh:D

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzKi:Z

    :goto_3c
    return-void
.end method

.method private zzD(Landroid/content/Context;)V
    .registers 3

    sget-object v0, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzKl:Ljava/lang/String;

    return-void
.end method

.method private static zza(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/pm/ResolveInfo;
    .registers 5

    new-instance v2, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v0, 0x10000

    invoke-virtual {p0, v2, v0}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    return-object v0
.end method

.method private static zza(Landroid/content/pm/PackageManager;)Ljava/lang/String;
    .registers 6

    const-string v0, "market://details?id=com.google.android.gms.ads"

    invoke-static {p0, v0}, Lcom/google/android/gms/internal/zzhj$zza;->zza(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-nez v2, :cond_a

    const/4 v0, 0x0

    return-object v0

    :cond_a
    iget-object v3, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-nez v3, :cond_10

    const/4 v0, 0x0

    return-object v0

    :cond_10
    :try_start_10
    iget-object v0, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    if-eqz v4, :cond_35

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, v4, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_33
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_10 .. :try_end_33} :catch_37

    move-result-object v0

    goto :goto_36

    :cond_35
    const/4 v0, 0x0

    :goto_36
    return-object v0

    :catch_37
    move-exception v4

    const/4 v0, 0x0

    return-object v0
.end method

.method private zza(Landroid/content/Context;Landroid/content/pm/PackageManager;)V
    .registers 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/telephony/TelephonyManager;

    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/net/ConnectivityManager;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzJT:Ljava/lang/String;

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzKd:I

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzKe:I

    const/4 v0, -0x2

    iput v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzKc:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzKj:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzKk:I

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbC()Lcom/google/android/gms/internal/zzir;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.permission.ACCESS_NETWORK_STATE"

    invoke-virtual {v0, p2, v1, v2}, Lcom/google/android/gms/internal/zzir;->zza(Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_63

    invoke-virtual {v4}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v5

    if-eqz v5, :cond_54

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzKc:I

    invoke-virtual {v5}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzKk:I

    goto :goto_57

    :cond_54
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzKc:I

    :goto_57
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_63

    invoke-virtual {v4}, Landroid/net/ConnectivityManager;->isActiveNetworkMetered()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzhj$zza;->zzKj:Z

    :cond_63
    return-void
.end method


# virtual methods
.method public zzgI()Lcom/google/android/gms/internal/zzhj;
    .registers 30

    new-instance v2, Lcom/google/android/gms/internal/zzhj;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzJQ:I

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzJR:Z

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzJS:Z

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzJT:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzJU:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzJV:Z

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzJW:Z

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzJX:Z

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzJY:Z

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzJZ:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzKa:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzKb:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzKc:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzKd:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzKe:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzKf:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzKg:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzHF:F

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzHD:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzHE:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzKh:D

    move-wide/from16 v23, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzKi:Z

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzKj:Z

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzKk:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzhj$zza;->zzKl:Ljava/lang/String;

    move-object/from16 v28, v0

    invoke-direct/range {v2 .. v28}, Lcom/google/android/gms/internal/zzhj;-><init>(IZZLjava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;Ljava/lang/String;IIIIIIFIIDZZILjava/lang/String;)V

    return-object v2
.end method

.class final Lorg/threeten/bp/zone/Ser;
.super Ljava/lang/Object;
.source "Ser.java"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = -0x7b4f011483e5ac42L


# instance fields
.field private object:Ljava/lang/Object;

.field private type:B


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    return-void
.end method

.method constructor <init>(BLjava/lang/Object;)V
    .registers 3
    .param p1, "type"    # B
    .param p2, "object"    # Ljava/lang/Object;

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-byte p1, p0, Lorg/threeten/bp/zone/Ser;->type:B

    .line 86
    iput-object p2, p0, Lorg/threeten/bp/zone/Ser;->object:Ljava/lang/Object;

    .line 87
    return-void
.end method

.method static read(Ljava/io/DataInput;)Ljava/lang/Object;
    .registers 3
    .param p0, "in"    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .line 132
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v1

    .line 133
    .local v1, "type":B
    invoke-static {v1, p0}, Lorg/threeten/bp/zone/Ser;->readInternal(BLjava/io/DataInput;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static readEpochSec(Ljava/io/DataInput;)J
    .registers 10
    .param p0, "in"    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 215
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v0

    and-int/lit16 v4, v0, 0xff

    .line 216
    .local v4, "hiByte":I
    const/16 v0, 0xff

    if-ne v4, v0, :cond_f

    .line 217
    invoke-interface {p0}, Ljava/io/DataInput;->readLong()J

    move-result-wide v0

    return-wide v0

    .line 219
    :cond_f
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v0

    and-int/lit16 v5, v0, 0xff

    .line 220
    .local v5, "midByte":I
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v0

    and-int/lit16 v6, v0, 0xff

    .line 221
    .local v6, "loByte":I
    shl-int/lit8 v0, v4, 0x10

    shl-int/lit8 v1, v5, 0x8

    add-int/2addr v0, v1

    add-int/2addr v0, v6

    int-to-long v7, v0

    .line 222
    .local v7, "tot":J
    const-wide/16 v0, 0x384

    mul-long/2addr v0, v7

    const-wide v2, 0x110bc5000L

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method private static readInternal(BLjava/io/DataInput;)Ljava/lang/Object;
    .registers 4
    .param p0, "type"    # B
    .param p1, "in"    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .line 137
    packed-switch p0, :pswitch_data_1c

    goto :goto_13

    .line 139
    :pswitch_4
    invoke-static {p1}, Lorg/threeten/bp/zone/StandardZoneRules;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/zone/StandardZoneRules;

    move-result-object v0

    return-object v0

    .line 141
    :pswitch_9
    invoke-static {p1}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/zone/ZoneOffsetTransition;

    move-result-object v0

    return-object v0

    .line 143
    :pswitch_e
    invoke-static {p1}, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->readExternal(Ljava/io/DataInput;)Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    move-result-object v0

    return-object v0

    .line 145
    :goto_13
    new-instance v0, Ljava/io/StreamCorruptedException;

    const-string v1, "Unknown serialized type"

    invoke-direct {v0, v1}, Ljava/io/StreamCorruptedException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :pswitch_data_1c
    .packed-switch 0x1
        :pswitch_4
        :pswitch_9
        :pswitch_e
    .end packed-switch
.end method

.method static readOffset(Ljava/io/DataInput;)Lorg/threeten/bp/ZoneOffset;
    .registers 3
    .param p0, "in"    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 183
    invoke-interface {p0}, Ljava/io/DataInput;->readByte()B

    move-result v1

    .line 184
    .local v1, "offsetByte":I
    const/16 v0, 0x7f

    if-ne v1, v0, :cond_11

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v0

    invoke-static {v0}, Lorg/threeten/bp/ZoneOffset;->ofTotalSeconds(I)Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    goto :goto_17

    :cond_11
    mul-int/lit16 v0, v1, 0x384

    invoke-static {v0}, Lorg/threeten/bp/ZoneOffset;->ofTotalSeconds(I)Lorg/threeten/bp/ZoneOffset;

    move-result-object v0

    :goto_17
    return-object v0
.end method

.method private readResolve()Ljava/lang/Object;
    .registers 2

    .line 155
    iget-object v0, p0, Lorg/threeten/bp/zone/Ser;->object:Ljava/lang/Object;

    return-object v0
.end method

.method static writeEpochSec(JLjava/io/DataOutput;)V
    .registers 8
    .param p0, "epochSec"    # J
    .param p2, "out"    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 196
    const-wide v0, -0x110bc5000L

    cmp-long v0, p0, v0

    if-ltz v0, :cond_3a

    const-wide v0, 0x26cb5db00L

    cmp-long v0, p0, v0

    if-gez v0, :cond_3a

    const-wide/16 v0, 0x384

    rem-long v0, p0, v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_3a

    .line 197
    const-wide v0, 0x110bc5000L

    add-long/2addr v0, p0

    const-wide/16 v2, 0x384

    div-long/2addr v0, v2

    long-to-int v4, v0

    .line 198
    .local v4, "store":I
    ushr-int/lit8 v0, v4, 0x10

    and-int/lit16 v0, v0, 0xff

    invoke-interface {p2, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 199
    ushr-int/lit8 v0, v4, 0x8

    and-int/lit16 v0, v0, 0xff

    invoke-interface {p2, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 200
    and-int/lit16 v0, v4, 0xff

    invoke-interface {p2, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 201
    .end local v4    # "store":I
    goto :goto_42

    .line 202
    :cond_3a
    const/16 v0, 0xff

    invoke-interface {p2, v0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 203
    invoke-interface {p2, p0, p1}, Ljava/io/DataOutput;->writeLong(J)V

    .line 205
    :goto_42
    return-void
.end method

.method private static writeInternal(BLjava/lang/Object;Ljava/io/DataOutput;)V
    .registers 5
    .param p0, "type"    # B
    .param p1, "object"    # Ljava/lang/Object;
    .param p2, "out"    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 104
    invoke-interface {p2, p0}, Ljava/io/DataOutput;->writeByte(I)V

    .line 105
    packed-switch p0, :pswitch_data_26

    goto :goto_1c

    .line 107
    :pswitch_7
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/zone/StandardZoneRules;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/zone/StandardZoneRules;->writeExternal(Ljava/io/DataOutput;)V

    .line 108
    goto :goto_24

    .line 110
    :pswitch_e
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/zone/ZoneOffsetTransition;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/zone/ZoneOffsetTransition;->writeExternal(Ljava/io/DataOutput;)V

    .line 111
    goto :goto_24

    .line 113
    :pswitch_15
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;

    invoke-virtual {v0, p2}, Lorg/threeten/bp/zone/ZoneOffsetTransitionRule;->writeExternal(Ljava/io/DataOutput;)V

    .line 114
    goto :goto_24

    .line 116
    :goto_1c
    new-instance v0, Ljava/io/InvalidClassException;

    const-string v1, "Unknown serialized type"

    invoke-direct {v0, v1}, Ljava/io/InvalidClassException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118
    :goto_24
    return-void

    nop

    :pswitch_data_26
    .packed-switch 0x1
        :pswitch_7
        :pswitch_e
        :pswitch_15
    .end packed-switch
.end method

.method static writeOffset(Lorg/threeten/bp/ZoneOffset;Ljava/io/DataOutput;)V
    .registers 5
    .param p0, "offset"    # Lorg/threeten/bp/ZoneOffset;
    .param p1, "out"    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 167
    invoke-virtual {p0}, Lorg/threeten/bp/ZoneOffset;->getTotalSeconds()I

    move-result v1

    .line 168
    .local v1, "offsetSecs":I
    rem-int/lit16 v0, v1, 0x384

    if-nez v0, :cond_b

    div-int/lit16 v2, v1, 0x384

    goto :goto_d

    :cond_b
    const/16 v2, 0x7f

    .line 169
    .local v2, "offsetByte":I
    :goto_d
    invoke-interface {p1, v2}, Ljava/io/DataOutput;->writeByte(I)V

    .line 170
    const/16 v0, 0x7f

    if-ne v2, v0, :cond_17

    .line 171
    invoke-interface {p1, v1}, Ljava/io/DataOutput;->writeInt(I)V

    .line 173
    :cond_17
    return-void
.end method


# virtual methods
.method public readExternal(Ljava/io/ObjectInput;)V
    .registers 3
    .param p1, "in"    # Ljava/io/ObjectInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .line 127
    invoke-interface {p1}, Ljava/io/ObjectInput;->readByte()B

    move-result v0

    iput-byte v0, p0, Lorg/threeten/bp/zone/Ser;->type:B

    .line 128
    iget-byte v0, p0, Lorg/threeten/bp/zone/Ser;->type:B

    invoke-static {v0, p1}, Lorg/threeten/bp/zone/Ser;->readInternal(BLjava/io/DataInput;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/threeten/bp/zone/Ser;->object:Ljava/lang/Object;

    .line 129
    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .registers 4
    .param p1, "out"    # Ljava/io/ObjectOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 96
    iget-byte v0, p0, Lorg/threeten/bp/zone/Ser;->type:B

    iget-object v1, p0, Lorg/threeten/bp/zone/Ser;->object:Ljava/lang/Object;

    invoke-static {v0, v1, p1}, Lorg/threeten/bp/zone/Ser;->writeInternal(BLjava/lang/Object;Ljava/io/DataOutput;)V

    .line 97
    return-void
.end method

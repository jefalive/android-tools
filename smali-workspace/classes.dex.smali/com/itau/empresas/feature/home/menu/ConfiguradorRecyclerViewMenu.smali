.class final Lcom/itau/empresas/feature/home/menu/ConfiguradorRecyclerViewMenu;
.super Ljava/lang/Object;
.source "ConfiguradorRecyclerViewMenu.java"


# direct methods
.method static configura(Landroid/support/v7/widget/RecyclerView;Lcom/itau/empresas/api/model/MenuVO;Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;)V
    .registers 8
    .param p0, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p1, "menu"    # Lcom/itau/empresas/api/model/MenuVO;
    .param p2, "listener"    # Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;

    .line 24
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 26
    .local v4, "context":Landroid/content/Context;
    new-instance v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager;-><init>(II)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 29
    new-instance v0, Lcom/itau/empresas/feature/home/menu/adapter/MenuAdapter;

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/itau/empresas/feature/home/menu/adapter/RegraLayoutGrandePequeno;

    .line 30
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Lcom/itau/empresas/feature/home/menu/adapter/RegraLayoutGrandePequeno;-><init>(I)V

    invoke-direct {v0, v1, v2, p2}, Lcom/itau/empresas/feature/home/menu/adapter/MenuAdapter;-><init>(Ljava/util/List;Lcom/itau/empresas/feature/home/menu/adapter/RegraLayoutAdapter;Lcom/itau/empresas/feature/home/menu/adapter/CardMenuListener;)V

    .line 29
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 31
    new-instance v0, Lcom/itau/empresas/feature/home/menu/adapter/MenuItemDecoration;

    .line 32
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 33
    const v2, 0x7f080220

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-direct {v0, v1}, Lcom/itau/empresas/feature/home/menu/adapter/MenuItemDecoration;-><init>(I)V

    .line 31
    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 34
    return-void
.end method

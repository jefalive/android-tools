.class Lorg/simpleframework/xml/core/ModelAssembler;
.super Ljava/lang/Object;
.source "ModelAssembler.java"


# instance fields
.field private final builder:Lorg/simpleframework/xml/core/ExpressionBuilder;

.field private final detail:Lorg/simpleframework/xml/core/Detail;

.field private final format:Lorg/simpleframework/xml/stream/Format;


# direct methods
.method public constructor <init>(Lorg/simpleframework/xml/core/ExpressionBuilder;Lorg/simpleframework/xml/core/Detail;Lorg/simpleframework/xml/core/Support;)V
    .registers 5
    .param p1, "builder"    # Lorg/simpleframework/xml/core/ExpressionBuilder;
    .param p2, "detail"    # Lorg/simpleframework/xml/core/Detail;
    .param p3, "support"    # Lorg/simpleframework/xml/core/Support;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    invoke-virtual {p3}, Lorg/simpleframework/xml/core/Support;->getFormat()Lorg/simpleframework/xml/stream/Format;

    move-result-object v0

    iput-object v0, p0, Lorg/simpleframework/xml/core/ModelAssembler;->format:Lorg/simpleframework/xml/stream/Format;

    .line 75
    iput-object p1, p0, Lorg/simpleframework/xml/core/ModelAssembler;->builder:Lorg/simpleframework/xml/core/ExpressionBuilder;

    .line 76
    iput-object p2, p0, Lorg/simpleframework/xml/core/ModelAssembler;->detail:Lorg/simpleframework/xml/core/Detail;

    .line 77
    return-void
.end method

.method private assembleAttributes(Lorg/simpleframework/xml/core/Model;Lorg/simpleframework/xml/Order;)V
    .registers 15
    .param p1, "model"    # Lorg/simpleframework/xml/core/Model;
    .param p2, "order"    # Lorg/simpleframework/xml/Order;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 123
    invoke-interface {p2}, Lorg/simpleframework/xml/Order;->attributes()[Ljava/lang/String;

    move-result-object v5

    .local v5, "arr$":[Ljava/lang/String;
    array-length v6, v5

    .local v6, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_6
    if-ge v7, v6, :cond_49

    aget-object v8, v5, v7

    .line 124
    .local v8, "value":Ljava/lang/String;
    iget-object v0, p0, Lorg/simpleframework/xml/core/ModelAssembler;->builder:Lorg/simpleframework/xml/core/ExpressionBuilder;

    invoke-virtual {v0, v8}, Lorg/simpleframework/xml/core/ExpressionBuilder;->build(Ljava/lang/String;)Lorg/simpleframework/xml/core/Expression;

    move-result-object v9

    .line 126
    .local v9, "path":Lorg/simpleframework/xml/core/Expression;
    invoke-interface {v9}, Lorg/simpleframework/xml/core/Expression;->isAttribute()Z

    move-result v0

    if-nez v0, :cond_2f

    invoke-interface {v9}, Lorg/simpleframework/xml/core/Expression;->isPath()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 127
    new-instance v0, Lorg/simpleframework/xml/core/PathException;

    const-string v1, "Ordered attribute \'%s\' references an element in %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v9, v2, v3

    iget-object v3, p0, Lorg/simpleframework/xml/core/ModelAssembler;->detail:Lorg/simpleframework/xml/core/Detail;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-direct {v0, v1, v2}, Lorg/simpleframework/xml/core/PathException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    throw v0

    .line 129
    :cond_2f
    invoke-interface {v9}, Lorg/simpleframework/xml/core/Expression;->isPath()Z

    move-result v0

    if-nez v0, :cond_43

    .line 130
    iget-object v0, p0, Lorg/simpleframework/xml/core/ModelAssembler;->format:Lorg/simpleframework/xml/stream/Format;

    invoke-virtual {v0}, Lorg/simpleframework/xml/stream/Format;->getStyle()Lorg/simpleframework/xml/stream/Style;

    move-result-object v10

    .line 131
    .local v10, "style":Lorg/simpleframework/xml/stream/Style;
    invoke-interface {v10, v8}, Lorg/simpleframework/xml/stream/Style;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 133
    .local v11, "name":Ljava/lang/String;
    invoke-interface {p1, v11}, Lorg/simpleframework/xml/core/Model;->registerAttribute(Ljava/lang/String;)V

    .line 134
    .end local v10    # "style":Lorg/simpleframework/xml/stream/Style;
    .end local v11    # "name":Ljava/lang/String;
    goto :goto_46

    .line 135
    :cond_43
    invoke-direct {p0, p1, v9}, Lorg/simpleframework/xml/core/ModelAssembler;->registerAttributes(Lorg/simpleframework/xml/core/Model;Lorg/simpleframework/xml/core/Expression;)V

    .line 123
    .end local v8    # "value":Ljava/lang/String;
    .end local v9    # "path":Lorg/simpleframework/xml/core/Expression;
    :goto_46
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    .line 138
    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v6    # "len$":I
    .end local v7    # "i$":I
    :cond_49
    return-void
.end method

.method private assembleElements(Lorg/simpleframework/xml/core/Model;Lorg/simpleframework/xml/Order;)V
    .registers 13
    .param p1, "model"    # Lorg/simpleframework/xml/core/Model;
    .param p2, "order"    # Lorg/simpleframework/xml/Order;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 103
    invoke-interface {p2}, Lorg/simpleframework/xml/Order;->elements()[Ljava/lang/String;

    move-result-object v5

    .local v5, "arr$":[Ljava/lang/String;
    array-length v6, v5

    .local v6, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_6
    if-ge v7, v6, :cond_2f

    aget-object v8, v5, v7

    .line 104
    .local v8, "value":Ljava/lang/String;
    iget-object v0, p0, Lorg/simpleframework/xml/core/ModelAssembler;->builder:Lorg/simpleframework/xml/core/ExpressionBuilder;

    invoke-virtual {v0, v8}, Lorg/simpleframework/xml/core/ExpressionBuilder;->build(Ljava/lang/String;)Lorg/simpleframework/xml/core/Expression;

    move-result-object v9

    .line 106
    .local v9, "path":Lorg/simpleframework/xml/core/Expression;
    invoke-interface {v9}, Lorg/simpleframework/xml/core/Expression;->isAttribute()Z

    move-result v0

    if-eqz v0, :cond_29

    .line 107
    new-instance v0, Lorg/simpleframework/xml/core/PathException;

    const-string v1, "Ordered element \'%s\' references an attribute in %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v9, v2, v3

    iget-object v3, p0, Lorg/simpleframework/xml/core/ModelAssembler;->detail:Lorg/simpleframework/xml/core/Detail;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-direct {v0, v1, v2}, Lorg/simpleframework/xml/core/PathException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    throw v0

    .line 109
    :cond_29
    invoke-direct {p0, p1, v9}, Lorg/simpleframework/xml/core/ModelAssembler;->registerElements(Lorg/simpleframework/xml/core/Model;Lorg/simpleframework/xml/core/Expression;)V

    .line 103
    .end local v8    # "value":Ljava/lang/String;
    .end local v9    # "path":Lorg/simpleframework/xml/core/Expression;
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    .line 111
    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v6    # "len$":I
    .end local v7    # "i$":I
    :cond_2f
    return-void
.end method

.method private registerAttribute(Lorg/simpleframework/xml/core/Model;Lorg/simpleframework/xml/core/Expression;)V
    .registers 4
    .param p1, "model"    # Lorg/simpleframework/xml/core/Model;
    .param p2, "path"    # Lorg/simpleframework/xml/core/Expression;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 176
    invoke-interface {p2}, Lorg/simpleframework/xml/core/Expression;->getFirst()Ljava/lang/String;

    move-result-object v0

    .line 178
    .local v0, "name":Ljava/lang/String;
    if-eqz v0, :cond_9

    .line 179
    invoke-interface {p1, v0}, Lorg/simpleframework/xml/core/Model;->registerAttribute(Ljava/lang/String;)V

    .line 181
    :cond_9
    return-void
.end method

.method private registerAttributes(Lorg/simpleframework/xml/core/Model;Lorg/simpleframework/xml/core/Expression;)V
    .registers 13
    .param p1, "model"    # Lorg/simpleframework/xml/core/Model;
    .param p2, "path"    # Lorg/simpleframework/xml/core/Expression;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 149
    invoke-interface {p2}, Lorg/simpleframework/xml/core/Expression;->getPrefix()Ljava/lang/String;

    move-result-object v5

    .line 150
    .local v5, "prefix":Ljava/lang/String;
    invoke-interface {p2}, Lorg/simpleframework/xml/core/Expression;->getFirst()Ljava/lang/String;

    move-result-object v6

    .line 151
    .local v6, "name":Ljava/lang/String;
    invoke-interface {p2}, Lorg/simpleframework/xml/core/Expression;->getIndex()I

    move-result v7

    .line 153
    .local v7, "index":I
    invoke-interface {p2}, Lorg/simpleframework/xml/core/Expression;->isPath()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 154
    invoke-interface {p1, v6, v5, v7}, Lorg/simpleframework/xml/core/Model;->register(Ljava/lang/String;Ljava/lang/String;I)Lorg/simpleframework/xml/core/Model;

    move-result-object v8

    .line 155
    .local v8, "next":Lorg/simpleframework/xml/core/Model;
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Lorg/simpleframework/xml/core/Expression;->getPath(I)Lorg/simpleframework/xml/core/Expression;

    move-result-object v9

    .line 157
    .local v9, "child":Lorg/simpleframework/xml/core/Expression;
    if-nez v8, :cond_30

    .line 158
    new-instance v0, Lorg/simpleframework/xml/core/PathException;

    const-string v1, "Element \'%s\' does not exist in %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v6, v2, v3

    iget-object v3, p0, Lorg/simpleframework/xml/core/ModelAssembler;->detail:Lorg/simpleframework/xml/core/Detail;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-direct {v0, v1, v2}, Lorg/simpleframework/xml/core/PathException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    throw v0

    .line 160
    :cond_30
    invoke-direct {p0, v8, v9}, Lorg/simpleframework/xml/core/ModelAssembler;->registerAttributes(Lorg/simpleframework/xml/core/Model;Lorg/simpleframework/xml/core/Expression;)V

    .line 161
    .end local v8    # "next":Lorg/simpleframework/xml/core/Model;
    .end local v9    # "child":Lorg/simpleframework/xml/core/Expression;
    goto :goto_37

    .line 162
    :cond_34
    invoke-direct {p0, p1, p2}, Lorg/simpleframework/xml/core/ModelAssembler;->registerAttribute(Lorg/simpleframework/xml/core/Model;Lorg/simpleframework/xml/core/Expression;)V

    .line 164
    :goto_37
    return-void
.end method

.method private registerElement(Lorg/simpleframework/xml/core/Model;Lorg/simpleframework/xml/core/Expression;)V
    .registers 12
    .param p1, "model"    # Lorg/simpleframework/xml/core/Model;
    .param p2, "path"    # Lorg/simpleframework/xml/core/Expression;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 228
    invoke-interface {p2}, Lorg/simpleframework/xml/core/Expression;->getPrefix()Ljava/lang/String;

    move-result-object v5

    .line 229
    .local v5, "prefix":Ljava/lang/String;
    invoke-interface {p2}, Lorg/simpleframework/xml/core/Expression;->getFirst()Ljava/lang/String;

    move-result-object v6

    .line 230
    .local v6, "name":Ljava/lang/String;
    invoke-interface {p2}, Lorg/simpleframework/xml/core/Expression;->getIndex()I

    move-result v7

    .line 232
    .local v7, "index":I
    const/4 v0, 0x1

    if-le v7, v0, :cond_2d

    .line 233
    add-int/lit8 v0, v7, -0x1

    invoke-interface {p1, v6, v0}, Lorg/simpleframework/xml/core/Model;->lookup(Ljava/lang/String;I)Lorg/simpleframework/xml/core/Model;

    move-result-object v8

    .line 235
    .local v8, "previous":Lorg/simpleframework/xml/core/Model;
    if-nez v8, :cond_2d

    .line 236
    new-instance v0, Lorg/simpleframework/xml/core/PathException;

    const-string v1, "Ordered element \'%s\' in path \'%s\' is out of sequence for %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v6, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    iget-object v3, p0, Lorg/simpleframework/xml/core/ModelAssembler;->detail:Lorg/simpleframework/xml/core/Detail;

    const/4 v4, 0x2

    aput-object v3, v2, v4

    invoke-direct {v0, v1, v2}, Lorg/simpleframework/xml/core/PathException;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    throw v0

    .line 239
    .end local v8    # "previous":Lorg/simpleframework/xml/core/Model;
    :cond_2d
    invoke-interface {p1, v6, v5, v7}, Lorg/simpleframework/xml/core/Model;->register(Ljava/lang/String;Ljava/lang/String;I)Lorg/simpleframework/xml/core/Model;

    .line 240
    return-void
.end method

.method private registerElements(Lorg/simpleframework/xml/core/Model;Lorg/simpleframework/xml/core/Expression;)V
    .registers 9
    .param p1, "model"    # Lorg/simpleframework/xml/core/Model;
    .param p2, "path"    # Lorg/simpleframework/xml/core/Expression;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 192
    invoke-interface {p2}, Lorg/simpleframework/xml/core/Expression;->getPrefix()Ljava/lang/String;

    move-result-object v1

    .line 193
    .local v1, "prefix":Ljava/lang/String;
    invoke-interface {p2}, Lorg/simpleframework/xml/core/Expression;->getFirst()Ljava/lang/String;

    move-result-object v2

    .line 194
    .local v2, "name":Ljava/lang/String;
    invoke-interface {p2}, Lorg/simpleframework/xml/core/Expression;->getIndex()I

    move-result v3

    .line 196
    .local v3, "index":I
    if-eqz v2, :cond_20

    .line 197
    invoke-interface {p1, v2, v1, v3}, Lorg/simpleframework/xml/core/Model;->register(Ljava/lang/String;Ljava/lang/String;I)Lorg/simpleframework/xml/core/Model;

    move-result-object v4

    .line 198
    .local v4, "next":Lorg/simpleframework/xml/core/Model;
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Lorg/simpleframework/xml/core/Expression;->getPath(I)Lorg/simpleframework/xml/core/Expression;

    move-result-object v5

    .line 200
    .local v5, "child":Lorg/simpleframework/xml/core/Expression;
    invoke-interface {p2}, Lorg/simpleframework/xml/core/Expression;->isPath()Z

    move-result v0

    if-eqz v0, :cond_20

    .line 201
    invoke-direct {p0, v4, v5}, Lorg/simpleframework/xml/core/ModelAssembler;->registerElements(Lorg/simpleframework/xml/core/Model;Lorg/simpleframework/xml/core/Expression;)V

    .line 204
    .end local v4    # "next":Lorg/simpleframework/xml/core/Model;
    .end local v5    # "child":Lorg/simpleframework/xml/core/Expression;
    :cond_20
    invoke-direct {p0, p1, p2}, Lorg/simpleframework/xml/core/ModelAssembler;->registerElement(Lorg/simpleframework/xml/core/Model;Lorg/simpleframework/xml/core/Expression;)V

    .line 205
    return-void
.end method


# virtual methods
.method public assemble(Lorg/simpleframework/xml/core/Model;Lorg/simpleframework/xml/Order;)V
    .registers 3
    .param p1, "model"    # Lorg/simpleframework/xml/core/Model;
    .param p2, "order"    # Lorg/simpleframework/xml/Order;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 89
    invoke-direct {p0, p1, p2}, Lorg/simpleframework/xml/core/ModelAssembler;->assembleElements(Lorg/simpleframework/xml/core/Model;Lorg/simpleframework/xml/Order;)V

    .line 90
    invoke-direct {p0, p1, p2}, Lorg/simpleframework/xml/core/ModelAssembler;->assembleAttributes(Lorg/simpleframework/xml/core/Model;Lorg/simpleframework/xml/Order;)V

    .line 91
    return-void
.end method

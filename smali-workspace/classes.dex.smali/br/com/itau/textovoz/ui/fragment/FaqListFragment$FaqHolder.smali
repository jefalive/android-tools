.class Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "FaqListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FaqHolder"
.end annotation


# instance fields
.field faq:Lbr/com/itau/textovoz/model/Faq;

.field final synthetic this$0:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;


# direct methods
.method public constructor <init>(Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;Landroid/view/View;)V
    .registers 3
    .param p2, "itemView"    # Landroid/view/View;

    .line 130
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;->this$0:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;

    .line 131
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 132
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    return-void
.end method


# virtual methods
.method public bindFaq(Lbr/com/itau/textovoz/model/Faq;)V
    .registers 8
    .param p1, "f"    # Lbr/com/itau/textovoz/model/Faq;

    .line 136
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;->faq:Lbr/com/itau/textovoz/model/Faq;

    .line 138
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;->this$0:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;

    # getter for: Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->useCardView:Z
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->access$200(Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;)Z

    move-result v0

    if-eqz v0, :cond_65

    .line 139
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;->itemView:Landroid/view/View;

    sget v1, Lbr/com/itau/textovoz/R$id;->card_view_faq_item_title:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    .line 140
    .local v3, "faqTitleTextView":Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;->itemView:Landroid/view/View;

    sget v1, Lbr/com/itau/textovoz/R$id;->card_view_faq_item_description:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    .line 142
    .local v4, "faqDescriptionTextView":Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;->faq:Lbr/com/itau/textovoz/model/Faq;

    invoke-virtual {v0}, Lbr/com/itau/textovoz/model/Faq;->getQuestion()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;->this$0:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;

    # getter for: Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->segment:Ljava/lang/String;
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->access$300(Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/textovoz/util/CommonUtils;->getSegment(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v1, v0, :cond_46

    .line 146
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;->this$0:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;

    invoke-virtual {v0}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$color;->personnalite_theme_color:I

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {v3, v0}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setTextColor(I)V

    goto :goto_55

    .line 150
    :cond_46
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;->this$0:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;

    invoke-virtual {v0}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$color;->itau_theme_color:I

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {v3, v0}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setTextColor(I)V

    .line 154
    :goto_55
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Faq;->getAnswer()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\n"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 155
    .local v5, "stringFormat":Ljava/lang/String;
    invoke-virtual {v4, v5}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    .end local v3    # "faqTitleTextView":Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;
    .end local v4    # "faqDescriptionTextView":Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;
    .end local v5    # "stringFormat":Ljava/lang/String;
    goto :goto_79

    .line 157
    :cond_65
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;->itemView:Landroid/view/View;

    sget v1, Lbr/com/itau/textovoz/R$id;->text_name_item_list:I

    .line 158
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    .line 159
    .local v3, "faqTitleTextView":Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;->faq:Lbr/com/itau/textovoz/model/Faq;

    invoke-virtual {v0}, Lbr/com/itau/textovoz/model/Faq;->getQuestion()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    .end local v3    # "faqTitleTextView":Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;
    :goto_79
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4
    .param p1, "v"    # Landroid/view/View;

    .line 165
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;->this$0:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;

    # getter for: Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->listener:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqListListener;
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->access$400(Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;)Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqListListener;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 166
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;->this$0:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;

    # getter for: Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->listener:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqListListener;
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->access$400(Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;)Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqListListener;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;->faq:Lbr/com/itau/textovoz/model/Faq;

    invoke-interface {v0, v1}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqListListener;->onFaqItemClicked(Lbr/com/itau/textovoz/model/Faq;)V

    .line 168
    :cond_13
    return-void
.end method

.class public final Lcom/google/android/gms/internal/zzhd;
.super Lcom/google/android/gms/ads/internal/request/zzj$zza;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# static fields
.field private static zzIQ:Lcom/google/android/gms/internal/zzhd;

.field private static final zzqy:Ljava/lang/Object;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final zzIR:Lcom/google/android/gms/internal/zzhc;

.field private final zzIS:Lcom/google/android/gms/internal/zzbm;

.field private final zzIT:Lcom/google/android/gms/internal/zzeg;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/zzhd;->zzqy:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/zzbm;Lcom/google/android/gms/internal/zzhc;)V
    .registers 10

    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/request/zzj$zza;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/zzhd;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/gms/internal/zzhd;->zzIR:Lcom/google/android/gms/internal/zzhc;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzhd;->zzIS:Lcom/google/android/gms/internal/zzbm;

    new-instance v0, Lcom/google/android/gms/internal/zzeg;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_16

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    goto :goto_17

    :cond_16
    move-object v1, p1

    :goto_17
    new-instance v2, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    const v3, 0x818058

    const v4, 0x818058

    const/4 v5, 0x1

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;-><init>(IIZ)V

    invoke-virtual {p2}, Lcom/google/android/gms/internal/zzbm;->zzdp()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/internal/zzhd$6;

    invoke-direct {v4, p0}, Lcom/google/android/gms/internal/zzhd$6;-><init>(Lcom/google/android/gms/internal/zzhd;)V

    new-instance v5, Lcom/google/android/gms/internal/zzeg$zzc;

    invoke-direct {v5}, Lcom/google/android/gms/internal/zzeg$zzc;-><init>()V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/zzeg;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Ljava/lang/String;Lcom/google/android/gms/internal/zzeg$zzb;Lcom/google/android/gms/internal/zzeg$zzb;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzhd;->zzIT:Lcom/google/android/gms/internal/zzeg;

    return-void
.end method

.method private static zza(Landroid/content/Context;Lcom/google/android/gms/internal/zzeg;Lcom/google/android/gms/internal/zzbm;Lcom/google/android/gms/internal/zzhc;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;)Lcom/google/android/gms/ads/internal/request/AdResponseParcel;
    .registers 35

    const-string v0, "Starting ad request from service."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaI(Ljava/lang/String;)V

    invoke-static/range {p0 .. p0}, Lcom/google/android/gms/internal/zzbt;->initialize(Landroid/content/Context;)V

    new-instance v10, Lcom/google/android/gms/internal/zzcb;

    sget-object v0, Lcom/google/android/gms/internal/zzbt;->zzwg:Lcom/google/android/gms/internal/zzbp;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzbp;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const-string v1, "load_ad"

    move-object/from16 v2, p4

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->zzrp:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->zzuh:Ljava/lang/String;

    invoke-direct {v10, v0, v1, v2}, Lcom/google/android/gms/internal/zzcb;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p4

    iget v0, v0, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->versionCode:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_46

    move-object/from16 v0, p4

    iget-wide v0, v0, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->zzHL:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_46

    move-object/from16 v0, p4

    iget-wide v0, v0, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->zzHL:J

    invoke-virtual {v10, v0, v1}, Lcom/google/android/gms/internal/zzcb;->zzb(J)Lcom/google/android/gms/internal/zzbz;

    move-result-object v11

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "cts"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {v10, v11, v0}, Lcom/google/android/gms/internal/zzcb;->zza(Lcom/google/android/gms/internal/zzbz;[Ljava/lang/String;)Z

    :cond_46
    invoke-virtual {v10}, Lcom/google/android/gms/internal/zzcb;->zzdB()Lcom/google/android/gms/internal/zzbz;

    move-result-object v11

    const/4 v12, 0x0

    move-object/from16 v0, p4

    iget v0, v0, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->versionCode:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_5c

    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->zzHA:Landroid/os/Bundle;

    if-eqz v0, :cond_5c

    move-object/from16 v0, p4

    iget-object v12, v0, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->zzHA:Landroid/os/Bundle;

    :cond_5c
    const/4 v13, 0x0

    sget-object v0, Lcom/google/android/gms/internal/zzbt;->zzwp:Lcom/google/android/gms/internal/zzbp;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzbp;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_9d

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/gms/internal/zzhc;->zzIP:Lcom/google/android/gms/internal/zzhh;

    if-eqz v0, :cond_9d

    if-nez v12, :cond_8b

    sget-object v0, Lcom/google/android/gms/internal/zzbt;->zzwq:Lcom/google/android/gms/internal/zzbp;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzbp;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8b

    const-string v0, "contentInfo is not present, but we\'ll still launch the app index task"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V

    new-instance v12, Landroid/os/Bundle;

    invoke-direct {v12}, Landroid/os/Bundle;-><init>()V

    :cond_8b
    if-eqz v12, :cond_9d

    move-object v14, v12

    new-instance v0, Lcom/google/android/gms/internal/zzhd$1;

    move-object/from16 v1, p3

    move-object/from16 v2, p0

    move-object/from16 v3, p4

    invoke-direct {v0, v1, v2, v3, v14}, Lcom/google/android/gms/internal/zzhd$1;-><init>(Lcom/google/android/gms/internal/zzhc;Landroid/content/Context;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Landroid/os/Bundle;)V

    invoke-static {v0}, Lcom/google/android/gms/internal/zziq;->zza(Ljava/util/concurrent/Callable;)Lcom/google/android/gms/internal/zzjg;

    move-result-object v13

    :cond_9d
    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/gms/internal/zzhc;->zzIK:Lcom/google/android/gms/internal/zzek;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzek;->zzex()V

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbI()Lcom/google/android/gms/internal/zzhk;

    move-result-object v0

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzhk;->zzE(Landroid/content/Context;)Lcom/google/android/gms/internal/zzhj;

    move-result-object v14

    iget v0, v14, Lcom/google/android/gms/internal/zzhj;->zzKc:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_bf

    const-string v0, "Device is offline."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaI(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V

    return-object v0

    :cond_bf
    move-object/from16 v0, p4

    iget v0, v0, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->versionCode:I

    const/4 v1, 0x7

    if-lt v0, v1, :cond_cb

    move-object/from16 v0, p4

    iget-object v15, v0, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->zzHI:Ljava/lang/String;

    goto :goto_d3

    :cond_cb
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v15

    :goto_d3
    new-instance v16, Lcom/google/android/gms/internal/zzhf;

    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    move-object/from16 v1, v16

    invoke-direct {v1, v15, v0}, Lcom/google/android/gms/internal/zzhf;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->zzHt:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/client/AdRequestParcel;->extras:Landroid/os/Bundle;

    if-eqz v0, :cond_101

    move-object/from16 v0, p4

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->zzHt:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/client/AdRequestParcel;->extras:Landroid/os/Bundle;

    const-string v1, "_ad"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    if-eqz v17, :cond_101

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, v17

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/zzhe;->zza(Landroid/content/Context;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Ljava/lang/String;)Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-result-object v0

    return-object v0

    :cond_101
    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/gms/internal/zzhc;->zzIK:Lcom/google/android/gms/internal/zzek;

    const-wide/16 v1, 0xfa

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/internal/zzek;->zzd(J)Landroid/location/Location;

    move-result-object v17

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/gms/internal/zzhc;->zzIL:Lcom/google/android/gms/internal/zzfy;

    move-object/from16 v1, p4

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->zzrj:Ljava/lang/String;

    move-object/from16 v2, p4

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->zzHu:Landroid/content/pm/PackageInfo;

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v3, p0

    invoke-interface {v0, v3, v1, v2}, Lcom/google/android/gms/internal/zzfy;->getToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/gms/internal/zzhc;->zzII:Lcom/google/android/gms/internal/zzbo;

    move-object/from16 v1, p4

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/zzbo;->zza(Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;)Ljava/util/List;

    move-result-object v19

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/gms/internal/zzhc;->zzIM:Lcom/google/android/gms/internal/zzie;

    move-object/from16 v1, p4

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/zzie;->zzf(Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/gms/internal/zzhc;->zzIN:Lcom/google/android/gms/internal/zzhn;

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/zzhn;->zzF(Landroid/content/Context;)Lcom/google/android/gms/internal/zzhn$zza;

    move-result-object v21

    if-eqz v13, :cond_16a

    const-string v0, "Waiting for app index fetching task."

    :try_start_141
    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/internal/zzbt;->zzwr:Lcom/google/android/gms/internal/zzbp;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzbp;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v13, v0, v1, v2}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    const-string v0, "App index fetching task completed."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V
    :try_end_15a
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_141 .. :try_end_15a} :catch_15b
    .catch Ljava/lang/InterruptedException; {:try_start_141 .. :try_end_15a} :catch_15b
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_141 .. :try_end_15a} :catch_164

    goto :goto_16a

    :catch_15b
    move-exception v22

    const-string v0, "Failed to fetch app index signal"

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzin;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_16a

    :catch_164
    move-exception v22

    const-string v0, "Timed out waiting for app index fetching task"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaI(Ljava/lang/String;)V

    :cond_16a
    :goto_16a
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object v2, v14

    move-object/from16 v3, v21

    move-object/from16 v4, v17

    move-object/from16 v5, p2

    move-object/from16 v6, v18

    move-object/from16 v7, v20

    move-object/from16 v8, v19

    move-object v9, v12

    invoke-static/range {v0 .. v9}, Lcom/google/android/gms/internal/zzhe;->zza(Landroid/content/Context;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Lcom/google/android/gms/internal/zzhj;Lcom/google/android/gms/internal/zzhn$zza;Landroid/location/Location;Lcom/google/android/gms/internal/zzbm;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Landroid/os/Bundle;)Lorg/json/JSONObject;

    move-result-object v22

    move-object/from16 v0, p4

    iget v0, v0, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->versionCode:I

    const/4 v1, 0x7

    if-ge v0, v1, :cond_190

    const-string v0, "request_id"

    move-object/from16 v1, v22

    :try_start_18b
    invoke-virtual {v1, v0, v15}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_18e
    .catch Lorg/json/JSONException; {:try_start_18b .. :try_end_18e} :catch_18f

    goto :goto_190

    :catch_18f
    move-exception v23

    :cond_190
    :goto_190
    if-nez v22, :cond_199

    new-instance v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V

    return-object v0

    :cond_199
    invoke-virtual/range {v22 .. v22}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v23

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "arc"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {v10, v11, v0}, Lcom/google/android/gms/internal/zzcb;->zza(Lcom/google/android/gms/internal/zzbz;[Ljava/lang/String;)Z

    invoke-virtual {v10}, Lcom/google/android/gms/internal/zzcb;->zzdB()Lcom/google/android/gms/internal/zzbz;

    move-result-object v24

    sget-object v0, Lcom/google/android/gms/internal/zzbt;->zzvC:Lcom/google/android/gms/internal/zzbp;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzbp;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1ce

    sget-object v0, Lcom/google/android/gms/internal/zzir;->zzMc:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/internal/zzhd$2;

    move-object/from16 v2, p1

    move-object/from16 v3, v16

    move-object v4, v10

    move-object/from16 v5, v24

    move-object/from16 v6, v23

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/internal/zzhd$2;-><init>(Lcom/google/android/gms/internal/zzeg;Lcom/google/android/gms/internal/zzhf;Lcom/google/android/gms/internal/zzcb;Lcom/google/android/gms/internal/zzbz;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1e5

    :cond_1ce
    sget-object v0, Lcom/google/android/gms/internal/zzir;->zzMc:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/internal/zzhd$3;

    move-object/from16 v2, p0

    move-object/from16 v3, p4

    move-object/from16 v4, v16

    move-object v5, v10

    move-object/from16 v6, v24

    move-object/from16 v7, v23

    move-object/from16 v8, p2

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/internal/zzhd$3;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Lcom/google/android/gms/internal/zzhf;Lcom/google/android/gms/internal/zzcb;Lcom/google/android/gms/internal/zzbz;Ljava/lang/String;Lcom/google/android/gms/internal/zzbm;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_1e5
    :try_start_1e5
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/internal/zzhf;->zzgC()Ljava/util/concurrent/Future;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-interface {v0, v2, v3, v1}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v25, v0

    check-cast v25, Lcom/google/android/gms/internal/zzhi;
    :try_end_1f5
    .catch Ljava/lang/Exception; {:try_start_1e5 .. :try_end_1f5} :catch_1f6
    .catchall {:try_start_1e5 .. :try_end_1f5} :catchall_2e0

    goto :goto_212

    :catch_1f6
    move-exception v26

    :try_start_1f7
    new-instance v27, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-object/from16 v0, v27

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V
    :try_end_1ff
    .catchall {:try_start_1f7 .. :try_end_1ff} :catchall_2e0

    sget-object v0, Lcom/google/android/gms/internal/zzir;->zzMc:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/internal/zzhd$4;

    move-object/from16 v2, p3

    move-object/from16 v3, p0

    move-object/from16 v4, v16

    move-object/from16 v5, p4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/internal/zzhd$4;-><init>(Lcom/google/android/gms/internal/zzhc;Landroid/content/Context;Lcom/google/android/gms/internal/zzhf;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-object v27

    :goto_212
    if-nez v25, :cond_22f

    :try_start_214
    new-instance v26, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-object/from16 v0, v26

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V
    :try_end_21c
    .catchall {:try_start_214 .. :try_end_21c} :catchall_2e0

    sget-object v0, Lcom/google/android/gms/internal/zzir;->zzMc:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/internal/zzhd$4;

    move-object/from16 v2, p3

    move-object/from16 v3, p0

    move-object/from16 v4, v16

    move-object/from16 v5, p4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/internal/zzhd$4;-><init>(Lcom/google/android/gms/internal/zzhc;Landroid/content/Context;Lcom/google/android/gms/internal/zzhf;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-object v26

    :cond_22f
    :try_start_22f
    invoke-virtual/range {v25 .. v25}, Lcom/google/android/gms/internal/zzhi;->getErrorCode()I

    move-result v0

    const/4 v1, -0x2

    if-eq v0, v1, :cond_254

    new-instance v26, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/gms/internal/zzhi;->getErrorCode()I

    move-result v0

    move-object/from16 v1, v26

    invoke-direct {v1, v0}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V
    :try_end_241
    .catchall {:try_start_22f .. :try_end_241} :catchall_2e0

    sget-object v0, Lcom/google/android/gms/internal/zzir;->zzMc:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/internal/zzhd$4;

    move-object/from16 v2, p3

    move-object/from16 v3, p0

    move-object/from16 v4, v16

    move-object/from16 v5, p4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/internal/zzhd$4;-><init>(Lcom/google/android/gms/internal/zzhc;Landroid/content/Context;Lcom/google/android/gms/internal/zzhf;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-object v26

    :cond_254
    :try_start_254
    invoke-virtual {v10}, Lcom/google/android/gms/internal/zzcb;->zzdE()Lcom/google/android/gms/internal/zzbz;

    move-result-object v0

    if-eqz v0, :cond_269

    invoke-virtual {v10}, Lcom/google/android/gms/internal/zzcb;->zzdE()Lcom/google/android/gms/internal/zzbz;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "rur"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v10, v0, v1}, Lcom/google/android/gms/internal/zzcb;->zza(Lcom/google/android/gms/internal/zzbz;[Ljava/lang/String;)Z

    :cond_269
    const/16 v26, 0x0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/gms/internal/zzhi;->zzgG()Z

    move-result v0

    if-eqz v0, :cond_27f

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/gms/internal/zzhc;->zzIH:Lcom/google/android/gms/internal/zzib;

    move-object/from16 v1, p4

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->zzHu:Landroid/content/pm/PackageInfo;

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/zzib;->zzaz(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    :cond_27f
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    move-object/from16 v2, p4

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->zzrl:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->afmaVersion:Ljava/lang/String;

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/gms/internal/zzhi;->getUrl()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v4, v26

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/gms/internal/zzhi;->zzgH()Z

    move-result v5

    if-eqz v5, :cond_298

    move-object/from16 v5, v18

    goto :goto_299

    :cond_298
    const/4 v5, 0x0

    :goto_299
    move-object/from16 v6, v25

    move-object v7, v10

    move-object/from16 v8, p3

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/internal/zzhd;->zza(Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/zzhi;Lcom/google/android/gms/internal/zzcb;Lcom/google/android/gms/internal/zzhc;)Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-result-object v27

    move-object/from16 v0, v27

    iget v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->zzIf:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2b8

    move-object/from16 v0, p3

    iget-object v0, v0, Lcom/google/android/gms/internal/zzhc;->zzIL:Lcom/google/android/gms/internal/zzfy;

    move-object/from16 v1, p4

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->zzHu:Landroid/content/pm/PackageInfo;

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v2, p0

    invoke-interface {v0, v2, v1}, Lcom/google/android/gms/internal/zzfy;->clearToken(Landroid/content/Context;Ljava/lang/String;)V

    :cond_2b8
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "tts"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-virtual {v10, v11, v0}, Lcom/google/android/gms/internal/zzcb;->zza(Lcom/google/android/gms/internal/zzbz;[Ljava/lang/String;)Z

    invoke-virtual {v10}, Lcom/google/android/gms/internal/zzcb;->zzdD()Ljava/lang/String;

    move-result-object v0

    move-object/from16 v1, v27

    iput-object v0, v1, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->zzIh:Ljava/lang/String;
    :try_end_2cb
    .catchall {:try_start_254 .. :try_end_2cb} :catchall_2e0

    move-object/from16 v28, v27

    sget-object v0, Lcom/google/android/gms/internal/zzir;->zzMc:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/internal/zzhd$4;

    move-object/from16 v2, p3

    move-object/from16 v3, p0

    move-object/from16 v4, v16

    move-object/from16 v5, p4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/internal/zzhd$4;-><init>(Lcom/google/android/gms/internal/zzhc;Landroid/content/Context;Lcom/google/android/gms/internal/zzhf;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-object v28

    :catchall_2e0
    move-exception v29

    sget-object v0, Lcom/google/android/gms/internal/zzir;->zzMc:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/internal/zzhd$4;

    move-object/from16 v2, p3

    move-object/from16 v3, p0

    move-object/from16 v4, v16

    move-object/from16 v5, p4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/internal/zzhd$4;-><init>(Lcom/google/android/gms/internal/zzhc;Landroid/content/Context;Lcom/google/android/gms/internal/zzhf;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    throw v29
.end method

.method public static zza(Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/zzhi;Lcom/google/android/gms/internal/zzcb;Lcom/google/android/gms/internal/zzhc;)Lcom/google/android/gms/ads/internal/request/AdResponseParcel;
    .registers 28

    if-eqz p7, :cond_7

    invoke-virtual/range {p7 .. p7}, Lcom/google/android/gms/internal/zzcb;->zzdB()Lcom/google/android/gms/internal/zzbz;

    move-result-object v4

    goto :goto_8

    :cond_7
    const/4 v4, 0x0

    :goto_8
    :try_start_8
    new-instance v5, Lcom/google/android/gms/internal/zzhg;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/google/android/gms/internal/zzhg;-><init>(Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AdRequestServiceImpl: Sending request: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaI(Ljava/lang/String;)V

    new-instance v6, Ljava/net/URL;

    move-object/from16 v0, p3

    invoke-direct {v6, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const/4 v10, 0x0

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbG()Lcom/google/android/gms/internal/zzmq;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzmq;->elapsedRealtime()J

    move-result-wide v11

    :goto_37
    if-eqz p8, :cond_40

    move-object/from16 v0, p8

    iget-object v0, v0, Lcom/google/android/gms/internal/zzhc;->zzIO:Lcom/google/android/gms/internal/zzhm;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzhm;->zzgJ()V

    :cond_40
    invoke-virtual {v6}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Ljava/net/HttpURLConnection;
    :try_end_47
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_47} :catch_1a9

    :try_start_47
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbC()Lcom/google/android/gms/internal/zzir;

    move-result-object v0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3, v13}, Lcom/google/android/gms/internal/zzir;->zza(Landroid/content/Context;Ljava/lang/String;ZLjava/net/HttpURLConnection;)V

    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_60

    const-string v0, "x-afma-drt-cookie"

    move-object/from16 v1, p4

    invoke-virtual {v13, v0, v1}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_60
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_80

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Bearer "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    const-string v0, "Authorization"

    invoke-virtual {v13, v0, v14}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_80
    if-eqz p6, :cond_b3

    invoke-virtual/range {p6 .. p6}, Lcom/google/android/gms/internal/zzhi;->zzgF()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b3

    const/4 v0, 0x1

    invoke-virtual {v13, v0}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    invoke-virtual/range {p6 .. p6}, Lcom/google/android/gms/internal/zzhi;->zzgF()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v14

    array-length v0, v14

    invoke-virtual {v13, v0}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V
    :try_end_9c
    .catchall {:try_start_47 .. :try_end_9c} :catchall_199

    const/4 v15, 0x0

    :try_start_9d
    new-instance v0, Ljava/io/BufferedOutputStream;

    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object v15, v0

    invoke-virtual {v15, v14}, Ljava/io/BufferedOutputStream;->write([B)V
    :try_end_aa
    .catchall {:try_start_9d .. :try_end_aa} :catchall_ae

    :try_start_aa
    invoke-static {v15}, Lcom/google/android/gms/internal/zzna;->zzb(Ljava/io/Closeable;)V
    :try_end_ad
    .catchall {:try_start_aa .. :try_end_ad} :catchall_199

    goto :goto_b3

    :catchall_ae
    move-exception v16

    :try_start_af
    invoke-static {v15}, Lcom/google/android/gms/internal/zzna;->zzb(Ljava/io/Closeable;)V

    throw v16

    :cond_b3
    :goto_b3
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v9

    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v8

    const/16 v0, 0xc8

    if-lt v9, v0, :cond_109

    const/16 v0, 0x12c

    if-ge v9, v0, :cond_109

    invoke-virtual {v6}, Ljava/net/URL;->toString()Ljava/lang/String;
    :try_end_c6
    .catchall {:try_start_af .. :try_end_c6} :catchall_199

    move-result-object v14

    const/4 v15, 0x0

    :try_start_c8
    new-instance v0, Ljava/io/InputStreamReader;

    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object v15, v0

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbC()Lcom/google/android/gms/internal/zzir;

    move-result-object v0

    invoke-virtual {v0, v15}, Lcom/google/android/gms/internal/zzir;->zza(Ljava/io/InputStreamReader;)Ljava/lang/String;
    :try_end_d9
    .catchall {:try_start_c8 .. :try_end_d9} :catchall_de

    move-result-object v7

    :try_start_da
    invoke-static {v15}, Lcom/google/android/gms/internal/zzna;->zzb(Ljava/io/Closeable;)V
    :try_end_dd
    .catchall {:try_start_da .. :try_end_dd} :catchall_199

    goto :goto_e3

    :catchall_de
    move-exception v17

    :try_start_df
    invoke-static {v15}, Lcom/google/android/gms/internal/zzna;->zzb(Ljava/io/Closeable;)V

    throw v17

    :goto_e3
    invoke-static {v14, v8, v7, v9}, Lcom/google/android/gms/internal/zzhd;->zza(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;I)V

    invoke-virtual {v5, v14, v8, v7}, Lcom/google/android/gms/internal/zzhg;->zzb(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)V

    if-eqz p7, :cond_f8

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "ufe"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    move-object/from16 v1, p7

    invoke-virtual {v1, v4, v0}, Lcom/google/android/gms/internal/zzcb;->zza(Lcom/google/android/gms/internal/zzbz;[Ljava/lang/String;)Z

    :cond_f8
    invoke-virtual {v5, v11, v12}, Lcom/google/android/gms/internal/zzhg;->zzj(J)Lcom/google/android/gms/ads/internal/request/AdResponseParcel;
    :try_end_fb
    .catchall {:try_start_df .. :try_end_fb} :catchall_199

    move-result-object v16

    :try_start_fc
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->disconnect()V

    if-eqz p8, :cond_108

    move-object/from16 v0, p8

    iget-object v0, v0, Lcom/google/android/gms/internal/zzhc;->zzIO:Lcom/google/android/gms/internal/zzhm;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzhm;->zzgK()V
    :try_end_108
    .catch Ljava/io/IOException; {:try_start_fc .. :try_end_108} :catch_1a9

    :cond_108
    return-object v16

    :cond_109
    :try_start_109
    invoke-virtual {v6}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v8, v1, v9}, Lcom/google/android/gms/internal/zzhd;->zza(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;I)V

    const/16 v0, 0x12c

    if-lt v9, v0, :cond_145

    const/16 v0, 0x190

    if-ge v9, v0, :cond_145

    const-string v0, "Location"

    invoke-virtual {v13, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_13d

    const-string v0, "No location header to follow redirect."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    new-instance v15, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    const/4 v0, 0x0

    invoke-direct {v15, v0}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V
    :try_end_130
    .catchall {:try_start_109 .. :try_end_130} :catchall_199

    :try_start_130
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->disconnect()V

    if-eqz p8, :cond_13c

    move-object/from16 v0, p8

    iget-object v0, v0, Lcom/google/android/gms/internal/zzhc;->zzIO:Lcom/google/android/gms/internal/zzhm;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzhm;->zzgK()V
    :try_end_13c
    .catch Ljava/io/IOException; {:try_start_130 .. :try_end_13c} :catch_1a9

    :cond_13c
    return-object v15

    :cond_13d
    :try_start_13d
    new-instance v6, Ljava/net/URL;

    invoke-direct {v6, v14}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    add-int/lit8 v10, v10, 0x1

    goto :goto_16e

    :cond_145
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Received error HTTP response code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    new-instance v14, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    const/4 v0, 0x0

    invoke-direct {v14, v0}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V
    :try_end_161
    .catchall {:try_start_13d .. :try_end_161} :catchall_199

    :try_start_161
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->disconnect()V

    if-eqz p8, :cond_16d

    move-object/from16 v0, p8

    iget-object v0, v0, Lcom/google/android/gms/internal/zzhc;->zzIO:Lcom/google/android/gms/internal/zzhm;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzhm;->zzgK()V
    :try_end_16d
    .catch Ljava/io/IOException; {:try_start_161 .. :try_end_16d} :catch_1a9

    :cond_16d
    return-object v14

    :goto_16e
    const/4 v0, 0x5

    if-le v10, v0, :cond_189

    const-string v0, "Too many redirects."

    :try_start_173
    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    new-instance v14, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    const/4 v0, 0x0

    invoke-direct {v14, v0}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V
    :try_end_17c
    .catchall {:try_start_173 .. :try_end_17c} :catchall_199

    :try_start_17c
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->disconnect()V

    if-eqz p8, :cond_188

    move-object/from16 v0, p8

    iget-object v0, v0, Lcom/google/android/gms/internal/zzhc;->zzIO:Lcom/google/android/gms/internal/zzhm;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzhm;->zzgK()V
    :try_end_188
    .catch Ljava/io/IOException; {:try_start_17c .. :try_end_188} :catch_1a9

    :cond_188
    return-object v14

    :cond_189
    :try_start_189
    invoke-virtual {v5, v8}, Lcom/google/android/gms/internal/zzhg;->zzj(Ljava/util/Map;)V
    :try_end_18c
    .catchall {:try_start_189 .. :try_end_18c} :catchall_199

    :try_start_18c
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->disconnect()V

    if-eqz p8, :cond_1a7

    move-object/from16 v0, p8

    iget-object v0, v0, Lcom/google/android/gms/internal/zzhc;->zzIO:Lcom/google/android/gms/internal/zzhm;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzhm;->zzgK()V
    :try_end_198
    .catch Ljava/io/IOException; {:try_start_18c .. :try_end_198} :catch_1a9

    goto :goto_1a7

    :catchall_199
    move-exception v18

    :try_start_19a
    invoke-virtual {v13}, Ljava/net/HttpURLConnection;->disconnect()V

    if-eqz p8, :cond_1a6

    move-object/from16 v0, p8

    iget-object v0, v0, Lcom/google/android/gms/internal/zzhc;->zzIO:Lcom/google/android/gms/internal/zzhm;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzhm;->zzgK()V

    :cond_1a6
    throw v18
    :try_end_1a7
    .catch Ljava/io/IOException; {:try_start_19a .. :try_end_1a7} :catch_1a9

    :cond_1a7
    :goto_1a7
    goto/16 :goto_37

    :catch_1a9
    move-exception v5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error while connecting to ad server: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V

    return-object v0
.end method

.method public static zza(Landroid/content/Context;Lcom/google/android/gms/internal/zzbm;Lcom/google/android/gms/internal/zzhc;)Lcom/google/android/gms/internal/zzhd;
    .registers 7

    sget-object v2, Lcom/google/android/gms/internal/zzhd;->zzqy:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    sget-object v0, Lcom/google/android/gms/internal/zzhd;->zzIQ:Lcom/google/android/gms/internal/zzhd;

    if-nez v0, :cond_1a

    new-instance v0, Lcom/google/android/gms/internal/zzhd;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_14

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    goto :goto_15

    :cond_14
    move-object v1, p0

    :goto_15
    invoke-direct {v0, v1, p1, p2}, Lcom/google/android/gms/internal/zzhd;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/zzbm;Lcom/google/android/gms/internal/zzhc;)V

    sput-object v0, Lcom/google/android/gms/internal/zzhd;->zzIQ:Lcom/google/android/gms/internal/zzhd;

    :cond_1a
    sget-object v0, Lcom/google/android/gms/internal/zzhd;->zzIQ:Lcom/google/android/gms/internal/zzhd;
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1e

    monitor-exit v2

    return-object v0

    :catchall_1e
    move-exception v3

    monitor-exit v2

    throw v3
.end method

.method private static zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzcb;Lcom/google/android/gms/internal/zzbz;)Lcom/google/android/gms/internal/zzjq$zza;
    .registers 4

    new-instance v0, Lcom/google/android/gms/internal/zzhd$5;

    invoke-direct {v0, p1, p2, p0}, Lcom/google/android/gms/internal/zzhd$5;-><init>(Lcom/google/android/gms/internal/zzcb;Lcom/google/android/gms/internal/zzbz;Ljava/lang/String;)V

    return-object v0
.end method

.method private static zza(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;I)V
    .registers 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;Ljava/lang/String;I)V"
        }
    .end annotation

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzQ(I)Z

    move-result v0

    if-eqz v0, :cond_d0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Http Response: {\n  URL:\n    "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n  Headers:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V

    if-eqz p1, :cond_85

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2d
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_85

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "    "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_60
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_84

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "      "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V

    goto :goto_60

    :cond_84
    goto :goto_2d

    :cond_85
    const-string v0, "  Body:"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V

    if-eqz p2, :cond_af

    const/4 v2, 0x0

    :goto_8d
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const v1, 0x186a0

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-ge v2, v0, :cond_ae

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit16 v1, v2, 0x3e8

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p2, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V

    add-int/lit16 v2, v2, 0x3e8

    goto :goto_8d

    :cond_ae
    goto :goto_b4

    :cond_af
    const-string v0, "    null"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V

    :goto_b4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "  Response Code:\n    "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->v(Ljava/lang/String;)V

    :cond_d0
    return-void
.end method

.method static synthetic zzb(Ljava/lang/String;Lcom/google/android/gms/internal/zzcb;Lcom/google/android/gms/internal/zzbz;)Lcom/google/android/gms/internal/zzjq$zza;
    .registers 4

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/zzhd;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzcb;Lcom/google/android/gms/internal/zzbz;)Lcom/google/android/gms/internal/zzjq$zza;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public zza(Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Lcom/google/android/gms/ads/internal/request/zzk;)V
    .registers 6

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbF()Lcom/google/android/gms/internal/zzih;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/zzhd;->mContext:Landroid/content/Context;

    iget-object v2, p1, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->zzrl:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/zzih;->zzb(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V

    new-instance v0, Lcom/google/android/gms/internal/zzhd$7;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/internal/zzhd$7;-><init>(Lcom/google/android/gms/internal/zzhd;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Lcom/google/android/gms/ads/internal/request/zzk;)V

    invoke-static {v0}, Lcom/google/android/gms/internal/zziq;->zza(Ljava/lang/Runnable;)Lcom/google/android/gms/internal/zzjg;

    return-void
.end method

.method public zzd(Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;)Lcom/google/android/gms/ads/internal/request/AdResponseParcel;
    .registers 6

    iget-object v0, p0, Lcom/google/android/gms/internal/zzhd;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzhd;->zzIT:Lcom/google/android/gms/internal/zzeg;

    iget-object v2, p0, Lcom/google/android/gms/internal/zzhd;->zzIS:Lcom/google/android/gms/internal/zzbm;

    iget-object v3, p0, Lcom/google/android/gms/internal/zzhd;->zzIR:Lcom/google/android/gms/internal/zzhc;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/google/android/gms/internal/zzhd;->zza(Landroid/content/Context;Lcom/google/android/gms/internal/zzeg;Lcom/google/android/gms/internal/zzbm;Lcom/google/android/gms/internal/zzhc;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;)Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-result-object v0

    return-object v0
.end method

.class public Lcom/itau/empresas/feature/atendimento/model/AlertaVO;
.super Ljava/lang/Object;
.source "AlertaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private alertas:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "alertas"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/atendimento/model/AlertaFAQ;>;"
        }
    .end annotation
.end field

.field private mensagem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "mensagem"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAlertas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/feature/atendimento/model/AlertaFAQ;>;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/model/AlertaVO;->alertas:Ljava/util/List;

    return-object v0
.end method

.class Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "FaqListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FaqAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter<Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;>;"
    }
.end annotation


# instance fields
.field private faqs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lbr/com/itau/textovoz/model/Faq;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;


# direct methods
.method public constructor <init>(Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;Ljava/util/List;)V
    .registers 3
    .param p2, "faqs"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/textovoz/model/Faq;>;)V"
        }
    .end annotation

    .line 174
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;->this$0:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 175
    iput-object p2, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;->faqs:Ljava/util/List;

    .line 176
    return-void
.end method


# virtual methods
.method public getItemCount()I
    .registers 2

    .line 199
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;->faqs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .registers 4

    .line 171
    move-object v0, p1

    check-cast v0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;

    invoke-virtual {p0, v0, p2}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;->onBindViewHolder(Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;I)V
    .registers 5
    .param p1, "holder"    # Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;
    .param p2, "position"    # I

    .line 193
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;->faqs:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lbr/com/itau/textovoz/model/Faq;

    .line 194
    .local v1, "f":Lbr/com/itau/textovoz/model/Faq;
    invoke-virtual {p1, v1}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;->bindFaq(Lbr/com/itau/textovoz/model/Faq;)V

    .line 195
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .registers 4

    .line 171
    invoke-virtual {p0, p1, p2}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;
    .registers 7
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .line 180
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;->this$0:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;

    invoke-virtual {v0}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 182
    .local v2, "inflater":Landroid/view/LayoutInflater;
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;->this$0:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;

    # getter for: Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->useCardView:Z
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;->access$200(Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 183
    sget v0, Lbr/com/itau/textovoz/R$layout;->card_view_faq:I

    const/4 v1, 0x0

    invoke-virtual {v2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .local v3, "v":Landroid/view/View;
    goto :goto_21

    .line 185
    .end local v3    # "v":Landroid/view/View;
    :cond_1a
    sget v0, Lbr/com/itau/textovoz/R$layout;->search_details_row:I

    const/4 v1, 0x0

    invoke-virtual {v2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 188
    .local v3, "v":Landroid/view/View;
    :goto_21
    new-instance v0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;->this$0:Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;

    invoke-direct {v0, v1, v3}, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqHolder;-><init>(Lbr/com/itau/textovoz/ui/fragment/FaqListFragment;Landroid/view/View;)V

    return-object v0
.end method

.method public setFaqs(Ljava/util/List;)V
    .registers 2
    .param p1, "faqs"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/textovoz/model/Faq;>;)V"
        }
    .end annotation

    .line 203
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/fragment/FaqListFragment$FaqAdapter;->faqs:Ljava/util/List;

    .line 204
    return-void
.end method

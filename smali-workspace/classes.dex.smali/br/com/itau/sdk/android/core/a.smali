.class final Lbr/com/itau/sdk/android/core/a;
.super Lbr/com/itau/sdk/android/core/c;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/ref/WeakReference<Landroid/app/Activity;>;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/Class<+Landroid/app/Activity;>;Ljava/lang/Boolean;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/Integer;Ljava/util/Set<Ljava/lang/ref/WeakReference<Landroid/view/View;>;>;>;"
        }
    .end annotation
.end field

.field private final d:Lde/greenrobot/event/EventBus;

.field private e:I


# direct methods
.method constructor <init>()V
    .registers 2

    .line 17
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/c;-><init>()V

    .line 19
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/a;->a:Ljava/util/Map;

    .line 20
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/a;->b:Ljava/util/Map;

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/a;->c:Ljava/util/Map;

    .line 22
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/a;->d:Lde/greenrobot/event/EventBus;

    return-void
.end method

.method private a(I)V
    .registers 8

    .line 100
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/a;->c:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/util/Set;

    .line 102
    if-nez v2, :cond_10

    .line 103
    return-void

    .line 105
    :cond_10
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_14
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_38

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/ref/WeakReference;

    .line 106
    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/view/View;

    .line 108
    if-eqz v5, :cond_37

    iget-object v0, p0, Lbr/com/itau/sdk/android/core/a;->d:Lde/greenrobot/event/EventBus;

    invoke-virtual {v0, v5}, Lde/greenrobot/event/EventBus;->isRegistered(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_37

    .line 109
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/a;->d:Lde/greenrobot/event/EventBus;

    invoke-virtual {v0, v5}, Lde/greenrobot/event/EventBus;->unregister(Ljava/lang/Object;)V

    .line 110
    :cond_37
    goto :goto_14

    .line 112
    :cond_38
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/a;->c:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    return-void
.end method

.method private a(Landroid/app/Activity;)V
    .registers 3

    .line 75
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/a;->d:Lde/greenrobot/event/EventBus;

    invoke-virtual {v0, p1}, Lde/greenrobot/event/EventBus;->isRegistered(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 76
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/a;->d:Lde/greenrobot/event/EventBus;

    invoke-virtual {v0, p1}, Lde/greenrobot/event/EventBus;->unregister(Ljava/lang/Object;)V

    .line 77
    :cond_d
    return-void
.end method

.method private b(Landroid/app/Activity;)V
    .registers 9

    .line 81
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/a;->c:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/util/Set;

    .line 83
    if-nez v2, :cond_14

    .line 84
    return-void

    .line 86
    :cond_14
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_18
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/ref/WeakReference;

    .line 87
    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/view/View;

    .line 88
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v6

    .line 89
    if-eqz v5, :cond_3b

    invoke-virtual {v6, v5}, Lde/greenrobot/event/EventBus;->isRegistered(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3b

    .line 90
    invoke-virtual {v6, v5}, Lde/greenrobot/event/EventBus;->register(Ljava/lang/Object;)V

    .line 91
    :cond_3b
    goto :goto_18

    .line 92
    :cond_3c
    return-void
.end method

.method private c(Landroid/app/Activity;)V
    .registers 7

    .line 117
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 118
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/a;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/Boolean;

    .line 119
    if-eqz v3, :cond_23

    .line 121
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_22

    iget-object v0, p0, Lbr/com/itau/sdk/android/core/a;->d:Lde/greenrobot/event/EventBus;

    invoke-virtual {v0, p1}, Lde/greenrobot/event/EventBus;->isRegistered(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_22

    .line 122
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/a;->d:Lde/greenrobot/event/EventBus;

    invoke-virtual {v0, p1}, Lde/greenrobot/event/EventBus;->register(Ljava/lang/Object;)V

    .line 124
    :cond_22
    return-void

    .line 129
    :cond_23
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/a;->d:Lde/greenrobot/event/EventBus;

    invoke-virtual {v0, p1}, Lde/greenrobot/event/EventBus;->isRegistered(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_46

    .line 132
    :try_start_2b
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/a;->d:Lde/greenrobot/event/EventBus;

    invoke-virtual {v0, p1}, Lde/greenrobot/event/EventBus;->register(Ljava/lang/Object;)V

    .line 133
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/a;->b:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3a
    .catch Lde/greenrobot/event/EventBusException; {:try_start_2b .. :try_end_3a} :catch_3b

    .line 137
    goto :goto_46

    .line 135
    :catch_3b
    move-exception v4

    .line 136
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/a;->b:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    :cond_46
    :goto_46
    return-void
.end method


# virtual methods
.method a()Landroid/app/Activity;
    .registers 3

    .line 27
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/a;->a:Ljava/util/Map;

    iget v1, p0, Lbr/com/itau/sdk/android/core/a;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    return-object v0
.end method

.method a(Landroid/view/View;)V
    .registers 6

    .line 59
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/a;->a()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    .line 61
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/a;->c:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/util/Set;

    .line 63
    if-nez v3, :cond_25

    .line 64
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 65
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/a;->c:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    :cond_25
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/a;->d:Lde/greenrobot/event/EventBus;

    invoke-virtual {v0, p1}, Lde/greenrobot/event/EventBus;->isRegistered(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3a

    .line 69
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 70
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/a;->d:Lde/greenrobot/event/EventBus;

    invoke-virtual {v0, p1}, Lde/greenrobot/event/EventBus;->register(Ljava/lang/Object;)V

    .line 72
    :cond_3a
    return-void
.end method

.method b()V
    .registers 2

    .line 95
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/a;->a()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/a;->a(I)V

    .line 96
    return-void
.end method

.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .registers 6

    .line 32
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iput v0, p0, Lbr/com/itau/sdk/android/core/a;->e:I

    .line 33
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/a;->a:Ljava/util/Map;

    iget v1, p0, Lbr/com/itau/sdk/android/core/a;->e:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/a;->c(Landroid/app/Activity;)V

    .line 35
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .registers 6

    .line 46
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    .line 47
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/a;->a:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/ref/WeakReference;

    .line 49
    if-eqz v3, :cond_16

    .line 50
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->clear()V

    .line 52
    :cond_16
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/a;->a:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    invoke-direct {p0, v2}, Lbr/com/itau/sdk/android/core/a;->a(I)V

    .line 55
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/a;->a(Landroid/app/Activity;)V

    .line 56
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .registers 3

    .line 39
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iput v0, p0, Lbr/com/itau/sdk/android/core/a;->e:I

    .line 40
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/a;->c(Landroid/app/Activity;)V

    .line 41
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/a;->b(Landroid/app/Activity;)V

    .line 42
    return-void
.end method

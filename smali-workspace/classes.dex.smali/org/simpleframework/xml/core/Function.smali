.class Lorg/simpleframework/xml/core/Function;
.super Ljava/lang/Object;
.source "Function.java"


# instance fields
.field private final contextual:Z

.field private final method:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>(Ljava/lang/reflect/Method;)V
    .registers 3
    .param p1, "method"    # Ljava/lang/reflect/Method;

    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/simpleframework/xml/core/Function;-><init>(Ljava/lang/reflect/Method;Z)V

    .line 53
    return-void
.end method

.method public constructor <init>(Ljava/lang/reflect/Method;Z)V
    .registers 3
    .param p1, "method"    # Ljava/lang/reflect/Method;
    .param p2, "contextual"    # Z

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-boolean p2, p0, Lorg/simpleframework/xml/core/Function;->contextual:Z

    .line 65
    iput-object p1, p0, Lorg/simpleframework/xml/core/Function;->method:Ljava/lang/reflect/Method;

    .line 66
    return-void
.end method


# virtual methods
.method public call(Lorg/simpleframework/xml/core/Context;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 8
    .param p1, "context"    # Lorg/simpleframework/xml/core/Context;
    .param p2, "source"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 79
    if-eqz p2, :cond_25

    .line 80
    invoke-interface {p1}, Lorg/simpleframework/xml/core/Context;->getSession()Lorg/simpleframework/xml/core/Session;

    move-result-object v3

    .line 81
    .local v3, "session":Lorg/simpleframework/xml/core/Session;
    invoke-virtual {v3}, Lorg/simpleframework/xml/core/Session;->getMap()Ljava/util/Map;

    move-result-object v4

    .line 83
    .local v4, "table":Ljava/util/Map;
    iget-boolean v0, p0, Lorg/simpleframework/xml/core/Function;->contextual:Z

    if-eqz v0, :cond_1b

    .line 84
    iget-object v0, p0, Lorg/simpleframework/xml/core/Function;->method:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v4, v1, v2

    invoke-virtual {v0, p2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 86
    :cond_1b
    iget-object v0, p0, Lorg/simpleframework/xml/core/Function;->method:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 88
    .end local v3    # "session":Lorg/simpleframework/xml/core/Session;
    .end local v4    # "table":Ljava/util/Map;
    :cond_25
    const/4 v0, 0x0

    return-object v0
.end method

.class final Lcom/itau/empresas/feature/extrato/util/ExtratoUtil$1;
.super Ljava/lang/Object;
.source "ExtratoUtil.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/extrato/util/ExtratoUtil;->ordenar(Ljava/util/List;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Ljava/util/Comparator<Lcom/itau/empresas/api/model/LancamentoVO;>;"
    }
.end annotation


# instance fields
.field final synthetic val$ordem:I


# direct methods
.method constructor <init>(I)V
    .registers 2

    .line 55
    iput p1, p0, Lcom/itau/empresas/feature/extrato/util/ExtratoUtil$1;->val$ordem:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/itau/empresas/api/model/LancamentoVO;Lcom/itau/empresas/api/model/LancamentoVO;)I
    .registers 5
    .param p1, "lhs"    # Lcom/itau/empresas/api/model/LancamentoVO;
    .param p2, "rhs"    # Lcom/itau/empresas/api/model/LancamentoVO;

    .line 59
    iget v0, p0, Lcom/itau/empresas/feature/extrato/util/ExtratoUtil$1;->val$ordem:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_12

    .line 60
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LancamentoVO;->getDataLancamento()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/itau/empresas/api/model/LancamentoVO;->getDataLancamento()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0

    .line 63
    :cond_12
    invoke-virtual {p2}, Lcom/itau/empresas/api/model/LancamentoVO;->getDataLancamento()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LancamentoVO;->getDataLancamento()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 5

    .line 55
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/api/model/LancamentoVO;

    move-object v1, p2

    check-cast v1, Lcom/itau/empresas/api/model/LancamentoVO;

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/extrato/util/ExtratoUtil$1;->compare(Lcom/itau/empresas/api/model/LancamentoVO;Lcom/itau/empresas/api/model/LancamentoVO;)I

    move-result v0

    return v0
.end method

.class final Lorg/threeten/bp/temporal/TemporalAdjusters$RelativeDayOfWeek;
.super Ljava/lang/Object;
.source "TemporalAdjusters.java"

# interfaces
.implements Lorg/threeten/bp/temporal/TemporalAdjuster;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/threeten/bp/temporal/TemporalAdjusters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "RelativeDayOfWeek"
.end annotation


# instance fields
.field private final dowValue:I

.field private final relative:I


# direct methods
.method private constructor <init>(ILorg/threeten/bp/DayOfWeek;)V
    .registers 4
    .param p1, "relative"    # I
    .param p2, "dayOfWeek"    # Lorg/threeten/bp/DayOfWeek;

    .line 449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450
    const-string v0, "dayOfWeek"

    invoke-static {p2, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 451
    iput p1, p0, Lorg/threeten/bp/temporal/TemporalAdjusters$RelativeDayOfWeek;->relative:I

    .line 452
    invoke-virtual {p2}, Lorg/threeten/bp/DayOfWeek;->getValue()I

    move-result v0

    iput v0, p0, Lorg/threeten/bp/temporal/TemporalAdjusters$RelativeDayOfWeek;->dowValue:I

    .line 453
    return-void
.end method

.method synthetic constructor <init>(ILorg/threeten/bp/DayOfWeek;Lorg/threeten/bp/temporal/TemporalAdjusters$1;)V
    .registers 4
    .param p1, "x0"    # I
    .param p2, "x1"    # Lorg/threeten/bp/DayOfWeek;
    .param p3, "x2"    # Lorg/threeten/bp/temporal/TemporalAdjusters$1;

    .line 443
    invoke-direct {p0, p1, p2}, Lorg/threeten/bp/temporal/TemporalAdjusters$RelativeDayOfWeek;-><init>(ILorg/threeten/bp/DayOfWeek;)V

    return-void
.end method


# virtual methods
.method public adjustInto(Lorg/threeten/bp/temporal/Temporal;)Lorg/threeten/bp/temporal/Temporal;
    .registers 7
    .param p1, "temporal"    # Lorg/threeten/bp/temporal/Temporal;

    .line 457
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->DAY_OF_WEEK:Lorg/threeten/bp/temporal/ChronoField;

    invoke-interface {p1, v0}, Lorg/threeten/bp/temporal/Temporal;->get(Lorg/threeten/bp/temporal/TemporalField;)I

    move-result v3

    .line 458
    .local v3, "calDow":I
    iget v0, p0, Lorg/threeten/bp/temporal/TemporalAdjusters$RelativeDayOfWeek;->relative:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_10

    iget v0, p0, Lorg/threeten/bp/temporal/TemporalAdjusters$RelativeDayOfWeek;->dowValue:I

    if-ne v3, v0, :cond_10

    .line 459
    return-object p1

    .line 461
    :cond_10
    iget v0, p0, Lorg/threeten/bp/temporal/TemporalAdjusters$RelativeDayOfWeek;->relative:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_29

    .line 462
    iget v0, p0, Lorg/threeten/bp/temporal/TemporalAdjusters$RelativeDayOfWeek;->dowValue:I

    sub-int v4, v3, v0

    .line 463
    .local v4, "daysDiff":I
    if-ltz v4, :cond_20

    rsub-int/lit8 v0, v4, 0x7

    int-to-long v0, v0

    goto :goto_22

    :cond_20
    neg-int v0, v4

    int-to-long v0, v0

    :goto_22
    sget-object v2, Lorg/threeten/bp/temporal/ChronoUnit;->DAYS:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-interface {p1, v0, v1, v2}, Lorg/threeten/bp/temporal/Temporal;->plus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    return-object v0

    .line 465
    .end local v4    # "daysDiff":I
    :cond_29
    iget v0, p0, Lorg/threeten/bp/temporal/TemporalAdjusters$RelativeDayOfWeek;->dowValue:I

    sub-int v4, v0, v3

    .line 466
    .local v4, "daysDiff":I
    if-ltz v4, :cond_33

    rsub-int/lit8 v0, v4, 0x7

    int-to-long v0, v0

    goto :goto_35

    :cond_33
    neg-int v0, v4

    int-to-long v0, v0

    :goto_35
    sget-object v2, Lorg/threeten/bp/temporal/ChronoUnit;->DAYS:Lorg/threeten/bp/temporal/ChronoUnit;

    invoke-interface {p1, v0, v1, v2}, Lorg/threeten/bp/temporal/Temporal;->minus(JLorg/threeten/bp/temporal/TemporalUnit;)Lorg/threeten/bp/temporal/Temporal;

    move-result-object v0

    return-object v0
.end method

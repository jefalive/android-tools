.class public Lbr/com/itau/textovoz/util/ViewUtils;
.super Ljava/lang/Object;
.source "ViewUtils.java"


# direct methods
.method static getResourceFromAttr(Landroid/content/Context;I)I
    .registers 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "attr"    # I

    .line 86
    if-nez p0, :cond_8

    .line 87
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 89
    :cond_8
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    .line 90
    .local v2, "typedvalueattr":Landroid/util/TypedValue;
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v2, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 91
    iget v0, v2, Landroid/util/TypedValue;->resourceId:I

    return v0
.end method

.method public static hideSoftKeyboard(Landroid/app/Activity;)V
    .registers 4
    .param p0, "activity"    # Landroid/app/Activity;

    .line 30
    invoke-virtual {p0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1b

    .line 31
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    .line 32
    .local v2, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 34
    .end local v2    # "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    :cond_1b
    return-void
.end method

.method public static isVisible(Landroid/view/View;)Z
    .registers 2
    .param p0, "view"    # Landroid/view/View;

    .line 22
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    goto :goto_9

    :cond_8
    const/4 v0, 0x0

    :goto_9
    return v0
.end method

.method public static setHeightList(Landroid/widget/ExpandableListView;I)V
    .registers 15
    .param p0, "listView"    # Landroid/widget/ExpandableListView;
    .param p1, "group"    # I

    .line 47
    invoke-virtual {p0}, Landroid/widget/ExpandableListView;->getExpandableListAdapter()Landroid/widget/ExpandableListAdapter;

    move-result-object v6

    .line 48
    .local v6, "listAdapter":Landroid/widget/ExpandableListAdapter;
    const/4 v7, 0x0

    .line 49
    .local v7, "totalHeight":I
    invoke-virtual {p0}, Landroid/widget/ExpandableListView;->getWidth()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 50
    .local v8, "desiredWidth":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_10
    invoke-interface {v6}, Landroid/widget/ExpandableListAdapter;->getGroupCount()I

    move-result v0

    if-ge v9, v0, :cond_55

    .line 51
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-interface {v6, v9, v0, v1, p0}, Landroid/widget/ExpandableListAdapter;->getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    .line 52
    .local v10, "groupItem":Landroid/view/View;
    const/4 v0, 0x0

    invoke-virtual {v10, v8, v0}, Landroid/view/View;->measure(II)V

    .line 53
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v7, v0

    .line 55
    invoke-virtual {p0, v9}, Landroid/widget/ExpandableListView;->isGroupExpanded(I)Z

    move-result v0

    if-eqz v0, :cond_2d

    if-ne v9, p1, :cond_35

    :cond_2d
    invoke-virtual {p0, v9}, Landroid/widget/ExpandableListView;->isGroupExpanded(I)Z

    move-result v0

    if-nez v0, :cond_52

    if-ne v9, p1, :cond_52

    .line 56
    :cond_35
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_36
    invoke-interface {v6, v9}, Landroid/widget/ExpandableListAdapter;->getChildrenCount(I)I

    move-result v0

    if-ge v11, v0, :cond_52

    .line 57
    move-object v0, v6

    move v1, v9

    move v2, v11

    move-object v5, p0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface/range {v0 .. v5}, Landroid/widget/ExpandableListAdapter;->getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v12

    .line 58
    .local v12, "listItem":Landroid/view/View;
    const/4 v0, 0x0

    invoke-virtual {v12, v8, v0}, Landroid/view/View;->measure(II)V

    .line 59
    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v7, v0

    .line 56
    .end local v12    # "listItem":Landroid/view/View;
    add-int/lit8 v11, v11, 0x1

    goto :goto_36

    .line 50
    .end local v10    # "groupItem":Landroid/view/View;
    .end local v11    # "j":I
    :cond_52
    add-int/lit8 v9, v9, 0x1

    goto :goto_10

    .line 65
    .end local v9    # "i":I
    :cond_55
    invoke-virtual {p0}, Landroid/widget/ExpandableListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    .line 66
    .local v9, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Landroid/widget/ExpandableListView;->getDividerHeight()I

    move-result v0

    invoke-interface {v6}, Landroid/widget/ExpandableListAdapter;->getGroupCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    mul-int/2addr v0, v1

    add-int v10, v7, v0

    .line 67
    .local v10, "height":I
    const/16 v0, 0xa

    if-ge v10, v0, :cond_6c

    .line 68
    const/16 v10, 0xc8

    .line 70
    :cond_6c
    add-int/lit8 v0, v10, 0x14

    iput v0, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 71
    invoke-virtual {p0, v9}, Landroid/widget/ExpandableListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 72
    invoke-virtual {p0}, Landroid/widget/ExpandableListView;->requestLayout()V

    .line 73
    return-void
.end method

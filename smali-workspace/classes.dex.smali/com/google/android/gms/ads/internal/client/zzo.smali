.class public Lcom/google/android/gms/ads/internal/client/zzo;
.super Lcom/google/android/gms/ads/internal/client/zzx$zza;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# instance fields
.field private zzpV:Ljava/lang/Object;

.field private final zzuy:Ljava/util/Random;

.field private zzuz:J


# direct methods
.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/client/zzx$zza;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/client/zzo;->zzpV:Ljava/lang/Object;

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/client/zzo;->zzuy:Ljava/util/Random;

    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/client/zzo;->zzcY()V

    return-void
.end method


# virtual methods
.method public getValue()J
    .registers 3

    iget-wide v0, p0, Lcom/google/android/gms/ads/internal/client/zzo;->zzuz:J

    return-wide v0
.end method

.method public zzcY()V
    .registers 10

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/client/zzo;->zzpV:Ljava/lang/Object;

    monitor-enter v4

    const-wide/16 v5, 0x0

    const/4 v7, 0x3

    :cond_6
    add-int/lit8 v7, v7, -0x1

    if-lez v7, :cond_24

    :try_start_a
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/client/zzo;->zzuy:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    int-to-long v0, v0

    const-wide v2, 0x80000000L

    add-long v5, v0, v2

    iget-wide v0, p0, Lcom/google/android/gms/ads/internal/client/zzo;->zzuz:J

    cmp-long v0, v5, v0

    if-eqz v0, :cond_6

    const-wide/16 v0, 0x0

    cmp-long v0, v5, v0

    if-eqz v0, :cond_6

    :cond_24
    iput-wide v5, p0, Lcom/google/android/gms/ads/internal/client/zzo;->zzuz:J
    :try_end_26
    .catchall {:try_start_a .. :try_end_26} :catchall_28

    monitor-exit v4

    goto :goto_2b

    :catchall_28
    move-exception v8

    monitor-exit v4

    throw v8

    :goto_2b
    return-void
.end method

.class public Lcom/google/android/gms/internal/zzds;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/zzdf;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public zza(Lcom/google/android/gms/internal/zzjp;Ljava/util/Map;)V
    .registers 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/google/android/gms/internal/zzjp;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;)V"
        }
    .end annotation

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbR()Lcom/google/android/gms/internal/zzdq;

    move-result-object v1

    const-string v0, "abort"

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-virtual {v1, p1}, Lcom/google/android/gms/internal/zzdq;->zzd(Lcom/google/android/gms/internal/zzjp;)Z

    move-result v0

    if-nez v0, :cond_17

    const-string v0, "Precache abort but no preload task running."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    :cond_17
    return-void

    :cond_18
    const-string v0, "src"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/String;

    if-nez v2, :cond_29

    const-string v0, "Precache video action is missing the src parameter."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    return-void

    :cond_29
    const-string v0, "player"

    :try_start_2b
    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_34
    .catch Ljava/lang/NumberFormatException; {:try_start_2b .. :try_end_34} :catch_36

    move-result v3

    goto :goto_38

    :catch_36
    move-exception v4

    const/4 v3, 0x0

    :goto_38
    const-string v0, "mimetype"

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4a

    const-string v0, "mimetype"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/String;

    goto :goto_4c

    :cond_4a
    const-string v4, ""

    :goto_4c
    invoke-virtual {v1, p1}, Lcom/google/android/gms/internal/zzdq;->zze(Lcom/google/android/gms/internal/zzjp;)Z

    move-result v0

    if-eqz v0, :cond_58

    const-string v0, "Precache task already running."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    return-void

    :cond_58
    invoke-interface {p1}, Lcom/google/android/gms/internal/zzjp;->zzhR()Lcom/google/android/gms/ads/internal/zzd;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/zzb;->zzv(Ljava/lang/Object;)V

    invoke-interface {p1}, Lcom/google/android/gms/internal/zzjp;->zzhR()Lcom/google/android/gms/ads/internal/zzd;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/zzd;->zzpw:Lcom/google/android/gms/internal/zzdt;

    invoke-interface {v0, p1, v3, v4}, Lcom/google/android/gms/internal/zzdt;->zza(Lcom/google/android/gms/internal/zzjp;ILjava/lang/String;)Lcom/google/android/gms/internal/zzdr;

    move-result-object v5

    new-instance v6, Lcom/google/android/gms/internal/zzdp;

    invoke-direct {v6, p1, v5, v2}, Lcom/google/android/gms/internal/zzdp;-><init>(Lcom/google/android/gms/internal/zzjp;Lcom/google/android/gms/internal/zzdr;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/google/android/gms/internal/zzdp;->zzhn()Ljava/util/concurrent/Future;

    return-void
.end method

.class public final enum Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;
.super Ljava/lang/Enum;
.source "LimitLine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LimitLabelPosition"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

.field public static final enum POS_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

.field public static final enum POS_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 58
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    const-string v1, "POS_LEFT"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;->POS_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    const-string v1, "POS_RIGHT"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;->POS_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    .line 57
    const/4 v0, 0x2

    new-array v0, v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;->POS_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;->POS_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;->$VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 57
    const-class v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;
    .registers 1

    .line 57
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;->$VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    invoke-virtual {v0}, [Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine$LimitLabelPosition;

    return-object v0
.end method

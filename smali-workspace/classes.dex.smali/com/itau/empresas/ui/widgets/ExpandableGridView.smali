.class public Lcom/itau/empresas/ui/widgets/ExpandableGridView;
.super Landroid/widget/GridView;
.source "ExpandableGridView.java"


# instance fields
.field expanded:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 17
    invoke-direct {p0, p1}, Landroid/widget/GridView;-><init>(Landroid/content/Context;)V

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/widgets/ExpandableGridView;->expanded:Z

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/widgets/ExpandableGridView;->expanded:Z

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .line 26
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/widgets/ExpandableGridView;->expanded:Z

    .line 27
    return-void
.end method


# virtual methods
.method public isExpanded()Z
    .registers 2

    .line 30
    iget-boolean v0, p0, Lcom/itau/empresas/ui/widgets/ExpandableGridView;->expanded:Z

    return v0
.end method

.method public onMeasure(II)V
    .registers 7
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .line 36
    invoke-virtual {p0}, Lcom/itau/empresas/ui/widgets/ExpandableGridView;->isExpanded()Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 39
    const v0, 0xffffff

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 41
    .local v2, "expandSpec":I
    invoke-super {p0, p1, v2}, Landroid/widget/GridView;->onMeasure(II)V

    .line 43
    invoke-virtual {p0}, Lcom/itau/empresas/ui/widgets/ExpandableGridView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 44
    .local v3, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Lcom/itau/empresas/ui/widgets/ExpandableGridView;->getMeasuredHeight()I

    move-result v0

    iput v0, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 45
    .end local v2    # "expandSpec":I
    .end local v3    # "params":Landroid/view/ViewGroup$LayoutParams;
    goto :goto_20

    .line 46
    :cond_1d
    invoke-super {p0, p1, p2}, Landroid/widget/GridView;->onMeasure(II)V

    .line 48
    :goto_20
    return-void
.end method

.method public setExpanded(Z)V
    .registers 2
    .param p1, "expanded"    # Z

    .line 51
    iput-boolean p1, p0, Lcom/itau/empresas/ui/widgets/ExpandableGridView;->expanded:Z

    .line 52
    return-void
.end method

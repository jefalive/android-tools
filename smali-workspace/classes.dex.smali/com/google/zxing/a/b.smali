.class public final Lcom/google/zxing/a/b;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:[I


# direct methods
.method public constructor <init>(II)V
    .registers 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    if-lt p1, v0, :cond_9

    const/4 v0, 0x1

    if-ge p2, v0, :cond_11

    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Both dimensions must be greater than 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    iput p1, p0, Lcom/google/zxing/a/b;->a:I

    iput p2, p0, Lcom/google/zxing/a/b;->b:I

    add-int/lit8 v0, p1, 0x1f

    div-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/zxing/a/b;->c:I

    iget v0, p0, Lcom/google/zxing/a/b;->c:I

    mul-int/2addr v0, p2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/zxing/a/b;->d:[I

    return-void
.end method

.method private constructor <init>(III[I)V
    .registers 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/zxing/a/b;->a:I

    iput p2, p0, Lcom/google/zxing/a/b;->b:I

    iput p3, p0, Lcom/google/zxing/a/b;->c:I

    iput-object p4, p0, Lcom/google/zxing/a/b;->d:[I

    return-void
.end method


# virtual methods
.method public a()Lcom/google/zxing/a/b;
    .registers 6

    new-instance v0, Lcom/google/zxing/a/b;

    iget v1, p0, Lcom/google/zxing/a/b;->a:I

    iget v2, p0, Lcom/google/zxing/a/b;->b:I

    iget v3, p0, Lcom/google/zxing/a/b;->c:I

    iget-object v4, p0, Lcom/google/zxing/a/b;->d:[I

    invoke-virtual {v4}, [I->clone()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/zxing/a/b;-><init>(III[I)V

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4

    invoke-static {}, Ljava/lang/System;->lineSeparator()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/zxing/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 10

    new-instance v3, Ljava/lang/StringBuilder;

    iget v0, p0, Lcom/google/zxing/a/b;->b:I

    iget v1, p0, Lcom/google/zxing/a/b;->a:I

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v4, 0x0

    :goto_d
    iget v0, p0, Lcom/google/zxing/a/b;->b:I

    if-ge v4, v0, :cond_2b

    const/4 v5, 0x0

    :goto_12
    iget v0, p0, Lcom/google/zxing/a/b;->a:I

    if-ge v5, v0, :cond_25

    invoke-virtual {p0, v5, v4}, Lcom/google/zxing/a/b;->a(II)Z

    move-result v0

    if-eqz v0, :cond_1e

    move-object v0, p1

    goto :goto_1f

    :cond_1e
    move-object v0, p2

    :goto_1f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v5, v5, 0x1

    goto :goto_12

    :cond_25
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v4, 0x1

    goto :goto_d

    :cond_2b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(II)Z
    .registers 6

    iget v0, p0, Lcom/google/zxing/a/b;->c:I

    mul-int/2addr v0, p2

    div-int/lit8 v1, p1, 0x20

    add-int v2, v0, v1

    iget-object v0, p0, Lcom/google/zxing/a/b;->d:[I

    aget v0, v0, v2

    and-int/lit8 v1, p1, 0x1f

    ushr-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    goto :goto_15

    :cond_14
    const/4 v0, 0x0

    :goto_15
    return v0
.end method

.method public b(II)V
    .registers 8

    iget v0, p0, Lcom/google/zxing/a/b;->c:I

    mul-int/2addr v0, p2

    div-int/lit8 v1, p1, 0x20

    add-int v4, v0, v1

    iget-object v0, p0, Lcom/google/zxing/a/b;->d:[I

    aget v1, v0, v4

    and-int/lit8 v2, p1, 0x1f

    const/4 v3, 0x1

    shl-int v2, v3, v2

    or-int/2addr v1, v2

    aput v1, v0, v4

    return-void
.end method

.method public synthetic clone()Ljava/lang/Object;
    .registers 2

    invoke-virtual {p0}, Lcom/google/zxing/a/b;->a()Lcom/google/zxing/a/b;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5

    instance-of v0, p1, Lcom/google/zxing/a/b;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    return v0

    :cond_6
    move-object v2, p1

    check-cast v2, Lcom/google/zxing/a/b;

    iget v0, p0, Lcom/google/zxing/a/b;->a:I

    iget v1, v2, Lcom/google/zxing/a/b;->a:I

    if-ne v0, v1, :cond_27

    iget v0, p0, Lcom/google/zxing/a/b;->b:I

    iget v1, v2, Lcom/google/zxing/a/b;->b:I

    if-ne v0, v1, :cond_27

    iget v0, p0, Lcom/google/zxing/a/b;->c:I

    iget v1, v2, Lcom/google/zxing/a/b;->c:I

    if-ne v0, v1, :cond_27

    iget-object v0, p0, Lcom/google/zxing/a/b;->d:[I

    iget-object v1, v2, Lcom/google/zxing/a/b;->d:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    if-eqz v0, :cond_27

    const/4 v0, 0x1

    goto :goto_28

    :cond_27
    const/4 v0, 0x0

    :goto_28
    return v0
.end method

.method public hashCode()I
    .registers 4

    iget v2, p0, Lcom/google/zxing/a/b;->a:I

    mul-int/lit8 v0, v2, 0x1f

    iget v1, p0, Lcom/google/zxing/a/b;->a:I

    add-int v2, v0, v1

    mul-int/lit8 v0, v2, 0x1f

    iget v1, p0, Lcom/google/zxing/a/b;->b:I

    add-int v2, v0, v1

    mul-int/lit8 v0, v2, 0x1f

    iget v1, p0, Lcom/google/zxing/a/b;->c:I

    add-int v2, v0, v1

    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/google/zxing/a/b;->d:[I

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([I)I

    move-result v1

    add-int v2, v0, v1

    return v2
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    const-string v0, "X "

    const-string v1, "  "

    invoke-virtual {p0, v0, v1}, Lcom/google/zxing/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class final enum Lcom/google/zxing/client/android/d;
.super Ljava/lang/Enum;


# static fields
.field public static final enum a:Lcom/google/zxing/client/android/d;

.field public static final enum b:Lcom/google/zxing/client/android/d;

.field public static final enum c:Lcom/google/zxing/client/android/d;

.field private static final synthetic d:[Lcom/google/zxing/client/android/d;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    new-instance v0, Lcom/google/zxing/client/android/d;

    const-string v1, "PREVIEW"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/client/android/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/zxing/client/android/d;->a:Lcom/google/zxing/client/android/d;

    new-instance v0, Lcom/google/zxing/client/android/d;

    const-string v1, "SUCCESS"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/client/android/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/zxing/client/android/d;->b:Lcom/google/zxing/client/android/d;

    new-instance v0, Lcom/google/zxing/client/android/d;

    const-string v1, "DONE"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/google/zxing/client/android/d;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/zxing/client/android/d;->c:Lcom/google/zxing/client/android/d;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/zxing/client/android/d;

    sget-object v1, Lcom/google/zxing/client/android/d;->a:Lcom/google/zxing/client/android/d;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/client/android/d;->b:Lcom/google/zxing/client/android/d;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/zxing/client/android/d;->c:Lcom/google/zxing/client/android/d;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/zxing/client/android/d;->d:[Lcom/google/zxing/client/android/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/zxing/client/android/d;
    .registers 2

    const-class v0, Lcom/google/zxing/client/android/d;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/zxing/client/android/d;

    return-object v0
.end method

.method public static values()[Lcom/google/zxing/client/android/d;
    .registers 1

    sget-object v0, Lcom/google/zxing/client/android/d;->d:[Lcom/google/zxing/client/android/d;

    invoke-virtual {v0}, [Lcom/google/zxing/client/android/d;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/zxing/client/android/d;

    return-object v0
.end method

.class public Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;
.super Landroid/support/v7/widget/RecyclerView$ItemDecoration;
.source "HeaderDecoration.java"


# instance fields
.field private adapter:Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;

.field private mHeaderCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/Long;Landroid/support/v7/widget/RecyclerView$ViewHolder;>;"
        }
    .end annotation
.end field

.field private mRenderInline:Z


# direct methods
.method public constructor <init>(Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;)V
    .registers 3
    .param p1, "adapter"    # Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;

    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;-><init>(Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;Z)V

    .line 24
    return-void
.end method

.method private constructor <init>(Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;Z)V
    .registers 4
    .param p1, "adapter"    # Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;
    .param p2, "renderInline"    # Z

    .line 27
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$ItemDecoration;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->adapter:Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;

    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->mHeaderCache:Ljava/util/Map;

    .line 30
    iput-boolean p2, p0, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->mRenderInline:Z

    .line 31
    return-void
.end method

.method private calculaLarguraViewParaProximoHeader(Landroid/support/v7/widget/RecyclerView;III)Ljava/lang/Integer;
    .registers 9
    .param p1, "parent"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "headerHeight"    # I
    .param p3, "i"    # I
    .param p4, "posicaoAtual"    # I

    .line 149
    invoke-virtual {p1, p3}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 150
    .local v2, "next":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getY()F

    move-result v0

    float-to-int v0, v0

    invoke-direct {p0, p1, p4}, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->getHeader(Landroid/support/v7/widget/RecyclerView;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v1

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 151
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    add-int/2addr v1, p2

    sub-int v3, v0, v1

    .line 152
    .local v3, "offset":I
    if-gez v3, :cond_1d

    .line 153
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    .line 155
    :cond_1d
    const/4 v0, 0x0

    return-object v0
.end method

.method private ehHeader(I)Z
    .registers 6
    .param p1, "position"    # I

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->adapter:Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;

    invoke-interface {v0, p1}, Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;->getHeaderId(I)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

.method private getHeader(Landroid/support/v7/widget/RecyclerView;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .registers 15
    .param p1, "parent"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "position"    # I

    .line 60
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->adapter:Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;

    invoke-interface {v0, p2}, Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;->getHeaderId(I)J

    move-result-wide v4

    .line 62
    .local v4, "key":J
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->mHeaderCache:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->mHeaderCache:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$ViewHolder;

    return-object v0

    .line 65
    :cond_1f
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->adapter:Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;

    invoke-interface {v0, p1}, Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;->onCreateHeaderViewHolder(Landroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v6

    .line 66
    .local v6, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    iget-object v7, v6, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 67
    .local v7, "header":Landroid/view/View;
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->adapter:Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;

    invoke-interface {v0, v6, p2}, Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;->onBindHeaderViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V

    .line 69
    .line 70
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 71
    .line 72
    .local v8, "widthSpec":I
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    .line 74
    .line 75
    .local v9, "heightSpec":I
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 74
    invoke-static {v8, v0, v1}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v10

    .line 77
    .line 78
    .local v10, "childWidth":I
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 77
    invoke-static {v9, v0, v1}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v11

    .line 81
    .local v11, "childHeight":I
    invoke-virtual {v7, v10, v11}, Landroid/view/View;->measure(II)V

    .line 82
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v7, v2, v3, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 84
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->mHeaderCache:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    return-object v6
.end method

.method private getHeaderHeightForLayout(Landroid/view/View;)I
    .registers 3
    .param p1, "header"    # Landroid/view/View;

    .line 171
    iget-boolean v0, p0, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->mRenderInline:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    goto :goto_a

    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    :goto_a
    return v0
.end method

.method private getHeaderTop(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/view/View;II)I
    .registers 18
    .param p1, "parent"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "header"    # Landroid/view/View;
    .param p4, "posicao"    # I
    .param p5, "layoutPos"    # I

    .line 125
    invoke-direct {p0, p3}, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->getHeaderHeightForLayout(Landroid/view/View;)I

    move-result v2

    .line 126
    .local v2, "headerHeight":I
    invoke-virtual {p2}, Landroid/view/View;->getY()F

    move-result v0

    float-to-int v0, v0

    sub-int v3, v0, v2

    .line 127
    .local v3, "top":I
    if-nez p5, :cond_3e

    .line 128
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v4

    .line 129
    .local v4, "count":I
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->adapter:Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;

    move/from16 v1, p4

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;->getHeaderId(I)J

    move-result-wide v5

    .line 131
    .local v5, "idAtualDoHeader":J
    const/4 v7, 0x1

    .local v7, "i":I
    :goto_1a
    if-ge v7, v4, :cond_39

    .line 132
    invoke-virtual {p1, v7}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result v8

    .line 133
    .local v8, "posicaoAtual":I
    invoke-direct {p0, v8}, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->proximaPosicaoDoHeader(I)J

    move-result-wide v9

    .line 134
    .local v9, "proximoIdDoHeader":J
    cmp-long v0, v9, v5

    if-eqz v0, :cond_36

    .line 135
    invoke-direct {p0, p1, v2, v7, v8}, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->calculaLarguraViewParaProximoHeader(Landroid/support/v7/widget/RecyclerView;III)Ljava/lang/Integer;

    move-result-object v11

    .line 137
    .local v11, "offset":Ljava/lang/Integer;
    if-eqz v11, :cond_34

    const/4 v0, 0x1

    goto :goto_35

    :cond_34
    const/4 v0, 0x0

    :goto_35
    return v0

    .line 131
    .end local v8    # "posicaoAtual":I
    .end local v9    # "proximoIdDoHeader":J
    .end local v11    # "offset":Ljava/lang/Integer;
    :cond_36
    add-int/lit8 v7, v7, 0x1

    goto :goto_1a

    .line 140
    .end local v7    # "i":I
    :cond_39
    const/4 v0, 0x0

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 143
    .end local v4    # "count":I
    .end local v5    # "idAtualDoHeader":J
    :cond_3e
    return v3
.end method

.method private mostraHeaderAcimaDoItem(I)Z
    .registers 6
    .param p1, "itemAdapterPosition"    # I

    .line 51
    if-eqz p1, :cond_14

    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->adapter:Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;->getHeaderId(I)J

    move-result-wide v0

    iget-object v2, p0, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->adapter:Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;

    .line 52
    invoke-interface {v2, p1}, Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;->getHeaderId(I)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_16

    :cond_14
    const/4 v0, 0x1

    goto :goto_17

    :cond_16
    const/4 v0, 0x0

    .line 51
    :goto_17
    return v0
.end method

.method private posicaoAtualEhValida(I)Z
    .registers 3
    .param p1, "posicaoAtual"    # I

    .line 167
    const/4 v0, -0x1

    if-eq p1, v0, :cond_5

    const/4 v0, 0x1

    goto :goto_6

    :cond_5
    const/4 v0, 0x0

    :goto_6
    return v0
.end method

.method private proximaPosicaoDoHeader(I)J
    .registers 4
    .param p1, "posicaoAtual"    # I

    .line 159
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->posicaoAtualEhValida(I)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 160
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->adapter:Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;

    invoke-interface {v0, p1}, Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;->getHeaderId(I)J

    move-result-wide v0

    return-wide v0

    .line 162
    :cond_d
    const-wide/16 v0, -0x1

    return-wide v0
.end method


# virtual methods
.method public getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$State;)V
    .registers 11
    .param p1, "outRect"    # Landroid/graphics/Rect;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/support/v7/widget/RecyclerView;
    .param p4, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .line 37
    invoke-virtual {p3, p2}, Landroid/support/v7/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result v3

    .line 38
    .local v3, "position":I
    const/4 v4, 0x0

    .line 40
    .local v4, "headerHeight":I
    const/4 v0, -0x1

    if-eq v3, v0, :cond_1e

    .line 41
    invoke-direct {p0, v3}, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->ehHeader(I)Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-direct {p0, v3}, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->mostraHeaderAcimaDoItem(I)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 43
    invoke-direct {p0, p3, v3}, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->getHeader(Landroid/support/v7/widget/RecyclerView;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    iget-object v5, v0, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 44
    .local v5, "header":Landroid/view/View;
    invoke-direct {p0, v5}, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->getHeaderHeightForLayout(Landroid/view/View;)I

    move-result v4

    .line 47
    .end local v5    # "header":Landroid/view/View;
    :cond_1e
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v4, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 48
    return-void
.end method

.method public onDrawOver(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$State;)V
    .registers 21
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "parent"    # Landroid/support/v7/widget/RecyclerView;
    .param p3, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .line 95
    invoke-virtual/range {p2 .. p2}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v6

    .line 96
    .local v6, "count":I
    const-wide/16 v7, -0x1

    .line 98
    .local v7, "previousHeaderId":J
    const/4 v9, 0x0

    .local v9, "layoutPos":I
    :goto_7
    if-ge v9, v6, :cond_69

    .line 99
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 100
    .local v10, "child":Landroid/view/View;
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/support/v7/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result v11

    .line 102
    .local v11, "adapterPos":I
    const/4 v0, -0x1

    if-eq v11, v0, :cond_65

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->ehHeader(I)Z

    move-result v0

    if-eqz v0, :cond_65

    .line 103
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->adapter:Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;

    invoke-interface {v0, v11}, Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;->getHeaderId(I)J

    move-result-wide v12

    .line 105
    .local v12, "headerId":J
    cmp-long v0, v12, v7

    if-eqz v0, :cond_65

    .line 106
    move-wide v7, v12

    .line 107
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v11}, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->getHeader(Landroid/support/v7/widget/RecyclerView;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    iget-object v14, v0, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 108
    .local v14, "header":Landroid/view/View;
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 110
    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v15

    .line 111
    .local v15, "left":I
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object v2, v10

    move-object v3, v14

    move v4, v11

    move v5, v9

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/feature/extrato/adapter/HeaderDecoration;->getHeaderTop(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/view/View;II)I

    move-result v16

    .line 112
    .local v16, "top":I
    int-to-float v0, v15

    move/from16 v1, v16

    int-to-float v1, v1

    move-object/from16 v2, p1

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 114
    int-to-float v0, v15

    invoke-virtual {v14, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 115
    move/from16 v0, v16

    int-to-float v0, v0

    invoke-virtual {v14, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 116
    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 117
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 98
    .end local v10    # "child":Landroid/view/View;
    .end local v11    # "adapterPos":I
    .end local v12    # "headerId":J
    .end local v14    # "header":Landroid/view/View;
    .end local v15    # "left":I
    .end local v16    # "top":I
    :cond_65
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_7

    .line 121
    .end local v9    # "layoutPos":I
    :cond_69
    return-void
.end method

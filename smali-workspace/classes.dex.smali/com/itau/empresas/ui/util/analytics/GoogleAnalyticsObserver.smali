.class public Lcom/itau/empresas/ui/util/analytics/GoogleAnalyticsObserver;
.super Ljava/lang/Object;
.source "GoogleAnalyticsObserver.java"

# interfaces
.implements Ljava/util/Observer;


# instance fields
.field private tracker:Lcom/google/android/gms/analytics/Tracker;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/analytics/Tracker;)V
    .registers 2
    .param p1, "tracker"    # Lcom/google/android/gms/analytics/Tracker;

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/itau/empresas/ui/util/analytics/GoogleAnalyticsObserver;->tracker:Lcom/google/android/gms/analytics/Tracker;

    .line 18
    return-void
.end method

.method private tratarEventoAnalytics(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V
    .registers 5
    .param p1, "e"    # Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 35
    new-instance v0, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    invoke-direct {v0}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;-><init>()V

    .line 36
    invoke-virtual {p1}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->getCategoria()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setCategory(Ljava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object v0

    .line 37
    invoke-virtual {p1}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->getAcao()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setAction(Ljava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object v0

    .line 38
    invoke-virtual {p1}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setLabel(Ljava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    move-result-object v2

    .line 40
    .local v2, "builder":Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;
    invoke-virtual {p1}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->getValor()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_2e

    .line 41
    invoke-virtual {p1}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->getValor()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->setValue(J)Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;

    .line 44
    :cond_2e
    iget-object v0, p0, Lcom/itau/empresas/ui/util/analytics/GoogleAnalyticsObserver;->tracker:Lcom/google/android/gms/analytics/Tracker;

    invoke-virtual {v2}, Lcom/google/android/gms/analytics/HitBuilders$EventBuilder;->build()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/Tracker;->send(Ljava/util/Map;)V

    .line 45
    return-void
.end method

.method private tratarEventoScreenView(Lcom/itau/empresas/ui/util/analytics/EventoScreenView;)V
    .registers 5
    .param p1, "e"    # Lcom/itau/empresas/ui/util/analytics/EventoScreenView;

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/ui/util/analytics/GoogleAnalyticsObserver;->tracker:Lcom/google/android/gms/analytics/Tracker;

    invoke-virtual {p1}, Lcom/itau/empresas/ui/util/analytics/EventoScreenView;->getScreenName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/Tracker;->setScreenName(Ljava/lang/String;)V

    .line 58
    new-instance v2, Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;

    invoke-direct {v2}, Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;-><init>()V

    .line 59
    .local v2, "builder":Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;
    iget-object v0, p0, Lcom/itau/empresas/ui/util/analytics/GoogleAnalyticsObserver;->tracker:Lcom/google/android/gms/analytics/Tracker;

    invoke-virtual {v2}, Lcom/google/android/gms/analytics/HitBuilders$ScreenViewBuilder;->build()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/Tracker;->send(Ljava/util/Map;)V

    .line 60
    return-void
.end method

.method private tratarEventoTempoRequest(Lcom/itau/empresas/ui/util/analytics/EventoTempoRequest;)V
    .registers 6
    .param p1, "e"    # Lcom/itau/empresas/ui/util/analytics/EventoTempoRequest;

    .line 48
    new-instance v0, Lcom/google/android/gms/analytics/HitBuilders$TimingBuilder;

    invoke-direct {v0}, Lcom/google/android/gms/analytics/HitBuilders$TimingBuilder;-><init>()V

    .line 49
    invoke-virtual {p1}, Lcom/itau/empresas/ui/util/analytics/EventoTempoRequest;->getCategoria()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/HitBuilders$TimingBuilder;->setCategory(Ljava/lang/String;)Lcom/google/android/gms/analytics/HitBuilders$TimingBuilder;

    move-result-object v0

    .line 50
    invoke-virtual {p1}, Lcom/itau/empresas/ui/util/analytics/EventoTempoRequest;->getTempo()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/analytics/HitBuilders$TimingBuilder;->setValue(J)Lcom/google/android/gms/analytics/HitBuilders$TimingBuilder;

    move-result-object v3

    .line 52
    .local v3, "builder":Lcom/google/android/gms/analytics/HitBuilders$TimingBuilder;
    iget-object v0, p0, Lcom/itau/empresas/ui/util/analytics/GoogleAnalyticsObserver;->tracker:Lcom/google/android/gms/analytics/Tracker;

    invoke-virtual {v3}, Lcom/google/android/gms/analytics/HitBuilders$TimingBuilder;->build()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/Tracker;->send(Ljava/util/Map;)V

    .line 53
    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .registers 5
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "o"    # Ljava/lang/Object;

    .line 22
    instance-of v0, p2, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    if-eqz v0, :cond_b

    .line 23
    move-object v1, p2

    check-cast v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 24
    .local v1, "e":Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;
    invoke-direct {p0, v1}, Lcom/itau/empresas/ui/util/analytics/GoogleAnalyticsObserver;->tratarEventoAnalytics(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 25
    .end local v1    # "e":Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;
    goto :goto_20

    :cond_b
    instance-of v0, p2, Lcom/itau/empresas/ui/util/analytics/EventoTempoRequest;

    if-eqz v0, :cond_16

    .line 26
    move-object v1, p2

    check-cast v1, Lcom/itau/empresas/ui/util/analytics/EventoTempoRequest;

    .line 27
    .local v1, "e":Lcom/itau/empresas/ui/util/analytics/EventoTempoRequest;
    invoke-direct {p0, v1}, Lcom/itau/empresas/ui/util/analytics/GoogleAnalyticsObserver;->tratarEventoTempoRequest(Lcom/itau/empresas/ui/util/analytics/EventoTempoRequest;)V

    .line 28
    .end local v1    # "e":Lcom/itau/empresas/ui/util/analytics/EventoTempoRequest;
    goto :goto_20

    :cond_16
    instance-of v0, p2, Lcom/itau/empresas/ui/util/analytics/EventoScreenView;

    if-eqz v0, :cond_20

    .line 29
    move-object v1, p2

    check-cast v1, Lcom/itau/empresas/ui/util/analytics/EventoScreenView;

    .line 30
    .local v1, "e":Lcom/itau/empresas/ui/util/analytics/EventoScreenView;
    invoke-direct {p0, v1}, Lcom/itau/empresas/ui/util/analytics/GoogleAnalyticsObserver;->tratarEventoScreenView(Lcom/itau/empresas/ui/util/analytics/EventoScreenView;)V

    .line 32
    .end local v1    # "e":Lcom/itau/empresas/ui/util/analytics/EventoScreenView;
    :cond_20
    :goto_20
    return-void
.end method

.class public Lcom/itau/empresas/ui/util/analytics/TrackerEvento;
.super Ljava/util/Observable;
.source "TrackerEvento.java"


# instance fields
.field monitores:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 19
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 17
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->monitores:Ljava/util/Map;

    .line 20
    return-void
.end method


# virtual methods
.method public enviarHitEvento(Landroid/content/Context;Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "evento"    # Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;

    .line 23
    invoke-virtual {p2, p1}, Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;->toEventoGA(Landroid/content/Context;)Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 24
    return-void
.end method

.method public enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V
    .registers 3
    .param p1, "evento"    # Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 27
    invoke-virtual {p0}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->setChanged()V

    .line 28
    invoke-virtual {p1}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->ajustarFormatacao()Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->notifyObservers(Ljava/lang/Object;)V

    .line 29
    return-void
.end method

.method public enviarScreenView(Lcom/itau/empresas/ui/util/analytics/EventoScreenView;)V
    .registers 2
    .param p1, "e"    # Lcom/itau/empresas/ui/util/analytics/EventoScreenView;

    .line 32
    invoke-virtual {p0}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->setChanged()V

    .line 33
    invoke-virtual {p0, p1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->notifyObservers(Ljava/lang/Object;)V

    .line 34
    return-void
.end method

.method public fimMonitorarTempo(Ljava/lang/String;)V
    .registers 10
    .param p1, "opkey"    # Ljava/lang/String;

    .line 43
    move-object v5, p0

    monitor-enter v5

    .line 44
    :try_start_2
    iget-object v0, p0, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->monitores:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 45
    iget-object v0, p0, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->monitores:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/lang/Long;

    .line 46
    .local v6, "tempoInicial":Ljava/lang/Long;
    invoke-virtual {p0}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->setChanged()V

    .line 47
    new-instance v0, Lcom/itau/empresas/ui/util/analytics/EventoTempoRequest;

    .line 48
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    sub-long/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/itau/empresas/ui/util/analytics/EventoTempoRequest;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    .line 47
    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->notifyObservers(Ljava/lang/Object;)V
    :try_end_2b
    .catchall {:try_start_2 .. :try_end_2b} :catchall_2d

    .line 50
    .end local v6    # "tempoInicial":Ljava/lang/Long;
    :cond_2b
    monitor-exit v5

    goto :goto_30

    :catchall_2d
    move-exception v7

    monitor-exit v5

    throw v7

    .line 51
    :goto_30
    return-void
.end method

.method public inicioMonitorarTempo(Ljava/lang/String;)V
    .registers 7
    .param p1, "opkey"    # Ljava/lang/String;

    .line 37
    move-object v3, p0

    monitor-enter v3

    .line 38
    :try_start_2
    iget-object v0, p0, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->monitores:Ljava/util/Map;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_f
    .catchall {:try_start_2 .. :try_end_f} :catchall_11

    .line 39
    monitor-exit v3

    goto :goto_14

    :catchall_11
    move-exception v4

    monitor-exit v3

    throw v4

    .line 40
    :goto_14
    return-void
.end method

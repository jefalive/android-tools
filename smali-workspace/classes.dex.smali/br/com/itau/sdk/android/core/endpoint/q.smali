.class public final Lbr/com/itau/sdk/android/core/endpoint/q;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/CountDownLatch;

.field private static final b:[B

.field private static c:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 26
    const/16 v0, 0xc1

    new-array v0, v0, [B

    fill-array-data v0, :array_16

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v0, 0xa9

    sput v0, Lbr/com/itau/sdk/android/core/endpoint/q;->c:I

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/q;->a:Ljava/util/concurrent/CountDownLatch;

    return-void

    :array_16
    .array-data 1
        0x7at
        0xct
        0x1bt
        0x6et
        -0x35t
        -0x3t
        0x12t
        -0xdt
        0x1t
        -0x13t
        0x17t
        -0x13t
        0x10t
        0x1t
        -0x16t
        0xbt
        0x2t
        -0x14t
        0x16t
        -0x7t
        -0x7t
        0x6t
        -0xdt
        -0x5t
        0x6t
        -0x16t
        -0xat
        -0xct
        0x51t
        -0x56t
        0x11t
        -0x13t
        0xct
        -0x3t
        -0x22t
        -0x12t
        0x42t
        -0x37t
        -0xet
        0x0t
        0x3dt
        -0x3dt
        -0xdt
        0x11t
        -0x16t
        0x45t
        -0x47t
        0xdt
        -0x9t
        0x3bt
        -0x35t
        -0xft
        0x8t
        -0x10t
        0x1t
        0x4t
        0x3t
        0x34t
        -0x37t
        -0xet
        -0x5t
        0xbt
        0x35t
        -0x18t
        0x1t
        -0x15t
        0x11t
        -0x20t
        0x12t
        0x4t
        -0x16t
        -0x27t
        -0x8t
        0xdt
        -0xft
        -0x2t
        -0x5t
        0x3t
        -0x6t
        0xdt
        -0x1t
        0x42t
        -0x58t
        0xft
        -0xft
        -0x3t
        0x8t
        -0x8t
        -0x1t
        0x4ct
        -0x45t
        -0xet
        -0x1t
        -0x8t
        0x0t
        0x1t
        0x1t
        0x4at
        -0x56t
        0xat
        -0xct
        0xbt
        0x2t
        -0x5t
        -0x2et
        -0x1t
        -0x8t
        0x0t
        0x1t
        0x1t
        0x5t
        0x43t
        -0x46t
        -0x3t
        0x43t
        -0x58t
        0xft
        -0xft
        -0x3t
        -0x72t
        0x72t
        0x4dt
        -0x46t
        -0xdt
        0x4dt
        -0x56t
        0x9t
        -0x9t
        -0x1t
        0x33t
        0x18t
        -0xft
        0x8t
        -0x10t
        0x1t
        0x4t
        0x3t
        0x34t
        -0x3dt
        -0x7t
        -0x8t
        0xdt
        -0xbt
        -0x8t
        0x44t
        -0x35t
        -0x4t
        -0x13t
        0x9t
        -0x8t
        -0x1t
        0x3et
        -0x2at
        0xbt
        0x2t
        -0x14t
        -0x12t
        0x42t
        -0x37t
        -0xet
        0x0t
        0x3dt
        -0x3dt
        -0xdt
        0x11t
        -0x16t
        0x45t
        -0x47t
        0xdt
        -0x9t
        0x3bt
        -0x35t
        -0xft
        0x8t
        -0x10t
        0x1t
        0x4t
        0x3t
        0x34t
        -0x37t
        -0xet
        -0x5t
        0xbt
        0x35t
        -0x1ct
        0x5t
        -0x14t
        0x9t
        -0x19t
        0x12t
        0x4t
        -0x16t
        -0x1et
    .end array-data
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    sget-object v4, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    add-int/lit8 p0, p0, 0x1

    add-int/lit8 p1, p1, 0x41

    const/4 v5, 0x0

    new-instance v0, Ljava/lang/String;

    rsub-int p2, p2, 0xc1

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v4, :cond_19

    move v2, p1

    move v3, p2

    :goto_13
    add-int/lit8 p2, p2, 0x1

    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p1, v2, -0x2

    :cond_19
    int-to-byte v2, p1

    aput-byte v2, v1, v5

    move v2, v5

    add-int/lit8 v5, v5, 0x1

    if-ne v2, p0, :cond_26

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_26
    move v2, p1

    aget-byte v3, v4, p2

    goto :goto_13
.end method

.method static synthetic a(I)V
    .registers 1

    invoke-static {p0}, Lbr/com/itau/sdk/android/core/endpoint/q;->b(I)V

    return-void
.end method

.method static synthetic a(Landroid/app/Activity;Lbr/com/itau/sdk/android/core/model/VersionControlDTO;Landroid/content/DialogInterface;I)V
    .registers 4

    invoke-static {p0, p1, p2, p3}, Lbr/com/itau/sdk/android/core/endpoint/q;->d(Landroid/app/Activity;Lbr/com/itau/sdk/android/core/model/VersionControlDTO;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method static synthetic a(Landroid/content/DialogInterface;I)V
    .registers 2

    invoke-static {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/q;->b(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method private static a(Lbr/com/itau/sdk/android/core/login/model/a;)V
    .registers 13

    .line 117
    if-nez p0, :cond_3

    .line 118
    return-void

    .line 120
    :cond_3
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/login/model/a;->d()Lbr/com/itau/sdk/android/core/login/model/a$a;

    move-result-object v5

    .line 121
    new-instance v6, Lbr/com/itau/security/securestorage/SecureStorage;

    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v6, v0}, Lbr/com/itau/security/securestorage/SecureStorage;-><init>(Landroid/content/Context;)V

    .line 123
    sget-object v0, Lbr/com/itau/sdk/android/core/login/model/a$a;->b:Lbr/com/itau/sdk/android/core/login/model/a$a;

    invoke-virtual {v0, v5}, Lbr/com/itau/sdk/android/core/login/model/a$a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_72

    .line 124
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/login/model/a;->c()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v2, 0x27

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v3, 0x31

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v4, 0x27

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/q;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_52

    .line 125
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/login/model/a;->c()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v2, 0x8

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/4 v3, 0x2

    aget-byte v2, v2, v3

    or-int/lit16 v3, v2, 0x84

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/q;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    goto :goto_5c

    :cond_52
    const/4 v0, 0x1

    new-array v7, v0, [Ljava/lang/String;

    .line 126
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/login/model/a;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, v7, v1

    .line 127
    :goto_5c
    move-object v8, v7

    array-length v9, v8

    const/4 v10, 0x0

    :goto_5f
    if-ge v10, v9, :cond_71

    aget-object v11, v8, v10

    .line 128
    .line 129
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 130
    invoke-interface {v0, v11}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 131
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 127
    add-int/lit8 v10, v10, 0x1

    goto :goto_5f

    .line 133
    :cond_71
    goto :goto_8d

    :cond_72
    sget-object v0, Lbr/com/itau/sdk/android/core/login/model/a$a;->c:Lbr/com/itau/sdk/android/core/login/model/a$a;

    invoke-virtual {v0, v5}, Lbr/com/itau/sdk/android/core/login/model/a$a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8d

    .line 134
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 135
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/login/model/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/login/model/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 136
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 138
    :cond_8d
    :goto_8d
    return-void
.end method

.method private static a(Lbr/com/itau/sdk/android/core/model/RemoteBundleDTO;)V
    .registers 6

    .line 40
    if-nez p0, :cond_3

    return-void

    .line 42
    :cond_3
    new-instance v4, Lbr/com/itau/security/securestorage/SecureStorage;

    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v4, v0}, Lbr/com/itau/security/securestorage/SecureStorage;-><init>(Landroid/content/Context;)V

    .line 43
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const/16 v1, 0x24

    const/16 v2, 0x21

    const/16 v3, 0x25

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/q;->a(III)Ljava/lang/String;

    move-result-object v1

    .line 44
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/RemoteBundleDTO;->getHash()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const/16 v1, 0x24

    const/16 v2, 0x21

    const/16 v3, 0x9e

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/q;->a(III)Ljava/lang/String;

    move-result-object v1

    .line 45
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/RemoteBundleDTO;->getData()Lcom/google/gson/JsonObject;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/gson/JsonObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 46
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 47
    return-void
.end method

.method static a(Lbr/com/itau/sdk/android/core/model/SdkDataDTO;)V
    .registers 2

    .line 32
    if-nez p0, :cond_3

    return-void

    .line 34
    :cond_3
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/SdkDataDTO;->getVersionControl()Lbr/com/itau/sdk/android/core/model/VersionControlDTO;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/endpoint/q;->a(Lbr/com/itau/sdk/android/core/model/VersionControlDTO;)V

    .line 35
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/SdkDataDTO;->getRemoteBundle()Lbr/com/itau/sdk/android/core/model/RemoteBundleDTO;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/endpoint/q;->a(Lbr/com/itau/sdk/android/core/model/RemoteBundleDTO;)V

    .line 36
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/SdkDataDTO;->getKeyAccess()Lbr/com/itau/sdk/android/core/login/model/a;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/endpoint/q;->a(Lbr/com/itau/sdk/android/core/login/model/a;)V

    .line 37
    return-void
.end method

.method private static a(Lbr/com/itau/sdk/android/core/model/VersionControlDTO;)V
    .registers 12

    .line 51
    if-nez p0, :cond_3

    return-void

    .line 58
    :cond_3
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->currentActivity()Landroid/app/Activity;

    move-result-object v9

    .line 60
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/x;->a:[I

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/VersionControlDTO;->getType()Lbr/com/itau/sdk/android/core/model/VersionControlType;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/VersionControlType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_fa

    goto/16 :goto_7d

    .line 62
    :pswitch_18
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v1, 0x8

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v2, 0x26

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v3, 0x8

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/q;->a(III)Ljava/lang/String;

    move-result-object v5

    .line 63
    invoke-static {p0}, Lbr/com/itau/sdk/android/core/endpoint/r;->a(Lbr/com/itau/sdk/android/core/model/VersionControlDTO;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v6

    .line 64
    const/4 v7, 0x0

    .line 65
    const/4 v8, 0x0

    .line 66
    goto :goto_81

    .line 68
    :pswitch_36
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v1, 0x34

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v2, 0x27

    aget-byte v1, v1, v2

    or-int/lit16 v2, v1, 0xbd

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/q;->a(III)Ljava/lang/String;

    move-result-object v5

    .line 69
    invoke-static {v9, p0}, Lbr/com/itau/sdk/android/core/endpoint/s;->a(Landroid/app/Activity;Lbr/com/itau/sdk/android/core/model/VersionControlDTO;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v6

    .line 71
    const/4 v7, 0x0

    .line 72
    const/4 v8, 0x0

    .line 73
    goto :goto_81

    .line 75
    :pswitch_4f
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v1, 0x34

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v2, 0x27

    aget-byte v1, v1, v2

    or-int/lit16 v2, v1, 0xbd

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/q;->a(III)Ljava/lang/String;

    move-result-object v5

    .line 76
    invoke-static {v9, p0}, Lbr/com/itau/sdk/android/core/endpoint/t;->a(Landroid/app/Activity;Lbr/com/itau/sdk/android/core/model/VersionControlDTO;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v6

    .line 78
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v1, 0x7e

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/4 v2, 0x1

    aget-byte v1, v1, v2

    sget v2, Lbr/com/itau/sdk/android/core/endpoint/q;->c:I

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/q;->a(III)Ljava/lang/String;

    move-result-object v7

    .line 79
    invoke-static {}, Lbr/com/itau/sdk/android/core/endpoint/u;->a()Landroid/content/DialogInterface$OnClickListener;

    move-result-object v8

    .line 81
    goto :goto_81

    .line 83
    :goto_7d
    const/4 v5, 0x0

    .line 84
    const/4 v6, 0x0

    .line 85
    const/4 v7, 0x0

    .line 86
    const/4 v8, 0x0

    .line 89
    :goto_81
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->currentActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {p0, v5, v6, v7, v8}, Lbr/com/itau/sdk/android/core/endpoint/v;->a(Lbr/com/itau/sdk/android/core/model/VersionControlDTO;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 104
    :try_start_8c
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/q;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_91
    .catch Ljava/lang/InterruptedException; {:try_start_8c .. :try_end_91} :catch_92

    .line 107
    goto :goto_ac

    .line 105
    :catch_92
    move-exception v10

    .line 106
    new-instance v0, Ljava/lang/RuntimeException;

    sget v1, Lbr/com/itau/sdk/android/core/endpoint/q;->c:I

    and-int/lit8 v1, v1, 0x77

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v3, 0x34

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/q;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 109
    :goto_ac
    sget-object v0, Lbr/com/itau/sdk/android/core/model/VersionControlType;->REQUIRED:Lbr/com/itau/sdk/android/core/model/VersionControlType;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/VersionControlDTO;->getType()Lbr/com/itau/sdk/android/core/model/VersionControlType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/model/VersionControlType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c4

    sget-object v0, Lbr/com/itau/sdk/android/core/model/VersionControlType;->UNAVAILABLE:Lbr/com/itau/sdk/android/core/model/VersionControlType;

    .line 110
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/VersionControlDTO;->getType()Lbr/com/itau/sdk/android/core/model/VersionControlType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/model/VersionControlType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f8

    .line 111
    :cond_c4
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/i;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/4 v3, 0x2

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v4, 0x10

    aget-byte v3, v3, v4

    const/16 v4, 0x59

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core/endpoint/q;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 112
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/VersionControlDTO;->getType()Lbr/com/itau/sdk/android/core/model/VersionControlType;

    move-result-object v2

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/model/VersionControlType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/sdk/android/core/exception/i;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114
    :cond_f8
    return-void

    nop

    :pswitch_data_fa
    .packed-switch 0x1
        :pswitch_18
        :pswitch_36
        :pswitch_4f
    .end packed-switch
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core/model/VersionControlDTO;Landroid/content/DialogInterface;I)V
    .registers 3

    invoke-static {p0, p1, p2}, Lbr/com/itau/sdk/android/core/endpoint/q;->b(Lbr/com/itau/sdk/android/core/model/VersionControlDTO;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core/model/VersionControlDTO;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)V
    .registers 5

    invoke-static {p0, p1, p2, p3, p4}, Lbr/com/itau/sdk/android/core/endpoint/q;->b(Lbr/com/itau/sdk/android/core/model/VersionControlDTO;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.method private static synthetic b(I)V
    .registers 1

    .line 143
    packed-switch p0, :pswitch_data_8

    goto :goto_6

    .line 145
    :pswitch_4
    goto :goto_6

    .line 147
    :pswitch_5
    nop

    .line 149
    .line 153
    :goto_6
    :pswitch_6
    return-void

    nop

    :pswitch_data_8
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method static synthetic b(Landroid/app/Activity;Lbr/com/itau/sdk/android/core/model/VersionControlDTO;Landroid/content/DialogInterface;I)V
    .registers 4

    invoke-static {p0, p1, p2, p3}, Lbr/com/itau/sdk/android/core/endpoint/q;->c(Landroid/app/Activity;Lbr/com/itau/sdk/android/core/model/VersionControlDTO;Landroid/content/DialogInterface;I)V

    return-void
.end method

.method private static synthetic b(Landroid/content/DialogInterface;I)V
    .registers 2

    .line 80
    return-void
.end method

.method private static b(Lbr/com/itau/sdk/android/core/model/VersionControlDTO;)V
    .registers 7

    .line 141
    new-instance v4, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;

    invoke-direct {v4}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;-><init>()V

    .line 142
    invoke-static {}, Lbr/com/itau/sdk/android/core/endpoint/w;->a()Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$OnTokenAction;

    move-result-object v0

    invoke-virtual {v4, v0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->setListener(Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$OnTokenAction;)V

    .line 155
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/VersionControlDTO;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->loadUrl(Ljava/lang/String;)V

    .line 156
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->currentActivity()Landroid/app/Activity;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/support/v4/app/FragmentActivity;

    .line 157
    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v2, 0x2f

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v3, 0x3e

    aget-byte v2, v2, v3

    add-int/lit8 v2, v2, 0x1

    const/16 v3, 0xb5

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/q;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 158
    return-void
.end method

.method private static synthetic b(Lbr/com/itau/sdk/android/core/model/VersionControlDTO;Landroid/content/DialogInterface;I)V
    .registers 3

    .line 63
    invoke-static {p0}, Lbr/com/itau/sdk/android/core/endpoint/q;->b(Lbr/com/itau/sdk/android/core/model/VersionControlDTO;)V

    return-void
.end method

.method private static synthetic b(Lbr/com/itau/sdk/android/core/model/VersionControlDTO;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)V
    .registers 12

    .line 90
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/VersionControlDTO;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_20

    .line 91
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getSdkCustomUi()Lbr/com/itau/sdk/android/core/SDKCustomUi;

    move-result-object v0

    .line 92
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/VersionControlDTO;->getMessage()Ljava/lang/String;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    sget-object v6, Lbr/com/itau/sdk/android/core/endpoint/q;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-interface/range {v0 .. v6}, Lbr/com/itau/sdk/android/core/SDKCustomUi;->showVersionDialog(Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/util/concurrent/CountDownLatch;)V

    goto :goto_25

    .line 99
    :cond_20
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-interface {p2, v0, v1}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    .line 101
    :goto_25
    return-void
.end method

.method private static synthetic c(Landroid/app/Activity;Lbr/com/itau/sdk/android/core/model/VersionControlDTO;Landroid/content/DialogInterface;I)V
    .registers 9

    .line 76
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v2, 0xbc

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v3, 0x43

    aget-byte v2, v2, v3

    neg-int v2, v2

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v4, 0x97

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/q;->a(III)Ljava/lang/String;

    move-result-object v1

    .line 77
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/VersionControlDTO;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 76
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private static synthetic d(Landroid/app/Activity;Lbr/com/itau/sdk/android/core/model/VersionControlDTO;Landroid/content/DialogInterface;I)V
    .registers 9

    .line 69
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v2, 0xbc

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v3, 0x43

    aget-byte v2, v2, v3

    neg-int v2, v2

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/q;->b:[B

    const/16 v4, 0x97

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/q;->a(III)Ljava/lang/String;

    move-result-object v1

    .line 70
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/model/VersionControlDTO;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 69
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.class public final Lcom/itau/empresas/feature/home/view/CardContasView_;
.super Lcom/itau/empresas/feature/home/view/CardContasView;
.source "CardContasView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 44
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/view/CardContasView;-><init>(Landroid/content/Context;)V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->alreadyInflated_:Z

    .line 41
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 45
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/view/CardContasView_;->init_()V

    .line 46
    return-void
.end method

.method public static build(Landroid/content/Context;)Lcom/itau/empresas/feature/home/view/CardContasView;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 49
    new-instance v0, Lcom/itau/empresas/feature/home/view/CardContasView_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/home/view/CardContasView_;-><init>(Landroid/content/Context;)V

    .line 50
    .local v0, "instance":Lcom/itau/empresas/feature/home/view/CardContasView_;
    invoke-virtual {v0}, Lcom/itau/empresas/feature/home/view/CardContasView_;->onFinishInflate()V

    .line 51
    return-object v0
.end method

.method private init_()V
    .registers 3

    .line 70
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 71
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 72
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->application:Lcom/itau/empresas/CustomApplication;

    .line 73
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardContasView_;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/PreferenciaUtils_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/PreferenciaUtils_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->preferenciaUtils:Lcom/itau/empresas/PreferenciaUtils;

    .line 74
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardContasView_;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/ExtratoController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/controller/ExtratoController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->controller:Lcom/itau/empresas/controller/ExtratoController;

    .line 75
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 76
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 2

    .line 62
    iget-boolean v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->alreadyInflated_:Z

    if-nez v0, :cond_c

    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->alreadyInflated_:Z

    .line 64
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 66
    :cond_c
    invoke-super {p0}, Lcom/itau/empresas/feature/home/view/CardContasView;->onFinishInflate()V

    .line 67
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 5
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 80
    const v0, 0x7f0e0645

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->textoExpandir:Landroid/widget/TextView;

    .line 81
    const v0, 0x7f0e0643

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->textoTituloCard:Landroid/widget/TextView;

    .line 82
    const v0, 0x7f0e0646

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/TextViewIcon;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->icExpandir:Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 83
    const v0, 0x7f0e0648

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->frameInterno:Landroid/widget/FrameLayout;

    .line 84
    const v0, 0x7f0e041c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->divisorCard:Landroid/view/View;

    .line 85
    const v0, 0x7f0e064a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->textoVerMaisEsquerdo:Landroid/widget/TextView;

    .line 86
    const v0, 0x7f0e064b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->textoVerMaisDireito:Landroid/widget/TextView;

    .line 87
    const v0, 0x7f0e0647

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->progressCard:Landroid/widget/ProgressBar;

    .line 88
    const v0, 0x7f0e02e8

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->textoValorSaldo:Landroid/widget/TextView;

    .line 89
    const v0, 0x7f0e02ed

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->textoValorLis:Landroid/widget/TextView;

    .line 90
    const v0, 0x7f0e02ef

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->textoValorAdicional:Landroid/widget/TextView;

    .line 91
    const v0, 0x7f0e02f4

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->textoValorUtilizado:Landroid/widget/TextView;

    .line 92
    const v0, 0x7f0e02f7

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->textoValorComLimite:Landroid/widget/TextView;

    .line 93
    const v0, 0x7f0e02f3

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->textoLisTotal:Landroid/widget/TextView;

    .line 94
    const v0, 0x7f0e02eb

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->containerLis:Landroid/widget/LinearLayout;

    .line 95
    const v0, 0x7f0e02f0

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->containerLisUtilizado:Landroid/widget/RelativeLayout;

    .line 96
    const v0, 0x7f0e02ee

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->containerLisAdicional:Landroid/widget/LinearLayout;

    .line 97
    const v0, 0x7f0e02f2

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->slideLisUtilizado:Landroid/widget/SeekBar;

    .line 98
    const v0, 0x7f0e02e9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->separadorSaldo:Landroid/view/View;

    .line 99
    const v0, 0x7f0e02ec

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->textoTituloLimite:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0e02f6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->textoTituloSaldoLimite:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0e02f5

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->containerTotalLis:Landroid/widget/LinearLayout;

    .line 102
    const v0, 0x7f0e05db

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->textoErro:Landroid/widget/TextView;

    .line 103
    const v0, 0x7f0e0644

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 105
    .local v2, "view_ll_expandir":Landroid/view/View;
    if-eqz v2, :cond_10a

    .line 106
    new-instance v0, Lcom/itau/empresas/feature/home/view/CardContasView_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/home/view/CardContasView_$1;-><init>(Lcom/itau/empresas/feature/home/view/CardContasView_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    :cond_10a
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->textoVerMaisEsquerdo:Landroid/widget/TextView;

    if-eqz v0, :cond_118

    .line 116
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->textoVerMaisEsquerdo:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/feature/home/view/CardContasView_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/home/view/CardContasView_$2;-><init>(Lcom/itau/empresas/feature/home/view/CardContasView_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    :cond_118
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->textoVerMaisDireito:Landroid/widget/TextView;

    if-eqz v0, :cond_126

    .line 126
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardContasView_;->textoVerMaisDireito:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/feature/home/view/CardContasView_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/home/view/CardContasView_$3;-><init>(Lcom/itau/empresas/feature/home/view/CardContasView_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    :cond_126
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardContasView_;->afterViews()V

    .line 136
    return-void
.end method

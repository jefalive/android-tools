.class public Lcom/google/android/gms/internal/zziz;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# instance fields
.field private zzMJ:J

.field private zzMK:J

.field private zzpV:Ljava/lang/Object;


# direct methods
.method public constructor <init>(J)V
    .registers 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/android/gms/internal/zziz;->zzMK:J

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zziz;->zzpV:Ljava/lang/Object;

    iput-wide p1, p0, Lcom/google/android/gms/internal/zziz;->zzMJ:J

    return-void
.end method


# virtual methods
.method public tryAcquire()Z
    .registers 9

    iget-object v4, p0, Lcom/google/android/gms/internal/zziz;->zzpV:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbG()Lcom/google/android/gms/internal/zzmq;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzmq;->elapsedRealtime()J

    move-result-wide v5

    iget-wide v0, p0, Lcom/google/android/gms/internal/zziz;->zzMK:J

    iget-wide v2, p0, Lcom/google/android/gms/internal/zziz;->zzMJ:J
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_1c

    add-long/2addr v0, v2

    cmp-long v0, v0, v5

    if-lez v0, :cond_17

    monitor-exit v4

    const/4 v0, 0x0

    return v0

    :cond_17
    :try_start_17
    iput-wide v5, p0, Lcom/google/android/gms/internal/zziz;->zzMK:J
    :try_end_19
    .catchall {:try_start_17 .. :try_end_19} :catchall_1c

    monitor-exit v4

    const/4 v0, 0x1

    return v0

    :catchall_1c
    move-exception v7

    monitor-exit v4

    throw v7
.end method

.class public Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamento;
.super Ljava/lang/Object;
.source "AtualizaValorPagamento.java"


# instance fields
.field private atualizaDadosActivityListener:Lcom/itau/empresas/ui/util/listener/AtualizaDadosActivityListener;

.field private dadosDarf:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;


# direct methods
.method public constructor <init>(Lcom/itau/empresas/ui/util/listener/AtualizaDadosActivityListener;)V
    .registers 3
    .param p1, "atualizaDadosActivityListener"    # Lcom/itau/empresas/ui/util/listener/AtualizaDadosActivityListener;

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;

    invoke-direct {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamento;->dadosDarf:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;

    .line 13
    iput-object p1, p0, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamento;->atualizaDadosActivityListener:Lcom/itau/empresas/ui/util/listener/AtualizaDadosActivityListener;

    .line 14
    return-void
.end method


# virtual methods
.method public notifyObserver()V
    .registers 3

    .line 32
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamento;->atualizaDadosActivityListener:Lcom/itau/empresas/ui/util/listener/AtualizaDadosActivityListener;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamento;->dadosDarf:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;

    invoke-interface {v0, v1}, Lcom/itau/empresas/ui/util/listener/AtualizaDadosActivityListener;->atualizaValorPagamento(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;)V

    .line 33
    return-void
.end method

.method public setValorJuros(Ljava/lang/String;)V
    .registers 3
    .param p1, "valorJuros"    # Ljava/lang/String;

    .line 17
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamento;->dadosDarf:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;->setValorJuros(Ljava/lang/String;)V

    .line 18
    invoke-virtual {p0}, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamento;->notifyObserver()V

    .line 19
    return-void
.end method

.method public setValorMulta(Ljava/lang/String;)V
    .registers 3
    .param p1, "valorMulta"    # Ljava/lang/String;

    .line 22
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamento;->dadosDarf:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;->setValorMulta(Ljava/lang/String;)V

    .line 23
    invoke-virtual {p0}, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamento;->notifyObserver()V

    .line 24
    return-void
.end method

.method public setValorPrincipal(Ljava/lang/String;)V
    .registers 3
    .param p1, "valorPrincipal"    # Ljava/lang/String;

    .line 27
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamento;->dadosDarf:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosDarf;->setValorPrincipal(Ljava/lang/String;)V

    .line 28
    invoke-virtual {p0}, Lcom/itau/empresas/ui/util/listener/AtualizaValorPagamento;->notifyObserver()V

    .line 29
    return-void
.end method

.class public Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;
.super Landroid/widget/LinearLayout;
.source "EmprestimosParceladosDetalhadoView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;,
        Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;
    }
.end annotation


# instance fields
.field protected application:Lcom/itau/empresas/CustomApplication;

.field public botaoContratarCreditoDetalhado:Landroid/widget/Button;

.field public botaoCreditoDetalhadoCompartilhar:Landroid/widget/Button;

.field public botaoSimular:Landroid/widget/Button;

.field private custoEfetivaTotal:Ljava/lang/String;

.field private custoEfetivoTotalIof:Ljava/lang/String;

.field private custoEfetivoTotalPrincipal:Ljava/lang/String;

.field private custoEfetivoTotalSeguro:Ljava/lang/String;

.field private custoEfetivoTotalTarifa:Ljava/lang/String;

.field private dataOperacao:Ljava/lang/String;

.field private dataPrimeiroPagamento:Ljava/lang/String;

.field private devedorSolidario:Z

.field private exibirSimulacao:Z

.field private fluxoPersonalizado:Z

.field private horarioOperacao:Ljava/lang/String;

.field llCondicoesGeraisBotao:Landroid/widget/LinearLayout;

.field llEmprestimosParceladosDetalhadosViewComprovanteLogo:Landroid/widget/LinearLayout;

.field llEmprestimosParceladosDetalhadosViewConfirmacao:Landroid/widget/LinearLayout;

.field llEmprestimosParceladosPersonalizarSimulacaoResultado:Landroid/widget/LinearLayout;

.field llViewEmprestimosParceladosDetalhadosValorEmprestimo:Landroid/widget/LinearLayout;

.field llViewSeguro:Landroid/widget/LinearLayout;

.field private maquinaDeEstado:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

.field private quantidadeDeParcelas:I

.field simulacaoVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

.field public svEmprestimosParceladosSimulacao:Landroid/widget/ScrollView;

.field private taxaCetAno:Ljava/lang/String;

.field private taxaCetMes:Ljava/lang/String;

.field private taxaJurosAno:Ljava/lang/String;

.field private taxaJurosMes:Ljava/lang/String;

.field private temSeguro:Z

.field textoAoConfirmarVoceDeclara:Landroid/widget/TextView;

.field textoCreditoDetalhadoParcelaComValoresSeguro:Landroid/widget/TextView;

.field textoCreditoEfetuadoEm:Landroid/widget/TextView;

.field textoCreditoSimulacaoCustoEfetivoTotal:Landroid/widget/TextView;

.field textoCreditoSimulacaoParcelas:Landroid/widget/TextView;

.field textoCreditoSimulacaoTaxaIof:Landroid/widget/TextView;

.field textoCreditoSimulacaoTaxaJuros:Landroid/widget/TextView;

.field textoCreditoSimulacaoTaxaSeguro:Landroid/widget/TextView;

.field textoCreditoSimulacaoTaxaTarifaContratacao:Landroid/widget/TextView;

.field textoCreditoSimulacaoTaxaValorCredito:Landroid/widget/TextView;

.field textoCreditoSimulacaoTaxaValorTotalFinanciado:Landroid/widget/TextView;

.field textoCreditoSimulacaoTipoCapitalizacao:Landroid/widget/TextView;

.field textoCreditoSimulacaoValorCredito:Landroid/widget/TextView;

.field textoCreditoSimulacaoValorIof:Landroid/widget/TextView;

.field textoCreditoSimulacaoValorSeguro:Landroid/widget/TextView;

.field textoCreditoSimulacaoValorTarifaContratacao:Landroid/widget/TextView;

.field textoCreditoSimulacaoValorTotalFinanciado:Landroid/widget/TextView;

.field textoCreditoSimulacaoVencimentoPrimeiraParcela:Landroid/widget/TextView;

.field private tipoCapitalizacao:Ljava/lang/String;

.field tipoExibicao:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

.field private valorEmprestimo:F

.field private valorIof:F

.field private valorParcela:F

.field private valorSeguro:F

.field private valorTarifaContratacao:F

.field private valorTotalFinanciamento:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 137
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 138
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 141
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 142
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;)Z
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    .line 38
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->temSeguro:Z

    return v0
.end method

.method private static formataFloatParaString(F)Ljava/lang/String;
    .registers 6
    .param p0, "input"    # F

    .line 492
    invoke-static {p0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v4

    .line 494
    .local v4, "valorComoString":Ljava/lang/String;
    const-string v0, ".0"

    invoke-virtual {v4, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 495
    const-string v0, "%d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    float-to-long v2, p0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 497
    :cond_1e
    const-string v0, "%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private formataTipoCapitalizacao(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "tipo"    # Ljava/lang/String;

    .line 590
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 591
    const v1, 0x7f070548

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 590
    return-object v0
.end method

.method private inicializaMaquinaDeEstado()Lcom/itau/empresas/ui/util/MaquinaDeEstado;
    .registers 7

    .line 244
    new-instance v0, Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    invoke-direct {v0}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;-><init>()V

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;->SIMULACAO_OFERTA_ESPECIAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x4

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoContratarCreditoDetalhado:Landroid/widget/Button;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->llCondicoesGeraisBotao:Landroid/widget/LinearLayout;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->svEmprestimosParceladosSimulacao:Landroid/widget/ScrollView;

    const/4 v5, 0x2

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoSimular:Landroid/widget/Button;

    const/4 v5, 0x3

    aput-object v4, v3, v5

    .line 247
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visible([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoContratarCreditoDetalhado:Landroid/widget/Button;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoCreditoDetalhadoCompartilhar:Landroid/widget/Button;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->llEmprestimosParceladosPersonalizarSimulacaoResultado:Landroid/widget/LinearLayout;

    const/4 v5, 0x2

    aput-object v4, v3, v5

    .line 249
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    new-instance v3, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$1;

    invoke-direct {v3, p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$1;-><init>(Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;)V

    .line 252
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState(Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 271
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 245
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;->SIMULACAO_OFERTA_PERSONALIZADA:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x3

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->svEmprestimosParceladosSimulacao:Landroid/widget/ScrollView;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoCreditoDetalhadoCompartilhar:Landroid/widget/Button;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->llEmprestimosParceladosPersonalizarSimulacaoResultado:Landroid/widget/LinearLayout;

    const/4 v5, 0x2

    aput-object v4, v3, v5

    .line 274
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visible([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    const/4 v3, 0x4

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoContratarCreditoDetalhado:Landroid/widget/Button;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoContratarCreditoDetalhado:Landroid/widget/Button;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->llCondicoesGeraisBotao:Landroid/widget/LinearLayout;

    const/4 v5, 0x2

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoSimular:Landroid/widget/Button;

    const/4 v5, 0x3

    aput-object v4, v3, v5

    .line 277
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 280
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 272
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;->COMPROVANTE_EFETIVACAO:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x4

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->llEmprestimosParceladosDetalhadosViewConfirmacao:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoCreditoDetalhadoCompartilhar:Landroid/widget/Button;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->svEmprestimosParceladosSimulacao:Landroid/widget/ScrollView;

    const/4 v5, 0x2

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->llEmprestimosParceladosDetalhadosViewComprovanteLogo:Landroid/widget/LinearLayout;

    const/4 v5, 0x3

    aput-object v4, v3, v5

    .line 283
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visible([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    const/4 v3, 0x5

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoContratarCreditoDetalhado:Landroid/widget/Button;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoCreditoDetalhadoCompartilhar:Landroid/widget/Button;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->llEmprestimosParceladosPersonalizarSimulacaoResultado:Landroid/widget/LinearLayout;

    const/4 v5, 0x2

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->llCondicoesGeraisBotao:Landroid/widget/LinearLayout;

    const/4 v5, 0x3

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoSimular:Landroid/widget/Button;

    const/4 v5, 0x4

    aput-object v4, v3, v5

    .line 287
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 291
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 281
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;->CONTRATADO_DETALHE:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x4

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoCreditoDetalhadoCompartilhar:Landroid/widget/Button;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->svEmprestimosParceladosSimulacao:Landroid/widget/ScrollView;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->svEmprestimosParceladosSimulacao:Landroid/widget/ScrollView;

    const/4 v5, 0x2

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->llEmprestimosParceladosDetalhadosViewComprovanteLogo:Landroid/widget/LinearLayout;

    const/4 v5, 0x3

    aput-object v4, v3, v5

    .line 294
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visible([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    const/4 v3, 0x4

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->llCondicoesGeraisBotao:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoCreditoDetalhadoCompartilhar:Landroid/widget/Button;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->llEmprestimosParceladosPersonalizarSimulacaoResultado:Landroid/widget/LinearLayout;

    const/4 v5, 0x2

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoContratarCreditoDetalhado:Landroid/widget/Button;

    const/4 v5, 0x3

    aput-object v4, v3, v5

    .line 298
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 301
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 292
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    .line 244
    return-object v0
.end method

.method private montaTextoAoContinuarVoceConcorda(ZZ)Ljava/lang/String;
    .registers 17
    .param p1, "temSeguro"    # Z
    .param p2, "temDevedorSolidario"    # Z

    .line 399
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 401
    .local v5, "texto":Landroid/text/SpannableStringBuilder;
    const-string v6, "Ao continuar voc\u00ea declara estar de acordo com o "

    .line 402
    .local v6, "textoParte1":Ljava/lang/String;
    const-string v7, "seguro"

    .line 403
    .local v7, "textoParte2":Ljava/lang/String;
    const-string v8, ", "

    .line 404
    .local v8, "textoParte3":Ljava/lang/String;
    const-string v9, "termos das condi\u00e7\u00f5es gerais"

    .line 405
    .local v9, "textoParte4":Ljava/lang/String;
    const-string v10, " e com a "

    .line 406
    .local v10, "textoParte5":Ljava/lang/String;
    const-string v11, "declara\u00e7\u00e3o de devedor solid\u00e1rio"

    .line 407
    .local v11, "textoParte6":Ljava/lang/String;
    const-string v12, "."

    .line 408
    .local v12, "textoParte7":Ljava/lang/String;
    const-string v13, " e "

    .line 410
    .local v13, "textoParte8":Ljava/lang/String;
    invoke-virtual {v5, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 411
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v2

    .line 410
    const/4 v3, 0x0

    const/16 v4, 0x12

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 414
    if-eqz p1, :cond_a8

    .line 415
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->devedorSolidario:Z

    if-eqz v0, :cond_6b

    .line 416
    invoke-virtual {v5, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 418
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v2

    .line 417
    const/4 v3, 0x0

    const/16 v4, 0x12

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 421
    invoke-virtual {v5, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 423
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v2

    .line 422
    const/4 v3, 0x0

    const/16 v4, 0x12

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 426
    invoke-virtual {v5, v9}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 428
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v2

    .line 427
    const/4 v3, 0x0

    const/16 v4, 0x12

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_bc

    .line 432
    :cond_6b
    invoke-virtual {v5, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 434
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v2

    .line 433
    const/4 v3, 0x0

    const/16 v4, 0x12

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 437
    invoke-virtual {v5, v13}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 439
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v2

    .line 438
    const/4 v3, 0x0

    const/16 v4, 0x12

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 442
    invoke-virtual {v5, v9}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 444
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v2

    .line 443
    const/4 v3, 0x0

    const/16 v4, 0x12

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_bc

    .line 450
    :cond_a8
    invoke-virtual {v5, v9}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 451
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v2

    .line 450
    const/4 v3, 0x0

    const/16 v4, 0x12

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 455
    :goto_bc
    if-eqz p2, :cond_fb

    .line 456
    invoke-virtual {v5, v10}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 457
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v2

    .line 456
    const/4 v3, 0x0

    const/16 v4, 0x12

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 460
    invoke-virtual {v5, v11}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 461
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v2

    .line 460
    const/4 v3, 0x0

    const/16 v4, 0x12

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 464
    invoke-virtual {v5, v12}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 465
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v2

    .line 464
    const/4 v3, 0x0

    const/16 v4, 0x12

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_10f

    .line 468
    :cond_fb
    invoke-virtual {v5, v12}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 469
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v2

    .line 468
    const/4 v3, 0x0

    const/16 v4, 0x12

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 473
    :goto_10f
    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private setaViews()V
    .registers 6

    .line 311
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->textoCreditoSimulacaoValorCredito:Landroid/widget/TextView;

    iget v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->valorEmprestimo:F

    .line 312
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    .line 311
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 314
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->textoCreditoSimulacaoTaxaValorCredito:Landroid/widget/TextView;

    .line 315
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->custoEfetivoTotalPrincipal:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorTotalFinanciado(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 314
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 318
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->textoCreditoSimulacaoValorTotalFinanciado:Landroid/widget/TextView;

    iget v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->valorTotalFinanciamento:F

    .line 319
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    .line 318
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 321
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->textoCreditoSimulacaoTaxaValorTotalFinanciado:Landroid/widget/TextView;

    .line 322
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->custoEfetivaTotal:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorTotalFinanciado(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 321
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 325
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->textoCreditoDetalhadoParcelaComValoresSeguro:Landroid/widget/TextView;

    .line 326
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-boolean v2, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->temSeguro:Z

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataTextoValorEParcelas(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v1

    .line 325
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 327
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->textoCreditoSimulacaoParcelas:Landroid/widget/TextView;

    .line 328
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->quantidadeDeParcelas:I

    iget v3, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->valorParcela:F

    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataParcelasEValor(Landroid/content/Context;IF)Ljava/lang/String;

    move-result-object v1

    .line 327
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 331
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->textoCreditoSimulacaoVencimentoPrimeiraParcela:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->dataPrimeiroPagamento:Ljava/lang/String;

    invoke-static {v1}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 334
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->textoCreditoSimulacaoTaxaJuros:Landroid/widget/TextView;

    .line 335
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->taxaJurosMes:Ljava/lang/String;

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->taxaJurosAno:Ljava/lang/String;

    .line 334
    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataTaxaJuros(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 337
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->textoCreditoSimulacaoCustoEfetivoTotal:Landroid/widget/TextView;

    .line 338
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->taxaCetMes:Ljava/lang/String;

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->taxaCetAno:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataTaxaCustoEfetivoTotal(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 337
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 340
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->textoCreditoSimulacaoValorIof:Landroid/widget/TextView;

    iget v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->valorIof:F

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 343
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->textoCreditoSimulacaoTaxaIof:Landroid/widget/TextView;

    .line 344
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->custoEfetivoTotalIof:Ljava/lang/String;

    .line 343
    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorTotalFinanciado(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 346
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->temSeguro:Z

    if-eqz v0, :cond_c4

    .line 347
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->llViewSeguro:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 348
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->textoCreditoSimulacaoValorSeguro:Landroid/widget/TextView;

    iget v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->valorSeguro:F

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 351
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->textoCreditoSimulacaoTaxaSeguro:Landroid/widget/TextView;

    .line 352
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->custoEfetivoTotalSeguro:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorTotalFinanciado(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 351
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_cb

    .line 356
    :cond_c4
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->llViewSeguro:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 359
    :goto_cb
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->textoAoConfirmarVoceDeclara:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->temSeguro:Z

    iget-boolean v2, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->devedorSolidario:Z

    .line 360
    invoke-direct {p0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->montaTextoAoContinuarVoceConcorda(ZZ)Ljava/lang/String;

    move-result-object v1

    .line 359
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 362
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->textoCreditoSimulacaoTipoCapitalizacao:Landroid/widget/TextView;

    .line 363
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->tipoCapitalizacao:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->formataTipoCapitalizacao(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 362
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 365
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->textoCreditoSimulacaoValorTarifaContratacao:Landroid/widget/TextView;

    iget v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->valorTarifaContratacao:F

    .line 366
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    .line 365
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 368
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->textoCreditoSimulacaoTaxaTarifaContratacao:Landroid/widget/TextView;

    .line 369
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->custoEfetivoTotalTarifa:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorTotalFinanciado(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 368
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 372
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->tipoExibicao:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    if-eqz v0, :cond_135

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->tipoExibicao:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;->SIMULACAO_COMPROVANTE_EFETIVACAO:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    .line 373
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_135

    .line 375
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f070542

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->dataOperacao:Ljava/lang/String;

    .line 376
    invoke-static {v2}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->horarioOperacao:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 375
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 379
    .local v4, "textoEfetivacao":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->textoCreditoEfetuadoEm:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 383
    .end local v4    # "textoEfetivacao":Ljava/lang/String;
    :cond_135
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->exibirSimulacao:Z

    if-nez v0, :cond_140

    .line 385
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->botaoSimular:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 391
    :cond_140
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->llViewEmprestimosParceladosDetalhadosValorEmprestimo:Landroid/widget/LinearLayout;

    .line 392
    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setAccessibilityLiveRegion(Landroid/view/View;I)V

    .line 394
    return-void
.end method


# virtual methods
.method aoClicarCondicoesGerais()V
    .registers 6

    .line 504
    const-string v2, ""

    .line 505
    .local v2, "nome":Ljava/lang/String;
    const-string v3, ""

    .line 506
    .local v3, "tipoDoc":Ljava/lang/String;
    const-string v4, ""

    .line 508
    .local v4, "numeroDoc":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v0

    if-eqz v0, :cond_2c

    .line 510
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getNomeOperador()Ljava/lang/String;

    move-result-object v2

    .line 511
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getTipoDocumento()Ljava/lang/String;

    move-result-object v3

    .line 512
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getNumeroDocumento()Ljava/lang/String;

    move-result-object v4

    .line 515
    :cond_2c
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$IntentBuilder_;

    move-result-object v0

    iget-boolean v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->temSeguro:Z

    .line 516
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$IntentBuilder_;->temSeguro(Z)Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$IntentBuilder_;

    move-result-object v0

    iget-boolean v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->devedorSolidario:Z

    .line 517
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$IntentBuilder_;->temDevedorSolidario(Z)Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$IntentBuilder_;

    move-result-object v0

    .line 518
    invoke-virtual {v0, v2}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$IntentBuilder_;->nome(Ljava/lang/String;)Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$IntentBuilder_;

    move-result-object v0

    .line 519
    invoke-virtual {v0, v3}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$IntentBuilder_;->tipoDoc(Ljava/lang/String;)Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$IntentBuilder_;

    move-result-object v0

    .line 520
    invoke-virtual {v0, v4}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$IntentBuilder_;->numeroDoc(Ljava/lang/String;)Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$IntentBuilder_;

    move-result-object v0

    .line 521
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/activity/CondicoesGeraisActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 522
    return-void
.end method

.method aoClicarEmCompartilhar()V
    .registers 2

    .line 547
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$2;-><init>(Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;)V

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->publicaEvento(Ljava/lang/Object;)V

    .line 549
    return-void
.end method

.method aoClicarEmContratar()V
    .registers 3

    .line 539
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->tipoExibicao:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;->SIMULACAO_OFERTA_ESPECIAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 541
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/Evento$EventoGiroEfetivacao;

    invoke-direct {v1}, Lcom/itau/empresas/Evento$EventoGiroEfetivacao;-><init>()V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 543
    :cond_16
    return-void
.end method

.method aoClicarEmSimular()V
    .registers 4

    .line 529
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/Evento$EventoTrocaTipoSimulacao;

    iget-boolean v2, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->fluxoPersonalizado:Z

    invoke-direct {v1, v2}, Lcom/itau/empresas/Evento$EventoTrocaTipoSimulacao;-><init>(Z)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 530
    return-void
.end method

.method aoInicalizar()V
    .registers 2

    .line 146
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->inicializaMaquinaDeEstado()Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->maquinaDeEstado:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    .line 147
    return-void
.end method

.method public bind(Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;)V
    .registers 4
    .param p1, "camposComprovanteVO"    # Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;

    .line 213
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getValorEmprestimo()J

    move-result-wide v0

    long-to-float v0, v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->valorEmprestimo:F

    .line 214
    .line 215
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getPercentualValorTotalFinanciado()F

    move-result v0

    .line 214
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->formataFloatParaString(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->custoEfetivaTotal:Ljava/lang/String;

    .line 216
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getValorTotalFinanciado()F

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->valorTotalFinanciamento:F

    .line 217
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getIndicadorSeguro()Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->temSeguro:Z

    .line 218
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getQuantidadeParcelas()I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->quantidadeDeParcelas:I

    .line 219
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getValorParcela()F

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->valorParcela:F

    .line 220
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getDataVencimentoOperacao()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->dataPrimeiroPagamento:Ljava/lang/String;

    .line 221
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getTaxaJurosEfetivo()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->taxaJurosMes:Ljava/lang/String;

    .line 222
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getTaxaJurosEfetivoAno()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->taxaJurosAno:Ljava/lang/String;

    .line 223
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getTaxaCetMes()F

    move-result v0

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->formataFloatParaString(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->taxaCetMes:Ljava/lang/String;

    .line 224
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getTaxaCetAno()F

    move-result v0

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->formataFloatParaString(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->taxaCetAno:Ljava/lang/String;

    .line 225
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getValorTarifaIof()F

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->valorIof:F

    .line 226
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getPercentualValorIof()F

    move-result v0

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->formataFloatParaString(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->custoEfetivoTotalIof:Ljava/lang/String;

    .line 227
    .line 228
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getPercentualValorCredito()F

    move-result v0

    .line 227
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->formataFloatParaString(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->custoEfetivoTotalPrincipal:Ljava/lang/String;

    .line 229
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getValorTarifaContratacao()F

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->valorTarifaContratacao:F

    .line 230
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getDescricaoAmortizacao()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7f

    .line 231
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getDescricaoAmortizacao()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->tipoCapitalizacao:Ljava/lang/String;

    .line 233
    :cond_7f
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getValorSeguro()F

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->valorSeguro:F

    .line 234
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getPertencualSeguro()F

    move-result v0

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->formataFloatParaString(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->custoEfetivoTotalSeguro:Ljava/lang/String;

    .line 235
    .line 236
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getPercentualValorTarifa()F

    move-result v0

    .line 235
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->formataFloatParaString(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->custoEfetivoTotalTarifa:Ljava/lang/String;

    .line 238
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->maquinaDeEstado:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;->CONTRATADO_DETALHE:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 239
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->setaViews()V

    .line 240
    return-void
.end method

.method public bind(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;ZZLcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;ZZ)V
    .registers 10
    .param p1, "simulacaoVO"    # Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;
    .param p2, "tipoExibicao"    # Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;
    .param p3, "comSeguro"    # Z
    .param p4, "temDevedorSolidario"    # Z
    .param p5, "ofertaEspecialAceitaVO"    # Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;
    .param p6, "fluxoPersonalizado"    # Z
    .param p7, "exibirSimulacao"    # Z

    .line 159
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->setSimulacaoVO(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;)V

    .line 160
    iput-boolean p7, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->exibirSimulacao:Z

    .line 161
    iput-boolean p6, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->fluxoPersonalizado:Z

    .line 162
    iput-boolean p3, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->temSeguro:Z

    .line 163
    iput-boolean p4, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->devedorSolidario:Z

    .line 164
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getValorEmprestimo()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->valorEmprestimo:F

    .line 165
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getCustoEfetivoTotal()F

    move-result v0

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->formataFloatParaString(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->custoEfetivaTotal:Ljava/lang/String;

    .line 166
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getValorTotalFinanciamento()F

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->valorTotalFinanciamento:F

    .line 167
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getQuantidadeParcelas()I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->quantidadeDeParcelas:I

    .line 168
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getValorParcela()F

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->valorParcela:F

    .line 169
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getDataPrimeiroPagamento()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->dataPrimeiroPagamento:Ljava/lang/String;

    .line 170
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getTaxaJurosMes()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->taxaJurosMes:Ljava/lang/String;

    .line 171
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getTaxaJurosAno()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->taxaJurosAno:Ljava/lang/String;

    .line 172
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getTaxaCetMes()F

    move-result v0

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->formataFloatParaString(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->taxaCetMes:Ljava/lang/String;

    .line 173
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getTaxaCetAno()F

    move-result v0

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->formataFloatParaString(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->taxaCetAno:Ljava/lang/String;

    .line 174
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getValorIof()F

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->valorIof:F

    .line 175
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getCustoEfetivoTotalIof()F

    move-result v0

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->formataFloatParaString(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->custoEfetivoTotalIof:Ljava/lang/String;

    .line 176
    .line 177
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getCustoEfetivoTotalPrincipal()F

    move-result v0

    .line 176
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->formataFloatParaString(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->custoEfetivoTotalPrincipal:Ljava/lang/String;

    .line 178
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getValorTarifaContratacao()F

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->valorTarifaContratacao:F

    .line 180
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getTipoCapitalizacao()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_87

    .line 181
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getTipoCapitalizacao()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->tipoCapitalizacao:Ljava/lang/String;

    .line 184
    :cond_87
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getDadosSeguro()Lcom/itau/empresas/feature/credito/giro/model/SeguroProdutosParceladoSaidaVO;

    move-result-object v0

    if-eqz v0, :cond_97

    .line 185
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getDadosSeguro()Lcom/itau/empresas/feature/credito/giro/model/SeguroProdutosParceladoSaidaVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/SeguroProdutosParceladoSaidaVO;->getValorSeguro()F

    move-result v0

    iput v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->valorSeguro:F

    .line 188
    :cond_97
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getCustoEfetivoTotalSeguro()F

    move-result v0

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->formataFloatParaString(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->custoEfetivoTotalSeguro:Ljava/lang/String;

    .line 189
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->getCustoEfetivoTotalTarifa()F

    move-result v0

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->formataFloatParaString(F)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->custoEfetivoTotalTarifa:Ljava/lang/String;

    .line 191
    iput-object p2, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->tipoExibicao:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    .line 193
    if-eqz p5, :cond_bb

    .line 195
    invoke-virtual {p5}, Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;->getHorarioOperacao()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->horarioOperacao:Ljava/lang/String;

    .line 196
    invoke-virtual {p5}, Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;->getDataOperacao()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->dataOperacao:Ljava/lang/String;

    .line 199
    :cond_bb
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->svEmprestimosParceladosSimulacao:Landroid/widget/ScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 201
    sget-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;->SIMULACAO_COMPROVANTE_EFETIVACAO:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    invoke-virtual {p2, v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d1

    .line 202
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->maquinaDeEstado:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;->COMPROVANTE_EFETIVACAO:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    goto :goto_f0

    .line 203
    :cond_d1
    sget-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;->SIMULACAO_OFERTA_ESPECIAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    invoke-virtual {p2, v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e1

    .line 204
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->maquinaDeEstado:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;->SIMULACAO_OFERTA_ESPECIAL:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    goto :goto_f0

    .line 205
    :cond_e1
    sget-object v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;->SIMULACAO_PERSONALIZADA:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;

    invoke-virtual {p2, v0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$EnumTipoExibicao;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f0

    .line 206
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->maquinaDeEstado:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;->SIMULACAO_OFERTA_PERSONALIZADA:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 208
    :cond_f0
    :goto_f0
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->setaViews()V

    .line 209
    return-void
.end method

.method public getSimulacaoVO()Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;
    .registers 2

    .line 552
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->simulacaoVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    return-object v0
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;)V
    .registers 4
    .param p1, "efetivacao"    # Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;

    .line 562
    .line 563
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;

    move-result-object v0

    .line 564
    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;->efetivacaoVO(Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;

    move-result-object v0

    .line 565
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->getSimulacaoVO()Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;->simulacaoVO(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;

    move-result-object v0

    iget-boolean v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->temSeguro:Z

    .line 566
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;->temSeguro(Z)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;

    move-result-object v0

    iget-boolean v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->devedorSolidario:Z

    .line 567
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;->devedorSolidario(Z)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;

    move-result-object v0

    .line 568
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 569
    return-void
.end method

.method public publicaEvento(Ljava/lang/Object;)V
    .registers 3
    .param p1, "evento"    # Ljava/lang/Object;

    .line 578
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 579
    return-void
.end method

.method public setSimulacaoVO(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;)V
    .registers 2
    .param p1, "simulacaoVO"    # Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    .line 556
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;->simulacaoVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    .line 557
    return-void
.end method

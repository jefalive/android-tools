.class public Lcom/facebook/internal/Utility$DialogFeatureConfig;
.super Ljava/lang/Object;
.source "Utility.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/internal/Utility;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DialogFeatureConfig"
.end annotation


# instance fields
.field private dialogName:Ljava/lang/String;

.field private fallbackUrl:Landroid/net/Uri;

.field private featureName:Ljava/lang/String;

.field private featureVersionSpec:[I


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;[I)V
    .registers 5
    .param p1, "dialogName"    # Ljava/lang/String;
    .param p2, "featureName"    # Ljava/lang/String;
    .param p3, "fallbackUrl"    # Landroid/net/Uri;
    .param p4, "featureVersionSpec"    # [I

    .line 283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 284
    iput-object p1, p0, Lcom/facebook/internal/Utility$DialogFeatureConfig;->dialogName:Ljava/lang/String;

    .line 285
    iput-object p2, p0, Lcom/facebook/internal/Utility$DialogFeatureConfig;->featureName:Ljava/lang/String;

    .line 286
    iput-object p3, p0, Lcom/facebook/internal/Utility$DialogFeatureConfig;->fallbackUrl:Landroid/net/Uri;

    .line 287
    iput-object p4, p0, Lcom/facebook/internal/Utility$DialogFeatureConfig;->featureVersionSpec:[I

    .line 288
    return-void
.end method

.method static synthetic access$400(Lorg/json/JSONObject;)Lcom/facebook/internal/Utility$DialogFeatureConfig;
    .registers 2
    .param p0, "x0"    # Lorg/json/JSONObject;

    .line 207
    invoke-static {p0}, Lcom/facebook/internal/Utility$DialogFeatureConfig;->parseDialogConfig(Lorg/json/JSONObject;)Lcom/facebook/internal/Utility$DialogFeatureConfig;

    move-result-object v0

    return-object v0
.end method

.method private static parseDialogConfig(Lorg/json/JSONObject;)Lcom/facebook/internal/Utility$DialogFeatureConfig;
    .registers 11
    .param p0, "dialogConfigJSON"    # Lorg/json/JSONObject;

    .line 209
    const-string v0, "name"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 210
    .local v2, "dialogNameWithFeature":Ljava/lang/String;
    invoke-static {v2}, Lcom/facebook/internal/Utility;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 211
    const/4 v0, 0x0

    return-object v0

    .line 214
    :cond_e
    const-string v0, "\\|"

    invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 216
    .local v3, "components":[Ljava/lang/String;
    array-length v0, v3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1a

    .line 219
    const/4 v0, 0x0

    return-object v0

    .line 222
    :cond_1a
    const/4 v0, 0x0

    aget-object v4, v3, v0

    .line 223
    .local v4, "dialogName":Ljava/lang/String;
    const/4 v0, 0x1

    aget-object v5, v3, v0

    .line 224
    .local v5, "featureName":Ljava/lang/String;
    invoke-static {v4}, Lcom/facebook/internal/Utility;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2c

    invoke-static {v5}, Lcom/facebook/internal/Utility;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 225
    :cond_2c
    const/4 v0, 0x0

    return-object v0

    .line 228
    :cond_2e
    const-string v0, "url"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 229
    .local v6, "urlString":Ljava/lang/String;
    const/4 v7, 0x0

    .line 230
    .local v7, "fallbackUri":Landroid/net/Uri;
    invoke-static {v6}, Lcom/facebook/internal/Utility;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3f

    .line 231
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 234
    :cond_3f
    const-string v0, "versions"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    .line 236
    .local v8, "versionsJSON":Lorg/json/JSONArray;
    invoke-static {v8}, Lcom/facebook/internal/Utility$DialogFeatureConfig;->parseVersionSpec(Lorg/json/JSONArray;)[I

    move-result-object v9

    .line 238
    .local v9, "featureVersionSpec":[I
    new-instance v0, Lcom/facebook/internal/Utility$DialogFeatureConfig;

    invoke-direct {v0, v4, v5, v7, v9}, Lcom/facebook/internal/Utility$DialogFeatureConfig;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;[I)V

    return-object v0
.end method

.method private static parseVersionSpec(Lorg/json/JSONArray;)[I
    .registers 8
    .param p0, "versionsJSON"    # Lorg/json/JSONArray;

    .line 246
    const/4 v1, 0x0

    .line 247
    .local v1, "versionSpec":[I
    if-eqz p0, :cond_2f

    .line 248
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v2

    .line 249
    .local v2, "numVersions":I
    new-array v1, v2, [I

    .line 250
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_a
    if-ge v3, v2, :cond_2f

    .line 252
    const/4 v0, -0x1

    invoke-virtual {p0, v3, v0}, Lorg/json/JSONArray;->optInt(II)I

    move-result v4

    .line 253
    .local v4, "version":I
    const/4 v0, -0x1

    if-ne v4, v0, :cond_2a

    .line 256
    invoke-virtual {p0, v3}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v5

    .line 257
    .local v5, "versionString":Ljava/lang/String;
    invoke-static {v5}, Lcom/facebook/internal/Utility;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2a

    .line 259
    :try_start_1e
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_21
    .catch Ljava/lang/NumberFormatException; {:try_start_1e .. :try_end_21} :catch_23

    move-result v4

    .line 263
    goto :goto_2a

    .line 260
    :catch_23
    move-exception v6

    .line 261
    .local v6, "nfe":Ljava/lang/NumberFormatException;
    const-string v0, "FacebookSDK"

    invoke-static {v0, v6}, Lcom/facebook/internal/Utility;->logd(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 262
    const/4 v4, -0x1

    .line 267
    .end local v5    # "versionString":Ljava/lang/String;
    .end local v6    # "nfe":Ljava/lang/NumberFormatException;
    :cond_2a
    :goto_2a
    aput v4, v1, v3

    .line 250
    .end local v4    # "version":I
    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    .line 271
    .end local v2    # "numVersions":I
    .end local v3    # "i":I
    :cond_2f
    return-object v1
.end method


# virtual methods
.method public getDialogName()Ljava/lang/String;
    .registers 2

    .line 291
    iget-object v0, p0, Lcom/facebook/internal/Utility$DialogFeatureConfig;->dialogName:Ljava/lang/String;

    return-object v0
.end method

.method public getFallbackUrl()Landroid/net/Uri;
    .registers 2

    .line 299
    iget-object v0, p0, Lcom/facebook/internal/Utility$DialogFeatureConfig;->fallbackUrl:Landroid/net/Uri;

    return-object v0
.end method

.method public getFeatureName()Ljava/lang/String;
    .registers 2

    .line 295
    iget-object v0, p0, Lcom/facebook/internal/Utility$DialogFeatureConfig;->featureName:Ljava/lang/String;

    return-object v0
.end method

.method public getVersionSpec()[I
    .registers 2

    .line 303
    iget-object v0, p0, Lcom/facebook/internal/Utility$DialogFeatureConfig;->featureVersionSpec:[I

    return-object v0
.end method

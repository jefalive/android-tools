.class public Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;
.super Ljava/lang/Object;
.source "RecargaOperadoraVO.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Ljava/io/Serializable;Ljava/lang/Comparable<Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;>;"
    }
.end annotation


# instance fields
.field private listaRegiao:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "lista_regiao"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;>;"
        }
    .end annotation
.end field

.field private nomeOperadora:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_operadora"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;)I
    .registers 5
    .param p1, "o"    # Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->nomeOperadora:Ljava/lang/String;

    iget-object v1, p1, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->nomeOperadora:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    .line 55
    .local v2, "n":I
    if-eqz v2, :cond_b

    .line 56
    return v2

    .line 58
    :cond_b
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->listaRegiao:Ljava/util/List;

    if-nez v0, :cond_11

    .line 59
    const/4 v0, 0x1

    return v0

    .line 61
    :cond_11
    iget-object v0, p1, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->listaRegiao:Ljava/util/List;

    if-nez v0, :cond_17

    .line 62
    const/4 v0, -0x1

    return v0

    .line 64
    :cond_17
    new-instance v0, Ljava/lang/Integer;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->listaRegiao:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    iget-object v1, p1, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->listaRegiao:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3

    .line 8
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->compareTo(Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1, "o"    # Ljava/lang/Object;

    .line 29
    if-ne p0, p1, :cond_4

    .line 30
    const/4 v0, 0x1

    return v0

    .line 32
    :cond_4
    if-eqz p1, :cond_10

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_12

    .line 33
    :cond_10
    const/4 v0, 0x0

    return v0

    .line 36
    :cond_12
    move-object v2, p1

    check-cast v2, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;

    .line 38
    .local v2, "that":Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->nomeOperadora:Ljava/lang/String;

    iget-object v1, v2, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->nomeOperadora:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getListaRegiao()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;>;"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->listaRegiao:Ljava/util/List;

    return-object v0
.end method

.method public getNomeOperadora()Ljava/lang/String;
    .registers 2

    .line 16
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->nomeOperadora:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .registers 2

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->nomeOperadora:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->nomeOperadora:Ljava/lang/String;

    return-object v0
.end method

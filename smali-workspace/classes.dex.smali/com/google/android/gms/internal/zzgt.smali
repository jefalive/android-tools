.class public Lcom/google/android/gms/internal/zzgt;
.super Lcom/google/android/gms/internal/zzgs;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# instance fields
.field private zzGv:Ljava/lang/Object;

.field private zzGw:Landroid/widget/PopupWindow;

.field private zzGx:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/zzif$zza;Lcom/google/android/gms/internal/zzjp;Lcom/google/android/gms/internal/zzgr$zza;)V
    .registers 6

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/internal/zzgs;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/zzif$zza;Lcom/google/android/gms/internal/zzjp;Lcom/google/android/gms/internal/zzgr$zza;)V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzgt;->zzGv:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzgt;->zzGx:Z

    return-void
.end method

.method private zzgj()V
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzgt;->zzGv:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_4
    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzgt;->zzGx:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/zzgt;->mContext:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/google/android/gms/internal/zzgt;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_19

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzgt;->zzGw:Landroid/widget/PopupWindow;

    :cond_19
    iget-object v0, p0, Lcom/google/android/gms/internal/zzgt;->zzGw:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_2d

    iget-object v0, p0, Lcom/google/android/gms/internal/zzgt;->zzGw:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2a

    iget-object v0, p0, Lcom/google/android/gms/internal/zzgt;->zzGw:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    :cond_2a
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzgt;->zzGw:Landroid/widget/PopupWindow;
    :try_end_2d
    .catchall {:try_start_4 .. :try_end_2d} :catchall_2f

    :cond_2d
    monitor-exit v1

    goto :goto_32

    :catchall_2f
    move-exception v2

    monitor-exit v1

    throw v2

    :goto_32
    return-void
.end method


# virtual methods
.method public cancel()V
    .registers 1

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzgt;->zzgj()V

    invoke-super {p0}, Lcom/google/android/gms/internal/zzgs;->cancel()V

    return-void
.end method

.method protected zzC(I)V
    .registers 2

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzgt;->zzgj()V

    invoke-super {p0, p1}, Lcom/google/android/gms/internal/zzgs;->zzC(I)V

    return-void
.end method

.method protected zzgi()V
    .registers 11

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/zzgt;->mContext:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/gms/internal/zzgt;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v5

    :cond_f
    if-eqz v5, :cond_17

    invoke-virtual {v5}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_18

    :cond_17
    return-void

    :cond_18
    iget-object v0, p0, Lcom/google/android/gms/internal/zzgt;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_23

    return-void

    :cond_23
    new-instance v6, Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzgt;->mContext:Landroid/content/Context;

    invoke-direct {v6, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzgt;->zzpD:Lcom/google/android/gms/internal/zzjp;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzjp;->getView()Landroid/view/View;

    move-result-object v0

    const/4 v1, -0x1

    const/4 v2, -0x1

    invoke-virtual {v6, v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;II)V

    iget-object v7, p0, Lcom/google/android/gms/internal/zzgt;->zzGv:Ljava/lang/Object;

    monitor-enter v7

    :try_start_42
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzgt;->zzGx:Z
    :try_end_44
    .catchall {:try_start_42 .. :try_end_44} :catchall_76

    if-eqz v0, :cond_48

    monitor-exit v7

    return-void

    :cond_48
    :try_start_48
    new-instance v0, Landroid/widget/PopupWindow;

    const/4 v1, 0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v0, v6, v1, v2, v3}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzgt;->zzGw:Landroid/widget/PopupWindow;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzgt;->zzGw:Landroid/widget/PopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzgt;->zzGw:Landroid/widget/PopupWindow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    const-string v0, "Displaying the 1x1 popup off the screen."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaI(Ljava/lang/String;)V
    :try_end_63
    .catchall {:try_start_48 .. :try_end_63} :catchall_76

    :try_start_63
    iget-object v0, p0, Lcom/google/android/gms/internal/zzgt;->zzGw:Landroid/widget/PopupWindow;

    invoke-virtual {v5}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, -0x1

    const/4 v4, -0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V
    :try_end_6f
    .catch Ljava/lang/Exception; {:try_start_63 .. :try_end_6f} :catch_70
    .catchall {:try_start_63 .. :try_end_6f} :catchall_76

    goto :goto_74

    :catch_70
    move-exception v8

    const/4 v0, 0x0

    :try_start_72
    iput-object v0, p0, Lcom/google/android/gms/internal/zzgt;->zzGw:Landroid/widget/PopupWindow;
    :try_end_74
    .catchall {:try_start_72 .. :try_end_74} :catchall_76

    :goto_74
    monitor-exit v7

    goto :goto_79

    :catchall_76
    move-exception v9

    monitor-exit v7

    throw v9

    :goto_79
    return-void
.end method

.class public Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "SaldoDetalhadoActivity.java"

# interfaces
.implements Lcom/itau/empresas/api/ApiConsumidor;


# instance fields
.field containerLis:Landroid/widget/LinearLayout;

.field containerLisAdicional:Landroid/widget/LinearLayout;

.field containerLisUtilizado:Landroid/widget/RelativeLayout;

.field containerTotalLis:Landroid/widget/LinearLayout;

.field conteinerLimite:Landroid/widget/LinearLayout;

.field conteinerSaldo:Landroid/widget/LinearLayout;

.field extratoController:Lcom/itau/empresas/controller/ExtratoController;

.field flBotaoFecharFiltro:Landroid/widget/FrameLayout;

.field private limiteTotal:Ljava/lang/Double;

.field private lis:Ljava/lang/Double;

.field private lisAdicional:Ljava/lang/Double;

.field private lisUtilizado:Ljava/lang/Double;

.field private lisVO:Lcom/itau/empresas/api/model/LisVO;

.field private saldoDisponivel:Ljava/lang/Double;

.field private saldoVO:Lcom/itau/empresas/api/model/SaldoVO;

.field separadorSaldo:Landroid/view/View;

.field slideLisUtilizado:Landroid/widget/SeekBar;

.field textoLisTotal:Landroid/widget/TextView;

.field textoTituloLimite:Landroid/widget/TextView;

.field textoTituloSaldoLimite:Landroid/widget/TextView;

.field textoValorAdicional:Landroid/widget/TextView;

.field textoValorComLimite:Landroid/widget/TextView;

.field textoValorLis:Landroid/widget/TextView;

.field textoValorSaldo:Landroid/widget/TextView;

.field textoValorUtilizado:Landroid/widget/TextView;

.field private totalComLimite:Ljava/lang/Double;


# direct methods
.method public constructor <init>()V
    .registers 3

    .line 41
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    .line 64
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->saldoDisponivel:Ljava/lang/Double;

    .line 65
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lis:Ljava/lang/Double;

    .line 66
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lisAdicional:Ljava/lang/Double;

    .line 67
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lisUtilizado:Ljava/lang/Double;

    .line 68
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->limiteTotal:Ljava/lang/Double;

    .line 69
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->totalComLimite:Ljava/lang/Double;

    return-void
.end method

.method private ajustaVisibilidadeDosItens()V
    .registers 5

    .line 223
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->separadorSaldo:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 224
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->containerTotalLis:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 226
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->saldoVO:Lcom/itau/empresas/api/model/SaldoVO;

    if-eqz v0, :cond_18

    .line 227
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->conteinerSaldo:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 230
    :cond_18
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lisUtilizado:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_53

    .line 231
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->slideLisUtilizado:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->limiteTotal:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 232
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->slideLisUtilizado:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lisUtilizado:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 233
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->containerLisUtilizado:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 234
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->separadorSaldo:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 235
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->containerTotalLis:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 236
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->conteinerLimite:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_5a

    .line 238
    :cond_53
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->containerLisUtilizado:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 241
    :goto_5a
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lisAdicional:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_6e

    .line 242
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->containerLisAdicional:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_86

    .line 244
    :cond_6e
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->containerLisAdicional:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 245
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->separadorSaldo:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 246
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->containerTotalLis:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 247
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->conteinerLimite:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 250
    :goto_86
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lis:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_9a

    .line 251
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->containerLis:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_b2

    .line 253
    :cond_9a
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->containerLis:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 254
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->containerTotalLis:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 255
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->separadorSaldo:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 256
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->conteinerLimite:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 259
    :goto_b2
    return-void
.end method

.method private disparaEventoAnalytics()V
    .registers 5

    .line 298
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v3

    .line 299
    .line 300
    .local v3, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    const v0, 0x7f0700de

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 301
    const v1, 0x7f0700c8

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 302
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 299
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    return-void
.end method

.method private fazerRequest()V
    .registers 2

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->extratoController:Lcom/itau/empresas/controller/ExtratoController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/ExtratoController;->consultaSaldo()V

    .line 82
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->extratoController:Lcom/itau/empresas/controller/ExtratoController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/ExtratoController;->consultaMultiLimite()V

    .line 83
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->mostrarProgresso(Z)V

    .line 84
    return-void
.end method

.method private insereValoresLis(Lcom/itau/empresas/api/model/LisVO;)V
    .registers 6
    .param p1, "retornoLis"    # Lcom/itau/empresas/api/model/LisVO;

    .line 184
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->saldoVO:Lcom/itau/empresas/api/model/SaldoVO;

    if-eqz v0, :cond_6

    if-nez p1, :cond_7

    .line 185
    :cond_6
    return-void

    .line 188
    :cond_7
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteLis()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1b

    .line 189
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteLis()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lis:Ljava/lang/Double;

    .line 192
    :cond_1b
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->saldoVO:Lcom/itau/empresas/api/model/SaldoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/SaldoVO;->getValorLimiteUtilizado()D

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_33

    .line 193
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->saldoVO:Lcom/itau/empresas/api/model/SaldoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/SaldoVO;->getValorLimiteUtilizado()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lisUtilizado:Ljava/lang/Double;

    .line 196
    :cond_33
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteAdicional()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_47

    .line 197
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteAdicional()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lisAdicional:Ljava/lang/Double;

    .line 200
    :cond_47
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteTotal()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5b

    .line 201
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteTotal()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->limiteTotal:Ljava/lang/Double;

    .line 204
    :cond_5b
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->saldoVO:Lcom/itau/empresas/api/model/SaldoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/SaldoVO;->getDisponivelSaque()Ljava/lang/Double;

    move-result-object v0

    if-eqz v0, :cond_6b

    .line 205
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->saldoVO:Lcom/itau/empresas/api/model/SaldoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/SaldoVO;->getDisponivelSaque()Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->totalComLimite:Ljava/lang/Double;

    .line 207
    :cond_6b
    return-void
.end method

.method private insereValoresMultiLimite(Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;)V
    .registers 4
    .param p1, "retornoMultiLimite"    # Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;

    .line 142
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->saldoVO:Lcom/itau/empresas/api/model/SaldoVO;

    if-eqz v0, :cond_6

    if-nez p1, :cond_7

    .line 143
    :cond_6
    return-void

    .line 146
    :cond_7
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;->getLimiteTotalConcedido()Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->doubleValue()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lis:Ljava/lang/Double;

    .line 148
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;->getLimiteTotalUtilizado()Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lisUtilizado:Ljava/lang/Double;

    .line 150
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;->getLimiteTotal()Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->limiteTotal:Ljava/lang/Double;

    .line 152
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->saldoVO:Lcom/itau/empresas/api/model/SaldoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/SaldoVO;->getDisponivelSaque()Ljava/lang/Double;

    move-result-object v0

    if-eqz v0, :cond_43

    .line 153
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->saldoVO:Lcom/itau/empresas/api/model/SaldoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/SaldoVO;->getDisponivelSaque()Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->totalComLimite:Ljava/lang/Double;

    .line 155
    :cond_43
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 294
    new-instance v0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private mostrarProgresso(Z)V
    .registers 2
    .param p1, "exibir"    # Z

    .line 286
    if-eqz p1, :cond_6

    .line 287
    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->mostraDialogoDeProgresso()V

    .line 288
    return-void

    .line 290
    :cond_6
    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->escondeDialogoDeProgresso()V

    .line 291
    return-void
.end method

.method private verificaEstouroLimite()V
    .registers 5

    .line 210
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lisUtilizado:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    iget-object v2, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->limiteTotal:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-lez v0, :cond_14

    .line 211
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->limiteTotal:Ljava/lang/Double;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lisUtilizado:Ljava/lang/Double;

    .line 213
    :cond_14
    return-void
.end method

.method private zerarValores()V
    .registers 3

    .line 216
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lis:Ljava/lang/Double;

    .line 217
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lisAdicional:Ljava/lang/Double;

    .line 218
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lisUtilizado:Ljava/lang/Double;

    .line 219
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->limiteTotal:Ljava/lang/Double;

    .line 220
    return-void
.end method


# virtual methods
.method afterViews()V
    .registers 1

    .line 75
    invoke-static {p0}, Lcom/itau/empresas/ui/util/EventBusUtils;->registraEventBus(Landroid/app/Activity;)V

    .line 76
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->fazerRequest()V

    .line 77
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->disparaEventoAnalytics()V

    .line 78
    return-void
.end method

.method clicarBotaoVoltar()V
    .registers 1

    .line 88
    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->onBackPressed()V

    .line 89
    return-void
.end method

.method public direcionarDetalhesLimite()V
    .registers 4

    .line 93
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    if-eqz v0, :cond_16

    .line 94
    invoke-static {p0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_$IntentBuilder_;

    move-result-object v0

    const-string v1, "lisVO"

    iget-object v2, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_$IntentBuilder_;->extra(Ljava/lang/String;Ljava/io/Serializable;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_$IntentBuilder_;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    goto :goto_1d

    .line 96
    :cond_16
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 98
    :goto_1d
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 7
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 262
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    .line 263
    .local v2, "opKey":Ljava/lang/String;
    move-object v3, v2

    const/4 v4, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_5a

    goto :goto_2f

    :sswitch_12
    const-string v0, "multiLimites"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2f

    const/4 v4, 0x0

    goto :goto_2f

    :sswitch_1c
    const-string v0, "lis"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2f

    const/4 v4, 0x1

    goto :goto_2f

    :sswitch_26
    const-string v0, "saldo"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2f

    const/4 v4, 0x2

    :cond_2f
    :goto_2f
    packed-switch v4, :pswitch_data_68

    goto :goto_4c

    .line 266
    :pswitch_33
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->mostrarProgresso(Z)V

    .line 267
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->extratoController:Lcom/itau/empresas/controller/ExtratoController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/ExtratoController;->consultaLis()V

    .line 268
    goto :goto_59

    .line 271
    :pswitch_3d
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->mostrarProgresso(Z)V

    .line 272
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->zerarValores()V

    .line 273
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->ajustaVisibilidadeDosItens()V

    .line 274
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->mostrarProgresso(Z)V

    .line 275
    goto :goto_59

    .line 278
    :goto_4c
    :pswitch_4c
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->textoValorSaldo:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 279
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->zerarValores()V

    .line 280
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->ajustaVisibilidadeDosItens()V

    .line 283
    :goto_59
    return-void

    :sswitch_data_5a
    .sparse-switch
        -0x65c589d0 -> :sswitch_12
        0x1a296 -> :sswitch_1c
        0x68248e9 -> :sswitch_26
    .end sparse-switch

    :pswitch_data_68
    .packed-switch 0x0
        :pswitch_33
        :pswitch_3d
        :pswitch_4c
    .end packed-switch
.end method

.method public onEventMainThread(Lcom/itau/empresas/api/model/LisVO;)V
    .registers 6
    .param p1, "retornoLis"    # Lcom/itau/empresas/api/model/LisVO;

    .line 158
    iput-object p1, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    .line 159
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->zerarValores()V

    .line 160
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->textoTituloLimite:Landroid/widget/TextView;

    .line 161
    const v1, 0x7f07017a

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->textoTituloSaldoLimite:Landroid/widget/TextView;

    .line 163
    const v1, 0x7f07017c

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->insereValoresLis(Lcom/itau/empresas/api/model/LisVO;)V

    .line 167
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->verificaEstouroLimite()V

    .line 169
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->textoValorLis:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lis:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(DZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->textoValorUtilizado:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lisUtilizado:Ljava/lang/Double;

    .line 171
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(DZ)Ljava/lang/String;

    move-result-object v1

    .line 170
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->textoValorAdicional:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lisAdicional:Ljava/lang/Double;

    .line 173
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(DZ)Ljava/lang/String;

    move-result-object v1

    .line 172
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->textoLisTotal:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->limiteTotal:Ljava/lang/Double;

    .line 175
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(DZ)Ljava/lang/String;

    move-result-object v1

    .line 174
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->textoValorComLimite:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->totalComLimite:Ljava/lang/Double;

    .line 177
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(DZ)Ljava/lang/String;

    move-result-object v1

    .line 176
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->mostrarProgresso(Z)V

    .line 180
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->ajustaVisibilidadeDosItens()V

    .line 181
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/api/model/SaldoVO;)V
    .registers 6
    .param p1, "retornoSaldo"    # Lcom/itau/empresas/api/model/SaldoVO;

    .line 106
    iput-object p1, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->saldoVO:Lcom/itau/empresas/api/model/SaldoVO;

    .line 107
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->textoValorSaldo:Landroid/widget/TextView;

    .line 108
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/SaldoVO;->getSaldoDisponivelSaque()Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 107
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/SaldoVO;->getSaldoDisponivelSaque()Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->saldoDisponivel:Ljava/lang/Double;

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->saldoDisponivel:Ljava/lang/Double;

    invoke-static {p0, v0}, Lcom/itau/empresas/feature/saldo/SaldoUtils;->getCorSaldo(Landroid/content/Context;Ljava/lang/Double;)I

    move-result v3

    .line 114
    .local v3, "cor":I
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->textoValorSaldo:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 115
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;)V
    .registers 6
    .param p1, "retornoMultiLimite"    # Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;

    .line 118
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->zerarValores()V

    .line 120
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->textoTituloLimite:Landroid/widget/TextView;

    const v1, 0x7f07017f

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->textoTituloSaldoLimite:Landroid/widget/TextView;

    const v1, 0x7f07017d

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->insereValoresMultiLimite(Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;)V

    .line 125
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->verificaEstouroLimite()V

    .line 127
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->textoValorLis:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lis:Ljava/lang/Double;

    .line 128
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(DZ)Ljava/lang/String;

    move-result-object v1

    .line 127
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->textoValorUtilizado:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->lisUtilizado:Ljava/lang/Double;

    .line 131
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(DZ)Ljava/lang/String;

    move-result-object v1

    .line 130
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->textoValorComLimite:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->totalComLimite:Ljava/lang/Double;

    .line 134
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(DZ)Ljava/lang/String;

    move-result-object v1

    .line 133
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->ajustaVisibilidadeDosItens()V

    .line 137
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->mostrarProgresso(Z)V

    .line 138
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 102
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

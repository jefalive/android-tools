.class public abstract Lbr/com/itau/sdk/android/core/j$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lbr/com/itau/sdk/android/core/j$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/sdk/android/core/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "c"
.end annotation


# instance fields
.field private final builder:Lokhttp3/FormBody$Builder;

.field private final url:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Lokhttp3/FormBody$Builder;)V
    .registers 3

    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/j$c;->url:Ljava/lang/String;

    .line 178
    iput-object p2, p0, Lbr/com/itau/sdk/android/core/j$c;->builder:Lokhttp3/FormBody$Builder;

    .line 179
    return-void
.end method

.method static synthetic access$300(Lbr/com/itau/sdk/android/core/j$c;)Lokhttp3/FormBody$Builder;
    .registers 2

    .line 172
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/j$c;->builder:Lokhttp3/FormBody$Builder;

    return-object v0
.end method

.method static synthetic access$400(Lbr/com/itau/sdk/android/core/j$c;)Ljava/lang/String;
    .registers 2

    .line 172
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/j$c;->url:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected abstract onPageLoaded(Ljava/lang/String;)V
.end method

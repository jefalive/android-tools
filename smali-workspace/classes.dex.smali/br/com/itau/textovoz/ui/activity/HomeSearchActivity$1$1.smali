.class Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1$1;
.super Landroid/os/CountDownTimer;
.source "HomeSearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;->onQueryTextChange(Ljava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;

.field final synthetic val$query:Ljava/lang/String;

.field final synthetic val$text:Ljava/lang/String;


# direct methods
.method constructor <init>(Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;JJLjava/lang/String;Ljava/lang/String;)V
    .registers 8
    .param p1, "this$1"    # Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .line 343
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1$1;->this$1:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;

    iput-object p6, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1$1;->val$text:Ljava/lang/String;

    iput-object p7, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1$1;->val$query:Ljava/lang/String;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .registers 5

    .line 350
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1$1;->val$text:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1$1;->val$text:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_37

    .line 351
    :cond_11
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1$1;->this$1:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;

    iget-object v0, v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1$1;->this$1:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;

    iget-object v1, v1, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    sget v2, Lbr/com/itau/textovoz/R$string;->ga_categoria_busca:I

    invoke-virtual {v1, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1$1;->this$1:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;

    iget-object v2, v2, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    sget v3, Lbr/com/itau/textovoz/R$string;->ga_acao_parou_digitar:I

    invoke-virtual {v2, v3}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1$1;->val$query:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setEventTracker(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1$1;->this$1:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;

    iget-object v0, v0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1$1;->val$text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->search(Ljava/lang/String;)V

    .line 354
    :cond_37
    return-void
.end method

.method public onTick(J)V
    .registers 3
    .param p1, "millisUntilFinished"    # J

    .line 346
    return-void
.end method

.class public Lcom/google/android/gms/internal/zzev;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/zzem;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private zzCD:Lcom/google/android/gms/internal/zzer;

.field private final zzCf:Lcom/google/android/gms/internal/zzeo;

.field private final zzCu:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

.field private final zzCv:J

.field private final zzCw:J

.field private zzCy:Z

.field private final zzpV:Ljava/lang/Object;

.field private final zzpe:Lcom/google/android/gms/internal/zzcb;

.field private final zzpn:Lcom/google/android/gms/internal/zzex;

.field private final zzsA:Z

.field private final zzuS:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Lcom/google/android/gms/internal/zzex;Lcom/google/android/gms/internal/zzeo;ZZJJLcom/google/android/gms/internal/zzcb;)V
    .registers 13

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzev;->zzpV:Ljava/lang/Object;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzev;->zzCy:Z

    iput-object p1, p0, Lcom/google/android/gms/internal/zzev;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzev;->zzCu:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iput-object p3, p0, Lcom/google/android/gms/internal/zzev;->zzpn:Lcom/google/android/gms/internal/zzex;

    iput-object p4, p0, Lcom/google/android/gms/internal/zzev;->zzCf:Lcom/google/android/gms/internal/zzeo;

    iput-boolean p5, p0, Lcom/google/android/gms/internal/zzev;->zzsA:Z

    iput-boolean p6, p0, Lcom/google/android/gms/internal/zzev;->zzuS:Z

    iput-wide p7, p0, Lcom/google/android/gms/internal/zzev;->zzCv:J

    iput-wide p9, p0, Lcom/google/android/gms/internal/zzev;->zzCw:J

    iput-object p11, p0, Lcom/google/android/gms/internal/zzev;->zzpe:Lcom/google/android/gms/internal/zzcb;

    return-void
.end method


# virtual methods
.method public cancel()V
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzev;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_4
    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzev;->zzCy:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/zzev;->zzCD:Lcom/google/android/gms/internal/zzer;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/gms/internal/zzev;->zzCD:Lcom/google/android/gms/internal/zzer;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzer;->cancel()V
    :try_end_f
    .catchall {:try_start_4 .. :try_end_f} :catchall_11

    :cond_f
    monitor-exit v1

    goto :goto_14

    :catchall_11
    move-exception v2

    monitor-exit v1

    throw v2

    :goto_14
    return-void
.end method

.method public zzc(Ljava/util/List;)Lcom/google/android/gms/internal/zzes;
    .registers 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/google/android/gms/internal/zzen;>;)Lcom/google/android/gms/internal/zzes;"
        }
    .end annotation

    const-string v0, "Starting mediation."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaI(Ljava/lang/String;)V

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzev;->zzpe:Lcom/google/android/gms/internal/zzcb;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzcb;->zzdB()Lcom/google/android/gms/internal/zzbz;

    move-result-object v14

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_16
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13f

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v16, v0

    check-cast v16, Lcom/google/android/gms/internal/zzen;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Trying mediation network: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v1, v16

    iget-object v1, v1, Lcom/google/android/gms/internal/zzen;->zzBA:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaJ(Ljava/lang/String;)V

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/gms/internal/zzen;->zzBB:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_46
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13d

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object/from16 v18, v0

    check-cast v18, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzev;->zzpe:Lcom/google/android/gms/internal/zzcb;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzcb;->zzdB()Lcom/google/android/gms/internal/zzbz;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzev;->zzpV:Ljava/lang/Object;

    move-object/from16 v20, v0

    monitor-enter v20

    move-object/from16 v0, p0

    :try_start_65
    iget-boolean v0, v0, Lcom/google/android/gms/internal/zzev;->zzCy:Z

    if-eqz v0, :cond_71

    new-instance v0, Lcom/google/android/gms/internal/zzes;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/zzes;-><init>(I)V
    :try_end_6f
    .catchall {:try_start_65 .. :try_end_6f} :catchall_b2

    monitor-exit v20

    return-object v0

    :cond_71
    :try_start_71
    new-instance v0, Lcom/google/android/gms/internal/zzer;

    move-object/from16 v1, p0

    iget-object v1, v1, Lcom/google/android/gms/internal/zzev;->mContext:Landroid/content/Context;

    move-object/from16 v2, p0

    iget-object v3, v2, Lcom/google/android/gms/internal/zzev;->zzpn:Lcom/google/android/gms/internal/zzex;

    move-object/from16 v2, p0

    iget-object v4, v2, Lcom/google/android/gms/internal/zzev;->zzCf:Lcom/google/android/gms/internal/zzeo;

    move-object/from16 v2, p0

    iget-object v2, v2, Lcom/google/android/gms/internal/zzev;->zzCu:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iget-object v6, v2, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->zzHt:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    move-object/from16 v2, p0

    iget-object v2, v2, Lcom/google/android/gms/internal/zzev;->zzCu:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iget-object v7, v2, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->zzrp:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    move-object/from16 v2, p0

    iget-object v2, v2, Lcom/google/android/gms/internal/zzev;->zzCu:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iget-object v8, v2, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->zzrl:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    move-object/from16 v2, p0

    iget-boolean v9, v2, Lcom/google/android/gms/internal/zzev;->zzsA:Z

    move-object/from16 v2, p0

    iget-boolean v10, v2, Lcom/google/android/gms/internal/zzev;->zzuS:Z

    move-object/from16 v2, p0

    iget-object v2, v2, Lcom/google/android/gms/internal/zzev;->zzCu:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iget-object v11, v2, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->zzrD:Lcom/google/android/gms/ads/internal/formats/NativeAdOptionsParcel;

    move-object/from16 v2, p0

    iget-object v2, v2, Lcom/google/android/gms/internal/zzev;->zzCu:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iget-object v12, v2, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->zzrH:Ljava/util/List;

    move-object/from16 v2, v18

    move-object/from16 v5, v16

    invoke-direct/range {v0 .. v12}, Lcom/google/android/gms/internal/zzer;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/internal/zzex;Lcom/google/android/gms/internal/zzeo;Lcom/google/android/gms/internal/zzen;Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;ZZLcom/google/android/gms/ads/internal/formats/NativeAdOptionsParcel;Ljava/util/List;)V

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/gms/internal/zzev;->zzCD:Lcom/google/android/gms/internal/zzer;
    :try_end_b0
    .catchall {:try_start_71 .. :try_end_b0} :catchall_b2

    monitor-exit v20

    goto :goto_b5

    :catchall_b2
    move-exception v21

    monitor-exit v20

    throw v21

    :goto_b5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzev;->zzCD:Lcom/google/android/gms/internal/zzer;

    move-object/from16 v1, p0

    iget-wide v1, v1, Lcom/google/android/gms/internal/zzev;->zzCv:J

    move-object/from16 v3, p0

    iget-wide v3, v3, Lcom/google/android/gms/internal/zzev;->zzCw:J

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/gms/internal/zzer;->zza(JJ)Lcom/google/android/gms/internal/zzes;

    move-result-object v20

    move-object/from16 v0, v20

    iget v0, v0, Lcom/google/android/gms/internal/zzes;->zzCo:I

    if-nez v0, :cond_111

    const-string v0, "Adapter succeeded."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaI(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzev;->zzpe:Lcom/google/android/gms/internal/zzcb;

    const-string v1, "mediation_network_succeed"

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/zzcb;->zzc(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_f0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzev;->zzpe:Lcom/google/android/gms/internal/zzcb;

    const-string v1, "mediation_networks_fail"

    const-string v2, ","

    invoke-static {v2, v13}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/zzcb;->zzc(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzev;->zzpe:Lcom/google/android/gms/internal/zzcb;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "mls"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    move-object/from16 v2, v19

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/internal/zzcb;->zza(Lcom/google/android/gms/internal/zzbz;[Ljava/lang/String;)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzev;->zzpe:Lcom/google/android/gms/internal/zzcb;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "ttm"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v14, v1}, Lcom/google/android/gms/internal/zzcb;->zza(Lcom/google/android/gms/internal/zzbz;[Ljava/lang/String;)Z

    return-object v20

    :cond_111
    move-object/from16 v0, v18

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzev;->zzpe:Lcom/google/android/gms/internal/zzcb;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "mlf"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    move-object/from16 v2, v19

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/internal/zzcb;->zza(Lcom/google/android/gms/internal/zzbz;[Ljava/lang/String;)Z

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/gms/internal/zzes;->zzCq:Lcom/google/android/gms/internal/zzey;

    if-eqz v0, :cond_13b

    sget-object v0, Lcom/google/android/gms/internal/zzir;->zzMc:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/internal/zzev$1;

    move-object/from16 v2, p0

    move-object/from16 v3, v20

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/internal/zzev$1;-><init>(Lcom/google/android/gms/internal/zzev;Lcom/google/android/gms/internal/zzes;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_13b
    goto/16 :goto_46

    :cond_13d
    goto/16 :goto_16

    :cond_13f
    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_154

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/zzev;->zzpe:Lcom/google/android/gms/internal/zzcb;

    const-string v1, "mediation_networks_fail"

    const-string v2, ","

    invoke-static {v2, v13}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/zzcb;->zzc(Ljava/lang/String;Ljava/lang/String;)V

    :cond_154
    new-instance v0, Lcom/google/android/gms/internal/zzes;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/zzes;-><init>(I)V

    return-object v0
.end method

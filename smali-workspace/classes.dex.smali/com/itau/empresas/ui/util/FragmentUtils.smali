.class public Lcom/itau/empresas/ui/util/FragmentUtils;
.super Ljava/lang/Object;
.source "FragmentUtils.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static attachFragment(Landroid/support/v4/app/FragmentTransaction;ILcom/itau/empresas/ui/fragment/BaseFragment;Ljava/lang/String;)V
    .registers 5
    .param p0, "fragmentTransaction"    # Landroid/support/v4/app/FragmentTransaction;
    .param p1, "content"    # I
    .param p2, "fragment"    # Lcom/itau/empresas/ui/fragment/BaseFragment;
    .param p3, "tag"    # Ljava/lang/String;

    .line 34
    if-eqz p2, :cond_18

    .line 35
    invoke-virtual {p2}, Lcom/itau/empresas/ui/fragment/BaseFragment;->isDetached()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 36
    invoke-virtual {p0, p2}, Landroid/support/v4/app/FragmentTransaction;->attach(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_15

    .line 37
    :cond_c
    invoke-virtual {p2}, Lcom/itau/empresas/ui/fragment/BaseFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_15

    .line 38
    invoke-virtual {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 40
    :cond_15
    :goto_15
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 42
    :cond_18
    return-void
.end method

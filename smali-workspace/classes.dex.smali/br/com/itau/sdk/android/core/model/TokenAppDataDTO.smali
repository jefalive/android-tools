.class public Lbr/com/itau/sdk/android/core/model/TokenAppDataDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private cancelSerialNumber:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cancelSerialNumber"
    .end annotation
.end field

.field private ctf:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ctf"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCancelSerialNumber()Ljava/lang/String;
    .registers 2

    .line 20
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/TokenAppDataDTO;->cancelSerialNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getCtf()Ljava/lang/String;
    .registers 2

    .line 16
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/TokenAppDataDTO;->ctf:Ljava/lang/String;

    return-object v0
.end method

.method public setCancelSerialNumber(Ljava/lang/String;)V
    .registers 2

    .line 24
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/TokenAppDataDTO;->cancelSerialNumber:Ljava/lang/String;

    .line 25
    return-void
.end method

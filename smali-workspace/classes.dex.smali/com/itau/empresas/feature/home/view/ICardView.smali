.class public abstract Lcom/itau/empresas/feature/home/view/ICardView;
.super Landroid/widget/FrameLayout;
.source "ICardView.java"


# instance fields
.field protected divisorCard:Landroid/view/View;

.field protected frameInterno:Landroid/widget/FrameLayout;

.field protected icExpandir:Lcom/itau/empresas/ui/view/TextViewIcon;

.field preferenciaUtils:Lcom/itau/empresas/PreferenciaUtils;

.field protected progressCard:Landroid/widget/ProgressBar;

.field protected textoExpandir:Landroid/widget/TextView;

.field protected textoTituloCard:Landroid/widget/TextView;

.field protected textoVerMaisDireito:Landroid/widget/TextView;

.field protected textoVerMaisEsquerdo:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 51
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 52
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/view/ICardView;->init(Landroid/content/Context;)V

    .line 53
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .registers 6
    .param p1, "context"    # Landroid/content/Context;

    .line 100
    const v0, 0x7f030171

    invoke-static {p1, v0, p0}, Lcom/itau/empresas/feature/home/view/ICardView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 101
    .local v3, "v":Landroid/view/View;
    const v0, 0x7f0e0648

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->frameInterno:Landroid/widget/FrameLayout;

    .line 102
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/ICardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/ICardView;->layoutResId()I

    move-result v1

    iget-object v2, p0, Lcom/itau/empresas/feature/home/view/ICardView;->frameInterno:Landroid/widget/FrameLayout;

    invoke-static {v0, v1, v2}, Lcom/itau/empresas/feature/home/view/ICardView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 103
    return-void
.end method


# virtual methods
.method public buscaEstadoWidget(Ljava/lang/String;)Z
    .registers 3
    .param p1, "chave"    # Ljava/lang/String;

    .line 148
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->preferenciaUtils:Lcom/itau/empresas/PreferenciaUtils;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/PreferenciaUtils;->get(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected clicouEmExpandir()V
    .registers 2

    .line 107
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/ICardView;->isCarregando()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 108
    return-void

    .line 111
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/ICardView;->isCardAberto()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 112
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/ICardView;->fecharCard()V

    .line 113
    return-void

    .line 116
    :cond_11
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/ICardView;->fazerRequest()V

    .line 117
    return-void
.end method

.method protected expandirCard()V
    .registers 4

    .line 120
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->textoExpandir:Landroid/widget/TextView;

    const v1, 0x7f070629

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 121
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->icExpandir:Lcom/itau/empresas/ui/view/TextViewIcon;

    const v1, 0x7f070288

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/TextViewIcon;->setText(I)V

    .line 123
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->frameInterno:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 124
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->divisorCard:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 126
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_41

    .line 127
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->frameInterno:Landroid/widget/FrameLayout;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expandindo "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/home/view/ICardView;->textoTituloCard:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->announceForAccessibility(Ljava/lang/CharSequence;)V

    goto :goto_48

    .line 129
    :cond_41
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->frameInterno:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->sendAccessibilityEvent(I)V

    .line 132
    :goto_48
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/ICardView;->preferenciasTAG()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/home/view/ICardView;->salvaEstadoWidget(Ljava/lang/String;Z)V

    .line 133
    return-void
.end method

.method protected abstract fazerRequest()V
.end method

.method protected fecharCard()V
    .registers 3

    .line 136
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->textoExpandir:Landroid/widget/TextView;

    const v1, 0x7f070565

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 137
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->icExpandir:Lcom/itau/empresas/ui/view/TextViewIcon;

    const v1, 0x7f070286

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/TextViewIcon;->setText(I)V

    .line 138
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->frameInterno:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 139
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->divisorCard:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 140
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/ICardView;->preferenciasTAG()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/home/view/ICardView;->salvaEstadoWidget(Ljava/lang/String;Z)V

    .line 141
    return-void
.end method

.method protected isCardAberto()Z
    .registers 2

    .line 90
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->frameInterno:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method protected isCarregando()Z
    .registers 2

    .line 86
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->progressCard:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method protected abstract layoutResId()I
.end method

.method protected abstract preferenciasTAG()Ljava/lang/String;
.end method

.method protected retiraDivisor()V
    .registers 3

    .line 82
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->divisorCard:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 83
    return-void
.end method

.method public salvaEstadoWidget(Ljava/lang/String;Z)V
    .registers 4
    .param p1, "chave"    # Ljava/lang/String;
    .param p2, "estado"    # Z

    .line 144
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->preferenciaUtils:Lcom/itau/empresas/PreferenciaUtils;

    invoke-virtual {v0, p1, p2}, Lcom/itau/empresas/PreferenciaUtils;->set(Ljava/lang/String;Z)V

    .line 145
    return-void
.end method

.method protected setCarregando(Z)V
    .registers 4
    .param p1, "carregando"    # Z

    .line 94
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->progressCard:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_6

    const/4 v1, 0x0

    goto :goto_8

    :cond_6
    const/16 v1, 0x8

    :goto_8
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 95
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->textoExpandir:Landroid/widget/TextView;

    if-eqz p1, :cond_11

    const/4 v1, 0x4

    goto :goto_12

    :cond_11
    const/4 v1, 0x0

    :goto_12
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 96
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->icExpandir:Lcom/itau/empresas/ui/view/TextViewIcon;

    if-eqz p1, :cond_1b

    const/4 v1, 0x4

    goto :goto_1c

    :cond_1b
    const/4 v1, 0x0

    :goto_1c
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/TextViewIcon;->setVisibility(I)V

    .line 97
    return-void
.end method

.method protected setTextoVerMaisDireito(Ljava/lang/String;Z)V
    .registers 5
    .param p1, "texto"    # Ljava/lang/String;
    .param p2, "visivel"    # Z

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->textoVerMaisDireito:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    if-eqz p2, :cond_e

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->textoVerMaisDireito:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_15

    .line 75
    :cond_e
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->textoVerMaisDireito:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 77
    :goto_15
    return-void
.end method

.method protected setTextoVerMaisEsquerdo(Ljava/lang/String;Z)V
    .registers 5
    .param p1, "texto"    # Ljava/lang/String;
    .param p2, "visivel"    # Z

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->textoVerMaisEsquerdo:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    if-eqz p2, :cond_e

    .line 64
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->textoVerMaisEsquerdo:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_15

    .line 66
    :cond_e
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/ICardView;->textoVerMaisEsquerdo:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 68
    :goto_15
    return-void
.end method

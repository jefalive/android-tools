.class public Lnet/hockeyapp/android/tasks/SendFeedbackTask;
.super Landroid/os/AsyncTask;
.source "SendFeedbackTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    }
.end annotation


# instance fields
.field private attachmentUris:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Landroid/net/Uri;>;"
        }
    .end annotation
.end field

.field private context:Landroid/content/Context;

.field private email:Ljava/lang/String;

.field private handler:Landroid/os/Handler;

.field private isFetchMessages:Z

.field private lastMessageId:I

.field private name:Ljava/lang/String;

.field private progressDialog:Landroid/app/ProgressDialog;

.field private showProgressDialog:Z

.field private subject:Ljava/lang/String;

.field private text:Ljava/lang/String;

.field private token:Ljava/lang/String;

.field private urlString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Landroid/os/Handler;Z)V
    .registers 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "urlString"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "email"    # Ljava/lang/String;
    .param p5, "subject"    # Ljava/lang/String;
    .param p6, "text"    # Ljava/lang/String;
    .param p7, "attachmentUris"    # Ljava/util/List;
    .param p8, "token"    # Ljava/lang/String;
    .param p9, "handler"    # Landroid/os/Handler;
    .param p10, "isFetchMessages"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List<Landroid/net/Uri;>;Ljava/lang/String;Landroid/os/Handler;Z)V"
        }
    .end annotation

    .line 100
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 102
    iput-object p1, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->context:Landroid/content/Context;

    .line 103
    iput-object p2, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->urlString:Ljava/lang/String;

    .line 104
    iput-object p3, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->name:Ljava/lang/String;

    .line 105
    iput-object p4, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->email:Ljava/lang/String;

    .line 106
    iput-object p5, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->subject:Ljava/lang/String;

    .line 107
    iput-object p6, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->text:Ljava/lang/String;

    .line 108
    iput-object p7, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->attachmentUris:Ljava/util/List;

    .line 109
    iput-object p8, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->token:Ljava/lang/String;

    .line 110
    iput-object p9, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->handler:Landroid/os/Handler;

    .line 111
    iput-boolean p10, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->isFetchMessages:Z

    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->showProgressDialog:Z

    .line 113
    const/4 v0, -0x1

    iput v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->lastMessageId:I

    .line 115
    if-eqz p1, :cond_22

    .line 116
    invoke-static {p1}, Lnet/hockeyapp/android/Constants;->loadFromContext(Landroid/content/Context;)V

    .line 118
    :cond_22
    return-void
.end method

.method private doGet(Lorg/apache/http/client/HttpClient;)Ljava/util/HashMap;
    .registers 10
    .param p1, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lorg/apache/http/client/HttpClient;)Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
        }
    .end annotation

    .line 369
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 370
    .local v3, "sb":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->urlString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->token:Ljava/lang/String;

    invoke-static {v1}, Lnet/hockeyapp/android/utils/Util;->encodeParam(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 372
    iget v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->lastMessageId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3e

    .line 373
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "?last_message_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->lastMessageId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 376
    :cond_3e
    new-instance v4, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 378
    .local v4, "httpGet":Lorg/apache/http/client/methods/HttpGet;
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 379
    .local v5, "result":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "type"

    const-string v1, "fetch"

    invoke-virtual {v5, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 383
    :try_start_53
    invoke-interface {p1, v4}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v6

    .line 384
    .local v6, "response":Lorg/apache/http/HttpResponse;
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v7

    .line 386
    .local v7, "responseEntity":Lorg/apache/http/HttpEntity;
    const-string v0, "response"

    invoke-static {v7}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 387
    const-string v0, "status"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_84
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_53 .. :try_end_84} :catch_85
    .catch Ljava/lang/IllegalStateException; {:try_start_53 .. :try_end_84} :catch_8a
    .catch Ljava/io/IOException; {:try_start_53 .. :try_end_84} :catch_8f

    .line 397
    .end local v6    # "response":Lorg/apache/http/HttpResponse;
    .end local v7    # "responseEntity":Lorg/apache/http/HttpEntity;
    goto :goto_93

    .line 389
    :catch_85
    move-exception v6

    .line 390
    .local v6, "e":Lorg/apache/http/client/ClientProtocolException;
    invoke-virtual {v6}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    .line 397
    .end local v6    # "e":Lorg/apache/http/client/ClientProtocolException;
    goto :goto_93

    .line 392
    :catch_8a
    move-exception v6

    .line 393
    .local v6, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v6}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 397
    .end local v6    # "e":Ljava/lang/IllegalStateException;
    goto :goto_93

    .line 395
    :catch_8f
    move-exception v6

    .line 396
    .local v6, "e":Ljava/io/IOException;
    invoke-virtual {v6}, Ljava/io/IOException;->printStackTrace()V

    .line 399
    .end local v6    # "e":Ljava/io/IOException;
    :goto_93
    return-object v5
.end method

.method private doPostPut(Lorg/apache/http/client/HttpClient;)Ljava/util/HashMap;
    .registers 12
    .param p1, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lorg/apache/http/client/HttpClient;)Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
        }
    .end annotation

    .line 223
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 224
    .local v3, "result":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "type"

    const-string v1, "send"

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    :try_start_c
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 228
    .local v4, "nameValuePairs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "name"

    iget-object v2, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->name:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "email"

    iget-object v2, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->email:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 230
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "subject"

    iget-object v2, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->subject:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "text"

    iget-object v2, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->text:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "bundle_identifier"

    sget-object v2, Lnet/hockeyapp/android/Constants;->APP_PACKAGE:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 233
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "bundle_short_version"

    sget-object v2, Lnet/hockeyapp/android/Constants;->APP_VERSION_NAME:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 234
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "bundle_version"

    sget-object v2, Lnet/hockeyapp/android/Constants;->APP_VERSION:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 235
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "os_version"

    sget-object v2, Lnet/hockeyapp/android/Constants;->ANDROID_VERSION:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 236
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "oem"

    sget-object v2, Lnet/hockeyapp/android/Constants;->PHONE_MANUFACTURER:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 237
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "model"

    sget-object v2, Lnet/hockeyapp/android/Constants;->PHONE_MODEL:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 239
    new-instance v5, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    const-string v0, "UTF-8"

    invoke-direct {v5, v4, v0}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 240
    .local v5, "form":Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    const-string v0, "UTF-8"

    invoke-virtual {v5, v0}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;->setContentEncoding(Ljava/lang/String;)V

    .line 242
    const/4 v6, 0x0

    .line 243
    .local v6, "httpPost":Lorg/apache/http/client/methods/HttpPost;
    const/4 v7, 0x0

    .line 244
    .local v7, "httpPut":Lorg/apache/http/client/methods/HttpPut;
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->token:Ljava/lang/String;

    if-eqz v0, :cond_c0

    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->urlString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->urlString:Ljava/lang/String;

    .line 246
    new-instance v7, Lorg/apache/http/client/methods/HttpPut;

    iget-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->urlString:Ljava/lang/String;

    invoke-direct {v7, v0}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    goto :goto_c7

    .line 249
    :cond_c0
    new-instance v6, Lorg/apache/http/client/methods/HttpPost;

    iget-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->urlString:Ljava/lang/String;

    invoke-direct {v6, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 252
    :goto_c7
    const/4 v8, 0x0

    .line 253
    .local v8, "response":Lorg/apache/http/HttpResponse;
    if-eqz v7, :cond_d2

    .line 254
    invoke-virtual {v7, v5}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 255
    invoke-interface {p1, v7}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v8

    goto :goto_db

    .line 257
    :cond_d2
    if-eqz v6, :cond_db

    .line 258
    invoke-virtual {v6, v5}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 259
    invoke-interface {p1, v6}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v8

    .line 262
    :cond_db
    :goto_db
    if-eqz v8, :cond_10a

    .line 263
    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v9

    .line 264
    .local v9, "resEntity":Lorg/apache/http/HttpEntity;
    const-string v0, "response"

    invoke-static {v9}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    const-string v0, "status"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_10a
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_c .. :try_end_10a} :catch_10b
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_c .. :try_end_10a} :catch_110
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_10a} :catch_115

    .line 276
    .end local v4    # "nameValuePairs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .end local v4
    .end local v5    # "form":Lorg/apache/http/client/entity/UrlEncodedFormEntity;
    .end local v6    # "httpPost":Lorg/apache/http/client/methods/HttpPost;
    .end local v7    # "httpPut":Lorg/apache/http/client/methods/HttpPut;
    .end local v8    # "response":Lorg/apache/http/HttpResponse;
    .end local v9    # "resEntity":Lorg/apache/http/HttpEntity;
    :cond_10a
    goto :goto_119

    .line 268
    :catch_10b
    move-exception v4

    .line 269
    .local v4, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v4}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 276
    .end local v4    # "e":Ljava/io/UnsupportedEncodingException;
    goto :goto_119

    .line 271
    :catch_110
    move-exception v4

    .line 272
    .local v4, "e":Lorg/apache/http/client/ClientProtocolException;
    invoke-virtual {v4}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    .line 276
    .end local v4    # "e":Lorg/apache/http/client/ClientProtocolException;
    goto :goto_119

    .line 274
    :catch_115
    move-exception v4

    .line 275
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 278
    .end local v4    # "e":Ljava/io/IOException;
    :goto_119
    return-object v3
.end method

.method private doPostPutWithAttachments(Lorg/apache/http/client/HttpClient;)Ljava/util/HashMap;
    .registers 13
    .param p1, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lorg/apache/http/client/HttpClient;)Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
        }
    .end annotation

    .line 287
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 288
    .local v3, "result":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "type"

    const-string v1, "send"

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    :try_start_c
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 292
    .local v4, "nameValuePairs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "name"

    iget-object v2, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->name:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 293
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "email"

    iget-object v2, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->email:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 294
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "subject"

    iget-object v2, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->subject:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 295
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "text"

    iget-object v2, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->text:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 296
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "bundle_identifier"

    sget-object v2, Lnet/hockeyapp/android/Constants;->APP_PACKAGE:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 297
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "bundle_short_version"

    sget-object v2, Lnet/hockeyapp/android/Constants;->APP_VERSION_NAME:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 298
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "bundle_version"

    sget-object v2, Lnet/hockeyapp/android/Constants;->APP_VERSION:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 299
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "os_version"

    sget-object v2, Lnet/hockeyapp/android/Constants;->ANDROID_VERSION:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 300
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "oem"

    sget-object v2, Lnet/hockeyapp/android/Constants;->PHONE_MANUFACTURER:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 301
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "model"

    sget-object v2, Lnet/hockeyapp/android/Constants;->PHONE_MODEL:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 303
    new-instance v5, Lnet/hockeyapp/android/utils/SimpleMultipartEntity;

    invoke-direct {v5}, Lnet/hockeyapp/android/utils/SimpleMultipartEntity;-><init>()V

    .line 304
    .local v5, "entity":Lnet/hockeyapp/android/utils/SimpleMultipartEntity;
    invoke-virtual {v5}, Lnet/hockeyapp/android/utils/SimpleMultipartEntity;->writeFirstBoundaryIfNeeds()V

    .line 307
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_95
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_ae

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lorg/apache/http/NameValuePair;

    .line 308
    .local v7, "pair":Lorg/apache/http/NameValuePair;
    invoke-interface {v7}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Lnet/hockeyapp/android/utils/SimpleMultipartEntity;->addPart(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    .end local v7    # "pair":Lorg/apache/http/NameValuePair;
    goto :goto_95

    .line 312
    :cond_ae
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_af
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->attachmentUris:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_f4

    .line 313
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->attachmentUris:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/net/Uri;

    .line 314
    .local v7, "attachmentUri":Landroid/net/Uri;
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->attachmentUris:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v6, v0, :cond_cc

    const/4 v8, 0x1

    goto :goto_cd

    :cond_cc
    const/4 v8, 0x0

    .line 316
    .local v8, "lastFile":Z
    :goto_cd
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v9

    .line 317
    .local v9, "input":Ljava/io/InputStream;
    invoke-virtual {v7}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v10

    .line 318
    .local v10, "filename":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "attachment"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0, v10, v9, v8}, Lnet/hockeyapp/android/utils/SimpleMultipartEntity;->addPart(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;Z)V

    .line 312
    .end local v7    # "attachmentUri":Landroid/net/Uri;
    .end local v8    # "lastFile":Z
    .end local v9    # "input":Ljava/io/InputStream;
    .end local v10    # "filename":Ljava/lang/String;
    add-int/lit8 v6, v6, 0x1

    goto :goto_af

    .line 320
    .end local v6    # "i":I
    :cond_f4
    invoke-virtual {v5}, Lnet/hockeyapp/android/utils/SimpleMultipartEntity;->writeLastBoundaryIfNeeds()V

    .line 322
    const/4 v6, 0x0

    .line 323
    .local v6, "httpPost":Lorg/apache/http/client/methods/HttpPost;
    const/4 v7, 0x0

    .line 324
    .local v7, "httpPut":Lorg/apache/http/client/methods/HttpPut;
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->token:Ljava/lang/String;

    if-eqz v0, :cond_122

    .line 325
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->urlString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->urlString:Ljava/lang/String;

    .line 326
    new-instance v7, Lorg/apache/http/client/methods/HttpPut;

    iget-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->urlString:Ljava/lang/String;

    invoke-direct {v7, v0}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    goto :goto_129

    .line 329
    :cond_122
    new-instance v6, Lorg/apache/http/client/methods/HttpPost;

    iget-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->urlString:Ljava/lang/String;

    invoke-direct {v6, v0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 332
    :goto_129
    const/4 v8, 0x0

    .line 333
    .local v8, "response":Lorg/apache/http/HttpResponse;
    if-eqz v7, :cond_150

    .line 334
    const-string v0, "Content-type"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "multipart/form-data; boundary="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v5}, Lnet/hockeyapp/android/utils/SimpleMultipartEntity;->getBoundary()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Lorg/apache/http/client/methods/HttpPut;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    invoke-virtual {v7, v5}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 336
    invoke-interface {p1, v7}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v8

    goto :goto_175

    .line 338
    :cond_150
    if-eqz v6, :cond_175

    .line 339
    const-string v0, "Content-type"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "multipart/form-data; boundary="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v5}, Lnet/hockeyapp/android/utils/SimpleMultipartEntity;->getBoundary()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    invoke-virtual {v6, v5}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 341
    invoke-interface {p1, v6}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v8

    .line 344
    :cond_175
    :goto_175
    if-eqz v8, :cond_1a4

    .line 345
    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v9

    .line 346
    .local v9, "resEntity":Lorg/apache/http/HttpEntity;
    const-string v0, "response"

    invoke-static {v9}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 347
    const-string v0, "status"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1a4
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_c .. :try_end_1a4} :catch_1a5
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_c .. :try_end_1a4} :catch_1aa
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_1a4} :catch_1af

    .line 358
    .end local v4    # "nameValuePairs":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    .end local v4
    .end local v5    # "entity":Lnet/hockeyapp/android/utils/SimpleMultipartEntity;
    .end local v6    # "httpPost":Lorg/apache/http/client/methods/HttpPost;
    .end local v7    # "httpPut":Lorg/apache/http/client/methods/HttpPut;
    .end local v8    # "response":Lorg/apache/http/HttpResponse;
    .end local v9    # "resEntity":Lorg/apache/http/HttpEntity;
    :cond_1a4
    goto :goto_1b3

    .line 350
    :catch_1a5
    move-exception v4

    .line 351
    .local v4, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v4}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 358
    .end local v4    # "e":Ljava/io/UnsupportedEncodingException;
    goto :goto_1b3

    .line 353
    :catch_1aa
    move-exception v4

    .line 354
    .local v4, "e":Lorg/apache/http/client/ClientProtocolException;
    invoke-virtual {v4}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    .line 358
    .end local v4    # "e":Lorg/apache/http/client/ClientProtocolException;
    goto :goto_1b3

    .line 356
    :catch_1af
    move-exception v4

    .line 357
    .local v4, "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 360
    .end local v4    # "e":Ljava/io/IOException;
    :goto_1b3
    return-object v3
.end method


# virtual methods
.method public detach()V
    .registers 2

    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->context:Landroid/content/Context;

    .line 134
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->progressDialog:Landroid/app/ProgressDialog;

    .line 135
    return-void
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 3

    .line 67
    move-object v0, p1

    check-cast v0, [Ljava/lang/Void;

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->doInBackground([Ljava/lang/Void;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/HashMap;
    .registers 12
    .param p1, "args"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([Ljava/lang/Void;)Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
        }
    .end annotation

    .line 151
    invoke-static {}, Lnet/hockeyapp/android/utils/ConnectionManager;->getInstance()Lnet/hockeyapp/android/utils/ConnectionManager;

    move-result-object v0

    invoke-virtual {v0}, Lnet/hockeyapp/android/utils/ConnectionManager;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v2

    .line 153
    .local v2, "httpclient":Lorg/apache/http/client/HttpClient;
    iget-boolean v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->isFetchMessages:Z

    if-eqz v0, :cond_15

    iget-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->token:Ljava/lang/String;

    if-eqz v0, :cond_15

    .line 155
    invoke-direct {p0, v2}, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->doGet(Lorg/apache/http/client/HttpClient;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0

    .line 157
    :cond_15
    iget-boolean v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->isFetchMessages:Z

    if-nez v0, :cond_65

    .line 162
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->attachmentUris:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 163
    invoke-direct {p0, v2}, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->doPostPut(Lorg/apache/http/client/HttpClient;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0

    .line 166
    :cond_26
    invoke-direct {p0, v2}, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->doPostPutWithAttachments(Lorg/apache/http/client/HttpClient;)Ljava/util/HashMap;

    move-result-object v3

    .line 169
    .local v3, "result":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "status"

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/String;

    .line 170
    .local v4, "status":Ljava/lang/String;
    if-eqz v4, :cond_64

    const-string v0, "2"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_64

    iget-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->context:Landroid/content/Context;

    if-eqz v0, :cond_64

    .line 171
    new-instance v5, Ljava/io/File;

    iget-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    const-string v1, "HockeyApp"

    invoke-direct {v5, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 172
    .local v5, "folder":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_64

    .line 173
    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    array-length v7, v6

    const/4 v8, 0x0

    :goto_5a
    if-ge v8, v7, :cond_64

    aget-object v9, v6, v8

    .line 174
    .local v9, "file":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->delete()Z

    .line 173
    .end local v9    # "file":Ljava/io/File;
    add-int/lit8 v8, v8, 0x1

    goto :goto_5a

    .line 179
    .end local v5    # "folder":Ljava/io/File;
    :cond_64
    return-object v3

    .line 183
    .end local v3    # "result":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3
    .end local v4    # "status":Ljava/lang/String;
    :cond_65
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .registers 3

    .line 67
    move-object v0, p1

    check-cast v0, Ljava/util/HashMap;

    invoke-virtual {p0, v0}, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->onPostExecute(Ljava/util/HashMap;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/HashMap;)V
    .registers 6
    .param p1, "result"    # Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;)V"
        }
    .end annotation

    .line 188
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_e

    .line 190
    :try_start_4
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_9} :catch_a

    .line 194
    goto :goto_e

    .line 192
    :catch_a
    move-exception v2

    .line 193
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 198
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_e
    :goto_e
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->handler:Landroid/os/Handler;

    if-eqz v0, :cond_55

    .line 199
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 200
    .local v2, "msg":Landroid/os/Message;
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 202
    .local v3, "bundle":Landroid/os/Bundle;
    if-eqz p1, :cond_46

    .line 203
    const-string v0, "request_type"

    const-string v1, "type"

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    const-string v0, "feedback_response"

    const-string v1, "response"

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const-string v0, "feedback_status"

    const-string v1, "status"

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4d

    .line 208
    :cond_46
    const-string v0, "request_type"

    const-string v1, "unknown"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    :goto_4d
    invoke-virtual {v2, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 213
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 215
    .end local v2    # "msg":Landroid/os/Message;
    .end local v3    # "bundle":Landroid/os/Bundle;
    :cond_55
    return-void
.end method

.method protected onPreExecute()V
    .registers 6

    .line 139
    const-string v4, "Sending feedback.."

    .line 140
    .local v4, "loadingMessage":Ljava/lang/String;
    iget-boolean v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->isFetchMessages:Z

    if-eqz v0, :cond_8

    .line 141
    const-string v4, "Retrieving discussions..."

    .line 144
    :cond_8
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_24

    :cond_14
    iget-boolean v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->showProgressDialog:Z

    if-eqz v0, :cond_24

    .line 145
    iget-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->context:Landroid/content/Context;

    const-string v1, ""

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v4, v2, v3}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lnet/hockeyapp/android/tasks/SendFeedbackTask;->progressDialog:Landroid/app/ProgressDialog;

    .line 147
    :cond_24
    return-void
.end method

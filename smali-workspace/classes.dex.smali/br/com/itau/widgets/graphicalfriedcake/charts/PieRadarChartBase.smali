.class public abstract Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;
.source "PieRadarChartBase.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData<+Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<+Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;>;>Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart<TT;>;"
    }
.end annotation


# instance fields
.field private mRawRotationAngle:F

.field protected mRotateEnabled:Z

.field private mRotationAngle:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 45
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;-><init>(Landroid/content/Context;)V

    .line 36
    const/high16 v0, 0x43870000    # 270.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mRotationAngle:F

    .line 39
    const/high16 v0, 0x43870000    # 270.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mRawRotationAngle:F

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mRotateEnabled:Z

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 49
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    const/high16 v0, 0x43870000    # 270.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mRotationAngle:F

    .line 39
    const/high16 v0, 0x43870000    # 270.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mRawRotationAngle:F

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mRotateEnabled:Z

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .line 53
    invoke-direct {p0, p1, p2, p3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    const/high16 v0, 0x43870000    # 270.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mRotationAngle:F

    .line 39
    const/high16 v0, 0x43870000    # 270.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mRawRotationAngle:F

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mRotateEnabled:Z

    .line 54
    return-void
.end method


# virtual methods
.method protected calcMinMax()V
    .registers 3

    .line 65
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getXVals()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mDeltaX:F

    .line 66
    return-void
.end method

.method public calculateOffsets()V
    .registers 20

    .line 100
    const/4 v4, 0x0

    .local v4, "legendLeft":F
    const/4 v5, 0x0

    .local v5, "legendRight":F
    const/4 v6, 0x0

    .local v6, "legendBottom":F
    const/4 v7, 0x0

    .line 102
    .local v7, "legendTop":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    if-eqz v0, :cond_1c9

    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1c9

    .line 104
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mNeededWidth:F

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 105
    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getChartWidth()F

    move-result v1

    move-object/from16 v2, p0

    iget-object v2, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getMaxSizePercent()F

    move-result v2

    mul-float/2addr v1, v2

    .line 104
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    .line 106
    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getFormSize()F

    move-result v1

    add-float/2addr v0, v1

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getFormToTextSpace()F

    move-result v1

    add-float v8, v0, v1

    .line 108
    .local v8, "fullLegendWidth":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->RIGHT_OF_CHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    if-ne v0, v1, :cond_58

    .line 111
    const/high16 v0, 0x41500000    # 13.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v9

    .line 113
    .local v9, "spacing":F
    add-float v5, v8, v9

    .line 115
    .end local v9    # "spacing":F
    goto/16 :goto_1ba

    :cond_58
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->RIGHT_OF_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    if-ne v0, v1, :cond_df

    .line 118
    const/high16 v0, 0x41000000    # 8.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v9

    .line 120
    .local v9, "spacing":F
    add-float v10, v8, v9

    .line 122
    .local v10, "legendWidth":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mNeededHeight:F

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    iget v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mTextHeightMax:F

    add-float v11, v0, v1

    .line 124
    .local v11, "legendHeight":F
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getCenter()Landroid/graphics/PointF;

    move-result-object v12

    .line 126
    .local v12, "c":Landroid/graphics/PointF;
    new-instance v13, Landroid/graphics/PointF;

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getWidth()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v10

    const/high16 v1, 0x41700000    # 15.0f

    add-float/2addr v0, v1

    const/high16 v1, 0x41700000    # 15.0f

    add-float/2addr v1, v11

    invoke-direct {v13, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 127
    .local v13, "bottomRight":Landroid/graphics/PointF;
    iget v0, v13, Landroid/graphics/PointF;->x:F

    iget v1, v13, Landroid/graphics/PointF;->y:F

    move-object/from16 v2, p0

    invoke-virtual {v2, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->distanceToCenter(FF)F

    move-result v14

    .line 129
    .local v14, "distLegend":F
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getRadius()F

    move-result v0

    iget v1, v13, Landroid/graphics/PointF;->x:F

    iget v2, v13, Landroid/graphics/PointF;->y:F

    .line 130
    move-object/from16 v3, p0

    invoke-virtual {v3, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getAngleForPoint(FF)F

    move-result v1

    .line 129
    move-object/from16 v2, p0

    invoke-virtual {v2, v12, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getPosition(Landroid/graphics/PointF;FF)Landroid/graphics/PointF;

    move-result-object v15

    .line 132
    .local v15, "reference":Landroid/graphics/PointF;
    iget v0, v15, Landroid/graphics/PointF;->x:F

    iget v1, v15, Landroid/graphics/PointF;->y:F

    move-object/from16 v2, p0

    invoke-virtual {v2, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->distanceToCenter(FF)F

    move-result v16

    .line 133
    .local v16, "distReference":F
    const/high16 v0, 0x40a00000    # 5.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v17

    .line 135
    .local v17, "min":F
    cmpg-float v0, v14, v16

    if-gez v0, :cond_c5

    .line 137
    sub-float v18, v16, v14

    .line 138
    .local v18, "diff":F
    add-float v5, v17, v18

    .line 141
    .end local v18    # "diff":F
    :cond_c5
    iget v0, v13, Landroid/graphics/PointF;->y:F

    iget v1, v12, Landroid/graphics/PointF;->y:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_dd

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v10

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getWidth()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_dd

    .line 142
    move v5, v10

    .line 145
    .end local v9    # "spacing":F
    .end local v10    # "legendWidth":F
    .end local v11    # "legendHeight":F
    .end local v12    # "c":Landroid/graphics/PointF;
    .end local v13    # "bottomRight":Landroid/graphics/PointF;
    .end local v14    # "distLegend":F
    .end local v15    # "reference":Landroid/graphics/PointF;
    .end local v16    # "distReference":F
    .end local v17    # "min":F
    :cond_dd
    goto/16 :goto_1ba

    :cond_df
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->LEFT_OF_CHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    if-ne v0, v1, :cond_f5

    .line 148
    const/high16 v0, 0x41500000    # 13.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v9

    .line 150
    .local v9, "spacing":F
    add-float v4, v8, v9

    .line 152
    .end local v9    # "spacing":F
    goto/16 :goto_1ba

    :cond_f5
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->LEFT_OF_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    if-ne v0, v1, :cond_176

    .line 155
    const/high16 v0, 0x41000000    # 8.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v9

    .line 157
    .local v9, "spacing":F
    add-float v10, v8, v9

    .line 159
    .local v10, "legendWidth":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mNeededHeight:F

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    iget v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mTextHeightMax:F

    add-float v11, v0, v1

    .line 161
    .local v11, "legendHeight":F
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getCenter()Landroid/graphics/PointF;

    move-result-object v12

    .line 163
    .local v12, "c":Landroid/graphics/PointF;
    new-instance v13, Landroid/graphics/PointF;

    const/high16 v0, 0x41700000    # 15.0f

    sub-float v0, v10, v0

    const/high16 v1, 0x41700000    # 15.0f

    add-float/2addr v1, v11

    invoke-direct {v13, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 164
    .local v13, "bottomLeft":Landroid/graphics/PointF;
    iget v0, v13, Landroid/graphics/PointF;->x:F

    iget v1, v13, Landroid/graphics/PointF;->y:F

    move-object/from16 v2, p0

    invoke-virtual {v2, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->distanceToCenter(FF)F

    move-result v14

    .line 166
    .local v14, "distLegend":F
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getRadius()F

    move-result v0

    iget v1, v13, Landroid/graphics/PointF;->x:F

    iget v2, v13, Landroid/graphics/PointF;->y:F

    .line 167
    move-object/from16 v3, p0

    invoke-virtual {v3, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getAngleForPoint(FF)F

    move-result v1

    .line 166
    move-object/from16 v2, p0

    invoke-virtual {v2, v12, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getPosition(Landroid/graphics/PointF;FF)Landroid/graphics/PointF;

    move-result-object v15

    .line 169
    .local v15, "reference":Landroid/graphics/PointF;
    iget v0, v15, Landroid/graphics/PointF;->x:F

    iget v1, v15, Landroid/graphics/PointF;->y:F

    move-object/from16 v2, p0

    invoke-virtual {v2, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->distanceToCenter(FF)F

    move-result v16

    .line 170
    .local v16, "distReference":F
    const/high16 v0, 0x40a00000    # 5.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v17

    .line 172
    .local v17, "min":F
    cmpg-float v0, v14, v16

    if-gez v0, :cond_15d

    .line 174
    sub-float v18, v16, v14

    .line 175
    .local v18, "diff":F
    add-float v4, v17, v18

    .line 178
    .end local v18    # "diff":F
    :cond_15d
    iget v0, v13, Landroid/graphics/PointF;->y:F

    iget v1, v12, Landroid/graphics/PointF;->y:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_175

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v10

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getWidth()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_175

    .line 179
    move v4, v10

    .line 182
    .end local v9    # "spacing":F
    .end local v10    # "legendWidth":F
    .end local v11    # "legendHeight":F
    .end local v12    # "c":Landroid/graphics/PointF;
    .end local v13    # "bottomLeft":Landroid/graphics/PointF;
    .end local v14    # "distLegend":F
    .end local v15    # "reference":Landroid/graphics/PointF;
    .end local v16    # "distReference":F
    .end local v17    # "min":F
    :cond_175
    goto :goto_1ba

    :cond_176
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->BELOW_CHART_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    if-eq v0, v1, :cond_19a

    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    .line 183
    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->BELOW_CHART_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    if-eq v0, v1, :cond_19a

    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    .line 184
    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    move-result-object v0

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->BELOW_CHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    if-ne v0, v1, :cond_1ba

    .line 186
    :cond_19a
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getRequiredBottomOffset()F

    move-result v9

    .line 187
    .local v9, "yOffset":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mNeededHeight:F

    add-float/2addr v0, v9

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getChartHeight()F

    move-result v1

    move-object/from16 v2, p0

    iget-object v2, v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getMaxSizePercent()F

    move-result v2

    mul-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v6

    .line 191
    .end local v9    # "yOffset":F
    :cond_1ba
    :goto_1ba
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getRequiredBaseOffset()F

    move-result v0

    add-float/2addr v4, v0

    .line 192
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getRequiredBaseOffset()F

    move-result v0

    add-float/2addr v5, v0

    .line 193
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getRequiredBaseOffset()F

    move-result v0

    add-float/2addr v7, v0

    .line 196
    .end local v8    # "fullLegendWidth":F
    :cond_1c9
    const/high16 v0, 0x41200000    # 10.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v8

    .line 198
    .local v8, "min":F
    move-object/from16 v0, p0

    instance-of v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    if-eqz v0, :cond_1f0

    .line 199
    move-object/from16 v0, p0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getXAxis()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;

    move-result-object v9

    .line 201
    .local v9, "x":Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;
    invoke-virtual {v9}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1f0

    .line 202
    const/high16 v0, 0x41200000    # 10.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iget v1, v9, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;->mLabelWidth:I

    int-to-float v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v8

    .line 206
    .end local v9    # "x":Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;
    :cond_1f0
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getExtraTopOffset()F

    move-result v0

    add-float/2addr v7, v0

    .line 207
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getExtraRightOffset()F

    move-result v0

    add-float/2addr v5, v0

    .line 208
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getExtraBottomOffset()F

    move-result v0

    add-float/2addr v6, v0

    .line 209
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getExtraLeftOffset()F

    move-result v0

    add-float/2addr v4, v0

    .line 211
    invoke-static {v8, v4}, Ljava/lang/Math;->max(FF)F

    move-result v9

    .line 212
    .local v9, "offsetLeft":F
    invoke-static {v8, v7}, Ljava/lang/Math;->max(FF)F

    move-result v10

    .line 213
    .local v10, "offsetTop":F
    invoke-static {v8, v5}, Ljava/lang/Math;->max(FF)F

    move-result v11

    .line 214
    .local v11, "offsetRight":F
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getRequiredBaseOffset()F

    move-result v0

    invoke-static {v0, v6}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v8, v0}, Ljava/lang/Math;->max(FF)F

    move-result v12

    .line 216
    .local v12, "offsetBottom":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0, v9, v10, v11, v12}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->restrainViewPort(FFFF)V

    .line 218
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mLogEnabled:Z

    if-eqz v0, :cond_25f

    .line 219
    const-string v0, "MPAndroidChart"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "offsetLeft: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", offsetTop: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", offsetRight: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", offsetBottom: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    :cond_25f
    return-void
.end method

.method public computeScroll()V
    .registers 2

    .line 80
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mChartTouchListener:Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/ChartTouchListener;

    instance-of v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;

    if-eqz v0, :cond_d

    .line 81
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mChartTouchListener:Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/ChartTouchListener;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->computeScroll()V

    .line 82
    :cond_d
    return-void
.end method

.method public distanceToCenter(FF)F
    .registers 13
    .param p1, "x"    # F
    .param p2, "y"    # F

    .line 281
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getCenterOffsets()Landroid/graphics/PointF;

    move-result-object v6

    .line 283
    .local v6, "c":Landroid/graphics/PointF;
    const/4 v7, 0x0

    .line 285
    .local v7, "dist":F
    const/4 v8, 0x0

    .line 286
    .local v8, "xDist":F
    const/4 v9, 0x0

    .line 288
    .local v9, "yDist":F
    iget v0, v6, Landroid/graphics/PointF;->x:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_12

    .line 289
    iget v0, v6, Landroid/graphics/PointF;->x:F

    sub-float v8, p1, v0

    goto :goto_16

    .line 291
    :cond_12
    iget v0, v6, Landroid/graphics/PointF;->x:F

    sub-float v8, v0, p1

    .line 294
    :goto_16
    iget v0, v6, Landroid/graphics/PointF;->y:F

    cmpl-float v0, p2, v0

    if-lez v0, :cond_21

    .line 295
    iget v0, v6, Landroid/graphics/PointF;->y:F

    sub-float v9, p2, v0

    goto :goto_25

    .line 297
    :cond_21
    iget v0, v6, Landroid/graphics/PointF;->y:F

    sub-float v9, v0, p2

    .line 301
    :goto_25
    float-to-double v0, v8

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    float-to-double v2, v9

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v7, v0

    .line 303
    return v7
.end method

.method public getAngleForPoint(FF)F
    .registers 17
    .param p1, "x"    # F
    .param p2, "y"    # F

    .line 234
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getCenterOffsets()Landroid/graphics/PointF;

    move-result-object v4

    .line 236
    .local v4, "c":Landroid/graphics/PointF;
    iget v0, v4, Landroid/graphics/PointF;->x:F

    sub-float v0, p1, v0

    float-to-double v5, v0

    .local v5, "tx":D
    iget v0, v4, Landroid/graphics/PointF;->y:F

    sub-float v0, p2, v0

    float-to-double v7, v0

    .line 237
    .local v7, "ty":D
    mul-double v0, v5, v5

    mul-double v2, v7, v7

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v9

    .line 238
    .local v9, "length":D
    div-double v0, v7, v9

    invoke-static {v0, v1}, Ljava/lang/Math;->acos(D)D

    move-result-wide v11

    .line 240
    .local v11, "r":D
    invoke-static {v11, v12}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    double-to-float v13, v0

    .line 242
    .local v13, "angle":F
    iget v0, v4, Landroid/graphics/PointF;->x:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_2c

    .line 243
    const/high16 v0, 0x43b40000    # 360.0f

    sub-float v13, v0, v13

    .line 246
    :cond_2c
    const/high16 v0, 0x42b40000    # 90.0f

    add-float/2addr v13, v0

    .line 249
    const/high16 v0, 0x43b40000    # 360.0f

    cmpl-float v0, v13, v0

    if-lez v0, :cond_38

    .line 250
    const/high16 v0, 0x43b40000    # 360.0f

    sub-float/2addr v13, v0

    .line 252
    :cond_38
    return v13
.end method

.method public getDiameter()F
    .registers 4

    .line 373
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getContentRect()Landroid/graphics/RectF;

    move-result-object v2

    .line 374
    .local v2, "content":Landroid/graphics/RectF;
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method public abstract getIndexForAngle(F)I
.end method

.method protected getPosition(Landroid/graphics/PointF;FF)Landroid/graphics/PointF;
    .registers 12
    .param p1, "center"    # Landroid/graphics/PointF;
    .param p2, "dist"    # F
    .param p3, "angle"    # F

    .line 266
    new-instance v7, Landroid/graphics/PointF;

    iget v0, p1, Landroid/graphics/PointF;->x:F

    float-to-double v0, v0

    float-to-double v2, p2

    float-to-double v4, p3

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    double-to-float v0, v0

    iget v1, p1, Landroid/graphics/PointF;->y:F

    float-to-double v1, v1

    float-to-double v3, p2

    float-to-double v5, p3

    .line 267
    invoke-static {v5, v6}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v5

    mul-double/2addr v3, v5

    add-double/2addr v1, v3

    double-to-float v1, v1

    invoke-direct {v7, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 268
    .local v7, "p":Landroid/graphics/PointF;
    return-object v7
.end method

.method public abstract getRadius()F
.end method

.method public getRawRotationAngle()F
    .registers 2

    .line 335
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mRawRotationAngle:F

    return v0
.end method

.method protected abstract getRequiredBaseOffset()F
.end method

.method protected abstract getRequiredBottomOffset()F
.end method

.method public getRotationAngle()F
    .registers 2

    .line 345
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mRotationAngle:F

    return v0
.end method

.method public getSelectionDetailsAtIndex(I)Ljava/util/List;
    .registers 7
    .param p1, "xIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;>;"
        }
    .end annotation

    .line 421
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 423
    .local v1, "vals":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_6
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getDataSetCount()I

    move-result v0

    if-ge v2, v0, :cond_2a

    .line 425
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getDataSetByIndex(I)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    move-result-object v3

    .line 428
    .local v3, "dataSet":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    invoke-virtual {v3, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getYValForXIndex(I)F

    move-result v4

    .line 429
    .local v4, "yVal":F
    const/high16 v0, 0x7fc00000    # NaNf

    cmpl-float v0, v4, v0

    if-nez v0, :cond_1f

    .line 430
    goto :goto_27

    .line 432
    :cond_1f
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;

    invoke-direct {v0, v4, v2, v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;-><init>(FILbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 423
    .end local v3    # "dataSet":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
    .end local v3
    .end local v4    # "yVal":F
    :goto_27
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 435
    .end local v2    # "i":I
    :cond_2a
    return-object v1
.end method

.method public getYChartMax()F
    .registers 2

    .line 402
    const/4 v0, 0x0

    return v0
.end method

.method public getYChartMin()F
    .registers 2

    .line 408
    const/4 v0, 0x0

    return v0
.end method

.method protected init()V
    .registers 2

    .line 58
    invoke-super {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->init()V

    .line 60
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;

    invoke-direct {v0, p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mChartTouchListener:Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/ChartTouchListener;

    .line 61
    return-void
.end method

.method public isRotationEnabled()Z
    .registers 2

    .line 364
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mRotateEnabled:Z

    return v0
.end method

.method public notifyDataSetChanged()V
    .registers 3

    .line 86
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mDataNotSet:Z

    if-eqz v0, :cond_5

    .line 87
    return-void

    .line 89
    :cond_5
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->calcMinMax()V

    .line 91
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    if-eqz v0, :cond_13

    .line 92
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mLegendRenderer:Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mData:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->computeLegend(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;)V

    .line 94
    :cond_13
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->calculateOffsets()V

    .line 95
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 71
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mTouchEnabled:Z

    if-eqz v0, :cond_f

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mChartTouchListener:Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/ChartTouchListener;

    if-eqz v0, :cond_f

    .line 72
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mChartTouchListener:Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/ChartTouchListener;

    invoke-virtual {v0, p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/ChartTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 74
    :cond_f
    invoke-super {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setRotationAngle(F)V
    .registers 3
    .param p1, "angle"    # F

    .line 322
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mRawRotationAngle:F

    .line 323
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mRawRotationAngle:F

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getNormalizedAngle(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mRotationAngle:F

    .line 324
    return-void
.end method

.method public setRotationEnabled(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 355
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->mRotateEnabled:Z

    .line 356
    return-void
.end method

.method public spin(IFFLbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;)V
    .registers 9
    .param p1, "durationmillis"    # I
    .param p2, "fromangle"    # F
    .param p3, "toangle"    # F
    .param p4, "easing"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .line 453
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_7

    .line 454
    return-void

    .line 456
    :cond_7
    invoke-virtual {p0, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->setRotationAngle(F)V

    .line 458
    const-string v0, "rotationAngle"

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p2, v1, v2

    const/4 v2, 0x1

    aput p3, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 460
    .local v3, "spinAnimator":Landroid/animation/ObjectAnimator;
    int-to-long v0, p1

    invoke-virtual {v3, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 461
    invoke-static {p4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing;->getEasingFunctionFromOption(Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;)Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 463
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase$1;

    invoke-direct {v0, p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase$1;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;)V

    invoke-virtual {v3, v0}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 470
    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->start()V

    .line 471
    return-void
.end method

.class final Lcom/google/zxing/client/android/k;
.super Landroid/os/AsyncTask;


# instance fields
.field final synthetic a:Lcom/google/zxing/client/android/i;


# direct methods
.method private constructor <init>(Lcom/google/zxing/client/android/i;)V
    .registers 2

    iput-object p1, p0, Lcom/google/zxing/client/android/k;->a:Lcom/google/zxing/client/android/i;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/zxing/client/android/i;Lcom/google/zxing/client/android/j;)V
    .registers 3

    invoke-direct {p0, p1}, Lcom/google/zxing/client/android/k;-><init>(Lcom/google/zxing/client/android/i;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5

    const-wide/32 v0, 0x493e0

    :try_start_3
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    invoke-static {}, Lcom/google/zxing/client/android/i;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Finishing activity due to inactivity"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/zxing/client/android/k;->a:Lcom/google/zxing/client/android/i;

    invoke-static {v0}, Lcom/google/zxing/client/android/i;->b(Lcom/google/zxing/client/android/i;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V
    :try_end_18
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_18} :catch_19

    goto :goto_1a

    :catch_19
    move-exception v2

    :goto_1a
    const/4 v0, 0x0

    return-object v0
.end method

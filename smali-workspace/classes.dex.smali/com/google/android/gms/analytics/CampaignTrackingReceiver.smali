.class public Lcom/google/android/gms/analytics/CampaignTrackingReceiver;
.super Landroid/content/BroadcastReceiver;


# static fields
.field static zzOM:Lcom/google/android/gms/internal/zzrp;

.field static zzON:Ljava/lang/Boolean;

.field static zzqy:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/analytics/CampaignTrackingReceiver;->zzqy:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static zzY(Landroid/content/Context;)Z
    .registers 4

    invoke-static {p0}, Lcom/google/android/gms/common/internal/zzx;->zzz(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/analytics/CampaignTrackingReceiver;->zzON:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/google/android/gms/analytics/CampaignTrackingReceiver;->zzON:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_e
    const-class v0, Lcom/google/android/gms/analytics/CampaignTrackingReceiver;

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/analytics/internal/zzam;->zza(Landroid/content/Context;Ljava/lang/Class;Z)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/CampaignTrackingReceiver;->zzON:Ljava/lang/Boolean;

    return v2
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 16
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    invoke-static {p1}, Lcom/google/android/gms/analytics/internal/zzf;->zzaa(Landroid/content/Context;)Lcom/google/android/gms/analytics/internal/zzf;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/analytics/internal/zzf;->zzjm()Lcom/google/android/gms/analytics/internal/zzaf;

    move-result-object v4

    const-string v0, "referrer"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    const-string v0, "CampaignTrackingReceiver received"

    invoke-virtual {v4, v0, v6}, Lcom/google/android/gms/analytics/internal/zzaf;->zza(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "com.android.vending.INSTALL_REFERRER"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2b

    :cond_25
    const-string v0, "CampaignTrackingReceiver received unexpected intent without referrer extra"

    invoke-virtual {v4, v0}, Lcom/google/android/gms/analytics/internal/zzaf;->zzbg(Ljava/lang/String;)V

    return-void

    :cond_2b
    invoke-static {p1}, Lcom/google/android/gms/analytics/CampaignTrackingService;->zzZ(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_36

    const-string v0, "CampaignTrackingService not registered or disabled. Installation tracking not possible. See http://goo.gl/8Rd3yj for instructions."

    invoke-virtual {v4, v0}, Lcom/google/android/gms/analytics/internal/zzaf;->zzbg(Ljava/lang/String;)V

    :cond_36
    invoke-virtual {p0, v5}, Lcom/google/android/gms/analytics/CampaignTrackingReceiver;->zzaV(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/gms/analytics/internal/zzf;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkr()Z

    move-result v0

    if-eqz v0, :cond_49

    const-string v0, "Received unexpected installation campaign on package side"

    invoke-virtual {v4, v0}, Lcom/google/android/gms/analytics/internal/zzaf;->zzbh(Ljava/lang/String;)V

    return-void

    :cond_49
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/CampaignTrackingReceiver;->zziB()Ljava/lang/Class;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/gms/common/internal/zzx;->zzz(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9, p1, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "referrer"

    invoke-virtual {v9, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v10, Lcom/google/android/gms/analytics/CampaignTrackingReceiver;->zzqy:Ljava/lang/Object;

    monitor-enter v10

    :try_start_5d
    invoke-virtual {p1, v9}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_60
    .catchall {:try_start_5d .. :try_end_60} :catchall_88

    if-nez v7, :cond_64

    monitor-exit v10

    return-void

    :cond_64
    :try_start_64
    sget-object v0, Lcom/google/android/gms/analytics/CampaignTrackingReceiver;->zzOM:Lcom/google/android/gms/internal/zzrp;

    if-nez v0, :cond_78

    new-instance v0, Lcom/google/android/gms/internal/zzrp;

    const-string v1, "Analytics campaign WakeLock"

    const/4 v2, 0x1

    invoke-direct {v0, p1, v2, v1}, Lcom/google/android/gms/internal/zzrp;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/analytics/CampaignTrackingReceiver;->zzOM:Lcom/google/android/gms/internal/zzrp;

    sget-object v0, Lcom/google/android/gms/analytics/CampaignTrackingReceiver;->zzOM:Lcom/google/android/gms/internal/zzrp;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzrp;->setReferenceCounted(Z)V

    :cond_78
    sget-object v0, Lcom/google/android/gms/analytics/CampaignTrackingReceiver;->zzOM:Lcom/google/android/gms/internal/zzrp;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/zzrp;->acquire(J)V
    :try_end_7f
    .catch Ljava/lang/SecurityException; {:try_start_64 .. :try_end_7f} :catch_80
    .catchall {:try_start_64 .. :try_end_7f} :catchall_88

    goto :goto_86

    :catch_80
    move-exception v11

    const-string v0, "CampaignTrackingService service at risk of not starting. For more reliable installation campaign reports, add the WAKE_LOCK permission to your manifest. See http://goo.gl/8Rd3yj for instructions."

    :try_start_83
    invoke-virtual {v4, v0}, Lcom/google/android/gms/analytics/internal/zzaf;->zzbg(Ljava/lang/String;)V
    :try_end_86
    .catchall {:try_start_83 .. :try_end_86} :catchall_88

    :goto_86
    monitor-exit v10

    goto :goto_8b

    :catchall_88
    move-exception v12

    monitor-exit v10

    throw v12

    :goto_8b
    return-void
.end method

.method protected zzaV(Ljava/lang/String;)V
    .registers 2

    return-void
.end method

.method protected zziB()Ljava/lang/Class;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/lang/Class<+Lcom/google/android/gms/analytics/CampaignTrackingService;>;"
        }
    .end annotation

    const-class v0, Lcom/google/android/gms/analytics/CampaignTrackingService;

    return-object v0
.end method

.class public Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity$SectionsPagerAdapter;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "ProdutosActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SectionsPagerAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;Landroid/support/v4/app/FragmentManager;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;
    .param p2, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;

    .line 54
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity$SectionsPagerAdapter;->this$0:Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;

    .line 55
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 56
    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 3

    .line 60
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity$SectionsPagerAdapter;->this$0:Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .registers 4
    .param p1, "position"    # I

    .line 65
    new-instance v1, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment_;

    invoke-direct {v1}, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment_;-><init>()V

    .line 66
    .local v1, "widgetProdutosLimitesContratados":Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment_;
    invoke-virtual {v1, p1}, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment_;->setPosition(I)V

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity$SectionsPagerAdapter;->this$0:Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;->multiLimiteVO:Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;

    invoke-virtual {v1, v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/ProdutosLimitesContratadosFragment_;->setRetornoMultiLimite(Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;)V

    .line 68
    return-object v1
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .registers 4
    .param p1, "position"    # I

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity$SectionsPagerAdapter;->this$0:Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, p1

    return-object v0
.end method

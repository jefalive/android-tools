.class public final Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;
.super Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;
.source "SourceFile"


# static fields
.field private static final u:[B

.field private static v:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0xb

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->u:[B

    const/16 v0, 0x71

    sput v0, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->v:I

    return-void

    :array_e
    .array-data 1
        0x42t
        0x16t
        0x57t
        0x35t
        -0x3t
        0x9t
        -0x13t
        0x13t
        -0x2t
        -0x7t
        0x5t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 2

    .line 15
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;-><init>(Landroid/content/Context;)V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3

    .line 19
    invoke-direct {p0, p1, p2}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4

    .line 23
    invoke-direct {p0, p1, p2, p3}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    sget-object v5, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->u:[B

    add-int/lit8 p0, p0, 0x4

    new-instance v0, Ljava/lang/String;

    mul-int/lit8 p1, p1, 0x3

    rsub-int/lit8 p1, p1, 0x8

    const/4 v4, 0x0

    mul-int/lit8 p2, p2, 0x2

    rsub-int/lit8 p2, p2, 0x63

    new-array v1, p1, [B

    add-int/lit8 p1, p1, -0x1

    if-nez v5, :cond_1b

    move v2, p0

    move v3, p1

    :goto_17
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p2, v2, 0x2

    :cond_1b
    add-int/lit8 p0, p0, 0x1

    int-to-byte v2, p2

    aput-byte v2, v1, v4

    if-ne v4, p1, :cond_27

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_27
    move v2, p2

    aget-byte v3, v5, p0

    add-int/lit8 v4, v4, 0x1

    goto :goto_17
.end method


# virtual methods
.method a(Landroid/util/AttributeSet;)V
    .registers 5

    .line 32
    invoke-super {p0, p1}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->a(Landroid/util/AttributeSet;)V

    .line 34
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->g:Landroid/widget/EditText;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$string;->itausdkcore_view_token_type_key_edit_text_description:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 35
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->h:Landroid/widget/EditText;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$string;->itausdkcore_view_token_type_key_edit_text_description:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 36
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->f:Landroid/widget/Button;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$string;->itausdkcore_dialog_token_sms_resend_action_accessibility:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 37
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->e:Landroid/widget/Button;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$string;->itausdkcore_dialog_token_confirm_action_accessibility:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 38
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->t:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$string;->itausdkcore_view_token_type_key_description:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 39
    return-void
.end method

.method protected a(Ljava/lang/String;)V
    .registers 4

    .line 44
    sget-object v0, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->u:[B

    const/16 v1, 0x8

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v0, 0x1

    invoke-static {v0, v1, v1}, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 45
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->setVisibility(I)V

    .line 46
    return-void

    .line 49
    :cond_1a
    invoke-super {p0, p1}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->a(Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->i:Landroid/widget/TextView;

    sget v1, Lbr/com/itau/sdk/android/core_ui/R$string;->itausdkcore_unlock_warning_chaveiro:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 51
    return-void
.end method

.method protected getSelectionAction()Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;
    .registers 2

    .line 55
    sget-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->CHAVEIRO_SELECTED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    return-object v0
.end method

.method protected getSendAction()Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;
    .registers 2

    .line 60
    sget-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->CHAVEIRO_CONFIRMED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    return-object v0
.end method

.method getType()I
    .registers 2

    .line 65
    const/4 v0, 0x3

    return v0
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoFisicoResponseDTO;)V
    .registers 2

    .line 27
    invoke-virtual {p0, p1}, Lbr/com/itau/sdk/android/core_ui/token/ChaveiroTokenView;->a(Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;)V

    .line 28
    return-void
.end method

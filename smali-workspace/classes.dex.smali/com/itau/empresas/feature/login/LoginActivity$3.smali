.class Lcom/itau/empresas/feature/login/LoginActivity$3;
.super Ljava/lang/Object;
.source "LoginActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/login/LoginActivity;->efetuatrocaDeSenha(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/login/LoginActivity;

.field final synthetic val$dialog:Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

.field final synthetic val$extra:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/login/LoginActivity;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;Ljava/lang/String;)V
    .registers 4
    .param p1, "this$0"    # Lcom/itau/empresas/feature/login/LoginActivity;

    .line 430
    iput-object p1, p0, Lcom/itau/empresas/feature/login/LoginActivity$3;->this$0:Lcom/itau/empresas/feature/login/LoginActivity;

    iput-object p2, p0, Lcom/itau/empresas/feature/login/LoginActivity$3;->val$dialog:Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    iput-object p3, p0, Lcom/itau/empresas/feature/login/LoginActivity$3;->val$extra:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 5
    .param p1, "view"    # Landroid/view/View;

    .line 433
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity$3;->val$dialog:Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->dismiss()V

    .line 434
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity$3;->this$0:Lcom/itau/empresas/feature/login/LoginActivity;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/LoginActivity$3;->this$0:Lcom/itau/empresas/feature/login/LoginActivity;

    const v2, 0x7f0702c7

    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/login/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/LoginActivity;->analyticsHit(Ljava/lang/String;)V

    .line 436
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity$3;->this$0:Lcom/itau/empresas/feature/login/LoginActivity;

    invoke-static {v0}, Lcom/itau/empresas/ui/activity/GenericaActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;

    move-result-object v0

    const-class v1, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;

    .line 437
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;->fragmentClass(Ljava/lang/Class;)Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/login/LoginActivity$3;->val$extra:Ljava/lang/String;

    .line 438
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;->extra(Ljava/lang/String;Z)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;

    .line 439
    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 440
    return-void
.end method

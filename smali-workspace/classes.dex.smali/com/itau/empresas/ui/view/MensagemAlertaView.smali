.class public Lcom/itau/empresas/ui/view/MensagemAlertaView;
.super Landroid/widget/RelativeLayout;
.source "MensagemAlertaView.java"


# instance fields
.field application:Lcom/itau/empresas/CustomApplication;

.field corLaranja:I

.field imgWarning:Lcom/itau/empresas/ui/view/TextViewIcon;

.field linearCartaoAlerta:Landroid/widget/LinearLayout;

.field textoWarning:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 37
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    return-void
.end method


# virtual methods
.method afterViews()V
    .registers 4

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAlertaView;->imgWarning:Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 52
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/MensagemAlertaView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f07024d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 51
    invoke-static {v1}, Lcom/itau/empresas/ui/util/AtendimentoUtil;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/TextViewIcon;->setText(Ljava/lang/CharSequence;)V

    .line 53
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAlertaView;->linearCartaoAlerta:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/itau/empresas/ui/view/MensagemAlertaView;->corLaranja:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAlertaView;->textoWarning:Landroid/widget/TextView;

    const/high16 v1, 0x41600000    # 14.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 55
    return-void
.end method

.method public exibeWarning(Z)V
    .registers 3
    .param p1, "exibir"    # Z

    .line 58
    if-eqz p1, :cond_7

    .line 59
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/MensagemAlertaView;->setVisibility(I)V

    goto :goto_c

    .line 61
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/MensagemAlertaView;->setVisibility(I)V

    .line 63
    :goto_c
    return-void
.end method

.method public setMensagem(Ljava/lang/String;)V
    .registers 4
    .param p1, "mensagem"    # Ljava/lang/String;

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAlertaView;->textoWarning:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/itau/empresas/ui/util/AtendimentoUtil;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/ui/view/MensagemAlertaView;->linearCartaoAlerta:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 68
    return-void
.end method

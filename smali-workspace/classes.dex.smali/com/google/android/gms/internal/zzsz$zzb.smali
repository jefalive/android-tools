.class public final Lcom/google/android/gms/internal/zzsz$zzb;
.super Lcom/google/android/gms/internal/zzso;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/zzsz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "zzb"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/zzso<Lcom/google/android/gms/internal/zzsz$zzb;>;"
    }
.end annotation


# instance fields
.field public version:Ljava/lang/String;

.field public zzbuM:I

.field public zzbuN:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzso;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzsz$zzb;->zzJD()Lcom/google/android/gms/internal/zzsz$zzb;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1, "o"    # Ljava/lang/Object;

    if-ne p1, p0, :cond_4

    const/4 v0, 0x1

    return v0

    :cond_4
    instance-of v0, p1, Lcom/google/android/gms/internal/zzsz$zzb;

    if-nez v0, :cond_a

    const/4 v0, 0x0

    return v0

    :cond_a
    move-object v2, p1

    check-cast v2, Lcom/google/android/gms/internal/zzsz$zzb;

    iget v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuM:I

    iget v1, v2, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuM:I

    if-eq v0, v1, :cond_15

    const/4 v0, 0x0

    return v0

    :cond_15
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuN:Ljava/lang/String;

    if-nez v0, :cond_1f

    iget-object v0, v2, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuN:Ljava/lang/String;

    if-eqz v0, :cond_2b

    const/4 v0, 0x0

    return v0

    :cond_1f
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuN:Ljava/lang/String;

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2b

    const/4 v0, 0x0

    return v0

    :cond_2b
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->version:Ljava/lang/String;

    if-nez v0, :cond_35

    iget-object v0, v2, Lcom/google/android/gms/internal/zzsz$zzb;->version:Ljava/lang/String;

    if-eqz v0, :cond_41

    const/4 v0, 0x0

    return v0

    :cond_35
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->version:Ljava/lang/String;

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsz$zzb;->version:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_41

    const/4 v0, 0x0

    return v0

    :cond_41
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    if-eqz v0, :cond_4d

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzsq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5d

    :cond_4d
    iget-object v0, v2, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    if-eqz v0, :cond_59

    iget-object v0, v2, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzsq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5b

    :cond_59
    const/4 v0, 0x1

    goto :goto_5c

    :cond_5b
    const/4 v0, 0x0

    :goto_5c
    return v0

    :cond_5d
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzsq;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .registers 4

    const/16 v2, 0x11

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v2, v0, 0x20f

    mul-int/lit8 v0, v2, 0x1f

    iget v1, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuM:I

    add-int v2, v0, v1

    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuN:Ljava/lang/String;

    if-nez v1, :cond_1e

    const/4 v1, 0x0

    goto :goto_24

    :cond_1e
    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuN:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_24
    add-int v2, v0, v1

    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zzb;->version:Ljava/lang/String;

    if-nez v1, :cond_2e

    const/4 v1, 0x0

    goto :goto_34

    :cond_2e
    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zzb;->version:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_34
    add-int v2, v0, v1

    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    if-eqz v1, :cond_44

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zzsq;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_46

    :cond_44
    const/4 v1, 0x0

    goto :goto_4c

    :cond_46
    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zzsq;->hashCode()I

    move-result v1

    :goto_4c
    add-int v2, v0, v1

    return v2
.end method

.method public synthetic mergeFrom(Lcom/google/android/gms/internal/zzsm;)Lcom/google/android/gms/internal/zzsu;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/zzsz$zzb;->zzT(Lcom/google/android/gms/internal/zzsm;)Lcom/google/android/gms/internal/zzsz$zzb;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/android/gms/internal/zzsn;)V
    .registers 4
    .param p1, "output"    # Lcom/google/android/gms/internal/zzsn;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuM:I

    if-eqz v0, :cond_a

    iget v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuM:I

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzA(II)V

    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuN:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuN:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzn(ILjava/lang/String;)V

    :cond_1a
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->version:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2a

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->version:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzn(ILjava/lang/String;)V

    :cond_2a
    invoke-super {p0, p1}, Lcom/google/android/gms/internal/zzso;->writeTo(Lcom/google/android/gms/internal/zzsn;)V

    return-void
.end method

.method public zzJD()Lcom/google/android/gms/internal/zzsz$zzb;
    .registers 2

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuM:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuN:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->version:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuu:I

    return-object p0
.end method

.method public zzT(Lcom/google/android/gms/internal/zzsm;)Lcom/google/android/gms/internal/zzsz$zzb;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzIX()I

    move-result v1

    sparse-switch v1, :sswitch_data_2a

    goto :goto_9

    :sswitch_8
    return-object p0

    :goto_9
    invoke-virtual {p0, p1, v1}, Lcom/google/android/gms/internal/zzsz$zzb;->zza(Lcom/google/android/gms/internal/zzsm;I)Z

    move-result v0

    if-nez v0, :cond_28

    return-object p0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJb()I

    move-result v2

    packed-switch v2, :pswitch_data_3c

    goto :goto_1a

    :pswitch_18
    iput v2, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuM:I

    :goto_1a
    goto :goto_28

    :sswitch_1b
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuN:Ljava/lang/String;

    goto :goto_28

    :sswitch_22
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->version:Ljava/lang/String;

    :cond_28
    :goto_28
    goto/16 :goto_0

    :sswitch_data_2a
    .sparse-switch
        0x0 -> :sswitch_8
        0x8 -> :sswitch_10
        0x12 -> :sswitch_1b
        0x1a -> :sswitch_22
    .end sparse-switch

    :pswitch_data_3c
    .packed-switch 0x0
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
    .end packed-switch
.end method

.method protected zzz()I
    .registers 4

    invoke-super {p0}, Lcom/google/android/gms/internal/zzso;->zzz()I

    move-result v2

    iget v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuM:I

    if-eqz v0, :cond_10

    iget v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuM:I

    const/4 v1, 0x1

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzC(II)I

    move-result v0

    add-int/2addr v2, v0

    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuN:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_22

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->zzbuN:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzo(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v2, v0

    :cond_22
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->version:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_34

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzb;->version:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzo(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v2, v0

    :cond_34
    return v2
.end method

.class public final Luk/co/chrisjenx/calligraphy/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Luk/co/chrisjenx/calligraphy/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final ActionBar:[I

.field public static final ActionBarLayout:[I

.field public static final ActionBarLayout_android_layout_gravity:I = 0x0

.field public static final ActionBar_background:I = 0xa

.field public static final ActionBar_backgroundSplit:I = 0xc

.field public static final ActionBar_backgroundStacked:I = 0xb

.field public static final ActionBar_contentInsetEnd:I = 0x15

.field public static final ActionBar_contentInsetLeft:I = 0x16

.field public static final ActionBar_contentInsetRight:I = 0x17

.field public static final ActionBar_contentInsetStart:I = 0x14

.field public static final ActionBar_customNavigationLayout:I = 0xd

.field public static final ActionBar_displayOptions:I = 0x3

.field public static final ActionBar_divider:I = 0x9

.field public static final ActionBar_elevation:I = 0x1a

.field public static final ActionBar_height:I = 0x0

.field public static final ActionBar_hideOnContentScroll:I = 0x13

.field public static final ActionBar_homeAsUpIndicator:I = 0x1c

.field public static final ActionBar_homeLayout:I = 0xe

.field public static final ActionBar_icon:I = 0x7

.field public static final ActionBar_indeterminateProgressStyle:I = 0x10

.field public static final ActionBar_itemPadding:I = 0x12

.field public static final ActionBar_logo:I = 0x8

.field public static final ActionBar_navigationMode:I = 0x2

.field public static final ActionBar_popupTheme:I = 0x1b

.field public static final ActionBar_progressBarPadding:I = 0x11

.field public static final ActionBar_progressBarStyle:I = 0xf

.field public static final ActionBar_subtitle:I = 0x4

.field public static final ActionBar_subtitleTextStyle:I = 0x6

.field public static final ActionBar_title:I = 0x1

.field public static final ActionBar_titleTextStyle:I = 0x5

.field public static final ActionMenuItemView:[I

.field public static final ActionMenuItemView_android_minWidth:I = 0x0

.field public static final ActionMenuView:[I

.field public static final ActionMode:[I

.field public static final ActionMode_background:I = 0x3

.field public static final ActionMode_backgroundSplit:I = 0x4

.field public static final ActionMode_closeItemLayout:I = 0x5

.field public static final ActionMode_height:I = 0x0

.field public static final ActionMode_subtitleTextStyle:I = 0x2

.field public static final ActionMode_titleTextStyle:I = 0x1

.field public static final ActivityChooserView:[I

.field public static final ActivityChooserView_expandActivityOverflowButtonDrawable:I = 0x1

.field public static final ActivityChooserView_initialActivityCount:I = 0x0

.field public static final AlertDialog:[I

.field public static final AlertDialog_android_layout:I = 0x0

.field public static final AlertDialog_buttonPanelSideLayout:I = 0x1

.field public static final AlertDialog_listItemLayout:I = 0x5

.field public static final AlertDialog_listLayout:I = 0x2

.field public static final AlertDialog_multiChoiceItemLayout:I = 0x3

.field public static final AlertDialog_singleChoiceItemLayout:I = 0x4

.field public static final AppCompatTextView:[I

.field public static final AppCompatTextView_android_textAppearance:I = 0x0

.field public static final AppCompatTextView_textAllCaps:I = 0x1

.field public static final ButtonBarLayout:[I

.field public static final ButtonBarLayout_allowStacking:I = 0x0

.field public static final CompoundButton:[I

.field public static final CompoundButton_android_button:I = 0x0

.field public static final CompoundButton_buttonTint:I = 0x1

.field public static final CompoundButton_buttonTintMode:I = 0x2

.field public static final DrawerArrowToggle:[I

.field public static final DrawerArrowToggle_arrowHeadLength:I = 0x4

.field public static final DrawerArrowToggle_arrowShaftLength:I = 0x5

.field public static final DrawerArrowToggle_barLength:I = 0x6

.field public static final DrawerArrowToggle_color:I = 0x0

.field public static final DrawerArrowToggle_drawableSize:I = 0x2

.field public static final DrawerArrowToggle_gapBetweenBars:I = 0x3

.field public static final DrawerArrowToggle_spinBars:I = 0x1

.field public static final DrawerArrowToggle_thickness:I = 0x7

.field public static final LinearLayoutCompat:[I

.field public static final LinearLayoutCompat_Layout:[I

.field public static final LinearLayoutCompat_Layout_android_layout_gravity:I = 0x0

.field public static final LinearLayoutCompat_Layout_android_layout_height:I = 0x2

.field public static final LinearLayoutCompat_Layout_android_layout_weight:I = 0x3

.field public static final LinearLayoutCompat_Layout_android_layout_width:I = 0x1

.field public static final LinearLayoutCompat_android_baselineAligned:I = 0x2

.field public static final LinearLayoutCompat_android_baselineAlignedChildIndex:I = 0x3

.field public static final LinearLayoutCompat_android_gravity:I = 0x0

.field public static final LinearLayoutCompat_android_orientation:I = 0x1

.field public static final LinearLayoutCompat_android_weightSum:I = 0x4

.field public static final LinearLayoutCompat_divider:I = 0x5

.field public static final LinearLayoutCompat_dividerPadding:I = 0x8

.field public static final LinearLayoutCompat_measureWithLargestChild:I = 0x6

.field public static final LinearLayoutCompat_showDividers:I = 0x7

.field public static final ListPopupWindow:[I

.field public static final ListPopupWindow_android_dropDownHorizontalOffset:I = 0x0

.field public static final ListPopupWindow_android_dropDownVerticalOffset:I = 0x1

.field public static final MenuGroup:[I

.field public static final MenuGroup_android_checkableBehavior:I = 0x5

.field public static final MenuGroup_android_enabled:I = 0x0

.field public static final MenuGroup_android_id:I = 0x1

.field public static final MenuGroup_android_menuCategory:I = 0x3

.field public static final MenuGroup_android_orderInCategory:I = 0x4

.field public static final MenuGroup_android_visible:I = 0x2

.field public static final MenuItem:[I

.field public static final MenuItem_actionLayout:I = 0xe

.field public static final MenuItem_actionProviderClass:I = 0x10

.field public static final MenuItem_actionViewClass:I = 0xf

.field public static final MenuItem_android_alphabeticShortcut:I = 0x9

.field public static final MenuItem_android_checkable:I = 0xb

.field public static final MenuItem_android_checked:I = 0x3

.field public static final MenuItem_android_enabled:I = 0x1

.field public static final MenuItem_android_icon:I = 0x0

.field public static final MenuItem_android_id:I = 0x2

.field public static final MenuItem_android_menuCategory:I = 0x5

.field public static final MenuItem_android_numericShortcut:I = 0xa

.field public static final MenuItem_android_onClick:I = 0xc

.field public static final MenuItem_android_orderInCategory:I = 0x6

.field public static final MenuItem_android_title:I = 0x7

.field public static final MenuItem_android_titleCondensed:I = 0x8

.field public static final MenuItem_android_visible:I = 0x4

.field public static final MenuItem_showAsAction:I = 0xd

.field public static final MenuView:[I

.field public static final MenuView_android_headerBackground:I = 0x4

.field public static final MenuView_android_horizontalDivider:I = 0x2

.field public static final MenuView_android_itemBackground:I = 0x5

.field public static final MenuView_android_itemIconDisabledAlpha:I = 0x6

.field public static final MenuView_android_itemTextAppearance:I = 0x1

.field public static final MenuView_android_verticalDivider:I = 0x3

.field public static final MenuView_android_windowAnimationStyle:I = 0x0

.field public static final MenuView_preserveIconSpacing:I = 0x7

.field public static final PopupWindow:[I

.field public static final PopupWindowBackgroundState:[I

.field public static final PopupWindowBackgroundState_state_above_anchor:I = 0x0

.field public static final PopupWindow_android_popupBackground:I = 0x0

.field public static final PopupWindow_overlapAnchor:I = 0x2

.field public static final SearchView:[I

.field public static final SearchView_android_focusable:I = 0x0

.field public static final SearchView_android_imeOptions:I = 0x3

.field public static final SearchView_android_inputType:I = 0x2

.field public static final SearchView_android_maxWidth:I = 0x1

.field public static final SearchView_closeIcon:I = 0x8

.field public static final SearchView_commitIcon:I = 0xd

.field public static final SearchView_defaultQueryHint:I = 0x7

.field public static final SearchView_goIcon:I = 0x9

.field public static final SearchView_iconifiedByDefault:I = 0x5

.field public static final SearchView_layout:I = 0x4

.field public static final SearchView_queryBackground:I = 0xf

.field public static final SearchView_queryHint:I = 0x6

.field public static final SearchView_searchHintIcon:I = 0xb

.field public static final SearchView_searchIcon:I = 0xa

.field public static final SearchView_submitBackground:I = 0x10

.field public static final SearchView_suggestionRowLayout:I = 0xe

.field public static final SearchView_voiceIcon:I = 0xc

.field public static final Spinner:[I

.field public static final Spinner_android_dropDownWidth:I = 0x3

.field public static final Spinner_android_popupBackground:I = 0x1

.field public static final Spinner_android_prompt:I = 0x2

.field public static final Spinner_popupTheme:I = 0x4

.field public static final SwitchCompat:[I

.field public static final SwitchCompat_android_textOff:I = 0x1

.field public static final SwitchCompat_android_textOn:I = 0x0

.field public static final SwitchCompat_android_thumb:I = 0x2

.field public static final SwitchCompat_showText:I = 0xd

.field public static final SwitchCompat_splitTrack:I = 0xc

.field public static final SwitchCompat_switchMinWidth:I = 0xa

.field public static final SwitchCompat_switchPadding:I = 0xb

.field public static final SwitchCompat_switchTextAppearance:I = 0x9

.field public static final SwitchCompat_thumbTextPadding:I = 0x8

.field public static final SwitchCompat_track:I = 0x5

.field public static final TextAppearance:[I

.field public static final TextAppearance_android_shadowColor:I = 0x5

.field public static final TextAppearance_android_shadowDx:I = 0x6

.field public static final TextAppearance_android_shadowDy:I = 0x7

.field public static final TextAppearance_android_shadowRadius:I = 0x8

.field public static final TextAppearance_android_textColor:I = 0x3

.field public static final TextAppearance_android_textSize:I = 0x0

.field public static final TextAppearance_android_textStyle:I = 0x2

.field public static final TextAppearance_android_typeface:I = 0x1

.field public static final TextAppearance_textAllCaps:I = 0x9

.field public static final Toolbar:[I

.field public static final Toolbar_android_gravity:I = 0x0

.field public static final Toolbar_android_minHeight:I = 0x1

.field public static final Toolbar_collapseContentDescription:I = 0x17

.field public static final Toolbar_collapseIcon:I = 0x16

.field public static final Toolbar_contentInsetEnd:I = 0x6

.field public static final Toolbar_contentInsetLeft:I = 0x7

.field public static final Toolbar_contentInsetRight:I = 0x8

.field public static final Toolbar_contentInsetStart:I = 0x5

.field public static final Toolbar_logo:I = 0x4

.field public static final Toolbar_logoDescription:I = 0x1a

.field public static final Toolbar_maxButtonHeight:I = 0x14

.field public static final Toolbar_navigationContentDescription:I = 0x19

.field public static final Toolbar_navigationIcon:I = 0x18

.field public static final Toolbar_popupTheme:I = 0xb

.field public static final Toolbar_subtitle:I = 0x3

.field public static final Toolbar_subtitleTextAppearance:I = 0xd

.field public static final Toolbar_subtitleTextColor:I = 0x1c

.field public static final Toolbar_title:I = 0x2

.field public static final Toolbar_titleMarginBottom:I = 0x12

.field public static final Toolbar_titleMarginEnd:I = 0x10

.field public static final Toolbar_titleMarginStart:I = 0xf

.field public static final Toolbar_titleMarginTop:I = 0x11

.field public static final Toolbar_titleMargins:I = 0x13

.field public static final Toolbar_titleTextAppearance:I = 0xc

.field public static final Toolbar_titleTextColor:I = 0x1b

.field public static final View:[I

.field public static final ViewBackgroundHelper:[I

.field public static final ViewBackgroundHelper_android_background:I = 0x0

.field public static final ViewBackgroundHelper_backgroundTint:I = 0x1

.field public static final ViewBackgroundHelper_backgroundTintMode:I = 0x2

.field public static final ViewStubCompat:[I

.field public static final ViewStubCompat_android_id:I = 0x0

.field public static final ViewStubCompat_android_inflatedId:I = 0x2

.field public static final ViewStubCompat_android_layout:I = 0x1

.field public static final View_android_focusable:I = 0x1

.field public static final View_android_theme:I = 0x0

.field public static final View_paddingEnd:I = 0x3

.field public static final View_paddingStart:I = 0x2

.field public static final View_theme:I = 0x4


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 917
    const/16 v0, 0x1d

    new-array v0, v0, [I

    fill-array-data v0, :array_e0

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->ActionBar:[I

    .line 918
    const/4 v0, 0x1

    new-array v0, v0, [I

    fill-array-data v0, :array_11e

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->ActionBarLayout:[I

    .line 947
    const/4 v0, 0x1

    new-array v0, v0, [I

    fill-array-data v0, :array_124

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->ActionMenuItemView:[I

    .line 949
    const/4 v0, 0x0

    new-array v0, v0, [I

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->ActionMenuView:[I

    .line 950
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_12a

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->ActionMode:[I

    .line 957
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_13a

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->ActivityChooserView:[I

    .line 960
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_142

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->AlertDialog:[I

    .line 967
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_152

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->AppCompatTextView:[I

    .line 970
    const/4 v0, 0x1

    new-array v0, v0, [I

    fill-array-data v0, :array_15a

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->ButtonBarLayout:[I

    .line 972
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_160

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->CompoundButton:[I

    .line 976
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_16a

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->DrawerArrowToggle:[I

    .line 985
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_17e

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->LinearLayoutCompat:[I

    .line 986
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_194

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->LinearLayoutCompat_Layout:[I

    .line 1000
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_1a0

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->ListPopupWindow:[I

    .line 1003
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_1a8

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->MenuGroup:[I

    .line 1010
    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_1b8

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->MenuItem:[I

    .line 1028
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_1de

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->MenuView:[I

    .line 1037
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1f4

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->PopupWindow:[I

    .line 1038
    const/4 v0, 0x1

    new-array v0, v0, [I

    fill-array-data v0, :array_1fe

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->PopupWindowBackgroundState:[I

    .line 1042
    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_204

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->SearchView:[I

    .line 1060
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_22a

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->Spinner:[I

    .line 1065
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_238

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->SwitchCompat:[I

    .line 1076
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_258

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->TextAppearance:[I

    .line 1086
    const/16 v0, 0x1d

    new-array v0, v0, [I

    fill-array-data v0, :array_270

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->Toolbar:[I

    .line 1112
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_2ae

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->View:[I

    .line 1113
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_2bc

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->ViewBackgroundHelper:[I

    .line 1117
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_2c6

    sput-object v0, Luk/co/chrisjenx/calligraphy/R$styleable;->ViewStubCompat:[I

    return-void

    nop

    :array_e0
    .array-data 4
        0x7f010004
        0x7f010033
        0x7f010036
        0x7f010037
        0x7f010038
        0x7f010039
        0x7f01003a
        0x7f01003b
        0x7f01003c
        0x7f01003d
        0x7f01003e
        0x7f01003f
        0x7f010040
        0x7f010041
        0x7f010042
        0x7f010043
        0x7f010044
        0x7f010045
        0x7f010046
        0x7f010047
        0x7f010048
        0x7f010049
        0x7f01004a
        0x7f01004b
        0x7f01004c
        0x7f01004d
        0x7f01004e
        0x7f01004f
        0x7f010094
    .end array-data

    :array_11e
    .array-data 4
        0x10100b3
    .end array-data

    :array_124
    .array-data 4
        0x101013f
    .end array-data

    :array_12a
    .array-data 4
        0x7f010004
        0x7f010039
        0x7f01003a
        0x7f01003e
        0x7f010040
        0x7f010050
    .end array-data

    :array_13a
    .array-data 4
        0x7f010051
        0x7f010052
    .end array-data

    :array_142
    .array-data 4
        0x10100f2
        0x7f010056
        0x7f010057
        0x7f010058
        0x7f010059
        0x7f01005a
    .end array-data

    :array_152
    .array-data 4
        0x1010034
        0x7f010064
    .end array-data

    :array_15a
    .array-data 4
        0x7f0100e1
    .end array-data

    :array_160
    .array-data 4
        0x1010107
        0x7f010104
        0x7f010105
    .end array-data

    :array_16a
    .array-data 4
        0x7f010122
        0x7f010123
        0x7f010124
        0x7f010125
        0x7f010126
        0x7f010127
        0x7f010128
        0x7f010129
    .end array-data

    :array_17e
    .array-data 4
        0x10100af
        0x10100c4
        0x1010126
        0x1010127
        0x1010128
        0x7f01003d
        0x7f010152
        0x7f010153
        0x7f010154
    .end array-data

    :array_194
    .array-data 4
        0x10100b3
        0x10100f4
        0x10100f5
        0x1010181
    .end array-data

    :array_1a0
    .array-data 4
        0x10102ac
        0x10102ad
    .end array-data

    :array_1a8
    .array-data 4
        0x101000e
        0x10100d0
        0x1010194
        0x10101de
        0x10101df
        0x10101e0
    .end array-data

    :array_1b8
    .array-data 4
        0x1010002
        0x101000e
        0x10100d0
        0x1010106
        0x1010194
        0x10101de
        0x10101df
        0x10101e1
        0x10101e2
        0x10101e3
        0x10101e4
        0x10101e5
        0x101026f
        0x7f0101ba
        0x7f0101bb
        0x7f0101bc
        0x7f0101bd
    .end array-data

    :array_1de
    .array-data 4
        0x10100ae
        0x101012c
        0x101012d
        0x101012e
        0x101012f
        0x1010130
        0x1010131
        0x7f0101be
        0x7f0101bf
    .end array-data

    :array_1f4
    .array-data 4
        0x1010176
        0x10102c9
        0x7f0101c9
    .end array-data

    :array_1fe
    .array-data 4
        0x7f0101ca
    .end array-data

    :array_204
    .array-data 4
        0x10100da
        0x101011f
        0x1010220
        0x1010264
        0x7f0101da
        0x7f0101db
        0x7f0101dc
        0x7f0101dd
        0x7f0101de
        0x7f0101df
        0x7f0101e0
        0x7f0101e1
        0x7f0101e2
        0x7f0101e3
        0x7f0101e4
        0x7f0101e5
        0x7f0101e6
    .end array-data

    :array_22a
    .array-data 4
        0x10100b2
        0x1010176
        0x101017b
        0x1010262
        0x7f01004f
    .end array-data

    :array_238
    .array-data 4
        0x1010124
        0x1010125
        0x1010142
        0x7f010200
        0x7f010201
        0x7f010202
        0x7f010203
        0x7f010204
        0x7f010205
        0x7f010206
        0x7f010207
        0x7f010208
        0x7f010209
        0x7f01020a
    .end array-data

    :array_258
    .array-data 4
        0x1010095
        0x1010096
        0x1010097
        0x1010098
        0x101009a
        0x1010161
        0x1010162
        0x1010163
        0x1010164
        0x7f010064
    .end array-data

    :array_270
    .array-data 4
        0x10100af
        0x1010140
        0x7f010033
        0x7f010038
        0x7f01003c
        0x7f010048
        0x7f010049
        0x7f01004a
        0x7f01004b
        0x7f01004c
        0x7f01004d
        0x7f01004f
        0x7f010234
        0x7f010235
        0x7f010236
        0x7f010237
        0x7f010238
        0x7f010239
        0x7f01023a
        0x7f01023b
        0x7f01023c
        0x7f01023d
        0x7f01023e
        0x7f01023f
        0x7f010240
        0x7f010241
        0x7f010242
        0x7f010243
        0x7f010244
    .end array-data

    :array_2ae
    .array-data 4
        0x1010000
        0x10100da
        0x7f01025a
        0x7f01025b
        0x7f01025c
    .end array-data

    :array_2bc
    .array-data 4
        0x10100d4
        0x7f01025d
        0x7f01025e
    .end array-data

    :array_2c6
    .array-data 4
        0x10100d0
        0x10100f2
        0x10100f3
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 916
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public Lcom/itau/empresas/ui/view/CardAlertaView;
.super Landroid/widget/FrameLayout;
.source "CardAlertaView.java"


# instance fields
.field imagemCardInformativo:Landroid/widget/TextView;

.field llCardInformativo:Landroid/widget/LinearLayout;

.field textoCardInformativo:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 34
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 35
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/CardAlertaView;->init()V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/CardAlertaView;->init()V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/CardAlertaView;->init()V

    .line 46
    return-void
.end method

.method private init()V
    .registers 4

    .line 55
    new-instance v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/CardAlertaView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 57
    return-void
.end method


# virtual methods
.method public bind(ILjava/lang/String;Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;)V
    .registers 11
    .param p1, "quantidadePendencias"    # I
    .param p2, "textoCardAlerta"    # Ljava/lang/String;
    .param p3, "estado"    # Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    .line 61
    const-string v3, ""

    .line 62
    .local v3, "imagemCardAlerta":Ljava/lang/String;
    const-string v4, ""

    .line 63
    .local v4, "textoCardAlertaAcessibilidade":Ljava/lang/String;
    const-string v5, ""

    .line 65
    .local v5, "imagemCardAlertaAcessibilidade":Ljava/lang/String;
    const v0, 0x7f0c0016

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 67
    .local v6, "corBackground":Ljava/lang/Integer;
    sget-object v0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->POSSUI_PENDENCIAS:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    invoke-virtual {p3, v0}, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5f

    .line 68
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 70
    const v0, 0x7f0c0007

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CardAlertaView;->llCardInformativo:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 75
    const/4 v5, 0x0

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 77
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CardAlertaView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0704eb

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 82
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CardAlertaView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 83
    const v2, 0x7f0704ea

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_ba

    .line 85
    :cond_5f
    sget-object v0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->NAO_POSSUI_PENDENCIAS:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    invoke-virtual {p3, v0}, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a3

    .line 86
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CardAlertaView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f070289

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 88
    const v0, 0x7f0c014d

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 90
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CardAlertaView;->llCardInformativo:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 92
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CardAlertaView;->textoCardInformativo:Landroid/widget/TextView;

    .line 93
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CardAlertaView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c0022

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 92
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 94
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CardAlertaView;->imagemCardInformativo:Landroid/widget/TextView;

    .line 95
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CardAlertaView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c0022

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 94
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 98
    const-string v5, ""

    .line 99
    move-object v4, p2

    goto :goto_ba

    .line 101
    :cond_a3
    sget-object v0, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->SEM_CONEXAO:Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;

    invoke-virtual {p3, v0}, Lcom/itau/empresas/feature/pendencias/activity/PendenciasActivity$EstadosPendencias;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b3

    .line 102
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CardAlertaView;->llCardInformativo:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_ba

    .line 105
    :cond_b3
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CardAlertaView;->llCardInformativo:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 108
    :goto_ba
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CardAlertaView;->imagemCardInformativo:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CardAlertaView;->textoCardInformativo:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CardAlertaView;->llCardInformativo:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CardAlertaView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 114
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CardAlertaView;->imagemCardInformativo:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 115
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CardAlertaView;->textoCardInformativo:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 117
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_ef

    .line 118
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CardAlertaView;->textoCardInformativo:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/CardAlertaView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    goto :goto_f4

    .line 120
    :cond_ef
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/CardAlertaView;->sendAccessibilityEvent(I)V

    .line 122
    :goto_f4
    return-void
.end method

.method public configuraMargemWarning(IIII)V
    .registers 7
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .line 130
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CardAlertaView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    .line 131
    .local v1, "params":Landroid/support/v7/widget/RecyclerView$LayoutParams;
    invoke-virtual {v1, p1, p2, p3, p4}, Landroid/support/v7/widget/RecyclerView$LayoutParams;->setMargins(IIII)V

    .line 132
    invoke-virtual {p0, v1}, Lcom/itau/empresas/ui/view/CardAlertaView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 133
    return-void
.end method

.method public getTextoCardInformativo()Landroid/widget/TextView;
    .registers 2

    .line 125
    iget-object v0, p0, Lcom/itau/empresas/ui/view/CardAlertaView;->textoCardInformativo:Landroid/widget/TextView;

    return-object v0
.end method

.class Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$2;
.super Ljava/lang/Object;
.source "CardViewBalanceFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->showContent(Ljava/lang/Double;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;

.field final synthetic val$value:Ljava/lang/Double;


# direct methods
.method constructor <init>(Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;Ljava/lang/Double;)V
    .registers 3
    .param p1, "this$0"    # Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;

    .line 143
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$2;->this$0:Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;

    iput-object p2, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$2;->val$value:Ljava/lang/Double;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 3

    .line 146
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$2;->this$0:Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$2;->val$value:Ljava/lang/Double;

    # invokes: Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->setValueBalance(Ljava/lang/Double;)V
    invoke-static {v0, v1}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->access$100(Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;Ljava/lang/Double;)V

    .line 147
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$2;->this$0:Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;

    # invokes: Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->hideLoading()V
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->access$000(Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;)V

    .line 148
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$2;->this$0:Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;

    iget-object v0, v0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->relativeBalanceAccountBalanceContainer:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 149
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment$2;->this$0:Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;

    iget-object v0, v0, Lbr/com/itau/textovoz/ui/fragment/CardViewBalanceFragment;->textBalanceAccountError:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 150
    return-void
.end method

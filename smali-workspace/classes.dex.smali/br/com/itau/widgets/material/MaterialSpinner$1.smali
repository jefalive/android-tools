.class Lbr/com/itau/widgets/material/MaterialSpinner$1;
.super Ljava/lang/Object;
.source "MaterialSpinner.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/widgets/material/MaterialSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

.field final synthetic val$listener:Landroid/widget/AdapterView$OnItemSelectedListener;


# direct methods
.method constructor <init>(Lbr/com/itau/widgets/material/MaterialSpinner;Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .registers 3

    .line 561
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialSpinner$1;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    iput-object p2, p0, Lbr/com/itau/widgets/material/MaterialSpinner$1;->val$listener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 12
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/widget/AdapterView<*>;Landroid/view/View;IJ)V"
        }
    .end annotation

    .line 565
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$1;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # getter for: Lbr/com/itau/widgets/material/MaterialSpinner;->hint:Ljava/lang/CharSequence;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$000(Lbr/com/itau/widgets/material/MaterialSpinner;)Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$1;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # getter for: Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelText:Ljava/lang/CharSequence;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$100(Lbr/com/itau/widgets/material/MaterialSpinner;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_4c

    .line 566
    :cond_10
    if-eqz p3, :cond_26

    .line 567
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$1;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # getter for: Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelVisible:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$200(Lbr/com/itau/widgets/material/MaterialSpinner;)Z

    move-result v0

    if-nez v0, :cond_1f

    .line 568
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$1;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # invokes: Lbr/com/itau/widgets/material/MaterialSpinner;->showFloatingLabel()V
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$300(Lbr/com/itau/widgets/material/MaterialSpinner;)V

    .line 570
    :cond_1f
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$1;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    const/4 v1, 0x1

    # setter for: Lbr/com/itau/widgets/material/MaterialSpinner;->isSelected:Z
    invoke-static {v0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$402(Lbr/com/itau/widgets/material/MaterialSpinner;Z)Z

    goto :goto_39

    .line 572
    :cond_26
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$1;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # getter for: Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelVisible:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$200(Lbr/com/itau/widgets/material/MaterialSpinner;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 573
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$1;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # invokes: Lbr/com/itau/widgets/material/MaterialSpinner;->hideFloatingLabel()V
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$500(Lbr/com/itau/widgets/material/MaterialSpinner;)V

    .line 575
    :cond_33
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$1;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    const/4 v1, 0x0

    # setter for: Lbr/com/itau/widgets/material/MaterialSpinner;->isSelected:Z
    invoke-static {v0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$402(Lbr/com/itau/widgets/material/MaterialSpinner;Z)Z

    .line 577
    :goto_39
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$1;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # getter for: Lbr/com/itau/widgets/material/MaterialSpinner;->arrowColorSelected:I
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$600(Lbr/com/itau/widgets/material/MaterialSpinner;)I

    move-result v0

    iget-object v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner$1;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # getter for: Lbr/com/itau/widgets/material/MaterialSpinner;->arrowColor:I
    invoke-static {v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$700(Lbr/com/itau/widgets/material/MaterialSpinner;)I

    move-result v1

    if-eq v0, v1, :cond_4c

    .line 578
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$1;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->invalidate()V

    .line 582
    :cond_4c
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$1;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # getter for: Lbr/com/itau/widgets/material/MaterialSpinner;->lastPosition:I
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$800(Lbr/com/itau/widgets/material/MaterialSpinner;)I

    move-result v0

    if-eq p3, v0, :cond_62

    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$1;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # getter for: Lbr/com/itau/widgets/material/MaterialSpinner;->error:Ljava/lang/CharSequence;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$900(Lbr/com/itau/widgets/material/MaterialSpinner;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_62

    .line 583
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$1;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setError(Ljava/lang/CharSequence;)V

    .line 585
    :cond_62
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$1;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # setter for: Lbr/com/itau/widgets/material/MaterialSpinner;->lastPosition:I
    invoke-static {v0, p3}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$802(Lbr/com/itau/widgets/material/MaterialSpinner;I)I

    .line 587
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$1;->val$listener:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_81

    .line 588
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$1;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    # getter for: Lbr/com/itau/widgets/material/MaterialSpinner;->hint:Ljava/lang/CharSequence;
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->access$000(Lbr/com/itau/widgets/material/MaterialSpinner;)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_76

    add-int/lit8 v0, p3, -0x1

    goto :goto_77

    :cond_76
    move v0, p3

    :goto_77
    move p3, v0

    .line 589
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$1;->val$listener:Landroid/widget/AdapterView$OnItemSelectedListener;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemSelectedListener;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 591
    :cond_81
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .registers 3
    .param p1, "parent"    # Landroid/widget/AdapterView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/widget/AdapterView<*>;)V"
        }
    .end annotation

    .line 595
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$1;->val$listener:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_9

    .line 596
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$1;->val$listener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-interface {v0, p1}, Landroid/widget/AdapterView$OnItemSelectedListener;->onNothingSelected(Landroid/widget/AdapterView;)V

    .line 598
    :cond_9
    return-void
.end method

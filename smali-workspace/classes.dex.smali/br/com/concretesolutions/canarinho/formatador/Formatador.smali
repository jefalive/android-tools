.class public interface abstract Lbr/com/concretesolutions/canarinho/formatador/Formatador;
.super Ljava/lang/Object;
.source "Formatador.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;
    }
.end annotation


# static fields
.field public static final BOLETO:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

.field public static final CEP:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

.field public static final CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

.field public static final CPF:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

.field public static final CPF_CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

.field public static final LINHA_DIGITAVEL:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

.field public static final TELEFONE:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

.field public static final VALOR:Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;

.field public static final VALOR_COM_SIMBOLO:Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .line 15
    new-instance v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->CEP_FORMATADO:Ljava/util/regex/Pattern;

    const-string v2, "$1-$2"

    sget-object v3, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->CEP_DESFORMATADO:Ljava/util/regex/Pattern;

    const-string v4, "$1$2"

    invoke-direct {v0, v1, v2, v3, v4}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;-><init>(Ljava/util/regex/Pattern;Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)V

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CEP:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    .line 20
    new-instance v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->CPF_FORMATADO:Ljava/util/regex/Pattern;

    const-string v2, "$1.$2.$3-$4"

    sget-object v3, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->CPF_DESFORMATADO:Ljava/util/regex/Pattern;

    const-string v4, "$1$2$3$4"

    invoke-direct {v0, v1, v2, v3, v4}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;-><init>(Ljava/util/regex/Pattern;Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)V

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CPF:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    .line 25
    new-instance v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->CNPJ_FORMATADO:Ljava/util/regex/Pattern;

    const-string v2, "$1.$2.$3/$4-$5"

    sget-object v3, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->CNPJ_DESFORMATADO:Ljava/util/regex/Pattern;

    const-string v4, "$1$2$3$4$5"

    invoke-direct {v0, v1, v2, v3, v4}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;-><init>(Ljava/util/regex/Pattern;Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)V

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    .line 31
    invoke-static {}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;->getInstance()Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CPF_CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    .line 36
    const/4 v0, 0x0

    invoke-static {v0}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;->getInstance(Z)Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->VALOR:Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;

    .line 41
    const/4 v0, 0x1

    invoke-static {v0}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;->getInstance(Z)Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->VALOR_COM_SIMBOLO:Lbr/com/concretesolutions/canarinho/formatador/FormatadorValor;

    .line 46
    invoke-static {}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;->getInstance()Lbr/com/concretesolutions/canarinho/formatador/FormatadorBoleto;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->BOLETO:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    .line 51
    invoke-static {}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->getInstance()Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->TELEFONE:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    .line 56
    invoke-static {}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorLinhaDigitavel;->getInstance()Lbr/com/concretesolutions/canarinho/formatador/FormatadorLinhaDigitavel;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->LINHA_DIGITAVEL:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    return-void
.end method


# virtual methods
.method public abstract desformata(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract estaFormatado(Ljava/lang/String;)Z
.end method

.method public abstract formata(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract podeSerFormatado(Ljava/lang/String;)Z
.end method

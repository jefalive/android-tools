.class public Lorg/simpleframework/xml/util/Resolver;
.super Ljava/util/AbstractSet;
.source "Resolver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/simpleframework/xml/util/Resolver$1;,
        Lorg/simpleframework/xml/util/Resolver$Stack;,
        Lorg/simpleframework/xml/util/Resolver$Cache;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<M::Lorg/simpleframework/xml/util/Match;>Ljava/util/AbstractSet<TM;>;"
    }
.end annotation


# instance fields
.field protected final cache:Lorg/simpleframework/xml/util/Resolver$Cache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/simpleframework/xml/util/Resolver<TM;>.Cache;"
        }
    .end annotation
.end field

.field protected final stack:Lorg/simpleframework/xml/util/Resolver$Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/simpleframework/xml/util/Resolver<TM;>.Stack;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 3

    .line 82
    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    .line 83
    new-instance v0, Lorg/simpleframework/xml/util/Resolver$Stack;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/simpleframework/xml/util/Resolver$Stack;-><init>(Lorg/simpleframework/xml/util/Resolver;Lorg/simpleframework/xml/util/Resolver$1;)V

    iput-object v0, p0, Lorg/simpleframework/xml/util/Resolver;->stack:Lorg/simpleframework/xml/util/Resolver$Stack;

    .line 84
    new-instance v0, Lorg/simpleframework/xml/util/Resolver$Cache;

    invoke-direct {v0, p0}, Lorg/simpleframework/xml/util/Resolver$Cache;-><init>(Lorg/simpleframework/xml/util/Resolver;)V

    iput-object v0, p0, Lorg/simpleframework/xml/util/Resolver;->cache:Lorg/simpleframework/xml/util/Resolver$Cache;

    .line 85
    return-void
.end method

.method private match([CI[CI)Z
    .registers 7
    .param p1, "text"    # [C
    .param p2, "off"    # I
    .param p3, "wild"    # [C
    .param p4, "pos"    # I

    .line 251
    :cond_0
    array-length v0, p3

    if-ge p4, v0, :cond_63

    array-length v0, p1

    if-ge p2, v0, :cond_63

    .line 252
    aget-char v0, p3, p4

    const/16 v1, 0x2a

    if-ne v0, v1, :cond_4d

    .line 253
    :cond_c
    aget-char v0, p3, p4

    const/16 v1, 0x2a

    if-ne v0, v1, :cond_19

    .line 254
    add-int/lit8 p4, p4, 0x1

    array-length v0, p3

    if-lt p4, v0, :cond_c

    .line 255
    const/4 v0, 0x1

    return v0

    .line 257
    :cond_19
    aget-char v0, p3, p4

    const/16 v1, 0x3f

    if-ne v0, v1, :cond_26

    .line 258
    add-int/lit8 p4, p4, 0x1

    array-length v0, p3

    if-lt p4, v0, :cond_26

    .line 259
    const/4 v0, 0x1

    return v0

    .line 261
    :cond_26
    :goto_26
    array-length v0, p1

    if-ge p2, v0, :cond_48

    .line 262
    aget-char v0, p1, p2

    aget-char v1, p3, p4

    if-eq v0, v1, :cond_35

    aget-char v0, p3, p4

    const/16 v1, 0x3f

    if-ne v0, v1, :cond_45

    .line 263
    :cond_35
    add-int/lit8 v0, p4, -0x1

    aget-char v0, p3, v0

    const/16 v1, 0x3f

    if-eq v0, v1, :cond_48

    .line 264
    invoke-direct {p0, p1, p2, p3, p4}, Lorg/simpleframework/xml/util/Resolver;->match([CI[CI)Z

    move-result v0

    if-eqz v0, :cond_45

    .line 265
    const/4 v0, 0x1

    return v0

    .line 261
    :cond_45
    add-int/lit8 p2, p2, 0x1

    goto :goto_26

    .line 271
    :cond_48
    array-length v0, p1

    if-ne v0, p2, :cond_4d

    .line 272
    const/4 v0, 0x0

    return v0

    .line 274
    :cond_4d
    move v0, p2

    add-int/lit8 p2, p2, 0x1

    aget-char v0, p1, v0

    move v1, p4

    add-int/lit8 p4, p4, 0x1

    aget-char v1, p3, v1

    if-eq v0, v1, :cond_0

    .line 275
    add-int/lit8 v0, p4, -0x1

    aget-char v0, p3, v0

    const/16 v1, 0x3f

    if-eq v0, v1, :cond_0

    .line 276
    const/4 v0, 0x0

    return v0

    .line 279
    :cond_63
    array-length v0, p3

    if-ne v0, p4, :cond_6d

    .line 280
    array-length v0, p1

    if-ne v0, p2, :cond_6b

    const/4 v0, 0x1

    goto :goto_6c

    :cond_6b
    const/4 v0, 0x0

    :goto_6c
    return v0

    .line 282
    :cond_6d
    aget-char v0, p3, p4

    const/16 v1, 0x2a

    if-ne v0, v1, :cond_7a

    .line 283
    add-int/lit8 p4, p4, 0x1

    array-length v0, p3

    if-lt p4, v0, :cond_6d

    .line 284
    const/4 v0, 0x1

    return v0

    .line 286
    :cond_7a
    const/4 v0, 0x0

    return v0
.end method

.method private match([C[C)Z
    .registers 5
    .param p1, "text"    # [C
    .param p2, "wild"    # [C

    .line 235
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, p2, v1}, Lorg/simpleframework/xml/util/Resolver;->match([CI[CI)Z

    move-result v0

    return v0
.end method

.method private resolveAll(Ljava/lang/String;[C)Ljava/util/List;
    .registers 8
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "array"    # [C
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;[C)Ljava/util/List<TM;>;"
        }
    .end annotation

    .line 148
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 150
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<TM;>;"
    iget-object v0, p0, Lorg/simpleframework/xml/util/Resolver;->stack:Lorg/simpleframework/xml/util/Resolver$Stack;

    invoke-virtual {v0}, Lorg/simpleframework/xml/util/Resolver$Stack;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lorg/simpleframework/xml/util/Match;

    .line 151
    .local v3, "match":Lorg/simpleframework/xml/util/Match;, "TM;"
    invoke-interface {v3}, Lorg/simpleframework/xml/util/Match;->getPattern()Ljava/lang/String;

    move-result-object v4

    .line 153
    .local v4, "wild":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lorg/simpleframework/xml/util/Resolver;->match([C[C)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 154
    iget-object v0, p0, Lorg/simpleframework/xml/util/Resolver;->cache:Lorg/simpleframework/xml/util/Resolver$Cache;

    invoke-virtual {v0, p1, v1}, Lorg/simpleframework/xml/util/Resolver$Cache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    .end local v3    # "match":Lorg/simpleframework/xml/util/Match;, "TM;"
    .end local v3
    .end local v4    # "wild":Ljava/lang/String;
    :cond_2e
    goto :goto_b

    .line 158
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_2f
    return-object v1
.end method


# virtual methods
.method public bridge synthetic add(Ljava/lang/Object;)Z
    .registers 3
    .param p1, "x0"    # Ljava/lang/Object;

    .line 62
    move-object v0, p1

    check-cast v0, Lorg/simpleframework/xml/util/Match;

    invoke-virtual {p0, v0}, Lorg/simpleframework/xml/util/Resolver;->add(Lorg/simpleframework/xml/util/Match;)Z

    move-result v0

    return v0
.end method

.method public add(Lorg/simpleframework/xml/util/Match;)Z
    .registers 3
    .param p1, "match"    # Lorg/simpleframework/xml/util/Match;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;)Z"
        }
    .end annotation

    .line 172
    iget-object v0, p0, Lorg/simpleframework/xml/util/Resolver;->stack:Lorg/simpleframework/xml/util/Resolver$Stack;

    invoke-virtual {v0, p1}, Lorg/simpleframework/xml/util/Resolver$Stack;->push(Lorg/simpleframework/xml/util/Match;)V

    .line 173
    const/4 v0, 0x1

    return v0
.end method

.method public clear()V
    .registers 2

    .line 221
    iget-object v0, p0, Lorg/simpleframework/xml/util/Resolver;->cache:Lorg/simpleframework/xml/util/Resolver$Cache;

    invoke-virtual {v0}, Lorg/simpleframework/xml/util/Resolver$Cache;->clear()V

    .line 222
    iget-object v0, p0, Lorg/simpleframework/xml/util/Resolver;->stack:Lorg/simpleframework/xml/util/Resolver$Stack;

    invoke-virtual {v0}, Lorg/simpleframework/xml/util/Resolver$Stack;->clear()V

    .line 223
    return-void
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/Iterator<TM;>;"
        }
    .end annotation

    .line 185
    iget-object v0, p0, Lorg/simpleframework/xml/util/Resolver;->stack:Lorg/simpleframework/xml/util/Resolver$Stack;

    invoke-virtual {v0}, Lorg/simpleframework/xml/util/Resolver$Stack;->sequence()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public remove(Lorg/simpleframework/xml/util/Match;)Z
    .registers 3
    .param p1, "match"    # Lorg/simpleframework/xml/util/Match;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;)Z"
        }
    .end annotation

    .line 199
    iget-object v0, p0, Lorg/simpleframework/xml/util/Resolver;->cache:Lorg/simpleframework/xml/util/Resolver$Cache;

    invoke-virtual {v0}, Lorg/simpleframework/xml/util/Resolver$Cache;->clear()V

    .line 200
    iget-object v0, p0, Lorg/simpleframework/xml/util/Resolver;->stack:Lorg/simpleframework/xml/util/Resolver$Stack;

    invoke-virtual {v0, p1}, Lorg/simpleframework/xml/util/Resolver$Stack;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public resolve(Ljava/lang/String;)Lorg/simpleframework/xml/util/Match;
    .registers 4
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)TM;"
        }
    .end annotation

    .line 99
    iget-object v0, p0, Lorg/simpleframework/xml/util/Resolver;->cache:Lorg/simpleframework/xml/util/Resolver$Cache;

    invoke-virtual {v0, p1}, Lorg/simpleframework/xml/util/Resolver$Cache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/List;

    .line 101
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<TM;>;"
    if-nez v1, :cond_f

    .line 102
    invoke-virtual {p0, p1}, Lorg/simpleframework/xml/util/Resolver;->resolveAll(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 104
    :cond_f
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 105
    const/4 v0, 0x0

    return-object v0

    .line 107
    :cond_17
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/simpleframework/xml/util/Match;

    return-object v0
.end method

.method public resolveAll(Ljava/lang/String;)Ljava/util/List;
    .registers 5
    .param p1, "text"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)Ljava/util/List<TM;>;"
        }
    .end annotation

    .line 122
    iget-object v0, p0, Lorg/simpleframework/xml/util/Resolver;->cache:Lorg/simpleframework/xml/util/Resolver$Cache;

    invoke-virtual {v0, p1}, Lorg/simpleframework/xml/util/Resolver$Cache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/List;

    .line 124
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<TM;>;"
    if-eqz v1, :cond_c

    .line 125
    return-object v1

    .line 127
    :cond_c
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    .line 129
    .local v2, "array":[C
    if-nez v2, :cond_14

    .line 130
    const/4 v0, 0x0

    return-object v0

    .line 132
    :cond_14
    invoke-direct {p0, p1, v2}, Lorg/simpleframework/xml/util/Resolver;->resolveAll(Ljava/lang/String;[C)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .registers 2

    .line 211
    iget-object v0, p0, Lorg/simpleframework/xml/util/Resolver;->stack:Lorg/simpleframework/xml/util/Resolver$Stack;

    invoke-virtual {v0}, Lorg/simpleframework/xml/util/Resolver$Stack;->size()I

    move-result v0

    return v0
.end method

.class public Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;
.super Landroid/widget/BaseExpandableListAdapter;
.source "SaldoExpandableListAdapter.java"


# instance fields
.field private contexto:Landroid/content/Context;

.field private detalhesListClickListener:Landroid/view/View$OnClickListener;

.field private fecharClickListener:Landroid/view/View$OnClickListener;

.field private itensValores:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field private lisVO:Lcom/itau/empresas/api/model/LisVO;

.field private listaDadosCabecalho:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field private listaItensTipo:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
        }
    .end annotation
.end field

.field private opkey:Ljava/lang/String;

.field private sujeitoEncargos:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Ljava/util/HashMap;Ljava/util/List;Z)V
    .registers 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listaDadosCabecalho"    # Ljava/util/List;
    .param p3, "listaItensTipo"    # Ljava/util/HashMap;
    .param p4, "itensValores"    # Ljava/util/List;
    .param p5, "sujeitoEncargos"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;Ljava/util/List<Ljava/lang/String;>;Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;Ljava/util/List<Ljava/lang/String;>;Z)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->sujeitoEncargos:Z

    .line 39
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->contexto:Landroid/content/Context;

    .line 40
    iput-object p2, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->listaDadosCabecalho:Ljava/util/List;

    .line 41
    iput-object p3, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->listaItensTipo:Ljava/util/HashMap;

    .line 42
    iput-object p4, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->itensValores:Ljava/util/List;

    .line 43
    iput-boolean p5, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->sujeitoEncargos:Z

    .line 44
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;)Landroid/view/View$OnClickListener;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;

    .line 22
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->detalhesListClickListener:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;)Landroid/content/Context;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;

    .line 22
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->contexto:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;)Lcom/itau/empresas/api/model/LisVO;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;

    .line 22
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    return-object v0
.end method

.method private configuraLis(Landroid/widget/RelativeLayout;Landroid/view/View;Landroid/widget/TextView;)V
    .registers 8
    .param p1, "rlComposicao"    # Landroid/widget/RelativeLayout;
    .param p2, "linha"    # Landroid/view/View;
    .param p3, "textoComposicao"    # Landroid/widget/TextView;

    .line 146
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->contexto:Landroid/content/Context;

    const v1, 0x7f070519

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->contexto:Landroid/content/Context;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 149
    invoke-virtual {p3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 148
    const v2, 0x7f070088

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 147
    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 150
    const/4 v0, 0x0

    const/16 v1, 0xa

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 151
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 152
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 153
    new-instance v0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter$2;-><init>(Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;)V

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    return-void
.end method

.method private configuraMultilimite(Landroid/widget/RelativeLayout;Landroid/view/View;Landroid/widget/TextView;Landroid/view/View;)V
    .registers 9
    .param p1, "rlComposicao"    # Landroid/widget/RelativeLayout;
    .param p2, "linha"    # Landroid/view/View;
    .param p3, "textoComposicao"    # Landroid/widget/TextView;
    .param p4, "textoSujeitoEncargos"    # Landroid/view/View;

    .line 127
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->contexto:Landroid/content/Context;

    const v1, 0x7f07051a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->contexto:Landroid/content/Context;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    .line 130
    invoke-virtual {p3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 129
    const v2, 0x7f070088

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 128
    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 131
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->sujeitoEncargos:Z

    if-eqz v0, :cond_28

    const/4 v0, 0x0

    goto :goto_2a

    :cond_28
    const/16 v0, 0x8

    :goto_2a
    invoke-virtual {p4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 132
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 133
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 134
    new-instance v0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter$1;-><init>(Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;)V

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    return-void
.end method


# virtual methods
.method public getChild(II)Ljava/lang/Object;
    .registers 5
    .param p1, "groupPosition"    # I
    .param p2, "childPosititon"    # I

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->listaItensTipo:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->listaDadosCabecalho:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 68
    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 67
    return-object v0
.end method

.method public getChildId(II)J
    .registers 5
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .line 73
    int-to-long v0, p2

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 18
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I
    .param p3, "isLastChild"    # Z
    .param p4, "convertView"    # Landroid/view/View;
    .param p5, "parent"    # Landroid/view/ViewGroup;

    .line 80
    move-object/from16 v4, p4

    .line 82
    .local v4, "view":Landroid/view/View;
    if-nez v4, :cond_17

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->contexto:Landroid/content/Context;

    const-string v1, "layout_inflater"

    .line 84
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/view/LayoutInflater;

    .line 85
    .local v5, "infalInflater":Landroid/view/LayoutInflater;
    const v0, 0x7f030110

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 88
    .end local v5    # "infalInflater":Landroid/view/LayoutInflater;
    :cond_17
    const v0, 0x7f0e0546

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/TextView;

    .line 89
    .local v5, "textoTipoSaldo":Landroid/widget/TextView;
    const v0, 0x7f0e02d5

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/TextView;

    .line 90
    .line 91
    .local v6, "textoValorSaldo":Landroid/widget/TextView;
    const v0, 0x7f0e0561

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/widget/RelativeLayout;

    .line 92
    .local v7, "rlComposicao":Landroid/widget/RelativeLayout;
    const v0, 0x7f0e055d

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 93
    .local v8, "linhaTop":Landroid/view/View;
    const v0, 0x7f0e0560

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 94
    .local v9, "linha":Landroid/view/View;
    const v0, 0x7f0e0562

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/widget/TextView;

    .line 95
    .local v10, "textoComposicao":Landroid/widget/TextView;
    const v0, 0x7f0e055f

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .line 96
    .local v11, "textoSujeitoEncargos":Landroid/view/View;
    invoke-virtual {p0, p1, p2}, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->getChild(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->itensValores:Ljava/util/List;

    .line 99
    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->destacaValorMonetario(Ljava/lang/String;F)Landroid/text/SpannableString;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    if-nez p2, :cond_7d

    .line 102
    const/4 v0, 0x0

    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    .line 103
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/16 v3, 0xa

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_82

    .line 105
    :cond_7d
    const/16 v0, 0x8

    invoke-virtual {v8, v0}, Landroid/view/View;->setVisibility(I)V

    .line 108
    :goto_82
    const/16 v0, 0x8

    invoke-virtual {v9, v0}, Landroid/view/View;->setVisibility(I)V

    .line 109
    const/16 v0, 0x8

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 111
    if-eqz p3, :cond_b3

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->opkey:Ljava/lang/String;

    const-string v1, "lis"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9c

    .line 113
    invoke-direct {p0, v7, v9, v10}, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->configuraLis(Landroid/widget/RelativeLayout;Landroid/view/View;Landroid/widget/TextView;)V

    goto :goto_b3

    .line 114
    :cond_9c
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->opkey:Ljava/lang/String;

    const-string v1, "multiLimites"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_aa

    .line 115
    invoke-direct {p0, v7, v9, v10, v11}, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->configuraMultilimite(Landroid/widget/RelativeLayout;Landroid/view/View;Landroid/widget/TextView;Landroid/view/View;)V

    goto :goto_b3

    .line 117
    :cond_aa
    const/4 v0, 0x4

    invoke-virtual {v9, v0}, Landroid/view/View;->setVisibility(I)V

    .line 118
    const/16 v0, 0x8

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 122
    :cond_b3
    :goto_b3
    return-object v4
.end method

.method public getChildrenCount(I)I
    .registers 4
    .param p1, "groupPosition"    # I

    .line 166
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->listaItensTipo:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->listaDadosCabecalho:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getGroup(I)Ljava/lang/Object;
    .registers 3
    .param p1, "groupPosition"    # I

    .line 171
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->listaDadosCabecalho:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getGroupCount()I
    .registers 2

    .line 176
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->listaDadosCabecalho:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getGroupId(I)J
    .registers 4
    .param p1, "groupPosition"    # I

    .line 181
    int-to-long v0, p1

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 11
    .param p1, "groupPosition"    # I
    .param p2, "isExpanded"    # Z
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .line 187
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->getGroup(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/String;

    .line 188
    .local v2, "valor":Ljava/lang/String;
    move-object v3, p3

    .line 190
    .local v3, "view":Landroid/view/View;
    if-nez v3, :cond_1d

    .line 191
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->contexto:Landroid/content/Context;

    const-string v1, "layout_inflater"

    .line 192
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/view/LayoutInflater;

    .line 193
    .local v4, "infalInflater":Landroid/view/LayoutInflater;
    const v0, 0x7f030111

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 196
    .end local v4    # "infalInflater":Landroid/view/LayoutInflater;
    :cond_1d
    const v0, 0x7f0e0567

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/TextView;

    .line 197
    .local v4, "valorSaldo":Landroid/widget/TextView;
    invoke-static {v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->destacaValorMonetario(Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->fecharClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_41

    .line 200
    const v0, 0x7f0e0544

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/ImageView;

    .line 201
    .local v5, "icFechar":Landroid/widget/ImageView;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->fecharClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 204
    .end local v5    # "icFechar":Landroid/widget/ImageView;
    :cond_41
    return-object v3
.end method

.method public hasStableIds()Z
    .registers 2

    .line 209
    const/4 v0, 0x0

    return v0
.end method

.method public isChildSelectable(II)Z
    .registers 4
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .line 214
    const/4 v0, 0x1

    return v0
.end method

.method public setData(Ljava/util/List;Ljava/util/HashMap;Ljava/util/List;Ljava/lang/String;Z)V
    .registers 6
    .param p1, "listaDadosCabecalho"    # Ljava/util/List;
    .param p2, "listaItensTipo"    # Ljava/util/HashMap;
    .param p3, "itensValores"    # Ljava/util/List;
    .param p4, "opkey"    # Ljava/lang/String;
    .param p5, "sujeitoEncargos"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/String;>;Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;Ljava/util/List<Ljava/lang/String;>;Ljava/lang/String;Z)V"
        }
    .end annotation

    .line 49
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->listaDadosCabecalho:Ljava/util/List;

    .line 50
    iput-object p2, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->listaItensTipo:Ljava/util/HashMap;

    .line 51
    iput-object p3, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->itensValores:Ljava/util/List;

    .line 52
    iput-object p4, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->opkey:Ljava/lang/String;

    .line 53
    iput-boolean p5, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->sujeitoEncargos:Z

    .line 54
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->notifyDataSetChanged()V

    .line 55
    return-void
.end method

.method public setDetalhesLisClickListener(Landroid/view/View$OnClickListener;)V
    .registers 2
    .param p1, "detalhesListClickListener"    # Landroid/view/View$OnClickListener;

    .line 58
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->detalhesListClickListener:Landroid/view/View$OnClickListener;

    .line 59
    return-void
.end method

.method public setFecharClickListener(Landroid/view/View$OnClickListener;)V
    .registers 2
    .param p1, "fecharClickListener"    # Landroid/view/View$OnClickListener;

    .line 62
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->fecharClickListener:Landroid/view/View$OnClickListener;

    .line 63
    return-void
.end method

.method public setLisVO(Lcom/itau/empresas/api/model/LisVO;)V
    .registers 2
    .param p1, "lisVO"    # Lcom/itau/empresas/api/model/LisVO;

    .line 218
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    .line 219
    return-void
.end method

.class Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$8;
.super Ljava/lang/Object;
.source "EditTextLoginBarraUnica.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->aposCarregar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;


# direct methods
.method constructor <init>(Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    .line 204
    iput-object p1, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$8;->this$0:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 4
    .param p1, "s"    # Landroid/text/Editable;

    .line 217
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$8;->this$0:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getConteudo()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_1f

    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$8;->this$0:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    # getter for: Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->listener:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;
    invoke-static {v0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->access$000(Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;)Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 218
    iget-object v0, p0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$8;->this$0:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    # getter for: Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->listener:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;
    invoke-static {v0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->access$000(Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;)Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;->preencheuAgenciaContaOuOperador()V

    .line 220
    :cond_1f
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .line 208
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .line 213
    return-void
.end method

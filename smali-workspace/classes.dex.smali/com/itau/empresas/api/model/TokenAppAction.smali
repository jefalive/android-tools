.class public final enum Lcom/itau/empresas/api/model/TokenAppAction;
.super Ljava/lang/Enum;
.source "TokenAppAction.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/itau/empresas/api/model/TokenAppAction;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/itau/empresas/api/model/TokenAppAction;

.field public static final enum ASSOCIAR:Lcom/itau/empresas/api/model/TokenAppAction;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "A"
    .end annotation
.end field

.field public static final enum BLOQUEADO:Lcom/itau/empresas/api/model/TokenAppAction;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "B"
    .end annotation
.end field

.field public static final enum CANCELAR:Lcom/itau/empresas/api/model/TokenAppAction;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "C"
    .end annotation
.end field

.field public static final enum DESCONHECIDO:Lcom/itau/empresas/api/model/TokenAppAction;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "N"
    .end annotation
.end field

.field public static final enum INSTALAR:Lcom/itau/empresas/api/model/TokenAppAction;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "I"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 6
    new-instance v0, Lcom/itau/empresas/api/model/TokenAppAction;

    const-string v1, "INSTALAR"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/api/model/TokenAppAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/api/model/TokenAppAction;->INSTALAR:Lcom/itau/empresas/api/model/TokenAppAction;

    .line 9
    new-instance v0, Lcom/itau/empresas/api/model/TokenAppAction;

    const-string v1, "ASSOCIAR"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/api/model/TokenAppAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/api/model/TokenAppAction;->ASSOCIAR:Lcom/itau/empresas/api/model/TokenAppAction;

    .line 12
    new-instance v0, Lcom/itau/empresas/api/model/TokenAppAction;

    const-string v1, "CANCELAR"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/api/model/TokenAppAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/api/model/TokenAppAction;->CANCELAR:Lcom/itau/empresas/api/model/TokenAppAction;

    .line 15
    new-instance v0, Lcom/itau/empresas/api/model/TokenAppAction;

    const-string v1, "BLOQUEADO"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/api/model/TokenAppAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/api/model/TokenAppAction;->BLOQUEADO:Lcom/itau/empresas/api/model/TokenAppAction;

    .line 18
    new-instance v0, Lcom/itau/empresas/api/model/TokenAppAction;

    const-string v1, "DESCONHECIDO"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/api/model/TokenAppAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/itau/empresas/api/model/TokenAppAction;->DESCONHECIDO:Lcom/itau/empresas/api/model/TokenAppAction;

    .line 5
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/itau/empresas/api/model/TokenAppAction;

    sget-object v1, Lcom/itau/empresas/api/model/TokenAppAction;->INSTALAR:Lcom/itau/empresas/api/model/TokenAppAction;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/api/model/TokenAppAction;->ASSOCIAR:Lcom/itau/empresas/api/model/TokenAppAction;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/api/model/TokenAppAction;->CANCELAR:Lcom/itau/empresas/api/model/TokenAppAction;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/api/model/TokenAppAction;->BLOQUEADO:Lcom/itau/empresas/api/model/TokenAppAction;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/itau/empresas/api/model/TokenAppAction;->DESCONHECIDO:Lcom/itau/empresas/api/model/TokenAppAction;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/api/model/TokenAppAction;->$VALUES:[Lcom/itau/empresas/api/model/TokenAppAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 5
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/itau/empresas/api/model/TokenAppAction;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 5
    const-class v0, Lcom/itau/empresas/api/model/TokenAppAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/TokenAppAction;

    return-object v0
.end method

.method public static values()[Lcom/itau/empresas/api/model/TokenAppAction;
    .registers 1

    .line 5
    sget-object v0, Lcom/itau/empresas/api/model/TokenAppAction;->$VALUES:[Lcom/itau/empresas/api/model/TokenAppAction;

    invoke-virtual {v0}, [Lcom/itau/empresas/api/model/TokenAppAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/itau/empresas/api/model/TokenAppAction;

    return-object v0
.end method

.class public Lcom/itau/empresas/feature/credito/giro/adapter/EmprestimosParceladosContratadosAdapter;
.super Landroid/widget/BaseAdapter;
.source "EmprestimosParceladosContratadosAdapter.java"


# instance fields
.field activity:Lcom/itau/empresas/ui/activity/BaseActivity;

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 20
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 2

    .line 32
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/EmprestimosParceladosContratadosAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 3
    .param p1, "position"    # I

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/EmprestimosParceladosContratadosAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .param p1, "position"    # I

    .line 40
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .line 45
    move-object v2, p2

    .line 47
    .local v2, "view":Landroid/view/View;
    if-nez v2, :cond_9

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/EmprestimosParceladosContratadosAdapter;->activity:Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView_;->build(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;

    move-result-object v2

    .line 52
    :cond_9
    move-object v0, v2

    :try_start_a
    check-cast v0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/adapter/EmprestimosParceladosContratadosAdapter;->items:Ljava/util/List;

    .line 53
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;->bind(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;)Landroid/view/View;
    :try_end_17
    .catch Ljava/text/ParseException; {:try_start_a .. :try_end_17} :catch_19

    move-result-object v0

    .line 52
    return-object v0

    .line 54
    :catch_19
    move-exception v3

    .line 55
    .local v3, "e":Ljava/text/ParseException;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Ljava/text/ParseException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    .end local v3    # "e":Ljava/text/ParseException;
    const/4 v0, 0x0

    return-object v0
.end method

.method public setData(Ljava/util/List;)V
    .registers 2
    .param p1, "lista"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;>;)V"
        }
    .end annotation

    .line 28
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/adapter/EmprestimosParceladosContratadosAdapter;->items:Ljava/util/List;

    .line 29
    return-void
.end method

.class public Lcom/itau/empresas/ui/util/MascaraValorMonetario;
.super Ljava/lang/Object;
.source "MascaraValorMonetario.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/util/MascaraValorMonetario$TextWatcherValorMonetario;,
        Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static formataStringValorMonetarioParaDecimal(Ljava/lang/String;)Ljava/math/BigDecimal;
    .registers 4
    .param p0, "valor"    # Ljava/lang/String;

    .line 27
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x2

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Ljava/math/BigDecimal;->setScale(II)Ljava/math/BigDecimal;

    move-result-object v0

    new-instance v1, Ljava/math/BigDecimal;

    const/16 v2, 0x64

    invoke-direct {v1, v2}, Ljava/math/BigDecimal;-><init>(I)V

    .line 28
    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;I)Ljava/math/BigDecimal;

    move-result-object v0

    .line 27
    return-object v0
.end method

.method public static insere(Landroid/widget/EditText;Ljava/lang/String;Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;Lcom/itau/empresas/ui/util/INotificarAlteracaoView;)Landroid/text/TextWatcher;
    .registers 5
    .param p0, "ediTxt"    # Landroid/widget/EditText;
    .param p1, "simboloValorMonetarioRegex"    # Ljava/lang/String;
    .param p2, "moeda"    # Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;
    .param p3, "notificarAlteracao"    # Lcom/itau/empresas/ui/util/INotificarAlteracaoView;

    .line 22
    new-instance v0, Lcom/itau/empresas/ui/util/MascaraValorMonetario$TextWatcherValorMonetario;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/itau/empresas/ui/util/MascaraValorMonetario$TextWatcherValorMonetario;-><init>(Landroid/widget/EditText;Ljava/lang/String;Lcom/itau/empresas/ui/util/MascaraValorMonetario$Moeda;Lcom/itau/empresas/ui/util/INotificarAlteracaoView;)V

    return-object v0
.end method

.method public static removeMascara(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .param p0, "expressaoRegular"    # Ljava/lang/String;
    .param p1, "valor"    # Ljava/lang/String;

    .line 17
    const-string v0, ""

    invoke-virtual {p1, p0, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

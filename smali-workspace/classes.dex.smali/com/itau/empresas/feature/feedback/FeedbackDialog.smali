.class public Lcom/itau/empresas/feature/feedback/FeedbackDialog;
.super Lcom/itau/empresas/ui/dialog/BaseDialog;
.source "FeedbackDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;,
        Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;
    }
.end annotation


# instance fields
.field private adapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter<Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;>;"
        }
    .end annotation
.end field

.field botaoAvaliar:Landroid/widget/Button;

.field campoComentario:Lbr/com/itau/widgets/material/MaterialEditText;

.field campoEmail:Lbr/com/itau/widgets/material/MaterialEditText;

.field controller:Lcom/itau/empresas/controller/FeedbackController;

.field feedbackEspontaneo:Ljava/lang/Boolean;

.field private feedbackEspontaneoFlag:Ljava/lang/Boolean;

.field private isBeta:Ljava/lang/Boolean;

.field llDialogFeedbackAvalie:Landroid/widget/LinearLayout;

.field llDialogFeedbackObrigado:Landroid/widget/LinearLayout;

.field llDialogFeedbackObrigadoBeta:Landroid/widget/LinearLayout;

.field llDialogFeedbackPreenchimentoCompleto:Landroid/widget/LinearLayout;

.field llFeedbackBetaCabecalho:Landroid/widget/LinearLayout;

.field private maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

.field root:Landroid/widget/LinearLayout;

.field spinnerCategoria:Lbr/com/itau/widgets/material/MaterialSpinner;

.field textoFeedbackMensagem:Landroid/widget/TextView;

.field private topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 50
    invoke-direct {p0}, Lcom/itau/empresas/ui/dialog/BaseDialog;-><init>()V

    .line 66
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->feedbackEspontaneo:Ljava/lang/Boolean;

    .line 71
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->isBeta:Ljava/lang/Boolean;

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/feedback/FeedbackDialog;Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 3
    .param p0, "x0"    # Lcom/itau/empresas/feature/feedback/FeedbackDialog;
    .param p1, "x1"    # Landroid/view/accessibility/AccessibilityEvent;

    .line 50
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->isHoverEnterOrExit(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/feedback/FeedbackDialog;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/feedback/FeedbackDialog;

    .line 50
    invoke-direct {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->escondeTeclado()V

    return-void
.end method

.method private baseActivity()Lcom/itau/empresas/ui/activity/BaseActivity;
    .registers 2

    .line 200
    invoke-virtual {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    return-object v0
.end method

.method private escondeTeclado()V
    .registers 4

    .line 423
    invoke-virtual {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    .line 424
    .local v2, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 425
    return-void
.end method

.method private escondeTecladoNaAcessibilidade(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 403
    new-instance v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog$1;-><init>(Lcom/itau/empresas/feature/feedback/FeedbackDialog;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 415
    return-void
.end method

.method private exibeMensagemDeErro(Ljava/lang/String;)V
    .registers 4
    .param p1, "erro"    # Ljava/lang/String;

    .line 246
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->root:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 247
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 248
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->DIALOGO_ERROR:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 249
    return-void
.end method

.method private inicializaMaquinaDeEstado()Lcom/itau/empresas/ui/util/MaquinaDeEstado;
    .registers 7

    .line 269
    new-instance v0, Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    invoke-direct {v0}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;-><init>()V

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->SEM_PREENCHIMENTO_TOTAL:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x3

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llDialogFeedbackPreenchimentoCompleto:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llDialogFeedbackObrigado:Landroid/widget/LinearLayout;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llDialogFeedbackObrigadoBeta:Landroid/widget/LinearLayout;

    const/4 v5, 0x2

    aput-object v4, v3, v5

    .line 272
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llDialogFeedbackAvalie:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 275
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visible([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 276
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 270
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->PREENCHIMENTO_TOTAL:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x3

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llDialogFeedbackAvalie:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llDialogFeedbackObrigado:Landroid/widget/LinearLayout;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llDialogFeedbackObrigadoBeta:Landroid/widget/LinearLayout;

    const/4 v5, 0x2

    aput-object v4, v3, v5

    .line 279
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llDialogFeedbackPreenchimentoCompleto:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 281
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visible([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 282
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 277
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->DIALOGO_OK:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x3

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llDialogFeedbackAvalie:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llDialogFeedbackPreenchimentoCompleto:Landroid/widget/LinearLayout;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llDialogFeedbackObrigadoBeta:Landroid/widget/LinearLayout;

    const/4 v5, 0x2

    aput-object v4, v3, v5

    .line 285
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llDialogFeedbackObrigado:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 287
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visible([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 288
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 283
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->DIALOGO_ERROR:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llDialogFeedbackObrigado:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 291
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 292
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 289
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->PREENCHIMENTO_TOTAL_BETA:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x4

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llDialogFeedbackAvalie:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llDialogFeedbackObrigado:Landroid/widget/LinearLayout;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llDialogFeedbackPreenchimentoCompleto:Landroid/widget/LinearLayout;

    const/4 v5, 0x2

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llDialogFeedbackObrigadoBeta:Landroid/widget/LinearLayout;

    const/4 v5, 0x3

    aput-object v4, v3, v5

    .line 295
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llDialogFeedbackPreenchimentoCompleto:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llFeedbackBetaCabecalho:Landroid/widget/LinearLayout;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    .line 298
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visible([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 300
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 293
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->DIALOGO_OK_BETA:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x3

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llDialogFeedbackAvalie:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llDialogFeedbackPreenchimentoCompleto:Landroid/widget/LinearLayout;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llDialogFeedbackObrigadoBeta:Landroid/widget/LinearLayout;

    const/4 v5, 0x2

    aput-object v4, v3, v5

    .line 303
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->llDialogFeedbackObrigadoBeta:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 305
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visible([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 306
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 301
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    .line 269
    return-object v0
.end method

.method private isHoverEnterOrExit(Landroid/view/accessibility/AccessibilityEvent;)Z
    .registers 4
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .line 418
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x80

    if-eq v0, v1, :cond_10

    .line 419
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x100

    if-ne v0, v1, :cond_12

    :cond_10
    const/4 v0, 0x1

    goto :goto_13

    :cond_12
    const/4 v0, 0x0

    .line 418
    :goto_13
    return v0
.end method

.method private ocultaMensagemDeErro()V
    .registers 3

    .line 240
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    if-eqz v0, :cond_9

    .line 241
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismiss()V

    .line 242
    :cond_9
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->PREENCHIMENTO_TOTAL:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 243
    return-void
.end method

.method private validaDadosOpcionais()Z
    .registers 4

    .line 189
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->campoEmail:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_26

    .line 190
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->campoEmail:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->validate()Z

    move-result v2

    .line 191
    .local v2, "emailValido":Z
    if-nez v2, :cond_26

    .line 192
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->campoEmail:Lbr/com/itau/widgets/material/MaterialEditText;

    const v1, 0x7f0701e8

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setError(Ljava/lang/CharSequence;)V

    .line 193
    const/4 v0, 0x0

    return v0

    .line 196
    .end local v2    # "emailValido":Z
    :cond_26
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method protected agoraNaoEventoGA()V
    .registers 3

    .line 228
    const v0, 0x7f0702ac

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 229
    const v1, 0x7f0702fb

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 228
    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    invoke-virtual {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->dismiss()V

    .line 231
    return-void
.end method

.method public aoInicializar()V
    .registers 6

    .line 88
    invoke-direct {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->inicializaMaquinaDeEstado()Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    .line 90
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->isBeta:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_16

    .line 91
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->PREENCHIMENTO_TOTAL_BETA:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    goto :goto_1d

    .line 93
    :cond_16
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->SEM_PREENCHIMENTO_TOTAL:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 96
    :goto_1d
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 97
    .local v4, "categorias":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;>;"
    new-instance v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;

    const v1, 0x7f0703c6

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 98
    const v2, 0x7f0703c7

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v0, v3, v1, v2}, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 97
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    new-instance v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;

    const v1, 0x7f0703ca

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 100
    const v2, 0x7f0703cb

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v0, v3, v1, v2}, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 99
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    new-instance v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;

    const v1, 0x7f0703ce

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 102
    const v2, 0x7f0703cf

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    invoke-direct {v0, v3, v1, v2}, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 101
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    new-instance v0, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;

    const v1, 0x7f0703d0

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 104
    const v2, 0x7f0703d1

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    invoke-direct {v0, v3, v1, v2}, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 103
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 106
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f030146

    invoke-direct {v0, v1, v2, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->adapter:Landroid/widget/ArrayAdapter;

    .line 107
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->adapter:Landroid/widget/ArrayAdapter;

    const v1, 0x7f030145

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 109
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->spinnerCategoria:Lbr/com/itau/widgets/material/MaterialSpinner;

    iget-object v1, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->adapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 110
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->campoEmail:Lbr/com/itau/widgets/material/MaterialEditText;

    new-instance v1, Lbr/com/itau/widgets/material/validation/RegexpValidator;

    const v2, 0x7f07016a

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    .line 111
    invoke-virtual {v3}, Ljava/util/regex/Pattern;->pattern()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lbr/com/itau/widgets/material/validation/RegexpValidator;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->addValidator(Lbr/com/itau/widgets/material/validation/METValidator;)Lbr/com/itau/widgets/material/MaterialEditText;

    .line 113
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->isBeta:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_c6

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_c6

    .line 114
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->textoFeedbackMensagem:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextAlignment(I)V

    .line 118
    :cond_c6
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->campoComentario:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->escondeTecladoNaAcessibilidade(Landroid/view/View;)V

    .line 119
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->campoEmail:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->escondeTecladoNaAcessibilidade(Landroid/view/View;)V

    .line 120
    return-void
.end method

.method protected enviar()V
    .registers 10

    .line 168
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->validaDadosObrigatorios(I)Z

    move-result v0

    if-eqz v0, :cond_54

    .line 169
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->spinnerCategoria:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getSelectedItemPosition()I

    move-result v6

    .line 170
    .local v6, "selectedIndex":I
    const/4 v7, 0x0

    .line 173
    .local v7, "codigoCategoria":I
    if-eqz v6, :cond_1d

    .line 174
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->spinnerCategoria:Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 175
    invoke-virtual {v0, v6}, Lbr/com/itau/widgets/material/MaterialSpinner;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;

    .line 176
    .local v8, "categoria":Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;
    invoke-virtual {v8}, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;->getCodigo()I

    move-result v7

    .line 179
    .end local v8    # "categoria":Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;
    :cond_1d
    invoke-direct {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->validaDadosOpcionais()Z

    move-result v0

    if-eqz v0, :cond_54

    .line 180
    invoke-direct {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->baseActivity()Lcom/itau/empresas/ui/activity/BaseActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->mostraDialogoDeProgresso()V

    .line 181
    invoke-direct {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->ocultaMensagemDeErro()V

    .line 182
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->controller:Lcom/itau/empresas/controller/FeedbackController;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    move v2, v7

    iget-object v3, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->campoEmail:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 183
    invoke-virtual {v3}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v3, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->campoComentario:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v3}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 182
    const/4 v3, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/itau/empresas/controller/FeedbackController;->enviaFeedback(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    .line 186
    .end local v6    # "selectedIndex":I
    .end local v7    # "codigoCategoria":I
    :cond_54
    return-void
.end method

.method protected falarSobre()V
    .registers 3

    .line 205
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->PREENCHIMENTO_TOTAL:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 206
    return-void
.end method

.method protected irParaLoja()V
    .registers 6

    .line 211
    const v0, 0x7f0702ac

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 212
    const v1, 0x7f07030d

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 211
    invoke-virtual {p0, v0, v1, v2}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->analyticsHit(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 215
    :try_start_17
    invoke-virtual {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    const-string v3, "market://details?id=com.itau.empresas"

    .line 217
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 215
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_2b
    .catch Landroid/content/ActivityNotFoundException; {:try_start_17 .. :try_end_2b} :catch_2c

    .line 222
    goto :goto_4a

    .line 218
    :catch_2c
    move-exception v4

    .line 219
    .local v4, "ex":Landroid/content/ActivityNotFoundException;
    invoke-virtual {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    const-string v3, "https://play.google.com/store/apps/details?id=com.itau.empresas"

    .line 220
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 219
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 221
    const-string v0, "FeedbackDialog"

    invoke-virtual {v4}, Landroid/content/ActivityNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 223
    .end local v4    # "ex":Landroid/content/ActivityNotFoundException;
    :goto_4a
    invoke-virtual {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->dismiss()V

    .line 224
    return-void
.end method

.method protected ok()V
    .registers 3

    .line 235
    const v0, 0x7f0702ac

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0702af

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    invoke-virtual {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->dismiss()V

    .line 237
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 76
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/dialog/BaseDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v2

    .line 77
    .local v2, "dialog":Landroid/app/Dialog;
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    .line 78
    .local v3, "window":Landroid/view/Window;
    if-eqz v3, :cond_17

    .line 79
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/view/Window;->requestFeature(I)Z

    .line 80
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v3, v0}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 82
    :cond_17
    return-object v2
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;)V
    .registers 6
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;

    .line 253
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v3

    .line 254
    .local v3, "exception":Lbr/com/itau/sdk/android/core/exception/BackendException;
    invoke-direct {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->baseActivity()Lcom/itau/empresas/ui/activity/BaseActivity;

    const-string v0, "ItauEmpresas"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Erro na Chamanda: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 256
    invoke-direct {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->baseActivity()Lcom/itau/empresas/ui/activity/BaseActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 257
    invoke-virtual {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0701f2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->exibeMensagemDeErro(Ljava/lang/String;)V

    .line 258
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 6
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 261
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v3

    .line 262
    .local v3, "exception":Lbr/com/itau/sdk/android/core/exception/BackendException;
    const-string v0, "ItauEmpresas"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Erro na Chamada:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 263
    invoke-direct {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->baseActivity()Lcom/itau/empresas/ui/activity/BaseActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 264
    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->exibeMensagemDeErro(Ljava/lang/String;)V

    .line 265
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/api/model/ResultadoFeedbackVO;)V
    .registers 7
    .param p1, "resultadoFeedback"    # Lcom/itau/empresas/api/model/ResultadoFeedbackVO;

    .line 379
    invoke-direct {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->ocultaMensagemDeErro()V

    .line 381
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 382
    .local v2, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v0, "feedback"

    const/4 v1, 0x0

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 383
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 385
    invoke-direct {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->baseActivity()Lcom/itau/empresas/ui/activity/BaseActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 387
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->spinnerCategoria:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getSelectedItemPosition()I

    move-result v3

    .line 389
    .local v3, "selectedIndex":I
    if-eqz v3, :cond_3c

    .line 390
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->spinnerCategoria:Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 391
    invoke-virtual {v0, v3}, Lbr/com/itau/widgets/material/MaterialSpinner;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;

    .line 392
    .local v4, "categoria":Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;
    const v0, 0x7f0702ac

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;->getDescricao()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    .end local v4    # "categoria":Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;
    :cond_3c
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->isBeta:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 396
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->DIALOGO_OK_BETA:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    goto :goto_53

    .line 398
    :cond_4c
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->DIALOGO_OK:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 400
    :goto_53
    return-void
.end method

.method public onStart()V
    .registers 2

    .line 124
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->register(Ljava/lang/Object;)V

    .line 125
    invoke-super {p0}, Lcom/itau/empresas/ui/dialog/BaseDialog;->onStart()V

    .line 126
    return-void
.end method

.method public onStop()V
    .registers 2

    .line 130
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    .line 131
    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->unregister(Ljava/lang/Object;)V

    .line 132
    invoke-super {p0}, Lcom/itau/empresas/ui/dialog/BaseDialog;->onStop()V

    .line 133
    return-void
.end method

.method public setBeta(Ljava/lang/Boolean;)V
    .registers 2
    .param p1, "beta"    # Ljava/lang/Boolean;

    .line 428
    iput-object p1, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->isBeta:Ljava/lang/Boolean;

    .line 429
    return-void
.end method

.method public setFeedbackEspontaneoFlag(Ljava/lang/Boolean;)V
    .registers 2
    .param p1, "feedbackEspontaneoFlag"    # Ljava/lang/Boolean;

    .line 432
    iput-object p1, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->feedbackEspontaneoFlag:Ljava/lang/Boolean;

    .line 433
    return-void
.end method

.method public validaDadosObrigatorios(I)Z
    .registers 10
    .param p1, "nota"    # I

    .line 136
    if-eqz p1, :cond_4

    const/4 v3, 0x1

    goto :goto_5

    :cond_4
    const/4 v3, 0x0

    .line 137
    .local v3, "notaValida":Z
    :goto_5
    const/4 v4, 0x1

    .line 138
    .local v4, "categoriaValida":Z
    const/4 v5, 0x1

    .line 140
    .local v5, "comentarioValido":Z
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->getEstadoAtual()Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->PREENCHIMENTO_TOTAL:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    if-eq v0, v1, :cond_1b

    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    .line 141
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->getEstadoAtual()Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;->PREENCHIMENTO_TOTAL_BETA:Lcom/itau/empresas/feature/feedback/FeedbackDialog$Estados;

    if-ne v0, v1, :cond_39

    .line 142
    :cond_1b
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->spinnerCategoria:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getSelectedItemPosition()I

    move-result v0

    if-eqz v0, :cond_25

    const/4 v4, 0x1

    goto :goto_26

    :cond_25
    const/4 v4, 0x0

    .line 143
    :goto_26
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->campoComentario:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_38

    const/4 v5, 0x1

    goto :goto_39

    :cond_38
    const/4 v5, 0x0

    .line 146
    :cond_39
    :goto_39
    if-nez v4, :cond_43

    .line 147
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->spinnerCategoria:Lbr/com/itau/widgets/material/MaterialSpinner;

    const v1, 0x7f0703cd

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setError(I)V

    .line 150
    :cond_43
    if-nez v5, :cond_70

    .line 151
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->spinnerCategoria:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getSelectedItemPosition()I

    move-result v6

    .line 152
    .local v6, "selectedIndex":I
    if-nez v6, :cond_5e

    .line 153
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->campoComentario:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 154
    invoke-virtual {p0}, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 155
    const v2, 0x7f07016d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 154
    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_70

    .line 157
    :cond_5e
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->spinnerCategoria:Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 158
    invoke-virtual {v0, v6}, Lbr/com/itau/widgets/material/MaterialSpinner;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;

    .line 159
    .local v7, "categoria":Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;
    iget-object v0, p0, Lcom/itau/empresas/feature/feedback/FeedbackDialog;->campoComentario:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v7}, Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;->getInvalido()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setError(Ljava/lang/CharSequence;)V

    .line 163
    .end local v6    # "selectedIndex":I
    .end local v7    # "categoria":Lcom/itau/empresas/feature/feedback/FeedbackDialog$Categoria;
    :cond_70
    :goto_70
    if-eqz v3, :cond_78

    if-eqz v4, :cond_78

    if-eqz v5, :cond_78

    const/4 v0, 0x1

    goto :goto_79

    :cond_78
    const/4 v0, 0x0

    :goto_79
    return v0
.end method

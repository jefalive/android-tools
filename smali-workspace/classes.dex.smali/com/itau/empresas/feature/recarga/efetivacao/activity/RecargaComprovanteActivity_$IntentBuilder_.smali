.class public Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;
.super Lorg/androidannotations/api/builder/ActivityIntentBuilder;
.source "RecargaComprovanteActivity_.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IntentBuilder_"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/androidannotations/api/builder/ActivityIntentBuilder<Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;>;"
    }
.end annotation


# instance fields
.field private fragmentSupport_:Landroid/support/v4/app/Fragment;

.field private fragment_:Landroid/app/Fragment;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 233
    const-class v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_;

    invoke-direct {p0, p1, v0}, Lorg/androidannotations/api/builder/ActivityIntentBuilder;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 234
    return-void
.end method


# virtual methods
.method public nome(Ljava/lang/String;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;
    .registers 3
    .param p1, "nome"    # Ljava/lang/String;

    .line 280
    const-string v0, "nome"

    invoke-super {p0, v0, p1}, Lorg/androidannotations/api/builder/ActivityIntentBuilder;->extra(Ljava/lang/String;Ljava/lang/String;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;

    return-object v0
.end method

.method public novo(Z)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;
    .registers 3
    .param p1, "novo"    # Z

    .line 290
    const-string v0, "novo"

    invoke-super {p0, v0, p1}, Lorg/androidannotations/api/builder/ActivityIntentBuilder;->extra(Ljava/lang/String;Z)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;

    return-object v0
.end method

.method public resultadoEfetivacaoRecargaVO(Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;
    .registers 3
    .param p1, "resultadoEfetivacaoRecargaVO"    # Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    .line 300
    const-string v0, "resultadoEfetivacaoRecargaVO"

    invoke-super {p0, v0, p1}, Lorg/androidannotations/api/builder/ActivityIntentBuilder;->extra(Ljava/lang/String;Ljava/io/Serializable;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;

    return-object v0
.end method

.method public startForResult(I)Lorg/androidannotations/api/builder/PostActivityStarter;
    .registers 6
    .param p1, "requestCode"    # I

    .line 248
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->fragmentSupport_:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_c

    .line 249
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->fragmentSupport_:Landroid/support/v4/app/Fragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_52

    .line 251
    :cond_c
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->fragment_:Landroid/app/Fragment;

    if-eqz v0, :cond_28

    .line 252
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_20

    .line 253
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->fragment_:Landroid/app/Fragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->lastOptions:Landroid/os/Bundle;

    invoke-virtual {v0, v1, p1, v2}, Landroid/app/Fragment;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    goto :goto_52

    .line 255
    :cond_20
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->fragment_:Landroid/app/Fragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v1, p1}, Landroid/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_52

    .line 258
    :cond_28
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->context:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_3b

    .line 259
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->context:Landroid/content/Context;

    move-object v3, v0

    check-cast v3, Landroid/app/Activity;

    .line 260
    .local v3, "activity":Landroid/app/Activity;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->lastOptions:Landroid/os/Bundle;

    invoke-static {v3, v0, p1, v1}, Landroid/support/v4/app/ActivityCompat;->startActivityForResult(Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 261
    .end local v3    # "activity":Landroid/app/Activity;
    goto :goto_52

    .line 262
    :cond_3b
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_4b

    .line 263
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->lastOptions:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    goto :goto_52

    .line 265
    :cond_4b
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 270
    :goto_52
    new-instance v0, Lorg/androidannotations/api/builder/PostActivityStarter;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaComprovanteActivity_$IntentBuilder_;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lorg/androidannotations/api/builder/PostActivityStarter;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

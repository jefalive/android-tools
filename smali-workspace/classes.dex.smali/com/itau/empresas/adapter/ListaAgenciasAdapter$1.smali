.class Lcom/itau/empresas/adapter/ListaAgenciasAdapter$1;
.super Landroid/widget/Filter;
.source "ListaAgenciasAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->getFilter()Landroid/widget/Filter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/adapter/ListaAgenciasAdapter;


# direct methods
.method constructor <init>(Lcom/itau/empresas/adapter/ListaAgenciasAdapter;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    .line 63
    iput-object p1, p0, Lcom/itau/empresas/adapter/ListaAgenciasAdapter$1;->this$0:Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .registers 9
    .param p1, "busca"    # Ljava/lang/CharSequence;

    .line 67
    new-instance v2, Landroid/widget/Filter$FilterResults;

    invoke-direct {v2}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 68
    .local v2, "resultado":Landroid/widget/Filter$FilterResults;
    iget-object v0, p0, Lcom/itau/empresas/adapter/ListaAgenciasAdapter$1;->this$0:Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    # getter for: Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->listaAgencias:Ljava/util/List;
    invoke-static {v0}, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->access$000(Lcom/itau/empresas/adapter/ListaAgenciasAdapter;)Ljava/util/List;

    move-result-object v3

    .line 69
    .local v3, "novaLista":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;"
    if-eqz p1, :cond_13

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1d

    .line 70
    :cond_13
    iput-object v3, v2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 71
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v2, Landroid/widget/Filter$FilterResults;->count:I

    goto/16 :goto_9d

    .line 73
    :cond_1d
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 74
    .local v4, "listaAgenciaFiltrada":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;"
    iget-object v0, p0, Lcom/itau/empresas/adapter/ListaAgenciasAdapter$1;->this$0:Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    # getter for: Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->listaAgencias:Ljava/util/List;
    invoke-static {v0}, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->access$000(Lcom/itau/empresas/adapter/ListaAgenciasAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2c
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_95

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/itau/empresas/api/model/AgenciaVO;

    .line 75
    .local v6, "a":Lcom/itau/empresas/api/model/AgenciaVO;
    invoke-virtual {v6}, Lcom/itau/empresas/api/model/AgenciaVO;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_91

    .line 76
    invoke-virtual {v6}, Lcom/itau/empresas/api/model/AgenciaVO;->getNome()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_91

    .line 78
    invoke-virtual {v6}, Lcom/itau/empresas/api/model/AgenciaVO;->getEndereco()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 79
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_91

    .line 80
    invoke-virtual {v6}, Lcom/itau/empresas/api/model/AgenciaVO;->getComplementoEndereco()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 81
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_94

    .line 82
    :cond_91
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    .end local v6    # "a":Lcom/itau/empresas/api/model/AgenciaVO;
    :cond_94
    goto :goto_2c

    .line 85
    :cond_95
    iput-object v4, v2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 86
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v2, Landroid/widget/Filter$FilterResults;->count:I

    .line 89
    .end local v4    # "listaAgenciaFiltrada":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;"
    .end local v4
    :goto_9d
    return-object v2
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .registers 6
    .param p1, "constraint"    # Ljava/lang/CharSequence;
    .param p2, "resultado"    # Landroid/widget/Filter$FilterResults;

    .line 95
    iget v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-lez v0, :cond_12

    .line 96
    iget-object v0, p0, Lcom/itau/empresas/adapter/ListaAgenciasAdapter$1;->this$0:Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    iget-object v1, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v1, Ljava/util/List;

    # setter for: Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->agencias:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->access$102(Lcom/itau/empresas/adapter/ListaAgenciasAdapter;Ljava/util/List;)Ljava/util/List;

    .line 97
    iget-object v0, p0, Lcom/itau/empresas/adapter/ListaAgenciasAdapter$1;->this$0:Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    invoke-virtual {v0}, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->notifyDataSetChanged()V

    .line 100
    :cond_12
    iget-object v0, p0, Lcom/itau/empresas/adapter/ListaAgenciasAdapter$1;->this$0:Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    # getter for: Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->access$200(Lcom/itau/empresas/adapter/ListaAgenciasAdapter;)Landroid/app/Activity;

    move-result-object v0

    .line 101
    const v1, 0x7f0e042d

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/TextView;

    .line 103
    .local v2, "textoSemAgencias":Landroid/widget/TextView;
    iget-object v0, p0, Lcom/itau/empresas/adapter/ListaAgenciasAdapter$1;->this$0:Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    iget-object v1, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v1, Ljava/util/List;

    # setter for: Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->agencias:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->access$102(Lcom/itau/empresas/adapter/ListaAgenciasAdapter;Ljava/util/List;)Ljava/util/List;

    .line 104
    iget-object v0, p0, Lcom/itau/empresas/adapter/ListaAgenciasAdapter$1;->this$0:Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    invoke-virtual {v0}, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->notifyDataSetChanged()V

    .line 106
    iget v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-lez v0, :cond_3a

    .line 107
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_52

    .line 109
    :cond_3a
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 110
    iget-object v0, p0, Lcom/itau/empresas/adapter/ListaAgenciasAdapter$1;->this$0:Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    .line 111
    # getter for: Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->access$200(Lcom/itau/empresas/adapter/ListaAgenciasAdapter;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 112
    const v1, 0x7f07046b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 110
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    :goto_52
    return-void
.end method

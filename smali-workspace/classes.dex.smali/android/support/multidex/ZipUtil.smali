.class final Landroid/support/multidex/ZipUtil;
.super Ljava/lang/Object;
.source "ZipUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/multidex/ZipUtil$CentralDirectory;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method static computeCrcOfCentralDir(Ljava/io/RandomAccessFile;Landroid/support/multidex/ZipUtil$CentralDirectory;)J
    .registers 9
    .param p0, "raf"    # Ljava/io/RandomAccessFile;
    .param p1, "dir"    # Landroid/support/multidex/ZipUtil$CentralDirectory;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 108
    new-instance v2, Ljava/util/zip/CRC32;

    invoke-direct {v2}, Ljava/util/zip/CRC32;-><init>()V

    .line 109
    .local v2, "crc":Ljava/util/zip/CRC32;
    iget-wide v3, p1, Landroid/support/multidex/ZipUtil$CentralDirectory;->size:J

    .line 110
    .local v3, "stillToRead":J
    iget-wide v0, p1, Landroid/support/multidex/ZipUtil$CentralDirectory;->offset:J

    invoke-virtual {p0, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 111
    const-wide/16 v0, 0x4000

    invoke-static {v0, v1, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v5, v0

    .line 112
    .local v5, "length":I
    const/16 v0, 0x4000

    new-array v6, v0, [B

    .line 113
    .local v6, "buffer":[B
    const/4 v0, 0x0

    invoke-virtual {p0, v6, v0, v5}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v5

    .line 114
    :goto_1c
    const/4 v0, -0x1

    if-eq v5, v0, :cond_39

    .line 115
    const/4 v0, 0x0

    invoke-virtual {v2, v6, v0, v5}, Ljava/util/zip/CRC32;->update([BII)V

    .line 116
    int-to-long v0, v5

    sub-long/2addr v3, v0

    .line 117
    const-wide/16 v0, 0x0

    cmp-long v0, v3, v0

    if-nez v0, :cond_2c

    .line 118
    goto :goto_39

    .line 120
    :cond_2c
    const-wide/16 v0, 0x4000

    invoke-static {v0, v1, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v5, v0

    .line 121
    const/4 v0, 0x0

    invoke-virtual {p0, v6, v0, v5}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v5

    goto :goto_1c

    .line 123
    :cond_39
    :goto_39
    invoke-virtual {v2}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v0

    return-wide v0
.end method

.method static findCentralDirectory(Ljava/io/RandomAccessFile;)Landroid/support/multidex/ZipUtil$CentralDirectory;
    .registers 11
    .param p0, "raf"    # Ljava/io/RandomAccessFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/util/zip/ZipException;
        }
    .end annotation

    .line 68
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x16

    sub-long v4, v0, v2

    .line 69
    .local v4, "scanOffset":J
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-gez v0, :cond_2b

    .line 70
    new-instance v0, Ljava/util/zip/ZipException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "File too short to be a zip file: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/zip/ZipException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_2b
    const-wide/32 v0, 0x10000

    sub-long v6, v4, v0

    .line 74
    .local v6, "stopOffset":J
    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-gez v0, :cond_38

    .line 75
    const-wide/16 v6, 0x0

    .line 78
    :cond_38
    const v0, 0x6054b50

    invoke-static {v0}, Ljava/lang/Integer;->reverseBytes(I)I

    move-result v8

    .line 80
    .local v8, "endSig":I
    :cond_3f
    invoke-virtual {p0, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 81
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v0

    if-ne v0, v8, :cond_49

    .line 82
    goto :goto_58

    .line 85
    :cond_49
    const-wide/16 v0, 0x1

    sub-long/2addr v4, v0

    .line 86
    cmp-long v0, v4, v6

    if-gez v0, :cond_3f

    .line 87
    new-instance v0, Ljava/util/zip/ZipException;

    const-string v1, "End Of Central Directory signature not found"

    invoke-direct {v0, v1}, Ljava/util/zip/ZipException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :goto_58
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 96
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 97
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 98
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Ljava/io/RandomAccessFile;->skipBytes(I)I

    .line 99
    new-instance v9, Landroid/support/multidex/ZipUtil$CentralDirectory;

    invoke-direct {v9}, Landroid/support/multidex/ZipUtil$CentralDirectory;-><init>()V

    .line 100
    .local v9, "dir":Landroid/support/multidex/ZipUtil$CentralDirectory;
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->reverseBytes(I)I

    move-result v0

    int-to-long v0, v0

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    iput-wide v0, v9, Landroid/support/multidex/ZipUtil$CentralDirectory;->size:J

    .line 101
    invoke-virtual {p0}, Ljava/io/RandomAccessFile;->readInt()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->reverseBytes(I)I

    move-result v0

    int-to-long v0, v0

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    iput-wide v0, v9, Landroid/support/multidex/ZipUtil$CentralDirectory;->offset:J

    .line 102
    return-object v9
.end method

.method static getZipCrc(Ljava/io/File;)J
    .registers 7
    .param p0, "apk"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 55
    new-instance v1, Ljava/io/RandomAccessFile;

    const-string v0, "r"

    invoke-direct {v1, p0, v0}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 57
    .local v1, "raf":Ljava/io/RandomAccessFile;
    :try_start_7
    invoke-static {v1}, Landroid/support/multidex/ZipUtil;->findCentralDirectory(Ljava/io/RandomAccessFile;)Landroid/support/multidex/ZipUtil$CentralDirectory;

    move-result-object v2

    .line 59
    .local v2, "dir":Landroid/support/multidex/ZipUtil$CentralDirectory;
    invoke-static {v1, v2}, Landroid/support/multidex/ZipUtil;->computeCrcOfCentralDir(Ljava/io/RandomAccessFile;Landroid/support/multidex/ZipUtil$CentralDirectory;)J
    :try_end_e
    .catchall {:try_start_7 .. :try_end_e} :catchall_13

    move-result-wide v3

    .line 61
    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V

    return-wide v3

    .end local v2    # "dir":Landroid/support/multidex/ZipUtil$CentralDirectory;
    :catchall_13
    move-exception v5

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V

    throw v5
.end method

.class final Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;
.super Ljava/lang/Object;
.source "DateTimeFormatterBuilder.java"

# interfaces
.implements Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/threeten/bp/format/DateTimeFormatterBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "OffsetIdPrinterParser"
.end annotation


# static fields
.field static final INSTANCE_ID:Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;

.field static final PATTERNS:[Ljava/lang/String;


# instance fields
.field private final noOffsetText:Ljava/lang/String;

.field private final type:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 3057
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "+HH"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "+HHmm"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "+HH:mm"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "+HHMM"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "+HH:MM"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "+HHMMss"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "+HH:MM:ss"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "+HHMMSS"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string v1, "+HH:MM:SS"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sput-object v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->PATTERNS:[Ljava/lang/String;

    .line 3060
    new-instance v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;

    const-string v1, "Z"

    const-string v2, "+HH:MM:ss"

    invoke-direct {v0, v1, v2}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->INSTANCE_ID:Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "noOffsetText"    # Ljava/lang/String;
    .param p2, "pattern"    # Ljava/lang/String;

    .line 3071
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3072
    const-string v0, "noOffsetText"

    invoke-static {p1, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 3073
    const-string v0, "pattern"

    invoke-static {p2, v0}, Lorg/threeten/bp/jdk8/Jdk8Methods;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 3074
    iput-object p1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->noOffsetText:Ljava/lang/String;

    .line 3075
    invoke-direct {p0, p2}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->checkPattern(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->type:I

    .line 3076
    return-void
.end method

.method private checkPattern(Ljava/lang/String;)I
    .registers 6
    .param p1, "pattern"    # Ljava/lang/String;

    .line 3079
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    sget-object v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->PATTERNS:[Ljava/lang/String;

    array-length v0, v0

    if-ge v3, v0, :cond_14

    .line 3080
    sget-object v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->PATTERNS:[Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 3081
    return v3

    .line 3079
    :cond_11
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 3084
    .end local v3    # "i":I
    :cond_14
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid zone offset pattern: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private parseNumber([IILjava/lang/CharSequence;Z)Z
    .registers 11
    .param p1, "array"    # [I
    .param p2, "arrayIndex"    # I
    .param p3, "parseText"    # Ljava/lang/CharSequence;
    .param p4, "required"    # Z

    .line 3171
    iget v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->type:I

    add-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x2

    if-ge v0, p2, :cond_a

    .line 3172
    const/4 v0, 0x0

    return v0

    .line 3174
    :cond_a
    const/4 v0, 0x0

    aget v2, p1, v0

    .line 3175
    .local v2, "pos":I
    iget v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->type:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_29

    const/4 v0, 0x1

    if-le p2, v0, :cond_29

    .line 3176
    add-int/lit8 v0, v2, 0x1

    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-gt v0, v1, :cond_26

    invoke-interface {p3, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x3a

    if-eq v0, v1, :cond_27

    .line 3177
    :cond_26
    return p4

    .line 3179
    :cond_27
    add-int/lit8 v2, v2, 0x1

    .line 3181
    :cond_29
    add-int/lit8 v0, v2, 0x2

    invoke-interface {p3}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-le v0, v1, :cond_32

    .line 3182
    return p4

    .line 3184
    :cond_32
    move v0, v2

    add-int/lit8 v2, v2, 0x1

    invoke-interface {p3, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    .line 3185
    .local v3, "ch1":C
    move v0, v2

    add-int/lit8 v2, v2, 0x1

    invoke-interface {p3, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    .line 3186
    .local v4, "ch2":C
    const/16 v0, 0x30

    if-lt v3, v0, :cond_50

    const/16 v0, 0x39

    if-gt v3, v0, :cond_50

    const/16 v0, 0x30

    if-lt v4, v0, :cond_50

    const/16 v0, 0x39

    if-le v4, v0, :cond_51

    .line 3187
    :cond_50
    return p4

    .line 3189
    :cond_51
    add-int/lit8 v0, v3, -0x30

    mul-int/lit8 v0, v0, 0xa

    add-int/lit8 v1, v4, -0x30

    add-int v5, v0, v1

    .line 3190
    .local v5, "value":I
    if-ltz v5, :cond_5f

    const/16 v0, 0x3b

    if-le v5, v0, :cond_60

    .line 3191
    :cond_5f
    return p4

    .line 3193
    :cond_60
    aput v5, p1, p2

    .line 3194
    const/4 v0, 0x0

    aput v2, p1, v0

    .line 3195
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public parse(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;I)I
    .registers 19
    .param p1, "context"    # Lorg/threeten/bp/format/DateTimeParseContext;
    .param p2, "text"    # Ljava/lang/CharSequence;
    .param p3, "position"    # I

    .line 3124
    invoke-interface/range {p2 .. p2}, Ljava/lang/CharSequence;->length()I

    move-result v8

    .line 3125
    .local v8, "length":I
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->noOffsetText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    .line 3126
    .local v9, "noOffsetLen":I
    if-nez v9, :cond_1f

    .line 3127
    move/from16 v0, p3

    if-ne v0, v8, :cond_45

    .line 3128
    move-object/from16 v0, p1

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->OFFSET_SECONDS:Lorg/threeten/bp/temporal/ChronoField;

    move/from16 v4, p3

    move/from16 v5, p3

    const-wide/16 v2, 0x0

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->setParsedField(Lorg/threeten/bp/temporal/TemporalField;JII)I

    move-result v0

    return v0

    .line 3131
    :cond_1f
    move/from16 v0, p3

    if-ne v0, v8, :cond_26

    .line 3132
    xor-int/lit8 v0, p3, -0x1

    return v0

    .line 3134
    :cond_26
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move/from16 v2, p3

    iget-object v3, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->noOffsetText:Ljava/lang/String;

    move v5, v9

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->subSequenceEquals(Ljava/lang/CharSequence;ILjava/lang/CharSequence;II)Z

    move-result v0

    if-eqz v0, :cond_45

    .line 3135
    move-object/from16 v0, p1

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->OFFSET_SECONDS:Lorg/threeten/bp/temporal/ChronoField;

    move/from16 v4, p3

    add-int v5, p3, v9

    const-wide/16 v2, 0x0

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->setParsedField(Lorg/threeten/bp/temporal/TemporalField;JII)I

    move-result v0

    return v0

    .line 3140
    :cond_45
    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v10

    .line 3141
    .local v10, "sign":C
    const/16 v0, 0x2b

    if-eq v10, v0, :cond_55

    const/16 v0, 0x2d

    if-ne v10, v0, :cond_b4

    .line 3143
    :cond_55
    const/16 v0, 0x2d

    if-ne v10, v0, :cond_5b

    const/4 v11, -0x1

    goto :goto_5c

    :cond_5b
    const/4 v11, 0x1

    .line 3144
    .local v11, "negative":I
    :goto_5c
    const/4 v0, 0x4

    new-array v12, v0, [I

    .line 3145
    .local v12, "array":[I
    add-int/lit8 v0, p3, 0x1

    const/4 v1, 0x0

    aput v0, v12, v1

    .line 3146
    const/4 v0, 0x1

    move-object/from16 v1, p2

    const/4 v2, 0x1

    invoke-direct {p0, v12, v0, v1, v2}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->parseNumber([IILjava/lang/CharSequence;Z)Z

    move-result v0

    if-nez v0, :cond_89

    iget v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->type:I

    const/4 v1, 0x3

    if-lt v0, v1, :cond_75

    const/4 v0, 0x1

    goto :goto_76

    :cond_75
    const/4 v0, 0x0

    :goto_76
    const/4 v1, 0x2

    move-object/from16 v2, p2

    invoke-direct {p0, v12, v1, v2, v0}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->parseNumber([IILjava/lang/CharSequence;Z)Z

    move-result v0

    if-nez v0, :cond_89

    const/4 v0, 0x3

    move-object/from16 v1, p2

    const/4 v2, 0x0

    invoke-direct {p0, v12, v0, v1, v2}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->parseNumber([IILjava/lang/CharSequence;Z)Z

    move-result v0

    if-eqz v0, :cond_8b

    :cond_89
    const/4 v0, 0x1

    goto :goto_8c

    :cond_8b
    const/4 v0, 0x0

    :goto_8c
    if-nez v0, :cond_b4

    .line 3150
    int-to-long v0, v11

    const/4 v2, 0x1

    aget v2, v12, v2

    int-to-long v2, v2

    const-wide/16 v4, 0xe10

    mul-long/2addr v2, v4

    const/4 v4, 0x2

    aget v4, v12, v4

    int-to-long v4, v4

    const-wide/16 v6, 0x3c

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    const/4 v4, 0x3

    aget v4, v12, v4

    int-to-long v4, v4

    add-long/2addr v2, v4

    mul-long v13, v0, v2

    .line 3151
    .local v13, "offsetSecs":J
    move-object/from16 v0, p1

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->OFFSET_SECONDS:Lorg/threeten/bp/temporal/ChronoField;

    move-wide v2, v13

    move/from16 v4, p3

    const/4 v5, 0x0

    aget v5, v12, v5

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->setParsedField(Lorg/threeten/bp/temporal/TemporalField;JII)I

    move-result v0

    return v0

    .line 3155
    .end local v11    # "negative":I
    .end local v12    # "array":[I
    .end local v13    # "offsetSecs":J
    :cond_b4
    if-nez v9, :cond_c5

    .line 3156
    move-object/from16 v0, p1

    sget-object v1, Lorg/threeten/bp/temporal/ChronoField;->OFFSET_SECONDS:Lorg/threeten/bp/temporal/ChronoField;

    move/from16 v4, p3

    add-int v5, p3, v9

    const-wide/16 v2, 0x0

    invoke-virtual/range {v0 .. v5}, Lorg/threeten/bp/format/DateTimeParseContext;->setParsedField(Lorg/threeten/bp/temporal/TemporalField;JII)I

    move-result v0

    return v0

    .line 3158
    :cond_c5
    xor-int/lit8 v0, p3, -0x1

    return v0
.end method

.method public print(Lorg/threeten/bp/format/DateTimePrintContext;Ljava/lang/StringBuilder;)Z
    .registers 13
    .param p1, "context"    # Lorg/threeten/bp/format/DateTimePrintContext;
    .param p2, "buf"    # Ljava/lang/StringBuilder;

    .line 3089
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->OFFSET_SECONDS:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {p1, v0}, Lorg/threeten/bp/format/DateTimePrintContext;->getValue(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v3

    .line 3090
    .local v3, "offsetSecs":Ljava/lang/Long;
    if-nez v3, :cond_a

    .line 3091
    const/4 v0, 0x0

    return v0

    .line 3093
    :cond_a
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/threeten/bp/jdk8/Jdk8Methods;->safeToInt(J)I

    move-result v4

    .line 3094
    .local v4, "totalSecs":I
    if-nez v4, :cond_1b

    .line 3095
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->noOffsetText:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_b6

    .line 3097
    :cond_1b
    div-int/lit16 v0, v4, 0xe10

    rem-int/lit8 v0, v0, 0x64

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v5

    .line 3098
    .local v5, "absHours":I
    div-int/lit8 v0, v4, 0x3c

    rem-int/lit8 v0, v0, 0x3c

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v6

    .line 3099
    .local v6, "absMinutes":I
    rem-int/lit8 v0, v4, 0x3c

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v7

    .line 3100
    .local v7, "absSeconds":I
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    .line 3101
    .local v8, "bufPos":I
    move v9, v5

    .line 3102
    .local v9, "output":I
    if-gez v4, :cond_3b

    const-string v0, "-"

    goto :goto_3d

    :cond_3b
    const-string v0, "+"

    :goto_3d
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    div-int/lit8 v1, v5, 0xa

    add-int/lit8 v1, v1, 0x30

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    rem-int/lit8 v1, v5, 0xa

    add-int/lit8 v1, v1, 0x30

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3104
    iget v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->type:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_5e

    iget v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->type:I

    const/4 v1, 0x1

    if-lt v0, v1, :cond_ac

    if-lez v6, :cond_ac

    .line 3105
    :cond_5e
    iget v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->type:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_67

    const-string v0, ":"

    goto :goto_69

    :cond_67
    const-string v0, ""

    :goto_69
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    div-int/lit8 v1, v6, 0xa

    add-int/lit8 v1, v1, 0x30

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    rem-int/lit8 v1, v6, 0xa

    add-int/lit8 v1, v1, 0x30

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3107
    add-int/2addr v9, v6

    .line 3108
    iget v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->type:I

    const/4 v1, 0x7

    if-ge v0, v1, :cond_8b

    iget v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->type:I

    const/4 v1, 0x5

    if-lt v0, v1, :cond_ac

    if-lez v7, :cond_ac

    .line 3109
    :cond_8b
    iget v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->type:I

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_94

    const-string v0, ":"

    goto :goto_96

    :cond_94
    const-string v0, ""

    :goto_96
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    div-int/lit8 v1, v7, 0xa

    add-int/lit8 v1, v1, 0x30

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    rem-int/lit8 v1, v7, 0xa

    add-int/lit8 v1, v1, 0x30

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 3111
    add-int/2addr v9, v7

    .line 3114
    :cond_ac
    if-nez v9, :cond_b6

    .line 3115
    invoke-virtual {p2, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 3116
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->noOffsetText:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3119
    .end local v5    # "absHours":I
    .end local v6    # "absMinutes":I
    .end local v7    # "absSeconds":I
    .end local v8    # "bufPos":I
    .end local v9    # "output":I
    :cond_b6
    :goto_b6
    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .line 3200
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->noOffsetText:Ljava/lang/String;

    const-string v1, "\'"

    const-string v2, "\'\'"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 3201
    .local v3, "converted":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Offset("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->PATTERNS:[Ljava/lang/String;

    iget v2, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->type:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

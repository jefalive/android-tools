.class Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$1;
.super Ljava/lang/Object;
.source "AtendimentoController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->buscaFaqs(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

.field final synthetic val$filtro:Ljava/lang/String;

.field final synthetic val$idFaq:Ljava/lang/String;

.field final synthetic val$limit:I

.field final synthetic val$offset:I

.field final synthetic val$retornoSemMatch:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .registers 7
    .param p1, "this$0"    # Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

    .line 24
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$1;->this$0:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

    iput-object p2, p0, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$1;->val$idFaq:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$1;->val$filtro:Ljava/lang/String;

    iput p4, p0, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$1;->val$limit:I

    iput p5, p0, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$1;->val$offset:I

    iput-object p6, p0, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$1;->val$retornoSemMatch:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 9

    .line 27
    new-instance v7, Lcom/itau/empresas/Evento$EventoAtendimento;

    invoke-direct {v7}, Lcom/itau/empresas/Evento$EventoAtendimento;-><init>()V

    .line 28
    .local v7, "atendimento":Lcom/itau/empresas/Evento$EventoAtendimento;
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$1;->this$0:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

    # invokes: Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->api()Ljava/lang/Object;
    invoke-static {v0}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->access$000(Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$1;->val$idFaq:Ljava/lang/String;

    iget-object v2, p0, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$1;->this$0:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

    iget-object v2, v2, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->app:Lcom/itau/empresas/CustomApplication;

    .line 29
    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$1;->val$filtro:Ljava/lang/String;

    iget v4, p0, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$1;->val$limit:I

    .line 30
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$1;->val$offset:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController$1;->val$retornoSemMatch:Ljava/lang/String;

    .line 29
    invoke-interface/range {v0 .. v6}, Lcom/itau/empresas/api/Api;->buscaFaqs(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/feature/atendimento/model/AtendimentoVO;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/Evento$EventoAtendimento;->atendimentoVO:Lcom/itau/empresas/feature/atendimento/model/AtendimentoVO;

    .line 31
    sget-object v0, Lcom/itau/empresas/Evento$EventoAtendimento;->atendimentoVO:Lcom/itau/empresas/feature/atendimento/model/AtendimentoVO;

    # invokes: Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->post(Ljava/lang/Object;)V
    invoke-static {v0}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->access$100(Ljava/lang/Object;)V

    .line 32
    return-void
.end method

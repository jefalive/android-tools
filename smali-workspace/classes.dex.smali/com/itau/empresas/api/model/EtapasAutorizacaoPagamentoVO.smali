.class public Lcom/itau/empresas/api/model/EtapasAutorizacaoPagamentoVO;
.super Ljava/lang/Object;
.source "EtapasAutorizacaoPagamentoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private codigoOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_operador"
    .end annotation
.end field

.field private cpfOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf_operador"
    .end annotation
.end field

.field private dataEtapa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_etapa"
    .end annotation
.end field

.field private etapa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "etapa"
    .end annotation
.end field

.field private horaEtapa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "hora_etapa"
    .end annotation
.end field

.field private nomeOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_operador"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public final Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;
.super Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;
.source "SourceFile"

# interfaces
.implements Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$Listener;


# static fields
.field private static final $:[B

.field private static $$:I


# instance fields
.field private themeManager:Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;

.field private url:Ljava/lang/String;

.field private webviewTitle:Landroid/widget/TextView;

.field private webviewfrag:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    new-instance v0, Ljava/lang/String;

    mul-int/lit8 p1, p1, 0x3

    add-int/lit8 p1, p1, 0x77

    const/4 v5, 0x0

    mul-int/lit8 p2, p2, 0x2

    rsub-int/lit8 p2, p2, 0xb

    sget-object v4, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->$:[B

    mul-int/lit8 p0, p0, 0x2

    rsub-int/lit8 p0, p0, 0x3

    new-array v1, p2, [B

    if-nez v4, :cond_1a

    move v2, p1

    move v3, p0

    :goto_17
    add-int/2addr v2, v3

    add-int/lit8 p1, v2, 0x1

    :cond_1a
    move v2, v5

    add-int/lit8 v5, v5, 0x1

    int-to-byte v3, p1

    aput-byte v3, v1, v2

    if-ne v5, p2, :cond_27

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_27
    add-int/lit8 p0, p0, 0x1

    move v2, p1

    aget-byte v3, v4, p0

    goto :goto_17
.end method

.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0xe

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->$:[B

    const/16 v0, 0xa1

    sput v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->$$:I

    return-void

    :array_e
    .array-data 1
        0x37t
        0x2bt
        -0x14t
        -0x45t
        -0x13t
        -0x4t
        0x13t
        -0xet
        -0x5t
        0x11t
        -0x12t
        0xbt
        -0x12t
        0x5t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 20
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;-><init>()V

    return-void
.end method

.method static synthetic access$lambda$0(Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;Landroid/view/View;)V
    .registers 2

    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->lambda$onCreateView$0(Landroid/view/View;)V

    return-void
.end method

.method private applyTheme(Landroid/view/View;)V
    .registers 5

    .line 65
    sget v0, Lbr/com/itau/sdk/android/core/R$id;->webview_dialog:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 66
    if-eqz v2, :cond_d

    .line 67
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->themeManager:Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;

    invoke-virtual {v0, v2}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->checkBackgroundTheme(Landroid/view/View;)V

    .line 69
    :cond_d
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->themeManager:Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->loading:Landroid/view/View;

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->checkLoadingTheme(Landroid/view/View;)V

    .line 70
    return-void
.end method

.method private synthetic lambda$onCreateView$0(Landroid/view/View;)V
    .registers 2

    .line 53
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->onBackPressed()V

    return-void
.end method


# virtual methods
.method public loadUrl(Ljava/lang/String;)V
    .registers 2

    .line 82
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->url:Ljava/lang/String;

    .line 83
    return-void
.end method

.method protected onBackPressed()V
    .registers 2

    .line 74
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->webviewfrag:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 75
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->webviewfrag:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->goBack()V

    goto :goto_11

    .line 77
    :cond_e
    invoke-super {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->onBackPressed()V

    .line 79
    :goto_11
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .registers 4

    .line 33
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getSdkCustomUi()Lbr/com/itau/sdk/android/core/SDKCustomUi;

    move-result-object v0

    invoke-interface {v0}, Lbr/com/itau/sdk/android/core/SDKCustomUi;->getThemeManager()Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->themeManager:Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;

    .line 34
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->themeManager:Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;

    if-nez v0, :cond_19

    .line 35
    sget v0, Lbr/com/itau/sdk/android/core/R$style;->itausdkcore_WebViewDialog:I

    invoke-super {p0, v0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0

    .line 38
    :cond_19
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->themeManager:Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->getDialogTheme()I

    move-result v0

    invoke-super {p0, v0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    .line 39
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->themeManager:Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->checkDialogTheme(Landroid/app/Dialog;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 10

    .line 45
    sget v0, Lbr/com/itau/sdk/android/core/R$layout;->itausdkcore_dialog_webview:I

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 47
    sget v0, Lbr/com/itau/sdk/android/core/R$id;->webview_title:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->webviewTitle:Landroid/widget/TextView;

    .line 49
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->webviewfrag:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 50
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->webviewfrag:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    invoke-virtual {v0, p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->withListener(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$Listener;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->loadUrl(Ljava/lang/String;)V

    .line 52
    sget v0, Lbr/com/itau/sdk/android/core/R$id;->token_back:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/ImageView;

    .line 53
    invoke-static {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog$$Lambda$1;->lambdaFactory$(Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    sget v0, Lbr/com/itau/sdk/android/core/R$id;->global_loading:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->loading:Landroid/view/View;

    .line 57
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->themeManager:Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;

    if-eqz v0, :cond_51

    .line 58
    invoke-direct {p0, v4}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->applyTheme(Landroid/view/View;)V

    .line 61
    :cond_51
    return-object v4
.end method

.method public onPageError()V
    .registers 1

    .line 97
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->hideLoading()V

    .line 98
    return-void
.end method

.method public onPageFinished()V
    .registers 1

    .line 92
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->hideLoading()V

    .line 93
    return-void
.end method

.method public onPageStarted()V
    .registers 1

    .line 87
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->showLoading()V

    .line 88
    return-void
.end method

.method public pageTitle(Ljava/lang/String;)V
    .registers 3

    .line 102
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->webviewTitle:Landroid/widget/TextView;

    if-eqz v0, :cond_9

    .line 103
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewDialog;->webviewTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    :cond_9
    return-void
.end method

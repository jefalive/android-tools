.class public Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;
.super Ljava/lang/Object;
.source "PreLoginUsuarioVO.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;
    }
.end annotation


# instance fields
.field private agencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia"
    .end annotation
.end field

.field private conta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta"
    .end annotation
.end field

.field private dac:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dac"
    .end annotation
.end field

.field private nrDocumento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nrDocumento"
    .end annotation
.end field

.field private operador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "operador"
    .end annotation
.end field

.field private tipoDocumento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipoDocumento"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .param p1, "agencia"    # Ljava/lang/String;
    .param p2, "conta"    # Ljava/lang/String;
    .param p3, "dac"    # Ljava/lang/String;
    .param p4, "tipoDocumento"    # Ljava/lang/String;
    .param p5, "nrDocumento"    # Ljava/lang/String;
    .param p6, "operador"    # Ljava/lang/String;

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->agencia:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->conta:Ljava/lang/String;

    .line 26
    iput-object p3, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->dac:Ljava/lang/String;

    .line 27
    iput-object p6, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->operador:Ljava/lang/String;

    .line 28
    iput-object p4, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->tipoDocumento:Ljava/lang/String;

    .line 29
    iput-object p5, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->nrDocumento:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public static builder()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;
    .registers 1

    .line 57
    new-instance v0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;

    invoke-direct {v0}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getAgencia()Ljava/lang/String;
    .registers 2

    .line 33
    iget-object v0, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->agencia:Ljava/lang/String;

    return-object v0
.end method

.method public getConta()Ljava/lang/String;
    .registers 2

    .line 37
    iget-object v0, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->conta:Ljava/lang/String;

    return-object v0
.end method

.method public getDac()Ljava/lang/String;
    .registers 2

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->dac:Ljava/lang/String;

    return-object v0
.end method

.method public getNrDocumento()Ljava/lang/String;
    .registers 2

    .line 53
    iget-object v0, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->nrDocumento:Ljava/lang/String;

    return-object v0
.end method

.method public getOperador()Ljava/lang/String;
    .registers 2

    .line 45
    iget-object v0, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->operador:Ljava/lang/String;

    return-object v0
.end method

.method public getTipoDocumento()Ljava/lang/String;
    .registers 2

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->tipoDocumento:Ljava/lang/String;

    return-object v0
.end method

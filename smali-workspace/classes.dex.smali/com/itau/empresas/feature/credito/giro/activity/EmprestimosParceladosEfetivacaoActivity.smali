.class public Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "EmprestimosParceladosEfetivacaoActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field arquivoComprovante:Ljava/io/File;

.field controller:Lcom/itau/empresas/controller/EmprestimosParceladosEfetivacaoController;

.field devedorSolidario:Z

.field efetivacaoVO:Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;

.field llLabelEfetivacaoSucesso:Landroid/widget/LinearLayout;

.field simulacaoVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

.field temSeguro:Z

.field textoCreditoEfetuadoEm:Landroid/widget/TextView;

.field textoEfetivacaoEmprestimosParceladosValor:Landroid/widget/TextView;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 31
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 81
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->temSeguro:Z

    if-eqz v0, :cond_8

    .line 82
    const v1, 0x7f070699

    .local v1, "id":I
    goto :goto_b

    .line 85
    .end local v1    # "id":I
    :cond_8
    const v1, 0x7f07069a

    .line 88
    .local v1, "id":I
    :goto_b
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 90
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 93
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 104
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method aoCarregarElementosDeTela()V
    .registers 6

    .line 49
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->constroiToolbar()V

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->efetivacaoVO:Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;

    if-eqz v0, :cond_40

    .line 52
    .line 53
    const v0, 0x7f070542

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->efetivacaoVO:Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;

    .line 54
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;->getDataOperacao()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->efetivacaoVO:Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;

    .line 55
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;->getHorarioOperacao()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 53
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 57
    .local v4, "textoEfetivacao":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->textoCreditoEfetuadoEm:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->textoEfetivacaoEmprestimosParceladosValor:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->efetivacaoVO:Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;

    .line 61
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;->getValorEmprestimo()F

    move-result v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v1

    .line 59
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    .end local v4    # "textoEfetivacao":Ljava/lang/String;
    :cond_40
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->llLabelEfetivacaoSucesso:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setAccessibilityLiveRegion(Landroid/view/View;I)V

    .line 66
    return-void
.end method

.method aoClicarEmMaisDetalhes()V
    .registers 3

    .line 70
    .line 71
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->simulacaoVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    .line 72
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->simulacaoVO(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;

    move-result-object v0

    iget-boolean v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->temSeguro:Z

    .line 73
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->temSeguro(Z)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;

    move-result-object v0

    iget-boolean v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->devedorSolidario:Z

    .line 74
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->devedorSolidario(Z)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->efetivacaoVO:Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;

    .line 75
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->efetivacaoVO(Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosComprovanteContratacaoActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 77
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 2
    .param p1, "v"    # Landroid/view/View;

    .line 100
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosEfetivacaoActivity;->onBackPressed()V

    .line 101
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 96
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

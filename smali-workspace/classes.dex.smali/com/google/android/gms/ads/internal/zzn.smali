.class public Lcom/google/android/gms/ads/internal/zzn;
.super Lcom/google/android/gms/ads/internal/client/zzy$zza;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# static fields
.field private static final zzqy:Ljava/lang/Object;

.field private static zzqz:Lcom/google/android/gms/ads/internal/zzn;


# instance fields
.field private final zzpV:Ljava/lang/Object;

.field private zzqA:Z

.field private zzqB:F


# direct methods
.method static constructor <clinit>()V
    .registers 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/ads/internal/zzn;->zzqy:Ljava/lang/Object;

    return-void
.end method

.method public static zzbs()Lcom/google/android/gms/ads/internal/zzn;
    .registers 3

    sget-object v1, Lcom/google/android/gms/ads/internal/zzn;->zzqy:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    sget-object v0, Lcom/google/android/gms/ads/internal/zzn;->zzqz:Lcom/google/android/gms/ads/internal/zzn;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v1

    return-object v0

    :catchall_7
    move-exception v2

    monitor-exit v1

    throw v2
.end method


# virtual methods
.method public setAppVolume(F)V
    .registers 4
    .param p1, "volume"    # F

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/zzn;->zzpV:Ljava/lang/Object;

    monitor-enter v0

    :try_start_3
    iput p1, p0, Lcom/google/android/gms/ads/internal/zzn;->zzqB:F
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v0

    goto :goto_a

    :catchall_7
    move-exception v1

    monitor-exit v0

    throw v1

    :goto_a
    return-void
.end method

.method public zza()V
    .registers 4

    sget-object v1, Lcom/google/android/gms/ads/internal/zzn;->zzqy:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/zzn;->zzqA:Z

    if-eqz v0, :cond_e

    const-string v0, "Mobile ads is initialized already."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_13

    monitor-exit v1

    return-void

    :cond_e
    const/4 v0, 0x1

    :try_start_f
    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/zzn;->zzqA:Z
    :try_end_11
    .catchall {:try_start_f .. :try_end_11} :catchall_13

    monitor-exit v1

    goto :goto_16

    :catchall_13
    move-exception v2

    monitor-exit v1

    throw v2

    :goto_16
    return-void
.end method

.method public zzbt()F
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/zzn;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget v0, p0, Lcom/google/android/gms/ads/internal/zzn;->zzqB:F
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v1

    return v0

    :catchall_7
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method public zzbu()Z
    .registers 5

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/zzn;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget v0, p0, Lcom/google/android/gms/ads/internal/zzn;->zzqB:F
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_f

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_c

    const/4 v0, 0x1

    goto :goto_d

    :cond_c
    const/4 v0, 0x0

    :goto_d
    monitor-exit v2

    return v0

    :catchall_f
    move-exception v3

    monitor-exit v2

    throw v3
.end method

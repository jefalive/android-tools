.class Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager$1;
.super Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat$AuthenticationCallback;
.source "AndroidFingerprintManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager;->createAuthenticationCallback(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;)Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat$AuthenticationCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager;

.field final synthetic val$callback:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;


# direct methods
.method constructor <init>(Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager;Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;)V
    .registers 3
    .param p1, "this$0"    # Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager;

    .line 63
    iput-object p1, p0, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager$1;->this$0:Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager;

    iput-object p2, p0, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager$1;->val$callback:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;

    invoke-direct {p0}, Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat$AuthenticationCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onAuthenticationError(ILjava/lang/CharSequence;)V
    .registers 7
    .param p1, "errMsgId"    # I
    .param p2, "errString"    # Ljava/lang/CharSequence;

    .line 66
    invoke-super {p0, p1, p2}, Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat$AuthenticationCallback;->onAuthenticationError(ILjava/lang/CharSequence;)V

    .line 69
    sparse-switch p1, :sswitch_data_24

    goto :goto_e

    .line 71
    :sswitch_7
    sget-object v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->STATUS_USER_CANCELLED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->toString()Ljava/lang/String;

    move-result-object v3

    .line 72
    .local v3, "message":Ljava/lang/String;
    goto :goto_14

    .line 74
    .end local v3    # "message":Ljava/lang/String;
    :goto_e
    sget-object v0, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->UNEXPECTED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintExceptionResult;->toString()Ljava/lang/String;

    move-result-object v3

    .line 78
    .local v3, "message":Ljava/lang/String;
    :goto_14
    iget-object v0, p0, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager$1;->val$callback:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;

    new-instance v1, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v3, v2}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;->onError(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;)V

    .line 79
    return-void

    nop

    :sswitch_data_24
    .sparse-switch
        0x5 -> :sswitch_7
    .end sparse-switch
.end method

.method public onAuthenticationFailed()V
    .registers 3

    .line 83
    invoke-super {p0}, Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat$AuthenticationCallback;->onAuthenticationFailed()V

    .line 84
    iget-object v0, p0, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager$1;->val$callback:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;

    sget-object v1, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->FAILED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    invoke-interface {v0, v1}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;->onResult(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;)V

    .line 85
    return-void
.end method

.method public onAuthenticationHelp(ILjava/lang/CharSequence;)V
    .registers 5
    .param p1, "helpMsgId"    # I
    .param p2, "helpString"    # Ljava/lang/CharSequence;

    .line 89
    invoke-super {p0, p1, p2}, Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat$AuthenticationCallback;->onAuthenticationHelp(ILjava/lang/CharSequence;)V

    .line 90
    sget-object v1, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->HELP:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    .line 92
    .local v1, "result":Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;
    invoke-virtual {v1}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_12

    .line 93
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->setMessage(Ljava/lang/String;)Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    .line 96
    :cond_12
    iget-object v0, p0, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager$1;->val$callback:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;

    invoke-interface {v0, v1}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;->onResult(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;)V

    .line 97
    return-void
.end method

.method public onAuthenticationSucceeded(Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat$AuthenticationResult;)V
    .registers 4
    .param p1, "result"    # Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat$AuthenticationResult;

    .line 101
    invoke-super {p0, p1}, Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat$AuthenticationCallback;->onAuthenticationSucceeded(Landroid/support/v4/hardware/fingerprint/FingerprintManagerCompat$AuthenticationResult;)V

    .line 102
    iget-object v0, p0, Lbr/com/itau/sdk/android/fingerprintcore/android/AndroidFingerprintManager$1;->val$callback:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;

    sget-object v1, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->AUTHENTICATED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    invoke-interface {v0, v1}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintCallback;->onResult(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;)V

    .line 103
    return-void
.end method

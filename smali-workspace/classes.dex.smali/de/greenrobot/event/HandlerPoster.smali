.class final Lde/greenrobot/event/HandlerPoster;
.super Landroid/os/Handler;
.source "HandlerPoster.java"


# instance fields
.field private final eventBus:Lde/greenrobot/event/EventBus;

.field private handlerActive:Z

.field private final maxMillisInsideHandleMessage:I

.field private final queue:Lde/greenrobot/event/PendingPostQueue;


# direct methods
.method constructor <init>(Lde/greenrobot/event/EventBus;Landroid/os/Looper;I)V
    .registers 5
    .param p1, "eventBus"    # Lde/greenrobot/event/EventBus;
    .param p2, "looper"    # Landroid/os/Looper;
    .param p3, "maxMillisInsideHandleMessage"    # I

    .line 31
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 32
    iput-object p1, p0, Lde/greenrobot/event/HandlerPoster;->eventBus:Lde/greenrobot/event/EventBus;

    .line 33
    iput p3, p0, Lde/greenrobot/event/HandlerPoster;->maxMillisInsideHandleMessage:I

    .line 34
    new-instance v0, Lde/greenrobot/event/PendingPostQueue;

    invoke-direct {v0}, Lde/greenrobot/event/PendingPostQueue;-><init>()V

    iput-object v0, p0, Lde/greenrobot/event/HandlerPoster;->queue:Lde/greenrobot/event/PendingPostQueue;

    .line 35
    return-void
.end method


# virtual methods
.method enqueue(Lde/greenrobot/event/Subscription;Ljava/lang/Object;)V
    .registers 8
    .param p1, "subscription"    # Lde/greenrobot/event/Subscription;
    .param p2, "event"    # Ljava/lang/Object;

    .line 38
    invoke-static {p1, p2}, Lde/greenrobot/event/PendingPost;->obtainPendingPost(Lde/greenrobot/event/Subscription;Ljava/lang/Object;)Lde/greenrobot/event/PendingPost;

    move-result-object v2

    .line 39
    .local v2, "pendingPost":Lde/greenrobot/event/PendingPost;
    move-object v3, p0

    monitor-enter v3

    .line 40
    :try_start_6
    iget-object v0, p0, Lde/greenrobot/event/HandlerPoster;->queue:Lde/greenrobot/event/PendingPostQueue;

    invoke-virtual {v0, v2}, Lde/greenrobot/event/PendingPostQueue;->enqueue(Lde/greenrobot/event/PendingPost;)V

    .line 41
    iget-boolean v0, p0, Lde/greenrobot/event/HandlerPoster;->handlerActive:Z

    if-nez v0, :cond_24

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lde/greenrobot/event/HandlerPoster;->handlerActive:Z

    .line 43
    invoke-virtual {p0}, Lde/greenrobot/event/HandlerPoster;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lde/greenrobot/event/HandlerPoster;->sendMessage(Landroid/os/Message;)Z

    move-result v0

    if-nez v0, :cond_24

    .line 44
    new-instance v0, Lde/greenrobot/event/EventBusException;

    const-string v1, "Could not send handler message"

    invoke-direct {v0, v1}, Lde/greenrobot/event/EventBusException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_24
    .catchall {:try_start_6 .. :try_end_24} :catchall_26

    .line 47
    :cond_24
    monitor-exit v3

    goto :goto_29

    :catchall_26
    move-exception v4

    monitor-exit v3

    throw v4

    .line 48
    :goto_29
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .registers 11
    .param p1, "msg"    # Landroid/os/Message;

    .line 52
    const/4 v2, 0x0

    .line 54
    .local v2, "rescheduled":Z
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    .line 56
    .local v3, "started":J
    :goto_5
    iget-object v0, p0, Lde/greenrobot/event/HandlerPoster;->queue:Lde/greenrobot/event/PendingPostQueue;

    invoke-virtual {v0}, Lde/greenrobot/event/PendingPostQueue;->poll()Lde/greenrobot/event/PendingPost;

    move-result-object v5

    .line 57
    .local v5, "pendingPost":Lde/greenrobot/event/PendingPost;
    if-nez v5, :cond_23

    .line 58
    move-object v6, p0

    monitor-enter v6
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_4c

    .line 60
    :try_start_f
    iget-object v0, p0, Lde/greenrobot/event/HandlerPoster;->queue:Lde/greenrobot/event/PendingPostQueue;

    invoke-virtual {v0}, Lde/greenrobot/event/PendingPostQueue;->poll()Lde/greenrobot/event/PendingPost;

    move-result-object v5

    .line 61
    if-nez v5, :cond_1e

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lde/greenrobot/event/HandlerPoster;->handlerActive:Z
    :try_end_1a
    .catchall {:try_start_f .. :try_end_1a} :catchall_20

    .line 63
    monitor-exit v6

    .line 78
    iput-boolean v2, p0, Lde/greenrobot/event/HandlerPoster;->handlerActive:Z

    .line 63
    return-void

    .line 65
    :cond_1e
    monitor-exit v6

    goto :goto_23

    :catchall_20
    move-exception v7

    monitor-exit v6

    :try_start_22
    throw v7

    .line 67
    :cond_23
    :goto_23
    iget-object v0, p0, Lde/greenrobot/event/HandlerPoster;->eventBus:Lde/greenrobot/event/EventBus;

    invoke-virtual {v0, v5}, Lde/greenrobot/event/EventBus;->invokeSubscriber(Lde/greenrobot/event/PendingPost;)V

    .line 68
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sub-long v6, v0, v3

    .line 69
    .local v6, "timeInMethod":J
    iget v0, p0, Lde/greenrobot/event/HandlerPoster;->maxMillisInsideHandleMessage:I

    int-to-long v0, v0

    cmp-long v0, v6, v0

    if-ltz v0, :cond_4b

    .line 70
    invoke-virtual {p0}, Lde/greenrobot/event/HandlerPoster;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lde/greenrobot/event/HandlerPoster;->sendMessage(Landroid/os/Message;)Z

    move-result v0

    if-nez v0, :cond_47

    .line 71
    new-instance v0, Lde/greenrobot/event/EventBusException;

    const-string v1, "Could not send handler message"

    invoke-direct {v0, v1}, Lde/greenrobot/event/EventBusException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_47
    .catchall {:try_start_22 .. :try_end_47} :catchall_4c

    .line 73
    :cond_47
    const/4 v2, 0x1

    .line 78
    iput-boolean v2, p0, Lde/greenrobot/event/HandlerPoster;->handlerActive:Z

    .line 74
    return-void

    .line 76
    .end local v5    # "pendingPost":Lde/greenrobot/event/PendingPost;
    .end local v6    # "timeInMethod":J
    :cond_4b
    goto :goto_5

    .line 78
    .end local v3    # "started":J
    :catchall_4c
    move-exception v8

    iput-boolean v2, p0, Lde/greenrobot/event/HandlerPoster;->handlerActive:Z

    throw v8
.end method

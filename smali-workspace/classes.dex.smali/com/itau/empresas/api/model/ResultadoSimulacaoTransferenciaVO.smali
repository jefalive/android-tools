.class public Lcom/itau/empresas/api/model/ResultadoSimulacaoTransferenciaVO;
.super Ljava/lang/Object;
.source "ResultadoSimulacaoTransferenciaVO.java"


# instance fields
.field private agenciaDestino:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia_destino"
    .end annotation
.end field

.field private agenciaOrigem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia_origem"
    .end annotation
.end field

.field private bancoDestino:Ljava/lang/Integer;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "banco_destino"
    .end annotation
.end field

.field private complementoContaDestino:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "complemento_conta_destino"
    .end annotation
.end field

.field private complementoContaOrigem:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "complemento_conta_origem "
    .end annotation
.end field

.field private contaDestino:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta_destino"
    .end annotation
.end field

.field private contaOrigem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta_origem"
    .end annotation
.end field

.field private cpfCnpjDestino:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf_cnpj_destino"
    .end annotation
.end field

.field private cpfCnpjOrigem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf_cnpj_origem"
    .end annotation
.end field

.field private dataPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_pagamento"
    .end annotation
.end field

.field private digitoVerificadorContaDestino:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_verificador_conta_destino"
    .end annotation
.end field

.field private digitoVerificadorContaOrigem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_verificador_conta_origem"
    .end annotation
.end field

.field private favorecidoCadastrado:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "favorecido_cadastrado"
    .end annotation
.end field

.field private finalidade:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "finalidade"
    .end annotation
.end field

.field private moeda:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "moeda"
    .end annotation
.end field

.field private nomeBancoDestino:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_banco_destino"
    .end annotation
.end field

.field private nomeContatoCadastrado:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_contato_cadastrado"
    .end annotation
.end field

.field private nomeFavorecido:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_favorecido"
    .end annotation
.end field

.field private tipoTransferencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_transferencia"
    .end annotation
.end field

.field private valorPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_pagamento"
    .end annotation
.end field

.field private valorTarifa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_tarifa"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public Lbr/com/itau/sdk/android/core/exception/c;
.super Lokhttp3/ResponseBody;
.source "SourceFile"


# instance fields
.field private final a:Lokhttp3/ResponseBody;

.field private b:Ljava/io/IOException;


# direct methods
.method public constructor <init>(Lokhttp3/ResponseBody;)V
    .registers 2

    .line 19
    invoke-direct {p0}, Lokhttp3/ResponseBody;-><init>()V

    .line 20
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/exception/c;->a:Lokhttp3/ResponseBody;

    .line 21
    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core/exception/c;Ljava/io/IOException;)Ljava/io/IOException;
    .registers 2

    .line 14
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/exception/c;->b:Ljava/io/IOException;

    return-object p1
.end method


# virtual methods
.method public a()Ljava/io/IOException;
    .registers 2

    .line 56
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/exception/c;->b:Ljava/io/IOException;

    return-object v0
.end method

.method public b()Z
    .registers 2

    .line 60
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/exception/c;->b:Ljava/io/IOException;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    goto :goto_7

    :cond_6
    const/4 v0, 0x0

    :goto_7
    return v0
.end method

.method public close()V
    .registers 2

    .line 52
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/exception/c;->a:Lokhttp3/ResponseBody;

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->close()V

    .line 53
    return-void
.end method

.method public contentLength()J
    .registers 3

    .line 30
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/exception/c;->a:Lokhttp3/ResponseBody;

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->contentLength()J

    move-result-wide v0

    return-wide v0
.end method

.method public contentType()Lokhttp3/MediaType;
    .registers 2

    .line 25
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/exception/c;->a:Lokhttp3/ResponseBody;

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->contentType()Lokhttp3/MediaType;

    move-result-object v0

    return-object v0
.end method

.method public source()Lokio/BufferedSource;
    .registers 3

    .line 35
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/exception/c;->a:Lokhttp3/ResponseBody;

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->source()Lokio/BufferedSource;

    move-result-object v1

    .line 37
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/d;

    invoke-direct {v0, p0, v1}, Lbr/com/itau/sdk/android/core/exception/d;-><init>(Lbr/com/itau/sdk/android/core/exception/c;Lokio/Source;)V

    invoke-static {v0}, Lokio/Okio;->buffer(Lokio/Source;)Lokio/BufferedSource;

    move-result-object v0

    return-object v0
.end method

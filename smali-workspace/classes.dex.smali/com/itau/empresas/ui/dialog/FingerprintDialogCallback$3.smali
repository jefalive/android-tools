.class synthetic Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback$3;
.super Ljava/lang/Object;
.source "FingerprintDialogCallback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$br$com$itau$sdk$android$fingerprintcore$ItauFingerprintResult:[I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 36
    invoke-static {}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->values()[Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback$3;->$SwitchMap$br$com$itau$sdk$android$fingerprintcore$ItauFingerprintResult:[I

    :try_start_9
    sget-object v0, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback$3;->$SwitchMap$br$com$itau$sdk$android$fingerprintcore$ItauFingerprintResult:[I

    sget-object v1, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->FAILED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_14} :catch_15

    goto :goto_16

    :catch_15
    move-exception v3

    :goto_16
    :try_start_16
    sget-object v0, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback$3;->$SwitchMap$br$com$itau$sdk$android$fingerprintcore$ItauFingerprintResult:[I

    sget-object v1, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->AUTHENTICATED:Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintResult;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_21} :catch_22

    goto :goto_23

    :catch_22
    move-exception v3

    :goto_23
    return-void
.end method

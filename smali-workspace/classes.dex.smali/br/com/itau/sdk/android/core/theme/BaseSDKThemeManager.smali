.class public abstract Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected tokenTheme:I

.field private values:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Object;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->tokenTheme:I

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->values:Ljava/util/Map;

    return-void
.end method

.method private context()Landroid/content/Context;
    .registers 2

    .line 93
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method private getValue(I)I
    .registers 3

    .line 82
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->getValue(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private setValue(II)V
    .registers 6

    .line 76
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->values:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    return-void
.end method


# virtual methods
.method public abstract checkBackgroundTheme(Landroid/view/View;)V
.end method

.method public abstract checkDialogTheme(Landroid/app/Dialog;)Landroid/app/Dialog;
.end method

.method public abstract checkDialogTheme(Landroid/support/v7/app/AlertDialog;)Landroid/support/v7/app/AlertDialog;
.end method

.method public abstract checkLoadingTheme(Landroid/view/View;)V
.end method

.method public getColor(II)I
    .registers 5

    .line 65
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->getValue(I)I

    move-result v1

    .line 67
    const/4 v0, -0x1

    if-ne v1, v0, :cond_12

    .line 68
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->context()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 69
    invoke-direct {p0, p1, v1}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->setValue(II)V

    .line 72
    :cond_12
    return v1
.end method

.method public abstract getDialogTheme()I
.end method

.method public getDimension(II)I
    .registers 5

    .line 54
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->getValue(I)I

    move-result v1

    .line 56
    const/4 v0, -0x1

    if-ne v1, v0, :cond_16

    .line 57
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->context()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 58
    invoke-direct {p0, p1, v1}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->setValue(II)V

    .line 61
    :cond_16
    return v1
.end method

.method public abstract getFontBold()Ljava/lang/String;
.end method

.method public abstract getFontRegular()Ljava/lang/String;
.end method

.method public getValue(ILjava/lang/Object;)Ljava/lang/Object;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(ITT;)TT;"
        }
    .end annotation

    .line 86
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->values:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 87
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->values:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 89
    :cond_17
    return-object p2
.end method

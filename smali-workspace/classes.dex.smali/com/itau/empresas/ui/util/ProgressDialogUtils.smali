.class public Lcom/itau/empresas/ui/util/ProgressDialogUtils;
.super Ljava/lang/Object;
.source "ProgressDialogUtils.java"


# static fields
.field private static loadingFragment:Lcom/itau/empresas/ui/fragment/LoadingFragment;

.field private static tag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 14
    const-string v0, "loading"

    sput-object v0, Lcom/itau/empresas/ui/util/ProgressDialogUtils;->tag:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static escondeDialogoDeProgresso(Landroid/support/v4/app/FragmentManager;)V
    .registers 4
    .param p0, "fm"    # Landroid/support/v4/app/FragmentManager;

    .line 37
    :try_start_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentManager;->popBackStack()V

    .line 38
    sget-object v0, Lcom/itau/empresas/ui/util/ProgressDialogUtils;->tag:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_1c

    .line 39
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/ui/util/ProgressDialogUtils;->tag:Ljava/lang/String;

    .line 40
    invoke-virtual {p0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 44
    :cond_1c
    const/4 v0, 0x0

    sput-object v0, Lcom/itau/empresas/ui/util/ProgressDialogUtils;->loadingFragment:Lcom/itau/empresas/ui/fragment/LoadingFragment;
    :try_end_1f
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_1f} :catch_20

    .line 47
    goto :goto_2a

    .line 45
    :catch_20
    move-exception v2

    .line 46
    .local v2, "ignored":Ljava/lang/Exception;
    const-string v0, "ProgressDialUtil"

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 48
    .end local v2    # "ignored":Ljava/lang/Exception;
    :goto_2a
    return-void
.end method

.method public static mostraDialogoDeProgresso(Landroid/support/v4/app/FragmentManager;)V
    .registers 6
    .param p0, "fm"    # Landroid/support/v4/app/FragmentManager;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 19
    :try_start_0
    sget-object v0, Lcom/itau/empresas/ui/util/ProgressDialogUtils;->loadingFragment:Lcom/itau/empresas/ui/fragment/LoadingFragment;

    if-nez v0, :cond_20

    .line 20
    invoke-static {}, Lcom/itau/empresas/ui/fragment/LoadingFragment_;->builder()Lcom/itau/empresas/ui/fragment/LoadingFragment_$FragmentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/fragment/LoadingFragment_$FragmentBuilder_;->build()Lcom/itau/empresas/ui/fragment/LoadingFragment;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/ui/util/ProgressDialogUtils;->loadingFragment:Lcom/itau/empresas/ui/fragment/LoadingFragment;

    .line 21
    sget-object v0, Lcom/itau/empresas/ui/util/ProgressDialogUtils;->loadingFragment:Lcom/itau/empresas/ui/fragment/LoadingFragment;

    if-eqz v0, :cond_20

    .line 22
    .line 23
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/ui/util/ProgressDialogUtils;->loadingFragment:Lcom/itau/empresas/ui/fragment/LoadingFragment;

    sget-object v2, Lcom/itau/empresas/ui/util/ProgressDialogUtils;->tag:Ljava/lang/String;

    .line 22
    const v3, 0x1020002

    invoke-static {v0, v3, v1, v2}, Lcom/itau/empresas/ui/util/FragmentUtils;->attachFragment(Landroid/support/v4/app/FragmentTransaction;ILcom/itau/empresas/ui/fragment/BaseFragment;Ljava/lang/String;)V
    :try_end_20
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_20} :catch_21

    .line 32
    :cond_20
    goto :goto_2b

    .line 30
    :catch_21
    move-exception v4

    .line 31
    .local v4, "ignored":Ljava/lang/Exception;
    const-string v0, "ProgressDialUtil"

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 33
    .end local v4    # "ignored":Ljava/lang/Exception;
    :goto_2b
    return-void
.end method

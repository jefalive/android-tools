.class Lcom/itau/empresas/controller/PagamentoDarfController$9;
.super Ljava/lang/Object;
.source "PagamentoDarfController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/controller/PagamentoDarfController;->consultaDetalhesPagamento(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/controller/PagamentoDarfController;

.field final synthetic val$codigoOperador:Ljava/lang/String;

.field final synthetic val$identificacaoFuncionalidade:Ljava/lang/String;

.field final synthetic val$identificacaoPagamento:Ljava/lang/String;

.field final synthetic val$numeroCarrinho:Ljava/lang/String;

.field final synthetic val$numeroLote:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/controller/PagamentoDarfController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .param p1, "this$0"    # Lcom/itau/empresas/controller/PagamentoDarfController;

    .line 134
    iput-object p1, p0, Lcom/itau/empresas/controller/PagamentoDarfController$9;->this$0:Lcom/itau/empresas/controller/PagamentoDarfController;

    iput-object p2, p0, Lcom/itau/empresas/controller/PagamentoDarfController$9;->val$codigoOperador:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/controller/PagamentoDarfController$9;->val$identificacaoFuncionalidade:Ljava/lang/String;

    iput-object p4, p0, Lcom/itau/empresas/controller/PagamentoDarfController$9;->val$numeroCarrinho:Ljava/lang/String;

    iput-object p5, p0, Lcom/itau/empresas/controller/PagamentoDarfController$9;->val$numeroLote:Ljava/lang/String;

    iput-object p6, p0, Lcom/itau/empresas/controller/PagamentoDarfController$9;->val$identificacaoPagamento:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 7

    .line 138
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoDarfController$9;->this$0:Lcom/itau/empresas/controller/PagamentoDarfController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/PagamentoDarfController;->api()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/controller/PagamentoDarfController$9;->val$codigoOperador:Ljava/lang/String;

    iget-object v2, p0, Lcom/itau/empresas/controller/PagamentoDarfController$9;->val$identificacaoFuncionalidade:Ljava/lang/String;

    iget-object v3, p0, Lcom/itau/empresas/controller/PagamentoDarfController$9;->val$numeroCarrinho:Ljava/lang/String;

    iget-object v4, p0, Lcom/itau/empresas/controller/PagamentoDarfController$9;->val$numeroLote:Ljava/lang/String;

    iget-object v5, p0, Lcom/itau/empresas/controller/PagamentoDarfController$9;->val$identificacaoPagamento:Ljava/lang/String;

    invoke-interface/range {v0 .. v5}, Lcom/itau/empresas/api/Api;->consultaDetalhesAutorizacaoDarf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/BaseController;->post(Ljava/lang/Object;)V

    .line 140
    return-void
.end method

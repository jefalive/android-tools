.class public Lbr/com/itau/sdk/android/core/endpoint/a/c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/endpoint/a/c$a;
    }
.end annotation


# instance fields
.field private b:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap<Ljava/lang/String;Lbr/com/itau/sdk/android/core/endpoint/a/a;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .registers 2

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/a/c;->b:Ljava/util/concurrent/ConcurrentMap;

    .line 19
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/itau/sdk/android/core/endpoint/a/d;)V
    .registers 2

    .line 11
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/endpoint/a/c;-><init>()V

    return-void
.end method

.method public static a()Lbr/com/itau/sdk/android/core/endpoint/a/c;
    .registers 1

    .line 22
    invoke-static {}, Lbr/com/itau/sdk/android/core/endpoint/a/c$a;->a()Lbr/com/itau/sdk/android/core/endpoint/a/c;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Lbr/com/itau/sdk/android/core/endpoint/a/a;)V
    .registers 4

    .line 34
    invoke-virtual {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/a/c;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 35
    return-void

    .line 37
    :cond_7
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/a/c;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1, p2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .registers 3

    .line 26
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/a/c;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .registers 2

    .line 41
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/a/c;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->clear()V

    .line 42
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .registers 3

    .line 30
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/a/c;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    return-void
.end method

.method public c(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/endpoint/a/a;
    .registers 3

    .line 45
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/a/c;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/endpoint/a/a;

    return-object v0
.end method

.method public d(Ljava/lang/String;)Z
    .registers 8

    .line 49
    invoke-virtual {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/a/c;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 50
    const/4 v0, 0x1

    return v0

    .line 52
    :cond_8
    invoke-virtual {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/a/c;->c(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/endpoint/a/a;

    move-result-object v4

    .line 53
    invoke-virtual {v4}, Lbr/com/itau/sdk/android/core/endpoint/a/a;->c()J

    move-result-wide v0

    invoke-virtual {v4}, Lbr/com/itau/sdk/android/core/endpoint/a/a;->a()J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_1f

    const/4 v5, 0x1

    goto :goto_20

    :cond_1f
    const/4 v5, 0x0

    .line 55
    :goto_20
    if-eqz v5, :cond_25

    .line 56
    invoke-virtual {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/a/c;->b(Ljava/lang/String;)V

    .line 58
    :cond_25
    return v5
.end method

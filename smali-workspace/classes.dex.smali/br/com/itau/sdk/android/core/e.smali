.class final Lbr/com/itau/sdk/android/core/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lokhttp3/Interceptor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/e$a;
    }
.end annotation


# static fields
.field private static final a:Lcom/google/gson/Gson;

.field private static final e:[B

.field private static f:I


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 29
    const/16 v0, 0x3e

    new-array v0, v0, [B

    fill-array-data v0, :array_1e

    sput-object v0, Lbr/com/itau/sdk/android/core/e;->e:[B

    const/16 v0, 0xba

    sput v0, Lbr/com/itau/sdk/android/core/e;->f:I

    new-instance v0, Lcom/google/gson/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/GsonBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->disableHtmlEscaping()Lcom/google/gson/GsonBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    sput-object v0, Lbr/com/itau/sdk/android/core/e;->a:Lcom/google/gson/Gson;

    return-void

    nop

    :array_1e
    .array-data 1
        0x24t
        -0x4bt
        0x2at
        -0x29t
        -0x6t
        -0x3t
        0xct
        -0x12t
        -0xat
        0x8t
        -0x4t
        -0x12t
        0x40t
        -0x3dt
        -0xet
        0x6t
        -0x11t
        0x6t
        -0xat
        -0xat
        0x2t
        -0x23t
        -0x7t
        -0xat
        -0x2t
        0x33t
        -0x2ct
        -0x1at
        -0x9t
        0x3t
        0x0t
        -0x10t
        -0xct
        0x9t
        -0xat
        -0x12t
        0x2t
        -0x2t
        0x1t
        -0x4t
        -0x32t
        0x6t
        -0x1at
        0x54t
        -0x48t
        -0x3t
        -0x12t
        -0x5t
        -0x6t
        -0xat
        0x4ft
        -0x47t
        -0x8t
        0x40t
        -0x53t
        -0xct
        0x4t
        -0x5t
        -0x6t
        -0x2t
        -0x18t
        0xet
    .end array-data
.end method

.method constructor <init>(ZLjava/lang/String;Ljava/lang/String;)V
    .registers 4

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p3, p0, Lbr/com/itau/sdk/android/core/e;->b:Ljava/lang/String;

    .line 37
    iput-object p2, p0, Lbr/com/itau/sdk/android/core/e;->c:Ljava/lang/String;

    .line 38
    iput-boolean p1, p0, Lbr/com/itau/sdk/android/core/e;->d:Z

    .line 39
    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    const/4 v5, 0x0

    add-int/lit8 p1, p1, 0x42

    add-int/lit8 p0, p0, 0x2

    add-int/lit8 p2, p2, 0x4

    new-instance v0, Ljava/lang/String;

    sget-object v4, Lbr/com/itau/sdk/android/core/e;->e:[B

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v4, :cond_17

    move v2, p0

    move v3, p2

    :goto_13
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p1, v2, -0x5

    :cond_17
    int-to-byte v2, p1

    aput-byte v2, v1, v5

    if-ne v5, p0, :cond_21

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_21
    move v2, p1

    add-int/lit8 v5, v5, 0x1

    add-int/lit8 p2, p2, 0x1

    aget-byte v3, v4, p2

    goto :goto_13
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 93
    new-instance v3, Lbr/com/itau/sdk/android/core/model/DataReqResDTO;

    invoke-direct {v3}, Lbr/com/itau/sdk/android/core/model/DataReqResDTO;-><init>()V

    .line 94
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/e;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    iget-object v2, p0, Lbr/com/itau/sdk/android/core/e;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lbr/com/itau/security/routercryptor/MobileCryptor;->encryptBase64String([B[BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lbr/com/itau/sdk/android/core/model/DataReqResDTO;->setData(Ljava/lang/String;)V

    .line 95
    sget-object v0, Lbr/com/itau/sdk/android/core/e;->a:Lcom/google/gson/Gson;

    invoke-virtual {v0, v3}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lokhttp3/RequestBody;)Ljava/lang/String;
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 100
    new-instance v1, Lokio/Buffer;

    invoke-direct {v1}, Lokio/Buffer;-><init>()V

    .line 101
    invoke-virtual {p1, v1}, Lokhttp3/RequestBody;->writeTo(Lokio/BufferedSink;)V

    .line 102
    invoke-virtual {v1}, Lokio/Buffer;->readUtf8()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lokhttp3/Response;Lokhttp3/Request;Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 75
    invoke-virtual {p1}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->string()Ljava/lang/String;

    move-result-object v1

    .line 76
    invoke-virtual {p1}, Lokhttp3/Response;->code()I

    move-result v0

    invoke-direct {p0, p2, p3, v1, v0}, Lbr/com/itau/sdk/android/core/e;->a(Lokhttp3/Request;Ljava/lang/String;Ljava/lang/String;I)V

    .line 77
    return-object v1
.end method

.method private a(Lokhttp3/Response;Ljava/lang/String;)Lokhttp3/Response;
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 81
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->a:Lcom/google/gson/Gson;

    const-class v1, Lbr/com/itau/sdk/android/core/model/DataReqResDTO;

    invoke-virtual {v0, p2, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/sdk/android/core/model/DataReqResDTO;

    .line 82
    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/model/DataReqResDTO;->getData()Ljava/lang/String;

    move-result-object v3

    .line 84
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/e;->c:Ljava/lang/String;

    .line 85
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/e;->b:Ljava/lang/String;

    .line 84
    invoke-static {v3, v0, v1}, Lbr/com/itau/security/routercryptor/MobileCryptor;->decryptDataForBase64String(Ljava/lang/String;[BLjava/lang/String;)[B

    move-result-object v4

    .line 87
    invoke-virtual {p1}, Lokhttp3/Response;->newBuilder()Lokhttp3/Response$Builder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/n;->c:Lokhttp3/MediaType;

    .line 88
    invoke-static {v1, v4}, Lokhttp3/ResponseBody;->create(Lokhttp3/MediaType;[B)Lokhttp3/ResponseBody;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/Response$Builder;->body(Lokhttp3/ResponseBody;)Lokhttp3/Response$Builder;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Lokhttp3/Response$Builder;->build()Lokhttp3/Response;

    move-result-object v0

    .line 87
    return-object v0
.end method

.method private a(Lokhttp3/Request;Ljava/lang/String;Ljava/lang/String;I)V
    .registers 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 125
    sget-object v0, Lbr/com/itau/sdk/android/core/e;->e:[B

    const/16 v1, 0x9

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/e;->e:[B

    const/16 v2, 0x26

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/e;->e:[B

    const/16 v3, 0x1f

    aget-byte v2, v2, v3

    neg-int v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/e;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lokhttp3/Request;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 126
    if-eqz v8, :cond_70

    const/16 v0, 0xc8

    if-lt p4, v0, :cond_70

    const/16 v0, 0x12c

    if-ge p4, v0, :cond_70

    .line 127
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/e;->e:[B

    const/16 v2, 0x1e

    aget-byte v1, v1, v2

    or-int/lit8 v2, v1, 0x2d

    sget-object v3, Lbr/com/itau/sdk/android/core/e;->e:[B

    const/16 v4, 0x26

    aget-byte v3, v3, v4

    neg-int v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/e;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lokhttp3/Request;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/endpoint/a/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 129
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/a/a;

    invoke-static {v8}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 130
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    move-object v5, p3

    move v6, p4

    invoke-virtual {p1}, Lokhttp3/Request;->headers()Lokhttp3/Headers;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lbr/com/itau/sdk/android/core/endpoint/a/a;-><init>(JJLjava/lang/String;ILokhttp3/Headers;)V

    move-object v10, v0

    .line 132
    invoke-static {}, Lbr/com/itau/sdk/android/core/endpoint/a/c;->a()Lbr/com/itau/sdk/android/core/endpoint/a/c;

    move-result-object v0

    invoke-virtual {v0, v9, v10}, Lbr/com/itau/sdk/android/core/endpoint/a/c;->a(Ljava/lang/String;Lbr/com/itau/sdk/android/core/endpoint/a/a;)V

    .line 134
    :cond_70
    return-void
.end method


# virtual methods
.method public intercept(Lokhttp3/Interceptor$Chain;)Lokhttp3/Response;
    .registers 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 45
    invoke-interface {p1}, Lokhttp3/Interceptor$Chain;->request()Lokhttp3/Request;

    move-result-object v6

    .line 47
    invoke-virtual {v6}, Lokhttp3/Request;->body()Lokhttp3/RequestBody;

    move-result-object v0

    if-nez v0, :cond_28

    .line 48
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lbr/com/itau/sdk/android/core/e;->e:[B

    const/16 v2, 0x10

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/e;->e:[B

    const/16 v3, 0x1e

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/e;->e:[B

    const/16 v4, 0x15

    aget-byte v3, v3, v4

    neg-int v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/e;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_28
    invoke-virtual {v6}, Lokhttp3/Request;->newBuilder()Lokhttp3/Request$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v7

    .line 51
    invoke-virtual {v7}, Lokhttp3/Request;->body()Lokhttp3/RequestBody;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/e;->a(Lokhttp3/RequestBody;)Ljava/lang/String;

    move-result-object v8

    .line 53
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core/e;->d:Z

    invoke-static {v0, v6}, Lbr/com/itau/sdk/android/core/i;->a(ZLokhttp3/Request;)Z

    move-result v0

    if-nez v0, :cond_86

    .line 54
    invoke-virtual {v6}, Lokhttp3/Request;->newBuilder()Lokhttp3/Request$Builder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/e;->e:[B

    const/16 v2, 0x1d

    aget-byte v1, v1, v2

    const/16 v2, 0x21

    const/16 v3, 0x35

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/e;->a(III)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lbr/com/itau/sdk/android/core/e;->e:[B

    const/16 v3, 0x1d

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/e;->e:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    const/16 v4, 0x19

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core/e;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lokhttp3/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v0

    invoke-interface {p1, v0}, Lokhttp3/Interceptor$Chain;->proceed(Lokhttp3/Request;)Lokhttp3/Response;

    move-result-object v9

    .line 55
    invoke-direct {p0, v9, v7, v8}, Lbr/com/itau/sdk/android/core/e;->a(Lokhttp3/Response;Lokhttp3/Request;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 56
    invoke-virtual {v9}, Lokhttp3/Response;->newBuilder()Lokhttp3/Response$Builder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/n;->c:Lokhttp3/MediaType;

    invoke-static {v1, v10}, Lokhttp3/ResponseBody;->create(Lokhttp3/MediaType;Ljava/lang/String;)Lokhttp3/ResponseBody;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/Response$Builder;->body(Lokhttp3/ResponseBody;)Lokhttp3/Response$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Response$Builder;->build()Lokhttp3/Response;

    move-result-object v5

    .line 57
    goto :goto_d8

    .line 58
    :cond_86
    invoke-direct {p0, v8}, Lbr/com/itau/sdk/android/core/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 59
    invoke-virtual {v6}, Lokhttp3/Request;->newBuilder()Lokhttp3/Request$Builder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/e;->e:[B

    const/16 v2, 0x3d

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, 0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/e;->e:[B

    const/16 v3, 0x28

    aget-byte v2, v2, v3

    neg-int v2, v2

    sget-object v3, Lbr/com/itau/sdk/android/core/e;->e:[B

    const/16 v4, 0x1e

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/e;->a(III)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lbr/com/itau/sdk/android/core/e;->e:[B

    const/16 v3, 0x2f

    aget-byte v2, v2, v3

    neg-int v2, v2

    const/16 v3, 0x21

    const/16 v4, 0x1d

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core/e;->a(III)Ljava/lang/String;

    move-result-object v2

    .line 60
    invoke-virtual {v0, v1, v2}, Lokhttp3/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v0

    .line 61
    invoke-virtual {v6}, Lokhttp3/Request;->method()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lbr/com/itau/sdk/android/core/e$a;

    const/4 v3, 0x0

    invoke-direct {v2, v9, v3}, Lbr/com/itau/sdk/android/core/e$a;-><init>(Ljava/lang/String;Lbr/com/itau/sdk/android/core/f;)V

    invoke-virtual {v0, v1, v2}, Lokhttp3/Request$Builder;->method(Ljava/lang/String;Lokhttp3/RequestBody;)Lokhttp3/Request$Builder;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v10

    .line 64
    invoke-interface {p1, v10}, Lokhttp3/Interceptor$Chain;->proceed(Lokhttp3/Request;)Lokhttp3/Response;

    move-result-object v11

    .line 65
    invoke-direct {p0, v11, v7, v8}, Lbr/com/itau/sdk/android/core/e;->a(Lokhttp3/Response;Lokhttp3/Request;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 66
    invoke-direct {p0, v11, v12}, Lbr/com/itau/sdk/android/core/e;->a(Lokhttp3/Response;Ljava/lang/String;)Lokhttp3/Response;

    move-result-object v5

    .line 69
    :goto_d8
    return-object v5
.end method

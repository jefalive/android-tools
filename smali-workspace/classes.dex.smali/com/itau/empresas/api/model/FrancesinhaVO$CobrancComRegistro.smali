.class public Lcom/itau/empresas/api/model/FrancesinhaVO$CobrancComRegistro;
.super Ljava/lang/Object;
.source "FrancesinhaVO.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/api/model/FrancesinhaVO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CobrancComRegistro"
.end annotation


# instance fields
.field private aVencer:Lcom/itau/empresas/api/model/FrancesinhaVO$AVencer;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "a_vencer"
    .end annotation
.end field

.field private vencidos:Lcom/itau/empresas/api/model/FrancesinhaVO$ValorEQuantidade;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "vencidos"
    .end annotation
.end field


# virtual methods
.method public getVencidos()Lcom/itau/empresas/api/model/FrancesinhaVO$ValorEQuantidade;
    .registers 2

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/api/model/FrancesinhaVO$CobrancComRegistro;->vencidos:Lcom/itau/empresas/api/model/FrancesinhaVO$ValorEQuantidade;

    return-object v0
.end method

.method public getaVencer()Lcom/itau/empresas/api/model/FrancesinhaVO$AVencer;
    .registers 2

    .line 42
    iget-object v0, p0, Lcom/itau/empresas/api/model/FrancesinhaVO$CobrancComRegistro;->aVencer:Lcom/itau/empresas/api/model/FrancesinhaVO$AVencer;

    return-object v0
.end method

.class public Lcom/itau/empresas/feature/transferencia/TransferenciaController;
.super Lcom/itau/empresas/controller/BaseController;
.source "TransferenciaController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/itau/empresas/controller/BaseController<Lcom/itau/empresas/api/Api;>;"
    }
.end annotation


# instance fields
.field app:Lcom/itau/empresas/CustomApplication;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 15
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/transferencia/TransferenciaController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/transferencia/TransferenciaController;

    .line 15
    invoke-virtual {p0}, Lcom/itau/empresas/feature/transferencia/TransferenciaController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 15
    invoke-static {p0}, Lcom/itau/empresas/feature/transferencia/TransferenciaController;->post(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public buscaTransferenciaRecebidas(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "dataInicio"    # Ljava/lang/String;
    .param p2, "dataFim"    # Ljava/lang/String;
    .param p3, "tipoPagamento"    # Ljava/lang/String;

    .line 21
    new-instance v0, Lcom/itau/empresas/feature/transferencia/TransferenciaController$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/itau/empresas/feature/transferencia/TransferenciaController$1;-><init>(Lcom/itau/empresas/feature/transferencia/TransferenciaController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 30
    return-void
.end method

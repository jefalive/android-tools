.class public abstract Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;
.source "AxisBase.java"


# instance fields
.field private mAxisLineColor:I

.field private mAxisLineWidth:F

.field protected mDrawAxisLine:Z

.field protected mDrawGridLines:Z

.field protected mDrawLabels:Z

.field protected mDrawLimitLineBehindData:Z

.field private mGridColor:I

.field private mGridDashPathEffect:Landroid/graphics/DashPathEffect;

.field private mGridLineWidth:F

.field protected mLimitLines:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 59
    invoke-direct {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;-><init>()V

    .line 22
    const v0, -0x777778

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mGridColor:I

    .line 25
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mGridLineWidth:F

    .line 28
    const v0, -0x777778

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mAxisLineColor:I

    .line 31
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mAxisLineWidth:F

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mDrawGridLines:Z

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mDrawAxisLine:Z

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mDrawLabels:Z

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mGridDashPathEffect:Landroid/graphics/DashPathEffect;

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mDrawLimitLineBehindData:Z

    .line 60
    const/high16 v0, 0x41200000    # 10.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mTextSize:F

    .line 61
    const/high16 v0, 0x40a00000    # 5.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mXOffset:F

    .line 62
    const/high16 v0, 0x40a00000    # 5.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mYOffset:F

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mLimitLines:Ljava/util/List;

    .line 64
    return-void
.end method


# virtual methods
.method public addLimitLine(Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;)V
    .registers 4
    .param p1, "l"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;

    .line 218
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mLimitLines:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mLimitLines:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x6

    if-le v0, v1, :cond_15

    .line 222
    const-string v0, "MPAndroiChart"

    const-string v1, "Warning! You have more than 6 LimitLines on your axis, do you really want that?"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :cond_15
    return-void
.end method

.method public disableGridDashedLine()V
    .registers 2

    .line 301
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mGridDashPathEffect:Landroid/graphics/DashPathEffect;

    .line 302
    return-void
.end method

.method public enableGridDashedLine(FFF)V
    .registers 7
    .param p1, "lineLength"    # F
    .param p2, "spaceLength"    # F
    .param p3, "phase"    # F

    .line 291
    new-instance v0, Landroid/graphics/DashPathEffect;

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    invoke-direct {v0, v1, p3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mGridDashPathEffect:Landroid/graphics/DashPathEffect;

    .line 294
    return-void
.end method

.method public getAxisLineColor()I
    .registers 2

    .line 187
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mAxisLineColor:I

    return v0
.end method

.method public getAxisLineWidth()F
    .registers 2

    .line 145
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mAxisLineWidth:F

    return v0
.end method

.method public getGridColor()I
    .registers 2

    .line 125
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mGridColor:I

    return v0
.end method

.method public getGridDashPathEffect()Landroid/graphics/DashPathEffect;
    .registers 2

    .line 321
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mGridDashPathEffect:Landroid/graphics/DashPathEffect;

    return-object v0
.end method

.method public getGridLineWidth()F
    .registers 2

    .line 167
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mGridLineWidth:F

    return v0
.end method

.method public getLimitLines()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;>;"
        }
    .end annotation

    .line 252
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mLimitLines:Ljava/util/List;

    return-object v0
.end method

.method public abstract getLongestLabel()Ljava/lang/String;
.end method

.method public isDrawAxisLineEnabled()Z
    .registers 2

    .line 103
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mDrawAxisLine:Z

    return v0
.end method

.method public isDrawGridLinesEnabled()Z
    .registers 2

    .line 83
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mDrawGridLines:Z

    return v0
.end method

.method public isDrawLabelsEnabled()Z
    .registers 2

    .line 208
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mDrawLabels:Z

    return v0
.end method

.method public isDrawLimitLinesBehindDataEnabled()Z
    .registers 2

    .line 268
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mDrawLimitLineBehindData:Z

    return v0
.end method

.method public isGridDashedLineEnabled()Z
    .registers 2

    .line 311
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mGridDashPathEffect:Landroid/graphics/DashPathEffect;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    goto :goto_7

    :cond_6
    const/4 v0, 0x1

    :goto_7
    return v0
.end method

.method public removeAllLimitLines()V
    .registers 2

    .line 242
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mLimitLines:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 243
    return-void
.end method

.method public removeLimitLine(Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;)V
    .registers 3
    .param p1, "l"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/components/LimitLine;

    .line 234
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mLimitLines:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 235
    return-void
.end method

.method public setAxisLineColor(I)V
    .registers 2
    .param p1, "color"    # I

    .line 177
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mAxisLineColor:I

    .line 178
    return-void
.end method

.method public setAxisLineWidth(F)V
    .registers 3
    .param p1, "width"    # F

    .line 135
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mAxisLineWidth:F

    .line 136
    return-void
.end method

.method public setDrawAxisLine(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 93
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mDrawAxisLine:Z

    .line 94
    return-void
.end method

.method public setDrawGridLines(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 73
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mDrawGridLines:Z

    .line 74
    return-void
.end method

.method public setDrawLabels(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 198
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mDrawLabels:Z

    .line 199
    return-void
.end method

.method public setDrawLimitLinesBehindData(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 263
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mDrawLimitLineBehindData:Z

    .line 264
    return-void
.end method

.method public setGridColor(I)V
    .registers 2
    .param p1, "color"    # I

    .line 114
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mGridColor:I

    .line 115
    return-void
.end method

.method public setGridLineWidth(F)V
    .registers 3
    .param p1, "width"    # F

    .line 156
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/AxisBase;->mGridLineWidth:F

    .line 157
    return-void
.end method

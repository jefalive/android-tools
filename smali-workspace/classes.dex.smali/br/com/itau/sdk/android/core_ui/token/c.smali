.class final synthetic Lbr/com/itau/sdk/android/core_ui/token/c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;

.field private final b:Landroid/widget/ImageView;


# direct methods
.method private constructor <init>(Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;Landroid/widget/ImageView;)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbr/com/itau/sdk/android/core_ui/token/c;->a:Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;

    iput-object p2, p0, Lbr/com/itau/sdk/android/core_ui/token/c;->b:Landroid/widget/ImageView;

    return-void
.end method

.method public static a(Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;Landroid/widget/ImageView;)Ljava/lang/Runnable;
    .registers 3

    new-instance v0, Lbr/com/itau/sdk/android/core_ui/token/c;

    invoke-direct {v0, p0, p1}, Lbr/com/itau/sdk/android/core_ui/token/c;-><init>(Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;Landroid/widget/ImageView;)V

    return-object v0
.end method


# virtual methods
.method public run()V
    .registers 3
    .annotation runtime Ljava/lang/invoke/LambdaForm$Hidden;
    .end annotation

    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/c;->a:Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core_ui/token/c;->b:Landroid/widget/ImageView;

    invoke-static {v0, v1}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->a(Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;Landroid/widget/ImageView;)V

    return-void
.end method

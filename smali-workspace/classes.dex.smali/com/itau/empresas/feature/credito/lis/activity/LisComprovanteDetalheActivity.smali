.class public Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "LisComprovanteDetalheActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field comprovante:Lcom/itau/empresas/api/model/ComprovanteServiceVO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/itau/empresas/api/model/ComprovanteServiceVO<Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;>;"
        }
    .end annotation
.end field

.field lisComprovanteDetalhe:Landroid/view/View;

.field lisComprovanteDetalheView:Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;

.field svLisComprovanteDetalhe:Landroid/widget/ScrollView;

.field tipoComprovante:Ljava/lang/String;

.field toolbar:Landroid/support/v7/widget/Toolbar;

.field topoItauView:Lcom/itau/empresas/ui/view/TopoItauView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 30
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private configuraToolbar()V
    .registers 4

    .line 70
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0e05c2

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/TextView;

    .line 71
    .local v2, "title":Landroid/widget/TextView;
    const v0, 0x7f0704e6

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f020165

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(I)V

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0700b8

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setNavigationContentDescription(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    return-void
.end method

.method private configurarHeaderLisComprovanteDetalheView()V
    .registers 3

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->topoItauView:Lcom/itau/empresas/ui/view/TopoItauView;

    .line 82
    const v1, 0x7f0705c2

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 81
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/TopoItauView;->setLogoCustomText(Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->topoItauView:Lcom/itau/empresas/ui/view/TopoItauView;

    .line 84
    const v1, 0x7f0705c3

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 83
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/TopoItauView;->setLogoCustomContentDescription(Ljava/lang/String;)V

    .line 85
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 93
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method aoCarregarTela()V
    .registers 5

    .line 53
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->lisComprovanteDetalheView:Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->application:Lcom/itau/empresas/CustomApplication;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->tipoComprovante:Ljava/lang/String;

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->comprovante:Lcom/itau/empresas/api/model/ComprovanteServiceVO;

    invoke-virtual {v0, p0, v1, v2, v3}, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;->setData(Landroid/content/Context;Lcom/itau/empresas/CustomApplication;Ljava/lang/String;Lcom/itau/empresas/api/model/ComprovanteServiceVO;)V

    .line 54
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->configuraToolbar()V

    .line 55
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->configurarHeaderLisComprovanteDetalheView()V

    .line 56
    return-void
.end method

.method compartilhar(Landroid/view/View;)V
    .registers 6
    .param p1, "v"    # Landroid/view/View;

    .line 60
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 61
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->svLisComprovanteDetalhe:Landroid/widget/ScrollView;

    .line 62
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->lisComprovanteDetalhe:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/BitmapUtils;->getBitmapFromView(Landroid/view/View;Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 63
    .local v2, "comprovanteBitmap":Landroid/graphics/Bitmap;
    invoke-static {p0, v2}, Lcom/itau/empresas/ui/util/BitmapUtils;->getUriBitmapTemporario(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/net/Uri;

    move-result-object v3

    .line 64
    .local v3, "bitmapUri":Landroid/net/Uri;
    const-string v0, "Compartilhado"

    .line 65
    invoke-static {p0, v3, v0}, Lcom/itau/empresas/ui/util/CompartilhamentoUtils;->compartilharArquivoImagemPorUri(Landroid/app/Activity;Landroid/net/Uri;Ljava/lang/String;)V

    .line 66
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 67
    return-void
.end method

.method public hitAnalytics()Ljava/lang/String;
    .registers 2

    .line 48
    const v0, 0x7f070331

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 2
    .param p1, "v"    # Landroid/view/View;

    .line 89
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->onBackPressed()V

    .line 90
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 43
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

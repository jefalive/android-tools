.class Lbr/com/itau/sdk/android/core_ui/password/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;


# direct methods
.method constructor <init>(Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;)V
    .registers 2

    .line 55
    iput-object p1, p0, Lbr/com/itau/sdk/android/core_ui/password/b;->a:Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 5

    .line 62
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/password/b;->a:Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->b(Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;)Landroid/widget/Button;

    move-result-object v0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    iget-object v2, p0, Lbr/com/itau/sdk/android/core_ui/password/b;->a:Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;

    invoke-static {v2}, Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;->a(Lbr/com/itau/sdk/android/core_ui/password/EletronicPasswordDialog;)I

    move-result v2

    if-lt v1, v2, :cond_14

    const/4 v1, 0x1

    goto :goto_15

    :cond_14
    const/4 v1, 0x0

    :goto_15
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 63
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5

    .line 67
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5

    .line 58
    return-void
.end method

.class public Lbr/com/itau/sdk/android/core/endpoint/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lokhttp3/Interceptor;


# instance fields
.field public a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 2

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/endpoint/a/b;->a:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public intercept(Lokhttp3/Interceptor$Chain;)Lokhttp3/Response;
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 26
    invoke-static {}, Lbr/com/itau/sdk/android/core/endpoint/a/c;->a()Lbr/com/itau/sdk/android/core/endpoint/a/c;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/endpoint/a/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/endpoint/a/c;->c(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/endpoint/a/a;

    move-result-object v3

    .line 28
    new-instance v0, Lokhttp3/Response$Builder;

    invoke-direct {v0}, Lokhttp3/Response$Builder;-><init>()V

    sget-object v1, Lokhttp3/Protocol;->HTTP_1_1:Lokhttp3/Protocol;

    .line 29
    invoke-virtual {v0, v1}, Lokhttp3/Response$Builder;->protocol(Lokhttp3/Protocol;)Lokhttp3/Response$Builder;

    move-result-object v0

    .line 30
    invoke-interface {p1}, Lokhttp3/Interceptor$Chain;->request()Lokhttp3/Request;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/Response$Builder;->request(Lokhttp3/Request;)Lokhttp3/Response$Builder;

    move-result-object v0

    .line 31
    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/endpoint/a/a;->e()Lokhttp3/Headers;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/Response$Builder;->headers(Lokhttp3/Headers;)Lokhttp3/Response$Builder;

    move-result-object v0

    .line 32
    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/endpoint/a/a;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lokhttp3/Response$Builder;->code(I)Lokhttp3/Response$Builder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/n;->c:Lokhttp3/MediaType;

    .line 33
    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/endpoint/a/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lokhttp3/ResponseBody;->create(Lokhttp3/MediaType;Ljava/lang/String;)Lokhttp3/ResponseBody;

    move-result-object v1

    invoke-virtual {v0, v1}, Lokhttp3/Response$Builder;->body(Lokhttp3/ResponseBody;)Lokhttp3/Response$Builder;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Lokhttp3/Response$Builder;->build()Lokhttp3/Response;

    move-result-object v0

    .line 28
    return-object v0
.end method

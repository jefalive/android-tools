.class Landroid/support/v4/widget/ListViewCompatGingerbread;
.super Ljava/lang/Object;
.source "ListViewCompatGingerbread.java"


# direct methods
.method constructor <init>()V
    .registers 1

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static scrollListBy(Landroid/widget/ListView;I)V
    .registers 6
    .param p0, "listView"    # Landroid/widget/ListView;
    .param p1, "y"    # I

    .line 25
    invoke-virtual {p0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    .line 26
    .local v1, "firstPosition":I
    const/4 v0, -0x1

    if-ne v1, v0, :cond_8

    .line 27
    return-void

    .line 30
    :cond_8
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 31
    .local v2, "firstView":Landroid/view/View;
    if-nez v2, :cond_10

    .line 32
    return-void

    .line 35
    :cond_10
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v0

    sub-int v3, v0, p1

    .line 36
    .local v3, "newTop":I
    invoke-virtual {p0, v1, v3}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 37
    return-void
.end method

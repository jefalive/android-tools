.class Lcom/google/android/gms/tagmanager/zzcp;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/tagmanager/zzcp$zzb;,
        Lcom/google/android/gms/tagmanager/zzcp$zza;,
        Lcom/google/android/gms/tagmanager/zzcp$zzc;
    }
.end annotation


# static fields
.field private static final zzbkq:Lcom/google/android/gms/tagmanager/zzbw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/tagmanager/zzbw<Lcom/google/android/gms/internal/zzag$zza;>;"
        }
    .end annotation
.end field


# instance fields
.field private final zzbhN:Lcom/google/android/gms/tagmanager/DataLayer;

.field private volatile zzbkA:Ljava/lang/String;

.field private zzbkB:I

.field private final zzbks:Lcom/google/android/gms/tagmanager/zzah;

.field private final zzbkt:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/gms/tagmanager/zzak;>;"
        }
    .end annotation
.end field

.field private final zzbku:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/gms/tagmanager/zzak;>;"
        }
    .end annotation
.end field

.field private final zzbkv:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/gms/tagmanager/zzak;>;"
        }
    .end annotation
.end field

.field private final zzbkw:Lcom/google/android/gms/tagmanager/zzl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/tagmanager/zzl<Lcom/google/android/gms/internal/zzrs$zza;Lcom/google/android/gms/tagmanager/zzbw<Lcom/google/android/gms/internal/zzag$zza;>;>;"
        }
    .end annotation
.end field

.field private final zzbkx:Lcom/google/android/gms/tagmanager/zzl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/tagmanager/zzl<Ljava/lang/String;Lcom/google/android/gms/tagmanager/zzcp$zzb;>;"
        }
    .end annotation
.end field

.field private final zzbky:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<Lcom/google/android/gms/internal/zzrs$zze;>;"
        }
    .end annotation
.end field

.field private final zzbkz:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/gms/tagmanager/zzcp$zzc;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 3

    new-instance v0, Lcom/google/android/gms/tagmanager/zzbw;

    invoke-static {}, Lcom/google/android/gms/tagmanager/zzdf;->zzHF()Lcom/google/android/gms/internal/zzag$zza;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/tagmanager/zzbw;-><init>(Ljava/lang/Object;Z)V

    sput-object v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkq:Lcom/google/android/gms/tagmanager/zzbw;

    return-void
.end method

.method private zzHf()Ljava/lang/String;
    .registers 5

    iget v0, p0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkB:I

    const/4 v1, 0x1

    if-gt v0, v1, :cond_8

    const-string v0, ""

    return-object v0

    :cond_8
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v0, p0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkB:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x2

    :goto_17
    iget v0, p0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkB:I

    if-ge v3, v0, :cond_23

    const/16 v0, 0x20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_17

    :cond_23
    const-string v0, ": "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private zza(Lcom/google/android/gms/internal/zzag$zza;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzdi;)Lcom/google/android/gms/tagmanager/zzbw;
    .registers 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/google/android/gms/internal/zzag$zza;Ljava/util/Set<Ljava/lang/String;>;Lcom/google/android/gms/tagmanager/zzdi;)Lcom/google/android/gms/tagmanager/zzbw<Lcom/google/android/gms/internal/zzag$zza;>;"
        }
    .end annotation

    iget-boolean v0, p1, Lcom/google/android/gms/internal/zzag$zza;->zzjH:Z

    if-nez v0, :cond_b

    new-instance v0, Lcom/google/android/gms/tagmanager/zzbw;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/tagmanager/zzbw;-><init>(Ljava/lang/Object;Z)V

    return-object v0

    :cond_b
    iget v0, p1, Lcom/google/android/gms/internal/zzag$zza;->type:I

    packed-switch v0, :pswitch_data_174

    goto/16 :goto_158

    :pswitch_12
    invoke-static {p1}, Lcom/google/android/gms/internal/zzrs;->zzo(Lcom/google/android/gms/internal/zzag$zza;)Lcom/google/android/gms/internal/zzag$zza;

    move-result-object v2

    iget-object v0, p1, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    new-array v0, v0, [Lcom/google/android/gms/internal/zzag$zza;

    iput-object v0, v2, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    const/4 v3, 0x0

    :goto_1e
    iget-object v0, p1, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    if-ge v3, v0, :cond_43

    iget-object v0, p1, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    aget-object v0, v0, v3

    invoke-interface {p3, v3}, Lcom/google/android/gms/tagmanager/zzdi;->zzkh(I)Lcom/google/android/gms/tagmanager/zzdi;

    move-result-object v1

    invoke-direct {p0, v0, p2, v1}, Lcom/google/android/gms/tagmanager/zzcp;->zza(Lcom/google/android/gms/internal/zzag$zza;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzdi;)Lcom/google/android/gms/tagmanager/zzbw;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkq:Lcom/google/android/gms/tagmanager/zzbw;

    if-ne v4, v0, :cond_36

    sget-object v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkq:Lcom/google/android/gms/tagmanager/zzbw;

    return-object v0

    :cond_36
    iget-object v0, v2, Lcom/google/android/gms/internal/zzag$zza;->zzjy:[Lcom/google/android/gms/internal/zzag$zza;

    invoke-virtual {v4}, Lcom/google/android/gms/tagmanager/zzbw;->getObject()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/zzag$zza;

    aput-object v1, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1e

    :cond_43
    new-instance v0, Lcom/google/android/gms/tagmanager/zzbw;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/tagmanager/zzbw;-><init>(Ljava/lang/Object;Z)V

    return-object v0

    :pswitch_4a
    invoke-static {p1}, Lcom/google/android/gms/internal/zzrs;->zzo(Lcom/google/android/gms/internal/zzag$zza;)Lcom/google/android/gms/internal/zzag$zza;

    move-result-object v2

    iget-object v0, p1, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    iget-object v1, p1, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v1, v1

    if-eq v0, v1, :cond_73

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid serving value: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzag$zza;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzbg;->e(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkq:Lcom/google/android/gms/tagmanager/zzbw;

    return-object v0

    :cond_73
    iget-object v0, p1, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    new-array v0, v0, [Lcom/google/android/gms/internal/zzag$zza;

    iput-object v0, v2, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    iget-object v0, p1, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    new-array v0, v0, [Lcom/google/android/gms/internal/zzag$zza;

    iput-object v0, v2, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    const/4 v3, 0x0

    :goto_82
    iget-object v0, p1, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    if-ge v3, v0, :cond_c1

    iget-object v0, p1, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    aget-object v0, v0, v3

    invoke-interface {p3, v3}, Lcom/google/android/gms/tagmanager/zzdi;->zzki(I)Lcom/google/android/gms/tagmanager/zzdi;

    move-result-object v1

    invoke-direct {p0, v0, p2, v1}, Lcom/google/android/gms/tagmanager/zzcp;->zza(Lcom/google/android/gms/internal/zzag$zza;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzdi;)Lcom/google/android/gms/tagmanager/zzbw;

    move-result-object v4

    iget-object v0, p1, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    aget-object v0, v0, v3

    invoke-interface {p3, v3}, Lcom/google/android/gms/tagmanager/zzdi;->zzkj(I)Lcom/google/android/gms/tagmanager/zzdi;

    move-result-object v1

    invoke-direct {p0, v0, p2, v1}, Lcom/google/android/gms/tagmanager/zzcp;->zza(Lcom/google/android/gms/internal/zzag$zza;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzdi;)Lcom/google/android/gms/tagmanager/zzbw;

    move-result-object v5

    sget-object v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkq:Lcom/google/android/gms/tagmanager/zzbw;

    if-eq v4, v0, :cond_a7

    sget-object v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkq:Lcom/google/android/gms/tagmanager/zzbw;

    if-ne v5, v0, :cond_aa

    :cond_a7
    sget-object v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkq:Lcom/google/android/gms/tagmanager/zzbw;

    return-object v0

    :cond_aa
    iget-object v0, v2, Lcom/google/android/gms/internal/zzag$zza;->zzjz:[Lcom/google/android/gms/internal/zzag$zza;

    invoke-virtual {v4}, Lcom/google/android/gms/tagmanager/zzbw;->getObject()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/zzag$zza;

    aput-object v1, v0, v3

    iget-object v0, v2, Lcom/google/android/gms/internal/zzag$zza;->zzjA:[Lcom/google/android/gms/internal/zzag$zza;

    invoke-virtual {v5}, Lcom/google/android/gms/tagmanager/zzbw;->getObject()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/zzag$zza;

    aput-object v1, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_82

    :cond_c1
    new-instance v0, Lcom/google/android/gms/tagmanager/zzbw;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/tagmanager/zzbw;-><init>(Ljava/lang/Object;Z)V

    return-object v0

    :pswitch_c8
    iget-object v0, p1, Lcom/google/android/gms/internal/zzag$zza;->zzjB:Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_105

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Macro cycle detected.  Current macro reference: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/internal/zzag$zza;->zzjB:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  Previous macro references: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzbg;->e(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkq:Lcom/google/android/gms/tagmanager/zzbw;

    return-object v0

    :cond_105
    iget-object v0, p1, Lcom/google/android/gms/internal/zzag$zza;->zzjB:Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v0, p1, Lcom/google/android/gms/internal/zzag$zza;->zzjB:Ljava/lang/String;

    invoke-interface {p3}, Lcom/google/android/gms/tagmanager/zzdi;->zzGO()Lcom/google/android/gms/tagmanager/zzbj;

    move-result-object v1

    invoke-direct {p0, v0, p2, v1}, Lcom/google/android/gms/tagmanager/zzcp;->zza(Ljava/lang/String;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzbj;)Lcom/google/android/gms/tagmanager/zzbw;

    move-result-object v2

    iget-object v0, p1, Lcom/google/android/gms/internal/zzag$zza;->zzjG:[I

    invoke-static {v2, v0}, Lcom/google/android/gms/tagmanager/zzdj;->zza(Lcom/google/android/gms/tagmanager/zzbw;[I)Lcom/google/android/gms/tagmanager/zzbw;

    move-result-object v2

    iget-object v0, p1, Lcom/google/android/gms/internal/zzag$zza;->zzjB:Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-object v2

    :pswitch_120
    invoke-static {p1}, Lcom/google/android/gms/internal/zzrs;->zzo(Lcom/google/android/gms/internal/zzag$zza;)Lcom/google/android/gms/internal/zzag$zza;

    move-result-object v2

    iget-object v0, p1, Lcom/google/android/gms/internal/zzag$zza;->zzjF:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    new-array v0, v0, [Lcom/google/android/gms/internal/zzag$zza;

    iput-object v0, v2, Lcom/google/android/gms/internal/zzag$zza;->zzjF:[Lcom/google/android/gms/internal/zzag$zza;

    const/4 v3, 0x0

    :goto_12c
    iget-object v0, p1, Lcom/google/android/gms/internal/zzag$zza;->zzjF:[Lcom/google/android/gms/internal/zzag$zza;

    array-length v0, v0

    if-ge v3, v0, :cond_151

    iget-object v0, p1, Lcom/google/android/gms/internal/zzag$zza;->zzjF:[Lcom/google/android/gms/internal/zzag$zza;

    aget-object v0, v0, v3

    invoke-interface {p3, v3}, Lcom/google/android/gms/tagmanager/zzdi;->zzkk(I)Lcom/google/android/gms/tagmanager/zzdi;

    move-result-object v1

    invoke-direct {p0, v0, p2, v1}, Lcom/google/android/gms/tagmanager/zzcp;->zza(Lcom/google/android/gms/internal/zzag$zza;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzdi;)Lcom/google/android/gms/tagmanager/zzbw;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkq:Lcom/google/android/gms/tagmanager/zzbw;

    if-ne v4, v0, :cond_144

    sget-object v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkq:Lcom/google/android/gms/tagmanager/zzbw;

    return-object v0

    :cond_144
    iget-object v0, v2, Lcom/google/android/gms/internal/zzag$zza;->zzjF:[Lcom/google/android/gms/internal/zzag$zza;

    invoke-virtual {v4}, Lcom/google/android/gms/tagmanager/zzbw;->getObject()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/zzag$zza;

    aput-object v1, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_12c

    :cond_151
    new-instance v0, Lcom/google/android/gms/tagmanager/zzbw;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/tagmanager/zzbw;-><init>(Ljava/lang/Object;Z)V

    return-object v0

    :goto_158
    :pswitch_158
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Lcom/google/android/gms/internal/zzag$zza;->type:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzbg;->e(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkq:Lcom/google/android/gms/tagmanager/zzbw;

    return-object v0

    nop

    :pswitch_data_174
    .packed-switch 0x2
        :pswitch_12
        :pswitch_4a
        :pswitch_c8
        :pswitch_158
        :pswitch_158
        :pswitch_120
    .end packed-switch
.end method

.method private zza(Ljava/lang/String;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzbj;)Lcom/google/android/gms/tagmanager/zzbw;
    .registers 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;Ljava/util/Set<Ljava/lang/String;>;Lcom/google/android/gms/tagmanager/zzbj;)Lcom/google/android/gms/tagmanager/zzbw<Lcom/google/android/gms/internal/zzag$zza;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkB:I

    add-int/lit8 v0, v0, 0x1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/gms/tagmanager/zzcp;->zzbkB:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkx:Lcom/google/android/gms/tagmanager/zzl;

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/google/android/gms/tagmanager/zzl;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/google/android/gms/tagmanager/zzcp$zzb;

    if-eqz v9, :cond_3d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbks:Lcom/google/android/gms/tagmanager/zzah;

    invoke-interface {v0}, Lcom/google/android/gms/tagmanager/zzah;->zzGA()Z

    move-result v0

    if-nez v0, :cond_3d

    invoke-virtual {v9}, Lcom/google/android/gms/tagmanager/zzcp$zzb;->zzHh()Lcom/google/android/gms/internal/zzag$zza;

    move-result-object v0

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/tagmanager/zzcp;->zza(Lcom/google/android/gms/internal/zzag$zza;Ljava/util/Set;)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkB:I

    add-int/lit8 v0, v0, -0x1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/gms/tagmanager/zzcp;->zzbkB:I

    invoke-virtual {v9}, Lcom/google/android/gms/tagmanager/zzcp$zzb;->zzHg()Lcom/google/android/gms/tagmanager/zzbw;

    move-result-object v0

    return-object v0

    :cond_3d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkz:Ljava/util/Map;

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/google/android/gms/tagmanager/zzcp$zzc;

    if-nez v10, :cond_79

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/tagmanager/zzcp;->zzHf()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Invalid macro: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzbg;->e(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkB:I

    add-int/lit8 v0, v0, -0x1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/gms/tagmanager/zzcp;->zzbkB:I

    sget-object v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkq:Lcom/google/android/gms/tagmanager/zzbw;

    return-object v0

    :cond_79
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v10}, Lcom/google/android/gms/tagmanager/zzcp$zzc;->zzHi()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v10}, Lcom/google/android/gms/tagmanager/zzcp$zzc;->zzHj()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v10}, Lcom/google/android/gms/tagmanager/zzcp$zzc;->zzHk()Ljava/util/Map;

    move-result-object v4

    invoke-virtual {v10}, Lcom/google/android/gms/tagmanager/zzcp$zzc;->zzHm()Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v10}, Lcom/google/android/gms/tagmanager/zzcp$zzc;->zzHl()Ljava/util/Map;

    move-result-object v6

    move-object/from16 v7, p2

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/tagmanager/zzbj;->zzGq()Lcom/google/android/gms/tagmanager/zzco;

    move-result-object v8

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/tagmanager/zzcp;->zza(Ljava/lang/String;Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzco;)Lcom/google/android/gms/tagmanager/zzbw;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11}, Lcom/google/android/gms/tagmanager/zzbw;->getObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_ad

    invoke-virtual {v10}, Lcom/google/android/gms/tagmanager/zzcp$zzc;->zzHn()Lcom/google/android/gms/internal/zzrs$zza;

    move-result-object v12

    goto :goto_eb

    :cond_ad
    invoke-virtual {v11}, Lcom/google/android/gms/tagmanager/zzbw;->getObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_da

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/tagmanager/zzcp;->zzHf()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Multiple macros active for macroName "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzbg;->zzaK(Ljava/lang/String;)V

    :cond_da
    invoke-virtual {v11}, Lcom/google/android/gms/tagmanager/zzbw;->getObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/google/android/gms/internal/zzrs$zza;

    :goto_eb
    if-nez v12, :cond_fa

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkB:I

    add-int/lit8 v0, v0, -0x1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/gms/tagmanager/zzcp;->zzbkB:I

    sget-object v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkq:Lcom/google/android/gms/tagmanager/zzbw;

    return-object v0

    :cond_fa
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkv:Ljava/util/Map;

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/tagmanager/zzbj;->zzGG()Lcom/google/android/gms/tagmanager/zzch;

    move-result-object v1

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    invoke-direct {v2, v0, v12, v3, v1}, Lcom/google/android/gms/tagmanager/zzcp;->zza(Ljava/util/Map;Lcom/google/android/gms/internal/zzrs$zza;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzch;)Lcom/google/android/gms/tagmanager/zzbw;

    move-result-object v13

    invoke-virtual {v11}, Lcom/google/android/gms/tagmanager/zzbw;->zzGP()Z

    move-result v0

    if-eqz v0, :cond_118

    invoke-virtual {v13}, Lcom/google/android/gms/tagmanager/zzbw;->zzGP()Z

    move-result v0

    if-eqz v0, :cond_118

    const/4 v14, 0x1

    goto :goto_119

    :cond_118
    const/4 v14, 0x0

    :goto_119
    sget-object v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkq:Lcom/google/android/gms/tagmanager/zzbw;

    if-ne v13, v0, :cond_120

    sget-object v15, Lcom/google/android/gms/tagmanager/zzcp;->zzbkq:Lcom/google/android/gms/tagmanager/zzbw;

    goto :goto_129

    :cond_120
    new-instance v15, Lcom/google/android/gms/tagmanager/zzbw;

    invoke-virtual {v13}, Lcom/google/android/gms/tagmanager/zzbw;->getObject()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v15, v0, v14}, Lcom/google/android/gms/tagmanager/zzbw;-><init>(Ljava/lang/Object;Z)V

    :goto_129
    invoke-virtual {v12}, Lcom/google/android/gms/internal/zzrs$zza;->zzHh()Lcom/google/android/gms/internal/zzag$zza;

    move-result-object v16

    invoke-virtual {v15}, Lcom/google/android/gms/tagmanager/zzbw;->zzGP()Z

    move-result v0

    if-eqz v0, :cond_143

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkx:Lcom/google/android/gms/tagmanager/zzl;

    new-instance v1, Lcom/google/android/gms/tagmanager/zzcp$zzb;

    move-object/from16 v2, v16

    invoke-direct {v1, v15, v2}, Lcom/google/android/gms/tagmanager/zzcp$zzb;-><init>(Lcom/google/android/gms/tagmanager/zzbw;Lcom/google/android/gms/internal/zzag$zza;)V

    move-object/from16 v2, p1

    invoke-interface {v0, v2, v1}, Lcom/google/android/gms/tagmanager/zzl;->zzh(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_143
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/tagmanager/zzcp;->zza(Lcom/google/android/gms/internal/zzag$zza;Ljava/util/Set;)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkB:I

    add-int/lit8 v0, v0, -0x1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/gms/tagmanager/zzcp;->zzbkB:I

    return-object v15
.end method

.method private zza(Ljava/util/Map;Lcom/google/android/gms/internal/zzrs$zza;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzch;)Lcom/google/android/gms/tagmanager/zzbw;
    .registers 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Lcom/google/android/gms/tagmanager/zzak;>;Lcom/google/android/gms/internal/zzrs$zza;Ljava/util/Set<Ljava/lang/String;>;Lcom/google/android/gms/tagmanager/zzch;)Lcom/google/android/gms/tagmanager/zzbw<Lcom/google/android/gms/internal/zzag$zza;>;"
        }
    .end annotation

    invoke-virtual {p2}, Lcom/google/android/gms/internal/zzrs$zza;->zzHI()Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/internal/zzae;->zzfG:Lcom/google/android/gms/internal/zzae;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zzae;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/google/android/gms/internal/zzag$zza;

    if-nez v3, :cond_1b

    const-string v0, "No function id in properties"

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzbg;->e(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkq:Lcom/google/android/gms/tagmanager/zzbw;

    return-object v0

    :cond_1b
    iget-object v4, v3, Lcom/google/android/gms/internal/zzag$zza;->zzjC:Ljava/lang/String;

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/gms/tagmanager/zzak;

    if-nez v5, :cond_3f

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " has no backing implementation."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzbg;->e(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkq:Lcom/google/android/gms/tagmanager/zzbw;

    return-object v0

    :cond_3f
    iget-object v0, p0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkw:Lcom/google/android/gms/tagmanager/zzl;

    invoke-interface {v0, p2}, Lcom/google/android/gms/tagmanager/zzl;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/gms/tagmanager/zzbw;

    if-eqz v6, :cond_53

    iget-object v0, p0, Lcom/google/android/gms/tagmanager/zzcp;->zzbks:Lcom/google/android/gms/tagmanager/zzah;

    invoke-interface {v0}, Lcom/google/android/gms/tagmanager/zzah;->zzGA()Z

    move-result v0

    if-nez v0, :cond_53

    return-object v6

    :cond_53
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    const/4 v8, 0x1

    invoke-virtual {p2}, Lcom/google/android/gms/internal/zzrs$zza;->zzHI()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_65
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_bf

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Ljava/util/Map$Entry;

    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object/from16 v1, p4

    invoke-interface {v1, v0}, Lcom/google/android/gms/tagmanager/zzch;->zzgj(Ljava/lang/String;)Lcom/google/android/gms/tagmanager/zzcj;

    move-result-object v11

    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/zzag$zza;

    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/zzag$zza;

    invoke-interface {v11, v1}, Lcom/google/android/gms/tagmanager/zzcj;->zze(Lcom/google/android/gms/internal/zzag$zza;)Lcom/google/android/gms/tagmanager/zzdi;

    move-result-object v1

    move-object/from16 v2, p3

    invoke-direct {p0, v0, v2, v1}, Lcom/google/android/gms/tagmanager/zzcp;->zza(Lcom/google/android/gms/internal/zzag$zza;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzdi;)Lcom/google/android/gms/tagmanager/zzbw;

    move-result-object v12

    sget-object v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkq:Lcom/google/android/gms/tagmanager/zzbw;

    if-ne v12, v0, :cond_9b

    sget-object v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkq:Lcom/google/android/gms/tagmanager/zzbw;

    return-object v0

    :cond_9b
    invoke-virtual {v12}, Lcom/google/android/gms/tagmanager/zzbw;->zzGP()Z

    move-result v0

    if-eqz v0, :cond_b1

    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v12}, Lcom/google/android/gms/tagmanager/zzbw;->getObject()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/internal/zzag$zza;

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/internal/zzrs$zza;->zza(Ljava/lang/String;Lcom/google/android/gms/internal/zzag$zza;)V

    goto :goto_b2

    :cond_b1
    const/4 v8, 0x0

    :goto_b2
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v12}, Lcom/google/android/gms/tagmanager/zzbw;->getObject()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v7, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_65

    :cond_bf
    invoke-interface {v7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/google/android/gms/tagmanager/zzak;->zze(Ljava/util/Set;)Z

    move-result v0

    if-nez v0, :cond_fe

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Incorrect keys for function "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " required "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v5}, Lcom/google/android/gms/tagmanager/zzak;->zzGC()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " had "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzbg;->e(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkq:Lcom/google/android/gms/tagmanager/zzbw;

    return-object v0

    :cond_fe
    if-eqz v8, :cond_108

    invoke-virtual {v5}, Lcom/google/android/gms/tagmanager/zzak;->zzFW()Z

    move-result v0

    if-eqz v0, :cond_108

    const/4 v9, 0x1

    goto :goto_109

    :cond_108
    const/4 v9, 0x0

    :goto_109
    new-instance v10, Lcom/google/android/gms/tagmanager/zzbw;

    invoke-virtual {v5, v7}, Lcom/google/android/gms/tagmanager/zzak;->zzP(Ljava/util/Map;)Lcom/google/android/gms/internal/zzag$zza;

    move-result-object v0

    invoke-direct {v10, v0, v9}, Lcom/google/android/gms/tagmanager/zzbw;-><init>(Ljava/lang/Object;Z)V

    if-eqz v9, :cond_119

    iget-object v0, p0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkw:Lcom/google/android/gms/tagmanager/zzl;

    invoke-interface {v0, p2, v10}, Lcom/google/android/gms/tagmanager/zzl;->zzh(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_119
    invoke-virtual {v10}, Lcom/google/android/gms/tagmanager/zzbw;->getObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/zzag$zza;

    move-object/from16 v1, p4

    invoke-interface {v1, v0}, Lcom/google/android/gms/tagmanager/zzch;->zzd(Lcom/google/android/gms/internal/zzag$zza;)V

    return-object v10
.end method

.method private zza(Ljava/util/Set;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzcp$zza;Lcom/google/android/gms/tagmanager/zzco;)Lcom/google/android/gms/tagmanager/zzbw;
    .registers 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Set<Lcom/google/android/gms/internal/zzrs$zze;>;Ljava/util/Set<Ljava/lang/String;>;Lcom/google/android/gms/tagmanager/zzcp$zza;Lcom/google/android/gms/tagmanager/zzco;)Lcom/google/android/gms/tagmanager/zzbw<Ljava/util/Set<Lcom/google/android/gms/internal/zzrs$zza;>;>;"
        }
    .end annotation

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    const/4 v3, 0x1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_f
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/gms/internal/zzrs$zze;

    invoke-interface {p4}, Lcom/google/android/gms/tagmanager/zzco;->zzGN()Lcom/google/android/gms/tagmanager/zzck;

    move-result-object v6

    invoke-virtual {p0, v5, p2, v6}, Lcom/google/android/gms/tagmanager/zzcp;->zza(Lcom/google/android/gms/internal/zzrs$zze;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzck;)Lcom/google/android/gms/tagmanager/zzbw;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/tagmanager/zzbw;->getObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_33

    invoke-interface {p3, v5, v1, v2, v6}, Lcom/google/android/gms/tagmanager/zzcp$zza;->zza(Lcom/google/android/gms/internal/zzrs$zze;Ljava/util/Set;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzck;)V

    :cond_33
    if-eqz v3, :cond_3d

    invoke-virtual {v7}, Lcom/google/android/gms/tagmanager/zzbw;->zzGP()Z

    move-result v0

    if-eqz v0, :cond_3d

    const/4 v3, 0x1

    goto :goto_3e

    :cond_3d
    const/4 v3, 0x0

    :goto_3e
    goto :goto_f

    :cond_3f
    invoke-interface {v1, v2}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    invoke-interface {p4, v1}, Lcom/google/android/gms/tagmanager/zzco;->zzf(Ljava/util/Set;)V

    new-instance v0, Lcom/google/android/gms/tagmanager/zzbw;

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/tagmanager/zzbw;-><init>(Ljava/lang/Object;Z)V

    return-object v0
.end method

.method private zza(Lcom/google/android/gms/internal/zzag$zza;Ljava/util/Set;)V
    .registers 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/google/android/gms/internal/zzag$zza;Ljava/util/Set<Ljava/lang/String;>;)V"
        }
    .end annotation

    if-nez p1, :cond_3

    return-void

    :cond_3
    new-instance v0, Lcom/google/android/gms/tagmanager/zzbu;

    invoke-direct {v0}, Lcom/google/android/gms/tagmanager/zzbu;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/tagmanager/zzcp;->zza(Lcom/google/android/gms/internal/zzag$zza;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzdi;)Lcom/google/android/gms/tagmanager/zzbw;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkq:Lcom/google/android/gms/tagmanager/zzbw;

    if-ne v1, v0, :cond_11

    return-void

    :cond_11
    invoke-virtual {v1}, Lcom/google/android/gms/tagmanager/zzbw;->getObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/zzag$zza;

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzdf;->zzl(Lcom/google/android/gms/internal/zzag$zza;)Ljava/lang/Object;

    move-result-object v2

    instance-of v0, v2, Ljava/util/Map;

    if-eqz v0, :cond_28

    move-object v3, v2

    check-cast v3, Ljava/util/Map;

    iget-object v0, p0, Lcom/google/android/gms/tagmanager/zzcp;->zzbhN:Lcom/google/android/gms/tagmanager/DataLayer;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/tagmanager/DataLayer;->push(Ljava/util/Map;)V

    goto :goto_56

    :cond_28
    instance-of v0, v2, Ljava/util/List;

    if-eqz v0, :cond_51

    move-object v3, v2

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_33
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_50

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    instance-of v0, v5, Ljava/util/Map;

    if-eqz v0, :cond_4a

    move-object v6, v5

    check-cast v6, Ljava/util/Map;

    iget-object v0, p0, Lcom/google/android/gms/tagmanager/zzcp;->zzbhN:Lcom/google/android/gms/tagmanager/DataLayer;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/tagmanager/DataLayer;->push(Ljava/util/Map;)V

    goto :goto_4f

    :cond_4a
    const-string v0, "pushAfterEvaluate: value not a Map"

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzbg;->zzaK(Ljava/lang/String;)V

    :goto_4f
    goto :goto_33

    :cond_50
    goto :goto_56

    :cond_51
    const-string v0, "pushAfterEvaluate: value not a Map or List"

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzbg;->zzaK(Ljava/lang/String;)V

    :goto_56
    return-void
.end method


# virtual methods
.method zza(Lcom/google/android/gms/internal/zzrs$zza;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzch;)Lcom/google/android/gms/tagmanager/zzbw;
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/google/android/gms/internal/zzrs$zza;Ljava/util/Set<Ljava/lang/String;>;Lcom/google/android/gms/tagmanager/zzch;)Lcom/google/android/gms/tagmanager/zzbw<Ljava/lang/Boolean;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/tagmanager/zzcp;->zzbku:Ljava/util/Map;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/gms/tagmanager/zzcp;->zza(Ljava/util/Map;Lcom/google/android/gms/internal/zzrs$zza;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzch;)Lcom/google/android/gms/tagmanager/zzbw;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/tagmanager/zzbw;->getObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/zzag$zza;

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzdf;->zzk(Lcom/google/android/gms/internal/zzag$zza;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/tagmanager/zzdf;->zzR(Ljava/lang/Object;)Lcom/google/android/gms/internal/zzag$zza;

    move-result-object v0

    invoke-interface {p3, v0}, Lcom/google/android/gms/tagmanager/zzch;->zzd(Lcom/google/android/gms/internal/zzag$zza;)V

    new-instance v0, Lcom/google/android/gms/tagmanager/zzbw;

    invoke-virtual {v2}, Lcom/google/android/gms/tagmanager/zzbw;->zzGP()Z

    move-result v1

    invoke-direct {v0, v3, v1}, Lcom/google/android/gms/tagmanager/zzbw;-><init>(Ljava/lang/Object;Z)V

    return-object v0
.end method

.method zza(Lcom/google/android/gms/internal/zzrs$zze;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzck;)Lcom/google/android/gms/tagmanager/zzbw;
    .registers 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/google/android/gms/internal/zzrs$zze;Ljava/util/Set<Ljava/lang/String;>;Lcom/google/android/gms/tagmanager/zzck;)Lcom/google/android/gms/tagmanager/zzbw<Ljava/lang/Boolean;>;"
        }
    .end annotation

    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzrs$zze;->zzHQ()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_9
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_51

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/gms/internal/zzrs$zza;

    invoke-interface {p3}, Lcom/google/android/gms/tagmanager/zzck;->zzGH()Lcom/google/android/gms/tagmanager/zzch;

    move-result-object v0

    invoke-virtual {p0, v5, p2, v0}, Lcom/google/android/gms/tagmanager/zzcp;->zza(Lcom/google/android/gms/internal/zzrs$zza;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzch;)Lcom/google/android/gms/tagmanager/zzbw;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/tagmanager/zzbw;->getObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_45

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzdf;->zzR(Ljava/lang/Object;)Lcom/google/android/gms/internal/zzag$zza;

    move-result-object v0

    invoke-interface {p3, v0}, Lcom/google/android/gms/tagmanager/zzck;->zzf(Lcom/google/android/gms/internal/zzag$zza;)V

    new-instance v0, Lcom/google/android/gms/tagmanager/zzbw;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v6}, Lcom/google/android/gms/tagmanager/zzbw;->zzGP()Z

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/tagmanager/zzbw;-><init>(Ljava/lang/Object;Z)V

    return-object v0

    :cond_45
    if-eqz v3, :cond_4f

    invoke-virtual {v6}, Lcom/google/android/gms/tagmanager/zzbw;->zzGP()Z

    move-result v0

    if-eqz v0, :cond_4f

    const/4 v3, 0x1

    goto :goto_50

    :cond_4f
    const/4 v3, 0x0

    :goto_50
    goto :goto_9

    :cond_51
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzrs$zze;->zzHP()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_59
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/gms/internal/zzrs$zza;

    invoke-interface {p3}, Lcom/google/android/gms/tagmanager/zzck;->zzGI()Lcom/google/android/gms/tagmanager/zzch;

    move-result-object v0

    invoke-virtual {p0, v5, p2, v0}, Lcom/google/android/gms/tagmanager/zzcp;->zza(Lcom/google/android/gms/internal/zzrs$zza;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzch;)Lcom/google/android/gms/tagmanager/zzbw;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/tagmanager/zzbw;->getObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_95

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzdf;->zzR(Ljava/lang/Object;)Lcom/google/android/gms/internal/zzag$zza;

    move-result-object v0

    invoke-interface {p3, v0}, Lcom/google/android/gms/tagmanager/zzck;->zzf(Lcom/google/android/gms/internal/zzag$zza;)V

    new-instance v0, Lcom/google/android/gms/tagmanager/zzbw;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v6}, Lcom/google/android/gms/tagmanager/zzbw;->zzGP()Z

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/tagmanager/zzbw;-><init>(Ljava/lang/Object;Z)V

    return-object v0

    :cond_95
    if-eqz v3, :cond_9f

    invoke-virtual {v6}, Lcom/google/android/gms/tagmanager/zzbw;->zzGP()Z

    move-result v0

    if-eqz v0, :cond_9f

    const/4 v3, 0x1

    goto :goto_a0

    :cond_9f
    const/4 v3, 0x0

    :goto_a0
    goto :goto_59

    :cond_a1
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/tagmanager/zzdf;->zzR(Ljava/lang/Object;)Lcom/google/android/gms/internal/zzag$zza;

    move-result-object v0

    invoke-interface {p3, v0}, Lcom/google/android/gms/tagmanager/zzck;->zzf(Lcom/google/android/gms/internal/zzag$zza;)V

    new-instance v0, Lcom/google/android/gms/tagmanager/zzbw;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/tagmanager/zzbw;-><init>(Ljava/lang/Object;Z)V

    return-object v0
.end method

.method zza(Ljava/lang/String;Ljava/util/Set;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzco;)Lcom/google/android/gms/tagmanager/zzbw;
    .registers 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;Ljava/util/Set<Lcom/google/android/gms/internal/zzrs$zze;>;Ljava/util/Map<Lcom/google/android/gms/internal/zzrs$zze;Ljava/util/List<Lcom/google/android/gms/internal/zzrs$zza;>;>;Ljava/util/Map<Lcom/google/android/gms/internal/zzrs$zze;Ljava/util/List<Ljava/lang/String;>;>;Ljava/util/Map<Lcom/google/android/gms/internal/zzrs$zze;Ljava/util/List<Lcom/google/android/gms/internal/zzrs$zza;>;>;Ljava/util/Map<Lcom/google/android/gms/internal/zzrs$zze;Ljava/util/List<Ljava/lang/String;>;>;Ljava/util/Set<Ljava/lang/String;>;Lcom/google/android/gms/tagmanager/zzco;)Lcom/google/android/gms/tagmanager/zzbw<Ljava/util/Set<Lcom/google/android/gms/internal/zzrs$zza;>;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/gms/tagmanager/zzcp$3;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/tagmanager/zzcp$3;-><init>(Lcom/google/android/gms/tagmanager/zzcp;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    invoke-direct {p0, p2, p7, v0, p8}, Lcom/google/android/gms/tagmanager/zzcp;->zza(Ljava/util/Set;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzcp$zza;Lcom/google/android/gms/tagmanager/zzco;)Lcom/google/android/gms/tagmanager/zzbw;

    move-result-object v0

    return-object v0
.end method

.method zza(Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzco;)Lcom/google/android/gms/tagmanager/zzbw;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Set<Lcom/google/android/gms/internal/zzrs$zze;>;Lcom/google/android/gms/tagmanager/zzco;)Lcom/google/android/gms/tagmanager/zzbw<Ljava/util/Set<Lcom/google/android/gms/internal/zzrs$zza;>;>;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    new-instance v1, Lcom/google/android/gms/tagmanager/zzcp$4;

    invoke-direct {v1, p0}, Lcom/google/android/gms/tagmanager/zzcp$4;-><init>(Lcom/google/android/gms/tagmanager/zzcp;)V

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/google/android/gms/tagmanager/zzcp;->zza(Ljava/util/Set;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzcp$zza;Lcom/google/android/gms/tagmanager/zzco;)Lcom/google/android/gms/tagmanager/zzbw;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized zzfR(Ljava/lang/String;)V
    .registers 9

    monitor-enter p0

    :try_start_1
    invoke-virtual {p0, p1}, Lcom/google/android/gms/tagmanager/zzcp;->zzgo(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/tagmanager/zzcp;->zzbks:Lcom/google/android/gms/tagmanager/zzah;

    invoke-interface {v0, p1}, Lcom/google/android/gms/tagmanager/zzah;->zzge(Ljava/lang/String;)Lcom/google/android/gms/tagmanager/zzag;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/tagmanager/zzag;->zzGy()Lcom/google/android/gms/tagmanager/zzu;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/tagmanager/zzcp;->zzbky:Ljava/util/Set;

    invoke-interface {v4}, Lcom/google/android/gms/tagmanager/zzu;->zzGq()Lcom/google/android/gms/tagmanager/zzco;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/tagmanager/zzcp;->zza(Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzco;)Lcom/google/android/gms/tagmanager/zzbw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/tagmanager/zzbw;->getObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_22
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3e

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/gms/internal/zzrs$zza;

    iget-object v0, p0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkt:Ljava/util/Map;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v4}, Lcom/google/android/gms/tagmanager/zzu;->zzGp()Lcom/google/android/gms/tagmanager/zzch;

    move-result-object v2

    invoke-direct {p0, v0, v6, v1, v2}, Lcom/google/android/gms/tagmanager/zzcp;->zza(Ljava/util/Map;Lcom/google/android/gms/internal/zzrs$zza;Ljava/util/Set;Lcom/google/android/gms/tagmanager/zzch;)Lcom/google/android/gms/tagmanager/zzbw;

    goto :goto_22

    :cond_3e
    invoke-interface {v3}, Lcom/google/android/gms/tagmanager/zzag;->zzGz()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/tagmanager/zzcp;->zzgo(Ljava/lang/String;)V
    :try_end_45
    .catchall {:try_start_1 .. :try_end_45} :catchall_47

    monitor-exit p0

    return-void

    :catchall_47
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method declared-synchronized zzgo(Ljava/lang/String;)V
    .registers 2

    monitor-enter p0

    :try_start_1
    iput-object p1, p0, Lcom/google/android/gms/tagmanager/zzcp;->zzbkA:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    monitor-exit p0

    return-void

    :catchall_5
    move-exception p1

    monitor-exit p0

    throw p1
.end method

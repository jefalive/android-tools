.class Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "MenuListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MenuHolder"
.end annotation


# instance fields
.field itemIconContainer:Landroid/widget/RelativeLayout;

.field itemIconTextView:Landroid/widget/ImageView;

.field itemNameTextView:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

.field menu:Lbr/com/itau/textovoz/model/Menu;

.field final synthetic this$0:Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;


# direct methods
.method public constructor <init>(Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;Landroid/view/View;)V
    .registers 3
    .param p2, "itemView"    # Landroid/view/View;

    .line 91
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;->this$0:Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;

    .line 92
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 93
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    return-void
.end method


# virtual methods
.method public bindMenu(Lbr/com/itau/textovoz/model/Menu;)V
    .registers 5
    .param p1, "m"    # Lbr/com/itau/textovoz/model/Menu;

    .line 97
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;->menu:Lbr/com/itau/textovoz/model/Menu;

    .line 99
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;->itemView:Landroid/view/View;

    sget v1, Lbr/com/itau/textovoz/R$id;->text_name_item_list:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;->itemNameTextView:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    .line 100
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;->itemView:Landroid/view/View;

    sget v1, Lbr/com/itau/textovoz/R$id;->image_menu_icon_search:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;->itemIconTextView:Landroid/widget/ImageView;

    .line 101
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;->itemView:Landroid/view/View;

    sget v1, Lbr/com/itau/textovoz/R$id;->relative_icon_row:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;->itemIconContainer:Landroid/widget/RelativeLayout;

    .line 103
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;->menu:Lbr/com/itau/textovoz/model/Menu;

    invoke-virtual {v0}, Lbr/com/itau/textovoz/model/Menu;->getIcon()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5d

    .line 105
    :try_start_32
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;->this$0:Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;

    .line 106
    invoke-virtual {v0}, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;->this$0:Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;

    .line 107
    invoke-virtual {v1}, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lbr/com/itau/textovoz/util/MenuUtils;->getMenuDrawable(Landroid/content/Context;Lbr/com/itau/textovoz/model/Menu;)I

    move-result v1

    .line 106
    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 109
    .local v2, "icon":Landroid/graphics/drawable/Drawable;
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;->itemIconTextView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 110
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;->itemIconContainer:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V
    :try_end_51
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_32 .. :try_end_51} :catch_52

    .line 113
    .end local v2    # "icon":Landroid/graphics/drawable/Drawable;
    goto :goto_5d

    .line 111
    :catch_52
    move-exception v2

    .line 112
    .local v2, "e":Landroid/content/res/Resources$NotFoundException;
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Menu;->getName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v1}, Ltimber/log/Timber;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    .end local v2    # "e":Landroid/content/res/Resources$NotFoundException;
    :cond_5d
    :goto_5d
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;->menu:Lbr/com/itau/textovoz/model/Menu;

    invoke-virtual {v0}, Lbr/com/itau/textovoz/model/Menu;->getName()Ljava/lang/String;

    move-result-object v2

    .line 116
    .local v2, "itemName":Ljava/lang/String;
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;->itemNameTextView:Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    invoke-virtual {v0, v2}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4
    .param p1, "v"    # Landroid/view/View;

    .line 121
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;->this$0:Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;

    # getter for: Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;->listener:Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuListListener;
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;->access$000(Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;)Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuListListener;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 122
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;->this$0:Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;

    # getter for: Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;->listener:Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuListListener;
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;->access$000(Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;)Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuListListener;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;->menu:Lbr/com/itau/textovoz/model/Menu;

    invoke-interface {v0, v1}, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuListListener;->onMenuItemClicked(Lbr/com/itau/textovoz/model/Menu;)V

    .line 124
    :cond_13
    return-void
.end method

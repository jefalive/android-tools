.class public final Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lbr/com/itau/sdk/android/core/webview/RouterRequestJs$RouterRequestResult;
.implements Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck$Contract;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$OnLocaStorageWebviewChangeListener;,
        Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$Listener;
    }
.end annotation


# static fields
.field private static final $:[B

.field private static $$:I = 0x0

.field public static final PAGE_ITOKEN_APP:I = 0x1

.field public static final PAGE_OTHER:I = 0xff

.field public static final PAGE_TIPO_DISPOSITIVO:I = 0x0

.field public static final WEBVIEW_REQUEST_CODE:I = 0x10


# instance fields
.field private lastPage:I

.field private listener:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$Listener;

.field private onLocaStorageWebviewChangeListener:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$OnLocaStorageWebviewChangeListener;

.field private otpModel:Lbr/com/itau/security/token/model/OTPModel;

.field private reqBuilder:Lokhttp3/FormBody$Builder;

.field private requestJavascriptInterface:Lbr/com/itau/sdk/android/core/webview/RouterRequestJs;

.field private responseNativeFeature:Ljava/lang/String;

.field private final unusedTAG:Ljava/lang/String;

.field private urlParameters:Ljava/lang/String;

.field private webView:Landroid/webkit/WebView;


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    add-int/lit8 p1, p1, 0x4

    add-int/lit8 p0, p0, 0x1

    const/4 v4, 0x0

    add-int/lit8 p2, p2, 0x20

    new-instance v0, Ljava/lang/String;

    sget-object v5, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    new-array v1, p0, [B

    if-nez v5, :cond_16

    move v2, p0

    move v3, p2

    :goto_11
    neg-int v3, v3

    add-int p2, v2, v3

    add-int/lit8 p1, p1, 0x1

    :cond_16
    move v2, v4

    add-int/lit8 v4, v4, 0x1

    int-to-byte v3, p2

    aput-byte v3, v1, v2

    if-ne v4, p0, :cond_23

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_23
    move v2, p2

    aget-byte v3, v5, p1

    goto :goto_11
.end method

.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x3dc

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v0, 0x8e

    sput v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$$:I

    return-void

    :array_e
    .array-data 1
        0x18t
        0x7ct
        0x66t
        0x4ct
        -0xbt
        0xct
        -0x12t
        0x8t
        0x8t
        -0x9t
        -0x6t
        0x46t
        -0x39t
        0x2t
        -0xft
        0x2ft
        -0x27t
        0x7t
        -0x8t
        0x8t
        -0x9t
        -0x6t
        0x1t
        0x31t
        -0x37t
        0x25t
        -0xdt
        -0x6t
        0x19t
        -0x13t
        -0xct
        0x8t
        0x3dt
        0x1t
        -0x41t
        0x3t
        0x4t
        -0x3t
        0x3dt
        -0x2t
        -0x32t
        0x2bt
        -0x2dt
        0x2ft
        -0x33t
        -0xft
        0x0t
        0xbt
        -0x9t
        0xat
        0x21t
        -0x25t
        -0x1t
        -0x3t
        0x8t
        0x3ct
        -0x4bt
        0x10t
        -0xft
        0x9t
        -0x7t
        -0x4t
        0x4bt
        -0x12t
        0x10t
        -0xft
        0x9t
        -0x7t
        -0x4t
        0x46t
        -0x46t
        -0x5t
        0x9t
        0xbt
        0x45t
        -0x1dt
        0x1dt
        -0x7t
        -0x4dt
        0xft
        -0x13t
        0x4t
        0x45t
        -0x3bt
        0x9t
        -0x15t
        0x15t
        -0x12t
        0x10t
        -0xft
        0x9t
        -0x7t
        -0x4t
        0x4dt
        -0x14t
        -0x4et
        0x36t
        0x18t
        -0x4et
        0x23t
        0x14t
        0x5bt
        -0xdt
        0xat
        -0xet
        0x3t
        0x6t
        0x5t
        0x1t
        0x2t
        -0x4et
        0x4ct
        -0x2t
        -0x12t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        -0x5dt
        0x10t
        -0xft
        0x9t
        -0x7t
        -0x4t
        0x46t
        -0x46t
        0xft
        -0x13t
        0x4t
        0x54t
        -0x1dt
        0x1dt
        -0x2t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        -0x56t
        0x15t
        -0x11t
        0x52t
        -0x53t
        0x3t
        0xft
        -0xdt
        0x4et
        -0x1dt
        0x1dt
        -0x53t
        0x3t
        0xft
        -0xdt
        -0x5t
        0x18t
        -0xft
        0xdt
        0x22t
        0x0t
        -0x56t
        0x15t
        -0x11t
        0x52t
        -0x4ct
        0x3t
        -0x5t
        0x3t
        -0x8t
        0x53t
        -0x1dt
        0x1dt
        -0x44t
        -0xbt
        0xct
        -0x12t
        0x8t
        0x8t
        -0x9t
        -0x6t
        0x46t
        -0x39t
        0x2t
        -0xft
        0x2ft
        -0x27t
        0x7t
        -0x8t
        0x8t
        -0x9t
        -0x6t
        0x1t
        0x31t
        -0x37t
        0x25t
        -0xdt
        -0x6t
        0x19t
        -0x13t
        -0xct
        0x8t
        0x3dt
        0x1t
        -0x3at
        0x3at
        -0x2t
        -0x12t
        0x0t
        -0x46t
        -0x9t
        -0x3t
        0x52t
        -0x8t
        -0x4et
        0x15t
        -0x11t
        0x52t
        -0x49t
        0x49t
        -0x1dt
        0x1dt
        -0x10t
        -0xbt
        0x1bt
        -0x49t
        0x49t
        -0x1ct
        0x1ct
        -0x4ct
        0x3t
        -0x5t
        0x3t
        -0x8t
        0x45t
        -0x3et
        0x7t
        -0x9t
        0x7t
        -0xdt
        0xct
        0x2dt
        0x1bt
        -0x49t
        0x3et
        0x0t
        0x2t
        0x9t
        -0x5bt
        -0xct
        0x0t
        0x4t
        -0x3t
        0x39t
        0xbt
        0x0t
        -0x41t
        0x4t
        0xbt
        -0x18t
        0x4bt
        -0x39t
        -0x8t
        0x0t
        0x8t
        -0x5t
        0x7t
        0x37t
        -0x35t
        -0xct
        0x2t
        0x3et
        -0x44t
        -0x1t
        0x5t
        -0x3t
        0xdt
        0x36t
        -0x32t
        -0xft
        0x0t
        -0x3t
        0x44t
        -0x35t
        -0x1t
        -0xft
        0x13t
        -0x8t
        -0x3t
        -0x7t
        0x34t
        -0x2at
        0x5t
        0x27t
        -0x26t
        -0xct
        0x2t
        0x3ft
        -0x39t
        -0x8t
        0x0t
        0x8t
        -0x5t
        0x7t
        0x37t
        -0x33t
        -0xdt
        0xat
        -0xet
        0x3t
        0x6t
        0x5t
        0x36t
        -0x49t
        0x12t
        0x3t
        -0x14t
        0xdt
        0x4t
        -0x12t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        -0x56t
        0x15t
        -0x11t
        0x52t
        -0x53t
        0x3t
        0xft
        -0xdt
        -0x5t
        0x53t
        -0x1dt
        0x1dt
        -0x4ct
        0x3t
        -0x5t
        0x3t
        0x3dt
        -0x39t
        0x2t
        -0xft
        0x2ft
        -0x27t
        0x7t
        -0x8t
        0x8t
        -0x9t
        -0x6t
        0x1t
        0x31t
        -0x37t
        0x25t
        -0xdt
        -0x6t
        0x19t
        -0x13t
        -0xct
        0x8t
        0x3dt
        0x1t
        -0x4ct
        0x3t
        0xft
        -0xdt
        0x47t
        -0x2t
        -0x12t
        -0xft
        0x7t
        0xbt
        -0x11t
        0xbt
        -0x6t
        0x1t
        0x4et
        -0x43t
        -0x5t
        -0x7t
        0x0t
        -0x4t
        0xet
        0x11t
        -0x1bt
        0x4t
        0x6t
        -0x9t
        0x2dt
        -0x2ft
        0x0t
        0x48t
        -0x1t
        0x9t
        -0x5bt
        0xdt
        -0x13t
        0xbt
        -0xdt
        0x11t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        -0x4ct
        0x3t
        -0x5t
        0x3t
        0x3dt
        -0x35t
        -0x9t
        0x3t
        0x6t
        -0x8t
        0x43t
        -0x1t
        -0x12t
        -0x29t
        -0x8t
        0xdt
        0x8t
        0xdt
        -0x1bt
        0x4t
        0x6t
        -0x9t
        0x2dt
        -0x2ft
        0x0t
        0x48t
        0x1t
        0x2t
        -0x4et
        0x4ct
        -0x2t
        -0x12t
        -0x27t
        0x9t
        0x0t
        0x4ct
        -0x4ft
        -0x3t
        0x52t
        -0x45t
        -0x8t
        -0x3t
        -0x4t
        -0x5t
        0x59t
        -0x55t
        0x3t
        0x6t
        0x4ct
        -0x49t
        -0xat
        0x53t
        -0x4et
        -0x1t
        -0x5t
        0x54t
        -0x41t
        -0xbt
        0x0t
        -0x3t
        -0x8t
        0x12t
        0x1t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        -0x5ct
        0x0t
        0x5ct
        -0x4ct
        0x3t
        -0x5t
        0x3t
        0x3dt
        -0x3at
        -0xat
        0xdt
        -0x1t
        0x38t
        -0x46t
        0x5t
        0x23t
        -0x23t
        -0x8t
        0x12t
        -0xdt
        0x2ft
        -0x1et
        -0x12t
        0xet
        0x3dt
        -0x1t
        -0x5t
        -0x3bt
        -0x5t
        0xat
        -0x1t
        -0x13t
        0x29t
        -0x17t
        0x3et
        0x1t
        -0x4dt
        0xbt
        -0x7t
        0x1t
        0xbt
        -0x5t
        -0xat
        0x3t
        0x1t
        -0x4t
        0xat
        -0xbt
        0xbt
        -0xdt
        0x7t
        0xet
        -0xft
        0x0t
        0x42t
        -0x33t
        -0x12t
        0x3t
        -0x8t
        0x51t
        -0x2t
        0x9t
        -0x1et
        0x1et
        -0xdt
        -0x4t
        0x8t
        0x9t
        -0x5bt
        0x9t
        -0x15t
        0x15t
        -0x12t
        0x10t
        -0xft
        0x9t
        -0x7t
        -0x4t
        0x3at
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        -0x5dt
        0x9t
        -0x15t
        0x15t
        -0x12t
        0x10t
        -0xft
        0x9t
        -0x7t
        -0x4t
        0x3at
        -0x27t
        -0xdt
        0xat
        -0xet
        0x3t
        0x6t
        0x5t
        0x36t
        -0x42t
        -0x2t
        0x3t
        0xct
        -0x2t
        -0xet
        0x0t
        0x20t
        -0x1bt
        0x6t
        0x40t
        -0x4bt
        0x5t
        0x6t
        0x40t
        -0x1t
        -0x3t
        0xct
        -0x46t
        0x5t
        -0xbt
        -0x7t
        0xet
        0x3ct
        -0x12t
        -0x5t
        -0x7t
        0x0t
        -0x4t
        0xet
        0x11t
        -0x1bt
        0x4t
        0x6t
        -0x9t
        0x2dt
        -0x2ft
        0x0t
        0x48t
        -0x1t
        -0x12t
        -0x1bt
        0x11t
        -0x12t
        -0xdt
        -0x4t
        0x56t
        -0x49t
        -0xat
        0x53t
        -0x4dt
        0xct
        -0xdt
        0xat
        0x3t
        -0x13t
        0x5t
        -0x3t
        -0x7t
        0x0t
        0x0t
        0x0t
        -0x5dt
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        -0x49t
        0x3t
        0x46t
        -0x8t
        -0x4bt
        0x3t
        0xft
        -0xdt
        0x40t
        -0x3bt
        -0x5t
        0x0t
        0x9t
        -0xdt
        0x2at
        -0xct
        0x7t
        0x1t
        0x1et
        -0x46t
        0x5t
        0x23t
        -0x23t
        -0x8t
        0x12t
        -0xdt
        0x2ft
        -0x1et
        -0x12t
        0xet
        0x3dt
        -0x1t
        -0x5t
        -0x3bt
        -0x5t
        0xat
        -0x1t
        -0x13t
        0x29t
        -0x17t
        0x3et
        0x1t
        -0x3at
        -0xft
        0x4t
        0x3t
        0x6t
        0x2t
        -0x13t
        0xbt
        -0xdt
        0x7t
        0x48t
        -0x2t
        0x9t
        -0x1et
        0x1et
        -0xdt
        -0x4t
        0x8t
        0x9t
        -0x5bt
        0x0t
        0x0t
        0x0t
        -0x49t
        0x3t
        0x46t
        -0x8t
        -0x44t
        0x3t
        -0x5t
        0x3t
        0x3dt
        -0x3at
        -0xat
        0xdt
        -0x1t
        0x38t
        -0x46t
        0x5t
        0x23t
        -0x23t
        -0x8t
        0x12t
        -0xdt
        0x2ft
        -0x1et
        -0x12t
        0xet
        0x3dt
        -0x1t
        -0x5t
        -0x3bt
        -0x5t
        0xat
        -0x1t
        -0x13t
        0x29t
        -0x17t
        0x3et
        0x1t
        -0x4dt
        0xbt
        -0x7t
        0x1t
        0xbt
        -0x5t
        -0xat
        0x3t
        0x1t
        -0x4t
        0xat
        -0xbt
        0xbt
        -0xdt
        0x7t
        0x41t
        -0x33t
        -0x12t
        0x3t
        -0x8t
        0x51t
        -0x2t
        0x9t
        -0x1et
        0x1et
        -0xdt
        -0x4t
        0x9t
        -0x15t
        0x15t
        -0x12t
        0x10t
        -0xft
        0x9t
        -0x7t
        -0x4t
        0x3at
        -0x27t
        -0xdt
        0xat
        -0xet
        0x3t
        0x6t
        0x5t
        0x36t
        -0x42t
        -0x2t
        0x3t
        0xct
        -0x2t
        -0xet
        0x0t
        0x20t
        -0x1bt
        0x6t
        0x40t
        -0x4bt
        0x5t
        0x6t
        0x40t
        -0x1t
        0x0t
        -0x12t
        0x15t
        -0x11t
        0x52t
        -0x53t
        0x10t
        -0xft
        0x9t
        -0x7t
        -0x4t
        0x54t
        -0x1dt
        0x1dt
        -0x44t
        -0xbt
        0xct
        -0x12t
        0x8t
        0x8t
        -0x9t
        -0x6t
        0x46t
        -0x35t
        -0xft
        0xdt
        0x4t
        -0x13t
        0xft
        0x20t
        -0x27t
        0x7t
        -0x8t
        0x8t
        -0x9t
        -0x6t
        0x4ct
        0x1t
        -0x4ct
        0x10t
        -0xft
        0x9t
        -0x7t
        -0x4t
        0x4dt
        -0x2t
        -0x12t
        0x1t
        0x2t
        -0x4et
        0x4ct
        -0x5t
        0xct
        -0x7t
        0x2t
        -0x4et
        0x4ct
        -0x5t
        0xct
        -0x7t
        0x2t
        -0x4et
        0x4ct
        -0x2t
        -0x12t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        -0x46t
        -0x9t
        -0x3t
        0x52t
        -0x8t
        -0x4et
        0x15t
        -0x11t
        0x52t
        -0x4at
        0x4at
        -0x1dt
        0x1dt
        -0x10t
        -0xbt
        0x1bt
        -0x4at
        0x4at
        -0x1ct
        0x1ct
        -0x53t
        0x3t
        0xft
        -0xdt
        -0x5t
        0x45t
        -0x3et
        0x7t
        -0x9t
        0x7t
        -0xdt
        0xct
        0x2dt
        0x1bt
        -0x4at
        0x3ft
        0x0t
        0x2t
        0x9t
        -0x5bt
        0x0t
        0x0t
        0x0t
        -0x56t
        0x15t
        -0x11t
        0x52t
        -0x4ct
        0x3t
        -0x5t
        0x3t
        0x4bt
        -0x1dt
        0x1dt
        -0x4ct
        0x3t
        -0x5t
        0x3t
        -0x8t
        0x18t
        -0xet
        0xct
        0x22t
        -0xdt
        0xat
        -0xet
        0x3t
        0x6t
        0x5t
        0x36t
        -0x3bt
        -0x5t
        -0x6t
        0xft
        -0x9t
        -0x6t
        0x46t
        -0x33t
        -0x2t
        -0x11t
        0xbt
        -0x6t
        0x1t
        0x40t
        -0x28t
        0xdt
        0x4t
        -0x12t
        0x7t
        -0x7t
        0x2t
        0xet
        0x1t
        0x0t
        -0x5dt
    .end array-data
.end method

.method public constructor <init>()V
    .registers 4

    .line 42
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 53
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0x6b

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x1cb

    aget-byte v1, v1, v2

    neg-int v1, v1

    const/16 v2, 0x3d1

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->unusedTAG:Ljava/lang/String;

    .line 55
    new-instance v0, Lokhttp3/FormBody$Builder;

    invoke-direct {v0}, Lokhttp3/FormBody$Builder;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->reqBuilder:Lokhttp3/FormBody$Builder;

    .line 59
    const/4 v0, -0x1

    iput v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->lastPage:I

    .line 63
    const-string v0, ""

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->urlParameters:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$Listener;
    .registers 2

    .line 42
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->listener:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$Listener;

    return-object v0
.end method

.method static synthetic access$100(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;)Landroid/webkit/WebView;
    .registers 2

    .line 42
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic access$lambda$0(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;)V
    .registers 1

    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->lambda$checkCurrentPage$0()V

    return-void
.end method

.method static synthetic access$lambda$1(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;)V
    .registers 1

    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->lambda$checkCurrentPage$1()V

    return-void
.end method

.method static synthetic access$lambda$2(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;)V
    .registers 1

    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->lambda$checkCurrentPage$2()V

    return-void
.end method

.method static synthetic access$lambda$3(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;Z)V
    .registers 2

    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->lambda$handleShn$3(Z)V

    return-void
.end method

.method static synthetic access$lambda$4(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;Landroid/content/DialogInterface;I)V
    .registers 3

    invoke-direct {p0, p1, p2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->lambda$showUpdateWebviewDialog$4(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method static synthetic access$lambda$5(Landroid/content/DialogInterface;I)V
    .registers 2

    invoke-static {p0, p1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->lambda$showUpdateWebviewDialog$5(Landroid/content/DialogInterface;I)V

    return-void
.end method

.method static synthetic access$lambda$6(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4

    invoke-direct {p0, p1, p2, p3}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->lambda$onError$6(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$lambda$7(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4

    invoke-direct {p0, p1, p2, p3}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->lambda$onSuccess$7(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private evaluateJavascript(Landroid/webkit/WebView;Ljava/lang/String;)V
    .registers 7

    .line 367
    if-nez p1, :cond_3

    return-void

    .line 369
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_30

    .line 370
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x31

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x387

    aget-byte v2, v2, v3

    const/16 v3, 0x226

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_34

    .line 372
    :cond_30
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/webkit/WebView;->evaluateJavascript(Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    .line 373
    :goto_34
    return-void
.end method

.method private finishedBypass()V
    .registers 2

    .line 345
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getTokenAppByPassValidation()Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 346
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getTokenAppByPassValidation()Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;

    move-result-object v0

    invoke-interface {v0}, Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;->onFinishedByPass()V

    .line 348
    :cond_15
    return-void
.end method

.method private handleShn(Ljava/lang/String;Z)V
    .registers 8

    .line 403
    :try_start_0
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lbr/com/itau/security/token/til/Token;->getOTP(Landroid/content/Context;Ljava/lang/String;)Lbr/com/itau/security/token/model/OTPModel;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    .line 404
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    if-eqz v0, :cond_1d

    .line 405
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->startingBypass()V

    .line 406
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {p0, p2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$$Lambda$6;->lambdaFactory$(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;Z)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_20

    .line 416
    :cond_1d
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->finishedBypass()V
    :try_end_20
    .catch Lbr/com/itau/security/token/exception/UnavailableTokenException; {:try_start_0 .. :try_end_20} :catch_21

    .line 442
    :goto_20
    goto :goto_59

    .line 419
    :catch_21
    move-exception v2

    .line 420
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    .line 421
    new-instance v3, Lbr/com/itau/sdk/android/core/token/TokenWaiter;

    invoke-direct {v3}, Lbr/com/itau/sdk/android/core/token/TokenWaiter;-><init>()V

    .line 423
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getTokenAppByPassValidation()Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;

    move-result-object v0

    if-eqz v0, :cond_45

    .line 425
    :try_start_34
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    .line 426
    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getTokenAppByPassValidation()Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;

    move-result-object v0

    .line 427
    invoke-virtual {v2}, Lbr/com/itau/security/token/exception/UnavailableTokenException;->getSecondsRemaining()I

    move-result v1

    invoke-interface {v0, v1, v3}, Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;->onWaitingForToken(ILbr/com/itau/sdk/android/core/token/TokenWaiter;)V
    :try_end_43
    .catch Ljava/lang/Exception; {:try_start_34 .. :try_end_43} :catch_44

    .line 430
    goto :goto_45

    .line 428
    :catch_44
    move-exception v4

    .line 434
    :cond_45
    :goto_45
    :try_start_45
    invoke-virtual {v2}, Lbr/com/itau/security/token/exception/UnavailableTokenException;->getSecondsRemaining()I

    move-result v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v0, v1}, Lbr/com/itau/sdk/android/core/token/TokenWaiter;->startWaiting(ILjava/util/concurrent/TimeUnit;)V

    .line 436
    invoke-virtual {v3}, Lbr/com/itau/sdk/android/core/token/TokenWaiter;->wasCanceled()Z

    move-result v0

    if-nez v0, :cond_57

    .line 437
    invoke-direct {p0, p1, p2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->handleShn(Ljava/lang/String;Z)V
    :try_end_57
    .catch Ljava/lang/InterruptedException; {:try_start_45 .. :try_end_57} :catch_58

    .line 441
    :cond_57
    goto :goto_59

    .line 440
    :catch_58
    move-exception v4

    .line 443
    :goto_59
    return-void
.end method

.method private injectTokenAppScript(Landroid/webkit/WebView;)V
    .registers 7

    .line 376
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 377
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0xf2

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x27c

    aget-byte v1, v1, v2

    const/16 v2, 0x335

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 378
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0x255

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x37

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0xab

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 379
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0x181

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0xab

    aget-byte v1, v1, v2

    const/16 v2, 0x74

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 380
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0xe1

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0xb

    aget-byte v1, v1, v2

    const/16 v2, 0x170

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 381
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0x17

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x2e

    aget-byte v1, v1, v2

    const/16 v2, 0x9d

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 382
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0x205

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x2e

    aget-byte v1, v1, v2

    const/16 v2, 0xcd

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 383
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0x206

    aget-byte v0, v0, v1

    neg-int v0, v0

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x2e

    aget-byte v1, v1, v2

    const/16 v2, 0x3a1

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 384
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0x1a8

    aget-byte v0, v0, v1

    sget v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$$:I

    or-int/lit16 v1, v1, 0x240

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x2e

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 385
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0x5d

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x2e

    aget-byte v1, v1, v2

    const/16 v2, 0x1d9

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 386
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0x2c

    aget-byte v0, v0, v1

    neg-int v0, v0

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x2e

    aget-byte v1, v1, v2

    const/16 v2, 0x13d

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 387
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0xf2

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x2e

    aget-byte v1, v1, v2

    const/16 v2, 0x374

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 388
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0xe1

    aget-byte v0, v0, v1

    sget v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$$:I

    and-int/lit16 v1, v1, 0x3e3

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x2e

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 389
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0x4a

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x2e

    aget-byte v1, v1, v2

    const/16 v2, 0x289

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 390
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    sget v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$$:I

    or-int/lit16 v1, v1, 0x101

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x2e

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 391
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/4 v1, 0x7

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x2e

    aget-byte v1, v1, v2

    const/16 v2, 0x230

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 392
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0x6a

    aget-byte v0, v0, v1

    or-int/lit8 v1, v0, 0x68

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x2e

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 393
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0x24

    aget-byte v0, v0, v1

    or-int/lit16 v1, v0, 0x281

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x2e

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 394
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0xd

    aget-byte v0, v0, v1

    or-int/lit16 v1, v0, 0x3d4

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x2e

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 395
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0x16

    aget-byte v0, v0, v1

    or-int/lit8 v1, v0, 0x60

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x77

    aget-byte v2, v2, v3

    neg-int v2, v2

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 396
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0x2e

    aget-byte v0, v0, v1

    or-int/lit16 v1, v0, 0x3d6

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0xe1

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 397
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0x37

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x2e

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x11b

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 398
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->evaluateJavascript(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 399
    return-void
.end method

.method private synthetic lambda$checkCurrentPage$0()V
    .registers 5

    .line 324
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x19

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x387

    aget-byte v2, v2, v3

    const/16 v3, 0x311

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    return-void
.end method

.method private synthetic lambda$checkCurrentPage$1()V
    .registers 6

    .line 332
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x11f

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x19

    aget-byte v2, v2, v3

    const/16 v3, 0x1a7

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    invoke-virtual {v3}, Lbr/com/itau/security/token/model/OTPModel;->token()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->evaluateJavascript(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 333
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    .line 334
    return-void
.end method

.method private synthetic lambda$checkCurrentPage$2()V
    .registers 6

    .line 337
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x29

    aget-byte v1, v1, v2

    sget v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$$:I

    shl-int/lit8 v2, v2, 0x2

    sget-object v3, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v4, 0x387

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    return-void
.end method

.method private synthetic lambda$handleShn$3(Z)V
    .registers 7

    .line 407
    if-eqz p1, :cond_1f

    .line 408
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->injectTokenAppScript(Landroid/webkit/WebView;)V

    .line 409
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x39

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x1a8

    aget-byte v2, v2, v3

    const/16 v3, 0x263

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->evaluateJavascript(Landroid/webkit/WebView;Ljava/lang/String;)V

    goto :goto_4a

    .line 410
    :cond_1f
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    if-eqz v0, :cond_4a

    .line 411
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x11f

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x19

    aget-byte v2, v2, v3

    const/16 v3, 0x1a7

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    .line 412
    invoke-virtual {v3}, Lbr/com/itau/security/token/model/OTPModel;->token()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 411
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->evaluateJavascript(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 414
    :cond_4a
    :goto_4a
    return-void
.end method

.method private synthetic lambda$onError$6(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9

    .line 505
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x13b

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/4 v4, 0x7

    aget-byte v3, v3, v4

    const/16 v4, 0x362

    invoke-static {v2, v4, v3}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const-string v3, ""

    const/4 v4, 0x1

    aput-object v3, v2, v4

    const/4 v3, 0x2

    aput-object p3, v2, v3

    .line 506
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 505
    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->evaluateJavascript(Landroid/webkit/WebView;Ljava/lang/String;)V

    return-void
.end method

.method private synthetic lambda$onSuccess$7(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 9

    .line 513
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x13b

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/4 v4, 0x7

    aget-byte v3, v3, v4

    const/16 v4, 0x362

    invoke-static {v2, v4, v3}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    const-string v3, ""

    const/4 v4, 0x2

    aput-object v3, v2, v4

    .line 514
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 513
    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->evaluateJavascript(Landroid/webkit/WebView;Ljava/lang/String;)V

    return-void
.end method

.method private synthetic lambda$showUpdateWebviewDialog$4(Landroid/content/DialogInterface;I)V
    .registers 10

    .line 468
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 469
    new-instance v5, Landroid/content/Intent;

    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0x1c

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x309

    aget-byte v1, v1, v2

    const/16 v2, 0x3b8

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x171

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x18a

    aget-byte v2, v2, v3

    const/16 v3, 0xf6

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v1

    .line 470
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v5, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 471
    const/high16 v0, 0x10000

    invoke-virtual {v4, v5, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    .line 474
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_44

    .line 475
    invoke-virtual {p0, v5}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->startActivity(Landroid/content/Intent;)V

    .line 477
    :cond_44
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 478
    return-void
.end method

.method private static synthetic lambda$showUpdateWebviewDialog$5(Landroid/content/DialogInterface;I)V
    .registers 2

    .line 479
    invoke-interface {p0}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method private showUpdateWebviewDialog(Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;)V
    .registers 6

    .line 465
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lbr/com/itau/sdk/android/core/R$style;->itausdkcore_WebViewDialogError:I

    invoke-direct {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 466
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lbr/com/itau/sdk/android/core/R$string;->itausdkcore_webview_update_positve:I

    invoke-static {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$$Lambda$7;->lambdaFactory$(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    .line 467
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lbr/com/itau/sdk/android/core/R$string;->itausdkcore_webview_update_negative:I

    invoke-static {}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$$Lambda$8;->lambdaFactory$()Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    .line 479
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    move-result-object v3

    .line 482
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getSdkCustomUi()Lbr/com/itau/sdk/android/core/SDKCustomUi;

    move-result-object v0

    invoke-interface {v0}, Lbr/com/itau/sdk/android/core/SDKCustomUi;->getThemeManager()Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;->checkDialogTheme(Landroid/support/v7/app/AlertDialog;)Landroid/support/v7/app/AlertDialog;

    .line 484
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->incrementHowManyUpdateDialogShowed(Landroid/content/Context;)V

    .line 485
    return-void
.end method

.method private startingBypass()V
    .registers 2

    .line 303
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getTokenAppByPassValidation()Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 304
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getTokenAppByPassValidation()Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;

    move-result-object v0

    invoke-interface {v0}, Lbr/com/itau/sdk/android/core/token/TokenAppByPassValidation;->onStartedByPass()V

    .line 306
    :cond_15
    return-void
.end method


# virtual methods
.method public addParameter(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;
    .registers 6

    .line 177
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0xd

    aget-byte v0, v0, v1

    or-int/lit8 v1, v0, 0x5d

    and-int/lit16 v2, v1, 0x1f8

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_19

    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->reqBuilder:Lokhttp3/FormBody$Builder;

    invoke-virtual {v0, p1, p2}, Lokhttp3/FormBody$Builder;->add(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/FormBody$Builder;

    .line 178
    :cond_19
    return-object p0
.end method

.method public addParameterUrl(Ljava/lang/String;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;
    .registers 8

    .line 187
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0xd

    aget-byte v0, v0, v1

    or-int/lit8 v1, v0, 0x5d

    and-int/lit16 v2, v1, 0x1f8

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7b

    .line 188
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->urlParameters:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_43

    .line 189
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->urlParameters:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x2e

    aget-byte v1, v1, v2

    or-int/lit16 v2, v1, 0x263

    sget-object v3, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v4, 0x6a

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->urlParameters:Ljava/lang/String;

    .line 191
    :cond_43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->urlParameters:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x24

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x65

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v4, 0x6b

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->urlParameters:Ljava/lang/String;

    .line 193
    :cond_7b
    return-object p0
.end method

.method public canGoBack()Z
    .registers 2

    .line 254
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    return v0
.end method

.method public checkCurrentPage(ZZ)V
    .registers 6
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .line 312
    if-eqz p2, :cond_6

    if-eqz p1, :cond_6

    .line 313
    const/4 v2, 0x1

    goto :goto_18

    .line 314
    :cond_6
    if-eqz p1, :cond_a

    .line 315
    const/4 v2, 0x0

    goto :goto_18

    .line 317
    :cond_a
    iget v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->lastPage:I

    if-eqz v0, :cond_13

    iget v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->lastPage:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_16

    .line 318
    :cond_13
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->finishedBypass()V

    .line 320
    :cond_16
    const/16 v2, 0xff

    .line 323
    :goto_18
    if-nez v2, :cond_26

    .line 324
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$$Lambda$1;->lambdaFactory$(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_53

    .line 325
    :cond_26
    const/4 v0, 0x1

    if-ne v2, v0, :cond_53

    .line 326
    iget v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->lastPage:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_34

    .line 327
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->finishedBypass()V

    .line 328
    const/16 v2, 0xff

    goto :goto_53

    .line 329
    :cond_34
    iget v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->lastPage:I

    if-nez v0, :cond_48

    .line 330
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->otpModel:Lbr/com/itau/security/token/model/OTPModel;

    if-eqz v0, :cond_53

    .line 331
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$$Lambda$4;->lambdaFactory$(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_53

    .line 337
    :cond_48
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$$Lambda$5;->lambdaFactory$(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 341
    :cond_53
    :goto_53
    iput v2, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->lastPage:I

    .line 342
    return-void
.end method

.method public checkWebviewVersion(Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;Landroid/content/pm/PackageInfo;)V
    .registers 12

    .line 447
    if-eqz p1, :cond_8d

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->isCheckVersion()Z

    move-result v0

    if-eqz v0, :cond_8d

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->getAndroidVersion()I

    move-result v1

    if-ge v0, v1, :cond_8d

    .line 448
    iget-object v6, p2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 450
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->getHowManyUpdateDialogShowed(Landroid/content/Context;)I

    move-result v7

    .line 452
    if-eqz v6, :cond_8a

    .line 453
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0x2e

    aget-byte v0, v0, v1

    or-int/lit16 v1, v0, 0x263

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x2e

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x2e

    aget-byte v1, v1, v2

    or-int/lit16 v2, v1, 0x2ce

    sget-object v3, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v4, 0x181

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x2e

    aget-byte v2, v2, v3

    or-int/lit16 v3, v2, 0x3d6

    sget-object v4, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v5, 0xe1

    aget-byte v4, v4, v5

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x2e

    aget-byte v1, v1, v2

    or-int/lit16 v2, v1, 0x3d6

    sget-object v3, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v4, 0xe1

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v8, v0, v1

    .line 454
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->getWebviewVersion()I

    move-result v1

    if-ge v0, v1, :cond_89

    .line 455
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->getTimesToShow()I

    move-result v0

    if-ge v7, v0, :cond_89

    .line 456
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->showUpdateWebviewDialog(Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;)V

    .line 458
    :cond_89
    goto :goto_8d

    .line 459
    :cond_8a
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->showUpdateWebviewDialog(Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;)V

    .line 462
    :cond_8d
    :goto_8d
    return-void
.end method

.method public clearHistory()V
    .registers 2

    .line 262
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->clearHistory()V

    .line 263
    return-void
.end method

.method public getItem(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .line 284
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->currentActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;->getInstance(Landroid/content/Context;)Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;->findItem(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLastPage()I
    .registers 2

    .line 266
    iget v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->lastPage:I

    return v0
.end method

.method public getURLWithParameter(Ljava/lang/String;)Ljava/lang/String;
    .registers 7

    .line 197
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->urlParameters:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 198
    return-object p1

    .line 200
    :cond_9
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0x2e

    aget-byte v0, v0, v1

    or-int/lit16 v1, v0, 0x3d8

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x255

    aget-byte v2, v2, v3

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_36

    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0x2e

    aget-byte v0, v0, v1

    or-int/lit16 v1, v0, 0x263

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x6a

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v4

    goto :goto_4a

    :cond_36
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v1, 0x2e

    aget-byte v0, v0, v1

    or-int/lit16 v1, v0, 0x3d8

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x255

    aget-byte v2, v2, v3

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v4

    .line 201
    :goto_4a
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->urlParameters:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public goBack()V
    .registers 2

    .line 258
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 259
    return-void
.end method

.method public init(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;
    .registers 9

    .line 236
    if-nez p1, :cond_1a

    .line 237
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x13b

    aget-byte v1, v1, v2

    or-int/lit16 v2, v1, 0x261

    sget-object v3, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v4, 0x205

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 239
    :cond_1a
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->urlUsabilidade()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->getURLWithParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 241
    const/4 v0, -0x1

    iput v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->lastPage:I

    .line 242
    new-instance v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$3;

    iget-object v4, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->reqBuilder:Lokhttp3/FormBody$Builder;

    move-object v1, p0

    move-object v2, v6

    move-object v3, p1

    move-object v5, v6

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$3;-><init>(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;Ljava/lang/String;Ljava/lang/String;Lokhttp3/FormBody$Builder;Ljava/lang/String;)V

    .line 243
    invoke-static {v0}, Lbr/com/itau/sdk/android/core/j;->a(Lbr/com/itau/sdk/android/core/j$b;)V

    .line 250
    return-object p0
.end method

.method public initWithUrl(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;
    .registers 7

    .line 217
    if-eqz p1, :cond_c

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 218
    :cond_c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x255

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0xf

    aget-byte v2, v2, v3

    add-int/lit8 v2, v2, -0x1

    const/16 v3, 0x1ba

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :cond_28
    invoke-virtual {p0, p1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->getURLWithParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 223
    const/4 v0, -0x1

    iput v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->lastPage:I

    .line 224
    new-instance v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$2;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->reqBuilder:Lokhttp3/FormBody$Builder;

    invoke-direct {v0, p0, v4, v1, v4}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$2;-><init>(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;Ljava/lang/String;Lokhttp3/FormBody$Builder;Ljava/lang/String;)V

    .line 225
    invoke-static {v0}, Lbr/com/itau/sdk/android/core/j;->a(Lbr/com/itau/sdk/android/core/j$c;)V

    .line 232
    return-object p0
.end method

.method public loadUrl(Ljava/lang/String;)V
    .registers 4

    .line 166
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    return-void

    .line 168
    :cond_7
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    invoke-virtual {p0, p1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->getURLWithParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 169
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 10

    .line 490
    const/4 v0, -0x1

    if-ne v0, p2, :cond_51

    const/16 v0, 0x10

    if-ne p1, v0, :cond_51

    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->responseNativeFeature:Ljava/lang/String;

    if-eqz v0, :cond_51

    .line 491
    if-eqz p3, :cond_13

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_16

    :cond_13
    const-string v5, ""

    goto :goto_1e

    :cond_16
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    .line 492
    :goto_1e
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->responseNativeFeature:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x6a

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/4 v4, 0x7

    aget-byte v3, v3, v4

    const/16 v4, 0x68

    invoke-static {v2, v4, v3}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v5, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->evaluateJavascript(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 494
    :cond_51
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .line 70
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p0}, Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck;->performCheck(Landroid/content/Context;Lbr/com/itau/sdk/android/core/webview/WebviewVersionCheck$Contract;)V

    .line 72
    invoke-static {p0}, Lbr/com/itau/sdk/android/core/webview/RouterRequestJs;->newInstance(Lbr/com/itau/sdk/android/core/webview/RouterRequestJs$RouterRequestResult;)Lbr/com/itau/sdk/android/core/webview/RouterRequestJs;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->requestJavascriptInterface:Lbr/com/itau/sdk/android/core/webview/RouterRequestJs;

    .line 74
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    if-eqz v0, :cond_16

    .line 75
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 77
    :cond_16
    new-instance v0, Landroid/webkit/WebView;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    .line 78
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v5

    .line 79
    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 80
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v2, 0x6a

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x309

    aget-byte v2, v2, v3

    const/16 v3, 0x62

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->requestJavascriptInterface:Lbr/com/itau/sdk/android/core/webview/RouterRequestJs;

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x6b

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v4, 0x95

    aget-byte v3, v3, v4

    const/16 v4, 0x18a

    invoke-static {v2, v4, v3}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/webkit/WebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    new-instance v1, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$1;

    invoke-direct {v1, p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$1;-><init>(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 131
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    return-object v0
.end method

.method public onDestroy()V
    .registers 2

    .line 157
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    if-eqz v0, :cond_f

    .line 158
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    .line 160
    const/4 v0, -0x1

    iput v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->lastPage:I

    .line 162
    :cond_f
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 163
    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6

    .line 504
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {p0, p3, p1, p2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$$Lambda$9;->lambdaFactory$(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 508
    return-void
.end method

.method public onPause()V
    .registers 2

    .line 139
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 140
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->onPause()V

    .line 141
    return-void
.end method

.method public onResume()V
    .registers 2

    .line 148
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->onResume()V

    .line 149
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 150
    return-void
.end method

.method public onSuccess(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6

    .line 512
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {p0, p3, p1, p2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$$Lambda$10;->lambdaFactory$(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 515
    return-void
.end method

.method public processPageTitle(Ljava/lang/String;)V
    .registers 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .line 278
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->listener:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$Listener;

    if-eqz v0, :cond_9

    .line 279
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->listener:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$Listener;

    invoke-interface {v0, p1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$Listener;->pageTitle(Ljava/lang/String;)V

    .line 280
    :cond_9
    return-void
.end method

.method public processSnh(Ljava/lang/String;)V
    .registers 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .line 353
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->processSnh(Ljava/lang/String;Z)V

    .line 354
    return-void
.end method

.method public processSnh(Ljava/lang/String;Z)V
    .registers 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .line 359
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 360
    return-void

    .line 363
    :cond_7
    invoke-direct {p0, p1, p2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->handleShn(Ljava/lang/String;Z)V

    .line 364
    return-void
.end method

.method public registerOnLocalstoragewebviewChangeListener(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$OnLocaStorageWebviewChangeListener;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;
    .registers 2

    .line 212
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->onLocaStorageWebviewChangeListener:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$OnLocaStorageWebviewChangeListener;

    .line 213
    return-object p0
.end method

.method public requestNativeFeature(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .line 298
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->responseNativeFeature:Ljava/lang/String;

    .line 299
    new-instance v0, Lbr/com/itau/sdk/android/core/webview/WebviewNativeAdapter;

    invoke-direct {v0, p2, p0}, Lbr/com/itau/sdk/android/core/webview/WebviewNativeAdapter;-><init>(Ljava/lang/String;Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;)V

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/webview/WebviewNativeAdapter;->resolveIntent()V

    .line 300
    return-void
.end method

.method public responseNativeFeature(Ljava/lang/String;)V
    .registers 7

    .line 497
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->responseNativeFeature:Ljava/lang/String;

    if-eqz v0, :cond_37

    .line 498
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->webView:Landroid/webkit/WebView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->responseNativeFeature:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/16 v3, 0x6a

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$:[B

    const/4 v4, 0x7

    aget-byte v3, v3, v4

    const/16 v4, 0x68

    invoke-static {v2, v4, v3}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->$(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->evaluateJavascript(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 500
    :cond_37
    return-void
.end method

.method public setItem(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .line 290
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->currentActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;->getInstance(Landroid/content/Context;)Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;->save(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->onLocaStorageWebviewChangeListener:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$OnLocaStorageWebviewChangeListener;

    if-eqz v0, :cond_14

    .line 293
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->onLocaStorageWebviewChangeListener:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$OnLocaStorageWebviewChangeListener;

    invoke-interface {v0, p2}, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$OnLocaStorageWebviewChangeListener;->onLocaStorageWebviewChange(Ljava/lang/String;)V

    .line 294
    :cond_14
    return-void
.end method

.method public setLastPage(I)V
    .registers 3

    .line 270
    const/16 v0, 0xff

    if-eq p1, v0, :cond_9

    const/4 v0, 0x1

    if-eq p1, v0, :cond_9

    if-nez p1, :cond_b

    .line 271
    :cond_9
    iput p1, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->lastPage:I

    .line 273
    :cond_b
    return-void
.end method

.method public withListener(Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$Listener;)Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;
    .registers 2

    .line 206
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;->listener:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$Listener;

    .line 207
    return-object p0
.end method

.class Lcom/google/android/gms/internal/zzhf$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/zzdf;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/zzhf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic zzJm:Lcom/google/android/gms/internal/zzhf;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/zzhf;)V
    .registers 2

    iput-object p1, p0, Lcom/google/android/gms/internal/zzhf$1;->zzJm:Lcom/google/android/gms/internal/zzhf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public zza(Lcom/google/android/gms/internal/zzjp;Ljava/util/Map;)V
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/google/android/gms/internal/zzjp;Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/internal/zzhf$1;->zzJm:Lcom/google/android/gms/internal/zzhf;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzhf;->zza(Lcom/google/android/gms/internal/zzhf;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    :try_start_7
    iget-object v0, p0, Lcom/google/android/gms/internal/zzhf$1;->zzJm:Lcom/google/android/gms/internal/zzhf;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzhf;->zzb(Lcom/google/android/gms/internal/zzhf;)Lcom/google/android/gms/internal/zzjd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzjd;->isDone()Z
    :try_end_10
    .catchall {:try_start_7 .. :try_end_10} :catchall_62

    move-result v0

    if-eqz v0, :cond_15

    monitor-exit v2

    return-void

    :cond_15
    :try_start_15
    iget-object v0, p0, Lcom/google/android/gms/internal/zzhf$1;->zzJm:Lcom/google/android/gms/internal/zzhf;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzhf;->zzc(Lcom/google/android/gms/internal/zzhf;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "request_id"

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_24
    .catchall {:try_start_15 .. :try_end_24} :catchall_62

    move-result v0

    if-nez v0, :cond_29

    monitor-exit v2

    return-void

    :cond_29
    :try_start_29
    new-instance v3, Lcom/google/android/gms/internal/zzhi;

    const/4 v0, 0x1

    invoke-direct {v3, v0, p2}, Lcom/google/android/gms/internal/zzhi;-><init>(ILjava/util/Map;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/android/gms/internal/zzhi;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " request error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3}, Lcom/google/android/gms/internal/zzhi;->zzgE()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzhf$1;->zzJm:Lcom/google/android/gms/internal/zzhf;

    invoke-static {v0}, Lcom/google/android/gms/internal/zzhf;->zzb(Lcom/google/android/gms/internal/zzhf;)Lcom/google/android/gms/internal/zzjd;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/internal/zzjd;->zzg(Ljava/lang/Object;)V
    :try_end_60
    .catchall {:try_start_29 .. :try_end_60} :catchall_62

    monitor-exit v2

    goto :goto_65

    :catchall_62
    move-exception v4

    monitor-exit v2

    throw v4

    :goto_65
    return-void
.end method

.class public Lcom/google/gson/stream/JsonReader;
.super Ljava/lang/Object;
.source "JsonReader.java"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static final NON_EXECUTE_PREFIX:[C


# instance fields
.field private final buffer:[C

.field private final in:Ljava/io/Reader;

.field private lenient:Z

.field private limit:I

.field private lineNumber:I

.field private lineStart:I

.field private pathIndices:[I

.field private pathNames:[Ljava/lang/String;

.field peeked:I

.field private peekedLong:J

.field private peekedNumberLength:I

.field private peekedString:Ljava/lang/String;

.field private pos:I

.field private stack:[I

.field private stackSize:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 192
    const-string v0, ")]}\'\n"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/gson/stream/JsonReader;->NON_EXECUTE_PREFIX:[C

    .line 1585
    new-instance v0, Lcom/google/gson/stream/JsonReader$1;

    invoke-direct {v0}, Lcom/google/gson/stream/JsonReader$1;-><init>()V

    sput-object v0, Lcom/google/gson/internal/JsonReaderInternalAccess;->INSTANCE:Lcom/google/gson/internal/JsonReaderInternalAccess;

    .line 1607
    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .registers 5
    .param p1, "in"    # Ljava/io/Reader;

    .line 289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/gson/stream/JsonReader;->lenient:Z

    .line 238
    const/16 v0, 0x400

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    .line 239
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 240
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    .line 242
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I

    .line 243
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I

    .line 245
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 269
    const/16 v0, 0x20

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/gson/stream/JsonReader;->stack:[I

    .line 270
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    .line 272
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->stack:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    const/4 v2, 0x6

    aput v2, v0, v1

    .line 283
    const/16 v0, 0x20

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathNames:[Ljava/lang/String;

    .line 284
    const/16 v0, 0x20

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathIndices:[I

    .line 290
    if-nez p1, :cond_45

    .line 291
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "in == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 293
    :cond_45
    iput-object p1, p0, Lcom/google/gson/stream/JsonReader;->in:Ljava/io/Reader;

    .line 294
    return-void
.end method

.method static synthetic access$000(Lcom/google/gson/stream/JsonReader;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/google/gson/stream/JsonReader;

    .line 190
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private checkLenient()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1400
    iget-boolean v0, p0, Lcom/google/gson/stream/JsonReader;->lenient:Z

    if-nez v0, :cond_b

    .line 1401
    const-string v0, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1403
    :cond_b
    return-void
.end method

.method private consumeNonExecutePrefix()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1567
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->nextNonWhitespace(Z)I

    .line 1568
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1570
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    sget-object v1, Lcom/google/gson/stream/JsonReader;->NON_EXECUTE_PREFIX:[C

    array-length v1, v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    if-le v0, v1, :cond_1e

    sget-object v0, Lcom/google/gson/stream/JsonReader;->NON_EXECUTE_PREFIX:[C

    array-length v0, v0

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-nez v0, :cond_1e

    .line 1571
    return-void

    .line 1574
    :cond_1e
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1f
    sget-object v0, Lcom/google/gson/stream/JsonReader;->NON_EXECUTE_PREFIX:[C

    array-length v0, v0

    if-ge v2, v0, :cond_35

    .line 1575
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v1, v2

    aget-char v0, v0, v1

    sget-object v1, Lcom/google/gson/stream/JsonReader;->NON_EXECUTE_PREFIX:[C

    aget-char v1, v1, v2

    if-eq v0, v1, :cond_32

    .line 1576
    return-void

    .line 1574
    :cond_32
    add-int/lit8 v2, v2, 0x1

    goto :goto_1f

    .line 1581
    .end local v2    # "i":I
    :cond_35
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    sget-object v1, Lcom/google/gson/stream/JsonReader;->NON_EXECUTE_PREFIX:[C

    array-length v1, v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1582
    return-void
.end method

.method private fillBuffer(I)Z
    .registers 8
    .param p1, "minimum"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1276
    iget-object v4, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    .line 1277
    .local v4, "buffer":[C
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I

    .line 1278
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    if-eq v0, v1, :cond_1f

    .line 1279
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    .line 1280
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    const/4 v2, 0x0

    invoke-static {v4, v0, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_22

    .line 1282
    :cond_1f
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    .line 1285
    :goto_22
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1287
    :cond_25
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->in:Ljava/io/Reader;

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    array-length v2, v4

    iget v3, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    sub-int/2addr v2, v3

    invoke-virtual {v0, v4, v1, v2}, Ljava/io/Reader;->read([CII)I

    move-result v0

    move v5, v0

    .local v5, "total":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_62

    .line 1288
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    add-int/2addr v0, v5

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    .line 1291
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I

    if-nez v0, :cond_5c

    iget v0, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I

    if-nez v0, :cond_5c

    iget v0, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    if-lez v0, :cond_5c

    const/4 v0, 0x0

    aget-char v0, v4, v0

    const v1, 0xfeff

    if-ne v0, v1, :cond_5c

    .line 1292
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1293
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I

    .line 1294
    add-int/lit8 p1, p1, 0x1

    .line 1297
    :cond_5c
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    if-lt v0, p1, :cond_25

    .line 1298
    const/4 v0, 0x1

    return v0

    .line 1301
    :cond_62
    const/4 v0, 0x0

    return v0
.end method

.method private isLiteral(C)Z
    .registers 3
    .param p1, "c"    # C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 745
    sparse-switch p1, :sswitch_data_c

    goto/16 :goto_a

    .line 751
    :sswitch_5
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V

    .line 763
    :sswitch_8
    const/4 v0, 0x0

    return v0

    .line 765
    :goto_a
    const/4 v0, 0x1

    return v0

    :sswitch_data_c
    .sparse-switch
        0x9 -> :sswitch_8
        0xa -> :sswitch_8
        0xc -> :sswitch_8
        0xd -> :sswitch_8
        0x20 -> :sswitch_8
        0x23 -> :sswitch_5
        0x2c -> :sswitch_8
        0x2f -> :sswitch_5
        0x3a -> :sswitch_8
        0x3b -> :sswitch_5
        0x3d -> :sswitch_5
        0x5b -> :sswitch_8
        0x5c -> :sswitch_5
        0x5d -> :sswitch_8
        0x7b -> :sswitch_8
        0x7d -> :sswitch_8
    .end sparse-switch
.end method

.method private locationString()Ljava/lang/String;
    .registers 5

    .line 1449
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I

    add-int/lit8 v2, v0, 0x1

    .line 1450
    .local v2, "line":I
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I

    sub-int/2addr v0, v1

    add-int/lit8 v3, v0, 0x1

    .line 1451
    .local v3, "column":I
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " at line "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " column "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " path "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private nextNonWhitespace(Z)I
    .registers 10
    .param p1, "throwOnEof"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1319
    iget-object v3, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    .line 1320
    .local v3, "buffer":[C
    iget v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1321
    .local v4, "p":I
    iget v5, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    .line 1323
    .local v5, "l":I
    :cond_6
    :goto_6
    if-ne v4, v5, :cond_17

    .line 1324
    iput v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1325
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-nez v0, :cond_13

    .line 1326
    goto/16 :goto_a0

    .line 1328
    :cond_13
    iget v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1329
    iget v5, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    .line 1332
    :cond_17
    move v0, v4

    add-int/lit8 v4, v4, 0x1

    aget-char v6, v3, v0

    .line 1333
    .local v6, "c":I
    const/16 v0, 0xa

    if-ne v6, v0, :cond_29

    .line 1334
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I

    .line 1335
    iput v4, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I

    .line 1336
    goto :goto_6

    .line 1337
    :cond_29
    const/16 v0, 0x20

    if-eq v6, v0, :cond_6

    const/16 v0, 0xd

    if-eq v6, v0, :cond_6

    const/16 v0, 0x9

    if-ne v6, v0, :cond_36

    .line 1338
    goto :goto_6

    .line 1341
    :cond_36
    const/16 v0, 0x2f

    if-ne v6, v0, :cond_8a

    .line 1342
    iput v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1343
    if-ne v4, v5, :cond_52

    .line 1344
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1345
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v7

    .line 1346
    .local v7, "charsLoaded":Z
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1347
    if-nez v7, :cond_52

    .line 1348
    return v6

    .line 1352
    .end local v7    # "charsLoaded":Z
    :cond_52
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V

    .line 1353
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    aget-char v7, v3, v0

    .line 1354
    .local v7, "peek":C
    sparse-switch v7, :sswitch_data_c2

    goto :goto_89

    .line 1357
    :sswitch_5d
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1358
    const-string v0, "*/"

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->skipTo(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_72

    .line 1359
    const-string v0, "Unterminated comment"

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1361
    :cond_72
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/lit8 v4, v0, 0x2

    .line 1362
    iget v5, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    .line 1363
    goto/16 :goto_6

    .line 1367
    :sswitch_7a
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1368
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->skipToEndOfLine()V

    .line 1369
    iget v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1370
    iget v5, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    .line 1371
    goto/16 :goto_6

    .line 1374
    :goto_89
    return v6

    .line 1376
    .end local v7    # "peek":C
    :cond_8a
    const/16 v0, 0x23

    if-ne v6, v0, :cond_9b

    .line 1377
    iput v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1383
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V

    .line 1384
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->skipToEndOfLine()V

    .line 1385
    iget v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1386
    iget v5, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    goto :goto_9e

    .line 1388
    :cond_9b
    iput v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1389
    return v6

    .line 1391
    .end local v6    # "c":I
    :goto_9e
    goto/16 :goto_6

    .line 1392
    :goto_a0
    if-eqz p1, :cond_bf

    .line 1393
    new-instance v0, Ljava/io/EOFException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "End of input"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1395
    :cond_bf
    const/4 v0, -0x1

    return v0

    nop

    :sswitch_data_c2
    .sparse-switch
        0x2a -> :sswitch_5d
        0x2f -> :sswitch_7a
    .end sparse-switch
.end method

.method private nextQuotedValue(C)Ljava/lang/String;
    .registers 10
    .param p1, "quote"    # C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 987
    iget-object v2, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    .line 988
    .local v2, "buffer":[C
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 990
    .local v3, "builder":Ljava/lang/StringBuilder;
    :goto_7
    iget v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 991
    .local v4, "p":I
    iget v5, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    .line 993
    .local v5, "l":I
    move v6, v4

    .line 994
    .local v6, "start":I
    :goto_c
    if-ge v4, v5, :cond_4a

    .line 995
    move v0, v4

    add-int/lit8 v4, v4, 0x1

    aget-char v7, v2, v0

    .line 997
    .local v7, "c":I
    if-ne v7, p1, :cond_23

    .line 998
    iput v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 999
    sub-int v0, v4, v6

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v3, v2, v6, v0}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 1000
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1001
    :cond_23
    const/16 v0, 0x5c

    if-ne v7, v0, :cond_3d

    .line 1002
    iput v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1003
    sub-int v0, v4, v6

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v3, v2, v6, v0}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 1004
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->readEscapeCharacter()C

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1005
    iget v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1006
    iget v5, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    .line 1007
    move v6, v4

    goto :goto_49

    .line 1008
    :cond_3d
    const/16 v0, 0xa

    if-ne v7, v0, :cond_49

    .line 1009
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I

    .line 1010
    iput v4, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I

    .line 1012
    .end local v7    # "c":I
    :cond_49
    :goto_49
    goto :goto_c

    .line 1014
    :cond_4a
    sub-int v0, v4, v6

    invoke-virtual {v3, v2, v6, v0}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 1015
    iput v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1016
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-nez v0, :cond_5f

    .line 1017
    const-string v0, "Unterminated string"

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1019
    .end local v4    # "p":I
    .end local v5    # "l":I
    .end local v6    # "start":I
    :cond_5f
    goto/16 :goto_7
.end method

.method private nextUnquotedValue()Ljava/lang/String;
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1027
    const/4 v2, 0x0

    .line 1028
    .local v2, "builder":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .line 1032
    .local v3, "i":I
    :cond_2
    :goto_2
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v0, v3

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    if-ge v0, v1, :cond_1d

    .line 1033
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v1, v3

    aget-char v0, v0, v1

    sparse-switch v0, :sswitch_data_64

    goto/16 :goto_19

    .line 1039
    :sswitch_15
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V

    .line 1051
    :sswitch_18
    goto :goto_47

    .line 1032
    :goto_19
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    .line 1056
    :cond_1d
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    array-length v0, v0

    if-ge v3, v0, :cond_2c

    .line 1057
    add-int/lit8 v0, v3, 0x1

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-eqz v0, :cond_47

    .line 1058
    goto/16 :goto_2

    .line 1065
    :cond_2c
    if-nez v2, :cond_33

    .line 1066
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1068
    :cond_33
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    invoke-virtual {v2, v0, v1, v3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 1069
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1070
    const/4 v3, 0x0

    .line 1071
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1072
    .line 1077
    :cond_47
    :goto_47
    if-nez v2, :cond_53

    .line 1078
    new-instance v4, Ljava/lang/String;

    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    invoke-direct {v4, v0, v1, v3}, Ljava/lang/String;-><init>([CII)V

    .local v4, "result":Ljava/lang/String;
    goto :goto_5e

    .line 1080
    .end local v4    # "result":Ljava/lang/String;
    :cond_53
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    invoke-virtual {v2, v0, v1, v3}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    .line 1081
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1083
    .local v4, "result":Ljava/lang/String;
    :goto_5e
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1084
    return-object v4

    :sswitch_data_64
    .sparse-switch
        0x9 -> :sswitch_18
        0xa -> :sswitch_18
        0xc -> :sswitch_18
        0xd -> :sswitch_18
        0x20 -> :sswitch_18
        0x23 -> :sswitch_15
        0x2c -> :sswitch_18
        0x2f -> :sswitch_15
        0x3a -> :sswitch_18
        0x3b -> :sswitch_15
        0x3d -> :sswitch_15
        0x5b -> :sswitch_18
        0x5c -> :sswitch_15
        0x5d -> :sswitch_18
        0x7b -> :sswitch_18
        0x7d -> :sswitch_18
    .end sparse-switch
.end method

.method private peekKeyword()I
    .registers 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 599
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    aget-char v2, v0, v1

    .line 603
    .local v2, "c":C
    const/16 v0, 0x74

    if-eq v2, v0, :cond_e

    const/16 v0, 0x54

    if-ne v2, v0, :cond_14

    .line 604
    :cond_e
    const-string v3, "true"

    .line 605
    .local v3, "keyword":Ljava/lang/String;
    const-string v4, "TRUE"

    .line 606
    .local v4, "keywordUpper":Ljava/lang/String;
    const/4 v5, 0x5

    .local v5, "peeking":I
    goto :goto_32

    .line 607
    .end local v3    # "keyword":Ljava/lang/String;
    .end local v4    # "keywordUpper":Ljava/lang/String;
    .end local v5    # "peeking":I
    :cond_14
    const/16 v0, 0x66

    if-eq v2, v0, :cond_1c

    const/16 v0, 0x46

    if-ne v2, v0, :cond_22

    .line 608
    :cond_1c
    const-string v3, "false"

    .line 609
    .local v3, "keyword":Ljava/lang/String;
    const-string v4, "FALSE"

    .line 610
    .local v4, "keywordUpper":Ljava/lang/String;
    const/4 v5, 0x6

    .local v5, "peeking":I
    goto :goto_32

    .line 611
    .end local v3    # "keyword":Ljava/lang/String;
    .end local v4    # "keywordUpper":Ljava/lang/String;
    .end local v5    # "peeking":I
    :cond_22
    const/16 v0, 0x6e

    if-eq v2, v0, :cond_2a

    const/16 v0, 0x4e

    if-ne v2, v0, :cond_30

    .line 612
    :cond_2a
    const-string v3, "null"

    .line 613
    .local v3, "keyword":Ljava/lang/String;
    const-string v4, "NULL"

    .line 614
    .local v4, "keywordUpper":Ljava/lang/String;
    const/4 v5, 0x7

    .local v5, "peeking":I
    goto :goto_32

    .line 616
    .end local v3    # "keyword":Ljava/lang/String;
    .end local v4    # "keywordUpper":Ljava/lang/String;
    .end local v5    # "peeking":I
    :cond_30
    const/4 v0, 0x0

    return v0

    .line 620
    .local v3, "keyword":Ljava/lang/String;
    .local v4, "keywordUpper":Ljava/lang/String;
    .local v5, "peeking":I
    :goto_32
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    .line 621
    .local v6, "length":I
    const/4 v7, 0x1

    .local v7, "i":I
    :goto_37
    if-ge v7, v6, :cond_62

    .line 622
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v0, v7

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    if-lt v0, v1, :cond_4a

    add-int/lit8 v0, v7, 0x1

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-nez v0, :cond_4a

    .line 623
    const/4 v0, 0x0

    return v0

    .line 625
    :cond_4a
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v1, v7

    aget-char v2, v0, v1

    .line 626
    invoke-virtual {v3, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-eq v2, v0, :cond_5f

    invoke-virtual {v4, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-eq v2, v0, :cond_5f

    .line 627
    const/4 v0, 0x0

    return v0

    .line 621
    :cond_5f
    add-int/lit8 v7, v7, 0x1

    goto :goto_37

    .line 631
    .end local v7    # "i":I
    :cond_62
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v0, v6

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    if-lt v0, v1, :cond_71

    add-int/lit8 v0, v6, 0x1

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-eqz v0, :cond_80

    :cond_71
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v1, v6

    aget-char v0, v0, v1

    .line 632
    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->isLiteral(C)Z

    move-result v0

    if-eqz v0, :cond_80

    .line 633
    const/4 v0, 0x0

    return v0

    .line 637
    :cond_80
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v0, v6

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 638
    iput v5, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    return v5
.end method

.method private peekNumber()I
    .registers 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 643
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    .line 644
    .local v4, "buffer":[C
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 645
    .local v5, "p":I
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/gson/stream/JsonReader;->limit:I

    .line 647
    .local v6, "l":I
    const-wide/16 v7, 0x0

    .line 648
    .local v7, "value":J
    const/4 v9, 0x0

    .line 649
    .local v9, "negative":Z
    const/4 v10, 0x1

    .line 650
    .local v10, "fitsInLong":Z
    const/4 v11, 0x0

    .line 652
    .local v11, "last":I
    const/4 v12, 0x0

    .line 656
    .local v12, "i":I
    :goto_12
    add-int v0, v5, v12

    if-ne v0, v6, :cond_2f

    .line 657
    array-length v0, v4

    if-ne v12, v0, :cond_1b

    .line 660
    const/4 v0, 0x0

    return v0

    .line 662
    :cond_1b
    add-int/lit8 v0, v12, 0x1

    move-object/from16 v1, p0

    invoke-direct {v1, v0}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-nez v0, :cond_27

    .line 663
    goto/16 :goto_bf

    .line 665
    :cond_27
    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 666
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/gson/stream/JsonReader;->limit:I

    .line 669
    :cond_2f
    add-int v0, v5, v12

    aget-char v13, v4, v0

    .line 670
    .local v13, "c":C
    sparse-switch v13, :sswitch_data_100

    goto/16 :goto_61

    .line 672
    :sswitch_38
    if-nez v11, :cond_3e

    .line 673
    const/4 v9, 0x1

    .line 674
    const/4 v11, 0x1

    .line 675
    goto/16 :goto_bb

    .line 676
    :cond_3e
    const/4 v0, 0x5

    if-ne v11, v0, :cond_44

    .line 677
    const/4 v11, 0x6

    .line 678
    goto/16 :goto_bb

    .line 680
    :cond_44
    const/4 v0, 0x0

    return v0

    .line 683
    :sswitch_46
    const/4 v0, 0x5

    if-ne v11, v0, :cond_4c

    .line 684
    const/4 v11, 0x6

    .line 685
    goto/16 :goto_bb

    .line 687
    :cond_4c
    const/4 v0, 0x0

    return v0

    .line 691
    :sswitch_4e
    const/4 v0, 0x2

    if-eq v11, v0, :cond_54

    const/4 v0, 0x4

    if-ne v11, v0, :cond_57

    .line 692
    :cond_54
    const/4 v11, 0x5

    .line 693
    goto/16 :goto_bb

    .line 695
    :cond_57
    const/4 v0, 0x0

    return v0

    .line 698
    :sswitch_59
    const/4 v0, 0x2

    if-ne v11, v0, :cond_5f

    .line 699
    const/4 v11, 0x3

    .line 700
    goto/16 :goto_bb

    .line 702
    :cond_5f
    const/4 v0, 0x0

    return v0

    .line 705
    :goto_61
    const/16 v0, 0x30

    if-lt v13, v0, :cond_69

    const/16 v0, 0x39

    if-le v13, v0, :cond_75

    .line 706
    :cond_69
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/google/gson/stream/JsonReader;->isLiteral(C)Z

    move-result v0

    if-nez v0, :cond_73

    .line 707
    goto/16 :goto_bf

    .line 709
    :cond_73
    const/4 v0, 0x0

    return v0

    .line 711
    :cond_75
    const/4 v0, 0x1

    if-eq v11, v0, :cond_7a

    if-nez v11, :cond_80

    .line 712
    :cond_7a
    add-int/lit8 v0, v13, -0x30

    neg-int v0, v0

    int-to-long v7, v0

    .line 713
    const/4 v11, 0x2

    goto :goto_bb

    .line 714
    :cond_80
    const/4 v0, 0x2

    if-ne v11, v0, :cond_af

    .line 715
    const-wide/16 v0, 0x0

    cmp-long v0, v7, v0

    if-nez v0, :cond_8b

    .line 716
    const/4 v0, 0x0

    return v0

    .line 718
    :cond_8b
    const-wide/16 v0, 0xa

    mul-long/2addr v0, v7

    add-int/lit8 v2, v13, -0x30

    int-to-long v2, v2

    sub-long v14, v0, v2

    .line 719
    .local v14, "newValue":J
    const-wide v0, -0xcccccccccccccccL

    cmp-long v0, v7, v0

    if-gtz v0, :cond_a9

    const-wide v0, -0xcccccccccccccccL

    cmp-long v0, v7, v0

    if-nez v0, :cond_ab

    cmp-long v0, v14, v7

    if-gez v0, :cond_ab

    :cond_a9
    const/4 v0, 0x1

    goto :goto_ac

    :cond_ab
    const/4 v0, 0x0

    :goto_ac
    and-int/2addr v10, v0

    .line 721
    move-wide v7, v14

    .line 722
    .end local v14    # "newValue":J
    goto :goto_bb

    :cond_af
    const/4 v0, 0x3

    if-ne v11, v0, :cond_b4

    .line 723
    const/4 v11, 0x4

    goto :goto_bb

    .line 724
    :cond_b4
    const/4 v0, 0x5

    if-eq v11, v0, :cond_ba

    const/4 v0, 0x6

    if-ne v11, v0, :cond_bb

    .line 725
    :cond_ba
    const/4 v11, 0x7

    .line 655
    .end local v13    # "c":C
    :cond_bb
    :goto_bb
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_12

    .line 731
    :goto_bf
    const/4 v0, 0x2

    if-ne v11, v0, :cond_e7

    if-eqz v10, :cond_e7

    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, v7, v0

    if-nez v0, :cond_cc

    if-eqz v9, :cond_e7

    .line 732
    :cond_cc
    if-eqz v9, :cond_d0

    move-wide v0, v7

    goto :goto_d1

    :cond_d0
    neg-long v0, v7

    :goto_d1
    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/gson/stream/JsonReader;->peekedLong:J

    .line 733
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v0, v12

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 734
    const/16 v0, 0xf

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/gson/stream/JsonReader;->peeked:I

    const/16 v0, 0xf

    return v0

    .line 735
    :cond_e7
    const/4 v0, 0x2

    if-eq v11, v0, :cond_f0

    const/4 v0, 0x4

    if-eq v11, v0, :cond_f0

    const/4 v0, 0x7

    if-ne v11, v0, :cond_fd

    .line 737
    :cond_f0
    move-object/from16 v0, p0

    iput v12, v0, Lcom/google/gson/stream/JsonReader;->peekedNumberLength:I

    .line 738
    const/16 v0, 0x10

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/gson/stream/JsonReader;->peeked:I

    const/16 v0, 0x10

    return v0

    .line 740
    :cond_fd
    const/4 v0, 0x0

    return v0

    nop

    :sswitch_data_100
    .sparse-switch
        0x2b -> :sswitch_46
        0x2d -> :sswitch_38
        0x2e -> :sswitch_59
        0x45 -> :sswitch_4e
        0x65 -> :sswitch_4e
    .end sparse-switch
.end method

.method private push(I)V
    .registers 9
    .param p1, "newTop"    # I

    .line 1256
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    iget-object v1, p0, Lcom/google/gson/stream/JsonReader;->stack:[I

    array-length v1, v1

    if-ne v0, v1, :cond_3a

    .line 1257
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    mul-int/lit8 v0, v0, 0x2

    new-array v4, v0, [I

    .line 1258
    .local v4, "newStack":[I
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    mul-int/lit8 v0, v0, 0x2

    new-array v5, v0, [I

    .line 1259
    .local v5, "newPathIndices":[I
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    mul-int/lit8 v0, v0, 0x2

    new-array v6, v0, [Ljava/lang/String;

    .line 1260
    .local v6, "newPathNames":[Ljava/lang/String;
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->stack:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v2, v4, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1261
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v2, v5, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1262
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathNames:[Ljava/lang/String;

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v2, v6, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1263
    iput-object v4, p0, Lcom/google/gson/stream/JsonReader;->stack:[I

    .line 1264
    iput-object v5, p0, Lcom/google/gson/stream/JsonReader;->pathIndices:[I

    .line 1265
    iput-object v6, p0, Lcom/google/gson/stream/JsonReader;->pathNames:[Ljava/lang/String;

    .line 1267
    .end local v4    # "newStack":[I
    .end local v5    # "newPathIndices":[I
    .end local v6    # "newPathNames":[Ljava/lang/String;
    :cond_3a
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->stack:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    aput p1, v0, v1

    .line 1268
    return-void
.end method

.method private readEscapeCharacter()C
    .registers 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1495
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    if-ne v0, v1, :cond_14

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-nez v0, :cond_14

    .line 1496
    const-string v0, "Unterminated escape sequence"

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1499
    :cond_14
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    aget-char v6, v0, v1

    .line 1500
    .local v6, "escaped":C
    sparse-switch v6, :sswitch_data_c2

    goto/16 :goto_ba

    .line 1502
    :sswitch_23
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, 0x4

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    if-le v0, v1, :cond_39

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-nez v0, :cond_39

    .line 1503
    const-string v0, "Unterminated escape sequence"

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 1506
    :cond_39
    const/4 v7, 0x0

    .line 1507
    .local v7, "result":C
    iget v8, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .local v8, "i":I
    add-int/lit8 v9, v8, 0x4

    .local v9, "end":I
    :goto_3e
    if-ge v8, v9, :cond_99

    .line 1508
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    aget-char v10, v0, v8

    .line 1509
    .local v10, "c":C
    shl-int/lit8 v0, v7, 0x4

    int-to-char v7, v0

    .line 1510
    const/16 v0, 0x30

    if-lt v10, v0, :cond_54

    const/16 v0, 0x39

    if-gt v10, v0, :cond_54

    .line 1511
    add-int/lit8 v0, v10, -0x30

    add-int/2addr v0, v7

    int-to-char v7, v0

    goto :goto_95

    .line 1512
    :cond_54
    const/16 v0, 0x61

    if-lt v10, v0, :cond_63

    const/16 v0, 0x66

    if-gt v10, v0, :cond_63

    .line 1513
    add-int/lit8 v0, v10, -0x61

    add-int/lit8 v0, v0, 0xa

    add-int/2addr v0, v7

    int-to-char v7, v0

    goto :goto_95

    .line 1514
    :cond_63
    const/16 v0, 0x41

    if-lt v10, v0, :cond_72

    const/16 v0, 0x46

    if-gt v10, v0, :cond_72

    .line 1515
    add-int/lit8 v0, v10, -0x41

    add-int/lit8 v0, v0, 0xa

    add-int/2addr v0, v7

    int-to-char v7, v0

    goto :goto_95

    .line 1517
    :cond_72
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\\u"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    const/4 v5, 0x4

    invoke-direct {v2, v3, v4, v5}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1507
    .end local v10    # "c":C
    :goto_95
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_3e

    .line 1520
    .end local v8    # "i":I
    .end local v9    # "end":I
    :cond_99
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1521
    return v7

    .line 1524
    .end local v7    # "result":C
    :sswitch_a0
    const/16 v0, 0x9

    return v0

    .line 1527
    :sswitch_a3
    const/16 v0, 0x8

    return v0

    .line 1530
    :sswitch_a6
    const/16 v0, 0xa

    return v0

    .line 1533
    :sswitch_a9
    const/16 v0, 0xd

    return v0

    .line 1536
    :sswitch_ac
    const/16 v0, 0xc

    return v0

    .line 1539
    :sswitch_af
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I

    .line 1540
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I

    .line 1547
    :sswitch_b9
    return v6

    .line 1550
    :goto_ba
    const-string v0, "Invalid escape sequence"

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    nop

    :sswitch_data_c2
    .sparse-switch
        0xa -> :sswitch_af
        0x22 -> :sswitch_b9
        0x27 -> :sswitch_b9
        0x2f -> :sswitch_b9
        0x5c -> :sswitch_b9
        0x62 -> :sswitch_a3
        0x66 -> :sswitch_ac
        0x6e -> :sswitch_a6
        0x72 -> :sswitch_a9
        0x74 -> :sswitch_a0
        0x75 -> :sswitch_23
    .end sparse-switch
.end method

.method private skipQuotedValue(C)V
    .registers 8
    .param p1, "quote"    # C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1089
    iget-object v2, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    .line 1091
    .local v2, "buffer":[C
    :cond_2
    iget v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1092
    .local v3, "p":I
    iget v4, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    .line 1094
    .local v4, "l":I
    :goto_6
    if-ge v3, v4, :cond_2d

    .line 1095
    move v0, v3

    add-int/lit8 v3, v3, 0x1

    aget-char v5, v2, v0

    .line 1096
    .local v5, "c":I
    if-ne v5, p1, :cond_12

    .line 1097
    iput v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1098
    return-void

    .line 1099
    :cond_12
    const/16 v0, 0x5c

    if-ne v5, v0, :cond_20

    .line 1100
    iput v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1101
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->readEscapeCharacter()C

    .line 1102
    iget v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1103
    iget v4, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    goto :goto_2c

    .line 1104
    :cond_20
    const/16 v0, 0xa

    if-ne v5, v0, :cond_2c

    .line 1105
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I

    .line 1106
    iput v3, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I

    .line 1108
    .end local v5    # "c":I
    :cond_2c
    :goto_2c
    goto :goto_6

    .line 1109
    :cond_2d
    iput v3, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1110
    .end local v3    # "p":I
    .end local v4    # "l":I
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1111
    const-string v0, "Unterminated string"

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method private skipTo(Ljava/lang/String;)Z
    .registers 5
    .param p1, "toFind"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1428
    :goto_0
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    if-le v0, v1, :cond_15

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-eqz v0, :cond_4d

    .line 1429
    :cond_15
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    aget-char v0, v0, v1

    const/16 v1, 0xa

    if-ne v0, v1, :cond_2c

    .line 1430
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I

    .line 1431
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I

    .line 1432
    goto :goto_46

    .line 1434
    :cond_2c
    const/4 v2, 0x0

    .local v2, "c":I
    :goto_2d
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v2, v0, :cond_44

    .line 1435
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v1, v2

    aget-char v0, v0, v1

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-eq v0, v1, :cond_41

    .line 1436
    goto :goto_46

    .line 1434
    :cond_41
    add-int/lit8 v2, v2, 0x1

    goto :goto_2d

    .line 1439
    .end local v2    # "c":I
    :cond_44
    const/4 v0, 0x1

    return v0

    .line 1428
    :goto_46
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    goto :goto_0

    .line 1441
    :cond_4d
    const/4 v0, 0x0

    return v0
.end method

.method private skipToEndOfLine()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1411
    :goto_0
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    if-lt v0, v1, :cond_d

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 1412
    :cond_d
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    aget-char v3, v0, v1

    .line 1413
    .local v3, "c":C
    const/16 v0, 0xa

    if-ne v3, v0, :cond_26

    .line 1414
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->lineNumber:I

    .line 1415
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->lineStart:I

    .line 1416
    goto :goto_2c

    .line 1417
    :cond_26
    const/16 v0, 0xd

    if-ne v3, v0, :cond_2b

    .line 1418
    goto :goto_2c

    .line 1420
    .end local v3    # "c":C
    :cond_2b
    goto :goto_0

    .line 1421
    :cond_2c
    :goto_2c
    return-void
.end method

.method private skipUnquotedValue()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1116
    :cond_0
    const/4 v2, 0x0

    .line 1117
    .local v2, "i":I
    :goto_1
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v0, v2

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    if-ge v0, v1, :cond_21

    .line 1118
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v1, v2

    aget-char v0, v0, v1

    sparse-switch v0, :sswitch_data_2e

    goto/16 :goto_1d

    .line 1124
    :sswitch_14
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V

    .line 1136
    :sswitch_17
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1137
    return-void

    .line 1117
    :goto_1d
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 1140
    :cond_21
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1141
    .end local v2    # "i":I
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1142
    return-void

    :sswitch_data_2e
    .sparse-switch
        0x9 -> :sswitch_17
        0xa -> :sswitch_17
        0xc -> :sswitch_17
        0xd -> :sswitch_17
        0x20 -> :sswitch_17
        0x23 -> :sswitch_14
        0x2c -> :sswitch_17
        0x2f -> :sswitch_14
        0x3a -> :sswitch_17
        0x3b -> :sswitch_14
        0x3d -> :sswitch_14
        0x5b -> :sswitch_17
        0x5c -> :sswitch_14
        0x5d -> :sswitch_17
        0x7b -> :sswitch_17
        0x7d -> :sswitch_17
    .end sparse-switch
.end method

.method private syntaxError(Ljava/lang/String;)Ljava/io/IOException;
    .registers 5
    .param p1, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1559
    new-instance v0, Lcom/google/gson/stream/MalformedJsonException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/gson/stream/MalformedJsonException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public beginArray()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 341
    iget v3, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 342
    .local v3, "p":I
    if-nez v3, :cond_8

    .line 343
    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I

    move-result v3

    .line 345
    :cond_8
    const/4 v0, 0x3

    if-ne v3, v0, :cond_1c

    .line 346
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->push(I)V

    .line 347
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    aput v2, v0, v1

    .line 348
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    goto :goto_41

    .line 350
    :cond_1c
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected BEGIN_ARRAY but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 352
    :goto_41
    return-void
.end method

.method public beginObject()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 377
    iget v3, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 378
    .local v3, "p":I
    if-nez v3, :cond_8

    .line 379
    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I

    move-result v3

    .line 381
    :cond_8
    const/4 v0, 0x1

    if-ne v3, v0, :cond_13

    .line 382
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->push(I)V

    .line 383
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    goto :goto_38

    .line 385
    :cond_13
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected BEGIN_OBJECT but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 387
    :goto_38
    return-void
.end method

.method public close()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1208
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 1209
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->stack:[I

    const/16 v1, 0x8

    const/4 v2, 0x0

    aput v1, v0, v2

    .line 1210
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    .line 1211
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->in:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->close()V

    .line 1212
    return-void
.end method

.method doPeek()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 462
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->stack:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v3, v0, v1

    .line 463
    .local v3, "peekStack":I
    const/4 v0, 0x1

    if-ne v3, v0, :cond_16

    .line 464
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->stack:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x2

    aput v2, v0, v1

    goto/16 :goto_121

    .line 465
    :cond_16
    const/4 v0, 0x2

    if-ne v3, v0, :cond_34

    .line 467
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->nextNonWhitespace(Z)I

    move-result v4

    .line 468
    .local v4, "c":I
    sparse-switch v4, :sswitch_data_19a

    goto :goto_2b

    .line 470
    :sswitch_22
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    const/4 v0, 0x4

    return v0

    .line 472
    :sswitch_27
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V

    .line 474
    :sswitch_2a
    goto :goto_32

    .line 476
    :goto_2b
    const-string v0, "Unterminated array"

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 478
    .end local v4    # "c":I
    :goto_32
    goto/16 :goto_121

    :cond_34
    const/4 v0, 0x3

    if-eq v3, v0, :cond_3a

    const/4 v0, 0x5

    if-ne v3, v0, :cond_a6

    .line 479
    :cond_3a
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->stack:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x4

    aput v2, v0, v1

    .line 481
    const/4 v0, 0x5

    if-ne v3, v0, :cond_5f

    .line 482
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->nextNonWhitespace(Z)I

    move-result v4

    .line 483
    .local v4, "c":I
    sparse-switch v4, :sswitch_data_1a8

    goto :goto_58

    .line 485
    :sswitch_4f
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    const/4 v0, 0x2

    return v0

    .line 487
    :sswitch_54
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V

    .line 489
    :sswitch_57
    goto :goto_5f

    .line 491
    :goto_58
    const-string v0, "Unterminated object"

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 494
    .end local v4    # "c":I
    :cond_5f
    :goto_5f
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->nextNonWhitespace(Z)I

    move-result v4

    .line 495
    .local v4, "c":I
    sparse-switch v4, :sswitch_data_1b6

    goto :goto_88

    .line 497
    :sswitch_68
    const/16 v0, 0xd

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    const/16 v0, 0xd

    return v0

    .line 499
    :sswitch_6f
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V

    .line 500
    const/16 v0, 0xc

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    const/16 v0, 0xc

    return v0

    .line 502
    :sswitch_79
    const/4 v0, 0x5

    if-eq v3, v0, :cond_81

    .line 503
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    const/4 v0, 0x2

    return v0

    .line 505
    :cond_81
    const-string v0, "Expected name"

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 508
    :goto_88
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V

    .line 509
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 510
    int-to-char v0, v4

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->isLiteral(C)Z

    move-result v0

    if-eqz v0, :cond_9f

    .line 511
    const/16 v0, 0xe

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    const/16 v0, 0xe

    return v0

    .line 513
    :cond_9f
    const-string v0, "Expected name"

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 516
    .end local v4    # "c":I
    :cond_a6
    const/4 v0, 0x4

    if-ne v3, v0, :cond_e5

    .line 517
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->stack:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x5

    aput v2, v0, v1

    .line 519
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->nextNonWhitespace(Z)I

    move-result v4

    .line 520
    .local v4, "c":I
    sparse-switch v4, :sswitch_data_1c4

    goto :goto_dd

    .line 522
    :sswitch_bb
    goto :goto_e4

    .line 524
    :sswitch_bc
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V

    .line 525
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->limit:I

    if-lt v0, v1, :cond_cc

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->fillBuffer(I)Z

    move-result v0

    if-eqz v0, :cond_e4

    :cond_cc
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    aget-char v0, v0, v1

    const/16 v1, 0x3e

    if-ne v0, v1, :cond_e4

    .line 526
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    goto :goto_e4

    .line 530
    :goto_dd
    const-string v0, "Expected \':\'"

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 532
    .end local v4    # "c":I
    :cond_e4
    :goto_e4
    goto :goto_121

    :cond_e5
    const/4 v0, 0x6

    if-ne v3, v0, :cond_f9

    .line 533
    iget-boolean v0, p0, Lcom/google/gson/stream/JsonReader;->lenient:Z

    if-eqz v0, :cond_ef

    .line 534
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->consumeNonExecutePrefix()V

    .line 536
    :cond_ef
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->stack:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x7

    aput v2, v0, v1

    goto :goto_121

    .line 537
    :cond_f9
    const/4 v0, 0x7

    if-ne v3, v0, :cond_115

    .line 538
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->nextNonWhitespace(Z)I

    move-result v4

    .line 539
    .local v4, "c":I
    const/4 v0, -0x1

    if-ne v4, v0, :cond_10b

    .line 540
    const/16 v0, 0x11

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    const/16 v0, 0x11

    return v0

    .line 542
    :cond_10b
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V

    .line 543
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 545
    .end local v4    # "c":I
    goto :goto_121

    :cond_115
    const/16 v0, 0x8

    if-ne v3, v0, :cond_121

    .line 546
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "JsonReader is closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 549
    :cond_121
    :goto_121
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->nextNonWhitespace(Z)I

    move-result v4

    .line 550
    .local v4, "c":I
    sparse-switch v4, :sswitch_data_1ce

    goto/16 :goto_169

    .line 552
    :sswitch_12b
    const/4 v0, 0x1

    if-ne v3, v0, :cond_133

    .line 553
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    const/4 v0, 0x4

    return v0

    .line 559
    :cond_133
    :sswitch_133
    const/4 v0, 0x1

    if-eq v3, v0, :cond_139

    const/4 v0, 0x2

    if-ne v3, v0, :cond_147

    .line 560
    :cond_139
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V

    .line 561
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 562
    const/4 v0, 0x7

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    const/4 v0, 0x7

    return v0

    .line 564
    :cond_147
    const-string v0, "Unexpected value"

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 567
    :sswitch_14e
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V

    .line 568
    const/16 v0, 0x8

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    const/16 v0, 0x8

    return v0

    .line 570
    :sswitch_158
    const/16 v0, 0x9

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    const/16 v0, 0x9

    return v0

    .line 572
    :sswitch_15f
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    const/4 v0, 0x3

    return v0

    .line 574
    :sswitch_164
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    const/4 v0, 0x1

    return v0

    .line 576
    :goto_169
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 579
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->peekKeyword()I

    move-result v5

    .line 580
    .local v5, "result":I
    if-eqz v5, :cond_176

    .line 581
    return v5

    .line 584
    :cond_176
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->peekNumber()I

    move-result v5

    .line 585
    if-eqz v5, :cond_17d

    .line 586
    return v5

    .line 589
    :cond_17d
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    aget-char v0, v0, v1

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->isLiteral(C)Z

    move-result v0

    if-nez v0, :cond_190

    .line 590
    const-string v0, "Expected value"

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    .line 593
    :cond_190
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->checkLenient()V

    .line 594
    const/16 v0, 0xa

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    const/16 v0, 0xa

    return v0

    :sswitch_data_19a
    .sparse-switch
        0x2c -> :sswitch_2a
        0x3b -> :sswitch_27
        0x5d -> :sswitch_22
    .end sparse-switch

    :sswitch_data_1a8
    .sparse-switch
        0x2c -> :sswitch_57
        0x3b -> :sswitch_54
        0x7d -> :sswitch_4f
    .end sparse-switch

    :sswitch_data_1b6
    .sparse-switch
        0x22 -> :sswitch_68
        0x27 -> :sswitch_6f
        0x7d -> :sswitch_79
    .end sparse-switch

    :sswitch_data_1c4
    .sparse-switch
        0x3a -> :sswitch_bb
        0x3d -> :sswitch_bc
    .end sparse-switch

    :sswitch_data_1ce
    .sparse-switch
        0x22 -> :sswitch_158
        0x27 -> :sswitch_14e
        0x2c -> :sswitch_133
        0x3b -> :sswitch_133
        0x5b -> :sswitch_15f
        0x5d -> :sswitch_12b
        0x7b -> :sswitch_164
    .end sparse-switch
.end method

.method public endArray()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 359
    iget v4, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 360
    .local v4, "p":I
    if-nez v4, :cond_8

    .line 361
    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I

    move-result v4

    .line 363
    :cond_8
    const/4 v0, 0x4

    if-ne v4, v0, :cond_21

    .line 364
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    .line 365
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 366
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    goto :goto_46

    .line 368
    :cond_21
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected END_ARRAY but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 370
    :goto_46
    return-void
.end method

.method public endObject()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 394
    iget v4, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 395
    .local v4, "p":I
    if-nez v4, :cond_8

    .line 396
    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I

    move-result v4

    .line 398
    :cond_8
    const/4 v0, 0x2

    if-ne v4, v0, :cond_28

    .line 399
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    .line 400
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathNames:[Ljava/lang/String;

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 401
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 402
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    goto :goto_4d

    .line 404
    :cond_28
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected END_OBJECT but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 406
    :goto_4d
    return-void
.end method

.method public getPath()Ljava/lang/String;
    .registers 6

    .line 1459
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1460
    .local v2, "result":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .local v3, "i":I
    iget v4, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    .local v4, "size":I
    :goto_e
    if-ge v3, v4, :cond_41

    .line 1461
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->stack:[I

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_46

    goto :goto_3e

    .line 1464
    :pswitch_18
    const/16 v0, 0x5b

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/gson/stream/JsonReader;->pathIndices:[I

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1465
    goto :goto_3e

    .line 1470
    :pswitch_2c
    const/16 v0, 0x2e

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1471
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathNames:[Ljava/lang/String;

    aget-object v0, v0, v3

    if-eqz v0, :cond_3e

    .line 1472
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathNames:[Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1460
    :cond_3e
    :goto_3e
    :pswitch_3e
    add-int/lit8 v3, v3, 0x1

    goto :goto_e

    .line 1482
    .end local v3    # "i":I
    .end local v4    # "size":I
    :cond_41
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_data_46
    .packed-switch 0x1
        :pswitch_18
        :pswitch_18
        :pswitch_2c
        :pswitch_2c
        :pswitch_2c
        :pswitch_3e
        :pswitch_3e
        :pswitch_3e
    .end packed-switch
.end method

.method public hasNext()Z
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 412
    iget v1, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 413
    .local v1, "p":I
    if-nez v1, :cond_8

    .line 414
    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I

    move-result v1

    .line 416
    :cond_8
    const/4 v0, 0x2

    if-eq v1, v0, :cond_10

    const/4 v0, 0x4

    if-eq v1, v0, :cond_10

    const/4 v0, 0x1

    goto :goto_11

    :cond_10
    const/4 v0, 0x0

    :goto_11
    return v0
.end method

.method public final isLenient()Z
    .registers 2

    .line 333
    iget-boolean v0, p0, Lcom/google/gson/stream/JsonReader;->lenient:Z

    return v0
.end method

.method public nextBoolean()Z
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 840
    iget v4, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 841
    .local v4, "p":I
    if-nez v4, :cond_8

    .line 842
    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I

    move-result v4

    .line 844
    :cond_8
    const/4 v0, 0x5

    if-ne v4, v0, :cond_1c

    .line 845
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 846
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 847
    const/4 v0, 0x1

    return v0

    .line 848
    :cond_1c
    const/4 v0, 0x6

    if-ne v4, v0, :cond_30

    .line 849
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 850
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 851
    const/4 v0, 0x0

    return v0

    .line 853
    :cond_30
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a boolean but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public nextDouble()D
    .registers 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 886
    iget v4, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 887
    .local v4, "p":I
    if-nez v4, :cond_8

    .line 888
    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I

    move-result v4

    .line 891
    :cond_8
    const/16 v0, 0xf

    if-ne v4, v0, :cond_1f

    .line 892
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 893
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 894
    iget-wide v0, p0, Lcom/google/gson/stream/JsonReader;->peekedLong:J

    long-to-double v0, v0

    return-wide v0

    .line 897
    :cond_1f
    const/16 v0, 0x10

    if-ne v4, v0, :cond_38

    .line 898
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    iget v3, p0, Lcom/google/gson/stream/JsonReader;->peekedNumberLength:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, Lcom/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    .line 899
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->peekedNumberLength:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    goto :goto_84

    .line 900
    :cond_38
    const/16 v0, 0x8

    if-eq v4, v0, :cond_40

    const/16 v0, 0x9

    if-ne v4, v0, :cond_50

    .line 901
    :cond_40
    const/16 v0, 0x8

    if-ne v4, v0, :cond_47

    const/16 v0, 0x27

    goto :goto_49

    :cond_47
    const/16 v0, 0x22

    :goto_49
    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->nextQuotedValue(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    goto :goto_84

    .line 902
    :cond_50
    const/16 v0, 0xa

    if-ne v4, v0, :cond_5b

    .line 903
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->nextUnquotedValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    goto :goto_84

    .line 904
    :cond_5b
    const/16 v0, 0xb

    if-eq v4, v0, :cond_84

    .line 905
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a double but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 908
    :cond_84
    :goto_84
    const/16 v0, 0xb

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 909
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v5

    .line 910
    .local v5, "result":D
    iget-boolean v0, p0, Lcom/google/gson/stream/JsonReader;->lenient:Z

    if-nez v0, :cond_bf

    invoke-static {v5, v6}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_9e

    invoke-static {v5, v6}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-eqz v0, :cond_bf

    .line 911
    :cond_9e
    new-instance v0, Lcom/google/gson/stream/MalformedJsonException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSON forbids NaN and infinities: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 912
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/gson/stream/MalformedJsonException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 914
    :cond_bf
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    .line 915
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 916
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 917
    return-wide v5
.end method

.method public nextInt()I
    .registers 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1155
    iget v4, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 1156
    .local v4, "p":I
    if-nez v4, :cond_8

    .line 1157
    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I

    move-result v4

    .line 1161
    :cond_8
    const/16 v0, 0xf

    if-ne v4, v0, :cond_49

    .line 1162
    iget-wide v0, p0, Lcom/google/gson/stream/JsonReader;->peekedLong:J

    long-to-int v5, v0

    .line 1163
    .local v5, "result":I
    iget-wide v0, p0, Lcom/google/gson/stream/JsonReader;->peekedLong:J

    int-to-long v2, v5

    cmp-long v0, v0, v2

    if-eqz v0, :cond_39

    .line 1164
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected an int but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/gson/stream/JsonReader;->peekedLong:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1166
    :cond_39
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 1167
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 1168
    return v5

    .line 1171
    .end local v5    # "result":I
    :cond_49
    const/16 v0, 0x10

    if-ne v4, v0, :cond_63

    .line 1172
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    iget v3, p0, Lcom/google/gson/stream/JsonReader;->peekedNumberLength:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, Lcom/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    .line 1173
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->peekedNumberLength:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    goto/16 :goto_c6

    .line 1174
    :cond_63
    const/16 v0, 0x8

    if-eq v4, v0, :cond_6f

    const/16 v0, 0x9

    if-eq v4, v0, :cond_6f

    const/16 v0, 0xa

    if-ne v4, v0, :cond_a1

    .line 1175
    :cond_6f
    const/16 v0, 0xa

    if-ne v4, v0, :cond_7a

    .line 1176
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->nextUnquotedValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    goto :goto_89

    .line 1178
    :cond_7a
    const/16 v0, 0x8

    if-ne v4, v0, :cond_81

    const/16 v0, 0x27

    goto :goto_83

    :cond_81
    const/16 v0, 0x22

    :goto_83
    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->nextQuotedValue(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    .line 1181
    :goto_89
    :try_start_89
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 1182
    .local v5, "result":I
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 1183
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1
    :try_end_9e
    .catch Ljava/lang/NumberFormatException; {:try_start_89 .. :try_end_9e} :catch_9f

    .line 1184
    return v5

    .line 1185
    .end local v5    # "result":I
    :catch_9f
    move-exception v6

    .line 1187
    goto :goto_c6

    .line 1189
    :cond_a1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected an int but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1192
    :goto_c6
    const/16 v0, 0xb

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 1193
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v6

    .line 1194
    .local v6, "asDouble":D
    double-to-int v5, v6

    .line 1195
    .local v5, "result":I
    int-to-double v0, v5

    cmpl-double v0, v0, v6

    if-eqz v0, :cond_f9

    .line 1196
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected an int but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1198
    :cond_f9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    .line 1199
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 1200
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 1201
    return v5
.end method

.method public nextLong()J
    .registers 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 931
    iget v4, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 932
    .local v4, "p":I
    if-nez v4, :cond_8

    .line 933
    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I

    move-result v4

    .line 936
    :cond_8
    const/16 v0, 0xf

    if-ne v4, v0, :cond_1e

    .line 937
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 938
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 939
    iget-wide v0, p0, Lcom/google/gson/stream/JsonReader;->peekedLong:J

    return-wide v0

    .line 942
    :cond_1e
    const/16 v0, 0x10

    if-ne v4, v0, :cond_38

    .line 943
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    iget v3, p0, Lcom/google/gson/stream/JsonReader;->peekedNumberLength:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, Lcom/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    .line 944
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->peekedNumberLength:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    goto/16 :goto_9b

    .line 945
    :cond_38
    const/16 v0, 0x8

    if-eq v4, v0, :cond_44

    const/16 v0, 0x9

    if-eq v4, v0, :cond_44

    const/16 v0, 0xa

    if-ne v4, v0, :cond_76

    .line 946
    :cond_44
    const/16 v0, 0xa

    if-ne v4, v0, :cond_4f

    .line 947
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->nextUnquotedValue()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    goto :goto_5e

    .line 949
    :cond_4f
    const/16 v0, 0x8

    if-ne v4, v0, :cond_56

    const/16 v0, 0x27

    goto :goto_58

    :cond_56
    const/16 v0, 0x22

    :goto_58
    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->nextQuotedValue(C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    .line 952
    :goto_5e
    :try_start_5e
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 953
    .local v5, "result":J
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 954
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1
    :try_end_73
    .catch Ljava/lang/NumberFormatException; {:try_start_5e .. :try_end_73} :catch_74

    .line 955
    return-wide v5

    .line 956
    .end local v5    # "result":J
    :catch_74
    move-exception v5

    .line 958
    goto :goto_9b

    .line 960
    :cond_76
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a long but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 963
    :goto_9b
    const/16 v0, 0xb

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 964
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v5

    .line 965
    .local v5, "asDouble":D
    double-to-long v7, v5

    .line 966
    .local v7, "result":J
    long-to-double v0, v7

    cmpl-double v0, v0, v5

    if-eqz v0, :cond_ce

    .line 967
    new-instance v0, Ljava/lang/NumberFormatException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a long but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 969
    :cond_ce
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    .line 970
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 971
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 972
    return-wide v7
.end method

.method public nextName()Ljava/lang/String;
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 777
    iget v3, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 778
    .local v3, "p":I
    if-nez v3, :cond_8

    .line 779
    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I

    move-result v3

    .line 782
    :cond_8
    const/16 v0, 0xe

    if-ne v3, v0, :cond_11

    .line 783
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->nextUnquotedValue()Ljava/lang/String;

    move-result-object v4

    .local v4, "result":Ljava/lang/String;
    goto :goto_4c

    .line 784
    .end local v4    # "result":Ljava/lang/String;
    :cond_11
    const/16 v0, 0xc

    if-ne v3, v0, :cond_1c

    .line 785
    const/16 v0, 0x27

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->nextQuotedValue(C)Ljava/lang/String;

    move-result-object v4

    .local v4, "result":Ljava/lang/String;
    goto :goto_4c

    .line 786
    .end local v4    # "result":Ljava/lang/String;
    :cond_1c
    const/16 v0, 0xd

    if-ne v3, v0, :cond_27

    .line 787
    const/16 v0, 0x22

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->nextQuotedValue(C)Ljava/lang/String;

    move-result-object v4

    .local v4, "result":Ljava/lang/String;
    goto :goto_4c

    .line 789
    .end local v4    # "result":Ljava/lang/String;
    :cond_27
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a name but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 791
    .local v4, "result":Ljava/lang/String;
    :goto_4c
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 792
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathNames:[Ljava/lang/String;

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aput-object v4, v0, v1

    .line 793
    return-object v4
.end method

.method public nextNull()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 864
    iget v4, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 865
    .local v4, "p":I
    if-nez v4, :cond_8

    .line 866
    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I

    move-result v4

    .line 868
    :cond_8
    const/4 v0, 0x7

    if-ne v4, v0, :cond_1b

    .line 869
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 870
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    goto :goto_40

    .line 872
    :cond_1b
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected null but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 874
    :goto_40
    return-void
.end method

.method public nextString()Ljava/lang/String;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 805
    iget v4, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 806
    .local v4, "p":I
    if-nez v4, :cond_8

    .line 807
    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I

    move-result v4

    .line 810
    :cond_8
    const/16 v0, 0xa

    if-ne v4, v0, :cond_12

    .line 811
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->nextUnquotedValue()Ljava/lang/String;

    move-result-object v5

    .local v5, "result":Ljava/lang/String;
    goto/16 :goto_7a

    .line 812
    .end local v5    # "result":Ljava/lang/String;
    :cond_12
    const/16 v0, 0x8

    if-ne v4, v0, :cond_1e

    .line 813
    const/16 v0, 0x27

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->nextQuotedValue(C)Ljava/lang/String;

    move-result-object v5

    .local v5, "result":Ljava/lang/String;
    goto/16 :goto_7a

    .line 814
    .end local v5    # "result":Ljava/lang/String;
    :cond_1e
    const/16 v0, 0x9

    if-ne v4, v0, :cond_29

    .line 815
    const/16 v0, 0x22

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->nextQuotedValue(C)Ljava/lang/String;

    move-result-object v5

    .local v5, "result":Ljava/lang/String;
    goto :goto_7a

    .line 816
    .end local v5    # "result":Ljava/lang/String;
    :cond_29
    const/16 v0, 0xb

    if-ne v4, v0, :cond_33

    .line 817
    iget-object v5, p0, Lcom/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    .line 818
    .local v5, "result":Ljava/lang/String;
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/gson/stream/JsonReader;->peekedString:Ljava/lang/String;

    goto :goto_7a

    .line 819
    .end local v5    # "result":Ljava/lang/String;
    :cond_33
    const/16 v0, 0xf

    if-ne v4, v0, :cond_3e

    .line 820
    iget-wide v0, p0, Lcom/google/gson/stream/JsonReader;->peekedLong:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    .local v5, "result":Ljava/lang/String;
    goto :goto_7a

    .line 821
    .end local v5    # "result":Ljava/lang/String;
    :cond_3e
    const/16 v0, 0x10

    if-ne v4, v0, :cond_55

    .line 822
    new-instance v5, Ljava/lang/String;

    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->buffer:[C

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    iget v2, p0, Lcom/google/gson/stream/JsonReader;->peekedNumberLength:I

    invoke-direct {v5, v0, v1, v2}, Ljava/lang/String;-><init>([CII)V

    .line 823
    .local v5, "result":Ljava/lang/String;
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->peekedNumberLength:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    goto :goto_7a

    .line 825
    .end local v5    # "result":Ljava/lang/String;
    :cond_55
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a string but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->peek()Lcom/google/gson/stream/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 827
    .local v5, "result":Ljava/lang/String;
    :goto_7a
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 828
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 829
    return-object v5
.end method

.method public peek()Lcom/google/gson/stream/JsonToken;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 423
    iget v1, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 424
    .local v1, "p":I
    if-nez v1, :cond_8

    .line 425
    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I

    move-result v1

    .line 428
    :cond_8
    packed-switch v1, :pswitch_data_30

    goto :goto_2a

    .line 430
    :pswitch_c
    sget-object v0, Lcom/google/gson/stream/JsonToken;->BEGIN_OBJECT:Lcom/google/gson/stream/JsonToken;

    return-object v0

    .line 432
    :pswitch_f
    sget-object v0, Lcom/google/gson/stream/JsonToken;->END_OBJECT:Lcom/google/gson/stream/JsonToken;

    return-object v0

    .line 434
    :pswitch_12
    sget-object v0, Lcom/google/gson/stream/JsonToken;->BEGIN_ARRAY:Lcom/google/gson/stream/JsonToken;

    return-object v0

    .line 436
    :pswitch_15
    sget-object v0, Lcom/google/gson/stream/JsonToken;->END_ARRAY:Lcom/google/gson/stream/JsonToken;

    return-object v0

    .line 440
    :pswitch_18
    sget-object v0, Lcom/google/gson/stream/JsonToken;->NAME:Lcom/google/gson/stream/JsonToken;

    return-object v0

    .line 443
    :pswitch_1b
    sget-object v0, Lcom/google/gson/stream/JsonToken;->BOOLEAN:Lcom/google/gson/stream/JsonToken;

    return-object v0

    .line 445
    :pswitch_1e
    sget-object v0, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    return-object v0

    .line 450
    :pswitch_21
    sget-object v0, Lcom/google/gson/stream/JsonToken;->STRING:Lcom/google/gson/stream/JsonToken;

    return-object v0

    .line 453
    :pswitch_24
    sget-object v0, Lcom/google/gson/stream/JsonToken;->NUMBER:Lcom/google/gson/stream/JsonToken;

    return-object v0

    .line 455
    :pswitch_27
    sget-object v0, Lcom/google/gson/stream/JsonToken;->END_DOCUMENT:Lcom/google/gson/stream/JsonToken;

    return-object v0

    .line 457
    :goto_2a
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_data_30
    .packed-switch 0x1
        :pswitch_c
        :pswitch_f
        :pswitch_12
        :pswitch_15
        :pswitch_1b
        :pswitch_1b
        :pswitch_1e
        :pswitch_21
        :pswitch_21
        :pswitch_21
        :pswitch_21
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_24
        :pswitch_24
        :pswitch_27
    .end packed-switch
.end method

.method public final setLenient(Z)V
    .registers 2
    .param p1, "lenient"    # Z

    .line 326
    iput-boolean p1, p0, Lcom/google/gson/stream/JsonReader;->lenient:Z

    .line 327
    return-void
.end method

.method public skipValue()V
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1220
    const/4 v4, 0x0

    .line 1222
    .local v4, "count":I
    :cond_1
    iget v5, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 1223
    .local v5, "p":I
    if-nez v5, :cond_9

    .line 1224
    invoke-virtual {p0}, Lcom/google/gson/stream/JsonReader;->doPeek()I

    move-result v5

    .line 1227
    :cond_9
    const/4 v0, 0x3

    if-ne v5, v0, :cond_14

    .line 1228
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->push(I)V

    .line 1229
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_69

    .line 1230
    :cond_14
    const/4 v0, 0x1

    if-ne v5, v0, :cond_1e

    .line 1231
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->push(I)V

    .line 1232
    add-int/lit8 v4, v4, 0x1

    goto :goto_69

    .line 1233
    :cond_1e
    const/4 v0, 0x4

    if-ne v5, v0, :cond_2a

    .line 1234
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    .line 1235
    add-int/lit8 v4, v4, -0x1

    goto :goto_69

    .line 1236
    :cond_2a
    const/4 v0, 0x2

    if-ne v5, v0, :cond_36

    .line 1237
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    .line 1238
    add-int/lit8 v4, v4, -0x1

    goto :goto_69

    .line 1239
    :cond_36
    const/16 v0, 0xe

    if-eq v5, v0, :cond_3e

    const/16 v0, 0xa

    if-ne v5, v0, :cond_42

    .line 1240
    :cond_3e
    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->skipUnquotedValue()V

    goto :goto_69

    .line 1241
    :cond_42
    const/16 v0, 0x8

    if-eq v5, v0, :cond_4a

    const/16 v0, 0xc

    if-ne v5, v0, :cond_50

    .line 1242
    :cond_4a
    const/16 v0, 0x27

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->skipQuotedValue(C)V

    goto :goto_69

    .line 1243
    :cond_50
    const/16 v0, 0x9

    if-eq v5, v0, :cond_58

    const/16 v0, 0xd

    if-ne v5, v0, :cond_5e

    .line 1244
    :cond_58
    const/16 v0, 0x22

    invoke-direct {p0, v0}, Lcom/google/gson/stream/JsonReader;->skipQuotedValue(C)V

    goto :goto_69

    .line 1245
    :cond_5e
    const/16 v0, 0x10

    if-ne v5, v0, :cond_69

    .line 1246
    iget v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->peekedNumberLength:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->pos:I

    .line 1248
    :cond_69
    :goto_69
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/gson/stream/JsonReader;->peeked:I

    .line 1249
    .end local v5    # "p":I
    if-nez v4, :cond_1

    .line 1251
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathIndices:[I

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, v1

    .line 1252
    iget-object v0, p0, Lcom/google/gson/stream/JsonReader;->pathNames:[Ljava/lang/String;

    iget v1, p0, Lcom/google/gson/stream/JsonReader;->stackSize:I

    add-int/lit8 v1, v1, -0x1

    const-string v2, "null"

    aput-object v2, v0, v1

    .line 1253
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 1445
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/gson/stream/JsonReader;->locationString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

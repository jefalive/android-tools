.class public final Lcom/itau/empresas/feature/login/PreLoginInputImpl;
.super Ljava/lang/Object;
.source "PreLoginInputImpl.java"

# interfaces
.implements Lcom/itau/empresas/feature/login/PreLoginInput;
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;
.implements Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ListenerItemSelecionado;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/itau/empresas/feature/login/PreLoginCampoDocumentoListener$NotificaCampoDocumentoAlterado;
.implements Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;


# instance fields
.field private botaoComProgresso:Lcom/itau/empresas/ui/view/BotaoComProgresso;

.field private campoDocumentoListener:Lcom/itau/empresas/feature/login/PreLoginCampoDocumentoListener;

.field private campoListaOperador:Landroid/widget/TextView;

.field private final context:Landroid/content/Context;

.field private cpfWatcher:Landroid/text/TextWatcher;

.field private dialogOperadores:Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;

.field private elegibilidadeBotaoAcessar:Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;

.field private final inputs:Landroid/view/View;

.field private final listener:Lcom/itau/empresas/feature/login/PreLoginInput$PreLoginInputListener;

.field private modoAtual:I

.field private operadorSelecionado:Lcom/itau/empresas/api/model/OperadorVO;

.field private rgFiltroDocumentos:Landroid/widget/RadioGroup;

.field private rlMultiploOperador:Landroid/widget/RelativeLayout;

.field private textoCampoDocumento:Landroid/widget/EditText;

.field private textoLogin:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

.field private tipoDocumento:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/itau/empresas/feature/login/PreLoginInput$PreLoginInputListener;)V
    .registers 4
    .param p1, "inputs"    # Landroid/view/View;
    .param p2, "listener"    # Lcom/itau/empresas/feature/login/PreLoginInput$PreLoginInputListener;

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->modoAtual:I

    .line 65
    iput-object p1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->inputs:Landroid/view/View;

    .line 66
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->context:Landroid/content/Context;

    .line 67
    iput-object p2, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->listener:Lcom/itau/empresas/feature/login/PreLoginInput$PreLoginInputListener;

    .line 68
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->iniciaViews()V

    .line 69
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->iniciaElegibilidadeBotaoAcessar()V

    .line 70
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->iniciaListeners()V

    .line 71
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->ocultaCampoDocumento()V

    .line 72
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->ocultaMultiplosOperadores()V

    .line 73
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->ocultaTiposDeDocumentos()V

    .line 74
    return-void
.end method

.method private alternaExibicaoParaProgresso(Z)V
    .registers 3
    .param p1, "alternaParaProgresso"    # Z

    .line 344
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->botaoComProgresso:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->alternaExibicaoParaProgresso(Z)V

    .line 345
    return-void
.end method

.method private atualizaModoOperadorSeDigitarAgenciaConta()V
    .registers 2

    .line 137
    iget v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->modoAtual:I

    if-nez v0, :cond_12

    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoLogin:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->isValidoAgenciaConta()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 138
    const/4 v0, 0x1

    iput v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->modoAtual:I

    .line 139
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->trataElegibilidadeBotaoAcessar()V

    .line 141
    :cond_12
    return-void
.end method

.method private configuraCampoDocumentoPassaporte()V
    .registers 4

    .line 302
    const-string v0, "PASSAPORTE"

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->tipoDocumento:Ljava/lang/String;

    .line 303
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->context:Landroid/content/Context;

    const v1, 0x7f070359

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->setHintCampoDocumento(Ljava/lang/String;)V

    .line 304
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->cpfWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 305
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 306
    .local v2, "filter":Ljava/util/List;, "Ljava/util/List<Landroid/text/InputFilter;>;"
    new-instance v0, Lcom/itau/empresas/feature/validador/filter/AlphaNumericoFilter;

    invoke-direct {v0}, Lcom/itau/empresas/feature/validador/filter/AlphaNumericoFilter;-><init>()V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 307
    new-instance v0, Landroid/text/InputFilter$AllCaps;

    invoke-direct {v0}, Landroid/text/InputFilter$AllCaps;-><init>()V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 308
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Landroid/text/InputFilter;

    invoke-interface {v2, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/InputFilter;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 309
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    .line 310
    const/16 v1, 0x1001

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 311
    return-void
.end method

.method private configuraCampoDocumentoRNE()V
    .registers 4

    .line 290
    const-string v0, "RNE"

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->tipoDocumento:Ljava/lang/String;

    .line 291
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->context:Landroid/content/Context;

    const v1, 0x7f070351

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->setHintCampoDocumento(Ljava/lang/String;)V

    .line 292
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->cpfWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 293
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 294
    .local v2, "filter":Ljava/util/List;, "Ljava/util/List<Landroid/text/InputFilter;>;"
    new-instance v0, Lcom/itau/empresas/feature/validador/filter/AlphaNumericoFilter;

    invoke-direct {v0}, Lcom/itau/empresas/feature/validador/filter/AlphaNumericoFilter;-><init>()V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 295
    new-instance v0, Landroid/text/InputFilter$AllCaps;

    invoke-direct {v0}, Landroid/text/InputFilter$AllCaps;-><init>()V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 296
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Landroid/text/InputFilter;

    invoke-interface {v2, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/InputFilter;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 297
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    .line 298
    const/16 v1, 0x1001

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 299
    return-void
.end method

.method private configuraCampoParaCPF()V
    .registers 4

    .line 280
    const-string v0, "CPF"

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->tipoDocumento:Ljava/lang/String;

    .line 281
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->context:Landroid/content/Context;

    const v1, 0x7f070350

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->setHintCampoDocumento(Ljava/lang/String;)V

    .line 282
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->cpfWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 283
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 284
    .local v2, "filter":Ljava/util/List;, "Ljava/util/List<Landroid/text/InputFilter;>;"
    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    const/16 v1, 0xe

    invoke-direct {v0, v1}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 285
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Landroid/text/InputFilter;

    invoke-interface {v2, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/text/InputFilter;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 286
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 287
    return-void
.end method

.method private disparaEventoAnalytcs(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "tela"    # Ljava/lang/String;
    .param p2, "secao"    # Ljava/lang/String;
    .param p3, "estado"    # Ljava/lang/String;

    .line 408
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v0

    .line 409
    .local v0, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    invoke-virtual {v0, p1, p2, p3}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    return-void
.end method

.method private disparaEventoAnalytcsMultiDocumentos()V
    .registers 5

    .line 388
    const-string v0, "CPF"

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->tipoDocumento:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 389
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->context:Landroid/content/Context;

    const v1, 0x7f0700ea

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->context:Landroid/content/Context;

    .line 390
    const v2, 0x7f0700cc

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->context:Landroid/content/Context;

    .line 391
    const v3, 0x7f0700c4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 389
    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->disparaEventoAnalytcs(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    return-void

    .line 394
    :cond_29
    const-string v0, "RNE"

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->tipoDocumento:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 395
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->context:Landroid/content/Context;

    const v1, 0x7f0700eb

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->context:Landroid/content/Context;

    .line 396
    const v2, 0x7f0700cc

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->context:Landroid/content/Context;

    .line 397
    const v3, 0x7f0700c4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 395
    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->disparaEventoAnalytcs(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    :cond_51
    return-void
.end method

.method private disparaEventoAnalyticsDocUnico()V
    .registers 5

    .line 402
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->context:Landroid/content/Context;

    const v1, 0x7f0700e9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->context:Landroid/content/Context;

    .line 403
    const v2, 0x7f0700cc

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->context:Landroid/content/Context;

    .line 404
    const v3, 0x7f0700c4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 402
    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->disparaEventoAnalytcs(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    return-void
.end method

.method private exibeCampoDocumento()V
    .registers 3

    .line 231
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 232
    return-void
.end method

.method private exibeHintErro(Landroid/view/View;Ljava/lang/String;)V
    .registers 5
    .param p1, "input"    # Landroid/view/View;
    .param p2, "mensagem"    # Ljava/lang/String;

    .line 276
    new-instance v0, Lbr/com/itau/widgets/hintview/Hint$Builder;

    const v1, 0x7f09011d

    invoke-direct {v0, p1, v1}, Lbr/com/itau/widgets/hintview/Hint$Builder;-><init>(Landroid/view/View;I)V

    invoke-static {v0, p2}, Lcom/itau/empresas/ui/util/HintUtils;->montaHintErro(Lbr/com/itau/widgets/hintview/Hint$Builder;Ljava/lang/String;)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->show()Lbr/com/itau/widgets/hintview/Hint;

    .line 277
    return-void
.end method

.method private exibeMultiplosOperadores()V
    .registers 3

    .line 239
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->rlMultiploOperador:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 240
    return-void
.end method

.method private exibeTiposDeDocumentos()V
    .registers 3

    .line 235
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->rgFiltroDocumentos:Landroid/widget/RadioGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 236
    return-void
.end method

.method private getNumeroDocumento()Ljava/lang/String;
    .registers 4

    .line 334
    const-string v0, "CPF"

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->tipoDocumento:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 335
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->CPF:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    .line 336
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->desformata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .local v2, "tipoDocumentoDesformatado":Ljava/lang/String;
    goto :goto_25

    .line 338
    .end local v2    # "tipoDocumentoDesformatado":Ljava/lang/String;
    :cond_1b
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 340
    .local v2, "tipoDocumentoDesformatado":Ljava/lang/String;
    :goto_25
    return-object v2
.end method

.method private iniciaElegibilidadeBotaoAcessar()V
    .registers 5

    .line 348
    new-instance v0, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->botaoComProgresso:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    iget-object v2, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoLogin:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    iget-object v3, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;-><init>(Lcom/itau/empresas/ui/view/BotaoComProgresso;Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;Landroid/widget/EditText;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->elegibilidadeBotaoAcessar:Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;

    .line 350
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->trataElegibilidadeBotaoAcessar()V

    .line 351
    return-void
.end method

.method private iniciaListeners()V
    .registers 3

    .line 327
    new-instance v0, Lcom/itau/empresas/feature/login/PreLoginCampoDocumentoListener;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    invoke-direct {v0, v1, p0}, Lcom/itau/empresas/feature/login/PreLoginCampoDocumentoListener;-><init>(Landroid/widget/EditText;Lcom/itau/empresas/feature/login/PreLoginCampoDocumentoListener$NotificaCampoDocumentoAlterado;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->campoDocumentoListener:Lcom/itau/empresas/feature/login/PreLoginCampoDocumentoListener;

    .line 328
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoLogin:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    invoke-virtual {v0, p0}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->setBarraUnicaListener(Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica$BarraUnicaListener;)V

    .line 329
    return-void
.end method

.method private iniciaViews()V
    .registers 3

    .line 314
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->inputs:Landroid/view/View;

    const v1, 0x7f0e05b1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoLogin:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    .line 315
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->inputs:Landroid/view/View;

    const v1, 0x7f0e05b2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->rgFiltroDocumentos:Landroid/widget/RadioGroup;

    .line 316
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->inputs:Landroid/view/View;

    const v1, 0x7f0e05b6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    .line 317
    const-string v0, "###.###.###-##"

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    .line 318
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/MascaraEditText;->insere(Ljava/lang/String;Landroid/widget/EditText;)Landroid/text/TextWatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->cpfWatcher:Landroid/text/TextWatcher;

    .line 319
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->inputs:Landroid/view/View;

    const v1, 0x7f0e05b7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->rlMultiploOperador:Landroid/widget/RelativeLayout;

    .line 320
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->inputs:Landroid/view/View;

    const v1, 0x7f0e05b8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->campoListaOperador:Landroid/widget/TextView;

    .line 321
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->inputs:Landroid/view/View;

    const v1, 0x7f0e05bb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/BotaoComProgresso;

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->botaoComProgresso:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    .line 322
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->campoListaOperador:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 323
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->rgFiltroDocumentos:Landroid/widget/RadioGroup;

    invoke-virtual {v0, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 324
    return-void
.end method

.method private ocultaCampoDocumento()V
    .registers 3

    .line 243
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    if-eqz v0, :cond_b

    .line 244
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 246
    :cond_b
    return-void
.end method

.method private ocultaMultiplosOperadores()V
    .registers 3

    .line 255
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->rlMultiploOperador:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_b

    .line 256
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->rlMultiploOperador:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 258
    :cond_b
    return-void
.end method

.method private ocultaTiposDeDocumentos()V
    .registers 3

    .line 249
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->rgFiltroDocumentos:Landroid/widget/RadioGroup;

    if-eqz v0, :cond_b

    .line 250
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->rgFiltroDocumentos:Landroid/widget/RadioGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 252
    :cond_b
    return-void
.end method

.method private postaTipoDeInputModoUnico()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;
    .registers 6

    .line 144
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->validaUnicoDocumento()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 145
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->disparaEventoAnalyticsDocUnico()V

    .line 146
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->preloginAgenciaContaDocumento()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;

    move-result-object v0

    return-object v0

    .line 148
    :cond_e
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->context:Landroid/content/Context;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    .line 149
    invoke-virtual {v3}, Landroid/widget/EditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 148
    const v3, 0x7f0704b5

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->exibeHintErro(Landroid/view/View;Ljava/lang/String;)V

    .line 150
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->alternaExibicaoParaProgresso(Z)V

    .line 151
    const/4 v0, 0x0

    return-object v0
.end method

.method private postaTipoDeInputMultiDocumentos()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;
    .registers 6

    .line 156
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->validaMultiDocumento()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 157
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->disparaEventoAnalytcsMultiDocumentos()V

    .line 158
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->preloginAgenciaContaDocumento()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;

    move-result-object v0

    return-object v0

    .line 160
    :cond_e
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->context:Landroid/content/Context;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    .line 161
    invoke-virtual {v3}, Landroid/widget/EditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 160
    const v3, 0x7f0704b5

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->exibeHintErro(Landroid/view/View;Ljava/lang/String;)V

    .line 162
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->alternaExibicaoParaProgresso(Z)V

    .line 163
    const/4 v0, 0x0

    return-object v0
.end method

.method private preloginAgenciaConta()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;
    .registers 3

    .line 360
    invoke-static {}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->builder()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoLogin:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    .line 361
    invoke-virtual {v1}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getAgencia()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->agencia(Ljava/lang/String;)Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoLogin:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    .line 362
    invoke-virtual {v1}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getConta()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->conta(Ljava/lang/String;)Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoLogin:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    .line 363
    invoke-virtual {v1}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getDac()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->dac(Ljava/lang/String;)Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;

    move-result-object v0

    .line 364
    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->build()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;

    move-result-object v0

    .line 360
    return-object v0
.end method

.method private preloginAgenciaContaDocumento()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;
    .registers 3

    .line 374
    invoke-static {}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->builder()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoLogin:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    .line 375
    invoke-virtual {v1}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getAgencia()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->agencia(Ljava/lang/String;)Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoLogin:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    .line 376
    invoke-virtual {v1}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getConta()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->conta(Ljava/lang/String;)Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoLogin:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    .line 377
    invoke-virtual {v1}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getDac()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->dac(Ljava/lang/String;)Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->tipoDocumento:Ljava/lang/String;

    .line 378
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->tipoDocumento(Ljava/lang/String;)Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;

    move-result-object v0

    .line 379
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->getNumeroDocumento()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->numeroDocumento(Ljava/lang/String;)Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;

    move-result-object v0

    .line 380
    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->build()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;

    move-result-object v0

    .line 374
    return-object v0
.end method

.method private preloginMultiOperadores()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;
    .registers 3

    .line 368
    invoke-static {}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->builder()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->operadorSelecionado:Lcom/itau/empresas/api/model/OperadorVO;

    .line 369
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/OperadorVO;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->operador(Ljava/lang/String;)Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;

    move-result-object v0

    .line 370
    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->build()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;

    move-result-object v0

    .line 368
    return-object v0
.end method

.method private preloginOperador()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;
    .registers 3

    .line 354
    invoke-static {}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;->builder()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoLogin:Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;

    .line 355
    invoke-virtual {v1}, Lcom/itau/empresas/ui/view/EditTextLoginBarraUnica;->getOperador()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->operador(Ljava/lang/String;)Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;

    move-result-object v0

    .line 356
    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->build()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;

    move-result-object v0

    .line 354
    return-object v0
.end method

.method private setHintCampoDocumento(Ljava/lang/String;)V
    .registers 3
    .param p1, "hint"    # Ljava/lang/String;

    .line 227
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 228
    return-void
.end method

.method private trataElegibilidadeBotaoAcessar()V
    .registers 3

    .line 384
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->elegibilidadeBotaoAcessar:Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;

    iget v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->modoAtual:I

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->trataElegibilidadeBotaoAcessar(I)V

    .line 385
    return-void
.end method

.method private validaMultiDocumento()Z
    .registers 3

    .line 266
    const-string v0, "CPF"

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->tipoDocumento:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 267
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->validaUnicoDocumento()Z

    move-result v0

    return v0

    .line 268
    :cond_f
    const-string v0, "PASSAPORTE"

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->tipoDocumento:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 269
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-eqz v0, :cond_23

    const/4 v0, 0x1

    goto :goto_24

    :cond_23
    const/4 v0, 0x0

    :goto_24
    return v0

    .line 271
    :cond_25
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    if-eqz v0, :cond_2f

    const/4 v0, 0x1

    goto :goto_30

    :cond_2f
    const/4 v0, 0x0

    :goto_30
    return v0
.end method

.method private validaUnicoDocumento()Z
    .registers 3

    .line 261
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 262
    .local v1, "cpf":Ljava/lang/String;
    sget-object v0, Lbr/com/concretesolutions/canarinho/validator/ValidadorCPF;->CPF:Lbr/com/concretesolutions/canarinho/validator/Validador;

    invoke-interface {v0, v1}, Lbr/com/concretesolutions/canarinho/validator/Validador;->ehValido(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public alternaExibicaoBotaoEntrarParProgresso(Z)V
    .registers 2
    .param p1, "alternaParaProgresso"    # Z

    .line 168
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->alternaExibicaoParaProgresso(Z)V

    .line 169
    return-void
.end method

.method public alterouConteudo()V
    .registers 3

    .line 218
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->ocultaTiposDeDocumentos()V

    .line 219
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->ocultaCampoDocumento()V

    .line 220
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->ocultaMultiplosOperadores()V

    .line 221
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 222
    const/4 v0, 0x0

    iput v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->modoAtual:I

    .line 223
    return-void
.end method

.method public campoDocumentoAlterado()V
    .registers 1

    .line 208
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->trataElegibilidadeBotaoAcessar()V

    .line 209
    return-void
.end method

.method public itemSelcecionado(Lcom/itau/empresas/api/model/OperadorVO;)V
    .registers 4
    .param p1, "operador"    # Lcom/itau/empresas/api/model/OperadorVO;

    .line 193
    iput-object p1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->operadorSelecionado:Lcom/itau/empresas/api/model/OperadorVO;

    .line 194
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->elegibilidadeBotaoAcessar:Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->operadorSelecionado:Lcom/itau/empresas/api/model/OperadorVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/PreLoginElegibilidadeBotaoAcessar;->setOperadorSelecionado(Lcom/itau/empresas/api/model/OperadorVO;)V

    .line 195
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->trataElegibilidadeBotaoAcessar()V

    .line 196
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->campoListaOperador:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/OperadorVO;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->dialogOperadores:Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;->fecharDialog()V

    .line 199
    return-void
.end method

.method public modoDocumentoUnico(Ljava/lang/String;)V
    .registers 3
    .param p1, "tipoDocumento"    # Ljava/lang/String;

    .line 78
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->alternaExibicaoBotaoEntrarParProgresso(Z)V

    .line 79
    const-string v0, "CPF"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 80
    const-string v0, "CPF"

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->tipoDocumento:Ljava/lang/String;

    .line 81
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->configuraCampoParaCPF()V

    .line 82
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->exibeCampoDocumento()V

    .line 84
    :cond_1b
    const/4 v0, 0x2

    iput v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->modoAtual:I

    .line 85
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->trataElegibilidadeBotaoAcessar()V

    .line 86
    return-void
.end method

.method public modoListaOperadores(Ljava/util/List;)V
    .registers 3
    .param p1, "operadores"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/OperadorVO;>;)V"
        }
    .end annotation

    .line 90
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->alternaExibicaoBotaoEntrarParProgresso(Z)V

    .line 91
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->exibeMultiplosOperadores()V

    .line 92
    new-instance v0, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;

    invoke-direct {v0, p1, p0}, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;-><init>(Ljava/util/List;Lcom/itau/empresas/feature/login/adapter/OperadoresAdapter$ListenerItemSelecionado;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->dialogOperadores:Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;

    .line 93
    const/4 v0, 0x4

    iput v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->modoAtual:I

    .line 94
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->trataElegibilidadeBotaoAcessar()V

    .line 95
    return-void
.end method

.method public modoMultiDocumentos()V
    .registers 2

    .line 99
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->alternaExibicaoBotaoEntrarParProgresso(Z)V

    .line 100
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->configuraCampoParaCPF()V

    .line 101
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->exibeCampoDocumento()V

    .line 102
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->exibeTiposDeDocumentos()V

    .line 103
    const/4 v0, 0x3

    iput v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->modoAtual:I

    .line 104
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->trataElegibilidadeBotaoAcessar()V

    .line 105
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .registers 5
    .param p1, "group"    # Landroid/widget/RadioGroup;
    .param p2, "checkedId"    # I

    .line 173
    packed-switch p2, :pswitch_data_2a

    goto :goto_c

    .line 175
    :pswitch_4
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->configuraCampoDocumentoRNE()V

    .line 176
    goto :goto_f

    .line 178
    :pswitch_8
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->configuraCampoDocumentoPassaporte()V

    .line 179
    goto :goto_f

    .line 182
    :goto_c
    :pswitch_c
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->configuraCampoParaCPF()V

    .line 185
    :goto_f
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 186
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->inputs:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/TecladoUtils;->esconderTecladoDaView(Landroid/content/Context;Landroid/view/View;)V

    .line 187
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 188
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->textoCampoDocumento:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/TecladoUtils;->mostrarTeclado(Landroid/content/Context;Landroid/view/View;)V

    .line 189
    return-void

    :pswitch_data_2a
    .packed-switch 0x7f0e05b3
        :pswitch_c
        :pswitch_4
        :pswitch_8
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4
    .param p1, "v"    # Landroid/view/View;

    .line 203
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->dialogOperadores:Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/util/DialogListaOperadoresUtil;->abreDialog(Landroid/content/Context;)V

    .line 204
    return-void
.end method

.method public postaTipoDeInput()V
    .registers 4

    .line 109
    const/4 v2, 0x0

    .line 110
    .local v2, "usuarioVO":Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->atualizaModoOperadorSeDigitarAgenciaConta()V

    .line 112
    iget v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->modoAtual:I

    packed-switch v0, :pswitch_data_2c

    goto :goto_22

    .line 114
    :pswitch_a
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->postaTipoDeInputModoUnico()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;

    move-result-object v2

    .line 115
    goto :goto_22

    .line 117
    :pswitch_f
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->postaTipoDeInputMultiDocumentos()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;

    move-result-object v2

    .line 118
    goto :goto_22

    .line 120
    :pswitch_14
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->preloginMultiOperadores()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;

    move-result-object v2

    .line 121
    goto :goto_22

    .line 123
    :pswitch_19
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->preloginAgenciaConta()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;

    move-result-object v2

    .line 124
    goto :goto_22

    .line 126
    :pswitch_1e
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->preloginOperador()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;

    move-result-object v2

    .line 127
    .line 131
    :goto_22
    if-eqz v2, :cond_2b

    .line 132
    iget-object v0, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->listener:Lcom/itau/empresas/feature/login/PreLoginInput$PreLoginInputListener;

    iget v1, p0, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->modoAtual:I

    invoke-interface {v0, v1, v2}, Lcom/itau/empresas/feature/login/PreLoginInput$PreLoginInputListener;->recebeConteudo(ILcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;)V

    .line 134
    :cond_2b
    return-void

    :pswitch_data_2c
    .packed-switch 0x0
        :pswitch_1e
        :pswitch_19
        :pswitch_a
        :pswitch_f
        :pswitch_14
    .end packed-switch
.end method

.method public preencheuAgenciaContaOuOperador()V
    .registers 1

    .line 213
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/PreLoginInputImpl;->trataElegibilidadeBotaoAcessar()V

    .line 214
    return-void
.end method

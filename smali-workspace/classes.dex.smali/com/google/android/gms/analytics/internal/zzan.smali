.class public Lcom/google/android/gms/analytics/internal/zzan;
.super Lcom/google/android/gms/analytics/internal/zzd;


# instance fields
.field protected zzPi:Z

.field protected zzRB:I

.field protected zzSE:Ljava/lang/String;

.field protected zzSF:Ljava/lang/String;

.field protected zzSH:I

.field protected zzTv:Z

.field protected zzTw:Z

.field protected zzTx:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/analytics/internal/zzf;)V
    .registers 2

    invoke-direct {p0, p1}, Lcom/google/android/gms/analytics/internal/zzd;-><init>(Lcom/google/android/gms/analytics/internal/zzf;)V

    return-void
.end method

.method private static zzby(Ljava/lang/String;)I
    .registers 3

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v0, "verbose"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x0

    return v0

    :cond_e
    const-string v0, "info"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    const/4 v0, 0x1

    return v0

    :cond_18
    const-string v0, "warning"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_22

    const/4 v0, 0x2

    return v0

    :cond_22
    const-string v0, "error"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2c

    const/4 v0, 0x3

    return v0

    :cond_2c
    const/4 v0, -0x1

    return v0
.end method


# virtual methods
.method public getLogLevel()I
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzan;->zzjv()V

    iget v0, p0, Lcom/google/android/gms/analytics/internal/zzan;->zzRB:I

    return v0
.end method

.method zza(Lcom/google/android/gms/analytics/internal/zzaa;)V
    .registers 5

    const-string v0, "Loading global XML config values"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzan;->zzbd(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzaa;->zzlf()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzaa;->zzlg()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/analytics/internal/zzan;->zzSE:Ljava/lang/String;

    const-string v0, "XML config - app name"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/analytics/internal/zzan;->zzb(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_16
    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzaa;->zzlh()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzaa;->zzli()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/analytics/internal/zzan;->zzSF:Ljava/lang/String;

    const-string v0, "XML config - app version"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/analytics/internal/zzan;->zzb(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_27
    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzaa;->zzlj()Z

    move-result v0

    if-eqz v0, :cond_42

    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzaa;->zzlk()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/analytics/internal/zzan;->zzby(Ljava/lang/String;)I

    move-result v2

    if-ltz v2, :cond_42

    iput v2, p0, Lcom/google/android/gms/analytics/internal/zzan;->zzRB:I

    const-string v0, "XML config - log level"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/analytics/internal/zzan;->zza(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_42
    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzaa;->zzll()Z

    move-result v0

    if-eqz v0, :cond_5a

    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzaa;->zzlm()I

    move-result v2

    iput v2, p0, Lcom/google/android/gms/analytics/internal/zzan;->zzSH:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/analytics/internal/zzan;->zzTw:Z

    const-string v0, "XML config - dispatch period (sec)"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/analytics/internal/zzan;->zzb(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_5a
    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzaa;->zzln()Z

    move-result v0

    if-eqz v0, :cond_72

    invoke-virtual {p1}, Lcom/google/android/gms/analytics/internal/zzaa;->zzlo()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/gms/analytics/internal/zzan;->zzPi:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/analytics/internal/zzan;->zzTx:Z

    const-string v0, "XML config - dry run"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/analytics/internal/zzan;->zzb(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_72
    return-void
.end method

.method protected zziJ()V
    .registers 1

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzan;->zzmd()V

    return-void
.end method

.method public zzlg()Ljava/lang/String;
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzan;->zzjv()V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzan;->zzSE:Ljava/lang/String;

    return-object v0
.end method

.method public zzli()Ljava/lang/String;
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzan;->zzjv()V

    iget-object v0, p0, Lcom/google/android/gms/analytics/internal/zzan;->zzSF:Ljava/lang/String;

    return-object v0
.end method

.method public zzlj()Z
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzan;->zzjv()V

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/internal/zzan;->zzTv:Z

    return v0
.end method

.method public zzll()Z
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzan;->zzjv()V

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/internal/zzan;->zzTw:Z

    return v0
.end method

.method public zzln()Z
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzan;->zzjv()V

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/internal/zzan;->zzTx:Z

    return v0
.end method

.method public zzlo()Z
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzan;->zzjv()V

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/internal/zzan;->zzPi:Z

    return v0
.end method

.method public zzmc()I
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzan;->zzjv()V

    iget v0, p0, Lcom/google/android/gms/analytics/internal/zzan;->zzSH:I

    return v0
.end method

.method protected zzmd()V
    .registers 9

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzan;->getContext()Landroid/content/Context;

    move-result-object v4

    :try_start_5
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x81

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_12
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5 .. :try_end_12} :catch_15

    move-result-object v0

    move-object v3, v0

    goto :goto_1b

    :catch_15
    move-exception v5

    const-string v0, "PackageManager doesn\'t know about the app package"

    invoke-virtual {p0, v0, v5}, Lcom/google/android/gms/analytics/internal/zzan;->zzd(Ljava/lang/String;Ljava/lang/Object;)V

    :goto_1b
    if-nez v3, :cond_23

    const-string v0, "Couldn\'t get ApplicationInfo to load global config"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/analytics/internal/zzan;->zzbg(Ljava/lang/String;)V

    return-void

    :cond_23
    iget-object v5, v3, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v5, :cond_44

    const-string v0, "com.google.android.gms.analytics.globalConfigResource"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    if-lez v6, :cond_44

    new-instance v0, Lcom/google/android/gms/analytics/internal/zzz;

    invoke-virtual {p0}, Lcom/google/android/gms/analytics/internal/zzan;->zzji()Lcom/google/android/gms/analytics/internal/zzf;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/analytics/internal/zzz;-><init>(Lcom/google/android/gms/analytics/internal/zzf;)V

    invoke-virtual {v0, v6}, Lcom/google/android/gms/analytics/internal/zzz;->zzah(I)Lcom/google/android/gms/analytics/internal/zzp;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/android/gms/analytics/internal/zzaa;

    if-eqz v7, :cond_44

    invoke-virtual {p0, v7}, Lcom/google/android/gms/analytics/internal/zzan;->zza(Lcom/google/android/gms/analytics/internal/zzaa;)V

    :cond_44
    return-void
.end method

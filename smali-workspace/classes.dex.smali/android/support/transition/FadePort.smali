.class Landroid/support/transition/FadePort;
.super Landroid/support/transition/VisibilityPort;
.source "FadePort.java"


# static fields
.field private static DBG:Z


# instance fields
.field private mFadingMode:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 80
    const/4 v0, 0x0

    sput-boolean v0, Landroid/support/transition/FadePort;->DBG:Z

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .line 88
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Landroid/support/transition/FadePort;-><init>(I)V

    .line 89
    return-void
.end method

.method public constructor <init>(I)V
    .registers 2
    .param p1, "fadingMode"    # I

    .line 98
    invoke-direct {p0}, Landroid/support/transition/VisibilityPort;-><init>()V

    .line 99
    iput p1, p0, Landroid/support/transition/FadePort;->mFadingMode:I

    .line 100
    return-void
.end method

.method private captureValues(Landroid/support/transition/TransitionValues;)V
    .registers 6
    .param p1, "transitionValues"    # Landroid/support/transition/TransitionValues;

    .line 126
    const/4 v0, 0x2

    new-array v3, v0, [I

    .line 127
    .local v3, "loc":[I
    iget-object v0, p1, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 128
    iget-object v0, p1, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:fade:screenX"

    const/4 v2, 0x0

    aget v2, v3, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    iget-object v0, p1, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:fade:screenY"

    const/4 v2, 0x1

    aget v2, v3, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    return-void
.end method

.method private createAnimation(Landroid/view/View;FFLandroid/animation/AnimatorListenerAdapter;)Landroid/animation/Animator;
    .registers 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "startAlpha"    # F
    .param p3, "endAlpha"    # F
    .param p4, "listener"    # Landroid/animation/AnimatorListenerAdapter;

    .line 107
    cmpl-float v0, p2, p3

    if-nez v0, :cond_c

    .line 109
    if-eqz p4, :cond_a

    .line 110
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 112
    :cond_a
    const/4 v0, 0x0

    return-object v0

    .line 114
    :cond_c
    const-string v0, "alpha"

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p2, v1, v2

    const/4 v2, 0x1

    aput p3, v1, v2

    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 116
    .local v3, "anim":Landroid/animation/ObjectAnimator;
    sget-boolean v0, Landroid/support/transition/FadePort;->DBG:Z

    if-eqz v0, :cond_37

    .line 117
    const-string v0, "Fade"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Created animator "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    :cond_37
    if-eqz p4, :cond_3c

    .line 120
    invoke-virtual {v3, p4}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 122
    :cond_3c
    return-object v3
.end method


# virtual methods
.method public captureStartValues(Landroid/support/transition/TransitionValues;)V
    .registers 2
    .param p1, "transitionValues"    # Landroid/support/transition/TransitionValues;

    .line 134
    invoke-super {p0, p1}, Landroid/support/transition/VisibilityPort;->captureStartValues(Landroid/support/transition/TransitionValues;)V

    .line 135
    invoke-direct {p0, p1}, Landroid/support/transition/FadePort;->captureValues(Landroid/support/transition/TransitionValues;)V

    .line 136
    return-void
.end method

.method public onAppear(Landroid/view/ViewGroup;Landroid/support/transition/TransitionValues;ILandroid/support/transition/TransitionValues;I)Landroid/animation/Animator;
    .registers 11
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/support/transition/TransitionValues;
    .param p3, "startVisibility"    # I
    .param p4, "endValues"    # Landroid/support/transition/TransitionValues;
    .param p5, "endVisibility"    # I

    .line 142
    iget v0, p0, Landroid/support/transition/FadePort;->mFadingMode:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_9

    if-nez p4, :cond_b

    .line 143
    :cond_9
    const/4 v0, 0x0

    return-object v0

    .line 145
    :cond_b
    iget-object v3, p4, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    .line 146
    .local v3, "endView":Landroid/view/View;
    sget-boolean v0, Landroid/support/transition/FadePort;->DBG:Z

    if-eqz v0, :cond_4d

    .line 147
    if-eqz p2, :cond_16

    iget-object v4, p2, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    goto :goto_17

    :cond_16
    const/4 v4, 0x0

    .line 148
    .local v4, "startView":Landroid/view/View;
    :goto_17
    const-string v0, "Fade"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fade.onAppear: startView, startVis, endView, endVis = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    .end local v4    # "startView":Landroid/view/View;
    :cond_4d
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/view/View;->setAlpha(F)V

    .line 152
    new-instance v4, Landroid/support/transition/FadePort$1;

    invoke-direct {v4, p0, v3}, Landroid/support/transition/FadePort$1;-><init>(Landroid/support/transition/FadePort;Landroid/view/View;)V

    .line 181
    .local v4, "transitionListener":Landroid/support/transition/TransitionPort$TransitionListener;
    invoke-virtual {p0, v4}, Landroid/support/transition/FadePort;->addListener(Landroid/support/transition/TransitionPort$TransitionListener;)Landroid/support/transition/TransitionPort;

    .line 182
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {p0, v3, v0, v1, v2}, Landroid/support/transition/FadePort;->createAnimation(Landroid/view/View;FFLandroid/animation/AnimatorListenerAdapter;)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method public onDisappear(Landroid/view/ViewGroup;Landroid/support/transition/TransitionValues;ILandroid/support/transition/TransitionValues;I)Landroid/animation/Animator;
    .registers 29
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/support/transition/TransitionValues;
    .param p3, "startVisibility"    # I
    .param p4, "endValues"    # Landroid/support/transition/TransitionValues;
    .param p5, "endVisibility"    # I

    .line 189
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/transition/FadePort;->mFadingMode:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_b

    .line 190
    const/4 v0, 0x0

    return-object v0

    .line 192
    :cond_b
    const/4 v7, 0x0

    .line 193
    .local v7, "view":Landroid/view/View;
    if-eqz p2, :cond_13

    move-object/from16 v0, p2

    iget-object v8, v0, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    goto :goto_14

    :cond_13
    const/4 v8, 0x0

    .line 194
    .local v8, "startView":Landroid/view/View;
    :goto_14
    if-eqz p4, :cond_1b

    move-object/from16 v0, p4

    iget-object v9, v0, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    goto :goto_1c

    :cond_1b
    const/4 v9, 0x0

    .line 195
    .local v9, "endView":Landroid/view/View;
    :goto_1c
    sget-boolean v0, Landroid/support/transition/FadePort;->DBG:Z

    if-eqz v0, :cond_5a

    .line 196
    const-string v0, "Fade"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fade.onDisappear: startView, startVis, endView, endVis = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v2, p3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v2, p5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    :cond_5a
    const/4 v10, 0x0

    .line 200
    .local v10, "overlayView":Landroid/view/View;
    const/4 v11, 0x0

    .line 201
    .local v11, "viewToKeep":Landroid/view/View;
    if-eqz v9, :cond_64

    invoke-virtual {v9}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_a6

    .line 202
    :cond_64
    if-eqz v9, :cond_6a

    .line 204
    move-object v10, v9

    move-object v7, v9

    goto/16 :goto_b5

    .line 205
    :cond_6a
    if-eqz v8, :cond_b5

    .line 209
    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_75

    .line 211
    move-object v10, v8

    move-object v7, v8

    goto :goto_b5

    .line 212
    :cond_75
    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_b5

    .line 213
    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_b5

    .line 214
    invoke-virtual {v8}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Landroid/view/View;

    .line 215
    .local v12, "startParent":Landroid/view/View;
    invoke-virtual {v12}, Landroid/view/View;->getId()I

    move-result v13

    .line 216
    .local v13, "id":I
    const/4 v0, -0x1

    if-eq v13, v0, :cond_a5

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_a5

    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/support/transition/FadePort;->mCanRemoveViews:Z

    if-eqz v0, :cond_a5

    .line 220
    move-object v10, v8

    move-object v7, v8

    .line 222
    .end local v12    # "startParent":Landroid/view/View;
    .end local v13    # "id":I
    :cond_a5
    goto :goto_b5

    .line 226
    :cond_a6
    move/from16 v0, p5

    const/4 v1, 0x4

    if-ne v0, v1, :cond_ae

    .line 227
    move-object v7, v9

    .line 228
    move-object v11, v7

    goto :goto_b5

    .line 231
    :cond_ae
    if-ne v8, v9, :cond_b3

    .line 232
    move-object v7, v9

    .line 233
    move-object v11, v7

    goto :goto_b5

    .line 235
    :cond_b3
    move-object v7, v8

    .line 236
    move-object v10, v7

    .line 240
    :cond_b5
    :goto_b5
    move/from16 v12, p5

    .line 242
    .local v12, "finalVisibility":I
    if-eqz v10, :cond_12d

    .line 244
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:fade:screenX"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v13

    .line 245
    .local v13, "screenX":I
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:fade:screenY"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v14

    .line 246
    .local v14, "screenY":I
    const/4 v0, 0x2

    new-array v15, v0, [I

    .line 247
    .local v15, "loc":[I
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/view/ViewGroup;->getLocationOnScreen([I)V

    .line 248
    const/4 v0, 0x0

    aget v0, v15, v0

    sub-int v0, v13, v0

    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v10, v0}, Landroid/support/v4/view/ViewCompat;->offsetLeftAndRight(Landroid/view/View;I)V

    .line 249
    const/4 v0, 0x1

    aget v0, v15, v0

    sub-int v0, v14, v0

    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v10, v0}, Landroid/support/v4/view/ViewCompat;->offsetTopAndBottom(Landroid/view/View;I)V

    .line 250
    invoke-static/range {p1 .. p1}, Landroid/support/transition/ViewGroupOverlay;->createFrom(Landroid/view/ViewGroup;)Landroid/support/transition/ViewGroupOverlay;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/support/transition/ViewGroupOverlay;->add(Landroid/view/View;)V

    .line 253
    const/high16 v16, 0x3f800000    # 1.0f

    .line 254
    .local v16, "startAlpha":F
    const/16 v17, 0x0

    .line 255
    .local v17, "endAlpha":F
    move-object/from16 v18, v7

    .line 256
    .local v18, "finalView":Landroid/view/View;
    move-object/from16 v19, v10

    .line 257
    .local v19, "finalOverlayView":Landroid/view/View;
    move-object/from16 v20, v11

    .line 258
    .local v20, "finalViewToKeep":Landroid/view/View;
    move-object/from16 v21, p1

    .line 259
    .local v21, "finalSceneRoot":Landroid/view/ViewGroup;
    new-instance v0, Landroid/support/transition/FadePort$2;

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    move-object/from16 v3, v20

    move v4, v12

    move-object/from16 v5, v19

    move-object/from16 v6, v21

    invoke-direct/range {v0 .. v6}, Landroid/support/transition/FadePort$2;-><init>(Landroid/support/transition/FadePort;Landroid/view/View;Landroid/view/View;ILandroid/view/View;Landroid/view/ViewGroup;)V

    move-object/from16 v22, v0

    .line 287
    .local v22, "endListener":Landroid/animation/AnimatorListenerAdapter;
    move-object/from16 v0, p0

    const/high16 v1, 0x3f800000    # 1.0f

    move/from16 v2, v17

    move-object/from16 v3, v22

    invoke-direct {v0, v7, v1, v2, v3}, Landroid/support/transition/FadePort;->createAnimation(Landroid/view/View;FFLandroid/animation/AnimatorListenerAdapter;)Landroid/animation/Animator;

    move-result-object v0

    return-object v0

    .line 289
    .end local v13    # "screenX":I
    .end local v14    # "screenY":I
    .end local v15    # "loc":[I
    .end local v16    # "startAlpha":F
    .end local v17    # "endAlpha":F
    .end local v18    # "finalView":Landroid/view/View;
    .end local v19    # "finalOverlayView":Landroid/view/View;
    .end local v20    # "finalViewToKeep":Landroid/view/View;
    .end local v21    # "finalSceneRoot":Landroid/view/ViewGroup;
    .end local v22    # "endListener":Landroid/animation/AnimatorListenerAdapter;
    :cond_12d
    if-eqz v11, :cond_159

    .line 292
    const/4 v0, 0x0

    invoke-virtual {v11, v0}, Landroid/view/View;->setVisibility(I)V

    .line 294
    const/high16 v13, 0x3f800000    # 1.0f

    .line 295
    .local v13, "startAlpha":F
    const/4 v14, 0x0

    .line 296
    .local v14, "endAlpha":F
    move-object v15, v7

    .line 297
    .local v15, "finalView":Landroid/view/View;
    move-object/from16 v16, v10

    .line 298
    .local v16, "finalOverlayView":Landroid/view/View;
    move-object/from16 v17, v11

    .line 299
    .local v17, "finalViewToKeep":Landroid/view/View;
    move-object/from16 v18, p1

    .line 300
    .local v18, "finalSceneRoot":Landroid/view/ViewGroup;
    new-instance v0, Landroid/support/transition/FadePort$3;

    move-object/from16 v1, p0

    move-object v2, v15

    move-object/from16 v3, v17

    move v4, v12

    move-object/from16 v5, v16

    move-object/from16 v6, v18

    invoke-direct/range {v0 .. v6}, Landroid/support/transition/FadePort$3;-><init>(Landroid/support/transition/FadePort;Landroid/view/View;Landroid/view/View;ILandroid/view/View;Landroid/view/ViewGroup;)V

    move-object/from16 v19, v0

    .line 345
    .local v19, "endListener":Landroid/animation/AnimatorListenerAdapter;
    move-object/from16 v0, p0

    const/high16 v1, 0x3f800000    # 1.0f

    move-object/from16 v2, v19

    invoke-direct {v0, v7, v1, v14, v2}, Landroid/support/transition/FadePort;->createAnimation(Landroid/view/View;FFLandroid/animation/AnimatorListenerAdapter;)Landroid/animation/Animator;

    move-result-object v0

    return-object v0

    .line 347
    .end local v13    # "startAlpha":F
    .end local v14    # "endAlpha":F
    .end local v15    # "finalView":Landroid/view/View;
    .end local v16    # "finalOverlayView":Landroid/view/View;
    .end local v17    # "finalViewToKeep":Landroid/view/View;
    .end local v18    # "finalSceneRoot":Landroid/view/ViewGroup;
    .end local v19    # "endListener":Landroid/animation/AnimatorListenerAdapter;
    :cond_159
    const/4 v0, 0x0

    return-object v0
.end method

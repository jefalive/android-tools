.class public Lcom/itau/empresas/api/model/ResultadoFeedbackVO;
.super Ljava/lang/Object;
.source "ResultadoFeedbackVO.java"


# instance fields
.field codigo:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Codigo"
    .end annotation
.end field

.field data:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Data"
    .end annotation
.end field

.field ld:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "LD"
    .end annotation
.end field

.field mensagem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Mensagem"
    .end annotation
.end field

.field mensagemErro:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "MensagemErro"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

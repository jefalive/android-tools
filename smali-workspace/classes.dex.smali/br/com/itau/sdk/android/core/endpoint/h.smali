.class public final Lbr/com/itau/sdk/android/core/endpoint/h;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/endpoint/h$a;
    }
.end annotation


# static fields
.field private static b:I

.field private static final n:[B

.field private static o:I


# instance fields
.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/reflect/Method;Lbr/com/itau/sdk/android/core/endpoint/k;>;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/Class<*>;Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;>;"
        }
    .end annotation
.end field

.field private final e:Lokhttp3/OkHttpClient;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;

.field private final i:Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareCaller;

.field private final j:Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;

.field private final k:Lbr/com/itau/sdk/android/core/webview/WebviewEventedErrorHandler;

.field private l:Z

.field private final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 39
    const/16 v0, 0x95

    new-array v0, v0, [B

    fill-array-data v0, :array_12

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/h;->n:[B

    const/16 v0, 0xb9

    sput v0, Lbr/com/itau/sdk/android/core/endpoint/h;->o:I

    const/4 v0, 0x0

    sput v0, Lbr/com/itau/sdk/android/core/endpoint/h;->b:I

    return-void

    nop

    :array_12
    .array-data 1
        0x6t
        -0x3ft
        -0x1et
        -0x3t
        -0x14t
        -0x10t
        -0x2t
        -0x72t
        0x72t
        0x4dt
        -0x47t
        -0x15t
        0x6t
        0x5t
        -0xbt
        0xft
        -0x5t
        0x1t
        -0x2bt
        -0x5t
        -0x1t
        0x7t
        0x43t
        -0x50t
        -0x3t
        -0x7t
        0x52t
        -0x55t
        -0x4t
        0x3t
        -0x2t
        -0x1t
        -0x5t
        -0x4t
        0xdt
        -0x1t
        -0x8t
        -0xbt
        -0xat
        0xbt
        0x6t
        0xct
        0x4t
        -0x8t
        -0x19t
        0x13t
        -0xdt
        0x1t
        0x3t
        0x1t
        0xct
        -0x14t
        -0xbt
        0x4t
        0x5t
        -0x2ft
        -0x2t
        0x1t
        -0x5t
        0x50t
        -0x4at
        0x5t
        -0xft
        0x8t
        -0xat
        0x5t
        -0xft
        0x50t
        -0x54t
        0xbt
        -0x11t
        -0x3t
        0x1t
        0x2t
        0x7t
        -0x1t
        0x42t
        -0x50t
        -0x9t
        0x7t
        -0x2t
        0x4at
        -0x48t
        -0xbt
        -0x5t
        0x50t
        -0x59t
        0x3t
        0xft
        -0x11t
        -0x2t
        0x9t
        -0x1t
        0x42t
        -0x47t
        -0x15t
        0x13t
        -0x4t
        -0xdt
        -0x6t
        0x9t
        -0x8t
        -0x1t
        0x3et
        -0x19t
        0x13t
        -0xdt
        0x1t
        0x3t
        0x1t
        0xbt
        -0x1dt
        0x2t
        0x4t
        -0xbt
        0x2bt
        -0x11t
        -0x2t
        -0x19t
        0x13t
        -0xdt
        0x1t
        0x3t
        0x1t
        0xbt
        -0x1dt
        0x2t
        0x4t
        -0xbt
        0x26t
        -0x25t
        -0xct
        0x8t
        0x4t
        -0xet
        -0x19t
        0x13t
        -0xdt
        0x1t
        0x3t
        0x1t
        0xbt
        -0x1dt
        0x2t
        0x4t
        -0xbt
        0x19t
        0x4t
        -0x8t
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lokhttp3/OkHttpClient;Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareCaller;)V
    .registers 11

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->c:Ljava/util/Map;

    .line 42
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->d:Ljava/util/Map;

    .line 48
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;

    invoke-direct {v0}, Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->j:Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;

    .line 49
    new-instance v0, Lbr/com/itau/sdk/android/core/webview/WebviewEventedErrorHandler;

    invoke-direct {v0}, Lbr/com/itau/sdk/android/core/webview/WebviewEventedErrorHandler;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->k:Lbr/com/itau/sdk/android/core/webview/WebviewEventedErrorHandler;

    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->l:Z

    .line 53
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/h;->n:[B

    const/16 v2, 0x11

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    or-int/lit8 v2, v1, 0x20

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/h;->n:[B

    const/16 v4, 0x86

    aget-byte v3, v3, v4

    neg-int v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/h;->a(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/h;->n:[B

    const/4 v2, 0x0

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/h;->n:[B

    const/16 v3, 0x86

    aget-byte v2, v2, v3

    neg-int v2, v2

    const/16 v3, 0x83

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/endpoint/h;->a(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/h;->n:[B

    const/4 v2, 0x0

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/h;->n:[B

    const/16 v3, 0x86

    aget-byte v2, v2, v3

    neg-int v2, v2

    const/16 v3, 0x64

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/endpoint/h;->a(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/h;->n:[B

    const/16 v2, 0x5b

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/h;->n:[B

    const/16 v3, 0x8

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/h;->n:[B

    const/16 v4, 0x86

    aget-byte v3, v3, v4

    neg-int v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/h;->a(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/h;->n:[B

    const/16 v2, 0x1d

    aget-byte v1, v1, v2

    sget v2, Lbr/com/itau/sdk/android/core/endpoint/h;->o:I

    and-int/lit8 v2, v2, 0x6e

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/h;->n:[B

    const/16 v4, 0x86

    aget-byte v3, v3, v4

    neg-int v3, v3

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/h;->a(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->m:Ljava/util/List;

    .line 65
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->f:Ljava/lang/String;

    .line 66
    iput-object p2, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->g:Ljava/lang/String;

    .line 67
    iput-object p3, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->e:Lokhttp3/OkHttpClient;

    .line 68
    iput-object p4, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->h:Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;

    .line 69
    iput-object p5, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->i:Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareCaller;

    .line 70
    return-void
.end method

.method private varargs a(Ljava/lang/reflect/Method;[Ljava/lang/Object;)Lbr/com/itau/sdk/android/core/endpoint/k;
    .registers 7

    .line 195
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lbr/com/itau/sdk/android/core/endpoint/k;

    .line 196
    if-eqz v1, :cond_13

    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/a/b;

    invoke-virtual {p1, v0}, Ljava/lang/reflect/Method;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 197
    :cond_13
    iget-object v2, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->c:Ljava/util/Map;

    monitor-enter v2

    .line 198
    :try_start_16
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lbr/com/itau/sdk/android/core/endpoint/k;

    .line 199
    if-eqz v1, :cond_29

    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/a/b;

    invoke-virtual {p1, v0}, Ljava/lang/reflect/Method;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_4c

    .line 200
    :cond_29
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/a/b;

    invoke-virtual {p1, v0}, Ljava/lang/reflect/Method;->isAnnotationPresent(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 201
    new-instance v1, Lbr/com/itau/sdk/android/core/endpoint/k;

    const/4 v0, 0x0

    aget-object v0, p2, v0

    check-cast v0, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;

    invoke-direct {v1, p1, v0}, Lbr/com/itau/sdk/android/core/endpoint/k;-><init>(Ljava/lang/reflect/Method;Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;)V

    .line 202
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->l:Z

    goto :goto_47

    .line 204
    :cond_3f
    new-instance v1, Lbr/com/itau/sdk/android/core/endpoint/k;

    invoke-direct {v1, p1}, Lbr/com/itau/sdk/android/core/endpoint/k;-><init>(Ljava/lang/reflect/Method;)V

    .line 205
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->l:Z

    .line 207
    :goto_47
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->c:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4c
    .catchall {:try_start_16 .. :try_end_4c} :catchall_4e

    .line 209
    :cond_4c
    monitor-exit v2

    goto :goto_51

    :catchall_4e
    move-exception v3

    monitor-exit v2

    throw v3

    .line 211
    :cond_51
    :goto_51
    return-object v1
.end method

.method private a(Lbr/com/itau/sdk/android/core/endpoint/a;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .registers 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lbr/com/itau/sdk/android/core/endpoint/a;Ljava/lang/String;Ljava/lang/Class<*>;)Ljava/lang/Object;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 135
    const/4 v4, 0x0

    .line 136
    const/4 v5, 0x0

    .line 139
    :try_start_2
    invoke-interface {p1}, Lbr/com/itau/sdk/android/core/endpoint/a;->a()Lbr/com/itau/sdk/android/core/endpoint/e;

    move-result-object v6

    .line 140
    instance-of v0, v6, Lbr/com/itau/sdk/android/core/endpoint/b;

    if-eqz v0, :cond_1c

    move-object v0, v6

    check-cast v0, Lbr/com/itau/sdk/android/core/endpoint/b;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/endpoint/b;->b()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 141
    move-object v0, v6

    check-cast v0, Lbr/com/itau/sdk/android/core/endpoint/b;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/endpoint/b;->e()Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;

    move-result-object v0

    move-object v5, v0

    goto :goto_21

    .line 143
    :cond_1c
    invoke-interface {v6}, Lbr/com/itau/sdk/android/core/endpoint/e;->d()Ljava/lang/Object;
    :try_end_1f
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_1f} :catch_22
    .catch Lbr/com/itau/sdk/android/core/exception/BackendException; {:try_start_2 .. :try_end_1f} :catch_28
    .catch Lbr/com/itau/sdk/android/core/exception/e; {:try_start_2 .. :try_end_1f} :catch_2b
    .catch Lbr/com/itau/sdk/android/core/exception/b; {:try_start_2 .. :try_end_1f} :catch_31
    .catch Lbr/com/itau/sdk/android/core/exception/TokenException; {:try_start_2 .. :try_end_1f} :catch_37
    .catch Lbr/com/itau/sdk/android/core/exception/i; {:try_start_2 .. :try_end_1f} :catch_3d
    .catch Lbr/com/itau/sdk/android/core/exception/h; {:try_start_2 .. :try_end_1f} :catch_43
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_1f} :catch_49

    move-result-object v0

    move-object v5, v0

    .line 161
    :goto_21
    goto :goto_4e

    .line 145
    :catch_22
    move-exception v6

    .line 146
    invoke-static {v6}, Lbr/com/itau/sdk/android/core/exception/BackendException;->networkError(Ljava/io/IOException;)Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v4

    .line 161
    goto :goto_4e

    .line 147
    :catch_28
    move-exception v6

    .line 148
    move-object v4, v6

    .line 161
    goto :goto_4e

    .line 149
    :catch_2b
    move-exception v6

    .line 150
    invoke-static {v6}, Lbr/com/itau/sdk/android/core/exception/BackendException;->routerException(Lbr/com/itau/sdk/android/core/exception/e;)Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v4

    .line 161
    goto :goto_4e

    .line 151
    :catch_31
    move-exception v6

    .line 152
    invoke-static {v6}, Lbr/com/itau/sdk/android/core/exception/BackendException;->conversionError(Lbr/com/itau/sdk/android/core/exception/b;)Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v4

    .line 161
    goto :goto_4e

    .line 153
    :catch_37
    move-exception v6

    .line 154
    invoke-static {v6}, Lbr/com/itau/sdk/android/core/exception/BackendException;->tokenException(Lbr/com/itau/sdk/android/core/exception/TokenException;)Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v4

    .line 161
    goto :goto_4e

    .line 155
    :catch_3d
    move-exception v6

    .line 156
    invoke-static {v6}, Lbr/com/itau/sdk/android/core/exception/BackendException;->versionControlException(Lbr/com/itau/sdk/android/core/exception/i;)Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v4

    .line 161
    goto :goto_4e

    .line 157
    :catch_43
    move-exception v6

    .line 158
    invoke-static {v6}, Lbr/com/itau/sdk/android/core/exception/BackendException;->simultaneousSessionException(Lbr/com/itau/sdk/android/core/exception/h;)Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v4

    .line 161
    goto :goto_4e

    .line 159
    :catch_49
    move-exception v6

    .line 160
    invoke-static {v6}, Lbr/com/itau/sdk/android/core/exception/BackendException;->unexpectedError(Ljava/lang/Throwable;)Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v4

    .line 163
    :goto_4e
    if-eqz v4, :cond_ba

    .line 165
    invoke-virtual {v4}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Ljavax/net/ssl/SSLHandshakeException;

    if-eqz v0, :cond_66

    sget v0, Lbr/com/itau/sdk/android/core/endpoint/h;->b:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lbr/com/itau/sdk/android/core/endpoint/h;->b:I

    const/4 v1, 0x4

    if-ge v0, v1, :cond_66

    .line 166
    invoke-direct {p0, p1, p2, p3}, Lbr/com/itau/sdk/android/core/endpoint/h;->a(Lbr/com/itau/sdk/android/core/endpoint/a;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 169
    :cond_66
    const/4 v0, 0x0

    sput v0, Lbr/com/itau/sdk/android/core/endpoint/h;->b:I

    .line 170
    invoke-virtual {v4, p2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->setOpKey(Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->m:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7b

    .line 173
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->j:Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;

    invoke-virtual {v0, v4}, Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;->handleError(Lbr/com/itau/sdk/android/core/exception/BackendException;)Ljava/lang/Throwable;

    move-result-object v6

    goto :goto_a1

    .line 174
    :cond_7b
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->l:Z

    if-eqz v0, :cond_9b

    .line 175
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->d:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_94

    .line 176
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->d:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;

    invoke-interface {v0, v4}, Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;->handleError(Lbr/com/itau/sdk/android/core/exception/BackendException;)Ljava/lang/Throwable;

    move-result-object v6

    goto :goto_a1

    .line 178
    :cond_94
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->h:Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;

    invoke-interface {v0, v4}, Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;->handleError(Lbr/com/itau/sdk/android/core/exception/BackendException;)Ljava/lang/Throwable;

    move-result-object v6

    goto :goto_a1

    .line 181
    :cond_9b
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->k:Lbr/com/itau/sdk/android/core/webview/WebviewEventedErrorHandler;

    invoke-virtual {v0, v4}, Lbr/com/itau/sdk/android/core/webview/WebviewEventedErrorHandler;->handleError(Lbr/com/itau/sdk/android/core/exception/BackendException;)Ljava/lang/Throwable;

    move-result-object v6

    .line 184
    :goto_a1
    if-nez v6, :cond_b9

    .line 185
    new-instance v0, Ljava/lang/IllegalStateException;

    sget v1, Lbr/com/itau/sdk/android/core/endpoint/h;->o:I

    and-int/lit8 v1, v1, 0x6f

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/h;->n:[B

    const/16 v3, 0x11

    aget-byte v2, v2, v3

    const/16 v3, 0x33

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/endpoint/h;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 187
    :cond_b9
    throw v6

    .line 190
    :cond_ba
    const/4 v0, 0x0

    sput v0, Lbr/com/itau/sdk/android/core/endpoint/h;->b:I

    .line 191
    return-object v5
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    add-int/lit8 p1, p1, 0x4

    add-int/lit8 p0, p0, 0x9

    const/4 v4, -0x1

    sget-object v5, Lbr/com/itau/sdk/android/core/endpoint/h;->n:[B

    mul-int/lit8 p2, p2, 0x2

    add-int/lit8 p2, p2, 0x43

    new-instance v0, Ljava/lang/String;

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v5, :cond_1b

    move v2, p0

    move v3, p1

    :goto_15
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p1, p1, 0x1

    add-int/lit8 p2, v2, -0x2

    :cond_1b
    add-int/lit8 v4, v4, 0x1

    int-to-byte v2, p2

    aput-byte v2, v1, v4

    if-ne v4, p0, :cond_27

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_27
    move v2, p2

    aget-byte v3, v5, p1

    goto :goto_15
.end method


# virtual methods
.method public a(Ljava/lang/Class;)Ljava/lang/Object;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Ljava/lang/Class<TT;>;)TT;"
        }
    .end annotation

    .line 77
    invoke-static {p1}, Lbr/com/itau/sdk/android/core/endpoint/aa;->a(Ljava/lang/Class;)V

    .line 79
    invoke-virtual {p1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    new-instance v2, Lbr/com/itau/sdk/android/core/endpoint/h$a;

    invoke-direct {v2, p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/h$a;-><init>(Lbr/com/itau/sdk/android/core/endpoint/h;Ljava/lang/Class;)V

    invoke-static {v0, v1, v2}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Class;Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;)Ljava/lang/Object;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:Ljava/lang/Object;>(Ljava/lang/Class<TT;>;Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;)TT;"
        }
    .end annotation

    .line 87
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->d:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    invoke-virtual {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/h;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method varargs a(ZLjava/lang/Class;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .registers 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZLjava/lang/Class<*>;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 99
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->isSessionValidOrEmpty()Z

    move-result v0

    if-nez v0, :cond_31

    .line 100
    new-instance v0, Lbr/com/itau/sdk/android/core/exception/g;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/h;->n:[B

    const/4 v2, 0x0

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/h;->n:[B

    const/16 v3, 0x11

    aget-byte v2, v2, v3

    add-int/lit8 v2, v2, -0x1

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/h;->n:[B

    const/16 v4, 0x3f

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/h;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/sdk/android/core/exception/g;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->sessionException(Lbr/com/itau/sdk/android/core/exception/g;)Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v7

    .line 101
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->h:Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;

    invoke-interface {v0, v7}, Lbr/com/itau/sdk/android/core/endpoint/ErrorHandler;->handleError(Lbr/com/itau/sdk/android/core/exception/BackendException;)Ljava/lang/Throwable;

    move-result-object v0

    throw v0

    .line 104
    :cond_31
    invoke-direct {p0, p3, p4}, Lbr/com/itau/sdk/android/core/endpoint/h;->a(Ljava/lang/reflect/Method;[Ljava/lang/Object;)Lbr/com/itau/sdk/android/core/endpoint/k;

    move-result-object v7

    .line 108
    iget-boolean v0, v7, Lbr/com/itau/sdk/android/core/endpoint/k;->h:Z

    if-eqz v0, :cond_52

    .line 109
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getFlowManager()Lbr/com/itau/sdk/android/core/FlowManager;

    move-result-object v0

    iget-object v1, v7, Lbr/com/itau/sdk/android/core/endpoint/k;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/FlowManager;->a(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/model/MiddlewareKeyValue;

    move-result-object v9

    .line 110
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/l;

    move-object v1, v9

    move-object v2, p4

    iget-object v3, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->i:Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareCaller;

    iget v4, v7, Lbr/com/itau/sdk/android/core/endpoint/k;->r:I

    iget v5, v7, Lbr/com/itau/sdk/android/core/endpoint/k;->s:I

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/sdk/android/core/endpoint/l;-><init>(Lbr/com/itau/sdk/android/core/model/MiddlewareKeyValue;[Ljava/lang/Object;Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareCaller;II)V

    move-object v8, v0

    .line 115
    goto :goto_86

    :cond_52
    iget-boolean v0, v7, Lbr/com/itau/sdk/android/core/endpoint/k;->i:Z

    if-eqz v0, :cond_6f

    .line 116
    const/4 v9, 0x0

    .line 117
    if-eqz p4, :cond_5f

    array-length v0, p4

    if-lez v0, :cond_5f

    .line 118
    const/4 v0, 0x0

    aget-object v9, p4, v0

    .line 119
    :cond_5f
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/l;

    iget-object v1, v7, Lbr/com/itau/sdk/android/core/endpoint/k;->p:Ljava/lang/String;

    move-object v2, v9

    iget-object v3, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->i:Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareCaller;

    iget v4, v7, Lbr/com/itau/sdk/android/core/endpoint/k;->r:I

    iget v5, v7, Lbr/com/itau/sdk/android/core/endpoint/k;->s:I

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/sdk/android/core/endpoint/l;-><init>(Ljava/lang/String;Ljava/lang/Object;Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareCaller;II)V

    move-object v8, v0

    .line 124
    goto :goto_86

    .line 125
    :cond_6f
    iget-boolean v0, v7, Lbr/com/itau/sdk/android/core/endpoint/k;->g:Z

    if-eqz v0, :cond_76

    iget-object v9, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->g:Ljava/lang/String;

    goto :goto_78

    :cond_76
    iget-object v9, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->f:Ljava/lang/String;

    .line 126
    :goto_78
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/n;

    move-object v1, v9

    iget-object v2, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->e:Lokhttp3/OkHttpClient;

    move v3, p1

    iget-object v4, p0, Lbr/com/itau/sdk/android/core/endpoint/h;->j:Lbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;

    move-object v5, v7

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lbr/com/itau/sdk/android/core/endpoint/n;-><init>(Ljava/lang/String;Lokhttp3/OkHttpClient;ZLbr/com/itau/sdk/android/core/endpoint/TokenErrorHandler;Lbr/com/itau/sdk/android/core/endpoint/k;[Ljava/lang/Object;)V

    move-object v8, v0

    .line 129
    :goto_86
    invoke-static {}, Lbr/com/itau/sdk/android/core/o;->a()Lbr/com/itau/sdk/android/core/o;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/o;->b()V

    .line 131
    iget-object v0, v7, Lbr/com/itau/sdk/android/core/endpoint/k;->p:Ljava/lang/String;

    invoke-direct {p0, v8, v0, p2}, Lbr/com/itau/sdk/android/core/endpoint/h;->a(Lbr/com/itau/sdk/android/core/endpoint/a;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public clone()Ljava/lang/Object;
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 95
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/h;->n:[B

    const/16 v2, 0x26

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/h;->n:[B

    const/16 v3, 0x86

    aget-byte v2, v2, v3

    neg-int v2, v2

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/h;->n:[B

    const/16 v4, 0x11

    aget-byte v3, v3, v4

    add-int/lit8 v3, v3, -0x1

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/h;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/CloneNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

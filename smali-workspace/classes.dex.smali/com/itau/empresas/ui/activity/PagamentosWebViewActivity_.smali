.class public final Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;
.super Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;
.source "PagamentosWebViewActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 32
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;-><init>()V

    .line 36
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;

    .line 32
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;

    .line 32
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private findSupportFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
    .registers 3
    .param p1, "tag"    # Ljava/lang/String;

    .line 96
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 51
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 52
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 53
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 54
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;->injectExtras_()V

    .line 55
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;->afterInject()V

    .line 56
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 100
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 101
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_3a

    .line 102
    const-string v0, "chaveMobile"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 103
    const-string v0, "chaveMobile"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;->chaveMobile:Ljava/lang/String;

    .line 105
    :cond_1a
    const-string v0, "nomeMenu"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 106
    const-string v0, "nomeMenu"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;->nomeMenu:Ljava/lang/String;

    .line 108
    :cond_2a
    const-string v0, "codigoBarra"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 109
    const-string v0, "codigoBarra"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;->codigoBarra:Ljava/lang/String;

    .line 112
    :cond_3a
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 137
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_$2;-><init>(Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 145
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 125
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_$1;-><init>(Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 133
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 43
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 44
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;->init_(Landroid/os/Bundle;)V

    .line 45
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->onCreate(Landroid/os/Bundle;)V

    .line 46
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 47
    const v0, 0x7f03006b

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;->setContentView(I)V

    .line 48
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 90
    const v0, 0x7f0e025a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 91
    const-string v0, "webviewfragment"

    invoke-direct {p0, v0}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;->findSupportFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;->webviewfragment:Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;

    .line 92
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;->aoCarregarWebView()V

    .line 93
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 60
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->setContentView(I)V

    .line 61
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 62
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 72
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->setContentView(Landroid/view/View;)V

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 74
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 66
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 68
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 116
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity;->setIntent(Landroid/content/Intent;)V

    .line 117
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/PagamentosWebViewActivity_;->injectExtras_()V

    .line 118
    return-void
.end method

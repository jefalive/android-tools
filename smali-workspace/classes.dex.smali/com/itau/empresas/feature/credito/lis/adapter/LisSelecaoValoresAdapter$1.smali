.class Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$1;
.super Ljava/lang/Object;
.source "LisSelecaoValoresAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;

    .line 53
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$1;->this$0:Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 4
    .param p1, "v"    # Landroid/view/View;

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$1;->this$0:Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;

    .line 57
    # getter for: Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->access$200(Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$1;->this$0:Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;

    .line 58
    # getter for: Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;
    invoke-static {v1}, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->access$100(Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;)Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$IntentBuilder_;->ofertaContratacaoLisVo(Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;)Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter$1;->this$0:Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;

    .line 59
    # getter for: Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->possuiLisAdicional:Z
    invoke-static {v1}, Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;->access$000(Lcom/itau/empresas/feature/credito/lis/adapter/LisSelecaoValoresAdapter;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$IntentBuilder_;->possuiProdutoLisAdiciona(Z)Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$IntentBuilder_;

    move-result-object v0

    .line 60
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 61
    return-void
.end method

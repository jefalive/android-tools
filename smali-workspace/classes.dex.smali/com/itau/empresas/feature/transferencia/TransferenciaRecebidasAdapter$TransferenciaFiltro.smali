.class final Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$TransferenciaFiltro;
.super Landroid/widget/Filter;
.source "TransferenciaRecebidasAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "TransferenciaFiltro"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;


# direct methods
.method private constructor <init>(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;)V
    .registers 2

    .line 295
    iput-object p1, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$TransferenciaFiltro;->this$0:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$1;)V
    .registers 3
    .param p1, "x0"    # Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;
    .param p2, "x1"    # Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$1;

    .line 295
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$TransferenciaFiltro;-><init>(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;)V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .registers 9
    .param p1, "busca"    # Ljava/lang/CharSequence;

    .line 299
    new-instance v2, Landroid/widget/Filter$FilterResults;

    invoke-direct {v2}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 300
    .local v2, "resultado":Landroid/widget/Filter$FilterResults;
    if-eqz p1, :cond_d

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_22

    .line 301
    :cond_d
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$TransferenciaFiltro;->this$0:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    # getter for: Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->listaOriginal:Ljava/util/List;
    invoke-static {v0}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->access$400(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;)Ljava/util/List;

    move-result-object v0

    iput-object v0, v2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 302
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$TransferenciaFiltro;->this$0:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    # getter for: Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->listaOriginal:Ljava/util/List;
    invoke-static {v0}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->access$400(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v2, Landroid/widget/Filter$FilterResults;->count:I

    goto :goto_72

    .line 304
    :cond_22
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 305
    .local v3, "listaTransferenciaRecebidaFiltrada":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$TransferenciaFiltro;->this$0:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    # getter for: Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->listaOriginal:Ljava/util/List;
    invoke-static {v0}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->access$400(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_31
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    .line 306
    .local v5, "c":Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;
    invoke-virtual {v5}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getValorPagamento()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 307
    .local v6, "valorPagamento":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;->getRemetente()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_66

    .line 308
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_69

    .line 309
    :cond_66
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 311
    .end local v5    # "c":Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;
    .end local v6    # "valorPagamento":Ljava/lang/String;
    :cond_69
    goto :goto_31

    .line 312
    :cond_6a
    iput-object v3, v2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 313
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v2, Landroid/widget/Filter$FilterResults;->count:I

    .line 315
    .end local v3    # "listaTransferenciaRecebidaFiltrada":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;>;"
    .end local v3
    :goto_72
    return-object v2
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .registers 5
    .param p1, "constraint"    # Ljava/lang/CharSequence;
    .param p2, "resultado"    # Landroid/widget/Filter$FilterResults;

    .line 320
    iget v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-ltz v0, :cond_17

    .line 321
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$TransferenciaFiltro;->this$0:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    iget-object v1, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v1, Ljava/util/List;

    # setter for: Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->listaTransferenciaRecebidas:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->access$502(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;Ljava/util/List;)Ljava/util/List;

    .line 322
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$TransferenciaFiltro;->this$0:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    # invokes: Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->ordenarAdapter()V
    invoke-static {v0}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->access$600(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;)V

    .line 323
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$TransferenciaFiltro;->this$0:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->notifyDataSetChanged()V

    .line 325
    :cond_17
    return-void
.end method

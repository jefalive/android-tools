.class final Lbr/com/itau/topsnackbar/DefaultManager;
.super Lbr/com/itau/topsnackbar/ViewManager;
.source "DefaultManager.java"


# direct methods
.method constructor <init>(Landroid/view/ViewGroup;)V
    .registers 2
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .line 9
    invoke-direct {p0, p1}, Lbr/com/itau/topsnackbar/ViewManager;-><init>(Landroid/view/ViewGroup;)V

    .line 10
    return-void
.end method


# virtual methods
.method public addContent(Landroid/view/View;)V
    .registers 3
    .param p1, "content"    # Landroid/view/View;

    .line 13
    iget-object v0, p0, Lbr/com/itau/topsnackbar/DefaultManager;->parent:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 14
    return-void
.end method

.method public removeContent(Landroid/view/View;)V
    .registers 3
    .param p1, "content"    # Landroid/view/View;

    .line 17
    iget-object v0, p0, Lbr/com/itau/topsnackbar/DefaultManager;->parent:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 18
    return-void
.end method

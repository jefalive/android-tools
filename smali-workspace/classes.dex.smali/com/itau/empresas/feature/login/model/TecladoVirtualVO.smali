.class public Lcom/itau/empresas/feature/login/model/TecladoVirtualVO;
.super Ljava/lang/Object;
.source "TecladoVirtualVO.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;>;"
        }
    .end annotation
.end field


# instance fields
.field private final idTeclado:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id_teclado"
    .end annotation
.end field

.field private final teclas:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "mapa_teclas"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 18
    new-instance v0, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$1;

    invoke-direct {v0}, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$1;-><init>()V

    sput-object v0, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method


# virtual methods
.method public getIdTeclado()Ljava/lang/String;
    .registers 2

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO;->idTeclado:Ljava/lang/String;

    return-object v0
.end method

.method public getTeclas()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lcom/itau/empresas/feature/login/model/TecladoVirtualVO$Tecla;>;"
        }
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO;->teclas:Ljava/util/ArrayList;

    return-object v0
.end method

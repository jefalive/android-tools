.class public Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;
.super Ljava/lang/Object;
.source "EtapasAutorizacaoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private codigoOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_operador"
    .end annotation
.end field

.field private cpfOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf_operador"
    .end annotation
.end field

.field private dataEtapa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_etapa"
    .end annotation
.end field

.field private etapa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "etapa"
    .end annotation
.end field

.field private horaEtapa:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "hora_etapa"
    .end annotation
.end field

.field private nomeOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_operador"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCodigoOperador()Ljava/lang/String;
    .registers 2

    .line 59
    iget-object v0, p0, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;->codigoOperador:Ljava/lang/String;

    return-object v0
.end method

.method public getCpfOperador()Ljava/lang/String;
    .registers 2

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;->cpfOperador:Ljava/lang/String;

    return-object v0
.end method

.method public getDataEtapa()Ljava/lang/String;
    .registers 2

    .line 35
    iget-object v0, p0, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;->dataEtapa:Ljava/lang/String;

    return-object v0
.end method

.method public getEtapa()Ljava/lang/String;
    .registers 2

    .line 27
    iget-object v0, p0, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;->etapa:Ljava/lang/String;

    return-object v0
.end method

.method public getHoraEtapa()Ljava/lang/String;
    .registers 2

    .line 43
    iget-object v0, p0, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;->horaEtapa:Ljava/lang/String;

    return-object v0
.end method

.method public getNomeOperador()Ljava/lang/String;
    .registers 2

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/api/model/EtapasAutorizacaoVO;->nomeOperador:Ljava/lang/String;

    return-object v0
.end method

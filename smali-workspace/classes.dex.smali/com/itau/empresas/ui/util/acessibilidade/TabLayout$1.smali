.class Lcom/itau/empresas/ui/util/acessibilidade/TabLayout$1;
.super Ljava/lang/Object;
.source "TabLayout.java"

# interfaces
.implements Landroid/support/design/widget/TabLayout$OnTabSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->setOnTabSelectedListener(Landroid/support/design/widget/TabLayout$OnTabSelectedListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;

.field final synthetic val$onTabSelectedListener:Landroid/support/design/widget/TabLayout$OnTabSelectedListener;


# direct methods
.method constructor <init>(Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;Landroid/support/design/widget/TabLayout$OnTabSelectedListener;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;

    .line 123
    iput-object p1, p0, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout$1;->this$0:Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;

    iput-object p2, p0, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout$1;->val$onTabSelectedListener:Landroid/support/design/widget/TabLayout$OnTabSelectedListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTabReselected(Landroid/support/design/widget/TabLayout$Tab;)V
    .registers 3
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;

    .line 145
    iget-object v0, p0, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout$1;->val$onTabSelectedListener:Landroid/support/design/widget/TabLayout$OnTabSelectedListener;

    if-eqz v0, :cond_9

    .line 146
    iget-object v0, p0, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout$1;->val$onTabSelectedListener:Landroid/support/design/widget/TabLayout$OnTabSelectedListener;

    invoke-interface {v0, p1}, Landroid/support/design/widget/TabLayout$OnTabSelectedListener;->onTabReselected(Landroid/support/design/widget/TabLayout$Tab;)V

    .line 148
    :cond_9
    return-void
.end method

.method public onTabSelected(Landroid/support/design/widget/TabLayout$Tab;)V
    .registers 4
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;

    .line 127
    iget-object v0, p0, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout$1;->this$0:Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;

    const/4 v1, 0x1

    # invokes: Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->aplicaAcessibilidadeTab(Landroid/support/design/widget/TabLayout$Tab;Z)V
    invoke-static {v0, p1, v1}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->access$000(Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;Landroid/support/design/widget/TabLayout$Tab;Z)V

    .line 129
    iget-object v0, p0, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout$1;->val$onTabSelectedListener:Landroid/support/design/widget/TabLayout$OnTabSelectedListener;

    if-eqz v0, :cond_f

    .line 130
    iget-object v0, p0, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout$1;->val$onTabSelectedListener:Landroid/support/design/widget/TabLayout$OnTabSelectedListener;

    invoke-interface {v0, p1}, Landroid/support/design/widget/TabLayout$OnTabSelectedListener;->onTabSelected(Landroid/support/design/widget/TabLayout$Tab;)V

    .line 132
    :cond_f
    return-void
.end method

.method public onTabUnselected(Landroid/support/design/widget/TabLayout$Tab;)V
    .registers 4
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;

    .line 136
    iget-object v0, p0, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout$1;->this$0:Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;

    const/4 v1, 0x0

    # invokes: Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->aplicaAcessibilidadeTab(Landroid/support/design/widget/TabLayout$Tab;Z)V
    invoke-static {v0, p1, v1}, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;->access$000(Lcom/itau/empresas/ui/util/acessibilidade/TabLayout;Landroid/support/design/widget/TabLayout$Tab;Z)V

    .line 138
    iget-object v0, p0, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout$1;->val$onTabSelectedListener:Landroid/support/design/widget/TabLayout$OnTabSelectedListener;

    if-eqz v0, :cond_f

    .line 139
    iget-object v0, p0, Lcom/itau/empresas/ui/util/acessibilidade/TabLayout$1;->val$onTabSelectedListener:Landroid/support/design/widget/TabLayout$OnTabSelectedListener;

    invoke-interface {v0, p1}, Landroid/support/design/widget/TabLayout$OnTabSelectedListener;->onTabUnselected(Landroid/support/design/widget/TabLayout$Tab;)V

    .line 141
    :cond_f
    return-void
.end method

.class public final Lcom/google/android/gms/common/stats/WakeLockEvent;
.super Lcom/google/android/gms/common/stats/zzf;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<Lcom/google/android/gms/common/stats/WakeLockEvent;>;"
        }
    .end annotation
.end field


# instance fields
.field private final mTimeout:J

.field final mVersionCode:I

.field private final zzanQ:Ljava/lang/String;

.field private final zzanR:I

.field private final zzanS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field private final zzanT:Ljava/lang/String;

.field private zzanU:I

.field private final zzanV:Ljava/lang/String;

.field private final zzanW:Ljava/lang/String;

.field private final zzanX:F

.field private final zzane:J

.field private zzanf:I

.field private final zzanm:J

.field private zzano:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    new-instance v0, Lcom/google/android/gms/common/stats/zzh;

    invoke-direct {v0}, Lcom/google/android/gms/common/stats/zzh;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/stats/WakeLockEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IJILjava/lang/String;ILjava/util/List;Ljava/lang/String;JILjava/lang/String;Ljava/lang/String;FJ)V
    .registers 19
    .param p1, "versionCode"    # I
    .param p2, "timeMillis"    # J
    .param p4, "eventType"    # I
    .param p5, "wakelockName"    # Ljava/lang/String;
    .param p6, "wakelockType"    # I
    .param p7, "callingPackages"    # Ljava/util/List;
    .param p8, "eventKey"    # Ljava/lang/String;
    .param p9, "elapsedRealtime"    # J
    .param p11, "deviceState"    # I
    .param p12, "secondaryWakeLockName"    # Ljava/lang/String;
    .param p13, "hostPackageName"    # Ljava/lang/String;
    .param p14, "beginPowerPercentage"    # F
    .param p15, "timeout"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJILjava/lang/String;ILjava/util/List<Ljava/lang/String;>;Ljava/lang/String;JILjava/lang/String;Ljava/lang/String;FJ)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/gms/common/stats/zzf;-><init>()V

    iput p1, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->mVersionCode:I

    iput-wide p2, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzane:J

    iput p4, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzanf:I

    iput-object p5, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzanQ:Ljava/lang/String;

    iput-object p12, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzanV:Ljava/lang/String;

    iput p6, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzanR:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzano:J

    iput-object p7, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzanS:Ljava/util/List;

    iput-object p8, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzanT:Ljava/lang/String;

    iput-wide p9, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzanm:J

    iput p11, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzanU:I

    iput-object p13, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzanW:Ljava/lang/String;

    move/from16 v0, p14

    iput v0, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzanX:F

    move-wide/from16 v0, p15

    iput-wide v0, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->mTimeout:J

    return-void
.end method

.method public constructor <init>(JILjava/lang/String;ILjava/util/List;Ljava/lang/String;JILjava/lang/String;Ljava/lang/String;FJ)V
    .registers 33
    .param p1, "timeMillis"    # J
    .param p3, "eventType"    # I
    .param p4, "wakelockName"    # Ljava/lang/String;
    .param p5, "wakelockType"    # I
    .param p6, "callingPackages"    # Ljava/util/List;
    .param p7, "eventKey"    # Ljava/lang/String;
    .param p8, "elapsedRealtime"    # J
    .param p10, "deviceState"    # I
    .param p11, "secondaryWakeLockName"    # Ljava/lang/String;
    .param p12, "hostPackageName"    # Ljava/lang/String;
    .param p13, "beginPowerPercentage"    # F
    .param p14, "timeout"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JILjava/lang/String;ILjava/util/List<Ljava/lang/String;>;Ljava/lang/String;JILjava/lang/String;Ljava/lang/String;FJ)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-wide/from16 v2, p1

    move/from16 v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-wide/from16 v9, p8

    move/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move/from16 v14, p13

    move-wide/from16 v15, p14

    const/4 v1, 0x1

    invoke-direct/range {v0 .. v16}, Lcom/google/android/gms/common/stats/WakeLockEvent;-><init>(IJILjava/lang/String;ILjava/util/List;Ljava/lang/String;JILjava/lang/String;Ljava/lang/String;FJ)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    const/4 v0, 0x0

    return v0
.end method

.method public getEventType()I
    .registers 2

    iget v0, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzanf:I

    return v0
.end method

.method public getTimeMillis()J
    .registers 3

    iget-wide v0, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzane:J

    return-wide v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 3
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/common/stats/zzh;->zza(Lcom/google/android/gms/common/stats/WakeLockEvent;Landroid/os/Parcel;I)V

    return-void
.end method

.method public zzrK()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzanT:Ljava/lang/String;

    return-object v0
.end method

.method public zzrL()J
    .registers 3

    iget-wide v0, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzano:J

    return-wide v0
.end method

.method public zzrN()J
    .registers 3

    iget-wide v0, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzanm:J

    return-wide v0
.end method

.method public zzrO()Ljava/lang/String;
    .registers 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\t"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzrR()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\t"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzrT()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\t"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzrU()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_30

    const-string v1, ""

    goto :goto_3a

    :cond_30
    const-string v1, ","

    invoke-virtual {p0}, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzrU()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    :goto_3a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\t"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzrV()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\t"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzrS()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5b

    const-string v1, ""

    goto :goto_5f

    :cond_5b
    invoke-virtual {p0}, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzrS()Ljava/lang/String;

    move-result-object v1

    :goto_5f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\t"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzrW()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_72

    const-string v1, ""

    goto :goto_76

    :cond_72
    invoke-virtual {p0}, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzrW()Ljava/lang/String;

    move-result-object v1

    :goto_76
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\t"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzrX()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public zzrR()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzanQ:Ljava/lang/String;

    return-object v0
.end method

.method public zzrS()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzanV:Ljava/lang/String;

    return-object v0
.end method

.method public zzrT()I
    .registers 2

    iget v0, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzanR:I

    return v0
.end method

.method public zzrU()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzanS:Ljava/util/List;

    return-object v0
.end method

.method public zzrV()I
    .registers 2

    iget v0, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzanU:I

    return v0
.end method

.method public zzrW()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzanW:Ljava/lang/String;

    return-object v0
.end method

.method public zzrX()F
    .registers 2

    iget v0, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->zzanX:F

    return v0
.end method

.method public zzrY()J
    .registers 3

    iget-wide v0, p0, Lcom/google/android/gms/common/stats/WakeLockEvent;->mTimeout:J

    return-wide v0
.end method

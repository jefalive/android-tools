.class public final Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter_;
.super Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;
.source "ContatosRecargaAdapter_.java"


# instance fields
.field private context_:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 20
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter_;->context_:Landroid/content/Context;

    .line 22
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter_;->init_()V

    .line 23
    return-void
.end method

.method public static getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 26
    new-instance v0, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private init_()V
    .registers 4

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter_;->context_:Landroid/content/Context;

    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter_;->contatoRecargaController:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    .line 31
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter_;->context_:Landroid/content/Context;

    instance-of v0, v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    if-eqz v0, :cond_15

    .line 32
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter_;->context_:Landroid/content/Context;

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter_;->activity:Lcom/itau/empresas/ui/activity/BaseActivity;

    goto :goto_3d

    .line 34
    :cond_15
    const-string v0, "ContatosRecargaAdapter_"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Due to Context class "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/adapter/ContatosRecargaAdapter_;->context_:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", the @RootContext BaseActivity won\'t be populated"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    :goto_3d
    return-void
.end method

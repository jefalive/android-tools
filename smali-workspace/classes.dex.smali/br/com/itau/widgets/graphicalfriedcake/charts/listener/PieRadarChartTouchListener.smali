.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/ChartTouchListener;
.source "PieRadarChartTouchListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/ChartTouchListener<Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase<*>;>;"
    }
.end annotation


# instance fields
.field private _velocitySamples:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;>;"
        }
    .end annotation
.end field

.field private mDecelerationAngularVelocity:F

.field private mDecelerationLastTime:J

.field private mStartAngle:F

.field private mTouchStartPoint:Landroid/graphics/PointF;


# direct methods
.method public constructor <init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;)V
    .registers 4
    .param p1, "chart"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase<*>;)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/ChartTouchListener;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;)V

    .line 27
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mTouchStartPoint:Landroid/graphics/PointF;

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mStartAngle:F

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->_velocitySamples:Ljava/util/ArrayList;

    .line 34
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mDecelerationLastTime:J

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mDecelerationAngularVelocity:F

    .line 39
    return-void
.end method

.method private calculateVelocity()F
    .registers 11

    .line 226
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->_velocitySamples:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 227
    const/4 v0, 0x0

    return v0

    .line 229
    :cond_a
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->_velocitySamples:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;

    .line 230
    .local v4, "firstSample":Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->_velocitySamples:Ljava/util/ArrayList;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->_velocitySamples:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;

    .line 233
    .local v5, "lastSample":Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;
    move-object v6, v4

    .line 234
    .local v6, "beforeLastSample":Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->_velocitySamples:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v7, v0, -0x1

    .local v7, "i":I
    :goto_2e
    if-ltz v7, :cond_45

    .line 236
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->_velocitySamples:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;

    .line 237
    iget v0, v6, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;->angle:F

    iget v1, v5, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;->angle:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_42

    .line 239
    goto :goto_45

    .line 234
    :cond_42
    add-int/lit8 v7, v7, -0x1

    goto :goto_2e

    .line 244
    .end local v7    # "i":I
    :cond_45
    :goto_45
    iget-wide v0, v5, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;->time:J

    iget-wide v2, v4, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;->time:J

    sub-long/2addr v0, v2

    long-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float v7, v0, v1

    .line 245
    .local v7, "timeDelta":F
    const/4 v0, 0x0

    cmpl-float v0, v7, v0

    if-nez v0, :cond_57

    .line 247
    const v7, 0x3dcccccd    # 0.1f

    .line 252
    :cond_57
    iget v0, v5, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;->angle:F

    iget v1, v6, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;->angle:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_61

    const/4 v8, 0x1

    goto :goto_62

    :cond_61
    const/4 v8, 0x0

    .line 253
    .local v8, "clockwise":Z
    :goto_62
    iget v0, v5, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;->angle:F

    iget v1, v6, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;->angle:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x4070e00000000000L    # 270.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_7a

    .line 255
    if-nez v8, :cond_79

    const/4 v8, 0x1

    goto :goto_7a

    :cond_79
    const/4 v8, 0x0

    .line 259
    :cond_7a
    :goto_7a
    iget v0, v5, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;->angle:F

    iget v1, v4, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;->angle:F

    sub-float/2addr v0, v1

    float-to-double v0, v0

    const-wide v2, 0x4066800000000000L    # 180.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_96

    .line 261
    iget v0, v4, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;->angle:F

    float-to-double v0, v0

    const-wide v2, 0x4076800000000000L    # 360.0

    add-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, v4, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;->angle:F

    goto :goto_b1

    .line 263
    :cond_96
    iget v0, v4, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;->angle:F

    iget v1, v5, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;->angle:F

    sub-float/2addr v0, v1

    float-to-double v0, v0

    const-wide v2, 0x4066800000000000L    # 180.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_b1

    .line 265
    iget v0, v5, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;->angle:F

    float-to-double v0, v0

    const-wide v2, 0x4076800000000000L    # 360.0

    add-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, v5, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;->angle:F

    .line 269
    :cond_b1
    :goto_b1
    iget v0, v5, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;->angle:F

    iget v1, v4, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;->angle:F

    sub-float/2addr v0, v1

    div-float/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v9

    .line 272
    .local v9, "velocity":F
    if-nez v8, :cond_be

    .line 274
    neg-float v9, v9

    .line 277
    :cond_be
    return v9
.end method

.method private resetVelocity()V
    .registers 2

    .line 199
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->_velocitySamples:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 200
    return-void
.end method

.method private sampleVelocity(FF)V
    .registers 11
    .param p1, "touchLocationX"    # F
    .param p2, "touchLocationY"    # F

    .line 204
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v4

    .line 206
    .local v4, "currentTime":J
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->_velocitySamples:Ljava/util/ArrayList;

    new-instance v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    invoke-virtual {v2, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getAngleForPoint(FF)F

    move-result v2

    invoke-direct {v1, p0, v4, v5, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;JF)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 209
    const/4 v6, 0x0

    .local v6, "i":I
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->_velocitySamples:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    .local v7, "count":I
    :goto_1d
    add-int/lit8 v0, v7, -0x2

    if-ge v6, v0, :cond_40

    .line 211
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->_velocitySamples:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;

    iget-wide v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener$AngularVelocitySample;->time:J

    sub-long v0, v4, v0

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-lez v0, :cond_40

    .line 213
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->_velocitySamples:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 214
    add-int/lit8 v6, v6, -0x1

    .line 215
    add-int/lit8 v7, v7, -0x1

    .line 209
    add-int/lit8 v6, v6, 0x1

    goto :goto_1d

    .line 222
    .end local v6    # "i":I
    .end local v7    # "count":I
    :cond_40
    return-void
.end method


# virtual methods
.method public computeScroll()V
    .registers 8

    .line 311
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mDecelerationAngularVelocity:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_8

    .line 312
    return-void

    .line 314
    :cond_8
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v4

    .line 316
    .local v4, "currentTime":J
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mDecelerationAngularVelocity:F

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getDragDecelerationFrictionCoef()F

    move-result v1

    mul-float/2addr v0, v1

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mDecelerationAngularVelocity:F

    .line 318
    iget-wide v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mDecelerationLastTime:J

    sub-long v0, v4, v0

    long-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float v6, v0, v1

    .line 320
    .local v6, "timeInterval":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getRotationAngle()F

    move-result v1

    iget v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mDecelerationAngularVelocity:F

    mul-float/2addr v2, v6

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->setRotationAngle(F)V

    .line 322
    iput-wide v4, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mDecelerationLastTime:J

    .line 324
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mDecelerationAngularVelocity:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x3f50624dd2f1a9fcL    # 0.001

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_4d

    .line 325
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->postInvalidateOnAnimation(Landroid/view/View;)V

    goto :goto_50

    .line 327
    :cond_4d
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->stopDeceleration()V

    .line 328
    :goto_50
    return-void
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .registers 4
    .param p1, "me"    # Landroid/view/MotionEvent;

    .line 115
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getOnChartGestureListener()Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartGestureListener;

    move-result-object v1

    .line 117
    .local v1, "l":Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartGestureListener;
    if-eqz v1, :cond_d

    .line 118
    invoke-interface {v1, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartGestureListener;->onChartLongPressed(Landroid/view/MotionEvent;)V

    .line 120
    :cond_d
    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .registers 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .line 124
    const/4 v0, 0x1

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .registers 12
    .param p1, "e"    # Landroid/view/MotionEvent;

    .line 130
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getOnChartGestureListener()Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartGestureListener;

    move-result-object v3

    .line 132
    .local v3, "l":Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartGestureListener;
    if-eqz v3, :cond_d

    .line 133
    invoke-interface {v3, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/OnChartGestureListener;->onChartSingleTapped(Landroid/view/MotionEvent;)V

    .line 136
    :cond_d
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->distanceToCenter(FF)F

    move-result v4

    .line 139
    .local v4, "distance":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getRadius()F

    move-result v0

    cmpl-float v0, v4, v0

    if-lez v0, :cond_2b

    goto/16 :goto_91

    .line 147
    :cond_2b
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getAngleForPoint(FF)F

    move-result v5

    .line 149
    .local v5, "angle":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    instance-of v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieChart;

    if-eqz v0, :cond_4e

    .line 150
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getAnimator()Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;->getPhaseY()F

    move-result v0

    div-float/2addr v5, v0

    .line 153
    :cond_4e
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    invoke-virtual {v0, v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getIndexForAngle(F)I

    move-result v6

    .line 156
    .local v6, "index":I
    if-gez v6, :cond_59

    goto :goto_91

    .line 163
    :cond_59
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    invoke-virtual {v0, v6}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getSelectionDetailsAtIndex(I)Ljava/util/List;

    move-result-object v7

    .line 165
    .local v7, "valsAtIndex":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;>;"
    const/4 v8, 0x0

    .line 170
    .local v8, "dataSetIndex":I
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    instance-of v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    if-eqz v0, :cond_77

    .line 172
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    .line 173
    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getFactor()F

    move-result v0

    div-float v0, v4, v0

    .line 172
    const/4 v1, 0x0

    invoke-static {v7, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getClosestDataSetIndex(Ljava/util/List;FLbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;)I

    move-result v8

    .line 176
    :cond_77
    if-gez v8, :cond_7a

    goto :goto_91

    .line 180
    :cond_7a
    new-instance v9, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    invoke-direct {v9, v6, v8}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;-><init>(II)V

    .line 182
    .local v9, "h":Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mLastHighlighted:Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    invoke-virtual {v9, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->equalTo(Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)Z

    move-result v0

    if-eqz v0, :cond_88

    goto :goto_91

    .line 188
    :cond_88
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    invoke-virtual {v0, v9}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->highlightTouch(Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)V

    .line 189
    iput-object v9, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mLastHighlighted:Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    .line 195
    .end local v5    # "angle":F
    .end local v6    # "index":I
    .end local v7    # "valsAtIndex":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;>;"
    .end local v7
    .end local v8    # "dataSetIndex":I
    .end local v9    # "h":Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;
    :goto_91
    const/4 v0, 0x1

    return v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .line 45
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 46
    const/4 v0, 0x1

    return v0

    .line 49
    :cond_a
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->isRotationEnabled()Z

    move-result v0

    if-eqz v0, :cond_b9

    .line 51
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 52
    .local v2, "x":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    .line 54
    .local v3, "y":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_bc

    goto/16 :goto_b9

    .line 58
    :pswitch_25
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->stopDeceleration()V

    .line 60
    invoke-direct {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->resetVelocity()V

    .line 62
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->isDragDecelerationEnabled()Z

    move-result v0

    if-eqz v0, :cond_38

    .line 63
    invoke-direct {p0, v2, v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->sampleVelocity(FF)V

    .line 65
    :cond_38
    invoke-virtual {p0, v2, v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->setGestureStartAngle(FF)V

    .line 66
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mTouchStartPoint:Landroid/graphics/PointF;

    iput v2, v0, Landroid/graphics/PointF;->x:F

    .line 67
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mTouchStartPoint:Landroid/graphics/PointF;

    iput v3, v0, Landroid/graphics/PointF;->y:F

    .line 69
    goto/16 :goto_b9

    .line 72
    :pswitch_45
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->isDragDecelerationEnabled()Z

    move-result v0

    if-eqz v0, :cond_52

    .line 73
    invoke-direct {p0, v2, v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->sampleVelocity(FF)V

    .line 75
    :cond_52
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mTouchMode:I

    if-nez v0, :cond_77

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mTouchStartPoint:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mTouchStartPoint:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 76
    invoke-static {v2, v0, v3, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->distance(FFFF)F

    move-result v0

    .line 77
    const/high16 v1, 0x41000000    # 8.0f

    invoke-static {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_77

    .line 78
    const/4 v0, 0x6

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mTouchMode:I

    .line 79
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->disableScroll()V

    goto :goto_b9

    .line 80
    :cond_77
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mTouchMode:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_b9

    .line 81
    invoke-virtual {p0, v2, v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->updateGestureRotation(FF)V

    .line 82
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->invalidate()V

    goto :goto_b9

    .line 88
    :pswitch_87
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->isDragDecelerationEnabled()Z

    move-result v0

    if-eqz v0, :cond_af

    .line 90
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->stopDeceleration()V

    .line 92
    invoke-direct {p0, v2, v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->sampleVelocity(FF)V

    .line 94
    invoke-direct {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->calculateVelocity()F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mDecelerationAngularVelocity:F

    .line 96
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mDecelerationAngularVelocity:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_af

    .line 97
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mDecelerationLastTime:J

    .line 99
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->postInvalidateOnAnimation(Landroid/view/View;)V

    .line 103
    :cond_af
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->enableScroll()V

    .line 104
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mTouchMode:I

    .line 110
    .end local v2    # "x":F
    .end local v3    # "y":F
    :cond_b9
    :goto_b9
    const/4 v0, 0x1

    return v0

    nop

    :pswitch_data_bc
    .packed-switch 0x0
        :pswitch_25
        :pswitch_87
        :pswitch_45
    .end packed-switch
.end method

.method public setGestureStartAngle(FF)V
    .registers 5
    .param p1, "x"    # F
    .param p2, "y"    # F

    .line 288
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    invoke-virtual {v0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getAngleForPoint(FF)F

    move-result v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getRawRotationAngle()F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mStartAngle:F

    .line 289
    return-void
.end method

.method public stopDeceleration()V
    .registers 2

    .line 306
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mDecelerationAngularVelocity:F

    .line 307
    return-void
.end method

.method public updateGestureRotation(FF)V
    .registers 6
    .param p1, "x"    # F
    .param p2, "y"    # F

    .line 299
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/Chart;

    check-cast v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;

    invoke-virtual {v1, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->getAngleForPoint(FF)F

    move-result v1

    iget v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/listener/PieRadarChartTouchListener;->mStartAngle:F

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/PieRadarChartBase;->setRotationAngle(F)V

    .line 300
    return-void
.end method

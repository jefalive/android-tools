.class public Lcom/itau/empresas/ui/util/analytics/EventoTempoRequest;
.super Ljava/lang/Object;
.source "EventoTempoRequest.java"


# instance fields
.field private categoria:Ljava/lang/String;

.field private tempo:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;)V
    .registers 3
    .param p1, "categoria"    # Ljava/lang/String;
    .param p2, "tempo"    # Ljava/lang/Long;

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/itau/empresas/ui/util/analytics/EventoTempoRequest;->categoria:Ljava/lang/String;

    .line 10
    iput-object p2, p0, Lcom/itau/empresas/ui/util/analytics/EventoTempoRequest;->tempo:Ljava/lang/Long;

    .line 11
    return-void
.end method


# virtual methods
.method public getCategoria()Ljava/lang/String;
    .registers 2

    .line 14
    iget-object v0, p0, Lcom/itau/empresas/ui/util/analytics/EventoTempoRequest;->categoria:Ljava/lang/String;

    return-object v0
.end method

.method public getTempo()Ljava/lang/Long;
    .registers 2

    .line 18
    iget-object v0, p0, Lcom/itau/empresas/ui/util/analytics/EventoTempoRequest;->tempo:Ljava/lang/Long;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 23
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EventoTempoRequest{categoria=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/util/analytics/EventoTempoRequest;->categoria:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tempo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/util/analytics/EventoTempoRequest;->tempo:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

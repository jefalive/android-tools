.class public Lcom/google/android/gms/analytics/internal/zzx;
.super Ljava/lang/Object;


# direct methods
.method public static version()I
    .registers 4

    :try_start_0
    sget-object v0, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_5} :catch_7

    move-result v2

    goto :goto_10

    :catch_7
    move-exception v3

    const-string v0, "Invalid version number"

    sget-object v1, Landroid/os/Build$VERSION;->SDK:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/analytics/internal/zzae;->zzf(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v2, 0x0

    :goto_10
    return v2
.end method

.method public static zzbo(Ljava/lang/String;)Z
    .registers 6

    const/16 v2, 0x9

    invoke-static {}, Lcom/google/android/gms/analytics/internal/zzx;->version()I

    move-result v0

    if-ge v0, v2, :cond_a

    const/4 v0, 0x0

    return v0

    :cond_a
    const/4 v3, 0x1

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Ljava/io/File;->setReadable(ZZ)Z

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Ljava/io/File;->setWritable(ZZ)Z

    const/4 v0, 0x1

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Ljava/io/File;->setReadable(ZZ)Z

    const/4 v0, 0x1

    const/4 v1, 0x1

    invoke-virtual {v4, v0, v1}, Ljava/io/File;->setWritable(ZZ)Z

    const/4 v0, 0x1

    return v0
.end method

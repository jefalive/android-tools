.class public Lcom/google/ads/conversiontracking/e;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ads/conversiontracking/e$b;,
        Lcom/google/ads/conversiontracking/e$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/google/ads/conversiontracking/d;>;"
        }
    .end annotation
.end field

.field private c:Landroid/content/Context;

.field private d:Z

.field private e:Z

.field private f:Lcom/google/ads/conversiontracking/f;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 16

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/ads/conversiontracking/e;->a:Ljava/lang/Object;

    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ads/conversiontracking/e;->d:Z

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ads/conversiontracking/e;->e:Z

    .line 44
    iput-object p1, p0, Lcom/google/ads/conversiontracking/e;->c:Landroid/content/Context;

    .line 45
    new-instance v0, Lcom/google/ads/conversiontracking/f;

    invoke-direct {v0, p1}, Lcom/google/ads/conversiontracking/f;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/ads/conversiontracking/e;->f:Lcom/google/ads/conversiontracking/f;

    .line 46
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/ads/conversiontracking/e;->b:Ljava/util/List;

    .line 47
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/ads/conversiontracking/e$b;

    invoke-direct {v1, p0}, Lcom/google/ads/conversiontracking/e$b;-><init>(Lcom/google/ads/conversiontracking/e;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 48
    new-instance v7, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const/4 v0, 0x1

    invoke-direct {v7, v0}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    .line 49
    invoke-static {p1}, Lcom/google/ads/conversiontracking/g;->b(Landroid/content/Context;)J

    move-result-wide v8

    .line 50
    invoke-static {}, Lcom/google/ads/conversiontracking/g;->a()J

    move-result-wide v10

    .line 51
    const-wide/32 v0, 0x493e0

    add-long/2addr v0, v8

    sub-long v12, v0, v10

    .line 52
    move-object v0, v7

    new-instance v1, Lcom/google/ads/conversiontracking/e$a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/ads/conversiontracking/e$a;-><init>(Lcom/google/ads/conversiontracking/e;Lcom/google/ads/conversiontracking/e$1;)V

    const-wide/16 v2, 0x0

    cmp-long v2, v12, v2

    if-lez v2, :cond_50

    .line 53
    move-wide v2, v12

    goto :goto_52

    :cond_50
    const-wide/16 v2, 0x0

    :goto_52
    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 52
    const-wide/32 v4, 0x493e0

    invoke-virtual/range {v0 .. v6}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 56
    return-void
.end method

.method static synthetic a(Lcom/google/ads/conversiontracking/e;)Ljava/lang/Object;
    .registers 2

    .line 20
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e;->a:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(Lcom/google/ads/conversiontracking/e;Z)Z
    .registers 2

    .line 20
    iput-boolean p1, p0, Lcom/google/ads/conversiontracking/e;->d:Z

    return p1
.end method

.method static synthetic b(Lcom/google/ads/conversiontracking/e;)Z
    .registers 2

    .line 20
    iget-boolean v0, p0, Lcom/google/ads/conversiontracking/e;->e:Z

    return v0
.end method

.method static synthetic b(Lcom/google/ads/conversiontracking/e;Z)Z
    .registers 2

    .line 20
    iput-boolean p1, p0, Lcom/google/ads/conversiontracking/e;->e:Z

    return p1
.end method

.method static synthetic c(Lcom/google/ads/conversiontracking/e;)Landroid/content/Context;
    .registers 2

    .line 20
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e;->c:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic d(Lcom/google/ads/conversiontracking/e;)Z
    .registers 2

    .line 20
    iget-boolean v0, p0, Lcom/google/ads/conversiontracking/e;->d:Z

    return v0
.end method

.method static synthetic e(Lcom/google/ads/conversiontracking/e;)Lcom/google/ads/conversiontracking/f;
    .registers 2

    .line 20
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e;->f:Lcom/google/ads/conversiontracking/f;

    return-object v0
.end method

.method static synthetic f(Lcom/google/ads/conversiontracking/e;)Ljava/util/List;
    .registers 2

    .line 20
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e;->b:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method protected a(Lcom/google/ads/conversiontracking/d;)I
    .registers 14

    .line 211
    const-string v0, "GoogleConversionReporter"

    const-string v2, "Pinging: "

    iget-object v1, p1, Lcom/google/ads/conversiontracking/d;->g:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_15

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1a

    :cond_15
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    :goto_1a
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    const/4 v4, 0x0

    .line 214
    iget-object v5, p1, Lcom/google/ads/conversiontracking/d;->g:Ljava/lang/String;

    .line 216
    const/4 v6, 0x0

    .line 217
    :goto_21
    const/4 v0, 0x5

    if-ge v6, v0, :cond_bc

    .line 219
    :try_start_24
    new-instance v7, Ljava/net/URL;

    invoke-direct {v7, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 220
    invoke-virtual {v7}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v4, v0

    .line 225
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 226
    const v0, 0xea60

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 227
    const v0, 0xea60

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 228
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 230
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v8

    .line 234
    const/16 v0, 0x12c

    if-gt v0, v8, :cond_70

    const/16 v0, 0x190

    if-ge v8, v0, :cond_70

    .line 235
    const-string v0, "Location"

    invoke-virtual {v4, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 236
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6a

    .line 237
    const-string v0, "GoogleConversionReporter"

    const-string v1, "Unable to follow redirect, no Location header."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_63
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_63} :catch_a2
    .catchall {:try_start_24 .. :try_end_63} :catchall_b1

    .line 238
    const/4 v9, 0x0

    .line 259
    if-eqz v4, :cond_69

    .line 260
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_69
    return v9

    .line 259
    :cond_6a
    if-eqz v4, :cond_b8

    .line 260
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_b8

    .line 243
    :cond_70
    const-string v0, "GoogleConversionReporter"

    :try_start_72
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x21

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Receive response code "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    const/16 v0, 0xc8

    if-gt v0, v8, :cond_94

    const/16 v0, 0x12c

    if-ge v8, v0, :cond_94

    const/4 v9, 0x2

    goto :goto_95

    :cond_94
    const/4 v9, 0x1

    .line 250
    :goto_95
    const/4 v0, 0x2

    if-ne v9, v0, :cond_9b

    .line 251
    invoke-virtual {p0, p1}, Lcom/google/ads/conversiontracking/e;->b(Lcom/google/ads/conversiontracking/d;)V
    :try_end_9b
    .catch Ljava/io/IOException; {:try_start_72 .. :try_end_9b} :catch_a2
    .catchall {:try_start_72 .. :try_end_9b} :catchall_b1

    .line 254
    :cond_9b
    move v10, v9

    .line 259
    if-eqz v4, :cond_a1

    .line 260
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_a1
    return v10

    .line 255
    :catch_a2
    move-exception v7

    .line 256
    const-string v0, "GoogleConversionReporter"

    const-string v1, "Error sending ping"

    :try_start_a7
    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_aa
    .catchall {:try_start_a7 .. :try_end_aa} :catchall_b1

    .line 257
    const/4 v8, 0x0

    .line 259
    if-eqz v4, :cond_b0

    .line 260
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_b0
    return v8

    .line 259
    :catchall_b1
    move-exception v11

    if-eqz v4, :cond_b7

    .line 260
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_b7
    throw v11

    .line 217
    :cond_b8
    :goto_b8
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_21

    .line 267
    :cond_bc
    const-string v0, "GoogleConversionReporter"

    const-string v1, "Ping failed; too many redirects."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    const/4 v0, 0x1

    return v0
.end method

.method protected a(Ljava/lang/Runnable;)V
    .registers 3

    .line 109
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 110
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/google/ads/conversiontracking/g$c;ZZZ)V
    .registers 10

    .line 86
    new-instance v1, Lcom/google/ads/conversiontracking/d;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/google/ads/conversiontracking/d;-><init>(Ljava/lang/String;Lcom/google/ads/conversiontracking/g$c;ZZ)V

    .line 88
    iget-object v2, p0, Lcom/google/ads/conversiontracking/e;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 89
    if-nez p5, :cond_14

    .line 90
    :try_start_a
    new-instance v0, Lcom/google/ads/conversiontracking/e$1;

    invoke-direct {v0, p0, v1}, Lcom/google/ads/conversiontracking/e$1;-><init>(Lcom/google/ads/conversiontracking/e;Lcom/google/ads/conversiontracking/d;)V

    invoke-virtual {p0, v0}, Lcom/google/ads/conversiontracking/e;->a(Ljava/lang/Runnable;)V
    :try_end_12
    .catchall {:try_start_a .. :try_end_12} :catchall_34

    .line 96
    monitor-exit v2

    return-void

    .line 99
    :cond_14
    :try_start_14
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e;->f:Lcom/google/ads/conversiontracking/f;

    invoke-virtual {v0, v1}, Lcom/google/ads/conversiontracking/f;->b(Lcom/google/ads/conversiontracking/d;)V

    .line 100
    iget-boolean v0, p0, Lcom/google/ads/conversiontracking/e;->e:Z

    if-eqz v0, :cond_32

    iget-object v0, p0, Lcom/google/ads/conversiontracking/e;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/ads/conversiontracking/g;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_32

    .line 101
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ads/conversiontracking/e;->d:Z

    .line 103
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V
    :try_end_32
    .catchall {:try_start_14 .. :try_end_32} :catchall_34

    .line 105
    :cond_32
    monitor-exit v2

    goto :goto_37

    :catchall_34
    move-exception v3

    monitor-exit v2

    throw v3

    .line 106
    :goto_37
    return-void
.end method

.method protected b(Lcom/google/ads/conversiontracking/d;)V
    .registers 5

    .line 276
    iget-boolean v0, p1, Lcom/google/ads/conversiontracking/d;->b:Z

    if-nez v0, :cond_11

    iget-boolean v0, p1, Lcom/google/ads/conversiontracking/d;->a:Z

    if-eqz v0, :cond_11

    .line 277
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e;->c:Landroid/content/Context;

    iget-object v1, p1, Lcom/google/ads/conversiontracking/d;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ads/conversiontracking/d;->f:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/ads/conversiontracking/g;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    :cond_11
    return-void
.end method

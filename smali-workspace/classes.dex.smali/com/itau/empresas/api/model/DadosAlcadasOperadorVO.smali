.class public Lcom/itau/empresas/api/model/DadosAlcadasOperadorVO;
.super Ljava/lang/Object;
.source "DadosAlcadasOperadorVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private alcadaDupla:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "alcada_dupla"
    .end annotation
.end field

.field private alcadaSimples:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "alcada_simples"
    .end annotation
.end field

.field private indicadorAdmPerfil:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_adm_perfil"
    .end annotation
.end field

.field private indicadorNecessitaAprovacao:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_necessita_aprovacao"
    .end annotation
.end field

.field private indicadorNomesSalarios:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_nomes_salarios"
    .end annotation
.end field

.field private indicadorProdSalarios:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_prod_salarios"
    .end annotation
.end field

.field private indicadorValoresSalarios:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_valores_salarios"
    .end annotation
.end field

.field private valorAlcadaDupla:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_alcada_dupla"
    .end annotation
.end field

.field private valorAlcadaSimples:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_alcada_simples"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

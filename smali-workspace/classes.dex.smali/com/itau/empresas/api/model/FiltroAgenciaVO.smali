.class public Lcom/itau/empresas/api/model/FiltroAgenciaVO;
.super Ljava/lang/Object;
.source "FiltroAgenciaVO.java"


# instance fields
.field private filtroCaixas:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "filtroCaixas"
    .end annotation
.end field

.field private filtroCaixasExc:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "filtroCaixasExc"
    .end annotation
.end field

.field private filtroDisp:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "filtroDisp"
    .end annotation
.end field

.field private filtroEmpresas:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "filtroEmpresas"
    .end annotation
.end field

.field private filtroHAbertura:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "filtroHAbertura"
    .end annotation
.end field

.field private filtroHFechamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "filtroHFechamento"
    .end annotation
.end field

.field private filtroHorarios:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "filtroHorarios"
    .end annotation
.end field

.field private filtroItau:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "filtroItau"
    .end annotation
.end field

.field private filtroPers:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "filtroPers"
    .end annotation
.end field

.field private filtroUniclass:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "filtroUniclass"
    .end annotation
.end field

.field private lati:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "lati"
    .end annotation
.end field

.field private longi:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "longi"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-string v0, "false"

    invoke-virtual {p0, v0}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->setFiltroCaixas(Ljava/lang/String;)V

    .line 34
    const-string v0, "false"

    invoke-virtual {p0, v0}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->setFiltroCaixasExc(Ljava/lang/String;)V

    .line 35
    const-string v0, "false"

    invoke-virtual {p0, v0}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->setFiltroDisp(Ljava/lang/String;)V

    .line 36
    const-string v0, "true"

    invoke-virtual {p0, v0}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->setFiltroEmpresas(Ljava/lang/String;)V

    .line 37
    const-string v0, "00"

    invoke-virtual {p0, v0}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->setFiltroHAbertura(Ljava/lang/String;)V

    .line 38
    const-string v0, "23"

    invoke-virtual {p0, v0}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->setFiltroHFechamento(Ljava/lang/String;)V

    .line 39
    const-string v0, "false"

    invoke-virtual {p0, v0}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->setFiltroHorarios(Ljava/lang/String;)V

    .line 40
    const-string v0, "true"

    invoke-virtual {p0, v0}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->setFiltroPers(Ljava/lang/String;)V

    .line 41
    const-string v0, "false"

    invoke-virtual {p0, v0}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->setFiltroUniclass(Ljava/lang/String;)V

    .line 42
    const-string v0, "true"

    invoke-virtual {p0, v0}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->setFiltroItau(Ljava/lang/String;)V

    .line 43
    const-string v0, "-23.5505199"

    invoke-virtual {p0, v0}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->setLati(Ljava/lang/String;)V

    .line 44
    const-string v0, "-46.6333094"

    invoke-virtual {p0, v0}, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->setLongi(Ljava/lang/String;)V

    .line 45
    return-void
.end method


# virtual methods
.method public getFiltroCaixas()Z
    .registers 2

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->filtroCaixas:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getFiltroCaixasExc()Z
    .registers 2

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->filtroCaixasExc:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getFiltroEmpresas()Z
    .registers 2

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->filtroEmpresas:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getFiltroItau()Z
    .registers 2

    .line 108
    iget-object v0, p0, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->filtroItau:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public getFiltroPers()Z
    .registers 2

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->filtroPers:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public setFiltroCaixas(Ljava/lang/String;)V
    .registers 2
    .param p1, "filtroCaixas"    # Ljava/lang/String;

    .line 52
    iput-object p1, p0, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->filtroCaixas:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public setFiltroCaixasExc(Ljava/lang/String;)V
    .registers 2
    .param p1, "filtroCaixasExc"    # Ljava/lang/String;

    .line 60
    iput-object p1, p0, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->filtroCaixasExc:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public setFiltroDisp(Ljava/lang/String;)V
    .registers 2
    .param p1, "filtroDisp"    # Ljava/lang/String;

    .line 64
    iput-object p1, p0, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->filtroDisp:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public setFiltroEmpresas(Ljava/lang/String;)V
    .registers 2
    .param p1, "filtroEmpresas"    # Ljava/lang/String;

    .line 72
    iput-object p1, p0, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->filtroEmpresas:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public setFiltroHAbertura(Ljava/lang/String;)V
    .registers 2
    .param p1, "filtroHAbertura"    # Ljava/lang/String;

    .line 76
    iput-object p1, p0, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->filtroHAbertura:Ljava/lang/String;

    .line 77
    return-void
.end method

.method public setFiltroHFechamento(Ljava/lang/String;)V
    .registers 2
    .param p1, "filtroHFechamento"    # Ljava/lang/String;

    .line 80
    iput-object p1, p0, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->filtroHFechamento:Ljava/lang/String;

    .line 81
    return-void
.end method

.method public setFiltroHorarios(Ljava/lang/String;)V
    .registers 2
    .param p1, "filtroHorarios"    # Ljava/lang/String;

    .line 84
    iput-object p1, p0, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->filtroHorarios:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public setFiltroItau(Ljava/lang/String;)V
    .registers 2
    .param p1, "filtroItau"    # Ljava/lang/String;

    .line 112
    iput-object p1, p0, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->filtroItau:Ljava/lang/String;

    .line 113
    return-void
.end method

.method public setFiltroPers(Ljava/lang/String;)V
    .registers 2
    .param p1, "filtroPers"    # Ljava/lang/String;

    .line 92
    iput-object p1, p0, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->filtroPers:Ljava/lang/String;

    .line 93
    return-void
.end method

.method public setFiltroUniclass(Ljava/lang/String;)V
    .registers 2
    .param p1, "filtroUniclass"    # Ljava/lang/String;

    .line 96
    iput-object p1, p0, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->filtroUniclass:Ljava/lang/String;

    .line 97
    return-void
.end method

.method public setLati(Ljava/lang/String;)V
    .registers 2
    .param p1, "lati"    # Ljava/lang/String;

    .line 100
    iput-object p1, p0, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->lati:Ljava/lang/String;

    .line 101
    return-void
.end method

.method public setLongi(Ljava/lang/String;)V
    .registers 2
    .param p1, "longi"    # Ljava/lang/String;

    .line 104
    iput-object p1, p0, Lcom/itau/empresas/api/model/FiltroAgenciaVO;->longi:Ljava/lang/String;

    .line 105
    return-void
.end method

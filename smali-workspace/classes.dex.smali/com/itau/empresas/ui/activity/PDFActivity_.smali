.class public final Lcom/itau/empresas/ui/activity/PDFActivity_;
.super Lcom/itau/empresas/ui/activity/PDFActivity;
.source "PDFActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/activity/PDFActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 31
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/PDFActivity;-><init>()V

    .line 35
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/PDFActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/ui/activity/PDFActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/PDFActivity_;

    .line 31
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/PDFActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/ui/activity/PDFActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/PDFActivity_;

    .line 31
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/PDFActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 48
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/PDFActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 49
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 50
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/PDFActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 51
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/PDFActivity_;->injectExtras_()V

    .line 52
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/PDFActivity_;->afterInject()V

    .line 53
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 92
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/PDFActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 93
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_1a

    .line 94
    const-string v0, "url"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 95
    const-string v0, "url"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/PDFActivity_;->url:Ljava/lang/String;

    .line 98
    :cond_1a
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 123
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/ui/activity/PDFActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/PDFActivity_$2;-><init>(Lcom/itau/empresas/ui/activity/PDFActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 131
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 111
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/ui/activity/PDFActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/PDFActivity_$1;-><init>(Lcom/itau/empresas/ui/activity/PDFActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 119
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 40
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PDFActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 41
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/activity/PDFActivity_;->init_(Landroid/os/Bundle;)V

    .line 42
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/PDFActivity;->onCreate(Landroid/os/Bundle;)V

    .line 43
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 44
    const v0, 0x7f030056

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/PDFActivity_;->setContentView(I)V

    .line 45
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 87
    const v0, 0x7f0e0292

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/PDFActivity_;->webView:Landroid/webkit/WebView;

    .line 88
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/PDFActivity_;->aoCarregarTela()V

    .line 89
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 57
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/PDFActivity;->setContentView(I)V

    .line 58
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PDFActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 59
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 69
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/PDFActivity;->setContentView(Landroid/view/View;)V

    .line 70
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PDFActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 71
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 63
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/ui/activity/PDFActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 64
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/PDFActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 65
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 102
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/PDFActivity;->setIntent(Landroid/content/Intent;)V

    .line 103
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/PDFActivity_;->injectExtras_()V

    .line 104
    return-void
.end method

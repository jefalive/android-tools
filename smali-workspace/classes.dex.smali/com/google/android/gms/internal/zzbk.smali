.class public Lcom/google/android/gms/internal/zzbk;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/zzbk$zza;
    }
.end annotation


# direct methods
.method static zza(IIJJJ)J
    .registers 16

    int-to-long v0, p0

    const-wide/32 v2, 0x7fffffff

    add-long/2addr v0, v2

    const-wide/32 v2, 0x4000ffff

    rem-long/2addr v0, v2

    mul-long/2addr v0, p4

    const-wide/32 v2, 0x4000ffff

    rem-long v4, v0, v2

    const-wide/32 v0, 0x4000ffff

    add-long/2addr v0, p2

    sub-long/2addr v0, v4

    const-wide/32 v2, 0x4000ffff

    rem-long p2, v0, v2

    mul-long v0, p2, p6

    const-wide/32 v2, 0x4000ffff

    rem-long p2, v0, v2

    int-to-long v0, p1

    const-wide/32 v2, 0x7fffffff

    add-long/2addr v0, v2

    const-wide/32 v2, 0x4000ffff

    rem-long v6, v0, v2

    add-long/2addr p2, v6

    const-wide/32 v0, 0x4000ffff

    rem-long/2addr p2, v0

    return-wide p2
.end method

.method static zza(JI)J
    .registers 7

    if-nez p2, :cond_5

    const-wide/16 v0, 0x1

    return-wide v0

    :cond_5
    const/4 v0, 0x1

    if-ne p2, v0, :cond_9

    return-wide p0

    :cond_9
    rem-int/lit8 v0, p2, 0x2

    if-nez v0, :cond_1e

    mul-long v0, p0, p0

    const-wide/32 v2, 0x4000ffff

    rem-long/2addr v0, v2

    div-int/lit8 v2, p2, 0x2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/zzbk;->zza(JI)J

    move-result-wide v0

    const-wide/32 v2, 0x4000ffff

    rem-long/2addr v0, v2

    return-wide v0

    :cond_1e
    mul-long v0, p0, p0

    const-wide/32 v2, 0x4000ffff

    rem-long/2addr v0, v2

    div-int/lit8 v2, p2, 0x2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/zzbk;->zza(JI)J

    move-result-wide v0

    const-wide/32 v2, 0x4000ffff

    rem-long/2addr v0, v2

    mul-long/2addr v0, p0

    const-wide/32 v2, 0x4000ffff

    rem-long/2addr v0, v2

    return-wide v0
.end method

.method static zza([Ljava/lang/String;II)Ljava/lang/String;
    .registers 7

    array-length v0, p0

    add-int v1, p1, p2

    if-ge v0, v1, :cond_d

    const-string v0, "Unable to construct shingle"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->e(Ljava/lang/String;)V

    const-string v0, ""

    return-object v0

    :cond_d
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    move v3, p1

    :goto_13
    add-int v0, p1, p2

    add-int/lit8 v0, v0, -0x1

    if-ge v3, v0, :cond_26

    aget-object v0, p0, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v0, 0x20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v3, v3, 0x1

    goto :goto_13

    :cond_26
    add-int v0, p1, p2

    add-int/lit8 v0, v0, -0x1

    aget-object v0, p0, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static zza(IJLjava/lang/String;ILjava/util/PriorityQueue;)V
    .registers 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJLjava/lang/String;ILjava/util/PriorityQueue<Lcom/google/android/gms/internal/zzbk$zza;>;)V"
        }
    .end annotation

    new-instance v4, Lcom/google/android/gms/internal/zzbk$zza;

    invoke-direct {v4, p1, p2, p3, p4}, Lcom/google/android/gms/internal/zzbk$zza;-><init>(JLjava/lang/String;I)V

    invoke-virtual {p5}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    if-ne v0, p0, :cond_1a

    invoke-virtual {p5}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/zzbk$zza;

    iget-wide v0, v0, Lcom/google/android/gms/internal/zzbk$zza;->value:J

    iget-wide v2, v4, Lcom/google/android/gms/internal/zzbk$zza;->value:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1a

    return-void

    :cond_1a
    invoke-virtual {p5, v4}, Ljava/util/PriorityQueue;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    return-void

    :cond_21
    invoke-virtual {p5, v4}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    invoke-virtual {p5}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    if-le v0, p0, :cond_2d

    invoke-virtual {p5}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    :cond_2d
    return-void
.end method

.method public static zza([Ljava/lang/String;IILjava/util/PriorityQueue;)V
    .registers 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([Ljava/lang/String;IILjava/util/PriorityQueue<Lcom/google/android/gms/internal/zzbk$zza;>;)V"
        }
    .end annotation

    array-length v0, p0

    if-ge v0, p2, :cond_18

    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lcom/google/android/gms/internal/zzbk;->zzb([Ljava/lang/String;II)J

    move-result-wide v8

    move v0, p1

    move-wide v1, v8

    array-length v3, p0

    const/4 v4, 0x0

    invoke-static {p0, v4, v3}, Lcom/google/android/gms/internal/zzbk;->zza([Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v3

    array-length v4, p0

    move-object/from16 v5, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/internal/zzbk;->zza(IJLjava/lang/String;ILjava/util/PriorityQueue;)V

    return-void

    :cond_18
    const/4 v0, 0x0

    invoke-static {p0, v0, p2}, Lcom/google/android/gms/internal/zzbk;->zzb([Ljava/lang/String;II)J

    move-result-wide v8

    move v0, p1

    move-wide v1, v8

    const/4 v3, 0x0

    invoke-static {p0, v3, p2}, Lcom/google/android/gms/internal/zzbk;->zza([Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v3

    move v4, p2

    move-object/from16 v5, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/internal/zzbk;->zza(IJLjava/lang/String;ILjava/util/PriorityQueue;)V

    add-int/lit8 v0, p2, -0x1

    const-wide/32 v1, 0x1001fff

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/internal/zzbk;->zza(JI)J

    move-result-wide v10

    const/4 v12, 0x1

    :goto_34
    array-length v0, p0

    sub-int/2addr v0, p2

    add-int/lit8 v0, v0, 0x1

    if-ge v12, v0, :cond_64

    add-int/lit8 v0, v12, -0x1

    aget-object v0, p0, v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzbi;->zzx(Ljava/lang/String;)I

    move-result v0

    add-int v1, v12, p2

    add-int/lit8 v1, v1, -0x1

    aget-object v1, p0, v1

    invoke-static {v1}, Lcom/google/android/gms/internal/zzbi;->zzx(Ljava/lang/String;)I

    move-result v1

    move-wide v2, v8

    move-wide v4, v10

    const-wide/32 v6, 0x1001fff

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/internal/zzbk;->zza(IIJJJ)J

    move-result-wide v8

    move v0, p1

    move-wide v1, v8

    invoke-static {p0, v12, p2}, Lcom/google/android/gms/internal/zzbk;->zza([Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v3

    array-length v4, p0

    move-object/from16 v5, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/internal/zzbk;->zza(IJLjava/lang/String;ILjava/util/PriorityQueue;)V

    add-int/lit8 v12, v12, 0x1

    goto :goto_34

    :cond_64
    return-void
.end method

.method private static zzb([Ljava/lang/String;II)J
    .registers 10

    aget-object v0, p0, p1

    invoke-static {v0}, Lcom/google/android/gms/internal/zzbi;->zzx(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x7fffffff

    add-long/2addr v0, v2

    const-wide/32 v2, 0x4000ffff

    rem-long v4, v0, v2

    add-int/lit8 v6, p1, 0x1

    :goto_12
    add-int v0, p1, p2

    if-ge v6, v0, :cond_36

    const-wide/32 v0, 0x1001fff

    mul-long/2addr v0, v4

    const-wide/32 v2, 0x4000ffff

    rem-long v4, v0, v2

    aget-object v0, p0, v6

    invoke-static {v0}, Lcom/google/android/gms/internal/zzbi;->zzx(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x7fffffff

    add-long/2addr v0, v2

    const-wide/32 v2, 0x4000ffff

    rem-long/2addr v0, v2

    add-long/2addr v4, v0

    const-wide/32 v0, 0x4000ffff

    rem-long/2addr v4, v0

    add-int/lit8 v6, v6, 0x1

    goto :goto_12

    :cond_36
    return-wide v4
.end method

.class public final Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;
.super Ljava/lang/Object;


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:I


# direct methods
.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lbr/com/itau/sdk/android/leitorboleto/R$layout;->zxing_capture:I

    iput v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->a:I

    sget v0, Lbr/com/itau/sdk/android/leitorboleto/R$id;->zxing_back_button:I

    iput v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->b:I

    sget v0, Lbr/com/itau/sdk/android/leitorboleto/R$id;->button_flash:I

    iput v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->c:I

    sget v0, Lbr/com/itau/sdk/android/leitorboleto/R$id;->zxing_preview_view:I

    iput v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->d:I

    sget v0, Lbr/com/itau/sdk/android/leitorboleto/R$id;->zxing_viewfinder_view:I

    iput v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->e:I

    const-string v0, "Erro"

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->i:Ljava/lang/String;

    const-string v0, "Leitor"

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->j:Ljava/lang/String;

    const-string v0, "O leitor est\u00e1 com dificuldade em reconhecer o c\u00f3digo de barras."

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->k:Ljava/lang/String;

    const-string v0, "Dificuldade para ler"

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->l:Ljava/lang/String;

    const-string v0, "Qualidade da c\u00e2mera"

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->m:Ljava/lang/String;

    const-string v0, "Recomendamos c\u00e2meras com foco cont\u00ednuo e qualidade de v\u00eddeo superior para utilizar este recurso."

    iput-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->n:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->o:I

    return-void
.end method


# virtual methods
.method public getBackButtonId()I
    .registers 2

    iget v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->b:I

    return v0
.end method

.method public getBadCameraMessage()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->n:Ljava/lang/String;

    return-object v0
.end method

.method public getBadCameraTitle()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->m:Ljava/lang/String;

    return-object v0
.end method

.method public getDialogError()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->i:Ljava/lang/String;

    return-object v0
.end method

.method public getDialogStyle()I
    .registers 2

    iget v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->o:I

    return v0
.end method

.method public getDialogTitle()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->j:Ljava/lang/String;

    return-object v0
.end method

.method public getFlashButtonId()I
    .registers 2

    iget v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->c:I

    return v0
.end method

.method public getHardToReadMessage()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->k:Ljava/lang/String;

    return-object v0
.end method

.method public getHardToReadTitle()Ljava/lang/String;
    .registers 2

    iget-object v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->l:Ljava/lang/String;

    return-object v0
.end method

.method public getLayoutId()I
    .registers 2

    iget v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->a:I

    return v0
.end method

.method public getSurfaceViewId()I
    .registers 2

    iget v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->d:I

    return v0
.end method

.method public getViewFinderId()I
    .registers 2

    iget v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->e:I

    return v0
.end method

.method public isBeepEnabled()Z
    .registers 2

    iget-boolean v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->f:Z

    return v0
.end method

.method public isDrawResultBitmap()Z
    .registers 2

    iget-boolean v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->h:Z

    return v0
.end method

.method public isVibrateEnabled()Z
    .registers 2

    iget-boolean v0, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->g:Z

    return v0
.end method

.method public withDialogError(Ljava/lang/String;)Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;
    .registers 2

    iput-object p1, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->i:Ljava/lang/String;

    return-object p0
.end method

.method public withDialogTitle(Ljava/lang/String;)Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;
    .registers 2

    iput-object p1, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->j:Ljava/lang/String;

    return-object p0
.end method

.method public withLayoutId(I)Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;
    .registers 2

    iput p1, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->a:I

    return-object p0
.end method

.method public withSurfaceViewId(I)Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;
    .registers 2

    iput p1, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->d:I

    return-object p0
.end method

.method public withViewFinderId(I)Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;
    .registers 2

    iput p1, p0, Lbr/com/itau/sdk/android/leitorboleto/ui/utils/ReaderState;->e:I

    return-object p0
.end method

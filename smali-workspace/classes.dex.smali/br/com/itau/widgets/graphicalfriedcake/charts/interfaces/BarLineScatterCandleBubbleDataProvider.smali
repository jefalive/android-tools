.class public interface abstract Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/BarLineScatterCandleBubbleDataProvider;
.super Ljava/lang/Object;
.source "BarLineScatterCandleBubbleDataProvider.java"

# interfaces
.implements Lbr/com/itau/widgets/graphicalfriedcake/charts/interfaces/ChartInterface;


# virtual methods
.method public abstract getData()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarLineScatterCandleData;
.end method

.method public abstract getHighestVisibleXIndex()I
.end method

.method public abstract getLowestVisibleXIndex()I
.end method

.method public abstract getMaxVisibleCount()I
.end method

.method public abstract getTransformer(Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;)Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Transformer;
.end method

.method public abstract isInverted(Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis$AxisDependency;)Z
.end method

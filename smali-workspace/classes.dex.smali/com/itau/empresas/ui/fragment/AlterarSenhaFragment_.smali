.class public final Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;
.super Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;
.source "AlterarSenhaFragment_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private contentView_:Landroid/view/View;

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 28
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;-><init>()V

    .line 32
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 75
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 76
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->injectFragmentArguments_()V

    .line 77
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->application:Lcom/itau/empresas/CustomApplication;

    .line 78
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/TrocaSenhaController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/controller/TrocaSenhaController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->trocaSenhaController:Lcom/itau/empresas/controller/TrocaSenhaController;

    .line 79
    return-void
.end method

.method private injectFragmentArguments_()V
    .registers 3

    .line 176
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 177
    .local v1, "args_":Landroid/os/Bundle;
    if-eqz v1, :cond_2a

    .line 178
    const-string v0, "trocaObrigatoria"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 179
    const-string v0, "trocaObrigatoria"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->trocaObrigatoria:Ljava/lang/Boolean;

    .line 181
    :cond_18
    const-string v0, "primeiroAcesso"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 182
    const-string v0, "primeiroAcesso"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->primeiroAcesso:Ljava/lang/Boolean;

    .line 185
    :cond_2a
    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .registers 3
    .param p1, "id"    # I

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_6

    .line 48
    const/4 v0, 0x0

    return-object v0

    .line 50
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->contentView_:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 39
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 40
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->init_(Landroid/os/Bundle;)V

    .line 41
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->onCreate(Landroid/os/Bundle;)V

    .line 42
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 43
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 55
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->contentView_:Landroid/view/View;

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_14

    .line 57
    const v0, 0x7f030094

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->contentView_:Landroid/view/View;

    .line 59
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->contentView_:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .registers 2

    .line 64
    invoke-super {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->onDestroyView()V

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->contentView_:Landroid/view/View;

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->campoCodigoOperador:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->campoSenhaAtual:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->campoNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->campoConfirmarNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->botaoContinuar:Landroid/widget/Button;

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->viewEspacoSemCampoSenhaAtual:Landroid/view/View;

    .line 72
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 5
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 93
    const v0, 0x7f0e03ea

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/material/MaterialEditText;

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->campoCodigoOperador:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 94
    const v0, 0x7f0e03eb

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/material/MaterialEditText;

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->campoSenhaAtual:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 95
    const v0, 0x7f0e03ec

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/material/MaterialEditText;

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->campoNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 96
    const v0, 0x7f0e03ed

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/material/MaterialEditText;

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->campoConfirmarNovaSenha:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 97
    const v0, 0x7f0e018f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->botaoContinuar:Landroid/widget/Button;

    .line 98
    const v0, 0x7f0e03ee

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->viewEspacoSemCampoSenhaAtual:Landroid/view/View;

    .line 99
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->botaoContinuar:Landroid/widget/Button;

    if-eqz v0, :cond_4e

    .line 100
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->botaoContinuar:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_$1;-><init>(Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    :cond_4e
    const v0, 0x7f0e03eb

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/TextView;

    .line 111
    .local v2, "view":Landroid/widget/TextView;
    if-eqz v2, :cond_62

    .line 112
    new-instance v0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_$2;-><init>(Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;)V

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 131
    .end local v2    # "view":Landroid/widget/TextView;
    :cond_62
    const v0, 0x7f0e03ec

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/TextView;

    .line 132
    .local v2, "view":Landroid/widget/TextView;
    if-eqz v2, :cond_76

    .line 133
    new-instance v0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_$3;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_$3;-><init>(Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;)V

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 152
    .end local v2    # "view":Landroid/widget/TextView;
    :cond_76
    const v0, 0x7f0e03ed

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/TextView;

    .line 153
    .local v2, "view":Landroid/widget/TextView;
    if-eqz v2, :cond_8a

    .line 154
    new-instance v0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_$4;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_$4;-><init>(Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;)V

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 172
    .end local v2    # "view":Landroid/widget/TextView;
    :cond_8a
    invoke-virtual {p0}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->aoIniciarActivity()V

    .line 173
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 83
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 84
    iget-object v0, p0, Lcom/itau/empresas/ui/fragment/AlterarSenhaFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 85
    return-void
.end method

.class public Lcom/itau/empresas/api/model/ConsultarLimiteCarrinhoVO;
.super Ljava/lang/Object;
.source "ConsultarLimiteCarrinhoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private msg:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "msg"
    .end annotation
.end field

.field private taxaJurosMensal:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros_mensal"
    .end annotation
.end field

.field private valorAgencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_agencia"
    .end annotation
.end field

.field private valorAgendaComTrava:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_agenda_com_trava"
    .end annotation
.end field

.field private valorAntecipacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_antecipacao"
    .end annotation
.end field

.field private valorDisponivel:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_disponivel"
    .end annotation
.end field

.field private valorLimiteConvenio:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_limite_convenio"
    .end annotation
.end field

.field private valorMaximo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_maximo"
    .end annotation
.end field

.field private valorMaximoOperacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_maximo_operacao"
    .end annotation
.end field

.field private valorMinimo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_minimo"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

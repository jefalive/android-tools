.class Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$2;
.super Ljava/lang/Object;
.source "HomeSearchActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;


# direct methods
.method constructor <init>(Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;)V
    .registers 2
    .param p1, "this$0"    # Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    .line 363
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$2;->this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 5
    .param p1, "v"    # Landroid/view/View;

    .line 365
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lbr/com/itau/textovoz/R$id;->search_voice_btn:I

    if-ne v0, v1, :cond_41

    .line 366
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$2;->this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    .line 367
    invoke-static {}, Lbr/com/itau/textovoz/util/PermissionUtils;->getPermissionRecordAudio()[Ljava/lang/String;

    move-result-object v1

    .line 366
    invoke-static {v0, v1}, Lbr/com/itau/textovoz/util/PermissionUtils;->hasSelfPermissions(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3c

    .line 368
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$2;->this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    .line 369
    invoke-static {}, Lbr/com/itau/textovoz/util/PermissionUtils;->getPermissionRecordAudio()[Ljava/lang/String;

    move-result-object v1

    .line 368
    invoke-static {v0, v1}, Lbr/com/itau/textovoz/util/PermissionUtils;->shouldShowRequestPermissionRationale(Landroid/app/Activity;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 370
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$2;->this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$2;->this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    invoke-virtual {v1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$string;->text_relational_permission:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->showRationaleMessage(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->access$100(Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;Ljava/lang/String;)V

    .line 373
    :cond_31
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$2;->this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    .line 374
    invoke-static {}, Lbr/com/itau/textovoz/util/PermissionUtils;->getPermissionRecordAudio()[Ljava/lang/String;

    move-result-object v1

    .line 373
    const/4 v2, 0x6

    invoke-static {v0, v1, v2}, Landroid/support/v4/app/ActivityCompat;->requestPermissions(Landroid/app/Activity;[Ljava/lang/String;I)V

    goto :goto_41

    .line 376
    :cond_3c
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$2;->this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    # invokes: Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->openSearchVoice()V
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->access$200(Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;)V

    .line 379
    :cond_41
    :goto_41
    return-void
.end method

.class public Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;
.super Lcom/itau/empresas/ui/fragment/BaseFragment;
.source "RecargaNovoNumeroFragment.java"


# instance fields
.field application:Lcom/itau/empresas/CustomApplication;

.field botaoAgenda:Landroid/widget/ImageButton;

.field botaoAutorizarRecarga:Landroid/widget/Button;

.field botaoFavorito:Landroid/widget/ImageButton;

.field botaoIncluirRecarga:Landroid/widget/Button;

.field campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

.field campoNome:Lbr/com/itau/widgets/material/MaterialEditText;

.field private favorecidoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

.field private fechouHintComClique:Z

.field private hintUtils:Lcom/itau/empresas/ui/util/HintUtils;

.field llRadioValor:Landroid/widget/LinearLayout;

.field llRegiao:Landroid/widget/LinearLayout;

.field private novoContatoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

.field recargaController:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

.field private recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

.field spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

.field spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

.field valorRecarga:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 80
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;)Z
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;

    .line 80
    iget-boolean v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->fechouHintComClique:Z

    return v0
.end method

.method static synthetic access$002(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;Z)Z
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;
    .param p1, "x1"    # Z

    .line 80
    iput-boolean p1, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->fechouHintComClique:Z

    return p1
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;)Lcom/itau/empresas/ui/util/HintUtils;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->hintUtils:Lcom/itau/empresas/ui/util/HintUtils;

    return-object v0
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;Z)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;
    .param p1, "x1"    # Z

    .line 80
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->simularRecarga(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;
    .param p1, "x1"    # Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    .line 80
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->ajustaDadosDialog(Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    return-void
.end method

.method private adicionaHint(Landroid/view/View;)V
    .registers 6
    .param p1, "view"    # Landroid/view/View;

    .line 178
    new-instance v0, Lbr/com/itau/widgets/hintview/Hint$Builder;

    const v1, 0x7f09011b

    invoke-direct {v0, p1, v1}, Lbr/com/itau/widgets/hintview/Hint$Builder;-><init>(Landroid/view/View;I)V

    .line 179
    const v1, 0x7f07035a

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 178
    const/16 v2, 0x50

    invoke-static {v0, v1, v2}, Lcom/itau/empresas/ui/util/HintUtils;->montaHintAlerta(Lbr/com/itau/widgets/hintview/Hint$Builder;Ljava/lang/String;I)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v3

    .line 181
    .local v3, "builder":Lbr/com/itau/widgets/hintview/Hint$Builder;
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->fechouHintComClique:Z

    .line 183
    new-instance v0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment$2;-><init>(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;)V

    .line 184
    invoke-virtual {v3, v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setOnClickListener(Lbr/com/itau/widgets/hintview/OnClickListener;)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment$1;-><init>(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;)V

    .line 193
    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setOnDismissListener(Lbr/com/itau/widgets/hintview/OnDismissListener;)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    .line 210
    invoke-virtual {v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->show()Lbr/com/itau/widgets/hintview/Hint;

    .line 211
    return-void
.end method

.method private ajustaDadosDialog(Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V
    .registers 12
    .param p1, "dialog"    # Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    .line 572
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getCelularSemMascara()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 573
    .local v3, "ddd":Ljava/lang/String;
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getCelularSemMascara()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 575
    .line 576
    .local v4, "numero":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3, v4}, Lcom/itau/empresas/ui/util/ViewUtils;->formataNumeroTelefonico(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 577
    .local v5, "telefoneFormatado":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v5, v0, v1

    const v1, 0x7f0701ab

    invoke-virtual {p0, v1, v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 578
    .local v6, "mensagemDialogo":Ljava/lang/String;
    new-instance v7, Landroid/text/SpannableString;

    invoke-direct {v7, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 579
    .local v7, "spannableString":Landroid/text/SpannableString;
    const-string v0, "n\u00e3o"

    invoke-virtual {v6, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v8

    .line 580
    .local v8, "indexOfNao":I
    invoke-virtual {v6, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    .line 581
    .local v9, "indexOfTelefoneFormatado":I
    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    const-string v1, "n\u00e3o"

    .line 583
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v8

    .line 582
    const/4 v2, 0x0

    invoke-virtual {v7, v0, v8, v1, v2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 584
    new-instance v0, Landroid/text/style/StyleSpan;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 585
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v9

    .line 584
    const/4 v2, 0x0

    invoke-virtual {v7, v0, v9, v1, v2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 587
    invoke-virtual {p1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->getTextoMensagem()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 588
    return-void
.end method

.method private buscaDetalheOperadorasRecarga(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "nomeOperadora"    # Ljava/lang/String;
    .param p2, "nomeRegiao"    # Ljava/lang/String;

    .line 330
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->novoContatoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->novoContatoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 331
    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getPositionValorOperadora()I

    move-result v0

    if-ltz v0, :cond_18

    .line 332
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->valorRecarga:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->novoContatoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 333
    invoke-virtual {v1}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getPositionValorOperadora()I

    move-result v1

    .line 332
    invoke-virtual {v0, p1, p2, v1}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->carregarValores(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1d

    .line 335
    :cond_18
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->valorRecarga:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;

    invoke-virtual {v0, p1, p2}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->carregarValores(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    :goto_1d
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->rehabilitarBotaoIncluirAutorizar(Z)V

    .line 338
    return-void
.end method

.method private buscaOperadorasRecarga()V
    .registers 3

    .line 287
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/SnackbarUtils;->tentaDispensarSnackbarNo(Ljava/lang/Object;)V

    .line 288
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->baseActivity()Lcom/itau/empresas/ui/activity/BaseActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->mostraDialogoDeProgresso()V

    .line 290
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->recargaController:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    const-string v1, "recargas"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->buscaOperadorasRecarga(Ljava/lang/String;)V

    .line 292
    return-void
.end method

.method private confirmarSimulacao(Z)V
    .registers 5
    .param p1, "autorizante"    # Z

    .line 537
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->isValido()Z

    move-result v0

    if-nez v0, :cond_7

    .line 538
    return-void

    .line 541
    :cond_7
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->DOIS_BOTOES:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 542
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 543
    const v1, 0x7f0704d2

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoPositivo(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 544
    const v1, 0x7f070479

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNegativo(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 545
    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v2

    .line 547
    .local v2, "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    new-instance v0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment$3;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment$3;-><init>(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNegativoListener(Landroid/view/View$OnClickListener;)V

    .line 553
    new-instance v0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment$4;

    invoke-direct {v0, p0, v2, p1}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment$4;-><init>(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;Z)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setPositivoListener(Landroid/view/View$OnClickListener;)V

    .line 561
    new-instance v0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment$5;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment$5;-><init>(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 568
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 569
    return-void
.end method

.method private criarRecargaInputVO()Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;
    .registers 7

    .line 341
    new-instance v1, Lcom/itau/empresas/api/model/DadosClienteVO;

    invoke-direct {v1}, Lcom/itau/empresas/api/model/DadosClienteVO;-><init>()V

    .line 342
    .local v1, "dadosClienteVO":Lcom/itau/empresas/api/model/DadosClienteVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/itau/empresas/api/model/DadosClienteVO;->setAgencia(Ljava/lang/String;)V

    .line 343
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/itau/empresas/api/model/DadosClienteVO;->setConta(Ljava/lang/String;)V

    .line 344
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/itau/empresas/api/model/DadosClienteVO;->setDigitoVerificadorDaConta(Ljava/lang/String;)V

    .line 346
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;

    .line 347
    .local v2, "operadoraVO":Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;
    new-instance v3, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    invoke-direct {v3}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;-><init>()V

    .line 348
    .local v3, "recargaInputVO":Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;
    new-instance v4, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;

    invoke-direct {v4}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;-><init>()V

    .line 349
    .local v4, "recargaVO":Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;
    invoke-virtual {v2}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->setNomeOperadora(Ljava/lang/String;)V

    .line 350
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->hasRegiao()Z

    move-result v0

    if-eqz v0, :cond_5d

    .line 351
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;

    .line 353
    .local v5, "regiaoVO":Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;
    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;->getRegiao()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->setNomeRegiao(Ljava/lang/String;)V

    .line 354
    .end local v5    # "regiaoVO":Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;
    goto :goto_62

    .line 355
    :cond_5d
    const-string v0, ""

    invoke-virtual {v4, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->setNomeRegiao(Ljava/lang/String;)V

    .line 357
    :goto_62
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->valorRecarga:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->getValor()Lorg/joda/money/Money;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/money/Money;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->setValorRecarga(Ljava/lang/Integer;)V

    .line 359
    new-instance v5, Lcom/itau/empresas/api/model/DadosPJVO;

    invoke-direct {v5}, Lcom/itau/empresas/api/model/DadosPJVO;-><init>()V

    .line 360
    .local v5, "dadosPJVO":Lcom/itau/empresas/api/model/DadosPJVO;
    const-string v0, "PEX"

    invoke-virtual {v5, v0}, Lcom/itau/empresas/api/model/DadosPJVO;->setProduto(Ljava/lang/String;)V

    .line 361
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/itau/empresas/api/model/DadosPJVO;->setCodigoOperador(Ljava/lang/String;)V

    .line 363
    invoke-virtual {v3, v1}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->setCliente(Lcom/itau/empresas/api/model/DadosClienteVO;)V

    .line 364
    invoke-virtual {v3, v4}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->setRecarga(Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;)V

    .line 365
    invoke-virtual {v3, v5}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->setPessoaJuridica(Lcom/itau/empresas/api/model/DadosPJVO;)V

    .line 367
    return-object v3
.end method

.method private desabilitaValorRecarga()V
    .registers 2

    .line 449
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->valorRecarga:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->desabilitado()V

    .line 450
    return-void
.end method

.method private getCelularSemMascara()Ljava/lang/String;
    .registers 2

    .line 320
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/MascaraEditText;->removeMascara(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getWarningMessage(Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;)Ljava/lang/String;
    .registers 6
    .param p1, "resultadoSimulacaoRecargaVO"    # Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse<Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;>;)Ljava/lang/String;"
        }
    .end annotation

    .line 658
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;->getHeaders()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_8

    .line 659
    const/4 v0, 0x0

    return-object v0

    .line 662
    :cond_8
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;->getHeaders()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_31

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/sdk/android/core/model/KeyValue;

    .line 663
    .local v2, "kv":Lbr/com/itau/sdk/android/core/model/KeyValue;
    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/model/KeyValue;->getKey()Ljava/lang/String;

    move-result-object v3

    .line 664
    .local v3, "key":Ljava/lang/String;
    if-eqz v3, :cond_30

    const-string v0, "Warning"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_30

    .line 665
    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/model/KeyValue;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 666
    .end local v2    # "kv":Lbr/com/itau/sdk/android/core/model/KeyValue;
    .end local v3    # "key":Ljava/lang/String;
    :cond_30
    goto :goto_10

    .line 667
    :cond_31
    const/4 v0, 0x0

    return-object v0
.end method

.method private hasRegiao()Z
    .registers 2

    .line 214
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getVisibility()I

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method private inicializaSpinner(Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;)V
    .registers 5
    .param p1, "item"    # Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;

    .line 487
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setVisibility(I)V

    .line 488
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 489
    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->getListaRegiao()Ljava/util/List;

    move-result-object v2

    .line 488
    invoke-static {v0, v1, v2}, Lcom/itau/empresas/ui/util/ViewUtils;->inicializarSpinnerOrdenado(Landroid/content/Context;Landroid/widget/Spinner;Ljava/util/List;)V

    .line 490
    return-void
.end method

.method private isValido()Z
    .registers 7

    .line 371
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->validate()Z

    move-result v2

    .line 372
    .local v2, "celularValido":Z
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getSelectedItemPosition()I

    move-result v0

    if-eqz v0, :cond_10

    const/4 v3, 0x1

    goto :goto_11

    :cond_10
    const/4 v3, 0x0

    .line 373
    .local v3, "operadoraValida":Z
    :goto_11
    const/4 v4, 0x1

    .line 374
    .local v4, "regiaoValida":Z
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->valorRecarga:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->validaValor()Z

    move-result v5

    .line 376
    .local v5, "valorValido":Z
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->hasRegiao()Z

    move-result v0

    if-eqz v0, :cond_29

    .line 377
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getSelectedItemPosition()I

    move-result v0

    if-eqz v0, :cond_28

    const/4 v4, 0x1

    goto :goto_29

    :cond_28
    const/4 v4, 0x0

    .line 380
    :cond_29
    :goto_29
    if-nez v3, :cond_33

    .line 381
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    const v1, 0x7f070653

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setError(I)V

    .line 384
    :cond_33
    if-nez v4, :cond_3d

    .line 385
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    const v1, 0x7f070658

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setError(I)V

    .line 388
    :cond_3d
    if-eqz v2, :cond_47

    if-eqz v3, :cond_47

    if-eqz v4, :cond_47

    if-eqz v5, :cond_47

    const/4 v0, 0x1

    goto :goto_48

    :cond_47
    const/4 v0, 0x0

    :goto_48
    return v0
.end method

.method private preencherComFavorecido()V
    .registers 5

    .line 256
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->favorecidoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->selectOperadora(Ljava/lang/String;)V

    .line 257
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->campoNome:Lbr/com/itau/widgets/material/MaterialEditText;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->favorecidoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getApelido()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setText(Ljava/lang/CharSequence;)V

    .line 258
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 259
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->favorecidoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    invoke-virtual {v2}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getDdd()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->favorecidoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 260
    invoke-virtual {v3}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getTelefone()Ljava/lang/String;

    move-result-object v3

    .line 259
    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/ViewUtils;->formataNumeroTelefonico(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 258
    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setText(Ljava/lang/CharSequence;)V

    .line 261
    return-void
.end method

.method private preencherContato(Landroid/database/Cursor;)V
    .registers 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .line 267
    const-string v0, "display_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 268
    .local v1, "indexName":I
    const-string v0, "data1"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 270
    .local v2, "indexNumber":I
    const/4 v0, -0x1

    if-eq v1, v0, :cond_18

    .line 271
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 272
    .local v3, "nomeContato":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->campoNome:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0, v3}, Lbr/com/itau/widgets/material/MaterialEditText;->setText(Ljava/lang/CharSequence;)V

    .line 275
    .end local v3    # "nomeContato":Ljava/lang/String;
    :cond_18
    const/4 v0, -0x1

    if-eq v2, v0, :cond_30

    .line 276
    const-string v0, "data1"

    .line 278
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 277
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 279
    .local v3, "numeroContato":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0, v3}, Lcom/itau/empresas/ui/util/ViewUtils;->ajustarNumeroAgenda(Lcom/itau/empresas/CustomApplication;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 281
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0, v3}, Lbr/com/itau/widgets/material/MaterialEditText;->setText(Ljava/lang/CharSequence;)V

    .line 284
    .end local v3    # "numeroContato":Ljava/lang/String;
    :cond_30
    return-void
.end method

.method private rehabilitarBotaoIncluirAutorizar(Z)V
    .registers 5
    .param p1, "checkValor"    # Z

    .line 403
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->tamanhoCampoCelularValido()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->valorRecarga:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->validaValor()Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v2, 0x1

    goto :goto_11

    :cond_10
    const/4 v2, 0x0

    .line 405
    .local v2, "habilitar":Z
    :goto_11
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->botaoIncluirRecarga:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 406
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->botaoAutorizarRecarga:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 408
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->application:Lcom/itau/empresas/CustomApplication;

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_RECARGA_AUTORIZAR:Lcom/itau/empresas/CustomApplication$Exibicao;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->verificarPermissao(Lcom/itau/empresas/CustomApplication$Exibicao;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 409
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->botaoAutorizarRecarga:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_33

    .line 411
    :cond_2c
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->botaoAutorizarRecarga:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 413
    :goto_33
    return-void
.end method

.method private selectOperadora(Ljava/lang/String;)V
    .registers 7
    .param p1, "operadora"    # Ljava/lang/String;

    .line 223
    const/4 v1, -0x1

    .line 224
    .local v1, "pos":I
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v2

    .line 226
    .local v2, "adapterOperadora":Landroid/widget/SpinnerAdapter;
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_8
    invoke-interface {v2}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v3, v0, :cond_23

    .line 227
    invoke-interface {v2, v3}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;

    .line 228
    .local v4, "operadoraVO":Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;
    invoke-virtual {v4}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 229
    move v1, v3

    .line 226
    .end local v4    # "operadoraVO":Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;
    :cond_20
    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    .line 232
    .end local v3    # "i":I
    :cond_23
    const/4 v0, -0x1

    if-eq v1, v0, :cond_2b

    .line 233
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setSelection(I)V

    .line 235
    :cond_2b
    return-void
.end method

.method private selectRegiao(Ljava/lang/String;)V
    .registers 7
    .param p1, "regiao"    # Ljava/lang/String;

    .line 238
    const/4 v1, -0x1

    .line 239
    .local v1, "pos":I
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v2

    .line 241
    .local v2, "adapterRegiao":Landroid/widget/SpinnerAdapter;
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_8
    invoke-interface {v2}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-ge v3, v0, :cond_23

    .line 242
    invoke-interface {v2, v3}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;

    .line 243
    .local v4, "regiaoVO":Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;
    invoke-virtual {v4}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;->getRegiao()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 244
    move v1, v3

    .line 241
    .end local v4    # "regiaoVO":Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;
    :cond_20
    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    .line 247
    .end local v3    # "i":I
    :cond_23
    const/4 v0, -0x1

    if-eq v1, v0, :cond_2b

    .line 248
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setSelection(I)V

    .line 250
    :cond_2b
    return-void
.end method

.method private setaDadosRegiao(ZLcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;)V
    .registers 5
    .param p1, "achouRegiao"    # Z
    .param p2, "item"    # Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;

    .line 468
    if-eqz p1, :cond_24

    .line 469
    invoke-direct {p0, p2}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->inicializaSpinner(Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;)V

    .line 471
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->favorecidoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    if-eqz v0, :cond_16

    .line 472
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->favorecidoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getEstadoOperadora()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->selectRegiao(Ljava/lang/String;)V

    .line 473
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->setFavorecidoRecargaVO(Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;)V

    .line 476
    :cond_16
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->novoContatoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    if-eqz v0, :cond_33

    .line 477
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->novoContatoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getEstadoOperadora()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->selectRegiao(Ljava/lang/String;)V

    goto :goto_33

    .line 481
    :cond_24
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setVisibility(I)V

    .line 482
    invoke-virtual {p2}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->buscaDetalheOperadorasRecarga(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    :cond_33
    :goto_33
    return-void
.end method

.method private simularRecarga(Z)V
    .registers 9
    .param p1, "autorizante"    # Z

    .line 299
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/SnackbarUtils;->tentaDispensarSnackbarNo(Ljava/lang/Object;)V

    .line 300
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->baseActivity()Lcom/itau/empresas/ui/activity/BaseActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->mostraDialogoDeProgresso()V

    .line 302
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->criarRecargaInputVO()Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    .line 303
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->getPessoaJuridica()Lcom/itau/empresas/api/model/DadosPJVO;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/itau/empresas/api/model/DadosPJVO;->setAutorizacao(Z)V

    .line 305
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 306
    const v2, 0x7f0702e8

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 307
    const v3, 0x7f0702bc

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    .line 309
    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->getRecarga()Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;

    move-result-object v5

    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    .line 308
    const v5, 0x7f070324

    invoke-virtual {p0, v5, v4}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    .line 310
    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->getRecarga()Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;

    move-result-object v5

    invoke-virtual {v5}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;->getValorRecarga()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 305
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 312
    if-eqz p1, :cond_70

    .line 313
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->recargaController:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getCelularSemMascara()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->simulaRecargaAutorizar(Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)V

    goto :goto_7b

    .line 315
    :cond_70
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->recargaController:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getCelularSemMascara()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;->simulaRecarga(Ljava/lang/String;Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)V

    .line 317
    :goto_7b
    return-void
.end method

.method private tamanhoCampoCelularValido()Z
    .registers 3

    .line 416
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->length()I

    move-result v1

    .line 418
    .local v1, "tamCampo":I
    const-string v0, "(##) #####-####"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eq v1, v0, :cond_16

    const-string v0, "(##) ####-####"

    .line 419
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v1, v0, :cond_18

    :cond_16
    const/4 v0, 0x1

    goto :goto_19

    :cond_18
    const/4 v0, 0x0

    .line 418
    :goto_19
    return v0
.end method

.method private verificaSeAchouRegiao(Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;)Z
    .registers 6
    .param p1, "item"    # Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;

    .line 453
    const/4 v2, 0x1

    .line 455
    .local v2, "achouRegiao":Z
    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->getListaRegiao()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_30

    .line 456
    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->getListaRegiao()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;->getRegiao()Ljava/lang/String;

    move-result-object v3

    .line 457
    .local v3, "regiao":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2e

    .line 458
    const v0, 0x7f0704bb

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 459
    :cond_2e
    const/4 v2, 0x0

    .line 461
    .end local v3    # "regiao":Ljava/lang/String;
    :cond_2f
    goto :goto_3b

    :cond_30
    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->getListaRegiao()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3b

    .line 462
    const/4 v2, 0x0

    .line 464
    :cond_3b
    :goto_3b
    return v2
.end method


# virtual methods
.method aoClicarAgenda()V
    .registers 6

    .line 592
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 593
    const v2, 0x7f0702e8

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 594
    const v3, 0x7f0702bc

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 595
    new-instance v4, Landroid/content/Intent;

    const-string v0, "android.intent.action.PICK"

    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v4, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 596
    .local v4, "intent":Landroid/content/Intent;
    const-string v0, "vnd.android.cursor.dir/phone_v2"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 597
    const/4 v0, 0x1

    invoke-virtual {p0, v4, v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 598
    return-void
.end method

.method protected aoClicarAutorizar()V
    .registers 7

    .line 527
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->isValido()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 528
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;

    const v3, 0x7f0702e8

    const v4, 0x7f0702bc

    const v5, 0x7f0702fe

    invoke-direct {v2, v3, v4, v5}, Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;-><init>(III)V

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Landroid/content/Context;Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;)V

    .line 532
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->confirmarSimulacao(Z)V

    .line 534
    :cond_25
    return-void
.end method

.method protected aoClicarIncluir()V
    .registers 7

    .line 516
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->isValido()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 517
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;

    const v3, 0x7f0702e8

    const v4, 0x7f0702bc

    const v5, 0x7f070308

    invoke-direct {v2, v3, v4, v5}, Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;-><init>(III)V

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Landroid/content/Context;Lcom/itau/empresas/ui/util/analytics/EventoAnalyticsRes;)V

    .line 521
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->confirmarSimulacao(Z)V

    .line 523
    :cond_25
    return-void
.end method

.method protected aoMudarTelefone()V
    .registers 2

    .line 399
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->rehabilitarBotaoIncluirAutorizar(Z)V

    .line 400
    return-void
.end method

.method protected aoSelecionarOperadora(ZI)V
    .registers 9
    .param p1, "selected"    # Z
    .param p2, "index"    # I

    .line 427
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->application:Lcom/itau/empresas/CustomApplication;

    const-string v1, "input_method"

    .line 428
    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    .line 429
    .local v3, "inputManager":Landroid/view/inputmethod/InputMethodManager;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v3, v0, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 433
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->desabilitaValorRecarga()V

    .line 434
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1d

    if-nez p1, :cond_2c

    .line 435
    :cond_1d
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1, v2}, Lcom/itau/empresas/ui/util/ViewUtils;->inicializarSpinnerOrdenado(Landroid/content/Context;Landroid/widget/Spinner;Ljava/util/List;)V

    .line 437
    return-void

    .line 441
    :cond_2c
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    add-int/lit8 v1, p2, 0x1

    .line 442
    invoke-interface {v0, v1}, Landroid/widget/SpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;

    .line 444
    .local v4, "item":Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;
    invoke-direct {p0, v4}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->verificaSeAchouRegiao(Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;)Z

    move-result v5

    .line 445
    .local v5, "achouRegiao":Z
    invoke-direct {p0, v5, v4}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->setaDadosRegiao(ZLcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;)V

    .line 446
    return-void
.end method

.method protected aoSelecionarRegiao(ZI)V
    .registers 8
    .param p1, "selected"    # Z
    .param p2, "index"    # I

    .line 497
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->application:Lcom/itau/empresas/CustomApplication;

    const-string v1, "input_method"

    .line 498
    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    .line 499
    .local v2, "inputManager":Landroid/view/inputmethod/InputMethodManager;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v2, v0, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 502
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->valorRecarga:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->desabilitado()V

    .line 504
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1f

    if-nez p1, :cond_20

    .line 505
    :cond_1f
    return-void

    .line 508
    :cond_20
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;

    .line 509
    .local v3, "operadoraVO":Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;

    .line 511
    .local v4, "regiaoVO":Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;
    invoke-virtual {v3}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaRegiaoVO;->getRegiao()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->buscaDetalheOperadorasRecarga(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    return-void
.end method

.method protected inicializar()V
    .registers 7

    .line 133
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    new-instance v1, Lbr/com/itau/widgets/material/validation/RegexpValidator;

    .line 134
    const v2, 0x7f070649

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "^\\(\\d{2}\\) \\d{4,5}-\\d{4}$"

    invoke-direct {v1, v2, v3}, Lbr/com/itau/widgets/material/validation/RegexpValidator;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->addValidator(Lbr/com/itau/widgets/material/validation/METValidator;)Lbr/com/itau/widgets/material/MaterialEditText;

    .line 136
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-static {v0}, Lcom/itau/empresas/ui/util/MascaraCelular;->insere(Landroid/widget/EditText;)Landroid/text/TextWatcher;

    move-result-object v4

    .line 137
    .local v4, "mascara":Landroid/text/TextWatcher;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0, v4}, Lbr/com/itau/widgets/material/MaterialEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 139
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->buscaOperadorasRecarga()V

    .line 141
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "accessibility"

    .line 142
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/view/accessibility/AccessibilityManager;

    .line 144
    .local v5, "manager":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v5}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    if-nez v0, :cond_3a

    invoke-virtual {v5}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_46

    .line 145
    :cond_3a
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    const v1, 0x7f0704ed

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 148
    :cond_46
    new-instance v0, Lcom/itau/empresas/ui/util/HintUtils;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/itau/empresas/ui/util/HintUtils;-><init>(Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->hintUtils:Lcom/itau/empresas/ui/util/HintUtils;

    .line 149
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 14
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .line 603
    const/4 v0, 0x1

    if-ne p1, v0, :cond_4b

    const/4 v0, -0x1

    if-ne p2, v0, :cond_4b

    .line 604
    const/4 v6, 0x0

    .line 606
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_7
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    .line 607
    .local v7, "contactUri":Landroid/net/Uri;
    const/4 v0, 0x2

    new-array v8, v0, [Ljava/lang/String;

    const-string v0, "display_name"

    const/4 v1, 0x0

    aput-object v0, v8, v1

    const-string v0, "data1"

    const/4 v1, 0x1

    aput-object v0, v8, v1

    .line 609
    .local v8, "projection":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, v7

    move-object v2, v8

    .line 610
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    move-object v6, v0

    .line 611
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 612
    invoke-direct {p0, v6}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->preencherContato(Landroid/database/Cursor;)V
    :try_end_30
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_30} :catch_36
    .catchall {:try_start_7 .. :try_end_30} :catchall_44

    .line 616
    .end local v7    # "contactUri":Landroid/net/Uri;
    .end local v8    # "projection":[Ljava/lang/String;
    if-eqz v6, :cond_4b

    .line 617
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_4b

    .line 613
    :catch_36
    move-exception v7

    .line 614
    .local v7, "e":Ljava/lang/Exception;
    const-string v0, "RecargaNovoNumeroFrgmt"

    const-string v1, "Erro ao inserir contato"

    :try_start_3b
    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3e
    .catchall {:try_start_3b .. :try_end_3e} :catchall_44

    .line 616
    .end local v7    # "e":Ljava/lang/Exception;
    if-eqz v6, :cond_4b

    .line 617
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_4b

    .line 616
    :catchall_44
    move-exception v9

    if-eqz v6, :cond_4a

    .line 617
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4a
    throw v9

    .line 621
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_4b
    :goto_4b
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/fragment/BaseFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 622
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;)V
    .registers 5
    .param p1, "resultadoSimulacaoRecargaVO"    # Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse<Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;>;)V"
        }
    .end annotation

    .line 641
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->baseActivity()Lcom/itau/empresas/ui/activity/BaseActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 643
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    if-eqz v0, :cond_41

    .line 644
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getWarningMessage(Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;)Ljava/lang/String;

    move-result-object v2

    .line 646
    .local v2, "warning":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    .line 647
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->novo(Z)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->campoNome:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 648
    invoke-virtual {v1}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->nome(Ljava/lang/String;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    .line 649
    invoke-virtual {v0, v2}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->aviso(Ljava/lang/String;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    .line 650
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;->getData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->resultadoSimulacaoRecargaVO(Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoSimulacaoRecargaVO;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->recargaInputVO:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;

    .line 651
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->recargaInputVO(Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 654
    .end local v2    # "warning":Ljava/lang/String;
    :cond_41
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 9
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 671
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 672
    return-void

    .line 675
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->baseActivity()Lcom/itau/empresas/ui/activity/BaseActivity;

    move-result-object v3

    .line 676
    .local v3, "menuActivity":Lcom/itau/empresas/ui/activity/BaseActivity;
    invoke-virtual {v3}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 677
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v4

    .line 678
    .local v4, "error":Lbr/com/itau/sdk/android/core/model/ErrorDTO;
    if-eqz v4, :cond_6c

    .line 679
    invoke-virtual {v4}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getCodigo()Ljava/lang/String;

    move-result-object v5

    const/4 v6, -0x1

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_6e

    goto :goto_38

    :sswitch_25
    const-string v0, "limite_diario"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_38

    const/4 v6, 0x0

    goto :goto_38

    :sswitch_2f
    const-string v0, "saldo_insuficiente"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_38

    const/4 v6, 0x1

    :cond_38
    :goto_38
    sparse-switch v6, :sswitch_data_78

    goto :goto_60

    .line 681
    :sswitch_3c
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;->LDP:Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

    .line 682
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->limite(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    .line 683
    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 684
    goto :goto_6c

    .line 686
    :sswitch_4e
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;->CHEQUE_ESPECIAL:Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;

    .line 687
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->limite(Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity$ErrorLimite;)Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;

    move-result-object v0

    .line 688
    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/activity/RecargaEfetivarActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 689
    goto :goto_6c

    .line 691
    :goto_60
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v4}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/itau/empresas/ui/util/SnackbarUtils;->tentaMostrarSnackbarNo(Ljava/lang/Object;Ljava/lang/CharSequence;I)V

    .line 695
    :cond_6c
    :goto_6c
    return-void

    nop

    :sswitch_data_6e
    .sparse-switch
        -0x2479db0f -> :sswitch_25
        -0x59e0060 -> :sswitch_2f
    .end sparse-switch

    :sswitch_data_78
    .sparse-switch
        0x0 -> :sswitch_3c
        0x1 -> :sswitch_4e
    .end sparse-switch
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$ValorSelecionado;)V
    .registers 4
    .param p1, "valorSelecionado"    # Lcom/itau/empresas/Evento$ValorSelecionado;

    .line 720
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->botaoIncluirRecarga:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 721
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->botaoAutorizarRecarga:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 723
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->application:Lcom/itau/empresas/CustomApplication;

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_RECARGA_AUTORIZAR:Lcom/itau/empresas/CustomApplication$Exibicao;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->verificarPermissao(Lcom/itau/empresas/CustomApplication$Exibicao;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 724
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->botaoAutorizarRecarga:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_24

    .line 726
    :cond_1d
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->botaoAutorizarRecarga:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 728
    :goto_24
    return-void
.end method

.method public onEventMainThread(Ljava/util/List;)V
    .registers 5
    .param p1, "operadoras"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;>;)V"
        }
    .end annotation

    .line 628
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->baseActivity()Lcom/itau/empresas/ui/activity/BaseActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 629
    move-object v2, p1

    .line 631
    .local v2, "recargaOperadoraVOList":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/recarga/contatos/model/RecargaOperadoraVO;>;"
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-static {v0, v1, v2}, Lcom/itau/empresas/ui/util/ViewUtils;->inicializarSpinnerOrdenado(Landroid/content/Context;Landroid/widget/Spinner;Ljava/util/List;)V

    .line 634
    return-void
.end method

.method public onStart()V
    .registers 2

    .line 154
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->favorecidoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    if-eqz v0, :cond_7

    .line 155
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->preencherComFavorecido()V

    .line 158
    :cond_7
    invoke-super {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->onStart()V

    .line 159
    return-void
.end method

.method public onStop()V
    .registers 2

    .line 165
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->setFavorecidoRecargaVO(Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;)V

    .line 166
    invoke-super {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->onStop()V

    .line 167
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 708
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "listaOperadoras"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "contatosRecarga"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "cadastrarContatoRecarga"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "alterarContatoRecarga"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "deletarContatoRecarga"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "valoresOperadora"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "simularRecarga"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "exibicao_recarga_autorizar"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string v1, "efetivarRecarga"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public setContatoParaRecarga(Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;)V
    .registers 6
    .param p1, "contatoRecargaVO"    # Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 698
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->novoContatoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 699
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 700
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getDdd()Ljava/lang/String;

    move-result-object v2

    .line 701
    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getTelefone()Ljava/lang/String;

    move-result-object v3

    .line 700
    invoke-static {v1, v2, v3}, Lcom/itau/empresas/ui/util/ViewUtils;->formataNumeroTelefonico(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setText(Ljava/lang/CharSequence;)V

    .line 702
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->campoNome:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getApelido()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setText(Ljava/lang/CharSequence;)V

    .line 703
    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->selectOperadora(Ljava/lang/String;)V

    .line 704
    return-void
.end method

.method public setFavorecidoRecargaVO(Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;)V
    .registers 2
    .param p1, "favorecidoRecargaVO"    # Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 218
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->favorecidoRecargaVO:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    .line 219
    return-void
.end method

.method public setMenuVisibility(Z)V
    .registers 4
    .param p1, "menuVisible"    # Z

    .line 171
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/fragment/BaseFragment;->setMenuVisibility(Z)V

    .line 172
    if-eqz p1, :cond_14

    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->hintUtils:Lcom/itau/empresas/ui/util/HintUtils;

    const-string v1, "CHVREC-ID"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/HintUtils;->exibeHint(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 173
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->botaoAgenda:Landroid/widget/ImageButton;

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->adicionaHint(Landroid/view/View;)V

    .line 175
    :cond_14
    return-void
.end method

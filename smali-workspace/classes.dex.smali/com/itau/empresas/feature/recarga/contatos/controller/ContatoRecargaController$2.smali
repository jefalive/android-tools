.class Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$2;
.super Ljava/lang/Object;
.source "ContatoRecargaController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->insereContatoRecarga(Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

.field final synthetic val$contato:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    .line 30
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$2;->this$0:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    iput-object p2, p0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$2;->val$contato:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 4

    .line 33
    new-instance v2, Lcom/itau/empresas/Evento$EventoContatoRecargaInserido;

    invoke-direct {v2}, Lcom/itau/empresas/Evento$EventoContatoRecargaInserido;-><init>()V

    .line 34
    .local v2, "eventoContatoRecargaInserido":Lcom/itau/empresas/Evento$EventoContatoRecargaInserido;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$2;->this$0:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    # invokes: Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->api()Ljava/lang/Object;
    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->access$200(Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$2;->val$contato:Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;

    .line 35
    invoke-interface {v0, v1}, Lcom/itau/empresas/api/Api;->insereContatoRecarga(Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaInputVO;)Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;

    move-result-object v0

    .line 34
    invoke-virtual {v2, v0}, Lcom/itau/empresas/Evento$EventoContatoRecargaInserido;->setContatoRecargaVO(Lcom/itau/empresas/feature/recarga/contatos/model/ContatoRecargaVO;)V

    .line 36
    # invokes: Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->post(Ljava/lang/Object;)V
    invoke-static {v2}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->access$300(Ljava/lang/Object;)V

    .line 37
    return-void
.end method

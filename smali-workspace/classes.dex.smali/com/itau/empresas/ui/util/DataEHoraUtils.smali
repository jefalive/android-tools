.class public Lcom/itau/empresas/ui/util/DataEHoraUtils;
.super Ljava/lang/Object;
.source "DataEHoraUtils.java"


# static fields
.field private static final API_FORMAT_DATE:Lorg/threeten/bp/format/DateTimeFormatter;

.field private static final API_FORMAT_DATE_TIME:Lorg/threeten/bp/format/DateTimeFormatter;

.field private static final ARRAY_MESES:[Ljava/lang/String;

.field private static final VIEW_FORMAT_TIME:Lorg/threeten/bp/format/DateTimeFormatter;

.field private static dataMaximaMensal:Z

.field private static iniciaDatePickerComDatasAnterioresDesabilitadas:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 27
    const-string v0, "yyyy-MM-dd"

    .line 28
    invoke-static {v0}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/ui/util/DataEHoraUtils;->API_FORMAT_DATE:Lorg/threeten/bp/format/DateTimeFormatter;

    .line 29
    const-string v0, "HH:mm:ss"

    .line 30
    invoke-static {v0}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/ui/util/DataEHoraUtils;->VIEW_FORMAT_TIME:Lorg/threeten/bp/format/DateTimeFormatter;

    .line 31
    const-string v0, "yyyy-MM-dd HH:mm:ss"

    .line 32
    invoke-static {v0}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/ui/util/DataEHoraUtils;->API_FORMAT_DATE_TIME:Lorg/threeten/bp/format/DateTimeFormatter;

    .line 34
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "janeiro"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "fevereiro"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "mar\u00e7o"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "abril"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "maio"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "junho"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "julho"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "agosto"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string v1, "setembro"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const-string v1, "outubro"

    const/16 v2, 0x9

    aput-object v1, v0, v2

    const-string v1, "novembro"

    const/16 v2, 0xa

    aput-object v1, v0, v2

    const-string v1, "dezembro"

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sput-object v0, Lcom/itau/empresas/ui/util/DataEHoraUtils;->ARRAY_MESES:[Ljava/lang/String;

    return-void
.end method

.method public static adicionaDiasHoje(I)Ljava/lang/String;
    .registers 4
    .param p0, "dias"    # I

    .line 116
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 117
    .local v1, "dataAtual":Ljava/util/Calendar;
    const/4 v0, 0x5

    invoke-virtual {v1, v0, p0}, Ljava/util/Calendar;->add(II)V

    .line 119
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v0, "yyyy-MM-dd"

    invoke-direct {v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 120
    .local v2, "df":Ljava/text/SimpleDateFormat;
    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static formataDataApi(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .param p0, "data"    # Ljava/lang/String;

    .line 98
    if-nez p0, :cond_5

    .line 99
    const-string v0, ""

    return-object v0

    .line 102
    :cond_5
    const-string v0, "dd/MM/yyyy"

    invoke-static {v0}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/threeten/bp/LocalDate;->parse(Ljava/lang/CharSequence;Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-string v1, "yyyy-MM-dd"

    .line 103
    invoke-static {v1}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalDate;->format(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object v0

    .line 102
    return-object v0
.end method

.method public static formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .param p0, "data"    # Ljava/lang/String;

    .line 84
    if-nez p0, :cond_5

    .line 85
    const-string v0, ""

    return-object v0

    .line 88
    :cond_5
    const-string v0, "yyyy-MM-dd"

    invoke-static {v0}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/threeten/bp/LocalDate;->parse(Ljava/lang/CharSequence;Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-string v1, "dd/MM/yyyy"

    .line 89
    invoke-static {v1}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalDate;->format(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object v0

    .line 88
    return-object v0
.end method

.method public static formataDate(Ljava/lang/String;Ljava/util/Date;)Ljava/lang/String;
    .registers 5
    .param p0, "format"    # Ljava/lang/String;
    .param p1, "data"    # Ljava/util/Date;

    .line 129
    :try_start_0
    new-instance v2, Ljava/text/SimpleDateFormat;

    new-instance v0, Ljava/util/Locale;

    const-string v1, "pt_BR"

    invoke-direct {v0, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, p0, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 130
    .local v2, "sdf":Ljava/text/SimpleDateFormat;
    invoke-virtual {v2, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_f} :catch_11

    move-result-object v0

    return-object v0

    .line 131
    .end local v2    # "sdf":Ljava/text/SimpleDateFormat;
    :catch_11
    move-exception v2

    .line 132
    .local v2, "e":Ljava/lang/Exception;
    const-string v0, "formataDate"

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Lbr/com/itau/textovoz/util/Logger;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 134
    .end local v2    # "e":Ljava/lang/Exception;
    const-string v0, ""

    return-object v0
.end method

.method public static formataDatePickerParaStringComBarra(Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;)Ljava/lang/String;
    .registers 7
    .param p0, "datePickerDialog"    # Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    .line 244
    invoke-virtual {p0}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->getSelectedDay()Lcom/wdullaer/materialdatetimepicker/date/MonthAdapter$CalendarDay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/wdullaer/materialdatetimepicker/date/MonthAdapter$CalendarDay;->getDay()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 245
    .local v3, "dia":Ljava/lang/Integer;
    invoke-virtual {p0}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->getSelectedDay()Lcom/wdullaer/materialdatetimepicker/date/MonthAdapter$CalendarDay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/wdullaer/materialdatetimepicker/date/MonthAdapter$CalendarDay;->getMonth()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 246
    .local v4, "mes":Ljava/lang/Integer;
    invoke-virtual {p0}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->getSelectedDay()Lcom/wdullaer/materialdatetimepicker/date/MonthAdapter$CalendarDay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/wdullaer/materialdatetimepicker/date/MonthAdapter$CalendarDay;->getYear()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 248
    .local v5, "ano":Ljava/lang/Integer;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 249
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0xa

    if-ge v1, v2, :cond_59

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_5d

    :cond_59
    invoke-virtual {v4}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_5d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 250
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0xa

    if-ge v1, v2, :cond_87

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_8b

    :cond_87
    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_8b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 248
    return-object v0
.end method

.method public static formataHorario(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .param p0, "horario"    # Ljava/lang/String;

    .line 107
    const-string v0, "HH:mm:ss"

    invoke-static {v0}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/threeten/bp/LocalTime;->parse(Ljava/lang/CharSequence;Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/LocalTime;

    move-result-object v0

    const-string v1, "HH:mm\'h\'"

    .line 108
    invoke-static {v1}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalTime;->format(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object v0

    .line 107
    return-object v0
.end method

.method public static getAno(Ljava/lang/String;)Ljava/lang/Integer;
    .registers 2
    .param p0, "data"    # Ljava/lang/String;

    .line 300
    invoke-static {p0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->parse(Ljava/lang/String;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->getYear()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static getDataAtual()Ljava/lang/String;
    .registers 3

    .line 142
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 144
    .local v1, "dataAtual":Ljava/util/Calendar;
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v0, "yyyy-MM-dd"

    invoke-direct {v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 145
    .local v2, "df":Ljava/text/SimpleDateFormat;
    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDataAtualString()Ljava/lang/String;
    .registers 2

    .line 67
    invoke-static {}, Lorg/threeten/bp/LocalDate;->now()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/ui/util/DataEHoraUtils;->API_FORMAT_DATE:Lorg/threeten/bp/format/DateTimeFormatter;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalDate;->format(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDia(Ljava/lang/String;)Ljava/lang/Integer;
    .registers 2
    .param p0, "data"    # Ljava/lang/String;

    .line 288
    invoke-static {p0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->parse(Ljava/lang/String;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->getDayOfMonth()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static getHoraAtual()Ljava/lang/String;
    .registers 2

    .line 292
    invoke-static {}, Lorg/threeten/bp/LocalTime;->now()Lorg/threeten/bp/LocalTime;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/ui/util/DataEHoraUtils;->VIEW_FORMAT_TIME:Lorg/threeten/bp/format/DateTimeFormatter;

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalTime;->format(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getMesPorExtenso(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .param p0, "data"    # Ljava/lang/String;

    .line 296
    sget-object v0, Lcom/itau/empresas/ui/util/DataEHoraUtils;->ARRAY_MESES:[Ljava/lang/String;

    const-string v1, "yyyy-MM-dd"

    invoke-static {v1}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v1

    invoke-static {p0, v1}, Lorg/threeten/bp/LocalDate;->parse(Ljava/lang/CharSequence;Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/LocalDate;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/LocalDate;->getMonth()Lorg/threeten/bp/Month;

    move-result-object v1

    invoke-virtual {v1}, Lorg/threeten/bp/Month;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public static mostraDialogoData(Lcom/itau/empresas/ui/activity/BaseActivity;Landroid/widget/TextView;)Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;
    .registers 6
    .param p0, "activity"    # Lcom/itau/empresas/ui/activity/BaseActivity;
    .param p1, "campoParaMostrar"    # Landroid/widget/TextView;

    .line 274
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 275
    .local v3, "now":Ljava/util/Calendar;
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v1, 0x2

    invoke-virtual {v3, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x5

    invoke-virtual {v3, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-static {p0, p1, v0, v1, v2}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->mostraDialogoData(Lcom/itau/empresas/ui/activity/BaseActivity;Landroid/widget/TextView;III)Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    move-result-object v0

    return-object v0
.end method

.method public static mostraDialogoData(Lcom/itau/empresas/ui/activity/BaseActivity;Landroid/widget/TextView;III)Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;
    .registers 10
    .param p0, "activity"    # Lcom/itau/empresas/ui/activity/BaseActivity;
    .param p1, "campoParaMostrar"    # Landroid/widget/TextView;
    .param p2, "ano"    # I
    .param p3, "mes"    # I
    .param p4, "dia"    # I

    .line 166
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 168
    .local v2, "now":Ljava/util/Calendar;
    const/4 v0, 0x0

    invoke-static {v0, p2, p3, p4}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->newInstance(Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog$OnDateSetListener;III)Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    move-result-object v3

    .line 175
    .local v3, "dpd":Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;
    sget-boolean v0, Lcom/itau/empresas/ui/util/DataEHoraUtils;->iniciaDatePickerComDatasAnterioresDesabilitadas:Z

    if-eqz v0, :cond_11

    .line 176
    invoke-virtual {v3, v2}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->setMinDate(Ljava/util/Calendar;)V

    goto :goto_15

    .line 178
    :cond_11
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->setMinDate(Ljava/util/Calendar;)V

    .line 180
    :goto_15
    sget-boolean v0, Lcom/itau/empresas/ui/util/DataEHoraUtils;->dataMaximaMensal:Z

    if-eqz v0, :cond_3b

    .line 182
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 184
    .local v4, "cal":Ljava/util/Calendar;
    const/4 v0, 0x5

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v0

    const/4 v1, 0x5

    invoke-virtual {v4, v1, v0}, Ljava/util/Calendar;->set(II)V

    .line 185
    const/4 v0, 0x2

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v1, 0x2

    invoke-virtual {v4, v1, v0}, Ljava/util/Calendar;->set(II)V

    .line 186
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {v4, v1, v0}, Ljava/util/Calendar;->set(II)V

    .line 187
    invoke-virtual {v3, v4}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->setMaxDate(Ljava/util/Calendar;)V

    .line 189
    .end local v4    # "cal":Ljava/util/Calendar;
    :cond_3b
    new-instance v0, Lcom/itau/empresas/ui/util/DataEHoraUtils$1;

    invoke-direct {v0, p1}, Lcom/itau/empresas/ui/util/DataEHoraUtils$1;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v3, v0}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->setOnDateSetListener(Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog$OnDateSetListener;)V

    .line 204
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->setThemeDark(Z)V

    .line 206
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "Calendario"

    invoke-virtual {v3, v0, v1}, Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 208
    return-object v3
.end method

.method public static final mostraDialogoDataComAnterioresDesabilitadasEDataMaximaMesAtual(Lcom/itau/empresas/ui/activity/BaseActivity;Landroid/widget/TextView;)Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;
    .registers 3
    .param p0, "activity"    # Lcom/itau/empresas/ui/activity/BaseActivity;
    .param p1, "campoParaMostrar"    # Landroid/widget/TextView;

    .line 230
    const/4 v0, 0x1

    sput-boolean v0, Lcom/itau/empresas/ui/util/DataEHoraUtils;->iniciaDatePickerComDatasAnterioresDesabilitadas:Z

    .line 231
    const/4 v0, 0x1

    sput-boolean v0, Lcom/itau/empresas/ui/util/DataEHoraUtils;->dataMaximaMensal:Z

    .line 232
    invoke-static {p0, p1}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->mostraDialogoData(Lcom/itau/empresas/ui/activity/BaseActivity;Landroid/widget/TextView;)Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    move-result-object v0

    return-object v0
.end method

.method public static final mostraDialogoDataComTodasDatas(Lcom/itau/empresas/ui/activity/BaseActivity;Landroid/widget/TextView;)Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;
    .registers 3
    .param p0, "activity"    # Lcom/itau/empresas/ui/activity/BaseActivity;
    .param p1, "campoParaMostrar"    # Landroid/widget/TextView;

    .line 214
    const/4 v0, 0x0

    sput-boolean v0, Lcom/itau/empresas/ui/util/DataEHoraUtils;->iniciaDatePickerComDatasAnterioresDesabilitadas:Z

    .line 215
    const/4 v0, 0x0

    sput-boolean v0, Lcom/itau/empresas/ui/util/DataEHoraUtils;->dataMaximaMensal:Z

    .line 216
    invoke-static {p0, p1}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->mostraDialogoData(Lcom/itau/empresas/ui/activity/BaseActivity;Landroid/widget/TextView;)Lcom/wdullaer/materialdatetimepicker/date/DatePickerDialog;

    move-result-object v0

    return-object v0
.end method

.method public static parse(Ljava/lang/String;)Lorg/threeten/bp/LocalDate;
    .registers 2
    .param p0, "dataApi"    # Ljava/lang/String;

    .line 57
    sget-object v0, Lcom/itau/empresas/ui/util/DataEHoraUtils;->API_FORMAT_DATE:Lorg/threeten/bp/format/DateTimeFormatter;

    invoke-static {p0, v0}, Lorg/threeten/bp/LocalDate;->parse(Ljava/lang/CharSequence;Lorg/threeten/bp/format/DateTimeFormatter;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    return-object v0
.end method

.method public static printData(Ljava/util/Date;)Ljava/lang/String;
    .registers 2
    .param p0, "data"    # Ljava/util/Date;

    .line 124
    const-string v0, "dd/MM/yyyy"

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDate(Ljava/lang/String;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static printDataComBarra(Ljava/util/Date;)Ljava/lang/String;
    .registers 2
    .param p0, "data"    # Ljava/util/Date;

    .line 138
    const-string v0, "yyyy-MM-dd"

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDate(Ljava/lang/String;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static subtraiDiasDeHoje(I)Ljava/lang/String;
    .registers 4
    .param p0, "dias"    # I

    .line 112
    invoke-static {}, Lorg/threeten/bp/LocalDate;->now()Lorg/threeten/bp/LocalDate;

    move-result-object v0

    int-to-long v1, p0

    invoke-virtual {v0, v1, v2}, Lorg/threeten/bp/LocalDate;->minusDays(J)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    const-string v1, "yyyy-MM-dd"

    invoke-static {v1}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalDate;->format(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public Lcom/itau/empresas/api/model/ExtratoVO;
.super Ljava/lang/Object;
.source "ExtratoVO.java"


# instance fields
.field private lancamentos:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "lancamentos"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/LancamentoVO;>;"
        }
    .end annotation
.end field

.field private lancamentosDia:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "lancamentos_dia"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/LancamentoVO;>;"
        }
    .end annotation
.end field

.field private lancamentosFuturos:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "lancamentos_futuros"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/LancamentoVO;>;"
        }
    .end annotation
.end field

.field private saldo:Lcom/itau/empresas/api/model/SaldoVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "saldo"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/api/model/ExtratoVO;->lancamentos:Ljava/util/List;

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/api/model/ExtratoVO;->lancamentosDia:Ljava/util/List;

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/api/model/ExtratoVO;->lancamentosFuturos:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getLancamentos()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/api/model/LancamentoVO;>;"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/itau/empresas/api/model/ExtratoVO;->lancamentos:Ljava/util/List;

    return-object v0
.end method

.method public getLancamentosFuturos()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/api/model/LancamentoVO;>;"
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/api/model/ExtratoVO;->lancamentosFuturos:Ljava/util/List;

    return-object v0
.end method

.method public getSaldo()Lcom/itau/empresas/api/model/SaldoVO;
    .registers 2

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/api/model/ExtratoVO;->saldo:Lcom/itau/empresas/api/model/SaldoVO;

    return-object v0
.end method

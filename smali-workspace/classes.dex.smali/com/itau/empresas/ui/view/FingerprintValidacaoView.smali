.class public Lcom/itau/empresas/ui/view/FingerprintValidacaoView;
.super Landroid/widget/LinearLayout;
.source "FingerprintValidacaoView.java"


# instance fields
.field private textoInternoFingerprint:Landroid/widget/TextView;

.field private textoTituloFingerprint:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 34
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 35
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/FingerprintValidacaoView;->init()V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/FingerprintValidacaoView;->init()V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/FingerprintValidacaoView;->init()V

    .line 46
    return-void
.end method

.method private init()V
    .registers 3

    .line 56
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/FingerprintValidacaoView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03016d

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/ui/view/FingerprintValidacaoView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 57
    const v0, 0x7f0e018b

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/FingerprintValidacaoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/FingerprintValidacaoView;->textoTituloFingerprint:Landroid/widget/TextView;

    .line 58
    const v0, 0x7f0e063d

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/FingerprintValidacaoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/FingerprintValidacaoView;->textoInternoFingerprint:Landroid/widget/TextView;

    .line 59
    return-void
.end method


# virtual methods
.method public mudaTextoInterno(Ljava/lang/String;)V
    .registers 3
    .param p1, "texto"    # Ljava/lang/String;

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/ui/view/FingerprintValidacaoView;->textoInternoFingerprint:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31
    return-void
.end method

.method public reinicia()V
    .registers 3

    .line 24
    iget-object v0, p0, Lcom/itau/empresas/ui/view/FingerprintValidacaoView;->textoTituloFingerprint:Landroid/widget/TextView;

    const v1, 0x7f070231

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 26
    iget-object v0, p0, Lcom/itau/empresas/ui/view/FingerprintValidacaoView;->textoTituloFingerprint:Landroid/widget/TextView;

    const v1, 0x7f0c0035

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 27
    return-void
.end method

.method public setFalhou()V
    .registers 3

    .line 18
    iget-object v0, p0, Lcom/itau/empresas/ui/view/FingerprintValidacaoView;->textoTituloFingerprint:Landroid/widget/TextView;

    const v1, 0x7f0701e2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 20
    iget-object v0, p0, Lcom/itau/empresas/ui/view/FingerprintValidacaoView;->textoTituloFingerprint:Landroid/widget/TextView;

    const v1, 0x7f0c012b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 21
    return-void
.end method

.class public final Lcom/itau/empresas/ui/util/SnackbarUtils;
.super Ljava/lang/Object;
.source "SnackbarUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/util/SnackbarUtils$GerenciadorDelegate;,
        Lcom/itau/empresas/ui/util/SnackbarUtils$Gerenciador;,
        Lcom/itau/empresas/ui/util/SnackbarUtils$Apresentador;,
        Lcom/itau/empresas/ui/util/SnackbarUtils$Dispensador;
    }
.end annotation


# direct methods
.method public static tentaDispensarSnackbarNo(Ljava/lang/Object;)V
    .registers 2
    .param p0, "alvo"    # Ljava/lang/Object;

    .line 13
    instance-of v0, p0, Lcom/itau/empresas/ui/util/SnackbarUtils$Dispensador;

    if-eqz v0, :cond_a

    .line 14
    move-object v0, p0

    check-cast v0, Lcom/itau/empresas/ui/util/SnackbarUtils$Dispensador;

    invoke-interface {v0}, Lcom/itau/empresas/ui/util/SnackbarUtils$Dispensador;->dismissSnackbar()V

    .line 15
    :cond_a
    return-void
.end method

.method public static tentaMostrarSnackbarNo(Ljava/lang/Object;Ljava/lang/CharSequence;I)V
    .registers 4
    .param p0, "alvo"    # Ljava/lang/Object;
    .param p1, "texto"    # Ljava/lang/CharSequence;
    .param p2, "duracao"    # I

    .line 19
    instance-of v0, p0, Lcom/itau/empresas/ui/util/SnackbarUtils$Apresentador;

    if-eqz v0, :cond_a

    .line 20
    move-object v0, p0

    check-cast v0, Lcom/itau/empresas/ui/util/SnackbarUtils$Apresentador;

    invoke-interface {v0, p1, p2}, Lcom/itau/empresas/ui/util/SnackbarUtils$Apresentador;->showSnackbar(Ljava/lang/CharSequence;I)V

    .line 21
    :cond_a
    return-void
.end method

.class public Lcom/itau/empresas/SearchViewDelay;
.super Ljava/lang/Object;
.source "SearchViewDelay.java"

# interfaces
.implements Landroid/support/v7/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/SearchViewDelay$SearchViewDelayCallback;
    }
.end annotation


# instance fields
.field private handler:Landroid/os/Handler;

.field private isValid:Z

.field private final mListener:Lcom/itau/empresas/SearchViewDelay$SearchViewDelayCallback;

.field private runnable:Ljava/lang/Runnable;

.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/itau/empresas/SearchViewDelay$SearchViewDelayCallback;)V
    .registers 3
    .param p1, "listener"    # Lcom/itau/empresas/SearchViewDelay$SearchViewDelayCallback;

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/itau/empresas/SearchViewDelay;->text:Ljava/lang/String;

    .line 19
    iput-object p1, p0, Lcom/itau/empresas/SearchViewDelay;->mListener:Lcom/itau/empresas/SearchViewDelay$SearchViewDelayCallback;

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/SearchViewDelay;->isValid:Z

    .line 23
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/SearchViewDelay;->handler:Landroid/os/Handler;

    .line 25
    new-instance v0, Lcom/itau/empresas/SearchViewDelay$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/SearchViewDelay$1;-><init>(Lcom/itau/empresas/SearchViewDelay;)V

    iput-object v0, p0, Lcom/itau/empresas/SearchViewDelay;->runnable:Ljava/lang/Runnable;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/SearchViewDelay;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/SearchViewDelay;

    .line 7
    iget-object v0, p0, Lcom/itau/empresas/SearchViewDelay;->text:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/itau/empresas/SearchViewDelay;)Lcom/itau/empresas/SearchViewDelay$SearchViewDelayCallback;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/SearchViewDelay;

    .line 7
    iget-object v0, p0, Lcom/itau/empresas/SearchViewDelay;->mListener:Lcom/itau/empresas/SearchViewDelay$SearchViewDelayCallback;

    return-object v0
.end method

.method private searchDelay(Ljava/lang/String;)V
    .registers 7
    .param p1, "searchText"    # Ljava/lang/String;

    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/SearchViewDelay;->isValid:Z

    .line 51
    iput-object p1, p0, Lcom/itau/empresas/SearchViewDelay;->text:Ljava/lang/String;

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/SearchViewDelay;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/itau/empresas/SearchViewDelay;->runnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 53
    const/16 v4, 0x1c2

    .line 54
    .local v4, "defaultDelay":I
    iget-object v0, p0, Lcom/itau/empresas/SearchViewDelay;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/itau/empresas/SearchViewDelay;->runnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1c2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 55
    return-void
.end method


# virtual methods
.method public cancelSearch()V
    .registers 3

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/SearchViewDelay;->isValid:Z

    .line 59
    iget-object v0, p0, Lcom/itau/empresas/SearchViewDelay;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 60
    return-void
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .registers 3
    .param p1, "searchText"    # Ljava/lang/String;

    .line 43
    iget-boolean v0, p0, Lcom/itau/empresas/SearchViewDelay;->isValid:Z

    if-eqz v0, :cond_7

    .line 44
    invoke-direct {p0, p1}, Lcom/itau/empresas/SearchViewDelay;->searchDelay(Ljava/lang/String;)V

    .line 46
    :cond_7
    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .registers 3
    .param p1, "searchText"    # Ljava/lang/String;

    .line 35
    iget-boolean v0, p0, Lcom/itau/empresas/SearchViewDelay;->isValid:Z

    if-eqz v0, :cond_7

    .line 36
    invoke-direct {p0, p1}, Lcom/itau/empresas/SearchViewDelay;->searchDelay(Ljava/lang/String;)V

    .line 38
    :cond_7
    const/4 v0, 0x0

    return v0
.end method

.class public Lbr/com/itau/textovoz/util/AnimationUtils;
.super Ljava/lang/Object;
.source "AnimationUtils.java"


# direct methods
.method public static collapseAnimation(Landroid/view/View;I)V
    .registers 3
    .param p0, "view"    # Landroid/view/View;
    .param p1, "duration"    # I

    .line 177
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lbr/com/itau/textovoz/util/AnimationUtils;->collapseAnimation(Landroid/view/View;ILandroid/view/animation/Animation$AnimationListener;)V

    .line 178
    return-void
.end method

.method public static collapseAnimation(Landroid/view/View;ILandroid/view/animation/Animation$AnimationListener;)V
    .registers 7
    .param p0, "view"    # Landroid/view/View;
    .param p1, "duration"    # I
    .param p2, "listener"    # Landroid/view/animation/Animation$AnimationListener;

    .line 182
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 184
    .local v2, "initialHeight":I
    new-instance v3, Lbr/com/itau/textovoz/util/AnimationUtils$7;

    invoke-direct {v3, p0, v2}, Lbr/com/itau/textovoz/util/AnimationUtils$7;-><init>(Landroid/view/View;I)V

    .line 203
    .local v3, "a":Landroid/view/animation/Animation;
    int-to-long v0, p1

    invoke-virtual {v3, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 204
    invoke-virtual {v3, p2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 205
    new-instance v0, Landroid/support/v4/view/animation/FastOutSlowInInterpolator;

    invoke-direct {v0}, Landroid/support/v4/view/animation/FastOutSlowInInterpolator;-><init>()V

    invoke-virtual {v3, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 206
    invoke-virtual {p0, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 207
    return-void
.end method

.method public static expandAnimation(Landroid/view/View;I)V
    .registers 3
    .param p0, "view"    # Landroid/view/View;
    .param p1, "duration"    # I

    .line 143
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lbr/com/itau/textovoz/util/AnimationUtils;->expandAnimation(Landroid/view/View;ILandroid/view/animation/Animation$AnimationListener;)V

    .line 144
    return-void
.end method

.method public static expandAnimation(Landroid/view/View;ILandroid/view/animation/Animation$AnimationListener;)V
    .registers 7
    .param p0, "view"    # Landroid/view/View;
    .param p1, "duration"    # I
    .param p2, "animationListener"    # Landroid/view/animation/Animation$AnimationListener;

    .line 148
    const/4 v0, -0x1

    const/4 v1, -0x2

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->measure(II)V

    .line 149
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 151
    .local v2, "targetHeight":I
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 152
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 153
    new-instance v3, Lbr/com/itau/textovoz/util/AnimationUtils$6;

    invoke-direct {v3, p0, v2}, Lbr/com/itau/textovoz/util/AnimationUtils$6;-><init>(Landroid/view/View;I)V

    .line 167
    .local v3, "a":Landroid/view/animation/Animation;
    int-to-long v0, p1

    invoke-virtual {v3, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 168
    new-instance v0, Landroid/support/v4/view/animation/FastOutSlowInInterpolator;

    invoke-direct {v0}, Landroid/support/v4/view/animation/FastOutSlowInInterpolator;-><init>()V

    invoke-virtual {v3, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 169
    if-eqz p2, :cond_2a

    .line 170
    invoke-virtual {v3, p2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 172
    :cond_2a
    invoke-virtual {p0, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 174
    return-void
.end method

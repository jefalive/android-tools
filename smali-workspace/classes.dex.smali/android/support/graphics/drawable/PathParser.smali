.class Landroid/support/graphics/drawable/PathParser;
.super Ljava/lang/Object;
.source "PathParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/graphics/drawable/PathParser$PathDataNode;,
        Landroid/support/graphics/drawable/PathParser$ExtractFloatResult;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 295
    return-void
.end method

.method private static addNode(Ljava/util/ArrayList;C[F)V
    .registers 4
    .param p0, "list"    # Ljava/util/ArrayList;
    .param p1, "cmd"    # C
    .param p2, "val"    # [F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Landroid/support/graphics/drawable/PathParser$PathDataNode;>;C[F)V"
        }
    .end annotation

    .line 178
    new-instance v0, Landroid/support/graphics/drawable/PathParser$PathDataNode;

    invoke-direct {v0, p1, p2}, Landroid/support/graphics/drawable/PathParser$PathDataNode;-><init>(C[F)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 179
    return-void
.end method

.method static copyOfRange([FII)[F
    .registers 8
    .param p0, "original"    # [F
    .param p1, "start"    # I
    .param p2, "end"    # I

    .line 44
    if-le p1, p2, :cond_8

    .line 45
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 47
    :cond_8
    array-length v1, p0

    .line 48
    .local v1, "originalLength":I
    if-ltz p1, :cond_d

    if-le p1, v1, :cond_13

    .line 49
    :cond_d
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 51
    :cond_13
    sub-int v2, p2, p1

    .line 52
    .local v2, "resultLength":I
    sub-int v0, v1, p1

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 53
    .local v3, "copyLength":I
    new-array v4, v2, [F

    .line 54
    .local v4, "result":[F
    const/4 v0, 0x0

    invoke-static {p0, p1, v4, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 55
    return-object v4
.end method

.method public static createNodesFromPathData(Ljava/lang/String;)[Landroid/support/graphics/drawable/PathParser$PathDataNode;
    .registers 8
    .param p0, "pathData"    # Ljava/lang/String;

    .line 81
    if-nez p0, :cond_4

    .line 82
    const/4 v0, 0x0

    return-object v0

    .line 84
    :cond_4
    const/4 v2, 0x0

    .line 85
    .local v2, "start":I
    const/4 v3, 0x1

    .line 87
    .local v3, "end":I
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 88
    .local v4, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/graphics/drawable/PathParser$PathDataNode;>;"
    :goto_b
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v3, v0, :cond_33

    .line 89
    invoke-static {p0, v3}, Landroid/support/graphics/drawable/PathParser;->nextStart(Ljava/lang/String;I)I

    move-result v3

    .line 90
    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 91
    .local v5, "s":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2f

    .line 92
    invoke-static {v5}, Landroid/support/graphics/drawable/PathParser;->getFloats(Ljava/lang/String;)[F

    move-result-object v6

    .line 93
    .local v6, "val":[F
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v4, v0, v6}, Landroid/support/graphics/drawable/PathParser;->addNode(Ljava/util/ArrayList;C[F)V

    .line 96
    .end local v6    # "val":[F
    :cond_2f
    move v2, v3

    .line 97
    add-int/lit8 v3, v3, 0x1

    .line 98
    .end local v5    # "s":Ljava/lang/String;
    goto :goto_b

    .line 99
    :cond_33
    sub-int v0, v3, v2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_48

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v2, v0, :cond_48

    .line 100
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/4 v1, 0x0

    new-array v1, v1, [F

    invoke-static {v4, v0, v1}, Landroid/support/graphics/drawable/PathParser;->addNode(Ljava/util/ArrayList;C[F)V

    .line 102
    :cond_48
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Landroid/support/graphics/drawable/PathParser$PathDataNode;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/support/graphics/drawable/PathParser$PathDataNode;

    return-object v0
.end method

.method public static deepCopyNodes([Landroid/support/graphics/drawable/PathParser$PathDataNode;)[Landroid/support/graphics/drawable/PathParser$PathDataNode;
    .registers 5
    .param p0, "source"    # [Landroid/support/graphics/drawable/PathParser$PathDataNode;

    .line 110
    if-nez p0, :cond_4

    .line 111
    const/4 v0, 0x0

    return-object v0

    .line 113
    :cond_4
    array-length v0, p0

    new-array v2, v0, [Landroid/support/graphics/drawable/PathParser$PathDataNode;

    .line 114
    .local v2, "copy":[Landroid/support/graphics/drawable/PathParser$PathDataNode;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_8
    array-length v0, p0

    if-ge v3, v0, :cond_17

    .line 115
    new-instance v0, Landroid/support/graphics/drawable/PathParser$PathDataNode;

    aget-object v1, p0, v3

    invoke-direct {v0, v1}, Landroid/support/graphics/drawable/PathParser$PathDataNode;-><init>(Landroid/support/graphics/drawable/PathParser$PathDataNode;)V

    aput-object v0, v2, v3

    .line 114
    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    .line 117
    .end local v3    # "i":I
    :cond_17
    return-object v2
.end method

.method private static extract(Ljava/lang/String;ILandroid/support/graphics/drawable/PathParser$ExtractFloatResult;)V
    .registers 10
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "start"    # I
    .param p2, "result"    # Landroid/support/graphics/drawable/PathParser$ExtractFloatResult;

    .line 246
    move v1, p1

    .line 247
    .local v1, "currentIndex":I
    const/4 v2, 0x0

    .line 248
    .local v2, "foundSeparator":Z
    const/4 v0, 0x0

    iput-boolean v0, p2, Landroid/support/graphics/drawable/PathParser$ExtractFloatResult;->mEndWithNegOrDot:Z

    .line 249
    const/4 v3, 0x0

    .line 250
    .local v3, "secondDot":Z
    const/4 v4, 0x0

    .line 251
    .local v4, "isExponential":Z
    :goto_7
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v1, v0, :cond_33

    .line 252
    move v5, v4

    .line 253
    .local v5, "isPrevExponential":Z
    const/4 v4, 0x0

    .line 254
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 255
    .local v6, "currentChar":C
    sparse-switch v6, :sswitch_data_36

    goto :goto_2c

    .line 258
    :sswitch_17
    const/4 v2, 0x1

    .line 259
    goto :goto_2c

    .line 262
    :sswitch_19
    if-eq v1, p1, :cond_2c

    if-nez v5, :cond_2c

    .line 263
    const/4 v2, 0x1

    .line 264
    const/4 v0, 0x1

    iput-boolean v0, p2, Landroid/support/graphics/drawable/PathParser$ExtractFloatResult;->mEndWithNegOrDot:Z

    goto :goto_2c

    .line 268
    :sswitch_22
    if-nez v3, :cond_26

    .line 269
    const/4 v3, 0x1

    goto :goto_2c

    .line 272
    :cond_26
    const/4 v2, 0x1

    .line 273
    const/4 v0, 0x1

    iput-boolean v0, p2, Landroid/support/graphics/drawable/PathParser$ExtractFloatResult;->mEndWithNegOrDot:Z

    .line 275
    goto :goto_2c

    .line 278
    :sswitch_2b
    const/4 v4, 0x1

    .line 281
    :cond_2c
    :goto_2c
    if-eqz v2, :cond_2f

    .line 282
    goto :goto_33

    .line 251
    .end local v5    # "isPrevExponential":Z
    .end local v6    # "currentChar":C
    :cond_2f
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_7

    .line 287
    :cond_33
    :goto_33
    iput v1, p2, Landroid/support/graphics/drawable/PathParser$ExtractFloatResult;->mEndPosition:I

    .line 288
    return-void

    :sswitch_data_36
    .sparse-switch
        0x20 -> :sswitch_17
        0x2c -> :sswitch_17
        0x2d -> :sswitch_19
        0x2e -> :sswitch_22
        0x45 -> :sswitch_2b
        0x65 -> :sswitch_2b
    .end sparse-switch
.end method

.method private static getFloats(Ljava/lang/String;)[F
    .registers 10
    .param p0, "s"    # Ljava/lang/String;

    .line 199
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x7a

    if-ne v0, v1, :cond_b

    const/4 v0, 0x1

    goto :goto_c

    :cond_b
    const/4 v0, 0x0

    :goto_c
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x5a

    if-ne v1, v2, :cond_17

    const/4 v1, 0x1

    goto :goto_18

    :cond_17
    const/4 v1, 0x0

    :goto_18
    or-int/2addr v0, v1

    if-eqz v0, :cond_1f

    .line 200
    const/4 v0, 0x0

    new-array v0, v0, [F

    return-object v0

    .line 203
    :cond_1f
    :try_start_1f
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    new-array v3, v0, [F

    .line 204
    .local v3, "results":[F
    const/4 v4, 0x0

    .line 205
    .local v4, "count":I
    const/4 v5, 0x1

    .line 206
    .local v5, "startPosition":I
    const/4 v6, 0x0

    .line 208
    .local v6, "endPosition":I
    new-instance v7, Landroid/support/graphics/drawable/PathParser$ExtractFloatResult;

    invoke-direct {v7}, Landroid/support/graphics/drawable/PathParser$ExtractFloatResult;-><init>()V

    .line 209
    .local v7, "result":Landroid/support/graphics/drawable/PathParser$ExtractFloatResult;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    .line 214
    .local v8, "totalLength":I
    :goto_31
    if-ge v5, v8, :cond_50

    .line 215
    invoke-static {p0, v5, v7}, Landroid/support/graphics/drawable/PathParser;->extract(Ljava/lang/String;ILandroid/support/graphics/drawable/PathParser$ExtractFloatResult;)V

    .line 216
    iget v6, v7, Landroid/support/graphics/drawable/PathParser$ExtractFloatResult;->mEndPosition:I

    .line 218
    if-ge v5, v6, :cond_47

    .line 219
    move v0, v4

    add-int/lit8 v4, v4, 0x1

    .line 220
    invoke-virtual {p0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 219
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    aput v1, v3, v0

    .line 223
    :cond_47
    iget-boolean v0, v7, Landroid/support/graphics/drawable/PathParser$ExtractFloatResult;->mEndWithNegOrDot:Z

    if-eqz v0, :cond_4d

    .line 225
    move v5, v6

    goto :goto_31

    .line 227
    :cond_4d
    add-int/lit8 v5, v6, 0x1

    goto :goto_31

    .line 230
    :cond_50
    const/4 v0, 0x0

    invoke-static {v3, v0, v4}, Landroid/support/graphics/drawable/PathParser;->copyOfRange([FII)[F
    :try_end_54
    .catch Ljava/lang/NumberFormatException; {:try_start_1f .. :try_end_54} :catch_56

    move-result-object v0

    return-object v0

    .line 231
    .end local v3    # "results":[F
    .end local v4    # "count":I
    .end local v5    # "startPosition":I
    .end local v6    # "endPosition":I
    .end local v7    # "result":Landroid/support/graphics/drawable/PathParser$ExtractFloatResult;
    .end local v8    # "totalLength":I
    :catch_56
    move-exception v3

    .line 232
    .local v3, "e":Ljava/lang/NumberFormatException;
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "error in parsing \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method private static nextStart(Ljava/lang/String;I)I
    .registers 5
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "end"    # I

    .line 162
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge p1, v0, :cond_24

    .line 163
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 168
    .local v2, "c":C
    add-int/lit8 v0, v2, -0x41

    add-int/lit8 v1, v2, -0x5a

    mul-int/2addr v0, v1

    if-lez v0, :cond_18

    add-int/lit8 v0, v2, -0x61

    add-int/lit8 v1, v2, -0x7a

    mul-int/2addr v0, v1

    if-gtz v0, :cond_21

    :cond_18
    const/16 v0, 0x65

    if-eq v2, v0, :cond_21

    const/16 v0, 0x45

    if-eq v2, v0, :cond_21

    .line 170
    return p1

    .line 172
    :cond_21
    add-int/lit8 p1, p1, 0x1

    .end local v2    # "c":C
    goto :goto_0

    .line 174
    :cond_24
    return p1
.end method

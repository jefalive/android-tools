.class Lcom/google/android/gms/internal/zzv$zza;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/zzv;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "zza"
.end annotation


# instance fields
.field public key:Ljava/lang/String;

.field public zzaB:J

.field public zzb:Ljava/lang/String;

.field public zzc:J

.field public zzd:J

.field public zze:J

.field public zzf:J

.field public zzg:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .registers 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/gms/internal/zzb$zza;)V
    .registers 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/zzv$zza;->key:Ljava/lang/String;

    iget-object v0, p2, Lcom/google/android/gms/internal/zzb$zza;->data:[B

    array-length v0, v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzv$zza;->zzaB:J

    iget-object v0, p2, Lcom/google/android/gms/internal/zzb$zza;->zzb:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/internal/zzv$zza;->zzb:Ljava/lang/String;

    iget-wide v0, p2, Lcom/google/android/gms/internal/zzb$zza;->zzc:J

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzv$zza;->zzc:J

    iget-wide v0, p2, Lcom/google/android/gms/internal/zzb$zza;->zzd:J

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzv$zza;->zzd:J

    iget-wide v0, p2, Lcom/google/android/gms/internal/zzb$zza;->zze:J

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzv$zza;->zze:J

    iget-wide v0, p2, Lcom/google/android/gms/internal/zzb$zza;->zzf:J

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzv$zza;->zzf:J

    iget-object v0, p2, Lcom/google/android/gms/internal/zzb$zza;->zzg:Ljava/util/Map;

    iput-object v0, p0, Lcom/google/android/gms/internal/zzv$zza;->zzg:Ljava/util/Map;

    return-void
.end method

.method public static zzf(Ljava/io/InputStream;)Lcom/google/android/gms/internal/zzv$zza;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v2, Lcom/google/android/gms/internal/zzv$zza;

    invoke-direct {v2}, Lcom/google/android/gms/internal/zzv$zza;-><init>()V

    invoke-static {p0}, Lcom/google/android/gms/internal/zzv;->zzb(Ljava/io/InputStream;)I

    move-result v3

    const v0, 0x20150306

    if-eq v3, v0, :cond_14

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    :cond_14
    invoke-static {p0}, Lcom/google/android/gms/internal/zzv;->zzd(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/internal/zzv$zza;->key:Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/gms/internal/zzv;->zzd(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/internal/zzv$zza;->zzb:Ljava/lang/String;

    iget-object v0, v2, Lcom/google/android/gms/internal/zzv$zza;->zzb:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2d

    const/4 v0, 0x0

    iput-object v0, v2, Lcom/google/android/gms/internal/zzv$zza;->zzb:Ljava/lang/String;

    :cond_2d
    invoke-static {p0}, Lcom/google/android/gms/internal/zzv;->zzc(Ljava/io/InputStream;)J

    move-result-wide v0

    iput-wide v0, v2, Lcom/google/android/gms/internal/zzv$zza;->zzc:J

    invoke-static {p0}, Lcom/google/android/gms/internal/zzv;->zzc(Ljava/io/InputStream;)J

    move-result-wide v0

    iput-wide v0, v2, Lcom/google/android/gms/internal/zzv$zza;->zzd:J

    invoke-static {p0}, Lcom/google/android/gms/internal/zzv;->zzc(Ljava/io/InputStream;)J

    move-result-wide v0

    iput-wide v0, v2, Lcom/google/android/gms/internal/zzv$zza;->zze:J

    invoke-static {p0}, Lcom/google/android/gms/internal/zzv;->zzc(Ljava/io/InputStream;)J

    move-result-wide v0

    iput-wide v0, v2, Lcom/google/android/gms/internal/zzv$zza;->zzf:J

    invoke-static {p0}, Lcom/google/android/gms/internal/zzv;->zze(Ljava/io/InputStream;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/internal/zzv$zza;->zzg:Ljava/util/Map;

    return-object v2
.end method


# virtual methods
.method public zza(Ljava/io/OutputStream;)Z
    .registers 7

    const v0, 0x20150306

    :try_start_3
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/zzv;->zza(Ljava/io/OutputStream;I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzv$zza;->key:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/zzv;->zza(Ljava/io/OutputStream;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzv$zza;->zzb:Ljava/lang/String;

    if-nez v0, :cond_12

    const-string v0, ""

    goto :goto_14

    :cond_12
    iget-object v0, p0, Lcom/google/android/gms/internal/zzv$zza;->zzb:Ljava/lang/String;

    :goto_14
    invoke-static {p1, v0}, Lcom/google/android/gms/internal/zzv;->zza(Ljava/io/OutputStream;Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/google/android/gms/internal/zzv$zza;->zzc:J

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/internal/zzv;->zza(Ljava/io/OutputStream;J)V

    iget-wide v0, p0, Lcom/google/android/gms/internal/zzv$zza;->zzd:J

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/internal/zzv;->zza(Ljava/io/OutputStream;J)V

    iget-wide v0, p0, Lcom/google/android/gms/internal/zzv$zza;->zze:J

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/internal/zzv;->zza(Ljava/io/OutputStream;J)V

    iget-wide v0, p0, Lcom/google/android/gms/internal/zzv$zza;->zzf:J

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/internal/zzv;->zza(Ljava/io/OutputStream;J)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzv$zza;->zzg:Ljava/util/Map;

    invoke-static {v0, p1}, Lcom/google/android/gms/internal/zzv;->zza(Ljava/util/Map;Ljava/io/OutputStream;)V

    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V
    :try_end_33
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_33} :catch_35

    const/4 v0, 0x1

    return v0

    :catch_35
    move-exception v4

    const-string v0, "%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzs;->zzb(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x0

    return v0
.end method

.method public zzb([B)Lcom/google/android/gms/internal/zzb$zza;
    .registers 5

    new-instance v2, Lcom/google/android/gms/internal/zzb$zza;

    invoke-direct {v2}, Lcom/google/android/gms/internal/zzb$zza;-><init>()V

    iput-object p1, v2, Lcom/google/android/gms/internal/zzb$zza;->data:[B

    iget-object v0, p0, Lcom/google/android/gms/internal/zzv$zza;->zzb:Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/gms/internal/zzb$zza;->zzb:Ljava/lang/String;

    iget-wide v0, p0, Lcom/google/android/gms/internal/zzv$zza;->zzc:J

    iput-wide v0, v2, Lcom/google/android/gms/internal/zzb$zza;->zzc:J

    iget-wide v0, p0, Lcom/google/android/gms/internal/zzv$zza;->zzd:J

    iput-wide v0, v2, Lcom/google/android/gms/internal/zzb$zza;->zzd:J

    iget-wide v0, p0, Lcom/google/android/gms/internal/zzv$zza;->zze:J

    iput-wide v0, v2, Lcom/google/android/gms/internal/zzb$zza;->zze:J

    iget-wide v0, p0, Lcom/google/android/gms/internal/zzv$zza;->zzf:J

    iput-wide v0, v2, Lcom/google/android/gms/internal/zzb$zza;->zzf:J

    iget-object v0, p0, Lcom/google/android/gms/internal/zzv$zza;->zzg:Ljava/util/Map;

    iput-object v0, v2, Lcom/google/android/gms/internal/zzb$zza;->zzg:Ljava/util/Map;

    return-object v2
.end method

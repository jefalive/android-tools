.class public abstract Lcom/google/android/gms/common/api/internal/zzb;
.super Lcom/google/android/gms/common/api/PendingResult;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/common/api/internal/zzb$zza;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R::Lcom/google/android/gms/common/api/Result;>Lcom/google/android/gms/common/api/PendingResult<TR;>;"
    }
.end annotation


# instance fields
.field private zzL:Z

.field private final zzagI:Ljava/lang/Object;

.field protected final zzagJ:Lcom/google/android/gms/common/api/internal/zzb$zza;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/internal/zzb$zza<TR;>;"
        }
    .end annotation
.end field

.field private final zzagK:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<Lcom/google/android/gms/common/api/GoogleApiClient;>;"
        }
    .end annotation
.end field

.field private final zzagL:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/google/android/gms/common/api/PendingResult$zza;>;"
        }
    .end annotation
.end field

.field private zzagM:Lcom/google/android/gms/common/api/ResultCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/ResultCallback<-TR;>;"
        }
    .end annotation
.end field

.field private volatile zzagN:Z

.field private zzagO:Z

.field private zzagP:Z

.field private zzagQ:Lcom/google/android/gms/common/internal/zzq;

.field private zzagR:Ljava/lang/Integer;

.field private volatile zzagS:Lcom/google/android/gms/common/api/internal/zzx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/internal/zzx<TR;>;"
        }
    .end annotation
.end field

.field private volatile zzagy:Lcom/google/android/gms/common/api/Result;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field

.field private final zzpJ:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/common/api/GoogleApiClient;)V
    .registers 5

    invoke-direct {p0}, Lcom/google/android/gms/common/api/PendingResult;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagI:Ljava/lang/Object;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzpJ:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagL:Ljava/util/ArrayList;

    if-eqz p1, :cond_20

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/GoogleApiClient;->getLooper()Landroid/os/Looper;

    move-result-object v2

    goto :goto_24

    :cond_20
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    :goto_24
    new-instance v0, Lcom/google/android/gms/common/api/internal/zzb$zza;

    invoke-direct {v0, v2}, Lcom/google/android/gms/common/api/internal/zzb$zza;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagJ:Lcom/google/android/gms/common/api/internal/zzb$zza;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagK:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method private get()Lcom/google/android/gms/common/api/Result;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TR;"
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagI:Ljava/lang/Object;

    monitor-enter v3

    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagN:Z

    if-nez v0, :cond_9

    const/4 v0, 0x1

    goto :goto_a

    :cond_9
    const/4 v0, 0x0

    :goto_a
    const-string v1, "Result has already been consumed."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->zza(ZLjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzb;->isReady()Z

    move-result v0

    const-string v1, "Result is not ready."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->zza(ZLjava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagy:Lcom/google/android/gms/common/api/Result;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagy:Lcom/google/android/gms/common/api/Result;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagM:Lcom/google/android/gms/common/api/ResultCallback;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagN:Z
    :try_end_23
    .catchall {:try_start_3 .. :try_end_23} :catchall_25

    monitor-exit v3

    goto :goto_28

    :catchall_25
    move-exception v4

    monitor-exit v3

    throw v4

    :goto_28
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzb;->zzpf()V

    return-object v2
.end method

.method private zzb(Lcom/google/android/gms/common/api/Result;)V
    .registers 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagy:Lcom/google/android/gms/common/api/Result;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagQ:Lcom/google/android/gms/common/internal/zzq;

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzpJ:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagy:Lcom/google/android/gms/common/api/Result;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/Result;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagM:Lcom/google/android/gms/common/api/ResultCallback;

    if-eqz v0, :cond_28

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagJ:Lcom/google/android/gms/common/api/internal/zzb$zza;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/internal/zzb$zza;->zzph()V

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzL:Z

    if-nez v0, :cond_28

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagJ:Lcom/google/android/gms/common/api/internal/zzb$zza;

    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagM:Lcom/google/android/gms/common/api/ResultCallback;

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzb;->get()Lcom/google/android/gms/common/api/Result;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/api/internal/zzb$zza;->zza(Lcom/google/android/gms/common/api/ResultCallback;Lcom/google/android/gms/common/api/Result;)V

    :cond_28
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagL:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3f

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/gms/common/api/PendingResult$zza;

    invoke-interface {v5, v3}, Lcom/google/android/gms/common/api/PendingResult$zza;->zzu(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_2e

    :cond_3f
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagL:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public static zzc(Lcom/google/android/gms/common/api/Result;)V
    .registers 5

    instance-of v0, p0, Lcom/google/android/gms/common/api/Releasable;

    if-eqz v0, :cond_24

    move-object v0, p0

    :try_start_5
    check-cast v0, Lcom/google/android/gms/common/api/Releasable;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/Releasable;->release()V
    :try_end_a
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_a} :catch_b

    goto :goto_24

    :catch_b
    move-exception v3

    const-string v0, "BasePendingResult"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to release "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_24
    :goto_24
    return-void
.end method


# virtual methods
.method public cancel()V
    .registers 5

    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagI:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzL:Z

    if-nez v0, :cond_b

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagN:Z
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_2e

    if-eqz v0, :cond_d

    :cond_b
    monitor-exit v1

    return-void

    :cond_d
    :try_start_d
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagQ:Lcom/google/android/gms/common/internal/zzq;
    :try_end_f
    .catchall {:try_start_d .. :try_end_f} :catchall_2e

    if-eqz v0, :cond_18

    :try_start_11
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagQ:Lcom/google/android/gms/common/internal/zzq;

    invoke-interface {v0}, Lcom/google/android/gms/common/internal/zzq;->cancel()V
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_16} :catch_17
    .catchall {:try_start_11 .. :try_end_16} :catchall_2e

    goto :goto_18

    :catch_17
    move-exception v2

    :cond_18
    :goto_18
    :try_start_18
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagy:Lcom/google/android/gms/common/api/Result;

    invoke-static {v0}, Lcom/google/android/gms/common/api/internal/zzb;->zzc(Lcom/google/android/gms/common/api/Result;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagM:Lcom/google/android/gms/common/api/ResultCallback;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzL:Z

    sget-object v0, Lcom/google/android/gms/common/api/Status;->zzagG:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/internal/zzb;->zzc(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/internal/zzb;->zzb(Lcom/google/android/gms/common/api/Result;)V
    :try_end_2c
    .catchall {:try_start_18 .. :try_end_2c} :catchall_2e

    monitor-exit v1

    goto :goto_31

    :catchall_2e
    move-exception v3

    monitor-exit v1

    throw v3

    :goto_31
    return-void
.end method

.method public isCanceled()Z
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagI:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzL:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_7

    monitor-exit v1

    return v0

    :catchall_7
    move-exception v2

    monitor-exit v1

    throw v2
.end method

.method public final isReady()Z
    .registers 5

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzpJ:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_e

    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

.method public final setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V
    .registers 7
    .param p1, "callback"    # Lcom/google/android/gms/common/api/ResultCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/google/android/gms/common/api/ResultCallback<-TR;>;)V"
        }
    .end annotation

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagN:Z

    if-nez v0, :cond_6

    const/4 v0, 0x1

    goto :goto_7

    :cond_6
    const/4 v0, 0x0

    :goto_7
    const-string v1, "Result has already been consumed."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->zza(ZLjava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagI:Ljava/lang/Object;

    monitor-enter v2

    :try_start_f
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagS:Lcom/google/android/gms/common/api/internal/zzx;

    if-nez v0, :cond_15

    const/4 v0, 0x1

    goto :goto_16

    :cond_15
    const/4 v0, 0x0

    :goto_16
    const-string v1, "Cannot set callbacks if then() has been called."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->zza(ZLjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzb;->isCanceled()Z
    :try_end_1e
    .catchall {:try_start_f .. :try_end_1e} :catchall_4f

    move-result v0

    if-eqz v0, :cond_23

    monitor-exit v2

    return-void

    :cond_23
    :try_start_23
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagP:Z

    if-eqz v0, :cond_3b

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagK:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/google/android/gms/common/api/GoogleApiClient;

    if-eqz v3, :cond_36

    instance-of v0, p1, Lcom/google/android/gms/common/api/internal/zzx;

    if-nez v0, :cond_3b

    :cond_36
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzb;->cancel()V
    :try_end_39
    .catchall {:try_start_23 .. :try_end_39} :catchall_4f

    monitor-exit v2

    return-void

    :cond_3b
    :try_start_3b
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzb;->isReady()Z

    move-result v0

    if-eqz v0, :cond_4b

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagJ:Lcom/google/android/gms/common/api/internal/zzb$zza;

    invoke-direct {p0}, Lcom/google/android/gms/common/api/internal/zzb;->get()Lcom/google/android/gms/common/api/Result;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/common/api/internal/zzb$zza;->zza(Lcom/google/android/gms/common/api/ResultCallback;Lcom/google/android/gms/common/api/Result;)V

    goto :goto_4d

    :cond_4b
    iput-object p1, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagM:Lcom/google/android/gms/common/api/ResultCallback;
    :try_end_4d
    .catchall {:try_start_3b .. :try_end_4d} :catchall_4f

    :goto_4d
    monitor-exit v2

    goto :goto_52

    :catchall_4f
    move-exception v4

    monitor-exit v2

    throw v4

    :goto_52
    return-void
.end method

.method public final zza(Lcom/google/android/gms/common/api/PendingResult$zza;)V
    .registers 6

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagN:Z

    if-nez v0, :cond_6

    const/4 v0, 0x1

    goto :goto_7

    :cond_6
    const/4 v0, 0x0

    :goto_7
    const-string v1, "Result has already been consumed."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->zza(ZLjava/lang/Object;)V

    if-eqz p1, :cond_10

    const/4 v0, 0x1

    goto :goto_11

    :cond_10
    const/4 v0, 0x0

    :goto_11
    const-string v1, "Callback cannot be null."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->zzb(ZLjava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagI:Ljava/lang/Object;

    monitor-enter v2

    :try_start_19
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzb;->isReady()Z

    move-result v0

    if-eqz v0, :cond_29

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagy:Lcom/google/android/gms/common/api/Result;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/Result;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/PendingResult$zza;->zzu(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_2e

    :cond_29
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagL:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2e
    .catchall {:try_start_19 .. :try_end_2e} :catchall_30

    :goto_2e
    monitor-exit v2

    goto :goto_33

    :catchall_30
    move-exception v3

    monitor-exit v2

    throw v3

    :goto_33
    return-void
.end method

.method public final zza(Lcom/google/android/gms/common/api/Result;)V
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagI:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagO:Z

    if-nez v0, :cond_b

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzL:Z

    if-eqz v0, :cond_10

    :cond_b
    invoke-static {p1}, Lcom/google/android/gms/common/api/internal/zzb;->zzc(Lcom/google/android/gms/common/api/Result;)V
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_2f

    monitor-exit v2

    return-void

    :cond_10
    :try_start_10
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzb;->isReady()Z

    move-result v0

    if-nez v0, :cond_18

    const/4 v0, 0x1

    goto :goto_19

    :cond_18
    const/4 v0, 0x0

    :goto_19
    const-string v1, "Results have already been set"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->zza(ZLjava/lang/Object;)V

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagN:Z

    if-nez v0, :cond_24

    const/4 v0, 0x1

    goto :goto_25

    :cond_24
    const/4 v0, 0x0

    :goto_25
    const-string v1, "Result has already been consumed"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/zzx;->zza(ZLjava/lang/Object;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/internal/zzb;->zzb(Lcom/google/android/gms/common/api/Result;)V
    :try_end_2d
    .catchall {:try_start_10 .. :try_end_2d} :catchall_2f

    monitor-exit v2

    goto :goto_32

    :catchall_2f
    move-exception v3

    monitor-exit v2

    throw v3

    :goto_32
    return-void
.end method

.method protected abstract zzc(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lcom/google/android/gms/common/api/Status;)TR;"
        }
    .end annotation
.end method

.method public zzpa()Ljava/lang/Integer;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagR:Ljava/lang/Integer;

    return-object v0
.end method

.method protected zzpf()V
    .registers 1

    return-void
.end method

.method public zzpg()V
    .registers 5

    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagI:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagK:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/common/api/GoogleApiClient;

    if-nez v2, :cond_13

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzb;->cancel()V
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_26

    monitor-exit v1

    return-void

    :cond_13
    :try_start_13
    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagM:Lcom/google/android/gms/common/api/ResultCallback;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagM:Lcom/google/android/gms/common/api/ResultCallback;

    instance-of v0, v0, Lcom/google/android/gms/common/api/internal/zzx;

    if-eqz v0, :cond_21

    :cond_1d
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagP:Z

    goto :goto_24

    :cond_21
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzb;->cancel()V
    :try_end_24
    .catchall {:try_start_13 .. :try_end_24} :catchall_26

    :goto_24
    monitor-exit v1

    goto :goto_29

    :catchall_26
    move-exception v3

    monitor-exit v1

    throw v3

    :goto_29
    return-void
.end method

.method public final zzx(Lcom/google/android/gms/common/api/Status;)V
    .registers 5

    iget-object v1, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagI:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/internal/zzb;->isReady()Z

    move-result v0

    if-nez v0, :cond_13

    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/api/internal/zzb;->zzc(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Result;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/internal/zzb;->zza(Lcom/google/android/gms/common/api/Result;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/internal/zzb;->zzagO:Z
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_15

    :cond_13
    monitor-exit v1

    goto :goto_18

    :catchall_15
    move-exception v2

    monitor-exit v1

    throw v2

    :goto_18
    return-void
.end method

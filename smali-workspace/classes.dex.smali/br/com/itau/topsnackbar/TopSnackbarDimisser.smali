.class Lbr/com/itau/topsnackbar/TopSnackbarDimisser;
.super Ljava/lang/Object;
.source "TopSnackbarDimisser.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final snackbar:Lbr/com/itau/topsnackbar/TopSnackbar;


# direct methods
.method constructor <init>(Lbr/com/itau/topsnackbar/TopSnackbar;)V
    .registers 2
    .param p1, "snackbar"    # Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput-object p1, p0, Lbr/com/itau/topsnackbar/TopSnackbarDimisser;->snackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 8
    return-void
.end method


# virtual methods
.method public run()V
    .registers 2

    .line 11
    iget-object v0, p0, Lbr/com/itau/topsnackbar/TopSnackbarDimisser;->snackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismiss()V

    .line 12
    return-void
.end method

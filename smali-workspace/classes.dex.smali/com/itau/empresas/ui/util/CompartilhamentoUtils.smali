.class public final Lcom/itau/empresas/ui/util/CompartilhamentoUtils;
.super Ljava/lang/Object;
.source "CompartilhamentoUtils.java"


# direct methods
.method public static compartilharArquivoImagem(Landroid/app/Activity;Ljava/io/File;)V
    .registers 5
    .param p0, "mContext"    # Landroid/app/Activity;
    .param p1, "imageFile"    # Ljava/io/File;

    .line 20
    invoke-static {}, Lcom/itau/empresas/ui/util/CompartilhamentoUtils;->criaIntent()Landroid/content/Intent;

    move-result-object v1

    .line 21
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 22
    .local v2, "uri":Landroid/net/Uri;
    const-string v0, "image/*"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 23
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 24
    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 25
    return-void
.end method

.method public static compartilharArquivoImagemPorUri(Landroid/app/Activity;Landroid/net/Uri;Ljava/lang/String;)V
    .registers 5
    .param p0, "context"    # Landroid/app/Activity;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "title"    # Ljava/lang/String;

    .line 49
    new-instance v1, Landroid/content/Intent;

    const-string v0, "android.intent.action.SEND"

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 50
    .local v1, "intent":Landroid/content/Intent;
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 51
    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 52
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 53
    invoke-static {v1, p2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 54
    return-void
.end method

.method public static compartilharPDF(Landroid/app/Activity;Ljava/lang/String;)V
    .registers 4
    .param p0, "mContext"    # Landroid/app/Activity;
    .param p1, "url"    # Ljava/lang/String;

    .line 28
    invoke-static {}, Lcom/itau/empresas/ui/util/CompartilhamentoUtils;->criaIntent()Landroid/content/Intent;

    move-result-object v1

    .line 29
    .local v1, "sendIntent":Landroid/content/Intent;
    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 30
    const-string v0, "text/plain"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 31
    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 32
    return-void
.end method

.method private static criaIntent()Landroid/content/Intent;
    .registers 2

    .line 35
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

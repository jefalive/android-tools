.class public Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils;
.super Ljava/lang/Object;
.source "AcessibilidadeUtils.java"


# static fields
.field private static final LOCALE_BRAZIL:Ljava/util/Locale;

.field private static initializedWithSuccess:Z

.field private static textToSpeech:Landroid/speech/tts/TextToSpeech;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 27
    new-instance v0, Ljava/util/Locale;

    const-string v1, "pt"

    const-string v2, "BR"

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils;->LOCALE_BRAZIL:Ljava/util/Locale;

    return-void
.end method

.method static synthetic access$000()Ljava/util/Locale;
    .registers 1

    .line 25
    sget-object v0, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils;->LOCALE_BRAZIL:Ljava/util/Locale;

    return-object v0
.end method

.method static synthetic access$100()Landroid/speech/tts/TextToSpeech;
    .registers 1

    .line 25
    sget-object v0, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils;->textToSpeech:Landroid/speech/tts/TextToSpeech;

    return-object v0
.end method

.method static synthetic access$202(Z)Z
    .registers 1
    .param p0, "x0"    # Z

    .line 25
    sput-boolean p0, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils;->initializedWithSuccess:Z

    return p0
.end method

.method private static accessibilityIsActive(Landroid/content/Context;)Z
    .registers 5
    .param p0, "context"    # Landroid/content/Context;

    .line 93
    const-string v0, "accessibility"

    .line 94
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/view/accessibility/AccessibilityManager;

    .line 96
    .local v1, "am":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    .line 97
    .local v2, "isAccessibilityEnabledFlag":Z
    const/4 v3, 0x0

    .line 99
    .local v3, "isExploreByTouchEnabledFlag":Z
    invoke-static {p0}, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils;->isScreenReaderActivePreAndPostAPI14(Landroid/content/Context;)Z

    move-result v3

    .line 101
    if-eqz v2, :cond_18

    if-eqz v3, :cond_18

    const/4 v0, 0x1

    goto :goto_19

    :cond_18
    const/4 v0, 0x0

    :goto_19
    return v0
.end method

.method public static aplicaAcessibilidadeTabLayout(Landroid/content/Context;Landroid/support/design/widget/TabLayout$Tab;II)V
    .registers 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "tab"    # Landroid/support/design/widget/TabLayout$Tab;
    .param p2, "indice"    # I
    .param p3, "totalAbas"    # I

    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/support/design/widget/TabLayout$Tab;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    .line 144
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 145
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 143
    const v2, 0x7f0700a0

    invoke-virtual {p0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 142
    invoke-virtual {p1, v0}, Landroid/support/design/widget/TabLayout$Tab;->setContentDescription(Ljava/lang/CharSequence;)Landroid/support/design/widget/TabLayout$Tab;

    .line 146
    return-void
.end method

.method private static isScreenReaderActivePreAndPostAPI14(Landroid/content/Context;)Z
    .registers 16
    .param p0, "context"    # Landroid/content/Context;

    .line 106
    new-instance v6, Landroid/content/Intent;

    const-string v0, "android.accessibilityservice.AccessibilityService"

    invoke-direct {v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 107
    .local v6, "screenReaderIntent":Landroid/content/Intent;
    const-string v0, "android.accessibilityservice.category.FEEDBACK_SPOKEN"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 109
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 110
    const/4 v1, 0x0

    invoke-virtual {v0, v6, v1}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v7

    .line 111
    .local v7, "screenReaders":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 112
    .local v8, "cr":Landroid/content/ContentResolver;
    const/4 v9, 0x0

    .line 115
    .local v9, "cursor":Landroid/database/Cursor;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 117
    .local v11, "runningServices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v0, "activity"

    .line 118
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Landroid/app/ActivityManager;

    .line 119
    .line 120
    .local v12, "manager":Landroid/app/ActivityManager;
    const v0, 0x7fffffff

    invoke-virtual {v12, v0}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v0

    .line 119
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_33
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4a

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 121
    .local v14, "service":Landroid/app/ActivityManager$RunningServiceInfo;
    iget-object v0, v14, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    .end local v14    # "service":Landroid/app/ActivityManager$RunningServiceInfo;
    goto :goto_33

    .line 124
    :cond_4a
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a5

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Landroid/content/pm/ResolveInfo;

    .line 125
    .local v14, "screenReader":Landroid/content/pm/ResolveInfo;
    move-object v0, v8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "content://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v14, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v2, v2, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".providers.StatusProvider"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 128
    if-eqz v9, :cond_9c

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_9c

    .line 129
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 130
    .local v10, "status":I
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 131
    const/4 v0, 0x1

    if-ne v10, v0, :cond_9a

    const/4 v0, 0x1

    goto :goto_9b

    :cond_9a
    const/4 v0, 0x0

    :goto_9b
    return v0

    .line 133
    .end local v10    # "status":I
    :cond_9c
    iget-object v0, v14, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    invoke-interface {v11, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 137
    .end local v14    # "screenReader":Landroid/content/pm/ResolveInfo;
    :cond_a5
    const/4 v0, 0x0

    return v0
.end method

.method public static setRequestFocus(Landroid/view/View;)V
    .registers 4
    .param p0, "view"    # Landroid/view/View;

    .line 60
    if-nez p0, :cond_3

    .line 61
    return-void

    .line 64
    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    .line 65
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/accessibility/AccessibilityManager;

    .line 66
    .local v2, "manager":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 67
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 69
    :cond_1b
    return-void
.end method

.method public static textToSpeech(Landroid/content/Context;Ljava/lang/String;)V
    .registers 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "toSpeak"    # Ljava/lang/String;

    .line 72
    invoke-static {p0}, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils;->accessibilityIsActive(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 73
    sget-object v0, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils;->textToSpeech:Landroid/speech/tts/TextToSpeech;

    if-nez v0, :cond_16

    .line 74
    new-instance v0, Landroid/speech/tts/TextToSpeech;

    new-instance v1, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils$1;

    invoke-direct {v1, p0, p1}, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils$1;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-direct {v0, p0, v1}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    sput-object v0, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils;->textToSpeech:Landroid/speech/tts/TextToSpeech;

    .line 85
    :cond_16
    sget-boolean v0, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils;->initializedWithSuccess:Z

    if-eqz v0, :cond_21

    .line 86
    sget-object v0, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils;->textToSpeech:Landroid/speech/tts/TextToSpeech;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    .line 90
    :cond_21
    return-void
.end method

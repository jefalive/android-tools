.class public Lcom/itau/empresas/controller/MapasController;
.super Lcom/itau/empresas/controller/BaseController;
.source "MapasController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/itau/empresas/controller/BaseController<Lcom/itau/empresas/api/Api;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 19
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/controller/MapasController;Ljava/lang/String;)Ljava/util/List;
    .registers 3
    .param p0, "x0"    # Lcom/itau/empresas/controller/MapasController;
    .param p1, "x1"    # Ljava/lang/String;

    .line 19
    invoke-direct {p0, p1}, Lcom/itau/empresas/controller/MapasController;->getReposta(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private getReposta(Ljava/lang/String;)Ljava/util/List;
    .registers 8
    .param p1, "resposta"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;"
        }
    .end annotation

    .line 47
    new-instance v1, Lcom/google/gson/Gson;

    invoke-direct {v1}, Lcom/google/gson/Gson;-><init>()V

    .line 49
    .local v1, "gson":Lcom/google/gson/Gson;
    const-class v0, Lcom/itau/empresas/api/model/RetornoAgenciasVO;

    invoke-virtual {v1, p1, v0}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/api/model/RetornoAgenciasVO;

    .line 50
    .local v2, "retorno":Lcom/itau/empresas/api/model/RetornoAgenciasVO;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 51
    .local v3, "agencias":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;"
    if-eqz v2, :cond_2f

    .line 52
    invoke-virtual {v2}, Lcom/itau/empresas/api/model/RetornoAgenciasVO;->getData()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1d
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/util/List;

    .line 53
    .local v5, "lista":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;"
    invoke-interface {v3, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 54
    .end local v5    # "lista":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;"
    .end local v5
    goto :goto_1d

    .line 55
    :cond_2e
    return-object v3

    .line 57
    :cond_2f
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public buscaAgencias(Lcom/itau/empresas/api/model/FiltroAgenciaVO;)V
    .registers 3
    .param p1, "filtro"    # Lcom/itau/empresas/api/model/FiltroAgenciaVO;

    .line 22
    new-instance v0, Lcom/itau/empresas/controller/MapasController$1;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/controller/MapasController$1;-><init>(Lcom/itau/empresas/controller/MapasController;Lcom/itau/empresas/api/model/FiltroAgenciaVO;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 44
    return-void
.end method

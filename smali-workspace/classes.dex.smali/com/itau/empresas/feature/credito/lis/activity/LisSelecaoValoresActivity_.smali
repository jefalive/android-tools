.class public final Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;
.super Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;
.source "LisSelecaoValoresActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 35
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;-><init>()V

    .line 39
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;

    .line 35
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;

    .line 35
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 53
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 54
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 55
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 56
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;->injectExtras_()V

    .line 57
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;->afterInject()V

    .line 58
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 101
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 102
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_2c

    .line 103
    const-string v0, "ofertaContratacaoLisVo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 104
    const-string v0, "ofertaContratacaoLisVo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    .line 106
    :cond_1c
    const-string v0, "possuiProdutoLisAdiciona"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 107
    const-string v0, "possuiProdutoLisAdiciona"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;->possuiProdutoLisAdiciona:Z

    .line 110
    :cond_2c
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 135
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_$2;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 143
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 123
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_$1;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 131
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 45
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 46
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;->init_(Landroid/os/Bundle;)V

    .line 47
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->onCreate(Landroid/os/Bundle;)V

    .line 48
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 49
    const v0, 0x7f03004c

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;->setContentView(I)V

    .line 50
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 92
    const v0, 0x7f0e0242

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 93
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 94
    const v0, 0x7f0e0241

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;->valorPreAprovado:Landroid/widget/TextView;

    .line 95
    const v0, 0x7f0e0240

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;->valorTaxa:Landroid/widget/TextView;

    .line 96
    const v0, 0x7f0e023e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;->rlLisSelecaoValores:Landroid/view/View;

    .line 97
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;->aoCarregarTela()V

    .line 98
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 62
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->setContentView(I)V

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 64
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 74
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->setContentView(Landroid/view/View;)V

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 76
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 68
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 70
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 114
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity;->setIntent(Landroid/content/Intent;)V

    .line 115
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSelecaoValoresActivity_;->injectExtras_()V

    .line 116
    return-void
.end method

.class public final Lokhttp3/Cookie;
.super Ljava/lang/Object;
.source "Cookie.java"


# static fields
.field private static final DAY_OF_MONTH_PATTERN:Ljava/util/regex/Pattern;

.field private static final MONTH_PATTERN:Ljava/util/regex/Pattern;

.field private static final TIME_PATTERN:Ljava/util/regex/Pattern;

.field private static final YEAR_PATTERN:Ljava/util/regex/Pattern;


# instance fields
.field private final domain:Ljava/lang/String;

.field private final expiresAt:J

.field private final hostOnly:Z

.field private final httpOnly:Z

.field private final name:Ljava/lang/String;

.field private final path:Ljava/lang/String;

.field private final persistent:Z

.field private final secure:Z

.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 44
    const-string v0, "(\\d{2,4})[^\\d]*"

    .line 45
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lokhttp3/Cookie;->YEAR_PATTERN:Ljava/util/regex/Pattern;

    .line 46
    const-string v0, "(?i)(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec).*"

    .line 47
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lokhttp3/Cookie;->MONTH_PATTERN:Ljava/util/regex/Pattern;

    .line 48
    const-string v0, "(\\d{1,2})[^\\d]*"

    .line 49
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lokhttp3/Cookie;->DAY_OF_MONTH_PATTERN:Ljava/util/regex/Pattern;

    .line 50
    const-string v0, "(\\d{1,2}):(\\d{1,2}):(\\d{1,2})[^\\d]*"

    .line 51
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lokhttp3/Cookie;->TIME_PATTERN:Ljava/util/regex/Pattern;

    .line 50
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;ZZZZ)V
    .registers 11
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "expiresAt"    # J
    .param p5, "domain"    # Ljava/lang/String;
    .param p6, "path"    # Ljava/lang/String;
    .param p7, "secure"    # Z
    .param p8, "httpOnly"    # Z
    .param p9, "hostOnly"    # Z
    .param p10, "persistent"    # Z

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lokhttp3/Cookie;->name:Ljava/lang/String;

    .line 67
    iput-object p2, p0, Lokhttp3/Cookie;->value:Ljava/lang/String;

    .line 68
    iput-wide p3, p0, Lokhttp3/Cookie;->expiresAt:J

    .line 69
    iput-object p5, p0, Lokhttp3/Cookie;->domain:Ljava/lang/String;

    .line 70
    iput-object p6, p0, Lokhttp3/Cookie;->path:Ljava/lang/String;

    .line 71
    iput-boolean p7, p0, Lokhttp3/Cookie;->secure:Z

    .line 72
    iput-boolean p8, p0, Lokhttp3/Cookie;->httpOnly:Z

    .line 73
    iput-boolean p9, p0, Lokhttp3/Cookie;->hostOnly:Z

    .line 74
    iput-boolean p10, p0, Lokhttp3/Cookie;->persistent:Z

    .line 75
    return-void
.end method

.method private static dateCharacterOffset(Ljava/lang/String;IIZ)I
    .registers 8
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "pos"    # I
    .param p2, "limit"    # I
    .param p3, "invert"    # Z

    .line 380
    move v1, p1

    .local v1, "i":I
    :goto_1
    if-ge v1, p2, :cond_3d

    .line 381
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 382
    .local v2, "c":I
    const/16 v0, 0x20

    if-ge v2, v0, :cond_f

    const/16 v0, 0x9

    if-ne v2, v0, :cond_2f

    :cond_f
    const/16 v0, 0x7f

    if-ge v2, v0, :cond_2f

    const/16 v0, 0x30

    if-lt v2, v0, :cond_1b

    const/16 v0, 0x39

    if-le v2, v0, :cond_2f

    :cond_1b
    const/16 v0, 0x61

    if-lt v2, v0, :cond_23

    const/16 v0, 0x7a

    if-le v2, v0, :cond_2f

    :cond_23
    const/16 v0, 0x41

    if-lt v2, v0, :cond_2b

    const/16 v0, 0x5a

    if-le v2, v0, :cond_2f

    :cond_2b
    const/16 v0, 0x3a

    if-ne v2, v0, :cond_31

    :cond_2f
    const/4 v3, 0x1

    goto :goto_32

    :cond_31
    const/4 v3, 0x0

    .line 387
    .local v3, "dateCharacter":Z
    :goto_32
    if-nez p3, :cond_36

    const/4 v0, 0x1

    goto :goto_37

    :cond_36
    const/4 v0, 0x0

    :goto_37
    if-ne v3, v0, :cond_3a

    return v1

    .line 380
    .end local v2    # "c":I
    .end local v3    # "dateCharacter":Z
    :cond_3a
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 389
    .end local v1    # "i":I
    :cond_3d
    return p2
.end method

.method private static domainMatch(Lokhttp3/HttpUrl;Ljava/lang/String;)Z
    .registers 5
    .param p0, "url"    # Lokhttp3/HttpUrl;
    .param p1, "domain"    # Ljava/lang/String;

    .line 183
    invoke-virtual {p0}, Lokhttp3/HttpUrl;->host()Ljava/lang/String;

    move-result-object v2

    .line 185
    .local v2, "urlHost":Ljava/lang/String;
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 186
    const/4 v0, 0x1

    return v0

    .line 189
    :cond_c
    invoke-virtual {v2, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 190
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2e

    if-ne v0, v1, :cond_2d

    .line 191
    invoke-static {v2}, Lokhttp3/internal/Util;->verifyAsIpAddress(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2d

    .line 192
    const/4 v0, 0x1

    return v0

    .line 195
    :cond_2d
    const/4 v0, 0x0

    return v0
.end method

.method static parse(JLokhttp3/HttpUrl;Ljava/lang/String;)Lokhttp3/Cookie;
    .registers 36
    .param p0, "currentTimeMillis"    # J
    .param p2, "url"    # Lokhttp3/HttpUrl;
    .param p3, "setCookie"    # Ljava/lang/String;

    .line 222
    const/4 v11, 0x0

    .line 223
    .local v11, "pos":I
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v12

    .line 224
    .local v12, "limit":I
    move-object/from16 v0, p3

    const/16 v1, 0x3b

    invoke-static {v0, v11, v12, v1}, Lokhttp3/internal/Util;->delimiterOffset(Ljava/lang/String;IIC)I

    move-result v13

    .line 226
    .local v13, "cookiePairEnd":I
    move-object/from16 v0, p3

    const/16 v1, 0x3d

    invoke-static {v0, v11, v13, v1}, Lokhttp3/internal/Util;->delimiterOffset(Ljava/lang/String;IIC)I

    move-result v14

    .line 227
    .local v14, "pairEqualsSign":I
    if-ne v14, v13, :cond_19

    const/4 v0, 0x0

    return-object v0

    .line 229
    :cond_19
    move-object/from16 v0, p3

    invoke-static {v0, v11, v14}, Lokhttp3/internal/Util;->trimSubstring(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v15

    .line 230
    .local v15, "cookieName":Ljava/lang/String;
    invoke-virtual {v15}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_27

    const/4 v0, 0x0

    return-object v0

    .line 232
    :cond_27
    add-int/lit8 v0, v14, 0x1

    move-object/from16 v1, p3

    invoke-static {v1, v0, v13}, Lokhttp3/internal/Util;->trimSubstring(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v16

    .line 234
    .local v16, "cookieValue":Ljava/lang/String;
    const-wide v17, 0xe677d21fdbffL

    .line 235
    .local v17, "expiresAt":J
    const-wide/16 v19, -0x1

    .line 236
    .local v19, "deltaSeconds":J
    const/16 v21, 0x0

    .line 237
    .local v21, "domain":Ljava/lang/String;
    const/16 v22, 0x0

    .line 238
    .local v22, "path":Ljava/lang/String;
    const/16 v23, 0x0

    .line 239
    .local v23, "secureOnly":Z
    const/16 v24, 0x0

    .line 240
    .local v24, "httpOnly":Z
    const/16 v25, 0x1

    .line 241
    .local v25, "hostOnly":Z
    const/16 v26, 0x0

    .line 243
    .local v26, "persistent":Z
    add-int/lit8 v11, v13, 0x1

    .line 244
    :goto_44
    if-ge v11, v12, :cond_e3

    .line 245
    move-object/from16 v0, p3

    const/16 v1, 0x3b

    invoke-static {v0, v11, v12, v1}, Lokhttp3/internal/Util;->delimiterOffset(Ljava/lang/String;IIC)I

    move-result v27

    .line 247
    .local v27, "attributePairEnd":I
    move-object/from16 v0, p3

    move/from16 v1, v27

    const/16 v2, 0x3d

    invoke-static {v0, v11, v1, v2}, Lokhttp3/internal/Util;->delimiterOffset(Ljava/lang/String;IIC)I

    move-result v28

    .line 248
    .local v28, "attributeEqualsSign":I
    move-object/from16 v0, p3

    move/from16 v1, v28

    invoke-static {v0, v11, v1}, Lokhttp3/internal/Util;->trimSubstring(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v29

    .line 249
    .local v29, "attributeName":Ljava/lang/String;
    move/from16 v0, v28

    move/from16 v1, v27

    if-ge v0, v1, :cond_71

    add-int/lit8 v0, v28, 0x1

    .line 250
    move-object/from16 v1, p3

    move/from16 v2, v27

    invoke-static {v1, v0, v2}, Lokhttp3/internal/Util;->trimSubstring(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v30

    goto :goto_73

    :cond_71
    const-string v30, ""

    .line 253
    .local v30, "attributeValue":Ljava/lang/String;
    :goto_73
    const-string v0, "expires"

    move-object/from16 v1, v29

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8f

    .line 255
    :try_start_7d
    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v0

    move-object/from16 v1, v30

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Lokhttp3/Cookie;->parseExpires(Ljava/lang/String;II)J
    :try_end_87
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7d .. :try_end_87} :catch_8d

    move-result-wide v0

    move-wide/from16 v17, v0

    .line 256
    const/16 v26, 0x1

    .line 259
    goto :goto_df

    .line 257
    :catch_8d
    move-exception v31

    .line 259
    goto :goto_df

    .line 260
    :cond_8f
    const-string v0, "max-age"

    move-object/from16 v1, v29

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a4

    .line 262
    :try_start_99
    invoke-static/range {v30 .. v30}, Lokhttp3/Cookie;->parseMaxAge(Ljava/lang/String;)J
    :try_end_9c
    .catch Ljava/lang/NumberFormatException; {:try_start_99 .. :try_end_9c} :catch_a2

    move-result-wide v0

    move-wide/from16 v19, v0

    .line 263
    const/16 v26, 0x1

    .line 266
    goto :goto_df

    .line 264
    :catch_a2
    move-exception v31

    .line 266
    goto :goto_df

    .line 267
    :cond_a4
    const-string v0, "domain"

    move-object/from16 v1, v29

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b9

    .line 269
    :try_start_ae
    invoke-static/range {v30 .. v30}, Lokhttp3/Cookie;->parseDomain(Ljava/lang/String;)Ljava/lang/String;
    :try_end_b1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_ae .. :try_end_b1} :catch_b7

    move-result-object v0

    move-object/from16 v21, v0

    .line 270
    const/16 v25, 0x0

    .line 273
    goto :goto_df

    .line 271
    :catch_b7
    move-exception v31

    .line 273
    goto :goto_df

    .line 274
    :cond_b9
    const-string v0, "path"

    move-object/from16 v1, v29

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c6

    .line 275
    move-object/from16 v22, v30

    goto :goto_df

    .line 276
    :cond_c6
    const-string v0, "secure"

    move-object/from16 v1, v29

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d3

    .line 277
    const/16 v23, 0x1

    goto :goto_df

    .line 278
    :cond_d3
    const-string v0, "httponly"

    move-object/from16 v1, v29

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_df

    .line 279
    const/16 v24, 0x1

    .line 282
    :cond_df
    :goto_df
    add-int/lit8 v11, v27, 0x1

    .line 283
    .end local v27    # "attributePairEnd":I
    .end local v28    # "attributeEqualsSign":I
    .end local v29    # "attributeName":Ljava/lang/String;
    .end local v30    # "attributeValue":Ljava/lang/String;
    goto/16 :goto_44

    .line 287
    :cond_e3
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, v19, v0

    if-nez v0, :cond_ec

    .line 288
    const-wide/high16 v17, -0x8000000000000000L

    goto :goto_119

    .line 289
    :cond_ec
    const-wide/16 v0, -0x1

    cmp-long v0, v19, v0

    if-eqz v0, :cond_119

    .line 290
    const-wide v0, 0x20c49ba5e353f7L

    cmp-long v0, v19, v0

    if-gtz v0, :cond_100

    const-wide/16 v0, 0x3e8

    mul-long v27, v19, v0

    goto :goto_105

    :cond_100
    const-wide v27, 0x7fffffffffffffffL

    .line 293
    .local v27, "deltaMilliseconds":J
    :goto_105
    add-long v17, p0, v27

    .line 294
    cmp-long v0, v17, p0

    if-ltz v0, :cond_114

    const-wide v0, 0xe677d21fdbffL

    cmp-long v0, v17, v0

    if-lez v0, :cond_119

    .line 295
    :cond_114
    const-wide v17, 0xe677d21fdbffL

    .line 300
    .end local v27    # "deltaMilliseconds":J
    :cond_119
    :goto_119
    if-nez v21, :cond_120

    .line 301
    invoke-virtual/range {p2 .. p2}, Lokhttp3/HttpUrl;->host()Ljava/lang/String;

    move-result-object v21

    goto :goto_12c

    .line 302
    :cond_120
    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lokhttp3/Cookie;->domainMatch(Lokhttp3/HttpUrl;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12c

    .line 303
    const/4 v0, 0x0

    return-object v0

    .line 308
    :cond_12c
    :goto_12c
    if-eqz v22, :cond_138

    const-string v0, "/"

    move-object/from16 v1, v22

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_152

    .line 309
    :cond_138
    invoke-virtual/range {p2 .. p2}, Lokhttp3/HttpUrl;->encodedPath()Ljava/lang/String;

    move-result-object v27

    .line 310
    .local v27, "encodedPath":Ljava/lang/String;
    move-object/from16 v0, v27

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v28

    .line 311
    .local v28, "lastSlash":I
    if-eqz v28, :cond_150

    move-object/from16 v0, v27

    const/4 v1, 0x0

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v22

    goto :goto_152

    :cond_150
    const-string v22, "/"

    .line 314
    .end local v27    # "encodedPath":Ljava/lang/String;
    .end local v28    # "lastSlash":I
    :cond_152
    :goto_152
    new-instance v0, Lokhttp3/Cookie;

    move-object v1, v15

    move-object/from16 v2, v16

    move-wide/from16 v3, v17

    move-object/from16 v5, v21

    move-object/from16 v6, v22

    move/from16 v7, v23

    move/from16 v8, v24

    move/from16 v9, v25

    move/from16 v10, v26

    invoke-direct/range {v0 .. v10}, Lokhttp3/Cookie;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;ZZZZ)V

    return-object v0
.end method

.method public static parse(Lokhttp3/HttpUrl;Ljava/lang/String;)Lokhttp3/Cookie;
    .registers 4
    .param p0, "url"    # Lokhttp3/HttpUrl;
    .param p1, "setCookie"    # Ljava/lang/String;

    .line 218
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1, p0, p1}, Lokhttp3/Cookie;->parse(JLokhttp3/HttpUrl;Ljava/lang/String;)Lokhttp3/Cookie;

    move-result-object v0

    return-object v0
.end method

.method public static parseAll(Lokhttp3/HttpUrl;Lokhttp3/Headers;)Ljava/util/List;
    .registers 8
    .param p0, "url"    # Lokhttp3/HttpUrl;
    .param p1, "headers"    # Lokhttp3/Headers;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lokhttp3/HttpUrl;Lokhttp3/Headers;)Ljava/util/List<Lokhttp3/Cookie;>;"
        }
    .end annotation

    .line 432
    const-string v0, "Set-Cookie"

    invoke-virtual {p1, v0}, Lokhttp3/Headers;->values(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 433
    .local v1, "cookieStrings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 435
    .local v2, "cookies":Ljava/util/List;, "Ljava/util/List<Lokhttp3/Cookie;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    .local v4, "size":I
    :goto_c
    if-ge v3, v4, :cond_28

    .line 436
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v0}, Lokhttp3/Cookie;->parse(Lokhttp3/HttpUrl;Ljava/lang/String;)Lokhttp3/Cookie;

    move-result-object v5

    .line 437
    .local v5, "cookie":Lokhttp3/Cookie;
    if-nez v5, :cond_1b

    goto :goto_25

    .line 438
    :cond_1b
    if-nez v2, :cond_22

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 439
    :cond_22
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 435
    .end local v5    # "cookie":Lokhttp3/Cookie;
    :goto_25
    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    .line 442
    .end local v3    # "i":I
    .end local v4    # "size":I
    :cond_28
    if-eqz v2, :cond_2f

    .line 443
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_33

    .line 444
    :cond_2f
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 442
    :goto_33
    return-object v0
.end method

.method private static parseDomain(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .param p0, "s"    # Ljava/lang/String;

    .line 417
    const-string v0, "."

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 418
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 420
    :cond_e
    const-string v0, "."

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 421
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 423
    :cond_1b
    invoke-static {p0}, Lokhttp3/internal/Util;->domainToAscii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 424
    .local v1, "canonicalDomain":Ljava/lang/String;
    if-nez v1, :cond_27

    .line 425
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 427
    :cond_27
    return-object v1
.end method

.method private static parseExpires(Ljava/lang/String;II)J
    .registers 14
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "pos"    # I
    .param p2, "limit"    # I

    .line 320
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lokhttp3/Cookie;->dateCharacterOffset(Ljava/lang/String;IIZ)I

    move-result p1

    .line 322
    const/4 v2, -0x1

    .line 323
    .local v2, "hour":I
    const/4 v3, -0x1

    .line 324
    .local v3, "minute":I
    const/4 v4, -0x1

    .line 325
    .local v4, "second":I
    const/4 v5, -0x1

    .line 326
    .local v5, "dayOfMonth":I
    const/4 v6, -0x1

    .line 327
    .local v6, "month":I
    const/4 v7, -0x1

    .line 328
    .local v7, "year":I
    sget-object v0, Lokhttp3/Cookie;->TIME_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v8

    .line 330
    .local v8, "matcher":Ljava/util/regex/Matcher;
    :goto_11
    if-ge p1, p2, :cond_a9

    .line 331
    add-int/lit8 v0, p1, 0x1

    const/4 v1, 0x1

    invoke-static {p0, v0, p2, v1}, Lokhttp3/Cookie;->dateCharacterOffset(Ljava/lang/String;IIZ)I

    move-result v9

    .line 332
    .local v9, "end":I
    invoke-virtual {v8, p1, v9}, Ljava/util/regex/Matcher;->region(II)Ljava/util/regex/Matcher;

    .line 334
    const/4 v0, -0x1

    if-ne v2, v0, :cond_48

    sget-object v0, Lokhttp3/Cookie;->TIME_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v8, v0}, Ljava/util/regex/Matcher;->usePattern(Ljava/util/regex/Pattern;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_48

    .line 335
    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 336
    const/4 v0, 0x2

    invoke-virtual {v8, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 337
    const/4 v0, 0x3

    invoke-virtual {v8, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    goto :goto_a0

    .line 338
    :cond_48
    const/4 v0, -0x1

    if-ne v5, v0, :cond_61

    sget-object v0, Lokhttp3/Cookie;->DAY_OF_MONTH_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v8, v0}, Ljava/util/regex/Matcher;->usePattern(Ljava/util/regex/Pattern;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_61

    .line 339
    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    goto :goto_a0

    .line 340
    :cond_61
    const/4 v0, -0x1

    if-ne v6, v0, :cond_88

    sget-object v0, Lokhttp3/Cookie;->MONTH_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v8, v0}, Ljava/util/regex/Matcher;->usePattern(Ljava/util/regex/Pattern;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_88

    .line 341
    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v10

    .line 342
    .local v10, "monthString":Ljava/lang/String;
    sget-object v0, Lokhttp3/Cookie;->MONTH_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v0}, Ljava/util/regex/Pattern;->pattern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    div-int/lit8 v6, v0, 0x4

    .line 343
    .end local v10    # "monthString":Ljava/lang/String;
    goto :goto_a0

    :cond_88
    const/4 v0, -0x1

    if-ne v7, v0, :cond_a0

    sget-object v0, Lokhttp3/Cookie;->YEAR_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v8, v0}, Ljava/util/regex/Matcher;->usePattern(Ljava/util/regex/Pattern;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_a0

    .line 344
    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 347
    :cond_a0
    :goto_a0
    add-int/lit8 v0, v9, 0x1

    const/4 v1, 0x0

    invoke-static {p0, v0, p2, v1}, Lokhttp3/Cookie;->dateCharacterOffset(Ljava/lang/String;IIZ)I

    move-result p1

    .line 348
    .end local v9    # "end":I
    goto/16 :goto_11

    .line 351
    :cond_a9
    const/16 v0, 0x46

    if-lt v7, v0, :cond_b3

    const/16 v0, 0x63

    if-gt v7, v0, :cond_b3

    add-int/lit16 v7, v7, 0x76c

    .line 352
    :cond_b3
    if-ltz v7, :cond_bb

    const/16 v0, 0x45

    if-gt v7, v0, :cond_bb

    add-int/lit16 v7, v7, 0x7d0

    .line 356
    :cond_bb
    const/16 v0, 0x641

    if-ge v7, v0, :cond_c5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 357
    :cond_c5
    const/4 v0, -0x1

    if-ne v6, v0, :cond_ce

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 358
    :cond_ce
    const/4 v0, 0x1

    if-lt v5, v0, :cond_d5

    const/16 v0, 0x1f

    if-le v5, v0, :cond_db

    :cond_d5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 359
    :cond_db
    if-ltz v2, :cond_e1

    const/16 v0, 0x17

    if-le v2, v0, :cond_e7

    :cond_e1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 360
    :cond_e7
    if-ltz v3, :cond_ed

    const/16 v0, 0x3b

    if-le v3, v0, :cond_f3

    :cond_ed
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 361
    :cond_f3
    if-ltz v4, :cond_f9

    const/16 v0, 0x3b

    if-le v4, v0, :cond_ff

    :cond_f9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 363
    :cond_ff
    new-instance v9, Ljava/util/GregorianCalendar;

    sget-object v0, Lokhttp3/internal/Util;->UTC:Ljava/util/TimeZone;

    invoke-direct {v9, v0}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 364
    .local v9, "calendar":Ljava/util/Calendar;
    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Ljava/util/Calendar;->setLenient(Z)V

    .line 365
    const/4 v0, 0x1

    invoke-virtual {v9, v0, v7}, Ljava/util/Calendar;->set(II)V

    .line 366
    add-int/lit8 v0, v6, -0x1

    const/4 v1, 0x2

    invoke-virtual {v9, v1, v0}, Ljava/util/Calendar;->set(II)V

    .line 367
    const/4 v0, 0x5

    invoke-virtual {v9, v0, v5}, Ljava/util/Calendar;->set(II)V

    .line 368
    const/16 v0, 0xb

    invoke-virtual {v9, v0, v2}, Ljava/util/Calendar;->set(II)V

    .line 369
    const/16 v0, 0xc

    invoke-virtual {v9, v0, v3}, Ljava/util/Calendar;->set(II)V

    .line 370
    const/16 v0, 0xd

    invoke-virtual {v9, v0, v4}, Ljava/util/Calendar;->set(II)V

    .line 371
    const/16 v0, 0xe

    const/4 v1, 0x0

    invoke-virtual {v9, v0, v1}, Ljava/util/Calendar;->set(II)V

    .line 372
    invoke-virtual {v9}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method private static parseMaxAge(Ljava/lang/String;)J
    .registers 5
    .param p0, "s"    # Ljava/lang/String;

    .line 401
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_3} :catch_f

    move-result-wide v2

    .line 402
    .local v2, "parsed":J
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-gtz v0, :cond_d

    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_e

    :cond_d
    move-wide v0, v2

    :goto_e
    return-wide v0

    .line 403
    .end local v2    # "parsed":J
    :catch_f
    move-exception v2

    .line 405
    .local v2, "e":Ljava/lang/NumberFormatException;
    const-string v0, "-?\\d+"

    invoke-virtual {p0, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 406
    const-string v0, "-"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_28

    :cond_23
    const-wide v0, 0x7fffffffffffffffL

    :goto_28
    return-wide v0

    .line 408
    :cond_29
    throw v2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 7
    .param p1, "other"    # Ljava/lang/Object;

    .line 564
    instance-of v0, p1, Lokhttp3/Cookie;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    return v0

    .line 565
    :cond_6
    move-object v4, p1

    check-cast v4, Lokhttp3/Cookie;

    .line 566
    .local v4, "that":Lokhttp3/Cookie;
    iget-object v0, v4, Lokhttp3/Cookie;->name:Ljava/lang/String;

    iget-object v1, p0, Lokhttp3/Cookie;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_53

    iget-object v0, v4, Lokhttp3/Cookie;->value:Ljava/lang/String;

    iget-object v1, p0, Lokhttp3/Cookie;->value:Ljava/lang/String;

    .line 567
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_53

    iget-object v0, v4, Lokhttp3/Cookie;->domain:Ljava/lang/String;

    iget-object v1, p0, Lokhttp3/Cookie;->domain:Ljava/lang/String;

    .line 568
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_53

    iget-object v0, v4, Lokhttp3/Cookie;->path:Ljava/lang/String;

    iget-object v1, p0, Lokhttp3/Cookie;->path:Ljava/lang/String;

    .line 569
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_53

    iget-wide v0, v4, Lokhttp3/Cookie;->expiresAt:J

    iget-wide v2, p0, Lokhttp3/Cookie;->expiresAt:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_53

    iget-boolean v0, v4, Lokhttp3/Cookie;->secure:Z

    iget-boolean v1, p0, Lokhttp3/Cookie;->secure:Z

    if-ne v0, v1, :cond_53

    iget-boolean v0, v4, Lokhttp3/Cookie;->httpOnly:Z

    iget-boolean v1, p0, Lokhttp3/Cookie;->httpOnly:Z

    if-ne v0, v1, :cond_53

    iget-boolean v0, v4, Lokhttp3/Cookie;->persistent:Z

    iget-boolean v1, p0, Lokhttp3/Cookie;->persistent:Z

    if-ne v0, v1, :cond_53

    iget-boolean v0, v4, Lokhttp3/Cookie;->hostOnly:Z

    iget-boolean v1, p0, Lokhttp3/Cookie;->hostOnly:Z

    if-ne v0, v1, :cond_53

    const/4 v0, 0x1

    goto :goto_54

    :cond_53
    const/4 v0, 0x0

    .line 566
    :goto_54
    return v0
.end method

.method public hashCode()I
    .registers 8

    .line 578
    const/16 v6, 0x11

    .line 579
    .local v6, "hash":I
    iget-object v0, p0, Lokhttp3/Cookie;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v6, v0, 0x20f

    .line 580
    mul-int/lit8 v0, v6, 0x1f

    iget-object v1, p0, Lokhttp3/Cookie;->value:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int v6, v0, v1

    .line 581
    mul-int/lit8 v0, v6, 0x1f

    iget-object v1, p0, Lokhttp3/Cookie;->domain:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int v6, v0, v1

    .line 582
    mul-int/lit8 v0, v6, 0x1f

    iget-object v1, p0, Lokhttp3/Cookie;->path:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int v6, v0, v1

    .line 583
    mul-int/lit8 v0, v6, 0x1f

    iget-wide v1, p0, Lokhttp3/Cookie;->expiresAt:J

    iget-wide v3, p0, Lokhttp3/Cookie;->expiresAt:J

    const/16 v5, 0x20

    ushr-long/2addr v3, v5

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int v6, v0, v1

    .line 584
    mul-int/lit8 v0, v6, 0x1f

    iget-boolean v1, p0, Lokhttp3/Cookie;->secure:Z

    if-eqz v1, :cond_3d

    const/4 v1, 0x0

    goto :goto_3e

    :cond_3d
    const/4 v1, 0x1

    :goto_3e
    add-int v6, v0, v1

    .line 585
    mul-int/lit8 v0, v6, 0x1f

    iget-boolean v1, p0, Lokhttp3/Cookie;->httpOnly:Z

    if-eqz v1, :cond_48

    const/4 v1, 0x0

    goto :goto_49

    :cond_48
    const/4 v1, 0x1

    :goto_49
    add-int v6, v0, v1

    .line 586
    mul-int/lit8 v0, v6, 0x1f

    iget-boolean v1, p0, Lokhttp3/Cookie;->persistent:Z

    if-eqz v1, :cond_53

    const/4 v1, 0x0

    goto :goto_54

    :cond_53
    const/4 v1, 0x1

    :goto_54
    add-int v6, v0, v1

    .line 587
    mul-int/lit8 v0, v6, 0x1f

    iget-boolean v1, p0, Lokhttp3/Cookie;->hostOnly:Z

    if-eqz v1, :cond_5e

    const/4 v1, 0x0

    goto :goto_5f

    :cond_5e
    const/4 v1, 0x1

    :goto_5f
    add-int v6, v0, v1

    .line 588
    return v6
.end method

.method public name()Ljava/lang/String;
    .registers 2

    .line 95
    iget-object v0, p0, Lokhttp3/Cookie;->name:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    .line 533
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 534
    .local v4, "result":Ljava/lang/StringBuilder;
    iget-object v0, p0, Lokhttp3/Cookie;->name:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 535
    const/16 v0, 0x3d

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 536
    iget-object v0, p0, Lokhttp3/Cookie;->value:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 538
    iget-boolean v0, p0, Lokhttp3/Cookie;->persistent:Z

    if-eqz v0, :cond_3a

    .line 539
    iget-wide v0, p0, Lokhttp3/Cookie;->expiresAt:J

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v0, v0, v2

    if-nez v0, :cond_26

    .line 540
    const-string v0, "; max-age=0"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3a

    .line 542
    :cond_26
    const-string v0, "; expires="

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    iget-wide v2, p0, Lokhttp3/Cookie;->expiresAt:J

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-static {v1}, Lokhttp3/internal/http/HttpDate;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 546
    :cond_3a
    :goto_3a
    iget-boolean v0, p0, Lokhttp3/Cookie;->hostOnly:Z

    if-nez v0, :cond_49

    .line 547
    const-string v0, "; domain="

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lokhttp3/Cookie;->domain:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 550
    :cond_49
    const-string v0, "; path="

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lokhttp3/Cookie;->path:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 552
    iget-boolean v0, p0, Lokhttp3/Cookie;->secure:Z

    if-eqz v0, :cond_5d

    .line 553
    const-string v0, "; secure"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 556
    :cond_5d
    iget-boolean v0, p0, Lokhttp3/Cookie;->httpOnly:Z

    if-eqz v0, :cond_66

    .line 557
    const-string v0, "; httponly"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 560
    :cond_66
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public value()Ljava/lang/String;
    .registers 2

    .line 100
    iget-object v0, p0, Lokhttp3/Cookie;->value:Ljava/lang/String;

    return-object v0
.end method

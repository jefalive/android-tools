.class Lbr/com/itau/sdk/android/core_ui/token/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Landroid/widget/Button;

.field final synthetic b:Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;


# direct methods
.method constructor <init>(Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;Landroid/widget/Button;)V
    .registers 3

    .line 61
    iput-object p1, p0, Lbr/com/itau/sdk/android/core_ui/token/d;->b:Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;

    iput-object p2, p0, Lbr/com/itau/sdk/android/core_ui/token/d;->a:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 5

    .line 68
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/d;->a:Landroid/widget/Button;

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    iget-object v2, p0, Lbr/com/itau/sdk/android/core_ui/token/d;->b:Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;

    invoke-static {v2}, Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;->a(Lbr/com/itau/sdk/android/core_ui/token/CardPasswordDialog;)I

    move-result v2

    if-lt v1, v2, :cond_10

    const/4 v1, 0x1

    goto :goto_11

    :cond_10
    const/4 v1, 0x0

    :goto_11
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 69
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5

    .line 73
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5

    .line 64
    return-void
.end method

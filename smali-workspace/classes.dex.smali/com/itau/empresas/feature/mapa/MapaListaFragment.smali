.class public Lcom/itau/empresas/feature/mapa/MapaListaFragment;
.super Lcom/itau/empresas/ui/fragment/BaseFragment;
.source "MapaListaFragment.java"


# instance fields
.field private adapter:Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

.field private agencias:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;"
        }
    .end annotation
.end field

.field listaAgencias:Landroid/widget/ListView;

.field textoSemAgencias:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 31
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/mapa/MapaListaFragment;)Lcom/itau/empresas/adapter/ListaAgenciasAdapter;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/mapa/MapaListaFragment;

    .line 31
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->adapter:Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    return-object v0
.end method

.method private pesquisar()Landroid/support/v7/widget/SearchView$OnQueryTextListener;
    .registers 2

    .line 89
    new-instance v0, Lcom/itau/empresas/feature/mapa/MapaListaFragment$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/mapa/MapaListaFragment$2;-><init>(Lcom/itau/empresas/feature/mapa/MapaListaFragment;)V

    return-object v0
.end method


# virtual methods
.method public carregarAgencias()V
    .registers 3

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->agencias:Ljava/util/List;

    if-eqz v0, :cond_17

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->adapter:Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->agencias:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->setItems(Ljava/util/List;)V

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->listaAgencias:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->adapter:Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 57
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->adapter:Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    invoke-virtual {v0}, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->notifyDataSetChanged()V

    .line 59
    :cond_17
    return-void
.end method

.method carregarTela()V
    .registers 3

    .line 48
    new-instance v0, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->adapter:Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    .line 49
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->carregarAgencias()V

    .line 50
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->setHasOptionsMenu(Z)V

    .line 51
    return-void
.end method

.method menuFilter()V
    .registers 4

    .line 128
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/feature/mapa/MapaActivity;

    .line 129
    .local v2, "activity":Lcom/itau/empresas/feature/mapa/MapaActivity;
    if-eqz v2, :cond_f

    .line 130
    iget-object v0, v2, Lcom/itau/empresas/feature/mapa/MapaActivity;->drawerLayoutFilter:Landroid/support/v4/widget/DrawerLayout;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->openDrawer(I)V

    .line 132
    :cond_f
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .registers 6
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .line 63
    const v0, 0x7f0f0005

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 65
    const v0, 0x7f0e06c6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 66
    .local v1, "itemMenu":Landroid/view/MenuItem;
    invoke-static {v1}, Landroid/support/v4/view/MenuItemCompat;->getActionView(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/support/v7/widget/SearchView;

    .line 67
    .local v2, "searchView":Landroid/support/v7/widget/SearchView;
    const v0, 0x7f070635

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 68
    invoke-direct {p0}, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->pesquisar()Landroid/support/v7/widget/SearchView$OnQueryTextListener;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/SearchView;->setOnQueryTextListener(Landroid/support/v7/widget/SearchView$OnQueryTextListener;)V

    .line 70
    new-instance v0, Lcom/itau/empresas/feature/mapa/MapaListaFragment$1;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/feature/mapa/MapaListaFragment$1;-><init>(Lcom/itau/empresas/feature/mapa/MapaListaFragment;Landroid/view/Menu;)V

    .line 71
    invoke-static {v1, v0}, Landroid/support/v4/view/MenuItemCompat;->setOnActionExpandListener(Landroid/view/MenuItem;Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;)Landroid/view/MenuItem;

    .line 86
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoListaMapas;)V
    .registers 6
    .param p1, "eventoListaMapas"    # Lcom/itau/empresas/Evento$EventoListaMapas;

    .line 105
    invoke-static {}, Lcom/itau/empresas/Evento$EventoListaMapas;->getAgencias()Ljava/util/List;

    move-result-object v2

    .line 106
    .local v2, "agencias":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_36

    .line 108
    invoke-virtual {p0}, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    .line 110
    .local v3, "activity":Landroid/support/v4/app/FragmentActivity;
    instance-of v0, v3, Lcom/itau/empresas/ui/activity/BaseActivity;

    if-eqz v0, :cond_18

    .line 111
    move-object v0, v3

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 112
    :cond_18
    invoke-static {v3}, Lcom/itau/empresas/ui/util/SnackbarUtils;->tentaDispensarSnackbarNo(Ljava/lang/Object;)V

    .line 114
    iput-object v2, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->agencias:Ljava/util/List;

    .line 115
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->adapter:Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    invoke-virtual {v0, v2}, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->setItems(Ljava/util/List;)V

    .line 116
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->listaAgencias:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->adapter:Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 117
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->adapter:Lcom/itau/empresas/adapter/ListaAgenciasAdapter;

    invoke-virtual {v0}, Lcom/itau/empresas/adapter/ListaAgenciasAdapter;->notifyDataSetChanged()V

    .line 118
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->textoSemAgencias:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 119
    .end local v3    # "activity":Landroid/support/v4/app/FragmentActivity;
    goto :goto_4b

    .line 120
    :cond_36
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->listaAgencias:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 121
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->textoSemAgencias:Landroid/widget/TextView;

    const v1, 0x7f07046b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 122
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->textoSemAgencias:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 124
    :goto_4b
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 136
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public setAgencias(Ljava/util/List;)V
    .registers 2
    .param p1, "agencias"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/AgenciaVO;>;)V"
        }
    .end annotation

    .line 43
    iput-object p1, p0, Lcom/itau/empresas/feature/mapa/MapaListaFragment;->agencias:Ljava/util/List;

    .line 44
    return-void
.end method

.class Lcom/itau/empresas/feature/login/controller/LoginController$1;
.super Ljava/lang/Object;
.source "LoginController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/login/controller/LoginController;->buscaTeclado(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/login/controller/LoginController;

.field final synthetic val$codigoOperador:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/login/controller/LoginController;Ljava/lang/String;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/login/controller/LoginController;

    .line 33
    iput-object p1, p0, Lcom/itau/empresas/feature/login/controller/LoginController$1;->this$0:Lcom/itau/empresas/feature/login/controller/LoginController;

    iput-object p2, p0, Lcom/itau/empresas/feature/login/controller/LoginController$1;->val$codigoOperador:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 3

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/LoginController$1;->this$0:Lcom/itau/empresas/feature/login/controller/LoginController;

    iget-object v0, v0, Lcom/itau/empresas/feature/login/controller/LoginController;->app:Lcom/itau/empresas/CustomApplication;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/controller/LoginController$1;->val$codigoOperador:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->setCodigoOperador(Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/LoginController$1;->this$0:Lcom/itau/empresas/feature/login/controller/LoginController;

    iget-object v0, v0, Lcom/itau/empresas/feature/login/controller/LoginController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    const-string v1, "teclado"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->inicioMonitorarTempo(Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/LoginController$1;->this$0:Lcom/itau/empresas/feature/login/controller/LoginController;

    # invokes: Lcom/itau/empresas/feature/login/controller/LoginController;->api()Ljava/lang/Object;
    invoke-static {v0}, Lcom/itau/empresas/feature/login/controller/LoginController;->access$000(Lcom/itau/empresas/feature/login/controller/LoginController;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/login/LoginApi;

    invoke-interface {v0}, Lcom/itau/empresas/feature/login/LoginApi;->tecladoVirtual()Lcom/itau/empresas/feature/login/model/TecladoVirtualVO;

    move-result-object v0

    # invokes: Lcom/itau/empresas/feature/login/controller/LoginController;->post(Ljava/lang/Object;)V
    invoke-static {v0}, Lcom/itau/empresas/feature/login/controller/LoginController;->access$100(Ljava/lang/Object;)V

    .line 40
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/LoginController$1;->this$0:Lcom/itau/empresas/feature/login/controller/LoginController;

    iget-object v0, v0, Lcom/itau/empresas/feature/login/controller/LoginController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    const-string v1, "teclado"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->fimMonitorarTempo(Ljava/lang/String;)V

    .line 41
    return-void
.end method

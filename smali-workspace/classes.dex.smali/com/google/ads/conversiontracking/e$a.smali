.class Lcom/google/ads/conversiontracking/e$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ads/conversiontracking/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/ads/conversiontracking/e;


# direct methods
.method private constructor <init>(Lcom/google/ads/conversiontracking/e;)V
    .registers 2

    .line 116
    iput-object p1, p0, Lcom/google/ads/conversiontracking/e$a;->a:Lcom/google/ads/conversiontracking/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/ads/conversiontracking/e;Lcom/google/ads/conversiontracking/e$1;)V
    .registers 3

    .line 116
    invoke-direct {p0, p1}, Lcom/google/ads/conversiontracking/e$a;-><init>(Lcom/google/ads/conversiontracking/e;)V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 7

    .line 119
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e$a;->a:Lcom/google/ads/conversiontracking/e;

    invoke-static {v0}, Lcom/google/ads/conversiontracking/e;->a(Lcom/google/ads/conversiontracking/e;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 127
    :try_start_7
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e$a;->a:Lcom/google/ads/conversiontracking/e;

    invoke-static {v0}, Lcom/google/ads/conversiontracking/e;->b(Lcom/google/ads/conversiontracking/e;)Z

    move-result v0

    if-eqz v0, :cond_23

    iget-object v0, p0, Lcom/google/ads/conversiontracking/e$a;->a:Lcom/google/ads/conversiontracking/e;

    .line 128
    invoke-static {v0}, Lcom/google/ads/conversiontracking/e;->c(Lcom/google/ads/conversiontracking/e;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/ads/conversiontracking/g;->d(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_23

    iget-object v0, p0, Lcom/google/ads/conversiontracking/e$a;->a:Lcom/google/ads/conversiontracking/e;

    .line 129
    invoke-static {v0}, Lcom/google/ads/conversiontracking/e;->d(Lcom/google/ads/conversiontracking/e;)Z
    :try_end_20
    .catchall {:try_start_7 .. :try_end_20} :catchall_54

    move-result v0

    if-eqz v0, :cond_25

    .line 130
    :cond_23
    monitor-exit v4

    return-void

    .line 132
    :cond_25
    :try_start_25
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e$a;->a:Lcom/google/ads/conversiontracking/e;

    invoke-static {v0}, Lcom/google/ads/conversiontracking/e;->f(Lcom/google/ads/conversiontracking/e;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/ads/conversiontracking/e$a;->a:Lcom/google/ads/conversiontracking/e;

    invoke-static {v1}, Lcom/google/ads/conversiontracking/e;->e(Lcom/google/ads/conversiontracking/e;)Lcom/google/ads/conversiontracking/f;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v2, v3}, Lcom/google/ads/conversiontracking/f;->a(J)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 133
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e$a;->a:Lcom/google/ads/conversiontracking/e;

    invoke-static {v0}, Lcom/google/ads/conversiontracking/e;->c(Lcom/google/ads/conversiontracking/e;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/ads/conversiontracking/g;->c(Landroid/content/Context;)V

    .line 134
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e$a;->a:Lcom/google/ads/conversiontracking/e;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/ads/conversiontracking/e;->a(Lcom/google/ads/conversiontracking/e;Z)Z

    .line 135
    iget-object v0, p0, Lcom/google/ads/conversiontracking/e$a;->a:Lcom/google/ads/conversiontracking/e;

    invoke-static {v0}, Lcom/google/ads/conversiontracking/e;->a(Lcom/google/ads/conversiontracking/e;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V
    :try_end_52
    .catchall {:try_start_25 .. :try_end_52} :catchall_54

    .line 136
    monitor-exit v4

    goto :goto_57

    :catchall_54
    move-exception v5

    monitor-exit v4

    throw v5

    .line 137
    :goto_57
    return-void
.end method

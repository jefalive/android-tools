.class public Landroid/support/design/internal/TextScale;
.super Landroid/support/transition/Transition;
.source "TextScale.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 31
    invoke-direct {p0}, Landroid/support/transition/Transition;-><init>()V

    return-void
.end method

.method private captureValues(Landroid/support/transition/TransitionValues;)V
    .registers 6
    .param p1, "transitionValues"    # Landroid/support/transition/TransitionValues;

    .line 45
    iget-object v0, p1, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1a

    .line 46
    iget-object v0, p1, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    move-object v3, v0

    check-cast v3, Landroid/widget/TextView;

    .line 47
    .local v3, "textview":Landroid/widget/TextView;
    iget-object v0, p1, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:textscale:scale"

    invoke-virtual {v3}, Landroid/widget/TextView;->getScaleX()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    .end local v3    # "textview":Landroid/widget/TextView;
    :cond_1a
    return-void
.end method


# virtual methods
.method public captureEndValues(Landroid/support/transition/TransitionValues;)V
    .registers 2
    .param p1, "transitionValues"    # Landroid/support/transition/TransitionValues;

    .line 41
    invoke-direct {p0, p1}, Landroid/support/design/internal/TextScale;->captureValues(Landroid/support/transition/TransitionValues;)V

    .line 42
    return-void
.end method

.method public captureStartValues(Landroid/support/transition/TransitionValues;)V
    .registers 2
    .param p1, "transitionValues"    # Landroid/support/transition/TransitionValues;

    .line 36
    invoke-direct {p0, p1}, Landroid/support/design/internal/TextScale;->captureValues(Landroid/support/transition/TransitionValues;)V

    .line 37
    return-void
.end method

.method public createAnimator(Landroid/view/ViewGroup;Landroid/support/transition/TransitionValues;Landroid/support/transition/TransitionValues;)Landroid/animation/Animator;
    .registers 12
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/support/transition/TransitionValues;
    .param p3, "endValues"    # Landroid/support/transition/TransitionValues;

    .line 54
    if-eqz p2, :cond_10

    if-eqz p3, :cond_10

    iget-object v0, p2, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/TextView;

    if-eqz v0, :cond_10

    iget-object v0, p3, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/TextView;

    if-nez v0, :cond_12

    .line 56
    :cond_10
    const/4 v0, 0x0

    return-object v0

    .line 58
    :cond_12
    iget-object v0, p3, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    move-object v2, v0

    check-cast v2, Landroid/widget/TextView;

    .line 59
    .local v2, "view":Landroid/widget/TextView;
    iget-object v3, p2, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    .line 60
    .local v3, "startVals":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v4, p3, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    .line 61
    .local v4, "endVals":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "android:textscale:scale"

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_30

    const-string v0, "android:textscale:scale"

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v5

    goto :goto_32

    :cond_30
    const/high16 v5, 0x3f800000    # 1.0f

    .line 63
    .local v5, "startSize":F
    :goto_32
    const-string v0, "android:textscale:scale"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_47

    const-string v0, "android:textscale:scale"

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v6

    goto :goto_49

    :cond_47
    const/high16 v6, 0x3f800000    # 1.0f

    .line 65
    .local v6, "endSize":F
    :goto_49
    cmpl-float v0, v5, v6

    if-nez v0, :cond_4f

    .line 66
    const/4 v0, 0x0

    return-object v0

    .line 69
    :cond_4f
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput v5, v0, v1

    const/4 v1, 0x1

    aput v6, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v7

    .line 71
    .local v7, "animator":Landroid/animation/ValueAnimator;
    new-instance v0, Landroid/support/design/internal/TextScale$1;

    invoke-direct {v0, p0, v2}, Landroid/support/design/internal/TextScale$1;-><init>(Landroid/support/design/internal/TextScale;Landroid/widget/TextView;)V

    invoke-virtual {v7, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 79
    return-object v7
.end method

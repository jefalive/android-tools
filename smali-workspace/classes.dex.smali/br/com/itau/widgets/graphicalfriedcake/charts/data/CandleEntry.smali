.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
.source "CandleEntry.java"


# instance fields
.field private mClose:F

.field private mOpen:F

.field private mShadowHigh:F

.field private mShadowLow:F


# direct methods
.method public constructor <init>(IFFFF)V
    .registers 8
    .param p1, "xIndex"    # I
    .param p2, "shadowH"    # F
    .param p3, "shadowL"    # F
    .param p4, "open"    # F
    .param p5, "close"    # F

    .line 37
    add-float v0, p2, p3

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    invoke-direct {p0, v0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;-><init>(FI)V

    .line 12
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mShadowHigh:F

    .line 16
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mShadowLow:F

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mClose:F

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mOpen:F

    .line 40
    iput p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mShadowHigh:F

    .line 41
    iput p3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mShadowLow:F

    .line 42
    iput p4, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mOpen:F

    .line 43
    iput p5, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mClose:F

    .line 44
    return-void
.end method

.method public constructor <init>(IFFFFLjava/lang/Object;)V
    .registers 9
    .param p1, "xIndex"    # I
    .param p2, "shadowH"    # F
    .param p3, "shadowL"    # F
    .param p4, "open"    # F
    .param p5, "close"    # F
    .param p6, "data"    # Ljava/lang/Object;

    .line 59
    add-float v0, p2, p3

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    invoke-direct {p0, v0, p1, p6}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;-><init>(FILjava/lang/Object;)V

    .line 12
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mShadowHigh:F

    .line 16
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mShadowLow:F

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mClose:F

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mOpen:F

    .line 62
    iput p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mShadowHigh:F

    .line 63
    iput p3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mShadowLow:F

    .line 64
    iput p4, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mOpen:F

    .line 65
    iput p5, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mClose:F

    .line 66
    return-void
.end method


# virtual methods
.method public copy()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;
    .registers 9

    .line 103
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->getXIndex()I

    move-result v1

    iget v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mShadowHigh:F

    iget v3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mShadowLow:F

    iget v4, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mOpen:F

    iget v5, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mClose:F

    .line 104
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->getData()Ljava/lang/Object;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;-><init>(IFFFFLjava/lang/Object;)V

    move-object v7, v0

    .line 107
    .local v7, "c":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;
    return-object v7
.end method

.method public bridge synthetic copy()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    .registers 2

    .line 8
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->copy()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;

    move-result-object v0

    return-object v0
.end method

.method public getBodyRange()F
    .registers 3

    .line 86
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mOpen:F

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mClose:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    return v0
.end method

.method public getClose()F
    .registers 2

    .line 147
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mClose:F

    return v0
.end method

.method public getHigh()F
    .registers 2

    .line 117
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mShadowHigh:F

    return v0
.end method

.method public getLow()F
    .registers 2

    .line 132
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mShadowLow:F

    return v0
.end method

.method public getOpen()F
    .registers 2

    .line 162
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mOpen:F

    return v0
.end method

.method public getShadowRange()F
    .registers 3

    .line 76
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mShadowHigh:F

    iget v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mShadowLow:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    return v0
.end method

.method public getVal()F
    .registers 2

    .line 96
    invoke-super {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    return v0
.end method

.method public setClose(F)V
    .registers 2
    .param p1, "mClose"    # F

    .line 152
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mClose:F

    .line 153
    return-void
.end method

.method public setHigh(F)V
    .registers 2
    .param p1, "mShadowHigh"    # F

    .line 122
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mShadowHigh:F

    .line 123
    return-void
.end method

.method public setLow(F)V
    .registers 2
    .param p1, "mShadowLow"    # F

    .line 137
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mShadowLow:F

    .line 138
    return-void
.end method

.method public setOpen(F)V
    .registers 2
    .param p1, "mOpen"    # F

    .line 167
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/CandleEntry;->mOpen:F

    .line 168
    return-void
.end method

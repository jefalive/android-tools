.class public Lcom/google/android/gms/ads/AdActivity;
.super Landroid/app/Activity;


# instance fields
.field private zzoA:Lcom/google/android/gms/internal/zzfv;


# direct methods
.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private zzaD()V
    .registers 3

    iget-object v0, p0, Lcom/google/android/gms/ads/AdActivity;->zzoA:Lcom/google/android/gms/internal/zzfv;

    if-eqz v0, :cond_10

    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/ads/AdActivity;->zzoA:Lcom/google/android/gms/internal/zzfv;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzfv;->zzaD()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    goto :goto_10

    :catch_a
    move-exception v1

    const-string v0, "Could not forward setContentViewSet to ad overlay:"

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_10
    :goto_10
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .registers 4

    const/4 v1, 0x1

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/ads/AdActivity;->zzoA:Lcom/google/android/gms/internal/zzfv;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/ads/AdActivity;->zzoA:Lcom/google/android/gms/internal/zzfv;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzfv;->zzfn()Z
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_a} :catch_d

    move-result v0

    move v1, v0

    :cond_c
    goto :goto_13

    :catch_d
    move-exception v2

    const-string v0, "Could not forward onBackPressed to ad overlay:"

    invoke-static {v0, v2}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_13
    if-eqz v1, :cond_18

    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    :cond_18
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/google/android/gms/internal/zzfu;->createAdOverlay(Landroid/app/Activity;)Lcom/google/android/gms/internal/zzfv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/AdActivity;->zzoA:Lcom/google/android/gms/internal/zzfv;

    iget-object v0, p0, Lcom/google/android/gms/ads/AdActivity;->zzoA:Lcom/google/android/gms/internal/zzfv;

    if-nez v0, :cond_16

    const-string v0, "Could not create ad overlay."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzaK(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/ads/AdActivity;->finish()V

    return-void

    :cond_16
    :try_start_16
    iget-object v0, p0, Lcom/google/android/gms/ads/AdActivity;->zzoA:Lcom/google/android/gms/internal/zzfv;

    invoke-interface {v0, p1}, Lcom/google/android/gms/internal/zzfv;->onCreate(Landroid/os/Bundle;)V
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_1b} :catch_1c

    goto :goto_25

    :catch_1c
    move-exception v1

    const-string v0, "Could not forward onCreate to ad overlay:"

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p0}, Lcom/google/android/gms/ads/AdActivity;->finish()V

    :goto_25
    return-void
.end method

.method protected onDestroy()V
    .registers 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/AdActivity;->zzoA:Lcom/google/android/gms/internal/zzfv;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/ads/AdActivity;->zzoA:Lcom/google/android/gms/internal/zzfv;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzfv;->onDestroy()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_a

    :cond_9
    goto :goto_10

    :catch_a
    move-exception v1

    const-string v0, "Could not forward onDestroy to ad overlay:"

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_10
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onPause()V
    .registers 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/AdActivity;->zzoA:Lcom/google/android/gms/internal/zzfv;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/ads/AdActivity;->zzoA:Lcom/google/android/gms/internal/zzfv;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzfv;->onPause()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_a

    :cond_9
    goto :goto_13

    :catch_a
    move-exception v1

    const-string v0, "Could not forward onPause to ad overlay:"

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p0}, Lcom/google/android/gms/ads/AdActivity;->finish()V

    :goto_13
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onRestart()V
    .registers 3

    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/ads/AdActivity;->zzoA:Lcom/google/android/gms/internal/zzfv;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/ads/AdActivity;->zzoA:Lcom/google/android/gms/internal/zzfv;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzfv;->onRestart()V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_c} :catch_d

    :cond_c
    goto :goto_16

    :catch_d
    move-exception v1

    const-string v0, "Could not forward onRestart to ad overlay:"

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p0}, Lcom/google/android/gms/ads/AdActivity;->finish()V

    :goto_16
    return-void
.end method

.method protected onResume()V
    .registers 3

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/ads/AdActivity;->zzoA:Lcom/google/android/gms/internal/zzfv;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/ads/AdActivity;->zzoA:Lcom/google/android/gms/internal/zzfv;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzfv;->onResume()V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_c} :catch_d

    :cond_c
    goto :goto_16

    :catch_d
    move-exception v1

    const-string v0, "Could not forward onResume to ad overlay:"

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p0}, Lcom/google/android/gms/ads/AdActivity;->finish()V

    :goto_16
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "outState"    # Landroid/os/Bundle;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/AdActivity;->zzoA:Lcom/google/android/gms/internal/zzfv;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/ads/AdActivity;->zzoA:Lcom/google/android/gms/internal/zzfv;

    invoke-interface {v0, p1}, Lcom/google/android/gms/internal/zzfv;->onSaveInstanceState(Landroid/os/Bundle;)V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_a

    :cond_9
    goto :goto_13

    :catch_a
    move-exception v1

    const-string v0, "Could not forward onSaveInstanceState to ad overlay:"

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p0}, Lcom/google/android/gms/ads/AdActivity;->finish()V

    :goto_13
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onStart()V
    .registers 3

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/ads/AdActivity;->zzoA:Lcom/google/android/gms/internal/zzfv;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/ads/AdActivity;->zzoA:Lcom/google/android/gms/internal/zzfv;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzfv;->onStart()V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_c} :catch_d

    :cond_c
    goto :goto_16

    :catch_d
    move-exception v1

    const-string v0, "Could not forward onStart to ad overlay:"

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p0}, Lcom/google/android/gms/ads/AdActivity;->finish()V

    :goto_16
    return-void
.end method

.method protected onStop()V
    .registers 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/AdActivity;->zzoA:Lcom/google/android/gms/internal/zzfv;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/ads/AdActivity;->zzoA:Lcom/google/android/gms/internal/zzfv;

    invoke-interface {v0}, Lcom/google/android/gms/internal/zzfv;->onStop()V
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_9} :catch_a

    :cond_9
    goto :goto_13

    :catch_a
    move-exception v1

    const-string v0, "Could not forward onStop to ad overlay:"

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p0}, Lcom/google/android/gms/ads/AdActivity;->finish()V

    :goto_13
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public setContentView(I)V
    .registers 2
    .param p1, "layoutResID"    # I

    invoke-super {p0, p1}, Landroid/app/Activity;->setContentView(I)V

    invoke-direct {p0}, Lcom/google/android/gms/ads/AdActivity;->zzaD()V

    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 2
    .param p1, "view"    # Landroid/view/View;

    invoke-super {p0, p1}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/google/android/gms/ads/AdActivity;->zzaD()V

    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0}, Lcom/google/android/gms/ads/AdActivity;->zzaD()V

    return-void
.end method

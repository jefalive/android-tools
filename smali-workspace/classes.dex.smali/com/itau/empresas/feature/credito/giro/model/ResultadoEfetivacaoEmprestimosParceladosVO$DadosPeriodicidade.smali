.class public Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO$DadosPeriodicidade;
.super Ljava/lang/Object;
.source "ResultadoEfetivacaoEmprestimosParceladosVO.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "DadosPeriodicidade"
.end annotation


# instance fields
.field private codigoPeriodicidade:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_periodicidade"
    .end annotation
.end field

.field private descricaoPeriodicidade:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_periodicidade"
    .end annotation
.end field

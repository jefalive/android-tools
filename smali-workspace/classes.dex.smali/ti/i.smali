.class public final Lti/i;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:[B

.field private static c:I


# instance fields
.field private final a:Lorg/json/JSONArray;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x38

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lti/i;->b:[B

    const/16 v0, 0xaf

    sput v0, Lti/i;->c:I

    return-void

    :array_e
    .array-data 1
        0x64t
        0x36t
        0x0t
        -0x1ct
        0x2et
        0x8t
        -0x7t
        -0x6t
        -0x42t
        0x50t
        0x3t
        0x7t
        -0x52t
        0x52t
        -0xdt
        0x13t
        0x3t
        -0xct
        0x4t
        -0x1t
        -0x7t
        0x5t
        0x5t
        -0x5t
        0x6t
        0x6t
        -0xat
        0x8t
        0x0t
        0x20t
        0xft
        0x2t
        0x3t
        0x7t
        -0x52t
        0x52t
        -0xdt
        0x13t
        0x3t
        -0xct
        0x4t
        -0x1t
        -0x7t
        0x5t
        0x5t
        -0x5t
        0x6t
        0x6t
        -0xat
        0x8t
        0x0t
        0x10t
        -0x4t
        -0xat
        0xdt
        0x2t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 2

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    iput-object v0, p0, Lti/i;->a:Lorg/json/JSONArray;

    .line 18
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 7

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    :try_start_3
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lti/i;->a:Lorg/json/JSONArray;
    :try_end_a
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_a} :catch_b

    .line 25
    return-void

    .line 23
    .line 24
    :catch_b
    new-instance v0, Lbr/com/itau/security/commons/exception/CommonSecurityException;

    sget-object v1, Lti/i;->b:[B

    const/16 v2, 0x1f

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lti/i;->b:[B

    const/16 v3, 0x13

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    sget v3, Lti/i;->c:I

    and-int/lit8 v3, v3, 0x74

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lti/i;->a(IIB)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/security/commons/exception/CommonSecurityException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(IIB)Ljava/lang/String;
    .registers 9

    sget-object v5, Lti/i;->b:[B

    rsub-int/lit8 p2, p2, 0x27

    mul-int/lit8 p1, p1, 0x2d

    rsub-int/lit8 p1, p1, 0x70

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/String;

    rsub-int/lit8 v1, p0, 0x11

    move p0, v1

    new-array v1, v1, [B

    if-nez v5, :cond_17

    move v2, p2

    move v3, p1

    :goto_14
    add-int/2addr v2, v3

    add-int/lit8 p1, v2, -0x2

    :cond_17
    add-int/lit8 p2, p2, 0x1

    move v2, v4

    int-to-byte v3, p1

    add-int/lit8 v4, v4, 0x1

    aput-byte v3, v1, v2

    if-ne v4, p0, :cond_26

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_26
    move v2, p1

    aget-byte v3, v5, p2

    goto :goto_14
.end method

.method private a(Lti/e;)Lti/i;
    .registers 7

    .line 46
    :try_start_0
    iget-object v0, p0, Lti/i;->a:Lorg/json/JSONArray;

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    sget-object v2, Lti/i;->b:[B

    const/16 v3, 0x15

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    sget-object v3, Lti/i;->b:[B

    const/4 v4, 0x2

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    or-int/lit8 v4, v3, 0x16

    int-to-byte v4, v4

    invoke-static {v2, v3, v4}, Lti/i;->a(IIB)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lti/e;->a:Ljava/lang/String;

    .line 47
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    sget-object v2, Lti/i;->b:[B

    const/4 v3, 0x2

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    int-to-byte v3, v2

    int-to-byte v4, v3

    invoke-static {v2, v3, v4}, Lti/i;->a(IIB)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lti/e;->b:Ljava/lang/String;

    .line 48
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    .line 46
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_3e
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_3e} :catch_3f

    .line 51
    goto :goto_61

    .line 49
    .line 50
    :catch_3f
    new-instance v0, Lbr/com/itau/security/commons/exception/CommonSecurityException;

    sget-object v1, Lti/i;->b:[B

    const/16 v2, 0x1f

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lti/i;->b:[B

    const/16 v3, 0x13

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    sget v3, Lti/i;->c:I

    and-int/lit8 v3, v3, 0x74

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lti/i;->a(IIB)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/security/commons/exception/CommonSecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :goto_61
    return-object p0
.end method


# virtual methods
.method public final a()Ljava/util/ArrayList;
    .registers 9

    .line 29
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 31
    const/4 v5, 0x0

    :goto_6
    iget-object v0, p0, Lti/i;->a:Lorg/json/JSONArray;

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v5, v0, :cond_71

    .line 33
    :try_start_e
    iget-object v0, p0, Lti/i;->a:Lorg/json/JSONArray;

    invoke-virtual {v0, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 34
    sget-object v0, Lti/i;->b:[B

    const/16 v1, 0x15

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    sget-object v1, Lti/i;->b:[B

    const/4 v2, 0x2

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    or-int/lit8 v2, v1, 0x16

    int-to-byte v2, v2

    invoke-static {v0, v1, v2}, Lti/i;->a(IIB)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 35
    sget-object v0, Lti/i;->b:[B

    const/4 v1, 0x2

    aget-byte v0, v0, v1

    int-to-byte v0, v0

    int-to-byte v1, v0

    int-to-byte v2, v1

    invoke-static {v0, v1, v2}, Lti/i;->a(IIB)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 36
    new-instance v0, Lti/e;

    invoke-direct {v0, v6, v7}, Lti/e;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4c
    .catch Lorg/json/JSONException; {:try_start_e .. :try_end_4c} :catch_4d

    .line 39
    goto :goto_6d

    .line 37
    .line 38
    :catch_4d
    new-instance v0, Lbr/com/itau/security/commons/exception/CommonSecurityException;

    sget-object v1, Lti/i;->b:[B

    const/16 v2, 0x15

    aget-byte v1, v1, v2

    int-to-byte v1, v1

    sget-object v2, Lti/i;->b:[B

    const/16 v3, 0x13

    aget-byte v2, v2, v3

    neg-int v2, v2

    int-to-byte v2, v2

    or-int/lit8 v3, v2, 0xa

    int-to-byte v3, v3

    invoke-static {v1, v2, v3}, Lti/i;->a(IIB)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lbr/com/itau/security/commons/exception/CommonSecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :goto_6d
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_6

    .line 41
    :cond_71
    return-object v4
.end method

.method public final a(Ljava/util/ArrayList;)Lti/i;
    .registers 4

    .line 57
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lti/e;

    .line 58
    invoke-direct {p0, v1}, Lti/i;->a(Lti/e;)Lti/i;

    goto :goto_4

    .line 60
    :cond_15
    return-object p0
.end method

.method public final toString()Ljava/lang/String;
    .registers 2

    .line 65
    iget-object v0, p0, Lti/i;->a:Lorg/json/JSONArray;

    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

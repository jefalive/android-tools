.class synthetic Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer$1;
.super Ljava/lang/Object;
.source "LegendRenderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$br$com$itau$widgets$graphicalfriedcake$charts$components$Legend$LegendForm:[I

.field static final synthetic $SwitchMap$br$com$itau$widgets$graphicalfriedcake$charts$components$Legend$LegendPosition:[I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 394
    invoke-static {}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;->values()[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer$1;->$SwitchMap$br$com$itau$widgets$graphicalfriedcake$charts$components$Legend$LegendForm:[I

    :try_start_9
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer$1;->$SwitchMap$br$com$itau$widgets$graphicalfriedcake$charts$components$Legend$LegendForm:[I

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;->CIRCLE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_14} :catch_15

    goto :goto_16

    :catch_15
    move-exception v3

    :goto_16
    :try_start_16
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer$1;->$SwitchMap$br$com$itau$widgets$graphicalfriedcake$charts$components$Legend$LegendForm:[I

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;->SQUARE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_21} :catch_22

    goto :goto_23

    :catch_22
    move-exception v3

    :goto_23
    :try_start_23
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer$1;->$SwitchMap$br$com$itau$widgets$graphicalfriedcake$charts$components$Legend$LegendForm:[I

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;->LINE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_23 .. :try_end_2e} :catch_2f

    goto :goto_30

    :catch_2f
    move-exception v3

    .line 198
    :goto_30
    invoke-static {}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->values()[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer$1;->$SwitchMap$br$com$itau$widgets$graphicalfriedcake$charts$components$Legend$LegendPosition:[I

    :try_start_39
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer$1;->$SwitchMap$br$com$itau$widgets$graphicalfriedcake$charts$components$Legend$LegendPosition:[I

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->BELOW_CHART_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_44
    .catch Ljava/lang/NoSuchFieldError; {:try_start_39 .. :try_end_44} :catch_45

    goto :goto_46

    :catch_45
    move-exception v3

    :goto_46
    :try_start_46
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer$1;->$SwitchMap$br$com$itau$widgets$graphicalfriedcake$charts$components$Legend$LegendPosition:[I

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->BELOW_CHART_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_51
    .catch Ljava/lang/NoSuchFieldError; {:try_start_46 .. :try_end_51} :catch_52

    goto :goto_53

    :catch_52
    move-exception v3

    :goto_53
    :try_start_53
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer$1;->$SwitchMap$br$com$itau$widgets$graphicalfriedcake$charts$components$Legend$LegendPosition:[I

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->BELOW_CHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_53 .. :try_end_5e} :catch_5f

    goto :goto_60

    :catch_5f
    move-exception v3

    :goto_60
    :try_start_60
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer$1;->$SwitchMap$br$com$itau$widgets$graphicalfriedcake$charts$components$Legend$LegendPosition:[I

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->PIECHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_6b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_60 .. :try_end_6b} :catch_6c

    goto :goto_6d

    :catch_6c
    move-exception v3

    :goto_6d
    :try_start_6d
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer$1;->$SwitchMap$br$com$itau$widgets$graphicalfriedcake$charts$components$Legend$LegendPosition:[I

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->RIGHT_OF_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_78
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6d .. :try_end_78} :catch_79

    goto :goto_7a

    :catch_79
    move-exception v3

    :goto_7a
    :try_start_7a
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer$1;->$SwitchMap$br$com$itau$widgets$graphicalfriedcake$charts$components$Legend$LegendPosition:[I

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->RIGHT_OF_CHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_85
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7a .. :try_end_85} :catch_86

    goto :goto_87

    :catch_86
    move-exception v3

    :goto_87
    :try_start_87
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer$1;->$SwitchMap$br$com$itau$widgets$graphicalfriedcake$charts$components$Legend$LegendPosition:[I

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->RIGHT_OF_CHART_INSIDE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_92
    .catch Ljava/lang/NoSuchFieldError; {:try_start_87 .. :try_end_92} :catch_93

    goto :goto_94

    :catch_93
    move-exception v3

    :goto_94
    :try_start_94
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer$1;->$SwitchMap$br$com$itau$widgets$graphicalfriedcake$charts$components$Legend$LegendPosition:[I

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->LEFT_OF_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_a0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_94 .. :try_end_a0} :catch_a1

    goto :goto_a2

    :catch_a1
    move-exception v3

    :goto_a2
    :try_start_a2
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer$1;->$SwitchMap$br$com$itau$widgets$graphicalfriedcake$charts$components$Legend$LegendPosition:[I

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->LEFT_OF_CHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_ae
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a2 .. :try_end_ae} :catch_af

    goto :goto_b0

    :catch_af
    move-exception v3

    :goto_b0
    :try_start_b0
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer$1;->$SwitchMap$br$com$itau$widgets$graphicalfriedcake$charts$components$Legend$LegendPosition:[I

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->LEFT_OF_CHART_INSIDE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_bc
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b0 .. :try_end_bc} :catch_bd

    goto :goto_be

    :catch_bd
    move-exception v3

    :goto_be
    return-void
.end method

.class public Lbr/com/itau/security/commons/binary/Base32;
.super Lbr/com/itau/security/commons/binary/BaseNCodec;
.source "SourceFile"


# static fields
.field private static final l:[B

.field private static final m:[B

.field private static final n:[B

.field private static final o:[B

.field private static final p:[B

.field private static final w:[B

.field private static x:I


# instance fields
.field private final r:I

.field private final s:[B

.field private final t:I

.field private final u:[B

.field private final v:[B


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 60
    const/16 v0, 0x98

    new-array v0, v0, [B

    fill-array-data v0, :array_3a

    sput-object v0, Lbr/com/itau/security/commons/binary/Base32;->w:[B

    const/16 v0, 0x43

    sput v0, Lbr/com/itau/security/commons/binary/Base32;->x:I

    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_8a

    sput-object v0, Lbr/com/itau/security/commons/binary/Base32;->l:[B

    .line 67
    const/16 v0, 0x5b

    new-array v0, v0, [B

    fill-array-data v0, :array_90

    sput-object v0, Lbr/com/itau/security/commons/binary/Base32;->m:[B

    .line 81
    const/16 v0, 0x20

    new-array v0, v0, [B

    fill-array-data v0, :array_c2

    sput-object v0, Lbr/com/itau/security/commons/binary/Base32;->n:[B

    .line 92
    const/16 v0, 0x57

    new-array v0, v0, [B

    fill-array-data v0, :array_d6

    sput-object v0, Lbr/com/itau/security/commons/binary/Base32;->o:[B

    .line 106
    const/16 v0, 0x20

    new-array v0, v0, [B

    fill-array-data v0, :array_106

    sput-object v0, Lbr/com/itau/security/commons/binary/Base32;->p:[B

    return-void

    :array_3a
    .array-data 1
        0x4ct
        -0x34t
        0x55t
        -0x42t
        -0x1dt
        0x1ft
        -0xft
        0x5t
        0xdt
        -0x41t
        -0x12t
        0x2t
        0x55t
        -0x4bt
        0x4t
        -0x4t
        0xat
        0x13t
        -0x11t
        -0xat
        0x10t
        -0x10t
        0x12t
        -0x12t
        0x6t
        -0x2t
        0x53t
        -0x48t
        -0x9t
        0x54t
        -0x4dt
        -0x6t
        0xat
        0x1t
        -0x23t
        -0x2t
        0x2t
        -0x3t
        0x1t
        0xbt
        0x8t
        -0x9t
        0x8t
        0x46t
        -0x4ct
        -0x1t
        0xct
        -0x10t
        0xat
        -0x8t
        0x3t
        0x54t
        0x4t
        -0x4t
        0xat
        0x1at
        -0x18t
        -0x8t
        0x8t
        -0xct
        0xdt
        0x49t
        0x10t
        -0x2t
        0x45t
        -0x4ct
        -0x7t
        0x3t
        0x0t
        0x55t
        -0x4dt
        0x0t
        -0x4t
        0x55t
        -0x41t
        -0x2t
        0x46t
        -0x48t
        -0x4t
        0x4ft
        -0x40t
        -0xat
        -0x3t
        0x9t
        0x8t
        0x0t
        -0x2t
        -0xet
        0x55t
        -0x4et
        -0x2t
        0x53t
        -0x56t
        0x10t
        0x0t
        -0xat
        0x10t
        -0xdt
        0x4t
        0x10t
        -0x1t
        -0x1t
        0x4t
        -0x4t
        0xat
        0x13t
        -0x11t
        -0xat
        0x10t
        -0x10t
        0x12t
        -0x12t
        0x6t
        -0x2t
        0x53t
        -0x4ct
        -0x7t
        0x3t
        0x0t
        0x55t
        -0x4dt
        0x0t
        -0x4t
        0x55t
        -0x42t
        -0xbt
        0x2t
        -0x5t
        0x14t
        -0x7t
        -0x4t
        0x4ft
        -0x21t
        -0x1et
        -0x11t
        0xft
        0x33t
        0x2t
        0x13t
        -0x42t
        -0x4t
        0x8t
        -0x10t
        0x12t
        -0x1t
        -0x10t
        0x10t
        -0xct
        0x0t
        0x3at
        0x1bt
        -0x3at
    .end array-data

    :array_8a
    .array-data 1
        0xdt
        0xat
    .end array-data

    nop

    :array_90
    .array-data 1
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        0x1at
        0x1bt
        0x1ct
        0x1dt
        0x1et
        0x1ft
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        0x0t
        0x1t
        0x2t
        0x3t
        0x4t
        0x5t
        0x6t
        0x7t
        0x8t
        0x9t
        0xat
        0xbt
        0xct
        0xdt
        0xet
        0xft
        0x10t
        0x11t
        0x12t
        0x13t
        0x14t
        0x15t
        0x16t
        0x17t
        0x18t
        0x19t
    .end array-data

    :array_c2
    .array-data 1
        0x41t
        0x42t
        0x43t
        0x44t
        0x45t
        0x46t
        0x47t
        0x48t
        0x49t
        0x4at
        0x4bt
        0x4ct
        0x4dt
        0x4et
        0x4ft
        0x50t
        0x51t
        0x52t
        0x53t
        0x54t
        0x55t
        0x56t
        0x57t
        0x58t
        0x59t
        0x5at
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
    .end array-data

    :array_d6
    .array-data 1
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        0x0t
        0x1t
        0x2t
        0x3t
        0x4t
        0x5t
        0x6t
        0x7t
        0x8t
        0x9t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        -0x1t
        0xat
        0xbt
        0xct
        0xdt
        0xet
        0xft
        0x10t
        0x11t
        0x12t
        0x13t
        0x14t
        0x15t
        0x16t
        0x17t
        0x18t
        0x19t
        0x1at
        0x1bt
        0x1ct
        0x1dt
        0x1et
        0x1ft
    .end array-data

    :array_106
    .array-data 1
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x41t
        0x42t
        0x43t
        0x44t
        0x45t
        0x46t
        0x47t
        0x48t
        0x49t
        0x4at
        0x4bt
        0x4ct
        0x4dt
        0x4et
        0x4ft
        0x50t
        0x51t
        0x52t
        0x53t
        0x54t
        0x55t
        0x56t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 2

    .line 160
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbr/com/itau/security/commons/binary/Base32;-><init>(Z)V

    .line 161
    return-void
.end method

.method public constructor <init>(I[BZB)V
    .registers 11

    .line 273
    move-object v0, p0

    move v3, p1

    if-nez p2, :cond_6

    const/4 v4, 0x0

    goto :goto_7

    :cond_6
    array-length v4, p2

    :goto_7
    move v5, p4

    const/4 v1, 0x5

    const/16 v2, 0x8

    invoke-direct/range {v0 .. v5}, Lbr/com/itau/security/commons/binary/BaseNCodec;-><init>(IIIIB)V

    .line 275
    if-eqz p3, :cond_19

    .line 276
    sget-object v0, Lbr/com/itau/security/commons/binary/Base32;->p:[B

    iput-object v0, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    .line 277
    sget-object v0, Lbr/com/itau/security/commons/binary/Base32;->o:[B

    iput-object v0, p0, Lbr/com/itau/security/commons/binary/Base32;->s:[B

    goto :goto_21

    .line 279
    :cond_19
    sget-object v0, Lbr/com/itau/security/commons/binary/Base32;->n:[B

    iput-object v0, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    .line 280
    sget-object v0, Lbr/com/itau/security/commons/binary/Base32;->m:[B

    iput-object v0, p0, Lbr/com/itau/security/commons/binary/Base32;->s:[B

    .line 282
    :goto_21
    if-lez p1, :cond_d8

    .line 283
    if-nez p2, :cond_75

    .line 284
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lbr/com/itau/security/commons/binary/Base32;->w:[B

    const/16 v3, 0x88

    aget-byte v2, v2, v3

    add-int/lit8 v2, v2, -0x1

    int-to-byte v2, v2

    sget-object v3, Lbr/com/itau/security/commons/binary/Base32;->w:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    sget-object v4, Lbr/com/itau/security/commons/binary/Base32;->w:[B

    const/16 v5, 0x80

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v2, v3, v4}, Lbr/com/itau/security/commons/binary/Base32;->a(IBS)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lbr/com/itau/security/commons/binary/Base32;->w:[B

    const/16 v3, 0x3d

    aget-byte v2, v2, v3

    add-int/lit8 v2, v2, 0x1

    int-to-byte v2, v2

    sget-object v3, Lbr/com/itau/security/commons/binary/Base32;->w:[B

    const/16 v4, 0x44

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    sget-object v4, Lbr/com/itau/security/commons/binary/Base32;->w:[B

    const/16 v5, 0x10

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v2, v3, v4}, Lbr/com/itau/security/commons/binary/Base32;->a(IBS)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 287
    :cond_75
    invoke-virtual {p0, p2}, Lbr/com/itau/security/commons/binary/Base32;->a([B)Z

    move-result v0

    if-eqz v0, :cond_c5

    .line 288
    invoke-static {p2}, Lbr/com/itau/security/commons/binary/ByteUtils;->to([B)Ljava/lang/String;

    move-result-object p1

    .line 289
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lbr/com/itau/security/commons/binary/Base32;->w:[B

    const/16 v3, 0x37

    aget-byte v2, v2, v3

    add-int/lit8 v2, v2, -0x1

    int-to-byte v2, v2

    sget-object v3, Lbr/com/itau/security/commons/binary/Base32;->w:[B

    const/4 v4, 0x0

    aget-byte v3, v3, v4

    int-to-byte v3, v3

    sget-object v4, Lbr/com/itau/security/commons/binary/Base32;->w:[B

    const/16 v5, 0x44

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v2, v3, v4}, Lbr/com/itau/security/commons/binary/Base32;->a(IBS)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lbr/com/itau/security/commons/binary/Base32;->w:[B

    const/16 v3, 0x44

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    or-int/lit8 v3, v2, 0x3d

    int-to-byte v3, v3

    and-int/lit8 v4, v3, 0x5b

    int-to-byte v4, v4

    invoke-static {v2, v3, v4}, Lbr/com/itau/security/commons/binary/Base32;->a(IBS)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 291
    :cond_c5
    array-length v0, p2

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Lbr/com/itau/security/commons/binary/Base32;->t:I

    .line 292
    array-length v0, p2

    new-array v0, v0, [B

    iput-object v0, p0, Lbr/com/itau/security/commons/binary/Base32;->v:[B

    .line 293
    iget-object v0, p0, Lbr/com/itau/security/commons/binary/Base32;->v:[B

    array-length v1, p2

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {p2, v2, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_df

    .line 295
    :cond_d8
    const/16 v0, 0x8

    iput v0, p0, Lbr/com/itau/security/commons/binary/Base32;->t:I

    .line 296
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/security/commons/binary/Base32;->v:[B

    .line 298
    :goto_df
    iget v0, p0, Lbr/com/itau/security/commons/binary/Base32;->t:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbr/com/itau/security/commons/binary/Base32;->r:I

    .line 300
    invoke-virtual {p0, p4}, Lbr/com/itau/security/commons/binary/Base32;->isInAlphabet(B)Z

    move-result v0

    if-nez v0, :cond_f1

    invoke-static {p4}, Lbr/com/itau/security/commons/binary/Base32;->a(B)Z

    move-result v0

    if-eqz v0, :cond_10c

    .line 301
    :cond_f1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lbr/com/itau/security/commons/binary/Base32;->w:[B

    const/16 v2, 0x4f

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, 0x1

    int-to-byte v1, v1

    sget-object v2, Lbr/com/itau/security/commons/binary/Base32;->w:[B

    const/4 v3, 0x7

    aget-byte v2, v2, v3

    int-to-byte v2, v2

    const/16 v3, 0x2d

    invoke-static {v3, v1, v2}, Lbr/com/itau/security/commons/binary/Base32;->a(IBS)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 303
    :cond_10c
    return-void
.end method

.method public constructor <init>(Z)V
    .registers 5

    .line 184
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/16 v2, 0x3d

    invoke-direct {p0, v0, v1, p1, v2}, Lbr/com/itau/security/commons/binary/Base32;-><init>(I[BZB)V

    .line 185
    return-void
.end method

.method private static a(IBS)Ljava/lang/String;
    .registers 9

    const/4 v5, 0x0

    new-instance v0, Ljava/lang/String;

    sget-object v4, Lbr/com/itau/security/commons/binary/Base32;->w:[B

    mul-int/lit8 p2, p2, 0x2

    rsub-int/lit8 p2, p2, 0x33

    add-int/lit8 p1, p1, 0x20

    mul-int/lit8 p0, p0, 0x2

    rsub-int p0, p0, 0x97

    new-array v1, p2, [B

    if-nez v4, :cond_19

    move v2, p1

    move v3, p0

    :goto_15
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p1, v2, 0x1

    :cond_19
    add-int/lit8 p0, p0, 0x1

    int-to-byte v2, p1

    aput-byte v2, v1, v5

    add-int/lit8 v5, v5, 0x1

    if-ne v5, p2, :cond_2b

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2b
    move v2, p1

    aget-byte v3, v4, p0

    goto :goto_15
.end method


# virtual methods
.method final b([BIILcc/b;)V
    .registers 14

    .line 421
    iget-boolean v0, p4, Lcc/b;->f:Z

    if-eqz v0, :cond_5

    .line 422
    return-void

    .line 426
    :cond_5
    if-gez p3, :cond_279

    .line 427
    const/4 v0, 0x1

    iput-boolean v0, p4, Lcc/b;->f:Z

    .line 428
    iget v0, p4, Lcc/b;->h:I

    if-nez v0, :cond_13

    iget v0, p0, Lbr/com/itau/security/commons/binary/Base32;->h:I

    if-nez v0, :cond_13

    .line 429
    return-void

    .line 431
    :cond_13
    iget v8, p0, Lbr/com/itau/security/commons/binary/Base32;->t:I

    move-object v7, p4

    .line 3279
    iget-object v0, v7, Lcc/b;->c:[B

    if-eqz v0, :cond_22

    iget-object v0, v7, Lcc/b;->c:[B

    array-length v0, v0

    iget v1, v7, Lcc/b;->d:I

    add-int/2addr v1, v8

    if-ge v0, v1, :cond_27

    .line 3280
    :cond_22
    invoke-static {v7}, Lbr/com/itau/security/commons/binary/BaseNCodec;->a(Lcc/b;)[B

    move-result-object v6

    goto :goto_29

    .line 3282
    :cond_27
    iget-object v6, v7, Lcc/b;->c:[B

    .line 431
    .line 432
    :goto_29
    iget v7, p4, Lcc/b;->d:I

    .line 433
    iget v0, p4, Lcc/b;->h:I

    packed-switch v0, :pswitch_data_378

    goto/16 :goto_225

    .line 435
    :pswitch_32
    goto/16 :goto_255

    .line 437
    :pswitch_34
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/4 v4, 0x3

    shr-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v6, v0

    .line 438
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/4 v4, 0x2

    shl-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v6, v0

    .line 439
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-byte v1, p0, Lbr/com/itau/security/commons/binary/Base32;->g:B

    aput-byte v1, v6, v0

    .line 440
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-byte v1, p0, Lbr/com/itau/security/commons/binary/Base32;->g:B

    aput-byte v1, v6, v0

    .line 441
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-byte v1, p0, Lbr/com/itau/security/commons/binary/Base32;->g:B

    aput-byte v1, v6, v0

    .line 442
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-byte v1, p0, Lbr/com/itau/security/commons/binary/Base32;->g:B

    aput-byte v1, v6, v0

    .line 443
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-byte v1, p0, Lbr/com/itau/security/commons/binary/Base32;->g:B

    aput-byte v1, v6, v0

    .line 444
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-byte v1, p0, Lbr/com/itau/security/commons/binary/Base32;->g:B

    aput-byte v1, v6, v0

    .line 445
    goto/16 :goto_255

    .line 447
    :pswitch_98
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/16 v4, 0xb

    shr-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v6, v0

    .line 448
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/4 v4, 0x6

    shr-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v6, v0

    .line 449
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/4 v4, 0x1

    shr-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v6, v0

    .line 450
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/4 v4, 0x4

    shl-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v6, v0

    .line 451
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-byte v1, p0, Lbr/com/itau/security/commons/binary/Base32;->g:B

    aput-byte v1, v6, v0

    .line 452
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-byte v1, p0, Lbr/com/itau/security/commons/binary/Base32;->g:B

    aput-byte v1, v6, v0

    .line 453
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-byte v1, p0, Lbr/com/itau/security/commons/binary/Base32;->g:B

    aput-byte v1, v6, v0

    .line 454
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-byte v1, p0, Lbr/com/itau/security/commons/binary/Base32;->g:B

    aput-byte v1, v6, v0

    .line 455
    goto/16 :goto_255

    .line 457
    :pswitch_10f
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/16 v4, 0x13

    shr-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v6, v0

    .line 458
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/16 v4, 0xe

    shr-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v6, v0

    .line 459
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/16 v4, 0x9

    shr-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v6, v0

    .line 460
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/4 v4, 0x4

    shr-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v6, v0

    .line 461
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/4 v4, 0x1

    shl-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v6, v0

    .line 462
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-byte v1, p0, Lbr/com/itau/security/commons/binary/Base32;->g:B

    aput-byte v1, v6, v0

    .line 463
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-byte v1, p0, Lbr/com/itau/security/commons/binary/Base32;->g:B

    aput-byte v1, v6, v0

    .line 464
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-byte v1, p0, Lbr/com/itau/security/commons/binary/Base32;->g:B

    aput-byte v1, v6, v0

    .line 465
    goto/16 :goto_255

    .line 467
    :pswitch_191
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/16 v4, 0x1b

    shr-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v6, v0

    .line 468
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/16 v4, 0x16

    shr-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v6, v0

    .line 469
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/16 v4, 0x11

    shr-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v6, v0

    .line 470
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/16 v4, 0xc

    shr-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v6, v0

    .line 471
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/4 v4, 0x7

    shr-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v6, v0

    .line 472
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/4 v4, 0x2

    shr-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v6, v0

    .line 473
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/4 v4, 0x3

    shl-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v6, v0

    .line 474
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-byte v1, p0, Lbr/com/itau/security/commons/binary/Base32;->g:B

    aput-byte v1, v6, v0

    .line 475
    goto :goto_255

    .line 477
    :goto_225
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lbr/com/itau/security/commons/binary/Base32;->w:[B

    const/16 v3, 0x95

    aget-byte v2, v2, v3

    add-int/lit8 v2, v2, 0x1

    int-to-byte v2, v2

    and-int/lit16 v3, v2, 0xed

    int-to-byte v3, v3

    sget-object v4, Lbr/com/itau/security/commons/binary/Base32;->w:[B

    const/16 v5, 0x14

    aget-byte v4, v4, v5

    int-to-byte v4, v4

    invoke-static {v2, v3, v4}, Lbr/com/itau/security/commons/binary/Base32;->a(IBS)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p4, Lcc/b;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 479
    :goto_255
    iget v0, p4, Lcc/b;->g:I

    iget v1, p4, Lcc/b;->d:I

    sub-int/2addr v1, v7

    add-int/2addr v0, v1

    iput v0, p4, Lcc/b;->g:I

    .line 481
    iget v0, p0, Lbr/com/itau/security/commons/binary/Base32;->h:I

    if-lez v0, :cond_278

    iget v0, p4, Lcc/b;->g:I

    if-lez v0, :cond_278

    .line 482
    iget-object v0, p0, Lbr/com/itau/security/commons/binary/Base32;->v:[B

    iget v1, p4, Lcc/b;->d:I

    iget-object v2, p0, Lbr/com/itau/security/commons/binary/Base32;->v:[B

    array-length v2, v2

    const/4 v3, 0x0

    invoke-static {v0, v3, v6, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 483
    iget v0, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->v:[B

    array-length v1, v1

    add-int/2addr v0, v1

    iput v0, p4, Lcc/b;->d:I

    .line 485
    :cond_278
    return-void

    .line 486
    :cond_279
    const/4 v6, 0x0

    :goto_27a
    if-ge v6, p3, :cond_377

    .line 487
    iget v8, p0, Lbr/com/itau/security/commons/binary/Base32;->t:I

    move-object v7, p4

    .line 4279
    iget-object v0, v7, Lcc/b;->c:[B

    if-eqz v0, :cond_28b

    iget-object v0, v7, Lcc/b;->c:[B

    array-length v0, v0

    iget v1, v7, Lcc/b;->d:I

    add-int/2addr v1, v8

    if-ge v0, v1, :cond_290

    .line 4280
    :cond_28b
    invoke-static {v7}, Lbr/com/itau/security/commons/binary/BaseNCodec;->a(Lcc/b;)[B

    move-result-object v7

    goto :goto_292

    .line 4282
    :cond_290
    iget-object v7, v7, Lcc/b;->c:[B

    .line 487
    .line 488
    :goto_292
    iget v0, p4, Lcc/b;->h:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0x5

    iput v0, p4, Lcc/b;->h:I

    .line 489
    move v0, p2

    add-int/lit8 p2, p2, 0x1

    aget-byte v0, p1, v0

    .line 490
    move v8, v0

    if-gez v0, :cond_2a4

    .line 491
    add-int/lit16 v8, v8, 0x100

    .line 493
    :cond_2a4
    iget-wide v0, p4, Lcc/b;->b:J

    const/16 v2, 0x8

    shl-long/2addr v0, v2

    int-to-long v2, v8

    add-long/2addr v0, v2

    iput-wide v0, p4, Lcc/b;->b:J

    .line 494
    iget v0, p4, Lcc/b;->h:I

    if-nez v0, :cond_373

    .line 495
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/16 v4, 0x23

    shr-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v7, v0

    .line 496
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/16 v4, 0x1e

    shr-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v7, v0

    .line 497
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/16 v4, 0x19

    shr-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v7, v0

    .line 498
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/16 v4, 0x14

    shr-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v7, v0

    .line 499
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/16 v4, 0xf

    shr-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v7, v0

    .line 500
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/16 v4, 0xa

    shr-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v7, v0

    .line 501
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    const/4 v4, 0x5

    shr-long/2addr v2, v4

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v7, v0

    .line 502
    iget v0, p4, Lcc/b;->d:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->u:[B

    iget-wide v2, p4, Lcc/b;->b:J

    long-to-int v2, v2

    and-int/lit8 v2, v2, 0x1f

    aget-byte v1, v1, v2

    aput-byte v1, v7, v0

    .line 503
    iget v0, p4, Lcc/b;->g:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p4, Lcc/b;->g:I

    .line 504
    iget v0, p0, Lbr/com/itau/security/commons/binary/Base32;->h:I

    if-lez v0, :cond_373

    iget v0, p0, Lbr/com/itau/security/commons/binary/Base32;->h:I

    iget v1, p4, Lcc/b;->g:I

    if-gt v0, v1, :cond_373

    .line 505
    iget-object v0, p0, Lbr/com/itau/security/commons/binary/Base32;->v:[B

    iget v1, p4, Lcc/b;->d:I

    iget-object v2, p0, Lbr/com/itau/security/commons/binary/Base32;->v:[B

    array-length v2, v2

    const/4 v3, 0x0

    invoke-static {v0, v3, v7, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 506
    iget v0, p4, Lcc/b;->d:I

    iget-object v1, p0, Lbr/com/itau/security/commons/binary/Base32;->v:[B

    array-length v1, v1

    add-int/2addr v0, v1

    iput v0, p4, Lcc/b;->d:I

    .line 507
    const/4 v0, 0x0

    iput v0, p4, Lcc/b;->g:I

    .line 486
    :cond_373
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_27a

    .line 512
    :cond_377
    return-void

    :pswitch_data_378
    .packed-switch 0x0
        :pswitch_32
        :pswitch_34
        :pswitch_98
        :pswitch_10f
        :pswitch_191
    .end packed-switch
.end method

.method public isInAlphabet(B)Z
    .registers 4

    .line 522
    if-ltz p1, :cond_10

    iget-object v0, p0, Lbr/com/itau/security/commons/binary/Base32;->s:[B

    array-length v0, v0

    if-ge p1, v0, :cond_10

    iget-object v0, p0, Lbr/com/itau/security/commons/binary/Base32;->s:[B

    aget-byte v0, v0, p1

    const/4 v1, -0x1

    if-eq v0, v1, :cond_10

    const/4 v0, 0x1

    return v0

    :cond_10
    const/4 v0, 0x0

    return v0
.end method

.class Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "ReceiptsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReceiptHolder"
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;


# direct methods
.method public constructor <init>(Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;Landroid/view/View;)V
    .registers 3
    .param p2, "itemView"    # Landroid/view/View;

    .line 103
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptHolder;->this$0:Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;

    .line 104
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 105
    return-void
.end method


# virtual methods
.method public bindReceipt(Lbr/com/itau/textovoz/model/Receipt;)V
    .registers 10
    .param p1, "receipt"    # Lbr/com/itau/textovoz/model/Receipt;

    .line 108
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptHolder;->this$0:Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;

    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Receipt;->getTransactionValue()Ljava/lang/String;

    move-result-object v1

    # invokes: Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->getValueAsCurrency(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v1}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->access$300(Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 109
    .local v2, "currencyValue":Ljava/lang/String;
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptHolder;->this$0:Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;

    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Receipt;->getTransactionDate()Ljava/lang/String;

    move-result-object v1

    # invokes: Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->formatDateString(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v1}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->access$400(Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 111
    .local v3, "date":Ljava/lang/String;
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptHolder;->itemView:Landroid/view/View;

    sget v1, Lbr/com/itau/textovoz/R$id;->receipt_title_textView:I

    .line 112
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    .line 113
    .local v4, "itemTitleTextView":Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptHolder;->itemView:Landroid/view/View;

    sget v1, Lbr/com/itau/textovoz/R$id;->receipt_value_textView:I

    .line 114
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    .line 115
    .local v5, "itemValueTextView":Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptHolder;->itemView:Landroid/view/View;

    sget v1, Lbr/com/itau/textovoz/R$id;->receipt_description_textView:I

    .line 116
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    .line 117
    .local v6, "itemHistoryTextView":Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptHolder;->itemView:Landroid/view/View;

    sget v1, Lbr/com/itau/textovoz/R$id;->receipt_date_textView:I

    .line 118
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    .line 120
    .local v7, "itemDateTextView":Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Receipt;->getTransactionDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    invoke-virtual {v5, v2}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    invoke-virtual {v7, v3}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Receipt;->getTransactionHistory()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    return-void
.end method

.class final Lokhttp3/internal/framed/Http2$FrameLogger;
.super Ljava/lang/Object;
.source "Http2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lokhttp3/internal/framed/Http2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "FrameLogger"
.end annotation


# static fields
.field private static final BINARY:[Ljava/lang/String;

.field private static final FLAGS:[Ljava/lang/String;

.field private static final TYPES:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 15

    .line 706
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "DATA"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "HEADERS"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "PRIORITY"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "RST_STREAM"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "SETTINGS"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const-string v1, "PUSH_PROMISE"

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const-string v1, "PING"

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const-string v1, "GOAWAY"

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const-string v1, "WINDOW_UPDATE"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const-string v1, "CONTINUATION"

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sput-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->TYPES:[Ljava/lang/String;

    .line 723
    const/16 v0, 0x40

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->FLAGS:[Ljava/lang/String;

    .line 724
    const/16 v0, 0x100

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->BINARY:[Ljava/lang/String;

    .line 727
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_47
    sget-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->BINARY:[Ljava/lang/String;

    array-length v0, v0

    if-ge v5, v0, :cond_6b

    .line 728
    sget-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->BINARY:[Ljava/lang/String;

    const-string v1, "%8s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lokhttp3/internal/Util;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x20

    const/16 v3, 0x30

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    .line 727
    add-int/lit8 v5, v5, 0x1

    goto :goto_47

    .line 731
    .end local v5    # "i":I
    :cond_6b
    sget-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->FLAGS:[Ljava/lang/String;

    const-string v1, ""

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 732
    sget-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->FLAGS:[Ljava/lang/String;

    const-string v1, "END_STREAM"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 734
    const/4 v0, 0x1

    new-array v5, v0, [I

    fill-array-data v5, :array_14a

    .line 736
    .local v5, "prefixFlags":[I
    sget-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->FLAGS:[Ljava/lang/String;

    const-string v1, "PADDED"

    const/16 v2, 0x8

    aput-object v1, v0, v2

    .line 737
    move-object v6, v5

    array-length v7, v6

    const/4 v8, 0x0

    :goto_8a
    if-ge v8, v7, :cond_ae

    aget v9, v6, v8

    .line 738
    .local v9, "prefixFlag":I
    sget-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->FLAGS:[Ljava/lang/String;

    or-int/lit8 v1, v9, 0x8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lokhttp3/internal/framed/Http2$FrameLogger;->FLAGS:[Ljava/lang/String;

    aget-object v3, v3, v9

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "|PADDED"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 737
    .end local v9    # "prefixFlag":I
    add-int/lit8 v8, v8, 0x1

    goto :goto_8a

    .line 741
    :cond_ae
    sget-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->FLAGS:[Ljava/lang/String;

    const-string v1, "END_HEADERS"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 742
    sget-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->FLAGS:[Ljava/lang/String;

    const-string v1, "PRIORITY"

    const/16 v2, 0x20

    aput-object v1, v0, v2

    .line 743
    sget-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->FLAGS:[Ljava/lang/String;

    const-string v1, "END_HEADERS|PRIORITY"

    const/16 v2, 0x24

    aput-object v1, v0, v2

    .line 744
    const/4 v0, 0x3

    new-array v6, v0, [I

    fill-array-data v6, :array_150

    .line 747
    .local v6, "frameFlags":[I
    move-object v7, v6

    array-length v8, v7

    const/4 v9, 0x0

    :goto_ce
    if-ge v9, v8, :cond_132

    aget v10, v7, v9

    .line 748
    .local v10, "frameFlag":I
    move-object v11, v5

    array-length v12, v11

    const/4 v13, 0x0

    :goto_d5
    if-ge v13, v12, :cond_12e

    aget v14, v11, v13

    .line 749
    .local v14, "prefixFlag":I
    sget-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->FLAGS:[Ljava/lang/String;

    or-int v1, v14, v10

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lokhttp3/internal/framed/Http2$FrameLogger;->FLAGS:[Ljava/lang/String;

    aget-object v3, v3, v14

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x7c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lokhttp3/internal/framed/Http2$FrameLogger;->FLAGS:[Ljava/lang/String;

    aget-object v3, v3, v10

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 750
    sget-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->FLAGS:[Ljava/lang/String;

    or-int v1, v14, v10

    or-int/lit8 v1, v1, 0x8

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lokhttp3/internal/framed/Http2$FrameLogger;->FLAGS:[Ljava/lang/String;

    aget-object v3, v3, v14

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x7c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lokhttp3/internal/framed/Http2$FrameLogger;->FLAGS:[Ljava/lang/String;

    aget-object v3, v3, v10

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "|PADDED"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 748
    .end local v14    # "prefixFlag":I
    add-int/lit8 v13, v13, 0x1

    goto :goto_d5

    .line 747
    .end local v10    # "frameFlag":I
    :cond_12e
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_ce

    .line 755
    :cond_132
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_133
    sget-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->FLAGS:[Ljava/lang/String;

    array-length v0, v0

    if-ge v7, v0, :cond_149

    .line 756
    sget-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->FLAGS:[Ljava/lang/String;

    aget-object v0, v0, v7

    if-nez v0, :cond_146

    sget-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->FLAGS:[Ljava/lang/String;

    sget-object v1, Lokhttp3/internal/framed/Http2$FrameLogger;->BINARY:[Ljava/lang/String;

    aget-object v1, v1, v7

    aput-object v1, v0, v7

    .line 755
    :cond_146
    add-int/lit8 v7, v7, 0x1

    goto :goto_133

    .line 758
    .end local v5    # "prefixFlags":[I
    .end local v6    # "frameFlags":[I
    .end local v7    # "i":I
    :cond_149
    return-void

    :array_14a
    .array-data 4
        0x1
    .end array-data

    :array_150
    .array-data 4
        0x4
        0x20
        0x24
    .end array-data
.end method

.method constructor <init>()V
    .registers 1

    .line 669
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static formatFlags(BB)Ljava/lang/String;
    .registers 5
    .param p0, "type"    # B
    .param p1, "flags"    # B

    .line 684
    if-nez p1, :cond_5

    const-string v0, ""

    return-object v0

    .line 685
    :cond_5
    packed-switch p0, :pswitch_data_48

    goto :goto_19

    .line 688
    :pswitch_9
    const/4 v0, 0x1

    if-ne p1, v0, :cond_f

    const-string v0, "ACK"

    goto :goto_13

    :cond_f
    sget-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->BINARY:[Ljava/lang/String;

    aget-object v0, v0, p1

    :goto_13
    return-object v0

    .line 693
    :pswitch_14
    sget-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->BINARY:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0

    .line 695
    :goto_19
    :pswitch_19
    sget-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->FLAGS:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_23

    sget-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->FLAGS:[Ljava/lang/String;

    aget-object v2, v0, p1

    goto :goto_27

    :cond_23
    sget-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->BINARY:[Ljava/lang/String;

    aget-object v2, v0, p1

    .line 697
    .local v2, "result":Ljava/lang/String;
    :goto_27
    const/4 v0, 0x5

    if-ne p0, v0, :cond_37

    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_37

    .line 698
    const-string v0, "HEADERS"

    const-string v1, "PUSH_PROMISE"

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 699
    :cond_37
    if-nez p0, :cond_46

    and-int/lit8 v0, p1, 0x20

    if-eqz v0, :cond_46

    .line 700
    const-string v0, "PRIORITY"

    const-string v1, "COMPRESSED"

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 702
    :cond_46
    return-object v2

    nop

    :pswitch_data_48
    .packed-switch 0x2
        :pswitch_14
        :pswitch_14
        :pswitch_9
        :pswitch_19
        :pswitch_9
        :pswitch_14
        :pswitch_14
    .end packed-switch
.end method

.method static formatHeader(ZIIBB)Ljava/lang/String;
    .registers 11
    .param p0, "inbound"    # Z
    .param p1, "streamId"    # I
    .param p2, "length"    # I
    .param p3, "type"    # B
    .param p4, "flags"    # B

    .line 672
    sget-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->TYPES:[Ljava/lang/String;

    array-length v0, v0

    if-ge p3, v0, :cond_a

    sget-object v0, Lokhttp3/internal/framed/Http2$FrameLogger;->TYPES:[Ljava/lang/String;

    aget-object v4, v0, p3

    goto :goto_1a

    :cond_a
    const-string v0, "0x%02x"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lokhttp3/internal/Util;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 673
    .local v4, "formattedType":Ljava/lang/String;
    :goto_1a
    invoke-static {p3, p4}, Lokhttp3/internal/framed/Http2$FrameLogger;->formatFlags(BB)Ljava/lang/String;

    move-result-object v5

    .line 674
    .local v5, "formattedFlags":Ljava/lang/String;
    const-string v0, "%s 0x%08x %5d %-13s %s"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    if-eqz p0, :cond_28

    const-string v2, "<<"

    goto :goto_2a

    :cond_28
    const-string v2, ">>"

    :goto_2a
    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    const/4 v2, 0x3

    aput-object v4, v1, v2

    const/4 v2, 0x4

    aput-object v5, v1, v2

    invoke-static {v0, v1}, Lokhttp3/internal/Util;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

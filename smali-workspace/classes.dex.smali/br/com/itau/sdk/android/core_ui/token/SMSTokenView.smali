.class public final Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;
.super Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;
.source "SourceFile"


# static fields
.field private static final B:[S

.field private static C:I


# instance fields
.field private A:Ljava/lang/Runnable;

.field private u:Landroid/widget/TextView;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/view/View;

.field private x:Z

.field private y:Landroid/widget/ProgressBar;

.field private z:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x62

    new-array v0, v0, [S

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->B:[S

    const/16 v0, 0xf9

    sput v0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->C:I

    return-void

    :array_e
    .array-data 2
        0x31s
        -0x1cs
        -0x22s
        0x4es
        0x92s
        -0x77s
        -0x52s
        0x43s
        0x6s
        -0x9s
        -0x4cs
        0x4ds
        -0x4s
        0x1s
        -0x3s
        0x77s
        -0x7as
        -0x14s
        0x4s
        -0x4fs
        0x42s
        0x6s
        0x5s
        -0x10s
        -0xbs
        0xes
        -0x55s
        0x4cs
        -0x52s
        0x30s
        -0x9s
        0x3s
        -0x28s
        -0x9s
        0x3s
        -0x3s
        0x43s
        0xes
        0xes
        -0x14s
        -0x44s
        0x41s
        -0x2s
        0xbs
        -0x14s
        0x7s
        0x0s
        -0x1s
        0x1s
        -0x13s
        -0x7s
        0xes
        -0x55s
        0x50s
        -0x11s
        0xds
        -0x58s
        0x46s
        -0x18s
        0x18s
        -0x7s
        -0x9s
        0x6s
        -0x45s
        -0xfs
        0x41s
        0x2s
        -0x5s
        -0x1s
        0x8s
        -0x12s
        -0x48s
        0x4cs
        0x1s
        -0x56s
        0x41s
        0x8s
        -0x9s
        0x7s
        -0x56s
        0x40s
        0x8ds
        -0x92s
        0x2s
        -0x5s
        0x5s
        0x1s
        -0x56s
        0x4fs
        -0x10s
        -0x5s
        -0x1s
        -0x6s
        0x4s
        -0x8s
        0x8s
        0x1s
        -0x48s
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    .line 36
    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;-><init>(Landroid/content/Context;)V

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->x:Z

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4

    .line 40
    invoke-direct {p0, p1, p2}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->x:Z

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5

    .line 44
    invoke-direct {p0, p1, p2, p3}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->x:Z

    .line 45
    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    add-int/lit8 p0, p0, 0xa

    rsub-int/lit8 p2, p2, 0x22

    sget-object v5, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->B:[S

    add-int/lit8 p1, p1, 0x3

    const/4 v4, 0x0

    new-instance v0, Ljava/lang/String;

    new-array v1, p1, [B

    if-nez v5, :cond_14

    move v2, p0

    move v3, p2

    :goto_11
    add-int/2addr v2, v3

    add-int/lit8 p0, v2, 0x3

    :cond_14
    int-to-byte v2, p0

    aput-byte v2, v1, v4

    add-int/lit8 v4, v4, 0x1

    add-int/lit8 p2, p2, 0x1

    if-ne v4, p1, :cond_22

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_22
    move v2, p0

    aget-short v3, v5, p2

    goto :goto_11
.end method

.method private synthetic a(Landroid/view/View;)V
    .registers 2

    .line 125
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->g()V

    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;)V
    .registers 1

    invoke-direct {p0}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->h()V

    return-void
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;Landroid/view/View;)V
    .registers 2

    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->a(Landroid/view/View;)V

    return-void
.end method

.method private f()V
    .registers 2

    .line 134
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->y:Landroid/widget/ProgressBar;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core_ui/theme/b;->a(Landroid/widget/ProgressBar;)V

    .line 135
    return-void
.end method

.method private g()V
    .registers 4

    .line 181
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;->TOKEN:Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;

    sget-object v2, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->RESEND:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/sdk/android/core/BackendClient;->track(Lbr/com/itau/sdk/android/core/token/TokenEvent$Category;Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;)V

    .line 183
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->q:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 184
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->o:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 185
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->p:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 186
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->y:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 187
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->g:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 188
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->u:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->u:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 190
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->v:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 191
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->w:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 192
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->s:Z

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/SDKCustomUiController;->enviaSMS(Z)V

    .line 193
    return-void
.end method

.method private synthetic h()V
    .registers 3

    .line 69
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->f:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method


# virtual methods
.method a()I
    .registers 2

    .line 111
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$layout;->itausdkcore_view_sms_token_type:I

    return v0
.end method

.method a(Landroid/util/AttributeSet;)V
    .registers 5

    .line 116
    invoke-super {p0, p1}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->a(Landroid/util/AttributeSet;)V

    .line 118
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->message_number_text:I

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->u:Landroid/widget/TextView;

    .line 119
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->u:Landroid/widget/TextView;

    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v1

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    invoke-static {v1, v2}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getFontRegular(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 120
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->message_number_time:I

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->v:Landroid/widget/TextView;

    .line 121
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->divisor_sms:I

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->w:Landroid/view/View;

    .line 122
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->v:Landroid/widget/TextView;

    invoke-static {}, Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;->getInstance()Lbr/com/itau/sdk/android/core_ui/theme/SDKThemeManager;

    move-result-object v1

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    invoke-static {v1, v2}, Lbr/com/itau/sdk/android/core/util/TypefaceUtils;->getFontRegular(Lbr/com/itau/sdk/android/core/theme/BaseSDKThemeManager;Landroid/content/res/AssetManager;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 123
    sget v0, Lbr/com/itau/sdk/android/core_ui/R$id;->token_loading:I

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->y:Landroid/widget/ProgressBar;

    .line 124
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->f()V

    .line 125
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->f:Landroid/widget/Button;

    invoke-static {p0}, Lbr/com/itau/sdk/android/core_ui/token/f;->a(Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->g:Landroid/widget/EditText;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$string;->itausdkcore_view_token_type_sms_edit_text_description:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 128
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->h:Landroid/widget/EditText;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$string;->itausdkcore_view_token_type_sms_edit_text_description:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 129
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->f:Landroid/widget/Button;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$string;->itausdkcore_dialog_token_sms_resend_action_accessibility:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->t:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lbr/com/itau/sdk/android/core_ui/R$string;->itausdkcore_view_token_type_sms_description:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 131
    return-void
.end method

.method a(Ljava/lang/String;)V
    .registers 6

    .line 171
    sget v0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->C:I

    and-int/lit16 v0, v0, 0x16f

    sget-object v1, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->B:[S

    const/16 v2, 0x2e

    aget-short v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->B:[S

    const/16 v3, 0x42

    aget-short v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_20

    .line 172
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->setVisibility(I)V

    .line 173
    return-void

    .line 176
    :cond_20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->s:Z

    .line 177
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->g()V

    .line 178
    return-void
.end method

.method b()V
    .registers 3

    .line 140
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->x:Z

    if-nez v0, :cond_15

    .line 141
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->y:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 142
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->q:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 143
    invoke-super {p0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->b()V

    .line 144
    return-void

    .line 147
    :cond_15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->x:Z

    .line 148
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->g()V

    .line 149
    return-void
.end method

.method c()V
    .registers 3

    .line 153
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->q:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 154
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->x:Z

    .line 155
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->d()V

    .line 156
    return-void
.end method

.method protected getSelectionAction()Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;
    .registers 2

    .line 160
    sget-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->SMS_SELECTED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    return-object v0
.end method

.method protected getSendAction()Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;
    .registers 2

    .line 165
    sget-object v0, Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;->SMS_CONFIRMED:Lbr/com/itau/sdk/android/core/token/TokenEvent$Action;

    return-object v0
.end method

.method getType()I
    .registers 2

    .line 197
    const/4 v0, 0x1

    return v0
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/SDKCustomUiController$EnvioSMSResponseDTO;)V
    .registers 13

    .line 58
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/SDKCustomUiController$EnvioSMSResponseDTO;->isSucesso()Z

    move-result v0

    if-nez v0, :cond_28

    .line 59
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->y:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 60
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->q:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 61
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->d:Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;

    sget-object v1, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->B:[S

    const/16 v2, 0x28

    aget-short v1, v1, v2

    neg-int v1, v1

    const/16 v2, 0x1b

    const/16 v3, 0x1f

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core_ui/token/TokenDialog;->showError(Ljava/lang/String;)V

    .line 62
    return-void

    .line 65
    :cond_28
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->b()V

    .line 67
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->z:Landroid/os/Handler;

    if-nez v0, :cond_3c

    .line 68
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->z:Landroid/os/Handler;

    .line 69
    invoke-static {p0}, Lbr/com/itau/sdk/android/core_ui/token/e;->a(Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->A:Ljava/lang/Runnable;

    .line 72
    :cond_3c
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->f:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 73
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->z:Landroid/os/Handler;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->A:Ljava/lang/Runnable;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x1e

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 75
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/SDKCustomUiController$EnvioSMSResponseDTO;->getMensagem()Ljava/lang/String;

    move-result-object v6

    .line 76
    const/16 v0, 0x2e

    invoke-virtual {v6, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/lit8 v7, v0, 0x1

    .line 80
    if-lez v7, :cond_77

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v7, v0, :cond_77

    .line 81
    const/4 v0, 0x0

    invoke-virtual {v6, v0, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 82
    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    goto :goto_79

    .line 84
    :cond_77
    move-object v8, v6

    .line 85
    const/4 v9, 0x0

    .line 89
    :goto_79
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->u:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 93
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->v:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 94
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->w:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 96
    iget-boolean v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->s:Z

    if-nez v0, :cond_9a

    .line 97
    return-void

    .line 99
    :cond_9a
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->u:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->v:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 101
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->w:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 102
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->i:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/SDKCustomUiController$EnvioSMSResponseDTO;->getMensagem()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->B:[S

    const/16 v3, 0x2e

    aget-short v2, v2, v3

    or-int/lit8 v3, v2, 0x3d

    sget-object v4, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->B:[S

    const/16 v5, 0x2e

    aget-short v4, v4, v5

    invoke-static {v2, v3, v4}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->a(III)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    sget v0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->C:I

    and-int/lit16 v0, v0, 0x16f

    sget-object v1, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->B:[S

    const/16 v2, 0x2e

    aget-short v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->B:[S

    const/16 v3, 0x42

    aget-short v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, Lbr/com/itau/sdk/android/core_ui/token/TokenTypeView;->a(Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->f:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/widget/RelativeLayout$LayoutParams;

    .line 106
    const/16 v0, 0xb

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 107
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidacaoSMSResponseDTO;)V
    .registers 2

    .line 48
    invoke-virtual {p0, p1}, Lbr/com/itau/sdk/android/core_ui/token/SMSTokenView;->a(Lbr/com/itau/sdk/android/core/type/SecurityAssertResponseDTO;)V

    .line 49
    return-void
.end method

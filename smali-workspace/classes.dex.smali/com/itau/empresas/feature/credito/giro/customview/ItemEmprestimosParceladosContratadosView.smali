.class public Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;
.super Landroid/widget/LinearLayout;
.source "ItemEmprestimosParceladosContratadosView.java"


# instance fields
.field emprestimosParceladosContratadosVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;

.field textoCreditoContratadosDataContratacao:Landroid/widget/TextView;

.field textoCreditoContratadosOperacao:Landroid/widget/TextView;

.field textoCreditoContratadosParcela:Landroid/widget/TextView;

.field textoCreditoContratadosTitulo:Landroid/widget/TextView;

.field textoCreditoContratadosValor:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 39
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 43
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method private setaViews(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;)V
    .registers 11
    .param p1, "contratadosVO"    # Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 48
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;->emprestimosParceladosContratadosVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;->emprestimosParceladosContratadosVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;->getCamposComprovate()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_8f

    .line 53
    const-string v4, ""

    .line 58
    .local v4, "dataContratacao":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;->emprestimosParceladosContratadosVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;->getCamposComprovate()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_8f

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;->emprestimosParceladosContratadosVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;

    .line 59
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;->getCamposComprovate()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_8f

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;->emprestimosParceladosContratadosVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;

    .line 63
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;->getCamposComprovate()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;

    .line 65
    .local v8, "camposComprovanteVO":Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;
    invoke-virtual {v8}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getIndicadorSeguro()Z

    move-result v0

    if-eqz v0, :cond_40

    .line 66
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f07057c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .local v3, "titulo":Ljava/lang/String;
    goto :goto_4b

    .line 68
    .end local v3    # "titulo":Ljava/lang/String;
    :cond_40
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f07057b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 71
    .local v3, "titulo":Ljava/lang/String;
    :goto_4b
    invoke-virtual {v8}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getDataContratacao()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_59

    .line 72
    .line 73
    invoke-virtual {v8}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getDataContratacao()Ljava/lang/String;

    move-result-object v0

    .line 72
    invoke-static {v0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 76
    :cond_59
    invoke-virtual {v8}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getNumeroControle()Ljava/lang/String;

    move-result-object v5

    .line 77
    .line 78
    .local v5, "operacao":Ljava/lang/String;
    invoke-virtual {v8}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getValorEmprestimo()J

    move-result-wide v0

    .line 77
    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(JZ)Ljava/lang/String;

    move-result-object v7

    .line 79
    .line 80
    .local v7, "valor":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v8}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getQuantidadeParcelas()I

    move-result v1

    .line 81
    invoke-virtual {v8}, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;->getValorParcela()F

    move-result v2

    .line 79
    invoke-static {v0, v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataParcelasEValor(Landroid/content/Context;IF)Ljava/lang/String;

    move-result-object v6

    .line 83
    .local v6, "parcelaValorFormatado":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;->textoCreditoContratadosTitulo:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;->textoCreditoContratadosDataContratacao:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;->textoCreditoContratadosOperacao:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;->textoCreditoContratadosParcela:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;->textoCreditoContratadosValor:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    .end local v3    # "titulo":Ljava/lang/String;
    .end local v4    # "dataContratacao":Ljava/lang/String;
    .end local v5    # "operacao":Ljava/lang/String;
    .end local v6    # "parcelaValorFormatado":Ljava/lang/String;
    .end local v7    # "valor":Ljava/lang/String;
    .end local v8    # "camposComprovanteVO":Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;
    :cond_8f
    return-void
.end method


# virtual methods
.method aoClicarEmMaisDetalhes()V
    .registers 4

    .line 100
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;->emprestimosParceladosContratadosVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;

    if-eqz v0, :cond_34

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;->emprestimosParceladosContratadosVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;

    .line 101
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;->getCamposComprovate()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_34

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;->emprestimosParceladosContratadosVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;

    .line 102
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;->getCamposComprovate()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_34

    .line 105
    .line 106
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;->emprestimosParceladosContratadosVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;

    .line 108
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;->getCamposComprovate()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;

    .line 107
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_$IntentBuilder_;->camposComprovanteVO(Lcom/itau/empresas/feature/credito/giro/model/CamposComprovanteVO;)Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_$IntentBuilder_;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosContratadosDetalhesActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 111
    :cond_34
    return-void
.end method

.method public bind(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;)Landroid/view/View;
    .registers 2
    .param p1, "contratadosVO"    # Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .line 93
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/giro/customview/ItemEmprestimosParceladosContratadosView;->setaViews(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosContratadosVO;)V

    .line 94
    return-object p0
.end method

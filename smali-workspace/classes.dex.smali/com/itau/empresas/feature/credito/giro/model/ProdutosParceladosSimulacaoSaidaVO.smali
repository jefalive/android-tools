.class public Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;
.super Ljava/lang/Object;
.source "ProdutosParceladosSimulacaoSaidaVO.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# instance fields
.field private agencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia"
    .end annotation
.end field

.field private cnpjCliente:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cnpj_cliente"
    .end annotation
.end field

.field private codigoPlanoFinanciamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_plano_financiamento"
    .end annotation
.end field

.field private codigoProdutoAmortizacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_produto_amortizacao"
    .end annotation
.end field

.field private conta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta"
    .end annotation
.end field

.field private cpfOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf_operador"
    .end annotation
.end field

.field private custoEfetivoTotal:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "custo_efetivo_total"
    .end annotation
.end field

.field private custoEfetivoTotalIof:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "custo_efetivo_total_iof"
    .end annotation
.end field

.field private custoEfetivoTotalPrincipal:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "custo_efetivo_total_principal"
    .end annotation
.end field

.field private custoEfetivoTotalSeguro:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "custo_efetivo_total_seguro"
    .end annotation
.end field

.field private custoEfetivoTotalTarifa:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "custo_efetivo_total_tarifa"
    .end annotation
.end field

.field dadosProduto:Lcom/itau/empresas/feature/credito/giro/model/ResultadoEfetivacaoEmprestimosParceladosVO$DadosProduto;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dados_produto"
    .end annotation
.end field

.field private dadosSeguro:Lcom/itau/empresas/feature/credito/giro/model/SeguroProdutosParceladoSaidaVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dados_seguro"
    .end annotation
.end field

.field private dataContratacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_contratacao"
    .end annotation
.end field

.field private dataPrimeiroPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_primeiro_pagamento"
    .end annotation
.end field

.field private dataUltimoPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_ultimo_pagamento"
    .end annotation
.end field

.field private descricaoPrazo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_prazo"
    .end annotation
.end field

.field private diaVencimento:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dia_vencimento"
    .end annotation
.end field

.field private digitoConta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_conta"
    .end annotation
.end field

.field private nomeCliente:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_cliente"
    .end annotation
.end field

.field private parcelasEmprestimoVO:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "planos"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;>;"
        }
    .end annotation
.end field

.field private prazoDias:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "prazo_dias"
    .end annotation
.end field

.field private prazoMaximoContratacao:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "prazo_maximo_contratacao"
    .end annotation
.end field

.field private prazoMinimoContratacao:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "prazo_minimo_contratacao"
    .end annotation
.end field

.field private quantidadeMaximaDiasPrimeiroPagamento:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade_maxima_dias_primeiro_pagamento"
    .end annotation
.end field

.field private quantidadeOpcao:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade_opcao"
    .end annotation
.end field

.field private quantidadeParcelas:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade_parcelas"
    .end annotation
.end field

.field private taxaBasica:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_basica"
    .end annotation
.end field

.field private taxaCetAno:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_cet_ano"
    .end annotation
.end field

.field private taxaCetMes:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_cet_mes"
    .end annotation
.end field

.field private taxaCustoAnual:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_custo_anual"
    .end annotation
.end field

.field private taxaCustoMensal:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_custo_mensal"
    .end annotation
.end field

.field private taxaJurosAno:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros_ano"
    .end annotation
.end field

.field private taxaJurosAnual:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros_anual"
    .end annotation
.end field

.field private taxaJurosEfetivaAnual:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros_efetiva_anual"
    .end annotation
.end field

.field private taxaJurosEfetivaMensal:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros_efetiva_mensal"
    .end annotation
.end field

.field private taxaJurosMensal:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros_mensal"
    .end annotation
.end field

.field private taxaJurosMes:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros_mes"
    .end annotation
.end field

.field private tipoCapitalizacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "periodicidade_capitalizacao"
    .end annotation
.end field

.field private valorEmprestimo:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_emprestimo"
    .end annotation
.end field

.field private valorIof:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_iof"
    .end annotation
.end field

.field private valorMaximoContratacao:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_maximo_contratacao"
    .end annotation
.end field

.field private valorMinimoContratacao:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_minimo_contratacao"
    .end annotation
.end field

.field private valorParcela:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_parcela"
    .end annotation
.end field

.field private valorPercentualCetIof:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "percentual_cet_iof"
    .end annotation
.end field

.field private valorTarifa:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_tarifa"
    .end annotation
.end field

.field private valorTarifaContratacao:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_tarifa_contratacao"
    .end annotation
.end field

.field private valorTotalFinanciamento:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_total_financiamento"
    .end annotation
.end field

.field private valoresEmprestimos:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valores_emprestimos"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 571
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getCustoEfetivoTotal()F
    .registers 2

    .line 390
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->custoEfetivoTotal:F

    return v0
.end method

.method public getCustoEfetivoTotalIof()F
    .registers 2

    .line 352
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->custoEfetivoTotalIof:F

    return v0
.end method

.method public getCustoEfetivoTotalPrincipal()F
    .registers 2

    .line 376
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->custoEfetivoTotalPrincipal:F

    return v0
.end method

.method public getCustoEfetivoTotalSeguro()F
    .registers 2

    .line 360
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->custoEfetivoTotalSeguro:F

    return v0
.end method

.method public getCustoEfetivoTotalTarifa()F
    .registers 2

    .line 368
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->custoEfetivoTotalTarifa:F

    return v0
.end method

.method public getDadosSeguro()Lcom/itau/empresas/feature/credito/giro/model/SeguroProdutosParceladoSaidaVO;
    .registers 2

    .line 395
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->dadosSeguro:Lcom/itau/empresas/feature/credito/giro/model/SeguroProdutosParceladoSaidaVO;

    return-object v0
.end method

.method public getDataPrimeiroPagamento()Ljava/lang/String;
    .registers 2

    .line 272
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->dataPrimeiroPagamento:Ljava/lang/String;

    return-object v0
.end method

.method public getParcelasEmprestimo()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;>;"
        }
    .end annotation

    .line 418
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->parcelasEmprestimoVO:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getQuantidadeParcelas()I
    .registers 2

    .line 168
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->quantidadeParcelas:I

    return v0
.end method

.method public getTaxaCetAno()F
    .registers 2

    .line 184
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->taxaCetAno:F

    return v0
.end method

.method public getTaxaCetMes()F
    .registers 2

    .line 176
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->taxaCetMes:F

    return v0
.end method

.method public getTaxaJurosAno()Ljava/lang/String;
    .registers 2

    .line 405
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->taxaJurosAno:Ljava/lang/String;

    return-object v0
.end method

.method public getTaxaJurosMes()Ljava/lang/String;
    .registers 2

    .line 400
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->taxaJurosMes:Ljava/lang/String;

    return-object v0
.end method

.method public getTipoCapitalizacao()Ljava/lang/String;
    .registers 2

    .line 160
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->tipoCapitalizacao:Ljava/lang/String;

    return-object v0
.end method

.method public getValorEmprestimo()Ljava/math/BigDecimal;
    .registers 2

    .line 192
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->valorEmprestimo:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getValorIof()F
    .registers 2

    .line 304
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->valorIof:F

    return v0
.end method

.method public getValorParcela()F
    .registers 2

    .line 296
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->valorParcela:F

    return v0
.end method

.method public getValorTarifaContratacao()F
    .registers 2

    .line 328
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->valorTarifaContratacao:F

    return v0
.end method

.method public getValorTotalFinanciamento()F
    .registers 2

    .line 200
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->valorTotalFinanciamento:F

    return v0
.end method

.method public setQuantidadeParcelas(I)V
    .registers 2
    .param p1, "quantidadeParcelas"    # I

    .line 172
    iput p1, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->quantidadeParcelas:I

    .line 173
    return-void
.end method

.method public setTaxaJurosAnual(Ljava/lang/String;)V
    .registers 2
    .param p1, "taxaJurosAnual"    # Ljava/lang/String;

    .line 228
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->taxaJurosAnual:Ljava/lang/String;

    .line 229
    return-void
.end method

.method public setTaxaJurosMensal(Ljava/lang/String;)V
    .registers 2
    .param p1, "taxaJurosMensal"    # Ljava/lang/String;

    .line 220
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->taxaJurosMensal:Ljava/lang/String;

    .line 221
    return-void
.end method

.method public setTaxaJurosMes(Ljava/lang/String;)V
    .registers 2
    .param p1, "taxaJurosMes"    # Ljava/lang/String;

    .line 413
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->taxaJurosMes:Ljava/lang/String;

    .line 414
    return-void
.end method

.method public setValorParcela(F)V
    .registers 2
    .param p1, "valorParcela"    # F

    .line 300
    iput p1, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->valorParcela:F

    .line 301
    return-void
.end method

.method public setValorTarifa(F)V
    .registers 2
    .param p1, "valorTarifa"    # F

    .line 524
    iput p1, p0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;->valorTarifa:F

    .line 525
    return-void
.end method

.class public final Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;
.super Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;
.source "RecargaNovoNumeroFragment_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_$FragmentBuilder_;
    }
.end annotation


# instance fields
.field private contentView_:Landroid/view/View;

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 34
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;-><init>()V

    .line 38
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method public static builder()Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_$FragmentBuilder_;
    .registers 1

    .line 97
    new-instance v0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_$FragmentBuilder_;

    invoke-direct {v0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_$FragmentBuilder_;-><init>()V

    return-object v0
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 84
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 85
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->application:Lcom/itau/empresas/CustomApplication;

    .line 86
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->application:Lcom/itau/empresas/CustomApplication;

    .line 87
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->recargaController:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    .line 88
    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .registers 3
    .param p1, "id"    # I

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_6

    .line 52
    const/4 v0, 0x0

    return-object v0

    .line 54
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->contentView_:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 43
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 44
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->init_(Landroid/os/Bundle;)V

    .line 45
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->onCreate(Landroid/os/Bundle;)V

    .line 46
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 47
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 59
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->contentView_:Landroid/view/View;

    .line 60
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_14

    .line 61
    const v0, 0x7f0300bb

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->contentView_:Landroid/view/View;

    .line 63
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->contentView_:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .registers 2

    .line 68
    invoke-super {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->onDestroyView()V

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->contentView_:Landroid/view/View;

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->llRadioValor:Landroid/widget/LinearLayout;

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->llRegiao:Landroid/widget/LinearLayout;

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->campoNome:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->valorRecarga:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->botaoIncluirRecarga:Landroid/widget/Button;

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->botaoAutorizarRecarga:Landroid/widget/Button;

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->botaoAgenda:Landroid/widget/ImageButton;

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->botaoFavorito:Landroid/widget/ImageButton;

    .line 81
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 5
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 102
    const v0, 0x7f0e0699

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->llRadioValor:Landroid/widget/LinearLayout;

    .line 103
    const v0, 0x7f0e0609

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->llRegiao:Landroid/widget/LinearLayout;

    .line 104
    const v0, 0x7f0e02a5

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/material/MaterialEditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->campoCelular:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 105
    const v0, 0x7f0e0468

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/material/MaterialEditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->campoNome:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 106
    const v0, 0x7f0e0469

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->valorRecarga:Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;

    .line 107
    const v0, 0x7f0e02a9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/material/MaterialSpinner;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 108
    const v0, 0x7f0e02aa

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/material/MaterialSpinner;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 109
    const v0, 0x7f0e046a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->botaoIncluirRecarga:Landroid/widget/Button;

    .line 110
    const v0, 0x7f0e046b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->botaoAutorizarRecarga:Landroid/widget/Button;

    .line 111
    const v0, 0x7f0e02a7

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->botaoAgenda:Landroid/widget/ImageButton;

    .line 112
    const v0, 0x7f0e02a6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->botaoFavorito:Landroid/widget/ImageButton;

    .line 113
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->botaoIncluirRecarga:Landroid/widget/Button;

    if-eqz v0, :cond_87

    .line 114
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->botaoIncluirRecarga:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_$1;-><init>(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    :cond_87
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->botaoAutorizarRecarga:Landroid/widget/Button;

    if-eqz v0, :cond_95

    .line 124
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->botaoAutorizarRecarga:Landroid/widget/Button;

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_$2;-><init>(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    :cond_95
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->botaoAgenda:Landroid/widget/ImageButton;

    if-eqz v0, :cond_a3

    .line 134
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->botaoAgenda:Landroid/widget/ImageButton;

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_$3;-><init>(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    :cond_a3
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    if-eqz v0, :cond_b1

    .line 144
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->spinnerOperadora:Lbr/com/itau/widgets/material/MaterialSpinner;

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_$4;-><init>(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 158
    :cond_b1
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    if-eqz v0, :cond_bf

    .line 159
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->spinnerRegiao:Lbr/com/itau/widgets/material/MaterialSpinner;

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_$5;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_$5;-><init>(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 174
    :cond_bf
    const v0, 0x7f0e02a5

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/TextView;

    .line 175
    .local v2, "view":Landroid/widget/TextView;
    if-eqz v2, :cond_d3

    .line 176
    new-instance v0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_$6;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_$6;-><init>(Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;)V

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 194
    .end local v2    # "view":Landroid/widget/TextView;
    :cond_d3
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->inicializar()V

    .line 195
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 92
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 93
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/fragment/RecargaNovoNumeroFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 94
    return-void
.end method

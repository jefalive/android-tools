.class public final Landroid/support/v4/content/PermissionChecker;
.super Ljava/lang/Object;
.source "PermissionChecker.java"


# direct methods
.method public static checkPermission(Landroid/content/Context;Ljava/lang/String;IILjava/lang/String;)I
    .registers 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "permission"    # Ljava/lang/String;
    .param p2, "pid"    # I
    .param p3, "uid"    # I
    .param p4, "packageName"    # Ljava/lang/String;

    .line 89
    invoke-virtual {p0, p1, p2, p3}, Landroid/content/Context;->checkPermission(Ljava/lang/String;II)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_9

    .line 90
    const/4 v0, -0x1

    return v0

    .line 93
    :cond_9
    invoke-static {p1}, Landroid/support/v4/app/AppOpsManagerCompat;->permissionToOp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 94
    .local v2, "op":Ljava/lang/String;
    if-nez v2, :cond_11

    .line 95
    const/4 v0, 0x0

    return v0

    .line 98
    :cond_11
    if-nez p4, :cond_25

    .line 99
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v3

    .line 100
    .local v3, "packageNames":[Ljava/lang/String;
    if-eqz v3, :cond_20

    array-length v0, v3

    if-gtz v0, :cond_22

    .line 101
    :cond_20
    const/4 v0, -0x1

    return v0

    .line 103
    :cond_22
    const/4 v0, 0x0

    aget-object p4, v3, v0

    .line 106
    .end local v3    # "packageNames":[Ljava/lang/String;
    :cond_25
    invoke-static {p0, v2, p4}, Landroid/support/v4/app/AppOpsManagerCompat;->noteProxyOp(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_2d

    .line 108
    const/4 v0, -0x2

    return v0

    .line 111
    :cond_2d
    const/4 v0, 0x0

    return v0
.end method

.method public static checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I
    .registers 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "permission"    # Ljava/lang/String;

    .line 125
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    .line 126
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 125
    invoke-static {p0, p1, v0, v1, v2}, Landroid/support/v4/content/PermissionChecker;->checkPermission(Landroid/content/Context;Ljava/lang/String;IILjava/lang/String;)I

    move-result v0

    return v0
.end method

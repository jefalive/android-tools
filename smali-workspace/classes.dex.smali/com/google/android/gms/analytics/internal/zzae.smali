.class public Lcom/google/android/gms/analytics/internal/zzae;
.super Ljava/lang/Object;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static volatile zzSV:Lcom/google/android/gms/analytics/Logger;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    new-instance v0, Lcom/google/android/gms/analytics/internal/zzs;

    invoke-direct {v0}, Lcom/google/android/gms/analytics/internal/zzs;-><init>()V

    invoke-static {v0}, Lcom/google/android/gms/analytics/internal/zzae;->setLogger(Lcom/google/android/gms/analytics/Logger;)V

    return-void
.end method

.method public static getLogger()Lcom/google/android/gms/analytics/Logger;
    .registers 1

    sget-object v0, Lcom/google/android/gms/analytics/internal/zzae;->zzSV:Lcom/google/android/gms/analytics/Logger;

    return-object v0
.end method

.method public static setLogger(Lcom/google/android/gms/analytics/Logger;)V
    .registers 1
    .param p0, "logger"    # Lcom/google/android/gms/analytics/Logger;

    sput-object p0, Lcom/google/android/gms/analytics/internal/zzae;->zzSV:Lcom/google/android/gms/analytics/Logger;

    return-void
.end method

.method public static v(Ljava/lang/String;)V
    .registers 4
    .param p0, "msg"    # Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/analytics/internal/zzaf;->zzlx()Lcom/google/android/gms/analytics/internal/zzaf;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {v1, p0}, Lcom/google/android/gms/analytics/internal/zzaf;->zzbd(Ljava/lang/String;)V

    goto :goto_1c

    :cond_a
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/gms/analytics/internal/zzae;->zzQ(I)Z

    move-result v0

    if-eqz v0, :cond_1c

    sget-object v0, Lcom/google/android/gms/analytics/internal/zzy;->zzRL:Lcom/google/android/gms/analytics/internal/zzy$zza;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzy$zza;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, p0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1c
    :goto_1c
    sget-object v2, Lcom/google/android/gms/analytics/internal/zzae;->zzSV:Lcom/google/android/gms/analytics/Logger;

    if-eqz v2, :cond_23

    invoke-interface {v2, p0}, Lcom/google/android/gms/analytics/Logger;->verbose(Ljava/lang/String;)V

    :cond_23
    return-void
.end method

.method public static zzQ(I)Z
    .registers 3

    invoke-static {}, Lcom/google/android/gms/analytics/internal/zzae;->getLogger()Lcom/google/android/gms/analytics/Logger;

    move-result-object v1

    if-eqz v1, :cond_14

    invoke-static {}, Lcom/google/android/gms/analytics/internal/zzae;->getLogger()Lcom/google/android/gms/analytics/Logger;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/analytics/Logger;->getLogLevel()I

    move-result v0

    if-gt v0, p0, :cond_12

    const/4 v0, 0x1

    goto :goto_13

    :cond_12
    const/4 v0, 0x0

    :goto_13
    return v0

    :cond_14
    const/4 v0, 0x0

    return v0
.end method

.method public static zzaJ(Ljava/lang/String;)V
    .registers 4

    invoke-static {}, Lcom/google/android/gms/analytics/internal/zzaf;->zzlx()Lcom/google/android/gms/analytics/internal/zzaf;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {v1, p0}, Lcom/google/android/gms/analytics/internal/zzaf;->zzbf(Ljava/lang/String;)V

    goto :goto_1c

    :cond_a
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/analytics/internal/zzae;->zzQ(I)Z

    move-result v0

    if-eqz v0, :cond_1c

    sget-object v0, Lcom/google/android/gms/analytics/internal/zzy;->zzRL:Lcom/google/android/gms/analytics/internal/zzy$zza;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzy$zza;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, p0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1c
    :goto_1c
    sget-object v2, Lcom/google/android/gms/analytics/internal/zzae;->zzSV:Lcom/google/android/gms/analytics/Logger;

    if-eqz v2, :cond_23

    invoke-interface {v2, p0}, Lcom/google/android/gms/analytics/Logger;->info(Ljava/lang/String;)V

    :cond_23
    return-void
.end method

.method public static zzaK(Ljava/lang/String;)V
    .registers 4

    invoke-static {}, Lcom/google/android/gms/analytics/internal/zzaf;->zzlx()Lcom/google/android/gms/analytics/internal/zzaf;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {v1, p0}, Lcom/google/android/gms/analytics/internal/zzaf;->zzbg(Ljava/lang/String;)V

    goto :goto_1c

    :cond_a
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/analytics/internal/zzae;->zzQ(I)Z

    move-result v0

    if-eqz v0, :cond_1c

    sget-object v0, Lcom/google/android/gms/analytics/internal/zzy;->zzRL:Lcom/google/android/gms/analytics/internal/zzy$zza;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzy$zza;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1c
    :goto_1c
    sget-object v2, Lcom/google/android/gms/analytics/internal/zzae;->zzSV:Lcom/google/android/gms/analytics/Logger;

    if-eqz v2, :cond_23

    invoke-interface {v2, p0}, Lcom/google/android/gms/analytics/Logger;->warn(Ljava/lang/String;)V

    :cond_23
    return-void
.end method

.method public static zzf(Ljava/lang/String;Ljava/lang/Object;)V
    .registers 6

    invoke-static {}, Lcom/google/android/gms/analytics/internal/zzaf;->zzlx()Lcom/google/android/gms/analytics/internal/zzaf;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-virtual {v2, p0, p1}, Lcom/google/android/gms/analytics/internal/zzaf;->zze(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_37

    :cond_a
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/analytics/internal/zzae;->zzQ(I)Z

    move-result v0

    if-eqz v0, :cond_37

    if-eqz p1, :cond_2b

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_2c

    :cond_2b
    move-object v3, p0

    :goto_2c
    sget-object v0, Lcom/google/android/gms/analytics/internal/zzy;->zzRL:Lcom/google/android/gms/analytics/internal/zzy$zza;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzy$zza;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_37
    :goto_37
    sget-object v3, Lcom/google/android/gms/analytics/internal/zzae;->zzSV:Lcom/google/android/gms/analytics/Logger;

    if-eqz v3, :cond_3e

    invoke-interface {v3, p0}, Lcom/google/android/gms/analytics/Logger;->error(Ljava/lang/String;)V

    :cond_3e
    return-void
.end method

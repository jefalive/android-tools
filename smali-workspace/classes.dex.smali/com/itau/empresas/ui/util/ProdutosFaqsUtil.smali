.class public Lcom/itau/empresas/ui/util/ProdutosFaqsUtil;
.super Ljava/lang/Object;
.source "ProdutosFaqsUtil.java"


# direct methods
.method public static carregaProdutos(Landroid/content/Context;)Ljava/util/List;
    .registers 17
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;)Ljava/util/List<Lcom/itau/empresas/feature/atendimento/model/AtendimentoDuvida;>;"
        }
    .end annotation

    .line 29
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v3

    .line 30
    .local v3, "raw":Ljava/io/InputStream;
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v4, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 31
    .local v4, "reader":Ljava/io/Reader;
    new-instance v5, Lcom/google/gson/Gson;

    invoke-direct {v5}, Lcom/google/gson/Gson;-><init>()V

    .line 32
    .local v5, "gson":Lcom/google/gson/Gson;
    new-instance v0, Lcom/itau/empresas/ui/util/ProdutosFaqsUtil$1;

    invoke-direct {v0}, Lcom/itau/empresas/ui/util/ProdutosFaqsUtil$1;-><init>()V

    .line 33
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/ProdutosFaqsUtil$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 32
    invoke-virtual {v5, v4, v0}, Lcom/google/gson/Gson;->fromJson(Ljava/io/Reader;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/gson/JsonObject;

    .line 35
    .local v6, "object":Lcom/google/gson/JsonObject;
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    .line 36
    .local v7, "produtosFaqLista":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/atendimento/model/AtendimentoDuvida;>;"
    const-string v0, "produtos"

    invoke-virtual {v6, v0}, Lcom/google/gson/JsonObject;->getAsJsonArray(Ljava/lang/String;)Lcom/google/gson/JsonArray;

    move-result-object v2

    .line 39
    .local v2, "produtosLista":Lcom/google/gson/JsonArray;
    invoke-virtual {v2}, Lcom/google/gson/JsonArray;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_39
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8c

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/google/gson/JsonElement;

    .line 40
    .local v9, "produto":Lcom/google/gson/JsonElement;
    invoke-virtual {v9}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;

    move-result-object v10

    .line 41
    .local v10, "object1":Lcom/google/gson/JsonObject;
    new-instance v11, Lcom/itau/empresas/feature/atendimento/model/AtendimentoDuvida;

    invoke-direct {v11}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoDuvida;-><init>()V

    .line 42
    .local v11, "produtoItens":Lcom/itau/empresas/feature/atendimento/model/AtendimentoDuvida;
    const-string v0, "produto"

    invoke-virtual {v10, v0}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/JsonElement;->getAsString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoDuvida;->setProduto(Ljava/lang/String;)V

    .line 43
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 44
    .local v12, "subProductsFaqList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v0, "assunto"

    invoke-virtual {v10, v0}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/JsonElement;->getAsJsonArray()Lcom/google/gson/JsonArray;

    move-result-object v13

    .line 45
    .local v13, "subProductsFaq":Lcom/google/gson/JsonArray;
    invoke-virtual {v13}, Lcom/google/gson/JsonArray;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_6f
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_84

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v15, v0

    check-cast v15, Lcom/google/gson/JsonElement;

    .line 46
    .local v15, "subProduct":Lcom/google/gson/JsonElement;
    invoke-virtual {v15}, Lcom/google/gson/JsonElement;->getAsString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 47
    .end local v15    # "subProduct":Lcom/google/gson/JsonElement;
    goto :goto_6f

    .line 48
    :cond_84
    invoke-virtual {v11, v12}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoDuvida;->setAssunto(Ljava/util/List;)V

    .line 49
    invoke-interface {v7, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    .end local v9    # "produto":Lcom/google/gson/JsonElement;
    .end local v10    # "object1":Lcom/google/gson/JsonObject;
    .end local v11    # "produtoItens":Lcom/itau/empresas/feature/atendimento/model/AtendimentoDuvida;
    .end local v12    # "subProductsFaqList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v12
    .end local v13    # "subProductsFaq":Lcom/google/gson/JsonArray;
    goto/16 :goto_39

    .line 51
    :cond_8c
    return-object v7
.end method

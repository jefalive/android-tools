.class public Lcom/itau/empresas/api/model/ResultadoCadastroSenhaEletronicaVO;
.super Ljava/lang/Object;
.source "ResultadoCadastroSenhaEletronicaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private cnpj:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cnpj"
    .end annotation
.end field

.field private codigoOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_operador"
    .end annotation
.end field

.field private dataSolicitacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_solicitacao"
    .end annotation
.end field

.field private nomeRepresentante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_representante"
    .end annotation
.end field

.field private numeroControle:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_controle"
    .end annotation
.end field

.field private razaoSocial:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "razao_social"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public Lbr/com/itau/textovoz/util/CommonUtils;
.super Ljava/lang/Object;
.source "CommonUtils.java"


# static fields
.field private static final BRAZIL:Ljava/util/Locale;

.field private static final REAL:Ljava/text/DecimalFormatSymbols;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 25
    new-instance v0, Ljava/util/Locale;

    const-string v1, "pt"

    const-string v2, "BR"

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lbr/com/itau/textovoz/util/CommonUtils;->BRAZIL:Ljava/util/Locale;

    .line 26
    new-instance v0, Ljava/text/DecimalFormatSymbols;

    sget-object v1, Lbr/com/itau/textovoz/util/CommonUtils;->BRAZIL:Ljava/util/Locale;

    invoke-direct {v0, v1}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    sput-object v0, Lbr/com/itau/textovoz/util/CommonUtils;->REAL:Ljava/text/DecimalFormatSymbols;

    return-void
.end method

.method public static getMoneyFormatter()Ljava/text/DecimalFormat;
    .registers 3

    .line 57
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "###,###,##0.00"

    sget-object v2, Lbr/com/itau/textovoz/util/CommonUtils;->REAL:Ljava/text/DecimalFormatSymbols;

    invoke-direct {v0, v1, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;Ljava/text/DecimalFormatSymbols;)V

    return-object v0
.end method

.method public static getSegment(Ljava/lang/String;)I
    .registers 2
    .param p0, "accountSegment"    # Ljava/lang/String;

    .line 145
    if-eqz p0, :cond_20

    .line 146
    const-string v0, "7"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 147
    const/4 v0, 0x2

    return v0

    .line 148
    :cond_c
    const-string v0, "L"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 149
    const/4 v0, 0x3

    return v0

    .line 150
    :cond_16
    const-string v0, "4"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 151
    const/4 v0, 0x4

    return v0

    .line 154
    :cond_20
    const/4 v0, 0x1

    return v0
.end method

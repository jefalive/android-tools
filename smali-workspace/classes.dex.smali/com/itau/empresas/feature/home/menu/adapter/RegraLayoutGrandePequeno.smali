.class public final Lcom/itau/empresas/feature/home/menu/adapter/RegraLayoutGrandePequeno;
.super Ljava/lang/Object;
.source "RegraLayoutGrandePequeno.java"

# interfaces
.implements Lcom/itau/empresas/feature/home/menu/adapter/RegraLayoutAdapter;


# instance fields
.field private final listSize:I


# direct methods
.method public constructor <init>(I)V
    .registers 2
    .param p1, "listSize"    # I

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput p1, p0, Lcom/itau/empresas/feature/home/menu/adapter/RegraLayoutGrandePequeno;->listSize:I

    .line 15
    return-void
.end method


# virtual methods
.method public calculaViewType(I)I
    .registers 5
    .param p1, "position"    # I

    .line 20
    rem-int/lit8 v0, p1, 0xa

    if-nez v0, :cond_b

    .line 21
    div-int/lit8 v0, p1, 0xa

    add-int/lit8 v0, v0, 0x8

    sub-int v2, p1, v0

    .local v2, "usedPosition":I
    goto :goto_c

    .line 23
    .end local v2    # "usedPosition":I
    :cond_b
    move v2, p1

    .line 26
    .local v2, "usedPosition":I
    :goto_c
    add-int/lit8 v0, p1, 0x1

    iget v1, p0, Lcom/itau/empresas/feature/home/menu/adapter/RegraLayoutGrandePequeno;->listSize:I

    if-ne v0, v1, :cond_14

    .line 27
    const/4 v0, 0x0

    return v0

    .line 28
    :cond_14
    const/4 v0, 0x2

    if-eq v2, v0, :cond_1a

    const/4 v0, 0x6

    if-ne v2, v0, :cond_1c

    .line 29
    :cond_1a
    const/4 v0, 0x1

    return v0

    .line 31
    :cond_1c
    const/4 v0, 0x0

    return v0
.end method

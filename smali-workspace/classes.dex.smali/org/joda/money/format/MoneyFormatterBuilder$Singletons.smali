.class abstract enum Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;
.super Ljava/lang/Enum;
.source "MoneyFormatterBuilder.java"

# interfaces
.implements Lorg/joda/money/format/MoneyPrinter;
.implements Lorg/joda/money/format/MoneyParser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/joda/money/format/MoneyFormatterBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x440a
    name = "Singletons"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;>;Lorg/joda/money/format/MoneyPrinter;Lorg/joda/money/format/MoneyParser;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;

.field public static final enum CODE:Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;

.field public static final enum NUMERIC_3_CODE:Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;

.field public static final enum NUMERIC_CODE:Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;


# instance fields
.field private final toString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 335
    new-instance v0, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons$1;

    const-string v1, "CODE"

    const-string v2, "${code}"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons$1;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;->CODE:Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;

    .line 356
    new-instance v0, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons$2;

    const-string v1, "NUMERIC_3_CODE"

    const-string v2, "${numeric3Code}"

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3, v2}, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons$2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;->NUMERIC_3_CODE:Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;

    .line 377
    new-instance v0, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons$3;

    const-string v1, "NUMERIC_CODE"

    const-string v2, "${numericCode}"

    const/4 v3, 0x2

    invoke-direct {v0, v1, v3, v2}, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons$3;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;->NUMERIC_CODE:Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;

    .line 334
    const/4 v0, 0x3

    new-array v0, v0, [Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;

    sget-object v1, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;->CODE:Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;->NUMERIC_3_CODE:Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;->NUMERIC_CODE:Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;->$VALUES:[Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .registers 4
    .param p3, "toString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/String;)V"
        }
    .end annotation

    .line 402
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 403
    iput-object p3, p0, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;->toString:Ljava/lang/String;

    .line 404
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;Lorg/joda/money/format/MoneyFormatterBuilder$1;)V
    .registers 5
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # I
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Lorg/joda/money/format/MoneyFormatterBuilder$1;

    .line 334
    invoke-direct {p0, p1, p2, p3}, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 334
    const-class v0, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;

    return-object v0
.end method

.method public static values()[Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;
    .registers 1

    .line 334
    sget-object v0, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;->$VALUES:[Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;

    invoke-virtual {v0}, [Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 2

    .line 407
    iget-object v0, p0, Lorg/joda/money/format/MoneyFormatterBuilder$Singletons;->toString:Ljava/lang/String;

    return-object v0
.end method

.class public Lbr/com/itau/sdk/android/core/login/model/RouterResponseKeyAccessDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final $:[B

.field private static $$:I


# instance fields
.field private message:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "message"
    .end annotation
.end field

.field private status:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "status"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x3b

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/login/model/RouterResponseKeyAccessDTO;->$:[B

    const/16 v0, 0xc3

    sput v0, Lbr/com/itau/sdk/android/core/login/model/RouterResponseKeyAccessDTO;->$$:I

    return-void

    :array_e
    .array-data 1
        0x59t
        0x4dt
        0x6et
        0x49t
        0x3t
        -0x5t
        -0xbt
        -0x7t
        -0x1t
        -0xft
        0xft
        -0x7t
        -0xft
        -0x14t
        0xct
        -0xat
        -0x3t
        -0x2t
        -0x1t
        -0x14t
        0x17t
        -0x9t
        -0xet
        -0x1t
        -0x3t
        -0x8t
        0x6t
        -0xft
        0x7t
        -0x11t
        0x3t
        -0x5t
        -0x12t
        0xet
        -0x5t
        -0xbt
        -0x7t
        -0x1t
        -0xft
        0xft
        -0x7t
        -0xft
        -0x2t
        -0x7t
        -0x1t
        -0x16t
        -0x5t
        -0x2t
        0xdt
        -0x15t
        0x3t
        -0x5t
        -0xbt
        -0x7t
        -0x1t
        -0xft
        0xft
        -0x7t
        -0xft
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class public Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;
.super Ljava/lang/Object;
.source "CustomAlertDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private cancelable:Ljava/lang/Boolean;

.field private context:Landroid/content/Context;

.field private estado:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

.field private gravity:I

.field private gravityCustom:Z

.field private mensagem:Ljava/lang/String;

.field private textoNegativo:Ljava/lang/String;

.field private textoNeutro:Ljava/lang/String;

.field private textoPositivo:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->cancelable:Ljava/lang/Boolean;

    .line 180
    const/16 v0, 0x11

    iput v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->gravity:I

    .line 185
    iput-object p1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->context:Landroid/content/Context;

    .line 186
    return-void
.end method


# virtual methods
.method public alinhamentoTexto(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;
    .registers 3
    .param p1, "gravity"    # I

    .line 240
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->gravityCustom:Z

    .line 241
    iput p1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->gravity:I

    .line 242
    return-object p0
.end method

.method public build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    .registers 4

    .line 246
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    if-nez v0, :cond_c

    .line 247
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Estado n\u00e3o pode ser nulo"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 248
    :cond_c
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->DOIS_BOTOES:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    if-ne v0, v1, :cond_2e

    .line 249
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNeutro:Ljava/lang/String;

    if-eqz v0, :cond_1e

    .line 250
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Texto neutro apenas pode ser setado para o estado de um bot\u00e3o"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 253
    :cond_1e
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoPositivo:Ljava/lang/String;

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNegativo:Ljava/lang/String;

    if-nez v0, :cond_50

    .line 254
    :cond_26
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Texto positivo e negativo devem ser diferente de nulo"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 257
    :cond_2e
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->UM_BOTAO:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    if-ne v0, v1, :cond_50

    .line 258
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNeutro:Ljava/lang/String;

    if-nez v0, :cond_40

    .line 259
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Texto neutro deve ser diferente de nulo"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 261
    :cond_40
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoPositivo:Ljava/lang/String;

    if-eqz v0, :cond_50

    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNegativo:Ljava/lang/String;

    if-eqz v0, :cond_50

    .line 262
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Texto positivo ou negativo apenas podem ser setados para o estado de dois bot\u00f5es"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 267
    :cond_50
    invoke-static {}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->builder()Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem:Ljava/lang/String;

    .line 268
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;->mensagem(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 269
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;->estadoArg(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNegativo:Ljava/lang/String;

    .line 270
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;->textoBotaoNegativo(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoPositivo:Ljava/lang/String;

    .line 271
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;->textoBotaoPositivo(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNeutro:Ljava/lang/String;

    .line 272
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;->textoBotaoNeutro(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;

    move-result-object v0

    .line 273
    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v2

    .line 274
    .local v2, "alertDialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->cancelable:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setCancelable(Z)V

    .line 275
    iget v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->gravity:I

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setGravityTexto(I)V

    .line 276
    iget-boolean v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->gravityCustom:Z

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setGravityCustom(Z)V

    .line 277
    return-object v2
.end method

.method public cancelable(Z)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;
    .registers 3
    .param p1, "c"    # Z

    .line 194
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->cancelable:Ljava/lang/Boolean;

    .line 195
    return-object p0
.end method

.method public estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;
    .registers 2
    .param p1, "estado"    # Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 189
    iput-object p1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 190
    return-object p0
.end method

.method public mensagem(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;
    .registers 3
    .param p1, "mensagemRes"    # I

    .line 199
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mensagem(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;
    .registers 2
    .param p1, "mensagem"    # Ljava/lang/String;

    .line 220
    iput-object p1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem:Ljava/lang/String;

    .line 221
    return-object p0
.end method

.method public textoNegativo(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;
    .registers 3
    .param p1, "negativoRes"    # I

    .line 207
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNegativo(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    return-object v0
.end method

.method textoNegativo(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;
    .registers 2
    .param p1, "negativo"    # Ljava/lang/String;

    .line 230
    iput-object p1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNegativo:Ljava/lang/String;

    .line 231
    return-object p0
.end method

.method public textoNeutro(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;
    .registers 3
    .param p1, "neutroRes"    # I

    .line 216
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNeutro(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    return-object v0
.end method

.method textoNeutro(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;
    .registers 2
    .param p1, "neutro"    # Ljava/lang/String;

    .line 235
    iput-object p1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNeutro:Ljava/lang/String;

    .line 236
    return-object p0
.end method

.method public textoPositivo(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;
    .registers 3
    .param p1, "positivoRes"    # I

    .line 203
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoPositivo(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    return-object v0
.end method

.method textoPositivo(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;
    .registers 2
    .param p1, "positivo"    # Ljava/lang/String;

    .line 225
    iput-object p1, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoPositivo:Ljava/lang/String;

    .line 226
    return-object p0
.end method

.class public final Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_;
.super Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment;
.source "ITokenEfetivacaoDesbloqueioFragment_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_$FragmentBuilder_;
    }
.end annotation


# instance fields
.field private contentView_:Landroid/view/View;

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 22
    invoke-direct {p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment;-><init>()V

    .line 26
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method public static builder()Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_$FragmentBuilder_;
    .registers 1

    .line 72
    new-instance v0, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_$FragmentBuilder_;

    invoke-direct {v0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_$FragmentBuilder_;-><init>()V

    return-object v0
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 61
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 62
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_;->application:Lcom/itau/empresas/CustomApplication;

    .line 63
    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .registers 3
    .param p1, "id"    # I

    .line 39
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_6

    .line 40
    const/4 v0, 0x0

    return-object v0

    .line 42
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_;->contentView_:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 31
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 32
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_;->init_(Landroid/os/Bundle;)V

    .line 33
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment;->onCreate(Landroid/os/Bundle;)V

    .line 34
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 35
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 47
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_;->contentView_:Landroid/view/View;

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_14

    .line 49
    const v0, 0x7f0300ab

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_;->contentView_:Landroid/view/View;

    .line 51
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_;->contentView_:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .registers 2

    .line 56
    invoke-super {p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment;->onDestroyView()V

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_;->contentView_:Landroid/view/View;

    .line 58
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 77
    const v0, 0x7f0e0422

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 79
    .local v1, "view_botao_orientacoes_seguranca":Landroid/view/View;
    if-eqz v1, :cond_11

    .line 80
    new-instance v0, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_$1;-><init>(Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    :cond_11
    invoke-virtual {p0}, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_;->aposCarregar()V

    .line 90
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 67
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/fragment/ITokenEfetivacaoDesbloqueioFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 69
    return-void
.end method

.class public final Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;
.super Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;
.source "SaldoDetalhadoActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 36
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;-><init>()V

    .line 40
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;

    .line 36
    invoke-super {p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;

    .line 36
    invoke-super {p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 52
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 53
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 54
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 55
    invoke-static {p0}, Lcom/itau/empresas/controller/ExtratoController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/controller/ExtratoController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->extratoController:Lcom/itau/empresas/controller/ExtratoController;

    .line 56
    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->afterInject()V

    .line 57
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 150
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_$4;-><init>(Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 158
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 138
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_$3;-><init>(Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 146
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 45
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->init_(Landroid/os/Bundle;)V

    .line 46
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->onCreate(Landroid/os/Bundle;)V

    .line 47
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 48
    const v0, 0x7f030062

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->setContentView(I)V

    .line 49
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 5
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 91
    const v0, 0x7f0e02e4

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->flBotaoFecharFiltro:Landroid/widget/FrameLayout;

    .line 92
    const v0, 0x7f0e02e8

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->textoValorSaldo:Landroid/widget/TextView;

    .line 93
    const v0, 0x7f0e02ed

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->textoValorLis:Landroid/widget/TextView;

    .line 94
    const v0, 0x7f0e02ef

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->textoValorAdicional:Landroid/widget/TextView;

    .line 95
    const v0, 0x7f0e02f4

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->textoValorUtilizado:Landroid/widget/TextView;

    .line 96
    const v0, 0x7f0e02f7

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->textoValorComLimite:Landroid/widget/TextView;

    .line 97
    const v0, 0x7f0e02f3

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->textoLisTotal:Landroid/widget/TextView;

    .line 98
    const v0, 0x7f0e02eb

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->containerLis:Landroid/widget/LinearLayout;

    .line 99
    const v0, 0x7f0e02f0

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->containerLisUtilizado:Landroid/widget/RelativeLayout;

    .line 100
    const v0, 0x7f0e02ee

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->containerLisAdicional:Landroid/widget/LinearLayout;

    .line 101
    const v0, 0x7f0e02f2

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->slideLisUtilizado:Landroid/widget/SeekBar;

    .line 102
    const v0, 0x7f0e02e9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->separadorSaldo:Landroid/view/View;

    .line 103
    const v0, 0x7f0e02ec

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->textoTituloLimite:Landroid/widget/TextView;

    .line 104
    const v0, 0x7f0e02f6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->textoTituloSaldoLimite:Landroid/widget/TextView;

    .line 105
    const v0, 0x7f0e02f5

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->containerTotalLis:Landroid/widget/LinearLayout;

    .line 106
    const v0, 0x7f0e02e6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->conteinerSaldo:Landroid/widget/LinearLayout;

    .line 107
    const v0, 0x7f0e02ea

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->conteinerLimite:Landroid/widget/LinearLayout;

    .line 108
    const v0, 0x7f0e02f8

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 110
    .local v2, "view_texto_consultar_limite":Landroid/view/View;
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->flBotaoFecharFiltro:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_ce

    .line 111
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->flBotaoFecharFiltro:Landroid/widget/FrameLayout;

    new-instance v1, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_$1;-><init>(Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    :cond_ce
    if-eqz v2, :cond_d8

    .line 121
    new-instance v0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_$2;-><init>(Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    :cond_d8
    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->afterViews()V

    .line 131
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 61
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->setContentView(I)V

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 63
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 73
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->setContentView(Landroid/view/View;)V

    .line 74
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 75
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 67
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/SaldoDetalhadoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 69
    return-void
.end method

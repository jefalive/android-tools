.class public final Lcom/itau/empresas/ui/view/LayoutExpandivelView_;
.super Lcom/itau/empresas/ui/view/LayoutExpandivelView;
.source "LayoutExpandivelView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 37
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/LayoutExpandivelView;-><init>(Landroid/content/Context;)V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView_;->alreadyInflated_:Z

    .line 34
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 38
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/LayoutExpandivelView_;->init_()V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 42
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/view/LayoutExpandivelView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView_;->alreadyInflated_:Z

    .line 34
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 43
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/LayoutExpandivelView_;->init_()V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/ui/view/LayoutExpandivelView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView_;->alreadyInflated_:Z

    .line 34
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 48
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/LayoutExpandivelView_;->init_()V

    .line 49
    return-void
.end method

.method private init_()V
    .registers 3

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 80
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 81
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 82
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 70
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView_;->alreadyInflated_:Z

    .line 72
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/LayoutExpandivelView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030178

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/ui/view/LayoutExpandivelView_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 75
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/ui/view/LayoutExpandivelView;->onFinishInflate()V

    .line 76
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 104
    const v0, 0x7f0e0661

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView_;->arrow:Landroid/widget/ImageView;

    .line 105
    const v0, 0x7f0e0662

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView_;->conteudoExpandivel:Landroid/widget/TextView;

    .line 106
    const v0, 0x7f0e0660

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/LayoutExpandivelView_;->textViewTitulo:Landroid/widget/TextView;

    .line 107
    const v0, 0x7f0e065f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 109
    .local v1, "view_rl__view_expandivel_click":Landroid/view/View;
    if-eqz v1, :cond_32

    .line 110
    new-instance v0, Lcom/itau/empresas/ui/view/LayoutExpandivelView_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/view/LayoutExpandivelView_$1;-><init>(Lcom/itau/empresas/ui/view/LayoutExpandivelView_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    :cond_32
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/LayoutExpandivelView_;->aterViews()V

    .line 120
    return-void
.end method

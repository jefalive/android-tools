.class final Lcom/google/zxing/client/android/e;
.super Landroid/os/Handler;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;

.field private final c:Lcom/google/zxing/b/a;

.field private final d:Ljava/util/Map;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const-class v0, Lcom/google/zxing/client/android/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/zxing/client/android/e;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;Ljava/util/Map;)V
    .registers 4

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/zxing/client/android/e;->e:Z

    iput-object p2, p0, Lcom/google/zxing/client/android/e;->d:Ljava/util/Map;

    new-instance v0, Lcom/google/zxing/b/a;

    invoke-direct {v0}, Lcom/google/zxing/b/a;-><init>()V

    iput-object v0, p0, Lcom/google/zxing/client/android/e;->c:Lcom/google/zxing/b/a;

    iput-object p1, p0, Lcom/google/zxing/client/android/e;->b:Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;

    return-void
.end method

.method private static a(Lcom/google/zxing/i;Landroid/os/Bundle;)V
    .registers 13

    invoke-virtual {p0}, Lcom/google/zxing/i;->f()[I

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/zxing/i;->g()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/zxing/i;->h()I

    move-result v8

    move-object v0, v6

    move v2, v7

    move v3, v7

    move v4, v8

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x0

    invoke-static/range {v0 .. v5}, Landroid/graphics/Bitmap;->createBitmap([IIIIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v9

    new-instance v10, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v10}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v1, 0x32

    invoke-virtual {v9, v0, v1, v10}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    const-string v0, "barcode_bitmap"

    invoke-virtual {v10}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    const-string v0, "barcode_scaled_factor"

    int-to-float v1, v7

    invoke-virtual {p0}, Lcom/google/zxing/i;->b()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    return-void
.end method

.method private a([BII)V
    .registers 17

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/zxing/client/android/e;->b:Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->getCameraManager()Lcom/google/zxing/client/android/a/g;

    move-result-object v0

    move/from16 v1, p3

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/zxing/client/android/a/g;->a([BII)Lcom/google/zxing/i;

    move-result-object v7

    if-eqz v7, :cond_3a

    new-instance v8, Lcom/google/zxing/c;

    new-instance v0, Lcom/google/zxing/a/d;

    invoke-direct {v0, v7}, Lcom/google/zxing/a/d;-><init>(Lcom/google/zxing/g;)V

    invoke-direct {v8, v0}, Lcom/google/zxing/c;-><init>(Lcom/google/zxing/b;)V

    :try_start_1d
    iget-object v0, p0, Lcom/google/zxing/client/android/e;->c:Lcom/google/zxing/b/a;

    iget-object v1, p0, Lcom/google/zxing/client/android/e;->d:Ljava/util/Map;

    invoke-virtual {v0, v8, v1}, Lcom/google/zxing/b/a;->a(Lcom/google/zxing/c;Ljava/util/Map;)Lcom/google/zxing/k;
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_24} :catch_2c
    .catchall {:try_start_1d .. :try_end_24} :catchall_33

    move-result-object v0

    move-object v6, v0

    iget-object v0, p0, Lcom/google/zxing/client/android/e;->c:Lcom/google/zxing/b/a;

    invoke-virtual {v0}, Lcom/google/zxing/b/a;->a()V

    goto :goto_3a

    :catch_2c
    move-exception v9

    iget-object v0, p0, Lcom/google/zxing/client/android/e;->c:Lcom/google/zxing/b/a;

    invoke-virtual {v0}, Lcom/google/zxing/b/a;->a()V

    goto :goto_3a

    :catchall_33
    move-exception v10

    iget-object v0, p0, Lcom/google/zxing/client/android/e;->c:Lcom/google/zxing/b/a;

    invoke-virtual {v0}, Lcom/google/zxing/b/a;->a()V

    throw v10

    :cond_3a
    :goto_3a
    iget-object v0, p0, Lcom/google/zxing/client/android/e;->b:Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/leitorboleto/ui/activity/BaseBoletoReaderActivity;->getHandler()Landroid/os/Handler;

    move-result-object v8

    if-eqz v6, :cond_7d

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sget-object v0, Lcom/google/zxing/client/android/e;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Found barcode in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sub-long v2, v9, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v8, :cond_7c

    sget v0, Lbr/com/itau/sdk/android/leitorboleto/R$id;->zxing_decode_succeeded:I

    invoke-static {v8, v0, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v11

    new-instance v12, Landroid/os/Bundle;

    invoke-direct {v12}, Landroid/os/Bundle;-><init>()V

    invoke-static {v7, v12}, Lcom/google/zxing/client/android/e;->a(Lcom/google/zxing/i;Landroid/os/Bundle;)V

    invoke-virtual {v11, v12}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    invoke-virtual {v11}, Landroid/os/Message;->sendToTarget()V

    :cond_7c
    goto :goto_88

    :cond_7d
    if-eqz v8, :cond_88

    sget v0, Lbr/com/itau/sdk/android/leitorboleto/R$id;->zxing_decode_failed:I

    invoke-static {v8, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v9

    invoke-virtual {v9}, Landroid/os/Message;->sendToTarget()V

    :cond_88
    :goto_88
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 5

    iget-boolean v0, p0, Lcom/google/zxing/client/android/e;->e:Z

    if-nez v0, :cond_5

    return-void

    :cond_5
    iget v0, p1, Landroid/os/Message;->what:I

    sget v1, Lbr/com/itau/sdk/android/leitorboleto/R$id;->zxing_decode:I

    if-ne v0, v1, :cond_19

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [B

    check-cast v0, [B

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/zxing/client/android/e;->a([BII)V

    goto :goto_29

    :cond_19
    iget v0, p1, Landroid/os/Message;->what:I

    sget v1, Lbr/com/itau/sdk/android/leitorboleto/R$id;->zxing_quit:I

    if-ne v0, v1, :cond_29

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/zxing/client/android/e;->e:Z

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    :cond_29
    :goto_29
    return-void
.end method

.class public Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;
.super Ljava/lang/Object;
.source "WhatsNewVO.java"


# instance fields
.field private final conteudo:Ljava/lang/String;

.field private final imagem:I

.field private final titulo:Ljava/lang/String;


# direct methods
.method constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "imagem"    # I
    .param p2, "titulo"    # Ljava/lang/String;
    .param p3, "conteudo"    # Ljava/lang/String;

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput p1, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;->imagem:I

    .line 16
    iput-object p2, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;->titulo:Ljava/lang/String;

    .line 17
    iput-object p3, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;->conteudo:Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method getConteudo()Ljava/lang/String;
    .registers 2

    .line 29
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;->conteudo:Ljava/lang/String;

    return-object v0
.end method

.method getImagem()I
    .registers 2

    .line 21
    iget v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;->imagem:I

    return v0
.end method

.method getTitulo()Ljava/lang/String;
    .registers 2

    .line 25
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;->titulo:Ljava/lang/String;

    return-object v0
.end method

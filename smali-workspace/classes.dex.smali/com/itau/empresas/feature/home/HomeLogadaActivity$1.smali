.class Lcom/itau/empresas/feature/home/HomeLogadaActivity$1;
.super Ljava/lang/Object;
.source "HomeLogadaActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/home/HomeLogadaActivity;->AfterViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/home/HomeLogadaActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/home/HomeLogadaActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/home/HomeLogadaActivity;

    .line 150
    iput-object p1, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity$1;->this$0:Lcom/itau/empresas/feature/home/HomeLogadaActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 5
    .param p1, "view"    # Landroid/view/View;

    .line 153
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_20

    .line 154
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity$1;->this$0:Lcom/itau/empresas/feature/home/HomeLogadaActivity;

    # invokes: Lcom/itau/empresas/feature/home/HomeLogadaActivity;->criarAnimacaoDeTransicao()Landroid/support/v4/app/ActivityOptionsCompat;
    invoke-static {v0}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->access$000(Lcom/itau/empresas/feature/home/HomeLogadaActivity;)Landroid/support/v4/app/ActivityOptionsCompat;

    move-result-object v2

    .line 156
    .local v2, "options":Landroid/support/v4/app/ActivityOptionsCompat;
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity$1;->this$0:Lcom/itau/empresas/feature/home/HomeLogadaActivity;

    invoke-static {v0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity_$IntentBuilder_;

    move-result-object v0

    .line 157
    invoke-virtual {v2}, Landroid/support/v4/app/ActivityOptionsCompat;->toBundle()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity_$IntentBuilder_;->withOptions(Landroid/os/Bundle;)Lorg/androidannotations/api/builder/ActivityStarter;

    move-result-object v0

    .line 158
    const/16 v1, 0xc8

    invoke-interface {v0, v1}, Lorg/androidannotations/api/builder/ActivityStarter;->startForResult(I)Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 159
    .end local v2    # "options":Landroid/support/v4/app/ActivityOptionsCompat;
    goto :goto_2b

    .line 160
    :cond_20
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeLogadaActivity$1;->this$0:Lcom/itau/empresas/feature/home/HomeLogadaActivity;

    invoke-static {v0}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity_$IntentBuilder_;

    move-result-object v0

    .line 161
    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/activity/areausuario/AreaUsuarioActivity_$IntentBuilder_;->startForResult(I)Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 164
    :goto_2b
    return-void
.end method

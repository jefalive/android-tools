.class public final Lbr/com/itau/sdk/android/core/c/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/c/a$a;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/itau/sdk/android/core/c/c;)V
    .registers 2

    .line 9
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/c/a;-><init>()V

    return-void
.end method

.method public static a()Lbr/com/itau/sdk/android/core/c/a;
    .registers 1

    .line 34
    invoke-static {}, Lbr/com/itau/sdk/android/core/c/a$a;->a()Lbr/com/itau/sdk/android/core/c/a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbr/com/itau/sdk/android/core/c/a;)V
    .registers 1

    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/c/a;->d()V

    return-void
.end method

.method private b()Z
    .registers 2

    .line 24
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/c/a;->c()Lbr/com/itau/sdk/android/core/c/d;

    move-result-object v0

    invoke-interface {v0}, Lbr/com/itau/sdk/android/core/c/d;->a()Ljava/lang/String;

    .line 25
    invoke-static {}, Lbr/com/itau/sdk/android/core/endpoint/a/c;->a()Lbr/com/itau/sdk/android/core/endpoint/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/endpoint/a/c;->b()V

    .line 26
    const/4 v0, 0x1

    return v0
.end method

.method private c()Lbr/com/itau/sdk/android/core/c/d;
    .registers 2

    .line 30
    const-class v0, Lbr/com/itau/sdk/android/core/c/d;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->api(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/c/d;

    return-object v0
.end method

.method private synthetic d()V
    .registers 2

    .line 19
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/c/a;->b()Z

    return-void
.end method


# virtual methods
.method public a(Z)Z
    .registers 4

    .line 15
    if-eqz p1, :cond_7

    .line 16
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/c/a;->b()Z

    move-result v0

    return v0

    .line 19
    :cond_7
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-static {p0}, Lbr/com/itau/sdk/android/core/c/b;->a(Lbr/com/itau/sdk/android/core/c/a;)Lbr/com/itau/sdk/android/core/type/TokenCall;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/sdk/android/core/BackendClient;->call(Lbr/com/itau/sdk/android/core/type/TokenCall;)V

    .line 20
    const/4 v0, 0x1

    return v0
.end method

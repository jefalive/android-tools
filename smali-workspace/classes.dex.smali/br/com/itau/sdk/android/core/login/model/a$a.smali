.class public final enum Lbr/com/itau/sdk/android/core/login/model/a$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/sdk/android/core/login/model/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/sdk/android/core/login/model/a$a;>;"
    }
.end annotation


# static fields
.field public static final enum a:Lbr/com/itau/sdk/android/core/login/model/a$a;

.field public static final enum b:Lbr/com/itau/sdk/android/core/login/model/a$a;

.field public static final enum c:Lbr/com/itau/sdk/android/core/login/model/a$a;

.field private static final synthetic d:[Lbr/com/itau/sdk/android/core/login/model/a$a;

.field private static final e:[B

.field private static f:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 54
    const/16 v0, 0x15

    new-array v0, v0, [B

    fill-array-data v0, :array_5c

    sput-object v0, Lbr/com/itau/sdk/android/core/login/model/a$a;->e:[B

    const/16 v0, 0x37

    sput v0, Lbr/com/itau/sdk/android/core/login/model/a$a;->f:I

    new-instance v0, Lbr/com/itau/sdk/android/core/login/model/a$a;

    sget-object v1, Lbr/com/itau/sdk/android/core/login/model/a$a;->e:[B

    const/4 v2, 0x6

    aget-byte v1, v1, v2

    neg-int v1, v1

    add-int/lit8 v2, v1, -0x3

    const/4 v3, 0x0

    invoke-static {v3, v1, v2}, Lbr/com/itau/sdk/android/core/login/model/a$a;->a(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/login/model/a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/login/model/a$a;->a:Lbr/com/itau/sdk/android/core/login/model/a$a;

    .line 59
    new-instance v0, Lbr/com/itau/sdk/android/core/login/model/a$a;

    const/4 v1, 0x0

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/login/model/a$a;->a(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/login/model/a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/login/model/a$a;->b:Lbr/com/itau/sdk/android/core/login/model/a$a;

    .line 64
    new-instance v0, Lbr/com/itau/sdk/android/core/login/model/a$a;

    sget v1, Lbr/com/itau/sdk/android/core/login/model/a$a;->f:I

    and-int/lit8 v1, v1, 0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/login/model/a$a;->e:[B

    const/4 v3, 0x4

    aget-byte v2, v2, v3

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/login/model/a$a;->a(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/login/model/a$a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/login/model/a$a;->c:Lbr/com/itau/sdk/android/core/login/model/a$a;

    .line 50
    const/4 v0, 0x3

    new-array v0, v0, [Lbr/com/itau/sdk/android/core/login/model/a$a;

    sget-object v1, Lbr/com/itau/sdk/android/core/login/model/a$a;->a:Lbr/com/itau/sdk/android/core/login/model/a$a;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/login/model/a$a;->b:Lbr/com/itau/sdk/android/core/login/model/a$a;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/login/model/a$a;->c:Lbr/com/itau/sdk/android/core/login/model/a$a;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/sdk/android/core/login/model/a$a;->d:[Lbr/com/itau/sdk/android/core/login/model/a$a;

    return-void

    :array_5c
    .array-data 1
        0x18t
        0x7ct
        0x66t
        0x4ct
        0xbt
        -0xat
        -0x4t
        -0x9t
        0xft
        0xbt
        -0x4t
        -0x4t
        -0xct
        -0x3t
        0xdt
        -0xft
        -0xbt
        0xbt
        -0x3t
        -0xct
        0x5t
    .end array-data
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    add-int/lit8 p1, p1, 0x4

    mul-int/lit8 p0, p0, 0xd

    rsub-int/lit8 p0, p0, 0x52

    sget-object v4, Lbr/com/itau/sdk/android/core/login/model/a$a;->e:[B

    new-instance v0, Ljava/lang/String;

    const/4 v5, 0x0

    mul-int/lit8 p2, p2, 0x2

    add-int/lit8 p2, p2, 0x6

    new-array v1, p2, [B

    add-int/lit8 p2, p2, -0x1

    if-nez v4, :cond_1b

    move v2, p0

    move v3, p2

    :goto_17
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p0, v2, -0x2

    :cond_1b
    int-to-byte v2, p0

    aput-byte v2, v1, v5

    add-int/lit8 p1, p1, 0x1

    if-ne v5, p2, :cond_27

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_27
    add-int/lit8 v5, v5, 0x1

    move v2, p0

    aget-byte v3, v4, p1

    goto :goto_17
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/login/model/a$a;
    .registers 2

    .line 50
    const-class v0, Lbr/com/itau/sdk/android/core/login/model/a$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/login/model/a$a;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/sdk/android/core/login/model/a$a;
    .registers 1

    .line 50
    sget-object v0, Lbr/com/itau/sdk/android/core/login/model/a$a;->d:[Lbr/com/itau/sdk/android/core/login/model/a$a;

    invoke-virtual {v0}, [Lbr/com/itau/sdk/android/core/login/model/a$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/sdk/android/core/login/model/a$a;

    return-object v0
.end method

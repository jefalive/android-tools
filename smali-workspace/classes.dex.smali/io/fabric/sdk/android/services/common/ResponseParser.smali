.class public Lio/fabric/sdk/android/services/common/ResponseParser;
.super Ljava/lang/Object;
.source "ResponseParser.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(I)I
    .registers 2
    .param p0, "statusCode"    # I

    .line 35
    const/16 v0, 0xc8

    if-lt p0, v0, :cond_a

    const/16 v0, 0x12b

    if-gt p0, v0, :cond_a

    .line 36
    const/4 v0, 0x0

    return v0

    .line 37
    :cond_a
    const/16 v0, 0x12c

    if-lt p0, v0, :cond_14

    const/16 v0, 0x18f

    if-gt p0, v0, :cond_14

    .line 38
    const/4 v0, 0x1

    return v0

    .line 39
    :cond_14
    const/16 v0, 0x190

    if-lt p0, v0, :cond_1e

    const/16 v0, 0x1f3

    if-gt p0, v0, :cond_1e

    .line 40
    const/4 v0, 0x0

    return v0

    .line 41
    :cond_1e
    const/16 v0, 0x1f4

    if-lt p0, v0, :cond_24

    .line 42
    const/4 v0, 0x1

    return v0

    .line 45
    :cond_24
    const/4 v0, 0x1

    return v0
.end method

.class public Lcom/viewpagerindicator/LinePageIndicator;
.super Landroid/view/View;
.source "LinePageIndicator.java"

# interfaces
.implements Lcom/viewpagerindicator/PageIndicator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/viewpagerindicator/LinePageIndicator$1;,
        Lcom/viewpagerindicator/LinePageIndicator$SavedState;
    }
.end annotation


# instance fields
.field private mActivePointerId:I

.field private mCentered:Z

.field private mCurrentPage:I

.field private mGapWidth:F

.field private mIsDragging:Z

.field private mLastMotionX:F

.field private mLineWidth:F

.field private mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

.field private final mPaintSelected:Landroid/graphics/Paint;

.field private final mPaintUnselected:Landroid/graphics/Paint;

.field private mTouchSlop:I

.field private mViewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/viewpagerindicator/LinePageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 62
    sget v0, Lcom/viewpagerindicator/R$attr;->vpiLinePageIndicatorStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/viewpagerindicator/LinePageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 19
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .line 66
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {p0, v0, v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mPaintUnselected:Landroid/graphics/Paint;

    .line 43
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mPaintSelected:Landroid/graphics/Paint;

    .line 52
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mLastMotionX:F

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mActivePointerId:I

    .line 67
    invoke-virtual {p0}, Lcom/viewpagerindicator/LinePageIndicator;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_27

    return-void

    .line 69
    :cond_27
    invoke-virtual {p0}, Lcom/viewpagerindicator/LinePageIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 72
    .local v5, "res":Landroid/content/res/Resources;
    sget v0, Lcom/viewpagerindicator/R$color;->default_line_indicator_selected_color:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 73
    .local v6, "defaultSelectedColor":I
    sget v0, Lcom/viewpagerindicator/R$color;->default_line_indicator_unselected_color:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    .line 74
    .local v7, "defaultUnselectedColor":I
    sget v0, Lcom/viewpagerindicator/R$dimen;->default_line_indicator_line_width:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    .line 75
    .local v8, "defaultLineWidth":F
    sget v0, Lcom/viewpagerindicator/R$dimen;->default_line_indicator_gap_width:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    .line 76
    .local v9, "defaultGapWidth":F
    sget v0, Lcom/viewpagerindicator/R$dimen;->default_line_indicator_stroke_width:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    .line 77
    .local v10, "defaultStrokeWidth":F
    sget v0, Lcom/viewpagerindicator/R$bool;->default_line_indicator_centered:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v11

    .line 80
    .local v11, "defaultCentered":Z
    sget-object v0, Lcom/viewpagerindicator/R$styleable;->LinePageIndicator:[I

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, p3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v12

    .line 82
    .local v12, "a":Landroid/content/res/TypedArray;
    const/4 v0, 0x1

    invoke-virtual {v12, v0, v11}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mCentered:Z

    .line 83
    const/4 v0, 0x5

    invoke-virtual {v12, v0, v8}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mLineWidth:F

    .line 84
    const/4 v0, 0x6

    invoke-virtual {v12, v0, v9}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mGapWidth:F

    .line 85
    const/4 v0, 0x3

    invoke-virtual {v12, v0, v10}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/viewpagerindicator/LinePageIndicator;->setStrokeWidth(F)V

    .line 86
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mPaintUnselected:Landroid/graphics/Paint;

    const/4 v1, 0x4

    invoke-virtual {v12, v1, v7}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 87
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mPaintSelected:Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-virtual {v12, v1, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 89
    const/4 v0, 0x0

    invoke-virtual {v12, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    .line 90
    .local v13, "background":Landroid/graphics/drawable/Drawable;
    if-eqz v13, :cond_97

    .line 91
    invoke-virtual {p0, v13}, Lcom/viewpagerindicator/LinePageIndicator;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 94
    :cond_97
    invoke-virtual {v12}, Landroid/content/res/TypedArray;->recycle()V

    .line 96
    invoke-static/range {p1 .. p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v14

    .line 97
    .local v14, "configuration":Landroid/view/ViewConfiguration;
    invoke-static {v14}, Landroid/support/v4/view/ViewConfigurationCompat;->getScaledPagingTouchSlop(Landroid/view/ViewConfiguration;)I

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mTouchSlop:I

    .line 98
    return-void
.end method

.method private measureHeight(I)I
    .registers 7
    .param p1, "measureSpec"    # I

    .line 384
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 385
    .local v3, "specMode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 387
    .local v4, "specSize":I
    const/high16 v0, 0x40000000    # 2.0f

    if-ne v3, v0, :cond_e

    .line 389
    int-to-float v2, v4

    .local v2, "result":F
    goto :goto_2a

    .line 392
    .end local v2    # "result":F
    :cond_e
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mPaintSelected:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    invoke-virtual {p0}, Lcom/viewpagerindicator/LinePageIndicator;->getPaddingTop()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/viewpagerindicator/LinePageIndicator;->getPaddingBottom()I

    move-result v1

    int-to-float v1, v1

    add-float v2, v0, v1

    .line 394
    .local v2, "result":F
    const/high16 v0, -0x80000000

    if-ne v3, v0, :cond_2a

    .line 395
    int-to-float v0, v4

    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 398
    :cond_2a
    :goto_2a
    invoke-static {v2}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method private measureWidth(I)I
    .registers 9
    .param p1, "measureSpec"    # I

    .line 357
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 358
    .local v4, "specMode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 360
    .local v5, "specSize":I
    const/high16 v0, 0x40000000    # 2.0f

    if-eq v4, v0, :cond_10

    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_12

    .line 362
    :cond_10
    int-to-float v3, v5

    .local v3, "result":F
    goto :goto_3c

    .line 365
    .end local v3    # "result":F
    :cond_12
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v6

    .line 366
    .local v6, "count":I
    invoke-virtual {p0}, Lcom/viewpagerindicator/LinePageIndicator;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/viewpagerindicator/LinePageIndicator;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    int-to-float v0, v0

    int-to-float v1, v6

    iget v2, p0, Lcom/viewpagerindicator/LinePageIndicator;->mLineWidth:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    add-int/lit8 v1, v6, -0x1

    int-to-float v1, v1

    iget v2, p0, Lcom/viewpagerindicator/LinePageIndicator;->mGapWidth:F

    mul-float/2addr v1, v2

    add-float v3, v0, v1

    .line 368
    .local v3, "result":F
    const/high16 v0, -0x80000000

    if-ne v4, v0, :cond_3c

    .line 369
    int-to-float v0, v5

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 372
    .end local v6    # "count":I
    :cond_3c
    :goto_3c
    invoke-static {v3}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 19
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 158
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-super {v0, v1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 160
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_e

    .line 161
    return-void

    .line 163
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v6

    .line 164
    .local v6, "count":I
    if-nez v6, :cond_1d

    .line 165
    return-void

    .line 168
    :cond_1d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/LinePageIndicator;->mCurrentPage:I

    if-lt v0, v6, :cond_2b

    .line 169
    add-int/lit8 v0, v6, -0x1

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lcom/viewpagerindicator/LinePageIndicator;->setCurrentItem(I)V

    .line 170
    return-void

    .line 173
    :cond_2b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/LinePageIndicator;->mLineWidth:F

    move-object/from16 v1, p0

    iget v1, v1, Lcom/viewpagerindicator/LinePageIndicator;->mGapWidth:F

    add-float v7, v0, v1

    .line 174
    .local v7, "lineWidthAndGap":F
    int-to-float v0, v6

    mul-float/2addr v0, v7

    move-object/from16 v1, p0

    iget v1, v1, Lcom/viewpagerindicator/LinePageIndicator;->mGapWidth:F

    sub-float v8, v0, v1

    .line 175
    .local v8, "indicatorWidth":F
    invoke-virtual/range {p0 .. p0}, Lcom/viewpagerindicator/LinePageIndicator;->getPaddingTop()I

    move-result v0

    int-to-float v9, v0

    .line 176
    .local v9, "paddingTop":F
    invoke-virtual/range {p0 .. p0}, Lcom/viewpagerindicator/LinePageIndicator;->getPaddingLeft()I

    move-result v0

    int-to-float v10, v0

    .line 177
    .local v10, "paddingLeft":F
    invoke-virtual/range {p0 .. p0}, Lcom/viewpagerindicator/LinePageIndicator;->getPaddingRight()I

    move-result v0

    int-to-float v11, v0

    .line 179
    .local v11, "paddingRight":F
    invoke-virtual/range {p0 .. p0}, Lcom/viewpagerindicator/LinePageIndicator;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v9

    invoke-virtual/range {p0 .. p0}, Lcom/viewpagerindicator/LinePageIndicator;->getPaddingBottom()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    add-float v12, v9, v0

    .line 180
    .local v12, "verticalOffset":F
    move v13, v10

    .line 181
    .local v13, "horizontalOffset":F
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/viewpagerindicator/LinePageIndicator;->mCentered:Z

    if-eqz v0, :cond_74

    .line 182
    invoke-virtual/range {p0 .. p0}, Lcom/viewpagerindicator/LinePageIndicator;->getWidth()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr v0, v10

    sub-float/2addr v0, v11

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v8, v1

    sub-float/2addr v0, v1

    add-float/2addr v13, v0

    .line 186
    :cond_74
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_75
    if-ge v14, v6, :cond_9d

    .line 187
    int-to-float v0, v14

    mul-float/2addr v0, v7

    add-float v15, v13, v0

    .line 188
    .local v15, "dx1":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/viewpagerindicator/LinePageIndicator;->mLineWidth:F

    add-float v16, v15, v0

    .line 189
    .local v16, "dx2":F
    move-object/from16 v0, p1

    move v1, v15

    move v2, v12

    move/from16 v3, v16

    move v4, v12

    move-object/from16 v5, p0

    iget v5, v5, Lcom/viewpagerindicator/LinePageIndicator;->mCurrentPage:I

    if-ne v14, v5, :cond_93

    move-object/from16 v5, p0

    iget-object v5, v5, Lcom/viewpagerindicator/LinePageIndicator;->mPaintSelected:Landroid/graphics/Paint;

    goto :goto_97

    :cond_93
    move-object/from16 v5, p0

    iget-object v5, v5, Lcom/viewpagerindicator/LinePageIndicator;->mPaintUnselected:Landroid/graphics/Paint;

    :goto_97
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 186
    .end local v15    # "dx1":F
    .end local v16    # "dx2":F
    add-int/lit8 v14, v14, 0x1

    goto :goto_75

    .line 191
    .end local v14    # "i":I
    :cond_9d
    return-void
.end method

.method protected onMeasure(II)V
    .registers 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .line 345
    invoke-direct {p0, p1}, Lcom/viewpagerindicator/LinePageIndicator;->measureWidth(I)I

    move-result v0

    invoke-direct {p0, p2}, Lcom/viewpagerindicator/LinePageIndicator;->measureHeight(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/viewpagerindicator/LinePageIndicator;->setMeasuredDimension(II)V

    .line 346
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .registers 3
    .param p1, "state"    # I

    .line 316
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_9

    .line 317
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrollStateChanged(I)V

    .line 319
    :cond_9
    return-void
.end method

.method public onPageScrolled(IFI)V
    .registers 5
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .line 323
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_9

    .line 324
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageScrolled(IFI)V

    .line 326
    :cond_9
    return-void
.end method

.method public onPageSelected(I)V
    .registers 3
    .param p1, "position"    # I

    .line 330
    iput p1, p0, Lcom/viewpagerindicator/LinePageIndicator;->mCurrentPage:I

    .line 331
    invoke-virtual {p0}, Lcom/viewpagerindicator/LinePageIndicator;->invalidate()V

    .line 333
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_e

    .line 334
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->onPageSelected(I)V

    .line 336
    :cond_e
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .registers 4
    .param p1, "state"    # Landroid/os/Parcelable;

    .line 403
    move-object v1, p1

    check-cast v1, Lcom/viewpagerindicator/LinePageIndicator$SavedState;

    .line 404
    .local v1, "savedState":Lcom/viewpagerindicator/LinePageIndicator$SavedState;
    invoke-virtual {v1}, Lcom/viewpagerindicator/LinePageIndicator$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 405
    iget v0, v1, Lcom/viewpagerindicator/LinePageIndicator$SavedState;->currentPage:I

    iput v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mCurrentPage:I

    .line 406
    invoke-virtual {p0}, Lcom/viewpagerindicator/LinePageIndicator;->requestLayout()V

    .line 407
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .registers 4

    .line 411
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 412
    .local v1, "superState":Landroid/os/Parcelable;
    new-instance v2, Lcom/viewpagerindicator/LinePageIndicator$SavedState;

    invoke-direct {v2, v1}, Lcom/viewpagerindicator/LinePageIndicator$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 413
    .local v2, "savedState":Lcom/viewpagerindicator/LinePageIndicator$SavedState;
    iget v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mCurrentPage:I

    iput v0, v2, Lcom/viewpagerindicator/LinePageIndicator$SavedState;->currentPage:I

    .line 414
    return-object v2
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 10
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .line 194
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 195
    const/4 v0, 0x1

    return v0

    .line 197
    :cond_8
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1a

    .line 198
    :cond_18
    const/4 v0, 0x0

    return v0

    .line 201
    :cond_1a
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v3, v0, 0xff

    .line 202
    .local v3, "action":I
    packed-switch v3, :pswitch_data_112

    goto/16 :goto_10f

    .line 204
    :pswitch_25
    const/4 v0, 0x0

    invoke-static {p1, v0}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mActivePointerId:I

    .line 205
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mLastMotionX:F

    .line 206
    goto/16 :goto_10f

    .line 209
    :pswitch_34
    iget v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mActivePointerId:I

    invoke-static {p1, v0}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v4

    .line 210
    .local v4, "activePointerIndex":I
    invoke-static {p1, v4}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v5

    .line 211
    .local v5, "x":F
    iget v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mLastMotionX:F

    sub-float v6, v5, v0

    .line 213
    .local v6, "deltaX":F
    iget-boolean v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mIsDragging:Z

    if-nez v0, :cond_54

    .line 214
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/viewpagerindicator/LinePageIndicator;->mTouchSlop:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_54

    .line 215
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mIsDragging:Z

    .line 219
    :cond_54
    iget-boolean v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mIsDragging:Z

    if-eqz v0, :cond_10f

    .line 220
    iput v5, p0, Lcom/viewpagerindicator/LinePageIndicator;->mLastMotionX:F

    .line 221
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->isFakeDragging()Z

    move-result v0

    if-nez v0, :cond_6a

    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->beginFakeDrag()Z

    move-result v0

    if-eqz v0, :cond_10f

    .line 222
    :cond_6a
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v6}, Landroid/support/v4/view/ViewPager;->fakeDragBy(F)V

    goto/16 :goto_10f

    .line 231
    .end local v4    # "activePointerIndex":I
    .end local v5    # "x":F
    .end local v6    # "deltaX":F
    :pswitch_71
    iget-boolean v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mIsDragging:Z

    if-nez v0, :cond_c7

    .line 232
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v4

    .line 233
    .local v4, "count":I
    invoke-virtual {p0}, Lcom/viewpagerindicator/LinePageIndicator;->getWidth()I

    move-result v5

    .line 234
    .local v5, "width":I
    int-to-float v0, v5

    const/high16 v1, 0x40000000    # 2.0f

    div-float v6, v0, v1

    .line 235
    .local v6, "halfWidth":F
    int-to-float v0, v5

    const/high16 v1, 0x40c00000    # 6.0f

    div-float v7, v0, v1

    .line 237
    .local v7, "sixthWidth":F
    iget v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mCurrentPage:I

    if-lez v0, :cond_a9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    sub-float v1, v6, v7

    cmpg-float v0, v0, v1

    if-gez v0, :cond_a9

    .line 238
    const/4 v0, 0x3

    if-eq v3, v0, :cond_a7

    .line 239
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/viewpagerindicator/LinePageIndicator;->mCurrentPage:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 241
    :cond_a7
    const/4 v0, 0x1

    return v0

    .line 242
    :cond_a9
    iget v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mCurrentPage:I

    add-int/lit8 v1, v4, -0x1

    if-ge v0, v1, :cond_c7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    add-float v1, v6, v7

    cmpl-float v0, v0, v1

    if-lez v0, :cond_c7

    .line 243
    const/4 v0, 0x3

    if-eq v3, v0, :cond_c5

    .line 244
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/viewpagerindicator/LinePageIndicator;->mCurrentPage:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 246
    :cond_c5
    const/4 v0, 0x1

    return v0

    .line 250
    .end local v4    # "count":I
    .end local v5    # "width":I
    .end local v6    # "halfWidth":F
    .end local v7    # "sixthWidth":F
    :cond_c7
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mIsDragging:Z

    .line 251
    const/4 v0, -0x1

    iput v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mActivePointerId:I

    .line 252
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->isFakeDragging()Z

    move-result v0

    if-eqz v0, :cond_10f

    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->endFakeDrag()V

    goto :goto_10f

    .line 256
    :pswitch_db
    invoke-static {p1}, Landroid/support/v4/view/MotionEventCompat;->getActionIndex(Landroid/view/MotionEvent;)I

    move-result v4

    .line 257
    .local v4, "index":I
    invoke-static {p1, v4}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mLastMotionX:F

    .line 258
    invoke-static {p1, v4}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mActivePointerId:I

    .line 259
    goto :goto_10f

    .line 263
    .end local v4    # "index":I
    :pswitch_ec
    invoke-static {p1}, Landroid/support/v4/view/MotionEventCompat;->getActionIndex(Landroid/view/MotionEvent;)I

    move-result v4

    .line 264
    .local v4, "pointerIndex":I
    invoke-static {p1, v4}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v5

    .line 265
    .local v5, "pointerId":I
    iget v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mActivePointerId:I

    if-ne v5, v0, :cond_103

    .line 266
    if-nez v4, :cond_fc

    const/4 v6, 0x1

    goto :goto_fd

    :cond_fc
    const/4 v6, 0x0

    .line 267
    .local v6, "newPointerIndex":I
    :goto_fd
    invoke-static {p1, v6}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mActivePointerId:I

    .line 269
    .end local v6    # "newPointerIndex":I
    :cond_103
    iget v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mActivePointerId:I

    invoke-static {p1, v0}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v0

    invoke-static {p1, v0}, Landroid/support/v4/view/MotionEventCompat;->getX(Landroid/view/MotionEvent;I)F

    move-result v0

    iput v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mLastMotionX:F

    .line 273
    .end local v4    # "pointerIndex":I
    .end local v5    # "pointerId":I
    :cond_10f
    :goto_10f
    :pswitch_10f
    const/4 v0, 0x1

    return v0

    nop

    :pswitch_data_112
    .packed-switch 0x0
        :pswitch_25
        :pswitch_71
        :pswitch_34
        :pswitch_71
        :pswitch_10f
        :pswitch_db
        :pswitch_ec
    .end packed-switch
.end method

.method public setCentered(Z)V
    .registers 2
    .param p1, "centered"    # Z

    .line 102
    iput-boolean p1, p0, Lcom/viewpagerindicator/LinePageIndicator;->mCentered:Z

    .line 103
    invoke-virtual {p0}, Lcom/viewpagerindicator/LinePageIndicator;->invalidate()V

    .line 104
    return-void
.end method

.method public setCurrentItem(I)V
    .registers 4
    .param p1, "item"    # I

    .line 301
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_c

    .line 302
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ViewPager has not been bound."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 304
    :cond_c
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 305
    iput p1, p0, Lcom/viewpagerindicator/LinePageIndicator;->mCurrentPage:I

    .line 306
    invoke-virtual {p0}, Lcom/viewpagerindicator/LinePageIndicator;->invalidate()V

    .line 307
    return-void
.end method

.method public setGapWidth(F)V
    .registers 2
    .param p1, "gapWidth"    # F

    .line 148
    iput p1, p0, Lcom/viewpagerindicator/LinePageIndicator;->mGapWidth:F

    .line 149
    invoke-virtual {p0}, Lcom/viewpagerindicator/LinePageIndicator;->invalidate()V

    .line 150
    return-void
.end method

.method public setLineWidth(F)V
    .registers 2
    .param p1, "lineWidth"    # F

    .line 129
    iput p1, p0, Lcom/viewpagerindicator/LinePageIndicator;->mLineWidth:F

    .line 130
    invoke-virtual {p0}, Lcom/viewpagerindicator/LinePageIndicator;->invalidate()V

    .line 131
    return-void
.end method

.method public setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V
    .registers 2
    .param p1, "listener"    # Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 340
    iput-object p1, p0, Lcom/viewpagerindicator/LinePageIndicator;->mListener:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 341
    return-void
.end method

.method public setSelectedColor(I)V
    .registers 3
    .param p1, "selectedColor"    # I

    .line 120
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mPaintSelected:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 121
    invoke-virtual {p0}, Lcom/viewpagerindicator/LinePageIndicator;->invalidate()V

    .line 122
    return-void
.end method

.method public setStrokeWidth(F)V
    .registers 3
    .param p1, "lineHeight"    # F

    .line 138
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mPaintSelected:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 139
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mPaintUnselected:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 140
    invoke-virtual {p0}, Lcom/viewpagerindicator/LinePageIndicator;->invalidate()V

    .line 141
    return-void
.end method

.method public setUnselectedColor(I)V
    .registers 3
    .param p1, "unselectedColor"    # I

    .line 111
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mPaintUnselected:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 112
    invoke-virtual {p0}, Lcom/viewpagerindicator/LinePageIndicator;->invalidate()V

    .line 113
    return-void
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;)V
    .registers 4
    .param p1, "viewPager"    # Landroid/support/v4/view/ViewPager;

    .line 278
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-ne v0, p1, :cond_5

    .line 279
    return-void

    .line 281
    :cond_5
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_f

    .line 283
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 285
    :cond_f
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    if-nez v0, :cond_1d

    .line 286
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ViewPager does not have adapter instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 288
    :cond_1d
    iput-object p1, p0, Lcom/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 289
    iget-object v0, p0, Lcom/viewpagerindicator/LinePageIndicator;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 290
    invoke-virtual {p0}, Lcom/viewpagerindicator/LinePageIndicator;->invalidate()V

    .line 291
    return-void
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;I)V
    .registers 3
    .param p1, "view"    # Landroid/support/v4/view/ViewPager;
    .param p2, "initialPosition"    # I

    .line 295
    invoke-virtual {p0, p1}, Lcom/viewpagerindicator/LinePageIndicator;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 296
    invoke-virtual {p0, p2}, Lcom/viewpagerindicator/LinePageIndicator;->setCurrentItem(I)V

    .line 297
    return-void
.end method

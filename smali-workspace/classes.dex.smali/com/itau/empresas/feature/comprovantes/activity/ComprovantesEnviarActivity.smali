.class public Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "ComprovantesEnviarActivity.java"


# instance fields
.field campoComentario:Lbr/com/itau/widgets/material/MaterialEditText;

.field campoEmail:Lbr/com/itau/widgets/material/MaterialEditText;

.field comprovantesController:Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;

.field comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

.field private idComprovante:Ljava/lang/String;

.field recargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

.field root:Landroid/widget/LinearLayout;

.field textoComprovante:Landroid/widget/TextView;

.field textoDataComprovante:Landroid/widget/TextView;

.field textoNomeComprovante:Landroid/widget/TextView;

.field textoValorComprovante:Landroid/widget/TextView;

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field private topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 51
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 240
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 241
    const v1, 0x7f0706a5

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 242
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 243
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 244
    return-void
.end method

.method private disparaEventoAnalytics()V
    .registers 5

    .line 102
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v3

    .line 103
    .line 104
    .local v3, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    const v0, 0x7f0700db

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 105
    const v1, 0x7f0700c6

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 106
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 103
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 247
    new-instance v0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private isValido()Z
    .registers 4

    .line 208
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->campoEmail:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->validate()Z

    move-result v1

    .line 209
    .local v1, "emailValido":Z
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->campoComentario:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->validate()Z

    move-result v2

    .line 210
    .local v2, "comentarioValido":Z
    if-eqz v1, :cond_12

    if-eqz v2, :cond_12

    const/4 v0, 0x1

    goto :goto_13

    :cond_12
    const/4 v0, 0x0

    :goto_13
    return v0
.end method

.method private preenchaComComprovanteVO(Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;)V
    .registers 4
    .param p1, "comprovantesVO"    # Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    .line 160
    invoke-virtual {p1}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getIdComprovante()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->idComprovante:Ljava/lang/String;

    .line 161
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->textoComprovante:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getNomeOperacao()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    invoke-virtual {p1}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getDataComprovante()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->setTextoDataPagamento(Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->textoNomeComprovante:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getLiteralIdentificacaoComprovante()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    invoke-virtual {p1}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getValor()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->setTextoValorComprovante(Ljava/lang/String;)V

    .line 165
    return-void
.end method

.method private preenchaComRecargaVO(Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;)V
    .registers 8
    .param p1, "recargaVO"    # Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    .line 168
    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;->getComprovante()Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;

    move-result-object v2

    .line 169
    .local v2, "comprovanteVO":Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;
    invoke-virtual {v2}, Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;->getCodigoComprovante()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->idComprovante:Ljava/lang/String;

    .line 170
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->textoComprovante:Landroid/widget/TextView;

    const-string v1, "Recarga"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    invoke-virtual {v2}, Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;->getDataPagamento()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->setTextoDataPagamento(Ljava/lang/String;)V

    .line 172
    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;->getRecarga()Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getNomeOperadora()Ljava/lang/String;

    move-result-object v3

    .line 173
    .local v3, "nomeOperadora":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;->getRecarga()Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getNomeRegiao()Ljava/lang/String;

    move-result-object v4

    .line 174
    .local v4, "nomeRegiao":Ljava/lang/String;
    if-eqz v3, :cond_55

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_55

    .line 175
    move-object v5, v3

    .line 176
    .local v5, "operadora":Ljava/lang/String;
    if-eqz v4, :cond_50

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_50

    .line 177
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 179
    :cond_50
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->textoNomeComprovante:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    .end local v5    # "operadora":Ljava/lang/String;
    :cond_55
    invoke-virtual {p1}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;->getRecarga()Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;->getValorRecarga()Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->setTextoValorComprovante(Ljava/lang/String;)V

    .line 182
    return-void
.end method

.method private setTextoDataPagamento(Ljava/lang/String;)V
    .registers 6
    .param p1, "dataPagamento"    # Ljava/lang/String;

    .line 185
    if-eqz p1, :cond_2c

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2c

    .line 186
    invoke-static {p1}, Lorg/threeten/bp/LocalDate;->parse(Ljava/lang/CharSequence;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    .line 187
    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->atStartOfDay()Lorg/threeten/bp/LocalDateTime;

    move-result-object v0

    .line 188
    invoke-static {}, Lorg/threeten/bp/ZoneId;->systemDefault()Lorg/threeten/bp/ZoneId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/threeten/bp/LocalDateTime;->atZone(Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    .line 189
    invoke-virtual {v0}, Lorg/threeten/bp/ZonedDateTime;->toInstant()Lorg/threeten/bp/Instant;

    move-result-object v0

    .line 186
    invoke-static {v0}, Lorg/threeten/bp/DateTimeUtils;->toDate(Lorg/threeten/bp/Instant;)Ljava/util/Date;

    move-result-object v2

    .line 190
    .local v2, "date":Ljava/util/Date;
    const-string v0, "dd/MM/yyyy"

    invoke-static {v0, v2}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDate(Ljava/lang/String;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .line 191
    .local v3, "dataFormatada":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->textoDataComprovante:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 192
    .end local v2    # "date":Ljava/util/Date;
    .end local v3    # "dataFormatada":Ljava/lang/String;
    goto :goto_33

    .line 193
    :cond_2c
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->textoDataComprovante:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    :goto_33
    return-void
.end method

.method private setTextoValorComprovante(Ljava/lang/String;)V
    .registers 5
    .param p1, "valor"    # Ljava/lang/String;

    .line 198
    if-eqz p1, :cond_13

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_13

    .line 199
    .line 200
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 201
    .local v2, "valorFormatado":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->textoValorComprovante:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    .end local v2    # "valorFormatado":Ljava/lang/String;
    goto :goto_1a

    .line 203
    :cond_13
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->textoValorComprovante:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 205
    :goto_1a
    return-void
.end method


# virtual methods
.method protected botaoEnviar()V
    .registers 8

    .line 111
    invoke-direct {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->isValido()Z

    move-result v0

    if-nez v0, :cond_7

    .line 112
    return-void

    .line 114
    :cond_7
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    if-eqz v0, :cond_10

    .line 115
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismiss()V

    .line 116
    :cond_10
    invoke-virtual {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->mostraDialogoDeProgresso()V

    .line 117
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 118
    const v2, 0x7f0706a5

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 119
    const v3, 0x7f0702a9

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 121
    new-instance v5, Lcom/itau/empresas/api/model/EnvioComprovanteVO;

    invoke-direct {v5}, Lcom/itau/empresas/api/model/EnvioComprovanteVO;-><init>()V

    .line 122
    .local v5, "envioVO":Lcom/itau/empresas/api/model/EnvioComprovanteVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getNomePerfil()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/itau/empresas/api/model/EnvioComprovanteVO;->setNome(Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->campoEmail:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/itau/empresas/api/model/EnvioComprovanteVO;->setEnderecoEmail(Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->campoComentario:Lbr/com/itau/widgets/material/MaterialEditText;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 125
    .local v6, "mensagem":Ljava/lang/String;
    if-eqz v6, :cond_61

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_63

    .line 126
    :cond_61
    const-string v6, " "

    .line 128
    :cond_63
    invoke-virtual {v5, v6}, Lcom/itau/empresas/api/model/EnvioComprovanteVO;->setMensagem(Ljava/lang/String;)V

    .line 129
    const v0, 0x7f0702a9

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->analyticsHit(Ljava/lang/String;)V

    .line 130
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->comprovantesController:Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;

    iget-object v1, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->idComprovante:Ljava/lang/String;

    invoke-virtual {v0, v1, v5}, Lcom/itau/empresas/feature/comprovantes/controller/ComprovantesController;->enviarComprovante(Ljava/lang/String;Lcom/itau/empresas/api/model/EnvioComprovanteVO;)V

    .line 131
    return-void
.end method

.method protected carregaElementosDaTela()V
    .registers 5

    .line 79
    invoke-direct {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->constroiToolbar()V

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->campoEmail:Lbr/com/itau/widgets/material/MaterialEditText;

    iget-object v1, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->campoEmail:Lbr/com/itau/widgets/material/MaterialEditText;

    new-instance v1, Lbr/com/itau/widgets/material/validation/RegexpValidator;

    const v2, 0x7f07016a

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    .line 82
    invoke-virtual {v3}, Ljava/util/regex/Pattern;->pattern()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lbr/com/itau/widgets/material/validation/RegexpValidator;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->addValidator(Lbr/com/itau/widgets/material/validation/METValidator;)Lbr/com/itau/widgets/material/MaterialEditText;

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->campoComentario:Lbr/com/itau/widgets/material/MaterialEditText;

    new-instance v1, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity$1;

    .line 84
    const v2, 0x7f070162

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity$1;-><init>(Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialEditText;->addValidator(Lbr/com/itau/widgets/material/validation/METValidator;)Lbr/com/itau/widgets/material/MaterialEditText;

    .line 93
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    if-eqz v0, :cond_4c

    .line 94
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->comprovantesVO:Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->preenchaComComprovanteVO(Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;)V

    goto :goto_55

    .line 95
    :cond_4c
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->recargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    if-eqz v0, :cond_55

    .line 96
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->recargaVO:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->preenchaComRecargaVO(Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;)V

    .line 98
    :cond_55
    :goto_55
    invoke-direct {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->disparaEventoAnalytics()V

    .line 99
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 6
    .param p1, "menu"    # Landroid/view/Menu;

    .line 220
    invoke-static {}, Lcom/itau/empresas/ui/util/PoyntUtils;->isPoynt()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 221
    invoke-virtual {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0005

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 222
    const v0, 0x7f0e06c6

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 223
    .local v2, "itemMenuPesquisa":Landroid/view/MenuItem;
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 224
    const v0, 0x7f0e06c5

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 225
    .local v3, "itemMenuFiltro":Landroid/view/MenuItem;
    const/4 v0, 0x0

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 227
    .end local v2    # "itemMenuPesquisa":Landroid/view/MenuItem;
    .end local v3    # "itemMenuFiltro":Landroid/view/MenuItem;
    :cond_26
    const/4 v0, 0x1

    return v0
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;

    .line 141
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 142
    return-void

    .line 144
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->escondeDialogoDeProgresso()V

    .line 145
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->root:Landroid/widget/LinearLayout;

    const v1, 0x7f0701f2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;II)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 146
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 147
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 151
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 152
    return-void

    .line 154
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->escondeDialogoDeProgresso()V

    .line 155
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 156
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 157
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/api/model/ResultadoComprovanteEnviadoVO;)V
    .registers 5
    .param p1, "resultadoComprovanteEnviadoVO"    # Lcom/itau/empresas/api/model/ResultadoComprovanteEnviadoVO;

    .line 134
    invoke-virtual {p0}, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->escondeDialogoDeProgresso()V

    .line 135
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->root:Landroid/widget/LinearLayout;

    const v1, 0x7f070520

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;II)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 136
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 137
    invoke-static {p0}, Lcom/itau/empresas/feature/feedback/FeedbackHelper;->exibirFeedback(Landroid/support/v7/app/AppCompatActivity;)V

    .line 138
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 232
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0e06cf

    if-ne v0, v1, :cond_10

    .line 233
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/activity/ComprovantesEnviarActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/ViewUtils;->aoClicarBotaoSairMenu(Lcom/itau/empresas/CustomApplication;Lcom/itau/empresas/ui/activity/BaseActivity;)V

    .line 234
    const/4 v0, 0x1

    return v0

    .line 236
    :cond_10
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 215
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "enviarComprovanteEmail"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "comprovantes"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

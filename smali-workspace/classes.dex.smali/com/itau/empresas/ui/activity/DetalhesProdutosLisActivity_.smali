.class public final Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;
.super Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;
.source "DetalhesProdutosLisActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 35
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;-><init>()V

    .line 39
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;

    .line 35
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;

    .line 35
    invoke-super {p0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 52
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 53
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 54
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 55
    invoke-static {p0}, Lcom/itau/empresas/adapter/DetalhesLisAdapter_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/adapter/DetalhesLisAdapter_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;->detalhesProdutosListAdapter:Lcom/itau/empresas/adapter/DetalhesLisAdapter;

    .line 56
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;->injectExtras_()V

    .line 57
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;->afterInject()V

    .line 58
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 98
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 99
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_1c

    .line 100
    const-string v0, "lisVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 101
    const-string v0, "lisVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/LisVO;

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;->lisVO:Lcom/itau/empresas/api/model/LisVO;

    .line 104
    :cond_1c
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 129
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_$2;-><init>(Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 137
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 117
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_$1;-><init>(Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 125
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 45
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;->init_(Landroid/os/Bundle;)V

    .line 46
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->onCreate(Landroid/os/Bundle;)V

    .line 47
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 48
    const v0, 0x7f03004e

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;->setContentView(I)V

    .line 49
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 92
    const v0, 0x7f0e0258

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;->lista:Landroid/widget/ListView;

    .line 93
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 94
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;->aoIniciarActivity()V

    .line 95
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 62
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->setContentView(I)V

    .line 63
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 64
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 74
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->setContentView(Landroid/view/View;)V

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 76
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 68
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 70
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 108
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity;->setIntent(Landroid/content/Intent;)V

    .line 109
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/DetalhesProdutosLisActivity_;->injectExtras_()V

    .line 110
    return-void
.end method

.class public Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;
.super Ljava/lang/Object;
.source "EmprestimosParceladosDadosSeguroVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private indicadorSeguro:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_seguro"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isIndicadorSeguro()Z
    .registers 2

    .line 14
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;->indicadorSeguro:Z

    return v0
.end method

.method public setIndicadorSeguro(Z)V
    .registers 2
    .param p1, "indicadorSeguro"    # Z

    .line 18
    iput-boolean p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;->indicadorSeguro:Z

    .line 19
    return-void
.end method

.class final synthetic Lbr/com/itau/sdk/android/core/token/h;
.super Ljava/lang/Object;

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/TokenCall;


# instance fields
.field private final a:Lbr/com/itau/sdk/android/core/token/c;

.field private final b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lbr/com/itau/sdk/android/core/token/c;Ljava/lang/String;)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbr/com/itau/sdk/android/core/token/h;->a:Lbr/com/itau/sdk/android/core/token/c;

    iput-object p2, p0, Lbr/com/itau/sdk/android/core/token/h;->b:Ljava/lang/String;

    return-void
.end method

.method public static a(Lbr/com/itau/sdk/android/core/token/c;Ljava/lang/String;)Lbr/com/itau/sdk/android/core/type/TokenCall;
    .registers 3

    new-instance v0, Lbr/com/itau/sdk/android/core/token/h;

    invoke-direct {v0, p0, p1}, Lbr/com/itau/sdk/android/core/token/h;-><init>(Lbr/com/itau/sdk/android/core/token/c;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public callBackend()V
    .registers 3
    .annotation runtime Ljava/lang/invoke/LambdaForm$Hidden;
    .end annotation

    iget-object v0, p0, Lbr/com/itau/sdk/android/core/token/h;->a:Lbr/com/itau/sdk/android/core/token/c;

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/token/h;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lbr/com/itau/sdk/android/core/token/c;->a(Lbr/com/itau/sdk/android/core/token/c;Ljava/lang/String;)V

    return-void
.end method

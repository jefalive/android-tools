.class public Lcom/itau/empresas/feature/whatsnew/WhatsNewIntroducaoActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "WhatsNewIntroducaoActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragmentListener;


# instance fields
.field private botaoNaoObrigado:Landroid/support/v7/widget/AppCompatButton;

.field private botaoNovidadades:Landroid/support/v7/widget/AppCompatButton;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 25
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    return-void
.end method

.method private navegarParaNovidades()V
    .registers 4

    .line 62
    invoke-static {}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->newInstance()Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;

    move-result-object v2

    .line 63
    .local v2, "fragment":Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;
    invoke-virtual {v2, p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->setBotaoFecharListener(Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragmentListener;)V

    .line 64
    invoke-virtual {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewIntroducaoActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "whatsnewDialog"

    invoke-virtual {v2, v0, v1}, Lcom/itau/empresas/feature/whatsnew/WhatsNewDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method private navegarParaTelaAnterior()V
    .registers 1

    .line 68
    invoke-virtual {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewIntroducaoActivity;->finish()V

    .line 69
    return-void
.end method

.method private salvarWhatsNew()V
    .registers 5

    .line 72
    const-string v0, "whats-new"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/whatsnew/WhatsNewIntroducaoActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 73
    .local v3, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 74
    invoke-static {}, Lcom/itau/empresas/ui/util/VersaoUtils;->versaoSemMinor()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 75
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 76
    return-void
.end method


# virtual methods
.method public clicouNoBotaoFechar()V
    .registers 1

    .line 58
    invoke-virtual {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewIntroducaoActivity;->finish()V

    .line 59
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 4
    .param p1, "v"    # Landroid/view/View;

    .line 47
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0e0359

    if-ne v0, v1, :cond_d

    .line 48
    invoke-direct {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewIntroducaoActivity;->navegarParaNovidades()V

    .line 49
    return-void

    .line 51
    :cond_d
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0e035a

    if-ne v0, v1, :cond_19

    .line 52
    invoke-direct {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewIntroducaoActivity;->navegarParaTelaAnterior()V

    .line 54
    :cond_19
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 36
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    const v0, 0x7f03006d

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewIntroducaoActivity;->setContentView(I)V

    .line 38
    const v0, 0x7f0e0359

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewIntroducaoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/AppCompatButton;

    iput-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewIntroducaoActivity;->botaoNovidadades:Landroid/support/v7/widget/AppCompatButton;

    .line 39
    const v0, 0x7f0e035a

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewIntroducaoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/AppCompatButton;

    iput-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewIntroducaoActivity;->botaoNaoObrigado:Landroid/support/v7/widget/AppCompatButton;

    .line 40
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewIntroducaoActivity;->botaoNovidadades:Landroid/support/v7/widget/AppCompatButton;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/AppCompatButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/feature/whatsnew/WhatsNewIntroducaoActivity;->botaoNaoObrigado:Landroid/support/v7/widget/AppCompatButton;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/AppCompatButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    invoke-direct {p0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewIntroducaoActivity;->salvarWhatsNew()V

    .line 43
    return-void
.end method

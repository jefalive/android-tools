.class public final Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;
.super Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;
.source "ValorRecargaView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 41
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;-><init>(Landroid/content/Context;)V

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->alreadyInflated_:Z

    .line 38
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 42
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->init_()V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 46
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->alreadyInflated_:Z

    .line 38
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 47
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->init_()V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 51
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->alreadyInflated_:Z

    .line 38
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 52
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->init_()V

    .line 53
    return-void
.end method

.method private init_()V
    .registers 3

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 84
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 85
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->application:Lcom/itau/empresas/CustomApplication;

    .line 86
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->controller:Lcom/itau/empresas/feature/recarga/efetivacao/controller/RecargaController;

    .line 87
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 88
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 74
    iget-boolean v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->alreadyInflated_:Z

    .line 76
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030187

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 79
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView;->onFinishInflate()V

    .line 80
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 110
    const v0, 0x7f0e069a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/library/LinearValuePickerView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->escolhePeriodoValor:Lbr/com/itau/library/LinearValuePickerView;

    .line 111
    const v0, 0x7f0e0698

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->llCampoValor:Landroid/widget/LinearLayout;

    .line 112
    const v0, 0x7f0e0699

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->llRadioValor:Landroid/widget/LinearLayout;

    .line 113
    const v0, 0x7f0e069b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->linhaEscolhePeriodoValor:Landroid/view/View;

    .line 114
    const v0, 0x7f0e0502

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/EditTextMonetario;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->campoValor:Lcom/itau/empresas/ui/view/EditTextMonetario;

    .line 115
    const v0, 0x7f0e069c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->textoValorErro:Landroid/widget/TextView;

    .line 117
    const v0, 0x7f0e0502

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/widget/TextView;

    .line 118
    .local v1, "view":Landroid/widget/TextView;
    if-eqz v1, :cond_54

    .line 119
    new-instance v0, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_$1;-><init>(Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;)V

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 137
    .end local v1    # "view":Landroid/widget/TextView;
    :cond_54
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ValorRecargaView_;->aoCarregarTela()V

    .line 138
    return-void
.end method

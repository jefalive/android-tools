.class final Lcom/adobe/mobile/MessageFullScreen;
.super Lcom/adobe/mobile/Message;
.source "MessageFullScreen.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenWebViewClient;,
        Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;
    }
.end annotation


# instance fields
.field protected html:Ljava/lang/String;

.field protected messageFullScreenActivity:Landroid/app/Activity;

.field protected replacedHtml:Ljava/lang/String;

.field protected rootViewGroup:Landroid/view/ViewGroup;

.field private webView:Landroid/webkit/WebView;


# direct methods
.method constructor <init>()V
    .registers 1

    .line 42
    invoke-direct {p0}, Lcom/adobe/mobile/Message;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/adobe/mobile/MessageFullScreen;)Landroid/webkit/WebView;
    .registers 2
    .param p0, "x0"    # Lcom/adobe/mobile/MessageFullScreen;

    .line 42
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen;->webView:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic access$002(Lcom/adobe/mobile/MessageFullScreen;Landroid/webkit/WebView;)Landroid/webkit/WebView;
    .registers 2
    .param p0, "x0"    # Lcom/adobe/mobile/MessageFullScreen;
    .param p1, "x1"    # Landroid/webkit/WebView;

    .line 42
    iput-object p1, p0, Lcom/adobe/mobile/MessageFullScreen;->webView:Landroid/webkit/WebView;

    return-object p1
.end method

.method static synthetic access$100(Lcom/adobe/mobile/MessageFullScreen;)V
    .registers 1
    .param p0, "x0"    # Lcom/adobe/mobile/MessageFullScreen;

    .line 42
    invoke-static {p0}, Lcom/adobe/mobile/MessageFullScreen;->killMessageActivity(Lcom/adobe/mobile/MessageFullScreen;)V

    return-void
.end method

.method private static killMessageActivity(Lcom/adobe/mobile/MessageFullScreen;)V
    .registers 4
    .param p0, "message"    # Lcom/adobe/mobile/MessageFullScreen;

    .line 359
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen;->messageFullScreenActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 360
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen;->messageFullScreenActivity:Landroid/app/Activity;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 361
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/adobe/mobile/MessageFullScreen;->isVisible:Z

    .line 362
    return-void
.end method


# virtual methods
.method protected initWithPayloadObject(Lorg/json/JSONObject;)Z
    .registers 14
    .param p1, "dictionary"    # Lorg/json/JSONObject;

    .line 66
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-gtz v0, :cond_a

    .line 67
    :cond_8
    const/4 v0, 0x0

    return v0

    .line 71
    :cond_a
    invoke-super {p0, p1}, Lcom/adobe/mobile/Message;->initWithPayloadObject(Lorg/json/JSONObject;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 72
    const/4 v0, 0x0

    return v0

    .line 78
    :cond_12
    const-string v0, "payload"

    :try_start_14
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 79
    .local v4, "jsonPayload":Lorg/json/JSONObject;
    invoke-virtual {v4}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-gtz v0, :cond_2d

    .line 80
    const-string v0, "Messages - Unable to create fullscreen message \"%s\", payload is empty"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/MessageFullScreen;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2b
    .catch Lorg/json/JSONException; {:try_start_14 .. :try_end_2b} :catch_2e

    .line 81
    const/4 v0, 0x0

    return v0

    .line 87
    :cond_2d
    goto :goto_3e

    .line 84
    .end local v4    # "jsonPayload":Lorg/json/JSONObject;
    :catch_2e
    move-exception v5

    .line 85
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - Unable to create fullscreen message \"%s\", payload is required"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/MessageFullScreen;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 86
    const/4 v0, 0x0

    return v0

    .line 91
    .local v4, "jsonPayload":Lorg/json/JSONObject;
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_3e
    const-string v0, "html"

    :try_start_40
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MessageFullScreen;->html:Ljava/lang/String;

    .line 92
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen;->html:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_5d

    .line 93
    const-string v0, "Messages - Unable to create fullscreen message \"%s\", html is empty"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/MessageFullScreen;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5b
    .catch Lorg/json/JSONException; {:try_start_40 .. :try_end_5b} :catch_5e

    .line 94
    const/4 v0, 0x0

    return v0

    .line 100
    :cond_5d
    goto :goto_6e

    .line 97
    :catch_5e
    move-exception v5

    .line 98
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - Unable to create fullscreen message \"%s\", html is required"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/MessageFullScreen;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 99
    const/4 v0, 0x0

    return v0

    .line 104
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_6e
    const-string v0, "assets"

    :try_start_70
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 105
    .local v5, "assetsArray":Lorg/json/JSONArray;
    if-eqz v5, :cond_b4

    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_b4

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/adobe/mobile/MessageFullScreen;->assets:Ljava/util/ArrayList;

    .line 107
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    .line 108
    .local v6, "count":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_88
    if-ge v7, v6, :cond_b4

    .line 109
    invoke-virtual {v5, v7}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v8

    .line 110
    .local v8, "currentAssetJson":Lorg/json/JSONArray;
    if-eqz v8, :cond_b1

    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_b1

    .line 111
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 112
    .local v9, "currentAsset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v10

    .line 113
    .local v10, "innerCount":I
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_a0
    if-ge v11, v10, :cond_ac

    .line 114
    invoke-virtual {v8, v11}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    add-int/lit8 v11, v11, 0x1

    goto :goto_a0

    .line 117
    .end local v11    # "j":I
    :cond_ac
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen;->assets:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_b1
    .catch Lorg/json/JSONException; {:try_start_70 .. :try_end_b1} :catch_b5

    .line 108
    .end local v8    # "currentAssetJson":Lorg/json/JSONArray;
    .end local v9    # "currentAsset":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v9
    .end local v10    # "innerCount":I
    :cond_b1
    add-int/lit8 v7, v7, 0x1

    goto :goto_88

    .line 124
    .end local v5    # "assetsArray":Lorg/json/JSONArray;
    .end local v6    # "count":I
    .end local v7    # "i":I
    :cond_b4
    goto :goto_c3

    .line 122
    :catch_b5
    move-exception v5

    .line 123
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - No assets found for fullscreen message \"%s\""

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/MessageFullScreen;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 126
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_c3
    const/4 v0, 0x1

    return v0
.end method

.method protected show()V
    .registers 15

    .line 146
    :try_start_0
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCurrentActivity()Landroid/app/Activity;
    :try_end_3
    .catch Lcom/adobe/mobile/StaticMethods$NullActivityException; {:try_start_0 .. :try_end_3} :catch_5

    move-result-object v4

    .line 151
    .local v4, "currentActivity":Landroid/app/Activity;
    goto :goto_11

    .line 148
    .end local v4    # "currentActivity":Landroid/app/Activity;
    :catch_5
    move-exception v5

    .line 149
    .local v5, "ex":Lcom/adobe/mobile/StaticMethods$NullActivityException;
    invoke-virtual {v5}, Lcom/adobe/mobile/StaticMethods$NullActivityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 150
    return-void

    .line 153
    .local v4, "currentActivity":Landroid/app/Activity;
    .end local v5    # "ex":Lcom/adobe/mobile/StaticMethods$NullActivityException;
    :goto_11
    invoke-super {p0}, Lcom/adobe/mobile/Message;->show()V

    .line 156
    invoke-static {p0}, Lcom/adobe/mobile/Messages;->setCurrentMessageFullscreen(Lcom/adobe/mobile/MessageFullScreen;)V

    .line 158
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 161
    .local v5, "imageTokens":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen;->assets:Ljava/util/ArrayList;

    if-eqz v0, :cond_8c

    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen;->assets:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_8c

    .line 162
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen;->assets:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2e
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8c

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/util/ArrayList;

    .line 163
    .local v7, "currentAssetArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 166
    .local v8, "currentAssetArrayCount":I
    if-gtz v8, :cond_42

    .line 167
    goto :goto_2e

    .line 170
    :cond_42
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Ljava/lang/String;

    .line 171
    .local v9, "assetUrl":Ljava/lang/String;
    const/4 v10, 0x0

    .line 173
    .local v10, "assetValue":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_4f
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6e

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Ljava/lang/String;

    .line 175
    .local v12, "currentAsset":Ljava/lang/String;
    const-string v0, "messageImages"

    invoke-static {v12, v0}, Lcom/adobe/mobile/RemoteDownload;->getFileForCachedURL(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v13

    .line 176
    .local v13, "imageFile":Ljava/io/File;
    if-eqz v13, :cond_6d

    .line 177
    invoke-virtual {v13}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v10

    .line 178
    goto :goto_6e

    .line 180
    .end local v12    # "currentAsset":Ljava/lang/String;
    .end local v13    # "imageFile":Ljava/io/File;
    :cond_6d
    goto :goto_4f

    .line 183
    :cond_6e
    :goto_6e
    if-nez v10, :cond_85

    .line 184
    add-int/lit8 v0, v8, -0x1

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Ljava/lang/String;

    .line 185
    .local v11, "lastAsset":Ljava/lang/String;
    invoke-static {v11}, Lcom/adobe/mobile/RemoteDownload;->stringIsUrl(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_81

    const/4 v12, 0x1

    goto :goto_82

    :cond_81
    const/4 v12, 0x0

    .line 187
    .local v12, "isLocalImage":Z
    :goto_82
    if-eqz v12, :cond_85

    .line 188
    move-object v10, v11

    .line 192
    .end local v11    # "lastAsset":Ljava/lang/String;
    .end local v12    # "isLocalImage":Z
    :cond_85
    if-eqz v10, :cond_8a

    .line 193
    invoke-virtual {v5, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    .end local v7    # "currentAssetArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7
    .end local v8    # "currentAssetArrayCount":I
    .end local v9    # "assetUrl":Ljava/lang/String;
    .end local v10    # "assetValue":Ljava/lang/String;
    :cond_8a
    goto/16 :goto_2e

    .line 198
    :cond_8c
    iget-object v0, p0, Lcom/adobe/mobile/MessageFullScreen;->html:Ljava/lang/String;

    invoke-static {v0, v5}, Lcom/adobe/mobile/StaticMethods;->expandTokens(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MessageFullScreen;->replacedHtml:Ljava/lang/String;

    .line 201
    :try_start_94
    new-instance v6, Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/adobe/mobile/MessageFullScreenActivity;

    invoke-direct {v6, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 202
    .local v6, "fullscreen":Landroid/content/Intent;
    const/high16 v0, 0x10000

    invoke-virtual {v6, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 203
    invoke-virtual {v4, v6}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 204
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V
    :try_end_ac
    .catch Landroid/content/ActivityNotFoundException; {:try_start_94 .. :try_end_ac} :catch_ad

    .line 208
    .end local v6    # "fullscreen":Landroid/content/Intent;
    goto :goto_bd

    .line 206
    :catch_ad
    move-exception v6

    .line 207
    .local v6, "ex":Landroid/content/ActivityNotFoundException;
    const-string v0, "Messages - Must declare MessageFullScreenActivity in AndroidManifest.xml in order to show full screen messages (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Landroid/content/ActivityNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 209
    .end local v6    # "ex":Landroid/content/ActivityNotFoundException;
    :goto_bd
    return-void
.end method

.method protected showInRootViewGroup()V
    .registers 4

    .line 130
    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCurrentOrientation()I

    move-result v1

    .line 131
    .local v1, "currentOrientation":I
    iget-boolean v0, p0, Lcom/adobe/mobile/MessageFullScreen;->isVisible:Z

    if-eqz v0, :cond_d

    iget v0, p0, Lcom/adobe/mobile/MessageFullScreen;->orientationWhenShown:I

    if-ne v0, v1, :cond_d

    .line 132
    return-void

    .line 135
    :cond_d
    iput v1, p0, Lcom/adobe/mobile/MessageFullScreen;->orientationWhenShown:I

    .line 138
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 139
    .local v2, "mainHandler":Landroid/os/Handler;
    new-instance v0, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;

    invoke-direct {v0, p0}, Lcom/adobe/mobile/MessageFullScreen$MessageFullScreenRunner;-><init>(Lcom/adobe/mobile/MessageFullScreen;)V

    invoke-virtual {v2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 140
    return-void
.end method

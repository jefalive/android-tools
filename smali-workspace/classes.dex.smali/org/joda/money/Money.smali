.class public final Lorg/joda/money/Money;
.super Ljava/lang/Object;
.source "Money.java"

# interfaces
.implements Lorg/joda/money/BigMoneyProvider;
.implements Ljava/lang/Comparable;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Lorg/joda/money/BigMoneyProvider;Ljava/lang/Comparable<Lorg/joda/money/BigMoneyProvider;>;Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final money:Lorg/joda/money/BigMoney;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 46
    const-class v0, Lorg/joda/money/Money;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    sput-boolean v0, Lorg/joda/money/Money;->$assertionsDisabled:Z

    return-void
.end method

.method constructor <init>(Lorg/joda/money/BigMoney;)V
    .registers 4
    .param p1, "money"    # Lorg/joda/money/BigMoney;

    .line 372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373
    sget-boolean v0, Lorg/joda/money/Money;->$assertionsDisabled:Z

    if-nez v0, :cond_11

    if-nez p1, :cond_11

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Joda-Money bug: BigMoney must not be null"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 374
    :cond_11
    sget-boolean v0, Lorg/joda/money/Money;->$assertionsDisabled:Z

    if-nez v0, :cond_23

    invoke-virtual {p1}, Lorg/joda/money/BigMoney;->isCurrencyScale()Z

    move-result v0

    if-nez v0, :cond_23

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Joda-Money bug: Only currency scale is valid for Money"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 375
    :cond_23
    iput-object p1, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    .line 376
    return-void
.end method

.method public static nonNull(Lorg/joda/money/Money;Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/Money;
    .registers 4
    .param p0, "money"    # Lorg/joda/money/Money;
    .param p1, "currency"    # Lorg/joda/money/CurrencyUnit;

    .line 356
    if-nez p0, :cond_7

    .line 357
    invoke-static {p1}, Lorg/joda/money/Money;->zero(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0

    .line 359
    :cond_7
    invoke-virtual {p0}, Lorg/joda/money/Money;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/joda/money/CurrencyUnit;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_20

    .line 360
    const-string v0, "Currency must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 361
    new-instance v0, Lorg/joda/money/CurrencyMismatchException;

    invoke-virtual {p0}, Lorg/joda/money/Money;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lorg/joda/money/CurrencyMismatchException;-><init>(Lorg/joda/money/CurrencyUnit;Lorg/joda/money/CurrencyUnit;)V

    throw v0

    .line 363
    :cond_20
    return-object p0
.end method

.method public static of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/Money;
    .registers 2
    .param p0, "moneyProvider"    # Lorg/joda/money/BigMoneyProvider;

    .line 212
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-static {p0, v0}, Lorg/joda/money/Money;->of(Lorg/joda/money/BigMoneyProvider;Ljava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public static of(Lorg/joda/money/BigMoneyProvider;Ljava/math/RoundingMode;)Lorg/joda/money/Money;
    .registers 4
    .param p0, "moneyProvider"    # Lorg/joda/money/BigMoneyProvider;
    .param p1, "roundingMode"    # Ljava/math/RoundingMode;

    .line 228
    const-string v0, "BigMoneyProvider must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    const-string v0, "RoundingMode must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 230
    new-instance v0, Lorg/joda/money/Money;

    invoke-static {p0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/joda/money/BigMoney;->withCurrencyScale(Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/joda/money/Money;-><init>(Lorg/joda/money/BigMoney;)V

    return-object v0
.end method

.method public static of(Lorg/joda/money/CurrencyUnit;D)Lorg/joda/money/Money;
    .registers 4
    .param p0, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p1, "amount"    # D

    .line 122
    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/joda/money/Money;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public static of(Lorg/joda/money/CurrencyUnit;DLjava/math/RoundingMode;)Lorg/joda/money/Money;
    .registers 5
    .param p0, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p1, "amount"    # D
    .param p3, "roundingMode"    # Ljava/math/RoundingMode;

    .line 146
    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {p0, v0, p3}, Lorg/joda/money/Money;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public static of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/Money;
    .registers 5
    .param p0, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p1, "amount"    # Ljava/math/BigDecimal;

    .line 72
    const-string v0, "Currency must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    const-string v0, "Amount must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-virtual {p1}, Ljava/math/BigDecimal;->scale()I

    move-result v0

    invoke-virtual {p0}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v1

    if-le v0, v1, :cond_37

    .line 75
    new-instance v0, Ljava/lang/ArithmeticException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Scale of amount "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is greater than the scale of the currency "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArithmeticException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :cond_37
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-static {p0, p1, v0}, Lorg/joda/money/Money;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public static of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;
    .registers 5
    .param p0, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p1, "amount"    # Ljava/math/BigDecimal;
    .param p2, "roundingMode"    # Ljava/math/RoundingMode;

    .line 94
    const-string v0, "CurrencyUnit must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    const-string v0, "Amount must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    const-string v0, "RoundingMode must not be null"

    invoke-static {p2, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    invoke-virtual {p0}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v0

    invoke-virtual {p1, v0, p2}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object p1

    .line 98
    new-instance v0, Lorg/joda/money/Money;

    invoke-static {p0, p1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/joda/money/Money;-><init>(Lorg/joda/money/BigMoney;)V

    return-object v0
.end method

.method public static ofMajor(Lorg/joda/money/CurrencyUnit;J)Lorg/joda/money/Money;
    .registers 5
    .param p0, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p1, "amountMajor"    # J

    .line 163
    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-static {p0, v0, v1}, Lorg/joda/money/Money;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public static ofMinor(Lorg/joda/money/CurrencyUnit;J)Lorg/joda/money/Money;
    .registers 5
    .param p0, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p1, "amountMinor"    # J

    .line 180
    new-instance v0, Lorg/joda/money/Money;

    invoke-static {p0, p1, p2}, Lorg/joda/money/BigMoney;->ofMinor(Lorg/joda/money/CurrencyUnit;J)Lorg/joda/money/BigMoney;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/joda/money/Money;-><init>(Lorg/joda/money/BigMoney;)V

    return-object v0
.end method

.method public static parse(Ljava/lang/String;)Lorg/joda/money/Money;
    .registers 2
    .param p0, "moneyStr"    # Ljava/lang/String;
    .annotation runtime Lorg/joda/convert/FromString;
    .end annotation

    .line 339
    invoke-static {p0}, Lorg/joda/money/BigMoney;->parse(Ljava/lang/String;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-static {v0}, Lorg/joda/money/Money;->of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .registers 4
    .param p1, "ois"    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InvalidObjectException;
        }
    .end annotation

    .line 385
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Serialization delegate required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static total(Ljava/lang/Iterable;)Lorg/joda/money/Money;
    .registers 5
    .param p0, "monies"    # Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/Iterable<Lorg/joda/money/Money;>;)Lorg/joda/money/Money;"
        }
    .end annotation

    .line 272
    const-string v0, "Money iterator must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 273
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 274
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/joda/money/Money;>;"
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_17

    .line 275
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Money iterator must not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 277
    :cond_17
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lorg/joda/money/Money;

    .line 278
    .local v3, "total":Lorg/joda/money/Money;
    const-string v0, "Money iterator must not contain null entries"

    invoke-static {v3, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 279
    :goto_23
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 280
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/joda/money/Money;

    invoke-virtual {v3, v0}, Lorg/joda/money/Money;->plus(Lorg/joda/money/Money;)Lorg/joda/money/Money;

    move-result-object v3

    goto :goto_23

    .line 282
    :cond_34
    return-object v3
.end method

.method public static total(Lorg/joda/money/CurrencyUnit;Ljava/lang/Iterable;)Lorg/joda/money/Money;
    .registers 3
    .param p0, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p1, "monies"    # Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lorg/joda/money/CurrencyUnit;Ljava/lang/Iterable<Lorg/joda/money/Money;>;)Lorg/joda/money/Money;"
        }
    .end annotation

    .line 316
    invoke-static {p0}, Lorg/joda/money/Money;->zero(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/Money;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/joda/money/Money;->plus(Ljava/lang/Iterable;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public static varargs total(Lorg/joda/money/CurrencyUnit;[Lorg/joda/money/Money;)Lorg/joda/money/Money;
    .registers 4
    .param p0, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p1, "monies"    # [Lorg/joda/money/Money;

    .line 299
    invoke-static {p0}, Lorg/joda/money/Money;->zero(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/Money;

    move-result-object v0

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/joda/money/Money;->plus(Ljava/lang/Iterable;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public static varargs total([Lorg/joda/money/Money;)Lorg/joda/money/Money;
    .registers 5
    .param p0, "monies"    # [Lorg/joda/money/Money;

    .line 247
    const-string v0, "Money array must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 248
    array-length v0, p0

    if-nez v0, :cond_10

    .line 249
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Money array must not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 251
    :cond_10
    const/4 v0, 0x0

    aget-object v2, p0, v0

    .line 252
    .local v2, "total":Lorg/joda/money/Money;
    const-string v0, "Money arary must not contain null entries"

    invoke-static {v2, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 253
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_19
    array-length v0, p0

    if-ge v3, v0, :cond_25

    .line 254
    aget-object v0, p0, v3

    invoke-virtual {v2, v0}, Lorg/joda/money/Money;->plus(Lorg/joda/money/Money;)Lorg/joda/money/Money;

    move-result-object v2

    .line 253
    add-int/lit8 v3, v3, 0x1

    goto :goto_19

    .line 256
    .end local v3    # "i":I
    :cond_25
    return-object v2
.end method

.method private with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;
    .registers 3
    .param p1, "newInstance"    # Lorg/joda/money/BigMoney;

    .line 407
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 408
    return-object p0

    .line 410
    :cond_9
    new-instance v0, Lorg/joda/money/Money;

    invoke-direct {v0, p1}, Lorg/joda/money/Money;-><init>(Lorg/joda/money/BigMoney;)V

    return-object v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .registers 3

    .line 394
    new-instance v0, Lorg/joda/money/Ser;

    const/16 v1, 0x4d

    invoke-direct {v0, v1, p0}, Lorg/joda/money/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method

.method public static zero(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/Money;
    .registers 5
    .param p0, "currency"    # Lorg/joda/money/CurrencyUnit;

    .line 193
    const-string v0, "Currency must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 194
    invoke-virtual {p0}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v0

    const-wide/16 v1, 0x0

    invoke-static {v1, v2, v0}, Ljava/math/BigDecimal;->valueOf(JI)Ljava/math/BigDecimal;

    move-result-object v3

    .line 195
    .local v3, "bd":Ljava/math/BigDecimal;
    new-instance v0, Lorg/joda/money/Money;

    invoke-static {p0, v3}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/joda/money/Money;-><init>(Lorg/joda/money/BigMoney;)V

    return-object v0
.end method


# virtual methods
.method public abs()Lorg/joda/money/Money;
    .registers 2

    .line 1155
    invoke-virtual {p0}, Lorg/joda/money/Money;->isNegative()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lorg/joda/money/Money;->negated()Lorg/joda/money/Money;

    move-result-object v0

    goto :goto_c

    :cond_b
    move-object v0, p0

    :goto_c
    return-object v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .param p1, "x0"    # Ljava/lang/Object;

    .line 46
    move-object v0, p1

    check-cast v0, Lorg/joda/money/BigMoneyProvider;

    invoke-virtual {p0, v0}, Lorg/joda/money/Money;->compareTo(Lorg/joda/money/BigMoneyProvider;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/joda/money/BigMoneyProvider;)I
    .registers 3
    .param p1, "other"    # Lorg/joda/money/BigMoneyProvider;

    .line 1241
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->compareTo(Lorg/joda/money/BigMoneyProvider;)I

    move-result v0

    return v0
.end method

.method public convertedTo(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;
    .registers 5
    .param p1, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p2, "conversionMultipler"    # Ljava/math/BigDecimal;
    .param p3, "roundingMode"    # Ljava/math/RoundingMode;

    .line 1201
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->convertedTo(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v0, p3}, Lorg/joda/money/BigMoney;->withCurrencyScale(Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public dividedBy(DLjava/math/RoundingMode;)Lorg/joda/money/Money;
    .registers 5
    .param p1, "valueToDivideBy"    # D
    .param p3, "roundingMode"    # Ljava/math/RoundingMode;

    .line 1114
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2, p3}, Lorg/joda/money/BigMoney;->dividedBy(DLjava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public dividedBy(JLjava/math/RoundingMode;)Lorg/joda/money/Money;
    .registers 5
    .param p1, "valueToDivideBy"    # J
    .param p3, "roundingMode"    # Ljava/math/RoundingMode;

    .line 1132
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2, p3}, Lorg/joda/money/BigMoney;->dividedBy(JLjava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public dividedBy(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;
    .registers 4
    .param p1, "valueToDivideBy"    # Ljava/math/BigDecimal;
    .param p2, "roundingMode"    # Ljava/math/RoundingMode;

    .line 1090
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->dividedBy(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1, "other"    # Ljava/lang/Object;

    .line 1302
    if-ne p0, p1, :cond_4

    .line 1303
    const/4 v0, 0x1

    return v0

    .line 1305
    :cond_4
    instance-of v0, p1, Lorg/joda/money/Money;

    if-eqz v0, :cond_14

    .line 1306
    move-object v2, p1

    check-cast v2, Lorg/joda/money/Money;

    .line 1307
    .local v2, "otherMoney":Lorg/joda/money/Money;
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    iget-object v1, v2, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, v1}, Lorg/joda/money/BigMoney;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 1309
    .end local v2    # "otherMoney":Lorg/joda/money/Money;
    :cond_14
    const/4 v0, 0x0

    return v0
.end method

.method public getAmount()Ljava/math/BigDecimal;
    .registers 2

    .line 487
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public getAmountMajor()Ljava/math/BigDecimal;
    .registers 2

    .line 504
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getAmountMajor()Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public getAmountMajorInt()I
    .registers 2

    .line 532
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getAmountMajorInt()I

    move-result v0

    return v0
.end method

.method public getAmountMajorLong()J
    .registers 3

    .line 518
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getAmountMajorLong()J

    move-result-wide v0

    return-wide v0
.end method

.method public getAmountMinor()Ljava/math/BigDecimal;
    .registers 2

    .line 549
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getAmountMinor()Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public getAmountMinorInt()I
    .registers 2

    .line 577
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getAmountMinorInt()I

    move-result v0

    return v0
.end method

.method public getAmountMinorLong()J
    .registers 3

    .line 563
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getAmountMinorLong()J

    move-result-wide v0

    return-wide v0
.end method

.method public getCurrencyUnit()Lorg/joda/money/CurrencyUnit;
    .registers 2

    .line 420
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    return-object v0
.end method

.method public getMinorPart()I
    .registers 2

    .line 593
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getMinorPart()I

    move-result v0

    return v0
.end method

.method public getScale()I
    .registers 2

    .line 474
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->getScale()I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .registers 3

    .line 1319
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x3

    return v0
.end method

.method public isEqual(Lorg/joda/money/BigMoneyProvider;)Z
    .registers 3
    .param p1, "other"    # Lorg/joda/money/BigMoneyProvider;

    .line 1257
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->isEqual(Lorg/joda/money/BigMoneyProvider;)Z

    move-result v0

    return v0
.end method

.method public isGreaterThan(Lorg/joda/money/BigMoneyProvider;)Z
    .registers 3
    .param p1, "other"    # Lorg/joda/money/BigMoneyProvider;

    .line 1272
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->isGreaterThan(Lorg/joda/money/BigMoneyProvider;)Z

    move-result v0

    return v0
.end method

.method public isLessThan(Lorg/joda/money/BigMoneyProvider;)Z
    .registers 3
    .param p1, "other"    # Lorg/joda/money/BigMoneyProvider;

    .line 1287
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->isLessThan(Lorg/joda/money/BigMoneyProvider;)Z

    move-result v0

    return v0
.end method

.method public isNegative()Z
    .registers 2

    .line 630
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->isNegative()Z

    move-result v0

    return v0
.end method

.method public isNegativeOrZero()Z
    .registers 2

    .line 639
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->isNegativeOrZero()Z

    move-result v0

    return v0
.end method

.method public isPositive()Z
    .registers 2

    .line 612
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->isPositive()Z

    move-result v0

    return v0
.end method

.method public isPositiveOrZero()Z
    .registers 2

    .line 621
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->isPositiveOrZero()Z

    move-result v0

    return v0
.end method

.method public isSameCurrency(Lorg/joda/money/BigMoneyProvider;)Z
    .registers 3
    .param p1, "other"    # Lorg/joda/money/BigMoneyProvider;

    .line 1224
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->isSameCurrency(Lorg/joda/money/BigMoneyProvider;)Z

    move-result v0

    return v0
.end method

.method public isZero()Z
    .registers 2

    .line 603
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->isZero()Z

    move-result v0

    return v0
.end method

.method public minus(D)Lorg/joda/money/Money;
    .registers 4
    .param p1, "amountToSubtract"    # D

    .line 963
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-virtual {p0, p1, p2, v0}, Lorg/joda/money/Money;->minus(DLjava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public minus(DLjava/math/RoundingMode;)Lorg/joda/money/Money;
    .registers 5
    .param p1, "amountToSubtract"    # D
    .param p3, "roundingMode"    # Ljava/math/RoundingMode;

    .line 986
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2, p3}, Lorg/joda/money/BigMoney;->minusRetainScale(DLjava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public minus(Ljava/lang/Iterable;)Lorg/joda/money/Money;
    .registers 3
    .param p1, "moniesToSubtract"    # Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/Iterable<Lorg/joda/money/Money;>;)Lorg/joda/money/Money;"
        }
    .end annotation

    .line 886
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->minus(Ljava/lang/Iterable;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public minus(Ljava/math/BigDecimal;)Lorg/joda/money/Money;
    .registers 3
    .param p1, "amountToSubtract"    # Ljava/math/BigDecimal;

    .line 923
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-virtual {p0, p1, v0}, Lorg/joda/money/Money;->minus(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public minus(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;
    .registers 4
    .param p1, "amountToSubtract"    # Ljava/math/BigDecimal;
    .param p2, "roundingMode"    # Ljava/math/RoundingMode;

    .line 940
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->minusRetainScale(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public minus(Lorg/joda/money/Money;)Lorg/joda/money/Money;
    .registers 3
    .param p1, "moneyToSubtract"    # Lorg/joda/money/Money;

    .line 906
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->minus(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public minusMajor(J)Lorg/joda/money/Money;
    .registers 4
    .param p1, "amountToSubtract"    # J

    .line 1001
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->minusMajor(J)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public minusMinor(J)Lorg/joda/money/Money;
    .registers 4
    .param p1, "amountToSubtract"    # J

    .line 1016
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->minusMinor(J)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public multipliedBy(DLjava/math/RoundingMode;)Lorg/joda/money/Money;
    .registers 5
    .param p1, "valueToMultiplyBy"    # D
    .param p3, "roundingMode"    # Ljava/math/RoundingMode;

    .line 1057
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2, p3}, Lorg/joda/money/BigMoney;->multiplyRetainScale(DLjava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public multipliedBy(J)Lorg/joda/money/Money;
    .registers 4
    .param p1, "valueToMultiplyBy"    # J

    .line 1071
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->multipliedBy(J)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public multipliedBy(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;
    .registers 4
    .param p1, "valueToMultiplyBy"    # Ljava/math/BigDecimal;
    .param p2, "roundingMode"    # Ljava/math/RoundingMode;

    .line 1034
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->multiplyRetainScale(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public negated()Lorg/joda/money/Money;
    .registers 2

    .line 1144
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->negated()Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public plus(D)Lorg/joda/money/Money;
    .registers 4
    .param p1, "amountToAdd"    # D

    .line 816
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-virtual {p0, p1, p2, v0}, Lorg/joda/money/Money;->plus(DLjava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public plus(DLjava/math/RoundingMode;)Lorg/joda/money/Money;
    .registers 5
    .param p1, "amountToAdd"    # D
    .param p3, "roundingMode"    # Ljava/math/RoundingMode;

    .line 839
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2, p3}, Lorg/joda/money/BigMoney;->plusRetainScale(DLjava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public plus(Ljava/lang/Iterable;)Lorg/joda/money/Money;
    .registers 3
    .param p1, "moniesToAdd"    # Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/Iterable<Lorg/joda/money/Money;>;)Lorg/joda/money/Money;"
        }
    .end annotation

    .line 739
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->plus(Ljava/lang/Iterable;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public plus(Ljava/math/BigDecimal;)Lorg/joda/money/Money;
    .registers 3
    .param p1, "amountToAdd"    # Ljava/math/BigDecimal;

    .line 776
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-virtual {p0, p1, v0}, Lorg/joda/money/Money;->plus(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public plus(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;
    .registers 4
    .param p1, "amountToAdd"    # Ljava/math/BigDecimal;
    .param p2, "roundingMode"    # Ljava/math/RoundingMode;

    .line 793
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->plusRetainScale(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public plus(Lorg/joda/money/Money;)Lorg/joda/money/Money;
    .registers 3
    .param p1, "moneyToAdd"    # Lorg/joda/money/Money;

    .line 759
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->plus(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public plusMajor(J)Lorg/joda/money/Money;
    .registers 4
    .param p1, "amountToAdd"    # J

    .line 854
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->plusMajor(J)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public plusMinor(J)Lorg/joda/money/Money;
    .registers 4
    .param p1, "amountToAdd"    # J

    .line 869
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->plusMinor(J)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public rounded(ILjava/math/RoundingMode;)Lorg/joda/money/Money;
    .registers 4
    .param p1, "scale"    # I
    .param p2, "roundingMode"    # Ljava/math/RoundingMode;

    .line 1181
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->rounded(ILjava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public toBigMoney()Lorg/joda/money/BigMoney;
    .registers 2

    .line 1213
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2
    .annotation runtime Lorg/joda/convert/ToString;
    .end annotation

    .line 1334
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Lorg/joda/money/BigMoney;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public withAmount(D)Lorg/joda/money/Money;
    .registers 4
    .param p1, "amount"    # D

    .line 698
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-virtual {p0, p1, p2, v0}, Lorg/joda/money/Money;->withAmount(DLjava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public withAmount(DLjava/math/RoundingMode;)Lorg/joda/money/Money;
    .registers 5
    .param p1, "amount"    # D
    .param p3, "roundingMode"    # Ljava/math/RoundingMode;

    .line 722
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1, p2}, Lorg/joda/money/BigMoney;->withAmount(D)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v0, p3}, Lorg/joda/money/BigMoney;->withCurrencyScale(Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public withAmount(Ljava/math/BigDecimal;)Lorg/joda/money/Money;
    .registers 3
    .param p1, "amount"    # Ljava/math/BigDecimal;

    .line 657
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-virtual {p0, p1, v0}, Lorg/joda/money/Money;->withAmount(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public withAmount(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/Money;
    .registers 4
    .param p1, "amount"    # Ljava/math/BigDecimal;
    .param p2, "roundingMode"    # Ljava/math/RoundingMode;

    .line 674
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->withAmount(Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v0, p2}, Lorg/joda/money/BigMoney;->withCurrencyScale(Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public withCurrencyUnit(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/Money;
    .registers 3
    .param p1, "currency"    # Lorg/joda/money/CurrencyUnit;

    .line 439
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-virtual {p0, p1, v0}, Lorg/joda/money/Money;->withCurrencyUnit(Lorg/joda/money/CurrencyUnit;Ljava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public withCurrencyUnit(Lorg/joda/money/CurrencyUnit;Ljava/math/RoundingMode;)Lorg/joda/money/Money;
    .registers 4
    .param p1, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p2, "roundingMode"    # Ljava/math/RoundingMode;

    .line 457
    iget-object v0, p0, Lorg/joda/money/Money;->money:Lorg/joda/money/BigMoney;

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->withCurrencyUnit(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v0, p2}, Lorg/joda/money/BigMoney;->withCurrencyScale(Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/joda/money/Money;->with(Lorg/joda/money/BigMoney;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

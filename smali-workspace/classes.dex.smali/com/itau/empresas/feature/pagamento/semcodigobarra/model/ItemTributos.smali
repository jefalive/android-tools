.class public Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/ItemTributos;
.super Ljava/lang/Object;
.source "ItemTributos.java"


# instance fields
.field private activity:Ljava/lang/Class;

.field private itemMenu:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Class;)V
    .registers 3
    .param p1, "itemMenu"    # Ljava/lang/String;
    .param p2, "activity"    # Ljava/lang/Class;

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/ItemTributos;->itemMenu:Ljava/lang/String;

    .line 11
    iput-object p2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/ItemTributos;->activity:Ljava/lang/Class;

    .line 12
    return-void
.end method


# virtual methods
.method public getActivity()Ljava/lang/Class;
    .registers 2

    .line 19
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/ItemTributos;->activity:Ljava/lang/Class;

    return-object v0
.end method

.method public getItemMenu()Ljava/lang/String;
    .registers 2

    .line 15
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/ItemTributos;->itemMenu:Ljava/lang/String;

    return-object v0
.end method

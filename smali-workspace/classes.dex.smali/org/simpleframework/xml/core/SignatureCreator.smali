.class Lorg/simpleframework/xml/core/SignatureCreator;
.super Ljava/lang/Object;
.source "SignatureCreator.java"

# interfaces
.implements Lorg/simpleframework/xml/core/Creator;


# instance fields
.field private final list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lorg/simpleframework/xml/core/Parameter;>;"
        }
    .end annotation
.end field

.field private final signature:Lorg/simpleframework/xml/core/Signature;

.field private final type:Ljava/lang/Class;


# direct methods
.method public constructor <init>(Lorg/simpleframework/xml/core/Signature;)V
    .registers 3
    .param p1, "signature"    # Lorg/simpleframework/xml/core/Signature;

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    invoke-virtual {p1}, Lorg/simpleframework/xml/core/Signature;->getType()Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lorg/simpleframework/xml/core/SignatureCreator;->type:Ljava/lang/Class;

    .line 62
    invoke-virtual {p1}, Lorg/simpleframework/xml/core/Signature;->getAll()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/simpleframework/xml/core/SignatureCreator;->list:Ljava/util/List;

    .line 63
    iput-object p1, p0, Lorg/simpleframework/xml/core/SignatureCreator;->signature:Lorg/simpleframework/xml/core/Signature;

    .line 64
    return-void
.end method

.method private getAdjustment(D)D
    .registers 9
    .param p1, "score"    # D

    .line 228
    iget-object v0, p0, Lorg/simpleframework/xml/core/SignatureCreator;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-double v0, v0

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double v4, v0, v2

    .line 230
    .local v4, "adjustment":D
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-lez v0, :cond_1f

    .line 231
    iget-object v0, p0, Lorg/simpleframework/xml/core/SignatureCreator;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-double v0, v0

    div-double v0, p1, v0

    add-double/2addr v0, v4

    return-wide v0

    .line 233
    :cond_1f
    iget-object v0, p0, Lorg/simpleframework/xml/core/SignatureCreator;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    int-to-double v0, v0

    div-double v0, p1, v0

    return-wide v0
.end method

.method private getPercentage(Lorg/simpleframework/xml/core/Criteria;)D
    .registers 10
    .param p1, "criteria"    # Lorg/simpleframework/xml/core/Criteria;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 197
    const-wide/16 v2, 0x0

    .line 199
    .local v2, "score":D
    iget-object v0, p0, Lorg/simpleframework/xml/core/SignatureCreator;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_35

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lorg/simpleframework/xml/core/Parameter;

    .line 200
    .local v5, "value":Lorg/simpleframework/xml/core/Parameter;
    invoke-interface {v5}, Lorg/simpleframework/xml/core/Parameter;->getKey()Ljava/lang/Object;

    move-result-object v6

    .line 201
    .local v6, "key":Ljava/lang/Object;
    invoke-interface {p1, v6}, Lorg/simpleframework/xml/core/Criteria;->get(Ljava/lang/Object;)Lorg/simpleframework/xml/core/Variable;

    move-result-object v7

    .line 203
    .local v7, "label":Lorg/simpleframework/xml/core/Label;
    if-nez v7, :cond_31

    .line 204
    invoke-interface {v5}, Lorg/simpleframework/xml/core/Parameter;->isRequired()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 205
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    return-wide v0

    .line 207
    :cond_28
    invoke-interface {v5}, Lorg/simpleframework/xml/core/Parameter;->isPrimitive()Z

    move-result v0

    if-eqz v0, :cond_34

    .line 208
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    return-wide v0

    .line 211
    :cond_31
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    add-double/2addr v2, v0

    .line 213
    .end local v5    # "value":Lorg/simpleframework/xml/core/Parameter;
    .end local v6    # "key":Ljava/lang/Object;
    .end local v7    # "label":Lorg/simpleframework/xml/core/Label;
    :cond_34
    goto :goto_8

    .line 214
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_35
    invoke-direct {p0, v2, v3}, Lorg/simpleframework/xml/core/SignatureCreator;->getAdjustment(D)D

    move-result-wide v0

    return-wide v0
.end method

.method private getVariable(Lorg/simpleframework/xml/core/Criteria;I)Ljava/lang/Object;
    .registers 7
    .param p1, "criteria"    # Lorg/simpleframework/xml/core/Criteria;
    .param p2, "index"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 130
    iget-object v0, p0, Lorg/simpleframework/xml/core/SignatureCreator;->list:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lorg/simpleframework/xml/core/Parameter;

    .line 131
    .local v1, "parameter":Lorg/simpleframework/xml/core/Parameter;
    invoke-interface {v1}, Lorg/simpleframework/xml/core/Parameter;->getKey()Ljava/lang/Object;

    move-result-object v2

    .line 132
    .local v2, "key":Ljava/lang/Object;
    invoke-interface {p1, v2}, Lorg/simpleframework/xml/core/Criteria;->remove(Ljava/lang/Object;)Lorg/simpleframework/xml/core/Variable;

    move-result-object v3

    .line 134
    .local v3, "variable":Lorg/simpleframework/xml/core/Variable;
    if-eqz v3, :cond_18

    .line 135
    invoke-virtual {v3}, Lorg/simpleframework/xml/core/Variable;->getValue()Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 137
    :cond_18
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public getInstance()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 96
    iget-object v0, p0, Lorg/simpleframework/xml/core/SignatureCreator;->signature:Lorg/simpleframework/xml/core/Signature;

    invoke-virtual {v0}, Lorg/simpleframework/xml/core/Signature;->create()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getInstance(Lorg/simpleframework/xml/core/Criteria;)Ljava/lang/Object;
    .registers 5
    .param p1, "criteria"    # Lorg/simpleframework/xml/core/Criteria;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 110
    iget-object v0, p0, Lorg/simpleframework/xml/core/SignatureCreator;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    .line 112
    .local v1, "values":[Ljava/lang/Object;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_7
    iget-object v0, p0, Lorg/simpleframework/xml/core/SignatureCreator;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_18

    .line 113
    invoke-direct {p0, p1, v2}, Lorg/simpleframework/xml/core/SignatureCreator;->getVariable(Lorg/simpleframework/xml/core/Criteria;I)Ljava/lang/Object;

    move-result-object v0

    aput-object v0, v1, v2

    .line 112
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 115
    .end local v2    # "i":I
    :cond_18
    iget-object v0, p0, Lorg/simpleframework/xml/core/SignatureCreator;->signature:Lorg/simpleframework/xml/core/Signature;

    invoke-virtual {v0, v1}, Lorg/simpleframework/xml/core/Signature;->create([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getScore(Lorg/simpleframework/xml/core/Criteria;)D
    .registers 13
    .param p1, "criteria"    # Lorg/simpleframework/xml/core/Criteria;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 155
    iget-object v0, p0, Lorg/simpleframework/xml/core/SignatureCreator;->signature:Lorg/simpleframework/xml/core/Signature;

    invoke-virtual {v0}, Lorg/simpleframework/xml/core/Signature;->copy()Lorg/simpleframework/xml/core/Signature;

    move-result-object v2

    .line 157
    .local v2, "match":Lorg/simpleframework/xml/core/Signature;
    invoke-interface {p1}, Lorg/simpleframework/xml/core/Criteria;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_43

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 158
    .local v4, "key":Ljava/lang/Object;
    invoke-virtual {v2, v4}, Lorg/simpleframework/xml/core/Signature;->get(Ljava/lang/Object;)Lorg/simpleframework/xml/core/Parameter;

    move-result-object v5

    .line 159
    .local v5, "parameter":Lorg/simpleframework/xml/core/Parameter;
    invoke-interface {p1, v4}, Lorg/simpleframework/xml/core/Criteria;->get(Ljava/lang/Object;)Lorg/simpleframework/xml/core/Variable;

    move-result-object v6

    .line 160
    .local v6, "label":Lorg/simpleframework/xml/core/Variable;
    invoke-virtual {v6}, Lorg/simpleframework/xml/core/Variable;->getContact()Lorg/simpleframework/xml/core/Contact;

    move-result-object v7

    .line 162
    .local v7, "contact":Lorg/simpleframework/xml/core/Contact;
    if-eqz v5, :cond_37

    .line 163
    invoke-virtual {v6}, Lorg/simpleframework/xml/core/Variable;->getValue()Ljava/lang/Object;

    move-result-object v8

    .line 164
    .local v8, "value":Ljava/lang/Object;
    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    .line 165
    .local v9, "expect":Ljava/lang/Class;
    invoke-interface {v5}, Lorg/simpleframework/xml/core/Parameter;->getType()Ljava/lang/Class;

    move-result-object v10

    .line 167
    .local v10, "actual":Ljava/lang/Class;
    invoke-static {v9, v10}, Lorg/simpleframework/xml/core/Support;->isAssignable(Ljava/lang/Class;Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_37

    .line 168
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    return-wide v0

    .line 171
    .end local v8    # "value":Ljava/lang/Object;
    .end local v9    # "expect":Ljava/lang/Class;
    .end local v10    # "actual":Ljava/lang/Class;
    :cond_37
    invoke-interface {v7}, Lorg/simpleframework/xml/core/Contact;->isReadOnly()Z

    move-result v0

    if-eqz v0, :cond_42

    .line 172
    if-nez v5, :cond_42

    .line 173
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    return-wide v0

    .line 176
    .end local v4    # "key":Ljava/lang/Object;
    .end local v5    # "parameter":Lorg/simpleframework/xml/core/Parameter;
    .end local v6    # "label":Lorg/simpleframework/xml/core/Variable;
    .end local v7    # "contact":Lorg/simpleframework/xml/core/Contact;
    :cond_42
    goto :goto_a

    .line 177
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_43
    invoke-direct {p0, p1}, Lorg/simpleframework/xml/core/SignatureCreator;->getPercentage(Lorg/simpleframework/xml/core/Criteria;)D

    move-result-wide v0

    return-wide v0
.end method

.method public getSignature()Lorg/simpleframework/xml/core/Signature;
    .registers 2

    .line 85
    iget-object v0, p0, Lorg/simpleframework/xml/core/SignatureCreator;->signature:Lorg/simpleframework/xml/core/Signature;

    return-object v0
.end method

.method public getType()Ljava/lang/Class;
    .registers 2

    .line 73
    iget-object v0, p0, Lorg/simpleframework/xml/core/SignatureCreator;->type:Ljava/lang/Class;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .line 244
    iget-object v0, p0, Lorg/simpleframework/xml/core/SignatureCreator;->signature:Lorg/simpleframework/xml/core/Signature;

    invoke-virtual {v0}, Lorg/simpleframework/xml/core/Signature;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

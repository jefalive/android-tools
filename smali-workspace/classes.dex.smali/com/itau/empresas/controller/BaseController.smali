.class public Lcom/itau/empresas/controller/BaseController;
.super Ljava/lang/Object;
.source "BaseController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:Ljava/lang/Object;>Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private api:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getGenericClass()Ljava/lang/Class;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/lang/Class<TT;>;"
        }
    .end annotation

    .line 25
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 26
    .local v2, "aClass":Ljava/lang/Class;
    :goto_4
    const-class v0, Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1b

    .line 27
    invoke-virtual {v2}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v3

    .line 28
    .local v3, "superclass":Ljava/lang/Class;
    const-class v0, Lcom/itau/empresas/controller/BaseController;

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 29
    goto :goto_1b

    .line 30
    :cond_19
    move-object v2, v3

    .line 31
    .end local v3    # "superclass":Ljava/lang/Class;
    goto :goto_4

    .line 32
    .line 33
    :cond_1b
    :goto_1b
    invoke-virtual {v2}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    .line 34
    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, Ljava/lang/Class;

    .line 32
    return-object v0
.end method

.method protected static post(Ljava/lang/Object;)V
    .registers 2
    .param p0, "post"    # Ljava/lang/Object;

    .line 14
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    invoke-virtual {v0, p0}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 15
    return-void
.end method


# virtual methods
.method protected api()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/itau/empresas/controller/BaseController;->api:Ljava/lang/Object;

    if-nez v0, :cond_e

    .line 19
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;->getGenericClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->api(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/controller/BaseController;->api:Ljava/lang/Object;

    .line 20
    :cond_e
    iget-object v0, p0, Lcom/itau/empresas/controller/BaseController;->api:Ljava/lang/Object;

    return-object v0
.end method

.class public Lcom/itau/empresas/api/model/DadosTransferenciaVO;
.super Ljava/lang/Object;
.source "DadosTransferenciaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private agenciaDestino:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia_destino"
    .end annotation
.end field

.field private agenciaOrigem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia_origem"
    .end annotation
.end field

.field private codigoBancoDestino:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_banco_destino"
    .end annotation
.end field

.field private complementoContaDestino:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "complemento_conta_destino"
    .end annotation
.end field

.field private complementoContaOrigem:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "complemento_conta_origem"
    .end annotation
.end field

.field private contaDestino:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta_destino"
    .end annotation
.end field

.field private contaOrigem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta_origem"
    .end annotation
.end field

.field private cpfCnpjDestino:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf_cnpj_destino"
    .end annotation
.end field

.field private cpfCnpjOrigem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf_cnpj_origem"
    .end annotation
.end field

.field private dataPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_pagamento"
    .end annotation
.end field

.field private descricaoComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_comprovante"
    .end annotation
.end field

.field private descricaoExtratoDestino:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_extrato_destino"
    .end annotation
.end field

.field private digitoVerificadorContaDestino:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_verificador_conta_destino"
    .end annotation
.end field

.field private digitoVerificadorContaOrigem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_verificador_conta_origem"
    .end annotation
.end field

.field private finalidade:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "finalidade"
    .end annotation
.end field

.field private idSistemaPagamentoBancario:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id_sistema_pagamento_bancario"
    .end annotation
.end field

.field private nomeFavorecido:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_favorecido"
    .end annotation
.end field

.field private recorrenciaMensal:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "recorrencia_mensal"
    .end annotation
.end field

.field private tipoTransferencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_transferencia"
    .end annotation
.end field

.field private valorDebito:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_debito"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

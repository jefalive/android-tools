.class Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$1;
.super Ljava/lang/Object;
.source "ContatoRecargaController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->buscaContatos(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

.field final synthetic val$agencia:Ljava/lang/String;

.field final synthetic val$conta:Ljava/lang/String;

.field final synthetic val$dac:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "this$0"    # Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    .line 18
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$1;->this$0:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    iput-object p2, p0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$1;->val$agencia:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$1;->val$conta:Ljava/lang/String;

    iput-object p4, p0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$1;->val$dac:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 6

    .line 21
    new-instance v4, Lcom/itau/empresas/Evento$EventoBuscaContatosRecarga;

    invoke-direct {v4}, Lcom/itau/empresas/Evento$EventoBuscaContatosRecarga;-><init>()V

    .line 22
    .local v4, "eventoBuscaContatosRecarga":Lcom/itau/empresas/Evento$EventoBuscaContatosRecarga;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$1;->this$0:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    # invokes: Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->api()Ljava/lang/Object;
    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->access$000(Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$1;->val$agencia:Ljava/lang/String;

    iget-object v2, p0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$1;->val$conta:Ljava/lang/String;

    iget-object v3, p0, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController$1;->val$dac:Ljava/lang/String;

    .line 23
    invoke-interface {v0, v1, v2, v3}, Lcom/itau/empresas/api/Api;->buscaContatosRecarga(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 22
    invoke-virtual {v4, v0}, Lcom/itau/empresas/Evento$EventoBuscaContatosRecarga;->setContatosRecarga(Ljava/util/List;)V

    .line 24
    # invokes: Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->post(Ljava/lang/Object;)V
    invoke-static {v4}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;->access$100(Ljava/lang/Object;)V

    .line 25
    return-void
.end method

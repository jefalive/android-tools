.class Lcom/itau/empresas/feature/whatsnew/WhatsNewItemBuilder;
.super Ljava/lang/Object;
.source "WhatsNewItemBuilder.java"


# direct methods
.method constructor <init>()V
    .registers 1

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static criaListaApartirDosAssets(Landroid/content/Context;)Ljava/util/List;
    .registers 6
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;)Ljava/util/List<Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;>;"
        }
    .end annotation

    .line 19
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 21
    .local v3, "lista":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;>;"
    new-instance v4, Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;

    .line 22
    const v0, 0x7f0706ee

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewItemBuilder;->getIdDoAsset(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 23
    const v1, 0x7f0706ef

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 24
    const v2, 0x7f0706ed

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v0, v1, v2}, Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 25
    .local v4, "whatsNewVO":Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 27
    new-instance v4, Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;

    .line 28
    const v0, 0x7f0706f1

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewItemBuilder;->getIdDoAsset(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 29
    const v1, 0x7f0706f2

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 30
    const v2, 0x7f0706f0

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v0, v1, v2}, Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 31
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    new-instance v4, Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;

    .line 34
    const v0, 0x7f0706f4

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewItemBuilder;->getIdDoAsset(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 35
    const v1, 0x7f0706f5

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 36
    const v2, 0x7f0706f3

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v0, v1, v2}, Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 37
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    new-instance v4, Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;

    .line 40
    const v0, 0x7f0706f7

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/itau/empresas/feature/whatsnew/WhatsNewItemBuilder;->getIdDoAsset(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 41
    const v1, 0x7f0706f8

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 42
    const v2, 0x7f0706f6

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v0, v1, v2}, Lcom/itau/empresas/feature/whatsnew/WhatsNewVO;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 44
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46
    return-object v3
.end method

.method private static getIdDoAsset(Landroid/content/Context;Ljava/lang/String;)I
    .registers 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "nomeAsset"    # Ljava/lang/String;

    .line 58
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "drawable"

    .line 59
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 58
    return v0
.end method

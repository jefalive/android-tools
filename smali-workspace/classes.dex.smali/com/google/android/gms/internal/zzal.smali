.class public abstract Lcom/google/android/gms/internal/zzal;
.super Lcom/google/android/gms/internal/zzak;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/zzal$zza;
    }
.end annotation


# static fields
.field private static startTime:J

.field private static zznH:Ljava/lang/reflect/Method;

.field private static zznI:Ljava/lang/reflect/Method;

.field private static zznJ:Ljava/lang/reflect/Method;

.field private static zznK:Ljava/lang/reflect/Method;

.field private static zznL:Ljava/lang/reflect/Method;

.field private static zznM:Ljava/lang/reflect/Method;

.field private static zznN:Ljava/lang/reflect/Method;

.field private static zznO:Ljava/lang/reflect/Method;

.field private static zznP:Ljava/lang/reflect/Method;

.field private static zznQ:Ljava/lang/reflect/Method;

.field private static zznR:Ljava/lang/reflect/Method;

.field private static zznS:Ljava/lang/reflect/Method;

.field private static zznT:Ljava/lang/reflect/Method;

.field private static zznU:Ljava/lang/String;

.field private static zznV:Ljava/lang/String;

.field private static zznW:Ljava/lang/String;

.field private static zznX:Lcom/google/android/gms/internal/zzaq;

.field static zznY:Z

.field protected static zznZ:Lcom/google/android/gms/clearcut/zzb;

.field protected static zzoa:Lcom/google/ads/afma/nano/NanoAdshieldEvent$AdShieldEvent;

.field protected static zzob:I

.field private static zzoc:Ljava/util/Random;

.field private static zzod:Lcom/google/android/gms/common/zzc;

.field private static zzoe:Z

.field protected static zzof:Z

.field protected static zzog:Z

.field protected static zzoh:Z

.field protected static zzoi:Z

.field private static zzoj:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/google/android/gms/internal/zzal;->startTime:J

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/internal/zzal;->zznY:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zznZ:Lcom/google/android/gms/clearcut/zzb;

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zzoc:Ljava/util/Random;

    invoke-static {}, Lcom/google/android/gms/common/zzc;->zzoK()Lcom/google/android/gms/common/zzc;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zzod:Lcom/google/android/gms/common/zzc;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/internal/zzal;->zzof:Z

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/internal/zzal;->zzog:Z

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/internal/zzal;->zzoh:Z

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/internal/zzal;->zzoi:Z

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/internal/zzal;->zzoj:Z

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/zzap;)V
    .registers 5

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/internal/zzak;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/zzap;)V

    new-instance v0, Lcom/google/ads/afma/nano/NanoAdshieldEvent$AdShieldEvent;

    invoke-direct {v0}, Lcom/google/ads/afma/nano/NanoAdshieldEvent$AdShieldEvent;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zzoa:Lcom/google/ads/afma/nano/NanoAdshieldEvent$AdShieldEvent;

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zzoa:Lcom/google/ads/afma/nano/NanoAdshieldEvent$AdShieldEvent;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ads/afma/nano/NanoAdshieldEvent$AdShieldEvent;->appId:Ljava/lang/String;

    return-void
.end method

.method private zzT()V
    .registers 6

    sget-boolean v0, Lcom/google/android/gms/internal/zzal;->zzoi:Z

    if-eqz v0, :cond_18

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznZ:Lcom/google/android/gms/clearcut/zzb;

    if-eqz v0, :cond_18

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznZ:Lcom/google/android/gms/clearcut/zzb;

    sget-object v1, Lcom/google/android/gms/internal/zzal;->zznG:Lcom/google/android/gms/common/api/GoogleApiClient;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v3, 0x64

    invoke-virtual {v0, v1, v3, v4, v2}, Lcom/google/android/gms/clearcut/zzb;->zza(Lcom/google/android/gms/common/api/GoogleApiClient;JLjava/util/concurrent/TimeUnit;)Z

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznG:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->disconnect()V

    :cond_18
    return-void
.end method

.method static zzU()Ljava/lang/String;
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/zzal$zza;
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznU:Ljava/lang/String;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzal$zza;-><init>()V

    throw v0

    :cond_a
    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznU:Ljava/lang/String;

    return-object v0
.end method

.method static zzV()Ljava/lang/Long;
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/zzal$zza;
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznH:Ljava/lang/reflect/Method;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzal$zza;-><init>()V

    throw v0

    :cond_a
    :try_start_a
    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznH:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;
    :try_end_16
    .catch Ljava/lang/IllegalAccessException; {:try_start_a .. :try_end_16} :catch_17
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_a .. :try_end_16} :catch_1e

    return-object v0

    :catch_17
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_1e
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method static zzW()Ljava/lang/String;
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/zzal$zza;
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznJ:Ljava/lang/reflect/Method;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzal$zza;-><init>()V

    throw v0

    :cond_a
    :try_start_a
    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznJ:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_16
    .catch Ljava/lang/IllegalAccessException; {:try_start_a .. :try_end_16} :catch_17
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_a .. :try_end_16} :catch_1e

    return-object v0

    :catch_17
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_1e
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method static zzX()Ljava/lang/Long;
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/zzal$zza;
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznI:Ljava/lang/reflect/Method;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzal$zza;-><init>()V

    throw v0

    :cond_a
    :try_start_a
    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznI:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;
    :try_end_16
    .catch Ljava/lang/IllegalAccessException; {:try_start_a .. :try_end_16} :catch_17
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_a .. :try_end_16} :catch_1e

    return-object v0

    :catch_17
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_1e
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method static zza(Landroid/content/Context;Lcom/google/android/gms/internal/zzap;)Ljava/lang/String;
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/zzal$zza;
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznV:Ljava/lang/String;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznV:Ljava/lang/String;

    return-object v0

    :cond_7
    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznK:Ljava/lang/reflect/Method;

    if-nez v0, :cond_11

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzal$zza;-><init>()V

    throw v0

    :cond_11
    :try_start_11
    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznK:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/nio/ByteBuffer;

    if-nez v3, :cond_29

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzal$zza;-><init>()V

    throw v0

    :cond_29
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/internal/zzap;->zza([BZ)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zznV:Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznV:Ljava/lang/String;
    :try_end_36
    .catch Ljava/lang/IllegalAccessException; {:try_start_11 .. :try_end_36} :catch_37
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_11 .. :try_end_36} :catch_3e

    return-object v0

    :catch_37
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_3e
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method static zza(Landroid/view/MotionEvent;Landroid/util/DisplayMetrics;)Ljava/util/ArrayList;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/view/MotionEvent;Landroid/util/DisplayMetrics;)Ljava/util/ArrayList<Ljava/lang/Long;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/zzal$zza;
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznL:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_6

    if-nez p0, :cond_c

    :cond_6
    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzal$zza;-><init>()V

    throw v0

    :cond_c
    :try_start_c
    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznL:Ljava/lang/reflect/Method;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/util/ArrayList;
    :try_end_1f
    .catch Ljava/lang/IllegalAccessException; {:try_start_c .. :try_end_1f} :catch_20
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_c .. :try_end_1f} :catch_27

    return-object v3

    :catch_20
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_27
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method protected static zza(II)V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-boolean v0, Lcom/google/android/gms/internal/zzal;->zzoi:Z

    if-eqz v0, :cond_23

    sget-boolean v0, Lcom/google/android/gms/internal/zzal;->zzof:Z

    if-eqz v0, :cond_23

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznZ:Lcom/google/android/gms/clearcut/zzb;

    if-eqz v0, :cond_23

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznZ:Lcom/google/android/gms/clearcut/zzb;

    sget-object v1, Lcom/google/android/gms/internal/zzal;->zzoa:Lcom/google/ads/afma/nano/NanoAdshieldEvent$AdShieldEvent;

    invoke-static {v1}, Lcom/google/android/gms/internal/zzsu;->toByteArray(Lcom/google/android/gms/internal/zzsu;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/clearcut/zzb;->zzi([B)Lcom/google/android/gms/clearcut/zzb$zza;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/android/gms/clearcut/zzb$zza;->zzbr(I)Lcom/google/android/gms/clearcut/zzb$zza;

    invoke-virtual {v2, p0}, Lcom/google/android/gms/clearcut/zzb$zza;->zzbq(I)Lcom/google/android/gms/clearcut/zzb$zza;

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznG:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/clearcut/zzb$zza;->zzd(Lcom/google/android/gms/common/api/GoogleApiClient;)Lcom/google/android/gms/common/api/PendingResult;

    :cond_23
    return-void
.end method

.method protected static declared-synchronized zza(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/gms/internal/zzap;)V
    .registers 9

    const-class v5, Lcom/google/android/gms/internal/zzal;

    monitor-enter v5

    :try_start_3
    sget-boolean v0, Lcom/google/android/gms/internal/zzal;->zznY:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_80

    if-nez v0, :cond_7e

    :try_start_7
    new-instance v0, Lcom/google/android/gms/internal/zzaq;

    const/4 v1, 0x0

    invoke-direct {v0, p2, v1}, Lcom/google/android/gms/internal/zzaq;-><init>(Lcom/google/android/gms/internal/zzap;Ljava/security/SecureRandom;)V

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zznX:Lcom/google/android/gms/internal/zzaq;

    sput-object p0, Lcom/google/android/gms/internal/zzal;->zznU:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/gms/internal/zzbt;->initialize(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/google/android/gms/internal/zzal;->zzm(Landroid/content/Context;)V

    invoke-static {}, Lcom/google/android/gms/internal/zzal;->zzV()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/internal/zzal;->startTime:J

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zzoc:Ljava/util/Random;
    :try_end_28
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_7 .. :try_end_28} :catch_7b
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_7 .. :try_end_28} :catch_7d
    .catchall {:try_start_7 .. :try_end_28} :catchall_80

    :try_start_28
    new-instance v0, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/clearcut/zzb;->API:Lcom/google/android/gms/common/api/Api;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->addApi(Lcom/google/android/gms/common/api/Api;)Lcom/google/android/gms/common/api/GoogleApiClient$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient$Builder;->build()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zznG:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-static {}, Lcom/google/android/gms/common/zzc;->zzoK()Lcom/google/android/gms/common/zzc;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zzod:Lcom/google/android/gms/common/zzc;

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zzod:Lcom/google/android/gms/common/zzc;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/zzc;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_49

    const/4 v0, 0x1

    goto :goto_4a

    :cond_49
    const/4 v0, 0x0

    :goto_4a
    sput-boolean v0, Lcom/google/android/gms/internal/zzal;->zzoe:Z

    invoke-static {p1}, Lcom/google/android/gms/internal/zzbt;->initialize(Landroid/content/Context;)V

    sget-object v0, Lcom/google/android/gms/internal/zzbt;->zzwZ:Lcom/google/android/gms/internal/zzbp;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzbp;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gms/internal/zzal;->zzof:Z

    new-instance v0, Lcom/google/android/gms/clearcut/zzb;

    const-string v1, "ADSHIELD"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/gms/clearcut/zzb;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zznZ:Lcom/google/android/gms/clearcut/zzb;
    :try_end_68
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_28 .. :try_end_68} :catch_69
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_28 .. :try_end_68} :catch_7b
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_28 .. :try_end_68} :catch_7d
    .catchall {:try_start_28 .. :try_end_68} :catchall_80

    goto :goto_6a

    :catch_69
    move-exception v4

    :goto_6a
    :try_start_6a
    sget-object v0, Lcom/google/android/gms/internal/zzal;->zzod:Lcom/google/android/gms/common/zzc;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/zzc;->zzaj(Landroid/content/Context;)I

    move-result v0

    if-lez v0, :cond_74

    const/4 v0, 0x1

    goto :goto_75

    :cond_74
    const/4 v0, 0x0

    :goto_75
    sput-boolean v0, Lcom/google/android/gms/internal/zzal;->zzoj:Z

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/gms/internal/zzal;->zznY:Z
    :try_end_7a
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_6a .. :try_end_7a} :catch_7b
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_6a .. :try_end_7a} :catch_7d
    .catchall {:try_start_6a .. :try_end_7a} :catchall_80

    goto :goto_7e

    :catch_7b
    move-exception v4

    goto :goto_7e

    :catch_7d
    move-exception v4

    :cond_7e
    :goto_7e
    monitor-exit v5

    return-void

    :catchall_80
    move-exception p0

    monitor-exit v5

    throw p0
.end method

.method static zzb(Landroid/content/Context;Lcom/google/android/gms/internal/zzap;)Ljava/lang/String;
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/zzal$zza;
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznW:Ljava/lang/String;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznW:Ljava/lang/String;

    return-object v0

    :cond_7
    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznN:Ljava/lang/reflect/Method;

    if-nez v0, :cond_11

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzal$zza;-><init>()V

    throw v0

    :cond_11
    :try_start_11
    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznN:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/nio/ByteBuffer;

    if-nez v3, :cond_29

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzal$zza;-><init>()V

    throw v0

    :cond_29
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/internal/zzap;->zza([BZ)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zznW:Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznW:Ljava/lang/String;
    :try_end_36
    .catch Ljava/lang/IllegalAccessException; {:try_start_11 .. :try_end_36} :catch_37
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_11 .. :try_end_36} :catch_3e

    return-object v0

    :catch_37
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_3e
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private static zzb([BLjava/lang/String;)Ljava/lang/String;
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/zzal$zza;
        }
    .end annotation

    :try_start_0
    new-instance v0, Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/internal/zzal;->zznX:Lcom/google/android/gms/internal/zzaq;

    invoke-virtual {v1, p0, p1}, Lcom/google/android/gms/internal/zzaq;->zzc([BLjava/lang/String;)[B

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_d
    .catch Lcom/google/android/gms/internal/zzaq$zza; {:try_start_0 .. :try_end_d} :catch_e
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_d} :catch_15

    return-object v0

    :catch_e
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_15
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private zze(Landroid/content/Context;)V
    .registers 3

    sget-boolean v0, Lcom/google/android/gms/internal/zzal;->zzoe:Z

    if-eqz v0, :cond_d

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznG:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->connect()V

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/gms/internal/zzal;->zzoi:Z

    goto :goto_10

    :cond_d
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/internal/zzal;->zzoi:Z

    :goto_10
    return-void
.end method

.method static zzf(Landroid/content/Context;)Ljava/lang/String;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/zzal$zza;
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznM:Ljava/lang/reflect/Method;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzal$zza;-><init>()V

    throw v0

    :cond_a
    :try_start_a
    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznM:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/String;

    if-nez v3, :cond_22

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzal$zza;-><init>()V

    throw v0
    :try_end_22
    .catch Ljava/lang/IllegalAccessException; {:try_start_a .. :try_end_22} :catch_23
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_a .. :try_end_22} :catch_2a

    :cond_22
    return-object v3

    :catch_23
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_2a
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method static zzg(Landroid/content/Context;)Ljava/lang/String;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/zzal$zza;
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznQ:Ljava/lang/reflect/Method;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzal$zza;-><init>()V

    throw v0

    :cond_a
    :try_start_a
    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznQ:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/String;
    :try_end_1a
    .catch Ljava/lang/IllegalAccessException; {:try_start_a .. :try_end_1a} :catch_1b
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_a .. :try_end_1a} :catch_22

    return-object v3

    :catch_1b
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_22
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method static zzh(Landroid/content/Context;)Ljava/lang/Long;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/zzal$zza;
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznR:Ljava/lang/reflect/Method;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzal$zza;-><init>()V

    throw v0

    :cond_a
    :try_start_a
    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznR:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/Long;
    :try_end_1a
    .catch Ljava/lang/IllegalAccessException; {:try_start_a .. :try_end_1a} :catch_1b
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_a .. :try_end_1a} :catch_22

    return-object v3

    :catch_1b
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_22
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method static zzi(Landroid/content/Context;)Ljava/util/ArrayList;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;)Ljava/util/ArrayList<Ljava/lang/Long;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/zzal$zza;
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznO:Ljava/lang/reflect/Method;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzal$zza;-><init>()V

    throw v0

    :cond_a
    :try_start_a
    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznO:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/util/ArrayList;

    if-eqz v3, :cond_23

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_29

    :cond_23
    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzal$zza;-><init>()V

    throw v0
    :try_end_29
    .catch Ljava/lang/IllegalAccessException; {:try_start_a .. :try_end_29} :catch_2a
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_a .. :try_end_29} :catch_31

    :cond_29
    return-object v3

    :catch_2a
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_31
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method static zzj(Landroid/content/Context;)[I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/zzal$zza;
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznP:Ljava/lang/reflect/Method;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzal$zza;-><init>()V

    throw v0

    :cond_a
    :try_start_a
    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznP:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I
    :try_end_19
    .catch Ljava/lang/IllegalAccessException; {:try_start_a .. :try_end_19} :catch_1a
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_a .. :try_end_19} :catch_21

    return-object v0

    :catch_1a
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_21
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method static zzk(Landroid/content/Context;)I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/zzal$zza;
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznS:Ljava/lang/reflect/Method;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzal$zza;-><init>()V

    throw v0

    :cond_a
    :try_start_a
    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznS:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_1c
    .catch Ljava/lang/IllegalAccessException; {:try_start_a .. :try_end_1c} :catch_1e
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_a .. :try_end_1c} :catch_25

    move-result v0

    return v0

    :catch_1e
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_25
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method static zzl(Landroid/content/Context;)I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/zzal$zza;
        }
    .end annotation

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznT:Ljava/lang/reflect/Method;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzal$zza;-><init>()V

    throw v0

    :cond_a
    :try_start_a
    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznT:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_1c
    .catch Ljava/lang/IllegalAccessException; {:try_start_a .. :try_end_1c} :catch_1e
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_a .. :try_end_1c} :catch_25

    move-result v0

    return v0

    :catch_1e
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_25
    move-exception v3

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v3}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private static zzm(Landroid/content/Context;)V
    .registers 26
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/zzal$zza;
        }
    .end annotation

    :try_start_0
    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznX:Lcom/google/android/gms/internal/zzaq;

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzaq;->zzl(Ljava/lang/String;)[B

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zznX:Lcom/google/android/gms/internal/zzaq;

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzac()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Lcom/google/android/gms/internal/zzaq;->zzc([BLjava/lang/String;)[B

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v6

    if-nez v6, :cond_2b

    const-string v0, "dex"

    move-object/from16 v1, p0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v6

    if-nez v6, :cond_2b

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0}, Lcom/google/android/gms/internal/zzal$zza;-><init>()V

    throw v0

    :cond_2b
    const-string v0, "ads"

    const-string v1, ".jar"

    invoke-static {v0, v1, v6}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v7

    new-instance v8, Ljava/io/FileOutputStream;

    invoke-direct {v8, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    array-length v0, v5

    const/4 v1, 0x0

    invoke-virtual {v8, v5, v1, v0}, Ljava/io/FileOutputStream;->write([BII)V

    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_40
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_40} :catch_244
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_40} :catch_24b
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_40} :catch_252
    .catch Lcom/google/android/gms/internal/zzaq$zza; {:try_start_0 .. :try_end_40} :catch_259
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_40} :catch_260
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_40} :catch_267

    :try_start_40
    new-instance v9, Ldalvik/system/DexClassLoader;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v9, v0, v1, v3, v2}, Ldalvik/system/DexClassLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzal()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v10

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzaz()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v11

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzat()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v12

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzap()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v13

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzaB()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v14

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzan()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v15

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzax()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v16

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzav()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v17

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzaj()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v18

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzah()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v19

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzaf()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v20

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzar()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v21

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzad()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v22

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzam()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {v10, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zznH:Ljava/lang/reflect/Method;

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzaA()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {v11, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zznI:Ljava/lang/reflect/Method;

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzau()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {v12, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zznJ:Ljava/lang/reflect/Method;

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzaq()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Landroid/content/Context;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v13, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zznK:Ljava/lang/reflect/Method;

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzaC()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Landroid/view/MotionEvent;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-class v2, Landroid/util/DisplayMetrics;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-virtual {v14, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zznL:Ljava/lang/reflect/Method;

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzao()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Landroid/content/Context;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v15, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zznM:Ljava/lang/reflect/Method;

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzay()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Landroid/content/Context;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    move-object/from16 v2, v16

    invoke-virtual {v2, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zznN:Ljava/lang/reflect/Method;

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzaw()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Landroid/content/Context;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    move-object/from16 v2, v17

    invoke-virtual {v2, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zznO:Ljava/lang/reflect/Method;

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzak()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Landroid/content/Context;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    move-object/from16 v2, v18

    invoke-virtual {v2, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zznP:Ljava/lang/reflect/Method;

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzai()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Landroid/content/Context;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    move-object/from16 v2, v19

    invoke-virtual {v2, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zznQ:Ljava/lang/reflect/Method;

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzag()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Landroid/content/Context;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    move-object/from16 v2, v20

    invoke-virtual {v2, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zznR:Ljava/lang/reflect/Method;

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzas()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Landroid/content/Context;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    move-object/from16 v2, v21

    invoke-virtual {v2, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zznS:Ljava/lang/reflect/Method;

    invoke-static {}, Lcom/google/android/gms/internal/zzar;->zzae()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/internal/zzal;->zzb([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Landroid/content/Context;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    move-object/from16 v2, v22

    invoke-virtual {v2, v0, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/zzal;->zznT:Ljava/lang/reflect/Method;
    :try_end_210
    .catchall {:try_start_40 .. :try_end_210} :catchall_228

    :try_start_210
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    new-instance v0, Ljava/io/File;

    const-string v1, ".jar"

    const-string v2, ".dex"

    invoke-virtual {v9, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v6, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_227
    .catch Ljava/io/FileNotFoundException; {:try_start_210 .. :try_end_227} :catch_244
    .catch Ljava/io/IOException; {:try_start_210 .. :try_end_227} :catch_24b
    .catch Ljava/lang/ClassNotFoundException; {:try_start_210 .. :try_end_227} :catch_252
    .catch Lcom/google/android/gms/internal/zzaq$zza; {:try_start_210 .. :try_end_227} :catch_259
    .catch Ljava/lang/NoSuchMethodException; {:try_start_210 .. :try_end_227} :catch_260
    .catch Ljava/lang/NullPointerException; {:try_start_210 .. :try_end_227} :catch_267

    goto :goto_243

    :catchall_228
    move-exception v23

    :try_start_229
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v24

    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    new-instance v0, Ljava/io/File;

    const-string v1, ".jar"

    const-string v2, ".dex"

    move-object/from16 v3, v24

    invoke-virtual {v3, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v6, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    throw v23
    :try_end_243
    .catch Ljava/io/FileNotFoundException; {:try_start_229 .. :try_end_243} :catch_244
    .catch Ljava/io/IOException; {:try_start_229 .. :try_end_243} :catch_24b
    .catch Ljava/lang/ClassNotFoundException; {:try_start_229 .. :try_end_243} :catch_252
    .catch Lcom/google/android/gms/internal/zzaq$zza; {:try_start_229 .. :try_end_243} :catch_259
    .catch Ljava/lang/NoSuchMethodException; {:try_start_229 .. :try_end_243} :catch_260
    .catch Ljava/lang/NullPointerException; {:try_start_229 .. :try_end_243} :catch_267

    :goto_243
    goto :goto_26e

    :catch_244
    move-exception v4

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v4}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_24b
    move-exception v4

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v4}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_252
    move-exception v4

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v4}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_259
    move-exception v4

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v4}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_260
    move-exception v4

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v4}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catch_267
    move-exception v4

    new-instance v0, Lcom/google/android/gms/internal/zzal$zza;

    invoke-direct {v0, v4}, Lcom/google/android/gms/internal/zzal$zza;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :goto_26e
    return-void
.end method


# virtual methods
.method protected zzS()Z
    .registers 2

    sget-boolean v0, Lcom/google/android/gms/internal/zzal;->zzoj:Z

    return v0
.end method

.method protected zzc(Landroid/content/Context;)Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;
    .registers 9

    new-instance v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;

    invoke-direct {v4}, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;-><init>()V

    :try_start_5
    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/zzal;->zze(Landroid/content/Context;)V

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zzoc:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    sput v0, Lcom/google/android/gms/internal/zzal;->zzob:I

    sget v0, Lcom/google/android/gms/internal/zzal;->zzob:I

    const/4 v1, 0x0

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzal;->zza(II)V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_16} :catch_135

    :try_start_16
    invoke-static {}, Lcom/google/android/gms/internal/zzal;->zzW()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->osVersion:Ljava/lang/String;

    sget v0, Lcom/google/android/gms/internal/zzal;->zzob:I

    const/4 v1, 0x1

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzal;->zza(II)V
    :try_end_22
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_16 .. :try_end_22} :catch_23
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_22} :catch_135

    goto :goto_24

    :catch_23
    move-exception v5

    :goto_24
    :try_start_24
    invoke-static {}, Lcom/google/android/gms/internal/zzal;->zzU()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->afmaVersion:Ljava/lang/String;

    sget v0, Lcom/google/android/gms/internal/zzal;->zzob:I

    const/4 v1, 0x2

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzal;->zza(II)V
    :try_end_30
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_24 .. :try_end_30} :catch_31
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_30} :catch_135

    goto :goto_32

    :catch_31
    move-exception v5

    :goto_32
    :try_start_32
    invoke-static {}, Lcom/google/android/gms/internal/zzal;->zzV()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->evtTime:Ljava/lang/Long;

    sget-wide v0, Lcom/google/android/gms/internal/zzal;->startTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5a

    sget-wide v0, Lcom/google/android/gms/internal/zzal;->startTime:J

    sub-long v0, v5, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->uptSignal:Ljava/lang/Long;

    sget-wide v0, Lcom/google/android/gms/internal/zzal;->startTime:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->usgSignal:Ljava/lang/Long;

    :cond_5a
    sget v0, Lcom/google/android/gms/internal/zzal;->zzob:I

    const/16 v1, 0x19

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzal;->zza(II)V
    :try_end_61
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_32 .. :try_end_61} :catch_62
    .catch Ljava/io/IOException; {:try_start_32 .. :try_end_61} :catch_135

    goto :goto_63

    :catch_62
    move-exception v5

    :goto_63
    :try_start_63
    invoke-static {p1}, Lcom/google/android/gms/internal/zzal;->zzi(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v5

    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->uwSignal:Ljava/lang/Long;

    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->uhSignal:Ljava/lang/Long;

    sget v0, Lcom/google/android/gms/internal/zzal;->zzob:I

    const/16 v1, 0x1f

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzal;->zza(II)V
    :try_end_90
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_63 .. :try_end_90} :catch_91
    .catch Ljava/io/IOException; {:try_start_63 .. :try_end_90} :catch_135

    goto :goto_92

    :catch_91
    move-exception v5

    :goto_92
    :try_start_92
    invoke-static {}, Lcom/google/android/gms/internal/zzal;->zzX()Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->utzSignal:Ljava/lang/Long;

    sget v0, Lcom/google/android/gms/internal/zzal;->zzob:I

    const/16 v1, 0x21

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzal;->zza(II)V
    :try_end_9f
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_92 .. :try_end_9f} :catch_a0
    .catch Ljava/io/IOException; {:try_start_92 .. :try_end_9f} :catch_135

    goto :goto_a1

    :catch_a0
    move-exception v5

    :goto_a1
    :try_start_a1
    sget-boolean v0, Lcom/google/android/gms/internal/zzal;->zzog:Z

    if-eqz v0, :cond_aa

    sget-boolean v0, Lcom/google/android/gms/internal/zzal;->zzoh:Z

    if-eqz v0, :cond_aa

    goto :goto_b9

    :cond_aa
    iget-object v0, p0, Lcom/google/android/gms/internal/zzal;->zznF:Lcom/google/android/gms/internal/zzap;

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/zzal;->zza(Landroid/content/Context;Lcom/google/android/gms/internal/zzap;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->intSignal:Ljava/lang/String;

    sget v0, Lcom/google/android/gms/internal/zzal;->zzob:I

    const/16 v1, 0x1b

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzal;->zza(II)V
    :try_end_b9
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_a1 .. :try_end_b9} :catch_ba
    .catch Ljava/io/IOException; {:try_start_a1 .. :try_end_b9} :catch_135

    :goto_b9
    goto :goto_bb

    :catch_ba
    move-exception v5

    :goto_bb
    :try_start_bb
    iget-object v0, p0, Lcom/google/android/gms/internal/zzal;->zznF:Lcom/google/android/gms/internal/zzap;

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/zzal;->zzb(Landroid/content/Context;Lcom/google/android/gms/internal/zzap;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->cerSignal:Ljava/lang/String;

    sget v0, Lcom/google/android/gms/internal/zzal;->zzob:I

    const/16 v1, 0x1d

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzal;->zza(II)V
    :try_end_ca
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_bb .. :try_end_ca} :catch_cb
    .catch Ljava/io/IOException; {:try_start_bb .. :try_end_ca} :catch_135

    goto :goto_cc

    :catch_cb
    move-exception v5

    :goto_cc
    :try_start_cc
    invoke-static {p1}, Lcom/google/android/gms/internal/zzal;->zzj(Landroid/content/Context;)[I

    move-result-object v5

    const/4 v0, 0x0

    aget v0, v5, v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->btsSignal:Ljava/lang/Long;

    const/4 v0, 0x1

    aget v0, v5, v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->btlSignal:Ljava/lang/Long;

    sget v0, Lcom/google/android/gms/internal/zzal;->zzob:I

    const/4 v1, 0x5

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzal;->zza(II)V
    :try_end_ea
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_cc .. :try_end_ea} :catch_eb
    .catch Ljava/io/IOException; {:try_start_cc .. :try_end_ea} :catch_135

    goto :goto_ec

    :catch_eb
    move-exception v5

    :goto_ec
    :try_start_ec
    invoke-static {p1}, Lcom/google/android/gms/internal/zzal;->zzk(Landroid/content/Context;)I

    move-result v0

    int-to-long v5, v0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->ornSignal:Ljava/lang/Long;

    sget v0, Lcom/google/android/gms/internal/zzal;->zzob:I

    const/16 v1, 0xc

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzal;->zza(II)V
    :try_end_fe
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_ec .. :try_end_fe} :catch_ff
    .catch Ljava/io/IOException; {:try_start_ec .. :try_end_fe} :catch_135

    goto :goto_100

    :catch_ff
    move-exception v5

    :goto_100
    :try_start_100
    invoke-static {p1}, Lcom/google/android/gms/internal/zzal;->zzl(Landroid/content/Context;)I

    move-result v0

    int-to-long v5, v0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->atvSignal:Ljava/lang/Long;

    sget v0, Lcom/google/android/gms/internal/zzal;->zzob:I

    const/4 v1, 0x3

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzal;->zza(II)V
    :try_end_111
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_100 .. :try_end_111} :catch_112
    .catch Ljava/io/IOException; {:try_start_100 .. :try_end_111} :catch_135

    goto :goto_113

    :catch_112
    move-exception v5

    :goto_113
    :try_start_113
    invoke-static {p1}, Lcom/google/android/gms/internal/zzal;->zzg(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->vnmSignal:Ljava/lang/String;

    sget v0, Lcom/google/android/gms/internal/zzal;->zzob:I

    const/16 v1, 0x22

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzal;->zza(II)V
    :try_end_120
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_113 .. :try_end_120} :catch_121
    .catch Ljava/io/IOException; {:try_start_113 .. :try_end_120} :catch_135

    goto :goto_122

    :catch_121
    move-exception v5

    :goto_122
    :try_start_122
    invoke-static {p1}, Lcom/google/android/gms/internal/zzal;->zzh(Landroid/content/Context;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->vcdSignal:Ljava/lang/Long;

    sget v0, Lcom/google/android/gms/internal/zzal;->zzob:I

    const/16 v1, 0x23

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzal;->zza(II)V
    :try_end_12f
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_122 .. :try_end_12f} :catch_130
    .catch Ljava/io/IOException; {:try_start_122 .. :try_end_12f} :catch_135

    goto :goto_131

    :catch_130
    move-exception v5

    :goto_131
    :try_start_131
    invoke-direct {p0}, Lcom/google/android/gms/internal/zzal;->zzT()V
    :try_end_134
    .catch Ljava/io/IOException; {:try_start_131 .. :try_end_134} :catch_135

    goto :goto_136

    :catch_135
    move-exception v5

    :goto_136
    return-object v4
.end method

.method protected zzd(Landroid/content/Context;)Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;
    .registers 11

    new-instance v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;

    invoke-direct {v4}, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;-><init>()V

    :try_start_5
    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/zzal;->zze(Landroid/content/Context;)V

    sget-object v0, Lcom/google/android/gms/internal/zzal;->zzoc:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    sput v0, Lcom/google/android/gms/internal/zzal;->zzob:I
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_10} :catch_113

    :try_start_10
    invoke-static {}, Lcom/google/android/gms/internal/zzal;->zzU()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->afmaVersion:Ljava/lang/String;
    :try_end_16
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_10 .. :try_end_16} :catch_17
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_16} :catch_113

    goto :goto_18

    :catch_17
    move-exception v5

    :goto_18
    :try_start_18
    invoke-static {}, Lcom/google/android/gms/internal/zzal;->zzW()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->osVersion:Ljava/lang/String;
    :try_end_1e
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_18 .. :try_end_1e} :catch_1f
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_1e} :catch_113

    goto :goto_20

    :catch_1f
    move-exception v5

    :goto_20
    :try_start_20
    invoke-static {}, Lcom/google/android/gms/internal/zzal;->zzV()Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->evtTime:Ljava/lang/Long;
    :try_end_26
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_20 .. :try_end_26} :catch_27
    .catch Ljava/io/IOException; {:try_start_20 .. :try_end_26} :catch_113

    goto :goto_28

    :catch_27
    move-exception v5

    :goto_28
    :try_start_28
    sget v0, Lcom/google/android/gms/internal/zzal;->zzob:I

    const/4 v1, 0x0

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzal;->zza(II)V
    :try_end_2e
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_2e} :catch_113

    :try_start_2e
    iget-object v0, p0, Lcom/google/android/gms/internal/zzal;->zznx:Landroid/view/MotionEvent;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzal;->zznE:Landroid/util/DisplayMetrics;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzal;->zza(Landroid/view/MotionEvent;Landroid/util/DisplayMetrics;)Ljava/util/ArrayList;

    move-result-object v5

    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->tcxSignal:Ljava/lang/Long;

    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->tcySignal:Ljava/lang/Long;

    const/4 v0, 0x2

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_62

    const/4 v0, 0x2

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->tctSignal:Ljava/lang/Long;

    :cond_62
    const/4 v0, 0x3

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->tcpSignal:Ljava/lang/Long;

    const/4 v0, 0x4

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->tcdSignal:Ljava/lang/Long;

    sget v0, Lcom/google/android/gms/internal/zzal;->zzob:I

    const/16 v1, 0xe

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzal;->zza(II)V
    :try_end_7b
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_2e .. :try_end_7b} :catch_7c
    .catch Ljava/io/IOException; {:try_start_2e .. :try_end_7b} :catch_113

    goto :goto_7d

    :catch_7c
    move-exception v5

    :goto_7d
    :try_start_7d
    iget-wide v0, p0, Lcom/google/android/gms/internal/zzal;->zznz:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_8d

    iget-wide v0, p0, Lcom/google/android/gms/internal/zzal;->zznz:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->tcdnSignal:Ljava/lang/Long;

    :cond_8d
    iget-wide v0, p0, Lcom/google/android/gms/internal/zzal;->zznA:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_9d

    iget-wide v0, p0, Lcom/google/android/gms/internal/zzal;->zznA:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->tcmSignal:Ljava/lang/Long;

    :cond_9d
    iget-wide v0, p0, Lcom/google/android/gms/internal/zzal;->zznB:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_ad

    iget-wide v0, p0, Lcom/google/android/gms/internal/zzal;->zznB:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->tcuSignal:Ljava/lang/Long;

    :cond_ad
    iget-wide v0, p0, Lcom/google/android/gms/internal/zzal;->zznC:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_bd

    iget-wide v0, p0, Lcom/google/android/gms/internal/zzal;->zznC:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->tccSignal:Ljava/lang/Long;
    :try_end_bd
    .catch Ljava/io/IOException; {:try_start_7d .. :try_end_bd} :catch_113

    :cond_bd
    :try_start_bd
    iget-object v0, p0, Lcom/google/android/gms/internal/zzal;->zzny:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    add-int/lit8 v5, v0, -0x1

    if-lez v5, :cond_fa

    new-array v0, v5, [Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals$TouchInfo;

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->previousTouches:[Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals$TouchInfo;

    const/4 v6, 0x0

    :goto_cc
    if-ge v6, v5, :cond_fa

    iget-object v0, p0, Lcom/google/android/gms/internal/zzal;->zzny:Ljava/util/LinkedList;

    invoke-virtual {v0, v6}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MotionEvent;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzal;->zznE:Landroid/util/DisplayMetrics;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzal;->zza(Landroid/view/MotionEvent;Landroid/util/DisplayMetrics;)Ljava/util/ArrayList;

    move-result-object v7

    new-instance v8, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals$TouchInfo;

    invoke-direct {v8}, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals$TouchInfo;-><init>()V

    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iput-object v0, v8, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals$TouchInfo;->tcxSignal:Ljava/lang/Long;

    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iput-object v0, v8, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals$TouchInfo;->tcySignal:Ljava/lang/Long;

    iget-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->previousTouches:[Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals$TouchInfo;

    aput-object v8, v0, v6
    :try_end_f7
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_bd .. :try_end_f7} :catch_fb
    .catch Ljava/io/IOException; {:try_start_bd .. :try_end_f7} :catch_113

    add-int/lit8 v6, v6, 0x1

    goto :goto_cc

    :cond_fa
    goto :goto_ff

    :catch_fb
    move-exception v5

    const/4 v0, 0x0

    :try_start_fd
    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->previousTouches:[Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals$TouchInfo;
    :try_end_ff
    .catch Ljava/io/IOException; {:try_start_fd .. :try_end_ff} :catch_113

    :goto_ff
    :try_start_ff
    invoke-static {p1}, Lcom/google/android/gms/internal/zzal;->zzg(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->vnmSignal:Ljava/lang/String;
    :try_end_105
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_ff .. :try_end_105} :catch_106
    .catch Ljava/io/IOException; {:try_start_ff .. :try_end_105} :catch_113

    goto :goto_107

    :catch_106
    move-exception v5

    :goto_107
    :try_start_107
    invoke-static {p1}, Lcom/google/android/gms/internal/zzal;->zzh(Landroid/content/Context;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ads/afma/nano/NanoAfmaSignals$AFMASignals;->vcdSignal:Ljava/lang/Long;
    :try_end_10d
    .catch Lcom/google/android/gms/internal/zzal$zza; {:try_start_107 .. :try_end_10d} :catch_10e
    .catch Ljava/io/IOException; {:try_start_107 .. :try_end_10d} :catch_113

    goto :goto_10f

    :catch_10e
    move-exception v5

    :goto_10f
    :try_start_10f
    invoke-direct {p0}, Lcom/google/android/gms/internal/zzal;->zzT()V
    :try_end_112
    .catch Ljava/io/IOException; {:try_start_10f .. :try_end_112} :catch_113

    goto :goto_114

    :catch_113
    move-exception v5

    :goto_114
    return-object v4
.end method

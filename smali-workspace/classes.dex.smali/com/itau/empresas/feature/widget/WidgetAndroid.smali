.class public Lcom/itau/empresas/feature/widget/WidgetAndroid;
.super Landroid/appwidget/AppWidgetProvider;
.source "WidgetAndroid.java"


# instance fields
.field private context:Landroid/content/Context;

.field private remoteViews:Landroid/widget/RemoteViews;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 20
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method private escondeToken()V
    .registers 5

    .line 135
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f03018d

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    .line 136
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    const v1, 0x7f0e06aa

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 137
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    const v1, 0x7f0e0426

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 139
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->context:Landroid/content/Context;

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->context:Landroid/content/Context;

    .line 140
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    .line 141
    return-void
.end method

.method private mostraToken()V
    .registers 14

    .line 144
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getOfflineOTP()Lbr/com/itau/security/token/model/OTPModel;

    move-result-object v5

    .line 145
    .local v5, "otpModel":Lbr/com/itau/security/token/model/OTPModel;
    if-eqz v5, :cond_e0

    .line 146
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f03018d

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    .line 147
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    const v1, 0x7f0e06aa

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 148
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    const v1, 0x7f0e0426

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 150
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->context:Landroid/content/Context;

    const-string v1, "WIDGET_TOKEN"

    .line 151
    const v2, 0x8000

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 152
    .local v6, "wToken":Landroid/content/SharedPreferences;
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->context:Landroid/content/Context;

    const v1, 0x7f070206

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v6, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 153
    .local v7, "exibindo":I
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->context:Landroid/content/Context;

    const-string v1, "WIDGET_TOKEN"

    .line 154
    const v2, 0x8000

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    .line 155
    .local v8, "wTokenEditor":Landroid/content/SharedPreferences$Editor;
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->context:Landroid/content/Context;

    const v1, 0x7f070206

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v1, v7, 0x1

    invoke-interface {v8, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 156
    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 158
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    .line 159
    invoke-virtual {v5}, Lbr/com/itau/security/token/model/OTPModel;->timeInterval()J

    move-result-wide v1

    long-to-int v1, v1

    .line 160
    invoke-virtual {v5}, Lbr/com/itau/security/token/model/OTPModel;->timeRemaining()J

    move-result-wide v2

    long-to-int v2, v2

    .line 159
    const v3, 0x7f0e06b0

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v1, v2, v4}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    .line 161
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v5}, Lbr/com/itau/security/token/model/OTPModel;->token()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0e06af

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 162
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v5}, Lbr/com/itau/security/token/model/OTPModel;->serialNumber()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0e06b1

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 164
    new-instance v9, Landroid/content/Intent;

    const-string v0, "WIDGET_UPDATE_ITOKEN"

    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 165
    .local v9, "startIntent":Landroid/content/Intent;
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->context:Landroid/content/Context;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v9, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v10

    .line 166
    .local v10, "startPIntent":Landroid/app/PendingIntent;
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->context:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Landroid/app/AlarmManager;

    .line 167
    .local v11, "alarm":Landroid/app/AlarmManager;
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ne v0, v1, :cond_c4

    .line 169
    const-wide/16 v0, 0x3e8

    :try_start_af
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_b2
    .catch Ljava/lang/InterruptedException; {:try_start_af .. :try_end_b2} :catch_b3

    .line 173
    goto :goto_c4

    .line 170
    :catch_b3
    move-exception v12

    .line 171
    .local v12, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 172
    const-string v0, "WidgetAndroid"

    invoke-virtual {v12}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 175
    .end local v12    # "e":Ljava/lang/InterruptedException;
    :cond_c4
    :goto_c4
    const/4 v0, 0x0

    const-wide/16 v1, 0x3e8

    invoke-virtual {v11, v0, v1, v2, v10}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 177
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->context:Landroid/content/Context;

    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->context:Landroid/content/Context;

    .line 178
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    .line 181
    .end local v6    # "wToken":Landroid/content/SharedPreferences;
    .end local v7    # "exibindo":I
    .end local v8    # "wTokenEditor":Landroid/content/SharedPreferences$Editor;
    .end local v9    # "startIntent":Landroid/content/Intent;
    .end local v10    # "startPIntent":Landroid/app/PendingIntent;
    .end local v11    # "alarm":Landroid/app/AlarmManager;
    :cond_e0
    return-void
.end method

.method private trataExibicaoToken(Landroid/content/Context;)V
    .registers 7
    .param p1, "context"    # Landroid/content/Context;

    .line 116
    const-string v0, "WIDGET_TOKEN"

    .line 117
    const v1, 0x8000

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 118
    .local v2, "wToken":Landroid/content/SharedPreferences;
    const v0, 0x7f070206

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 119
    .local v3, "exibindo":I
    if-eqz v3, :cond_3b

    const/16 v0, 0x1e

    if-ge v3, v0, :cond_3b

    .line 120
    const-string v0, "WIDGET_TOKEN"

    .line 121
    const v1, 0x8000

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 122
    .local v4, "wTokenEditor":Landroid/content/SharedPreferences$Editor;
    const v0, 0x7f070206

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x1f

    invoke-interface {v4, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 123
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 124
    invoke-direct {p0}, Lcom/itau/empresas/feature/widget/WidgetAndroid;->escondeToken()V

    .line 125
    .end local v4    # "wTokenEditor":Landroid/content/SharedPreferences$Editor;
    goto :goto_59

    .line 126
    :cond_3b
    const-string v0, "WIDGET_TOKEN"

    .line 127
    const v1, 0x8000

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 128
    .local v4, "wTokenEditor":Landroid/content/SharedPreferences$Editor;
    const v0, 0x7f070206

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v4, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 129
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 130
    invoke-direct {p0}, Lcom/itau/empresas/feature/widget/WidgetAndroid;->mostraToken()V

    .line 132
    .end local v4    # "wTokenEditor":Landroid/content/SharedPreferences$Editor;
    :goto_59
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .line 86
    iput-object p1, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->context:Landroid/content/Context;

    .line 87
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 88
    const-string v0, "WIDGET_BUTTON_ITOKEN"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    .line 89
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getOfflineOTP()Lbr/com/itau/security/token/model/OTPModel;

    move-result-object v2

    .line 90
    .local v2, "otpModel":Lbr/com/itau/security/token/model/OTPModel;
    if-nez v2, :cond_2e

    .line 91
    new-instance v3, Landroid/content/Intent;

    const-class v0, Lcom/itau/empresas/feature/login/PreLoginActivity_;

    invoke-direct {v3, p1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 92
    .local v3, "i":Landroid/content/Intent;
    const/high16 v0, 0x10000000

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 93
    const-string v0, "WIDGET_ANDROID"

    const-string v1, "WIDGET_ANDROID_ITOKEN"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 94
    invoke-virtual {p1, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 95
    .end local v3    # "i":Landroid/content/Intent;
    goto :goto_31

    .line 96
    :cond_2e
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/widget/WidgetAndroid;->trataExibicaoToken(Landroid/content/Context;)V

    .line 98
    .end local v2    # "otpModel":Lbr/com/itau/security/token/model/OTPModel;
    :goto_31
    goto/16 :goto_9f

    :cond_33
    const-string v0, "WIDGET_UPDATE_ITOKEN"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_60

    .line 99
    const-string v0, "WIDGET_TOKEN"

    .line 100
    const v1, 0x8000

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 101
    .local v2, "wToken":Landroid/content/SharedPreferences;
    const v0, 0x7f070206

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v2, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 102
    .local v3, "exibindo":I
    const/16 v0, 0x1e

    if-ge v3, v0, :cond_5c

    .line 103
    invoke-direct {p0}, Lcom/itau/empresas/feature/widget/WidgetAndroid;->mostraToken()V

    goto :goto_5f

    .line 105
    :cond_5c
    invoke-direct {p0}, Lcom/itau/empresas/feature/widget/WidgetAndroid;->escondeToken()V

    .line 107
    .end local v2    # "wToken":Landroid/content/SharedPreferences;
    .end local v3    # "exibindo":I
    :goto_5f
    goto :goto_9f

    :cond_60
    const-string v0, "WIDGET_BUTTON_EXTRATO"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_90

    const-string v0, "WIDGET_BUTTON_ITAU"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_90

    const-string v0, "WIDGET_BUTTON_PAGTOS"

    .line 108
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_90

    const-string v0, "WIDGET_BUTTON_RECARGA"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9f

    .line 109
    :cond_90
    new-instance v2, Landroid/content/Intent;

    const-class v0, Lcom/itau/empresas/feature/login/PreLoginActivity_;

    invoke-direct {v2, p1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 110
    .local v2, "i":Landroid/content/Intent;
    const/high16 v0, 0x10000000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 111
    invoke-virtual {p1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 113
    .end local v2    # "i":Landroid/content/Intent;
    :cond_9f
    :goto_9f
    return-void
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .registers 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .line 37
    const-string v0, "WIDGET_TOKEN"

    .line 38
    const v1, 0x8000

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 39
    .local v3, "wTokenEditor":Landroid/content/SharedPreferences$Editor;
    const v0, 0x7f070206

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 40
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 42
    iput-object p1, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->context:Landroid/content/Context;

    .line 47
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f03018d

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    .line 48
    new-instance v4, Landroid/content/ComponentName;

    const-class v0, Lcom/itau/empresas/feature/widget/WidgetAndroid;

    invoke-direct {v4, p1, v0}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 50
    .local v4, "watchWidget":Landroid/content/ComponentName;
    new-instance v5, Landroid/content/Intent;

    const-string v0, "WIDGET_BUTTON_ITAU"

    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 51
    .line 52
    .local v5, "intent":Landroid/content/Intent;
    const/4 v0, 0x0

    const/high16 v1, 0x8000000

    invoke-static {p1, v0, v5, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 53
    .local v6, "pendingIntent":Landroid/app/PendingIntent;
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    const v1, 0x7f0e06a9

    invoke-virtual {v0, v1, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {p2, v4, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    .line 56
    new-instance v5, Landroid/content/Intent;

    const-string v0, "WIDGET_BUTTON_EXTRATO"

    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 57
    .line 58
    const/4 v0, 0x0

    const/high16 v1, 0x8000000

    invoke-static {p1, v0, v5, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 59
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    const v1, 0x7f0e06ab

    invoke-virtual {v0, v1, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 60
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {p2, v4, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    .line 62
    new-instance v5, Landroid/content/Intent;

    const-string v0, "WIDGET_BUTTON_RECARGA"

    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 63
    .line 64
    const/4 v0, 0x0

    const/high16 v1, 0x8000000

    invoke-static {p1, v0, v5, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    const v1, 0x7f0e06ac

    invoke-virtual {v0, v1, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {p2, v4, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    .line 68
    new-instance v5, Landroid/content/Intent;

    const-string v0, "WIDGET_BUTTON_PAGTOS"

    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 69
    .line 70
    const/4 v0, 0x0

    const/high16 v1, 0x8000000

    invoke-static {p1, v0, v5, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    const v1, 0x7f0e06ad

    invoke-virtual {v0, v1, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {p2, v4, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    .line 74
    new-instance v5, Landroid/content/Intent;

    const-string v0, "WIDGET_BUTTON_ITOKEN"

    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 75
    .line 76
    const/4 v0, 0x0

    const/high16 v1, 0x8000000

    invoke-static {p1, v0, v5, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    const v1, 0x7f0e06ae

    invoke-virtual {v0, v1, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    const v1, 0x7f0e0426

    invoke-virtual {v0, v1, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/feature/widget/WidgetAndroid;->remoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {p2, v4, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    .line 81
    return-void
.end method

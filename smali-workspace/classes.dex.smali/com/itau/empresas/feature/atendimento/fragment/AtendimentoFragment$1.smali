.class Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment$1;
.super Ljava/lang/Object;
.source "AtendimentoFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->carregaAdapterFaqs(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;

.field final synthetic val$faqs:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;Ljava/util/ArrayList;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;

    .line 182
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment$1;->this$0:Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;

    iput-object p2, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment$1;->val$faqs:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 11
    .param p1, "adapterView"    # Landroid/widget/AdapterView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "l"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/widget/AdapterView<*>;Landroid/view/View;IJ)V"
        }
    .end annotation

    .line 186
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment$1;->this$0:Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;

    iget-object v0, v0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    iget-object v2, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment$1;->this$0:Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;

    .line 187
    const v3, 0x7f0702db

    invoke-virtual {v2, v3}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment$1;->this$0:Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;

    .line 188
    const v4, 0x7f0702ad

    invoke-virtual {v3, v4}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment$1;->val$faqs:Ljava/util/ArrayList;

    .line 189
    invoke-virtual {v4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;

    invoke-virtual {v4}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->getPergunta()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 191
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;

    move-result-object v0

    const-string v1, "FAQ"

    iget-object v2, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment$1;->val$faqs:Ljava/util/ArrayList;

    .line 192
    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/Serializable;

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;->extra(Ljava/lang/String;Ljava/io/Serializable;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 193
    return-void
.end method

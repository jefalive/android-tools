.class Landroid/support/transition/ChangeBoundsPort;
.super Landroid/support/transition/TransitionPort;
.source "ChangeBoundsPort.java"


# static fields
.field private static sRectEvaluator:Landroid/support/transition/RectEvaluator;

.field private static final sTransitionProperties:[Ljava/lang/String;


# instance fields
.field mReparent:Z

.field mResizeClip:Z

.field tempLocation:[I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 50
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android:changeBounds:bounds"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "android:changeBounds:parent"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "android:changeBounds:windowX"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "android:changeBounds:windowY"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Landroid/support/transition/ChangeBoundsPort;->sTransitionProperties:[Ljava/lang/String;

    .line 59
    new-instance v0, Landroid/support/transition/RectEvaluator;

    invoke-direct {v0}, Landroid/support/transition/RectEvaluator;-><init>()V

    sput-object v0, Landroid/support/transition/ChangeBoundsPort;->sRectEvaluator:Landroid/support/transition/RectEvaluator;

    return-void
.end method

.method constructor <init>()V
    .registers 2

    .line 40
    invoke-direct {p0}, Landroid/support/transition/TransitionPort;-><init>()V

    .line 61
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Landroid/support/transition/ChangeBoundsPort;->tempLocation:[I

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/transition/ChangeBoundsPort;->mResizeClip:Z

    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/transition/ChangeBoundsPort;->mReparent:Z

    return-void
.end method

.method private captureValues(Landroid/support/transition/TransitionValues;)V
    .registers 10
    .param p1, "values"    # Landroid/support/transition/TransitionValues;

    .line 91
    iget-object v7, p1, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    .line 92
    .local v7, "view":Landroid/view/View;
    iget-object v0, p1, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:changeBounds:bounds"

    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v4

    .line 93
    invoke-virtual {v7}, Landroid/view/View;->getRight()I

    move-result v5

    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result v6

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 92
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    iget-object v0, p1, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:changeBounds:parent"

    iget-object v2, p1, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    iget-object v0, p1, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    iget-object v1, p0, Landroid/support/transition/ChangeBoundsPort;->tempLocation:[I

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationInWindow([I)V

    .line 96
    iget-object v0, p1, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:changeBounds:windowX"

    iget-object v2, p0, Landroid/support/transition/ChangeBoundsPort;->tempLocation:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    iget-object v0, p1, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:changeBounds:windowY"

    iget-object v2, p0, Landroid/support/transition/ChangeBoundsPort;->tempLocation:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    return-void
.end method


# virtual methods
.method public captureEndValues(Landroid/support/transition/TransitionValues;)V
    .registers 2
    .param p1, "transitionValues"    # Landroid/support/transition/TransitionValues;

    .line 107
    invoke-direct {p0, p1}, Landroid/support/transition/ChangeBoundsPort;->captureValues(Landroid/support/transition/TransitionValues;)V

    .line 108
    return-void
.end method

.method public captureStartValues(Landroid/support/transition/TransitionValues;)V
    .registers 2
    .param p1, "transitionValues"    # Landroid/support/transition/TransitionValues;

    .line 102
    invoke-direct {p0, p1}, Landroid/support/transition/ChangeBoundsPort;->captureValues(Landroid/support/transition/TransitionValues;)V

    .line 103
    return-void
.end method

.method public createAnimator(Landroid/view/ViewGroup;Landroid/support/transition/TransitionValues;Landroid/support/transition/TransitionValues;)Landroid/animation/Animator;
    .registers 39
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/support/transition/TransitionValues;
    .param p3, "endValues"    # Landroid/support/transition/TransitionValues;

    .line 113
    if-eqz p2, :cond_4

    if-nez p3, :cond_6

    .line 114
    :cond_4
    const/4 v0, 0x0

    return-object v0

    .line 116
    :cond_6
    move-object/from16 v0, p2

    iget-object v5, v0, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    .line 117
    .local v5, "startParentVals":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    move-object/from16 v0, p3

    iget-object v6, v0, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    .line 118
    .local v6, "endParentVals":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "android:changeBounds:parent"

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/view/ViewGroup;

    .line 119
    .local v7, "startParent":Landroid/view/ViewGroup;
    const-string v0, "android:changeBounds:parent"

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/view/ViewGroup;

    .line 120
    .local v8, "endParent":Landroid/view/ViewGroup;
    if-eqz v7, :cond_24

    if-nez v8, :cond_26

    .line 121
    :cond_24
    const/4 v0, 0x0

    return-object v0

    .line 123
    :cond_26
    move-object/from16 v0, p3

    iget-object v9, v0, Landroid/support/transition/TransitionValues;->view:Landroid/view/View;

    .line 124
    .local v9, "view":Landroid/view/View;
    if-eq v7, v8, :cond_36

    .line 125
    invoke-virtual {v7}, Landroid/view/ViewGroup;->getId()I

    move-result v0

    invoke-virtual {v8}, Landroid/view/ViewGroup;->getId()I

    move-result v1

    if-ne v0, v1, :cond_38

    :cond_36
    const/4 v10, 0x1

    goto :goto_39

    :cond_38
    const/4 v10, 0x0

    .line 130
    .local v10, "parentsEqual":Z
    :goto_39
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/support/transition/ChangeBoundsPort;->mReparent:Z

    if-eqz v0, :cond_41

    if-eqz v10, :cond_256

    .line 131
    :cond_41
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:changeBounds:bounds"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Landroid/graphics/Rect;

    .line 132
    .local v11, "startBounds":Landroid/graphics/Rect;
    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:changeBounds:bounds"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Landroid/graphics/Rect;

    .line 133
    .local v12, "endBounds":Landroid/graphics/Rect;
    iget v13, v11, Landroid/graphics/Rect;->left:I

    .line 134
    .local v13, "startLeft":I
    iget v14, v12, Landroid/graphics/Rect;->left:I

    .line 135
    .local v14, "endLeft":I
    iget v15, v11, Landroid/graphics/Rect;->top:I

    .line 136
    .local v15, "startTop":I
    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v16, v0

    .line 137
    .local v16, "endTop":I
    iget v0, v11, Landroid/graphics/Rect;->right:I

    move/from16 v17, v0

    .line 138
    .local v17, "startRight":I
    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v18, v0

    .line 139
    .local v18, "endRight":I
    iget v0, v11, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    .line 140
    .local v19, "startBottom":I
    iget v0, v12, Landroid/graphics/Rect;->bottom:I

    move/from16 v20, v0

    .line 141
    .local v20, "endBottom":I
    sub-int v21, v17, v13

    .line 142
    .local v21, "startWidth":I
    sub-int v22, v19, v15

    .line 143
    .local v22, "startHeight":I
    sub-int v23, v18, v14

    .line 144
    .local v23, "endWidth":I
    sub-int v24, v20, v16

    .line 145
    .local v24, "endHeight":I
    const/16 v25, 0x0

    .line 146
    .local v25, "numChanges":I
    if-eqz v21, :cond_a1

    if-eqz v22, :cond_a1

    if-eqz v23, :cond_a1

    if-eqz v24, :cond_a1

    .line 147
    if-eq v13, v14, :cond_8b

    .line 148
    add-int/lit8 v25, v25, 0x1

    .line 150
    :cond_8b
    move/from16 v0, v16

    if-eq v15, v0, :cond_91

    .line 151
    add-int/lit8 v25, v25, 0x1

    .line 153
    :cond_91
    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_99

    .line 154
    add-int/lit8 v25, v25, 0x1

    .line 156
    :cond_99
    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_a1

    .line 157
    add-int/lit8 v25, v25, 0x1

    .line 160
    :cond_a1
    if-lez v25, :cond_254

    .line 161
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/support/transition/ChangeBoundsPort;->mResizeClip:Z

    if-nez v0, :cond_161

    .line 162
    move/from16 v0, v25

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v26, v0

    .line 163
    .local v26, "pvh":[Landroid/animation/PropertyValuesHolder;
    const/16 v27, 0x0

    .line 164
    .local v27, "pvhIndex":I
    if-eq v13, v14, :cond_b6

    .line 165
    invoke-virtual {v9, v13}, Landroid/view/View;->setLeft(I)V

    .line 167
    :cond_b6
    move/from16 v0, v16

    if-eq v15, v0, :cond_bd

    .line 168
    invoke-virtual {v9, v15}, Landroid/view/View;->setTop(I)V

    .line 170
    :cond_bd
    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_c8

    .line 171
    move/from16 v0, v17

    invoke-virtual {v9, v0}, Landroid/view/View;->setRight(I)V

    .line 173
    :cond_c8
    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_d3

    .line 174
    move/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/view/View;->setBottom(I)V

    .line 176
    :cond_d3
    if-eq v13, v14, :cond_eb

    .line 177
    move/from16 v0, v27

    add-int/lit8 v27, v27, 0x1

    const-string v1, "left"

    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput v13, v2, v3

    const/4 v3, 0x1

    aput v14, v2, v3

    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    const/4 v0, 0x0

    aput-object v1, v26, v0

    .line 179
    :cond_eb
    move/from16 v0, v16

    if-eq v15, v0, :cond_104

    .line 180
    move/from16 v0, v27

    add-int/lit8 v27, v27, 0x1

    const-string v1, "top"

    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput v15, v2, v3

    const/4 v3, 0x1

    aput v16, v2, v3

    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    aput-object v1, v26, v0

    .line 182
    :cond_104
    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_11f

    .line 183
    move/from16 v0, v27

    add-int/lit8 v27, v27, 0x1

    const-string v1, "right"

    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput v17, v2, v3

    const/4 v3, 0x1

    aput v18, v2, v3

    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    aput-object v1, v26, v0

    .line 186
    :cond_11f
    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_13a

    .line 187
    move/from16 v0, v27

    add-int/lit8 v27, v27, 0x1

    const-string v1, "bottom"

    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput v19, v2, v3

    const/4 v3, 0x1

    aput v20, v2, v3

    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    aput-object v1, v26, v0

    .line 190
    :cond_13a
    move-object/from16 v0, v26

    invoke-static {v9, v0}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v28

    .line 191
    .local v28, "anim":Landroid/animation/ObjectAnimator;
    invoke-virtual {v9}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_160

    .line 192
    invoke-virtual {v9}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    move-object/from16 v29, v0

    check-cast v29, Landroid/view/ViewGroup;

    .line 194
    .local v29, "parent":Landroid/view/ViewGroup;
    new-instance v30, Landroid/support/transition/ChangeBoundsPort$1;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/support/transition/ChangeBoundsPort$1;-><init>(Landroid/support/transition/ChangeBoundsPort;)V

    .line 220
    .local v30, "transitionListener":Landroid/support/transition/TransitionPort$TransitionListener;
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/support/transition/ChangeBoundsPort;->addListener(Landroid/support/transition/TransitionPort$TransitionListener;)Landroid/support/transition/TransitionPort;

    .line 222
    .end local v29    # "parent":Landroid/view/ViewGroup;
    .end local v30    # "transitionListener":Landroid/support/transition/TransitionPort$TransitionListener;
    :cond_160
    return-object v28

    .line 224
    .end local v26    # "pvh":[Landroid/animation/PropertyValuesHolder;
    .end local v27    # "pvhIndex":I
    .end local v28    # "anim":Landroid/animation/ObjectAnimator;
    :cond_161
    move/from16 v0, v21

    move/from16 v1, v23

    if-eq v0, v1, :cond_173

    .line 225
    .line 226
    move/from16 v0, v21

    move/from16 v1, v23

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, v14

    .line 225
    invoke-virtual {v9, v0}, Landroid/view/View;->setRight(I)V

    .line 228
    :cond_173
    move/from16 v0, v22

    move/from16 v1, v24

    if-eq v0, v1, :cond_186

    .line 229
    .line 230
    move/from16 v0, v22

    move/from16 v1, v24

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int v0, v0, v16

    .line 229
    invoke-virtual {v9, v0}, Landroid/view/View;->setBottom(I)V

    .line 233
    :cond_186
    if-eq v13, v14, :cond_18e

    .line 234
    sub-int v0, v13, v14

    int-to-float v0, v0

    invoke-virtual {v9, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 236
    :cond_18e
    move/from16 v0, v16

    if-eq v15, v0, :cond_198

    .line 237
    sub-int v0, v15, v16

    int-to-float v0, v0

    invoke-virtual {v9, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 240
    :cond_198
    sub-int v0, v14, v13

    int-to-float v1, v0

    move/from16 v26, v1

    .line 241
    .local v26, "transXDelta":F
    sub-int v0, v16, v15

    int-to-float v1, v0

    move/from16 v27, v1

    .line 242
    .local v27, "transYDelta":F
    sub-int v28, v23, v21

    .line 243
    .local v28, "widthDelta":I
    sub-int v29, v24, v22

    .line 244
    .local v29, "heightDelta":I
    const/16 v25, 0x0

    .line 245
    const/4 v0, 0x0

    cmpl-float v0, v26, v0

    if-eqz v0, :cond_1af

    .line 246
    add-int/lit8 v25, v25, 0x1

    .line 248
    :cond_1af
    const/4 v0, 0x0

    cmpl-float v0, v27, v0

    if-eqz v0, :cond_1b6

    .line 249
    add-int/lit8 v25, v25, 0x1

    .line 251
    :cond_1b6
    if-nez v28, :cond_1ba

    if-eqz v29, :cond_1bc

    .line 252
    :cond_1ba
    add-int/lit8 v25, v25, 0x1

    .line 254
    :cond_1bc
    move/from16 v0, v25

    new-array v0, v0, [Landroid/animation/PropertyValuesHolder;

    move-object/from16 v30, v0

    .line 255
    .local v30, "pvh":[Landroid/animation/PropertyValuesHolder;
    const/16 v31, 0x0

    .line 256
    .local v31, "pvhIndex":I
    const/4 v0, 0x0

    cmpl-float v0, v26, v0

    if-eqz v0, :cond_1e4

    .line 257
    move/from16 v0, v31

    add-int/lit8 v31, v31, 0x1

    const-string v1, "translationX"

    const/4 v2, 0x2

    new-array v2, v2, [F

    .line 258
    invoke-virtual {v9}, Landroid/view/View;->getTranslationX()F

    move-result v3

    const/4 v4, 0x0

    aput v3, v2, v4

    const/4 v3, 0x0

    const/4 v4, 0x1

    aput v3, v2, v4

    .line 257
    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    const/4 v0, 0x0

    aput-object v1, v30, v0

    .line 260
    :cond_1e4
    const/4 v0, 0x0

    cmpl-float v0, v27, v0

    if-eqz v0, :cond_203

    .line 261
    move/from16 v0, v31

    add-int/lit8 v31, v31, 0x1

    const-string v1, "translationY"

    const/4 v2, 0x2

    new-array v2, v2, [F

    .line 262
    invoke-virtual {v9}, Landroid/view/View;->getTranslationY()F

    move-result v3

    const/4 v4, 0x0

    aput v3, v2, v4

    const/4 v3, 0x0

    const/4 v4, 0x1

    aput v3, v2, v4

    .line 261
    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofFloat(Ljava/lang/String;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    aput-object v1, v30, v0

    .line 264
    :cond_203
    if-nez v28, :cond_207

    if-eqz v29, :cond_221

    .line 265
    :cond_207
    new-instance v32, Landroid/graphics/Rect;

    move-object/from16 v0, v32

    const/4 v1, 0x0

    const/4 v2, 0x0

    move/from16 v3, v21

    move/from16 v4, v22

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 266
    .local v32, "tempStartBounds":Landroid/graphics/Rect;
    new-instance v33, Landroid/graphics/Rect;

    move-object/from16 v0, v33

    const/4 v1, 0x0

    const/4 v2, 0x0

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 270
    .end local v32    # "tempStartBounds":Landroid/graphics/Rect;
    :cond_221
    move-object/from16 v0, v30

    invoke-static {v9, v0}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v32

    .line 271
    .local v32, "anim":Landroid/animation/ObjectAnimator;
    invoke-virtual {v9}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_247

    .line 272
    invoke-virtual {v9}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    move-object/from16 v33, v0

    check-cast v33, Landroid/view/ViewGroup;

    .line 274
    .local v33, "parent":Landroid/view/ViewGroup;
    new-instance v34, Landroid/support/transition/ChangeBoundsPort$2;

    move-object/from16 v0, v34

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/support/transition/ChangeBoundsPort$2;-><init>(Landroid/support/transition/ChangeBoundsPort;)V

    .line 300
    .local v34, "transitionListener":Landroid/support/transition/TransitionPort$TransitionListener;
    move-object/from16 v0, p0

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/support/transition/ChangeBoundsPort;->addListener(Landroid/support/transition/TransitionPort$TransitionListener;)Landroid/support/transition/TransitionPort;

    .line 302
    .end local v33    # "parent":Landroid/view/ViewGroup;
    .end local v34    # "transitionListener":Landroid/support/transition/TransitionPort$TransitionListener;
    :cond_247
    new-instance v0, Landroid/support/transition/ChangeBoundsPort$3;

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/support/transition/ChangeBoundsPort$3;-><init>(Landroid/support/transition/ChangeBoundsPort;)V

    move-object/from16 v1, v32

    invoke-virtual {v1, v0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 308
    return-object v32

    .line 311
    .end local v11    # "startBounds":Landroid/graphics/Rect;
    .end local v12    # "endBounds":Landroid/graphics/Rect;
    .end local v13    # "startLeft":I
    .end local v14    # "endLeft":I
    .end local v15    # "startTop":I
    .end local v16    # "endTop":I
    .end local v17    # "startRight":I
    .end local v18    # "endRight":I
    .end local v19    # "startBottom":I
    .end local v20    # "endBottom":I
    .end local v21    # "startWidth":I
    .end local v22    # "startHeight":I
    .end local v23    # "endWidth":I
    .end local v24    # "endHeight":I
    .end local v25    # "numChanges":I
    .end local v26    # "transXDelta":F
    .end local v27    # "transYDelta":F
    .end local v28    # "widthDelta":I
    .end local v29    # "heightDelta":I
    .end local v30    # "pvh":[Landroid/animation/PropertyValuesHolder;
    .end local v31    # "pvhIndex":I
    .end local v32    # "anim":Landroid/animation/ObjectAnimator;
    :cond_254
    goto/16 :goto_35f

    .line 312
    :cond_256
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:changeBounds:windowX"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 313
    .local v11, "startX":I
    move-object/from16 v0, p2

    iget-object v0, v0, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:changeBounds:windowY"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 314
    .local v12, "startY":I
    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:changeBounds:windowX"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v13

    .line 315
    .local v13, "endX":I
    move-object/from16 v0, p3

    iget-object v0, v0, Landroid/support/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v1, "android:changeBounds:windowY"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v14

    .line 317
    .local v14, "endY":I
    if-ne v11, v13, :cond_29a

    if-eq v12, v14, :cond_35f

    .line 318
    :cond_29a
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/transition/ChangeBoundsPort;->tempLocation:[I

    move-object/from16 v1, p1

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getLocationInWindow([I)V

    .line 319
    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 321
    .local v15, "bitmap":Landroid/graphics/Bitmap;
    new-instance v16, Landroid/graphics/Canvas;

    move-object/from16 v0, v16

    invoke-direct {v0, v15}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 322
    .local v16, "canvas":Landroid/graphics/Canvas;
    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 323
    new-instance v17, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, v17

    invoke-direct {v0, v15}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    .line 324
    .local v17, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    const/4 v0, 0x4

    invoke-virtual {v9, v0}, Landroid/view/View;->setVisibility(I)V

    .line 325
    invoke-static/range {p1 .. p1}, Landroid/support/transition/ViewOverlay;->createFrom(Landroid/view/View;)Landroid/support/transition/ViewOverlay;

    move-result-object v0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/support/transition/ViewOverlay;->add(Landroid/graphics/drawable/Drawable;)V

    .line 327
    new-instance v18, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/transition/ChangeBoundsPort;->tempLocation:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    sub-int v0, v11, v0

    move-object/from16 v1, p0

    iget-object v1, v1, Landroid/support/transition/ChangeBoundsPort;->tempLocation:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    sub-int v1, v12, v1

    move-object/from16 v2, p0

    iget-object v2, v2, Landroid/support/transition/ChangeBoundsPort;->tempLocation:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    sub-int v2, v11, v2

    .line 328
    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    move-object/from16 v3, p0

    iget-object v3, v3, Landroid/support/transition/ChangeBoundsPort;->tempLocation:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    sub-int v3, v12, v3

    .line 329
    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    move-object/from16 v4, v18

    invoke-direct {v4, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 330
    .local v18, "startBounds1":Landroid/graphics/Rect;
    new-instance v19, Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/transition/ChangeBoundsPort;->tempLocation:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    sub-int v0, v13, v0

    move-object/from16 v1, p0

    iget-object v1, v1, Landroid/support/transition/ChangeBoundsPort;->tempLocation:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    sub-int v1, v14, v1

    move-object/from16 v2, p0

    iget-object v2, v2, Landroid/support/transition/ChangeBoundsPort;->tempLocation:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    sub-int v2, v13, v2

    .line 331
    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    move-object/from16 v3, p0

    iget-object v3, v3, Landroid/support/transition/ChangeBoundsPort;->tempLocation:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    sub-int v3, v14, v3

    .line 332
    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    move-object/from16 v4, v19

    invoke-direct {v4, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 333
    .local v19, "endBounds1":Landroid/graphics/Rect;
    const-string v0, "bounds"

    sget-object v1, Landroid/support/transition/ChangeBoundsPort;->sRectEvaluator:Landroid/support/transition/RectEvaluator;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v18, v2, v3

    const/4 v3, 0x1

    aput-object v19, v2, v3

    move-object/from16 v3, v17

    invoke-static {v3, v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v20

    .line 335
    .local v20, "anim":Landroid/animation/ObjectAnimator;
    new-instance v0, Landroid/support/transition/ChangeBoundsPort$4;

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, v17

    invoke-direct {v0, v1, v2, v3, v9}, Landroid/support/transition/ChangeBoundsPort$4;-><init>(Landroid/support/transition/ChangeBoundsPort;Landroid/view/ViewGroup;Landroid/graphics/drawable/BitmapDrawable;Landroid/view/View;)V

    move-object/from16 v1, v20

    invoke-virtual {v1, v0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 343
    return-object v20

    .line 346
    .end local v11    # "startX":I
    .end local v12    # "startY":I
    .end local v13    # "endX":I
    .end local v14    # "endY":I
    .end local v15    # "bitmap":Landroid/graphics/Bitmap;
    .end local v16    # "canvas":Landroid/graphics/Canvas;
    .end local v17    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    .end local v18    # "startBounds1":Landroid/graphics/Rect;
    .end local v19    # "endBounds1":Landroid/graphics/Rect;
    .end local v20    # "anim":Landroid/animation/ObjectAnimator;
    :cond_35f
    :goto_35f
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTransitionProperties()[Ljava/lang/String;
    .registers 2

    .line 69
    sget-object v0, Landroid/support/transition/ChangeBoundsPort;->sTransitionProperties:[Ljava/lang/String;

    return-object v0
.end method

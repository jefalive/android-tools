.class public Lcom/itau/empresas/ui/util/menu/ExtratoCommand;
.super Ljava/lang/Object;
.source "ExtratoCommand.java"

# interfaces
.implements Lcom/itau/empresas/ui/util/menu/Command;


# instance fields
.field private classActivity:Ljava/lang/Class;


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .registers 2
    .param p1, "classActivity"    # Ljava/lang/Class;

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/itau/empresas/ui/util/menu/ExtratoCommand;->classActivity:Ljava/lang/Class;

    .line 15
    return-void
.end method


# virtual methods
.method public executar(Landroid/support/v7/app/AppCompatActivity;Lcom/itau/empresas/api/model/MenuVO;)V
    .registers 5
    .param p1, "activity"    # Landroid/support/v7/app/AppCompatActivity;
    .param p2, "menuVO"    # Lcom/itau/empresas/api/model/MenuVO;

    .line 19
    invoke-static {p1}, Lcom/itau/empresas/feature/home/HomeLogadaActivity;->iniciaHomeLogadaExibindoExtrato(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 20
    .local v1, "intent":Landroid/content/Intent;
    iget-object v0, p0, Lcom/itau/empresas/ui/util/menu/ExtratoCommand;->classActivity:Ljava/lang/Class;

    invoke-virtual {v1, p1, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 21
    invoke-virtual {p1, v1}, Landroid/support/v7/app/AppCompatActivity;->startActivity(Landroid/content/Intent;)V

    .line 22
    return-void
.end method

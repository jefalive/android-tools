.class public Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "ProdutosMultilimiteActivity.java"


# instance fields
.field adapter:Lcom/itau/empresas/feature/credito/produtos/adapter/ProdutosMultilimiteListAdapter;

.field lista:Landroid/widget/ListView;

.field produtosController:Lcom/itau/empresas/controller/ProdutosController;

.field root:Landroid/widget/LinearLayout;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 34
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 63
    const v1, 0x7f0706b4

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 66
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 97
    new-instance v0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method aoIniciarActivity()V
    .registers 3

    .line 45
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;->constroiToolbar()V

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;->lista:Landroid/widget/ListView;

    new-instance v1, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity$1;-><init>(Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 57
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;->mostraDialogoDeProgresso()V

    .line 58
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;->produtosController:Lcom/itau/empresas/controller/ProdutosController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/ProdutosController;->consultaDetalhesMultilimite()V

    .line 59
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "event"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 81
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 82
    return-void

    .line 85
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;->escondeDialogoDeProgresso()V

    .line 86
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;->root:Landroid/widget/LinearLayout;

    const v1, 0x7f0701ef

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 88
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoDetalhesMultilimiteErro;)V
    .registers 5
    .param p1, "multilimiteVO"    # Lcom/itau/empresas/Evento$EventoDetalhesMultilimiteErro;

    .line 75
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;->escondeDialogoDeProgresso()V

    .line 76
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;->root:Landroid/widget/LinearLayout;

    const v1, 0x7f0701ef

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 78
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteVO;)V
    .registers 4
    .param p1, "multilimiteVO"    # Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteVO;

    .line 69
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;->escondeDialogoDeProgresso()V

    .line 70
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;->adapter:Lcom/itau/empresas/feature/credito/produtos/adapter/ProdutosMultilimiteListAdapter;

    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteVO;->getProdutos()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/produtos/adapter/ProdutosMultilimiteListAdapter;->setData(Ljava/util/List;)V

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;->lista:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/activity/ProdutosMultilimiteActivity;->adapter:Lcom/itau/empresas/feature/credito/produtos/adapter/ProdutosMultilimiteListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 72
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 92
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "limitesProduto"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "MULTILIMITE_PJ"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "multiLimites"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.class public Lcom/itau/empresas/api/model/TrocaContaVO;
.super Ljava/lang/Object;
.source "TrocaContaVO.java"


# instance fields
.field private agencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia"
    .end annotation
.end field

.field private conta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta"
    .end annotation
.end field

.field private dac:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dac"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "agencia"    # Ljava/lang/String;
    .param p2, "conta"    # Ljava/lang/String;
    .param p3, "dac"    # Ljava/lang/String;

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/itau/empresas/api/model/TrocaContaVO;->agencia:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/itau/empresas/api/model/TrocaContaVO;->conta:Ljava/lang/String;

    .line 18
    iput-object p3, p0, Lcom/itau/empresas/api/model/TrocaContaVO;->dac:Ljava/lang/String;

    .line 19
    return-void
.end method

.class Lcom/google/android/gms/internal/zzsr;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private zzbuq:Lcom/google/android/gms/internal/zzsp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/zzsp<**>;"
        }
    .end annotation
.end field

.field private zzbur:Ljava/lang/Object;

.field private zzbus:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/google/android/gms/internal/zzsw;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .registers 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbus:Ljava/util/List;

    return-void
.end method

.method private toByteArray()[B
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzsr;->zzz()I

    move-result v0

    new-array v1, v0, [B

    invoke-static {v1}, Lcom/google/android/gms/internal/zzsn;->zzE([B)Lcom/google/android/gms/internal/zzsn;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/internal/zzsr;->writeTo(Lcom/google/android/gms/internal/zzsn;)V

    return-object v1
.end method


# virtual methods
.method public synthetic clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzsr;->zzJr()Lcom/google/android/gms/internal/zzsr;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .param p1, "o"    # Ljava/lang/Object;

    if-ne p1, p0, :cond_4

    const/4 v0, 0x1

    return v0

    :cond_4
    instance-of v0, p1, Lcom/google/android/gms/internal/zzsr;

    if-nez v0, :cond_a

    const/4 v0, 0x0

    return v0

    :cond_a
    move-object v2, p1

    check-cast v2, Lcom/google/android/gms/internal/zzsr;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    if-eqz v0, :cond_cb

    iget-object v0, v2, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    if-eqz v0, :cond_cb

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbuq:Lcom/google/android/gms/internal/zzsp;

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsr;->zzbuq:Lcom/google/android/gms/internal/zzsp;

    if-eq v0, v1, :cond_1d

    const/4 v0, 0x0

    return v0

    :cond_1d
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbuq:Lcom/google/android/gms/internal/zzsp;

    iget-object v0, v0, Lcom/google/android/gms/internal/zzsp;->zzbuk:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-nez v0, :cond_30

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    :cond_30
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_47

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v0, [B

    check-cast v0, [B

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v1, [B

    check-cast v1, [B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    return v0

    :cond_47
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    instance-of v0, v0, [I

    if-eqz v0, :cond_5e

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v0, [I

    check-cast v0, [I

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v1, [I

    check-cast v1, [I

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    return v0

    :cond_5e
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    instance-of v0, v0, [J

    if-eqz v0, :cond_75

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v0, [J

    check-cast v0, [J

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v1, [J

    check-cast v1, [J

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v0

    return v0

    :cond_75
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    instance-of v0, v0, [F

    if-eqz v0, :cond_8c

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v0, [F

    check-cast v0, [F

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v1, [F

    check-cast v1, [F

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([F[F)Z

    move-result v0

    return v0

    :cond_8c
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    instance-of v0, v0, [D

    if-eqz v0, :cond_a3

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v0, [D

    check-cast v0, [D

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v1, [D

    check-cast v1, [D

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([D[D)Z

    move-result v0

    return v0

    :cond_a3
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    instance-of v0, v0, [Z

    if-eqz v0, :cond_ba

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v0, [Z

    check-cast v0, [Z

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v1, [Z

    check-cast v1, [Z

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Z[Z)Z

    move-result v0

    return v0

    :cond_ba
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v1, [Ljava/lang/Object;

    check-cast v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/util/Arrays;->deepEquals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    return v0

    :cond_cb
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbus:Ljava/util/List;

    if-eqz v0, :cond_dc

    iget-object v0, v2, Lcom/google/android/gms/internal/zzsr;->zzbus:Ljava/util/List;

    if-eqz v0, :cond_dc

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbus:Ljava/util/List;

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsr;->zzbus:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    :cond_dc
    :try_start_dc
    invoke-direct {p0}, Lcom/google/android/gms/internal/zzsr;->toByteArray()[B

    move-result-object v0

    invoke-direct {v2}, Lcom/google/android/gms/internal/zzsr;->toByteArray()[B

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z
    :try_end_e7
    .catch Ljava/io/IOException; {:try_start_dc .. :try_end_e7} :catch_e9

    move-result v0

    return v0

    :catch_e9
    move-exception v3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public hashCode()I
    .registers 5

    const/16 v2, 0x11

    :try_start_2
    invoke-direct {p0}, Lcom/google/android/gms/internal/zzsr;->toByteArray()[B

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_9} :catch_d

    move-result v0

    add-int/lit16 v2, v0, 0x20f

    goto :goto_14

    :catch_d
    move-exception v3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :goto_14
    return v2
.end method

.method writeTo(Lcom/google/android/gms/internal/zzsn;)V
    .registers 6
    .param p1, "output"    # Lcom/google/android/gms/internal/zzsn;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbuq:Lcom/google/android/gms/internal/zzsp;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/internal/zzsp;->zza(Ljava/lang/Object;Lcom/google/android/gms/internal/zzsn;)V

    goto :goto_23

    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbus:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_12
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/google/android/gms/internal/zzsw;

    invoke-virtual {v3, p1}, Lcom/google/android/gms/internal/zzsw;->writeTo(Lcom/google/android/gms/internal/zzsn;)V

    goto :goto_12

    :cond_23
    :goto_23
    return-void
.end method

.method public final zzJr()Lcom/google/android/gms/internal/zzsr;
    .registers 7

    new-instance v2, Lcom/google/android/gms/internal/zzsr;

    invoke-direct {v2}, Lcom/google/android/gms/internal/zzsr;-><init>()V

    :try_start_5
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbuq:Lcom/google/android/gms/internal/zzsp;

    iput-object v0, v2, Lcom/google/android/gms/internal/zzsr;->zzbuq:Lcom/google/android/gms/internal/zzsp;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbus:Ljava/util/List;

    if-nez v0, :cond_11

    const/4 v0, 0x0

    iput-object v0, v2, Lcom/google/android/gms/internal/zzsr;->zzbus:Ljava/util/List;

    goto :goto_18

    :cond_11
    iget-object v0, v2, Lcom/google/android/gms/internal/zzsr;->zzbus:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsr;->zzbus:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :goto_18
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    if-nez v0, :cond_1e

    goto/16 :goto_eb

    :cond_1e
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    instance-of v0, v0, Lcom/google/android/gms/internal/zzsu;

    if-eqz v0, :cond_30

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/internal/zzsu;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzsu;->clone()Lcom/google/android/gms/internal/zzsu;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    goto/16 :goto_eb

    :cond_30
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-eqz v0, :cond_44

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v0, [B

    check-cast v0, [B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    goto/16 :goto_eb

    :cond_44
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    instance-of v0, v0, [[B

    if-eqz v0, :cond_69

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v0, [[B

    move-object v3, v0

    check-cast v3, [[B

    array-length v0, v3

    new-array v4, v0, [[B

    iput-object v4, v2, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    const/4 v5, 0x0

    :goto_57
    array-length v0, v3

    if-ge v5, v0, :cond_67

    aget-object v0, v3, v5

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    aput-object v0, v4, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_57

    :cond_67
    goto/16 :goto_eb

    :cond_69
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    instance-of v0, v0, [Z

    if-eqz v0, :cond_7d

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v0, [Z

    check-cast v0, [Z

    invoke-virtual {v0}, [Z->clone()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    goto/16 :goto_eb

    :cond_7d
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    instance-of v0, v0, [I

    if-eqz v0, :cond_91

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v0, [I

    check-cast v0, [I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    goto/16 :goto_eb

    :cond_91
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    instance-of v0, v0, [J

    if-eqz v0, :cond_a4

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v0, [J

    check-cast v0, [J

    invoke-virtual {v0}, [J->clone()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    goto :goto_eb

    :cond_a4
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    instance-of v0, v0, [F

    if-eqz v0, :cond_b7

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v0, [F

    check-cast v0, [F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    goto :goto_eb

    :cond_b7
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    instance-of v0, v0, [D

    if-eqz v0, :cond_ca

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v0, [D

    check-cast v0, [D

    invoke-virtual {v0}, [D->clone()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    goto :goto_eb

    :cond_ca
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    instance-of v0, v0, [Lcom/google/android/gms/internal/zzsu;

    if-eqz v0, :cond_eb

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    check-cast v0, [Lcom/google/android/gms/internal/zzsu;

    move-object v3, v0

    check-cast v3, [Lcom/google/android/gms/internal/zzsu;

    array-length v0, v3

    new-array v4, v0, [Lcom/google/android/gms/internal/zzsu;

    iput-object v4, v2, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    const/4 v5, 0x0

    :goto_dd
    array-length v0, v3

    if-ge v5, v0, :cond_eb

    aget-object v0, v3, v5

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzsu;->clone()Lcom/google/android/gms/internal/zzsu;

    move-result-object v0

    aput-object v0, v4, v5
    :try_end_e8
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_5 .. :try_end_e8} :catch_ec

    add-int/lit8 v5, v5, 0x1

    goto :goto_dd

    :cond_eb
    :goto_eb
    return-object v2

    :catch_ec
    move-exception v3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method zza(Lcom/google/android/gms/internal/zzsw;)V
    .registers 3

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbus:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method zzz()I
    .registers 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbuq:Lcom/google/android/gms/internal/zzsp;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsr;->zzbur:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzsp;->zzY(Ljava/lang/Object;)I

    move-result v2

    goto :goto_27

    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsr;->zzbus:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_14
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/google/android/gms/internal/zzsw;

    invoke-virtual {v4}, Lcom/google/android/gms/internal/zzsw;->zzz()I

    move-result v0

    add-int/2addr v2, v0

    goto :goto_14

    :cond_27
    :goto_27
    return v2
.end method

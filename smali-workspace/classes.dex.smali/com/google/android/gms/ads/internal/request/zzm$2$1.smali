.class Lcom/google/android/gms/ads/internal/request/zzm$2$1;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/zzji$zzc;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/ads/internal/request/zzm$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Lcom/google/android/gms/internal/zzji$zzc<Lcom/google/android/gms/internal/zzeh;>;"
    }
.end annotation


# instance fields
.field final synthetic zzIG:Lcom/google/android/gms/ads/internal/request/zzm$2;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/internal/request/zzm$2;)V
    .registers 2

    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/zzm$2$1;->zzIG:Lcom/google/android/gms/ads/internal/request/zzm$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public zzd(Lcom/google/android/gms/internal/zzeh;)V
    .registers 5

    const-string v0, "AFMA_getAdapterLessMediationAd"

    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/zzm$2$1;->zzIG:Lcom/google/android/gms/ads/internal/request/zzm$2;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/request/zzm$2;->zzIE:Lorg/json/JSONObject;

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/internal/zzeh;->zza(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_9} :catch_a

    goto :goto_1b

    :catch_a
    move-exception v2

    const-string v0, "Error requesting an ad url"

    invoke-static {v0, v2}, Lcom/google/android/gms/internal/zzin;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {}, Lcom/google/android/gms/ads/internal/request/zzm;->zzgx()Lcom/google/android/gms/internal/zzdk;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/zzm$2$1;->zzIG:Lcom/google/android/gms/ads/internal/request/zzm$2;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/request/zzm$2;->zzIF:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzdk;->zzS(Ljava/lang/String;)V

    :goto_1b
    return-void
.end method

.method public synthetic zze(Ljava/lang/Object;)V
    .registers 3

    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/internal/zzeh;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/request/zzm$2$1;->zzd(Lcom/google/android/gms/internal/zzeh;)V

    return-void
.end method

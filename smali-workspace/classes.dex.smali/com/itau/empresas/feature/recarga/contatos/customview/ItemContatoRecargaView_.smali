.class public final Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;
.super Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;
.source "ItemContatoRecargaView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 39
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;-><init>(Landroid/content/Context;)V

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->alreadyInflated_:Z

    .line 36
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 40
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->init_()V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->alreadyInflated_:Z

    .line 36
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 45
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->init_()V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->alreadyInflated_:Z

    .line 36
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 50
    invoke-direct {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->init_()V

    .line 51
    return-void
.end method

.method public static build(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 59
    new-instance v0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;-><init>(Landroid/content/Context;)V

    .line 60
    .local v0, "instance":Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;
    invoke-virtual {v0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->onFinishInflate()V

    .line 61
    return-object v0
.end method

.method private init_()V
    .registers 3

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 82
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 83
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->application:Lcom/itau/empresas/CustomApplication;

    .line 84
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->contatoRecargaController:Lcom/itau/empresas/feature/recarga/contatos/controller/ContatoRecargaController;

    .line 85
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 86
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 72
    iget-boolean v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->alreadyInflated_:Z

    .line 74
    invoke-virtual {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0300fc

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 77
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView;->onFinishInflate()V

    .line 78
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 5
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 108
    const v0, 0x7f0e011e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->textoNomeContato:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f0e011f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->textoNumeroContato:Landroid/widget/TextView;

    .line 110
    const v0, 0x7f0e0120

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->textoOperadoraContato:Landroid/widget/TextView;

    .line 111
    const v0, 0x7f0e0541

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->botaoEditarContato:Landroid/widget/ImageButton;

    .line 112
    const v0, 0x7f0e02ad

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->botaoExcluir:Landroid/widget/ImageButton;

    .line 113
    const v0, 0x7f0e0540

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 115
    .local v2, "view_rl_item_lista_contatos_recarga":Landroid/view/View;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->botaoEditarContato:Landroid/widget/ImageButton;

    if-eqz v0, :cond_4c

    .line 116
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->botaoEditarContato:Landroid/widget/ImageButton;

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_$1;-><init>(Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    :cond_4c
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->botaoExcluir:Landroid/widget/ImageButton;

    if-eqz v0, :cond_5a

    .line 126
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;->botaoExcluir:Landroid/widget/ImageButton;

    new-instance v1, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_$2;-><init>(Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    :cond_5a
    if-eqz v2, :cond_64

    .line 136
    new-instance v0, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_$3;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_$3;-><init>(Lcom/itau/empresas/feature/recarga/contatos/customview/ItemContatoRecargaView_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    :cond_64
    return-void
.end method

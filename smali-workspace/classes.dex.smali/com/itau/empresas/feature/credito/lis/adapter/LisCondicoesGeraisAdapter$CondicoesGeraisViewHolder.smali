.class public Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter$CondicoesGeraisViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "LisCondicoesGeraisAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CondicoesGeraisViewHolder"
.end annotation


# instance fields
.field private final icone:Landroid/widget/TextView;

.field private final textoDescricao:Landroid/widget/TextView;

.field private final textoTitulo:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter;


# direct methods
.method protected constructor <init>(Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter;Landroid/view/View;)V
    .registers 4
    .param p1, "this$0"    # Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .line 72
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter$CondicoesGeraisViewHolder;->this$0:Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter;

    .line 73
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 75
    const v0, 0x7f0e054d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter$CondicoesGeraisViewHolder;->icone:Landroid/widget/TextView;

    .line 76
    const v0, 0x7f0e054e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter$CondicoesGeraisViewHolder;->textoTitulo:Landroid/widget/TextView;

    .line 77
    const v0, 0x7f0e0550

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter$CondicoesGeraisViewHolder;->textoDescricao:Landroid/widget/TextView;

    .line 78
    return-void
.end method


# virtual methods
.method public getIcone()Landroid/widget/TextView;
    .registers 2

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter$CondicoesGeraisViewHolder;->icone:Landroid/widget/TextView;

    return-object v0
.end method

.method public getTextoDescricao()Landroid/widget/TextView;
    .registers 2

    .line 89
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter$CondicoesGeraisViewHolder;->textoDescricao:Landroid/widget/TextView;

    return-object v0
.end method

.method public getTextoTitulo()Landroid/widget/TextView;
    .registers 2

    .line 85
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/adapter/LisCondicoesGeraisAdapter$CondicoesGeraisViewHolder;->textoTitulo:Landroid/widget/TextView;

    return-object v0
.end method

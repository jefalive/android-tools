.class Lcom/google/android/gms/ads/internal/zzi$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gms/ads/internal/zzi;->zzf(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic zzpW:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

.field final synthetic zzpX:Lcom/google/android/gms/ads/internal/zzi;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/internal/zzi;Lcom/google/android/gms/ads/internal/client/AdRequestParcel;)V
    .registers 3

    iput-object p1, p0, Lcom/google/android/gms/ads/internal/zzi$1;->zzpX:Lcom/google/android/gms/ads/internal/zzi;

    iput-object p2, p0, Lcom/google/android/gms/ads/internal/zzi$1;->zzpW:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/zzi$1;->zzpX:Lcom/google/android/gms/ads/internal/zzi;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/zzi;->zza(Lcom/google/android/gms/ads/internal/zzi;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    :try_start_7
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/zzi$1;->zzpX:Lcom/google/android/gms/ads/internal/zzi;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/zzi;->zzbm()Lcom/google/android/gms/ads/internal/zzp;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/zzi$1;->zzpX:Lcom/google/android/gms/ads/internal/zzi;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/zzi;->zza(Lcom/google/android/gms/ads/internal/zzi;Ljava/lang/ref/WeakReference;)Ljava/lang/ref/WeakReference;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/zzi$1;->zzpX:Lcom/google/android/gms/ads/internal/zzi;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/zzi;->zzb(Lcom/google/android/gms/ads/internal/zzi;)Lcom/google/android/gms/internal/zzcr;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/ads/internal/zzp;->zzb(Lcom/google/android/gms/internal/zzcr;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/zzi$1;->zzpX:Lcom/google/android/gms/ads/internal/zzi;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/zzi;->zzc(Lcom/google/android/gms/ads/internal/zzi;)Lcom/google/android/gms/internal/zzcs;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/ads/internal/zzp;->zzb(Lcom/google/android/gms/internal/zzcs;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/zzi$1;->zzpX:Lcom/google/android/gms/ads/internal/zzi;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/zzi;->zzd(Lcom/google/android/gms/ads/internal/zzi;)Landroid/support/v4/util/SimpleArrayMap;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/ads/internal/zzp;->zza(Landroid/support/v4/util/SimpleArrayMap;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/zzi$1;->zzpX:Lcom/google/android/gms/ads/internal/zzi;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/zzi;->zze(Lcom/google/android/gms/ads/internal/zzi;)Lcom/google/android/gms/ads/internal/client/zzq;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/ads/internal/zzp;->zza(Lcom/google/android/gms/ads/internal/client/zzq;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/zzi$1;->zzpX:Lcom/google/android/gms/ads/internal/zzi;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/zzi;->zzf(Lcom/google/android/gms/ads/internal/zzi;)Landroid/support/v4/util/SimpleArrayMap;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/ads/internal/zzp;->zzb(Landroid/support/v4/util/SimpleArrayMap;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/zzi$1;->zzpX:Lcom/google/android/gms/ads/internal/zzi;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/zzi;->zzg(Lcom/google/android/gms/ads/internal/zzi;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/ads/internal/zzp;->zza(Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/zzi$1;->zzpX:Lcom/google/android/gms/ads/internal/zzi;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/zzi;->zzh(Lcom/google/android/gms/ads/internal/zzi;)Lcom/google/android/gms/ads/internal/formats/NativeAdOptionsParcel;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/ads/internal/zzp;->zzb(Lcom/google/android/gms/ads/internal/formats/NativeAdOptionsParcel;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/zzi$1;->zzpX:Lcom/google/android/gms/ads/internal/zzi;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/zzi;->zzi(Lcom/google/android/gms/ads/internal/zzi;)Lcom/google/android/gms/ads/internal/client/zzx;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/ads/internal/zzp;->zza(Lcom/google/android/gms/ads/internal/client/zzx;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/zzi$1;->zzpW:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/ads/internal/zzp;->zzb(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;)Z
    :try_end_64
    .catchall {:try_start_7 .. :try_end_64} :catchall_66

    monitor-exit v2

    goto :goto_69

    :catchall_66
    move-exception v4

    monitor-exit v2

    throw v4

    :goto_69
    return-void
.end method

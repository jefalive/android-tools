.class Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$1;
.super Ljava/lang/Object;
.source "ExtratoFragmentPrincipal.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->exibirDialogDeFiltro()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

.field final synthetic val$filtroExtratoDialog:Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    .line 308
    iput-object p1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$1;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    iput-object p2, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$1;->val$filtroExtratoDialog:Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 7
    .param p1, "v"    # Landroid/view/View;

    .line 311
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$1;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getQtdeDiasFiltro()I

    move-result v0

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$1;->val$filtroExtratoDialog:Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->getQuantidadeDias()I

    move-result v1

    if-eq v0, v1, :cond_4b

    .line 312
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$1;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$1;->val$filtroExtratoDialog:Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->getQuantidadeDias()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->setQtdeDiasFiltro(I)V

    .line 314
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$1;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$1;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    const v2, 0x7f0702b0

    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$1;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    .line 315
    const v3, 0x7f07031d

    invoke-virtual {v2, v3}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$1;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    .line 316
    invoke-virtual {v3}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->getQtdeDiasFiltro()I

    move-result v3

    int-to-long v3, v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 314
    invoke-virtual {v0, v1, v2, v3}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->analyticsHit(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 318
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$1;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$1;->val$filtroExtratoDialog:Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;

    .line 319
    invoke-virtual {v1}, Lcom/itau/empresas/feature/extrato/fragment/FiltroExtratoDialog;->getQuantidadeDias()I

    move-result v1

    invoke-static {v1}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->subtraiDiasDeHoje(I)Ljava/lang/String;

    move-result-object v1

    .line 318
    # invokes: Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->carregarDados(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->access$100(Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;Ljava/lang/String;)V

    goto :goto_56

    .line 321
    :cond_4b
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal$1;->this$0:Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;

    iget-object v0, v0, Lcom/itau/empresas/feature/extrato/fragment/ExtratoFragmentPrincipal;->pagerExtrato:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->notifyDataSetChanged()V

    .line 323
    :goto_56
    return-void
.end method

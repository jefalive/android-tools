.class public final Lbr/com/concretesolutions/canarinho/formatador/FormatadorLinhaDigitavel;
.super Ljava/lang/Object;
.source "FormatadorLinhaDigitavel.java"

# interfaces
.implements Lbr/com/concretesolutions/canarinho/formatador/Formatador;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/concretesolutions/canarinho/formatador/FormatadorLinhaDigitavel$SingletonHolder;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/concretesolutions/canarinho/formatador/FormatadorLinhaDigitavel$1;)V
    .registers 2
    .param p1, "x0"    # Lbr/com/concretesolutions/canarinho/formatador/FormatadorLinhaDigitavel$1;

    .line 15
    invoke-direct {p0}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorLinhaDigitavel;-><init>()V

    return-void
.end method

.method static getInstance()Lbr/com/concretesolutions/canarinho/formatador/FormatadorLinhaDigitavel;
    .registers 1

    .line 21
    # getter for: Lbr/com/concretesolutions/canarinho/formatador/FormatadorLinhaDigitavel$SingletonHolder;->INSTANCE:Lbr/com/concretesolutions/canarinho/formatador/FormatadorLinhaDigitavel;
    invoke-static {}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorLinhaDigitavel$SingletonHolder;->access$000()Lbr/com/concretesolutions/canarinho/formatador/FormatadorLinhaDigitavel;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public desformata(Ljava/lang/String;)Ljava/lang/String;
    .registers 11
    .param p1, "valor"    # Ljava/lang/String;

    .line 80
    if-eqz p1, :cond_a

    const-string v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 81
    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Valor n\u00e3o pode estar nulo."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_12
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->PADRAO_SOMENTE_NUMEROS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 86
    .local v2, "valorDesformatadao":Ljava/lang/String;
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x38

    if-ne v0, v1, :cond_7b

    .line 88
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x30

    if-eq v0, v1, :cond_37

    .line 89
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Valor para boletos que iniciam com 8 deve conter 48 d\u00edgitos"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_37
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 94
    .local v3, "builder":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    const/16 v1, 0xb

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 95
    .local v4, "primeiroBloco":Ljava/lang/String;
    const/16 v0, 0xc

    const/16 v1, 0x17

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 96
    .local v5, "segundoBloco":Ljava/lang/String;
    const/16 v0, 0x18

    const/16 v1, 0x23

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 97
    .local v6, "terceiroBloco":Ljava/lang/String;
    const/16 v0, 0x24

    const/16 v1, 0x2f

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 99
    .local v7, "quartoBloco":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 102
    .end local v3    # "builder":Ljava/lang/StringBuilder;
    .end local v4    # "primeiroBloco":Ljava/lang/String;
    .end local v5    # "segundoBloco":Ljava/lang/String;
    .end local v6    # "terceiroBloco":Ljava/lang/String;
    .end local v7    # "quartoBloco":Ljava/lang/String;
    :cond_7b
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x2f

    if-eq v0, v1, :cond_8b

    .line 103
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Valor para boletos deve conter 47 digitos"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :cond_8b
    const/4 v0, 0x0

    const/16 v1, 0x9

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 107
    .local v3, "primeiroBloco":Ljava/lang/String;
    const/16 v0, 0xa

    const/16 v1, 0x14

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 108
    .local v4, "segundoBloco":Ljava/lang/String;
    const/16 v0, 0x15

    const/16 v1, 0x1f

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 109
    .local v5, "terceiroBloco":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x20

    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 111
    .local v6, "quartoBloco":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 112
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 113
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 114
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    .line 117
    .local v7, "boletoOrdenado":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 118
    const/16 v0, 0x1d

    const/16 v1, 0x2c

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 119
    const/4 v0, 0x4

    const/16 v1, 0x9

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 120
    const/16 v0, 0x9

    const/16 v1, 0x13

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 121
    const/16 v0, 0x13

    const/16 v1, 0x1d

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 123
    .local v8, "quintoBloco":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public estaFormatado(Ljava/lang/String;)Z
    .registers 3
    .param p1, "value"    # Ljava/lang/String;

    .line 128
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->BOLETO:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    invoke-interface {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->estaFormatado(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public formata(Ljava/lang/String;)Ljava/lang/String;
    .registers 15
    .param p1, "value"    # Ljava/lang/String;

    .line 27
    if-eqz p1, :cond_a

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x2c

    if-eq v0, v1, :cond_23

    .line 28
    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Linha digit\u00e1vel deve conter 44 caracteres. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :cond_23
    const-string v0, "8"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a0

    .line 33
    const/4 v0, 0x0

    const/16 v1, 0xb

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 34
    .local v3, "primeiroBloco":Ljava/lang/String;
    const/16 v0, 0xb

    const/16 v1, 0x16

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 35
    .local v4, "segundoBloco":Ljava/lang/String;
    const/16 v0, 0x16

    const/16 v1, 0x21

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 36
    .local v5, "terceiroBloco":Ljava/lang/String;
    const/16 v0, 0x21

    const/16 v1, 0x2c

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 39
    .local v6, "quartoBloco":Ljava/lang/String;
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x36

    if-eq v0, v1, :cond_5c

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x37

    if-ne v0, v1, :cond_5e

    :cond_5c
    const/4 v7, 0x1

    goto :goto_5f

    :cond_5e
    const/4 v7, 0x0

    .line 40
    .local v7, "ehMod10":Z
    :goto_5f
    if-eqz v7, :cond_64

    sget-object v8, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->MOD_10:Lbr/com/concretesolutions/canarinho/DigitoPara;

    goto :goto_66

    :cond_64
    sget-object v8, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->MOD_11:Lbr/com/concretesolutions/canarinho/DigitoPara;

    .line 42
    .local v8, "mod":Lbr/com/concretesolutions/canarinho/DigitoPara;
    :goto_66
    invoke-virtual {v8, v3}, Lbr/com/concretesolutions/canarinho/DigitoPara;->calcula(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 43
    .local v9, "primeiroDigito":Ljava/lang/String;
    invoke-virtual {v8, v4}, Lbr/com/concretesolutions/canarinho/DigitoPara;->calcula(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 44
    .local v10, "segundoDigito":Ljava/lang/String;
    invoke-virtual {v8, v5}, Lbr/com/concretesolutions/canarinho/DigitoPara;->calcula(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 45
    .local v11, "terceiroDigito":Ljava/lang/String;
    invoke-virtual {v8, v6}, Lbr/com/concretesolutions/canarinho/DigitoPara;->calcula(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 47
    .local v12, "quartoDigito":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 51
    .end local v3    # "primeiroBloco":Ljava/lang/String;
    .end local v4    # "segundoBloco":Ljava/lang/String;
    .end local v5    # "terceiroBloco":Ljava/lang/String;
    .end local v6    # "quartoBloco":Ljava/lang/String;
    .end local v7    # "ehMod10":Z
    .end local v8    # "mod":Lbr/com/concretesolutions/canarinho/DigitoPara;
    .end local v9    # "primeiroDigito":Ljava/lang/String;
    .end local v10    # "segundoDigito":Ljava/lang/String;
    .end local v11    # "terceiroDigito":Ljava/lang/String;
    .end local v12    # "quartoDigito":Ljava/lang/String;
    :cond_a0
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 52
    .local v3, "primeiroBloco":Ljava/lang/String;
    const/4 v0, 0x4

    const/16 v1, 0x13

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 53
    .local v4, "segundoBloco":Ljava/lang/String;
    const/16 v0, 0x13

    const/16 v1, 0x18

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 54
    .local v5, "terceiroBloco":Ljava/lang/String;
    const/16 v0, 0x18

    const/16 v1, 0x22

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 55
    .local v6, "quartoBloco":Ljava/lang/String;
    const/16 v0, 0x22

    const/16 v1, 0x2c

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 58
    .local v7, "quintoBloco":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 59
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 60
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 61
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 62
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 64
    .local v8, "codigoOrdenado":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    const/16 v1, 0x9

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 65
    const/16 v0, 0x9

    const/16 v1, 0x13

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 66
    const/16 v0, 0x13

    const/16 v1, 0x1d

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 67
    const/16 v0, 0x1d

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 69
    sget-object v0, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->MOD_10:Lbr/com/concretesolutions/canarinho/DigitoPara;

    invoke-virtual {v0, v3}, Lbr/com/concretesolutions/canarinho/DigitoPara;->calcula(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 70
    .local v9, "primeiroDigito":Ljava/lang/String;
    sget-object v0, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->MOD_10:Lbr/com/concretesolutions/canarinho/DigitoPara;

    invoke-virtual {v0, v4}, Lbr/com/concretesolutions/canarinho/DigitoPara;->calcula(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 71
    .local v10, "segundoDigito":Ljava/lang/String;
    sget-object v0, Lbr/com/concretesolutions/canarinho/validator/ValidadorBoleto;->MOD_10:Lbr/com/concretesolutions/canarinho/DigitoPara;

    invoke-virtual {v0, v5}, Lbr/com/concretesolutions/canarinho/DigitoPara;->calcula(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 73
    .local v11, "terceiroDigito":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public podeSerFormatado(Ljava/lang/String;)Z
    .registers 5
    .param p1, "value"    # Ljava/lang/String;

    .line 133
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->PADRAO_SOMENTE_NUMEROS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 134
    .local v2, "sanitizado":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x2c

    if-ne v0, v1, :cond_16

    const/4 v0, 0x1

    goto :goto_17

    :cond_16
    const/4 v0, 0x0

    :goto_17
    return v0
.end method

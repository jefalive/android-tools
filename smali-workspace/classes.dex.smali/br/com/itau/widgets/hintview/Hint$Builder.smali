.class public final Lbr/com/itau/widgets/hintview/Hint$Builder;
.super Ljava/lang/Object;
.source "Hint.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/widgets/hintview/Hint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private isCancelable:Z

.field private isDismissOnClick:Z

.field private mAnchorView:Landroid/view/View;

.field private mArrowDrawable:Landroid/graphics/drawable/Drawable;

.field private mArrowHeight:F

.field private mArrowMargin:F

.field private mArrowWidth:F

.field private mBackgroundColor:I

.field private mContext:Landroid/content/Context;

.field private mCornerRadius:F

.field private mGravity:I

.field private mLineSpacingExtra:F

.field private mLineSpacingMultiplier:F

.field private mMargin:F

.field private mOnClickListener:Lbr/com/itau/widgets/hintview/OnClickListener;

.field private mOnDismissListener:Lbr/com/itau/widgets/hintview/OnDismissListener;

.field private mOnLongClickListener:Lbr/com/itau/widgets/hintview/OnLongClickListener;

.field private mPadding:F

.field private mPaddingBottom:F

.field private mPaddingLeft:F

.field private mPaddingRight:F

.field private mPaddingTop:F

.field private mText:Ljava/lang/CharSequence;

.field private mTextAppearance:I

.field private mTextColor:Landroid/content/res/ColorStateList;

.field private mTextSize:F

.field private mTextStyle:I

.field private mTypeface:Landroid/graphics/Typeface;


# direct methods
.method public constructor <init>(Landroid/view/MenuItem;)V
    .registers 3
    .param p1, "anchorMenuItem"    # Landroid/view/MenuItem;

    .line 419
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;-><init>(Landroid/view/MenuItem;I)V

    .line 420
    return-void
.end method

.method public constructor <init>(Landroid/view/MenuItem;I)V
    .registers 7
    .param p1, "anchorMenuItem"    # Landroid/view/MenuItem;
    .param p2, "resId"    # I

    .line 422
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 404
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mLineSpacingMultiplier:F

    .line 409
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mTypeface:Landroid/graphics/Typeface;

    .line 424
    invoke-static {p1}, Landroid/support/v4/view/MenuItemCompat;->getActionView(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v2

    .line 425
    .local v2, "anchorView":Landroid/view/View;
    if-eqz v2, :cond_23

    .line 426
    instance-of v0, v2, Lbr/com/itau/widgets/hintview/HintActionView;

    if-eqz v0, :cond_1b

    .line 427
    move-object v3, v2

    check-cast v3, Lbr/com/itau/widgets/hintview/HintActionView;

    .line 428
    .local v3, "hintActionView":Lbr/com/itau/widgets/hintview/HintActionView;
    invoke-virtual {v3, p1}, Lbr/com/itau/widgets/hintview/HintActionView;->setMenuItem(Landroid/view/MenuItem;)V

    .line 431
    .end local v3    # "hintActionView":Lbr/com/itau/widgets/hintview/HintActionView;
    :cond_1b
    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, v2, p2}, Lbr/com/itau/widgets/hintview/Hint$Builder;->init(Landroid/content/Context;Landroid/view/View;I)V

    goto :goto_2b

    .line 433
    :cond_23
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "anchor menuItem haven`t actionViewClass"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 435
    :goto_2b
    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .registers 3
    .param p1, "anchorView"    # Landroid/view/View;

    .line 438
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;-><init>(Landroid/view/View;I)V

    .line 439
    return-void
.end method

.method public constructor <init>(Landroid/view/View;I)V
    .registers 4
    .param p1, "anchorView"    # Landroid/view/View;
    .param p2, "resId"    # I

    .line 441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 404
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mLineSpacingMultiplier:F

    .line 409
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mTypeface:Landroid/graphics/Typeface;

    .line 442
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lbr/com/itau/widgets/hintview/Hint$Builder;->init(Landroid/content/Context;Landroid/view/View;I)V

    .line 443
    return-void
.end method

.method static synthetic access$1200(Lbr/com/itau/widgets/hintview/Hint$Builder;)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget-boolean v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->isCancelable:Z

    return v0
.end method

.method static synthetic access$1300(Lbr/com/itau/widgets/hintview/Hint$Builder;)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget-boolean v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->isDismissOnClick:Z

    return v0
.end method

.method static synthetic access$1400(Lbr/com/itau/widgets/hintview/Hint$Builder;)I
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mGravity:I

    return v0
.end method

.method static synthetic access$1500(Lbr/com/itau/widgets/hintview/Hint$Builder;)F
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mMargin:F

    return v0
.end method

.method static synthetic access$1600(Lbr/com/itau/widgets/hintview/Hint$Builder;)F
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowMargin:F

    return v0
.end method

.method static synthetic access$1700(Lbr/com/itau/widgets/hintview/Hint$Builder;)Landroid/view/View;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mAnchorView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1800(Lbr/com/itau/widgets/hintview/Hint$Builder;)Lbr/com/itau/widgets/hintview/OnClickListener;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mOnClickListener:Lbr/com/itau/widgets/hintview/OnClickListener;

    return-object v0
.end method

.method static synthetic access$1900(Lbr/com/itau/widgets/hintview/Hint$Builder;)Lbr/com/itau/widgets/hintview/OnLongClickListener;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mOnLongClickListener:Lbr/com/itau/widgets/hintview/OnLongClickListener;

    return-object v0
.end method

.method static synthetic access$2000(Lbr/com/itau/widgets/hintview/Hint$Builder;)Lbr/com/itau/widgets/hintview/OnDismissListener;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mOnDismissListener:Lbr/com/itau/widgets/hintview/OnDismissListener;

    return-object v0
.end method

.method static synthetic access$2100(Lbr/com/itau/widgets/hintview/Hint$Builder;)Landroid/content/Context;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2400(Lbr/com/itau/widgets/hintview/Hint$Builder;)I
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mBackgroundColor:I

    return v0
.end method

.method static synthetic access$2500(Lbr/com/itau/widgets/hintview/Hint$Builder;)F
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mCornerRadius:F

    return v0
.end method

.method static synthetic access$2600(Lbr/com/itau/widgets/hintview/Hint$Builder;)F
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mPadding:F

    return v0
.end method

.method static synthetic access$2700(Lbr/com/itau/widgets/hintview/Hint$Builder;)F
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mPaddingLeft:F

    return v0
.end method

.method static synthetic access$2800(Lbr/com/itau/widgets/hintview/Hint$Builder;)F
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mPaddingRight:F

    return v0
.end method

.method static synthetic access$2900(Lbr/com/itau/widgets/hintview/Hint$Builder;)F
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mPaddingTop:F

    return v0
.end method

.method static synthetic access$3000(Lbr/com/itau/widgets/hintview/Hint$Builder;)F
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mPaddingBottom:F

    return v0
.end method

.method static synthetic access$3100(Lbr/com/itau/widgets/hintview/Hint$Builder;)I
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mTextAppearance:I

    return v0
.end method

.method static synthetic access$3200(Lbr/com/itau/widgets/hintview/Hint$Builder;)Ljava/lang/CharSequence;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$3300(Lbr/com/itau/widgets/hintview/Hint$Builder;)F
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mLineSpacingExtra:F

    return v0
.end method

.method static synthetic access$3400(Lbr/com/itau/widgets/hintview/Hint$Builder;)F
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mLineSpacingMultiplier:F

    return v0
.end method

.method static synthetic access$3500(Lbr/com/itau/widgets/hintview/Hint$Builder;)Landroid/graphics/Typeface;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mTypeface:Landroid/graphics/Typeface;

    return-object v0
.end method

.method static synthetic access$3600(Lbr/com/itau/widgets/hintview/Hint$Builder;)I
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mTextStyle:I

    return v0
.end method

.method static synthetic access$3700(Lbr/com/itau/widgets/hintview/Hint$Builder;)F
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mTextSize:F

    return v0
.end method

.method static synthetic access$3800(Lbr/com/itau/widgets/hintview/Hint$Builder;)Landroid/content/res/ColorStateList;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mTextColor:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method static synthetic access$3900(Lbr/com/itau/widgets/hintview/Hint$Builder;)Landroid/graphics/drawable/Drawable;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$4000(Lbr/com/itau/widgets/hintview/Hint$Builder;)F
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowWidth:F

    return v0
.end method

.method static synthetic access$4100(Lbr/com/itau/widgets/hintview/Hint$Builder;)F
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/hintview/Hint$Builder;

    .line 383
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowHeight:F

    return v0
.end method

.method private getTypefaceFromAttr(Ljava/lang/String;II)Landroid/graphics/Typeface;
    .registers 6
    .param p1, "familyName"    # Ljava/lang/String;
    .param p2, "typefaceIndex"    # I
    .param p3, "styleIndex"    # I

    .line 842
    const/4 v1, 0x0

    .line 843
    .local v1, "tf":Landroid/graphics/Typeface;
    if-eqz p1, :cond_a

    .line 844
    invoke-static {p1, p3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    .line 845
    if-eqz v1, :cond_a

    .line 846
    return-object v1

    .line 849
    :cond_a
    packed-switch p2, :pswitch_data_18

    goto :goto_16

    .line 851
    :pswitch_e
    sget-object v1, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    .line 852
    goto :goto_16

    .line 854
    :pswitch_11
    sget-object v1, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    .line 855
    goto :goto_16

    .line 857
    :pswitch_14
    sget-object v1, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    .line 860
    :goto_16
    return-object v1

    nop

    :pswitch_data_18
    .packed-switch 0x1
        :pswitch_e
        :pswitch_11
        :pswitch_14
    .end packed-switch
.end method

.method private init(Landroid/content/Context;Landroid/view/View;I)V
    .registers 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "anchorView"    # Landroid/view/View;
    .param p3, "resId"    # I

    .line 446
    iput-object p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mContext:Landroid/content/Context;

    .line 447
    iput-object p2, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mAnchorView:Landroid/view/View;

    .line 449
    sget-object v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint:[I

    invoke-virtual {p1, p3, v0}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 451
    .local v2, "a":Landroid/content/res/TypedArray;
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_cancelable:I

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->isCancelable:Z

    .line 452
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_dismissOnClick:I

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->isDismissOnClick:Z

    .line 453
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_backgroundColor:I

    const v1, -0x777778

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mBackgroundColor:I

    .line 454
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_cornerRadius:I

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mCornerRadius:F

    .line 455
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_arrowHeight:I

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowHeight:F

    .line 456
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_arrowWidth:I

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowWidth:F

    .line 457
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_arrowDrawable:I

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowDrawable:Landroid/graphics/drawable/Drawable;

    .line 458
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_margin:I

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mMargin:F

    .line 459
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_arrowMargin:I

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowMargin:F

    .line 460
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_textAppearance:I

    const/4 v1, -0x1

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mTextAppearance:I

    .line 461
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_android_padding:I

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mPadding:F

    .line 462
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_android_paddingLeft:I

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mPaddingLeft:F

    .line 463
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_android_paddingRight:I

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mPaddingRight:F

    .line 464
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_android_paddingTop:I

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mPaddingTop:F

    .line 465
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_android_paddingBottom:I

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mPaddingBottom:F

    .line 466
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_android_gravity:I

    const/16 v1, 0x50

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mGravity:I

    .line 467
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_android_text:I

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mText:Ljava/lang/CharSequence;

    .line 468
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_android_textSize:I

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mTextSize:F

    .line 469
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_android_textColor:I

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mTextColor:Landroid/content/res/ColorStateList;

    .line 470
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_android_textStyle:I

    const/4 v1, -0x1

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mTextStyle:I

    .line 471
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_android_lineSpacingExtra:I

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mLineSpacingExtra:F

    .line 472
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_android_lineSpacingMultiplier:I

    iget v1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mLineSpacingMultiplier:F

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mLineSpacingMultiplier:F

    .line 474
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_android_fontFamily:I

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 475
    .local v3, "fontFamily":Ljava/lang/String;
    sget v0, Lbr/com/itau/widgets/hintview/R$styleable;->Hint_android_typeface:I

    const/4 v1, -0x1

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    .line 476
    .local v4, "typefaceIndex":I
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mTextStyle:I

    invoke-direct {p0, v3, v4, v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->getTypefaceFromAttr(Ljava/lang/String;II)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mTypeface:Landroid/graphics/Typeface;

    .line 478
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 479
    return-void
.end method


# virtual methods
.method public build()Lbr/com/itau/widgets/hintview/Hint;
    .registers 4

    .line 807
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mGravity:I

    invoke-static {v0}, Landroid/view/Gravity;->isHorizontal(I)Z

    move-result v0

    if-nez v0, :cond_18

    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mGravity:I

    invoke-static {v0}, Landroid/view/Gravity;->isVertical(I)Z

    move-result v0

    if-nez v0, :cond_18

    .line 808
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Gravity must have be START, END, TOP or BOTTOM."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 811
    :cond_18
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowHeight:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2e

    .line 812
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/widgets/hintview/R$dimen;->default_hint_arrow_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowHeight:F

    .line 814
    :cond_2e
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowWidth:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_44

    .line 815
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/widgets/hintview/R$dimen;->default_hint_arrow_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowWidth:F

    .line 817
    :cond_44
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_53

    .line 818
    new-instance v0, Lbr/com/itau/widgets/hintview/ArrowDrawable;

    iget v1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mBackgroundColor:I

    iget v2, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mGravity:I

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/hintview/ArrowDrawable;-><init>(II)V

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowDrawable:Landroid/graphics/drawable/Drawable;

    .line 820
    :cond_53
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mMargin:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_69

    .line 821
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/widgets/hintview/R$dimen;->default_hint_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mMargin:F

    .line 823
    :cond_69
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowMargin:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_7f

    .line 824
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/widgets/hintview/R$dimen;->default_hint_arrow_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowMargin:F

    .line 826
    :cond_7f
    iget v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mPadding:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_95

    .line 827
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/widgets/hintview/R$dimen;->default_hint_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mPadding:F

    .line 829
    :cond_95
    new-instance v0, Lbr/com/itau/widgets/hintview/Hint;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lbr/com/itau/widgets/hintview/Hint;-><init>(Lbr/com/itau/widgets/hintview/Hint$Builder;Lbr/com/itau/widgets/hintview/Hint$1;)V

    return-object v0
.end method

.method public setArrow(I)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 4
    .param p1, "resId"    # I

    .line 574
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/support/v4/content/res/ResourcesCompat;->getDrawable(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setArrow(Landroid/graphics/drawable/Drawable;)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setArrow(Landroid/graphics/drawable/Drawable;)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 2
    .param p1, "arrowDrawable"    # Landroid/graphics/drawable/Drawable;

    .line 583
    iput-object p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowDrawable:Landroid/graphics/drawable/Drawable;

    .line 584
    return-object p0
.end method

.method public setArrowHeight(F)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 2
    .param p1, "height"    # F

    .line 545
    iput p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowHeight:F

    .line 546
    return-object p0
.end method

.method public setArrowHeight(I)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 3
    .param p1, "resId"    # I

    .line 536
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setArrowHeight(F)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setArrowMargin(F)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 2
    .param p1, "arrowMargin"    # F

    .line 613
    iput p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowMargin:F

    .line 614
    return-object p0
.end method

.method public setArrowWidth(F)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 2
    .param p1, "width"    # F

    .line 564
    iput p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mArrowWidth:F

    .line 565
    return-object p0
.end method

.method public setArrowWidth(I)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 3
    .param p1, "resId"    # I

    .line 555
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setArrowWidth(F)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setBackgroundColor(I)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 2
    .param p1, "color"    # I

    .line 507
    iput p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mBackgroundColor:I

    .line 508
    return-object p0
.end method

.method public setCancelable(Z)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 2
    .param p1, "cancelable"    # Z

    .line 487
    iput-boolean p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->isCancelable:Z

    .line 488
    return-object p0
.end method

.method public setCornerRadius(F)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 2
    .param p1, "radius"    # F

    .line 526
    iput p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mCornerRadius:F

    .line 527
    return-object p0
.end method

.method public setCornerRadius(I)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 3
    .param p1, "resId"    # I

    .line 517
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setCornerRadius(F)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setDismissOnClick(Z)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 2
    .param p1, "isDismissOnClick"    # Z

    .line 497
    iput-boolean p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->isDismissOnClick:Z

    .line 498
    return-object p0
.end method

.method public setGravity(I)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 2
    .param p1, "gravity"    # I

    .line 673
    iput p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mGravity:I

    .line 674
    return-object p0
.end method

.method public setLineSpacing(FF)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 3
    .param p1, "add"    # F
    .param p2, "mult"    # F

    .line 755
    iput p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mLineSpacingExtra:F

    .line 756
    iput p2, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mLineSpacingMultiplier:F

    .line 757
    return-object p0
.end method

.method public setLineSpacing(IF)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 4
    .param p1, "addResId"    # I
    .param p2, "mult"    # F

    .line 743
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mLineSpacingExtra:F

    .line 744
    iput p2, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mLineSpacingMultiplier:F

    .line 745
    return-object p0
.end method

.method public setMargin(F)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 2
    .param p1, "margin"    # F

    .line 602
    iput p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mMargin:F

    .line 603
    return-object p0
.end method

.method public setMargin(I)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 3
    .param p1, "resId"    # I

    .line 593
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setMargin(F)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setOnClickListener(Lbr/com/itau/widgets/hintview/OnClickListener;)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 2
    .param p1, "listener"    # Lbr/com/itau/widgets/hintview/OnClickListener;

    .line 776
    iput-object p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mOnClickListener:Lbr/com/itau/widgets/hintview/OnClickListener;

    .line 777
    return-object p0
.end method

.method public setOnDismissListener(Lbr/com/itau/widgets/hintview/OnDismissListener;)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 2
    .param p1, "listener"    # Lbr/com/itau/widgets/hintview/OnDismissListener;

    .line 796
    iput-object p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mOnDismissListener:Lbr/com/itau/widgets/hintview/OnDismissListener;

    .line 797
    return-object p0
.end method

.method public setOnLongClickListener(Lbr/com/itau/widgets/hintview/OnLongClickListener;)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 2
    .param p1, "listener"    # Lbr/com/itau/widgets/hintview/OnLongClickListener;

    .line 786
    iput-object p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mOnLongClickListener:Lbr/com/itau/widgets/hintview/OnLongClickListener;

    .line 787
    return-object p0
.end method

.method public setPadding(F)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 2
    .param p1, "padding"    # F

    .line 642
    iput p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mPadding:F

    .line 643
    return-object p0
.end method

.method public setPadding(I)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 3
    .param p1, "resId"    # I

    .line 633
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setPadding(F)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setPaddingBottom(F)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 2
    .param p1, "paddingBottom"    # F

    .line 662
    iput p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mPaddingBottom:F

    .line 663
    return-object p0
.end method

.method public setPaddingLeft(F)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 2
    .param p1, "paddingLeft"    # F

    .line 647
    iput p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mPaddingLeft:F

    .line 648
    return-object p0
.end method

.method public setPaddingRight(F)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 2
    .param p1, "paddingRight"    # F

    .line 652
    iput p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mPaddingRight:F

    .line 653
    return-object p0
.end method

.method public setPaddingTop(F)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 2
    .param p1, "paddingTop"    # F

    .line 657
    iput p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mPaddingTop:F

    .line 658
    return-object p0
.end method

.method public setText(I)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 3
    .param p1, "resId"    # I

    .line 683
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->setText(Ljava/lang/CharSequence;)Lbr/com/itau/widgets/hintview/Hint$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setText(Ljava/lang/CharSequence;)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 2
    .param p1, "text"    # Ljava/lang/CharSequence;

    .line 692
    iput-object p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mText:Ljava/lang/CharSequence;

    .line 693
    return-object p0
.end method

.method public setTextAppearance(I)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 2
    .param p1, "resId"    # I

    .line 623
    iput p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mTextAppearance:I

    .line 624
    return-object p0
.end method

.method public setTextColor(I)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 3
    .param p1, "color"    # I

    .line 722
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mTextColor:Landroid/content/res/ColorStateList;

    .line 723
    return-object p0
.end method

.method public setTextSize(F)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 4
    .param p1, "size"    # F

    .line 712
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1, p1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mTextSize:F

    .line 713
    return-object p0
.end method

.method public setTextSize(I)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 3
    .param p1, "resId"    # I

    .line 702
    iget-object v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mTextSize:F

    .line 703
    return-object p0
.end method

.method public setTextStyle(I)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 2
    .param p1, "style"    # I

    .line 732
    iput p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mTextStyle:I

    .line 733
    return-object p0
.end method

.method public setTypeface(Landroid/graphics/Typeface;)Lbr/com/itau/widgets/hintview/Hint$Builder;
    .registers 2
    .param p1, "typeface"    # Landroid/graphics/Typeface;

    .line 766
    iput-object p1, p0, Lbr/com/itau/widgets/hintview/Hint$Builder;->mTypeface:Landroid/graphics/Typeface;

    .line 767
    return-object p0
.end method

.method public show()Lbr/com/itau/widgets/hintview/Hint;
    .registers 2

    .line 836
    invoke-virtual {p0}, Lbr/com/itau/widgets/hintview/Hint$Builder;->build()Lbr/com/itau/widgets/hintview/Hint;

    move-result-object v0

    .line 837
    .local v0, "hint":Lbr/com/itau/widgets/hintview/Hint;
    invoke-virtual {v0}, Lbr/com/itau/widgets/hintview/Hint;->show()V

    .line 838
    return-object v0
.end method

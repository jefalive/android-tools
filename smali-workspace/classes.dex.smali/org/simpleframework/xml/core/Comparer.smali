.class Lorg/simpleframework/xml/core/Comparer;
.super Ljava/lang/Object;
.source "Comparer.java"


# static fields
.field private static final NAME:Ljava/lang/String; = "name"


# instance fields
.field private final ignore:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 4

    .line 51
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "name"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-direct {p0, v0}, Lorg/simpleframework/xml/core/Comparer;-><init>([Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method public varargs constructor <init>([Ljava/lang/String;)V
    .registers 2
    .param p1, "ignore"    # [Ljava/lang/String;

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lorg/simpleframework/xml/core/Comparer;->ignore:[Ljava/lang/String;

    .line 63
    return-void
.end method

.method private isIgnore(Ljava/lang/reflect/Method;)Z
    .registers 8
    .param p1, "method"    # Ljava/lang/reflect/Method;

    .line 106
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v1

    .line 108
    .local v1, "name":Ljava/lang/String;
    iget-object v0, p0, Lorg/simpleframework/xml/core/Comparer;->ignore:[Ljava/lang/String;

    if-eqz v0, :cond_1b

    .line 109
    iget-object v2, p0, Lorg/simpleframework/xml/core/Comparer;->ignore:[Ljava/lang/String;

    .local v2, "arr$":[Ljava/lang/String;
    array-length v3, v2

    .local v3, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_c
    if-ge v4, v3, :cond_1b

    aget-object v5, v2, v4

    .line 110
    .local v5, "value":Ljava/lang/String;
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 111
    const/4 v0, 0x1

    return v0

    .line 109
    .end local v5    # "value":Ljava/lang/String;
    :cond_18
    add-int/lit8 v4, v4, 0x1

    goto :goto_c

    .line 115
    .end local v2    # "arr$":[Ljava/lang/String;
    .end local v3    # "len$":I
    .end local v4    # "i$":I
    :cond_1b
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public equals(Ljava/lang/annotation/Annotation;Ljava/lang/annotation/Annotation;)Z
    .registers 13
    .param p1, "left"    # Ljava/lang/annotation/Annotation;
    .param p2, "right"    # Ljava/lang/annotation/Annotation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 76
    invoke-interface {p1}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v1

    .line 77
    .local v1, "type":Ljava/lang/Class;
    invoke-interface {p2}, Ljava/lang/annotation/Annotation;->annotationType()Ljava/lang/Class;

    move-result-object v2

    .line 78
    .local v2, "expect":Ljava/lang/Class;
    invoke-virtual {v1}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v3

    .line 80
    .local v3, "list":[Ljava/lang/reflect/Method;
    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 81
    move-object v4, v3

    .local v4, "arr$":[Ljava/lang/reflect/Method;
    array-length v5, v4

    .local v5, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_15
    if-ge v6, v5, :cond_38

    aget-object v7, v4, v6

    .line 82
    .local v7, "method":Ljava/lang/reflect/Method;
    invoke-direct {p0, v7}, Lorg/simpleframework/xml/core/Comparer;->isIgnore(Ljava/lang/reflect/Method;)Z

    move-result v0

    if-nez v0, :cond_35

    .line 83
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {v7, p1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .line 84
    .local v8, "value":Ljava/lang/Object;
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {v7, p2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    .line 86
    .local v9, "other":Ljava/lang/Object;
    invoke-virtual {v8, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_35

    .line 87
    const/4 v0, 0x0

    return v0

    .line 81
    .end local v7    # "method":Ljava/lang/reflect/Method;
    .end local v8    # "value":Ljava/lang/Object;
    .end local v9    # "other":Ljava/lang/Object;
    :cond_35
    add-int/lit8 v6, v6, 0x1

    goto :goto_15

    .line 91
    .end local v4    # "arr$":[Ljava/lang/reflect/Method;
    .end local v5    # "len$":I
    .end local v6    # "i$":I
    :cond_38
    const/4 v0, 0x1

    return v0

    .line 93
    :cond_3a
    const/4 v0, 0x0

    return v0
.end method

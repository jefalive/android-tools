.class Lio/fabric/sdk/android/services/network/SystemKeyStore;
.super Ljava/lang/Object;
.source "SystemKeyStore.java"


# instance fields
.field private final trustRoots:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/security/Principal;Ljava/security/cert/X509Certificate;>;"
        }
    .end annotation
.end field

.field final trustStore:Ljava/security/KeyStore;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;)V
    .registers 5
    .param p1, "keystoreStream"    # Ljava/io/InputStream;
    .param p2, "passwd"    # Ljava/lang/String;

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-direct {p0, p1, p2}, Lio/fabric/sdk/android/services/network/SystemKeyStore;->getTrustStore(Ljava/io/InputStream;Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v1

    .line 46
    .local v1, "trustStore":Ljava/security/KeyStore;
    invoke-direct {p0, v1}, Lio/fabric/sdk/android/services/network/SystemKeyStore;->initializeTrustedRoots(Ljava/security/KeyStore;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lio/fabric/sdk/android/services/network/SystemKeyStore;->trustRoots:Ljava/util/HashMap;

    .line 47
    iput-object v1, p0, Lio/fabric/sdk/android/services/network/SystemKeyStore;->trustStore:Ljava/security/KeyStore;

    .line 48
    return-void
.end method

.method private getTrustStore(Ljava/io/InputStream;Ljava/lang/String;)Ljava/security/KeyStore;
    .registers 7
    .param p1, "keystoreStream"    # Ljava/io/InputStream;
    .param p2, "passwd"    # Ljava/lang/String;

    .line 98
    const-string v0, "BKS"

    :try_start_2
    invoke-static {v0}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v1

    .line 99
    .local v1, "trustStore":Ljava/security/KeyStore;
    new-instance v2, Ljava/io/BufferedInputStream;

    invoke-direct {v2, p1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_b
    .catch Ljava/security/KeyStoreException; {:try_start_2 .. :try_end_b} :catch_1c
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_b} :catch_23
    .catch Ljava/security/cert/CertificateException; {:try_start_2 .. :try_end_b} :catch_2a
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_b} :catch_31

    .line 102
    .local v2, "bin":Ljava/io/BufferedInputStream;
    :try_start_b
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V
    :try_end_12
    .catchall {:try_start_b .. :try_end_12} :catchall_16

    .line 104
    :try_start_12
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_15
    .catch Ljava/security/KeyStoreException; {:try_start_12 .. :try_end_15} :catch_1c
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_12 .. :try_end_15} :catch_23
    .catch Ljava/security/cert/CertificateException; {:try_start_12 .. :try_end_15} :catch_2a
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_15} :catch_31

    .line 105
    goto :goto_1b

    .line 104
    :catchall_16
    move-exception v3

    :try_start_17
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V

    throw v3
    :try_end_1b
    .catch Ljava/security/KeyStoreException; {:try_start_17 .. :try_end_1b} :catch_1c
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_17 .. :try_end_1b} :catch_23
    .catch Ljava/security/cert/CertificateException; {:try_start_17 .. :try_end_1b} :catch_2a
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_1b} :catch_31

    .line 107
    :goto_1b
    return-object v1

    .line 108
    .end local v1    # "trustStore":Ljava/security/KeyStore;
    .end local v2    # "bin":Ljava/io/BufferedInputStream;
    :catch_1c
    move-exception v1

    .line 109
    .local v1, "kse":Ljava/security/KeyStoreException;
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 110
    .end local v1    # "kse":Ljava/security/KeyStoreException;
    :catch_23
    move-exception v1

    .line 111
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 112
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_2a
    move-exception v1

    .line 113
    .local v1, "e":Ljava/security/cert/CertificateException;
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 114
    .end local v1    # "e":Ljava/security/cert/CertificateException;
    :catch_31
    move-exception v1

    .line 115
    .local v1, "e":Ljava/io/IOException;
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method private initializeTrustedRoots(Ljava/security/KeyStore;)Ljava/util/HashMap;
    .registers 7
    .param p1, "trustStore"    # Ljava/security/KeyStore;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/security/KeyStore;)Ljava/util/HashMap<Ljava/security/Principal;Ljava/security/cert/X509Certificate;>;"
        }
    .end annotation

    .line 77
    :try_start_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 80
    .local v1, "trusted":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/security/Principal;Ljava/security/cert/X509Certificate;>;"
    invoke-virtual {p1}, Ljava/security/KeyStore;->aliases()Ljava/util/Enumeration;

    move-result-object v2

    .line 81
    .local v2, "aliases":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    :goto_9
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 82
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/String;

    .line 83
    .local v3, "alias":Ljava/lang/String;
    invoke-virtual {p1, v3}, Ljava/security/KeyStore;->getCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/security/cert/X509Certificate;

    .line 85
    .local v4, "cert":Ljava/security/cert/X509Certificate;
    if-eqz v4, :cond_26

    .line 86
    invoke-virtual {v4}, Ljava/security/cert/X509Certificate;->getSubjectX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v0

    invoke-virtual {v1, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_26
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_26} :catch_28

    .line 88
    .end local v3    # "alias":Ljava/lang/String;
    .end local v4    # "cert":Ljava/security/cert/X509Certificate;
    :cond_26
    goto :goto_9

    .line 90
    .end local v2    # "aliases":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    .end local v2
    :cond_27
    return-object v1

    .line 91
    .end local v1    # "trusted":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/security/Principal;Ljava/security/cert/X509Certificate;>;"
    .end local v1
    :catch_28
    move-exception v1

    .line 92
    .local v1, "e":Ljava/security/KeyStoreException;
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method


# virtual methods
.method public getTrustRootFor(Ljava/security/cert/X509Certificate;)Ljava/security/cert/X509Certificate;
    .registers 6
    .param p1, "certificate"    # Ljava/security/cert/X509Certificate;

    .line 56
    iget-object v0, p0, Lio/fabric/sdk/android/services/network/SystemKeyStore;->trustRoots:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getIssuerX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/security/cert/X509Certificate;

    .line 58
    .local v2, "trustRoot":Ljava/security/cert/X509Certificate;
    if-nez v2, :cond_11

    .line 59
    const/4 v0, 0x0

    return-object v0

    .line 62
    :cond_11
    invoke-virtual {v2}, Ljava/security/cert/X509Certificate;->getSubjectX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v0

    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getSubjectX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljavax/security/auth/x500/X500Principal;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 63
    const/4 v0, 0x0

    return-object v0

    .line 67
    :cond_21
    :try_start_21
    invoke-virtual {v2}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/security/cert/X509Certificate;->verify(Ljava/security/PublicKey;)V
    :try_end_28
    .catch Ljava/security/GeneralSecurityException; {:try_start_21 .. :try_end_28} :catch_29

    .line 70
    goto :goto_2c

    .line 68
    :catch_29
    move-exception v3

    .line 69
    .local v3, "e":Ljava/security/GeneralSecurityException;
    const/4 v0, 0x0

    return-object v0

    .line 72
    .end local v3    # "e":Ljava/security/GeneralSecurityException;
    :goto_2c
    return-object v2
.end method

.method public isTrustRoot(Ljava/security/cert/X509Certificate;)Z
    .registers 5
    .param p1, "certificate"    # Ljava/security/cert/X509Certificate;

    .line 51
    iget-object v0, p0, Lio/fabric/sdk/android/services/network/SystemKeyStore;->trustRoots:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getSubjectX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/security/cert/X509Certificate;

    .line 52
    .local v2, "trustRoot":Ljava/security/cert/X509Certificate;
    if-eqz v2, :cond_1f

    invoke-virtual {v2}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v0

    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    const/4 v0, 0x1

    goto :goto_20

    :cond_1f
    const/4 v0, 0x0

    :goto_20
    return v0
.end method

.class public Lcom/facebook/appevents/internal/SourceApplicationInfo$Factory;
.super Ljava/lang/Object;
.source "SourceApplicationInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/facebook/appevents/internal/SourceApplicationInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Landroid/app/Activity;)Lcom/facebook/appevents/internal/SourceApplicationInfo;
    .registers 10
    .param p0, "activity"    # Landroid/app/Activity;

    .line 110
    const/4 v2, 0x0

    .line 112
    .local v2, "openedByApplink":Z
    invoke-virtual {p0}, Landroid/app/Activity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v3

    .line 113
    .local v3, "callingApplication":Landroid/content/ComponentName;
    if-nez v3, :cond_9

    .line 114
    const/4 v0, 0x0

    return-object v0

    .line 117
    :cond_9
    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 118
    .local v4, "callingApplicationPackage":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 120
    const/4 v0, 0x0

    return-object v0

    .line 126
    :cond_19
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    .line 127
    .local v5, "openIntent":Landroid/content/Intent;
    if-eqz v5, :cond_44

    const-string v0, "_fbSourceApplicationHasBeenSet"

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_44

    .line 131
    const-string v0, "_fbSourceApplicationHasBeenSet"

    const/4 v1, 0x1

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 132
    invoke-static {v5}, Lbolts/AppLinks;->getAppLinkData(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v6

    .line 133
    .local v6, "applinkData":Landroid/os/Bundle;
    if-eqz v6, :cond_44

    .line 134
    const/4 v2, 0x1

    .line 135
    const-string v0, "referer_app_link"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    .line 136
    .local v7, "applinkReferrerData":Landroid/os/Bundle;
    if-eqz v7, :cond_44

    .line 137
    const-string v0, "package"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 138
    .local v8, "applinkReferrerPackage":Ljava/lang/String;
    move-object v4, v8

    .line 144
    .end local v6    # "applinkData":Landroid/os/Bundle;
    .end local v7    # "applinkReferrerData":Landroid/os/Bundle;
    .end local v8    # "applinkReferrerPackage":Ljava/lang/String;
    :cond_44
    const-string v0, "_fbSourceApplicationHasBeenSet"

    const/4 v1, 0x1

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 145
    new-instance v0, Lcom/facebook/appevents/internal/SourceApplicationInfo;

    const/4 v1, 0x0

    invoke-direct {v0, v4, v2, v1}, Lcom/facebook/appevents/internal/SourceApplicationInfo;-><init>(Ljava/lang/String;ZLcom/facebook/appevents/internal/SourceApplicationInfo$1;)V

    return-object v0
.end method

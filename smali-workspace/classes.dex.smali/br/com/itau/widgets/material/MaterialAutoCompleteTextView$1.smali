.class Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$1;
.super Ljava/lang/Object;
.source "MaterialAutoCompleteTextView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->initTextWatcher()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;


# direct methods
.method constructor <init>(Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;)V
    .registers 2

    .line 415
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$1;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 4
    .param p1, "s"    # Landroid/text/Editable;

    .line 426
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$1;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    # invokes: Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->checkCharactersCount()V
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->access$000(Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;)V

    .line 427
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$1;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    # getter for: Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->autoValidate:Z
    invoke-static {v0}, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->access$100(Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 428
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$1;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->validate()Z

    goto :goto_19

    .line 430
    :cond_13
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$1;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->setError(Ljava/lang/CharSequence;)V

    .line 432
    :goto_19
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView$1;->this$0:Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;

    invoke-virtual {v0}, Lbr/com/itau/widgets/material/MaterialAutoCompleteTextView;->postInvalidate()V

    .line 433
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .line 418
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .line 422
    return-void
.end method

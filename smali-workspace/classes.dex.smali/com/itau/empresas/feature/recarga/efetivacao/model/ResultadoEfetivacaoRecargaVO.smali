.class public Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;
.super Ljava/lang/Object;
.source "ResultadoEfetivacaoRecargaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field cliente:Lcom/itau/empresas/api/model/DadosClienteVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cliente"
    .end annotation
.end field

.field comprovante:Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "comprovante"
    .end annotation
.end field

.field recarga:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "recarga"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCliente()Lcom/itau/empresas/api/model/DadosClienteVO;
    .registers 2

    .line 18
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;->cliente:Lcom/itau/empresas/api/model/DadosClienteVO;

    return-object v0
.end method

.method public getComprovante()Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;
    .registers 2

    .line 22
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;->comprovante:Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;

    return-object v0
.end method

.method public getRecarga()Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;
    .registers 2

    .line 30
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoEfetivacaoRecargaVO;->recarga:Lcom/itau/empresas/feature/recarga/efetivacao/model/ResultadoRecargaVO;

    return-object v0
.end method

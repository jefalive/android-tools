.class public abstract Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$OnTokenAction;
    }
.end annotation


# static fields
.field private static final $:[B

.field private static $$:I = 0x0

.field public static final CANCELED:I = 0x2

.field public static final INVALID:I = 0x1

.field public static final VALID:I


# instance fields
.field protected loading:Landroid/view/View;

.field private onActionListener:Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$OnTokenAction;


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    mul-int/lit8 p2, p2, 0xe

    rsub-int/lit8 p2, p2, 0x1a

    const/4 v4, -0x1

    sget-object v5, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->$:[B

    new-instance v0, Ljava/lang/String;

    mul-int/lit8 p0, p0, 0x24

    add-int/lit8 p0, p0, 0x45

    mul-int/lit8 p1, p1, 0x19

    rsub-int/lit8 p1, p1, 0x1d

    new-array v1, p2, [B

    add-int/lit8 p2, p2, -0x1

    if-nez v5, :cond_1f

    move v2, p2

    move v3, p1

    :goto_19
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p1, p1, 0x1

    add-int/lit8 p0, v2, 0x9

    :cond_1f
    add-int/lit8 v4, v4, 0x1

    int-to-byte v2, p0

    aput-byte v2, v1, v4

    if-ne v4, p2, :cond_2b

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_2b
    move v2, p0

    aget-byte v3, v5, p1

    goto :goto_19
.end method

.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x28

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->$:[B

    const/16 v0, 0xd2

    sput v0, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->$$:I

    return-void

    :array_e
    .array-data 1
        0x54t
        0x12t
        0x44t
        -0x51t
        -0x24t
        0x9t
        0xct
        0x58t
        -0x45t
        0x16t
        0x4at
        -0x4dt
        0x1et
        -0x2t
        0xct
        0xet
        0xct
        -0x7dt
        0xdt
        0x7dt
        0x58t
        -0x3bt
        -0x2t
        0x58t
        -0x4bt
        0xet
        0xdt
        0xft
        0x0t
        0x4t
        0x7t
        0x4t
        0xat
        0x1et
        -0x5t
        0x11t
        -0x6t
        0x15t
        0x2t
        0x14t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$lambda$0(Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;)V
    .registers 1

    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->lambda$hideLoading$0()V

    return-void
.end method

.method static synthetic access$lambda$1(Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;Landroid/widget/EditText;)V
    .registers 2

    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->lambda$showKeyboard$1(Landroid/widget/EditText;)V

    return-void
.end method

.method static synthetic access$lambda$2(Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;Ljava/lang/String;)V
    .registers 2

    invoke-direct {p0, p1}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->lambda$handleError$2(Ljava/lang/String;)V

    return-void
.end method

.method private getInputMethodManager()Landroid/view/inputmethod/InputMethodManager;
    .registers 5

    .line 127
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->currentActivity()Landroid/app/Activity;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->$:[B

    const/16 v2, 0x26

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->$:[B

    const/16 v3, 0x1c

    aget-byte v2, v2, v3

    add-int/lit8 v3, v2, 0x1

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->$(III)Ljava/lang/String;

    move-result-object v1

    .line 128
    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 126
    return-object v0
.end method

.method private synthetic lambda$handleError$2(Ljava/lang/String;)V
    .registers 2

    .line 155
    invoke-virtual {p0, p1}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->showError(Ljava/lang/String;)V

    return-void
.end method

.method private synthetic lambda$hideLoading$0()V
    .registers 4

    .line 78
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->loading:Landroid/view/View;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->loading:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_f

    .line 79
    :cond_e
    return-void

    .line 80
    :cond_f
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->loading:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 81
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 82
    const-wide/16 v1, 0x190

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$2;

    invoke-direct {v1, p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$2;-><init>(Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;)V

    .line 83
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 89
    return-void
.end method

.method private synthetic lambda$showKeyboard$1(Landroid/widget/EditText;)V
    .registers 4

    .line 120
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->getInputMethodManager()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    .line 121
    const/4 v0, 0x1

    invoke-virtual {v1, p1, v0}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 122
    return-void
.end method


# virtual methods
.method public handleError(Lbr/com/itau/sdk/android/core/exception/BackendException;)V
    .registers 9

    .line 133
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    if-eqz v0, :cond_1d

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1d

    .line 134
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v4

    goto :goto_2f

    .line 136
    :cond_1d
    sget-object v0, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->$:[B

    const/16 v1, 0x1c

    aget-byte v0, v0, v1

    add-int/lit8 v1, v0, 0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->$:[B

    const/16 v3, 0x1c

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->$(III)Ljava/lang/String;

    move-result-object v4

    .line 139
    :goto_2f
    const-class v0, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, v4}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 141
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    .line 143
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->hideLoading()V

    .line 145
    instance-of v0, v5, Lbr/com/itau/sdk/android/core/exception/TokenException;

    if-eqz v0, :cond_58

    .line 147
    move-object v6, v5

    check-cast v6, Lbr/com/itau/sdk/android/core/exception/TokenException;

    .line 149
    invoke-virtual {v6}, Lbr/com/itau/sdk/android/core/exception/TokenException;->getType()Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/exception/TokenException$Type;->CANCELED:Lbr/com/itau/sdk/android/core/exception/TokenException$Type;

    if-ne v0, v1, :cond_58

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_58

    .line 150
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->dismissAllowingStateLoss()V

    .line 151
    return-void

    .line 155
    :cond_58
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->currentActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {p0, v4}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$$Lambda$3;->lambdaFactory$(Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;Ljava/lang/String;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 156
    return-void
.end method

.method protected hideKeyboard()V
    .registers 6

    .line 105
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->getView()Landroid/view/View;

    move-result-object v2

    .line 107
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    .line 108
    if-nez v2, :cond_10

    if-eqz v3, :cond_10

    .line 109
    invoke-virtual {v3}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    .line 111
    :cond_10
    if-nez v2, :cond_1e

    .line 112
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->currentActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    .line 114
    :cond_1e
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->getInputMethodManager()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v4

    .line 115
    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 116
    return-void
.end method

.method public hideLoading()V
    .registers 3

    .line 77
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->currentActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$$Lambda$1;->lambdaFactory$(Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 92
    return-void
.end method

.method public onAction(I)V
    .registers 3

    .line 95
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->onActionListener:Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$OnTokenAction;

    if-eqz v0, :cond_9

    .line 96
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->onActionListener:Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$OnTokenAction;

    invoke-interface {v0, p1}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$OnTokenAction;->onAction(I)V

    .line 98
    :cond_9
    return-void
.end method

.method protected onBackPressed()V
    .registers 2

    .line 44
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->hideKeyboard()V

    .line 45
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->onAction(I)V

    .line 46
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->dismiss()V

    .line 47
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .registers 3

    .line 62
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->onAction(I)V

    .line 63
    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .registers 4

    .line 51
    new-instance v1, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$1;

    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v1, p0, v0, p1}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$1;-><init>(Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;Landroid/content/Context;I)V

    .line 57
    return-object v1
.end method

.method public setListener(Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$OnTokenAction;)V
    .registers 2

    .line 101
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->onActionListener:Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$OnTokenAction;

    .line 102
    return-void
.end method

.method public showError(Ljava/lang/String;)V
    .registers 3

    .line 159
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->getSdkCustomUi()Lbr/com/itau/sdk/android/core/SDKCustomUi;

    move-result-object v0

    invoke-interface {v0, p1}, Lbr/com/itau/sdk/android/core/SDKCustomUi;->onError(Ljava/lang/String;)V

    .line 160
    return-void
.end method

.method protected showKeyboard(Landroid/widget/EditText;)V
    .registers 3

    .line 119
    invoke-static {p0, p1}, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog$$Lambda$2;->lambdaFactory$(Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;Landroid/widget/EditText;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    .line 123
    return-void
.end method

.method public showLoading()V
    .registers 4

    .line 66
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->loading:Landroid/view/View;

    if-eqz v0, :cond_26

    .line 67
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->loading:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 68
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->loading:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 69
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/token/ui/BaseDialog;->loading:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 70
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 71
    const-wide/16 v1, 0x190

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 72
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 74
    :cond_26
    return-void
.end method

.class public final Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView_;
.super Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView;
.source "ItemDestinatarioComprovanteView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 35
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView;-><init>(Landroid/content/Context;)V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView_;->alreadyInflated_:Z

    .line 32
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 36
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView_;->init_()V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 40
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView_;->alreadyInflated_:Z

    .line 32
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 41
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView_;->init_()V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 45
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView_;->alreadyInflated_:Z

    .line 32
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 46
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView_;->init_()V

    .line 47
    return-void
.end method

.method private init_()V
    .registers 3

    .line 77
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 78
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 79
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 80
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 68
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 69
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView_;->alreadyInflated_:Z

    .line 70
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0300fd

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 73
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView;->onFinishInflate()V

    .line 74
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 102
    const v0, 0x7f0e0543

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/material/MaterialEditText;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView_;->textoItemDestinatario:Lbr/com/itau/widgets/material/MaterialEditText;

    .line 103
    const v0, 0x7f0e0544

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItemDestinatarioComprovanteView_;->icFechar:Landroid/widget/ImageButton;

    .line 104
    return-void
.end method

.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils;
.super Ljava/lang/Object;
.source "AnimationsUtils.java"


# static fields
.field public static final ANIMATION_DURATION:I = 0x12c


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static animateAlpha(Landroid/view/View;IZ)V
    .registers 6
    .param p0, "view"    # Landroid/view/View;
    .param p1, "duration"    # I
    .param p2, "show"    # Z

    .line 81
    if-eqz p2, :cond_22

    .line 82
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 83
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 85
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 86
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v1, p1

    .line 87
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 88
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_3c

    .line 91
    :cond_22
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 92
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v1, p1

    .line 93
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$3;

    invoke-direct {v1, p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$3;-><init>(Landroid/view/View;)V

    .line 94
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 102
    :goto_3c
    return-void
.end method

.method public static animateAlpha(Landroid/view/View;Z)V
    .registers 3
    .param p0, "view"    # Landroid/view/View;
    .param p1, "show"    # Z

    .line 77
    const/16 v0, 0x12c

    invoke-static {p0, v0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils;->animateAlpha(Landroid/view/View;IZ)V

    .line 78
    return-void
.end method

.method public static animateBackgroundColorChange(Landroid/view/View;III)V
    .registers 10
    .param p0, "view"    # Landroid/view/View;
    .param p1, "fromColor"    # I
    .param p2, "newColor"    # I
    .param p3, "duration"    # I

    .line 42
    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 43
    .local v4, "background":Landroid/graphics/drawable/Drawable;
    instance-of v0, v4, Landroid/graphics/drawable/ColorDrawable;

    if-eqz v0, :cond_f

    .line 44
    move-object v0, v4

    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result p1

    .line 45
    :cond_f
    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Landroid/animation/ValueAnimator;->ofObject(Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ValueAnimator;

    move-result-object v5

    .line 46
    .local v5, "colorAnimation":Landroid/animation/ValueAnimator;
    int-to-long v0, p3

    invoke-virtual {v5, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 47
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$2;

    invoke-direct {v0, p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$2;-><init>(Landroid/view/View;)V

    invoke-virtual {v5, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 54
    invoke-virtual {v5}, Landroid/animation/ValueAnimator;->start()V

    .line 55
    return-void
.end method

.method public static animateProgressBar(Landroid/widget/ProgressBar;FF)V
    .registers 6
    .param p0, "progressBar"    # Landroid/widget/ProgressBar;
    .param p1, "from"    # F
    .param p2, "to"    # F

    .line 202
    new-instance v2, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$6;

    invoke-direct {v2, p0, p1, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$6;-><init>(Landroid/widget/ProgressBar;FF)V

    .line 208
    .local v2, "animation":Landroid/view/animation/Animation;
    const-wide/16 v0, 0x1f4

    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 210
    invoke-virtual {p0, v2}, Landroid/widget/ProgressBar;->startAnimation(Landroid/view/animation/Animation;)V

    .line 211
    return-void
.end method

.method public static animateTextColorChange(Landroid/widget/TextView;I)V
    .registers 8
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "newColor"    # I

    .line 214
    invoke-virtual {p0}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 215
    .local v4, "colorFrom":Ljava/lang/Integer;
    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v4, v1, v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Landroid/animation/ValueAnimator;->ofObject(Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ValueAnimator;

    move-result-object v5

    .line 216
    .local v5, "colorAnimation":Landroid/animation/ValueAnimator;
    const-wide/16 v0, 0x12c

    invoke-virtual {v5, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 217
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$7;

    invoke-direct {v0, p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$7;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v5, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 224
    invoke-virtual {v5}, Landroid/animation/ValueAnimator;->start()V

    .line 225
    return-void
.end method

.method public static animateTextColorChange(Landroid/widget/TextView;II)V
    .registers 9
    .param p0, "text"    # Landroid/widget/TextView;
    .param p1, "newColor"    # I
    .param p2, "duration"    # I

    .line 28
    invoke-virtual {p0}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 29
    .local v4, "colorFrom":Ljava/lang/Integer;
    new-instance v0, Landroid/animation/ArgbEvaluator;

    invoke-direct {v0}, Landroid/animation/ArgbEvaluator;-><init>()V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v4, v1, v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Landroid/animation/ValueAnimator;->ofObject(Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ValueAnimator;

    move-result-object v5

    .line 30
    .local v5, "colorAnimation":Landroid/animation/ValueAnimator;
    int-to-long v0, p2

    invoke-virtual {v5, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 31
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$1;

    invoke-direct {v0, p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$1;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v5, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 38
    invoke-virtual {v5}, Landroid/animation/ValueAnimator;->start()V

    .line 39
    return-void
.end method

.method public static blendColors(IID)I
    .registers 16
    .param p0, "from"    # I
    .param p1, "to"    # I
    .param p2, "ratio"    # D

    .line 58
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    sub-double v4, v0, p2

    .line 60
    .local v4, "inverseRatio":D
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v0

    int-to-double v0, v0

    mul-double/2addr v0, p2

    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v2, v4

    add-double v6, v0, v2

    .line 61
    .local v6, "r":D
    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v0

    int-to-double v0, v0

    mul-double/2addr v0, p2

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v2, v4

    add-double v8, v0, v2

    .line 62
    .local v8, "g":D
    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    int-to-double v0, v0

    mul-double/2addr v0, p2

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v2, v4

    add-double v10, v0, v2

    .line 64
    .local v10, "b":D
    double-to-int v0, v6

    double-to-int v1, v8

    double-to-int v2, v10

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    return v0
.end method

.method public static collapseAnimation(Landroid/view/View;I)V
    .registers 3
    .param p0, "view"    # Landroid/view/View;
    .param p1, "duration"    # I

    .line 138
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils;->collapseAnimation(Landroid/view/View;ILandroid/view/animation/Animation$AnimationListener;)V

    .line 139
    return-void
.end method

.method public static collapseAnimation(Landroid/view/View;ILandroid/view/animation/Animation$AnimationListener;)V
    .registers 7
    .param p0, "view"    # Landroid/view/View;
    .param p1, "duration"    # I
    .param p2, "listener"    # Landroid/view/animation/Animation$AnimationListener;

    .line 142
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 144
    .local v2, "initialHeight":I
    new-instance v3, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$5;

    invoke-direct {v3, p0, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$5;-><init>(Landroid/view/View;I)V

    .line 162
    .local v3, "a":Landroid/view/animation/Animation;
    int-to-long v0, p1

    invoke-virtual {v3, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 163
    invoke-virtual {v3, p2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 164
    new-instance v0, Landroid/support/v4/view/animation/FastOutSlowInInterpolator;

    invoke-direct {v0}, Landroid/support/v4/view/animation/FastOutSlowInInterpolator;-><init>()V

    invoke-virtual {v3, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 165
    invoke-virtual {p0, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 166
    return-void
.end method

.method public static crossfade(Landroid/view/View;Landroid/view/View;)V
    .registers 3
    .param p0, "viewToShow"    # Landroid/view/View;
    .param p1, "viewToHide"    # Landroid/view/View;

    .line 68
    const/16 v0, 0x12c

    invoke-static {p0, p1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils;->crossfade(Landroid/view/View;Landroid/view/View;I)V

    .line 69
    return-void
.end method

.method public static crossfade(Landroid/view/View;Landroid/view/View;I)V
    .registers 4
    .param p0, "viewToShow"    # Landroid/view/View;
    .param p1, "viewToHide"    # Landroid/view/View;
    .param p2, "duration"    # I

    .line 72
    const/4 v0, 0x1

    invoke-static {p0, p2, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils;->animateAlpha(Landroid/view/View;IZ)V

    .line 73
    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils;->animateAlpha(Landroid/view/View;IZ)V

    .line 74
    return-void
.end method

.method public static expandAnimation(Landroid/view/View;I)V
    .registers 3
    .param p0, "view"    # Landroid/view/View;
    .param p1, "duration"    # I

    .line 106
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils;->expandAnimation(Landroid/view/View;ILandroid/view/animation/Animation$AnimationListener;)V

    .line 107
    return-void
.end method

.method public static expandAnimation(Landroid/view/View;ILandroid/view/animation/Animation$AnimationListener;)V
    .registers 7
    .param p0, "view"    # Landroid/view/View;
    .param p1, "duration"    # I
    .param p2, "animationListener"    # Landroid/view/animation/Animation$AnimationListener;

    .line 110
    const/4 v0, -0x1

    const/4 v1, -0x2

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->measure(II)V

    .line 111
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 113
    .local v2, "targetHeight":I
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 114
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 115
    new-instance v3, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$4;

    invoke-direct {v3, p0, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/AnimationsUtils$4;-><init>(Landroid/view/View;I)V

    .line 128
    .local v3, "a":Landroid/view/animation/Animation;
    int-to-long v0, p1

    invoke-virtual {v3, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 129
    new-instance v0, Landroid/support/v4/view/animation/FastOutSlowInInterpolator;

    invoke-direct {v0}, Landroid/support/v4/view/animation/FastOutSlowInInterpolator;-><init>()V

    invoke-virtual {v3, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 130
    if-eqz p2, :cond_2a

    .line 131
    invoke-virtual {v3, p2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 133
    :cond_2a
    invoke-virtual {p0, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 135
    return-void
.end method

.method public static fadeAnimation(Landroid/view/View;FFI)V
    .registers 7
    .param p0, "view"    # Landroid/view/View;
    .param p1, "start"    # F
    .param p2, "finish"    # F
    .param p3, "duration"    # I

    .line 194
    new-instance v2, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v2, p1, p2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 195
    .local v2, "animation":Landroid/view/animation/Animation;
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 196
    int-to-long v0, p3

    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 197
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 198
    invoke-virtual {p0, v2}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 199
    return-void
.end method

.method public static flipViewAnimation(Landroid/view/View;Landroid/view/View;)V
    .registers 8
    .param p0, "v1"    # Landroid/view/View;
    .param p1, "v2"    # Landroid/view/View;

    .line 181
    sget-object v0, Landroid/view/View;->ROTATION_X:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_42

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 182
    .local v4, "visToInvis":Landroid/animation/ObjectAnimator;
    const-wide/16 v0, 0xfa

    invoke-virtual {v4, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 183
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v4, v0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 184
    sget-object v0, Landroid/view/View;->ROTATION_X:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_4a

    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 185
    .local v5, "invisToVis":Landroid/animation/ObjectAnimator;
    const-wide/16 v0, 0xfa

    invoke-virtual {v5, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 186
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v5, v0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 187
    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    .line 188
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 189
    invoke-virtual {v5}, Landroid/animation/ObjectAnimator;->start()V

    .line 190
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 191
    return-void

    nop

    :array_42
    .array-data 4
        0x0
        0x42b40000    # 90.0f
    .end array-data

    :array_4a
    .array-data 4
        -0x3d4c0000    # -90.0f
        0x0
    .end array-data
.end method

.method public static rotateViewAnimation(Landroid/view/View;III)V
    .registers 13
    .param p0, "v"    # Landroid/view/View;
    .param p1, "fromDegreess"    # I
    .param p2, "toDegress"    # I
    .param p3, "duration"    # I

    .line 169
    new-instance v7, Landroid/view/animation/AnimationSet;

    const/4 v0, 0x1

    invoke-direct {v7, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 170
    .local v7, "animationSet":Landroid/view/animation/AnimationSet;
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v7, v0}, Landroid/view/animation/AnimationSet;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 171
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 172
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Landroid/view/animation/AnimationSet;->setFillEnabled(Z)V

    .line 173
    new-instance v0, Landroid/view/animation/RotateAnimation;

    int-to-float v1, p1

    int-to-float v2, p2

    const/4 v3, 0x1

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    move-object v8, v0

    .line 174
    .local v8, "rotateAnimation":Landroid/view/animation/RotateAnimation;
    int-to-long v0, p3

    invoke-virtual {v8, v0, v1}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 175
    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V

    .line 176
    invoke-virtual {v7, v8}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 177
    invoke-virtual {p0, v7}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 178
    return-void
.end method

.class public final Lbr/com/itau/sdk/android/core/model/FeedbackRequestDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final $:[B

.field private static $$:I


# instance fields
.field private guid:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Guid"
    .end annotation
.end field

.field private params:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "Params"
    .end annotation
.end field

.field private splitUsing:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "SplitUsing"
    .end annotation
.end field


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    sget-object v5, Lbr/com/itau/sdk/android/core/model/FeedbackRequestDTO;->$:[B

    mul-int/lit8 p2, p2, 0x3

    add-int/lit8 p2, p2, 0x25

    mul-int/lit8 p0, p0, 0x2

    rsub-int/lit8 p0, p0, 0x1

    const/4 v4, 0x0

    add-int/lit8 p1, p1, 0x4

    new-instance v0, Ljava/lang/String;

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v5, :cond_1a

    move v2, p2

    move v3, p0

    :goto_17
    neg-int v3, v3

    add-int p2, v2, v3

    :cond_1a
    int-to-byte v2, p2

    aput-byte v2, v1, v4

    add-int/lit8 p1, p1, 0x1

    move v2, v4

    add-int/lit8 v4, v4, 0x1

    if-ne v2, p0, :cond_29

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_29
    move v2, p2

    aget-byte v3, v5, p1

    goto :goto_17
.end method

.method static constructor <clinit>()V
    .registers 3

    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/model/FeedbackRequestDTO;->$:[B

    const/16 v0, 0xa

    sput v0, Lbr/com/itau/sdk/android/core/model/FeedbackRequestDTO;->$$:I

    return-void

    nop

    :array_e
    .array-data 1
        0x55t
        -0x3t
        0x65t
        0x29t
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/model/FeedbackRequestDTO;->$(III)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/model/FeedbackRequestDTO;->splitUsing:Ljava/lang/String;

    .line 20
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/FeedbackRequestDTO;->guid:Ljava/lang/String;

    .line 21
    iput-object p2, p0, Lbr/com/itau/sdk/android/core/model/FeedbackRequestDTO;->params:Ljava/lang/String;

    .line 22
    return-void
.end method

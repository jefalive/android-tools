.class Lcom/itau/empresas/ui/activity/WebViewActivity$2;
.super Ljava/lang/Object;
.source "WebViewActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/ui/activity/WebViewActivity;->apresentaDialogoAtualizarWebView(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/ui/activity/WebViewActivity;

.field final synthetic val$dialog:Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

.field final synthetic val$pacote:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/ui/activity/WebViewActivity;Ljava/lang/String;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V
    .registers 4
    .param p1, "this$0"    # Lcom/itau/empresas/ui/activity/WebViewActivity;

    .line 246
    iput-object p1, p0, Lcom/itau/empresas/ui/activity/WebViewActivity$2;->this$0:Lcom/itau/empresas/ui/activity/WebViewActivity;

    iput-object p2, p0, Lcom/itau/empresas/ui/activity/WebViewActivity$2;->val$pacote:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/ui/activity/WebViewActivity$2;->val$dialog:Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 8
    .param p1, "view"    # Landroid/view/View;

    .line 249
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity$2;->this$0:Lcom/itau/empresas/ui/activity/WebViewActivity;

    iget-object v0, v0, Lcom/itau/empresas/ui/activity/WebViewActivity;->contexto:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 250
    .local v3, "pm":Landroid/content/pm/PackageManager;
    new-instance v4, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "https://play.google.com/store/apps/details?id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/ui/activity/WebViewActivity$2;->val$pacote:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 251
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v4, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 252
    .line 253
    .local v4, "loja":Landroid/content/Intent;
    const/high16 v0, 0x10000

    invoke-virtual {v3, v4, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    .line 255
    .local v5, "resolveInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3b

    .line 256
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity$2;->this$0:Lcom/itau/empresas/ui/activity/WebViewActivity;

    iget-object v0, v0, Lcom/itau/empresas/ui/activity/WebViewActivity;->contexto:Landroid/content/Context;

    invoke-virtual {v0, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 259
    :cond_3b
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/WebViewActivity$2;->val$dialog:Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->dismiss()V

    .line 260
    return-void
.end method

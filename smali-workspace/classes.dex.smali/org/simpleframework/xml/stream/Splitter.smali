.class abstract Lorg/simpleframework/xml/stream/Splitter;
.super Ljava/lang/Object;
.source "Splitter.java"


# instance fields
.field protected builder:Ljava/lang/StringBuilder;

.field protected count:I

.field protected off:I

.field protected text:[C


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 3
    .param p1, "source"    # Ljava/lang/String;

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/simpleframework/xml/stream/Splitter;->builder:Ljava/lang/StringBuilder;

    .line 76
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, Lorg/simpleframework/xml/stream/Splitter;->text:[C

    .line 77
    iget-object v0, p0, Lorg/simpleframework/xml/stream/Splitter;->text:[C

    array-length v0, v0

    iput v0, p0, Lorg/simpleframework/xml/stream/Splitter;->count:I

    .line 78
    return-void
.end method

.method private acronym()Z
    .registers 7

    .line 145
    iget v3, p0, Lorg/simpleframework/xml/stream/Splitter;->off:I

    .line 146
    .local v3, "mark":I
    const/4 v4, 0x0

    .line 148
    .local v4, "size":I
    :goto_3
    iget v0, p0, Lorg/simpleframework/xml/stream/Splitter;->count:I

    if-ge v3, v0, :cond_16

    .line 149
    iget-object v0, p0, Lorg/simpleframework/xml/stream/Splitter;->text:[C

    aget-char v5, v0, v3

    .line 151
    .local v5, "ch":C
    invoke-direct {p0, v5}, Lorg/simpleframework/xml/stream/Splitter;->isUpper(C)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 152
    add-int/lit8 v4, v4, 0x1

    .line 156
    add-int/lit8 v3, v3, 0x1

    .line 157
    .end local v5    # "ch":C
    goto :goto_3

    .line 158
    :cond_16
    const/4 v0, 0x1

    if-le v4, v0, :cond_38

    .line 159
    iget v0, p0, Lorg/simpleframework/xml/stream/Splitter;->count:I

    if-ge v3, v0, :cond_2b

    .line 160
    iget-object v0, p0, Lorg/simpleframework/xml/stream/Splitter;->text:[C

    add-int/lit8 v1, v3, -0x1

    aget-char v5, v0, v1

    .line 162
    .local v5, "ch":C
    invoke-direct {p0, v5}, Lorg/simpleframework/xml/stream/Splitter;->isUpper(C)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 163
    add-int/lit8 v3, v3, -0x1

    .line 166
    .end local v5    # "ch":C
    :cond_2b
    iget-object v0, p0, Lorg/simpleframework/xml/stream/Splitter;->text:[C

    iget v1, p0, Lorg/simpleframework/xml/stream/Splitter;->off:I

    iget v2, p0, Lorg/simpleframework/xml/stream/Splitter;->off:I

    sub-int v2, v3, v2

    invoke-virtual {p0, v0, v1, v2}, Lorg/simpleframework/xml/stream/Splitter;->commit([CII)V

    .line 167
    iput v3, p0, Lorg/simpleframework/xml/stream/Splitter;->off:I

    .line 169
    :cond_38
    const/4 v0, 0x1

    if-le v4, v0, :cond_3d

    const/4 v0, 0x1

    goto :goto_3e

    :cond_3d
    const/4 v0, 0x0

    :goto_3e
    return v0
.end method

.method private isDigit(C)Z
    .registers 3
    .param p1, "ch"    # C

    .line 237
    invoke-static {p1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v0

    return v0
.end method

.method private isLetter(C)Z
    .registers 3
    .param p1, "ch"    # C

    .line 211
    invoke-static {p1}, Ljava/lang/Character;->isLetter(C)Z

    move-result v0

    return v0
.end method

.method private isSpecial(C)Z
    .registers 3
    .param p1, "ch"    # C

    .line 224
    invoke-static {p1}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    goto :goto_9

    :cond_8
    const/4 v0, 0x0

    :goto_9
    return v0
.end method

.method private isUpper(C)Z
    .registers 3
    .param p1, "ch"    # C

    .line 250
    invoke-static {p1}, Ljava/lang/Character;->isUpperCase(C)Z

    move-result v0

    return v0
.end method

.method private number()Z
    .registers 7

    .line 181
    iget v3, p0, Lorg/simpleframework/xml/stream/Splitter;->off:I

    .line 182
    .local v3, "mark":I
    const/4 v4, 0x0

    .line 184
    .local v4, "size":I
    :goto_3
    iget v0, p0, Lorg/simpleframework/xml/stream/Splitter;->count:I

    if-ge v3, v0, :cond_16

    .line 185
    iget-object v0, p0, Lorg/simpleframework/xml/stream/Splitter;->text:[C

    aget-char v5, v0, v3

    .line 187
    .local v5, "ch":C
    invoke-direct {p0, v5}, Lorg/simpleframework/xml/stream/Splitter;->isDigit(C)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 188
    add-int/lit8 v4, v4, 0x1

    .line 192
    add-int/lit8 v3, v3, 0x1

    .line 193
    .end local v5    # "ch":C
    goto :goto_3

    .line 194
    :cond_16
    if-lez v4, :cond_23

    .line 195
    iget-object v0, p0, Lorg/simpleframework/xml/stream/Splitter;->text:[C

    iget v1, p0, Lorg/simpleframework/xml/stream/Splitter;->off:I

    iget v2, p0, Lorg/simpleframework/xml/stream/Splitter;->off:I

    sub-int v2, v3, v2

    invoke-virtual {p0, v0, v1, v2}, Lorg/simpleframework/xml/stream/Splitter;->commit([CII)V

    .line 197
    :cond_23
    iput v3, p0, Lorg/simpleframework/xml/stream/Splitter;->off:I

    .line 198
    if-lez v4, :cond_29

    const/4 v0, 0x1

    goto :goto_2a

    :cond_29
    const/4 v0, 0x0

    :goto_2a
    return v0
.end method

.method private token()V
    .registers 6

    .line 114
    iget v3, p0, Lorg/simpleframework/xml/stream/Splitter;->off:I

    .line 116
    .local v3, "mark":I
    :goto_2
    iget v0, p0, Lorg/simpleframework/xml/stream/Splitter;->count:I

    if-ge v3, v0, :cond_1f

    .line 117
    iget-object v0, p0, Lorg/simpleframework/xml/stream/Splitter;->text:[C

    aget-char v4, v0, v3

    .line 119
    .local v4, "ch":C
    invoke-direct {p0, v4}, Lorg/simpleframework/xml/stream/Splitter;->isLetter(C)Z

    move-result v0

    if-nez v0, :cond_11

    .line 120
    goto :goto_1f

    .line 122
    :cond_11
    iget v0, p0, Lorg/simpleframework/xml/stream/Splitter;->off:I

    if-le v3, v0, :cond_1c

    .line 123
    invoke-direct {p0, v4}, Lorg/simpleframework/xml/stream/Splitter;->isUpper(C)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 124
    goto :goto_1f

    .line 127
    :cond_1c
    add-int/lit8 v3, v3, 0x1

    .line 128
    .end local v4    # "ch":C
    goto :goto_2

    .line 129
    :cond_1f
    :goto_1f
    iget v0, p0, Lorg/simpleframework/xml/stream/Splitter;->off:I

    if-le v3, v0, :cond_39

    .line 130
    iget-object v0, p0, Lorg/simpleframework/xml/stream/Splitter;->text:[C

    iget v1, p0, Lorg/simpleframework/xml/stream/Splitter;->off:I

    iget v2, p0, Lorg/simpleframework/xml/stream/Splitter;->off:I

    sub-int v2, v3, v2

    invoke-virtual {p0, v0, v1, v2}, Lorg/simpleframework/xml/stream/Splitter;->parse([CII)V

    .line 131
    iget-object v0, p0, Lorg/simpleframework/xml/stream/Splitter;->text:[C

    iget v1, p0, Lorg/simpleframework/xml/stream/Splitter;->off:I

    iget v2, p0, Lorg/simpleframework/xml/stream/Splitter;->off:I

    sub-int v2, v3, v2

    invoke-virtual {p0, v0, v1, v2}, Lorg/simpleframework/xml/stream/Splitter;->commit([CII)V

    .line 133
    :cond_39
    iput v3, p0, Lorg/simpleframework/xml/stream/Splitter;->off:I

    .line 134
    return-void
.end method


# virtual methods
.method protected abstract commit([CII)V
.end method

.method protected abstract parse([CII)V
.end method

.method public process()Ljava/lang/String;
    .registers 4

    .line 89
    :cond_0
    :goto_0
    iget v0, p0, Lorg/simpleframework/xml/stream/Splitter;->off:I

    iget v1, p0, Lorg/simpleframework/xml/stream/Splitter;->count:I

    if-ge v0, v1, :cond_2d

    .line 90
    :goto_6
    iget v0, p0, Lorg/simpleframework/xml/stream/Splitter;->off:I

    iget v1, p0, Lorg/simpleframework/xml/stream/Splitter;->count:I

    if-ge v0, v1, :cond_20

    .line 91
    iget-object v0, p0, Lorg/simpleframework/xml/stream/Splitter;->text:[C

    iget v1, p0, Lorg/simpleframework/xml/stream/Splitter;->off:I

    aget-char v2, v0, v1

    .line 93
    .local v2, "ch":C
    invoke-direct {p0, v2}, Lorg/simpleframework/xml/stream/Splitter;->isSpecial(C)Z

    move-result v0

    if-nez v0, :cond_19

    .line 94
    goto :goto_20

    .line 96
    :cond_19
    iget v0, p0, Lorg/simpleframework/xml/stream/Splitter;->off:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/simpleframework/xml/stream/Splitter;->off:I

    .line 97
    .end local v2    # "ch":C
    goto :goto_6

    .line 98
    :cond_20
    :goto_20
    invoke-direct {p0}, Lorg/simpleframework/xml/stream/Splitter;->acronym()Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    invoke-direct {p0}, Lorg/simpleframework/xml/stream/Splitter;->token()V

    .line 100
    invoke-direct {p0}, Lorg/simpleframework/xml/stream/Splitter;->number()Z

    goto :goto_0

    .line 103
    :cond_2d
    iget-object v0, p0, Lorg/simpleframework/xml/stream/Splitter;->builder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected toLower(C)C
    .registers 3
    .param p1, "ch"    # C

    .line 276
    invoke-static {p1}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    return v0
.end method

.method protected toUpper(C)C
    .registers 3
    .param p1, "ch"    # C

    .line 263
    invoke-static {p1}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    return v0
.end method

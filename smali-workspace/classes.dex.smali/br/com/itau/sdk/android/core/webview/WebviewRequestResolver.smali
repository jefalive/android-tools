.class public Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final $:[B

.field private static $$:I


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    sget-object v5, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->$:[B

    mul-int/lit8 p0, p0, 0x2a

    add-int/lit8 p0, p0, 0x1

    mul-int/lit8 p1, p1, 0x2

    rsub-int/lit8 p1, p1, 0x7d

    new-instance v0, Ljava/lang/String;

    mul-int/lit8 p2, p2, 0x2a

    rsub-int/lit8 p2, p2, 0x2d

    const/4 v4, 0x0

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v5, :cond_1d

    move v2, p2

    move v3, p1

    :goto_19
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p1, v2, 0x1

    :cond_1d
    add-int/lit8 p2, p2, 0x1

    int-to-byte v2, p1

    aput-byte v2, v1, v4

    move v2, v4

    add-int/lit8 v4, v4, 0x1

    if-ne v2, p0, :cond_2c

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_2c
    move v2, p1

    aget-byte v3, v5, p2

    goto :goto_19
.end method

.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x2e

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->$:[B

    const/16 v0, 0x53

    sput v0, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->$$:I

    return-void

    :array_e
    .array-data 1
        0x12t
        0x8t
        -0x5dt
        0x2t
        -0x2at
        0x55t
        -0x56t
        0x17t
        -0x11t
        0x54t
        -0x4dt
        0x0t
        -0x4t
        0x55t
        -0x4ft
        0x2t
        -0x3t
        0x1t
        0xbt
        0x8t
        -0x9t
        0x8t
        0x46t
        -0x42t
        -0xet
        0xet
        0x5t
        -0x12t
        0x10t
        0x46t
        -0x40t
        0x42t
        -0x29t
        -0x28t
        0x5t
        0x2t
        0x4ft
        -0x45t
        -0xbt
        0x4t
        0x3t
        0x4et
        -0x41t
        -0xct
        0xct
        -0x14t
    .end array-data
.end method

.method private constructor <init>()V
    .registers 1

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    return-void
.end method

.method public static getValueFromStoreWebviewKey(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 7

    .line 76
    instance-of v0, p0, Ljava/lang/String;

    if-eqz v0, :cond_69

    .line 77
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 79
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->$:[B

    const/16 v1, 0xb

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->$:[B

    const/16 v2, 0x11

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->$:[B

    const/16 v3, 0xb

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_69

    sget-object v0, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->$:[B

    const/16 v1, 0xb

    aget-byte v0, v0, v1

    invoke-static {v0, v0, v0}, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->$(III)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_69

    .line 80
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->$:[B

    const/16 v1, 0xb

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->$:[B

    const/16 v2, 0x11

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->$:[B

    const/16 v3, 0xb

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->$(III)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v4, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->$:[B

    const/16 v2, 0xb

    aget-byte v1, v1, v2

    invoke-static {v1, v1, v1}, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->$(III)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 81
    invoke-static {}, Lbr/com/itau/sdk/android/core/webview/MemoryStorageWebView;->getInstance()Lbr/com/itau/sdk/android/core/webview/MemoryStorageWebView;

    move-result-object v0

    invoke-virtual {v0, v5}, Lbr/com/itau/sdk/android/core/webview/MemoryStorageWebView;->findItem(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 85
    :cond_69
    return-object p0
.end method

.method public static resolveObject(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 61
    invoke-virtual {p0}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 62
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 63
    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 65
    instance-of v0, v3, Lorg/json/JSONObject;

    if-eqz v0, :cond_25

    .line 66
    move-object v0, v3

    check-cast v0, Lorg/json/JSONObject;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->resolveObject(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_2c

    .line 68
    :cond_25
    invoke-static {v3}, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->getValueFromStoreWebviewKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 70
    :goto_2c
    goto :goto_4

    .line 72
    :cond_2d
    return-object p0
.end method

.method public static resolveRequest(Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;)Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;
    .registers 7

    .line 27
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;->getHeader()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2b

    .line 28
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;->getHeader()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_e
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lbr/com/itau/sdk/android/core/model/KeyValue;

    .line 29
    invoke-virtual {v5}, Lbr/com/itau/sdk/android/core/model/KeyValue;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->getValueFromStoreWebviewKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lbr/com/itau/sdk/android/core/model/KeyValue;->setValue(Ljava/lang/String;)V

    .line 30
    goto :goto_e

    .line 33
    :cond_2b
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;->getPath()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_56

    .line 34
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;->getPath()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_39
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_56

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lbr/com/itau/sdk/android/core/model/KeyValue;

    .line 36
    invoke-virtual {v5}, Lbr/com/itau/sdk/android/core/model/KeyValue;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->getValueFromStoreWebviewKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lbr/com/itau/sdk/android/core/model/KeyValue;->setValue(Ljava/lang/String;)V

    .line 37
    goto :goto_39

    .line 40
    :cond_56
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;->getQuery()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_81

    .line 41
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;->getQuery()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_64
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_81

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lbr/com/itau/sdk/android/core/model/KeyValue;

    .line 43
    invoke-virtual {v5}, Lbr/com/itau/sdk/android/core/model/KeyValue;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->getValueFromStoreWebviewKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lbr/com/itau/sdk/android/core/model/KeyValue;->setValue(Ljava/lang/String;)V

    .line 44
    goto :goto_64

    .line 47
    :cond_81
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;->getBody()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_ca

    .line 49
    :try_start_87
    new-instance v0, Lcom/google/gson/JsonParser;

    invoke-direct {v0}, Lcom/google/gson/JsonParser;-><init>()V

    new-instance v1, Lorg/json/JSONObject;

    new-instance v2, Lcom/google/gson/Gson;

    invoke-direct {v2}, Lcom/google/gson/Gson;-><init>()V

    .line 50
    invoke-virtual {p0}, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;->getBody()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 49
    invoke-static {v1}, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->resolveObject(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v1

    .line 50
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    .line 49
    invoke-virtual {v0, v1}, Lcom/google/gson/JsonParser;->parse(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/google/gson/JsonObject;

    .line 51
    invoke-virtual {p0, v4}, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;->setBody(Ljava/lang/Object;)V
    :try_end_b0
    .catch Lorg/json/JSONException; {:try_start_87 .. :try_end_b0} :catch_b1

    .line 54
    goto :goto_ca

    .line 52
    :catch_b1
    move-exception v4

    .line 53
    new-instance v0, Ljava/lang/RuntimeException;

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->$:[B

    const/16 v2, 0x11

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->$:[B

    const/16 v3, 0x11

    aget-byte v2, v2, v3

    const/16 v3, 0x1a

    invoke-static {v1, v3, v2}, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_ca
    :goto_ca
    return-object p0
.end method

.method public static resolveRequest(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;
    .registers 3

    .line 21
    new-instance v1, Lcom/google/gson/Gson;

    invoke-direct {v1}, Lcom/google/gson/Gson;-><init>()V

    .line 22
    const-class v0, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;

    invoke-virtual {v1, p0, v0}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/webview/WebviewRequestResolver;->resolveRequest(Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;)Lbr/com/itau/sdk/android/core/model/WebViewRequestDTO;

    move-result-object v0

    return-object v0
.end method

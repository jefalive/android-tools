.class public Lnet/hockeyapp/android/utils/FeedbackParser;
.super Ljava/lang/Object;
.source "FeedbackParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/hockeyapp/android/utils/FeedbackParser$1;,
        Lnet/hockeyapp/android/utils/FeedbackParser$FeedbackParserHolder;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .registers 1

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    return-void
.end method

.method synthetic constructor <init>(Lnet/hockeyapp/android/utils/FeedbackParser$1;)V
    .registers 2
    .param p1, "x0"    # Lnet/hockeyapp/android/utils/FeedbackParser$1;

    .line 50
    invoke-direct {p0}, Lnet/hockeyapp/android/utils/FeedbackParser;-><init>()V

    return-void
.end method

.method public static getInstance()Lnet/hockeyapp/android/utils/FeedbackParser;
    .registers 1

    .line 64
    sget-object v0, Lnet/hockeyapp/android/utils/FeedbackParser$FeedbackParserHolder;->INSTANCE:Lnet/hockeyapp/android/utils/FeedbackParser;

    return-object v0
.end method


# virtual methods
.method public parseFeedbackResponse(Ljava/lang/String;)Lnet/hockeyapp/android/objects/FeedbackResponse;
    .registers 36
    .param p1, "feedbackResponseJson"    # Ljava/lang/String;

    .line 74
    const/4 v3, 0x0

    .line 75
    .local v3, "feedbackResponse":Lnet/hockeyapp/android/objects/FeedbackResponse;
    const/4 v4, 0x0

    .line 76
    .local v4, "feedback":Lnet/hockeyapp/android/objects/Feedback;
    if-eqz p1, :cond_24c

    .line 78
    :try_start_4
    new-instance v5, Lorg/json/JSONObject;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 80
    .local v5, "jsonObject":Lorg/json/JSONObject;
    const-string v0, "feedback"

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 81
    .local v6, "feedbackObject":Lorg/json/JSONObject;
    new-instance v4, Lnet/hockeyapp/android/objects/Feedback;

    invoke-direct {v4}, Lnet/hockeyapp/android/objects/Feedback;-><init>()V

    .line 84
    const-string v0, "messages"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 85
    .local v7, "messagesArray":Lorg/json/JSONArray;
    const/4 v8, 0x0

    .line 87
    .local v8, "messages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lnet/hockeyapp/android/objects/FeedbackMessage;>;"
    const/4 v9, 0x0

    .line 88
    .local v9, "feedbackMessage":Lnet/hockeyapp/android/objects/FeedbackMessage;
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_1d3

    .line 89
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 91
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_2a
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v10, v0, :cond_1d3

    .line 92
    invoke-virtual {v7, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "subject"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v11

    .line 93
    .local v11, "subject":Ljava/lang/String;
    invoke-virtual {v7, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "text"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v12

    .line 94
    .local v12, "text":Ljava/lang/String;
    invoke-virtual {v7, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "oem"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v13

    .line 95
    .local v13, "oem":Ljava/lang/String;
    invoke-virtual {v7, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "model"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v14

    .line 96
    .local v14, "model":Ljava/lang/String;
    invoke-virtual {v7, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "os_version"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v15

    .line 97
    .local v15, "osVersion":Ljava/lang/String;
    invoke-virtual {v7, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "created_at"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v16

    .line 98
    .local v16, "createdAt":Ljava/lang/String;
    invoke-virtual {v7, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v17

    .line 99
    .local v17, "id":I
    invoke-virtual {v7, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "token"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v18

    .line 100
    .local v18, "token":Ljava/lang/String;
    invoke-virtual {v7, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "via"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v19

    .line 101
    .local v19, "via":I
    invoke-virtual {v7, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "user_string"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v20

    .line 102
    .local v20, "userString":Ljava/lang/String;
    invoke-virtual {v7, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "clean_text"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v21

    .line 103
    .local v21, "cleanText":Ljava/lang/String;
    invoke-virtual {v7, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "name"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v22

    .line 104
    .local v22, "name":Ljava/lang/String;
    invoke-virtual {v7, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "app_id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v23

    .line 106
    .local v23, "appId":Ljava/lang/String;
    invoke-virtual {v7, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "attachments"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v24

    .line 107
    .local v24, "jsonAttachments":Lorg/json/JSONArray;
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v25

    .line 108
    .local v25, "feedbackAttachments":Ljava/util/List;, "Ljava/util/List<Lnet/hockeyapp/android/objects/FeedbackAttachment;>;"
    if-eqz v24, :cond_18b

    .line 109
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 111
    const/16 v26, 0x0

    .local v26, "j":I
    :goto_f5
    invoke-virtual/range {v24 .. v24}, Lorg/json/JSONArray;->length()I

    move-result v0

    move/from16 v1, v26

    if-ge v1, v0, :cond_18b

    .line 112
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v27

    .line 113
    .local v27, "attachmentId":I
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "feedback_message_id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v28

    .line 114
    .local v28, "attachmentMessageId":I
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "file_name"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    .line 115
    .local v29, "filename":Ljava/lang/String;
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "url"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 116
    .local v30, "url":Ljava/lang/String;
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "created_at"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 117
    .local v31, "attachmentCreatedAt":Ljava/lang/String;
    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "updated_at"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    .line 119
    .local v32, "attachmentUpdatedAt":Ljava/lang/String;
    new-instance v33, Lnet/hockeyapp/android/objects/FeedbackAttachment;

    invoke-direct/range {v33 .. v33}, Lnet/hockeyapp/android/objects/FeedbackAttachment;-><init>()V

    .line 120
    .local v33, "feedbackAttachment":Lnet/hockeyapp/android/objects/FeedbackAttachment;
    move-object/from16 v0, v33

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/objects/FeedbackAttachment;->setId(I)V

    .line 121
    move-object/from16 v0, v33

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/objects/FeedbackAttachment;->setMessageId(I)V

    .line 122
    move-object/from16 v0, v33

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/objects/FeedbackAttachment;->setFilename(Ljava/lang/String;)V

    .line 123
    move-object/from16 v0, v33

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/objects/FeedbackAttachment;->setUrl(Ljava/lang/String;)V

    .line 124
    move-object/from16 v0, v33

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/objects/FeedbackAttachment;->setCreatedAt(Ljava/lang/String;)V

    .line 125
    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lnet/hockeyapp/android/objects/FeedbackAttachment;->setUpdatedAt(Ljava/lang/String;)V

    .line 126
    move-object/from16 v0, v25

    move-object/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    .end local v27    # "attachmentId":I
    .end local v28    # "attachmentMessageId":I
    .end local v29    # "filename":Ljava/lang/String;
    .end local v30    # "url":Ljava/lang/String;
    .end local v31    # "attachmentCreatedAt":Ljava/lang/String;
    .end local v32    # "attachmentUpdatedAt":Ljava/lang/String;
    .end local v33    # "feedbackAttachment":Lnet/hockeyapp/android/objects/FeedbackAttachment;
    add-int/lit8 v26, v26, 0x1

    goto/16 :goto_f5

    .line 130
    .end local v26    # "j":I
    :cond_18b
    new-instance v9, Lnet/hockeyapp/android/objects/FeedbackMessage;

    invoke-direct {v9}, Lnet/hockeyapp/android/objects/FeedbackMessage;-><init>()V

    .line 131
    move-object/from16 v0, v23

    invoke-virtual {v9, v0}, Lnet/hockeyapp/android/objects/FeedbackMessage;->setAppId(Ljava/lang/String;)V

    .line 132
    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Lnet/hockeyapp/android/objects/FeedbackMessage;->setCleanText(Ljava/lang/String;)V

    .line 133
    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Lnet/hockeyapp/android/objects/FeedbackMessage;->setCreatedAt(Ljava/lang/String;)V

    .line 134
    move/from16 v0, v17

    invoke-virtual {v9, v0}, Lnet/hockeyapp/android/objects/FeedbackMessage;->setId(I)V

    .line 135
    invoke-virtual {v9, v14}, Lnet/hockeyapp/android/objects/FeedbackMessage;->setModel(Ljava/lang/String;)V

    .line 136
    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Lnet/hockeyapp/android/objects/FeedbackMessage;->setName(Ljava/lang/String;)V

    .line 137
    invoke-virtual {v9, v13}, Lnet/hockeyapp/android/objects/FeedbackMessage;->setOem(Ljava/lang/String;)V

    .line 138
    invoke-virtual {v9, v15}, Lnet/hockeyapp/android/objects/FeedbackMessage;->setOsVersion(Ljava/lang/String;)V

    .line 139
    invoke-virtual {v9, v11}, Lnet/hockeyapp/android/objects/FeedbackMessage;->setSubjec(Ljava/lang/String;)V

    .line 140
    invoke-virtual {v9, v12}, Lnet/hockeyapp/android/objects/FeedbackMessage;->setText(Ljava/lang/String;)V

    .line 141
    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Lnet/hockeyapp/android/objects/FeedbackMessage;->setToken(Ljava/lang/String;)V

    .line 142
    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Lnet/hockeyapp/android/objects/FeedbackMessage;->setUserString(Ljava/lang/String;)V

    .line 143
    move/from16 v0, v19

    invoke-virtual {v9, v0}, Lnet/hockeyapp/android/objects/FeedbackMessage;->setVia(I)V

    .line 144
    move-object/from16 v0, v25

    invoke-virtual {v9, v0}, Lnet/hockeyapp/android/objects/FeedbackMessage;->setFeedbackAttachments(Ljava/util/List;)V

    .line 145
    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    .end local v11    # "subject":Ljava/lang/String;
    .end local v12    # "text":Ljava/lang/String;
    .end local v13    # "oem":Ljava/lang/String;
    .end local v14    # "model":Ljava/lang/String;
    .end local v15    # "osVersion":Ljava/lang/String;
    .end local v16    # "createdAt":Ljava/lang/String;
    .end local v17    # "id":I
    .end local v18    # "token":Ljava/lang/String;
    .end local v19    # "via":I
    .end local v20    # "userString":Ljava/lang/String;
    .end local v21    # "cleanText":Ljava/lang/String;
    .end local v22    # "name":Ljava/lang/String;
    .end local v23    # "appId":Ljava/lang/String;
    .end local v24    # "jsonAttachments":Lorg/json/JSONArray;
    .end local v25    # "feedbackAttachments":Ljava/util/List;, "Ljava/util/List<Lnet/hockeyapp/android/objects/FeedbackAttachment;>;"
    .end local v25
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_2a

    .line 149
    .end local v10    # "i":I
    :cond_1d3
    invoke-virtual {v4, v8}, Lnet/hockeyapp/android/objects/Feedback;->setMessages(Ljava/util/ArrayList;)V
    :try_end_1d6
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_1d6} :catch_248

    .line 152
    const-string v0, "name"

    :try_start_1d8
    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lnet/hockeyapp/android/objects/Feedback;->setName(Ljava/lang/String;)V
    :try_end_1e3
    .catch Lorg/json/JSONException; {:try_start_1d8 .. :try_end_1e3} :catch_1e4

    .line 155
    goto :goto_1e8

    .line 153
    :catch_1e4
    move-exception v10

    .line 154
    .local v10, "e":Lorg/json/JSONException;
    :try_start_1e5
    invoke-virtual {v10}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_1e8
    .catch Lorg/json/JSONException; {:try_start_1e5 .. :try_end_1e8} :catch_248

    .line 158
    .end local v10    # "e":Lorg/json/JSONException;
    :goto_1e8
    const-string v0, "email"

    :try_start_1ea
    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lnet/hockeyapp/android/objects/Feedback;->setEmail(Ljava/lang/String;)V
    :try_end_1f5
    .catch Lorg/json/JSONException; {:try_start_1ea .. :try_end_1f5} :catch_1f6

    .line 161
    goto :goto_1fa

    .line 159
    :catch_1f6
    move-exception v10

    .line 160
    .local v10, "e":Lorg/json/JSONException;
    :try_start_1f7
    invoke-virtual {v10}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_1fa
    .catch Lorg/json/JSONException; {:try_start_1f7 .. :try_end_1fa} :catch_248

    .line 164
    .end local v10    # "e":Lorg/json/JSONException;
    :goto_1fa
    const-string v0, "id"

    :try_start_1fc
    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v4, v0}, Lnet/hockeyapp/android/objects/Feedback;->setId(I)V
    :try_end_203
    .catch Lorg/json/JSONException; {:try_start_1fc .. :try_end_203} :catch_204

    .line 167
    goto :goto_208

    .line 165
    :catch_204
    move-exception v10

    .line 166
    .local v10, "e":Lorg/json/JSONException;
    :try_start_205
    invoke-virtual {v10}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_208
    .catch Lorg/json/JSONException; {:try_start_205 .. :try_end_208} :catch_248

    .line 170
    .end local v10    # "e":Lorg/json/JSONException;
    :goto_208
    const-string v0, "created_at"

    :try_start_20a
    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lnet/hockeyapp/android/objects/Feedback;->setCreatedAt(Ljava/lang/String;)V
    :try_end_215
    .catch Lorg/json/JSONException; {:try_start_20a .. :try_end_215} :catch_216

    .line 173
    goto :goto_21a

    .line 171
    :catch_216
    move-exception v10

    .line 172
    .local v10, "e":Lorg/json/JSONException;
    :try_start_217
    invoke-virtual {v10}, Lorg/json/JSONException;->printStackTrace()V

    .line 175
    .end local v10    # "e":Lorg/json/JSONException;
    :goto_21a
    new-instance v0, Lnet/hockeyapp/android/objects/FeedbackResponse;

    invoke-direct {v0}, Lnet/hockeyapp/android/objects/FeedbackResponse;-><init>()V

    move-object v3, v0

    .line 176
    invoke-virtual {v3, v4}, Lnet/hockeyapp/android/objects/FeedbackResponse;->setFeedback(Lnet/hockeyapp/android/objects/Feedback;)V
    :try_end_223
    .catch Lorg/json/JSONException; {:try_start_217 .. :try_end_223} :catch_248

    .line 178
    const-string v0, "status"

    :try_start_225
    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lnet/hockeyapp/android/objects/FeedbackResponse;->setStatus(Ljava/lang/String;)V
    :try_end_230
    .catch Lorg/json/JSONException; {:try_start_225 .. :try_end_230} :catch_231

    .line 181
    goto :goto_235

    .line 179
    :catch_231
    move-exception v10

    .line 180
    .local v10, "e":Lorg/json/JSONException;
    :try_start_232
    invoke-virtual {v10}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_235
    .catch Lorg/json/JSONException; {:try_start_232 .. :try_end_235} :catch_248

    .line 185
    .end local v10    # "e":Lorg/json/JSONException;
    :goto_235
    const-string v0, "token"

    :try_start_237
    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lnet/hockeyapp/android/objects/FeedbackResponse;->setToken(Ljava/lang/String;)V
    :try_end_242
    .catch Lorg/json/JSONException; {:try_start_237 .. :try_end_242} :catch_243

    .line 188
    goto :goto_247

    .line 186
    :catch_243
    move-exception v10

    .line 187
    .local v10, "e":Lorg/json/JSONException;
    :try_start_244
    invoke-virtual {v10}, Lorg/json/JSONException;->printStackTrace()V
    :try_end_247
    .catch Lorg/json/JSONException; {:try_start_244 .. :try_end_247} :catch_248

    .line 191
    .end local v5    # "jsonObject":Lorg/json/JSONObject;
    .end local v6    # "feedbackObject":Lorg/json/JSONObject;
    .end local v7    # "messagesArray":Lorg/json/JSONArray;
    .end local v8    # "messages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lnet/hockeyapp/android/objects/FeedbackMessage;>;"
    .end local v8
    .end local v9    # "feedbackMessage":Lnet/hockeyapp/android/objects/FeedbackMessage;
    .end local v10    # "e":Lorg/json/JSONException;
    :goto_247
    goto :goto_24c

    .line 189
    :catch_248
    move-exception v5

    .line 190
    .local v5, "e":Lorg/json/JSONException;
    invoke-virtual {v5}, Lorg/json/JSONException;->printStackTrace()V

    .line 194
    .end local v5    # "e":Lorg/json/JSONException;
    :cond_24c
    :goto_24c
    return-object v3
.end method

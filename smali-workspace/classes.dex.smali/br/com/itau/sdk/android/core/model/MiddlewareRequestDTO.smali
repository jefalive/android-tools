.class public final Lbr/com/itau/sdk/android/core/model/MiddlewareRequestDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final deviceId:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "deviceId"
    .end annotation
.end field

.field private final pStr:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "pStr"
    .end annotation
.end field

.field private final pSv:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "pSv"
    .end annotation
.end field

.field private final userId:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "userId"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/MiddlewareRequestDTO;->userId:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lbr/com/itau/sdk/android/core/model/MiddlewareRequestDTO;->deviceId:Ljava/lang/String;

    .line 27
    iput-object p3, p0, Lbr/com/itau/sdk/android/core/model/MiddlewareRequestDTO;->pStr:Ljava/lang/String;

    .line 28
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->urlUniversal()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/model/MiddlewareRequestDTO;->pSv:Ljava/lang/String;

    .line 29
    return-void
.end method


# virtual methods
.method public getDeviceId()Ljava/lang/String;
    .registers 2

    .line 40
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/MiddlewareRequestDTO;->deviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getUserId()Ljava/lang/String;
    .registers 2

    .line 36
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/MiddlewareRequestDTO;->userId:Ljava/lang/String;

    return-object v0
.end method

.method public getpStr()Ljava/lang/String;
    .registers 2

    .line 21
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/MiddlewareRequestDTO;->pStr:Ljava/lang/String;

    return-object v0
.end method

.method public getpSv()Ljava/lang/String;
    .registers 2

    .line 32
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/MiddlewareRequestDTO;->pSv:Ljava/lang/String;

    return-object v0
.end method

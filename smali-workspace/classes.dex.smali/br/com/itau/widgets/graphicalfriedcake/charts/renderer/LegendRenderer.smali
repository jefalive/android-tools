.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/Renderer;
.source "LegendRenderer.java"


# instance fields
.field protected mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

.field protected mLegendFormPaint:Landroid/graphics/Paint;

.field protected mLegendLabelPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;)V
    .registers 5
    .param p1, "viewPortHandler"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;
    .param p2, "legend"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    .line 36
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/Renderer;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;)V

    .line 38
    iput-object p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    .line 40
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    .line 41
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x41100000    # 9.0f

    invoke-static {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 42
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 44
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendFormPaint:Landroid/graphics/Paint;

    .line 45
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendFormPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 46
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendFormPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 47
    return-void
.end method


# virtual methods
.method public computeLegend(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;)V
    .registers 14
    .param p1, "data"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData<*>;)V"
        }
    .end annotation

    .line 74
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->isLegendCustom()Z

    move-result v0

    if-nez v0, :cond_11c

    .line 76
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 77
    .local v3, "labels":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 80
    .local v4, "colors":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_13
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getDataSetCount()I

    move-result v0

    if-ge v5, v0, :cond_e3

    .line 82
    invoke-virtual {p1, v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getDataSetByIndex(I)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    move-result-object v6

    .line 84
    .local v6, "dataSet":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<+Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
    invoke-virtual {v6}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getColors()Ljava/util/List;

    move-result-object v7

    .line 85
    .local v7, "clrs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-virtual {v6}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getEntryCount()I

    move-result v8

    .line 88
    .local v8, "entryCount":I
    instance-of v0, v6, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;

    if-eqz v0, :cond_6f

    move-object v0, v6

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->isStacked()Z

    move-result v0

    if-eqz v0, :cond_6f

    .line 90
    move-object v9, v6

    check-cast v9, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;

    .line 91
    .local v9, "bds":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;
    invoke-virtual {v9}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->getStackLabels()[Ljava/lang/String;

    move-result-object v10

    .line 93
    .local v10, "sLabels":[Ljava/lang/String;
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_3a
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-ge v11, v0, :cond_58

    invoke-virtual {v9}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->getStackSize()I

    move-result v0

    if-ge v11, v0, :cond_58

    .line 95
    array-length v0, v10

    rem-int v0, v11, v0

    aget-object v0, v10, v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    invoke-interface {v7, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    add-int/lit8 v11, v11, 0x1

    goto :goto_3a

    .line 99
    .end local v11    # "j":I
    :cond_58
    invoke-virtual {v9}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->getLabel()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6d

    .line 101
    const/4 v0, -0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    invoke-virtual {v9}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    .end local v9    # "bds":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/BarDataSet;
    .end local v10    # "sLabels":[Ljava/lang/String;
    :cond_6d
    goto/16 :goto_df

    :cond_6f
    instance-of v0, v6, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;

    if-eqz v0, :cond_b0

    .line 107
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getXVals()Ljava/util/List;

    move-result-object v9

    .line 108
    .local v9, "xVals":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object v10, v6

    check-cast v10, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;

    .line 110
    .local v10, "pds":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_7b
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-ge v11, v0, :cond_9a

    if-ge v11, v8, :cond_9a

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    if-ge v11, v0, :cond_9a

    .line 112
    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    invoke-interface {v7, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    add-int/lit8 v11, v11, 0x1

    goto :goto_7b

    .line 116
    .end local v11    # "j":I
    :cond_9a
    invoke-virtual {v10}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->getLabel()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_af

    .line 118
    const/4 v0, -0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    invoke-virtual {v10}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;->getLabel()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    .end local v9    # "xVals":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v9
    .end local v10    # "pds":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/PieDataSet;
    :cond_af
    goto :goto_df

    .line 124
    :cond_b0
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_b1
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-ge v9, v0, :cond_df

    if-ge v9, v8, :cond_df

    .line 127
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v9, v0, :cond_ca

    add-int/lit8 v0, v8, -0x1

    if-ge v9, v0, :cond_ca

    .line 129
    const/4 v0, 0x0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_d5

    .line 132
    :cond_ca
    invoke-virtual {p1, v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;->getDataSetByIndex(I)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;->getLabel()Ljava/lang/String;

    move-result-object v10

    .line 133
    .local v10, "label":Ljava/lang/String;
    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    .end local v10    # "label":Ljava/lang/String;
    :goto_d5
    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 124
    add-int/lit8 v9, v9, 0x1

    goto :goto_b1

    .line 80
    .end local v6    # "dataSet":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;, "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<+Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
    .end local v6
    .end local v7    # "clrs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v7
    .end local v8    # "entryCount":I
    .end local v9    # "j":I
    :cond_df
    :goto_df
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_13

    .line 141
    .end local v5    # "i":I
    :cond_e3
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getExtraColors()[I

    move-result-object v0

    if-eqz v0, :cond_112

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getExtraLabels()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_112

    .line 142
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getExtraColors()[I

    move-result-object v5

    array-length v6, v5

    const/4 v7, 0x0

    :goto_fb
    if-ge v7, v6, :cond_109

    aget v8, v5, v7

    .line 143
    .local v8, "color":I
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    .end local v8    # "color":I
    add-int/lit8 v7, v7, 0x1

    goto :goto_fb

    .line 144
    :cond_109
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getExtraLabels()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 147
    :cond_112
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0, v4}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->setComputedColors(Ljava/util/List;)V

    .line 148
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0, v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->setComputedLabels(Ljava/util/List;)V

    .line 151
    .end local v3    # "labels":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3
    .end local v4    # "colors":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v4
    :cond_11c
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    .line 153
    .local v3, "tf":Landroid/graphics/Typeface;
    if-eqz v3, :cond_129

    .line 154
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 156
    :cond_129
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getTextSize()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 157
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 160
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->calculateDimensions(Landroid/graphics/Paint;Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;)V

    .line 161
    return-void
.end method

.method protected drawForm(Landroid/graphics/Canvas;FFILbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;)V
    .registers 14
    .param p1, "c"    # Landroid/graphics/Canvas;
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "index"    # I
    .param p5, "legend"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    .line 386
    invoke-virtual {p5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getColors()[I

    move-result-object v0

    aget v0, v0, p4

    const/4 v1, -0x2

    if-ne v0, v1, :cond_a

    .line 387
    return-void

    .line 389
    :cond_a
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendFormPaint:Landroid/graphics/Paint;

    invoke-virtual {p5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getColors()[I

    move-result-object v1

    aget v1, v1, p4

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 391
    invoke-virtual {p5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getFormSize()F

    move-result v6

    .line 392
    .local v6, "formsize":F
    const/high16 v0, 0x40000000    # 2.0f

    div-float v7, v6, v0

    .line 394
    .local v7, "half":F
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer$1;->$SwitchMap$br$com$itau$widgets$graphicalfriedcake$charts$components$Legend$LegendForm:[I

    invoke-virtual {p5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getForm()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendForm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_50

    goto :goto_4e

    .line 396
    :pswitch_2d
    add-float v0, p2, v7

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendFormPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, p3, v7, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 397
    goto :goto_4e

    .line 399
    :pswitch_35
    move-object v0, p1

    move v1, p2

    sub-float v2, p3, v7

    add-float v3, p2, v6

    add-float v4, p3, v7

    iget-object v5, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendFormPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 400
    goto :goto_4e

    .line 402
    :pswitch_43
    move-object v0, p1

    move v1, p2

    move v2, p3

    add-float v3, p2, v6

    move v4, p3

    iget-object v5, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendFormPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 405
    :goto_4e
    return-void

    nop

    :pswitch_data_50
    .packed-switch 0x1
        :pswitch_2d
        :pswitch_35
        :pswitch_43
    .end packed-switch
.end method

.method protected drawLabel(Landroid/graphics/Canvas;FFLjava/lang/String;)V
    .registers 6
    .param p1, "c"    # Landroid/graphics/Canvas;
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "label"    # Ljava/lang/String;

    .line 416
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p4, p2, p3, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 417
    return-void
.end method

.method public getFormPaint()Landroid/graphics/Paint;
    .registers 2

    .line 64
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendFormPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public getLabelPaint()Landroid/graphics/Paint;
    .registers 2

    .line 55
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public renderLegend(Landroid/graphics/Canvas;)V
    .registers 34
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 165
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_b

    .line 166
    return-void

    .line 168
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v6

    .line 170
    .local v6, "tf":Landroid/graphics/Typeface;
    if-eqz v6, :cond_1c

    .line 171
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 173
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getTextSize()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 174
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getTextColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 176
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getLineHeight(Landroid/graphics/Paint;)F

    move-result v7

    .line 177
    .local v7, "labelLineHeight":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getLineSpacing(Landroid/graphics/Paint;)F

    move-result v0

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getYEntrySpace()F

    move-result v1

    add-float v8, v0, v1

    .line 178
    .local v8, "labelLineSpacing":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    const-string v1, "ABC"

    invoke-static {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->calcTextHeight(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    sub-float v9, v7, v0

    .line 180
    .local v9, "formYOffset":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getLabels()[Ljava/lang/String;

    move-result-object v10

    .line 181
    .local v10, "labels":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getColors()[I

    move-result-object v11

    .line 183
    .local v11, "colors":[I
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getFormToTextSpace()F

    move-result v12

    .line 184
    .local v12, "formToTextSpace":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getXEntrySpace()F

    move-result v13

    .line 185
    .local v13, "xEntrySpace":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getDirection()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    move-result-object v14

    .line 186
    .local v14, "direction":Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getFormSize()F

    move-result v15

    .line 189
    .local v15, "formSize":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getStackSpace()F

    move-result v16

    .line 193
    .local v16, "stackSpace":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getYOffset()F

    move-result v19

    .line 194
    .local v19, "yoffset":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getXOffset()F

    move-result v20

    .line 196
    .local v20, "xoffset":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getPosition()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    move-result-object v21

    .line 198
    .local v21, "legendPosition":Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer$1;->$SwitchMap$br$com$itau$widgets$graphicalfriedcake$charts$components$Legend$LegendPosition:[I

    invoke-virtual/range {v21 .. v21}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_360

    goto/16 :goto_35f

    .line 203
    :pswitch_c1
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentWidth()F

    move-result v22

    .line 207
    .local v22, "contentWidth":F
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->BELOW_CHART_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    move-object/from16 v1, v21

    if-ne v1, v0, :cond_e6

    .line 209
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentLeft()F

    move-result v0

    add-float v23, v0, v20

    .line 211
    .local v23, "originPosX":F
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->RIGHT_TO_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    if-ne v14, v0, :cond_111

    .line 212
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mNeededWidth:F

    add-float v23, v23, v0

    goto :goto_111

    .line 214
    .end local v23    # "originPosX":F
    :cond_e6
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->BELOW_CHART_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    move-object/from16 v1, v21

    if-ne v1, v0, :cond_103

    .line 215
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentRight()F

    move-result v0

    sub-float v23, v0, v20

    .line 217
    .local v23, "originPosX":F
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->LEFT_TO_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    if-ne v14, v0, :cond_111

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mNeededWidth:F

    sub-float v23, v23, v0

    goto :goto_111

    .line 221
    .end local v23    # "originPosX":F
    :cond_103
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentLeft()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v22, v1

    add-float v23, v0, v1

    .line 223
    .local v23, "originPosX":F
    :cond_111
    :goto_111
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getCalculatedLineSizes()[Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    move-result-object v24

    .line 224
    .local v24, "calculatedLineSizes":[Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getCalculatedLabelSizes()[Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;

    move-result-object v25

    .line 225
    .local v25, "calculatedLabelSizes":[Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getCalculatedLabelBreakPoints()[Ljava/lang/Boolean;

    move-result-object v26

    .line 227
    .local v26, "calculatedLabelBreakPoints":[Ljava/lang/Boolean;
    move/from16 v17, v23

    .line 228
    .local v17, "posX":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getChartHeight()F

    move-result v0

    sub-float v0, v0, v19

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    iget v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mNeededHeight:F

    sub-float v18, v0, v1

    .line 230
    .local v18, "posY":F
    const/16 v27, 0x0

    .line 232
    .local v27, "lineIndex":I
    const/16 v28, 0x0

    .local v28, "i":I
    array-length v0, v10

    move/from16 v29, v0

    .local v29, "count":I
    :goto_144
    move/from16 v0, v28

    move/from16 v1, v29

    if-ge v0, v1, :cond_1f1

    .line 233
    aget-object v0, v26, v28

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_158

    .line 234
    move/from16 v17, v23

    .line 235
    add-float v0, v7, v8

    add-float v18, v18, v0

    .line 238
    :cond_158
    cmpl-float v0, v17, v23

    if-nez v0, :cond_177

    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->BELOW_CHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    move-object/from16 v1, v21

    if-ne v1, v0, :cond_177

    .line 239
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->RIGHT_TO_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    if-ne v14, v0, :cond_16b

    aget-object v0, v24, v27

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;->width:F

    goto :goto_170

    :cond_16b
    aget-object v0, v24, v27

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;->width:F

    neg-float v0, v0

    :goto_170
    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    add-float v17, v17, v0

    .line 240
    add-int/lit8 v27, v27, 0x1

    .line 243
    :cond_177
    aget v0, v11, v28

    const/4 v1, -0x2

    if-eq v0, v1, :cond_17f

    const/16 v30, 0x1

    goto :goto_181

    :cond_17f
    const/16 v30, 0x0

    .line 244
    .local v30, "drawingForm":Z
    :goto_181
    aget-object v0, v10, v28

    if-nez v0, :cond_188

    const/16 v31, 0x1

    goto :goto_18a

    :cond_188
    const/16 v31, 0x0

    .line 246
    .local v31, "isStacked":Z
    :goto_18a
    if-eqz v30, :cond_1a9

    .line 247
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->RIGHT_TO_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    if-ne v14, v0, :cond_192

    .line 248
    sub-float v17, v17, v15

    .line 250
    :cond_192
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v17

    add-float v3, v18, v9

    move/from16 v4, v28

    move-object/from16 v5, p0

    iget-object v5, v5, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual/range {v0 .. v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->drawForm(Landroid/graphics/Canvas;FFILbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;)V

    .line 252
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->LEFT_TO_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    if-ne v14, v0, :cond_1a9

    .line 253
    add-float v17, v17, v15

    .line 256
    :cond_1a9
    if-nez v31, :cond_1e1

    .line 257
    if-eqz v30, :cond_1b6

    .line 258
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->RIGHT_TO_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    if-ne v14, v0, :cond_1b3

    neg-float v0, v12

    goto :goto_1b4

    :cond_1b3
    move v0, v12

    :goto_1b4
    add-float v17, v17, v0

    .line 260
    :cond_1b6
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->RIGHT_TO_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    if-ne v14, v0, :cond_1c0

    .line 261
    aget-object v0, v25, v28

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;->width:F

    sub-float v17, v17, v0

    .line 263
    :cond_1c0
    add-float v0, v18, v7

    aget-object v1, v10, v28

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v4, v17

    invoke-virtual {v2, v3, v4, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->drawLabel(Landroid/graphics/Canvas;FFLjava/lang/String;)V

    .line 265
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->LEFT_TO_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    if-ne v14, v0, :cond_1d7

    .line 266
    aget-object v0, v25, v28

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;->width:F

    add-float v17, v17, v0

    .line 268
    :cond_1d7
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->RIGHT_TO_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    if-ne v14, v0, :cond_1dd

    neg-float v0, v13

    goto :goto_1de

    :cond_1dd
    move v0, v13

    :goto_1de
    add-float v17, v17, v0

    goto :goto_1ed

    .line 271
    :cond_1e1
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->RIGHT_TO_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    if-ne v14, v0, :cond_1e9

    move/from16 v0, v16

    neg-float v0, v0

    goto :goto_1eb

    :cond_1e9
    move/from16 v0, v16

    :goto_1eb
    add-float v17, v17, v0

    .line 232
    .end local v30    # "drawingForm":Z
    .end local v31    # "isStacked":Z
    :goto_1ed
    add-int/lit8 v28, v28, 0x1

    goto/16 :goto_144

    .line 275
    .end local v22    # "contentWidth":F
    .end local v23    # "originPosX":F
    .end local v24    # "calculatedLineSizes":[Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;
    .end local v25    # "calculatedLabelSizes":[Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/FSize;
    .end local v26    # "calculatedLabelBreakPoints":[Ljava/lang/Boolean;
    .end local v27    # "lineIndex":I
    .end local v28    # "i":I
    .end local v29    # "count":I
    :cond_1f1
    goto/16 :goto_35f

    .line 286
    .end local v17    # "posX":F
    .end local v18    # "posY":F
    :pswitch_1f3
    const/16 v22, 0x0

    .line 287
    .local v22, "stack":F
    const/16 v23, 0x0

    .line 289
    .local v23, "wasStacked":Z
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->PIECHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    move-object/from16 v1, v21

    if-ne v1, v0, :cond_243

    .line 290
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getChartWidth()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->LEFT_TO_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    if-ne v14, v1, :cond_217

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    iget v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mTextWidthMax:F

    neg-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    goto :goto_220

    :cond_217
    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    iget v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mTextWidthMax:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    :goto_220
    add-float v17, v0, v1

    .line 293
    .local v17, "posX":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getChartHeight()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    iget v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mNeededHeight:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    .line 294
    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->getYOffset()F

    move-result v1

    add-float v18, v0, v1

    .local v18, "posY":F
    goto/16 :goto_2c5

    .line 296
    .end local v17    # "posX":F
    .end local v18    # "posY":F
    :cond_243
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->RIGHT_OF_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    move-object/from16 v1, v21

    if-eq v1, v0, :cond_255

    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->RIGHT_OF_CHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    move-object/from16 v1, v21

    if-eq v1, v0, :cond_255

    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->RIGHT_OF_CHART_INSIDE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    move-object/from16 v1, v21

    if-ne v1, v0, :cond_258

    :cond_255
    const/16 v24, 0x1

    goto :goto_25a

    :cond_258
    const/16 v24, 0x0

    .line 301
    .local v24, "isRightAligned":Z
    :goto_25a
    if-eqz v24, :cond_273

    .line 302
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getChartWidth()F

    move-result v0

    sub-float v17, v0, v20

    .line 303
    .local v17, "posX":F
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->LEFT_TO_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    if-ne v14, v0, :cond_281

    .line 304
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mTextWidthMax:F

    sub-float v17, v17, v0

    goto :goto_281

    .line 306
    .end local v17    # "posX":F
    :cond_273
    move/from16 v17, v20

    .line 307
    .local v17, "posX":F
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->RIGHT_TO_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    if-ne v14, v0, :cond_281

    .line 308
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    iget v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mTextWidthMax:F

    add-float v17, v17, v0

    .line 311
    :cond_281
    :goto_281
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->RIGHT_OF_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    move-object/from16 v1, v21

    if-eq v1, v0, :cond_28d

    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->LEFT_OF_CHART:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    move-object/from16 v1, v21

    if-ne v1, v0, :cond_298

    .line 313
    :cond_28d
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentTop()F

    move-result v0

    add-float v18, v0, v19

    .local v18, "posY":F
    goto :goto_2c5

    .line 314
    .end local v18    # "posY":F
    :cond_298
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->RIGHT_OF_CHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    move-object/from16 v1, v21

    if-eq v1, v0, :cond_2a4

    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;->LEFT_OF_CHART_CENTER:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendPosition;

    move-object/from16 v1, v21

    if-ne v1, v0, :cond_2bb

    .line 316
    :cond_2a4
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getChartHeight()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    iget v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;->mNeededHeight:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float v18, v0, v1

    .local v18, "posY":F
    goto :goto_2c5

    .line 323
    .end local v18    # "posY":F
    :cond_2bb
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->contentTop()F

    move-result v0

    add-float v18, v0, v19

    .line 327
    .local v18, "posY":F
    .end local v24    # "isRightAligned":Z
    :goto_2c5
    const/16 v24, 0x0

    .local v24, "i":I
    :goto_2c7
    array-length v0, v10

    move/from16 v1, v24

    if-ge v1, v0, :cond_35f

    .line 329
    aget v0, v11, v24

    const/4 v1, -0x2

    if-eq v0, v1, :cond_2d3

    const/4 v0, 0x1

    goto :goto_2d4

    :cond_2d3
    const/4 v0, 0x0

    :goto_2d4
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v25

    .line 330
    .local v25, "drawingForm":Ljava/lang/Boolean;
    move/from16 v26, v17

    .line 332
    .local v26, "x":F
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_302

    .line 333
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->LEFT_TO_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    if-ne v14, v0, :cond_2e7

    .line 334
    add-float v26, v26, v22

    goto :goto_2eb

    .line 336
    :cond_2e7
    sub-float v0, v15, v22

    sub-float v26, v26, v0

    .line 338
    :goto_2eb
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v26

    add-float v3, v18, v9

    move/from16 v4, v24

    move-object/from16 v5, p0

    iget-object v5, v5, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegend:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;

    invoke-virtual/range {v0 .. v5}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->drawForm(Landroid/graphics/Canvas;FFILbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend;)V

    .line 340
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->LEFT_TO_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    if-ne v14, v0, :cond_302

    .line 341
    add-float v26, v26, v15

    .line 344
    :cond_302
    aget-object v0, v10, v24

    if-eqz v0, :cond_355

    .line 346
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_318

    if-nez v23, :cond_318

    .line 347
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->LEFT_TO_RIGHT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    if-ne v14, v0, :cond_314

    move v0, v12

    goto :goto_315

    :cond_314
    neg-float v0, v12

    :goto_315
    add-float v26, v26, v0

    goto :goto_31c

    .line 349
    :cond_318
    if-eqz v23, :cond_31c

    .line 350
    move/from16 v26, v17

    .line 352
    :cond_31c
    :goto_31c
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;->RIGHT_TO_LEFT:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/Legend$LegendDirection;

    if-ne v14, v0, :cond_32d

    .line 353
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->mLegendLabelPaint:Landroid/graphics/Paint;

    aget-object v1, v10, v24

    invoke-static {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->calcTextWidth(Landroid/graphics/Paint;Ljava/lang/String;)I

    move-result v0

    int-to-float v0, v0

    sub-float v26, v26, v0

    .line 355
    :cond_32d
    if-nez v23, :cond_33d

    .line 356
    add-float v0, v18, v7

    aget-object v1, v10, v24

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v4, v26

    invoke-virtual {v2, v3, v4, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->drawLabel(Landroid/graphics/Canvas;FFLjava/lang/String;)V

    goto :goto_34e

    .line 358
    :cond_33d
    add-float v0, v7, v8

    add-float v18, v18, v0

    .line 359
    add-float v0, v18, v7

    aget-object v1, v10, v24

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v4, v26

    invoke-virtual {v2, v3, v4, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LegendRenderer;->drawLabel(Landroid/graphics/Canvas;FFLjava/lang/String;)V

    .line 363
    :goto_34e
    add-float v0, v7, v8

    add-float v18, v18, v0

    .line 364
    const/16 v22, 0x0

    goto :goto_35b

    .line 366
    :cond_355
    add-float v0, v15, v16

    add-float v22, v22, v0

    .line 367
    const/16 v23, 0x1

    .line 327
    .end local v25    # "drawingForm":Ljava/lang/Boolean;
    .end local v26    # "x":F
    :goto_35b
    add-int/lit8 v24, v24, 0x1

    goto/16 :goto_2c7

    .line 373
    .end local v17    # "posX":F
    .end local v18    # "posY":F
    .end local v22    # "stack":F
    .end local v23    # "wasStacked":Z
    .end local v24    # "i":I
    :cond_35f
    :goto_35f
    return-void

    :pswitch_data_360
    .packed-switch 0x1
        :pswitch_c1
        :pswitch_c1
        :pswitch_c1
        :pswitch_1f3
        :pswitch_1f3
        :pswitch_1f3
        :pswitch_1f3
        :pswitch_1f3
        :pswitch_1f3
        :pswitch_1f3
    .end packed-switch
.end method

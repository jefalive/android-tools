.class public final Lcom/google/android/gms/internal/zzsz$zza;
.super Lcom/google/android/gms/internal/zzso;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/zzsz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "zza"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/zzso<Lcom/google/android/gms/internal/zzsz$zza;>;"
    }
.end annotation


# instance fields
.field public zzbuI:[Ljava/lang/String;

.field public zzbuJ:[Ljava/lang/String;

.field public zzbuK:[I

.field public zzbuL:[J


# direct methods
.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzso;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzsz$zza;->zzJC()Lcom/google/android/gms/internal/zzsz$zza;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1, "o"    # Ljava/lang/Object;

    if-ne p1, p0, :cond_4

    const/4 v0, 0x1

    return v0

    :cond_4
    instance-of v0, p1, Lcom/google/android/gms/internal/zzsz$zza;

    if-nez v0, :cond_a

    const/4 v0, 0x0

    return v0

    :cond_a
    move-object v2, p1

    check-cast v2, Lcom/google/android/gms/internal/zzsz$zza;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuI:[Ljava/lang/String;

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsz$zza;->zzbuI:[Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzss;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_19

    const/4 v0, 0x0

    return v0

    :cond_19
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuJ:[Ljava/lang/String;

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsz$zza;->zzbuJ:[Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzss;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_25

    const/4 v0, 0x0

    return v0

    :cond_25
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuK:[I

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsz$zza;->zzbuK:[I

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzss;->equals([I[I)Z

    move-result v0

    if-nez v0, :cond_31

    const/4 v0, 0x0

    return v0

    :cond_31
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuL:[J

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsz$zza;->zzbuL:[J

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzss;->equals([J[J)Z

    move-result v0

    if-nez v0, :cond_3d

    const/4 v0, 0x0

    return v0

    :cond_3d
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    if-eqz v0, :cond_49

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzsq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_59

    :cond_49
    iget-object v0, v2, Lcom/google/android/gms/internal/zzsz$zza;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    if-eqz v0, :cond_55

    iget-object v0, v2, Lcom/google/android/gms/internal/zzsz$zza;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzsq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_57

    :cond_55
    const/4 v0, 0x1

    goto :goto_58

    :cond_57
    const/4 v0, 0x0

    :goto_58
    return v0

    :cond_59
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsz$zza;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzsq;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .registers 4

    const/16 v2, 0x11

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v2, v0, 0x20f

    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuI:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/internal/zzss;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int v2, v0, v1

    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuJ:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/internal/zzss;->hashCode([Ljava/lang/Object;)I

    move-result v1

    add-int v2, v0, v1

    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuK:[I

    invoke-static {v1}, Lcom/google/android/gms/internal/zzss;->hashCode([I)I

    move-result v1

    add-int v2, v0, v1

    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuL:[J

    invoke-static {v1}, Lcom/google/android/gms/internal/zzss;->hashCode([J)I

    move-result v1

    add-int v2, v0, v1

    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    if-eqz v1, :cond_46

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zzsq;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_48

    :cond_46
    const/4 v1, 0x0

    goto :goto_4e

    :cond_48
    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zzsq;->hashCode()I

    move-result v1

    :goto_4e
    add-int v2, v0, v1

    return v2
.end method

.method public synthetic mergeFrom(Lcom/google/android/gms/internal/zzsm;)Lcom/google/android/gms/internal/zzsu;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/zzsz$zza;->zzS(Lcom/google/android/gms/internal/zzsm;)Lcom/google/android/gms/internal/zzsz$zza;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/android/gms/internal/zzsn;)V
    .registers 7
    .param p1, "output"    # Lcom/google/android/gms/internal/zzsn;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuI:[Ljava/lang/String;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuI:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_1c

    const/4 v3, 0x0

    :goto_a
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuI:[Ljava/lang/String;

    array-length v0, v0

    if-ge v3, v0, :cond_1c

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuI:[Ljava/lang/String;

    aget-object v4, v0, v3

    if-eqz v4, :cond_19

    const/4 v0, 0x1

    invoke-virtual {p1, v0, v4}, Lcom/google/android/gms/internal/zzsn;->zzn(ILjava/lang/String;)V

    :cond_19
    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    :cond_1c
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuJ:[Ljava/lang/String;

    if-eqz v0, :cond_38

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuJ:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_38

    const/4 v3, 0x0

    :goto_26
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuJ:[Ljava/lang/String;

    array-length v0, v0

    if-ge v3, v0, :cond_38

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuJ:[Ljava/lang/String;

    aget-object v4, v0, v3

    if-eqz v4, :cond_35

    const/4 v0, 0x2

    invoke-virtual {p1, v0, v4}, Lcom/google/android/gms/internal/zzsn;->zzn(ILjava/lang/String;)V

    :cond_35
    add-int/lit8 v3, v3, 0x1

    goto :goto_26

    :cond_38
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuK:[I

    if-eqz v0, :cond_52

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuK:[I

    array-length v0, v0

    if-lez v0, :cond_52

    const/4 v3, 0x0

    :goto_42
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuK:[I

    array-length v0, v0

    if-ge v3, v0, :cond_52

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuK:[I

    aget v0, v0, v3

    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzA(II)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_42

    :cond_52
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuL:[J

    if-eqz v0, :cond_6c

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuL:[J

    array-length v0, v0

    if-lez v0, :cond_6c

    const/4 v3, 0x0

    :goto_5c
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuL:[J

    array-length v0, v0

    if-ge v3, v0, :cond_6c

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuL:[J

    aget-wide v0, v0, v3

    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/android/gms/internal/zzsn;->zzb(IJ)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_5c

    :cond_6c
    invoke-super {p0, p1}, Lcom/google/android/gms/internal/zzso;->writeTo(Lcom/google/android/gms/internal/zzsn;)V

    return-void
.end method

.method public zzJC()Lcom/google/android/gms/internal/zzsz$zza;
    .registers 2

    sget-object v0, Lcom/google/android/gms/internal/zzsx;->zzbuB:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuI:[Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/internal/zzsx;->zzbuB:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuJ:[Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/internal/zzsx;->zzbuw:[I

    iput-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuK:[I

    sget-object v0, Lcom/google/android/gms/internal/zzsx;->zzbux:[J

    iput-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuL:[J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuu:I

    return-object p0
.end method

.method public zzS(Lcom/google/android/gms/internal/zzsm;)Lcom/google/android/gms/internal/zzsz$zza;
    .registers 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzIX()I

    move-result v3

    sparse-switch v3, :sswitch_data_176

    goto :goto_9

    :sswitch_8
    return-object p0

    :goto_9
    invoke-virtual {p0, p1, v3}, Lcom/google/android/gms/internal/zzsz$zza;->zza(Lcom/google/android/gms/internal/zzsm;I)Z

    move-result v0

    if-nez v0, :cond_174

    return-object p0

    :sswitch_10
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/zzsx;->zzc(Lcom/google/android/gms/internal/zzsm;I)I

    move-result v4

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuI:[Ljava/lang/String;

    if-nez v0, :cond_1c

    const/4 v5, 0x0

    goto :goto_1f

    :cond_1c
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuI:[Ljava/lang/String;

    array-length v5, v0

    :goto_1f
    add-int v0, v5, v4

    new-array v6, v0, [Ljava/lang/String;

    if-eqz v5, :cond_2c

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuI:[Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v6, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2c
    :goto_2c
    array-length v0, v6

    add-int/lit8 v0, v0, -0x1

    if-ge v5, v0, :cond_3d

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->readString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v5

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzIX()I

    add-int/lit8 v5, v5, 0x1

    goto :goto_2c

    :cond_3d
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->readString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v5

    iput-object v6, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuI:[Ljava/lang/String;

    goto/16 :goto_174

    :sswitch_47
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/zzsx;->zzc(Lcom/google/android/gms/internal/zzsm;I)I

    move-result v4

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuJ:[Ljava/lang/String;

    if-nez v0, :cond_53

    const/4 v5, 0x0

    goto :goto_56

    :cond_53
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuJ:[Ljava/lang/String;

    array-length v5, v0

    :goto_56
    add-int v0, v5, v4

    new-array v6, v0, [Ljava/lang/String;

    if-eqz v5, :cond_63

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuJ:[Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v6, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_63
    :goto_63
    array-length v0, v6

    add-int/lit8 v0, v0, -0x1

    if-ge v5, v0, :cond_74

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->readString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v5

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzIX()I

    add-int/lit8 v5, v5, 0x1

    goto :goto_63

    :cond_74
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->readString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v5

    iput-object v6, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuJ:[Ljava/lang/String;

    goto/16 :goto_174

    :sswitch_7e
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/zzsx;->zzc(Lcom/google/android/gms/internal/zzsm;I)I

    move-result v4

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuK:[I

    if-nez v0, :cond_8a

    const/4 v5, 0x0

    goto :goto_8d

    :cond_8a
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuK:[I

    array-length v5, v0

    :goto_8d
    add-int v0, v5, v4

    new-array v6, v0, [I

    if-eqz v5, :cond_9a

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuK:[I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v6, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_9a
    :goto_9a
    array-length v0, v6

    add-int/lit8 v0, v0, -0x1

    if-ge v5, v0, :cond_ab

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJb()I

    move-result v0

    aput v0, v6, v5

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzIX()I

    add-int/lit8 v5, v5, 0x1

    goto :goto_9a

    :cond_ab
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJb()I

    move-result v0

    aput v0, v6, v5

    iput-object v6, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuK:[I

    goto/16 :goto_174

    :sswitch_b5
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJf()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/android/gms/internal/zzsm;->zzmq(I)I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->getPosition()I

    move-result v7

    :goto_c2
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJk()I

    move-result v0

    if-lez v0, :cond_ce

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJb()I

    add-int/lit8 v6, v6, 0x1

    goto :goto_c2

    :cond_ce
    invoke-virtual {p1, v7}, Lcom/google/android/gms/internal/zzsm;->zzms(I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuK:[I

    if-nez v0, :cond_d7

    const/4 v8, 0x0

    goto :goto_da

    :cond_d7
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuK:[I

    array-length v8, v0

    :goto_da
    add-int v0, v8, v6

    new-array v9, v0, [I

    if-eqz v8, :cond_e7

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuK:[I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v9, v2, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_e7
    :goto_e7
    array-length v0, v9

    if-ge v8, v0, :cond_f3

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJb()I

    move-result v0

    aput v0, v9, v8

    add-int/lit8 v8, v8, 0x1

    goto :goto_e7

    :cond_f3
    iput-object v9, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuK:[I

    invoke-virtual {p1, v5}, Lcom/google/android/gms/internal/zzsm;->zzmr(I)V

    goto/16 :goto_174

    :sswitch_fa
    const/16 v0, 0x20

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/zzsx;->zzc(Lcom/google/android/gms/internal/zzsm;I)I

    move-result v4

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuL:[J

    if-nez v0, :cond_106

    const/4 v5, 0x0

    goto :goto_109

    :cond_106
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuL:[J

    array-length v5, v0

    :goto_109
    add-int v0, v5, v4

    new-array v6, v0, [J

    if-eqz v5, :cond_116

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuL:[J

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v6, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_116
    :goto_116
    array-length v0, v6

    add-int/lit8 v0, v0, -0x1

    if-ge v5, v0, :cond_127

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJa()J

    move-result-wide v0

    aput-wide v0, v6, v5

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzIX()I

    add-int/lit8 v5, v5, 0x1

    goto :goto_116

    :cond_127
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJa()J

    move-result-wide v0

    aput-wide v0, v6, v5

    iput-object v6, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuL:[J

    goto/16 :goto_174

    :sswitch_131
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJf()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/android/gms/internal/zzsm;->zzmq(I)I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->getPosition()I

    move-result v7

    :goto_13e
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJk()I

    move-result v0

    if-lez v0, :cond_14a

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJa()J

    add-int/lit8 v6, v6, 0x1

    goto :goto_13e

    :cond_14a
    invoke-virtual {p1, v7}, Lcom/google/android/gms/internal/zzsm;->zzms(I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuL:[J

    if-nez v0, :cond_153

    const/4 v8, 0x0

    goto :goto_156

    :cond_153
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuL:[J

    array-length v8, v0

    :goto_156
    add-int v0, v8, v6

    new-array v9, v0, [J

    if-eqz v8, :cond_163

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuL:[J

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v9, v2, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_163
    :goto_163
    array-length v0, v9

    if-ge v8, v0, :cond_16f

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJa()J

    move-result-wide v0

    aput-wide v0, v9, v8

    add-int/lit8 v8, v8, 0x1

    goto :goto_163

    :cond_16f
    iput-object v9, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuL:[J

    invoke-virtual {p1, v5}, Lcom/google/android/gms/internal/zzsm;->zzmr(I)V

    :cond_174
    :goto_174
    goto/16 :goto_0

    :sswitch_data_176
    .sparse-switch
        0x0 -> :sswitch_8
        0xa -> :sswitch_10
        0x12 -> :sswitch_47
        0x18 -> :sswitch_7e
        0x1a -> :sswitch_b5
        0x20 -> :sswitch_fa
        0x22 -> :sswitch_131
    .end sparse-switch
.end method

.method protected zzz()I
    .registers 8

    invoke-super {p0}, Lcom/google/android/gms/internal/zzso;->zzz()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuI:[Ljava/lang/String;

    if-eqz v0, :cond_29

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuI:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_29

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_10
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuI:[Ljava/lang/String;

    array-length v0, v0

    if-ge v5, v0, :cond_25

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuI:[Ljava/lang/String;

    aget-object v6, v0, v5

    if-eqz v6, :cond_22

    add-int/lit8 v3, v3, 0x1

    invoke-static {v6}, Lcom/google/android/gms/internal/zzsn;->zzgO(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v4, v0

    :cond_22
    add-int/lit8 v5, v5, 0x1

    goto :goto_10

    :cond_25
    add-int/2addr v2, v4

    mul-int/lit8 v0, v3, 0x1

    add-int/2addr v2, v0

    :cond_29
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuJ:[Ljava/lang/String;

    if-eqz v0, :cond_4e

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuJ:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_4e

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_35
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuJ:[Ljava/lang/String;

    array-length v0, v0

    if-ge v5, v0, :cond_4a

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuJ:[Ljava/lang/String;

    aget-object v6, v0, v5

    if-eqz v6, :cond_47

    add-int/lit8 v3, v3, 0x1

    invoke-static {v6}, Lcom/google/android/gms/internal/zzsn;->zzgO(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v4, v0

    :cond_47
    add-int/lit8 v5, v5, 0x1

    goto :goto_35

    :cond_4a
    add-int/2addr v2, v4

    mul-int/lit8 v0, v3, 0x1

    add-int/2addr v2, v0

    :cond_4e
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuK:[I

    if-eqz v0, :cond_71

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuK:[I

    array-length v0, v0

    if-lez v0, :cond_71

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_59
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuK:[I

    array-length v0, v0

    if-ge v4, v0, :cond_6a

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuK:[I

    aget v5, v0, v4

    invoke-static {v5}, Lcom/google/android/gms/internal/zzsn;->zzmx(I)I

    move-result v0

    add-int/2addr v3, v0

    add-int/lit8 v4, v4, 0x1

    goto :goto_59

    :cond_6a
    add-int/2addr v2, v3

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuK:[I

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    :cond_71
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuL:[J

    if-eqz v0, :cond_94

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuL:[J

    array-length v0, v0

    if-lez v0, :cond_94

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_7c
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuL:[J

    array-length v0, v0

    if-ge v4, v0, :cond_8d

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuL:[J

    aget-wide v5, v0, v4

    invoke-static {v5, v6}, Lcom/google/android/gms/internal/zzsn;->zzas(J)I

    move-result v0

    add-int/2addr v3, v0

    add-int/lit8 v4, v4, 0x1

    goto :goto_7c

    :cond_8d
    add-int/2addr v2, v3

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zza;->zzbuL:[J

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    :cond_94
    return v2
.end method

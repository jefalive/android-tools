.class public enum Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;>;"
    }
.end annotation


# static fields
.field private static final $:[B

.field private static $$:I

.field private static final synthetic $VALUES:[Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

.field public static final enum DELETE:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

.field public static final enum GET:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

.field public static final enum HEAD:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

.field public static final enum LEGACY:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

.field public static final enum OPTIONS:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

.field public static final enum PATCH:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

.field public static final enum POST:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

.field public static final enum PUT:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    rsub-int/lit8 p2, p2, 0x1f

    sget-object v4, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$:[B

    rsub-int/lit8 p0, p0, 0x50

    new-instance v0, Ljava/lang/String;

    const/4 v5, 0x0

    add-int/lit8 p1, p1, 0x3

    new-array v1, p1, [B

    add-int/lit8 p1, p1, -0x1

    if-nez v4, :cond_16

    move v2, p2

    move v3, p0

    :goto_13
    add-int/2addr v2, v3

    add-int/lit8 p0, v2, 0x2

    :cond_16
    int-to-byte v2, p0

    aput-byte v2, v1, v5

    move v2, v5

    add-int/lit8 p2, p2, 0x1

    add-int/lit8 v5, v5, 0x1

    if-ne v2, p1, :cond_25

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_25
    move v2, p0

    aget-byte v3, v4, p2

    goto :goto_13
.end method

.method static constructor <clinit>()V
    .registers 5

    .line 9
    const/16 v0, 0x22

    new-array v0, v0, [B

    fill-array-data v0, :array_110

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$:[B

    const/16 v0, 0x74

    sput v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$$:I

    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/http/a;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$:[B

    const/4 v2, 0x1

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$:[B

    const/16 v3, 0x10

    aget-byte v2, v2, v3

    invoke-static {v1, v2, v2}, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/http/a;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->GET:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    .line 16
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$:[B

    const/16 v2, 0x10

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$:[B

    const/4 v3, 0x6

    aget-byte v2, v2, v3

    or-int/lit8 v3, v2, 0x18

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->POST:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    .line 21
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$:[B

    const/16 v2, 0x10

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$:[B

    const/4 v3, 0x5

    aget-byte v2, v2, v3

    neg-int v2, v2

    invoke-static {v1, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->PUT:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    .line 26
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$:[B

    const/16 v2, 0xd

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$:[B

    const/16 v3, 0x19

    aget-byte v2, v2, v3

    const/16 v3, 0x16

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->DELETE:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    .line 31
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$:[B

    const/16 v2, 0x10

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$:[B

    const/16 v3, 0x8

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$:[B

    const/16 v4, 0x17

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->PATCH:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    .line 36
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$:[B

    const/16 v2, 0x11

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$:[B

    const/4 v3, 0x6

    aget-byte v2, v2, v3

    const/16 v3, 0x1c

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->HEAD:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    .line 41
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$:[B

    const/4 v2, 0x6

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$:[B

    const/16 v3, 0x17

    aget-byte v2, v2, v3

    or-int/lit8 v3, v2, 0x8

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->OPTIONS:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    .line 46
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$:[B

    const/16 v2, 0x17

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$:[B

    const/16 v3, 0x19

    aget-byte v2, v2, v3

    sget-object v3, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$:[B

    const/16 v4, 0x1d

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$(III)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->LEGACY:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    .line 4
    const/16 v0, 0x8

    new-array v0, v0, [Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->GET:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->POST:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->PUT:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->DELETE:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->PATCH:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->HEAD:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->OPTIONS:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    const/4 v2, 0x6

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->LEGACY:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    const/4 v2, 0x7

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$VALUES:[Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    return-void

    :array_110
    .array-data 1
        0x43t
        -0x9t
        -0x2t
        0x62t
        -0x5t
        -0x6t
        0x1t
        -0x3t
        0x2t
        -0x1t
        -0x1t
        0x5t
        -0x9t
        0xdt
        -0x11t
        -0x9t
        0x0t
        -0x8t
        0x0t
        0x14t
        -0x1t
        0x2t
        -0xdt
        0x4t
        -0x3t
        0x3t
        0x3t
        -0x3t
        -0x11t
        0x11t
        -0x13t
        0x3t
        -0x4t
        0xdt
    .end array-data
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILbr/com/itau/sdk/android/core/endpoint/http/a;)V
    .registers 4

    .line 4
    invoke-direct {p0, p1, p2}, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static hasBody(Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;)Z
    .registers 2

    .line 57
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->GET:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    if-eq p0, v0, :cond_a

    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->HEAD:Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    if-eq p0, v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method public static valueByName(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;
    .registers 6

    .line 49
    invoke-static {}, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->values()[Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    move-result-object v1

    array-length v2, v1

    const/4 v3, 0x0

    :goto_6
    if-ge v3, v2, :cond_18

    aget-object v4, v1, v3

    .line 50
    invoke-virtual {v4}, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 51
    return-object v4

    .line 49
    :cond_15
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 53
    :cond_18
    const/4 v0, 0x0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;
    .registers 2

    .line 4
    const-class v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;
    .registers 1

    .line 4
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->$VALUES:[Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    invoke-virtual {v0}, [Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/sdk/android/core/endpoint/http/CallMethod;

    return-object v0
.end method

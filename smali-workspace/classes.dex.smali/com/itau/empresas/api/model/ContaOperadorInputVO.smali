.class public Lcom/itau/empresas/api/model/ContaOperadorInputVO;
.super Ljava/lang/Object;
.source "ContaOperadorInputVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field agenciaOrigem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia_origem"
    .end annotation
.end field

.field contaOrigem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta_origem"
    .end annotation
.end field

.field cpfCnpjOrigem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf_cnpj_origem"
    .end annotation
.end field

.field digitoVerificadorContaOrigem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_verificador_conta_origem"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

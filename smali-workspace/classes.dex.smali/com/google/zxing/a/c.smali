.class public Lcom/google/zxing/a/c;
.super Lcom/google/zxing/b;


# static fields
.field private static final a:[B


# instance fields
.field private b:[B

.field private final c:[I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lcom/google/zxing/a/c;->a:[B

    return-void
.end method

.method public constructor <init>(Lcom/google/zxing/g;)V
    .registers 3

    invoke-direct {p0, p1}, Lcom/google/zxing/b;-><init>(Lcom/google/zxing/g;)V

    sget-object v0, Lcom/google/zxing/a/c;->a:[B

    iput-object v0, p0, Lcom/google/zxing/a/c;->b:[B

    const/16 v0, 0x20

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/zxing/a/c;->c:[I

    return-void
.end method

.method private static a([I)I
    .registers 14

    array-length v2, p0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_5
    if-ge v6, v2, :cond_17

    aget v0, p0, v6

    if-le v0, v5, :cond_e

    move v4, v6

    aget v5, p0, v6

    :cond_e
    aget v0, p0, v6

    if-le v0, v3, :cond_14

    aget v3, p0, v6

    :cond_14
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    :cond_17
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    :goto_1a
    if-ge v8, v2, :cond_2a

    sub-int v9, v8, v4

    aget v0, p0, v8

    mul-int/2addr v0, v9

    mul-int v10, v0, v9

    if-le v10, v7, :cond_27

    move v6, v8

    move v7, v10

    :cond_27
    add-int/lit8 v8, v8, 0x1

    goto :goto_1a

    :cond_2a
    if-le v4, v6, :cond_2f

    move v8, v4

    move v4, v6

    move v6, v8

    :cond_2f
    sub-int v0, v6, v4

    div-int/lit8 v1, v2, 0x10

    if-gt v0, v1, :cond_3a

    invoke-static {}, Lcom/google/zxing/h;->a()Lcom/google/zxing/h;

    move-result-object v0

    throw v0

    :cond_3a
    add-int/lit8 v8, v6, -0x1

    const/4 v9, -0x1

    add-int/lit8 v10, v6, -0x1

    :goto_3f
    if-le v10, v4, :cond_55

    sub-int v11, v10, v4

    mul-int v0, v11, v11

    sub-int v1, v6, v10

    mul-int/2addr v0, v1

    aget v1, p0, v10

    sub-int v1, v3, v1

    mul-int v12, v0, v1

    if-le v12, v9, :cond_52

    move v8, v10

    move v9, v12

    :cond_52
    add-int/lit8 v10, v10, -0x1

    goto :goto_3f

    :cond_55
    shl-int/lit8 v0, v8, 0x3

    return v0
.end method

.method private a(I)V
    .registers 5

    iget-object v0, p0, Lcom/google/zxing/a/c;->b:[B

    array-length v0, v0

    if-ge v0, p1, :cond_9

    new-array v0, p1, [B

    iput-object v0, p0, Lcom/google/zxing/a/c;->b:[B

    :cond_9
    const/4 v2, 0x0

    :goto_a
    const/16 v0, 0x20

    if-ge v2, v0, :cond_16

    iget-object v0, p0, Lcom/google/zxing/a/c;->c:[I

    const/4 v1, 0x0

    aput v1, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    :cond_16
    return-void
.end method


# virtual methods
.method public a(ILcom/google/zxing/a/a;)Lcom/google/zxing/a/a;
    .registers 16

    invoke-virtual {p0}, Lcom/google/zxing/a/c;->a()Lcom/google/zxing/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/zxing/g;->b()I

    move-result v4

    if-eqz p2, :cond_10

    invoke-virtual {p2}, Lcom/google/zxing/a/a;->a()I

    move-result v0

    if-ge v0, v4, :cond_16

    :cond_10
    new-instance p2, Lcom/google/zxing/a/a;

    invoke-direct {p2, v4}, Lcom/google/zxing/a/a;-><init>(I)V

    goto :goto_19

    :cond_16
    invoke-virtual {p2}, Lcom/google/zxing/a/a;->b()V

    :goto_19
    invoke-direct {p0, v4}, Lcom/google/zxing/a/c;->a(I)V

    iget-object v0, p0, Lcom/google/zxing/a/c;->b:[B

    invoke-virtual {v3, p1, v0}, Lcom/google/zxing/g;->a(I[B)[B

    move-result-object v5

    iget-object v6, p0, Lcom/google/zxing/a/c;->c:[I

    const/4 v7, 0x0

    :goto_25
    if-ge v7, v4, :cond_36

    aget-byte v0, v5, v7

    and-int/lit16 v8, v0, 0xff

    shr-int/lit8 v0, v8, 0x3

    aget v1, v6, v0

    add-int/lit8 v1, v1, 0x1

    aput v1, v6, v0

    add-int/lit8 v7, v7, 0x1

    goto :goto_25

    :cond_36
    invoke-static {v6}, Lcom/google/zxing/a/c;->a([I)I

    move-result v7

    const/4 v0, 0x0

    aget-byte v0, v5, v0

    and-int/lit16 v8, v0, 0xff

    const/4 v0, 0x1

    aget-byte v0, v5, v0

    and-int/lit16 v9, v0, 0xff

    const/4 v10, 0x1

    :goto_45
    add-int/lit8 v0, v4, -0x1

    if-ge v10, v0, :cond_5f

    add-int/lit8 v0, v10, 0x1

    aget-byte v0, v5, v0

    and-int/lit16 v11, v0, 0xff

    mul-int/lit8 v0, v9, 0x4

    sub-int/2addr v0, v8

    sub-int/2addr v0, v11

    div-int/lit8 v12, v0, 0x2

    if-ge v12, v7, :cond_5a

    invoke-virtual {p2, v10}, Lcom/google/zxing/a/a;->b(I)V

    :cond_5a
    move v8, v9

    move v9, v11

    add-int/lit8 v10, v10, 0x1

    goto :goto_45

    :cond_5f
    return-object p2
.end method

.method public a(Lcom/google/zxing/g;)Lcom/google/zxing/b;
    .registers 3

    new-instance v0, Lcom/google/zxing/a/c;

    invoke-direct {v0, p1}, Lcom/google/zxing/a/c;-><init>(Lcom/google/zxing/g;)V

    return-object v0
.end method

.method public b()Lcom/google/zxing/a/b;
    .registers 15

    invoke-virtual {p0}, Lcom/google/zxing/a/c;->a()Lcom/google/zxing/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/zxing/g;->b()I

    move-result v4

    invoke-virtual {v3}, Lcom/google/zxing/g;->c()I

    move-result v5

    new-instance v6, Lcom/google/zxing/a/b;

    invoke-direct {v6, v4, v5}, Lcom/google/zxing/a/b;-><init>(II)V

    invoke-direct {p0, v4}, Lcom/google/zxing/a/c;->a(I)V

    iget-object v7, p0, Lcom/google/zxing/a/c;->c:[I

    const/4 v8, 0x1

    :goto_17
    const/4 v0, 0x5

    if-ge v8, v0, :cond_3e

    mul-int v0, v5, v8

    div-int/lit8 v9, v0, 0x5

    iget-object v0, p0, Lcom/google/zxing/a/c;->b:[B

    invoke-virtual {v3, v9, v0}, Lcom/google/zxing/g;->a(I[B)[B

    move-result-object v10

    mul-int/lit8 v0, v4, 0x4

    div-int/lit8 v11, v0, 0x5

    div-int/lit8 v12, v4, 0x5

    :goto_2a
    if-ge v12, v11, :cond_3b

    aget-byte v0, v10, v12

    and-int/lit16 v13, v0, 0xff

    shr-int/lit8 v0, v13, 0x3

    aget v1, v7, v0

    add-int/lit8 v1, v1, 0x1

    aput v1, v7, v0

    add-int/lit8 v12, v12, 0x1

    goto :goto_2a

    :cond_3b
    add-int/lit8 v8, v8, 0x1

    goto :goto_17

    :cond_3e
    invoke-static {v7}, Lcom/google/zxing/a/c;->a([I)I

    move-result v8

    invoke-virtual {v3}, Lcom/google/zxing/g;->a()[B

    move-result-object v9

    const/4 v10, 0x0

    :goto_47
    if-ge v10, v5, :cond_5f

    mul-int v11, v10, v4

    const/4 v12, 0x0

    :goto_4c
    if-ge v12, v4, :cond_5c

    add-int v0, v11, v12

    aget-byte v0, v9, v0

    and-int/lit16 v13, v0, 0xff

    if-ge v13, v8, :cond_59

    invoke-virtual {v6, v12, v10}, Lcom/google/zxing/a/b;->b(II)V

    :cond_59
    add-int/lit8 v12, v12, 0x1

    goto :goto_4c

    :cond_5c
    add-int/lit8 v10, v10, 0x1

    goto :goto_47

    :cond_5f
    return-object v6
.end method

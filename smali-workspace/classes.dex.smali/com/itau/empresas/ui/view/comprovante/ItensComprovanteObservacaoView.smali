.class public Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteObservacaoView;
.super Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;
.source "ItensComprovanteObservacaoView.java"


# instance fields
.field private final observacao:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "titulo"    # Ljava/lang/String;
    .param p2, "descricao"    # Ljava/lang/String;
    .param p3, "observacao"    # Ljava/lang/String;

    .line 8
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    iput-object p3, p0, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteObservacaoView;->observacao:Ljava/lang/String;

    .line 10
    return-void
.end method


# virtual methods
.method public getObservacao()Ljava/lang/String;
    .registers 2

    .line 13
    iget-object v0, p0, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteObservacaoView;->observacao:Ljava/lang/String;

    return-object v0
.end method

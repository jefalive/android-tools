.class Lcom/itau/empresas/feature/pendencias/PendenciasController$1;
.super Ljava/lang/Object;
.source "PendenciasController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/pendencias/PendenciasController;->buscaPendencias(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/pendencias/PendenciasController;

.field final synthetic val$dataFinal:Ljava/lang/String;

.field final synthetic val$dataInicio:Ljava/lang/String;

.field final synthetic val$idConta:Ljava/lang/String;

.field final synthetic val$operador:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/pendencias/PendenciasController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .param p1, "this$0"    # Lcom/itau/empresas/feature/pendencias/PendenciasController;

    .line 18
    iput-object p1, p0, Lcom/itau/empresas/feature/pendencias/PendenciasController$1;->this$0:Lcom/itau/empresas/feature/pendencias/PendenciasController;

    iput-object p2, p0, Lcom/itau/empresas/feature/pendencias/PendenciasController$1;->val$idConta:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/feature/pendencias/PendenciasController$1;->val$operador:Ljava/lang/String;

    iput-object p4, p0, Lcom/itau/empresas/feature/pendencias/PendenciasController$1;->val$dataInicio:Ljava/lang/String;

    iput-object p5, p0, Lcom/itau/empresas/feature/pendencias/PendenciasController$1;->val$dataFinal:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 7

    .line 21
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasController$1;->this$0:Lcom/itau/empresas/feature/pendencias/PendenciasController;

    # invokes: Lcom/itau/empresas/feature/pendencias/PendenciasController;->api()Ljava/lang/Object;
    invoke-static {v0}, Lcom/itau/empresas/feature/pendencias/PendenciasController;->access$000(Lcom/itau/empresas/feature/pendencias/PendenciasController;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/feature/pendencias/PendenciasController$1;->val$idConta:Ljava/lang/String;

    iget-object v2, p0, Lcom/itau/empresas/feature/pendencias/PendenciasController$1;->val$operador:Ljava/lang/String;

    const-string v3, "pendencias"

    iget-object v4, p0, Lcom/itau/empresas/feature/pendencias/PendenciasController$1;->val$dataInicio:Ljava/lang/String;

    iget-object v5, p0, Lcom/itau/empresas/feature/pendencias/PendenciasController$1;->val$dataFinal:Ljava/lang/String;

    invoke-interface/range {v0 .. v5}, Lcom/itau/empresas/api/Api;->buscaPendencias(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/feature/pendencias/ListaPendenciasVO;

    move-result-object v0

    # invokes: Lcom/itau/empresas/feature/pendencias/PendenciasController;->post(Ljava/lang/Object;)V
    invoke-static {v0}, Lcom/itau/empresas/feature/pendencias/PendenciasController;->access$100(Ljava/lang/Object;)V

    .line 27
    return-void
.end method

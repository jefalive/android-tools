.class public Lcom/google/android/gms/internal/zzii;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# instance fields
.field zzLE:J

.field zzLF:J

.field zzLG:I

.field zzLH:I

.field zzLI:I

.field final zzLq:Ljava/lang/String;

.field private final zzpV:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .registers 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzii;->zzLE:J

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzii;->zzLF:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzii;->zzLG:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzii;->zzpV:Ljava/lang/Object;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/internal/zzii;->zzLH:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/internal/zzii;->zzLI:I

    iput-object p1, p0, Lcom/google/android/gms/internal/zzii;->zzLq:Ljava/lang/String;

    return-void
.end method

.method public static zzH(Landroid/content/Context;)Z
    .registers 8

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "Theme.Translucent"

    const-string v2, "style"

    const-string v3, "android"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_17

    const-string v0, "Please set theme of AdActivity to @android:style/Theme.Translucent to enable transparent background interstitial ad."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaJ(Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0

    :cond_17
    new-instance v5, Landroid/content/ComponentName;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.gms.ads.AdActivity"

    invoke-direct {v5, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_22
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v5, v1}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    iget v6, v0, Landroid/content/pm/ActivityInfo;->theme:I
    :try_end_2d
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_22 .. :try_end_2d} :catch_38

    if-ne v4, v6, :cond_31

    const/4 v0, 0x1

    return v0

    :cond_31
    const-string v0, "Please set theme of AdActivity to @android:style/Theme.Translucent to enable transparent background interstitial ad."

    :try_start_33
    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaJ(Ljava/lang/String;)V
    :try_end_36
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_33 .. :try_end_36} :catch_38

    const/4 v0, 0x0

    return v0

    :catch_38
    move-exception v6

    const-string v0, "Fail to fetch AdActivity theme"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    const-string v0, "Please set theme of AdActivity to @android:style/Theme.Translucent to enable transparent background interstitial ad."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaJ(Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public zzb(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;J)V
    .registers 11

    iget-object v4, p0, Lcom/google/android/gms/internal/zzii;->zzpV:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    iget-wide v0, p0, Lcom/google/android/gms/internal/zzii;->zzLF:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_12

    iput-wide p2, p0, Lcom/google/android/gms/internal/zzii;->zzLF:J

    iget-wide v0, p0, Lcom/google/android/gms/internal/zzii;->zzLF:J

    iput-wide v0, p0, Lcom/google/android/gms/internal/zzii;->zzLE:J

    goto :goto_14

    :cond_12
    iput-wide p2, p0, Lcom/google/android/gms/internal/zzii;->zzLE:J

    :goto_14
    iget-object v0, p1, Lcom/google/android/gms/ads/internal/client/AdRequestParcel;->extras:Landroid/os/Bundle;

    if-eqz v0, :cond_26

    iget-object v0, p1, Lcom/google/android/gms/ads/internal/client/AdRequestParcel;->extras:Landroid/os/Bundle;

    const-string v1, "gw"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_2e

    move-result v5

    const/4 v0, 0x1

    if-ne v5, v0, :cond_26

    monitor-exit v4

    return-void

    :cond_26
    :try_start_26
    iget v0, p0, Lcom/google/android/gms/internal/zzii;->zzLG:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzii;->zzLG:I
    :try_end_2c
    .catchall {:try_start_26 .. :try_end_2c} :catchall_2e

    monitor-exit v4

    goto :goto_31

    :catchall_2e
    move-exception v6

    monitor-exit v4

    throw v6

    :goto_31
    return-void
.end method

.method public zzc(Landroid/content/Context;Ljava/lang/String;)Landroid/os/Bundle;
    .registers 9

    iget-object v3, p0, Lcom/google/android/gms/internal/zzii;->zzpV:Ljava/lang/Object;

    monitor-enter v3

    :try_start_3
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v0, "session_id"

    iget-object v1, p0, Lcom/google/android/gms/internal/zzii;->zzLq:Ljava/lang/String;

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "basets"

    iget-wide v1, p0, Lcom/google/android/gms/internal/zzii;->zzLF:J

    invoke-virtual {v4, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "currts"

    iget-wide v1, p0, Lcom/google/android/gms/internal/zzii;->zzLE:J

    invoke-virtual {v4, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v0, "seq_num"

    invoke-virtual {v4, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "preqs"

    iget v1, p0, Lcom/google/android/gms/internal/zzii;->zzLG:I

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "pclick"

    iget v1, p0, Lcom/google/android/gms/internal/zzii;->zzLH:I

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "pimp"

    iget v1, p0, Lcom/google/android/gms/internal/zzii;->zzLI:I

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "support_transparent_background"

    invoke-static {p1}, Lcom/google/android/gms/internal/zzii;->zzH(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
    :try_end_40
    .catchall {:try_start_3 .. :try_end_40} :catchall_42

    monitor-exit v3

    return-object v4

    :catchall_42
    move-exception v5

    monitor-exit v3

    throw v5
.end method

.method public zzgS()V
    .registers 5

    iget-object v2, p0, Lcom/google/android/gms/internal/zzii;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget v0, p0, Lcom/google/android/gms/internal/zzii;->zzLI:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzii;->zzLI:I
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_b

    monitor-exit v2

    goto :goto_e

    :catchall_b
    move-exception v3

    monitor-exit v2

    throw v3

    :goto_e
    return-void
.end method

.method public zzgT()V
    .registers 5

    iget-object v2, p0, Lcom/google/android/gms/internal/zzii;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget v0, p0, Lcom/google/android/gms/internal/zzii;->zzLH:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzii;->zzLH:I
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_b

    monitor-exit v2

    goto :goto_e

    :catchall_b
    move-exception v3

    monitor-exit v2

    throw v3

    :goto_e
    return-void
.end method

.method public zzhl()J
    .registers 3

    iget-wide v0, p0, Lcom/google/android/gms/internal/zzii;->zzLF:J

    return-wide v0
.end method

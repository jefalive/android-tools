.class Lcom/itau/empresas/controller/ProdutosController$1;
.super Ljava/lang/Object;
.source "ProdutosController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/controller/ProdutosController;->consultaDetalhesMultilimite()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/controller/ProdutosController;


# direct methods
.method constructor <init>(Lcom/itau/empresas/controller/ProdutosController;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/controller/ProdutosController;

    .line 24
    iput-object p1, p0, Lcom/itau/empresas/controller/ProdutosController$1;->this$0:Lcom/itau/empresas/controller/ProdutosController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 12

    .line 28
    const-string v6, "7"

    .line 29
    .local v6, "dias":Ljava/lang/String;
    :try_start_2
    iget-object v0, p0, Lcom/itau/empresas/controller/ProdutosController$1;->this$0:Lcom/itau/empresas/controller/ProdutosController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/ProdutosController;->api()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    const-string v1, "7"

    const-string v2, "V"

    const-string v3, "CC"

    const-string v4, "A"

    const-string v5, "1"

    invoke-interface/range {v0 .. v5}, Lcom/itau/empresas/api/Api;->detalhesMultilimite(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 31
    .local v7, "xml":Ljava/lang/String;
    new-instance v8, Lorg/simpleframework/xml/core/Persister;

    invoke-direct {v8}, Lorg/simpleframework/xml/core/Persister;-><init>()V

    .line 33
    .local v8, "persister":Lorg/simpleframework/xml/core/Persister;
    const-string v0, "ISO-8859-1"

    const-string v1, "UTF-8"

    invoke-virtual {v7, v0, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 35
    const-class v0, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO;

    .line 36
    invoke-virtual {v8, v0, v7}, Lorg/simpleframework/xml/core/Persister;->read(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO;

    .line 37
    .local v9, "xmlVO":Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO;
    iget-object v0, p0, Lcom/itau/empresas/controller/ProdutosController$1;->this$0:Lcom/itau/empresas/controller/ProdutosController;

    iget-object v0, v0, Lcom/itau/empresas/controller/ProdutosController;->converter:Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter;

    invoke-virtual {v0, v9}, Lcom/itau/empresas/api/universal/converter/DetalhesMultilimiteConverter;->toModel(Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO;)Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteVO;

    move-result-object v10

    .line 39
    .local v10, "resposta":Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteVO;
    invoke-static {v10}, Lcom/itau/empresas/controller/BaseController;->post(Ljava/lang/Object;)V
    :try_end_39
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_39} :catch_3a

    .line 43
    .end local v6    # "dias":Ljava/lang/String;
    .end local v7    # "xml":Ljava/lang/String;
    .end local v8    # "persister":Lorg/simpleframework/xml/core/Persister;
    .end local v9    # "xmlVO":Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO;
    .end local v10    # "resposta":Lcom/itau/empresas/feature/credito/produtos/model/DetalhesMultilimiteVO;
    goto :goto_43

    .line 41
    :catch_3a
    move-exception v6

    .line 42
    .local v6, "e":Ljava/lang/Exception;
    new-instance v0, Lcom/itau/empresas/Evento$EventoDetalhesMultilimiteErro;

    invoke-direct {v0, v6}, Lcom/itau/empresas/Evento$EventoDetalhesMultilimiteErro;-><init>(Ljava/lang/Exception;)V

    invoke-static {v0}, Lcom/itau/empresas/controller/BaseController;->post(Ljava/lang/Object;)V

    .line 44
    .end local v6    # "e":Ljava/lang/Exception;
    :goto_43
    return-void
.end method

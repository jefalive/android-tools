.class public Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "AtendimentoRespostaFaqActivity.java"


# static fields
.field static final Duration:Ljava/lang/Integer;


# instance fields
.field private canaisExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

.field cartaoFeedBack:Landroid/support/v7/widget/CardView;

.field controller:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

.field listaCanais:Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;

.field listaFaqsRelacionada:Lcom/itau/empresas/ui/widgets/NonScrollListView;

.field root:Landroid/widget/RelativeLayout;

.field svDetalheFaq:Landroid/widget/ScrollView;

.field textoAtendimentoPergunta:Landroid/widget/TextView;

.field textoAtendimentoResposta:Landroid/widget/TextView;

.field textoIconCurtir:Lcom/itau/empresas/ui/view/TextViewIcon;

.field textoIconDescurtir:Lcom/itau/empresas/ui/view/TextViewIcon;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 51
    const/16 v0, 0x3e8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->Duration:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .line 49
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private carregaCanais(Ljava/util/ArrayList;)V
    .registers 4
    .param p1, "channels"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;>;)V"
        }
    .end annotation

    .line 96
    if-eqz p1, :cond_20

    .line 97
    new-instance v0, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoCanaisExpandableListAdapter;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoCanaisExpandableListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->canaisExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    .line 99
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->listaCanais:Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;->setGroupIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 100
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->listaCanais:Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->canaisExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 101
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->listaCanais:Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;

    new-instance v1, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity$1;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;->setOnGroupExpandListener(Landroid/widget/ExpandableListView$OnGroupExpandListener;)V

    .line 112
    :cond_20
    return-void
.end method

.method private carregaRelacionadas(Ljava/util/ArrayList;)V
    .registers 7
    .param p1, "relacionadas"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoRelacionadas;>;)V"
        }
    .end annotation

    .line 157
    if-eqz p1, :cond_54

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_54

    .line 158
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->listaFaqsRelacionada:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    .line 159
    const v2, 0x7f030153

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/view/ViewGroup;

    .line 161
    .local v4, "header":Landroid/view/ViewGroup;
    const v0, 0x7f0e05d3

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast v0, Landroid/widget/TextView;

    .line 162
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f070707

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 161
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->listaFaqsRelacionada:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v4, v1, v2}, Lcom/itau/empresas/ui/widgets/NonScrollListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 165
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->listaFaqsRelacionada:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    new-instance v1, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoRelacionadasAdapter;

    invoke-direct {v1, p0, p1}, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoRelacionadasAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 166
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 167
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->listaFaqsRelacionada:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    new-instance v1, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity$2;

    invoke-direct {v1, p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity$2;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 174
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->listaFaqsRelacionada:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollListView;->setVisibility(I)V

    .line 175
    .end local v4    # "header":Landroid/view/ViewGroup;
    goto :goto_61

    .line 176
    :cond_54
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->listaFaqsRelacionada:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 177
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->listaFaqsRelacionada:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollListView;->setVisibility(I)V

    .line 180
    :goto_61
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->svDetalheFaq:Landroid/widget/ScrollView;

    new-instance v1, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity$3;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 186
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 81
    const v1, 0x7f0706f9

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 82
    const v1, 0x7f07072a

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comContentDescription(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 84
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 85
    return-void
.end method

.method private curtir()V
    .registers 6

    .line 146
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 147
    const v2, 0x7f0702dc

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 148
    const v3, 0x7f0702ad

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 149
    const v4, 0x7f070725

    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 151
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->textoIconCurtir:Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 152
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f070268

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 151
    invoke-static {v1}, Lcom/itau/empresas/ui/util/AtendimentoUtil;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/TextViewIcon;->setText(Ljava/lang/CharSequence;)V

    .line 153
    return-void
.end method

.method private descurtir()V
    .registers 6

    .line 135
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 136
    const v2, 0x7f0702dc

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 137
    const v3, 0x7f0702ad

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 138
    const v4, 0x7f070723

    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 140
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->textoIconDescurtir:Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 142
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f07026b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 141
    invoke-static {v1}, Lcom/itau/empresas/ui/util/AtendimentoUtil;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 140
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/TextViewIcon;->setText(Ljava/lang/CharSequence;)V

    .line 143
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 227
    new-instance v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;
    .registers 2
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;

    .line 231
    new-instance v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;-><init>(Landroid/support/v4/app/Fragment;)V

    return-object v0
.end method

.method private mostraDetalhes(Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;)V
    .registers 6
    .param p1, "faq"    # Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->textoAtendimentoPergunta:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->getPergunta()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->textoAtendimentoResposta:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->getResposta()Ljava/lang/String;

    move-result-object v1

    const-string v2, "\\n"

    const-string v3, "\n"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    invoke-virtual {p1}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->getCanaisDisponiveis()Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->carregaCanais(Ljava/util/ArrayList;)V

    .line 92
    invoke-virtual {p1}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->getMobile_relacionadas()Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->carregaRelacionadas(Ljava/util/ArrayList;)V

    .line 93
    return-void
.end method


# virtual methods
.method carregandoElementosDeTela()V
    .registers 4

    .line 71
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->constroiToolbar()V

    .line 72
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 73
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v0, "FAQ"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;

    .line 74
    .local v2, "resposta":Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;
    invoke-direct {p0, v2}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->mostraDetalhes(Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;)V

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->textoAtendimentoPergunta:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/itau/empresas/ui/util/AtendimentoUtil;->setRequestFocus(Landroid/view/View;)V

    .line 76
    return-void
.end method

.method getAnswer(Landroid/view/View;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;

    .line 116
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_1c

    goto :goto_f

    .line 118
    :sswitch_8
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->curtir()V

    .line 119
    goto :goto_f

    .line 123
    :sswitch_c
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->descurtir()V

    .line 124
    .line 130
    :goto_f
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->cartaoFeedBack:Landroid/support/v7/widget/CardView;

    sget-object v1, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->Duration:Ljava/lang/Integer;

    .line 131
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/AnimationUtils;->collapseAnimation(Landroid/view/View;I)V

    .line 132
    return-void

    nop

    :sswitch_data_1c
    .sparse-switch
        0x7f0e05ef -> :sswitch_8
        0x7f0e05f0 -> :sswitch_c
    .end sparse-switch
.end method

.method public hitAnalytics()Ljava/lang/String;
    .registers 2

    .line 218
    const v0, 0x7f07072a

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method mostraRelacionada(Lcom/itau/empresas/feature/atendimento/model/AtendimentoVO;)V
    .registers 6
    .param p1, "atendimento"    # Lcom/itau/empresas/feature/atendimento/model/AtendimentoVO;

    .line 193
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->escondeDialogoDeProgresso()V

    .line 195
    invoke-static {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;

    move-result-object v0

    const-string v1, "FAQ"

    .line 196
    invoke-virtual {p1}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoVO;->getFaqs()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/Serializable;

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;->extra(Ljava/lang/String;Ljava/io/Serializable;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 198
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;

    .line 201
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 202
    return-void

    .line 204
    :cond_7
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->root:Landroid/widget/RelativeLayout;

    const v1, 0x7f0701ef

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 205
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 206
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 209
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 210
    return-void

    .line 212
    :cond_7
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->root:Landroid/widget/RelativeLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 213
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 214
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/atendimento/model/AtendimentoVO;)V
    .registers 2
    .param p1, "atendimento"    # Lcom/itau/empresas/feature/atendimento/model/AtendimentoVO;

    .line 189
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->mostraRelacionada(Lcom/itau/empresas/feature/atendimento/model/AtendimentoVO;)V

    .line 190
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 223
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

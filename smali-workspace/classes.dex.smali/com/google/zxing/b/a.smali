.class public final Lcom/google/zxing/b/a;
.super Lcom/google/zxing/b/b;


# static fields
.field static final a:[[I

.field private static final b:[I

.field private static final d:[I

.field private static final e:[I


# instance fields
.field private c:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    const/4 v0, 0x1

    new-array v0, v0, [I

    fill-array-data v0, :array_7c

    sput-object v0, Lcom/google/zxing/b/a;->b:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_82

    sput-object v0, Lcom/google/zxing/b/a;->d:[I

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_8e

    sput-object v0, Lcom/google/zxing/b/a;->e:[I

    const/16 v0, 0xa

    new-array v0, v0, [[I

    const/4 v1, 0x5

    new-array v1, v1, [I

    fill-array-data v1, :array_98

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v1, 0x5

    new-array v1, v1, [I

    fill-array-data v1, :array_a6

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const/4 v1, 0x5

    new-array v1, v1, [I

    fill-array-data v1, :array_b4

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const/4 v1, 0x5

    new-array v1, v1, [I

    fill-array-data v1, :array_c2

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const/4 v1, 0x5

    new-array v1, v1, [I

    fill-array-data v1, :array_d0

    const/4 v2, 0x4

    aput-object v1, v0, v2

    const/4 v1, 0x5

    new-array v1, v1, [I

    fill-array-data v1, :array_de

    const/4 v2, 0x5

    aput-object v1, v0, v2

    const/4 v1, 0x5

    new-array v1, v1, [I

    fill-array-data v1, :array_ec

    const/4 v2, 0x6

    aput-object v1, v0, v2

    const/4 v1, 0x5

    new-array v1, v1, [I

    fill-array-data v1, :array_fa

    const/4 v2, 0x7

    aput-object v1, v0, v2

    const/4 v1, 0x5

    new-array v1, v1, [I

    fill-array-data v1, :array_108

    const/16 v2, 0x8

    aput-object v1, v0, v2

    const/4 v1, 0x5

    new-array v1, v1, [I

    fill-array-data v1, :array_116

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/zxing/b/a;->a:[[I

    return-void

    nop

    :array_7c
    .array-data 4
        0x2c
    .end array-data

    :array_82
    .array-data 4
        0x1
        0x1
        0x1
        0x1
    .end array-data

    :array_8e
    .array-data 4
        0x1
        0x1
        0x3
    .end array-data

    :array_98
    .array-data 4
        0x1
        0x1
        0x3
        0x3
        0x1
    .end array-data

    :array_a6
    .array-data 4
        0x3
        0x1
        0x1
        0x1
        0x3
    .end array-data

    :array_b4
    .array-data 4
        0x1
        0x3
        0x1
        0x1
        0x3
    .end array-data

    :array_c2
    .array-data 4
        0x3
        0x3
        0x1
        0x1
        0x1
    .end array-data

    :array_d0
    .array-data 4
        0x1
        0x1
        0x3
        0x1
        0x3
    .end array-data

    :array_de
    .array-data 4
        0x3
        0x1
        0x3
        0x1
        0x1
    .end array-data

    :array_ec
    .array-data 4
        0x1
        0x3
        0x3
        0x1
        0x1
    .end array-data

    :array_fa
    .array-data 4
        0x1
        0x1
        0x1
        0x3
        0x3
    .end array-data

    :array_108
    .array-data 4
        0x3
        0x1
        0x1
        0x3
        0x1
    .end array-data

    :array_116
    .array-data 4
        0x1
        0x3
        0x1
        0x3
        0x1
    .end array-data
.end method

.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Lcom/google/zxing/b/b;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/zxing/b/a;->c:I

    return-void
.end method

.method private static a([I)I
    .registers 8

    const v1, 0x3ec28f5c    # 0.38f

    const/4 v2, -0x1

    sget-object v0, Lcom/google/zxing/b/a;->a:[[I

    array-length v3, v0

    const/4 v4, 0x0

    :goto_8
    if-ge v4, v3, :cond_1e

    sget-object v0, Lcom/google/zxing/b/a;->a:[[I

    aget-object v5, v0, v4

    const v0, 0x3f47ae14    # 0.78f

    invoke-static {p0, v5, v0}, Lcom/google/zxing/b/a;->a([I[IF)F

    move-result v6

    cmpg-float v0, v6, v1

    if-gez v0, :cond_1b

    move v1, v6

    move v2, v4

    :cond_1b
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    :cond_1e
    if-ltz v2, :cond_21

    return v2

    :cond_21
    invoke-static {}, Lcom/google/zxing/h;->a()Lcom/google/zxing/h;

    move-result-object v0

    throw v0
.end method

.method private a(Lcom/google/zxing/a/a;I)V
    .registers 7

    iget v0, p0, Lcom/google/zxing/b/a;->c:I

    mul-int/lit8 v2, v0, 0xa

    if-ge v2, p2, :cond_7

    goto :goto_8

    :cond_7
    move v2, p2

    :goto_8
    add-int/lit8 v3, p2, -0x1

    :goto_a
    if-lez v2, :cond_1a

    if-ltz v3, :cond_1a

    invoke-virtual {p1, v3}, Lcom/google/zxing/a/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_15

    goto :goto_1a

    :cond_15
    add-int/lit8 v2, v2, -0x1

    add-int/lit8 v3, v3, -0x1

    goto :goto_a

    :cond_1a
    :goto_1a
    if-eqz v2, :cond_21

    invoke-static {}, Lcom/google/zxing/h;->a()Lcom/google/zxing/h;

    move-result-object v0

    throw v0

    :cond_21
    return-void
.end method

.method private static a(Lcom/google/zxing/a/a;IILjava/lang/StringBuilder;)V
    .registers 13

    const/16 v0, 0xa

    new-array v1, v0, [I

    const/4 v0, 0x5

    new-array v2, v0, [I

    const/4 v0, 0x5

    new-array v3, v0, [I

    :goto_a
    if-ge p1, p2, :cond_42

    invoke-static {p0, p1, v1}, Lcom/google/zxing/b/a;->a(Lcom/google/zxing/a/a;I[I)V

    const/4 v4, 0x0

    :goto_10
    const/4 v0, 0x5

    if-ge v4, v0, :cond_22

    mul-int/lit8 v5, v4, 0x2

    aget v0, v1, v5

    aput v0, v2, v4

    add-int/lit8 v0, v5, 0x1

    aget v0, v1, v0

    aput v0, v3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_10

    :cond_22
    invoke-static {v2}, Lcom/google/zxing/b/a;->a([I)I

    move-result v4

    add-int/lit8 v0, v4, 0x30

    int-to-char v0, v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {v3}, Lcom/google/zxing/b/a;->a([I)I

    move-result v4

    add-int/lit8 v0, v4, 0x30

    int-to-char v0, v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-object v5, v1

    array-length v6, v5

    const/4 v7, 0x0

    :goto_39
    if-ge v7, v6, :cond_41

    aget v8, v5, v7

    add-int/2addr p1, v8

    add-int/lit8 v7, v7, 0x1

    goto :goto_39

    :cond_41
    goto :goto_a

    :cond_42
    return-void
.end method

.method private static b(Lcom/google/zxing/a/a;I[I)[I
    .registers 13

    array-length v3, p2

    new-array v4, v3, [I

    invoke-virtual {p0}, Lcom/google/zxing/a/a;->a()I

    move-result v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move v8, p1

    move v9, p1

    :goto_b
    if-ge v9, v5, :cond_61

    invoke-virtual {p0, v9}, Lcom/google/zxing/a/a;->a(I)Z

    move-result v0

    xor-int/2addr v0, v6

    if-eqz v0, :cond_1b

    aget v0, v4, v7

    add-int/lit8 v0, v0, 0x1

    aput v0, v4, v7

    goto :goto_5d

    :cond_1b
    add-int/lit8 v0, v3, -0x1

    if-ne v7, v0, :cond_53

    const v0, 0x3f47ae14    # 0.78f

    invoke-static {v4, p2, v0}, Lcom/google/zxing/b/a;->a([I[IF)F

    move-result v0

    const v1, 0x3ec28f5c    # 0.38f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_37

    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput v8, v0, v1

    const/4 v1, 0x1

    aput v9, v0, v1

    return-object v0

    :cond_37
    const/4 v0, 0x0

    aget v0, v4, v0

    const/4 v1, 0x1

    aget v1, v4, v1

    add-int/2addr v0, v1

    add-int/2addr v8, v0

    add-int/lit8 v0, v3, -0x2

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v4, v1, v4, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v0, v3, -0x2

    const/4 v1, 0x0

    aput v1, v4, v0

    add-int/lit8 v0, v3, -0x1

    const/4 v1, 0x0

    aput v1, v4, v0

    add-int/lit8 v7, v7, -0x1

    goto :goto_55

    :cond_53
    add-int/lit8 v7, v7, 0x1

    :goto_55
    const/4 v0, 0x1

    aput v0, v4, v7

    if-nez v6, :cond_5c

    const/4 v6, 0x1

    goto :goto_5d

    :cond_5c
    const/4 v6, 0x0

    :goto_5d
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_b

    :cond_61
    invoke-static {}, Lcom/google/zxing/h;->a()Lcom/google/zxing/h;

    move-result-object v0

    throw v0
.end method

.method private static c(Lcom/google/zxing/a/a;)I
    .registers 4

    invoke-virtual {p0}, Lcom/google/zxing/a/a;->a()I

    move-result v1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/zxing/a/a;->c(I)I

    move-result v2

    if-ne v2, v1, :cond_10

    invoke-static {}, Lcom/google/zxing/h;->a()Lcom/google/zxing/h;

    move-result-object v0

    throw v0

    :cond_10
    return v2
.end method


# virtual methods
.method public a(ILcom/google/zxing/a/a;Ljava/util/Map;)Lcom/google/zxing/k;
    .registers 21

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/zxing/b/a;->a(Lcom/google/zxing/a/a;)[I

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/zxing/b/a;->b(Lcom/google/zxing/a/a;)[I

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    const/16 v0, 0x14

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v0, 0x1

    aget v0, v5, v0

    const/4 v1, 0x0

    aget v1, v6, v1

    move-object/from16 v2, p2

    invoke-static {v2, v0, v1, v7}, Lcom/google/zxing/b/a;->a(Lcom/google/zxing/a/a;IILjava/lang/StringBuilder;)V

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    if-eqz p3, :cond_36

    sget-object v0, Lcom/google/zxing/e;->f:Lcom/google/zxing/e;

    move-object/from16 v1, p3

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    move-object v9, v0

    check-cast v9, [I

    :cond_36
    if-nez v9, :cond_3a

    sget-object v9, Lcom/google/zxing/b/a;->b:[I

    :cond_3a
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v13, v9

    array-length v14, v13

    const/4 v15, 0x0

    :goto_43
    if-ge v15, v14, :cond_56

    aget v16, v13, v15

    move/from16 v0, v16

    if-ne v10, v0, :cond_4d

    const/4 v11, 0x1

    goto :goto_56

    :cond_4d
    move/from16 v0, v16

    if-le v0, v12, :cond_53

    move/from16 v12, v16

    :cond_53
    add-int/lit8 v15, v15, 0x1

    goto :goto_43

    :cond_56
    :goto_56
    if-nez v11, :cond_5b

    if-le v10, v12, :cond_5b

    const/4 v11, 0x1

    :cond_5b
    if-nez v11, :cond_62

    invoke-static {}, Lcom/google/zxing/f;->a()Lcom/google/zxing/f;

    move-result-object v0

    throw v0

    :cond_62
    new-instance v0, Lcom/google/zxing/k;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/zxing/m;

    new-instance v2, Lcom/google/zxing/m;

    const/4 v3, 0x1

    aget v3, v5, v3

    int-to-float v3, v3

    move/from16 v4, p1

    int-to-float v4, v4

    invoke-direct {v2, v3, v4}, Lcom/google/zxing/m;-><init>(FF)V

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lcom/google/zxing/m;

    const/4 v3, 0x0

    aget v3, v6, v3

    int-to-float v3, v3

    move/from16 v4, p1

    int-to-float v4, v4

    invoke-direct {v2, v3, v4}, Lcom/google/zxing/m;-><init>(FF)V

    const/4 v3, 0x1

    aput-object v2, v1, v3

    sget-object v2, Lcom/google/zxing/a;->a:Lcom/google/zxing/a;

    const/4 v3, 0x0

    invoke-direct {v0, v8, v3, v1, v2}, Lcom/google/zxing/k;-><init>(Ljava/lang/String;[B[Lcom/google/zxing/m;Lcom/google/zxing/a;)V

    return-object v0
.end method

.method a(Lcom/google/zxing/a/a;)[I
    .registers 6

    invoke-static {p1}, Lcom/google/zxing/b/a;->c(Lcom/google/zxing/a/a;)I

    move-result v2

    sget-object v0, Lcom/google/zxing/b/a;->d:[I

    invoke-static {p1, v2, v0}, Lcom/google/zxing/b/a;->b(Lcom/google/zxing/a/a;I[I)[I

    move-result-object v3

    const/4 v0, 0x1

    aget v0, v3, v0

    const/4 v1, 0x0

    aget v1, v3, v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/zxing/b/a;->c:I

    const/4 v0, 0x0

    aget v0, v3, v0

    invoke-direct {p0, p1, v0}, Lcom/google/zxing/b/a;->a(Lcom/google/zxing/a/a;I)V

    return-object v3
.end method

.method b(Lcom/google/zxing/a/a;)[I
    .registers 9

    invoke-virtual {p1}, Lcom/google/zxing/a/a;->c()V

    :try_start_3
    invoke-static {p1}, Lcom/google/zxing/b/a;->c(Lcom/google/zxing/a/a;)I

    move-result v2

    sget-object v0, Lcom/google/zxing/b/a;->e:[I

    invoke-static {p1, v2, v0}, Lcom/google/zxing/b/a;->b(Lcom/google/zxing/a/a;I[I)[I

    move-result-object v3

    const/4 v0, 0x0

    aget v0, v3, v0

    invoke-direct {p0, p1, v0}, Lcom/google/zxing/b/a;->a(Lcom/google/zxing/a/a;I)V

    const/4 v0, 0x0

    aget v4, v3, v0

    invoke-virtual {p1}, Lcom/google/zxing/a/a;->a()I

    move-result v0

    const/4 v1, 0x1

    aget v1, v3, v1

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    aput v0, v3, v1

    invoke-virtual {p1}, Lcom/google/zxing/a/a;->a()I

    move-result v0

    sub-int/2addr v0, v4

    const/4 v1, 0x1

    aput v0, v3, v1
    :try_end_29
    .catchall {:try_start_3 .. :try_end_29} :catchall_2e

    move-object v5, v3

    invoke-virtual {p1}, Lcom/google/zxing/a/a;->c()V

    return-object v5

    :catchall_2e
    move-exception v6

    invoke-virtual {p1}, Lcom/google/zxing/a/a;->c()V

    throw v6
.end method

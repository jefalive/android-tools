.class public Lcom/itau/empresas/ui/activity/GenericaActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "GenericaActivity.java"


# instance fields
.field fragmentClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<+Lcom/itau/empresas/ui/fragment/BaseFragment;>;"
        }
    .end annotation
.end field

.field private idTituloToolbar:I

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 25
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 89
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/GenericaActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    iget v1, p0, Lcom/itau/empresas/ui/activity/GenericaActivity;->idTituloToolbar:I

    .line 90
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 93
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 96
    new-instance v0, Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/activity/GenericaActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method protected aoInicializar()V
    .registers 7

    .line 34
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/GenericaActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 36
    .local v2, "extras":Landroid/os/Bundle;
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/GenericaActivity;->fragmentClass:Ljava/lang/Class;

    if-nez v0, :cond_d

    .line 37
    return-void

    .line 39
    :cond_d
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/GenericaActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 42
    .local v3, "transaction":Landroid/support/v4/app/FragmentTransaction;
    :try_start_15
    iget-object v0, p0, Lcom/itau/empresas/ui/activity/GenericaActivity;->fragmentClass:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/itau/empresas/ui/fragment/BaseFragment;

    .line 44
    .local v4, "fragment":Lcom/itau/empresas/ui/fragment/BaseFragment;
    if-eqz v2, :cond_23

    .line 45
    invoke-virtual {v4, v2}, Lcom/itau/empresas/ui/fragment/BaseFragment;->setArguments(Landroid/os/Bundle;)V

    .line 48
    :cond_23
    const v0, 0x7f0e0191

    invoke-virtual {v3, v0, v4}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;
    :try_end_29
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_29} :catch_2d
    .catchall {:try_start_15 .. :try_end_29} :catchall_3e

    .line 53
    .end local v4    # "fragment":Lcom/itau/empresas/ui/fragment/BaseFragment;
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 54
    goto :goto_43

    .line 49
    :catch_2d
    move-exception v4

    .line 50
    .local v4, "e":Ljava/lang/Exception;
    :try_start_2e
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 51
    const-string v0, "generica"

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3a
    .catchall {:try_start_2e .. :try_end_3a} :catchall_3e

    .line 53
    .end local v4    # "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 54
    goto :goto_43

    .line 53
    :catchall_3e
    move-exception v5

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    throw v5

    .line 55
    :goto_43
    return-void
.end method

.method public hitAnalytics()Ljava/lang/String;
    .registers 2

    .line 59
    iget v0, p0, Lcom/itau/empresas/ui/activity/GenericaActivity;->idTituloToolbar:I

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/activity/GenericaActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .param p1, "menu"    # Landroid/view/Menu;

    .line 69
    invoke-virtual {p0}, Lcom/itau/empresas/ui/activity/GenericaActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0007

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 70
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 75
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0e06d1

    if-ne v0, v1, :cond_e

    .line 77
    invoke-static {p0}, Lcom/itau/empresas/ui/util/ViewUtils;->sairDoApp(Landroid/content/Context;)V

    .line 78
    const/4 v0, 0x1

    return v0

    .line 80
    :cond_e
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 64
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public setTitle(I)V
    .registers 2
    .param p1, "res"    # I

    .line 84
    iput p1, p0, Lcom/itau/empresas/ui/activity/GenericaActivity;->idTituloToolbar:I

    .line 85
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/GenericaActivity;->constroiToolbar()V

    .line 86
    return-void
.end method

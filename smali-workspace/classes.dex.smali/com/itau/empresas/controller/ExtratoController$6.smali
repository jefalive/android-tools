.class Lcom/itau/empresas/controller/ExtratoController$6;
.super Ljava/lang/Object;
.source "ExtratoController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/controller/ExtratoController;->consultaMultiLimite()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/controller/ExtratoController;

.field final synthetic val$contaSelecionada:Lcom/itau/empresas/api/model/ContaOperadorVO;


# direct methods
.method constructor <init>(Lcom/itau/empresas/controller/ExtratoController;Lcom/itau/empresas/api/model/ContaOperadorVO;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/controller/ExtratoController;

    .line 89
    iput-object p1, p0, Lcom/itau/empresas/controller/ExtratoController$6;->this$0:Lcom/itau/empresas/controller/ExtratoController;

    iput-object p2, p0, Lcom/itau/empresas/controller/ExtratoController$6;->val$contaSelecionada:Lcom/itau/empresas/api/model/ContaOperadorVO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 6

    .line 92
    iget-object v0, p0, Lcom/itau/empresas/controller/ExtratoController$6;->this$0:Lcom/itau/empresas/controller/ExtratoController;

    iget-object v0, v0, Lcom/itau/empresas/controller/ExtratoController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    const-string v1, "multiLimites"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->inicioMonitorarTempo(Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/itau/empresas/controller/ExtratoController$6;->this$0:Lcom/itau/empresas/controller/ExtratoController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/ExtratoController;->api()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    iget-object v1, p0, Lcom/itau/empresas/controller/ExtratoController$6;->val$contaSelecionada:Lcom/itau/empresas/api/model/ContaOperadorVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/controller/ExtratoController$6;->val$contaSelecionada:Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 94
    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/itau/empresas/controller/ExtratoController$6;->val$contaSelecionada:Lcom/itau/empresas/api/model/ContaOperadorVO;

    .line 95
    invoke-virtual {v3}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    .line 93
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/itau/empresas/api/Api;->consultaMultiLimite(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/BaseController;->post(Ljava/lang/Object;)V

    .line 96
    iget-object v0, p0, Lcom/itau/empresas/controller/ExtratoController$6;->this$0:Lcom/itau/empresas/controller/ExtratoController;

    iget-object v0, v0, Lcom/itau/empresas/controller/ExtratoController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    const-string v1, "multiLimites"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->fimMonitorarTempo(Ljava/lang/String;)V

    .line 97
    return-void
.end method

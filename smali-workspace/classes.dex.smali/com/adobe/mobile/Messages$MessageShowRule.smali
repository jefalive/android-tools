.class public final enum Lcom/adobe/mobile/Messages$MessageShowRule;
.super Ljava/lang/Enum;
.source "Messages.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adobe/mobile/Messages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "MessageShowRule"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lcom/adobe/mobile/Messages$MessageShowRule;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/adobe/mobile/Messages$MessageShowRule;

.field public static final enum MESSAGE_SHOW_RULE_ALWAYS:Lcom/adobe/mobile/Messages$MessageShowRule;

.field public static final enum MESSAGE_SHOW_RULE_ONCE:Lcom/adobe/mobile/Messages$MessageShowRule;

.field public static final enum MESSAGE_SHOW_RULE_UNKNOWN:Lcom/adobe/mobile/Messages$MessageShowRule;

.field public static final enum MESSAGE_SHOW_RULE_UNTIL_CLICK:Lcom/adobe/mobile/Messages$MessageShowRule;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 62
    new-instance v0, Lcom/adobe/mobile/Messages$MessageShowRule;

    const-string v1, "MESSAGE_SHOW_RULE_UNKNOWN"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/adobe/mobile/Messages$MessageShowRule;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/adobe/mobile/Messages$MessageShowRule;->MESSAGE_SHOW_RULE_UNKNOWN:Lcom/adobe/mobile/Messages$MessageShowRule;

    .line 63
    new-instance v0, Lcom/adobe/mobile/Messages$MessageShowRule;

    const-string v1, "MESSAGE_SHOW_RULE_ALWAYS"

    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/adobe/mobile/Messages$MessageShowRule;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/adobe/mobile/Messages$MessageShowRule;->MESSAGE_SHOW_RULE_ALWAYS:Lcom/adobe/mobile/Messages$MessageShowRule;

    .line 64
    new-instance v0, Lcom/adobe/mobile/Messages$MessageShowRule;

    const-string v1, "MESSAGE_SHOW_RULE_ONCE"

    const/4 v2, 0x2

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, Lcom/adobe/mobile/Messages$MessageShowRule;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/adobe/mobile/Messages$MessageShowRule;->MESSAGE_SHOW_RULE_ONCE:Lcom/adobe/mobile/Messages$MessageShowRule;

    .line 65
    new-instance v0, Lcom/adobe/mobile/Messages$MessageShowRule;

    const-string v1, "MESSAGE_SHOW_RULE_UNTIL_CLICK"

    const/4 v2, 0x3

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, Lcom/adobe/mobile/Messages$MessageShowRule;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/adobe/mobile/Messages$MessageShowRule;->MESSAGE_SHOW_RULE_UNTIL_CLICK:Lcom/adobe/mobile/Messages$MessageShowRule;

    .line 61
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/adobe/mobile/Messages$MessageShowRule;

    sget-object v1, Lcom/adobe/mobile/Messages$MessageShowRule;->MESSAGE_SHOW_RULE_UNKNOWN:Lcom/adobe/mobile/Messages$MessageShowRule;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/adobe/mobile/Messages$MessageShowRule;->MESSAGE_SHOW_RULE_ALWAYS:Lcom/adobe/mobile/Messages$MessageShowRule;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/adobe/mobile/Messages$MessageShowRule;->MESSAGE_SHOW_RULE_ONCE:Lcom/adobe/mobile/Messages$MessageShowRule;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/adobe/mobile/Messages$MessageShowRule;->MESSAGE_SHOW_RULE_UNTIL_CLICK:Lcom/adobe/mobile/Messages$MessageShowRule;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lcom/adobe/mobile/Messages$MessageShowRule;->$VALUES:[Lcom/adobe/mobile/Messages$MessageShowRule;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .registers 4
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 68
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 69
    iput p3, p0, Lcom/adobe/mobile/Messages$MessageShowRule;->value:I

    .line 70
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/adobe/mobile/Messages$MessageShowRule;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 61
    const-class v0, Lcom/adobe/mobile/Messages$MessageShowRule;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/adobe/mobile/Messages$MessageShowRule;

    return-object v0
.end method

.method public static values()[Lcom/adobe/mobile/Messages$MessageShowRule;
    .registers 1

    .line 61
    sget-object v0, Lcom/adobe/mobile/Messages$MessageShowRule;->$VALUES:[Lcom/adobe/mobile/Messages$MessageShowRule;

    invoke-virtual {v0}, [Lcom/adobe/mobile/Messages$MessageShowRule;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/adobe/mobile/Messages$MessageShowRule;

    return-object v0
.end method


# virtual methods
.method protected getValue()I
    .registers 2

    .line 73
    iget v0, p0, Lcom/adobe/mobile/Messages$MessageShowRule;->value:I

    return v0
.end method

.class public Lcom/itau/empresas/feature/login/model/PreLoginVO;
.super Ljava/lang/Object;
.source "PreLoginVO.java"


# instance fields
.field public final chaveAcesso:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "chave_acesso"
    .end annotation
.end field

.field public final tipoLogon:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipoLogon"
    .end annotation
.end field

.field public final usuario:Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "usuario"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;Z)V
    .registers 4
    .param p1, "usuario"    # Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;
    .param p2, "chaveAcesso"    # Z

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/16 v0, 0x32

    iput v0, p0, Lcom/itau/empresas/feature/login/model/PreLoginVO;->tipoLogon:I

    .line 21
    iput-object p1, p0, Lcom/itau/empresas/feature/login/model/PreLoginVO;->usuario:Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;

    .line 22
    iput-boolean p2, p0, Lcom/itau/empresas/feature/login/model/PreLoginVO;->chaveAcesso:Z

    .line 23
    return-void
.end method

.class public Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;
.super Ljava/lang/Object;
.source "RecargaComprovanteVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private codigoAutenticacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_autenticacao"
    .end annotation
.end field

.field private codigoComprovante:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_comprovante"
    .end annotation
.end field

.field private dataPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_pagamento"
    .end annotation
.end field

.field private horaPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "hora_pagamento"
    .end annotation
.end field

.field private statusPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "status_pagamento"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCodigoAutenticacao()Ljava/lang/String;
    .registers 2

    .line 25
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;->codigoAutenticacao:Ljava/lang/String;

    return-object v0
.end method

.method public getCodigoComprovante()Ljava/lang/String;
    .registers 2

    .line 21
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;->codigoComprovante:Ljava/lang/String;

    return-object v0
.end method

.method public getDataPagamento()Ljava/lang/String;
    .registers 2

    .line 29
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;->dataPagamento:Ljava/lang/String;

    return-object v0
.end method

.method public getHoraPagamento()Ljava/lang/String;
    .registers 2

    .line 33
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;->horaPagamento:Ljava/lang/String;

    return-object v0
.end method

.method public getStatusPagamento()Ljava/lang/String;
    .registers 2

    .line 37
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/efetivacao/model/RecargaComprovanteVO;->statusPagamento:Ljava/lang/String;

    return-object v0
.end method

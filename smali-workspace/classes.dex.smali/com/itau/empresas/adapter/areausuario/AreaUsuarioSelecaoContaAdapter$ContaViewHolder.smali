.class public Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "AreaUsuarioSelecaoContaAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ContaViewHolder"
.end annotation


# instance fields
.field private rlItemListaContas:Landroid/widget/RelativeLayout;

.field private textoAgencia:Landroid/widget/TextView;

.field private textoConta:Landroid/widget/TextView;

.field private textoIcone:Lcom/itau/empresas/ui/view/TextViewIcon;

.field private textoNome:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;


# direct methods
.method private constructor <init>(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;Landroid/view/View;)V
    .registers 4
    .param p1, "this$0"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;
    .param p2, "itemView"    # Landroid/view/View;

    .line 165
    iput-object p1, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->this$0:Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;

    .line 166
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 168
    .line 169
    const v0, 0x7f0e0524

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->rlItemListaContas:Landroid/widget/RelativeLayout;

    .line 170
    const v0, 0x7f0e0525

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/TextViewIcon;

    iput-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->textoIcone:Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 171
    const v0, 0x7f0e02b1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->textoNome:Landroid/widget/TextView;

    .line 172
    const v0, 0x7f0e0509

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->textoAgencia:Landroid/widget/TextView;

    .line 173
    const v0, 0x7f0e050b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->textoConta:Landroid/widget/TextView;

    .line 175
    new-instance v0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder$1;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder$1;-><init>(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    return-void
.end method

.method synthetic constructor <init>(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;Landroid/view/View;Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$1;)V
    .registers 4
    .param p1, "x0"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;
    .param p2, "x1"    # Landroid/view/View;
    .param p3, "x2"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$1;

    .line 158
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;-><init>(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter;Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$200(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;)Landroid/widget/TextView;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;

    .line 158
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->textoNome:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;)Landroid/widget/TextView;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;

    .line 158
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->textoAgencia:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;)Landroid/widget/TextView;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;

    .line 158
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->textoConta:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;)Landroid/widget/RelativeLayout;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;

    .line 158
    iget-object v0, p0, Lcom/itau/empresas/adapter/areausuario/AreaUsuarioSelecaoContaAdapter$ContaViewHolder;->rlItemListaContas:Landroid/widget/RelativeLayout;

    return-object v0
.end method

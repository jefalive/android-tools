.class public final Lorg/joda/money/format/MoneyParseContext;
.super Ljava/lang/Object;
.source "MoneyParseContext.java"


# instance fields
.field private amount:Ljava/math/BigDecimal;

.field private currency:Lorg/joda/money/CurrencyUnit;

.field private locale:Ljava/util/Locale;

.field private text:Ljava/lang/CharSequence;

.field private textErrorIndex:I

.field private textIndex:I


# direct methods
.method constructor <init>(Ljava/util/Locale;Ljava/lang/CharSequence;I)V
    .registers 5
    .param p1, "locale"    # Ljava/util/Locale;
    .param p2, "text"    # Ljava/lang/CharSequence;
    .param p3, "index"    # I

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lorg/joda/money/format/MoneyParseContext;->textErrorIndex:I

    .line 66
    iput-object p1, p0, Lorg/joda/money/format/MoneyParseContext;->locale:Ljava/util/Locale;

    .line 67
    iput-object p2, p0, Lorg/joda/money/format/MoneyParseContext;->text:Ljava/lang/CharSequence;

    .line 68
    iput p3, p0, Lorg/joda/money/format/MoneyParseContext;->textIndex:I

    .line 69
    return-void
.end method


# virtual methods
.method public getAmount()Ljava/math/BigDecimal;
    .registers 2

    .line 201
    iget-object v0, p0, Lorg/joda/money/format/MoneyParseContext;->amount:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getCurrency()Lorg/joda/money/CurrencyUnit;
    .registers 2

    .line 182
    iget-object v0, p0, Lorg/joda/money/format/MoneyParseContext;->currency:Lorg/joda/money/CurrencyUnit;

    return-object v0
.end method

.method public getErrorIndex()I
    .registers 2

    .line 156
    iget v0, p0, Lorg/joda/money/format/MoneyParseContext;->textErrorIndex:I

    return v0
.end method

.method public getIndex()I
    .registers 2

    .line 137
    iget v0, p0, Lorg/joda/money/format/MoneyParseContext;->textIndex:I

    return v0
.end method

.method public getLocale()Ljava/util/Locale;
    .registers 2

    .line 78
    iget-object v0, p0, Lorg/joda/money/format/MoneyParseContext;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method public getText()Ljava/lang/CharSequence;
    .registers 2

    .line 97
    iget-object v0, p0, Lorg/joda/money/format/MoneyParseContext;->text:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTextLength()I
    .registers 2

    .line 116
    iget-object v0, p0, Lorg/joda/money/format/MoneyParseContext;->text:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    return v0
.end method

.method public getTextSubstring(II)Ljava/lang/String;
    .registers 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .line 127
    iget-object v0, p0, Lorg/joda/money/format/MoneyParseContext;->text:Ljava/lang/CharSequence;

    invoke-interface {v0, p1, p2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isComplete()Z
    .registers 2

    .line 239
    iget-object v0, p0, Lorg/joda/money/format/MoneyParseContext;->currency:Lorg/joda/money/CurrencyUnit;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lorg/joda/money/format/MoneyParseContext;->amount:Ljava/math/BigDecimal;

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method public isError()Z
    .registers 2

    .line 220
    iget v0, p0, Lorg/joda/money/format/MoneyParseContext;->textErrorIndex:I

    if-ltz v0, :cond_6

    const/4 v0, 0x1

    goto :goto_7

    :cond_6
    const/4 v0, 0x0

    :goto_7
    return v0
.end method

.method public isFullyParsed()Z
    .registers 3

    .line 229
    iget v0, p0, Lorg/joda/money/format/MoneyParseContext;->textIndex:I

    invoke-virtual {p0}, Lorg/joda/money/format/MoneyParseContext;->getTextLength()I

    move-result v1

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method public setAmount(Ljava/math/BigDecimal;)V
    .registers 2
    .param p1, "amount"    # Ljava/math/BigDecimal;

    .line 210
    iput-object p1, p0, Lorg/joda/money/format/MoneyParseContext;->amount:Ljava/math/BigDecimal;

    .line 211
    return-void
.end method

.method public setCurrency(Lorg/joda/money/CurrencyUnit;)V
    .registers 2
    .param p1, "currency"    # Lorg/joda/money/CurrencyUnit;

    .line 191
    iput-object p1, p0, Lorg/joda/money/format/MoneyParseContext;->currency:Lorg/joda/money/CurrencyUnit;

    .line 192
    return-void
.end method

.method public setError()V
    .registers 2

    .line 172
    iget v0, p0, Lorg/joda/money/format/MoneyParseContext;->textIndex:I

    iput v0, p0, Lorg/joda/money/format/MoneyParseContext;->textErrorIndex:I

    .line 173
    return-void
.end method

.method public setErrorIndex(I)V
    .registers 2
    .param p1, "index"    # I

    .line 165
    iput p1, p0, Lorg/joda/money/format/MoneyParseContext;->textErrorIndex:I

    .line 166
    return-void
.end method

.method public setIndex(I)V
    .registers 2
    .param p1, "index"    # I

    .line 146
    iput p1, p0, Lorg/joda/money/format/MoneyParseContext;->textIndex:I

    .line 147
    return-void
.end method

.method public setLocale(Ljava/util/Locale;)V
    .registers 3
    .param p1, "locale"    # Ljava/util/Locale;

    .line 87
    const-string v0, "Locale must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/format/MoneyFormatter;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    iput-object p1, p0, Lorg/joda/money/format/MoneyParseContext;->locale:Ljava/util/Locale;

    .line 89
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .registers 3
    .param p1, "text"    # Ljava/lang/CharSequence;

    .line 106
    const-string v0, "Text must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/format/MoneyFormatter;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    iput-object p1, p0, Lorg/joda/money/format/MoneyParseContext;->text:Ljava/lang/CharSequence;

    .line 108
    return-void
.end method

.method public toBigMoney()Lorg/joda/money/BigMoney;
    .registers 3

    .line 261
    iget-object v0, p0, Lorg/joda/money/format/MoneyParseContext;->currency:Lorg/joda/money/CurrencyUnit;

    if-nez v0, :cond_c

    .line 262
    new-instance v0, Lorg/joda/money/format/MoneyFormatException;

    const-string v1, "Cannot convert to BigMoney as no currency found"

    invoke-direct {v0, v1}, Lorg/joda/money/format/MoneyFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 264
    :cond_c
    iget-object v0, p0, Lorg/joda/money/format/MoneyParseContext;->amount:Ljava/math/BigDecimal;

    if-nez v0, :cond_18

    .line 265
    new-instance v0, Lorg/joda/money/format/MoneyFormatException;

    const-string v1, "Cannot convert to BigMoney as no amount found"

    invoke-direct {v0, v1}, Lorg/joda/money/format/MoneyFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 267
    :cond_18
    iget-object v0, p0, Lorg/joda/money/format/MoneyParseContext;->currency:Lorg/joda/money/CurrencyUnit;

    iget-object v1, p0, Lorg/joda/money/format/MoneyParseContext;->amount:Ljava/math/BigDecimal;

    invoke-static {v0, v1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public toParsePosition()Ljava/text/ParsePosition;
    .registers 3

    .line 249
    new-instance v1, Ljava/text/ParsePosition;

    iget v0, p0, Lorg/joda/money/format/MoneyParseContext;->textIndex:I

    invoke-direct {v1, v0}, Ljava/text/ParsePosition;-><init>(I)V

    .line 250
    .local v1, "pp":Ljava/text/ParsePosition;
    iget v0, p0, Lorg/joda/money/format/MoneyParseContext;->textErrorIndex:I

    invoke-virtual {v1, v0}, Ljava/text/ParsePosition;->setErrorIndex(I)V

    .line 251
    return-object v1
.end method

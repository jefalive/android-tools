.class public Lcom/itau/empresas/feature/extrato/ExtratoFilter;
.super Landroid/widget/Filter;
.source "ExtratoFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;,
        Lcom/itau/empresas/feature/extrato/ExtratoFilter$ExtratoFilterCallback;
    }
.end annotation


# instance fields
.field private listaLancamentos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/LancamentoVO;>;"
        }
    .end annotation
.end field

.field private listener:Lcom/itau/empresas/feature/extrato/ExtratoFilter$ExtratoFilterCallback;

.field private listenerTextoBusca:Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/itau/empresas/feature/extrato/ExtratoFilter$ExtratoFilterCallback;Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;)V
    .registers 4
    .param p1, "listaLancamentos"    # Ljava/util/List;
    .param p2, "listener"    # Lcom/itau/empresas/feature/extrato/ExtratoFilter$ExtratoFilterCallback;
    .param p3, "listenerTextoBusca"    # Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/LancamentoVO;>;Lcom/itau/empresas/feature/extrato/ExtratoFilter$ExtratoFilterCallback;Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/itau/empresas/feature/extrato/ExtratoFilter;->listaLancamentos:Ljava/util/List;

    .line 21
    iput-object p2, p0, Lcom/itau/empresas/feature/extrato/ExtratoFilter;->listener:Lcom/itau/empresas/feature/extrato/ExtratoFilter$ExtratoFilterCallback;

    .line 22
    iput-object p3, p0, Lcom/itau/empresas/feature/extrato/ExtratoFilter;->listenerTextoBusca:Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;

    .line 23
    return-void
.end method

.method private static encontrouResultado(Lcom/itau/empresas/api/model/LancamentoVO;Ljava/lang/String;)Z
    .registers 7
    .param p0, "lancamentoVO"    # Lcom/itau/empresas/api/model/LancamentoVO;
    .param p1, "busca"    # Ljava/lang/String;

    .line 63
    invoke-virtual {p0}, Lcom/itau/empresas/api/model/LancamentoVO;->getValorLancamento()Ljava/math/BigDecimal;

    move-result-object v3

    .line 64
    .local v3, "valorLancamento":Ljava/math/BigDecimal;
    const-string v0, "[.,]"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 66
    .local v4, "buscaSatinizada":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/itau/empresas/api/model/LancamentoVO;->getDescricaoLancamento()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 67
    const/4 v0, 0x1

    return v0

    .line 68
    :cond_20
    const/4 v0, 0x0

    invoke-static {v3, v0}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v0

    const-string v1, "[^0-9]"

    const-string v2, ""

    .line 69
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_35

    .line 70
    const/4 v0, 0x1

    return v0

    .line 73
    :cond_35
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .registers 7
    .param p1, "busca"    # Ljava/lang/CharSequence;

    .line 27
    new-instance v1, Landroid/widget/Filter$FilterResults;

    invoke-direct {v1}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 30
    .local v1, "resultado":Landroid/widget/Filter$FilterResults;
    if-eqz p1, :cond_d

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1a

    .line 31
    :cond_d
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/ExtratoFilter;->listaLancamentos:Ljava/util/List;

    iput-object v0, v1, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 32
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/ExtratoFilter;->listaLancamentos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v1, Landroid/widget/Filter$FilterResults;->count:I

    goto :goto_48

    .line 34
    :cond_1a
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 35
    .local v2, "listaLancamentosFiltrada":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/LancamentoVO;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/ExtratoFilter;->listaLancamentos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_25
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_40

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/itau/empresas/api/model/LancamentoVO;

    .line 36
    .local v4, "l":Lcom/itau/empresas/api/model/LancamentoVO;
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/itau/empresas/feature/extrato/ExtratoFilter;->encontrouResultado(Lcom/itau/empresas/api/model/LancamentoVO;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 37
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    .end local v4    # "l":Lcom/itau/empresas/api/model/LancamentoVO;
    :cond_3f
    goto :goto_25

    .line 40
    :cond_40
    iput-object v2, v1, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 41
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v1, Landroid/widget/Filter$FilterResults;->count:I

    .line 44
    .end local v2    # "listaLancamentosFiltrada":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/LancamentoVO;>;"
    .end local v2
    :goto_48
    return-object v1
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .registers 5
    .param p1, "constraint"    # Ljava/lang/CharSequence;
    .param p2, "resultado"    # Landroid/widget/Filter$FilterResults;

    .line 50
    iget v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-lez v0, :cond_1c

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/ExtratoFilter;->listener:Lcom/itau/empresas/feature/extrato/ExtratoFilter$ExtratoFilterCallback;

    iget-object v1, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v1, Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/extrato/ExtratoFilter$ExtratoFilterCallback;->aplicarResultadoDeBusca(Ljava/util/List;)V

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/ExtratoFilter;->listenerTextoBusca:Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;->textoDaBusca(Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/ExtratoFilter;->listener:Lcom/itau/empresas/feature/extrato/ExtratoFilter$ExtratoFilterCallback;

    invoke-interface {v0}, Lcom/itau/empresas/feature/extrato/ExtratoFilter$ExtratoFilterCallback;->mostraEstadoOriginal()V

    goto :goto_33

    .line 55
    :cond_1c
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/ExtratoFilter;->listenerTextoBusca:Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;->textoDaBusca(Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/ExtratoFilter;->listener:Lcom/itau/empresas/feature/extrato/ExtratoFilter$ExtratoFilterCallback;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/extrato/ExtratoFilter$ExtratoFilterCallback;->exibeTextoDaBusca(Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/ExtratoFilter;->listener:Lcom/itau/empresas/feature/extrato/ExtratoFilter$ExtratoFilterCallback;

    invoke-interface {v0}, Lcom/itau/empresas/feature/extrato/ExtratoFilter$ExtratoFilterCallback;->exibeEmptyState()V

    .line 59
    :goto_33
    return-void
.end method

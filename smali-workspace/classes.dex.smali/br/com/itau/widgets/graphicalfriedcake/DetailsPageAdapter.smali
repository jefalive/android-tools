.class public Lbr/com/itau/widgets/graphicalfriedcake/DetailsPageAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "DetailsPageAdapter.java"


# instance fields
.field private context:Landroid/content/Context;

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "items"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;>;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 19
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/DetailsPageAdapter;->context:Landroid/content/Context;

    .line 20
    iput-object p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/DetailsPageAdapter;->items:Ljava/util/List;

    .line 21
    return-void
.end method

.method private formatLine(Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;)Ljava/lang/String;
    .registers 6
    .param p1, "item"    # Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;

    .line 55
    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->getCount()I

    move-result v0

    if-lez v0, :cond_2f

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->getCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getPercentualFormatter()Ljava/text/DecimalFormat;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->getPercentual()F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 57
    :cond_2f
    invoke-static {}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getPercentualFormatter()Ljava/text/DecimalFormat;

    move-result-object v0

    invoke-virtual {p1}, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->getPercentual()F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .registers 5
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .line 62
    move-object v0, p3

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 63
    return-void
.end method

.method public getCount()I
    .registers 3

    .line 30
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/DetailsPageAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .registers 10
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .line 40
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/DetailsPageAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    .line 43
    .local v4, "size":I
    add-int/lit8 v0, v4, -0x1

    if-le p2, v0, :cond_c

    .line 44
    const/4 v0, 0x0

    return-object v0

    .line 46
    :cond_c
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/DetailsPageAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;

    .line 47
    .local v5, "item":Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;
    new-instance v6, Lbr/com/itau/widgets/graphicalfriedcake/ItemDetailsView;

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/DetailsPageAdapter;->context:Landroid/content/Context;

    invoke-direct {v6, v0}, Lbr/com/itau/widgets/graphicalfriedcake/ItemDetailsView;-><init>(Landroid/content/Context;)V

    .line 48
    .local v6, "view":Lbr/com/itau/widgets/graphicalfriedcake/ItemDetailsView;
    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v5}, Lbr/com/itau/widgets/graphicalfriedcake/DetailsPageAdapter;->formatLine(Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Lbr/com/itau/widgets/graphicalfriedcake/model/ItemVO;->getColor()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v0, v1, v2, v3}, Lbr/com/itau/widgets/graphicalfriedcake/ItemDetailsView;->init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    invoke-virtual {p1, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 50
    return-object v6
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .line 35
    if-ne p1, p2, :cond_4

    const/4 v0, 0x1

    goto :goto_5

    :cond_4
    const/4 v0, 0x0

    :goto_5
    return v0
.end method

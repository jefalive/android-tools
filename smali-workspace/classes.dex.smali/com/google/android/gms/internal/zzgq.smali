.class public abstract Lcom/google/android/gms/internal/zzgq;
.super Lcom/google/android/gms/internal/zzim;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/zzgq$zza;
    }
.end annotation


# instance fields
.field protected final mContext:Landroid/content/Context;

.field protected final zzGc:Lcom/google/android/gms/internal/zzgr$zza;

.field protected final zzGd:Lcom/google/android/gms/internal/zzif$zza;

.field protected zzGe:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

.field protected final zzGg:Ljava/lang/Object;

.field protected final zzpV:Ljava/lang/Object;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/zzif$zza;Lcom/google/android/gms/internal/zzgr$zza;)V
    .registers 5

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/zzim;-><init>(Z)V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzgq;->zzpV:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzgq;->zzGg:Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/gms/internal/zzgq;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzgq;->zzGd:Lcom/google/android/gms/internal/zzif$zza;

    iget-object v0, p2, Lcom/google/android/gms/internal/zzif$zza;->zzLe:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iput-object v0, p0, Lcom/google/android/gms/internal/zzgq;->zzGe:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iput-object p3, p0, Lcom/google/android/gms/internal/zzgq;->zzGc:Lcom/google/android/gms/internal/zzgr$zza;

    return-void
.end method


# virtual methods
.method public onStop()V
    .registers 1

    return-void
.end method

.method protected abstract zzD(I)Lcom/google/android/gms/internal/zzif;
.end method

.method public zzbr()V
    .registers 8

    iget-object v3, p0, Lcom/google/android/gms/internal/zzgq;->zzpV:Ljava/lang/Object;

    monitor-enter v3

    const-string v0, "AdRendererBackgroundTask started."

    :try_start_5
    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaI(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzgq;->zzGd:Lcom/google/android/gms/internal/zzif$zza;

    iget v4, v0, Lcom/google/android/gms/internal/zzif$zza;->errorCode:I
    :try_end_c
    .catchall {:try_start_5 .. :try_end_c} :catchall_5f

    :try_start_c
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/zzgq;->zzh(J)V
    :try_end_13
    .catch Lcom/google/android/gms/internal/zzgq$zza; {:try_start_c .. :try_end_13} :catch_14
    .catchall {:try_start_c .. :try_end_13} :catchall_5f

    goto :goto_4f

    :catch_14
    move-exception v5

    :try_start_15
    invoke-virtual {v5}, Lcom/google/android/gms/internal/zzgq$zza;->getErrorCode()I

    move-result v4

    const/4 v0, 0x3

    if-eq v4, v0, :cond_1f

    const/4 v0, -0x1

    if-ne v4, v0, :cond_27

    :cond_1f
    invoke-virtual {v5}, Lcom/google/android/gms/internal/zzgq$zza;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaJ(Ljava/lang/String;)V

    goto :goto_2e

    :cond_27
    invoke-virtual {v5}, Lcom/google/android/gms/internal/zzgq$zza;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaK(Ljava/lang/String;)V

    :goto_2e
    iget-object v0, p0, Lcom/google/android/gms/internal/zzgq;->zzGe:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    if-nez v0, :cond_3a

    new-instance v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    invoke-direct {v0, v4}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzgq;->zzGe:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    goto :goto_45

    :cond_3a
    new-instance v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v1, p0, Lcom/google/android/gms/internal/zzgq;->zzGe:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-wide v1, v1, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->zzBU:J

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(IJ)V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzgq;->zzGe:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    :goto_45
    sget-object v0, Lcom/google/android/gms/internal/zzir;->zzMc:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/internal/zzgq$1;

    invoke-direct {v1, p0}, Lcom/google/android/gms/internal/zzgq$1;-><init>(Lcom/google/android/gms/internal/zzgq;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_4f
    invoke-virtual {p0, v4}, Lcom/google/android/gms/internal/zzgq;->zzD(I)Lcom/google/android/gms/internal/zzif;

    move-result-object v5

    sget-object v0, Lcom/google/android/gms/internal/zzir;->zzMc:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/internal/zzgq$2;

    invoke-direct {v1, p0, v5}, Lcom/google/android/gms/internal/zzgq$2;-><init>(Lcom/google/android/gms/internal/zzgq;Lcom/google/android/gms/internal/zzif;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_5d
    .catchall {:try_start_15 .. :try_end_5d} :catchall_5f

    monitor-exit v3

    goto :goto_62

    :catchall_5f
    move-exception v6

    monitor-exit v3

    throw v6

    :goto_62
    return-void
.end method

.method protected abstract zzh(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/gms/internal/zzgq$zza;
        }
    .end annotation
.end method

.method protected zzm(Lcom/google/android/gms/internal/zzif;)V
    .registers 3

    iget-object v0, p0, Lcom/google/android/gms/internal/zzgq;->zzGc:Lcom/google/android/gms/internal/zzgr$zza;

    invoke-interface {v0, p1}, Lcom/google/android/gms/internal/zzgr$zza;->zzb(Lcom/google/android/gms/internal/zzif;)V

    return-void
.end method

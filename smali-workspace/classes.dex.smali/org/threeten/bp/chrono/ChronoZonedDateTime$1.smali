.class final Lorg/threeten/bp/chrono/ChronoZonedDateTime$1;
.super Ljava/lang/Object;
.source "ChronoZonedDateTime.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/threeten/bp/chrono/ChronoZonedDateTime;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Ljava/util/Comparator<Lorg/threeten/bp/chrono/ChronoZonedDateTime<*>;>;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 5
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .line 116
    move-object v0, p1

    check-cast v0, Lorg/threeten/bp/chrono/ChronoZonedDateTime;

    move-object v1, p2

    check-cast v1, Lorg/threeten/bp/chrono/ChronoZonedDateTime;

    invoke-virtual {p0, v0, v1}, Lorg/threeten/bp/chrono/ChronoZonedDateTime$1;->compare(Lorg/threeten/bp/chrono/ChronoZonedDateTime;Lorg/threeten/bp/chrono/ChronoZonedDateTime;)I

    move-result v0

    return v0
.end method

.method public compare(Lorg/threeten/bp/chrono/ChronoZonedDateTime;Lorg/threeten/bp/chrono/ChronoZonedDateTime;)I
    .registers 8
    .param p1, "datetime1"    # Lorg/threeten/bp/chrono/ChronoZonedDateTime;
    .param p2, "datetime2"    # Lorg/threeten/bp/chrono/ChronoZonedDateTime;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lorg/threeten/bp/chrono/ChronoZonedDateTime<*>;Lorg/threeten/bp/chrono/ChronoZonedDateTime<*>;)I"
        }
    .end annotation

    .line 119
    invoke-virtual {p1}, Lorg/threeten/bp/chrono/ChronoZonedDateTime;->toEpochSecond()J

    move-result-wide v0

    invoke-virtual {p2}, Lorg/threeten/bp/chrono/ChronoZonedDateTime;->toEpochSecond()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lorg/threeten/bp/jdk8/Jdk8Methods;->compareLongs(JJ)I

    move-result v4

    .line 120
    .local v4, "cmp":I
    if-nez v4, :cond_22

    .line 121
    invoke-virtual {p1}, Lorg/threeten/bp/chrono/ChronoZonedDateTime;->toLocalTime()Lorg/threeten/bp/LocalTime;

    move-result-object v0

    invoke-virtual {v0}, Lorg/threeten/bp/LocalTime;->toNanoOfDay()J

    move-result-wide v0

    invoke-virtual {p2}, Lorg/threeten/bp/chrono/ChronoZonedDateTime;->toLocalTime()Lorg/threeten/bp/LocalTime;

    move-result-object v2

    invoke-virtual {v2}, Lorg/threeten/bp/LocalTime;->toNanoOfDay()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lorg/threeten/bp/jdk8/Jdk8Methods;->compareLongs(JJ)I

    move-result v4

    .line 123
    :cond_22
    return v4
.end method

.class final Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;
.super Ljava/lang/Object;
.source "DateTimeFormatterBuilder.java"

# interfaces
.implements Lorg/threeten/bp/format/DateTimeFormatterBuilder$DateTimePrinterParser;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/threeten/bp/format/DateTimeFormatterBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ZoneIdPrinterParser"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;
    }
.end annotation


# static fields
.field private static volatile cachedSubstringTree:Ljava/util/Map$Entry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map$Entry<Ljava/lang/Integer;Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;>;"
        }
    .end annotation
.end field


# instance fields
.field private final description:Ljava/lang/String;

.field private final query:Lorg/threeten/bp/temporal/TemporalQuery;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/threeten/bp/temporal/TemporalQuery<Lorg/threeten/bp/ZoneId;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lorg/threeten/bp/temporal/TemporalQuery;Ljava/lang/String;)V
    .registers 3
    .param p1, "query"    # Lorg/threeten/bp/temporal/TemporalQuery;
    .param p2, "description"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lorg/threeten/bp/temporal/TemporalQuery<Lorg/threeten/bp/ZoneId;>;Ljava/lang/String;)V"
        }
    .end annotation

    .line 3419
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3420
    iput-object p1, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->query:Lorg/threeten/bp/temporal/TemporalQuery;

    .line 3421
    iput-object p2, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->description:Ljava/lang/String;

    .line 3422
    return-void
.end method

.method private convertToZone(Ljava/util/Set;Ljava/lang/String;Z)Lorg/threeten/bp/ZoneId;
    .registers 7
    .param p1, "regionIds"    # Ljava/util/Set;
    .param p2, "parsedZoneId"    # Ljava/lang/String;
    .param p3, "caseSensitive"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Set<Ljava/lang/String;>;Ljava/lang/String;Z)Lorg/threeten/bp/ZoneId;"
        }
    .end annotation

    .line 3533
    if-nez p2, :cond_4

    .line 3534
    const/4 v0, 0x0

    return-object v0

    .line 3536
    :cond_4
    if-eqz p3, :cond_13

    .line 3537
    invoke-interface {p1, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-static {p2}, Lorg/threeten/bp/ZoneId;->of(Ljava/lang/String;)Lorg/threeten/bp/ZoneId;

    move-result-object v0

    goto :goto_12

    :cond_11
    const/4 v0, 0x0

    :goto_12
    return-object v0

    .line 3539
    :cond_13
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_17
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_30

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/lang/String;

    .line 3540
    .local v2, "regionId":Ljava/lang/String;
    invoke-virtual {v2, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 3541
    invoke-static {v2}, Lorg/threeten/bp/ZoneId;->of(Ljava/lang/String;)Lorg/threeten/bp/ZoneId;

    move-result-object v0

    return-object v0

    .end local v2    # "regionId":Ljava/lang/String;
    :cond_2f
    goto :goto_17

    .line 3544
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_30
    const/4 v0, 0x0

    return-object v0
.end method

.method private parsePrefixedOffset(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;II)I
    .registers 12
    .param p1, "context"    # Lorg/threeten/bp/format/DateTimeParseContext;
    .param p2, "text"    # Ljava/lang/CharSequence;
    .param p3, "prefixPos"    # I
    .param p4, "position"    # I

    .line 3549
    invoke-interface {p2, p3, p4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 3550
    .local v2, "prefix":Ljava/lang/String;
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeParseContext;->copy()Lorg/threeten/bp/format/DateTimeParseContext;

    move-result-object v3

    .line 3551
    .local v3, "newContext":Lorg/threeten/bp/format/DateTimeParseContext;
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-ge p4, v0, :cond_2c

    invoke-interface {p2, p4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x5a

    invoke-virtual {p1, v0, v1}, Lorg/threeten/bp/format/DateTimeParseContext;->charEquals(CC)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 3552
    sget-object v0, Lorg/threeten/bp/ZoneOffset;->UTC:Lorg/threeten/bp/ZoneOffset;

    invoke-static {v2, v0}, Lorg/threeten/bp/ZoneId;->ofOffset(Ljava/lang/String;Lorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/ZoneId;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->setParsed(Lorg/threeten/bp/ZoneId;)V

    .line 3553
    return p4

    .line 3555
    :cond_2c
    sget-object v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->INSTANCE_ID:Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;

    invoke-virtual {v0, v3, p2, p4}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->parse(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;I)I

    move-result v4

    .line 3556
    .local v4, "endPos":I
    if-gez v4, :cond_3e

    .line 3557
    sget-object v0, Lorg/threeten/bp/ZoneOffset;->UTC:Lorg/threeten/bp/ZoneOffset;

    invoke-static {v2, v0}, Lorg/threeten/bp/ZoneId;->ofOffset(Ljava/lang/String;Lorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/ZoneId;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->setParsed(Lorg/threeten/bp/ZoneId;)V

    .line 3558
    return p4

    .line 3560
    :cond_3e
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->OFFSET_SECONDS:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v3, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->getParsed(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v5, v0

    .line 3561
    .local v5, "offsetSecs":I
    invoke-static {v5}, Lorg/threeten/bp/ZoneOffset;->ofTotalSeconds(I)Lorg/threeten/bp/ZoneOffset;

    move-result-object v6

    .line 3562
    .local v6, "offset":Lorg/threeten/bp/ZoneOffset;
    invoke-static {v2, v6}, Lorg/threeten/bp/ZoneId;->ofOffset(Ljava/lang/String;Lorg/threeten/bp/ZoneOffset;)Lorg/threeten/bp/ZoneId;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->setParsed(Lorg/threeten/bp/ZoneId;)V

    .line 3563
    return v4
.end method

.method private static prepareParser(Ljava/util/Set;)Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;
    .registers 7
    .param p0, "availableIDs"    # Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Set<Ljava/lang/String;>;)Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;"
        }
    .end annotation

    .line 3651
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 3652
    .local v2, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder;->LENGTH_SORT:Ljava/util/Comparator;

    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 3655
    new-instance v3, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {v3, v0, v1}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;-><init>(ILorg/threeten/bp/format/DateTimeFormatterBuilder$1;)V

    .line 3656
    .local v3, "tree":Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1f
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_30

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/lang/String;

    .line 3657
    .local v5, "id":Ljava/lang/String;
    # invokes: Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;->add(Ljava/lang/String;)V
    invoke-static {v3, v5}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;->access$300(Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;Ljava/lang/String;)V

    .end local v5    # "id":Ljava/lang/String;
    goto :goto_1f

    .line 3659
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_30
    return-object v3
.end method


# virtual methods
.method public parse(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;I)I
    .registers 15
    .param p1, "context"    # Lorg/threeten/bp/format/DateTimeParseContext;
    .param p2, "text"    # Ljava/lang/CharSequence;
    .param p3, "position"    # I

    .line 3453
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    .line 3454
    .local v2, "length":I
    if-le p3, v2, :cond_c

    .line 3455
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 3457
    :cond_c
    if-ne p3, v2, :cond_11

    .line 3458
    xor-int/lit8 v0, p3, -0x1

    return v0

    .line 3462
    :cond_11
    invoke-interface {p2, p3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    .line 3463
    .local v3, "nextChar":C
    const/16 v0, 0x2b

    if-eq v3, v0, :cond_1d

    const/16 v0, 0x2d

    if-ne v3, v0, :cond_3d

    .line 3464
    :cond_1d
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeParseContext;->copy()Lorg/threeten/bp/format/DateTimeParseContext;

    move-result-object v4

    .line 3465
    .local v4, "newContext":Lorg/threeten/bp/format/DateTimeParseContext;
    sget-object v0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->INSTANCE_ID:Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;

    invoke-virtual {v0, v4, p2, p3}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$OffsetIdPrinterParser;->parse(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;I)I

    move-result v5

    .line 3466
    .local v5, "endPos":I
    if-gez v5, :cond_2a

    .line 3467
    return v5

    .line 3469
    :cond_2a
    sget-object v0, Lorg/threeten/bp/temporal/ChronoField;->OFFSET_SECONDS:Lorg/threeten/bp/temporal/ChronoField;

    invoke-virtual {v4, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->getParsed(Lorg/threeten/bp/temporal/TemporalField;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v6, v0

    .line 3470
    .local v6, "offset":I
    invoke-static {v6}, Lorg/threeten/bp/ZoneOffset;->ofTotalSeconds(I)Lorg/threeten/bp/ZoneOffset;

    move-result-object v7

    .line 3471
    .local v7, "zone":Lorg/threeten/bp/ZoneId;
    invoke-virtual {p1, v7}, Lorg/threeten/bp/format/DateTimeParseContext;->setParsed(Lorg/threeten/bp/ZoneId;)V

    .line 3472
    return v5

    .line 3473
    .end local v4    # "newContext":Lorg/threeten/bp/format/DateTimeParseContext;
    .end local v5    # "endPos":I
    .end local v6    # "offset":I
    .end local v7    # "zone":Lorg/threeten/bp/ZoneId;
    :cond_3d
    add-int/lit8 v0, p3, 0x2

    if-lt v2, v0, :cond_a0

    .line 3474
    add-int/lit8 v0, p3, 0x1

    invoke-interface {p2, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    .line 3475
    .local v4, "nextNextChar":C
    const/16 v0, 0x55

    invoke-virtual {p1, v3, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->charEquals(CC)Z

    move-result v0

    if-eqz v0, :cond_77

    const/16 v0, 0x54

    invoke-virtual {p1, v4, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->charEquals(CC)Z

    move-result v0

    if-eqz v0, :cond_77

    .line 3477
    add-int/lit8 v0, p3, 0x3

    if-lt v2, v0, :cond_70

    add-int/lit8 v0, p3, 0x2

    invoke-interface {p2, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x43

    invoke-virtual {p1, v0, v1}, Lorg/threeten/bp/format/DateTimeParseContext;->charEquals(CC)Z

    move-result v0

    if-eqz v0, :cond_70

    .line 3479
    add-int/lit8 v0, p3, 0x3

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->parsePrefixedOffset(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;II)I

    move-result v0

    return v0

    .line 3481
    :cond_70
    add-int/lit8 v0, p3, 0x2

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->parsePrefixedOffset(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;II)I

    move-result v0

    return v0

    .line 3482
    :cond_77
    const/16 v0, 0x47

    invoke-virtual {p1, v3, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->charEquals(CC)Z

    move-result v0

    if-eqz v0, :cond_a0

    add-int/lit8 v0, p3, 0x3

    if-lt v2, v0, :cond_a0

    const/16 v0, 0x4d

    invoke-virtual {p1, v4, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->charEquals(CC)Z

    move-result v0

    if-eqz v0, :cond_a0

    add-int/lit8 v0, p3, 0x2

    invoke-interface {p2, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x54

    invoke-virtual {p1, v0, v1}, Lorg/threeten/bp/format/DateTimeParseContext;->charEquals(CC)Z

    move-result v0

    if-eqz v0, :cond_a0

    .line 3486
    add-int/lit8 v0, p3, 0x3

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->parsePrefixedOffset(Lorg/threeten/bp/format/DateTimeParseContext;Ljava/lang/CharSequence;II)I

    move-result v0

    return v0

    .line 3491
    .end local v4    # "nextNextChar":C
    :cond_a0
    invoke-static {}, Lorg/threeten/bp/zone/ZoneRulesProvider;->getAvailableZoneIds()Ljava/util/Set;

    move-result-object v4

    .line 3492
    .local v4, "regionIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v5

    .line 3493
    .local v5, "regionIdsSize":I
    sget-object v6, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->cachedSubstringTree:Ljava/util/Map$Entry;

    .line 3494
    .local v6, "cached":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;>;"
    if-eqz v6, :cond_b8

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v5, :cond_de

    .line 3495
    :cond_b8
    move-object v7, p0

    monitor-enter v7

    .line 3496
    :try_start_ba
    sget-object v6, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->cachedSubstringTree:Ljava/util/Map$Entry;

    .line 3497
    if-eqz v6, :cond_ca

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v5, :cond_d9

    .line 3498
    :cond_ca
    new-instance v6, Ljava/util/AbstractMap$SimpleImmutableEntry;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v4}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->prepareParser(Ljava/util/Set;)Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;

    move-result-object v1

    invoke-direct {v6, v0, v1}, Ljava/util/AbstractMap$SimpleImmutableEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    sput-object v6, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->cachedSubstringTree:Ljava/util/Map$Entry;
    :try_end_d9
    .catchall {:try_start_ba .. :try_end_d9} :catchall_db

    .line 3500
    :cond_d9
    monitor-exit v7

    goto :goto_de

    :catchall_db
    move-exception v8

    monitor-exit v7

    throw v8

    .line 3502
    :cond_de
    :goto_de
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;

    .line 3505
    .local v7, "tree":Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;
    const/4 v8, 0x0

    .line 3506
    .local v8, "parsedZoneId":Ljava/lang/String;
    const/4 v9, 0x0

    .line 3507
    .local v9, "lastZoneId":Ljava/lang/String;
    :goto_e7
    if-eqz v7, :cond_104

    .line 3508
    iget v10, v7, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;->length:I

    .line 3509
    .local v10, "nodeLength":I
    add-int v0, p3, v10

    if-le v0, v2, :cond_f0

    .line 3510
    goto :goto_104

    .line 3512
    :cond_f0
    move-object v9, v8

    .line 3513
    add-int v0, p3, v10

    invoke-interface {p2, p3, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    .line 3514
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeParseContext;->isCaseSensitive()Z

    move-result v0

    # invokes: Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;->get(Ljava/lang/CharSequence;Z)Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;
    invoke-static {v7, v8, v0}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;->access$100(Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;Ljava/lang/CharSequence;Z)Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser$SubstringTree;

    move-result-object v7

    .line 3515
    .end local v10    # "nodeLength":I
    goto :goto_e7

    .line 3516
    :cond_104
    :goto_104
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeParseContext;->isCaseSensitive()Z

    move-result v0

    invoke-direct {p0, v4, v8, v0}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->convertToZone(Ljava/util/Set;Ljava/lang/String;Z)Lorg/threeten/bp/ZoneId;

    move-result-object v10

    .line 3517
    .local v10, "zone":Lorg/threeten/bp/ZoneId;
    if-nez v10, :cond_12c

    .line 3518
    invoke-virtual {p1}, Lorg/threeten/bp/format/DateTimeParseContext;->isCaseSensitive()Z

    move-result v0

    invoke-direct {p0, v4, v9, v0}, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->convertToZone(Ljava/util/Set;Ljava/lang/String;Z)Lorg/threeten/bp/ZoneId;

    move-result-object v10

    .line 3519
    if-nez v10, :cond_12b

    .line 3520
    const/16 v0, 0x5a

    invoke-virtual {p1, v3, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->charEquals(CC)Z

    move-result v0

    if-eqz v0, :cond_128

    .line 3521
    sget-object v0, Lorg/threeten/bp/ZoneOffset;->UTC:Lorg/threeten/bp/ZoneOffset;

    invoke-virtual {p1, v0}, Lorg/threeten/bp/format/DateTimeParseContext;->setParsed(Lorg/threeten/bp/ZoneId;)V

    .line 3522
    add-int/lit8 v0, p3, 0x1

    return v0

    .line 3524
    :cond_128
    xor-int/lit8 v0, p3, -0x1

    return v0

    .line 3526
    :cond_12b
    move-object v8, v9

    .line 3528
    :cond_12c
    invoke-virtual {p1, v10}, Lorg/threeten/bp/format/DateTimeParseContext;->setParsed(Lorg/threeten/bp/ZoneId;)V

    .line 3529
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, p3

    return v0
.end method

.method public print(Lorg/threeten/bp/format/DateTimePrintContext;Ljava/lang/StringBuilder;)Z
    .registers 5
    .param p1, "context"    # Lorg/threeten/bp/format/DateTimePrintContext;
    .param p2, "buf"    # Ljava/lang/StringBuilder;

    .line 3427
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->query:Lorg/threeten/bp/temporal/TemporalQuery;

    invoke-virtual {p1, v0}, Lorg/threeten/bp/format/DateTimePrintContext;->getValue(Lorg/threeten/bp/temporal/TemporalQuery;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lorg/threeten/bp/ZoneId;

    .line 3428
    .local v1, "zone":Lorg/threeten/bp/ZoneId;
    if-nez v1, :cond_d

    .line 3429
    const/4 v0, 0x0

    return v0

    .line 3431
    :cond_d
    invoke-virtual {v1}, Lorg/threeten/bp/ZoneId;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3432
    const/4 v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .line 3665
    iget-object v0, p0, Lorg/threeten/bp/format/DateTimeFormatterBuilder$ZoneIdPrinterParser;->description:Ljava/lang/String;

    return-object v0
.end method

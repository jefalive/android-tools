.class public final Lcom/itau/empresas/ui/dialog/RedesignDialog_;
.super Lcom/itau/empresas/ui/dialog/RedesignDialog;
.source "RedesignDialog_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/dialog/RedesignDialog_$FragmentBuilder_;
    }
.end annotation


# instance fields
.field private contentView_:Landroid/view/View;

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 23
    invoke-direct {p0}, Lcom/itau/empresas/ui/dialog/RedesignDialog;-><init>()V

    .line 27
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method public static builder()Lcom/itau/empresas/ui/dialog/RedesignDialog_$FragmentBuilder_;
    .registers 1

    .line 75
    new-instance v0, Lcom/itau/empresas/ui/dialog/RedesignDialog_$FragmentBuilder_;

    invoke-direct {v0}, Lcom/itau/empresas/ui/dialog/RedesignDialog_$FragmentBuilder_;-><init>()V

    return-object v0
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 64
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 65
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog_;->application:Lcom/itau/empresas/CustomApplication;

    .line 66
    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .registers 3
    .param p1, "id"    # I

    .line 40
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_6

    .line 41
    const/4 v0, 0x0

    return-object v0

    .line 43
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog_;->contentView_:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 32
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 33
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/ui/dialog/RedesignDialog_;->init_(Landroid/os/Bundle;)V

    .line 34
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/dialog/RedesignDialog;->onCreate(Landroid/os/Bundle;)V

    .line 35
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 36
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 48
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/dialog/RedesignDialog;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog_;->contentView_:Landroid/view/View;

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_14

    .line 50
    const v0, 0x7f03008a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog_;->contentView_:Landroid/view/View;

    .line 52
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog_;->contentView_:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .registers 2

    .line 57
    invoke-super {p0}, Lcom/itau/empresas/ui/dialog/RedesignDialog;->onDestroyView()V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog_;->contentView_:Landroid/view/View;

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog_;->textoTitulo:Landroid/widget/TextView;

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog_;->textoDescricao:Landroid/widget/TextView;

    .line 61
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 6
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 80
    const v0, 0x7f0e00c9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog_;->textoTitulo:Landroid/widget/TextView;

    .line 81
    const v0, 0x7f0e03a5

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog_;->textoDescricao:Landroid/widget/TextView;

    .line 82
    const v0, 0x7f0e021e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 83
    .local v1, "view_botao_fechar":Landroid/view/View;
    const v0, 0x7f0e03a6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 84
    .local v2, "view_botao_dialog_cancel":Landroid/view/View;
    const v0, 0x7f0e03a7

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 86
    .local v3, "view_botao_dialog_ok":Landroid/view/View;
    if-eqz v1, :cond_35

    .line 87
    new-instance v0, Lcom/itau/empresas/ui/dialog/RedesignDialog_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/dialog/RedesignDialog_$1;-><init>(Lcom/itau/empresas/ui/dialog/RedesignDialog_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    :cond_35
    if-eqz v2, :cond_3f

    .line 97
    new-instance v0, Lcom/itau/empresas/ui/dialog/RedesignDialog_$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/dialog/RedesignDialog_$2;-><init>(Lcom/itau/empresas/ui/dialog/RedesignDialog_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    :cond_3f
    if-eqz v3, :cond_49

    .line 107
    new-instance v0, Lcom/itau/empresas/ui/dialog/RedesignDialog_$3;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/dialog/RedesignDialog_$3;-><init>(Lcom/itau/empresas/ui/dialog/RedesignDialog_;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    :cond_49
    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/RedesignDialog_;->afterViews()V

    .line 117
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 70
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/ui/dialog/RedesignDialog;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 72
    return-void
.end method

.class Lcom/facebook/appevents/internal/AutomaticAnalyticsLogger;
.super Ljava/lang/Object;
.source "AutomaticAnalyticsLogger.java"


# direct methods
.method constructor <init>()V
    .registers 1

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static logActivityTimeSpentEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;J)V
    .registers 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "activityName"    # Ljava/lang/String;
    .param p3, "timeSpentInSeconds"    # J

    .line 37
    invoke-static {p0}, Lcom/facebook/appevents/AppEventsLogger;->newLogger(Landroid/content/Context;)Lcom/facebook/appevents/AppEventsLogger;

    move-result-object v3

    .line 38
    .local v3, "l":Lcom/facebook/appevents/AppEventsLogger;
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/facebook/internal/Utility;->queryAppSettings(Ljava/lang/String;Z)Lcom/facebook/internal/Utility$FetchedAppSettings;

    move-result-object v4

    .line 39
    .local v4, "settings":Lcom/facebook/internal/Utility$FetchedAppSettings;
    invoke-virtual {v4}, Lcom/facebook/internal/Utility$FetchedAppSettings;->getAutomaticLoggingEnabled()Z

    move-result v0

    if-eqz v0, :cond_26

    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-lez v0, :cond_26

    .line 40
    new-instance v5, Landroid/os/Bundle;

    const/4 v0, 0x1

    invoke-direct {v5, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 41
    .local v5, "params":Landroid/os/Bundle;
    const-string v0, "fb_aa_time_spent_view_name"

    invoke-virtual {v5, v0, p2}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 42
    const-string v0, "fb_aa_time_spent_on_view"

    long-to-double v1, p3

    invoke-virtual {v3, v0, v1, v2, v5}, Lcom/facebook/appevents/AppEventsLogger;->logEvent(Ljava/lang/String;DLandroid/os/Bundle;)V

    .line 44
    .end local v5    # "params":Landroid/os/Bundle;
    :cond_26
    return-void
.end method

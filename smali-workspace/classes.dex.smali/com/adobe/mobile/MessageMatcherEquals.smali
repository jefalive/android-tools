.class Lcom/adobe/mobile/MessageMatcherEquals;
.super Lcom/adobe/mobile/MessageMatcher;
.source "MessageMatcherEquals.java"


# direct methods
.method constructor <init>()V
    .registers 1

    .line 21
    invoke-direct {p0}, Lcom/adobe/mobile/MessageMatcher;-><init>()V

    return-void
.end method


# virtual methods
.method protected matches(Ljava/lang/Object;)Z
    .registers 9
    .param p1, "value"    # Ljava/lang/Object;

    .line 24
    iget-object v0, p0, Lcom/adobe/mobile/MessageMatcherEquals;->values:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_82

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 26
    .local v5, "potentialMatch":Ljava/lang/Object;
    instance-of v0, v5, Ljava/lang/String;

    if-eqz v0, :cond_28

    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_28

    .line 27
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_80

    .line 28
    const/4 v0, 0x1

    return v0

    .line 33
    :cond_28
    instance-of v0, v5, Ljava/lang/Number;

    if-eqz v0, :cond_44

    instance-of v0, p1, Ljava/lang/Number;

    if-eqz v0, :cond_44

    .line 34
    move-object v0, v5

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    move-object v2, p1

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_80

    .line 35
    const/4 v0, 0x1

    return v0

    .line 40
    :cond_44
    instance-of v0, v5, Ljava/lang/Number;

    if-eqz v0, :cond_68

    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_68

    .line 41
    invoke-virtual {p0, p1}, Lcom/adobe/mobile/MessageMatcherEquals;->tryParseDouble(Ljava/lang/Object;)Ljava/lang/Double;

    move-result-object v6

    .line 42
    .local v6, "valueAsDouble":Ljava/lang/Double;
    if-eqz v6, :cond_67

    move-object v0, v5

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p0, p1}, Lcom/adobe/mobile/MessageMatcherEquals;->tryParseDouble(Ljava/lang/Object;)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_67

    .line 43
    const/4 v0, 0x1

    return v0

    .line 45
    .end local v6    # "valueAsDouble":Ljava/lang/Double;
    :cond_67
    goto :goto_80

    .line 48
    :cond_68
    instance-of v0, v5, Ljava/lang/String;

    if-eqz v0, :cond_80

    instance-of v0, p1, Ljava/lang/Number;

    if-eqz v0, :cond_80

    .line 49
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_80

    .line 50
    const/4 v0, 0x1

    return v0

    .line 53
    .end local v5    # "potentialMatch":Ljava/lang/Object;
    :cond_80
    :goto_80
    goto/16 :goto_6

    .line 55
    :cond_82
    const/4 v0, 0x0

    return v0
.end method

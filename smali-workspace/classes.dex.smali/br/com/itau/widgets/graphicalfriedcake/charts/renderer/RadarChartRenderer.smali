.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;
.super Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LineScatterCandleRadarRenderer;
.source "RadarChartRenderer.java"


# instance fields
.field protected mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

.field protected mWebPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;)V
    .registers 8
    .param p1, "chart"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;
    .param p2, "animator"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;
    .param p3, "viewPortHandler"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 34
    invoke-direct {p0, p2, p3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/LineScatterCandleRadarRenderer;-><init>(Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/ChartAnimator;Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;)V

    .line 35
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    .line 38
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mHighlightPaint:Landroid/graphics/Paint;

    .line 39
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mHighlightPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 40
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mHighlightPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 41
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mHighlightPaint:Landroid/graphics/Paint;

    const/16 v1, 0xff

    const/16 v2, 0xbb

    const/16 v3, 0x73

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 44
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mWebPaint:Landroid/graphics/Paint;

    .line 45
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mWebPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 46
    return-void
.end method


# virtual methods
.method public drawData(Landroid/graphics/Canvas;)V
    .registers 6
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 64
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getData()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;

    .line 67
    .local v1, "radarData":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;
    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;->getDataSets()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_11
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;

    .line 70
    .local v3, "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;
    invoke-virtual {v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_27

    .line 71
    invoke-virtual {p0, p1, v3}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->drawDataSet(Landroid/graphics/Canvas;Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;)V

    .line 72
    .end local v3    # "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;
    :cond_27
    goto :goto_11

    .line 73
    :cond_28
    return-void
.end method

.method protected drawDataSet(Landroid/graphics/Canvas;Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;)V
    .registers 15
    .param p1, "c"    # Landroid/graphics/Canvas;
    .param p2, "dataSet"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;

    .line 79
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getSliceAngle()F

    move-result v3

    .line 84
    .local v3, "sliceangle":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getFactor()F

    move-result v4

    .line 87
    .local v4, "factor":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getCenterOffsets()Landroid/graphics/PointF;

    move-result-object v5

    .line 90
    .local v5, "center":Landroid/graphics/PointF;
    invoke-virtual {p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->getYVals()Ljava/util/List;

    move-result-object v6

    .line 93
    .local v6, "entries":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
    new-instance v7, Landroid/graphics/Path;

    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    .line 96
    .local v7, "surface":Landroid/graphics/Path;
    const/4 v8, 0x0

    .line 99
    .local v8, "hasMovedToPoint":Z
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_1d
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v9, v0, :cond_6b

    .line 102
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual {p2, v9}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 105
    invoke-interface {v6, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    .line 108
    .local v10, "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    invoke-virtual {v10}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getYChartMin()F

    move-result v1

    sub-float/2addr v0, v1

    mul-float/2addr v0, v4

    int-to-float v1, v9

    mul-float/2addr v1, v3

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    .line 109
    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getRotationAngle()F

    move-result v2

    add-float/2addr v1, v2

    .line 108
    invoke-static {v5, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getPosition(Landroid/graphics/PointF;FF)Landroid/graphics/PointF;

    move-result-object v11

    .line 112
    .local v11, "p":Landroid/graphics/PointF;
    iget v0, v11, Landroid/graphics/PointF;->x:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_55

    .line 113
    goto :goto_67

    .line 116
    :cond_55
    if-nez v8, :cond_60

    .line 117
    iget v0, v11, Landroid/graphics/PointF;->x:F

    iget v1, v11, Landroid/graphics/PointF;->y:F

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 118
    const/4 v8, 0x1

    goto :goto_67

    .line 120
    :cond_60
    iget v0, v11, Landroid/graphics/PointF;->x:F

    iget v1, v11, Landroid/graphics/PointF;->y:F

    invoke-virtual {v7, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 99
    .end local v10    # "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    .end local v11    # "p":Landroid/graphics/PointF;
    :goto_67
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_1d

    .line 124
    .end local v9    # "j":I
    :cond_6b
    invoke-virtual {v7}, Landroid/graphics/Path;->close()V

    .line 128
    invoke-virtual {p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->isDrawFilledEnabled()Z

    move-result v0

    if-eqz v0, :cond_90

    .line 129
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 130
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual {p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->getFillAlpha()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 131
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 132
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 136
    :cond_90
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual {p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->getLineWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 137
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 141
    invoke-virtual {p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->isDrawFilledEnabled()Z

    move-result v0

    if-eqz v0, :cond_ae

    invoke-virtual {p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->getFillAlpha()I

    move-result v0

    const/16 v1, 0xff

    if-ge v0, v1, :cond_b3

    .line 142
    :cond_ae
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mRenderPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 143
    :cond_b3
    return-void
.end method

.method public drawExtras(Landroid/graphics/Canvas;)V
    .registers 2
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 200
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->drawWeb(Landroid/graphics/Canvas;)V

    .line 201
    return-void
.end method

.method public drawHighlighted(Landroid/graphics/Canvas;[Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)V
    .registers 17
    .param p1, "c"    # Landroid/graphics/Canvas;
    .param p2, "indices"    # [Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;

    .line 268
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getSliceAngle()F

    move-result v3

    .line 269
    .local v3, "sliceangle":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getFactor()F

    move-result v4

    .line 272
    .local v4, "factor":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getCenterOffsets()Landroid/graphics/PointF;

    move-result-object v5

    .line 275
    .local v5, "center":Landroid/graphics/PointF;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_13
    move-object/from16 v0, p2

    array-length v0, v0

    if-ge v6, v0, :cond_c4

    .line 278
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getData()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;

    aget-object v1, p2, v6

    .line 280
    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->getDataSetIndex()I

    move-result v1

    .line 279
    invoke-virtual {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;->getDataSetByIndex(I)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;

    .line 283
    .local v7, "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;
    if-eqz v7, :cond_c0

    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->isHighlightEnabled()Z

    move-result v0

    if-nez v0, :cond_37

    .line 284
    goto/16 :goto_c0

    .line 287
    :cond_37
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mHighlightPaint:Landroid/graphics/Paint;

    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->getHighLightColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 288
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mHighlightPaint:Landroid/graphics/Paint;

    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->getHighlightLineWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 292
    aget-object v0, p2, v6

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;->getXIndex()I

    move-result v8

    .line 295
    .local v8, "xIndex":I
    invoke-virtual {v7, v8}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->getEntryForXIndex(I)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    move-result-object v9

    .line 296
    .local v9, "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    if-eqz v9, :cond_c0

    invoke-virtual {v9}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getXIndex()I

    move-result v0

    if-eq v0, v8, :cond_5d

    .line 297
    goto/16 :goto_c0

    .line 300
    :cond_5d
    invoke-virtual {v7, v9}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->getEntryPosition(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;)I

    move-result v10

    .line 301
    .local v10, "j":I
    invoke-virtual {v9}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getYChartMin()F

    move-result v1

    sub-float v11, v0, v1

    .line 304
    .local v11, "y":F
    invoke-static {v11}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_74

    .line 305
    goto :goto_c0

    .line 308
    :cond_74
    mul-float v0, v11, v4

    int-to-float v1, v10

    mul-float/2addr v1, v3

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    .line 309
    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getRotationAngle()F

    move-result v2

    add-float/2addr v1, v2

    .line 308
    invoke-static {v5, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getPosition(Landroid/graphics/PointF;FF)Landroid/graphics/PointF;

    move-result-object v12

    .line 312
    .local v12, "p":Landroid/graphics/PointF;
    const/16 v0, 0x8

    new-array v13, v0, [F

    iget v0, v12, Landroid/graphics/PointF;->x:F

    const/4 v1, 0x0

    aput v0, v13, v1

    const/4 v0, 0x0

    const/4 v1, 0x1

    aput v0, v13, v1

    iget v0, v12, Landroid/graphics/PointF;->x:F

    const/4 v1, 0x2

    aput v0, v13, v1

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 313
    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getChartHeight()F

    move-result v0

    const/4 v1, 0x3

    aput v0, v13, v1

    const/4 v0, 0x0

    const/4 v1, 0x4

    aput v0, v13, v1

    iget v0, v12, Landroid/graphics/PointF;->y:F

    const/4 v1, 0x5

    aput v0, v13, v1

    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mViewPortHandler:Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;

    .line 314
    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ViewPortHandler;->getChartWidth()F

    move-result v0

    const/4 v1, 0x6

    aput v0, v13, v1

    iget v0, v12, Landroid/graphics/PointF;->y:F

    const/4 v1, 0x7

    aput v0, v13, v1

    .line 319
    .local v13, "pts":[F
    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->isHorizontalHighlightIndicatorEnabled()Z

    move-result v0

    invoke-virtual {v7}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->isVerticalHighlightIndicatorEnabled()Z

    move-result v1

    invoke-virtual {p0, p1, v13, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->drawHighlightLines(Landroid/graphics/Canvas;[FZZ)V

    .line 275
    .end local v7    # "set":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;
    .end local v8    # "xIndex":I
    .end local v9    # "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    .end local v10    # "j":I
    .end local v11    # "y":F
    .end local v12    # "p":Landroid/graphics/PointF;
    .end local v13    # "pts":[F
    :cond_c0
    :goto_c0
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_13

    .line 321
    .end local v6    # "i":I
    :cond_c4
    return-void
.end method

.method public drawValues(Landroid/graphics/Canvas;)V
    .registers 17
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 150
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getSliceAngle()F

    move-result v5

    .line 155
    .local v5, "sliceangle":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getFactor()F

    move-result v6

    .line 158
    .local v6, "factor":F
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getCenterOffsets()Landroid/graphics/PointF;

    move-result-object v7

    .line 161
    .local v7, "center":Landroid/graphics/PointF;
    const/high16 v0, 0x40a00000    # 5.0f

    invoke-static {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v8

    .line 164
    .local v8, "yoffset":F
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_19
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getData()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;->getDataSetCount()I

    move-result v0

    if-ge v9, v0, :cond_8a

    .line 167
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getData()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;

    invoke-virtual {v0, v9}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;->getDataSetByIndex(I)Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;

    .line 170
    .local v10, "dataSet":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;
    invoke-virtual {v10}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->isDrawValuesEnabled()Z

    move-result v0

    if-nez v0, :cond_3d

    .line 171
    goto :goto_86

    .line 175
    :cond_3d
    invoke-virtual {p0, v10}, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->applyValueTextStyle(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;)V

    .line 178
    invoke-virtual {v10}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->getYVals()Ljava/util/List;

    move-result-object v11

    .line 181
    .local v11, "entries":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_45
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v0

    if-ge v12, v0, :cond_86

    .line 184
    invoke-interface {v11, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;

    .line 187
    .local v13, "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    invoke-virtual {v13}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v0

    iget-object v1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getYChartMin()F

    move-result v1

    sub-float/2addr v0, v1

    mul-float/2addr v0, v6

    int-to-float v1, v12

    mul-float/2addr v1, v5

    iget-object v2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    .line 188
    invoke-virtual {v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getRotationAngle()F

    move-result v2

    add-float/2addr v1, v2

    .line 187
    invoke-static {v7, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getPosition(Landroid/graphics/PointF;FF)Landroid/graphics/PointF;

    move-result-object v14

    .line 191
    .local v14, "p":Landroid/graphics/PointF;
    invoke-virtual {v10}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;->getValueFormatter()Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;

    move-result-object v0

    invoke-virtual {v13}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;->getVal()F

    move-result v1

    invoke-interface {v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/ValueFormatter;->getFormattedValue(F)Ljava/lang/String;

    move-result-object v0

    iget v1, v14, Landroid/graphics/PointF;->x:F

    iget v2, v14, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v8

    iget-object v3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mValuePaint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 181
    .end local v13    # "e":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;
    .end local v14    # "p":Landroid/graphics/PointF;
    add-int/lit8 v12, v12, 0x1

    goto :goto_45

    .line 164
    .end local v10    # "dataSet":Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarDataSet;
    .end local v11    # "entries":Ljava/util/List;, "Ljava/util/List<Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;>;"
    .end local v11
    .end local v12    # "j":I
    :cond_86
    :goto_86
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_19

    .line 195
    .end local v9    # "i":I
    :cond_8a
    return-void
.end method

.method protected drawWeb(Landroid/graphics/Canvas;)V
    .registers 18
    .param p1, "c"    # Landroid/graphics/Canvas;

    .line 207
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getSliceAngle()F

    move-result v6

    .line 212
    .local v6, "sliceangle":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getFactor()F

    move-result v7

    .line 213
    .local v7, "factor":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getRotationAngle()F

    move-result v8

    .line 216
    .local v8, "rotationangle":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getCenterOffsets()Landroid/graphics/PointF;

    move-result-object v9

    .line 220
    .local v9, "center":Landroid/graphics/PointF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mWebPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getWebLineWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 221
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mWebPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getWebColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 222
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mWebPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getWebAlpha()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 225
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_4e
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getData()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;->getXValCount()I

    move-result v0

    if-ge v10, v0, :cond_82

    .line 228
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getYRange()F

    move-result v0

    mul-float/2addr v0, v7

    int-to-float v1, v10

    mul-float/2addr v1, v6

    add-float/2addr v1, v8

    invoke-static {v9, v0, v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getPosition(Landroid/graphics/PointF;FF)Landroid/graphics/PointF;

    move-result-object v11

    .line 232
    .local v11, "p":Landroid/graphics/PointF;
    move-object/from16 v0, p1

    iget v1, v9, Landroid/graphics/PointF;->x:F

    iget v2, v9, Landroid/graphics/PointF;->y:F

    iget v3, v11, Landroid/graphics/PointF;->x:F

    iget v4, v11, Landroid/graphics/PointF;->y:F

    move-object/from16 v5, p0

    iget-object v5, v5, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mWebPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 225
    .end local v11    # "p":Landroid/graphics/PointF;
    add-int/lit8 v10, v10, 0x1

    goto :goto_4e

    .line 237
    .end local v10    # "i":I
    :cond_82
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mWebPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getWebLineWidthInner()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 238
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mWebPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getWebColorInner()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 239
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mWebPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getWebAlpha()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 242
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getYAxis()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    move-result-object v0

    iget v10, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntryCount:I

    .line 245
    .local v10, "labelCount":I
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_ba
    if-ge v11, v10, :cond_10b

    .line 248
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_bd
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getData()Lbr/com/itau/widgets/graphicalfriedcake/charts/data/ChartData;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/data/RadarData;->getXValCount()I

    move-result v0

    if-ge v12, v0, :cond_108

    .line 251
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getYAxis()Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;

    move-result-object v0

    iget-object v0, v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/YAxis;->mEntries:[F

    aget v0, v0, v11

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mChart:Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;

    invoke-virtual {v1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/RadarChart;->getYChartMin()F

    move-result v1

    sub-float/2addr v0, v1

    mul-float v13, v0, v7

    .line 254
    .local v13, "r":F
    int-to-float v0, v12

    mul-float/2addr v0, v6

    add-float/2addr v0, v8

    invoke-static {v9, v13, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getPosition(Landroid/graphics/PointF;FF)Landroid/graphics/PointF;

    move-result-object v14

    .line 255
    .local v14, "p1":Landroid/graphics/PointF;
    add-int/lit8 v0, v12, 0x1

    int-to-float v0, v0

    mul-float/2addr v0, v6

    add-float/2addr v0, v8

    invoke-static {v9, v13, v0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->getPosition(Landroid/graphics/PointF;FF)Landroid/graphics/PointF;

    move-result-object v15

    .line 258
    .local v15, "p2":Landroid/graphics/PointF;
    move-object/from16 v0, p1

    iget v1, v14, Landroid/graphics/PointF;->x:F

    iget v2, v14, Landroid/graphics/PointF;->y:F

    iget v3, v15, Landroid/graphics/PointF;->x:F

    iget v4, v15, Landroid/graphics/PointF;->y:F

    move-object/from16 v5, p0

    iget-object v5, v5, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mWebPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 248
    .end local v13    # "r":F
    .end local v14    # "p1":Landroid/graphics/PointF;
    .end local v15    # "p2":Landroid/graphics/PointF;
    add-int/lit8 v12, v12, 0x1

    goto :goto_bd

    .line 245
    .end local v12    # "i":I
    :cond_108
    add-int/lit8 v11, v11, 0x1

    goto :goto_ba

    .line 261
    .end local v11    # "j":I
    :cond_10b
    return-void
.end method

.method public getWebPaint()Landroid/graphics/Paint;
    .registers 2

    .line 50
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/renderer/RadarChartRenderer;->mWebPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public initBuffers()V
    .registers 1

    .line 58
    return-void
.end method

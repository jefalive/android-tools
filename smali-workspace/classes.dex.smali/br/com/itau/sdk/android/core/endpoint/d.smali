.class public Lbr/com/itau/sdk/android/core/endpoint/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[B

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0xd

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/endpoint/d;->a:[B

    const/16 v0, 0xb4

    sput v0, Lbr/com/itau/sdk/android/core/endpoint/d;->b:I

    return-void

    :array_e
    .array-data 1
        0x44t
        -0x26t
        -0x39t
        -0x6et
        -0x26t
        -0xat
        -0xdt
        -0x5t
        0x30t
        -0x2ft
        -0x1dt
        -0xct
        0x0t
    .end array-data
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    mul-int/lit8 p2, p2, 0x2

    add-int/lit8 p2, p2, 0xa

    new-instance v0, Ljava/lang/String;

    mul-int/lit8 p1, p1, 0x2

    rsub-int/lit8 p1, p1, 0x43

    sget-object v5, Lbr/com/itau/sdk/android/core/endpoint/d;->a:[B

    add-int/lit8 p0, p0, 0x4

    const/4 v4, 0x0

    new-array v1, p2, [B

    add-int/lit8 p2, p2, -0x1

    if-nez v5, :cond_1b

    move v2, p2

    move v3, p0

    :goto_17
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p1, v2, -0x8

    :cond_1b
    int-to-byte v2, p1

    aput-byte v2, v1, v4

    add-int/lit8 p0, p0, 0x1

    if-ne v4, p2, :cond_27

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_27
    move v2, p1

    add-int/lit8 v4, v4, 0x1

    aget-byte v3, v5, p0

    goto :goto_17
.end method

.method public static a(Lokhttp3/OkHttpClient;Lbr/com/itau/sdk/android/core/endpoint/p;Lbr/com/itau/sdk/android/core/endpoint/k;)Lokhttp3/Call;
    .registers 9

    .line 29
    iget-boolean v0, p2, Lbr/com/itau/sdk/android/core/endpoint/k;->k:Z

    if-eqz v0, :cond_6a

    .line 30
    sget-object v0, Lbr/com/itau/sdk/android/core/n;->a:Lcom/google/gson/Gson;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/endpoint/p;->b()Lbr/com/itau/sdk/android/core/model/RequestDTO;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 32
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getFlowManager()Lbr/com/itau/sdk/android/core/FlowManager;

    move-result-object v1

    iget-object v2, p2, Lbr/com/itau/sdk/android/core/endpoint/k;->p:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lbr/com/itau/sdk/android/core/FlowManager;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 31
    invoke-static {v0}, Lbr/com/itau/sdk/android/core/endpoint/a/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 34
    iget-object v0, p2, Lbr/com/itau/sdk/android/core/endpoint/k;->a:Ljava/lang/reflect/Method;

    const-class v1, Lbr/com/itau/sdk/android/core/endpoint/http/Cacheable;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lbr/com/itau/sdk/android/core/endpoint/http/Cacheable;

    .line 35
    sget-object v0, Lbr/com/itau/sdk/android/core/endpoint/d;->a:[B

    const/16 v1, 0xc

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    sget-object v1, Lbr/com/itau/sdk/android/core/endpoint/d;->a:[B

    const/16 v2, 0xc

    aget-byte v1, v1, v2

    invoke-static {v0, v1, v1}, Lbr/com/itau/sdk/android/core/endpoint/d;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5}, Lbr/com/itau/sdk/android/core/endpoint/http/Cacheable;->time()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v1, v2}, Lbr/com/itau/sdk/android/core/endpoint/p;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 37
    invoke-static {}, Lbr/com/itau/sdk/android/core/endpoint/a/c;->a()Lbr/com/itau/sdk/android/core/endpoint/a/c;

    move-result-object v0

    invoke-virtual {v0, v4}, Lbr/com/itau/sdk/android/core/endpoint/a/c;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6a

    .line 38
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/c;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/endpoint/p;->a()Lokhttp3/Request;

    move-result-object v1

    invoke-direct {v0, p0, v1, v4}, Lbr/com/itau/sdk/android/core/endpoint/c;-><init>(Lokhttp3/OkHttpClient;Lokhttp3/Request;Ljava/lang/String;)V

    return-object v0

    .line 43
    :cond_6a
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/endpoint/p;->a()Lokhttp3/Request;

    move-result-object v0

    invoke-virtual {p0, v0}, Lokhttp3/OkHttpClient;->newCall(Lokhttp3/Request;)Lokhttp3/Call;

    move-result-object v0

    return-object v0
.end method

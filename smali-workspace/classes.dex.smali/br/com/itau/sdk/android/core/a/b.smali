.class public final Lbr/com/itau/sdk/android/core/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/a/b$a;
    }
.end annotation


# static fields
.field private static final a:[B

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0xe

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/a/b;->a:[B

    const/16 v0, 0xb6

    sput v0, Lbr/com/itau/sdk/android/core/a/b;->b:I

    return-void

    :array_e
    .array-data 1
        0x12t
        0x8t
        -0x5dt
        0x2t
        0x4bt
        -0x4at
        -0xat
        0x4bt
        -0x4at
        -0xat
        0x4bt
        -0x4at
        -0xat
        0x4bt
    .end array-data
.end method

.method private constructor <init>()V
    .registers 1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/itau/sdk/android/core/a/c;)V
    .registers 2

    .line 12
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/a/b;-><init>()V

    return-void
.end method

.method public static a()Lbr/com/itau/sdk/android/core/a/b;
    .registers 1

    .line 36
    invoke-static {}, Lbr/com/itau/sdk/android/core/a/b$a;->a()Lbr/com/itau/sdk/android/core/a/b;

    move-result-object v0

    return-object v0
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    mul-int/lit8 p2, p2, 0x2

    rsub-int/lit8 p2, p2, 0x25

    mul-int/lit8 p1, p1, 0x2

    rsub-int/lit8 p1, p1, 0xb

    new-instance v0, Ljava/lang/String;

    sget-object v4, Lbr/com/itau/sdk/android/core/a/b;->a:[B

    const/4 v5, 0x0

    add-int/lit8 p0, p0, 0x4

    new-array v1, p1, [B

    add-int/lit8 p1, p1, -0x1

    if-nez v4, :cond_1a

    move v2, p0

    move v3, p2

    :goto_17
    add-int/2addr v2, v3

    add-int/lit8 p2, v2, 0x3

    :cond_1a
    int-to-byte v2, p2

    add-int/lit8 p0, p0, 0x1

    aput-byte v2, v1, v5

    move v2, v5

    add-int/lit8 v5, v5, 0x1

    if-ne v2, p1, :cond_29

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_29
    move v2, p2

    aget-byte v3, v4, p0

    goto :goto_17
.end method

.method private b()Lbr/com/itau/sdk/android/core/a/a;
    .registers 2

    .line 32
    const-class v0, Lbr/com/itau/sdk/android/core/a/a;

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->api(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/sdk/android/core/a/a;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/gson/JsonElement;
    .registers 12

    .line 22
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p4}, Lbr/com/itau/security/token/til/Feedback;->fillMessage(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 24
    const/4 v0, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/a/b;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v2, 0x2

    aput-object p5, v1, v2

    const/4 v2, 0x3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 26
    new-instance v5, Lbr/com/itau/sdk/android/core/model/FeedbackRequestDTO;

    invoke-direct {v5, p3, v4}, Lbr/com/itau/sdk/android/core/model/FeedbackRequestDTO;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/a/b;->b()Lbr/com/itau/sdk/android/core/a/a;

    move-result-object v0

    invoke-interface {v0, v5}, Lbr/com/itau/sdk/android/core/a/a;->a(Lbr/com/itau/sdk/android/core/model/FeedbackRequestDTO;)Lcom/google/gson/JsonElement;

    move-result-object v0

    return-object v0
.end method

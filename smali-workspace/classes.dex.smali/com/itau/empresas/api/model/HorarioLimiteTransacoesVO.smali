.class public Lcom/itau/empresas/api/model/HorarioLimiteTransacoesVO;
.super Ljava/lang/Object;
.source "HorarioLimiteTransacoesVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private horarioAutorizacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "horario_limite_autorizacao"
    .end annotation
.end field

.field private horarioInclusao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "horario_limite_inclusao"
    .end annotation
.end field

.field private tipoOperacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_operacao_sispag"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getHorarioAutorizacao()Ljava/lang/String;
    .registers 2

    .line 39
    iget-object v0, p0, Lcom/itau/empresas/api/model/HorarioLimiteTransacoesVO;->horarioAutorizacao:Ljava/lang/String;

    return-object v0
.end method

.method public getHorarioInclusao()Ljava/lang/String;
    .registers 2

    .line 31
    iget-object v0, p0, Lcom/itau/empresas/api/model/HorarioLimiteTransacoesVO;->horarioInclusao:Ljava/lang/String;

    return-object v0
.end method

.class public Lcom/itau/empresas/ui/view/ItauToolbar;
.super Landroid/support/design/widget/AppBarLayout;
.source "ItauToolbar.java"

# interfaces
.implements Landroid/support/design/widget/AppBarLayout$OnOffsetChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/ui/view/ItauToolbar$ItauToolbarListener;
    }
.end annotation


# instance fields
.field private mAttrCorBackgroundExpandido:I

.field private mAttrCorBackgroundNormal:I

.field private mAttrImagemLogoTextFont:I

.field private mAttrIsExpandido:Z

.field private mAttrSubtitulo1:Ljava/lang/String;

.field private mAttrSubtitulo1TextColor:I

.field private mAttrSubtitulo2:Ljava/lang/String;

.field private mAttrSubtitulo2TextColor:I

.field private mAttrTitulo:Ljava/lang/String;

.field private mAttrTituloTextColor:I

.field mCollapsingToolbar:Landroid/support/design/widget/CollapsingToolbarLayout;

.field private mDiffImageY:F

.field private mDiffTextoX:F

.field private mDiffTextoY:F

.field private mEstado:Z

.field mImagemLogo:Lcom/itau/empresas/ui/view/TextViewIcon;

.field private mListener:Lcom/itau/empresas/ui/view/ItauToolbar$ItauToolbarListener;

.field private mMaxScroolSize:I

.field private mPosYImage1:I

.field private mPosYTexto1:I

.field mRelativeFilho:Landroid/widget/RelativeLayout;

.field mRelativePai:Landroid/widget/RelativeLayout;

.field mSetaBaixo:Lcom/itau/empresas/ui/view/TextViewIcon;

.field mSetaCima:Lcom/itau/empresas/ui/view/TextViewIcon;

.field mSubtitulo1:Landroid/widget/TextView;

.field mSubtitulo2:Landroid/widget/TextView;

.field mTitulo:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 76
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/itau/empresas/ui/view/ItauToolbar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 80
    invoke-direct {p0, p1, p2}, Landroid/support/design/widget/AppBarLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 82
    invoke-virtual {p0, p2}, Lcom/itau/empresas/ui/view/ItauToolbar;->inicializarAtributos(Landroid/util/AttributeSet;)V

    .line 83
    return-void
.end method

.method private prepareAnimation()V
    .registers 6

    .line 195
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ItauToolbar;->getTotalScrollRange()I

    move-result v0

    if-nez v0, :cond_f

    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mCollapsingToolbar:Landroid/support/design/widget/CollapsingToolbarLayout;

    .line 196
    invoke-virtual {v0}, Landroid/support/design/widget/CollapsingToolbarLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_13

    :cond_f
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ItauToolbar;->getTotalScrollRange()I

    move-result v0

    :goto_13
    iput v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mMaxScroolSize:I

    .line 198
    iget v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mDiffImageY:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_55

    .line 199
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mRelativePai:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    div-int/lit8 v2, v0, 0x2

    .line 200
    .local v2, "midToolbarHeight":I
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mImagemLogo:Lcom/itau/empresas/ui/view/TextViewIcon;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/TextViewIcon;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v2, v0

    iput v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mPosYImage1:I

    .line 201
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mImagemLogo:Lcom/itau/empresas/ui/view/TextViewIcon;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/TextViewIcon;->getY()F

    move-result v0

    iget v1, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mPosYImage1:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mDiffImageY:F

    .line 203
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mRelativeFilho:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v2, v0

    iput v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mPosYTexto1:I

    .line 204
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mRelativeFilho:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getY()F

    move-result v0

    iget v1, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mPosYTexto1:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mDiffTextoY:F

    .line 207
    .end local v2    # "midToolbarHeight":I
    :cond_55
    iget v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mDiffTextoX:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_81

    .line 209
    const/high16 v2, 0x41800000    # 16.0f

    .line 210
    .local v2, "dp":F
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ItauToolbar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float v3, v0, v2

    .line 211
    .local v3, "fpixels":F
    const/high16 v0, 0x3f000000    # 0.5f

    add-float/2addr v0, v3

    float-to-int v4, v0

    .line 213
    .local v4, "pixels":I
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mImagemLogo:Lcom/itau/empresas/ui/view/TextViewIcon;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/TextViewIcon;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mImagemLogo:Lcom/itau/empresas/ui/view/TextViewIcon;

    invoke-virtual {v1}, Lcom/itau/empresas/ui/view/TextViewIcon;->getWidth()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    int-to-float v1, v4

    add-float/2addr v0, v1

    iput v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mDiffTextoX:F

    .line 215
    .end local v2    # "dp":F
    .end local v3    # "fpixels":F
    .end local v4    # "pixels":I
    :cond_81
    return-void
.end method


# virtual methods
.method public addItauToolbarListener(Lcom/itau/empresas/ui/view/ItauToolbar$ItauToolbarListener;)V
    .registers 2
    .param p1, "listener"    # Lcom/itau/empresas/ui/view/ItauToolbar$ItauToolbarListener;

    .line 191
    iput-object p1, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mListener:Lcom/itau/empresas/ui/view/ItauToolbar$ItauToolbarListener;

    .line 192
    return-void
.end method

.method afterViews()V
    .registers 3

    .line 117
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mTitulo:Landroid/widget/TextView;

    iget v1, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrTituloTextColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 118
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mSubtitulo1:Landroid/widget/TextView;

    iget v1, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrSubtitulo1TextColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 119
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mSubtitulo2:Landroid/widget/TextView;

    iget v1, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrSubtitulo2TextColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 121
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mTitulo:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrTitulo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mSubtitulo1:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrSubtitulo1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mSubtitulo2:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrSubtitulo2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mImagemLogo:Lcom/itau/empresas/ui/view/TextViewIcon;

    iget v1, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrImagemLogoTextFont:I

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/TextViewIcon;->setText(I)V

    .line 127
    invoke-virtual {p0, p0}, Lcom/itau/empresas/ui/view/ItauToolbar;->addOnOffsetChangedListener(Landroid/support/design/widget/AppBarLayout$OnOffsetChangedListener;)V

    .line 129
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrIsExpandido:Z

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/ui/view/ItauToolbar;->setExpanded(ZZ)V

    .line 131
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mCollapsingToolbar:Landroid/support/design/widget/CollapsingToolbarLayout;

    iget-boolean v1, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrIsExpandido:Z

    if-eqz v1, :cond_43

    iget v1, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrCorBackgroundExpandido:I

    goto :goto_45

    :cond_43
    iget v1, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrCorBackgroundNormal:I

    .line 132
    :goto_45
    invoke-virtual {v0, v1}, Landroid/support/design/widget/CollapsingToolbarLayout;->setBackgroundColor(I)V

    .line 133
    return-void
.end method

.method aoClicarSetaExpandir()V
    .registers 4

    .line 145
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mSetaBaixo:Lcom/itau/empresas/ui/view/TextViewIcon;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/TextViewIcon;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_13

    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mSetaBaixo:Lcom/itau/empresas/ui/view/TextViewIcon;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/view/TextViewIcon;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_14

    .line 146
    :cond_13
    return-void

    .line 148
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mListener:Lcom/itau/empresas/ui/view/ItauToolbar$ItauToolbarListener;

    if-eqz v0, :cond_4a

    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrIsExpandido:Z

    if-eqz v0, :cond_4a

    .line 149
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mEstado:Z

    if-nez v0, :cond_22

    const/4 v0, 0x1

    goto :goto_23

    :cond_22
    const/4 v0, 0x0

    :goto_23
    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mEstado:Z

    .line 152
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mEstado:Z

    if-eqz v0, :cond_35

    .line 153
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ItauToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f070288

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .local v2, "textoIcone":Ljava/lang/String;
    goto :goto_40

    .line 155
    .end local v2    # "textoIcone":Ljava/lang/String;
    :cond_35
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ItauToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f070286

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 157
    .local v2, "textoIcone":Ljava/lang/String;
    :goto_40
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mSetaBaixo:Lcom/itau/empresas/ui/view/TextViewIcon;

    invoke-virtual {v0, v2}, Lcom/itau/empresas/ui/view/TextViewIcon;->setText(Ljava/lang/CharSequence;)V

    .line 158
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mListener:Lcom/itau/empresas/ui/view/ItauToolbar$ItauToolbarListener;

    invoke-interface {v0}, Lcom/itau/empresas/ui/view/ItauToolbar$ItauToolbarListener;->onClickSetaBaixo()V

    .line 160
    .end local v2    # "textoIcone":Ljava/lang/String;
    :cond_4a
    return-void
.end method

.method aoClicarVoltar()V
    .registers 2

    .line 138
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mListener:Lcom/itau/empresas/ui/view/ItauToolbar$ItauToolbarListener;

    if-eqz v0, :cond_9

    .line 139
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mListener:Lcom/itau/empresas/ui/view/ItauToolbar$ItauToolbarListener;

    invoke-interface {v0}, Lcom/itau/empresas/ui/view/ItauToolbar$ItauToolbarListener;->onClickSetaEsquerda()V

    .line 141
    :cond_9
    return-void
.end method

.method inicializarAtributos(Landroid/util/AttributeSet;)V
    .registers 7
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .line 86
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ItauToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/R$styleable;->ItauToolbar:[I

    .line 87
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 89
    .local v4, "typedArray":Landroid/content/res/TypedArray;
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrImagemLogoTextFont:I

    .line 91
    .line 92
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ItauToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0c003d

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    .line 91
    const/4 v1, 0x0

    invoke-virtual {v4, v1, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrTituloTextColor:I

    .line 94
    .line 95
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ItauToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0c003d

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    .line 94
    const/4 v1, 0x1

    invoke-virtual {v4, v1, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrSubtitulo1TextColor:I

    .line 97
    .line 98
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ItauToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0c003d

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    .line 97
    const/4 v1, 0x2

    invoke-virtual {v4, v1, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrSubtitulo2TextColor:I

    .line 100
    .line 101
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ItauToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0c015d

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    .line 100
    const/4 v1, 0x6

    invoke-virtual {v4, v1, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrCorBackgroundExpandido:I

    .line 103
    .line 104
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/ItauToolbar;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0c015d

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    .line 103
    const/4 v1, 0x5

    invoke-virtual {v4, v1, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrCorBackgroundNormal:I

    .line 106
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrIsExpandido:Z

    .line 108
    const/4 v0, 0x7

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrTitulo:Ljava/lang/String;

    .line 109
    const/16 v0, 0x8

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrSubtitulo1:Ljava/lang/String;

    .line 110
    const/16 v0, 0x9

    invoke-virtual {v4, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrSubtitulo2:Ljava/lang/String;

    .line 112
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 113
    return-void
.end method

.method public onClick()V
    .registers 3

    .line 243
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mListener:Lcom/itau/empresas/ui/view/ItauToolbar$ItauToolbarListener;

    if-eqz v0, :cond_f

    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrIsExpandido:Z

    if-nez v0, :cond_f

    .line 244
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mListener:Lcom/itau/empresas/ui/view/ItauToolbar$ItauToolbarListener;

    iget-boolean v1, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrIsExpandido:Z

    invoke-interface {v0, p0, v1}, Lcom/itau/empresas/ui/view/ItauToolbar$ItauToolbarListener;->onClick(Landroid/support/design/widget/AppBarLayout;Z)V

    .line 246
    :cond_f
    return-void
.end method

.method public onOffsetChanged(Landroid/support/design/widget/AppBarLayout;I)V
    .registers 9
    .param p1, "appBarLayout"    # Landroid/support/design/widget/AppBarLayout;
    .param p2, "verticalOffset"    # I

    .line 219
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/ItauToolbar;->prepareAnimation()V

    .line 221
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mMaxScroolSize:I

    int-to-float v1, v1

    div-float v5, v0, v1

    .line 223
    .local v5, "percentage":F
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mImagemLogo:Lcom/itau/empresas/ui/view/TextViewIcon;

    iget v1, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mPosYImage1:I

    int-to-float v1, v1

    iget v2, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mDiffImageY:F

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v3, v5

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/TextViewIcon;->setY(F)V

    .line 224
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mRelativeFilho:Landroid/widget/RelativeLayout;

    iget v1, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mPosYTexto1:I

    int-to-float v1, v1

    iget v2, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mDiffTextoY:F

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v3, v5

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 225
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mRelativeFilho:Landroid/widget/RelativeLayout;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, v5

    const/high16 v2, 0x42480000    # 50.0f

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mDiffTextoX:F

    iget v3, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mDiffTextoX:F

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v4, v5

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    add-float/2addr v1, v2

    .line 226
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 228
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mSetaCima:Lcom/itau/empresas/ui/view/TextViewIcon;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, v5

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/TextViewIcon;->setAlpha(F)V

    .line 229
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mSetaBaixo:Lcom/itau/empresas/ui/view/TextViewIcon;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, v5

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/TextViewIcon;->setAlpha(F)V

    .line 231
    float-to-double v0, v5

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_62

    .line 232
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrIsExpandido:Z

    .line 233
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mCollapsingToolbar:Landroid/support/design/widget/CollapsingToolbarLayout;

    iget v1, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrCorBackgroundExpandido:I

    invoke-virtual {v0, v1}, Landroid/support/design/widget/CollapsingToolbarLayout;->setBackgroundColor(I)V

    goto :goto_6c

    .line 236
    :cond_62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrIsExpandido:Z

    .line 237
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mCollapsingToolbar:Landroid/support/design/widget/CollapsingToolbarLayout;

    iget v1, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mAttrCorBackgroundNormal:I

    invoke-virtual {v0, v1}, Landroid/support/design/widget/CollapsingToolbarLayout;->setBackgroundColor(I)V

    .line 239
    :goto_6c
    return-void
.end method

.method public setSetaCimaContentDescription(Ljava/lang/String;)V
    .registers 3
    .param p1, "mensagem"    # Ljava/lang/String;

    .line 175
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mSetaCima:Lcom/itau/empresas/ui/view/TextViewIcon;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/ui/view/TextViewIcon;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 176
    return-void
.end method

.method public setSubtitulo(Ljava/lang/String;)V
    .registers 3
    .param p1, "subtitulo"    # Ljava/lang/String;

    .line 167
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mSubtitulo1:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    return-void
.end method

.method public setSubtitulo1ContentDescription(Ljava/lang/String;)V
    .registers 3
    .param p1, "mensagem"    # Ljava/lang/String;

    .line 183
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mSubtitulo1:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 184
    return-void
.end method

.method public setSubtitulo2(Ljava/lang/String;)V
    .registers 3
    .param p1, "subtitulo2"    # Ljava/lang/String;

    .line 171
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mSubtitulo2:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    return-void
.end method

.method public setSubtitulo2ContentDescription(Ljava/lang/String;)V
    .registers 3
    .param p1, "mensagem"    # Ljava/lang/String;

    .line 187
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mSubtitulo2:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 188
    return-void
.end method

.method public setTitulo(Ljava/lang/String;)V
    .registers 3
    .param p1, "titulo"    # Ljava/lang/String;

    .line 163
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mTitulo:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    return-void
.end method

.method public setTituloContentDescription(Ljava/lang/String;)V
    .registers 3
    .param p1, "mensagem"    # Ljava/lang/String;

    .line 179
    iget-object v0, p0, Lcom/itau/empresas/ui/view/ItauToolbar;->mTitulo:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 180
    return-void
.end method

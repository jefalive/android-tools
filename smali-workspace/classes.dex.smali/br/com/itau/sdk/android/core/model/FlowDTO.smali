.class public final Lbr/com/itau/sdk/android/core/model/FlowDTO;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private op:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "op"
    .end annotation
.end field

.field private target:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "target"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .registers 2

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/FlowDTO;->target:Ljava/lang/String;

    .line 25
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/FlowDTO;->op:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 3

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/FlowDTO;->target:Ljava/lang/String;

    .line 20
    iput-object p2, p0, Lbr/com/itau/sdk/android/core/model/FlowDTO;->op:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public getOp()Ljava/lang/String;
    .registers 2

    .line 34
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/FlowDTO;->op:Ljava/lang/String;

    return-object v0
.end method

.method public getTarget()Ljava/lang/String;
    .registers 2

    .line 30
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/FlowDTO;->target:Ljava/lang/String;

    return-object v0
.end method

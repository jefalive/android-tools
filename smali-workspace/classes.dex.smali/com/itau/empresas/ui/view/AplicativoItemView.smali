.class public Lcom/itau/empresas/ui/view/AplicativoItemView;
.super Landroid/widget/LinearLayout;
.source "AplicativoItemView.java"


# instance fields
.field private activity:Lcom/itau/empresas/ui/activity/BaseActivity;

.field application:Lcom/itau/empresas/CustomApplication;

.field private divisorVisivel:Z

.field private imagemAplicativo:I

.field imgItemAplicativo:Landroid/widget/ImageView;

.field itemAplicativoDivisor:Landroid/view/View;

.field private nomeAplicativo:Ljava/lang/String;

.field private pacote:Ljava/lang/String;

.field textoAplicativoLabel:Landroid/widget/TextView;

.field textoItemAplicativoNome:Landroid/widget/TextView;

.field textoItemAplicativoStatus:Landroid/widget/TextView;

.field private urlDoPacote:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/itau/empresas/ui/activity/BaseActivity;ILjava/lang/String;Ljava/lang/String;Z)V
    .registers 8
    .param p1, "activity"    # Lcom/itau/empresas/ui/activity/BaseActivity;
    .param p2, "imagemAplicativo"    # I
    .param p3, "nomeAplicativo"    # Ljava/lang/String;
    .param p4, "pacote"    # Ljava/lang/String;
    .param p5, "divisorVisivel"    # Z

    .line 48
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 50
    iput-object p1, p0, Lcom/itau/empresas/ui/view/AplicativoItemView;->activity:Lcom/itau/empresas/ui/activity/BaseActivity;

    .line 51
    iput p2, p0, Lcom/itau/empresas/ui/view/AplicativoItemView;->imagemAplicativo:I

    .line 52
    iput-object p3, p0, Lcom/itau/empresas/ui/view/AplicativoItemView;->nomeAplicativo:Ljava/lang/String;

    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://play.google.com/store/apps/details?id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView;->urlDoPacote:Ljava/lang/String;

    .line 54
    iput-object p4, p0, Lcom/itau/empresas/ui/view/AplicativoItemView;->pacote:Ljava/lang/String;

    .line 55
    iput-boolean p5, p0, Lcom/itau/empresas/ui/view/AplicativoItemView;->divisorVisivel:Z

    .line 57
    return-void
.end method

.method private isAplicativoInstalado()Z
    .registers 5

    .line 77
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/AplicativoItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 80
    .local v2, "pm":Landroid/content/pm/PackageManager;
    :try_start_8
    iget-object v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView;->pacote:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_8 .. :try_end_e} :catch_10

    .line 81
    const/4 v0, 0x1

    return v0

    .line 82
    :catch_10
    move-exception v3

    .line 83
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method protected aoClicarAbrirAplicativo()V
    .registers 7

    .line 89
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/AplicativoItemView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 90
    .local v2, "c":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 92
    .local v3, "pm":Landroid/content/pm/PackageManager;
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/AplicativoItemView;->isAplicativoInstalado()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 93
    iget-object v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView;->pacote:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_34

    .line 95
    :cond_18
    new-instance v4, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    iget-object v1, p0, Lcom/itau/empresas/ui/view/AplicativoItemView;->urlDoPacote:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v4, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 96
    .line 97
    .local v4, "loja":Landroid/content/Intent;
    const/high16 v0, 0x10000

    invoke-virtual {v3, v4, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    .line 99
    .local v5, "resolveInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_34

    .line 100
    invoke-virtual {v2, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 104
    .end local v4    # "loja":Landroid/content/Intent;
    .end local v5    # "resolveInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v5
    :cond_34
    :goto_34
    iget-object v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView;->activity:Lcom/itau/empresas/ui/activity/BaseActivity;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/AplicativoItemView;->nomeAplicativo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/activity/BaseActivity;->analyticsHit(Ljava/lang/String;)V

    .line 105
    return-void
.end method

.method public inicializarViews()V
    .registers 3

    .line 61
    iget-object v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView;->imgItemAplicativo:Landroid/widget/ImageView;

    iget v1, p0, Lcom/itau/empresas/ui/view/AplicativoItemView;->imagemAplicativo:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView;->textoItemAplicativoNome:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/AplicativoItemView;->nomeAplicativo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    invoke-direct {p0}, Lcom/itau/empresas/ui/view/AplicativoItemView;->isAplicativoInstalado()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView;->textoItemAplicativoStatus:Landroid/widget/TextView;

    const v1, 0x7f07058b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView;->textoAplicativoLabel:Landroid/widget/TextView;

    const v1, 0x7f070589

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_35

    .line 68
    :cond_25
    iget-object v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView;->textoItemAplicativoStatus:Landroid/widget/TextView;

    const v1, 0x7f07058a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView;->textoAplicativoLabel:Landroid/widget/TextView;

    const v1, 0x7f07058c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 72
    :goto_35
    iget-object v0, p0, Lcom/itau/empresas/ui/view/AplicativoItemView;->itemAplicativoDivisor:Landroid/view/View;

    iget-boolean v1, p0, Lcom/itau/empresas/ui/view/AplicativoItemView;->divisorVisivel:Z

    if-eqz v1, :cond_3d

    const/4 v1, 0x0

    goto :goto_3f

    :cond_3d
    const/16 v1, 0x8

    :goto_3f
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 73
    return-void
.end method

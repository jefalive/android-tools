.class public final Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;
.super Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;
.source "LisComprovanteDetalheActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 38
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;-><init>()V

    .line 42
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;

    .line 38
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;

    .line 38
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 56
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 57
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 58
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 59
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;->injectExtras_()V

    .line 60
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;->afterInject()V

    .line 61
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 117
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 118
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_2c

    .line 119
    const-string v0, "comprovante"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 120
    const-string v0, "comprovante"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/ComprovanteServiceVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;->comprovante:Lcom/itau/empresas/api/model/ComprovanteServiceVO;

    .line 122
    :cond_1c
    const-string v0, "tipoComprovante"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2c

    .line 123
    const-string v0, "tipoComprovante"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;->tipoComprovante:Ljava/lang/String;

    .line 126
    :cond_2c
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 151
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_$3;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 159
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 139
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_$2;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 147
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 49
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;->init_(Landroid/os/Bundle;)V

    .line 50
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 52
    const v0, 0x7f030045

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;->setContentView(I)V

    .line 53
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 95
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 96
    const v0, 0x7f0e01ea

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;->svLisComprovanteDetalhe:Landroid/widget/ScrollView;

    .line 97
    const v0, 0x7f0e01e9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;->lisComprovanteDetalhe:Landroid/view/View;

    .line 98
    const v0, 0x7f0e01ec

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;->lisComprovanteDetalheView:Lcom/itau/empresas/feature/credito/lis/customview/LisComprovanteDetalheView;

    .line 99
    const v0, 0x7f0e00f9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/TopoItauView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;->topoItauView:Lcom/itau/empresas/ui/view/TopoItauView;

    .line 100
    const v0, 0x7f0e01f0

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 102
    .local v1, "view_botao_compartilhar":Landroid/view/View;
    if-eqz v1, :cond_46

    .line 103
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_$1;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    :cond_46
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;->aoCarregarTela()V

    .line 113
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 65
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->setContentView(I)V

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 67
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 77
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->setContentView(Landroid/view/View;)V

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 79
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 71
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 72
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 73
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 130
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity;->setIntent(Landroid/content/Intent;)V

    .line 131
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisComprovanteDetalheActivity_;->injectExtras_()V

    .line 132
    return-void
.end method

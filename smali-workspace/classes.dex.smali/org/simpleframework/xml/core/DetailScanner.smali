.class Lorg/simpleframework/xml/core/DetailScanner;
.super Ljava/lang/Object;
.source "DetailScanner.java"

# interfaces
.implements Lorg/simpleframework/xml/core/Detail;


# instance fields
.field private access:Lorg/simpleframework/xml/DefaultType;

.field private declaration:Lorg/simpleframework/xml/NamespaceList;

.field private fields:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lorg/simpleframework/xml/core/FieldDetail;>;"
        }
    .end annotation
.end field

.field private labels:[Ljava/lang/annotation/Annotation;

.field private methods:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lorg/simpleframework/xml/core/MethodDetail;>;"
        }
    .end annotation
.end field

.field private name:Ljava/lang/String;

.field private namespace:Lorg/simpleframework/xml/Namespace;

.field private order:Lorg/simpleframework/xml/Order;

.field private override:Lorg/simpleframework/xml/DefaultType;

.field private required:Z

.field private root:Lorg/simpleframework/xml/Root;

.field private strict:Z

.field private type:Ljava/lang/Class;


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .registers 3
    .param p1, "type"    # Ljava/lang/Class;

    .line 121
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/simpleframework/xml/core/DetailScanner;-><init>(Ljava/lang/Class;Lorg/simpleframework/xml/DefaultType;)V

    .line 122
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;Lorg/simpleframework/xml/DefaultType;)V
    .registers 4
    .param p1, "type"    # Ljava/lang/Class;
    .param p2, "override"    # Lorg/simpleframework/xml/DefaultType;

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->methods:Ljava/util/List;

    .line 135
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->fields:Ljava/util/List;

    .line 136
    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaredAnnotations()[Ljava/lang/annotation/Annotation;

    move-result-object v0

    iput-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->labels:[Ljava/lang/annotation/Annotation;

    .line 137
    iput-object p2, p0, Lorg/simpleframework/xml/core/DetailScanner;->override:Lorg/simpleframework/xml/DefaultType;

    .line 138
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->strict:Z

    .line 139
    iput-object p1, p0, Lorg/simpleframework/xml/core/DetailScanner;->type:Ljava/lang/Class;

    .line 140
    invoke-direct {p0, p1}, Lorg/simpleframework/xml/core/DetailScanner;->scan(Ljava/lang/Class;)V

    .line 141
    return-void
.end method

.method private access(Ljava/lang/annotation/Annotation;)V
    .registers 4
    .param p1, "label"    # Ljava/lang/annotation/Annotation;

    .line 494
    if-eqz p1, :cond_11

    .line 495
    move-object v1, p1

    check-cast v1, Lorg/simpleframework/xml/Default;

    .line 497
    .local v1, "value":Lorg/simpleframework/xml/Default;
    invoke-interface {v1}, Lorg/simpleframework/xml/Default;->required()Z

    move-result v0

    iput-boolean v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->required:Z

    .line 498
    invoke-interface {v1}, Lorg/simpleframework/xml/Default;->value()Lorg/simpleframework/xml/DefaultType;

    move-result-object v0

    iput-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->access:Lorg/simpleframework/xml/DefaultType;

    .line 500
    .end local v1    # "value":Lorg/simpleframework/xml/Default;
    :cond_11
    return-void
.end method

.method private extract(Ljava/lang/Class;)V
    .registers 7
    .param p1, "type"    # Ljava/lang/Class;

    .line 380
    iget-object v1, p0, Lorg/simpleframework/xml/core/DetailScanner;->labels:[Ljava/lang/annotation/Annotation;

    .local v1, "arr$":[Ljava/lang/annotation/Annotation;
    array-length v2, v1

    .local v2, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_4
    if-ge v3, v2, :cond_2e

    aget-object v4, v1, v3

    .line 381
    .local v4, "label":Ljava/lang/annotation/Annotation;
    instance-of v0, v4, Lorg/simpleframework/xml/Namespace;

    if-eqz v0, :cond_f

    .line 382
    invoke-direct {p0, v4}, Lorg/simpleframework/xml/core/DetailScanner;->namespace(Ljava/lang/annotation/Annotation;)V

    .line 384
    :cond_f
    instance-of v0, v4, Lorg/simpleframework/xml/NamespaceList;

    if-eqz v0, :cond_16

    .line 385
    invoke-direct {p0, v4}, Lorg/simpleframework/xml/core/DetailScanner;->scope(Ljava/lang/annotation/Annotation;)V

    .line 387
    :cond_16
    instance-of v0, v4, Lorg/simpleframework/xml/Root;

    if-eqz v0, :cond_1d

    .line 388
    invoke-direct {p0, v4}, Lorg/simpleframework/xml/core/DetailScanner;->root(Ljava/lang/annotation/Annotation;)V

    .line 390
    :cond_1d
    instance-of v0, v4, Lorg/simpleframework/xml/Order;

    if-eqz v0, :cond_24

    .line 391
    invoke-direct {p0, v4}, Lorg/simpleframework/xml/core/DetailScanner;->order(Ljava/lang/annotation/Annotation;)V

    .line 393
    :cond_24
    instance-of v0, v4, Lorg/simpleframework/xml/Default;

    if-eqz v0, :cond_2b

    .line 394
    invoke-direct {p0, v4}, Lorg/simpleframework/xml/core/DetailScanner;->access(Ljava/lang/annotation/Annotation;)V

    .line 380
    .end local v4    # "label":Ljava/lang/annotation/Annotation;
    :cond_2b
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 397
    .end local v1    # "arr$":[Ljava/lang/annotation/Annotation;
    .end local v2    # "len$":I
    .end local v3    # "i$":I
    :cond_2e
    return-void
.end method

.method private fields(Ljava/lang/Class;)V
    .registers 9
    .param p1, "type"    # Ljava/lang/Class;

    .line 425
    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    .line 427
    .local v1, "list":[Ljava/lang/reflect/Field;
    move-object v2, v1

    .local v2, "arr$":[Ljava/lang/reflect/Field;
    array-length v3, v2

    .local v3, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_7
    if-ge v4, v3, :cond_18

    aget-object v5, v2, v4

    .line 428
    .local v5, "field":Ljava/lang/reflect/Field;
    new-instance v6, Lorg/simpleframework/xml/core/FieldDetail;

    invoke-direct {v6, v5}, Lorg/simpleframework/xml/core/FieldDetail;-><init>(Ljava/lang/reflect/Field;)V

    .line 429
    .local v6, "detail":Lorg/simpleframework/xml/core/FieldDetail;
    iget-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->fields:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 427
    .end local v5    # "field":Ljava/lang/reflect/Field;
    .end local v6    # "detail":Lorg/simpleframework/xml/core/FieldDetail;
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 431
    .end local v2    # "arr$":[Ljava/lang/reflect/Field;
    .end local v3    # "len$":I
    .end local v4    # "i$":I
    :cond_18
    return-void
.end method

.method private isEmpty(Ljava/lang/String;)Z
    .registers 3
    .param p1, "value"    # Ljava/lang/String;

    .line 470
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    goto :goto_9

    :cond_8
    const/4 v0, 0x0

    :goto_9
    return v0
.end method

.method private methods(Ljava/lang/Class;)V
    .registers 9
    .param p1, "type"    # Ljava/lang/Class;

    .line 408
    invoke-virtual {p1}, Ljava/lang/Class;->getDeclaredMethods()[Ljava/lang/reflect/Method;

    move-result-object v1

    .line 410
    .local v1, "list":[Ljava/lang/reflect/Method;
    move-object v2, v1

    .local v2, "arr$":[Ljava/lang/reflect/Method;
    array-length v3, v2

    .local v3, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_7
    if-ge v4, v3, :cond_18

    aget-object v5, v2, v4

    .line 411
    .local v5, "method":Ljava/lang/reflect/Method;
    new-instance v6, Lorg/simpleframework/xml/core/MethodDetail;

    invoke-direct {v6, v5}, Lorg/simpleframework/xml/core/MethodDetail;-><init>(Ljava/lang/reflect/Method;)V

    .line 412
    .local v6, "detail":Lorg/simpleframework/xml/core/MethodDetail;
    iget-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->methods:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 410
    .end local v5    # "method":Ljava/lang/reflect/Method;
    .end local v6    # "detail":Lorg/simpleframework/xml/core/MethodDetail;
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 414
    .end local v2    # "arr$":[Ljava/lang/reflect/Method;
    .end local v3    # "len$":I
    .end local v4    # "i$":I
    :cond_18
    return-void
.end method

.method private namespace(Ljava/lang/annotation/Annotation;)V
    .registers 3
    .param p1, "label"    # Ljava/lang/annotation/Annotation;

    .line 511
    if-eqz p1, :cond_7

    .line 512
    move-object v0, p1

    check-cast v0, Lorg/simpleframework/xml/Namespace;

    iput-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->namespace:Lorg/simpleframework/xml/Namespace;

    .line 514
    :cond_7
    return-void
.end method

.method private order(Ljava/lang/annotation/Annotation;)V
    .registers 3
    .param p1, "label"    # Ljava/lang/annotation/Annotation;

    .line 481
    if-eqz p1, :cond_7

    .line 482
    move-object v0, p1

    check-cast v0, Lorg/simpleframework/xml/Order;

    iput-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->order:Lorg/simpleframework/xml/Order;

    .line 484
    :cond_7
    return-void
.end method

.method private root(Ljava/lang/annotation/Annotation;)V
    .registers 6
    .param p1, "label"    # Ljava/lang/annotation/Annotation;

    .line 441
    if-eqz p1, :cond_26

    .line 442
    move-object v1, p1

    check-cast v1, Lorg/simpleframework/xml/Root;

    .line 443
    .local v1, "value":Lorg/simpleframework/xml/Root;
    iget-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->type:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    .line 444
    .local v2, "real":Ljava/lang/String;
    move-object v3, v2

    .line 446
    .local v3, "text":Ljava/lang/String;
    if-eqz v1, :cond_26

    .line 447
    invoke-interface {v1}, Lorg/simpleframework/xml/Root;->name()Ljava/lang/String;

    move-result-object v3

    .line 449
    invoke-direct {p0, v3}, Lorg/simpleframework/xml/core/DetailScanner;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 450
    invoke-static {v2}, Lorg/simpleframework/xml/core/Reflector;->getName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 452
    :cond_1c
    invoke-interface {v1}, Lorg/simpleframework/xml/Root;->strict()Z

    move-result v0

    iput-boolean v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->strict:Z

    .line 453
    iput-object v1, p0, Lorg/simpleframework/xml/core/DetailScanner;->root:Lorg/simpleframework/xml/Root;

    .line 454
    iput-object v3, p0, Lorg/simpleframework/xml/core/DetailScanner;->name:Ljava/lang/String;

    .line 457
    .end local v1    # "value":Lorg/simpleframework/xml/Root;
    .end local v2    # "real":Ljava/lang/String;
    .end local v3    # "text":Ljava/lang/String;
    :cond_26
    return-void
.end method

.method private scan(Ljava/lang/Class;)V
    .registers 2
    .param p1, "type"    # Ljava/lang/Class;

    .line 366
    invoke-direct {p0, p1}, Lorg/simpleframework/xml/core/DetailScanner;->methods(Ljava/lang/Class;)V

    .line 367
    invoke-direct {p0, p1}, Lorg/simpleframework/xml/core/DetailScanner;->fields(Ljava/lang/Class;)V

    .line 368
    invoke-direct {p0, p1}, Lorg/simpleframework/xml/core/DetailScanner;->extract(Ljava/lang/Class;)V

    .line 369
    return-void
.end method

.method private scope(Ljava/lang/annotation/Annotation;)V
    .registers 3
    .param p1, "label"    # Ljava/lang/annotation/Annotation;

    .line 525
    if-eqz p1, :cond_7

    .line 526
    move-object v0, p1

    check-cast v0, Lorg/simpleframework/xml/NamespaceList;

    iput-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->declaration:Lorg/simpleframework/xml/NamespaceList;

    .line 528
    :cond_7
    return-void
.end method


# virtual methods
.method public getAccess()Lorg/simpleframework/xml/DefaultType;
    .registers 2

    .line 266
    iget-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->override:Lorg/simpleframework/xml/DefaultType;

    if-eqz v0, :cond_7

    .line 267
    iget-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->override:Lorg/simpleframework/xml/DefaultType;

    return-object v0

    .line 269
    :cond_7
    iget-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->access:Lorg/simpleframework/xml/DefaultType;

    return-object v0
.end method

.method public getAnnotations()[Ljava/lang/annotation/Annotation;
    .registers 2

    .line 326
    iget-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->labels:[Ljava/lang/annotation/Annotation;

    return-object v0
.end method

.method public getConstructors()[Ljava/lang/reflect/Constructor;
    .registers 2

    .line 338
    iget-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->type:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getDeclaredConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v0

    return-object v0
.end method

.method public getFields()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lorg/simpleframework/xml/core/FieldDetail;>;"
        }
    .end annotation

    .line 314
    iget-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->fields:Ljava/util/List;

    return-object v0
.end method

.method public getMethods()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lorg/simpleframework/xml/core/MethodDetail;>;"
        }
    .end annotation

    .line 303
    iget-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->methods:Ljava/util/List;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    .line 218
    iget-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getNamespace()Lorg/simpleframework/xml/Namespace;
    .registers 2

    .line 280
    iget-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->namespace:Lorg/simpleframework/xml/Namespace;

    return-object v0
.end method

.method public getNamespaceList()Lorg/simpleframework/xml/NamespaceList;
    .registers 2

    .line 292
    iget-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->declaration:Lorg/simpleframework/xml/NamespaceList;

    return-object v0
.end method

.method public getOrder()Lorg/simpleframework/xml/Order;
    .registers 2

    .line 242
    iget-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->order:Lorg/simpleframework/xml/Order;

    return-object v0
.end method

.method public getOverride()Lorg/simpleframework/xml/DefaultType;
    .registers 2

    .line 255
    iget-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->override:Lorg/simpleframework/xml/DefaultType;

    return-object v0
.end method

.method public getRoot()Lorg/simpleframework/xml/Root;
    .registers 2

    .line 205
    iget-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->root:Lorg/simpleframework/xml/Root;

    return-object v0
.end method

.method public getSuper()Ljava/lang/Class;
    .registers 3

    .line 349
    iget-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->type:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    .line 351
    .local v1, "base":Ljava/lang/Class;
    const-class v0, Ljava/lang/Object;

    if-ne v1, v0, :cond_c

    .line 352
    const/4 v0, 0x0

    return-object v0

    .line 354
    :cond_c
    return-object v1
.end method

.method public getType()Ljava/lang/Class;
    .registers 2

    .line 229
    iget-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->type:Ljava/lang/Class;

    return-object v0
.end method

.method public isInstantiable()Z
    .registers 3

    .line 189
    iget-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->type:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getModifiers()I

    move-result v1

    .line 191
    .local v1, "modifiers":I
    invoke-static {v1}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 192
    const/4 v0, 0x1

    return v0

    .line 194
    :cond_e
    iget-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->type:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->isMemberClass()Z

    move-result v0

    if-nez v0, :cond_18

    const/4 v0, 0x1

    goto :goto_19

    :cond_18
    const/4 v0, 0x0

    :goto_19
    return v0
.end method

.method public isPrimitive()Z
    .registers 2

    .line 177
    iget-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->type:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->isPrimitive()Z

    move-result v0

    return v0
.end method

.method public isRequired()Z
    .registers 2

    .line 152
    iget-boolean v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->required:Z

    return v0
.end method

.method public isStrict()Z
    .registers 2

    .line 166
    iget-boolean v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->strict:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .line 538
    iget-object v0, p0, Lorg/simpleframework/xml/core/DetailScanner;->type:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

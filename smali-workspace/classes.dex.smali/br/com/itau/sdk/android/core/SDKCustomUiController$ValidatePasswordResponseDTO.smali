.class public Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidatePasswordResponseDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/sdk/android/core/SDKCustomUiController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ValidatePasswordResponseDTO"
.end annotation


# static fields
.field private static final d:[B

.field private static e:I


# instance fields
.field private b:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo"
    .end annotation
.end field

.field private c:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "mensagem"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x31

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidatePasswordResponseDTO;->d:[B

    const/16 v0, 0x9e

    sput v0, Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidatePasswordResponseDTO;->e:I

    return-void

    :array_e
    .array-data 1
        0x6et
        -0xft
        0x12t
        -0x71t
        -0x2t
        -0x2t
        -0x10t
        -0x12t
        -0x10t
        0x6t
        -0x1ct
        -0x22t
        0x4t
        0x5t
        -0xdt
        -0x9t
        -0x15t
        -0xat
        -0x1bt
        -0x1ft
        0x6t
        0x1t
        -0x10t
        -0xet
        -0xet
        -0x8t
        -0x1bt
        -0x2et
        0x3t
        -0x12t
        0x1ft
        -0x25t
        -0x1t
        -0x18t
        -0x8t
        -0xft
        -0x5t
        -0x3ft
        -0x23t
        -0x19t
        0x40t
        -0x15t
        -0x4t
        -0x8t
        -0x1ft
        -0x7t
        -0xft
        -0x5t
        -0x3dt
    .end array-data
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    mul-int/lit8 p2, p2, 0x2a

    rsub-int/lit8 p2, p2, 0x56

    mul-int/lit8 p0, p0, 0x23

    add-int/lit8 p0, p0, 0x4

    const/4 v5, 0x0

    sget-object v4, Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidatePasswordResponseDTO;->d:[B

    mul-int/lit8 p1, p1, 0x19

    add-int/lit8 p1, p1, 0xb

    new-instance v0, Ljava/lang/String;

    new-array v1, p1, [B

    if-nez v4, :cond_1c

    move v2, p2

    move v3, p0

    :goto_17
    add-int/2addr v2, v3

    add-int/lit8 p2, v2, 0xd

    add-int/lit8 p0, p0, 0x1

    :cond_1c
    move v2, v5

    int-to-byte v3, p2

    add-int/lit8 v5, v5, 0x1

    aput-byte v3, v1, v2

    if-ne v5, p1, :cond_29

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_29
    move v2, p2

    aget-byte v3, v4, p0

    goto :goto_17
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 6

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidatePasswordResponseDTO;->d:[B

    const/16 v2, 0x15

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    sget-object v2, Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidatePasswordResponseDTO;->d:[B

    const/16 v3, 0x15

    aget-byte v2, v2, v3

    add-int/lit8 v3, v2, -0x1

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidatePasswordResponseDTO;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidatePasswordResponseDTO;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidatePasswordResponseDTO;->d:[B

    const/16 v2, 0x15

    aget-byte v1, v1, v2

    add-int/lit8 v2, v1, -0x1

    sget-object v3, Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidatePasswordResponseDTO;->d:[B

    const/16 v4, 0x15

    aget-byte v3, v3, v4

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidatePasswordResponseDTO;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/SDKCustomUiController$ValidatePasswordResponseDTO;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

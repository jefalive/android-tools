.class public final Lcom/itau/empresas/Evento$EventoLisConsulta;
.super Ljava/lang/Object;
.source "Evento.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/Evento;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EventoLisConsulta"
.end annotation


# instance fields
.field private produtoLimite:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getProdutoLimite()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;>;"
        }
    .end annotation

    .line 298
    iget-object v0, p0, Lcom/itau/empresas/Evento$EventoLisConsulta;->produtoLimite:Ljava/util/List;

    return-object v0
.end method

.method public setProdutoLimite(Ljava/util/List;)V
    .registers 2
    .param p1, "produtoLimite"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO;>;)V"
        }
    .end annotation

    .line 302
    iput-object p1, p0, Lcom/itau/empresas/Evento$EventoLisConsulta;->produtoLimite:Ljava/util/List;

    .line 303
    return-void
.end method

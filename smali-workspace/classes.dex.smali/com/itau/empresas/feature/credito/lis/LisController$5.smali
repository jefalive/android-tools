.class Lcom/itau/empresas/feature/credito/lis/LisController$5;
.super Ljava/lang/Object;
.source "LisController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/credito/lis/LisController;->buscaConsultaDetalha(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/credito/lis/LisController;

.field final synthetic val$cpfRepresentante:Ljava/lang/String;

.field final synthetic val$idProduto:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/credito/lis/LisController;Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "this$0"    # Lcom/itau/empresas/feature/credito/lis/LisController;

    .line 84
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/lis/LisController$5;->this$0:Lcom/itau/empresas/feature/credito/lis/LisController;

    iput-object p2, p0, Lcom/itau/empresas/feature/credito/lis/LisController$5;->val$idProduto:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/feature/credito/lis/LisController$5;->val$cpfRepresentante:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 11

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/LisController$5;->this$0:Lcom/itau/empresas/feature/credito/lis/LisController;

    iget-object v0, v0, Lcom/itau/empresas/feature/credito/lis/LisController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v9

    .line 89
    .local v9, "contaSelecionada":Lcom/itau/empresas/api/model/ContaOperadorVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/LisController$5;->this$0:Lcom/itau/empresas/feature/credito/lis/LisController;

    # invokes: Lcom/itau/empresas/feature/credito/lis/LisController;->api()Ljava/lang/Object;
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/lis/LisController;->access$800(Lcom/itau/empresas/feature/credito/lis/LisController;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    .line 90
    invoke-virtual {v9}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v1

    .line 91
    invoke-virtual {v9}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x7

    invoke-static {v3, v2}, Lcom/itau/empresas/ui/util/ViewUtils;->formataAdicionandoZeros(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 92
    invoke-virtual {v9}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v4

    .line 93
    invoke-virtual {v9}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x7

    invoke-static {v6, v5}, Lcom/itau/empresas/ui/util/ViewUtils;->formataAdicionandoZeros(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 94
    invoke-virtual {v9}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/itau/empresas/feature/credito/lis/LisController$5;->val$idProduto:Ljava/lang/String;

    iget-object v8, p0, Lcom/itau/empresas/feature/credito/lis/LisController$5;->val$cpfRepresentante:Ljava/lang/String;

    .line 89
    invoke-interface/range {v0 .. v8}, Lcom/itau/empresas/api/Api;->buscaConsultaDetalhadaLis(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;

    move-result-object v0

    # invokes: Lcom/itau/empresas/feature/credito/lis/LisController;->post(Ljava/lang/Object;)V
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/lis/LisController;->access$900(Ljava/lang/Object;)V

    .line 96
    return-void
.end method

.class public Lcom/itau/empresas/api/universal/model/DadosGps;
.super Ljava/lang/Object;
.source "DadosGps.java"


# instance fields
.field private valorAtualizacaoMonetaria:Ljava/lang/String;

.field private valorInss:Ljava/lang/String;

.field private valorOutrasEntidades:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getValorAPagar()Ljava/lang/String;
    .registers 2

    .line 22
    invoke-virtual {p0}, Lcom/itau/empresas/api/universal/model/DadosGps;->totalPagamentoGPS()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setValorAtualizacaoMonetaria(Ljava/lang/String;)V
    .registers 2
    .param p1, "valorAcrescimo"    # Ljava/lang/String;

    .line 30
    iput-object p1, p0, Lcom/itau/empresas/api/universal/model/DadosGps;->valorAtualizacaoMonetaria:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public setValorInss(Ljava/lang/String;)V
    .registers 2
    .param p1, "valorInss"    # Ljava/lang/String;

    .line 26
    iput-object p1, p0, Lcom/itau/empresas/api/universal/model/DadosGps;->valorInss:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public setValorOutrasEntidades(Ljava/lang/String;)V
    .registers 2
    .param p1, "valorOutrasEntidades"    # Ljava/lang/String;

    .line 34
    iput-object p1, p0, Lcom/itau/empresas/api/universal/model/DadosGps;->valorOutrasEntidades:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public totalPagamentoGPS()Ljava/math/BigDecimal;
    .registers 7

    .line 38
    new-instance v2, Ljava/math/BigDecimal;

    const-string v0, "0.00"

    invoke-direct {v2, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 39
    .local v2, "valorPrincipalFormatado":Ljava/math/BigDecimal;
    new-instance v3, Ljava/math/BigDecimal;

    const-string v0, "0.00"

    invoke-direct {v3, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 40
    .local v3, "valorJurosFormatado":Ljava/math/BigDecimal;
    new-instance v4, Ljava/math/BigDecimal;

    const-string v0, "0.00"

    invoke-direct {v4, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 42
    .local v4, "valorMultaFormatado":Ljava/math/BigDecimal;
    :try_start_15
    iget-object v0, p0, Lcom/itau/empresas/api/universal/model/DadosGps;->valorInss:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2d

    .line 43
    new-instance v2, Ljava/math/BigDecimal;

    iget-object v0, p0, Lcom/itau/empresas/api/universal/model/DadosGps;->valorInss:Ljava/lang/String;

    .line 44
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorMonetarioParaDecimal(Ljava/lang/String;Z)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 47
    :cond_2d
    iget-object v0, p0, Lcom/itau/empresas/api/universal/model/DadosGps;->valorAtualizacaoMonetaria:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_53

    iget-object v0, p0, Lcom/itau/empresas/api/universal/model/DadosGps;->valorAtualizacaoMonetaria:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, "000"

    .line 48
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_53

    .line 49
    new-instance v3, Ljava/math/BigDecimal;

    iget-object v0, p0, Lcom/itau/empresas/api/universal/model/DadosGps;->valorAtualizacaoMonetaria:Ljava/lang/String;

    .line 50
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorMonetarioParaDecimal(Ljava/lang/String;Z)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 53
    :cond_53
    iget-object v0, p0, Lcom/itau/empresas/api/universal/model/DadosGps;->valorOutrasEntidades:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6b

    .line 54
    new-instance v4, Ljava/math/BigDecimal;

    iget-object v0, p0, Lcom/itau/empresas/api/universal/model/DadosGps;->valorOutrasEntidades:Ljava/lang/String;

    .line 55
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorMonetarioParaDecimal(Ljava/lang/String;Z)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V
    :try_end_6b
    .catch Ljava/text/ParseException; {:try_start_15 .. :try_end_6b} :catch_6c

    .line 59
    :cond_6b
    goto :goto_75

    .line 57
    :catch_6c
    move-exception v5

    .line 58
    .local v5, "px":Ljava/text/ParseException;
    new-instance v0, Ljava/math/BigDecimal;

    const-string v1, "0.00"

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    return-object v0

    .line 60
    .end local v5    # "px":Ljava/text/ParseException;
    :goto_75
    invoke-virtual {v3, v4}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

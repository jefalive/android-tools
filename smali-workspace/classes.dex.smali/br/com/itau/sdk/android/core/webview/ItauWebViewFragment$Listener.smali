.class public interface abstract Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment$Listener;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/sdk/android/core/webview/ItauWebViewFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onPageError()V
.end method

.method public abstract onPageFinished()V
.end method

.method public abstract onPageStarted()V
.end method

.method public abstract pageTitle(Ljava/lang/String;)V
.end method

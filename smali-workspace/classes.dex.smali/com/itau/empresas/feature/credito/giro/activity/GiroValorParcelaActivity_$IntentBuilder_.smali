.class public Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;
.super Lorg/androidannotations/api/builder/ActivityIntentBuilder;
.source "GiroValorParcelaActivity_.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IntentBuilder_"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/androidannotations/api/builder/ActivityIntentBuilder<Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;>;"
    }
.end annotation


# instance fields
.field private fragmentSupport_:Landroid/support/v4/app/Fragment;

.field private fragment_:Landroid/app/Fragment;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 191
    const-class v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_;

    invoke-direct {p0, p1, v0}, Lorg/androidannotations/api/builder/ActivityIntentBuilder;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 192
    return-void
.end method


# virtual methods
.method public exibicao(Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;
    .registers 3
    .param p1, "exibicao"    # Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity$Exibicao;

    .line 238
    const-string v0, "exibicao"

    invoke-super {p0, v0, p1}, Lorg/androidannotations/api/builder/ActivityIntentBuilder;->extra(Ljava/lang/String;Ljava/io/Serializable;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    return-object v0
.end method

.method public ofertaEspecialAceita(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;
    .registers 3
    .param p1, "ofertaEspecialAceita"    # Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 308
    const-string v0, "ofertaEspecialAceita"

    invoke-super {p0, v0, p1}, Lorg/androidannotations/api/builder/ActivityIntentBuilder;->extra(Ljava/lang/String;Ljava/io/Serializable;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    return-object v0
.end method

.method public parcelaMaxima(J)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;
    .registers 4
    .param p1, "parcelaMaxima"    # J

    .line 288
    const-string v0, "parcelaMaxima"

    invoke-super {p0, v0, p1, p2}, Lorg/androidannotations/api/builder/ActivityIntentBuilder;->extra(Ljava/lang/String;J)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    return-object v0
.end method

.method public parcelaMinima(J)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;
    .registers 4
    .param p1, "parcelaMinima"    # J

    .line 278
    const-string v0, "parcelaMinima"

    invoke-super {p0, v0, p1, p2}, Lorg/androidannotations/api/builder/ActivityIntentBuilder;->extra(Ljava/lang/String;J)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    return-object v0
.end method

.method public produtosParceladosSimulacaoPropostaSaidaVO(Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;
    .registers 3
    .param p1, "produtosParceladosSimulacaoPropostaSaidaVO"    # Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoPropostaSaidaVO;

    .line 298
    const-string v0, "produtosParceladosSimulacaoPropostaSaidaVO"

    invoke-super {p0, v0, p1}, Lorg/androidannotations/api/builder/ActivityIntentBuilder;->extra(Ljava/lang/String;Ljava/io/Serializable;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    return-object v0
.end method

.method public startForResult(I)Lorg/androidannotations/api/builder/PostActivityStarter;
    .registers 6
    .param p1, "requestCode"    # I

    .line 206
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->fragmentSupport_:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_c

    .line 207
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->fragmentSupport_:Landroid/support/v4/app/Fragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_52

    .line 209
    :cond_c
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->fragment_:Landroid/app/Fragment;

    if-eqz v0, :cond_28

    .line 210
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_20

    .line 211
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->fragment_:Landroid/app/Fragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->lastOptions:Landroid/os/Bundle;

    invoke-virtual {v0, v1, p1, v2}, Landroid/app/Fragment;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V

    goto :goto_52

    .line 213
    :cond_20
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->fragment_:Landroid/app/Fragment;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v1, p1}, Landroid/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_52

    .line 216
    :cond_28
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->context:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_3b

    .line 217
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->context:Landroid/content/Context;

    move-object v3, v0

    check-cast v3, Landroid/app/Activity;

    .line 218
    .local v3, "activity":Landroid/app/Activity;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->lastOptions:Landroid/os/Bundle;

    invoke-static {v3, v0, p1, v1}, Landroid/support/v4/app/ActivityCompat;->startActivityForResult(Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)V

    .line 219
    .end local v3    # "activity":Landroid/app/Activity;
    goto :goto_52

    .line 220
    :cond_3b
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_4b

    .line 221
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->lastOptions:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    goto :goto_52

    .line 223
    :cond_4b
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 228
    :goto_52
    new-instance v0, Lorg/androidannotations/api/builder/PostActivityStarter;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lorg/androidannotations/api/builder/PostActivityStarter;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public temSeguro(Z)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;
    .registers 3
    .param p1, "temSeguro"    # Z

    .line 248
    const-string v0, "temSeguro"

    invoke-super {p0, v0, p1}, Lorg/androidannotations/api/builder/ActivityIntentBuilder;->extra(Ljava/lang/String;Z)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    return-object v0
.end method

.method public valorMaximo(F)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;
    .registers 3
    .param p1, "valorMaximo"    # F

    .line 268
    const-string v0, "valorMaximo"

    invoke-super {p0, v0, p1}, Lorg/androidannotations/api/builder/ActivityIntentBuilder;->extra(Ljava/lang/String;F)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    return-object v0
.end method

.method public valorMinimo(F)Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;
    .registers 3
    .param p1, "valorMinimo"    # F

    .line 258
    const-string v0, "valorMinimo"

    invoke-super {p0, v0, p1}, Lorg/androidannotations/api/builder/ActivityIntentBuilder;->extra(Ljava/lang/String;F)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/activity/GiroValorParcelaActivity_$IntentBuilder_;

    return-object v0
.end method

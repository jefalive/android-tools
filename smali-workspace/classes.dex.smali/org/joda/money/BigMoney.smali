.class public final Lorg/joda/money/BigMoney;
.super Ljava/lang/Object;
.source "BigMoney.java"

# interfaces
.implements Lorg/joda/money/BigMoneyProvider;
.implements Ljava/lang/Comparable;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Lorg/joda/money/BigMoneyProvider;Ljava/lang/Comparable<Lorg/joda/money/BigMoneyProvider;>;Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static final PARSE_REGEX:Ljava/util/regex/Pattern;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final amount:Ljava/math/BigDecimal;

.field private final currency:Lorg/joda/money/CurrencyUnit;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 45
    const-class v0, Lorg/joda/money/BigMoney;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    sput-boolean v0, Lorg/joda/money/BigMoney;->$assertionsDisabled:Z

    .line 54
    const-string v0, "[+-]?[0-9]*[.]?[0-9]*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lorg/joda/money/BigMoney;->PARSE_REGEX:Ljava/util/regex/Pattern;

    return-void
.end method

.method constructor <init>(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)V
    .registers 5
    .param p1, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p2, "amount"    # Ljava/math/BigDecimal;

    .line 418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 419
    sget-boolean v0, Lorg/joda/money/BigMoney;->$assertionsDisabled:Z

    if-nez v0, :cond_11

    if-nez p1, :cond_11

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Joda-Money bug: Currency must not be null"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 420
    :cond_11
    sget-boolean v0, Lorg/joda/money/BigMoney;->$assertionsDisabled:Z

    if-nez v0, :cond_1f

    if-nez p2, :cond_1f

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Joda-Money bug: Amount must not be null"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 421
    :cond_1f
    iput-object p1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    .line 422
    invoke-virtual {p2}, Ljava/math/BigDecimal;->scale()I

    move-result v0

    if-gez v0, :cond_2d

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v0

    goto :goto_2e

    :cond_2d
    move-object v0, p2

    :goto_2e
    iput-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    .line 423
    return-void
.end method

.method private checkCurrencyEqual(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;
    .registers 6
    .param p1, "moneyProvider"    # Lorg/joda/money/BigMoneyProvider;

    .line 819
    invoke-static {p1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v3

    .line 820
    .local v3, "money":Lorg/joda/money/BigMoney;
    invoke-virtual {p0, v3}, Lorg/joda/money/BigMoney;->isSameCurrency(Lorg/joda/money/BigMoneyProvider;)Z

    move-result v0

    if-nez v0, :cond_18

    .line 821
    new-instance v0, Lorg/joda/money/CurrencyMismatchException;

    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v1

    invoke-virtual {v3}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/joda/money/CurrencyMismatchException;-><init>(Lorg/joda/money/CurrencyUnit;Lorg/joda/money/CurrencyUnit;)V

    throw v0

    .line 823
    :cond_18
    return-object v3
.end method

.method public static nonNull(Lorg/joda/money/BigMoney;Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/BigMoney;
    .registers 4
    .param p0, "money"    # Lorg/joda/money/BigMoney;
    .param p1, "currency"    # Lorg/joda/money/CurrencyUnit;

    .line 401
    if-nez p0, :cond_7

    .line 402
    invoke-static {p1}, Lorg/joda/money/BigMoney;->zero(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0

    .line 404
    :cond_7
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/joda/money/CurrencyUnit;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_20

    .line 405
    const-string v0, "Currency must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 406
    new-instance v0, Lorg/joda/money/CurrencyMismatchException;

    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lorg/joda/money/CurrencyMismatchException;-><init>(Lorg/joda/money/CurrencyUnit;Lorg/joda/money/CurrencyUnit;)V

    throw v0

    .line 408
    :cond_20
    return-object p0
.end method

.method public static of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;
    .registers 3
    .param p0, "moneyProvider"    # Lorg/joda/money/BigMoneyProvider;

    .line 258
    const-string v0, "BigMoneyProvider must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 259
    invoke-interface {p0}, Lorg/joda/money/BigMoneyProvider;->toBigMoney()Lorg/joda/money/BigMoney;

    move-result-object v1

    .line 260
    .local v1, "money":Lorg/joda/money/BigMoney;
    const-string v0, "BigMoneyProvider must not return null"

    invoke-static {v1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 261
    return-object v1
.end method

.method public static of(Lorg/joda/money/CurrencyUnit;D)Lorg/joda/money/BigMoney;
    .registers 4
    .param p0, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p1, "amount"    # D

    .line 112
    const-string v0, "Currency must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->stripTrailingZeros()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public static of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;
    .registers 5
    .param p0, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p1, "amount"    # Ljava/math/BigDecimal;

    .line 79
    const-string v0, "Currency must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    const-string v0, "Amount must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Ljava/math/BigDecimal;

    if-eq v0, v1, :cond_3c

    .line 82
    invoke-virtual {p1}, Ljava/math/BigDecimal;->unscaledValue()Ljava/math/BigInteger;

    move-result-object v2

    .line 83
    .local v2, "value":Ljava/math/BigInteger;
    if-nez v2, :cond_20

    .line 84
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Illegal BigDecimal subclass"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_20
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Ljava/math/BigInteger;

    if-eq v0, v1, :cond_32

    .line 87
    new-instance v0, Ljava/math/BigInteger;

    invoke-virtual {v2}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    move-object v2, v0

    .line 89
    :cond_32
    new-instance v0, Ljava/math/BigDecimal;

    invoke-virtual {p1}, Ljava/math/BigDecimal;->scale()I

    move-result v1

    invoke-direct {v0, v2, v1}, Ljava/math/BigDecimal;-><init>(Ljava/math/BigInteger;I)V

    move-object p1, v0

    .line 91
    .end local v2    # "value":Ljava/math/BigInteger;
    :cond_3c
    new-instance v0, Lorg/joda/money/BigMoney;

    invoke-direct {v0, p0, p1}, Lorg/joda/money/BigMoney;-><init>(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)V

    return-object v0
.end method

.method public static ofMajor(Lorg/joda/money/CurrencyUnit;J)Lorg/joda/money/BigMoney;
    .registers 4
    .param p0, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p1, "amountMajor"    # J

    .line 194
    const-string v0, "CurrencyUnit must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 195
    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public static ofMinor(Lorg/joda/money/CurrencyUnit;J)Lorg/joda/money/BigMoney;
    .registers 4
    .param p0, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p1, "amountMinor"    # J

    .line 214
    const-string v0, "CurrencyUnit must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 215
    invoke-virtual {p0}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v0

    invoke-static {p1, p2, v0}, Ljava/math/BigDecimal;->valueOf(JI)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public static ofScale(Lorg/joda/money/CurrencyUnit;JI)Lorg/joda/money/BigMoney;
    .registers 5
    .param p0, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p1, "unscaledAmount"    # J
    .param p3, "scale"    # I

    .line 174
    const-string v0, "Currency must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    invoke-static {p1, p2, p3}, Ljava/math/BigDecimal;->valueOf(JI)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public static ofScale(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;I)Lorg/joda/money/BigMoney;
    .registers 4
    .param p0, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p1, "amount"    # Ljava/math/BigDecimal;
    .param p2, "scale"    # I

    .line 132
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-static {p0, p1, p2, v0}, Lorg/joda/money/BigMoney;->ofScale(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public static ofScale(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .registers 5
    .param p0, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p1, "amount"    # Ljava/math/BigDecimal;
    .param p2, "scale"    # I
    .param p3, "roundingMode"    # Ljava/math/RoundingMode;

    .line 152
    const-string v0, "CurrencyUnit must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    const-string v0, "Amount must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    const-string v0, "RoundingMode must not be null"

    invoke-static {p3, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    invoke-virtual {p1, p2, p3}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object p1

    .line 156
    invoke-static {p0, p1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public static parse(Ljava/lang/String;)Lorg/joda/money/BigMoney;
    .registers 7
    .param p0, "moneyStr"    # Ljava/lang/String;
    .annotation runtime Lorg/joda/convert/FromString;
    .end annotation

    .line 370
    const-string v0, "Money must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 371
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x4

    if-ge v0, v1, :cond_2b

    .line 372
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Money \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' cannot be parsed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 374
    :cond_2b
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 375
    .local v3, "currStr":Ljava/lang/String;
    const/4 v4, 0x3

    .line 376
    .local v4, "amountStart":I
    :goto_32
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v4, v0, :cond_43

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x20

    if-ne v0, v1, :cond_43

    .line 377
    add-int/lit8 v4, v4, 0x1

    goto :goto_32

    .line 379
    :cond_43
    invoke-virtual {p0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 380
    .local v5, "amountStr":Ljava/lang/String;
    sget-object v0, Lorg/joda/money/BigMoney;->PARSE_REGEX:Ljava/util/regex/Pattern;

    invoke-virtual {v0, v5}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_72

    .line 381
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Money amount \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' cannot be parsed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 383
    :cond_72
    invoke-static {v3}, Lorg/joda/money/CurrencyUnit;->of(Ljava/lang/String;)Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    new-instance v1, Ljava/math/BigDecimal;

    invoke-direct {v1, v5}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .registers 4
    .param p1, "ois"    # Ljava/io/ObjectInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/InvalidObjectException;
        }
    .end annotation

    .line 432
    new-instance v0, Ljava/io/InvalidObjectException;

    const-string v1, "Serialization delegate required"

    invoke-direct {v0, v1}, Ljava/io/InvalidObjectException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static total(Ljava/lang/Iterable;)Lorg/joda/money/BigMoney;
    .registers 5
    .param p0, "monies"    # Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/Iterable<+Lorg/joda/money/BigMoneyProvider;>;)Lorg/joda/money/BigMoney;"
        }
    .end annotation

    .line 303
    const-string v0, "Money iterator must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 304
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 305
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lorg/joda/money/BigMoneyProvider;>;"
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_17

    .line 306
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Money iterator must not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 308
    :cond_17
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/joda/money/BigMoneyProvider;

    invoke-static {v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v3

    .line 309
    .local v3, "total":Lorg/joda/money/BigMoney;
    const-string v0, "Money iterator must not contain null entries"

    invoke-static {v3, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 310
    :goto_26
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_37

    .line 311
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/joda/money/BigMoneyProvider;

    invoke-virtual {v3, v0}, Lorg/joda/money/BigMoney;->plus(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v3

    goto :goto_26

    .line 313
    :cond_37
    return-object v3
.end method

.method public static total(Lorg/joda/money/CurrencyUnit;Ljava/lang/Iterable;)Lorg/joda/money/BigMoney;
    .registers 3
    .param p0, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p1, "monies"    # Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lorg/joda/money/CurrencyUnit;Ljava/lang/Iterable<+Lorg/joda/money/BigMoneyProvider;>;)Lorg/joda/money/BigMoney;"
        }
    .end annotation

    .line 347
    invoke-static {p0}, Lorg/joda/money/BigMoney;->zero(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/joda/money/BigMoney;->plus(Ljava/lang/Iterable;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public static varargs total(Lorg/joda/money/CurrencyUnit;[Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;
    .registers 4
    .param p0, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p1, "monies"    # [Lorg/joda/money/BigMoneyProvider;

    .line 330
    invoke-static {p0}, Lorg/joda/money/BigMoney;->zero(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/joda/money/BigMoney;->plus(Ljava/lang/Iterable;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public static varargs total([Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;
    .registers 5
    .param p0, "monies"    # [Lorg/joda/money/BigMoneyProvider;

    .line 278
    const-string v0, "Money array must not be null"

    invoke-static {p0, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 279
    array-length v0, p0

    if-nez v0, :cond_10

    .line 280
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Money array must not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 282
    :cond_10
    const/4 v0, 0x0

    aget-object v0, p0, v0

    invoke-static {v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v2

    .line 283
    .local v2, "total":Lorg/joda/money/BigMoney;
    const-string v0, "Money array must not contain null entries"

    invoke-static {v2, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 284
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_1d
    array-length v0, p0

    if-ge v3, v0, :cond_2d

    .line 285
    aget-object v0, p0, v3

    invoke-static {v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {v2, v0}, Lorg/joda/money/BigMoney;->plus(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v2

    .line 284
    add-int/lit8 v3, v3, 0x1

    goto :goto_1d

    .line 287
    .end local v3    # "i":I
    :cond_2d
    return-object v2
.end method

.method private with(Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;
    .registers 4
    .param p1, "newAmount"    # Ljava/math/BigDecimal;

    .line 454
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    if-ne p1, v0, :cond_5

    .line 455
    return-object p0

    .line 457
    :cond_5
    new-instance v0, Lorg/joda/money/BigMoney;

    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-direct {v0, v1, p1}, Lorg/joda/money/BigMoney;-><init>(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)V

    return-object v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .registers 3

    .line 441
    new-instance v0, Lorg/joda/money/Ser;

    const/16 v1, 0x42

    invoke-direct {v0, v1, p0}, Lorg/joda/money/Ser;-><init>(BLjava/lang/Object;)V

    return-object v0
.end method

.method public static zero(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/BigMoney;
    .registers 2
    .param p0, "currency"    # Lorg/joda/money/CurrencyUnit;

    .line 229
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-static {p0, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public static zero(Lorg/joda/money/CurrencyUnit;I)Lorg/joda/money/BigMoney;
    .registers 4
    .param p0, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p1, "scale"    # I

    .line 243
    const-wide/16 v0, 0x0

    invoke-static {v0, v1, p1}, Ljava/math/BigDecimal;->valueOf(JI)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abs()Lorg/joda/money/BigMoney;
    .registers 2

    .line 1474
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->isNegative()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->negated()Lorg/joda/money/BigMoney;

    move-result-object v0

    goto :goto_c

    :cond_b
    move-object v0, p0

    :goto_c
    return-object v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3
    .param p1, "x0"    # Ljava/lang/Object;

    .line 45
    move-object v0, p1

    check-cast v0, Lorg/joda/money/BigMoneyProvider;

    invoke-virtual {p0, v0}, Lorg/joda/money/BigMoney;->compareTo(Lorg/joda/money/BigMoneyProvider;)I

    move-result v0

    return v0
.end method

.method public compareTo(Lorg/joda/money/BigMoneyProvider;)I
    .registers 6
    .param p1, "other"    # Lorg/joda/money/BigMoneyProvider;

    .line 1617
    invoke-static {p1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v3

    .line 1618
    .local v3, "otherMoney":Lorg/joda/money/BigMoney;
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    iget-object v1, v3, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v0, v1}, Lorg/joda/money/CurrencyUnit;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1c

    .line 1619
    new-instance v0, Lorg/joda/money/CurrencyMismatchException;

    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v1

    invoke-virtual {v3}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/joda/money/CurrencyMismatchException;-><init>(Lorg/joda/money/CurrencyUnit;Lorg/joda/money/CurrencyUnit;)V

    throw v0

    .line 1621
    :cond_1c
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    iget-object v1, v3, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    return v0
.end method

.method public convertRetainScale(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .registers 6
    .param p1, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p2, "conversionMultipler"    # Ljava/math/BigDecimal;
    .param p3, "roundingMode"    # Ljava/math/RoundingMode;

    .line 1558
    invoke-virtual {p0, p1, p2}, Lorg/joda/money/BigMoney;->convertedTo(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getScale()I

    move-result v1

    invoke-virtual {v0, v1, p3}, Lorg/joda/money/BigMoney;->withScale(ILjava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public convertedTo(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;
    .registers 6
    .param p1, "currency"    # Lorg/joda/money/CurrencyUnit;
    .param p2, "conversionMultipler"    # Ljava/math/BigDecimal;

    .line 1527
    const-string v0, "CurrencyUnit must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1528
    const-string v0, "Multiplier must not be null"

    invoke-static {p2, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1529
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    if-ne v0, p1, :cond_16

    .line 1530
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot convert to the same currency"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1532
    :cond_16
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {p2, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-gez v0, :cond_26

    .line 1533
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot convert using a negative conversion multiplier"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1535
    :cond_26
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0, p2}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 1536
    .local v2, "newAmount":Ljava/math/BigDecimal;
    invoke-static {p1, v2}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public dividedBy(DLjava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .registers 7
    .param p1, "valueToDivideBy"    # D
    .param p3, "roundingMode"    # Ljava/math/RoundingMode;

    .line 1420
    const-string v0, "RoundingMode must not be null"

    invoke-static {p3, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1421
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_c

    .line 1422
    return-object p0

    .line 1424
    :cond_c
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 1425
    .local v2, "newAmount":Ljava/math/BigDecimal;
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0, v2}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public dividedBy(JLjava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .registers 7
    .param p1, "valueToDivideBy"    # J
    .param p3, "roundingMode"    # Ljava/math/RoundingMode;

    .line 1444
    const-wide/16 v0, 0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_7

    .line 1445
    return-object p0

    .line 1447
    :cond_7
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 1448
    .local v2, "newAmount":Ljava/math/BigDecimal;
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0, v2}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public dividedBy(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .registers 5
    .param p1, "valueToDivideBy"    # Ljava/math/BigDecimal;
    .param p2, "roundingMode"    # Ljava/math/RoundingMode;

    .line 1388
    const-string v0, "Divisor must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1389
    const-string v0, "RoundingMode must not be null"

    invoke-static {p2, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1390
    sget-object v0, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_13

    .line 1391
    return-object p0

    .line 1393
    :cond_13
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1, p2}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 1394
    .local v1, "newAmount":Ljava/math/BigDecimal;
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0, v1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1, "other"    # Ljava/lang/Object;

    .line 1680
    if-ne p0, p1, :cond_4

    .line 1681
    const/4 v0, 0x1

    return v0

    .line 1683
    :cond_4
    instance-of v0, p1, Lorg/joda/money/BigMoney;

    if-eqz v0, :cond_25

    .line 1684
    move-object v2, p1

    check-cast v2, Lorg/joda/money/BigMoney;

    .line 1685
    .local v2, "otherMoney":Lorg/joda/money/BigMoney;
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v2}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/joda/money/CurrencyUnit;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    iget-object v1, v2, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_23

    const/4 v0, 0x1

    goto :goto_24

    :cond_23
    const/4 v0, 0x0

    :goto_24
    return v0

    .line 1688
    .end local v2    # "otherMoney":Lorg/joda/money/BigMoney;
    :cond_25
    const/4 v0, 0x0

    return v0
.end method

.method public getAmount()Ljava/math/BigDecimal;
    .registers 2

    .line 610
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getAmountMajor()Ljava/math/BigDecimal;
    .registers 4

    .line 627
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    sget-object v1, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public getAmountMajorInt()I
    .registers 2

    .line 655
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getAmountMajor()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->intValueExact()I

    move-result v0

    return v0
.end method

.method public getAmountMajorLong()J
    .registers 3

    .line 641
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getAmountMajor()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v0

    return-wide v0
.end method

.method public getAmountMinor()Ljava/math/BigDecimal;
    .registers 4

    .line 672
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v2

    .line 673
    .local v2, "cdp":I
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    sget-object v1, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v0, v2, v1}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->movePointRight(I)Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public getAmountMinorInt()I
    .registers 2

    .line 701
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getAmountMinor()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->intValueExact()I

    move-result v0

    return v0
.end method

.method public getAmountMinorLong()J
    .registers 3

    .line 687
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getAmountMinor()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValueExact()J

    move-result-wide v0

    return-wide v0
.end method

.method public getCurrencyUnit()Lorg/joda/money/CurrencyUnit;
    .registers 2

    .line 467
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    return-object v0
.end method

.method public getMinorPart()I
    .registers 4

    .line 717
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v0

    invoke-virtual {v0}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v2

    .line 718
    .local v2, "cdp":I
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    sget-object v1, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v0, v2, v1}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->remainder(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->movePointRight(I)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->intValueExact()I

    move-result v0

    return v0
.end method

.method public getScale()I
    .registers 2

    .line 504
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->scale()I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .registers 3

    .line 1698
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v0}, Lorg/joda/money/CurrencyUnit;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v1}, Ljava/math/BigDecimal;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public isCurrencyScale()Z
    .registers 3

    .line 516
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->scale()I

    move-result v0

    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v1}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v1

    if-ne v0, v1, :cond_10

    const/4 v0, 0x1

    goto :goto_11

    :cond_10
    const/4 v0, 0x0

    :goto_11
    return v0
.end method

.method public isEqual(Lorg/joda/money/BigMoneyProvider;)Z
    .registers 3
    .param p1, "other"    # Lorg/joda/money/BigMoneyProvider;

    .line 1638
    invoke-virtual {p0, p1}, Lorg/joda/money/BigMoney;->compareTo(Lorg/joda/money/BigMoneyProvider;)I

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    goto :goto_9

    :cond_8
    const/4 v0, 0x0

    :goto_9
    return v0
.end method

.method public isGreaterThan(Lorg/joda/money/BigMoneyProvider;)Z
    .registers 3
    .param p1, "other"    # Lorg/joda/money/BigMoneyProvider;

    .line 1650
    invoke-virtual {p0, p1}, Lorg/joda/money/BigMoney;->compareTo(Lorg/joda/money/BigMoneyProvider;)I

    move-result v0

    if-lez v0, :cond_8

    const/4 v0, 0x1

    goto :goto_9

    :cond_8
    const/4 v0, 0x0

    :goto_9
    return v0
.end method

.method public isLessThan(Lorg/joda/money/BigMoneyProvider;)Z
    .registers 3
    .param p1, "other"    # Lorg/joda/money/BigMoneyProvider;

    .line 1662
    invoke-virtual {p0, p1}, Lorg/joda/money/BigMoney;->compareTo(Lorg/joda/money/BigMoneyProvider;)I

    move-result v0

    if-gez v0, :cond_8

    const/4 v0, 0x1

    goto :goto_9

    :cond_8
    const/4 v0, 0x0

    :goto_9
    return v0
.end method

.method public isNegative()Z
    .registers 3

    .line 757
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-gez v0, :cond_c

    const/4 v0, 0x1

    goto :goto_d

    :cond_c
    const/4 v0, 0x0

    :goto_d
    return v0
.end method

.method public isNegativeOrZero()Z
    .registers 3

    .line 766
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-gtz v0, :cond_c

    const/4 v0, 0x1

    goto :goto_d

    :cond_c
    const/4 v0, 0x0

    :goto_d
    return v0
.end method

.method public isPositive()Z
    .registers 3

    .line 739
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-lez v0, :cond_c

    const/4 v0, 0x1

    goto :goto_d

    :cond_c
    const/4 v0, 0x0

    :goto_d
    return v0
.end method

.method public isPositiveOrZero()Z
    .registers 3

    .line 748
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-ltz v0, :cond_c

    const/4 v0, 0x1

    goto :goto_d

    :cond_c
    const/4 v0, 0x0

    :goto_d
    return v0
.end method

.method public isSameCurrency(Lorg/joda/money/BigMoneyProvider;)Z
    .registers 4
    .param p1, "money"    # Lorg/joda/money/BigMoneyProvider;

    .line 1603
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {p1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v1

    invoke-virtual {v1}, Lorg/joda/money/BigMoney;->getCurrencyUnit()Lorg/joda/money/CurrencyUnit;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/joda/money/CurrencyUnit;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isZero()Z
    .registers 3

    .line 730
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    goto :goto_d

    :cond_c
    const/4 v0, 0x0

    :goto_d
    return v0
.end method

.method public minus(D)Lorg/joda/money/BigMoney;
    .registers 6
    .param p1, "amountToSubtract"    # D

    .line 1127
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_7

    .line 1128
    return-object p0

    .line 1130
    :cond_7
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 1131
    .local v2, "newAmount":Ljava/math/BigDecimal;
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0, v2}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public minus(Ljava/lang/Iterable;)Lorg/joda/money/BigMoney;
    .registers 7
    .param p1, "moniesToSubtract"    # Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/Iterable<+Lorg/joda/money/BigMoneyProvider;>;)Lorg/joda/money/BigMoney;"
        }
    .end annotation

    .line 1053
    iget-object v1, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    .line 1054
    .local v1, "total":Ljava/math/BigDecimal;
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lorg/joda/money/BigMoneyProvider;

    .line 1055
    .local v3, "moneyProvider":Lorg/joda/money/BigMoneyProvider;
    invoke-direct {p0, v3}, Lorg/joda/money/BigMoney;->checkCurrencyEqual(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v4

    .line 1056
    .local v4, "money":Lorg/joda/money/BigMoney;
    iget-object v0, v4, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v1, v0}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 1057
    .end local v3    # "moneyProvider":Lorg/joda/money/BigMoneyProvider;
    .end local v4    # "money":Lorg/joda/money/BigMoney;
    goto :goto_6

    .line 1058
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1e
    invoke-direct {p0, v1}, Lorg/joda/money/BigMoney;->with(Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public minus(Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;
    .registers 4
    .param p1, "amountToSubtract"    # Ljava/math/BigDecimal;

    .line 1098
    const-string v0, "Amount must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1099
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_e

    .line 1100
    return-object p0

    .line 1102
    :cond_e
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 1103
    .local v1, "newAmount":Ljava/math/BigDecimal;
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0, v1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public minus(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;
    .registers 4
    .param p1, "moneyToSubtract"    # Lorg/joda/money/BigMoneyProvider;

    .line 1079
    invoke-direct {p0, p1}, Lorg/joda/money/BigMoney;->checkCurrencyEqual(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v1

    .line 1080
    .local v1, "toSubtract":Lorg/joda/money/BigMoney;
    invoke-virtual {v1}, Lorg/joda/money/BigMoney;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/joda/money/BigMoney;->minus(Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public minusMajor(J)Lorg/joda/money/BigMoney;
    .registers 6
    .param p1, "amountToSubtract"    # J

    .line 1150
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_7

    .line 1151
    return-object p0

    .line 1153
    :cond_7
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 1154
    .local v2, "newAmount":Ljava/math/BigDecimal;
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0, v2}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public minusMinor(J)Lorg/joda/money/BigMoney;
    .registers 6
    .param p1, "amountToSubtract"    # J

    .line 1173
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_7

    .line 1174
    return-object p0

    .line 1176
    :cond_7
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v1}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v1

    invoke-static {p1, p2, v1}, Ljava/math/BigDecimal;->valueOf(JI)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 1177
    .local v2, "newAmount":Ljava/math/BigDecimal;
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0, v2}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public minusRetainScale(DLjava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .registers 7
    .param p1, "amountToSubtract"    # D
    .param p3, "roundingMode"    # Ljava/math/RoundingMode;

    .line 1242
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_7

    .line 1243
    return-object p0

    .line 1245
    :cond_7
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 1246
    .local v2, "newAmount":Ljava/math/BigDecimal;
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getScale()I

    move-result v0

    invoke-virtual {v2, v0, p3}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 1247
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0, v2}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public minusRetainScale(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .registers 5
    .param p1, "amountToSubtract"    # Ljava/math/BigDecimal;
    .param p2, "roundingMode"    # Ljava/math/RoundingMode;

    .line 1213
    const-string v0, "Amount must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1214
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_e

    .line 1215
    return-object p0

    .line 1217
    :cond_e
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 1218
    .local v1, "newAmount":Ljava/math/BigDecimal;
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getScale()I

    move-result v0

    invoke-virtual {v1, v0, p2}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 1219
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0, v1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public minusRetainScale(Lorg/joda/money/BigMoneyProvider;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .registers 5
    .param p1, "moneyToSubtract"    # Lorg/joda/money/BigMoneyProvider;
    .param p2, "roundingMode"    # Ljava/math/RoundingMode;

    .line 1195
    invoke-direct {p0, p1}, Lorg/joda/money/BigMoney;->checkCurrencyEqual(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v1

    .line 1196
    .local v1, "toSubtract":Lorg/joda/money/BigMoney;
    invoke-virtual {v1}, Lorg/joda/money/BigMoney;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lorg/joda/money/BigMoney;->minusRetainScale(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public multipliedBy(D)Lorg/joda/money/BigMoney;
    .registers 6
    .param p1, "valueToMultiplyBy"    # D

    .line 1291
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_7

    .line 1292
    return-object p0

    .line 1294
    :cond_7
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 1295
    .local v2, "newAmount":Ljava/math/BigDecimal;
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0, v2}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public multipliedBy(J)Lorg/joda/money/BigMoney;
    .registers 6
    .param p1, "valueToMultiplyBy"    # J

    .line 1311
    const-wide/16 v0, 0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_7

    .line 1312
    return-object p0

    .line 1314
    :cond_7
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 1315
    .local v2, "newAmount":Ljava/math/BigDecimal;
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0, v2}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public multipliedBy(Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;
    .registers 4
    .param p1, "valueToMultiplyBy"    # Ljava/math/BigDecimal;

    .line 1264
    const-string v0, "Multiplier must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1265
    sget-object v0, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_e

    .line 1266
    return-object p0

    .line 1268
    :cond_e
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 1269
    .local v1, "newAmount":Ljava/math/BigDecimal;
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0, v1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public multiplyRetainScale(DLjava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .registers 5
    .param p1, "valueToMultiplyBy"    # D
    .param p3, "roundingMode"    # Ljava/math/RoundingMode;

    .line 1367
    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Lorg/joda/money/BigMoney;->multiplyRetainScale(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public multiplyRetainScale(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .registers 5
    .param p1, "valueToMultiplyBy"    # Ljava/math/BigDecimal;
    .param p2, "roundingMode"    # Ljava/math/RoundingMode;

    .line 1335
    const-string v0, "Multiplier must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1336
    const-string v0, "RoundingMode must not be null"

    invoke-static {p2, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1337
    sget-object v0, Ljava/math/BigDecimal;->ONE:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_13

    .line 1338
    return-object p0

    .line 1340
    :cond_13
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 1341
    .local v1, "newAmount":Ljava/math/BigDecimal;
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getScale()I

    move-result v0

    invoke-virtual {v1, v0, p2}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 1342
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0, v1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public negated()Lorg/joda/money/BigMoney;
    .registers 3

    .line 1460
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->isZero()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1461
    return-object p0

    .line 1463
    :cond_7
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    iget-object v1, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v1}, Ljava/math/BigDecimal;->negate()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public plus(D)Lorg/joda/money/BigMoney;
    .registers 6
    .param p1, "amountToAdd"    # D

    .line 915
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_7

    .line 916
    return-object p0

    .line 918
    :cond_7
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 919
    .local v2, "newAmount":Ljava/math/BigDecimal;
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0, v2}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public plus(Ljava/lang/Iterable;)Lorg/joda/money/BigMoney;
    .registers 7
    .param p1, "moniesToAdd"    # Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/lang/Iterable<+Lorg/joda/money/BigMoneyProvider;>;)Lorg/joda/money/BigMoney;"
        }
    .end annotation

    .line 841
    iget-object v1, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    .line 842
    .local v1, "total":Ljava/math/BigDecimal;
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lorg/joda/money/BigMoneyProvider;

    .line 843
    .local v3, "moneyProvider":Lorg/joda/money/BigMoneyProvider;
    invoke-direct {p0, v3}, Lorg/joda/money/BigMoney;->checkCurrencyEqual(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v4

    .line 844
    .local v4, "money":Lorg/joda/money/BigMoney;
    iget-object v0, v4, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v1, v0}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 845
    .end local v3    # "moneyProvider":Lorg/joda/money/BigMoneyProvider;
    .end local v4    # "money":Lorg/joda/money/BigMoney;
    goto :goto_6

    .line 846
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1e
    invoke-direct {p0, v1}, Lorg/joda/money/BigMoney;->with(Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public plus(Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;
    .registers 4
    .param p1, "amountToAdd"    # Ljava/math/BigDecimal;

    .line 886
    const-string v0, "Amount must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 887
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_e

    .line 888
    return-object p0

    .line 890
    :cond_e
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 891
    .local v1, "newAmount":Ljava/math/BigDecimal;
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0, v1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public plus(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;
    .registers 4
    .param p1, "moneyToAdd"    # Lorg/joda/money/BigMoneyProvider;

    .line 867
    invoke-direct {p0, p1}, Lorg/joda/money/BigMoney;->checkCurrencyEqual(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v1

    .line 868
    .local v1, "toAdd":Lorg/joda/money/BigMoney;
    invoke-virtual {v1}, Lorg/joda/money/BigMoney;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/joda/money/BigMoney;->plus(Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public plusMajor(J)Lorg/joda/money/BigMoney;
    .registers 6
    .param p1, "amountToAdd"    # J

    .line 938
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_7

    .line 939
    return-object p0

    .line 941
    :cond_7
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 942
    .local v2, "newAmount":Ljava/math/BigDecimal;
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0, v2}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public plusMinor(J)Lorg/joda/money/BigMoney;
    .registers 6
    .param p1, "amountToAdd"    # J

    .line 961
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_7

    .line 962
    return-object p0

    .line 964
    :cond_7
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v1}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v1

    invoke-static {p1, p2, v1}, Ljava/math/BigDecimal;->valueOf(JI)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 965
    .local v2, "newAmount":Ljava/math/BigDecimal;
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0, v2}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public plusRetainScale(DLjava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .registers 7
    .param p1, "amountToAdd"    # D
    .param p3, "roundingMode"    # Ljava/math/RoundingMode;

    .line 1030
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-nez v0, :cond_7

    .line 1031
    return-object p0

    .line 1033
    :cond_7
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 1034
    .local v2, "newAmount":Ljava/math/BigDecimal;
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getScale()I

    move-result v0

    invoke-virtual {v2, v0, p3}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 1035
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0, v2}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public plusRetainScale(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .registers 5
    .param p1, "amountToAdd"    # Ljava/math/BigDecimal;
    .param p2, "roundingMode"    # Ljava/math/RoundingMode;

    .line 1001
    const-string v0, "Amount must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1002
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_e

    .line 1003
    return-object p0

    .line 1005
    :cond_e
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 1006
    .local v1, "newAmount":Ljava/math/BigDecimal;
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getScale()I

    move-result v0

    invoke-virtual {v1, v0, p2}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 1007
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0, v1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public plusRetainScale(Lorg/joda/money/BigMoneyProvider;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .registers 5
    .param p1, "moneyToAdd"    # Lorg/joda/money/BigMoneyProvider;
    .param p2, "roundingMode"    # Ljava/math/RoundingMode;

    .line 983
    invoke-direct {p0, p1}, Lorg/joda/money/BigMoney;->checkCurrencyEqual(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/BigMoney;

    move-result-object v1

    .line 984
    .local v1, "toAdd":Lorg/joda/money/BigMoney;
    invoke-virtual {v1}, Lorg/joda/money/BigMoney;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lorg/joda/money/BigMoney;->plusRetainScale(Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public rounded(ILjava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .registers 6
    .param p1, "scale"    # I
    .param p2, "roundingMode"    # Ljava/math/RoundingMode;

    .line 1500
    const-string v0, "RoundingMode must not be null"

    invoke-static {p2, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1501
    invoke-virtual {p0}, Lorg/joda/money/BigMoney;->getScale()I

    move-result v0

    if-lt p1, v0, :cond_c

    .line 1502
    return-object p0

    .line 1504
    :cond_c
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->scale()I

    move-result v1

    .line 1505
    .local v1, "currentScale":I
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1, p2}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v2

    .line 1506
    .local v2, "newAmount":Ljava/math/BigDecimal;
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0, v2}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public toBigMoney()Lorg/joda/money/BigMoney;
    .registers 1

    .line 1570
    return-object p0
.end method

.method public toMoney()Lorg/joda/money/Money;
    .registers 2

    .line 1581
    invoke-static {p0}, Lorg/joda/money/Money;->of(Lorg/joda/money/BigMoneyProvider;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public toMoney(Ljava/math/RoundingMode;)Lorg/joda/money/Money;
    .registers 3
    .param p1, "roundingMode"    # Ljava/math/RoundingMode;

    .line 1592
    invoke-static {p0, p1}, Lorg/joda/money/Money;->of(Lorg/joda/money/BigMoneyProvider;Ljava/math/RoundingMode;)Lorg/joda/money/Money;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3
    .annotation runtime Lorg/joda/convert/ToString;
    .end annotation

    .line 1713
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v1}, Lorg/joda/money/CurrencyUnit;->getCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v1}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public withAmount(D)Lorg/joda/money/BigMoney;
    .registers 4
    .param p1, "amount"    # D

    .line 808
    invoke-static {p1, p2}, Ljava/math/BigDecimal;->valueOf(D)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/joda/money/BigMoney;->withAmount(Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public withAmount(Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;
    .registers 3
    .param p1, "amount"    # Ljava/math/BigDecimal;

    .line 782
    const-string v0, "Amount must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 783
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0, p1}, Ljava/math/BigDecimal;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 784
    return-object p0

    .line 786
    :cond_e
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-static {v0, p1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public withCurrencyScale()Lorg/joda/money/BigMoney;
    .registers 3

    .line 580
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v0}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v0

    sget-object v1, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-virtual {p0, v0, v1}, Lorg/joda/money/BigMoney;->withScale(ILjava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public withCurrencyScale(Ljava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .registers 3
    .param p1, "roundingMode"    # Ljava/math/RoundingMode;

    .line 597
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    invoke-virtual {v0}, Lorg/joda/money/CurrencyUnit;->getDecimalPlaces()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lorg/joda/money/BigMoney;->withScale(ILjava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public withCurrencyUnit(Lorg/joda/money/CurrencyUnit;)Lorg/joda/money/BigMoney;
    .registers 4
    .param p1, "currency"    # Lorg/joda/money/CurrencyUnit;

    .line 483
    const-string v0, "CurrencyUnit must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 484
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    if-ne v0, p1, :cond_a

    .line 485
    return-object p0

    .line 487
    :cond_a
    new-instance v0, Lorg/joda/money/BigMoney;

    iget-object v1, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-direct {v0, p1, v1}, Lorg/joda/money/BigMoney;-><init>(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)V

    return-object v0
.end method

.method public withScale(I)Lorg/joda/money/BigMoney;
    .registers 3
    .param p1, "scale"    # I

    .line 537
    sget-object v0, Ljava/math/RoundingMode;->UNNECESSARY:Ljava/math/RoundingMode;

    invoke-virtual {p0, p1, v0}, Lorg/joda/money/BigMoney;->withScale(ILjava/math/RoundingMode;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

.method public withScale(ILjava/math/RoundingMode;)Lorg/joda/money/BigMoney;
    .registers 5
    .param p1, "scale"    # I
    .param p2, "roundingMode"    # Ljava/math/RoundingMode;

    .line 557
    const-string v0, "RoundingMode must not be null"

    invoke-static {p2, v0}, Lorg/joda/money/MoneyUtils;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 558
    iget-object v0, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->scale()I

    move-result v0

    if-ne p1, v0, :cond_e

    .line 559
    return-object p0

    .line 561
    :cond_e
    iget-object v0, p0, Lorg/joda/money/BigMoney;->currency:Lorg/joda/money/CurrencyUnit;

    iget-object v1, p0, Lorg/joda/money/BigMoney;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v1, p1, p2}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/joda/money/BigMoney;->of(Lorg/joda/money/CurrencyUnit;Ljava/math/BigDecimal;)Lorg/joda/money/BigMoney;

    move-result-object v0

    return-object v0
.end method

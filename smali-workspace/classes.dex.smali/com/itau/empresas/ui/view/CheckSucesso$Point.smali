.class Lcom/itau/empresas/ui/view/CheckSucesso$Point;
.super Ljava/lang/Object;
.source "CheckSucesso.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/view/CheckSucesso;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Point"
.end annotation


# instance fields
.field private x:F

.field private y:F


# direct methods
.method constructor <init>()V
    .registers 2

    .line 412
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 413
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->x:F

    .line 414
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F

    .line 415
    return-void
.end method

.method constructor <init>(FF)V
    .registers 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .line 407
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 408
    iput p1, p0, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->x:F

    .line 409
    iput p2, p0, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F

    .line 410
    return-void
.end method

.method static synthetic access$100(Lcom/itau/empresas/ui/view/CheckSucesso$Point;)F
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    .line 403
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->x:F

    return v0
.end method

.method static synthetic access$102(Lcom/itau/empresas/ui/view/CheckSucesso$Point;F)F
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/CheckSucesso$Point;
    .param p1, "x1"    # F

    .line 403
    iput p1, p0, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->x:F

    return p1
.end method

.method static synthetic access$200(Lcom/itau/empresas/ui/view/CheckSucesso$Point;)F
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/CheckSucesso$Point;

    .line 403
    iget v0, p0, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F

    return v0
.end method

.method static synthetic access$202(Lcom/itau/empresas/ui/view/CheckSucesso$Point;F)F
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/CheckSucesso$Point;
    .param p1, "x1"    # F

    .line 403
    iput p1, p0, Lcom/itau/empresas/ui/view/CheckSucesso$Point;->y:F

    return p1
.end method

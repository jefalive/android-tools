.class public Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "AtendimentoAssuntoActivity.java"


# instance fields
.field controller:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

.field private duvidasPorAssuntoExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

.field listaDuvidasPorAssunto:Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;

.field private listaItensDuvida:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
        }
    .end annotation
.end field

.field root:Landroid/widget/LinearLayout;

.field toolBar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 36
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;Landroid/view/View;II)V
    .registers 4
    .param p0, "x0"    # Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .line 36
    invoke-direct {p0, p1, p2, p3}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->clickItemAssunto(Landroid/view/View;II)V

    return-void
.end method

.method private buscaProdutos(Ljava/util/List;)V
    .registers 6
    .param p1, "produtos"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/atendimento/model/AtendimentoDuvida;>;)V"
        }
    .end annotation

    .line 66
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->preparaListDuvidasPorAssunto(Ljava/util/List;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->listaItensDuvida:Ljava/util/HashMap;

    .line 67
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 68
    .local v3, "listaTituloAssunto":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->listaItensDuvida:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 69
    new-instance v0, Lcom/itau/empresas/adapter/PadraoExpandableListAdapter;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->listaItensDuvida:Ljava/util/HashMap;

    invoke-direct {v0, v1, v3, v2}, Lcom/itau/empresas/adapter/PadraoExpandableListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/util/HashMap;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->duvidasPorAssuntoExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    .line 71
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->configuraListaAssuntos()V

    .line 72
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->escondeDialogoDeProgresso()V

    .line 73
    return-void
.end method

.method private clickItemAssunto(Landroid/view/View;II)V
    .registers 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "groupPosition"    # I
    .param p3, "childPosition"    # I

    .line 110
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->duvidasPorAssuntoExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    .line 111
    invoke-interface {v0, p2, p3}, Landroid/widget/ExpandableListAdapter;->getChild(II)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/String;

    .line 114
    .local v4, "selected":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 115
    const v2, 0x7f0702db

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 116
    const v3, 0x7f0702a4

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 119
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_$IntentBuilder_;

    move-result-object v0

    .line 120
    const v1, 0x7f070709

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_$IntentBuilder_;->extra(Ljava/lang/String;Ljava/lang/String;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_$IntentBuilder_;

    .line 121
    invoke-virtual {v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoDuvidaActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 122
    return-void
.end method

.method private configuraListaAssuntos()V
    .registers 6

    .line 77
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->listaDuvidasPorAssunto:Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;

    .line 78
    const v2, 0x7f030153

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/view/ViewGroup;

    .line 80
    .local v4, "header":Landroid/view/ViewGroup;
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->listaDuvidasPorAssunto:Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v4, v1, v2}, Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->listaDuvidasPorAssunto:Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->duvidasPorAssuntoExpandableListAdapter:Landroid/widget/ExpandableListAdapter;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->listaDuvidasPorAssunto:Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;

    new-instance v1, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity$1;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;->setOnChildClickListener(Landroid/widget/ExpandableListView$OnChildClickListener;)V

    .line 96
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->listaDuvidasPorAssunto:Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;

    new-instance v1, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity$2;-><init>(Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;)V

    .line 97
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollExpandableListView;->setOnGroupExpandListener(Landroid/widget/ExpandableListView$OnGroupExpandListener;)V

    .line 107
    return-void
.end method

.method private constroiToolbar()V
    .registers 3

    .line 55
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    invoke-static {v0, p0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;->Builder(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 56
    const v1, 0x7f070704

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    .line 59
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 164
    new-instance v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private preparaListDuvidasPorAssunto(Ljava/util/List;)Ljava/util/HashMap;
    .registers 7
    .param p1, "produtos"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/atendimento/model/AtendimentoDuvida;>;)Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
        }
    .end annotation

    .line 155
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->listaItensDuvida:Ljava/util/HashMap;

    .line 157
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/itau/empresas/feature/atendimento/model/AtendimentoDuvida;

    .line 158
    .local v4, "produto":Lcom/itau/empresas/feature/atendimento/model/AtendimentoDuvida;
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->listaItensDuvida:Ljava/util/HashMap;

    invoke-virtual {v4}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoDuvida;->getProduto()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoDuvida;->getAssunto()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    .end local v4    # "produto":Lcom/itau/empresas/feature/atendimento/model/AtendimentoDuvida;
    goto :goto_b

    .line 160
    :cond_26
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->listaItensDuvida:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method carregaElementosDeTela()V
    .registers 2

    .line 50
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->constroiToolbar()V

    .line 51
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ProdutosFaqsUtil;->carregaProdutos(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->buscaProdutos(Ljava/util/List;)V

    .line 52
    return-void
.end method

.method public hitAnalytics()Ljava/lang/String;
    .registers 2

    .line 145
    const v0, 0x7f070704

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;

    .line 125
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 126
    return-void

    .line 128
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->escondeDialogoDeProgresso()V

    .line 129
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->root:Landroid/widget/LinearLayout;

    const v1, 0x7f0701ef

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 130
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 131
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 135
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 136
    return-void

    .line 138
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->escondeDialogoDeProgresso()V

    .line 139
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 140
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 141
    return-void
.end method

.method public onEventMainThread(Ljava/util/List;)V
    .registers 2
    .param p1, "produtos"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/atendimento/model/AtendimentoDuvida;>;)V"
        }
    .end annotation

    .line 62
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->buscaProdutos(Ljava/util/List;)V

    .line 63
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 150
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

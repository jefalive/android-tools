.class Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;
.super Ljava/lang/Object;
.source "BottomNavigationViewWrapper.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->changeBackgroundDrawable(Landroid/support/design/internal/BottomNavigationItemView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;

.field final synthetic val$icon:Landroid/widget/ImageView;

.field final synthetic val$item:Landroid/support/design/internal/BottomNavigationItemView;

.field final synthetic val$largeLabel:Landroid/view/View;

.field final synthetic val$smallLabel:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;Landroid/support/design/internal/BottomNavigationItemView;Landroid/widget/ImageView;Landroid/view/View;Landroid/view/View;)V
    .registers 6
    .param p1, "this$0"    # Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;

    .line 78
    iput-object p1, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;->this$0:Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;

    iput-object p2, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;->val$item:Landroid/support/design/internal/BottomNavigationItemView;

    iput-object p3, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;->val$icon:Landroid/widget/ImageView;

    iput-object p4, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;->val$smallLabel:Landroid/view/View;

    iput-object p5, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;->val$largeLabel:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .registers 5
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;->this$0:Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;

    # getter for: Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->lastMenuItem:Landroid/support/design/internal/BottomNavigationItemView;
    invoke-static {v0}, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->access$000(Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;)Landroid/support/design/internal/BottomNavigationItemView;

    move-result-object v0

    if-eqz v0, :cond_22

    iget-object v0, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;->val$item:Landroid/support/design/internal/BottomNavigationItemView;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;->this$0:Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;

    # getter for: Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->lastMenuItem:Landroid/support/design/internal/BottomNavigationItemView;
    invoke-static {v1}, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->access$000(Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;)Landroid/support/design/internal/BottomNavigationItemView;

    move-result-object v1

    if-eq v0, v1, :cond_22

    .line 82
    iget-object v0, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;->this$0:Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;->val$icon:Landroid/widget/ImageView;

    # invokes: Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->descentralizaItem(Landroid/widget/ImageView;)V
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->access$100(Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;Landroid/widget/ImageView;)V

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;->this$0:Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;->val$smallLabel:Landroid/view/View;

    iget-object v2, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;->val$largeLabel:Landroid/view/View;

    # invokes: Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->mostraTitulo(Landroid/view/View;Landroid/view/View;)V
    invoke-static {v0, v1, v2}, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->access$200(Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;Landroid/view/View;Landroid/view/View;)V

    .line 86
    :cond_22
    invoke-interface {p1}, Landroid/view/MenuItem;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;->this$0:Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;->val$item:Landroid/support/design/internal/BottomNavigationItemView;

    # setter for: Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->lastMenuItem:Landroid/support/design/internal/BottomNavigationItemView;
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->access$002(Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;Landroid/support/design/internal/BottomNavigationItemView;)Landroid/support/design/internal/BottomNavigationItemView;

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;->this$0:Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;->val$icon:Landroid/widget/ImageView;

    # invokes: Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->centralizaItem(Landroid/widget/ImageView;)V
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->access$300(Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;Landroid/widget/ImageView;)V

    .line 89
    iget-object v0, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;->this$0:Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;

    iget-object v1, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;->val$smallLabel:Landroid/view/View;

    iget-object v2, p0, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper$1;->val$largeLabel:Landroid/view/View;

    # invokes: Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->escondeTitulo(Landroid/view/View;Landroid/view/View;)V
    invoke-static {v0, v1, v2}, Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;->access$400(Lcom/itau/empresas/feature/home/BottomNavigationViewWrapper;Landroid/view/View;Landroid/view/View;)V

    .line 92
    :cond_3f
    const/4 v0, 0x0

    return v0
.end method

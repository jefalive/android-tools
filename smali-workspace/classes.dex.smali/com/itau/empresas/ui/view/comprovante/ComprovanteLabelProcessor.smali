.class Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelProcessor;
.super Ljava/lang/Object;
.source "ComprovanteLabelProcessor.java"


# direct methods
.method public static parse(Ljava/util/List;)Ljava/util/List;
    .registers 8
    .param p0, "itens"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;>;)Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalAccessException;
        }
    .end annotation

    .line 15
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 17
    .local v4, "list":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_9
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_47

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;

    .line 19
    .local v6, "item":Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;
    instance-of v0, v6, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperObservationImpl;

    if-eqz v0, :cond_32

    .line 21
    new-instance v0, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteObservacaoView;

    invoke-interface {v6}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;->getLabel()Ljava/lang/String;

    move-result-object v2

    move-object v3, v6

    check-cast v3, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperObservationImpl;

    .line 22
    invoke-virtual {v3}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperObservationImpl;->getObservation()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteObservacaoView;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_46

    .line 24
    :cond_32
    instance-of v0, v6, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapperImpl;

    if-eqz v0, :cond_46

    .line 26
    new-instance v0, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;

    invoke-interface {v6}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v6}, Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;->getLabel()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    .end local v6    # "item":Lcom/itau/empresas/ui/view/comprovante/ComprovanteLabelWrapper;
    :cond_46
    :goto_46
    goto :goto_9

    .line 30
    :cond_47
    return-object v4
.end method

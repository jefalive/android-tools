.class public Lcom/itau/empresas/api/model/DesbloqueiaSenhaEletronicaVO;
.super Ljava/lang/Object;
.source "DesbloqueiaSenhaEletronicaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field cnpj:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cnpj"
    .end annotation
.end field

.field novaSenha:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nova_senha"
    .end annotation
.end field

.field novaSenhaRepetida:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nova_senha_repetida"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

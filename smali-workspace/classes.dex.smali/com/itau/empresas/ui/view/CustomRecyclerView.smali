.class public Lcom/itau/empresas/ui/view/CustomRecyclerView;
.super Landroid/support/v7/widget/RecyclerView;
.source "CustomRecyclerView.java"


# instance fields
.field private isLoaded:Z

.field private mScrollable:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 17
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/itau/empresas/ui/view/CustomRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 21
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/itau/empresas/ui/view/CustomRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .line 25
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/CustomRecyclerView;->isLoaded:Z

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/view/CustomRecyclerView;->mScrollable:Z

    .line 27
    return-void
.end method

.method static synthetic access$002(Lcom/itau/empresas/ui/view/CustomRecyclerView;Z)Z
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/CustomRecyclerView;
    .param p1, "x1"    # Z

    .line 11
    iput-boolean p1, p0, Lcom/itau/empresas/ui/view/CustomRecyclerView;->mScrollable:Z

    return p1
.end method

.method static synthetic access$102(Lcom/itau/empresas/ui/view/CustomRecyclerView;Z)Z
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/view/CustomRecyclerView;
    .param p1, "x1"    # Z

    .line 11
    iput-boolean p1, p0, Lcom/itau/empresas/ui/view/CustomRecyclerView;->isLoaded:Z

    return p1
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .line 31
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/CustomRecyclerView;->mScrollable:Z

    if-eqz v0, :cond_a

    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_a
    const/4 v0, 0x1

    goto :goto_d

    :cond_c
    const/4 v0, 0x0

    :goto_d
    return v0
.end method

.method protected onLayout(ZIIII)V
    .registers 11
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .line 40
    invoke-super/range {p0 .. p5}, Landroid/support/v7/widget/RecyclerView;->onLayout(ZIIII)V

    .line 42
    iget-boolean v0, p0, Lcom/itau/empresas/ui/view/CustomRecyclerView;->isLoaded:Z

    if-nez v0, :cond_28

    .line 43
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_8
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CustomRecyclerView;->getChildCount()I

    move-result v0

    if-ge v4, v0, :cond_28

    .line 45
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CustomRecyclerView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v4, v0, :cond_25

    .line 46
    invoke-virtual {p0}, Lcom/itau/empresas/ui/view/CustomRecyclerView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/view/CustomRecyclerView$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/view/CustomRecyclerView$1;-><init>(Lcom/itau/empresas/ui/view/CustomRecyclerView;)V

    mul-int/lit16 v2, v4, 0xc8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 43
    :cond_25
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    .line 57
    .end local v4    # "i":I
    :cond_28
    return-void
.end method

.method public setLoaded(Z)V
    .registers 2
    .param p1, "loaded"    # Z

    .line 35
    iput-boolean p1, p0, Lcom/itau/empresas/ui/view/CustomRecyclerView;->isLoaded:Z

    .line 36
    return-void
.end method

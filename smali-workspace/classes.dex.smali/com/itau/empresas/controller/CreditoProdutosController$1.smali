.class Lcom/itau/empresas/controller/CreditoProdutosController$1;
.super Ljava/lang/Object;
.source "CreditoProdutosController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/controller/CreditoProdutosController;->buscaLimiteProduto(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/controller/CreditoProdutosController;

.field final synthetic val$cpf:Ljava/lang/String;

.field final synthetic val$familia:Ljava/lang/String;

.field final synthetic val$nomeProduto:Ljava/lang/String;

.field final synthetic val$tipoOperacao:Ljava/lang/String;

.field final synthetic val$tipoPessoa:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/controller/CreditoProdutosController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .param p1, "this$0"    # Lcom/itau/empresas/controller/CreditoProdutosController;

    .line 23
    iput-object p1, p0, Lcom/itau/empresas/controller/CreditoProdutosController$1;->this$0:Lcom/itau/empresas/controller/CreditoProdutosController;

    iput-object p2, p0, Lcom/itau/empresas/controller/CreditoProdutosController$1;->val$tipoPessoa:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/controller/CreditoProdutosController$1;->val$cpf:Ljava/lang/String;

    iput-object p4, p0, Lcom/itau/empresas/controller/CreditoProdutosController$1;->val$familia:Ljava/lang/String;

    iput-object p5, p0, Lcom/itau/empresas/controller/CreditoProdutosController$1;->val$nomeProduto:Ljava/lang/String;

    iput-object p6, p0, Lcom/itau/empresas/controller/CreditoProdutosController$1;->val$tipoOperacao:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 12

    .line 27
    iget-object v0, p0, Lcom/itau/empresas/controller/CreditoProdutosController$1;->this$0:Lcom/itau/empresas/controller/CreditoProdutosController;

    iget-object v0, v0, Lcom/itau/empresas/controller/CreditoProdutosController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v9

    .line 28
    .local v9, "contaSelecionada":Lcom/itau/empresas/api/model/ContaOperadorVO;
    if-nez v9, :cond_b

    .line 29
    return-void

    .line 31
    :cond_b
    new-instance v10, Lcom/itau/empresas/Evento$EventoLisConsulta;

    invoke-direct {v10}, Lcom/itau/empresas/Evento$EventoLisConsulta;-><init>()V

    .line 32
    .local v10, "evento":Lcom/itau/empresas/Evento$EventoLisConsulta;
    iget-object v0, p0, Lcom/itau/empresas/controller/CreditoProdutosController$1;->this$0:Lcom/itau/empresas/controller/CreditoProdutosController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/CreditoProdutosController;->api()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    invoke-virtual {v9}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v1

    .line 33
    invoke-virtual {v9}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x7

    invoke-static {v3, v2}, Lcom/itau/empresas/ui/util/ViewUtils;->formataAdicionandoZeros(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 34
    invoke-virtual {v9}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/itau/empresas/controller/CreditoProdutosController$1;->val$tipoPessoa:Ljava/lang/String;

    iget-object v5, p0, Lcom/itau/empresas/controller/CreditoProdutosController$1;->val$cpf:Ljava/lang/String;

    iget-object v6, p0, Lcom/itau/empresas/controller/CreditoProdutosController$1;->val$familia:Ljava/lang/String;

    iget-object v7, p0, Lcom/itau/empresas/controller/CreditoProdutosController$1;->val$nomeProduto:Ljava/lang/String;

    iget-object v8, p0, Lcom/itau/empresas/controller/CreditoProdutosController$1;->val$tipoOperacao:Ljava/lang/String;

    .line 32
    invoke-interface/range {v0 .. v8}, Lcom/itau/empresas/api/Api;->buscaLimitesProduto(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO$LisConsultaRespostaVO;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalheRespostaVO$LisConsultaRespostaVO;->getList()Ljava/util/List;

    move-result-object v0

    .line 32
    invoke-virtual {v10, v0}, Lcom/itau/empresas/Evento$EventoLisConsulta;->setProdutoLimite(Ljava/util/List;)V

    .line 36
    invoke-static {v10}, Lcom/itau/empresas/controller/BaseController;->post(Ljava/lang/Object;)V

    .line 37
    return-void
.end method

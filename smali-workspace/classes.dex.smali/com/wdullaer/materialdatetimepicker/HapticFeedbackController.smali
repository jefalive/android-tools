.class public Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;
.super Ljava/lang/Object;
.source "HapticFeedbackController.java"


# instance fields
.field private final mContentObserver:Landroid/database/ContentObserver;

.field private final mContext:Landroid/content/Context;

.field private mIsGloballyEnabled:Z

.field private mLastVibrate:J

.field private mVibrator:Landroid/os/Vibrator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;->mContext:Landroid/content/Context;

    .line 32
    new-instance v0, Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController$1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController$1;-><init>(Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;->mContentObserver:Landroid/database/ContentObserver;

    .line 38
    return-void
.end method

.method static synthetic access$002(Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;Z)Z
    .registers 2
    .param p0, "x0"    # Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;
    .param p1, "x1"    # Z

    .line 14
    iput-boolean p1, p0, Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;->mIsGloballyEnabled:Z

    return p1
.end method

.method static synthetic access$100(Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;)Landroid/content/Context;
    .registers 2
    .param p0, "x0"    # Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;

    .line 14
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Landroid/content/Context;)Z
    .registers 2
    .param p0, "x0"    # Landroid/content/Context;

    .line 14
    invoke-static {p0}, Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;->checkGlobalSetting(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private static checkGlobalSetting(Landroid/content/Context;)Z
    .registers 4
    .param p0, "context"    # Landroid/content/Context;

    .line 19
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "haptic_feedback_enabled"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_10

    const/4 v0, 0x1

    goto :goto_11

    :cond_10
    const/4 v0, 0x0

    :goto_11
    return v0
.end method


# virtual methods
.method public start()V
    .registers 5

    .line 44
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;->mContext:Landroid/content/Context;

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;->mVibrator:Landroid/os/Vibrator;

    .line 47
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;->checkGlobalSetting(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;->mIsGloballyEnabled:Z

    .line 48
    const-string v0, "haptic_feedback_enabled"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 49
    .local v3, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;->mContentObserver:Landroid/database/ContentObserver;

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v2, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 50
    return-void
.end method

.method public stop()V
    .registers 3

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;->mVibrator:Landroid/os/Vibrator;

    .line 57
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;->mContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 58
    return-void
.end method

.method public tryVibrate()V
    .registers 7

    .line 65
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;->mVibrator:Landroid/os/Vibrator;

    if-eqz v0, :cond_1f

    iget-boolean v0, p0, Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;->mIsGloballyEnabled:Z

    if-eqz v0, :cond_1f

    .line 66
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 68
    .local v4, "now":J
    iget-wide v0, p0, Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;->mLastVibrate:J

    sub-long v0, v4, v0

    const-wide/16 v2, 0x7d

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1f

    .line 69
    iget-object v0, p0, Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;->mVibrator:Landroid/os/Vibrator;

    const-wide/16 v1, 0x32

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate(J)V

    .line 70
    iput-wide v4, p0, Lcom/wdullaer/materialdatetimepicker/HapticFeedbackController;->mLastVibrate:J

    .line 73
    .end local v4    # "now":J
    :cond_1f
    return-void
.end method

.class public Lcom/itau/empresas/ui/widgets/ContatosChildItem;
.super Ljava/lang/Object;
.source "ContatosChildItem.java"


# instance fields
.field private horarioDeAtendimento:Ljava/lang/String;

.field private informacoes:Ljava/lang/String;

.field private numeroTelefone:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getHorarioDeAtendimento()Ljava/lang/String;
    .registers 2

    .line 26
    iget-object v0, p0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->horarioDeAtendimento:Ljava/lang/String;

    return-object v0
.end method

.method public getInformacoes()Ljava/lang/String;
    .registers 2

    .line 18
    iget-object v0, p0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->informacoes:Ljava/lang/String;

    return-object v0
.end method

.method public getNumeroTelefone()Ljava/lang/String;
    .registers 2

    .line 10
    iget-object v0, p0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->numeroTelefone:Ljava/lang/String;

    return-object v0
.end method

.method public setHorarioDeAtendimento(Ljava/lang/String;)V
    .registers 2
    .param p1, "horarioDeAtendimento"    # Ljava/lang/String;

    .line 30
    iput-object p1, p0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->horarioDeAtendimento:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public setInformacoes(Ljava/lang/String;)V
    .registers 2
    .param p1, "informacoes"    # Ljava/lang/String;

    .line 22
    iput-object p1, p0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->informacoes:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public setNumeroTelefone(Ljava/lang/String;)V
    .registers 2
    .param p1, "numeroTelefone"    # Ljava/lang/String;

    .line 14
    iput-object p1, p0, Lcom/itau/empresas/ui/widgets/ContatosChildItem;->numeroTelefone:Ljava/lang/String;

    .line 15
    return-void
.end method

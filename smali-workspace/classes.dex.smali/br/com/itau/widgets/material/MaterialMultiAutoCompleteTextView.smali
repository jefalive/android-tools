.class public Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;
.super Landroid/support/v7/widget/AppCompatMultiAutoCompleteTextView;
.source "MaterialMultiAutoCompleteTextView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$FloatingLabelType;
    }
.end annotation


# static fields
.field public static final FLOATING_LABEL_HIGHLIGHT:I = 0x2

.field public static final FLOATING_LABEL_NONE:I = 0x0

.field public static final FLOATING_LABEL_NORMAL:I = 0x1


# instance fields
.field private accentTypeface:Landroid/graphics/Typeface;

.field private autoValidate:Z

.field private baseColor:I

.field private bottomEllipsisSize:I

.field private bottomLines:F

.field bottomLinesAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

.field private bottomSpacing:I

.field private bottomTextSize:I

.field private charactersCountValid:Z

.field private checkCharactersCountAtBeginning:Z

.field private clearButtonBitmaps:[Landroid/graphics/Bitmap;

.field private clearButtonClicking:Z

.field private clearButtonTouched:Z

.field private currentBottomLines:F

.field private errorColor:I

.field private extraPaddingBottom:I

.field private extraPaddingLeft:I

.field private extraPaddingRight:I

.field private extraPaddingTop:I

.field private firstShown:Z

.field private floatingLabelAlwaysShown:Z

.field private floatingLabelAnimating:Z

.field private floatingLabelEnabled:Z

.field private floatingLabelFraction:F

.field private floatingLabelPadding:I

.field private floatingLabelShown:Z

.field private floatingLabelText:Ljava/lang/CharSequence;

.field private floatingLabelTextColor:I

.field private floatingLabelTextSize:I

.field private focusEvaluator:Lcom/nineoldandroids/animation/ArgbEvaluator;

.field private focusFraction:F

.field private helperText:Ljava/lang/String;

.field private helperTextAlwaysShown:Z

.field private helperTextColor:I

.field private hideUnderline:Z

.field private highlightFloatingLabel:Z

.field private iconLeftBitmaps:[Landroid/graphics/Bitmap;

.field private iconOuterHeight:I

.field private iconOuterWidth:I

.field private iconPadding:I

.field private iconRightBitmaps:[Landroid/graphics/Bitmap;

.field private iconSize:I

.field innerFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field private innerPaddingBottom:I

.field private innerPaddingLeft:I

.field private innerPaddingRight:I

.field private innerPaddingTop:I

.field labelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

.field labelFocusAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

.field private lengthChecker:Lbr/com/itau/widgets/material/validation/METLengthChecker;

.field private maxCharacters:I

.field private minBottomLines:I

.field private minBottomTextLines:I

.field private minCharacters:I

.field outerFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field paint:Landroid/graphics/Paint;

.field private primaryColor:I

.field private showClearButton:Z

.field private singleLineEllipsis:Z

.field private tempErrorText:Ljava/lang/String;

.field private textColorHintStateList:Landroid/content/res/ColorStateList;

.field private textColorStateList:Landroid/content/res/ColorStateList;

.field textLayout:Landroid/text/StaticLayout;

.field textPaint:Landroid/text/TextPaint;

.field private typeface:Landroid/graphics/Typeface;

.field private underlineColor:I

.field private validateOnFocusLost:Z

.field private validators:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lbr/com/itau/widgets/material/validation/METValidator;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;

    .line 265
    invoke-direct {p0, p1}, Landroid/support/v7/widget/AppCompatMultiAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 54
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->paint:Landroid/graphics/Paint;

    .line 55
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textPaint:Landroid/text/TextPaint;

    .line 181
    const/4 v0, -0x1

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->helperTextColor:I

    .line 260
    new-instance v0, Lcom/nineoldandroids/animation/ArgbEvaluator;

    invoke-direct {v0}, Lcom/nineoldandroids/animation/ArgbEvaluator;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->focusEvaluator:Lcom/nineoldandroids/animation/ArgbEvaluator;

    .line 266
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 267
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 270
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/AppCompatMultiAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->paint:Landroid/graphics/Paint;

    .line 55
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textPaint:Landroid/text/TextPaint;

    .line 181
    const/4 v0, -0x1

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->helperTextColor:I

    .line 260
    new-instance v0, Lcom/nineoldandroids/animation/ArgbEvaluator;

    invoke-direct {v0}, Lcom/nineoldandroids/animation/ArgbEvaluator;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->focusEvaluator:Lcom/nineoldandroids/animation/ArgbEvaluator;

    .line 271
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 272
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "style"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .line 276
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/AppCompatMultiAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->paint:Landroid/graphics/Paint;

    .line 55
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textPaint:Landroid/text/TextPaint;

    .line 181
    const/4 v0, -0x1

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->helperTextColor:I

    .line 260
    new-instance v0, Lcom/nineoldandroids/animation/ArgbEvaluator;

    invoke-direct {v0}, Lcom/nineoldandroids/animation/ArgbEvaluator;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->focusEvaluator:Lcom/nineoldandroids/animation/ArgbEvaluator;

    .line 277
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 278
    return-void
.end method

.method static synthetic access$000(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)V
    .registers 1
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    .line 49
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->checkCharactersCount()V

    return-void
.end method

.method static synthetic access$100(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    .line 49
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->autoValidate:Z

    return v0
.end method

.method static synthetic access$200(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    .line 49
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelEnabled:Z

    return v0
.end method

.method static synthetic access$300(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    .line 49
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelShown:Z

    return v0
.end method

.method static synthetic access$302(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;Z)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;
    .param p1, "x1"    # Z

    .line 49
    iput-boolean p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelShown:Z

    return p1
.end method

.method static synthetic access$400(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)Lcom/nineoldandroids/animation/ObjectAnimator;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    .line 49
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getLabelAnimator()Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    .line 49
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->highlightFloatingLabel:Z

    return v0
.end method

.method static synthetic access$600(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)Lcom/nineoldandroids/animation/ObjectAnimator;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    .line 49
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getLabelFocusAnimator()Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;

    .line 49
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->validateOnFocusLost:Z

    return v0
.end method

.method private adjustBottomLines()Z
    .registers 11

    .line 759
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getWidth()I

    move-result v0

    if-nez v0, :cond_8

    .line 760
    const/4 v0, 0x0

    return v0

    .line 763
    :cond_8
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textPaint:Landroid/text/TextPaint;

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomTextSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 764
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->tempErrorText:Ljava/lang/String;

    if-nez v0, :cond_18

    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->helperText:Ljava/lang/String;

    if-eqz v0, :cond_74

    .line 765
    :cond_18
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getGravity()I

    move-result v0

    and-int/lit8 v0, v0, 0x5

    const/4 v1, 0x5

    if-eq v0, v1, :cond_27

    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_2a

    :cond_27
    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    goto :goto_38

    :cond_2a
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getGravity()I

    move-result v0

    and-int/lit8 v0, v0, 0x3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_36

    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    goto :goto_38

    :cond_36
    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    .line 768
    .local v9, "alignment":Landroid/text/Layout$Alignment;
    :goto_38
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->tempErrorText:Ljava/lang/String;

    if-eqz v1, :cond_41

    iget-object v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->tempErrorText:Ljava/lang/String;

    goto :goto_43

    :cond_41
    iget-object v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->helperText:Ljava/lang/String;

    :goto_43
    iget-object v2, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getWidth()I

    move-result v3

    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getBottomTextLeftOffset()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getBottomTextRightOffset()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    move-object v4, v9

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textLayout:Landroid/text/StaticLayout;

    .line 769
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v0

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->minBottomTextLines:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 770
    .local v8, "destBottomLines":I
    .end local v9    # "alignment":Landroid/text/Layout$Alignment;
    goto :goto_76

    .line 771
    .end local v8    # "destBottomLines":I
    :cond_74
    iget v8, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->minBottomLines:I

    .line 773
    .local v8, "destBottomLines":I
    :goto_76
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomLines:F

    int-to-float v1, v8

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_85

    .line 774
    int-to-float v0, v8

    invoke-direct {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getBottomLinesAnimator(F)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->start()V

    .line 776
    :cond_85
    int-to-float v0, v8

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomLines:F

    .line 777
    const/4 v0, 0x1

    return v0
.end method

.method private checkCharactersCount()V
    .registers 4

    .line 1378
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->firstShown:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->checkCharactersCountAtBeginning:Z

    if-eqz v0, :cond_e

    :cond_8
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->hasCharactersCounter()Z

    move-result v0

    if-nez v0, :cond_12

    .line 1379
    :cond_e
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->charactersCountValid:Z

    goto :goto_2f

    .line 1381
    :cond_12
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 1382
    .local v1, "text":Ljava/lang/CharSequence;
    if-nez v1, :cond_1a

    const/4 v2, 0x0

    goto :goto_1e

    :cond_1a
    invoke-direct {p0, v1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->checkLength(Ljava/lang/CharSequence;)I

    move-result v2

    .line 1383
    .local v2, "count":I
    :goto_1e
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->minCharacters:I

    if-lt v2, v0, :cond_2c

    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->maxCharacters:I

    if-lez v0, :cond_2a

    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->maxCharacters:I

    if-gt v2, v0, :cond_2c

    :cond_2a
    const/4 v0, 0x1

    goto :goto_2d

    :cond_2c
    const/4 v0, 0x0

    :goto_2d
    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->charactersCountValid:Z

    .line 1385
    .end local v1    # "text":Ljava/lang/CharSequence;
    .end local v2    # "count":I
    :goto_2f
    return-void
.end method

.method private checkLength(Ljava/lang/CharSequence;)I
    .registers 3
    .param p1, "text"    # Ljava/lang/CharSequence;

    .line 1467
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->lengthChecker:Lbr/com/itau/widgets/material/validation/METLengthChecker;

    if-nez v0, :cond_9

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    return v0

    .line 1468
    :cond_9
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->lengthChecker:Lbr/com/itau/widgets/material/validation/METLengthChecker;

    invoke-virtual {v0, p1}, Lbr/com/itau/widgets/material/validation/METLengthChecker;->getLength(Ljava/lang/CharSequence;)I

    move-result v0

    return v0
.end method

.method private correctPaddings()V
    .registers 9

    .line 724
    const/4 v5, 0x0

    .local v5, "buttonsWidthLeft":I
    const/4 v6, 0x0

    .line 725
    .local v6, "buttonsWidthRight":I
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterWidth:I

    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getButtonsCount()I

    move-result v1

    mul-int v7, v0, v1

    .line 726
    .local v7, "buttonsWidth":I
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 727
    move v5, v7

    goto :goto_13

    .line 729
    :cond_12
    move v6, v7

    .line 731
    :goto_13
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->innerPaddingLeft:I

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->extraPaddingLeft:I

    add-int/2addr v0, v1

    add-int/2addr v0, v5

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->innerPaddingTop:I

    iget v2, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->extraPaddingTop:I

    add-int/2addr v1, v2

    iget v2, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->innerPaddingRight:I

    iget v3, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->extraPaddingRight:I

    add-int/2addr v2, v3

    add-int/2addr v2, v6

    iget v3, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->innerPaddingBottom:I

    iget v4, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->extraPaddingBottom:I

    add-int/2addr v3, v4

    invoke-super {p0, v0, v1, v2, v3}, Landroid/support/v7/widget/AppCompatMultiAutoCompleteTextView;->setPadding(IIII)V

    .line 732
    return-void
.end method

.method private generateIconBitmaps(I)[Landroid/graphics/Bitmap;
    .registers 6
    .param p1, "origin"    # I

    .line 477
    const/4 v0, -0x1

    if-ne p1, v0, :cond_5

    .line 478
    const/4 v0, 0x0

    return-object v0

    .line 480
    :cond_5
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 481
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v0, 0x1

    iput-boolean v0, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 482
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 483
    iget v0, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v1, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 484
    .local v3, "size":I
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconSize:I

    if-le v3, v0, :cond_25

    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconSize:I

    div-int v0, v3, v0

    goto :goto_26

    :cond_25
    const/4 v0, 0x1

    :goto_26
    iput v0, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 485
    const/4 v0, 0x0

    iput-boolean v0, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 486
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->generateIconBitmaps(Landroid/graphics/Bitmap;)[Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private generateIconBitmaps(Landroid/graphics/Bitmap;)[Landroid/graphics/Bitmap;
    .registers 6
    .param p1, "origin"    # Landroid/graphics/Bitmap;

    .line 500
    if-nez p1, :cond_4

    .line 501
    const/4 v0, 0x0

    return-object v0

    .line 503
    :cond_4
    const/4 v0, 0x4

    new-array v2, v0, [Landroid/graphics/Bitmap;

    .line 504
    .local v2, "iconBitmaps":[Landroid/graphics/Bitmap;
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->scaleIcon(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 505
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, v2, v1

    .line 506
    new-instance v3, Landroid/graphics/Canvas;

    const/4 v0, 0x0

    aget-object v0, v2, v0

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 507
    .local v3, "canvas":Landroid/graphics/Canvas;
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->baseColor:I

    const v1, 0xffffff

    and-int/2addr v0, v1

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->baseColor:I

    invoke-static {v1}, Lbr/com/itau/widgets/material/Colors;->isLight(I)Z

    move-result v1

    if-eqz v1, :cond_2e

    const/high16 v1, -0x1000000

    goto :goto_30

    :cond_2e
    const/high16 v1, -0x76000000

    :goto_30
    or-int/2addr v0, v1

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 508
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, v2, v1

    .line 509
    new-instance v3, Landroid/graphics/Canvas;

    const/4 v0, 0x1

    aget-object v0, v2, v0

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 510
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->primaryColor:I

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 511
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v1, 0x2

    aput-object v0, v2, v1

    .line 512
    new-instance v3, Landroid/graphics/Canvas;

    const/4 v0, 0x2

    aget-object v0, v2, v0

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 513
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->baseColor:I

    const v1, 0xffffff

    and-int/2addr v0, v1

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->baseColor:I

    invoke-static {v1}, Lbr/com/itau/widgets/material/Colors;->isLight(I)Z

    move-result v1

    if-eqz v1, :cond_72

    const/high16 v1, 0x4c000000    # 3.3554432E7f

    goto :goto_74

    :cond_72
    const/high16 v1, 0x42000000    # 32.0f

    :goto_74
    or-int/2addr v0, v1

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 514
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v1, 0x3

    aput-object v0, v2, v1

    .line 515
    new-instance v3, Landroid/graphics/Canvas;

    const/4 v0, 0x3

    aget-object v0, v2, v0

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 516
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->errorColor:I

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 517
    return-object v2
.end method

.method private generateIconBitmaps(Landroid/graphics/drawable/Drawable;)[Landroid/graphics/Bitmap;
    .registers 8
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .line 490
    if-nez p1, :cond_4

    .line 491
    const/4 v0, 0x0

    return-object v0

    .line 492
    :cond_4
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 493
    .local v4, "bitmap":Landroid/graphics/Bitmap;
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 494
    .local v5, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v5}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    invoke-virtual {v5}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 495
    invoke-virtual {p1, v5}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 496
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconSize:I

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconSize:I

    const/4 v2, 0x0

    invoke-static {v4, v0, v1, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->generateIconBitmaps(Landroid/graphics/Bitmap;)[Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private getBottomEllipsisWidth()I
    .registers 3

    .line 1374
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->singleLineEllipsis:Z

    if-eqz v0, :cond_f

    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomEllipsisSize:I

    mul-int/lit8 v0, v0, 0x5

    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getPixel(I)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_10

    :cond_f
    const/4 v0, 0x0

    :goto_10
    return v0
.end method

.method private getBottomLinesAnimator(F)Lcom/nineoldandroids/animation/ObjectAnimator;
    .registers 5
    .param p1, "destBottomLines"    # F

    .line 1210
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomLinesAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    if-nez v0, :cond_13

    .line 1211
    const-string v0, "currentBottomLines"

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/nineoldandroids/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomLinesAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    goto :goto_23

    .line 1213
    :cond_13
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomLinesAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->cancel()V

    .line 1214
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomLinesAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/nineoldandroids/animation/ObjectAnimator;->setFloatValues([F)V

    .line 1216
    :goto_23
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomLinesAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    return-object v0
.end method

.method private getBottomTextLeftOffset()I
    .registers 2

    .line 1362
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getCharactersCounterWidth()I

    move-result v0

    goto :goto_f

    :cond_b
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getBottomEllipsisWidth()I

    move-result v0

    :goto_f
    return v0
.end method

.method private getBottomTextRightOffset()I
    .registers 2

    .line 1366
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getBottomEllipsisWidth()I

    move-result v0

    goto :goto_f

    :cond_b
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getCharactersCounterWidth()I

    move-result v0

    :goto_f
    return v0
.end method

.method private getButtonsCount()I
    .registers 2

    .line 735
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isShowClearButton()Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    goto :goto_9

    :cond_8
    const/4 v0, 0x0

    :goto_9
    return v0
.end method

.method private getCharactersCounterText()Ljava/lang/String;
    .registers 4

    .line 1397
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->minCharacters:I

    if-gtz v0, :cond_4f

    .line 1398
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_2c

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->maxCharacters:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-direct {p0, v1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->checkLength(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_4d

    :cond_2c
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-direct {p0, v1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->checkLength(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->maxCharacters:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .local v2, "text":Ljava/lang/String;
    :goto_4d
    goto/16 :goto_10a

    .line 1399
    .end local v2    # "text":Ljava/lang/String;
    :cond_4f
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->maxCharacters:I

    if-gtz v0, :cond_a9

    .line 1400
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_81

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->minCharacters:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-direct {p0, v1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->checkLength(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_a8

    :cond_81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-direct {p0, v1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->checkLength(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->minCharacters:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .local v2, "text":Ljava/lang/String;
    :goto_a8
    goto :goto_10a

    .line 1402
    .end local v2    # "text":Ljava/lang/String;
    :cond_a9
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_dd

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->maxCharacters:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->minCharacters:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-direct {p0, v1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->checkLength(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_10a

    :cond_dd
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-direct {p0, v1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->checkLength(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->minCharacters:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->maxCharacters:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1404
    .local v2, "text":Ljava/lang/String;
    :goto_10a
    return-object v2
.end method

.method private getCharactersCounterWidth()I
    .registers 3

    .line 1370
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->hasCharactersCounter()Z

    move-result v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textPaint:Landroid/text/TextPaint;

    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getCharactersCounterText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    goto :goto_13

    :cond_12
    const/4 v0, 0x0

    :goto_13
    return v0
.end method

.method private getCustomTypeface(Ljava/lang/String;)Landroid/graphics/Typeface;
    .registers 3
    .param p1, "fontPath"    # Ljava/lang/String;

    .line 434
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    return-object v0
.end method

.method private getLabelAnimator()Lcom/nineoldandroids/animation/ObjectAnimator;
    .registers 5

    .line 1195
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->labelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    if-nez v0, :cond_12

    .line 1196
    const-string v0, "floatingLabelFraction"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_24

    invoke-static {p0, v0, v1}, Lcom/nineoldandroids/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->labelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    .line 1198
    :cond_12
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->labelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    iget-boolean v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelAnimating:Z

    if-eqz v1, :cond_1b

    const-wide/16 v1, 0x12c

    goto :goto_1d

    :cond_1b
    const-wide/16 v1, 0x0

    :goto_1d
    invoke-virtual {v0, v1, v2}, Lcom/nineoldandroids/animation/ObjectAnimator;->setDuration(J)Lcom/nineoldandroids/animation/ObjectAnimator;

    .line 1199
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->labelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    return-object v0

    nop

    :array_24
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private getLabelFocusAnimator()Lcom/nineoldandroids/animation/ObjectAnimator;
    .registers 5

    .line 1203
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->labelFocusAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    if-nez v0, :cond_12

    .line 1204
    const-string v0, "focusFraction"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_16

    invoke-static {p0, v0, v1}, Lcom/nineoldandroids/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->labelFocusAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    .line 1206
    :cond_12
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->labelFocusAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    return-object v0

    nop

    :array_16
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private getPixel(I)I
    .registers 4
    .param p1, "dp"    # I

    .line 679
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    int-to-float v1, p1

    invoke-static {v0, v1}, Lbr/com/itau/widgets/material/Density;->dp2px(Landroid/content/Context;F)I

    move-result v0

    return v0
.end method

.method private hasCharactersCounter()Z
    .registers 2

    .line 1392
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->minCharacters:I

    if-gtz v0, :cond_8

    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->maxCharacters:I

    if-lez v0, :cond_a

    :cond_8
    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 17
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 281
    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getPixel(I)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconSize:I

    .line 282
    const/16 v0, 0x30

    invoke-direct {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getPixel(I)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterWidth:I

    .line 283
    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getPixel(I)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterHeight:I

    .line 285
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/widgets/material/R$dimen;->inner_components_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomSpacing:I

    .line 286
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/widgets/material/R$dimen;->bottom_ellipsis_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomEllipsisSize:I

    .line 289
    const/high16 v4, -0x1000000

    .line 291
    .local v4, "defaultBaseColor":I
    sget-object v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText:[I

    move-object/from16 v1, p2

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v5

    .line 292
    .local v5, "typedArray":Landroid/content/res/TypedArray;
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_textColor:I

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textColorStateList:Landroid/content/res/ColorStateList;

    .line 293
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_textColorHint:I

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textColorHintStateList:Landroid/content/res/ColorStateList;

    .line 294
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_baseColor:I

    invoke-virtual {v5, v0, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->baseColor:I

    .line 298
    new-instance v7, Landroid/util/TypedValue;

    invoke-direct {v7}, Landroid/util/TypedValue;-><init>()V

    .line 300
    .local v7, "primaryColorTypedValue":Landroid/util/TypedValue;
    :try_start_57
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_6b

    .line 301
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v1, 0x1010433

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v7, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 302
    iget v6, v7, Landroid/util/TypedValue;->data:I

    .local v6, "defaultPrimaryColor":I
    goto :goto_73

    .line 304
    .end local v6    # "defaultPrimaryColor":I
    :cond_6b
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "SDK_INT less than LOLLIPOP"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_73
    .catch Ljava/lang/Exception; {:try_start_57 .. :try_end_73} :catch_74

    .line 318
    .local v6, "defaultPrimaryColor":I
    :goto_73
    goto :goto_a2

    .line 306
    :catch_74
    move-exception v8

    .line 308
    .local v8, "e":Ljava/lang/Exception;
    :try_start_75
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const-string v1, "colorPrimary"

    const-string v2, "attr"

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    .line 309
    .local v9, "colorPrimaryId":I
    if-eqz v9, :cond_96

    .line 310
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v9, v7, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 311
    iget v6, v7, Landroid/util/TypedValue;->data:I

    goto :goto_9e

    .line 313
    .end local v6    # "defaultPrimaryColor":I
    :cond_96
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "colorPrimary not found"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_9e
    .catch Ljava/lang/Exception; {:try_start_75 .. :try_end_9e} :catch_9f

    .line 317
    .local v6, "defaultPrimaryColor":I
    .end local v9    # "colorPrimaryId":I
    :goto_9e
    goto :goto_a2

    .line 315
    :catch_9f
    move-exception v9

    .line 316
    .local v9, "e1":Ljava/lang/Exception;
    iget v6, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->baseColor:I

    .line 320
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v9    # "e1":Ljava/lang/Exception;
    :goto_a2
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_primaryColor:I

    invoke-virtual {v5, v0, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->primaryColor:I

    .line 321
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_floatingLabel:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-direct {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->setFloatingLabelInternal(I)V

    .line 322
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_errorColor:I

    const-string v1, "#e7492E"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->errorColor:I

    .line 323
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_minCharacters:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->minCharacters:I

    .line 324
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_maxCharacters:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->maxCharacters:I

    .line 325
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_singleLineEllipsis:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->singleLineEllipsis:Z

    .line 326
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_helperText:I

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->helperText:Ljava/lang/String;

    .line 327
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_helperTextColor:I

    const/4 v1, -0x1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->helperTextColor:I

    .line 328
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_minBottomTextLines:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->minBottomTextLines:I

    .line 329
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_accentTypeface:I

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 330
    .local v8, "fontPathForAccent":Ljava/lang/String;
    if-eqz v8, :cond_112

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_112

    .line 331
    invoke-direct {p0, v8}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getCustomTypeface(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->accentTypeface:Landroid/graphics/Typeface;

    .line 332
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->accentTypeface:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 334
    :cond_112
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_typeface:I

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 335
    .local v9, "fontPathForView":Ljava/lang/String;
    if-eqz v9, :cond_12b

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isInEditMode()Z

    move-result v0

    if-nez v0, :cond_12b

    .line 336
    invoke-direct {p0, v9}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getCustomTypeface(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->typeface:Landroid/graphics/Typeface;

    .line 337
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->typeface:Landroid/graphics/Typeface;

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 339
    :cond_12b
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_floatingLabelText:I

    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelText:Ljava/lang/CharSequence;

    .line 340
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelText:Ljava/lang/CharSequence;

    if-nez v0, :cond_13d

    .line 341
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelText:Ljava/lang/CharSequence;

    .line 343
    :cond_13d
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_floatingLabelPadding:I

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomSpacing:I

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelPadding:I

    .line 344
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_floatingLabelTextSize:I

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/widgets/material/R$dimen;->floating_label_text_size:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelTextSize:I

    .line 345
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_floatingLabelTextColor:I

    const/4 v1, -0x1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelTextColor:I

    .line 346
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_floatingLabelAnimating:I

    const/4 v1, 0x1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelAnimating:Z

    .line 347
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_bottomTextSize:I

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/widgets/material/R$dimen;->bottom_text_size:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomTextSize:I

    .line 348
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_hideUnderline:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->hideUnderline:Z

    .line 349
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_underlineColor:I

    const/4 v1, -0x1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->underlineColor:I

    .line 350
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_autoValidate:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->autoValidate:Z

    .line 351
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_iconLeft:I

    const/4 v1, -0x1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    invoke-direct {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->generateIconBitmaps(I)[Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconLeftBitmaps:[Landroid/graphics/Bitmap;

    .line 352
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_iconRight:I

    const/4 v1, -0x1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    invoke-direct {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->generateIconBitmaps(I)[Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconRightBitmaps:[Landroid/graphics/Bitmap;

    .line 353
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_clearButton:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->showClearButton:Z

    .line 354
    sget v0, Lbr/com/itau/widgets/material/R$drawable;->met_ic_clear:I

    invoke-direct {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->generateIconBitmaps(I)[Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->clearButtonBitmaps:[Landroid/graphics/Bitmap;

    .line 355
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_iconPadding:I

    const/16 v1, 0x10

    invoke-direct {p0, v1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getPixel(I)I

    move-result v1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconPadding:I

    .line 356
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_floatingLabelAlwaysShown:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelAlwaysShown:Z

    .line 357
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_helperTextAlwaysShown:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->helperTextAlwaysShown:Z

    .line 358
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_validateOnFocusLost:I

    const/4 v1, 0x0

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->validateOnFocusLost:Z

    .line 359
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialEditText_met_checkCharactersCountAtBeginning:I

    const/4 v1, 0x1

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->checkCharactersCountAtBeginning:Z

    .line 360
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    .line 362
    const/4 v0, 0x5

    new-array v10, v0, [I

    fill-array-data v10, :array_25a

    .line 369
    .local v10, "paddings":[I
    move-object/from16 v0, p2

    invoke-virtual {p1, v0, v10}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v11

    .line 370
    .local v11, "paddingsTypedArray":Landroid/content/res/TypedArray;
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {v11, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v12

    .line 371
    .local v12, "padding":I
    const/4 v0, 0x1

    invoke-virtual {v11, v0, v12}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->innerPaddingLeft:I

    .line 372
    const/4 v0, 0x2

    invoke-virtual {v11, v0, v12}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->innerPaddingTop:I

    .line 373
    const/4 v0, 0x3

    invoke-virtual {v11, v0, v12}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->innerPaddingRight:I

    .line 374
    const/4 v0, 0x4

    invoke-virtual {v11, v0, v12}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->innerPaddingBottom:I

    .line 375
    invoke-virtual {v11}, Landroid/content/res/TypedArray;->recycle()V

    .line 377
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_234

    .line 378
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_238

    .line 380
    :cond_234
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 382
    :goto_238
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->singleLineEllipsis:Z

    if-eqz v0, :cond_246

    .line 383
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getTransformationMethod()Landroid/text/method/TransformationMethod;

    move-result-object v13

    .line 384
    .local v13, "transformationMethod":Landroid/text/method/TransformationMethod;
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->setSingleLine()V

    .line 385
    invoke-virtual {p0, v13}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 387
    .end local v13    # "transformationMethod":Landroid/text/method/TransformationMethod;
    :cond_246
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initMinBottomLines()V

    .line 388
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initPadding()V

    .line 389
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initText()V

    .line 390
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initFloatingLabel()V

    .line 391
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initTextWatcher()V

    .line 392
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->checkCharactersCount()V

    .line 393
    return-void

    nop

    :array_25a
    .array-data 4
        0x10100d5
        0x10100d6
        0x10100d7
        0x10100d8
        0x10100d9
    .end array-data
.end method

.method private initFloatingLabel()V
    .registers 2

    .line 810
    new-instance v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$2;

    invoke-direct {v0, p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$2;-><init>(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)V

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 835
    new-instance v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$3;

    invoke-direct {v0, p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$3;-><init>(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->innerFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    .line 853
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->innerFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-super {p0, v0}, Landroid/support/v7/widget/AppCompatMultiAutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 854
    return-void
.end method

.method private initMinBottomLines()V
    .registers 3

    .line 696
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->minCharacters:I

    if-gtz v0, :cond_14

    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->maxCharacters:I

    if-gtz v0, :cond_14

    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->singleLineEllipsis:Z

    if-nez v0, :cond_14

    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->tempErrorText:Ljava/lang/String;

    if-nez v0, :cond_14

    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->helperText:Ljava/lang/String;

    if-eqz v0, :cond_16

    :cond_14
    const/4 v1, 0x1

    goto :goto_17

    :cond_16
    const/4 v1, 0x0

    .line 697
    .local v1, "extendBottom":Z
    :goto_17
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->minBottomTextLines:I

    if-lez v0, :cond_1e

    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->minBottomTextLines:I

    goto :goto_23

    :cond_1e
    if-eqz v1, :cond_22

    const/4 v0, 0x1

    goto :goto_23

    :cond_22
    const/4 v0, 0x0

    :goto_23
    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->minBottomLines:I

    int-to-float v0, v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->currentBottomLines:F

    .line 698
    return-void
.end method

.method private initPadding()V
    .registers 5

    .line 683
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelEnabled:Z

    if-eqz v0, :cond_a

    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelTextSize:I

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelPadding:I

    add-int/2addr v0, v1

    goto :goto_c

    :cond_a
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelPadding:I

    :goto_c
    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->extraPaddingTop:I

    .line 684
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textPaint:Landroid/text/TextPaint;

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomTextSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 685
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v3

    .line 686
    .local v3, "textMetrics":Landroid/graphics/Paint$FontMetrics;
    iget v0, v3, Landroid/graphics/Paint$FontMetrics;->descent:F

    iget v1, v3, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float/2addr v0, v1

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->currentBottomLines:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget-boolean v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->hideUnderline:Z

    if-eqz v1, :cond_2c

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomSpacing:I

    goto :goto_30

    :cond_2c
    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomSpacing:I

    mul-int/lit8 v1, v1, 0x2

    :goto_30
    add-int/2addr v0, v1

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->extraPaddingBottom:I

    .line 687
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconLeftBitmaps:[Landroid/graphics/Bitmap;

    if-nez v0, :cond_39

    const/4 v0, 0x0

    goto :goto_3e

    :cond_39
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterWidth:I

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconPadding:I

    add-int/2addr v0, v1

    :goto_3e
    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->extraPaddingLeft:I

    .line 688
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconRightBitmaps:[Landroid/graphics/Bitmap;

    if-nez v0, :cond_46

    const/4 v0, 0x0

    goto :goto_4b

    :cond_46
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterWidth:I

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconPadding:I

    add-int/2addr v0, v1

    :goto_4b
    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->extraPaddingRight:I

    .line 689
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->correctPaddings()V

    .line 690
    return-void
.end method

.method private initText()V
    .registers 3

    .line 396
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_27

    .line 397
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 398
    .local v1, "text":Ljava/lang/CharSequence;
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 399
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->resetHintTextColor()V

    .line 400
    invoke-virtual {p0, v1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 401
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->setSelection(I)V

    .line 402
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelFraction:F

    .line 403
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelShown:Z

    .line 404
    .end local v1    # "text":Ljava/lang/CharSequence;
    goto :goto_2a

    .line 405
    :cond_27
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->resetHintTextColor()V

    .line 407
    :goto_2a
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->resetTextColor()V

    .line 408
    return-void
.end method

.method private initTextWatcher()V
    .registers 2

    .line 411
    new-instance v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$1;

    invoke-direct {v0, p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$1;-><init>(Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;)V

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 431
    return-void
.end method

.method private insideClearButton(Landroid/view/MotionEvent;)Z
    .registers 11
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 1452
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 1453
    .local v3, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 1454
    .local v4, "y":F
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getScrollX()I

    move-result v0

    iget-object v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconLeftBitmaps:[Landroid/graphics/Bitmap;

    if-nez v1, :cond_12

    const/4 v1, 0x0

    goto :goto_17

    :cond_12
    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterWidth:I

    iget v2, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconPadding:I

    add-int/2addr v1, v2

    :goto_17
    add-int v5, v0, v1

    .line 1455
    .local v5, "startX":I
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getScrollX()I

    move-result v0

    iget-object v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconRightBitmaps:[Landroid/graphics/Bitmap;

    if-nez v1, :cond_26

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getWidth()I

    move-result v1

    goto :goto_30

    :cond_26
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getWidth()I

    move-result v1

    iget v2, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterWidth:I

    sub-int/2addr v1, v2

    iget v2, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconPadding:I

    sub-int/2addr v1, v2

    :goto_30
    add-int v6, v0, v1

    .line 1457
    .local v6, "endX":I
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 1458
    move v7, v5

    .local v7, "buttonLeft":I
    goto :goto_3e

    .line 1460
    .end local v7    # "buttonLeft":I
    :cond_3a
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterWidth:I

    sub-int v7, v6, v0

    .line 1462
    .local v7, "buttonLeft":I
    :goto_3e
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getScrollY()I

    move-result v0

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomSpacing:I

    add-int/2addr v0, v1

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterHeight:I

    sub-int v8, v0, v1

    .line 1463
    .local v8, "buttonTop":I
    int-to-float v0, v7

    cmpl-float v0, v3, v0

    if-ltz v0, :cond_6f

    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterWidth:I

    add-int/2addr v0, v7

    int-to-float v0, v0

    cmpg-float v0, v3, v0

    if-gez v0, :cond_6f

    int-to-float v0, v8

    cmpl-float v0, v4, v0

    if-ltz v0, :cond_6f

    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterHeight:I

    add-int/2addr v0, v8

    int-to-float v0, v0

    cmpg-float v0, v4, v0

    if-gez v0, :cond_6f

    const/4 v0, 0x1

    goto :goto_70

    :cond_6f
    const/4 v0, 0x0

    :goto_70
    return v0
.end method

.method private isInternalValid()Z
    .registers 2

    .line 1068
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->tempErrorText:Ljava/lang/String;

    if-nez v0, :cond_c

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isCharactersCountValid()Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    goto :goto_d

    :cond_c
    const/4 v0, 0x0

    :goto_d
    return v0
.end method

.method private isRTL()Z
    .registers 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .line 1354
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_8

    .line 1355
    const/4 v0, 0x0

    return v0

    .line 1357
    :cond_8
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 1358
    .local v2, "config":Landroid/content/res/Configuration;
    invoke-virtual {v2}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_19

    const/4 v0, 0x1

    goto :goto_1a

    :cond_19
    const/4 v0, 0x0

    :goto_1a
    return v0
.end method

.method private resetHintTextColor()V
    .registers 3

    .line 921
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textColorHintStateList:Landroid/content/res/ColorStateList;

    if-nez v0, :cond_11

    .line 922
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->baseColor:I

    const v1, 0xffffff

    and-int/2addr v0, v1

    const/high16 v1, 0x44000000    # 512.0f

    or-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->setHintTextColor(I)V

    goto :goto_16

    .line 924
    :cond_11
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textColorHintStateList:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    .line 926
    :goto_16
    return-void
.end method

.method private resetTextColor()V
    .registers 6

    .line 896
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textColorStateList:Landroid/content/res/ColorStateList;

    if-nez v0, :cond_3d

    .line 897
    new-instance v0, Landroid/content/res/ColorStateList;

    const/4 v1, 0x2

    new-array v1, v1, [[I

    const/4 v2, 0x1

    new-array v2, v2, [I

    fill-array-data v2, :array_44

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->EMPTY_STATE_SET:[I

    const/4 v3, 0x1

    aput-object v2, v1, v3

    const/4 v2, 0x2

    new-array v2, v2, [I

    iget v3, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->baseColor:I

    const v4, 0xffffff

    and-int/2addr v3, v4

    const/high16 v4, -0x21000000

    or-int/2addr v3, v4

    const/4 v4, 0x0

    aput v3, v2, v4

    iget v3, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->baseColor:I

    const v4, 0xffffff

    and-int/2addr v3, v4

    const/high16 v4, 0x44000000    # 512.0f

    or-int/2addr v3, v4

    const/4 v4, 0x1

    aput v3, v2, v4

    invoke-direct {v0, v1, v2}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textColorStateList:Landroid/content/res/ColorStateList;

    .line 898
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textColorStateList:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_42

    .line 900
    :cond_3d
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textColorStateList:Landroid/content/res/ColorStateList;

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 902
    :goto_42
    return-void

    nop

    :array_44
    .array-data 4
        0x101009e
    .end array-data
.end method

.method private scaleIcon(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .registers 10
    .param p1, "origin"    # Landroid/graphics/Bitmap;

    .line 521
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 522
    .local v3, "width":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 523
    .local v4, "height":I
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 524
    .local v5, "size":I
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconSize:I

    if-ne v5, v0, :cond_11

    .line 525
    return-object p1

    .line 526
    :cond_11
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconSize:I

    if-le v5, v0, :cond_34

    .line 529
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconSize:I

    if-le v3, v0, :cond_24

    .line 530
    iget v6, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconSize:I

    .line 531
    .local v6, "scaledWidth":I
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconSize:I

    int-to-float v0, v0

    int-to-float v1, v4

    int-to-float v2, v3

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    float-to-int v7, v0

    .local v7, "scaledHeight":I
    goto :goto_2e

    .line 533
    .end local v6    # "scaledWidth":I
    .end local v7    # "scaledHeight":I
    :cond_24
    iget v7, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconSize:I

    .line 534
    .local v7, "scaledHeight":I
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconSize:I

    int-to-float v0, v0

    int-to-float v1, v3

    int-to-float v2, v4

    div-float/2addr v1, v2

    mul-float/2addr v0, v1

    float-to-int v6, v0

    .line 536
    .local v6, "scaledWidth":I
    :goto_2e
    const/4 v0, 0x0

    invoke-static {p1, v6, v7, v0}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    .line 538
    .end local v6    # "scaledWidth":I
    .end local v7    # "scaledHeight":I
    :cond_34
    return-object p1
.end method

.method private setFloatingLabelInternal(I)V
    .registers 3
    .param p1, "mode"    # I

    .line 929
    sparse-switch p1, :sswitch_data_1a

    goto :goto_12

    .line 931
    :sswitch_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelEnabled:Z

    .line 932
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->highlightFloatingLabel:Z

    .line 933
    goto :goto_18

    .line 935
    :sswitch_b
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelEnabled:Z

    .line 936
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->highlightFloatingLabel:Z

    .line 937
    goto :goto_18

    .line 939
    :goto_12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelEnabled:Z

    .line 940
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->highlightFloatingLabel:Z

    .line 943
    :goto_18
    return-void

    nop

    :sswitch_data_1a
    .sparse-switch
        0x1 -> :sswitch_4
        0x2 -> :sswitch_b
    .end sparse-switch
.end method


# virtual methods
.method public addValidator(Lbr/com/itau/widgets/material/validation/METValidator;)Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;
    .registers 3
    .param p1, "validator"    # Lbr/com/itau/widgets/material/validation/METValidator;

    .line 1163
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->validators:Ljava/util/List;

    if-nez v0, :cond_b

    .line 1164
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->validators:Ljava/util/List;

    .line 1166
    :cond_b
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->validators:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1167
    return-object p0
.end method

.method public clearValidators()V
    .registers 2

    .line 1171
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->validators:Ljava/util/List;

    if-eqz v0, :cond_9

    .line 1172
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->validators:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1174
    :cond_9
    return-void
.end method

.method public getAccentTypeface()Landroid/graphics/Typeface;
    .registers 2

    .line 589
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->accentTypeface:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public getBottomTextSize()I
    .registers 2

    .line 670
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomTextSize:I

    return v0
.end method

.method public getCurrentBottomLines()F
    .registers 2

    .line 561
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->currentBottomLines:F

    return v0
.end method

.method public getError()Ljava/lang/CharSequence;
    .registers 2

    .line 1053
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->tempErrorText:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorColor()I
    .registers 2

    .line 1023
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->errorColor:I

    return v0
.end method

.method public getFloatingLabelFraction()F
    .registers 2

    .line 543
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelFraction:F

    return v0
.end method

.method public getFloatingLabelPadding()I
    .registers 2

    .line 951
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelPadding:I

    return v0
.end method

.method public getFloatingLabelText()Ljava/lang/CharSequence;
    .registers 2

    .line 636
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getFloatingLabelTextColor()I
    .registers 2

    .line 661
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelTextColor:I

    return v0
.end method

.method public getFloatingLabelTextSize()I
    .registers 2

    .line 652
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelTextSize:I

    return v0
.end method

.method public getFocusFraction()F
    .registers 2

    .line 552
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->focusFraction:F

    return v0
.end method

.method public getHelperText()Ljava/lang/String;
    .registers 2

    .line 1032
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->helperText:Ljava/lang/String;

    return-object v0
.end method

.method public getHelperTextColor()I
    .registers 2

    .line 1043
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->helperTextColor:I

    return v0
.end method

.method public getInnerPaddingBottom()I
    .registers 2

    .line 791
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->innerPaddingBottom:I

    return v0
.end method

.method public getInnerPaddingLeft()I
    .registers 2

    .line 798
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->innerPaddingLeft:I

    return v0
.end method

.method public getInnerPaddingRight()I
    .registers 2

    .line 805
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->innerPaddingRight:I

    return v0
.end method

.method public getInnerPaddingTop()I
    .registers 2

    .line 784
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->innerPaddingTop:I

    return v0
.end method

.method public getMaxCharacters()I
    .registers 2

    .line 979
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->maxCharacters:I

    return v0
.end method

.method public getMinBottomTextLines()I
    .registers 2

    .line 1001
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->minBottomTextLines:I

    return v0
.end method

.method public getMinCharacters()I
    .registers 2

    .line 990
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->minCharacters:I

    return v0
.end method

.method public getUnderlineColor()I
    .registers 2

    .line 622
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->underlineColor:I

    return v0
.end method

.method public getValidators()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lbr/com/itau/widgets/material/validation/METValidator;>;"
        }
    .end annotation

    .line 1178
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->validators:Ljava/util/List;

    return-object v0
.end method

.method public hasValidators()Z
    .registers 2

    .line 1151
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->validators:Ljava/util/List;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->validators:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_e

    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

.method public isAutoValidate()Z
    .registers 2

    .line 1012
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->autoValidate:Z

    return v0
.end method

.method public isCharactersCountValid()Z
    .registers 2

    .line 1388
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->charactersCountValid:Z

    return v0
.end method

.method public isFloatingLabelAlwaysShown()Z
    .registers 2

    .line 570
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelAlwaysShown:Z

    return v0
.end method

.method public isFloatingLabelAnimating()Z
    .registers 2

    .line 960
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelAnimating:Z

    return v0
.end method

.method public isHelperTextAlwaysShown()Z
    .registers 2

    .line 579
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->helperTextAlwaysShown:Z

    return v0
.end method

.method public isHideUnderline()Z
    .registers 2

    .line 602
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->hideUnderline:Z

    return v0
.end method

.method public isShowClearButton()Z
    .registers 2

    .line 468
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->showClearButton:Z

    return v0
.end method

.method public isValid(Ljava/lang/String;)Z
    .registers 5
    .param p1, "regex"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1078
    if-nez p1, :cond_4

    .line 1079
    const/4 v0, 0x0

    return v0

    .line 1081
    :cond_4
    invoke-static {p1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 1082
    .local v1, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 1083
    .local v2, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method

.method public isValidateOnFocusLost()Z
    .registers 2

    .line 857
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->validateOnFocusLost:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .registers 2

    .line 740
    invoke-super {p0}, Landroid/support/v7/widget/AppCompatMultiAutoCompleteTextView;->onAttachedToWindow()V

    .line 741
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->firstShown:Z

    if-nez v0, :cond_a

    .line 742
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->firstShown:Z

    .line 744
    :cond_a
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 19
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 1221
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getScrollX()I

    move-result v0

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconLeftBitmaps:[Landroid/graphics/Bitmap;

    if-nez v1, :cond_c

    const/4 v1, 0x0

    goto :goto_15

    :cond_c
    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterWidth:I

    move-object/from16 v2, p0

    iget v2, v2, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconPadding:I

    add-int/2addr v1, v2

    :goto_15
    add-int v6, v0, v1

    .line 1222
    .local v6, "startX":I
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getScrollX()I

    move-result v0

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconRightBitmaps:[Landroid/graphics/Bitmap;

    if-nez v1, :cond_26

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getWidth()I

    move-result v1

    goto :goto_34

    :cond_26
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getWidth()I

    move-result v1

    move-object/from16 v2, p0

    iget v2, v2, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterWidth:I

    sub-int/2addr v1, v2

    move-object/from16 v2, p0

    iget v2, v2, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconPadding:I

    sub-int/2addr v1, v2

    :goto_34
    add-int v7, v0, v1

    .line 1223
    .local v7, "endX":I
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getScrollY()I

    move-result v0

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getPaddingBottom()I

    move-result v1

    sub-int v8, v0, v1

    .line 1226
    .local v8, "lineStartY":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->paint:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1227
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconLeftBitmaps:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_ad

    .line 1228
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconLeftBitmaps:[Landroid/graphics/Bitmap;

    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isInternalValid()Z

    move-result v1

    if-nez v1, :cond_60

    const/4 v1, 0x3

    goto :goto_71

    :cond_60
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_68

    const/4 v1, 0x2

    goto :goto_71

    :cond_68
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_70

    const/4 v1, 0x1

    goto :goto_71

    :cond_70
    const/4 v1, 0x0

    :goto_71
    aget-object v9, v0, v1

    .line 1229
    .local v9, "icon":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconPadding:I

    sub-int v0, v6, v0

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterWidth:I

    sub-int/2addr v0, v1

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterWidth:I

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int v10, v0, v1

    .line 1230
    .local v10, "iconLeft":I
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomSpacing:I

    add-int/2addr v0, v8

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterHeight:I

    sub-int/2addr v0, v1

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterHeight:I

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int v11, v0, v1

    .line 1231
    .local v11, "iconTop":I
    int-to-float v0, v10

    int-to-float v1, v11

    move-object/from16 v2, p0

    iget-object v2, v2, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->paint:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    invoke-virtual {v3, v9, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1233
    .end local v9    # "icon":Landroid/graphics/Bitmap;
    .end local v10    # "iconLeft":I
    .end local v11    # "iconTop":I
    :cond_ad
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconRightBitmaps:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_106

    .line 1234
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconRightBitmaps:[Landroid/graphics/Bitmap;

    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isInternalValid()Z

    move-result v1

    if-nez v1, :cond_bf

    const/4 v1, 0x3

    goto :goto_d0

    :cond_bf
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_c7

    const/4 v1, 0x2

    goto :goto_d0

    :cond_c7
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_cf

    const/4 v1, 0x1

    goto :goto_d0

    :cond_cf
    const/4 v1, 0x0

    :goto_d0
    aget-object v9, v0, v1

    .line 1235
    .local v9, "icon":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconPadding:I

    add-int/2addr v0, v7

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterWidth:I

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int v10, v0, v1

    .line 1236
    .local v10, "iconRight":I
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomSpacing:I

    add-int/2addr v0, v8

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterHeight:I

    sub-int/2addr v0, v1

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterHeight:I

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int v11, v0, v1

    .line 1237
    .local v11, "iconTop":I
    int-to-float v0, v10

    int-to-float v1, v11

    move-object/from16 v2, p0

    iget-object v2, v2, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->paint:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    invoke-virtual {v3, v9, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1241
    .end local v9    # "icon":Landroid/graphics/Bitmap;
    .end local v10    # "iconRight":I
    .end local v11    # "iconTop":I
    :cond_106
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_168

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->showClearButton:Z

    if-eqz v0, :cond_168

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_168

    .line 1242
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->paint:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1244
    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_12d

    .line 1245
    move v9, v6

    .local v9, "buttonLeft":I
    goto :goto_133

    .line 1247
    .end local v9    # "buttonLeft":I
    :cond_12d
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterWidth:I

    sub-int v9, v7, v0

    .line 1249
    .local v9, "buttonLeft":I
    :goto_133
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->clearButtonBitmaps:[Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    aget-object v10, v0, v1

    .line 1250
    .local v10, "clearButtonBitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterWidth:I

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v9, v0

    .line 1251
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomSpacing:I

    add-int/2addr v0, v8

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterHeight:I

    sub-int/2addr v0, v1

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconOuterHeight:I

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int v11, v0, v1

    .line 1252
    .local v11, "iconTop":I
    int-to-float v0, v9

    int-to-float v1, v11

    move-object/from16 v2, p0

    iget-object v2, v2, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->paint:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    invoke-virtual {v3, v10, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1256
    .end local v9    # "buttonLeft":I
    .end local v10    # "clearButtonBitmap":Landroid/graphics/Bitmap;
    .end local v11    # "iconTop":I
    :cond_168
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->hideUnderline:Z

    if-nez v0, :cond_249

    .line 1257
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomSpacing:I

    add-int/2addr v8, v0

    .line 1258
    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isInternalValid()Z

    move-result v0

    if-nez v0, :cond_19b

    .line 1259
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->paint:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->errorColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1260
    move-object/from16 v0, p1

    int-to-float v1, v6

    int-to-float v2, v8

    int-to-float v3, v7

    move-object/from16 v4, p0

    const/4 v5, 0x2

    invoke-direct {v4, v5}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getPixel(I)I

    move-result v4

    add-int/2addr v4, v8

    int-to-float v4, v4

    move-object/from16 v5, p0

    iget-object v5, v5, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->paint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_249

    .line 1261
    :cond_19b
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1ef

    .line 1262
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->paint:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->underlineColor:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1b1

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->underlineColor:I

    goto :goto_1bc

    :cond_1b1
    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->baseColor:I

    const v2, 0xffffff

    and-int/2addr v1, v2

    const/high16 v2, 0x44000000    # 512.0f

    or-int/2addr v1, v2

    :goto_1bc
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1263
    move-object/from16 v0, p0

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getPixel(I)I

    move-result v0

    int-to-float v9, v0

    .line 1264
    .local v9, "interval":F
    const/4 v10, 0x0

    .local v10, "xOffset":F
    :goto_1c8
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v10, v0

    if-gez v0, :cond_1ee

    .line 1265
    move-object/from16 v0, p1

    int-to-float v1, v6

    add-float/2addr v1, v10

    int-to-float v2, v8

    int-to-float v3, v6

    add-float/2addr v3, v10

    add-float/2addr v3, v9

    move-object/from16 v4, p0

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getPixel(I)I

    move-result v4

    add-int/2addr v4, v8

    int-to-float v4, v4

    move-object/from16 v5, p0

    iget-object v5, v5, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->paint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1264
    const/high16 v0, 0x40400000    # 3.0f

    mul-float/2addr v0, v9

    add-float/2addr v10, v0

    goto :goto_1c8

    .line 1267
    .end local v9    # "interval":F
    .end local v10    # "xOffset":F
    :cond_1ee
    goto :goto_249

    :cond_1ef
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_216

    .line 1268
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->paint:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->primaryColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1269
    move-object/from16 v0, p1

    int-to-float v1, v6

    int-to-float v2, v8

    int-to-float v3, v7

    move-object/from16 v4, p0

    const/4 v5, 0x2

    invoke-direct {v4, v5}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getPixel(I)I

    move-result v4

    add-int/2addr v4, v8

    int-to-float v4, v4

    move-object/from16 v5, p0

    iget-object v5, v5, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->paint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_249

    .line 1271
    :cond_216
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->paint:Landroid/graphics/Paint;

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->underlineColor:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_226

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->underlineColor:I

    goto :goto_231

    :cond_226
    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->baseColor:I

    const v2, 0xffffff

    and-int/2addr v1, v2

    const/high16 v2, 0x1e000000

    or-int/2addr v1, v2

    :goto_231
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1272
    move-object/from16 v0, p1

    int-to-float v1, v6

    int-to-float v2, v8

    int-to-float v3, v7

    move-object/from16 v4, p0

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getPixel(I)I

    move-result v4

    add-int/2addr v4, v8

    int-to-float v4, v4

    move-object/from16 v5, p0

    iget-object v5, v5, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->paint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1276
    :cond_249
    :goto_249
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textPaint:Landroid/text/TextPaint;

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomTextSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1277
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v9

    .line 1278
    .local v9, "textMetrics":Landroid/graphics/Paint$FontMetrics;
    iget v0, v9, Landroid/graphics/Paint$FontMetrics;->ascent:F

    neg-float v0, v0

    iget v1, v9, Landroid/graphics/Paint$FontMetrics;->descent:F

    sub-float v10, v0, v1

    .line 1279
    .local v10, "relativeHeight":F
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomTextSize:I

    int-to-float v0, v0

    iget v1, v9, Landroid/graphics/Paint$FontMetrics;->ascent:F

    add-float/2addr v0, v1

    iget v1, v9, Landroid/graphics/Paint$FontMetrics;->descent:F

    add-float v11, v0, v1

    .line 1282
    .local v11, "bottomTextPadding":F
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_27c

    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->hasCharactersCounter()Z

    move-result v0

    if-nez v0, :cond_282

    :cond_27c
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isCharactersCountValid()Z

    move-result v0

    if-nez v0, :cond_2c5

    .line 1283
    :cond_282
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textPaint:Landroid/text/TextPaint;

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isCharactersCountValid()Z

    move-result v1

    if-eqz v1, :cond_298

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->baseColor:I

    const v2, 0xffffff

    and-int/2addr v1, v2

    const/high16 v2, 0x44000000    # 512.0f

    or-int/2addr v1, v2

    goto :goto_29c

    :cond_298
    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->errorColor:I

    :goto_29c
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 1284
    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getCharactersCounterText()Ljava/lang/String;

    move-result-object v12

    .line 1285
    .local v12, "charactersCounterText":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_2ab

    int-to-float v0, v6

    goto :goto_2b5

    :cond_2ab
    int-to-float v0, v7

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v12}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v1

    sub-float/2addr v0, v1

    :goto_2b5
    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomSpacing:I

    add-int/2addr v1, v8

    int-to-float v1, v1

    add-float/2addr v1, v10

    move-object/from16 v2, p0

    iget-object v2, v2, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textPaint:Landroid/text/TextPaint;

    move-object/from16 v3, p1

    invoke-virtual {v3, v12, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1289
    .end local v12    # "charactersCounterText":Ljava/lang/String;
    :cond_2c5
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_34f

    .line 1290
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->tempErrorText:Ljava/lang/String;

    if-nez v0, :cond_2e7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->helperTextAlwaysShown:Z

    if-nez v0, :cond_2dd

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_34f

    :cond_2dd
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->helperText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_34f

    .line 1291
    :cond_2e7
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textPaint:Landroid/text/TextPaint;

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->tempErrorText:Ljava/lang/String;

    if-eqz v1, :cond_2f6

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->errorColor:I

    goto :goto_30d

    :cond_2f6
    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->helperTextColor:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_302

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->helperTextColor:I

    goto :goto_30d

    :cond_302
    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->baseColor:I

    const v2, 0xffffff

    and-int/2addr v1, v2

    const/high16 v2, 0x44000000    # 512.0f

    or-int/2addr v1, v2

    :goto_30d
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 1292
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 1293
    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_331

    .line 1294
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getWidth()I

    move-result v0

    sub-int v0, v7, v0

    int-to-float v0, v0

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomSpacing:I

    add-int/2addr v1, v8

    int-to-float v1, v1

    sub-float/2addr v1, v11

    move-object/from16 v2, p1

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_343

    .line 1296
    :cond_331
    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getBottomTextLeftOffset()I

    move-result v0

    add-int/2addr v0, v6

    int-to-float v0, v0

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomSpacing:I

    add-int/2addr v1, v8

    int-to-float v1, v1

    sub-float/2addr v1, v11

    move-object/from16 v2, p1

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1298
    :goto_343
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textLayout:Landroid/text/StaticLayout;

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1299
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 1304
    :cond_34f
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelEnabled:Z

    if-eqz v0, :cond_46e

    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelText:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_46e

    .line 1305
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textPaint:Landroid/text/TextPaint;

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelTextSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1307
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textPaint:Landroid/text/TextPaint;

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->focusEvaluator:Lcom/nineoldandroids/animation/ArgbEvaluator;

    move-object/from16 v2, p0

    iget v2, v2, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->focusFraction:F

    move-object/from16 v3, p0

    iget v3, v3, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelTextColor:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_383

    move-object/from16 v3, p0

    iget v3, v3, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelTextColor:I

    goto :goto_38e

    :cond_383
    move-object/from16 v3, p0

    iget v3, v3, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->baseColor:I

    const v4, 0xffffff

    and-int/2addr v3, v4

    const/high16 v4, 0x44000000    # 512.0f

    or-int/2addr v3, v4

    :goto_38e
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v4, p0

    iget v4, v4, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->primaryColor:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/nineoldandroids/animation/ArgbEvaluator;->evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 1310
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textPaint:Landroid/text/TextPaint;

    move-object/from16 v1, p0

    iget-object v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelText:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v12

    .line 1312
    .local v12, "floatingLabelWidth":F
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getGravity()I

    move-result v0

    and-int/lit8 v0, v0, 0x5

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3c6

    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_3ca

    .line 1313
    :cond_3c6
    int-to-float v0, v7

    sub-float/2addr v0, v12

    float-to-int v13, v0

    .local v13, "floatingLabelStartX":I
    goto :goto_3f1

    .line 1314
    .end local v13    # "floatingLabelStartX":I
    :cond_3ca
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getGravity()I

    move-result v0

    and-int/lit8 v0, v0, 0x3

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3d5

    .line 1315
    move v13, v6

    .local v13, "floatingLabelStartX":I
    goto :goto_3f1

    .line 1317
    .end local v13    # "floatingLabelStartX":I
    :cond_3d5
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getInnerPaddingLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getWidth()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getInnerPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getInnerPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    sub-float/2addr v1, v12

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    add-int v13, v6, v0

    .line 1321
    .local v13, "floatingLabelStartX":I
    :goto_3f1
    move-object/from16 v0, p0

    iget v14, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelPadding:I

    .line 1322
    .local v14, "distance":I
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->innerPaddingTop:I

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelTextSize:I

    add-int/2addr v0, v1

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelPadding:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    int-to-float v1, v14

    move-object/from16 v2, p0

    iget-boolean v2, v2, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelAlwaysShown:Z

    if-eqz v2, :cond_40e

    const/high16 v2, 0x3f800000    # 1.0f

    goto :goto_412

    :cond_40e
    move-object/from16 v2, p0

    iget v2, v2, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelFraction:F

    :goto_412
    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getScrollY()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    float-to-int v15, v0

    .line 1325
    .local v15, "floatingLabelStartY":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelAlwaysShown:Z

    if-eqz v0, :cond_424

    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_428

    :cond_424
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelFraction:F

    :goto_428
    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v0, v1

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->focusFraction:F

    const v2, 0x3f3d70a4    # 0.74f

    mul-float/2addr v1, v2

    const v2, 0x3e851eb8    # 0.26f

    add-float/2addr v1, v2

    mul-float/2addr v0, v1

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelTextColor:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_442

    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_44e

    :cond_442
    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelTextColor:I

    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x43800000    # 256.0f

    div-float/2addr v1, v2

    :goto_44e
    mul-float/2addr v0, v1

    float-to-int v1, v0

    move/from16 v16, v1

    .line 1326
    .local v16, "alpha":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textPaint:Landroid/text/TextPaint;

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 1329
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelText:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    int-to-float v1, v13

    int-to-float v2, v15

    move-object/from16 v3, p0

    iget-object v3, v3, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textPaint:Landroid/text/TextPaint;

    move-object/from16 v4, p1

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1333
    .end local v12    # "floatingLabelWidth":F
    .end local v13    # "floatingLabelStartX":I
    .end local v14    # "distance":I
    .end local v15    # "floatingLabelStartY":I
    .end local v16    # "alpha":I
    :cond_46e
    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_515

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->singleLineEllipsis:Z

    if-eqz v0, :cond_515

    invoke-virtual/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getScrollX()I

    move-result v0

    if-eqz v0, :cond_515

    .line 1334
    move-object/from16 v0, p0

    iget-object v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->paint:Landroid/graphics/Paint;

    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isInternalValid()Z

    move-result v1

    if-eqz v1, :cond_48f

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->primaryColor:I

    goto :goto_493

    :cond_48f
    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->errorColor:I

    :goto_493
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1335
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomSpacing:I

    add-int/2addr v0, v8

    int-to-float v12, v0

    .line 1337
    .local v12, "startY":F
    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_4a4

    .line 1338
    move v13, v7

    .local v13, "ellipsisStartX":I
    goto :goto_4a5

    .line 1340
    .end local v13    # "ellipsisStartX":I
    :cond_4a4
    move v13, v6

    .line 1342
    .local v13, "ellipsisStartX":I
    :goto_4a5
    invoke-direct/range {p0 .. p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isRTL()Z

    move-result v0

    if-eqz v0, :cond_4ad

    const/4 v14, -0x1

    goto :goto_4ae

    :cond_4ad
    const/4 v14, 0x1

    .line 1343
    .local v14, "signum":I
    :goto_4ae
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomEllipsisSize:I

    mul-int/2addr v0, v14

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v13

    int-to-float v0, v0

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomEllipsisSize:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v1, v12

    move-object/from16 v2, p0

    iget v2, v2, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomEllipsisSize:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    move-object/from16 v3, p0

    iget-object v3, v3, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->paint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1344
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomEllipsisSize:I

    mul-int/2addr v0, v14

    mul-int/lit8 v0, v0, 0x5

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v13

    int-to-float v0, v0

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomEllipsisSize:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v1, v12

    move-object/from16 v2, p0

    iget v2, v2, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomEllipsisSize:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    move-object/from16 v3, p0

    iget-object v3, v3, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->paint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1345
    move-object/from16 v0, p0

    iget v0, v0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomEllipsisSize:I

    mul-int/2addr v0, v14

    mul-int/lit8 v0, v0, 0x9

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v13

    int-to-float v0, v0

    move-object/from16 v1, p0

    iget v1, v1, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomEllipsisSize:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v1, v12

    move-object/from16 v2, p0

    iget v2, v2, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomEllipsisSize:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    move-object/from16 v3, p0

    iget-object v3, v3, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->paint:Landroid/graphics/Paint;

    move-object/from16 v4, p1

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1349
    .end local v12    # "startY":F
    .end local v13    # "ellipsisStartX":I
    .end local v14    # "signum":I
    :cond_515
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-super {v0, v1}, Landroid/support/v7/widget/AppCompatMultiAutoCompleteTextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 1350
    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 7
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .line 748
    invoke-super/range {p0 .. p5}, Landroid/support/v7/widget/AppCompatMultiAutoCompleteTextView;->onLayout(ZIIII)V

    .line 749
    if-eqz p1, :cond_8

    .line 750
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->adjustBottomLines()Z

    .line 752
    :cond_8
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .registers 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 1409
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->singleLineEllipsis:Z

    if-eqz v0, :cond_48

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getScrollX()I

    move-result v0

    if-lez v0, :cond_48

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_48

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    const/16 v1, 0x14

    invoke-direct {p0, v1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getPixel(I)I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_48

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getHeight()I

    move-result v1

    iget v2, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->extraPaddingBottom:I

    sub-int/2addr v1, v2

    iget v2, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->innerPaddingBottom:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_48

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getHeight()I

    move-result v1

    iget v2, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->innerPaddingBottom:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_48

    .line 1410
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->setSelection(I)V

    .line 1411
    const/4 v0, 0x0

    return v0

    .line 1413
    :cond_48
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_a4

    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->showClearButton:Z

    if-eqz v0, :cond_a4

    .line 1414
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_aa

    goto/16 :goto_a4

    .line 1416
    :pswitch_5b
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->insideClearButton(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_69

    .line 1417
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->clearButtonTouched:Z

    .line 1418
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->clearButtonClicking:Z

    .line 1419
    const/4 v0, 0x1

    return v0

    .line 1422
    :cond_69
    :pswitch_69
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->clearButtonClicking:Z

    if-eqz v0, :cond_76

    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->insideClearButton(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_76

    .line 1423
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->clearButtonClicking:Z

    .line 1425
    :cond_76
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->clearButtonTouched:Z

    if-eqz v0, :cond_a4

    .line 1426
    const/4 v0, 0x1

    return v0

    .line 1430
    :pswitch_7c
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->clearButtonClicking:Z

    if-eqz v0, :cond_91

    .line 1431
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8e

    .line 1432
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1434
    :cond_8e
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->clearButtonClicking:Z

    .line 1436
    :cond_91
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->clearButtonTouched:Z

    if-eqz v0, :cond_9a

    .line 1437
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->clearButtonTouched:Z

    .line 1438
    const/4 v0, 0x1

    return v0

    .line 1440
    :cond_9a
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->clearButtonTouched:Z

    .line 1441
    goto :goto_a4

    .line 1443
    :pswitch_9e
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->clearButtonTouched:Z

    .line 1444
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->clearButtonClicking:Z

    .line 1448
    :cond_a4
    :goto_a4
    invoke-super {p0, p1}, Landroid/support/v7/widget/AppCompatMultiAutoCompleteTextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    nop

    :pswitch_data_aa
    .packed-switch 0x0
        :pswitch_5b
        :pswitch_7c
        :pswitch_69
        :pswitch_9e
    .end packed-switch
.end method

.method public setAccentTypeface(Landroid/graphics/Typeface;)V
    .registers 3
    .param p1, "accentTypeface"    # Landroid/graphics/Typeface;

    .line 596
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->accentTypeface:Landroid/graphics/Typeface;

    .line 597
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 598
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->postInvalidate()V

    .line 599
    return-void
.end method

.method public setAutoValidate(Z)V
    .registers 3
    .param p1, "autoValidate"    # Z

    .line 1016
    iput-boolean p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->autoValidate:Z

    .line 1017
    if-eqz p1, :cond_7

    .line 1018
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->validate()Z

    .line 1020
    :cond_7
    return-void
.end method

.method public setBaseColor(I)V
    .registers 3
    .param p1, "color"    # I

    .line 865
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->baseColor:I

    if-eq v0, p1, :cond_6

    .line 866
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->baseColor:I

    .line 869
    :cond_6
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initText()V

    .line 871
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->postInvalidate()V

    .line 872
    return-void
.end method

.method public setBottomTextSize(I)V
    .registers 2
    .param p1, "size"    # I

    .line 674
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->bottomTextSize:I

    .line 675
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initPadding()V

    .line 676
    return-void
.end method

.method public setCurrentBottomLines(F)V
    .registers 2
    .param p1, "currentBottomLines"    # F

    .line 565
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->currentBottomLines:F

    .line 566
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initPadding()V

    .line 567
    return-void
.end method

.method public setError(Ljava/lang/CharSequence;)V
    .registers 3
    .param p1, "errorText"    # Ljava/lang/CharSequence;

    .line 1058
    if-nez p1, :cond_4

    const/4 v0, 0x0

    goto :goto_8

    :cond_4
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_8
    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->tempErrorText:Ljava/lang/String;

    .line 1059
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->adjustBottomLines()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1060
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->postInvalidate()V

    .line 1062
    :cond_13
    return-void
.end method

.method public setErrorColor(I)V
    .registers 2
    .param p1, "color"    # I

    .line 1027
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->errorColor:I

    .line 1028
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->postInvalidate()V

    .line 1029
    return-void
.end method

.method public setFloatingLabel(I)V
    .registers 2
    .param p1, "mode"    # I
        .annotation build Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView$FloatingLabelType;
        .end annotation
    .end param

    .line 946
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->setFloatingLabelInternal(I)V

    .line 947
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initPadding()V

    .line 948
    return-void
.end method

.method public setFloatingLabelAlwaysShown(Z)V
    .registers 2
    .param p1, "floatingLabelAlwaysShown"    # Z

    .line 574
    iput-boolean p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelAlwaysShown:Z

    .line 575
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->invalidate()V

    .line 576
    return-void
.end method

.method public setFloatingLabelAnimating(Z)V
    .registers 2
    .param p1, "animating"    # Z

    .line 964
    iput-boolean p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelAnimating:Z

    .line 965
    return-void
.end method

.method public setFloatingLabelFraction(F)V
    .registers 2
    .param p1, "floatingLabelFraction"    # F

    .line 547
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelFraction:F

    .line 548
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->invalidate()V

    .line 549
    return-void
.end method

.method public setFloatingLabelPadding(I)V
    .registers 2
    .param p1, "padding"    # I

    .line 955
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelPadding:I

    .line 956
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->postInvalidate()V

    .line 957
    return-void
.end method

.method public setFloatingLabelText(Ljava/lang/CharSequence;)V
    .registers 3
    .param p1, "floatingLabelText"    # Ljava/lang/CharSequence;

    .line 647
    if-nez p1, :cond_7

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_8

    :cond_7
    move-object v0, p1

    :goto_8
    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelText:Ljava/lang/CharSequence;

    .line 648
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->postInvalidate()V

    .line 649
    return-void
.end method

.method public setFloatingLabelTextColor(I)V
    .registers 2
    .param p1, "color"    # I

    .line 665
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelTextColor:I

    .line 666
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->postInvalidate()V

    .line 667
    return-void
.end method

.method public setFloatingLabelTextSize(I)V
    .registers 2
    .param p1, "size"    # I

    .line 656
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->floatingLabelTextSize:I

    .line 657
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initPadding()V

    .line 658
    return-void
.end method

.method public setFocusFraction(F)V
    .registers 2
    .param p1, "focusFraction"    # F

    .line 556
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->focusFraction:F

    .line 557
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->invalidate()V

    .line 558
    return-void
.end method

.method public setHelperText(Ljava/lang/CharSequence;)V
    .registers 3
    .param p1, "helperText"    # Ljava/lang/CharSequence;

    .line 1036
    if-nez p1, :cond_4

    const/4 v0, 0x0

    goto :goto_8

    :cond_4
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_8
    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->helperText:Ljava/lang/String;

    .line 1037
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->adjustBottomLines()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1038
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->postInvalidate()V

    .line 1040
    :cond_13
    return-void
.end method

.method public setHelperTextAlwaysShown(Z)V
    .registers 2
    .param p1, "helperTextAlwaysShown"    # Z

    .line 583
    iput-boolean p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->helperTextAlwaysShown:Z

    .line 584
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->invalidate()V

    .line 585
    return-void
.end method

.method public setHelperTextColor(I)V
    .registers 2
    .param p1, "color"    # I

    .line 1047
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->helperTextColor:I

    .line 1048
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->postInvalidate()V

    .line 1049
    return-void
.end method

.method public setHideUnderline(Z)V
    .registers 2
    .param p1, "hideUnderline"    # Z

    .line 613
    iput-boolean p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->hideUnderline:Z

    .line 614
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initPadding()V

    .line 615
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->postInvalidate()V

    .line 616
    return-void
.end method

.method public setIconLeft(I)V
    .registers 3
    .param p1, "res"    # I

    .line 438
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->generateIconBitmaps(I)[Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconLeftBitmaps:[Landroid/graphics/Bitmap;

    .line 439
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initPadding()V

    .line 440
    return-void
.end method

.method public setIconLeft(Landroid/graphics/Bitmap;)V
    .registers 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .line 448
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->generateIconBitmaps(Landroid/graphics/Bitmap;)[Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconLeftBitmaps:[Landroid/graphics/Bitmap;

    .line 449
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initPadding()V

    .line 450
    return-void
.end method

.method public setIconLeft(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .line 443
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->generateIconBitmaps(Landroid/graphics/drawable/Drawable;)[Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconLeftBitmaps:[Landroid/graphics/Bitmap;

    .line 444
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initPadding()V

    .line 445
    return-void
.end method

.method public setIconRight(I)V
    .registers 3
    .param p1, "res"    # I

    .line 453
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->generateIconBitmaps(I)[Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconRightBitmaps:[Landroid/graphics/Bitmap;

    .line 454
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initPadding()V

    .line 455
    return-void
.end method

.method public setIconRight(Landroid/graphics/Bitmap;)V
    .registers 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .line 463
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->generateIconBitmaps(Landroid/graphics/Bitmap;)[Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconRightBitmaps:[Landroid/graphics/Bitmap;

    .line 464
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initPadding()V

    .line 465
    return-void
.end method

.method public setIconRight(Landroid/graphics/drawable/Drawable;)V
    .registers 3
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .line 458
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->generateIconBitmaps(Landroid/graphics/drawable/Drawable;)[Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->iconRightBitmaps:[Landroid/graphics/Bitmap;

    .line 459
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initPadding()V

    .line 460
    return-void
.end method

.method public setLengthChecker(Lbr/com/itau/widgets/material/validation/METLengthChecker;)V
    .registers 2
    .param p1, "lengthChecker"    # Lbr/com/itau/widgets/material/validation/METLengthChecker;

    .line 1182
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->lengthChecker:Lbr/com/itau/widgets/material/validation/METLengthChecker;

    .line 1183
    return-void
.end method

.method public setMaxCharacters(I)V
    .registers 2
    .param p1, "max"    # I

    .line 983
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->maxCharacters:I

    .line 984
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initMinBottomLines()V

    .line 985
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initPadding()V

    .line 986
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->postInvalidate()V

    .line 987
    return-void
.end method

.method public setMetHintTextColor(I)V
    .registers 3
    .param p1, "color"    # I

    .line 908
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textColorHintStateList:Landroid/content/res/ColorStateList;

    .line 909
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->resetHintTextColor()V

    .line 910
    return-void
.end method

.method public setMetHintTextColor(Landroid/content/res/ColorStateList;)V
    .registers 2
    .param p1, "colors"    # Landroid/content/res/ColorStateList;

    .line 916
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textColorHintStateList:Landroid/content/res/ColorStateList;

    .line 917
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->resetHintTextColor()V

    .line 918
    return-void
.end method

.method public setMetTextColor(I)V
    .registers 3
    .param p1, "color"    # I

    .line 883
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textColorStateList:Landroid/content/res/ColorStateList;

    .line 884
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->resetTextColor()V

    .line 885
    return-void
.end method

.method public setMetTextColor(Landroid/content/res/ColorStateList;)V
    .registers 2
    .param p1, "colors"    # Landroid/content/res/ColorStateList;

    .line 891
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->textColorStateList:Landroid/content/res/ColorStateList;

    .line 892
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->resetTextColor()V

    .line 893
    return-void
.end method

.method public setMinBottomTextLines(I)V
    .registers 2
    .param p1, "lines"    # I

    .line 1005
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->minBottomTextLines:I

    .line 1006
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initMinBottomLines()V

    .line 1007
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initPadding()V

    .line 1008
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->postInvalidate()V

    .line 1009
    return-void
.end method

.method public setMinCharacters(I)V
    .registers 2
    .param p1, "min"    # I

    .line 994
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->minCharacters:I

    .line 995
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initMinBottomLines()V

    .line 996
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initPadding()V

    .line 997
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->postInvalidate()V

    .line 998
    return-void
.end method

.method public setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .registers 3
    .param p1, "listener"    # Landroid/view/View$OnFocusChangeListener;

    .line 1187
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->innerFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    if-nez v0, :cond_8

    .line 1188
    invoke-super {p0, p1}, Landroid/support/v7/widget/AppCompatMultiAutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    goto :goto_a

    .line 1190
    :cond_8
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->outerFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    .line 1192
    :goto_a
    return-void
.end method

.method public final setPadding(IIII)V
    .registers 5
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 706
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/widget/AppCompatMultiAutoCompleteTextView;->setPadding(IIII)V

    .line 707
    return-void
.end method

.method public setPaddings(IIII)V
    .registers 5
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .line 713
    iput p2, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->innerPaddingTop:I

    .line 714
    iput p4, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->innerPaddingBottom:I

    .line 715
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->innerPaddingLeft:I

    .line 716
    iput p3, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->innerPaddingRight:I

    .line 717
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->correctPaddings()V

    .line 718
    return-void
.end method

.method public setPrimaryColor(I)V
    .registers 2
    .param p1, "color"    # I

    .line 875
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->primaryColor:I

    .line 876
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->postInvalidate()V

    .line 877
    return-void
.end method

.method public setShowClearButton(Z)V
    .registers 2
    .param p1, "show"    # Z

    .line 472
    iput-boolean p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->showClearButton:Z

    .line 473
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->correctPaddings()V

    .line 474
    return-void
.end method

.method public setSingleLineEllipsis()V
    .registers 2

    .line 968
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->setSingleLineEllipsis(Z)V

    .line 969
    return-void
.end method

.method public setSingleLineEllipsis(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 972
    iput-boolean p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->singleLineEllipsis:Z

    .line 973
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initMinBottomLines()V

    .line 974
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->initPadding()V

    .line 975
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->postInvalidate()V

    .line 976
    return-void
.end method

.method public setUnderlineColor(I)V
    .registers 2
    .param p1, "color"    # I

    .line 631
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->underlineColor:I

    .line 632
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->postInvalidate()V

    .line 633
    return-void
.end method

.method public setValidateOnFocusLost(Z)V
    .registers 2
    .param p1, "validate"    # Z

    .line 861
    iput-boolean p1, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->validateOnFocusLost:Z

    .line 862
    return-void
.end method

.method public validate()Z
    .registers 7

    .line 1126
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->validators:Ljava/util/List;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->validators:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1127
    :cond_c
    const/4 v0, 0x1

    return v0

    .line 1130
    :cond_e
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 1131
    .local v1, "text":Ljava/lang/CharSequence;
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1a

    const/4 v2, 0x1

    goto :goto_1b

    :cond_1a
    const/4 v2, 0x0

    .line 1133
    .local v2, "isEmpty":Z
    :goto_1b
    const/4 v3, 0x1

    .line 1134
    .local v3, "isValid":Z
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->validators:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_22
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_45

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lbr/com/itau/widgets/material/validation/METValidator;

    .line 1136
    .local v5, "validator":Lbr/com/itau/widgets/material/validation/METValidator;
    if-eqz v3, :cond_39

    invoke-virtual {v5, v1, v2}, Lbr/com/itau/widgets/material/validation/METValidator;->isValid(Ljava/lang/CharSequence;Z)Z

    move-result v0

    if-eqz v0, :cond_39

    const/4 v3, 0x1

    goto :goto_3a

    :cond_39
    const/4 v3, 0x0

    .line 1137
    :goto_3a
    if-nez v3, :cond_44

    .line 1138
    invoke-virtual {v5}, Lbr/com/itau/widgets/material/validation/METValidator;->getErrorMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->setError(Ljava/lang/CharSequence;)V

    .line 1139
    goto :goto_45

    .line 1141
    .end local v5    # "validator":Lbr/com/itau/widgets/material/validation/METValidator;
    :cond_44
    goto :goto_22

    .line 1142
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_45
    :goto_45
    if-eqz v3, :cond_4b

    .line 1143
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->setError(Ljava/lang/CharSequence;)V

    .line 1146
    :cond_4b
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->postInvalidate()V

    .line 1147
    return v3
.end method

.method public validate(Ljava/lang/String;Ljava/lang/CharSequence;)Z
    .registers 4
    .param p1, "regex"    # Ljava/lang/String;
    .param p2, "errorText"    # Ljava/lang/CharSequence;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1094
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->isValid(Ljava/lang/String;)Z

    move-result v0

    .line 1095
    .local v0, "isValid":Z
    if-nez v0, :cond_9

    .line 1096
    invoke-virtual {p0, p2}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->setError(Ljava/lang/CharSequence;)V

    .line 1098
    :cond_9
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->postInvalidate()V

    .line 1099
    return v0
.end method

.method public validateWith(Lbr/com/itau/widgets/material/validation/METValidator;)Z
    .registers 5
    .param p1, "validator"    # Lbr/com/itau/widgets/material/validation/METValidator;

    .line 1109
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 1110
    .local v1, "text":Ljava/lang/CharSequence;
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x1

    goto :goto_d

    :cond_c
    const/4 v0, 0x0

    :goto_d
    invoke-virtual {p1, v1, v0}, Lbr/com/itau/widgets/material/validation/METValidator;->isValid(Ljava/lang/CharSequence;Z)Z

    move-result v2

    .line 1111
    .local v2, "isValid":Z
    if-nez v2, :cond_1a

    .line 1112
    invoke-virtual {p1}, Lbr/com/itau/widgets/material/validation/METValidator;->getErrorMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->setError(Ljava/lang/CharSequence;)V

    .line 1114
    :cond_1a
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialMultiAutoCompleteTextView;->postInvalidate()V

    .line 1115
    return v2
.end method

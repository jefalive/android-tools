.class Lcom/itau/empresas/feature/mapa/MapaFragment$1;
.super Ljava/lang/Object;
.source "MapaFragment.java"

# interfaces
.implements Lcom/google/android/gms/maps/GoogleMap$OnMyLocationButtonClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/mapa/MapaFragment;->onMapReady(Lcom/google/android/gms/maps/GoogleMap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/mapa/MapaFragment;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/mapa/MapaFragment;

    .line 199
    iput-object p1, p0, Lcom/itau/empresas/feature/mapa/MapaFragment$1;->this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMyLocationButtonClick()Z
    .registers 4

    .line 203
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment$1;->this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;

    .line 204
    invoke-virtual {v0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 205
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionShowLocation()[Ljava/lang/String;

    move-result-object v1

    .line 204
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/PermissoesUtil;->hasSelfPermissions(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2b

    .line 207
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment$1;->this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;

    .line 208
    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionShowLocation()[Ljava/lang/String;

    move-result-object v1

    .line 207
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/PermissoesUtil;->shouldShowRequestPermissionRationale(Landroid/support/v4/app/Fragment;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 209
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment$1;->this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;

    # invokes: Lcom/itau/empresas/feature/mapa/MapaFragment;->exibeMensagem()V
    invoke-static {v0}, Lcom/itau/empresas/feature/mapa/MapaFragment;->access$100(Lcom/itau/empresas/feature/mapa/MapaFragment;)V

    .line 212
    :cond_21
    iget-object v0, p0, Lcom/itau/empresas/feature/mapa/MapaFragment$1;->this$0:Lcom/itau/empresas/feature/mapa/MapaFragment;

    invoke-static {}, Lcom/itau/empresas/ui/util/PermissoesUtil;->getPermissionShowLocation()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/mapa/MapaFragment;->requestPermissions([Ljava/lang/String;I)V

    .line 215
    :cond_2b
    const/4 v0, 0x0

    return v0
.end method

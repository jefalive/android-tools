.class public final Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:Ljava/lang/Object;>Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private code:I

.field private final data:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private headers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lbr/com/itau/sdk/android/core/model/KeyValue;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/util/List;)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Ljava/util/List<Lbr/com/itau/sdk/android/core/model/KeyValue;>;)V"
        }
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p2, p0, Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;->headers:Ljava/util/List;

    .line 15
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;->data:Ljava/lang/Object;

    .line 16
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Ljava/util/List;I)V
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Ljava/util/List<Lbr/com/itau/sdk/android/core/model/KeyValue;>;I)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p2, p0, Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;->headers:Ljava/util/List;

    .line 20
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;->data:Ljava/lang/Object;

    .line 21
    iput p3, p0, Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;->code:I

    .line 22
    return-void
.end method


# virtual methods
.method public getCode()I
    .registers 2

    .line 33
    iget v0, p0, Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;->code:I

    return v0
.end method

.method public getData()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 29
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;->data:Ljava/lang/Object;

    return-object v0
.end method

.method public getHeaders()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lbr/com/itau/sdk/android/core/model/KeyValue;>;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/endpoint/http/RouterResponse;->headers:Ljava/util/List;

    return-object v0
.end method

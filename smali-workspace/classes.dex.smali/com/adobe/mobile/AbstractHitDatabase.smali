.class abstract Lcom/adobe/mobile/AbstractHitDatabase;
.super Lcom/adobe/mobile/AbstractDatabaseBacking;
.source "AbstractHitDatabase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adobe/mobile/AbstractHitDatabase$ReferrerTimeoutTask;,
        Lcom/adobe/mobile/AbstractHitDatabase$Hit;
    }
.end annotation


# instance fields
.field protected final backgroundMutex:Ljava/lang/Object;

.field protected bgThreadActive:Z

.field protected dbCreateStatement:Ljava/lang/String;

.field protected lastHitTimestamp:J

.field protected numberOfUnsentHits:J

.field private referrerTask:Ljava/util/TimerTask;

.field private referrerTimer:Ljava/util/Timer;

.field private final referrerTimerMutex:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .registers 2

    .line 16
    invoke-direct {p0}, Lcom/adobe/mobile/AbstractDatabaseBacking;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/adobe/mobile/AbstractHitDatabase;->bgThreadActive:Z

    .line 25
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/adobe/mobile/AbstractHitDatabase;->backgroundMutex:Ljava/lang/Object;

    .line 26
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/adobe/mobile/AbstractHitDatabase;->referrerTimerMutex:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method protected bringOnline()V
    .registers 6

    .line 107
    iget-boolean v0, p0, Lcom/adobe/mobile/AbstractHitDatabase;->bgThreadActive:Z

    if-nez v0, :cond_1d

    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/adobe/mobile/AbstractHitDatabase;->bgThreadActive:Z

    .line 110
    iget-object v3, p0, Lcom/adobe/mobile/AbstractHitDatabase;->backgroundMutex:Ljava/lang/Object;

    monitor-enter v3

    .line 111
    :try_start_a
    new-instance v0, Ljava/lang/Thread;

    invoke-virtual {p0}, Lcom/adobe/mobile/AbstractHitDatabase;->workerThread()Ljava/lang/Runnable;

    move-result-object v1

    const-string v2, "ADBMobileBackgroundThread"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_18
    .catchall {:try_start_a .. :try_end_18} :catchall_1a

    .line 112
    monitor-exit v3

    goto :goto_1d

    :catchall_1a
    move-exception v4

    monitor-exit v3

    throw v4

    .line 114
    :cond_1d
    :goto_1d
    return-void
.end method

.method protected clearTrackingQueue()V
    .registers 8

    .line 87
    iget-object v4, p0, Lcom/adobe/mobile/AbstractHitDatabase;->dbMutex:Ljava/lang/Object;

    monitor-enter v4

    .line 89
    :try_start_3
    iget-object v0, p0, Lcom/adobe/mobile/AbstractHitDatabase;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "HITS"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 90
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/adobe/mobile/AbstractHitDatabase;->numberOfUnsentHits:J
    :try_end_10
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_10} :catch_11
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_10} :catch_27
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_10} :catch_3d
    .catchall {:try_start_3 .. :try_end_10} :catchall_54

    .line 100
    goto :goto_52

    .line 92
    :catch_11
    move-exception v5

    .line 93
    .local v5, "x":Ljava/lang/NullPointerException;
    const-string v0, "%s - Unable to clear tracking queue due to an unopened database (%s)"

    const/4 v1, 0x2

    :try_start_15
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AbstractHitDatabase;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v5}, Ljava/lang/NullPointerException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    .end local v5    # "x":Ljava/lang/NullPointerException;
    goto :goto_52

    .line 95
    :catch_27
    move-exception v5

    .line 96
    .local v5, "x":Landroid/database/SQLException;
    const-string v0, "%s - Unable to clear tracking queue due to a sql error (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AbstractHitDatabase;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v5}, Landroid/database/SQLException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    .end local v5    # "x":Landroid/database/SQLException;
    goto :goto_52

    .line 98
    :catch_3d
    move-exception v5

    .line 99
    .local v5, "x":Ljava/lang/Exception;
    const-string v0, "%s - Unable to clear tracking queue due to an unexpected error (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AbstractHitDatabase;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v5}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_52
    .catchall {:try_start_15 .. :try_end_52} :catchall_54

    .line 101
    .end local v5    # "x":Ljava/lang/Exception;
    :goto_52
    monitor-exit v4

    goto :goto_57

    :catchall_54
    move-exception v6

    monitor-exit v4

    throw v6

    .line 102
    :goto_57
    return-void
.end method

.method protected deleteHit(Ljava/lang/String;)V
    .registers 10
    .param p1, "identifier"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException;
        }
    .end annotation

    .line 62
    if-eqz p1, :cond_c

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1a

    .line 63
    :cond_c
    const-string v0, "%s - Unable to delete hit due to an invalid parameter"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AbstractHitDatabase;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    return-void

    .line 67
    :cond_1a
    iget-object v5, p0, Lcom/adobe/mobile/AbstractHitDatabase;->dbMutex:Ljava/lang/Object;

    monitor-enter v5

    .line 69
    :try_start_1d
    iget-object v0, p0, Lcom/adobe/mobile/AbstractHitDatabase;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "HITS"

    const-string v2, "ID = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 70
    iget-wide v0, p0, Lcom/adobe/mobile/AbstractHitDatabase;->numberOfUnsentHits:J

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/adobe/mobile/AbstractHitDatabase;->numberOfUnsentHits:J
    :try_end_33
    .catch Ljava/lang/NullPointerException; {:try_start_1d .. :try_end_33} :catch_35
    .catch Landroid/database/SQLException; {:try_start_1d .. :try_end_33} :catch_4b
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_33} :catch_83
    .catchall {:try_start_1d .. :try_end_33} :catchall_bd

    .line 82
    goto/16 :goto_bb

    .line 72
    :catch_35
    move-exception v6

    .line 73
    .local v6, "x":Ljava/lang/NullPointerException;
    const-string v0, "%s - Unable to delete hit due to an unopened database (%s)"

    const/4 v1, 0x2

    :try_start_39
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AbstractHitDatabase;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v6}, Ljava/lang/NullPointerException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 82
    .end local v6    # "x":Ljava/lang/NullPointerException;
    goto :goto_bb

    .line 75
    :catch_4b
    move-exception v6

    .line 76
    .local v6, "x":Landroid/database/SQLException;
    const-string v0, "%s - Unable to delete hit due to a sql error (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AbstractHitDatabase;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v6}, Landroid/database/SQLException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    new-instance v0, Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to delete, database probably corrupted ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Landroid/database/SQLException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    .end local v6    # "x":Landroid/database/SQLException;
    :catch_83
    move-exception v6

    .line 80
    .local v6, "x":Ljava/lang/Exception;
    const-string v0, "%s - Unable to delete hit due to an unexpected error (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AbstractHitDatabase;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v6}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    new-instance v0, Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected exception, database probably corrupted ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/adobe/mobile/AbstractDatabaseBacking$CorruptedDatabaseException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_bb
    .catchall {:try_start_39 .. :try_end_bb} :catchall_bd

    .line 83
    .end local v6    # "x":Ljava/lang/Exception;
    :goto_bb
    monitor-exit v5

    goto :goto_c0

    :catchall_bd
    move-exception v7

    monitor-exit v5

    throw v7

    .line 84
    :goto_c0
    return-void
.end method

.method protected getTrackingQueueSize()J
    .registers 10

    .line 168
    const-wide/16 v4, 0x0

    .line 169
    .local v4, "numRows":J
    iget-object v6, p0, Lcom/adobe/mobile/AbstractHitDatabase;->dbMutex:Ljava/lang/Object;

    monitor-enter v6

    .line 171
    :try_start_5
    iget-object v0, p0, Lcom/adobe/mobile/AbstractHitDatabase;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "HITS"

    invoke-static {v0, v1}, Landroid/database/DatabaseUtils;->queryNumEntries(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J
    :try_end_c
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_c} :catch_f
    .catch Landroid/database/SQLException; {:try_start_5 .. :try_end_c} :catch_25
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_c} :catch_3b
    .catchall {:try_start_5 .. :try_end_c} :catchall_52

    move-result-wide v0

    move-wide v4, v0

    .line 181
    goto :goto_50

    .line 173
    :catch_f
    move-exception v7

    .line 174
    .local v7, "x":Ljava/lang/NullPointerException;
    const-string v0, "%s - Unable to get tracking queue size due to an unopened database (%s)"

    const/4 v1, 0x2

    :try_start_13
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AbstractHitDatabase;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v7}, Ljava/lang/NullPointerException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 181
    .end local v7    # "x":Ljava/lang/NullPointerException;
    goto :goto_50

    .line 176
    :catch_25
    move-exception v7

    .line 177
    .local v7, "x":Landroid/database/SQLException;
    const-string v0, "%s - Unable to get tracking queue size due to a sql error (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AbstractHitDatabase;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v7}, Landroid/database/SQLException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 181
    .end local v7    # "x":Landroid/database/SQLException;
    goto :goto_50

    .line 179
    :catch_3b
    move-exception v7

    .line 180
    .local v7, "x":Ljava/lang/Exception;
    const-string v0, "%s - Unable to get tracking queue size due to an unexpected error (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AbstractHitDatabase;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v7}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_50
    .catchall {:try_start_13 .. :try_end_50} :catchall_52

    .line 182
    .end local v7    # "x":Ljava/lang/Exception;
    :goto_50
    monitor-exit v6

    goto :goto_55

    :catchall_52
    move-exception v8

    monitor-exit v6

    throw v8

    .line 183
    :goto_55
    return-wide v4
.end method

.method protected initializeDatabase()V
    .registers 6

    .line 48
    :try_start_0
    iget-object v0, p0, Lcom/adobe/mobile/AbstractHitDatabase;->database:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v1, p0, Lcom/adobe/mobile/AbstractHitDatabase;->dbCreateStatement:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_7} :catch_8
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_7} :catch_1e
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_7} :catch_34

    .line 58
    goto :goto_49

    .line 50
    :catch_8
    move-exception v4

    .line 51
    .local v4, "x":Ljava/lang/NullPointerException;
    const-string v0, "%s - Unable to create database due to an invalid path (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AbstractHitDatabase;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v4}, Ljava/lang/NullPointerException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 58
    .end local v4    # "x":Ljava/lang/NullPointerException;
    goto :goto_49

    .line 53
    :catch_1e
    move-exception v4

    .line 54
    .local v4, "x":Landroid/database/SQLException;
    const-string v0, "%s - Unable to create database due to a sql error (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AbstractHitDatabase;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v4}, Landroid/database/SQLException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 58
    .end local v4    # "x":Landroid/database/SQLException;
    goto :goto_49

    .line 56
    :catch_34
    move-exception v4

    .line 57
    .local v4, "x":Ljava/lang/Exception;
    const-string v0, "%s - Unable to create database due to an unexpected error (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AbstractHitDatabase;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v4}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 59
    .end local v4    # "x":Ljava/lang/Exception;
    :goto_49
    return-void
.end method

.method protected kick(Z)V
    .registers 11
    .param p1, "ignoreBatchLimit"    # Z

    .line 117
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v4

    .line 120
    .local v4, "cfg":Lcom/adobe/mobile/MobileConfig;
    invoke-static {}, Lcom/adobe/mobile/ReferrerHandler;->getReferrerProcessed()Z

    move-result v0

    if-nez v0, :cond_51

    invoke-virtual {v4}, Lcom/adobe/mobile/MobileConfig;->getReferrerTimeout()I

    move-result v0

    if-lez v0, :cond_51

    .line 122
    iget-object v5, p0, Lcom/adobe/mobile/AbstractHitDatabase;->referrerTimerMutex:Ljava/lang/Object;

    monitor-enter v5

    .line 123
    :try_start_13
    iget-object v0, p0, Lcom/adobe/mobile/AbstractHitDatabase;->referrerTask:Ljava/util/TimerTask;
    :try_end_15
    .catchall {:try_start_13 .. :try_end_15} :catchall_4d

    if-nez v0, :cond_4b

    .line 125
    :try_start_17
    new-instance v0, Lcom/adobe/mobile/AbstractHitDatabase$ReferrerTimeoutTask;

    invoke-direct {v0, p0, p1}, Lcom/adobe/mobile/AbstractHitDatabase$ReferrerTimeoutTask;-><init>(Lcom/adobe/mobile/AbstractHitDatabase;Z)V

    iput-object v0, p0, Lcom/adobe/mobile/AbstractHitDatabase;->referrerTask:Ljava/util/TimerTask;

    .line 126
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/adobe/mobile/AbstractHitDatabase;->referrerTimer:Ljava/util/Timer;

    .line 127
    iget-object v0, p0, Lcom/adobe/mobile/AbstractHitDatabase;->referrerTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/adobe/mobile/AbstractHitDatabase;->referrerTask:Ljava/util/TimerTask;

    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/adobe/mobile/MobileConfig;->getReferrerTimeout()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_35
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_35} :catch_36
    .catchall {:try_start_17 .. :try_end_35} :catchall_4d

    .line 130
    goto :goto_4b

    .line 128
    :catch_36
    move-exception v6

    .line 129
    .local v6, "e":Ljava/lang/Exception;
    const-string v0, "%s - Error creating referrer timer (%s)"

    const/4 v1, 0x2

    :try_start_3a
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AbstractHitDatabase;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4b
    .catchall {:try_start_3a .. :try_end_4b} :catchall_4d

    .line 132
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_4b
    :goto_4b
    monitor-exit v5

    goto :goto_50

    :catchall_4d
    move-exception v7

    monitor-exit v5

    throw v7

    .line 133
    :goto_50
    return-void

    .line 135
    :cond_51
    iget-object v0, p0, Lcom/adobe/mobile/AbstractHitDatabase;->referrerTimer:Ljava/util/Timer;

    if-eqz v0, :cond_7b

    .line 137
    iget-object v5, p0, Lcom/adobe/mobile/AbstractHitDatabase;->referrerTimerMutex:Ljava/lang/Object;

    monitor-enter v5

    .line 139
    :try_start_58
    iget-object v0, p0, Lcom/adobe/mobile/AbstractHitDatabase;->referrerTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V
    :try_end_5d
    .catch Ljava/lang/Exception; {:try_start_58 .. :try_end_5d} :catch_5e
    .catchall {:try_start_58 .. :try_end_5d} :catchall_78

    .line 143
    goto :goto_73

    .line 141
    :catch_5e
    move-exception v6

    .line 142
    .local v6, "e":Ljava/lang/Exception;
    const-string v0, "%s - Error cancelling referrer timer (%s)"

    const/4 v1, 0x2

    :try_start_62
    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/AbstractHitDatabase;->logPrefix:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 144
    .end local v6    # "e":Ljava/lang/Exception;
    :goto_73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/adobe/mobile/AbstractHitDatabase;->referrerTask:Ljava/util/TimerTask;
    :try_end_76
    .catchall {:try_start_62 .. :try_end_76} :catchall_78

    .line 145
    monitor-exit v5

    goto :goto_7b

    :catchall_78
    move-exception v8

    monitor-exit v5

    throw v8

    .line 149
    :cond_7b
    :goto_7b
    invoke-virtual {v4}, Lcom/adobe/mobile/MobileConfig;->getPrivacyStatus()Lcom/adobe/mobile/MobilePrivacyStatus;

    move-result-object v0

    sget-object v1, Lcom/adobe/mobile/MobilePrivacyStatus;->MOBILE_PRIVACY_STATUS_OPT_IN:Lcom/adobe/mobile/MobilePrivacyStatus;

    if-eq v0, v1, :cond_84

    .line 150
    return-void

    .line 154
    :cond_84
    invoke-virtual {v4}, Lcom/adobe/mobile/MobileConfig;->getOfflineTrackingEnabled()Z

    move-result v0

    if-eqz v0, :cond_95

    iget-wide v0, p0, Lcom/adobe/mobile/AbstractHitDatabase;->numberOfUnsentHits:J

    invoke-virtual {v4}, Lcom/adobe/mobile/MobileConfig;->getBatchLimit()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_97

    :cond_95
    const/4 v5, 0x1

    goto :goto_98

    :cond_97
    const/4 v5, 0x0

    .line 156
    .local v5, "overBatchLimit":Z
    :goto_98
    if-nez v5, :cond_9c

    if-eqz p1, :cond_9f

    .line 157
    :cond_9c
    invoke-virtual {p0}, Lcom/adobe/mobile/AbstractHitDatabase;->bringOnline()V

    .line 159
    :cond_9f
    return-void
.end method

.method protected postReset()V
    .registers 3

    .line 191
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/adobe/mobile/AbstractHitDatabase;->numberOfUnsentHits:J

    .line 192
    return-void
.end method

.method protected selectOldestHit()Lcom/adobe/mobile/AbstractHitDatabase$Hit;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .line 34
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "getFirstHitInQueue must be overwritten"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected workerThread()Ljava/lang/Runnable;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .line 38
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "workerThread must be overwritten"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

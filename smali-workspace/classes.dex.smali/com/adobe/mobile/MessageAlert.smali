.class final Lcom/adobe/mobile/MessageAlert;
.super Lcom/adobe/mobile/Message;
.source "MessageAlert.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/adobe/mobile/MessageAlert$MessageShower;
    }
.end annotation


# instance fields
.field protected alertDialog:Landroid/app/AlertDialog;

.field protected cancelButtonText:Ljava/lang/String;

.field protected confirmButtonText:Ljava/lang/String;

.field protected content:Ljava/lang/String;

.field protected title:Ljava/lang/String;

.field protected url:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .registers 1

    .line 33
    invoke-direct {p0}, Lcom/adobe/mobile/Message;-><init>()V

    return-void
.end method

.method protected static clearCurrentDialog()V
    .registers 3

    .line 264
    invoke-static {}, Lcom/adobe/mobile/Messages;->getCurrentMessage()Lcom/adobe/mobile/Message;

    move-result-object v2

    .line 265
    .local v2, "currentMessage":Lcom/adobe/mobile/Message;
    if-eqz v2, :cond_a

    instance-of v0, v2, Lcom/adobe/mobile/MessageAlert;

    if-nez v0, :cond_b

    .line 266
    :cond_a
    return-void

    .line 270
    :cond_b
    iget v0, v2, Lcom/adobe/mobile/Message;->orientationWhenShown:I

    invoke-static {}, Lcom/adobe/mobile/StaticMethods;->getCurrentOrientation()I

    move-result v1

    if-eq v0, v1, :cond_33

    .line 271
    move-object v0, v2

    check-cast v0, Lcom/adobe/mobile/MessageAlert;

    iget-object v0, v0, Lcom/adobe/mobile/MessageAlert;->alertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2d

    move-object v0, v2

    check-cast v0, Lcom/adobe/mobile/MessageAlert;

    iget-object v0, v0, Lcom/adobe/mobile/MessageAlert;->alertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 272
    move-object v0, v2

    check-cast v0, Lcom/adobe/mobile/MessageAlert;

    iget-object v0, v0, Lcom/adobe/mobile/MessageAlert;->alertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 274
    :cond_2d
    move-object v0, v2

    check-cast v0, Lcom/adobe/mobile/MessageAlert;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/adobe/mobile/MessageAlert;->alertDialog:Landroid/app/AlertDialog;

    .line 276
    :cond_33
    return-void
.end method


# virtual methods
.method protected initWithPayloadObject(Lorg/json/JSONObject;)Z
    .registers 8
    .param p1, "dictionary"    # Lorg/json/JSONObject;

    .line 53
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-gtz v0, :cond_a

    .line 54
    :cond_8
    const/4 v0, 0x0

    return v0

    .line 58
    :cond_a
    invoke-super {p0, p1}, Lcom/adobe/mobile/Message;->initWithPayloadObject(Lorg/json/JSONObject;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 59
    const/4 v0, 0x0

    return v0

    .line 65
    :cond_12
    const-string v0, "payload"

    :try_start_14
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 66
    .local v4, "jsonPayload":Lorg/json/JSONObject;
    invoke-virtual {v4}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-gtz v0, :cond_2d

    .line 67
    const-string v0, "Messages - Unable to create alert message \"%s\", payload is empty"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/MessageAlert;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2b
    .catch Lorg/json/JSONException; {:try_start_14 .. :try_end_2b} :catch_2e

    .line 68
    const/4 v0, 0x0

    return v0

    .line 73
    :cond_2d
    goto :goto_3e

    .line 70
    .end local v4    # "jsonPayload":Lorg/json/JSONObject;
    :catch_2e
    move-exception v5

    .line 71
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - Unable to create alert message \"%s\", payload is required"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/MessageAlert;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 72
    const/4 v0, 0x0

    return v0

    .line 77
    .local v4, "jsonPayload":Lorg/json/JSONObject;
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_3e
    const-string v0, "title"

    :try_start_40
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MessageAlert;->title:Ljava/lang/String;

    .line 78
    iget-object v0, p0, Lcom/adobe/mobile/MessageAlert;->title:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_5d

    .line 79
    const-string v0, "Messages - Unable to create alert message \"%s\", title is empty"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/MessageAlert;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5b
    .catch Lorg/json/JSONException; {:try_start_40 .. :try_end_5b} :catch_5e

    .line 80
    const/4 v0, 0x0

    return v0

    .line 86
    :cond_5d
    goto :goto_6e

    .line 83
    :catch_5e
    move-exception v5

    .line 84
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - Unable to create alert message \"%s\", title is required"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/MessageAlert;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    const/4 v0, 0x0

    return v0

    .line 89
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_6e
    const-string v0, "content"

    :try_start_70
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MessageAlert;->content:Ljava/lang/String;

    .line 90
    iget-object v0, p0, Lcom/adobe/mobile/MessageAlert;->content:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_8d

    .line 91
    const-string v0, "Messages - Unable to create alert message \"%s\", content is empty"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/MessageAlert;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_8b
    .catch Lorg/json/JSONException; {:try_start_70 .. :try_end_8b} :catch_8e

    .line 92
    const/4 v0, 0x0

    return v0

    .line 98
    :cond_8d
    goto :goto_9e

    .line 95
    :catch_8e
    move-exception v5

    .line 96
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - Unable to create alert message \"%s\", content is required"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/MessageAlert;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 97
    const/4 v0, 0x0

    return v0

    .line 101
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_9e
    const-string v0, "confirm"

    :try_start_a0
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MessageAlert;->confirmButtonText:Ljava/lang/String;

    .line 102
    iget-object v0, p0, Lcom/adobe/mobile/MessageAlert;->confirmButtonText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_bd

    .line 103
    const-string v0, "Messages - Unable to create alert message \"%s\", confirm is empty"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/MessageAlert;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_bb
    .catch Lorg/json/JSONException; {:try_start_a0 .. :try_end_bb} :catch_be

    .line 104
    const/4 v0, 0x0

    return v0

    .line 110
    :cond_bd
    goto :goto_ce

    .line 107
    :catch_be
    move-exception v5

    .line 108
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - Unable to create alert message \"%s\", confirm is required"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/MessageAlert;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 109
    const/4 v0, 0x0

    return v0

    .line 113
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_ce
    const-string v0, "cancel"

    :try_start_d0
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MessageAlert;->cancelButtonText:Ljava/lang/String;

    .line 114
    iget-object v0, p0, Lcom/adobe/mobile/MessageAlert;->cancelButtonText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_ed

    .line 115
    const-string v0, "Messages - Unable to create alert message \"%s\", cancel is empty"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/MessageAlert;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_eb
    .catch Lorg/json/JSONException; {:try_start_d0 .. :try_end_eb} :catch_ee

    .line 116
    const/4 v0, 0x0

    return v0

    .line 122
    :cond_ed
    goto :goto_fe

    .line 119
    :catch_ee
    move-exception v5

    .line 120
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - Unable to create alert message \"%s\", cancel is required"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/adobe/mobile/MessageAlert;->messageId:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 121
    const/4 v0, 0x0

    return v0

    .line 126
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_fe
    const-string v0, "url"

    :try_start_100
    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/adobe/mobile/MessageAlert;->url:Ljava/lang/String;
    :try_end_106
    .catch Lorg/json/JSONException; {:try_start_100 .. :try_end_106} :catch_107

    .line 130
    goto :goto_110

    .line 128
    :catch_107
    move-exception v5

    .line 129
    .local v5, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - Tried to read url for alert message but found none.  This is not a required field"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    .end local v5    # "ex":Lorg/json/JSONException;
    :goto_110
    const/4 v0, 0x1

    return v0
.end method

.method protected show()V
    .registers 4

    .line 249
    iget-object v0, p0, Lcom/adobe/mobile/MessageAlert;->cancelButtonText:Ljava/lang/String;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/adobe/mobile/MessageAlert;->cancelButtonText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1b

    .line 250
    :cond_d
    iget-object v0, p0, Lcom/adobe/mobile/MessageAlert;->confirmButtonText:Ljava/lang/String;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/adobe/mobile/MessageAlert;->confirmButtonText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1b

    .line 251
    :cond_1a
    return-void

    .line 255
    :cond_1b
    invoke-super {p0}, Lcom/adobe/mobile/Message;->show()V

    .line 258
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 259
    .local v2, "mainHandler":Landroid/os/Handler;
    new-instance v0, Lcom/adobe/mobile/MessageAlert$MessageShower;

    invoke-direct {v0, p0}, Lcom/adobe/mobile/MessageAlert$MessageShower;-><init>(Lcom/adobe/mobile/MessageAlert;)V

    invoke-virtual {v2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 260
    return-void
.end method

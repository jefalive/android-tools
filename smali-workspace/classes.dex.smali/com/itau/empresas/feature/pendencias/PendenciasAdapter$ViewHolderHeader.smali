.class Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "PendenciasAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/pendencias/PendenciasAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewHolderHeader"
.end annotation


# instance fields
.field descricao:Landroid/widget/TextView;

.field parentView:Landroid/view/View;

.field titulo:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .registers 5
    .param p1, "itemView"    # Landroid/view/View;

    .line 139
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 140
    iput-object p1, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;->parentView:Landroid/view/View;

    .line 141
    const v0, 0x7f0e055a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;->titulo:Landroid/widget/TextView;

    .line 142
    const v0, 0x7f0e055b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;->descricao:Landroid/widget/TextView;

    .line 144
    iget-object v0, p0, Lcom/itau/empresas/feature/pendencias/PendenciasAdapter$ViewHolderHeader;->titulo:Landroid/widget/TextView;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 145
    return-void
.end method

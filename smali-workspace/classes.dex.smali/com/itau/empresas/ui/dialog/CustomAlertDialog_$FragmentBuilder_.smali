.class public Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;
.super Lorg/androidannotations/api/builder/FragmentBuilder;
.source "CustomAlertDialog_.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FragmentBuilder_"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/androidannotations/api/builder/FragmentBuilder<Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 153
    invoke-direct {p0}, Lorg/androidannotations/api/builder/FragmentBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    .registers 3

    .line 159
    new-instance v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;

    invoke-direct {v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;-><init>()V

    .line 160
    .local v1, "fragment_":Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;->args:Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_;->setArguments(Landroid/os/Bundle;)V

    .line 161
    return-object v1
.end method

.method public estadoArg(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;
    .registers 4
    .param p1, "estadoArg"    # Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 215
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;->args:Landroid/os/Bundle;

    const-string v1, "estadoArg"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 216
    return-object p0
.end method

.method public mensagem(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;
    .registers 4
    .param p1, "mensagem"    # Ljava/lang/String;

    .line 171
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;->args:Landroid/os/Bundle;

    const-string v1, "mensagem"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    return-object p0
.end method

.method public textoBotaoNegativo(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;
    .registers 4
    .param p1, "textoBotaoNegativo"    # Ljava/lang/String;

    .line 193
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;->args:Landroid/os/Bundle;

    const-string v1, "textoBotaoNegativo"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    return-object p0
.end method

.method public textoBotaoNeutro(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;
    .registers 4
    .param p1, "textoBotaoNeutro"    # Ljava/lang/String;

    .line 204
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;->args:Landroid/os/Bundle;

    const-string v1, "textoBotaoNeutro"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    return-object p0
.end method

.method public textoBotaoPositivo(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;
    .registers 4
    .param p1, "textoBotaoPositivo"    # Ljava/lang/String;

    .line 182
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog_$FragmentBuilder_;->args:Landroid/os/Bundle;

    const-string v1, "textoBotaoPositivo"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    return-object p0
.end method

.class public final Lbr/com/itau/sdk/android/core/o;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/o$a;
    }
.end annotation


# static fields
.field private static final f:[B

.field private static g:I


# instance fields
.field private b:Ljava/lang/Long;

.field private c:Ljava/lang/Long;

.field private d:Ljava/lang/Long;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0xf

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/o;->f:[B

    const/16 v0, 0x53

    sput v0, Lbr/com/itau/sdk/android/core/o;->g:I

    return-void

    :array_e
    .array-data 1
        0x26t
        0x4bt
        -0x4dt
        0x19t
        -0x28t
        0x17t
        0x37t
        0x2t
        -0x9t
        -0x38t
        0x2at
        0x1et
        -0x1t
        -0x3t
        0xct
    .end array-data
.end method

.method private constructor <init>()V
    .registers 1

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/itau/sdk/android/core/p;)V
    .registers 2

    .line 10
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/o;-><init>()V

    return-void
.end method

.method public static a()Lbr/com/itau/sdk/android/core/o;
    .registers 1

    .line 26
    invoke-static {}, Lbr/com/itau/sdk/android/core/o$a;->a()Lbr/com/itau/sdk/android/core/o;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/Long;)Z
    .registers 6

    .line 76
    if-eqz p1, :cond_15

    iget-object v0, p0, Lbr/com/itau/sdk/android/core/o;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_17

    :cond_15
    const/4 v0, 0x1

    goto :goto_18

    :cond_17
    const/4 v0, 0x0

    :goto_18
    return v0
.end method

.method private h()Z
    .registers 2

    .line 68
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/o;->b:Ljava/lang/Long;

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/o;->a(Ljava/lang/Long;)Z

    move-result v0

    return v0
.end method

.method private i()Z
    .registers 2

    .line 72
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/o;->c:Ljava/lang/Long;

    invoke-direct {p0, v0}, Lbr/com/itau/sdk/android/core/o;->a(Ljava/lang/Long;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Ljava/lang/Integer;)V
    .registers 5

    .line 34
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/o;->b:Ljava/lang/Long;

    .line 35
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .registers 2

    .line 84
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/o;->e:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public b()V
    .registers 3

    .line 30
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/o;->d:Ljava/lang/Long;

    .line 31
    return-void
.end method

.method public b(Ljava/lang/Integer;)V
    .registers 5

    .line 38
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/o;->c:Ljava/lang/Long;

    .line 39
    return-void
.end method

.method public c()Z
    .registers 2

    .line 42
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/o;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method d()Z
    .registers 2

    .line 47
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/o;->d:Ljava/lang/Long;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lbr/com/itau/sdk/android/core/o;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lbr/com/itau/sdk/android/core/o;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1a

    .line 48
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/o;->h()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/o;->i()Z

    move-result v0

    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    goto :goto_1b

    :cond_1a
    const/4 v0, 0x0

    .line 47
    :goto_1b
    return v0
.end method

.method e()Z
    .registers 3

    .line 53
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/o;->d:Ljava/lang/Long;

    if-eqz v0, :cond_10

    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/o;->h()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/o;->i()Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_10
    const/4 v1, 0x1

    goto :goto_13

    :cond_12
    const/4 v1, 0x0

    .line 55
    :goto_13
    if-nez v1, :cond_1c

    .line 56
    invoke-static {}, Lbr/com/itau/sdk/android/core/BackendClient;->getInstance()Lbr/com/itau/sdk/android/core/BackendClient;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->reset()V

    .line 58
    :cond_1c
    return v1
.end method

.method f()V
    .registers 2

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/o;->b:Ljava/lang/Long;

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/o;->c:Ljava/lang/Long;

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/o;->d:Ljava/lang/Long;

    .line 65
    return-void
.end method

.method public g()Ljava/lang/String;
    .registers 2

    .line 80
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/o;->e:Ljava/lang/String;

    return-object v0
.end method

.class public Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;
.super Ljava/lang/Object;
.source "ValoresEmprestimoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;
    }
.end annotation


# instance fields
.field private enumValoresEmprestimo:Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

.field private valorMaximo:F

.field private valorMinimo:F

.field private valoresEmprestimo:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valores_emprestimo"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getEnumValoresEmprestimo()Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;
    .registers 2

    .line 34
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;->enumValoresEmprestimo:Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    return-object v0
.end method

.method public getValoresEmprestimo()Ljava/math/BigDecimal;
    .registers 2

    .line 26
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;->valoresEmprestimo:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public setEnumValoresEmprestimo(Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;)V
    .registers 2
    .param p1, "enumValoresEmprestimo"    # Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    .line 61
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;->enumValoresEmprestimo:Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO$EnumValoresEmprestimo;

    .line 62
    return-void
.end method

.method public setValorMaximo(F)V
    .registers 2
    .param p1, "valorMaximo"    # F

    .line 71
    iput p1, p0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;->valorMaximo:F

    .line 72
    return-void
.end method

.method public setValorMinimo(F)V
    .registers 2
    .param p1, "valorMinimo"    # F

    .line 66
    iput p1, p0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;->valorMinimo:F

    .line 67
    return-void
.end method

.method public setValoresEmprestimo(Ljava/math/BigDecimal;)V
    .registers 2
    .param p1, "valoresEmprestimo"    # Ljava/math/BigDecimal;

    .line 51
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/ValoresEmprestimoVO;->valoresEmprestimo:Ljava/math/BigDecimal;

    .line 52
    return-void
.end method

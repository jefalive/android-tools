.class final Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils$1;
.super Ljava/lang/Object;
.source "AcessibilidadeUtils.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils;->textToSpeech(Landroid/content/Context;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$toSpeak:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 3

    .line 74
    iput-object p1, p0, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils$1;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils$1;->val$toSpeak:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInit(I)V
    .registers 4
    .param p1, "status"    # I

    .line 77
    const/4 v0, -0x1

    if-eq p1, v0, :cond_19

    .line 78
    # getter for: Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils;->textToSpeech:Landroid/speech/tts/TextToSpeech;
    invoke-static {}, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils;->access$100()Landroid/speech/tts/TextToSpeech;

    move-result-object v0

    # getter for: Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils;->LOCALE_BRAZIL:Ljava/util/Locale;
    invoke-static {}, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils;->access$000()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    .line 79
    const/4 v0, 0x1

    # setter for: Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils;->initializedWithSuccess:Z
    invoke-static {v0}, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils;->access$202(Z)Z

    .line 80
    iget-object v0, p0, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils$1;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils$1;->val$toSpeak:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils;->textToSpeech(Landroid/content/Context;Ljava/lang/String;)V

    .line 82
    :cond_19
    return-void
.end method

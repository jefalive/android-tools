.class public Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;
.super Ljava/lang/Object;
.source "ItensComprovanteView.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;Ljava/lang/Comparable<Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;>;"
    }
.end annotation


# instance fields
.field private final descricao:Ljava/lang/String;

.field private final order:I

.field private final titulo:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "descricao"    # Ljava/lang/String;
    .param p2, "titulo"    # Ljava/lang/String;

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;->descricao:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;->titulo:Ljava/lang/String;

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;->order:I

    .line 21
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;)I
    .registers 4
    .param p1, "another"    # Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;

    .line 33
    iget v0, p0, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;->order:I

    iget v1, p1, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;->order:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .registers 3

    .line 3
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;

    invoke-virtual {p0, v0}, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;->compareTo(Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1, "o"    # Ljava/lang/Object;

    .line 38
    if-ne p0, p1, :cond_4

    .line 39
    const/4 v0, 0x1

    return v0

    .line 41
    :cond_4
    if-eqz p1, :cond_10

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_12

    .line 42
    :cond_10
    const/4 v0, 0x0

    return v0

    .line 45
    :cond_12
    move-object v2, p1

    check-cast v2, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;

    .line 47
    .local v2, "that":Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;
    iget v0, p0, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;->order:I

    iget v1, v2, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;->order:I

    if-ne v0, v1, :cond_1d

    const/4 v0, 0x1

    goto :goto_1e

    :cond_1d
    const/4 v0, 0x0

    :goto_1e
    return v0
.end method

.method public getDescricao()Ljava/lang/String;
    .registers 2

    .line 28
    iget-object v0, p0, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;->descricao:Ljava/lang/String;

    return-object v0
.end method

.method public getTitulo()Ljava/lang/String;
    .registers 2

    .line 24
    iget-object v0, p0, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;->titulo:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .registers 2

    .line 53
    iget v0, p0, Lcom/itau/empresas/ui/view/comprovante/ItensComprovanteView;->order:I

    return v0
.end method

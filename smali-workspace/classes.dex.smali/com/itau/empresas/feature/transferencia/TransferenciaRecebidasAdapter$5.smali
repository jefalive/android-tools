.class Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$5;
.super Ljava/lang/Object;
.source "TransferenciaRecebidasAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->eventosCliqueListener(Landroid/view/View;Landroid/widget/TextView;Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

.field final synthetic val$itemVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    .line 171
    iput-object p1, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$5;->this$0:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    iput-object p2, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$5;->val$itemVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 4
    .param p1, "v"    # Landroid/view/View;

    .line 174
    iget-object v0, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$5;->this$0:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;

    # getter for: Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;->access$200(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasAdapter$5;->val$itemVO:Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;

    .line 175
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_$IntentBuilder_;->transferenciaRecebidasVO(Lcom/itau/empresas/feature/transferencia/TransferenciaRecebidasVO;)Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_$IntentBuilder_;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;->COMPARTILHAR:Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;

    .line 176
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_$IntentBuilder_;->acao(Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity$Acao;)Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_$IntentBuilder_;

    move-result-object v0

    .line 177
    invoke-virtual {v0}, Lcom/itau/empresas/feature/transferencia/activity/TransferenciaRecebidasDetalheActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 178
    return-void
.end method

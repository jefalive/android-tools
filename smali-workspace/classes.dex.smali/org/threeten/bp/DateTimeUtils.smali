.class public final Lorg/threeten/bp/DateTimeUtils;
.super Ljava/lang/Object;
.source "DateTimeUtils.java"


# direct methods
.method public static toDate(Lorg/threeten/bp/Instant;)Ljava/util/Date;
    .registers 5
    .param p0, "instant"    # Lorg/threeten/bp/Instant;

    .line 75
    :try_start_0
    new-instance v0, Ljava/util/Date;

    invoke-virtual {p0}, Lorg/threeten/bp/Instant;->toEpochMilli()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V
    :try_end_9
    .catch Ljava/lang/ArithmeticException; {:try_start_0 .. :try_end_9} :catch_a

    return-object v0

    .line 76
    :catch_a
    move-exception v3

    .line 77
    .local v3, "ex":Ljava/lang/ArithmeticException;
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

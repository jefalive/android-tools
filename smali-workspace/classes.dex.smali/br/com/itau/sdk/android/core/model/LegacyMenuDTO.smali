.class public final Lbr/com/itau/sdk/android/core/model/LegacyMenuDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private idu:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "IDU"
    .end annotation
.end field

.field private links:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "LINKS"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lbr/com/itau/sdk/android/core/model/MiddlewareKeyValue;>;"
        }
    .end annotation
.end field

.field private opu:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "OPU"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getIdu()Ljava/lang/String;
    .registers 2

    .line 19
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/LegacyMenuDTO;->idu:Ljava/lang/String;

    return-object v0
.end method

.method public getLinks()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lbr/com/itau/sdk/android/core/model/MiddlewareKeyValue;>;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/LegacyMenuDTO;->links:Ljava/util/List;

    return-object v0
.end method

.method public getOpu()Ljava/lang/String;
    .registers 2

    .line 23
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/LegacyMenuDTO;->opu:Ljava/lang/String;

    return-object v0
.end method

.class public Lcom/itau/empresas/api/model/LoginInputVO;
.super Ljava/lang/Object;
.source "LoginInputVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private codigoOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_operador"
    .end annotation
.end field

.field private idTeclado:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id_teclado"
    .end annotation
.end field

.field private senha:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "senha"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setCodigoOperador(Ljava/lang/String;)V
    .registers 2
    .param p1, "codigoOperador"    # Ljava/lang/String;

    .line 35
    iput-object p1, p0, Lcom/itau/empresas/api/model/LoginInputVO;->codigoOperador:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public setIdTeclado(Ljava/lang/String;)V
    .registers 2
    .param p1, "idTeclado"    # Ljava/lang/String;

    .line 19
    iput-object p1, p0, Lcom/itau/empresas/api/model/LoginInputVO;->idTeclado:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public setSenha(Ljava/lang/String;)V
    .registers 2
    .param p1, "senha"    # Ljava/lang/String;

    .line 27
    iput-object p1, p0, Lcom/itau/empresas/api/model/LoginInputVO;->senha:Ljava/lang/String;

    .line 28
    return-void
.end method

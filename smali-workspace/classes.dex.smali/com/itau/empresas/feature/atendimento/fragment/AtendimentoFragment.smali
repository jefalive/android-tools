.class public Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;
.super Lcom/itau/empresas/ui/fragment/BaseFragment;
.source "AtendimentoFragment.java"


# static fields
.field private static final limit:Ljava/lang/Integer;


# instance fields
.field application:Lcom/itau/empresas/CustomApplication;

.field controller:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

.field private faqs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;>;"
        }
    .end annotation
.end field

.field private filtro:Ljava/lang/String;

.field linearCartaoAlerta:Landroid/widget/LinearLayout;

.field listaFaqs:Lcom/itau/empresas/ui/widgets/NonScrollListView;

.field logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

.field mensagemAlertaView:Lcom/itau/empresas/ui/view/MensagemAlertaView;

.field svAtendimento:Landroid/widget/ScrollView;

.field private topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 65
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->limit:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>()V
    .registers 2

    .line 60
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;-><init>()V

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->filtro:Ljava/lang/String;

    return-void
.end method

.method private abreLink(Ljava/lang/String;)V
    .registers 6
    .param p1, "chaveMobile"    # Ljava/lang/String;

    .line 344
    new-instance v1, Lcom/itau/empresas/api/model/MenuVO;

    invoke-direct {v1}, Lcom/itau/empresas/api/model/MenuVO;-><init>()V

    .line 345
    .local v1, "item":Lcom/itau/empresas/api/model/MenuVO;
    invoke-virtual {v1, p1}, Lcom/itau/empresas/api/model/MenuVO;->setChaveMobile(Ljava/lang/String;)V

    .line 346
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->getInstance(Ljava/lang/String;)Lcom/itau/empresas/ui/util/menu/InformacaoMenu;

    move-result-object v2

    .line 347
    .local v2, "informacaoMenu":Lcom/itau/empresas/ui/util/menu/InformacaoMenu;
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/menu/InformacaoMenu;->getCommand()Lcom/itau/empresas/ui/util/menu/Command;

    move-result-object v3

    .line 348
    .local v3, "command":Lcom/itau/empresas/ui/util/menu/Command;
    if-eqz v3, :cond_1f

    .line 349
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-interface {v3, v0, v1}, Lcom/itau/empresas/ui/util/menu/Command;->executar(Landroid/support/v7/app/AppCompatActivity;Lcom/itau/empresas/api/model/MenuVO;)V

    .line 351
    :cond_1f
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;Ljava/lang/String;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .line 60
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->abreLink(Ljava/lang/String;)V

    return-void
.end method

.method private carregaAdapterFaqs(Ljava/util/ArrayList;)V
    .registers 5
    .param p1, "faqs"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;>;)V"
        }
    .end annotation

    .line 181
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->listaFaqs:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    new-instance v1, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoFaqsAdapter;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lcom/itau/empresas/feature/atendimento/adapter/AtendimentoFaqsAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 182
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->listaFaqs:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    new-instance v1, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment$1;

    invoke-direct {v1, p0, p1}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment$1;-><init>(Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 195
    return-void
.end method

.method private carregaAlertas()V
    .registers 2

    .line 130
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    if-eqz v0, :cond_9

    .line 131
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismiss()V

    .line 133
    :cond_9
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->carregarListaAlertas()V

    .line 134
    return-void
.end method

.method private carregaFaqs()V
    .registers 3

    .line 229
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    if-eqz v0, :cond_9

    .line 230
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismiss()V

    .line 232
    :cond_9
    sget-object v0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->limit:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->carregarListaPerguntas(II)V

    .line 233
    return-void
.end method

.method private carregarListaAlertas()V
    .registers 3

    .line 125
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ProgressDialogUtils;->mostraDialogoDeProgresso(Landroid/support/v4/app/FragmentManager;)V

    .line 126
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->controller:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

    const v1, 0x7f07036f

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->buscaAlertas(Ljava/lang/String;)V

    .line 127
    return-void
.end method

.method private carregarListaPerguntas(II)V
    .registers 9
    .param p1, "top"    # I
    .param p2, "offset"    # I

    .line 137
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ProgressDialogUtils;->mostraDialogoDeProgresso(Landroid/support/v4/app/FragmentManager;)V

    .line 138
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->controller:Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;

    iget-object v2, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->filtro:Ljava/lang/String;

    move v3, p1

    move v4, p2

    const-string v5, "true"

    const/4 v1, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/itau/empresas/feature/atendimento/controller/AtendimentoController;->buscaFaqs(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 139
    return-void
.end method

.method private disparaEventoAnalytics()V
    .registers 5

    .line 406
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v3

    .line 407
    .line 408
    .local v3, "analyticsHelper":Lcom/itau/empresas/feature/analytics/AnalyticsHelper;
    const v0, 0x7f0700d6

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 409
    const v1, 0x7f0700c5

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 410
    const v2, 0x7f0700c3

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 407
    invoke-virtual {v3, v0, v1, v2}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    return-void
.end method

.method private irParaAtendimento()V
    .registers 2

    .line 336
    const-string v0, "tabbar_ajuda"

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->abreLink(Ljava/lang/String;)V

    .line 337
    return-void
.end method

.method private irParaContaCorrente()V
    .registers 2

    .line 340
    const-string v0, "tabbar_extrato"

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->abreLink(Ljava/lang/String;)V

    .line 341
    return-void
.end method

.method private irParaFaqs(Lbr/com/itau/textovoz/model/Faq;)V
    .registers 8
    .param p1, "faq"    # Lbr/com/itau/textovoz/model/Faq;

    .line 354
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 355
    .local v2, "channels":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 356
    .local v3, "links":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 358
    .local v4, "relacionadas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoRelacionadas;>;"
    invoke-direct {p0, p1, v2}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->preparaCanais(Lbr/com/itau/textovoz/model/Faq;Ljava/util/ArrayList;)V

    .line 359
    invoke-direct {p0, p1, v3}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->preparaLinks(Lbr/com/itau/textovoz/model/Faq;Ljava/util/ArrayList;)V

    .line 360
    invoke-direct {p0, p1, v2, v3, v4}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->preparaFaq(Lbr/com/itau/textovoz/model/Faq;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;

    move-result-object v5

    .line 362
    .local v5, "atendimentoFAQ":Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;
    invoke-static {p0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity;->intent(Landroid/support/v4/app/Fragment;)Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;

    move-result-object v0

    const-string v1, "FAQ"

    .line 363
    invoke-virtual {v0, v1, v5}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;->extra(Ljava/lang/String;Ljava/io/Serializable;)Lorg/androidannotations/api/builder/IntentBuilder;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;

    .line 364
    invoke-virtual {v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoRespostaFaqActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 365
    return-void
.end method

.method private obterMenuAcesso(Ljava/util/List;)Ljava/util/ArrayList;
    .registers 7
    .param p1, "menuVOListApplication"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;)Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
        }
    .end annotation

    .line 270
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 272
    .local v2, "listaMenuComponente":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_6
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_72

    .line 273
    const-string v0, "in\u00edcio"

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/MenuVO;->getNome()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6e

    .line 274
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_4c

    .line 275
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4c

    .line 277
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v4

    .line 278
    .local v4, "menuFilhos":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;"
    invoke-direct {p0, v4}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->retornaMenuFilhos(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 279
    .end local v4    # "menuFilhos":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;"
    .end local v4
    goto :goto_6e

    .line 280
    :cond_4c
    new-instance v4, Lbr/com/itau/textovoz/model/Menu;

    invoke-direct {v4}, Lbr/com/itau/textovoz/model/Menu;-><init>()V

    .line 281
    .local v4, "menu":Lbr/com/itau/textovoz/model/Menu;
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getNome()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lbr/com/itau/textovoz/model/Menu;->setName(Ljava/lang/String;)V

    .line 282
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lbr/com/itau/textovoz/model/Menu;->setOpKey(Ljava/lang/String;)V

    .line 283
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 272
    .end local v4    # "menu":Lbr/com/itau/textovoz/model/Menu;
    :cond_6e
    :goto_6e
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_6

    .line 287
    .end local v3    # "i":I
    :cond_72
    return-object v2
.end method

.method private preparaCanais(Lbr/com/itau/textovoz/model/Faq;Ljava/util/ArrayList;)V
    .registers 7
    .param p1, "faq"    # Lbr/com/itau/textovoz/model/Faq;
    .param p2, "channels"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lbr/com/itau/textovoz/model/Faq;Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;>;)V"
        }
    .end annotation

    .line 379
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Faq;->getChannels()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_4a

    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Faq;->getChannels()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4a

    .line 380
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Faq;->getChannels()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_18
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/textovoz/model/Channel;

    .line 381
    .local v2, "channel":Lbr/com/itau/textovoz/model/Channel;
    new-instance v3, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;

    invoke-direct {v3}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;-><init>()V

    .line 382
    .local v3, "ch":Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;
    invoke-virtual {v2}, Lbr/com/itau/textovoz/model/Channel;->getChannel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;->setCanal(Ljava/lang/String;)V

    .line 383
    invoke-virtual {v2}, Lbr/com/itau/textovoz/model/Channel;->getExplanation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;->setTextoExplicativo(Ljava/lang/String;)V

    .line 384
    invoke-virtual {v2}, Lbr/com/itau/textovoz/model/Channel;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;->setUrl(Ljava/lang/String;)V

    .line 385
    invoke-virtual {v2}, Lbr/com/itau/textovoz/model/Channel;->getIcon()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;->setIcone(Ljava/lang/String;)V

    .line 386
    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 387
    .end local v2    # "channel":Lbr/com/itau/textovoz/model/Channel;
    .end local v3    # "ch":Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;
    goto :goto_18

    .line 389
    :cond_4a
    return-void
.end method

.method private preparaFaq(Lbr/com/itau/textovoz/model/Faq;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;
    .registers 7
    .param p1, "faq"    # Lbr/com/itau/textovoz/model/Faq;
    .param p2, "channels"    # Ljava/util/ArrayList;
    .param p3, "links"    # Ljava/util/ArrayList;
    .param p4, "relacionadas"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lbr/com/itau/textovoz/model/Faq;Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoCanal;>;Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;>;Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoRelacionadas;>;)Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;"
        }
    .end annotation

    .line 395
    new-instance v1, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;

    invoke-direct {v1}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;-><init>()V

    .line 396
    .local v1, "atendimentoFAQ":Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Faq;->getQuestion()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->setPergunta(Ljava/lang/String;)V

    .line 397
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Faq;->getAnswer()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->setResposta(Ljava/lang/String;)V

    .line 398
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Faq;->getProduct()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->setAssunto(Ljava/lang/String;)V

    .line 399
    invoke-virtual {v1, p2}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->setCanaisDisponiveis(Ljava/util/ArrayList;)V

    .line 400
    invoke-virtual {v1, p3}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->setLinks(Ljava/util/ArrayList;)V

    .line 401
    invoke-virtual {v1, p4}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;->setMobileRelacionadas(Ljava/util/ArrayList;)V

    .line 402
    return-object v1
.end method

.method private preparaLinks(Lbr/com/itau/textovoz/model/Faq;Ljava/util/ArrayList;)V
    .registers 7
    .param p1, "faq"    # Lbr/com/itau/textovoz/model/Faq;
    .param p2, "links"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Lbr/com/itau/textovoz/model/Faq;Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;>;)V"
        }
    .end annotation

    .line 368
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Faq;->getLinks()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_3c

    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Faq;->getLinks()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3c

    .line 369
    invoke-virtual {p1}, Lbr/com/itau/textovoz/model/Faq;->getLinks()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_18
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lbr/com/itau/textovoz/model/Link;

    .line 370
    .local v2, "link":Lbr/com/itau/textovoz/model/Link;
    new-instance v3, Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;

    invoke-direct {v3}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;-><init>()V

    .line 371
    .local v3, "ch":Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;
    invoke-virtual {v2}, Lbr/com/itau/textovoz/model/Link;->get_ref()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;->setRef(Ljava/lang/String;)V

    .line 372
    invoke-virtual {v2}, Lbr/com/itau/textovoz/model/Link;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;->setTexto(Ljava/lang/String;)V

    .line 373
    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 374
    .end local v2    # "link":Lbr/com/itau/textovoz/model/Link;
    .end local v3    # "ch":Lcom/itau/empresas/feature/atendimento/model/AtendimentoLink;
    goto :goto_18

    .line 376
    :cond_3c
    return-void
.end method

.method private retornaMenuFilhos(Ljava/util/List;)Ljava/util/ArrayList;
    .registers 6
    .param p1, "menuList"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/MenuVO;>;)Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
        }
    .end annotation

    .line 291
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 292
    .local v1, "retorno":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_6
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_31

    .line 293
    new-instance v3, Lbr/com/itau/textovoz/model/Menu;

    invoke-direct {v3}, Lbr/com/itau/textovoz/model/Menu;-><init>()V

    .line 294
    .local v3, "menu":Lbr/com/itau/textovoz/model/Menu;
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getNome()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lbr/com/itau/textovoz/model/Menu;->setName(Ljava/lang/String;)V

    .line 295
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/MenuVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getChaveMobile()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lbr/com/itau/textovoz/model/Menu;->setOpKey(Ljava/lang/String;)V

    .line 296
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 292
    .end local v3    # "menu":Lbr/com/itau/textovoz/model/Menu;
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 298
    .end local v2    # "j":I
    :cond_31
    return-object v1
.end method

.method private setListaFaqs(Ljava/util/ArrayList;)V
    .registers 2
    .param p1, "retornoFaqs"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lcom/itau/empresas/feature/atendimento/model/AtendimentoFAQ;>;)V"
        }
    .end annotation

    .line 142
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->carregaAdapterFaqs(Ljava/util/ArrayList;)V

    .line 143
    return-void
.end method


# virtual methods
.method abreDuvidasPorAssunto(Landroid/view/View;)V
    .registers 7
    .param p1, "view"    # Landroid/view/View;

    .line 148
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 149
    const v2, 0x7f0702db

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 150
    const v3, 0x7f0702bb

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 151
    const v4, 0x7f07032d

    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 153
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/atendimento/activity/AtendimentoAssuntoActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 154
    return-void
.end method

.method carregandoElementosDeTela()V
    .registers 6

    .line 98
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 99
    const v2, 0x7f0702db

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 100
    const v3, 0x7f070294

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 101
    const v4, 0x7f0702d5

    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 103
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->carregaAlertas()V

    .line 104
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->carregaFaqs()V

    .line 105
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContextoAtendimento()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_34

    const-string v0, ""

    goto :goto_3a

    :cond_34
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->application:Lcom/itau/empresas/CustomApplication;

    .line 107
    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContextoAtendimento()Ljava/lang/String;

    move-result-object v0

    :goto_3a
    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->filtro:Ljava/lang/String;

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->faqs:Ljava/util/ArrayList;

    .line 110
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->disparaEventoAnalytics()V

    .line 111
    return-void
.end method

.method protected irParaBusca()V
    .registers 8

    .line 159
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getMenuVO()Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v0

    if-nez v0, :cond_12

    .line 160
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/logout/LogoutHelper;->trataSessaoExpirada(Landroid/app/Activity;)V

    .line 161
    return-void

    .line 164
    :cond_12
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 165
    const v2, 0x7f0702db

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 166
    const v3, 0x7f070299

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 167
    const v4, 0x7f0702d6

    invoke-virtual {p0, v4}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->enviarHitEvento(Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;)V

    .line 169
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getMenuVO()Lcom/itau/empresas/api/model/MenuVO;

    move-result-object v0

    .line 170
    invoke-virtual {v0}, Lcom/itau/empresas/api/model/MenuVO;->getFilhos()Ljava/util/List;

    move-result-object v0

    .line 169
    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->obterMenuAcesso(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v5

    .line 172
    .local v5, "listaMenuComponente":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
    new-instance v6, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    invoke-direct {v6, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 173
    .local v6, "intent":Landroid/content/Intent;
    const-string v0, "operator"

    iget-object v1, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 174
    const-string v0, "menu"

    invoke-virtual {v6, v0, v5}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 175
    const-string v0, "type"

    const-string v1, "0"

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 176
    const/16 v0, 0x64

    invoke-virtual {p0, v6, v0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 177
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .registers 10
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .line 308
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/fragment/BaseFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 310
    const/16 v0, 0x64

    if-ne p1, v0, :cond_59

    const/4 v0, -0x1

    if-ne p2, v0, :cond_59

    .line 311
    const v0, 0x7f070209

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 313
    .local v2, "objectExtract":Z
    if-eqz v2, :cond_1c

    .line 314
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->irParaContaCorrente()V

    .line 315
    return-void

    .line 317
    :cond_1c
    const v0, 0x7f070126

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 319
    .local v3, "objectCardViewTel":Z
    if-eqz v3, :cond_2e

    .line 320
    invoke-direct {p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->irParaAtendimento()V

    .line 321
    return-void

    .line 323
    :cond_2e
    const v0, 0x7f070475

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lbr/com/itau/textovoz/model/Menu;

    .line 324
    .local v4, "menu":Lbr/com/itau/textovoz/model/Menu;
    if-eqz v4, :cond_46

    .line 325
    invoke-virtual {v4}, Lbr/com/itau/textovoz/model/Menu;->getOpKey()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->abreLink(Ljava/lang/String;)V

    .line 326
    return-void

    .line 328
    :cond_46
    const v0, 0x7f070211

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lbr/com/itau/textovoz/model/Faq;

    .line 329
    .local v5, "faq":Lbr/com/itau/textovoz/model/Faq;
    if-eqz v5, :cond_59

    .line 330
    invoke-direct {p0, v5}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->irParaFaqs(Lbr/com/itau/textovoz/model/Faq;)V

    .line 333
    .end local v2    # "objectExtract":Z
    .end local v3    # "objectCardViewTel":Z
    .end local v4    # "menu":Lbr/com/itau/textovoz/model/Menu;
    .end local v5    # "faq":Lbr/com/itau/textovoz/model/Faq;
    :cond_59
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;

    .line 241
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 242
    return-void

    .line 243
    :cond_7
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->listaFaqs:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 244
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ProgressDialogUtils;->escondeDialogoDeProgresso(Landroid/support/v4/app/FragmentManager;)V

    .line 245
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->svAtendimento:Landroid/widget/ScrollView;

    const v1, 0x7f0701f2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;II)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 246
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 247
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;

    .line 250
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 251
    return-void

    .line 252
    :cond_7
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->listaFaqs:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 253
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ProgressDialogUtils;->escondeDialogoDeProgresso(Landroid/support/v4/app/FragmentManager;)V

    .line 254
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->svAtendimento:Landroid/widget/ScrollView;

    const v1, 0x7f0701ef

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;II)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 255
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 256
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 259
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 260
    return-void

    .line 263
    :cond_7
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->listaFaqs:Lcom/itau/empresas/ui/widgets/NonScrollListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/widgets/NonScrollListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 264
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ProgressDialogUtils;->escondeDialogoDeProgresso(Landroid/support/v4/app/FragmentManager;)V

    .line 265
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->svAtendimento:Landroid/widget/ScrollView;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 266
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 267
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/atendimento/model/AlertaVO;)V
    .registers 6
    .param p1, "alerta"    # Lcom/itau/empresas/feature/atendimento/model/AlertaVO;

    .line 198
    invoke-virtual {p1}, Lcom/itau/empresas/feature/atendimento/model/AlertaVO;->getAlertas()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_56

    invoke-virtual {p1}, Lcom/itau/empresas/feature/atendimento/model/AlertaVO;->getAlertas()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_56

    .line 200
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->mensagemAlertaView:Lcom/itau/empresas/ui/view/MensagemAlertaView;

    invoke-virtual {p1}, Lcom/itau/empresas/feature/atendimento/model/AlertaVO;->getAlertas()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/atendimento/model/AlertaFAQ;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/atendimento/model/AlertaFAQ;->getMensagem_resumida()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/MensagemAlertaView;->setMensagem(Ljava/lang/String;)V

    .line 201
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->mensagemAlertaView:Lcom/itau/empresas/ui/view/MensagemAlertaView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/MensagemAlertaView;->exibeWarning(Z)V

    .line 203
    invoke-virtual {p1}, Lcom/itau/empresas/feature/atendimento/model/AlertaVO;->getAlertas()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/atendimento/model/AlertaFAQ;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/atendimento/model/AlertaFAQ;->getLink_botao_mobile()Ljava/lang/String;

    move-result-object v3

    .line 205
    .local v3, "link":Ljava/lang/String;
    if-eqz v3, :cond_4c

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_4c

    .line 206
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->linearCartaoAlerta:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment$2;

    invoke-direct {v1, p0, v3}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment$2;-><init>(Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_56

    .line 213
    :cond_4c
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->linearCartaoAlerta:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment$3;

    invoke-direct {v1, p0, p1}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment$3;-><init>(Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;Lcom/itau/empresas/feature/atendimento/model/AlertaVO;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 226
    .end local v3    # "link":Ljava/lang/String;
    :cond_56
    :goto_56
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/atendimento/model/AtendimentoVO;)V
    .registers 3
    .param p1, "atendimento"    # Lcom/itau/empresas/feature/atendimento/model/AtendimentoVO;

    .line 236
    invoke-virtual {p0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ProgressDialogUtils;->escondeDialogoDeProgresso(Landroid/support/v4/app/FragmentManager;)V

    .line 237
    invoke-virtual {p1}, Lcom/itau/empresas/feature/atendimento/model/AtendimentoVO;->getFaqs()Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/atendimento/fragment/AtendimentoFragment;->setListaFaqs(Ljava/util/ArrayList;)V

    .line 238
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 303
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "atendimentosFaqsPJ"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

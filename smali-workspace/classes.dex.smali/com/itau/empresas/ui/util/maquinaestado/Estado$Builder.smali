.class public final Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;
.super Ljava/lang/Object;
.source "Estado.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/util/maquinaestado/Estado;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private gones:[Landroid/view/View;

.field private onChangeState:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

.field private views:[Landroid/view/View;

.field private visibles:[Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;
    .registers 7

    .line 65
    new-instance v0, Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    iget-object v1, p0, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->views:[Landroid/view/View;

    iget-object v2, p0, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visibles:[Landroid/view/View;

    iget-object v3, p0, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gones:[Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/ui/util/maquinaestado/Estado;-><init>([Landroid/view/View;[Landroid/view/View;[Landroid/view/View;Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;Lcom/itau/empresas/ui/util/maquinaestado/Estado$1;)V

    return-object v0
.end method

.method public varargs gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;
    .registers 2
    .param p1, "views"    # [Landroid/view/View;

    .line 50
    iput-object p1, p0, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gones:[Landroid/view/View;

    .line 51
    return-object p0
.end method

.method public onChangeState(Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;
    .registers 2
    .param p1, "onChangeState"    # Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 60
    iput-object p1, p0, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState:Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;

    .line 61
    return-object p0
.end method

.method public varargs visible([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;
    .registers 2
    .param p1, "views"    # [Landroid/view/View;

    .line 45
    iput-object p1, p0, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visibles:[Landroid/view/View;

    .line 46
    return-object p0
.end method

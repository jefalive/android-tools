.class public final Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;
.super Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;
.source "PagamentoDarfActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 43
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;-><init>()V

    .line 47
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;

    .line 43
    invoke-super {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;

    .line 43
    invoke-super {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 59
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 60
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 61
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 62
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->app:Lcom/itau/empresas/CustomApplication;

    .line 63
    invoke-static {p0}, Lcom/itau/empresas/controller/PagamentoDarfController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/controller/PagamentoDarfController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->pagamentoDarfController:Lcom/itau/empresas/controller/PagamentoDarfController;

    .line 64
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->afterInject()V

    .line 65
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 358
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$18;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$18;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 366
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 346
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$17;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$17;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 354
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 52
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->init_(Landroid/os/Bundle;)V

    .line 53
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 55
    const v0, 0x7f030054

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->setContentView(I)V

    .line 56
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 13
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 99
    const v0, 0x7f0e0283

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->root:Landroid/widget/LinearLayout;

    .line 100
    const v0, 0x7f0e03c1

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/EditTextMonetario;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoValorPrincipal:Lcom/itau/empresas/ui/view/EditTextMonetario;

    .line 101
    const v0, 0x7f0e03c4

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/EditTextMonetario;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoValorJuros:Lcom/itau/empresas/ui/view/EditTextMonetario;

    .line 102
    const v0, 0x7f0e03bb

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/EditTextMonetario;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoValorMulta:Lcom/itau/empresas/ui/view/EditTextMonetario;

    .line 103
    const v0, 0x7f0e04a9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoHorarioLimiteInicial:Landroid/widget/TextView;

    .line 104
    const v0, 0x7f0e04aa

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoHorarioLimiteFinal:Landroid/widget/TextView;

    .line 105
    const v0, 0x7f0e05ab

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoNomeDaEmpresa:Landroid/widget/TextView;

    .line 106
    const v0, 0x7f0e0138

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoAgenciaConta:Landroid/widget/TextView;

    .line 107
    const v0, 0x7f0e0139

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoCnpjPagador:Landroid/widget/TextView;

    .line 108
    const v0, 0x7f0e0317

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoDataPagamento:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f0e03c6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoValorTotalPagarDarf:Landroid/widget/TextView;

    .line 110
    const v0, 0x7f0e03b4

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoDataApuracao:Landroid/widget/EditText;

    .line 111
    const v0, 0x7f0e03d1

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoDataVencimento:Landroid/widget/EditText;

    .line 112
    const v0, 0x7f0e03b7

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoCpfCnpjContribuinte:Landroid/widget/EditText;

    .line 113
    const v0, 0x7f0e02d9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoIdentificacaoComprovante:Landroid/widget/EditText;

    .line 114
    const v0, 0x7f0e03cd

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoNumeroReferencia:Landroid/widget/EditText;

    .line 115
    const v0, 0x7f0e03b0

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoNomeContribuinte:Landroid/widget/EditText;

    .line 116
    const v0, 0x7f0e03b9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoCodigoReceita:Landroid/widget/EditText;

    .line 117
    const v0, 0x7f0e03ca

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoReferenciaEmpresa:Landroid/widget/EditText;

    .line 118
    const v0, 0x7f0e0125

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->scroolDarf:Landroid/widget/ScrollView;

    .line 119
    const v0, 0x7f0e0288

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->rlAutorizacaoDarf:Landroid/widget/RelativeLayout;

    .line 120
    const v0, 0x7f0e0286

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->rlInclusaoDarf:Landroid/widget/RelativeLayout;

    .line 121
    const v0, 0x7f0e0285

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->rlHorarioLimite:Landroid/widget/RelativeLayout;

    .line 122
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 123
    const v0, 0x7f0e0287

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 124
    .local v2, "view_botao_incluir_pagamento_darf":Landroid/view/View;
    const v0, 0x7f0e05b0

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 125
    .local v3, "view_icone_abre_lista_cnpj":Landroid/view/View;
    const v0, 0x7f0e05ae

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 126
    .local v4, "view_rl_dados_cnpj_pagador":Landroid/view/View;
    const v0, 0x7f0e03c8

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 127
    .local v5, "view_icone_calendario_pagamento":Landroid/view/View;
    const v0, 0x7f0e03b3

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 128
    .local v6, "view_icone_periodo_apuracao":Landroid/view/View;
    const v0, 0x7f0e028a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 129
    .local v7, "view_botao_incluir_autorizar_darf":Landroid/view/View;
    const v0, 0x7f0e0289

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 130
    .local v8, "view_botao_apenas_incluir_darf":Landroid/view/View;
    const v0, 0x7f0e03d0

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 132
    .local v9, "view_imagem_seta_data_vencimento":Landroid/view/View;
    if-eqz v2, :cond_14a

    .line 133
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$1;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    :cond_14a
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoCnpjPagador:Landroid/widget/TextView;

    if-eqz v0, :cond_158

    .line 143
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoCnpjPagador:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$2;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    :cond_158
    if-eqz v3, :cond_162

    .line 153
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$3;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$3;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    :cond_162
    if-eqz v4, :cond_16c

    .line 163
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$4;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$4;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;)V

    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 172
    :cond_16c
    if-eqz v5, :cond_176

    .line 173
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$5;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$5;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;)V

    invoke-virtual {v5, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    :cond_176
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoDataPagamento:Landroid/widget/TextView;

    if-eqz v0, :cond_184

    .line 183
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoDataPagamento:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$6;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$6;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 192
    :cond_184
    if-eqz v6, :cond_18e

    .line 193
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$7;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$7;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;)V

    invoke-virtual {v6, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    :cond_18e
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoDataApuracao:Landroid/widget/EditText;

    if-eqz v0, :cond_19c

    .line 203
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoDataApuracao:Landroid/widget/EditText;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$8;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$8;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    :cond_19c
    if-eqz v7, :cond_1a6

    .line 213
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$9;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$9;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;)V

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 222
    :cond_1a6
    if-eqz v8, :cond_1b0

    .line 223
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$10;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$10;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;)V

    invoke-virtual {v8, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 232
    :cond_1b0
    if-eqz v9, :cond_1ba

    .line 233
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$11;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$11;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;)V

    invoke-virtual {v9, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 242
    :cond_1ba
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoDataVencimento:Landroid/widget/EditText;

    if-eqz v0, :cond_1c8

    .line 243
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->campoDataVencimento:Landroid/widget/EditText;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$12;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$12;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 253
    :cond_1c8
    const v0, 0x7f0e03c1

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/widget/TextView;

    .line 254
    .local v10, "view":Landroid/widget/TextView;
    if-eqz v10, :cond_1dc

    .line 255
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$13;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$13;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;)V

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 274
    .end local v10    # "view":Landroid/widget/TextView;
    :cond_1dc
    const v0, 0x7f0e03c4

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/widget/TextView;

    .line 275
    .local v10, "view":Landroid/widget/TextView;
    if-eqz v10, :cond_1f0

    .line 276
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$14;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$14;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;)V

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 296
    .end local v10    # "view":Landroid/widget/TextView;
    :cond_1f0
    const v0, 0x7f0e03bb

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/widget/TextView;

    .line 297
    .local v10, "view":Landroid/widget/TextView;
    if-eqz v10, :cond_204

    .line 298
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$15;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$15;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;)V

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 318
    .end local v10    # "view":Landroid/widget/TextView;
    :cond_204
    const v0, 0x7f0e03d1

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/widget/TextView;

    .line 319
    .local v10, "view":Landroid/widget/TextView;
    if-eqz v10, :cond_218

    .line 320
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$16;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_$16;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;)V

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 338
    .end local v10    # "view":Landroid/widget/TextView;
    :cond_218
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->carregaElementosDeTela()V

    .line 339
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 69
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->setContentView(I)V

    .line 70
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 71
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 81
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->setContentView(Landroid/view/View;)V

    .line 82
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 83
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 75
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 76
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoDarfActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 77
    return-void
.end method

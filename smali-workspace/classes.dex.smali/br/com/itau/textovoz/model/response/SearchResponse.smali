.class public Lbr/com/itau/textovoz/model/response/SearchResponse;
.super Ljava/lang/Object;
.source "SearchResponse.java"


# instance fields
.field private cards:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cards"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Cards;>;"
        }
    .end annotation
.end field

.field private classification:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "classificacao"
    .end annotation
.end field

.field private exibeFaq:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "exibeTermo"
    .end annotation
.end field

.field private faqs:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "faqs"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Faq;>;"
        }
    .end annotation
.end field

.field private menus:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "menus"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
        }
    .end annotation
.end field

.field private overflowMessages:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "transbordos"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/OverflowMessage;>;"
        }
    .end annotation
.end field

.field private receipts:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "comprovantes"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Receipt;>;"
        }
    .end annotation
.end field

.field private transactions:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "transacoes"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Transaction;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCards()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Cards;>;"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lbr/com/itau/textovoz/model/response/SearchResponse;->cards:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getClassification()Ljava/lang/String;
    .registers 2

    .line 71
    iget-object v0, p0, Lbr/com/itau/textovoz/model/response/SearchResponse;->classification:Ljava/lang/String;

    return-object v0
.end method

.method public getFaqs()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Faq;>;"
        }
    .end annotation

    .line 59
    iget-object v0, p0, Lbr/com/itau/textovoz/model/response/SearchResponse;->faqs:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getMenus()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
        }
    .end annotation

    .line 51
    iget-object v0, p0, Lbr/com/itau/textovoz/model/response/SearchResponse;->menus:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getOverflowMessages()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/OverflowMessage;>;"
        }
    .end annotation

    .line 75
    iget-object v0, p0, Lbr/com/itau/textovoz/model/response/SearchResponse;->overflowMessages:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getReceipts()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Receipt;>;"
        }
    .end annotation

    .line 83
    iget-object v0, p0, Lbr/com/itau/textovoz/model/response/SearchResponse;->receipts:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTransactions()Ljava/util/ArrayList;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Transaction;>;"
        }
    .end annotation

    .line 67
    iget-object v0, p0, Lbr/com/itau/textovoz/model/response/SearchResponse;->transactions:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isExibeFaq()Z
    .registers 2

    .line 47
    iget-boolean v0, p0, Lbr/com/itau/textovoz/model/response/SearchResponse;->exibeFaq:Z

    return v0
.end method

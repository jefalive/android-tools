.class public Lcom/google/ads/conversiontracking/g$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/ads/conversiontracking/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .registers 5

    .line 831
    invoke-static {}, Lcom/google/ads/conversiontracking/g;->a()J

    move-result-wide v0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/ads/conversiontracking/g$a;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 832
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;J)V
    .registers 5

    .line 824
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 825
    iput-object p1, p0, Lcom/google/ads/conversiontracking/g$a;->a:Ljava/lang/String;

    .line 826
    iput-object p2, p0, Lcom/google/ads/conversiontracking/g$a;->b:Ljava/lang/String;

    .line 827
    iput-wide p3, p0, Lcom/google/ads/conversiontracking/g$a;->c:J

    .line 828
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/ads/conversiontracking/g$a;
    .registers 7

    .line 851
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 852
    const/4 v0, 0x0

    return-object v0

    .line 854
    :cond_8
    const-string v0, " "

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 855
    array-length v0, v4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_14

    .line 856
    const/4 v0, 0x0

    return-object v0

    .line 859
    :cond_14
    :try_start_14
    new-instance v5, Lcom/google/ads/conversiontracking/g$a;

    const/4 v0, 0x0

    aget-object v0, v4, v0

    const/4 v1, 0x1

    aget-object v1, v4, v1

    const/4 v2, 0x2

    aget-object v2, v4, v2

    .line 862
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-direct {v5, v0, v1, v2, v3}, Lcom/google/ads/conversiontracking/g$a;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 863
    invoke-virtual {v5}, Lcom/google/ads/conversiontracking/g$a;->a()Z
    :try_end_29
    .catch Ljava/lang/NumberFormatException; {:try_start_14 .. :try_end_29} :catch_2f

    move-result v0

    if-eqz v0, :cond_2e

    .line 864
    const/4 v0, 0x0

    return-object v0

    .line 866
    :cond_2e
    return-object v5

    .line 867
    :catch_2f
    move-exception v5

    .line 868
    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic a(Lcom/google/ads/conversiontracking/g$a;)Ljava/lang/String;
    .registers 2

    .line 819
    iget-object v0, p0, Lcom/google/ads/conversiontracking/g$a;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/ads/conversiontracking/g$a;)Ljava/lang/String;
    .registers 2

    .line 819
    iget-object v0, p0, Lcom/google/ads/conversiontracking/g$a;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/ads/conversiontracking/g$a;)J
    .registers 3

    .line 819
    iget-wide v0, p0, Lcom/google/ads/conversiontracking/g$a;->c:J

    return-wide v0
.end method


# virtual methods
.method public a()Z
    .registers 5

    .line 847
    iget-wide v0, p0, Lcom/google/ads/conversiontracking/g$a;->c:J

    const-wide v2, 0x1cf7c5800L

    add-long/2addr v0, v2

    invoke-static {}, Lcom/google/ads/conversiontracking/g;->a()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_12

    const/4 v0, 0x1

    goto :goto_13

    :cond_12
    const/4 v0, 0x0

    :goto_13
    return v0
.end method

.class abstract Lcom/adobe/mobile/MessageMatcher;
.super Ljava/lang/Object;
.source "MessageMatcher.java"


# static fields
.field private static final _messageMatcherDictionary:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Class;>;"
        }
    .end annotation
.end field


# instance fields
.field protected key:Ljava/lang/String;

.field protected values:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Ljava/lang/Object;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 60
    new-instance v0, Lcom/adobe/mobile/MessageMatcher$1;

    invoke-direct {v0}, Lcom/adobe/mobile/MessageMatcher$1;-><init>()V

    sput-object v0, Lcom/adobe/mobile/MessageMatcher;->_messageMatcherDictionary:Ljava/util/Map;

    return-void
.end method

.method constructor <init>()V
    .registers 1

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static messageMatcherWithJsonObject(Lorg/json/JSONObject;)Lcom/adobe/mobile/MessageMatcher;
    .registers 11
    .param p0, "dictionary"    # Lorg/json/JSONObject;

    .line 77
    const/4 v5, 0x0

    .line 81
    .local v5, "matcher":Lcom/adobe/mobile/MessageMatcher;
    const-string v0, "matches"

    :try_start_3
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 82
    .local v6, "matcherString":Ljava/lang/String;
    if-eqz v6, :cond_17

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_17

    .line 83
    const-string v0, "Messages - message matcher type is empty"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_17
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_17} :catch_18

    .line 89
    :cond_17
    goto :goto_23

    .line 86
    .end local v6    # "matcherString":Ljava/lang/String;
    :catch_18
    move-exception v7

    .line 87
    .local v7, "ex":Lorg/json/JSONException;
    const-string v6, "UNKNOWN"

    .line 88
    .local v6, "matcherString":Ljava/lang/String;
    const-string v0, "Messages - message matcher type is required"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    .end local v7    # "ex":Lorg/json/JSONException;
    :goto_23
    sget-object v0, Lcom/adobe/mobile/MessageMatcher;->_messageMatcherDictionary:Ljava/util/Map;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/Class;

    .line 93
    .local v4, "matcherClass":Ljava/lang/Class;
    if-nez v4, :cond_3b

    .line 94
    const-class v4, Lcom/adobe/mobile/MessageMatcherUnknown;

    .line 95
    const-string v0, "Messages - message matcher type \"%s\" is invalid"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v6, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 98
    :cond_3b
    :try_start_3b
    invoke-virtual {v4}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/adobe/mobile/MessageMatcher;
    :try_end_41
    .catch Ljava/lang/InstantiationException; {:try_start_3b .. :try_end_41} :catch_43
    .catch Ljava/lang/IllegalAccessException; {:try_start_3b .. :try_end_41} :catch_54

    move-object v5, v0

    .line 105
    goto :goto_64

    .line 100
    :catch_43
    move-exception v7

    .line 101
    .local v7, "ex":Ljava/lang/InstantiationException;
    const-string v0, "Messages - Error creating matcher (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/lang/InstantiationException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    .end local v7    # "ex":Ljava/lang/InstantiationException;
    goto :goto_64

    .line 103
    :catch_54
    move-exception v7

    .line 104
    .local v7, "ex":Ljava/lang/IllegalAccessException;
    const-string v0, "Messages - Error creating matcher (%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v7}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logErrorFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    .end local v7    # "ex":Ljava/lang/IllegalAccessException;
    :goto_64
    if-eqz v5, :cond_d9

    .line 110
    const-string v0, "key"

    :try_start_68
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lcom/adobe/mobile/MessageMatcher;->key:Ljava/lang/String;

    .line 111
    iget-object v0, v5, Lcom/adobe/mobile/MessageMatcher;->key:Ljava/lang/String;

    if-eqz v0, :cond_86

    iget-object v0, v5, Lcom/adobe/mobile/MessageMatcher;->key:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_86

    .line 112
    const-string v0, "Messages - error creating matcher, key is empty"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_86
    .catch Lorg/json/JSONException; {:try_start_68 .. :try_end_86} :catch_87
    .catch Ljava/lang/NullPointerException; {:try_start_68 .. :try_end_86} :catch_91

    .line 120
    :cond_86
    goto :goto_9a

    .line 115
    :catch_87
    move-exception v7

    .line 116
    .local v7, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - error creating matcher, key is required"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 120
    .end local v7    # "ex":Lorg/json/JSONException;
    goto :goto_9a

    .line 118
    :catch_91
    move-exception v7

    .line 119
    .local v7, "ex":Ljava/lang/NullPointerException;
    const-string v0, "Messages - error creating matcher, key is required"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 123
    .end local v7    # "ex":Ljava/lang/NullPointerException;
    :goto_9a
    :try_start_9a
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v5, Lcom/adobe/mobile/MessageMatcher;->values:Ljava/util/ArrayList;
    :try_end_a1
    .catch Lorg/json/JSONException; {:try_start_9a .. :try_end_a1} :catch_d0

    .line 126
    instance-of v0, v5, Lcom/adobe/mobile/MessageMatcherExists;

    if-eqz v0, :cond_a6

    .line 127
    return-object v5

    .line 131
    :cond_a6
    const-string v0, "values"

    :try_start_a8
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 132
    .local v7, "jsonArray":Lorg/json/JSONArray;
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v8

    .line 133
    .local v8, "arrayLength":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_b1
    if-ge v9, v8, :cond_bf

    .line 134
    iget-object v0, v5, Lcom/adobe/mobile/MessageMatcher;->values:Ljava/util/ArrayList;

    invoke-virtual {v7, v9}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    add-int/lit8 v9, v9, 0x1

    goto :goto_b1

    .line 137
    .end local v9    # "i":I
    :cond_bf
    iget-object v0, v5, Lcom/adobe/mobile/MessageMatcher;->values:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_cf

    .line 138
    const-string v0, "Messages - error creating matcher, values is empty"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_cf
    .catch Lorg/json/JSONException; {:try_start_a8 .. :try_end_cf} :catch_d0

    .line 143
    .end local v7    # "jsonArray":Lorg/json/JSONArray;
    .end local v8    # "arrayLength":I
    :cond_cf
    goto :goto_d9

    .line 141
    :catch_d0
    move-exception v7

    .line 142
    .local v7, "ex":Lorg/json/JSONException;
    const-string v0, "Messages - error creating matcher, values is required"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 146
    .end local v7    # "ex":Lorg/json/JSONException;
    :cond_d9
    :goto_d9
    return-object v5
.end method


# virtual methods
.method protected matches(Ljava/lang/Object;)Z
    .registers 3
    .param p1, "value"    # Ljava/lang/Object;

    .line 171
    const/4 v0, 0x0

    return v0
.end method

.method protected varargs matchesInMaps([Ljava/util/Map;)Z
    .registers 8
    .param p1, "maps"    # [Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;)Z"
        }
    .end annotation

    .line 150
    if-eqz p1, :cond_5

    array-length v0, p1

    if-gtz v0, :cond_7

    .line 151
    :cond_5
    const/4 v0, 0x0

    return v0

    .line 154
    :cond_7
    const/4 v1, 0x0

    .line 156
    .local v1, "value":Ljava/lang/Object;
    move-object v2, p1

    array-length v3, v2

    const/4 v4, 0x0

    :goto_b
    if-ge v4, v3, :cond_24

    aget-object v5, v2, v4

    .line 157
    .local v5, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-nez v5, :cond_12

    .line 158
    goto :goto_21

    .line 161
    :cond_12
    iget-object v0, p0, Lcom/adobe/mobile/MessageMatcher;->key:Ljava/lang/String;

    invoke-interface {v5, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 162
    iget-object v0, p0, Lcom/adobe/mobile/MessageMatcher;->key:Ljava/lang/String;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 163
    goto :goto_24

    .line 156
    .end local v5    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v5
    :cond_21
    :goto_21
    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    .line 167
    :cond_24
    :goto_24
    if-eqz v1, :cond_2e

    invoke-virtual {p0, v1}, Lcom/adobe/mobile/MessageMatcher;->matches(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2e

    const/4 v0, 0x1

    goto :goto_2f

    :cond_2e
    const/4 v0, 0x0

    :goto_2f
    return v0
.end method

.method protected tryParseDouble(Ljava/lang/Object;)Ljava/lang/Double;
    .registers 4
    .param p1, "value"    # Ljava/lang/Object;

    .line 176
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_7} :catch_9

    move-result-object v0

    return-object v0

    .line 178
    :catch_9
    move-exception v1

    .line 179
    .local v1, "ex":Ljava/lang/Exception;
    const/4 v0, 0x0

    return-object v0
.end method

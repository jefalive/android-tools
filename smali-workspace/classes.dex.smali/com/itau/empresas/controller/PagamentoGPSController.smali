.class public Lcom/itau/empresas/controller/PagamentoGPSController;
.super Lcom/itau/empresas/controller/BaseController;
.source "PagamentoGPSController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/itau/empresas/controller/BaseController<Lcom/itau/empresas/api/Api;>;"
    }
.end annotation


# instance fields
.field app:Lcom/itau/empresas/CustomApplication;

.field validaEditText:Lcom/itau/empresas/ui/util/validacao/ValidaEditText;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 29
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/controller/PagamentoGPSController;Lcom/google/gson/JsonObject;)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/controller/PagamentoGPSController;
    .param p1, "x1"    # Lcom/google/gson/JsonObject;

    .line 29
    invoke-direct {p0, p1}, Lcom/itau/empresas/controller/PagamentoGPSController;->parseJson(Lcom/google/gson/JsonObject;)V

    return-void
.end method

.method private parseJson(Lcom/google/gson/JsonObject;)V
    .registers 4
    .param p1, "jsonStr"    # Lcom/google/gson/JsonObject;

    .line 102
    const-string v0, "duplicado"

    invoke-virtual {p1, v0}, Lcom/google/gson/JsonObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2a

    const-string v0, "duplicado"

    invoke-virtual {p1, v0}, Lcom/google/gson/JsonObject;->get(Ljava/lang/String;)Lcom/google/gson/JsonElement;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/JsonElement;->getAsBoolean()Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 103
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    new-instance v1, Lcom/itau/empresas/controller/PagamentoGPSController$6;

    invoke-direct {v1, p0}, Lcom/itau/empresas/controller/PagamentoGPSController$6;-><init>(Lcom/itau/empresas/controller/PagamentoGPSController;)V

    .line 104
    invoke-virtual {v1}, Lcom/itau/empresas/controller/PagamentoGPSController$6;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 103
    invoke-virtual {v0, p1, v1}, Lcom/google/gson/Gson;->fromJson(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/PagamentoGPSController;->post(Ljava/lang/Object;)V

    goto :goto_3f

    .line 106
    :cond_2a
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    new-instance v1, Lcom/itau/empresas/controller/PagamentoGPSController$7;

    invoke-direct {v1, p0}, Lcom/itau/empresas/controller/PagamentoGPSController$7;-><init>(Lcom/itau/empresas/controller/PagamentoGPSController;)V

    .line 107
    invoke-virtual {v1}, Lcom/itau/empresas/controller/PagamentoGPSController$7;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 106
    invoke-virtual {v0, p1, v1}, Lcom/google/gson/Gson;->fromJson(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/PagamentoGPSController;->post(Ljava/lang/Object;)V

    .line 109
    :goto_3f
    return-void
.end method


# virtual methods
.method public autorizarGPS()V
    .registers 2

    .line 98
    invoke-virtual {p0}, Lcom/itau/empresas/controller/PagamentoGPSController;->api()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    invoke-interface {v0}, Lcom/itau/empresas/api/Api;->incluirAutorizarGps()Lcom/google/gson/JsonObject;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/controller/PagamentoGPSController;->parseJson(Lcom/google/gson/JsonObject;)V

    .line 99
    return-void
.end method

.method public consultaAlcadas(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .param p1, "conta"    # Ljava/lang/String;
    .param p2, "operador"    # Ljava/lang/String;

    .line 112
    new-instance v0, Lcom/itau/empresas/controller/PagamentoGPSController$8;

    invoke-direct {v0, p0, p2, p1}, Lcom/itau/empresas/controller/PagamentoGPSController$8;-><init>(Lcom/itau/empresas/controller/PagamentoGPSController;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 118
    return-void
.end method

.method public consultaCnpjPagamentos()V
    .registers 2

    .line 54
    new-instance v0, Lcom/itau/empresas/controller/PagamentoGPSController$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/controller/PagamentoGPSController$2;-><init>(Lcom/itau/empresas/controller/PagamentoGPSController;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 62
    return-void
.end method

.method public consultaDadosPerfilLogado()Lcom/itau/empresas/api/model/PerfilOperadorVO;
    .registers 3

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoGPSController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getPerfilLogado()Lcom/itau/empresas/api/model/PerfilOperadorVO;

    move-result-object v1

    .line 47
    .local v1, "perfilOperadorVO":Lcom/itau/empresas/api/model/PerfilOperadorVO;
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoGPSController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getNomeEmpresa()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/itau/empresas/api/model/PerfilOperadorVO;->setNomeEmpresa(Ljava/lang/String;)V

    .line 49
    return-object v1
.end method

.method public consultaLimitesHorario(Ljava/lang/String;)V
    .registers 3
    .param p1, "tipoOperacaoSispag"    # Ljava/lang/String;

    .line 36
    new-instance v0, Lcom/itau/empresas/controller/PagamentoGPSController$1;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/controller/PagamentoGPSController$1;-><init>(Lcom/itau/empresas/controller/PagamentoGPSController;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 42
    return-void
.end method

.method public incluiComDuplicidade()V
    .registers 2

    .line 78
    new-instance v0, Lcom/itau/empresas/controller/PagamentoGPSController$4;

    invoke-direct {v0, p0}, Lcom/itau/empresas/controller/PagamentoGPSController$4;-><init>(Lcom/itau/empresas/controller/PagamentoGPSController;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 83
    return-void
.end method

.method public inclusaoGPS(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;)V
    .registers 3
    .param p1, "gpsRequestVO"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 66
    new-instance v0, Lcom/itau/empresas/controller/PagamentoGPSController$3;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/controller/PagamentoGPSController$3;-><init>(Lcom/itau/empresas/controller/PagamentoGPSController;Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 75
    return-void
.end method

.method public simularAutorizacaoGPS(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;)V
    .registers 3
    .param p1, "gpsRequestVO"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    .line 86
    new-instance v0, Lcom/itau/empresas/controller/PagamentoGPSController$5;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/controller/PagamentoGPSController$5;-><init>(Lcom/itau/empresas/controller/PagamentoGPSController;Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 95
    return-void
.end method

.method public validaTextoObrigatório(Ljava/util/List;Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;)V
    .registers 4
    .param p1, "editTextList"    # Ljava/util/List;
    .param p2, "listener"    # Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Landroid/widget/TextView;>;Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;)V"
        }
    .end annotation

    .line 122
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoGPSController;->validaEditText:Lcom/itau/empresas/ui/util/validacao/ValidaEditText;

    invoke-virtual {v0, p2}, Lcom/itau/empresas/ui/util/validacao/ValidaEditText;->setListener(Lcom/itau/empresas/ui/util/listener/ValidacaoEditTextListener;)V

    .line 123
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoGPSController;->validaEditText:Lcom/itau/empresas/ui/util/validacao/ValidaEditText;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/ui/util/validacao/ValidaEditText;->validaTextoVazio(Ljava/util/List;)V

    .line 124
    return-void
.end method

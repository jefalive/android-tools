.class public Lcom/itau/empresas/ui/util/RemoteBundleUtils;
.super Ljava/lang/Object;
.source "RemoteBundleUtils.java"


# static fields
.field private static instance:Lcom/itau/empresas/ui/util/RemoteBundleUtils;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 8
    new-instance v0, Lcom/itau/empresas/ui/util/RemoteBundleUtils;

    invoke-direct {v0}, Lcom/itau/empresas/ui/util/RemoteBundleUtils;-><init>()V

    sput-object v0, Lcom/itau/empresas/ui/util/RemoteBundleUtils;->instance:Lcom/itau/empresas/ui/util/RemoteBundleUtils;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    return-void
.end method

.method public static getInstance()Lcom/itau/empresas/ui/util/RemoteBundleUtils;
    .registers 1

    .line 16
    sget-object v0, Lcom/itau/empresas/ui/util/RemoteBundleUtils;->instance:Lcom/itau/empresas/ui/util/RemoteBundleUtils;

    return-object v0
.end method


# virtual methods
.method public isFingerprint(Lcom/itau/empresas/CustomApplication;)Z
    .registers 5
    .param p1, "application"    # Lcom/itau/empresas/CustomApplication;

    .line 23
    const-string v0, "release"

    const-string v1, "piloto"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 24
    const/4 v0, 0x1

    return v0

    .line 27
    :cond_c
    invoke-virtual {p1}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "mobilepj.toggle.fingerPrint"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

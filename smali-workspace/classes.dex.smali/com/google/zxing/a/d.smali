.class public final Lcom/google/zxing/a/d;
.super Lcom/google/zxing/a/c;


# instance fields
.field private a:Lcom/google/zxing/a/b;


# direct methods
.method public constructor <init>(Lcom/google/zxing/g;)V
    .registers 2

    invoke-direct {p0, p1}, Lcom/google/zxing/a/c;-><init>(Lcom/google/zxing/g;)V

    return-void
.end method

.method private static a(III)I
    .registers 4

    if-ge p0, p1, :cond_4

    move v0, p1

    goto :goto_9

    :cond_4
    if-le p0, p2, :cond_8

    move v0, p2

    goto :goto_9

    :cond_8
    move v0, p0

    :goto_9
    return v0
.end method

.method private static a([BIIIILcom/google/zxing/a/b;)V
    .registers 11

    const/4 v2, 0x0

    mul-int v0, p2, p4

    add-int v3, v0, p1

    :goto_5
    const/16 v0, 0x8

    if-ge v2, v0, :cond_24

    const/4 v4, 0x0

    :goto_a
    const/16 v0, 0x8

    if-ge v4, v0, :cond_20

    add-int v0, v3, v4

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    if-gt v0, p3, :cond_1d

    add-int v0, p1, v4

    add-int v1, p2, v2

    invoke-virtual {p5, v0, v1}, Lcom/google/zxing/a/b;->b(II)V

    :cond_1d
    add-int/lit8 v4, v4, 0x1

    goto :goto_a

    :cond_20
    add-int/lit8 v2, v2, 0x1

    add-int/2addr v3, p4

    goto :goto_5

    :cond_24
    return-void
.end method

.method private static a([BIIII[[ILcom/google/zxing/a/b;)V
    .registers 24

    const/4 v6, 0x0

    :goto_1
    move/from16 v0, p2

    if-ge v6, v0, :cond_5f

    shl-int/lit8 v7, v6, 0x3

    add-int/lit8 v8, p4, -0x8

    if-le v7, v8, :cond_c

    move v7, v8

    :cond_c
    const/4 v9, 0x0

    :goto_d
    move/from16 v0, p1

    if-ge v9, v0, :cond_5b

    shl-int/lit8 v10, v9, 0x3

    add-int/lit8 v11, p3, -0x8

    if-le v10, v11, :cond_18

    move v10, v11

    :cond_18
    add-int/lit8 v0, p1, -0x3

    const/4 v1, 0x2

    invoke-static {v9, v1, v0}, Lcom/google/zxing/a/d;->a(III)I

    move-result v12

    add-int/lit8 v0, p2, -0x3

    const/4 v1, 0x2

    invoke-static {v6, v1, v0}, Lcom/google/zxing/a/d;->a(III)I

    move-result v13

    const/4 v14, 0x0

    const/4 v15, -0x2

    :goto_28
    const/4 v0, 0x2

    if-gt v15, v0, :cond_49

    add-int v0, v13, v15

    aget-object v16, p5, v0

    add-int/lit8 v0, v12, -0x2

    aget v0, v16, v0

    add-int/lit8 v1, v12, -0x1

    aget v1, v16, v1

    add-int/2addr v0, v1

    aget v1, v16, v12

    add-int/2addr v0, v1

    add-int/lit8 v1, v12, 0x1

    aget v1, v16, v1

    add-int/2addr v0, v1

    add-int/lit8 v1, v12, 0x2

    aget v1, v16, v1

    add-int/2addr v0, v1

    add-int/2addr v14, v0

    add-int/lit8 v15, v15, 0x1

    goto :goto_28

    :cond_49
    div-int/lit8 v15, v14, 0x19

    move-object/from16 v0, p0

    move v1, v10

    move v2, v7

    move v3, v15

    move/from16 v4, p3

    move-object/from16 v5, p6

    invoke-static/range {v0 .. v5}, Lcom/google/zxing/a/d;->a([BIIIILcom/google/zxing/a/b;)V

    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_d

    :cond_5b
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1

    :cond_5f
    return-void
.end method

.method private static a([BIIII)[[I
    .registers 23

    move/from16 v0, p2

    move/from16 v1, p1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [[I

    const/4 v5, 0x0

    :goto_11
    move/from16 v0, p2

    if-ge v5, v0, :cond_b8

    shl-int/lit8 v6, v5, 0x3

    add-int/lit8 v7, p4, -0x8

    if-le v6, v7, :cond_1c

    move v6, v7

    :cond_1c
    const/4 v8, 0x0

    :goto_1d
    move/from16 v0, p1

    if-ge v8, v0, :cond_b4

    shl-int/lit8 v9, v8, 0x3

    add-int/lit8 v10, p3, -0x8

    if-le v9, v10, :cond_28

    move v9, v10

    :cond_28
    const/4 v11, 0x0

    const/16 v12, 0xff

    const/4 v13, 0x0

    const/4 v14, 0x0

    mul-int v0, v6, p3

    add-int v15, v0, v9

    :goto_31
    const/16 v0, 0x8

    if-ge v14, v0, :cond_81

    const/16 v16, 0x0

    :goto_37
    move/from16 v0, v16

    const/16 v1, 0x8

    if-ge v0, v1, :cond_56

    add-int v0, v15, v16

    aget-byte v0, p0, v0

    and-int/lit16 v1, v0, 0xff

    move/from16 v17, v1

    add-int v11, v11, v17

    move/from16 v0, v17

    if-ge v0, v12, :cond_4d

    move/from16 v12, v17

    :cond_4d
    move/from16 v0, v17

    if-le v0, v13, :cond_53

    move/from16 v13, v17

    :cond_53
    add-int/lit8 v16, v16, 0x1

    goto :goto_37

    :cond_56
    sub-int v0, v13, v12

    const/16 v1, 0x18

    if-le v0, v1, :cond_7b

    add-int/lit8 v14, v14, 0x1

    add-int v15, v15, p3

    :goto_60
    const/16 v0, 0x8

    if-ge v14, v0, :cond_7b

    const/16 v16, 0x0

    :goto_66
    move/from16 v0, v16

    const/16 v1, 0x8

    if-ge v0, v1, :cond_76

    add-int v0, v15, v16

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    add-int/2addr v11, v0

    add-int/lit8 v16, v16, 0x1

    goto :goto_66

    :cond_76
    add-int/lit8 v14, v14, 0x1

    add-int v15, v15, p3

    goto :goto_60

    :cond_7b
    add-int/lit8 v14, v14, 0x1

    add-int v15, v15, p3

    goto/16 :goto_31

    :cond_81
    shr-int/lit8 v14, v11, 0x6

    sub-int v0, v13, v12

    const/16 v1, 0x18

    if-gt v0, v1, :cond_ac

    div-int/lit8 v14, v12, 0x2

    if-lez v5, :cond_ac

    if-lez v8, :cond_ac

    add-int/lit8 v0, v5, -0x1

    aget-object v0, v4, v0

    aget v0, v0, v8

    aget-object v1, v4, v5

    add-int/lit8 v2, v8, -0x1

    aget v1, v1, v2

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    add-int/lit8 v1, v5, -0x1

    aget-object v1, v4, v1

    add-int/lit8 v2, v8, -0x1

    aget v1, v1, v2

    add-int/2addr v0, v1

    div-int/lit8 v15, v0, 0x4

    if-ge v12, v15, :cond_ac

    move v14, v15

    :cond_ac
    aget-object v0, v4, v5

    aput v14, v0, v8

    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1d

    :cond_b4
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_11

    :cond_b8
    return-object v4
.end method


# virtual methods
.method public a(Lcom/google/zxing/g;)Lcom/google/zxing/b;
    .registers 3

    new-instance v0, Lcom/google/zxing/a/d;

    invoke-direct {v0, p1}, Lcom/google/zxing/a/d;-><init>(Lcom/google/zxing/g;)V

    return-object v0
.end method

.method public b()Lcom/google/zxing/a/b;
    .registers 16

    iget-object v0, p0, Lcom/google/zxing/a/d;->a:Lcom/google/zxing/a/b;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/zxing/a/d;->a:Lcom/google/zxing/a/b;

    return-object v0

    :cond_7
    invoke-virtual {p0}, Lcom/google/zxing/a/d;->a()Lcom/google/zxing/g;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/zxing/g;->b()I

    move-result v8

    invoke-virtual {v7}, Lcom/google/zxing/g;->c()I

    move-result v9

    const/16 v0, 0x28

    if-lt v8, v0, :cond_45

    const/16 v0, 0x28

    if-lt v9, v0, :cond_45

    invoke-virtual {v7}, Lcom/google/zxing/g;->a()[B

    move-result-object v10

    shr-int/lit8 v11, v8, 0x3

    and-int/lit8 v0, v8, 0x7

    if-eqz v0, :cond_27

    add-int/lit8 v11, v11, 0x1

    :cond_27
    shr-int/lit8 v12, v9, 0x3

    and-int/lit8 v0, v9, 0x7

    if-eqz v0, :cond_2f

    add-int/lit8 v12, v12, 0x1

    :cond_2f
    invoke-static {v10, v11, v12, v8, v9}, Lcom/google/zxing/a/d;->a([BIIII)[[I

    move-result-object v13

    new-instance v14, Lcom/google/zxing/a/b;

    invoke-direct {v14, v8, v9}, Lcom/google/zxing/a/b;-><init>(II)V

    move-object v0, v10

    move v1, v11

    move v2, v12

    move v3, v8

    move v4, v9

    move-object v5, v13

    move-object v6, v14

    invoke-static/range {v0 .. v6}, Lcom/google/zxing/a/d;->a([BIIII[[ILcom/google/zxing/a/b;)V

    iput-object v14, p0, Lcom/google/zxing/a/d;->a:Lcom/google/zxing/a/b;

    goto :goto_4b

    :cond_45
    invoke-super {p0}, Lcom/google/zxing/a/c;->b()Lcom/google/zxing/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/zxing/a/d;->a:Lcom/google/zxing/a/b;

    :goto_4b
    iget-object v0, p0, Lcom/google/zxing/a/d;->a:Lcom/google/zxing/a/b;

    return-object v0
.end method

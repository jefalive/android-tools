.class public Lcom/itau/empresas/ui/util/AnimationUtils;
.super Ljava/lang/Object;
.source "AnimationUtils.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static animateAlpha(Landroid/view/View;IZ)V
    .registers 6
    .param p0, "view"    # Landroid/view/View;
    .param p1, "duration"    # I
    .param p2, "show"    # Z

    .line 86
    if-eqz p2, :cond_22

    .line 87
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 88
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 90
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 91
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v1, p1

    .line 92
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 93
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_3c

    .line 96
    :cond_22
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 97
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v1, p1

    .line 98
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/ui/util/AnimationUtils$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/ui/util/AnimationUtils$3;-><init>(Landroid/view/View;)V

    .line 99
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 107
    :goto_3c
    return-void
.end method

.method public static collapseAnimation(Landroid/view/View;I)V
    .registers 3
    .param p0, "view"    # Landroid/view/View;
    .param p1, "duration"    # I

    .line 167
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/itau/empresas/ui/util/AnimationUtils;->collapseAnimation(Landroid/view/View;ILandroid/view/animation/Animation$AnimationListener;)V

    .line 168
    return-void
.end method

.method public static collapseAnimation(Landroid/view/View;ILandroid/view/animation/Animation$AnimationListener;)V
    .registers 7
    .param p0, "view"    # Landroid/view/View;
    .param p1, "duration"    # I
    .param p2, "listener"    # Landroid/view/animation/Animation$AnimationListener;

    .line 172
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 174
    .local v2, "initialHeight":I
    new-instance v3, Lcom/itau/empresas/ui/util/AnimationUtils$7;

    invoke-direct {v3, p0, v2}, Lcom/itau/empresas/ui/util/AnimationUtils$7;-><init>(Landroid/view/View;I)V

    .line 193
    .local v3, "a":Landroid/view/animation/Animation;
    int-to-long v0, p1

    invoke-virtual {v3, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 194
    invoke-virtual {v3, p2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 195
    new-instance v0, Landroid/support/v4/view/animation/FastOutSlowInInterpolator;

    invoke-direct {v0}, Landroid/support/v4/view/animation/FastOutSlowInInterpolator;-><init>()V

    invoke-virtual {v3, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 196
    invoke-virtual {p0, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 197
    return-void
.end method

.method public static expandAnimation(Landroid/view/View;ILandroid/view/animation/Animation$AnimationListener;)V
    .registers 7
    .param p0, "view"    # Landroid/view/View;
    .param p1, "duration"    # I
    .param p2, "animationListener"    # Landroid/view/animation/Animation$AnimationListener;

    .line 137
    const/4 v0, -0x1

    const/4 v1, -0x2

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->measure(II)V

    .line 138
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 140
    .local v2, "targetHeight":I
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 141
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 142
    new-instance v3, Lcom/itau/empresas/ui/util/AnimationUtils$6;

    invoke-direct {v3, p0, v2}, Lcom/itau/empresas/ui/util/AnimationUtils$6;-><init>(Landroid/view/View;I)V

    .line 157
    .local v3, "a":Landroid/view/animation/Animation;
    int-to-long v0, p1

    invoke-virtual {v3, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 158
    new-instance v0, Landroid/support/v4/view/animation/FastOutSlowInInterpolator;

    invoke-direct {v0}, Landroid/support/v4/view/animation/FastOutSlowInInterpolator;-><init>()V

    invoke-virtual {v3, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 159
    if-eqz p2, :cond_2a

    .line 160
    invoke-virtual {v3, p2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 162
    :cond_2a
    invoke-virtual {p0, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 164
    return-void
.end method

.method public static flipViewAnimation(Landroid/view/View;Landroid/view/View;)V
    .registers 8
    .param p0, "v1"    # Landroid/view/View;
    .param p1, "v2"    # Landroid/view/View;

    .line 213
    sget-object v0, Landroid/view/View;->ROTATION_X:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_42

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 214
    .local v4, "visToInvis":Landroid/animation/ObjectAnimator;
    const-wide/16 v0, 0xfa

    invoke-virtual {v4, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 215
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v4, v0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 216
    sget-object v0, Landroid/view/View;->ROTATION_X:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_4a

    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 217
    .local v5, "invisToVis":Landroid/animation/ObjectAnimator;
    const-wide/16 v0, 0xfa

    invoke-virtual {v5, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 218
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v5, v0}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 219
    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    .line 220
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 221
    invoke-virtual {v5}, Landroid/animation/ObjectAnimator;->start()V

    .line 222
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 223
    return-void

    nop

    :array_42
    .array-data 4
        0x0
        0x42b40000    # 90.0f
    .end array-data

    :array_4a
    .array-data 4
        -0x3d4c0000    # -90.0f
        0x0
    .end array-data
.end method

.class public Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "ExtratoAdapter.java"

# interfaces
.implements Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter;
.implements Landroid/widget/Filterable;
.implements Lcom/itau/empresas/feature/extrato/ExtratoFilter$ExtratoFilterCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderMaisLancamentos;,
        Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyStateFuturo;,
        Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyState;,
        Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderSaldoDoDia;,
        Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$HeaderHolder;,
        Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter<Landroid/support/v7/widget/RecyclerView$ViewHolder;>;Lcom/itau/empresas/feature/extrato/adapter/StickHeaderAdapter<Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$HeaderHolder;>;Landroid/widget/Filterable;Lcom/itau/empresas/feature/extrato/ExtratoFilter$ExtratoFilterCallback;"
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private deveExibirEmptyState:Z

.field private ehFragmentFuturo:Z

.field private filtroLancamentos:Landroid/widget/Filter;

.field private listaLancamentos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/LancamentoVO;>;"
        }
    .end annotation
.end field

.field private listaSaldoDia:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field private listener:Lcom/itau/empresas/feature/extrato/fragment/OnClickMaisLancamentos;

.field private listenerTextoBusca:Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mapIcones:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
        }
    .end annotation
.end field

.field private onClickListenerMaisLancamentos:Landroid/view/View$OnClickListener;

.field private textoBuscado:Ljava/lang/String;

.field private ultimaPosicao:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/itau/empresas/feature/extrato/fragment/OnClickMaisLancamentos;Z)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/itau/empresas/feature/extrato/fragment/OnClickMaisLancamentos;
    .param p3, "ehFragmentFuturo"    # Z

    .line 72
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 63
    new-instance v0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$1;-><init>(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->onClickListenerMaisLancamentos:Landroid/view/View$OnClickListener;

    .line 73
    iput-object p1, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->context:Landroid/content/Context;

    .line 74
    iput-object p2, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listener:Lcom/itau/empresas/feature/extrato/fragment/OnClickMaisLancamentos;

    .line 75
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mapIcones:Ljava/util/Map;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaLancamentos:Ljava/util/List;

    .line 77
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 78
    iput-boolean p3, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->ehFragmentFuturo:Z

    .line 79
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->inicializaListaFiltroSaldoDia()V

    .line 81
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;)Lcom/itau/empresas/feature/extrato/fragment/OnClickMaisLancamentos;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;

    .line 35
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listener:Lcom/itau/empresas/feature/extrato/fragment/OnClickMaisLancamentos;

    return-object v0
.end method

.method private bindDadosExtrato(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;IZ)V
    .registers 11
    .param p1, "holder"    # Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;
    .param p2, "position"    # I
    .param p3, "saldoDia"    # Z

    .line 206
    invoke-direct {p0, p2}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->ehUltimaPosicao(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 207
    return-void

    .line 210
    :cond_7
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaLancamentos:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/itau/empresas/api/model/LancamentoVO;

    .line 212
    .local v5, "lancamento":Lcom/itau/empresas/api/model/LancamentoVO;
    invoke-virtual {v5}, Lcom/itau/empresas/api/model/LancamentoVO;->getValorLancamento()Ljava/math/BigDecimal;

    move-result-object v0

    if-nez v0, :cond_17

    .line 213
    return-void

    .line 215
    :cond_17
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->context:Landroid/content/Context;

    .line 216
    invoke-virtual {v5}, Lcom/itau/empresas/api/model/LancamentoVO;->getValorLancamento()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v1

    .line 217
    invoke-direct {p0, p2}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->deveExibirSaldoDia(I)Z

    move-result v3

    .line 216
    invoke-static {v0, v1, v2, v3}, Lcom/itau/empresas/feature/extrato/util/ExtratoUtil;->getCorItens(Landroid/content/Context;DZ)I

    move-result v6

    .line 219
    .local v6, "cor":I
    # getter for: Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->rlDados:Landroid/widget/RelativeLayout;
    invoke-static {p1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->access$1000(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;)Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->context:Landroid/content/Context;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 221
    invoke-virtual {v5}, Lcom/itau/empresas/api/model/LancamentoVO;->getDescricaoLancamento()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 223
    invoke-virtual {v5}, Lcom/itau/empresas/api/model/LancamentoVO;->getValorLancamento()Ljava/math/BigDecimal;

    move-result-object v3

    .line 222
    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v2, v4

    .line 220
    const v3, 0x7f07009b

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 219
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 226
    # getter for: Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->icone:Lcom/itau/empresas/ui/view/TextViewIcon;
    invoke-static {p1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->access$1100(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;)Lcom/itau/empresas/ui/view/TextViewIcon;

    move-result-object v0

    invoke-virtual {v5}, Lcom/itau/empresas/api/model/LancamentoVO;->getCategoriaLancamento()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->getIcone(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/TextViewIcon;->setText(Ljava/lang/CharSequence;)V

    .line 227
    # getter for: Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->textoDescricao:Landroid/widget/TextView;
    invoke-static {p1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->access$1200(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v5}, Lcom/itau/empresas/api/model/LancamentoVO;->getDescricaoLancamento()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    # getter for: Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->textoValor:Landroid/widget/TextView;
    invoke-static {p1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->access$1300(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    .line 231
    invoke-virtual {v5}, Lcom/itau/empresas/api/model/LancamentoVO;->getValorLancamento()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v1

    .line 230
    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetarioComCifrao(D)Ljava/lang/String;

    move-result-object v1

    .line 228
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 233
    # getter for: Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->icone:Lcom/itau/empresas/ui/view/TextViewIcon;
    invoke-static {p1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->access$1100(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;)Lcom/itau/empresas/ui/view/TextViewIcon;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/itau/empresas/ui/view/TextViewIcon;->setTextColor(I)V

    .line 234
    # getter for: Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->textoDescricao:Landroid/widget/TextView;
    invoke-static {p1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->access$1200(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 235
    # getter for: Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->textoValor:Landroid/widget/TextView;
    invoke-static {p1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->access$1300(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 236
    if-eqz p3, :cond_96

    .line 237
    invoke-direct {p0, p1, v5}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->customizaSaldoDoDia(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;Lcom/itau/empresas/api/model/LancamentoVO;)V

    .line 239
    :cond_96
    # getter for: Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->linhaVertical:Landroid/view/View;
    invoke-static {p1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->access$1400(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    add-int/lit8 v1, p2, 0x2

    iget v2, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->ultimaPosicao:I

    if-ne v1, v2, :cond_a3

    const/16 v1, 0x8

    goto :goto_a4

    :cond_a3
    const/4 v1, 0x0

    .line 240
    :goto_a4
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 241
    return-void
.end method

.method private bindEmptyState(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyState;)V
    .registers 7
    .param p1, "holderIten"    # Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyState;

    .line 197
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->textoBuscado:Ljava/lang/String;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->context:Landroid/content/Context;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->textoBuscado:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 198
    const v2, 0x7f07020e

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1f

    :cond_16
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->context:Landroid/content/Context;

    .line 200
    const v1, 0x7f0701ea

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 201
    .local v4, "mensagem":Ljava/lang/String;
    :goto_1f
    # getter for: Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyState;->textoEmptyState:Landroid/widget/TextView;
    invoke-static {p1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyState;->access$900(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyState;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    return-void
.end method

.method private customizaSaldoDoDia(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;Lcom/itau/empresas/api/model/LancamentoVO;)V
    .registers 6
    .param p1, "viewHolder"    # Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;
    .param p2, "lancamentoVO"    # Lcom/itau/empresas/api/model/LancamentoVO;

    .line 244
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->context:Landroid/content/Context;

    const v1, 0x7f0c0149

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    .line 245
    .local v2, "cor":I
    # getter for: Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->textoDescricao:Landroid/widget/TextView;
    invoke-static {p1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->access$1200(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 246
    # getter for: Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->icone:Lcom/itau/empresas/ui/view/TextViewIcon;
    invoke-static {p1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->access$1100(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;)Lcom/itau/empresas/ui/view/TextViewIcon;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/itau/empresas/ui/view/TextViewIcon;->setTextColor(I)V

    .line 247
    invoke-virtual {p2}, Lcom/itau/empresas/api/model/LancamentoVO;->getValorLancamento()Ljava/math/BigDecimal;

    move-result-object v0

    if-nez v0, :cond_1e

    .line 248
    return-void

    .line 250
    :cond_1e
    # getter for: Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->icone:Lcom/itau/empresas/ui/view/TextViewIcon;
    invoke-static {p1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;->access$1100(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;)Lcom/itau/empresas/ui/view/TextViewIcon;

    move-result-object v0

    const v1, 0x7f07025f

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/TextViewIcon;->setText(I)V

    .line 251
    return-void
.end method

.method private deveExibirEmptyStateAbaLancamentosFuturos()Z
    .registers 2

    .line 324
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaLancamentos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_e

    iget-boolean v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->ehFragmentFuturo:Z

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

.method private deveExibirEmptyStatte()Z
    .registers 2

    .line 328
    iget-boolean v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->deveExibirEmptyState:Z

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaLancamentos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_c
    const/4 v0, 0x1

    goto :goto_f

    :cond_e
    const/4 v0, 0x0

    :goto_f
    return v0
.end method

.method private deveExibirMaisLancamentos(I)Z
    .registers 4
    .param p1, "position"    # I

    .line 332
    add-int/lit8 v0, p1, 0x1

    iget v1, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->ultimaPosicao:I

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    goto :goto_9

    :cond_8
    const/4 v0, 0x0

    :goto_9
    return v0
.end method

.method private deveExibirSaldoDia(I)Z
    .registers 5
    .param p1, "position"    # I

    .line 285
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->ehUltimaPosicao(I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 286
    const/4 v0, 0x0

    return v0

    .line 288
    :cond_8
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaLancamentos:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/itau/empresas/api/model/LancamentoVO;

    .line 289
    .local v1, "lancamento":Lcom/itau/empresas/api/model/LancamentoVO;
    const-string v2, ""

    .line 290
    .local v2, "descricaoLancamento":Ljava/lang/String;
    if-eqz v1, :cond_1f

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/LancamentoVO;->getDescricaoLancamento()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1f

    .line 291
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/LancamentoVO;->getDescricaoLancamento()Ljava/lang/String;

    move-result-object v2

    .line 293
    :cond_1f
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaSaldoDia:Ljava/util/Set;

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private ehUltimaPosicao(I)Z
    .registers 4
    .param p1, "position"    # I

    .line 297
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaLancamentos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_16

    add-int/lit8 v0, p1, 0x1

    iget v1, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->ultimaPosicao:I

    if-eq v0, v1, :cond_16

    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaLancamentos:Ljava/util/List;

    .line 298
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_18

    :cond_16
    const/4 v0, 0x1

    goto :goto_19

    :cond_18
    const/4 v0, 0x0

    .line 297
    :goto_19
    return v0
.end method

.method private getIcone(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .param p1, "categoriaLancamento"    # Ljava/lang/String;

    .line 318
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mapIcones:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_12

    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->context:Landroid/content/Context;

    .line 319
    const v1, 0x7f07027a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_24

    :cond_12
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mapIcones:Ljava/util/Map;

    .line 320
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 318
    :goto_24
    return-object v0
.end method

.method private inicializaListaFiltroSaldoDia()V
    .registers 3

    .line 84
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaSaldoDia:Ljava/util/Set;

    .line 85
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaSaldoDia:Ljava/util/Set;

    const-string v1, "SALDO ANTERIOR"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 86
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaSaldoDia:Ljava/util/Set;

    const-string v1, "SALDO DO DIA"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaSaldoDia:Ljava/util/Set;

    const-string v1, "SALDO A LIBERAR"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaSaldoDia:Ljava/util/Set;

    const-string v1, "SALDO FINAL DISP CREDOR"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 89
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaSaldoDia:Ljava/util/Set;

    const-string v1, "SALDO FINAL DISP DEVEDOR"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 90
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaSaldoDia:Ljava/util/Set;

    const-string v1, "S A L D O"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 91
    return-void
.end method

.method private inicializarMapIcones()V
    .registers 4

    .line 302
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mapIcones:Ljava/util/Map;

    const-string v1, "cartoes"

    const v2, 0x7f07025a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mapIcones:Ljava/util/Map;

    const-string v1, "juros_descontos"

    const v2, 0x7f07036e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mapIcones:Ljava/util/Map;

    const-string v1, "salarios"

    const v2, 0x7f07027f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mapIcones:Ljava/util/Map;

    const-string v1, "saque"

    const v2, 0x7f070281

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mapIcones:Ljava/util/Map;

    const-string v1, "investimento"

    const v2, 0x7f070275

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 307
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mapIcones:Ljava/util/Map;

    const-string v1, "transfer\u00eancia"

    const v2, 0x7f07028c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mapIcones:Ljava/util/Map;

    const-string v1, "pagamento"

    const v2, 0x7f070263

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mapIcones:Ljava/util/Map;

    const-string v1, "dep\u00f3sito"

    const v2, 0x7f070269

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mapIcones:Ljava/util/Map;

    const-string v1, "capitaliza\u00e7\u00e3o"

    const v2, 0x7f07024f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mapIcones:Ljava/util/Map;

    const-string v1, "saldo"

    const v2, 0x7f070280

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 312
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mapIcones:Ljava/util/Map;

    const-string v1, "outros"

    const v2, 0x7f07027a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 313
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mapIcones:Ljava/util/Map;

    const-string v1, "outras"

    const v2, 0x7f07027a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mapIcones:Ljava/util/Map;

    const-string v1, "cifrao"

    const v2, 0x7f07025f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 315
    return-void
.end method


# virtual methods
.method public aplicarResultadoDeBusca(Ljava/util/List;)V
    .registers 4
    .param p1, "lancamentos"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/LancamentoVO;>;)V"
        }
    .end annotation

    .line 352
    iput-object p1, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaLancamentos:Ljava/util/List;

    .line 353
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaLancamentos:Ljava/util/List;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->setDados(Ljava/util/List;I)V

    .line 354
    return-void
.end method

.method public exibeEmptyState()V
    .registers 2

    .line 364
    new-instance v0, Lcom/itau/empresas/api/model/LancamentoVO;

    invoke-direct {v0}, Lcom/itau/empresas/api/model/LancamentoVO;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaLancamentos:Ljava/util/List;

    .line 365
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->textoBuscado:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_15

    const/4 v0, 0x1

    goto :goto_16

    :cond_15
    const/4 v0, 0x0

    :goto_16
    iput-boolean v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->deveExibirEmptyState:Z

    .line 366
    return-void
.end method

.method public exibeTextoDaBusca(Ljava/lang/String;)V
    .registers 2
    .param p1, "textoBuscado"    # Ljava/lang/String;

    .line 358
    iput-object p1, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->textoBuscado:Ljava/lang/String;

    .line 359
    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->notifyDataSetChanged()V

    .line 360
    return-void
.end method

.method public getFilter()Landroid/widget/Filter;
    .registers 4

    .line 267
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->filtroLancamentos:Landroid/widget/Filter;

    if-nez v0, :cond_f

    .line 268
    new-instance v0, Lcom/itau/empresas/feature/extrato/ExtratoFilter;

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaLancamentos:Ljava/util/List;

    iget-object v2, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listenerTextoBusca:Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;

    invoke-direct {v0, v1, p0, v2}, Lcom/itau/empresas/feature/extrato/ExtratoFilter;-><init>(Ljava/util/List;Lcom/itau/empresas/feature/extrato/ExtratoFilter$ExtratoFilterCallback;Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->filtroLancamentos:Landroid/widget/Filter;

    .line 270
    :cond_f
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->filtroLancamentos:Landroid/widget/Filter;

    return-object v0
.end method

.method public getHeaderId(I)J
    .registers 4
    .param p1, "position"    # I

    .line 95
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->ehUltimaPosicao(I)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 96
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->deveExibirEmptyStateAbaLancamentosFuturos()Z

    move-result v0

    if-nez v0, :cond_1a

    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaLancamentos:Ljava/util/List;

    .line 97
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/LancamentoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/LancamentoVO;->getDataLancamento()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1d

    .line 98
    :cond_1a
    const-wide/16 v0, -0x1

    return-wide v0

    .line 100
    :cond_1d
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaLancamentos:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/LancamentoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/LancamentoVO;->getDataLancamento()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->parse(Ljava/lang/String;)Lorg/threeten/bp/LocalDate;

    move-result-object v0

    .line 101
    invoke-virtual {v0}, Lorg/threeten/bp/LocalDate;->toEpochDay()J

    move-result-wide v0

    .line 100
    return-wide v0
.end method

.method public getItemCount()I
    .registers 3

    .line 256
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaLancamentos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_a

    .line 257
    const/4 v0, 0x1

    return v0

    .line 258
    :cond_a
    iget-boolean v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->ehFragmentFuturo:Z

    if-eqz v0, :cond_15

    .line 259
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaLancamentos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0

    .line 261
    :cond_15
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaLancamentos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getItemViewType(I)I
    .registers 3
    .param p1, "position"    # I

    .line 337
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->deveExibirEmptyStateAbaLancamentosFuturos()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 338
    const/4 v0, 0x2

    return v0

    .line 339
    :cond_8
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->deveExibirEmptyStatte()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 340
    const/4 v0, 0x1

    return v0

    .line 341
    :cond_10
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->deveExibirMaisLancamentos(I)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 342
    const/4 v0, 0x5

    return v0

    .line 343
    :cond_18
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->deveExibirSaldoDia(I)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 344
    const/4 v0, 0x4

    return v0

    .line 346
    :cond_20
    const/4 v0, 0x3

    return v0
.end method

.method public mostraEstadoOriginal()V
    .registers 2

    .line 370
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->deveExibirEmptyState:Z

    .line 371
    return-void
.end method

.method public bridge synthetic onBindHeaderViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .registers 4

    .line 35
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$HeaderHolder;

    invoke-virtual {p0, v0, p2}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->onBindHeaderViewHolder(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$HeaderHolder;I)V

    return-void
.end method

.method public onBindHeaderViewHolder(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$HeaderHolder;I)V
    .registers 8
    .param p1, "viewholder"    # Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$HeaderHolder;
    .param p2, "position"    # I

    .line 117
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaLancamentos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p2, v0, :cond_9

    .line 118
    return-void

    .line 120
    :cond_9
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaLancamentos:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/model/LancamentoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/LancamentoVO;->getDataLancamento()Ljava/lang/String;

    move-result-object v4

    .line 121
    .local v4, "dataLancamento":Ljava/lang/String;
    # getter for: Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$HeaderHolder;->textoDataLancamento:Landroid/widget/TextView;
    invoke-static {p1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$HeaderHolder;->access$200(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$HeaderHolder;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {v4}, Lcom/itau/empresas/feature/extrato/util/ExtratoUtil;->formatarDataPorExtenso(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    # getter for: Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$HeaderHolder;->textoDataLancamento:Landroid/widget/TextView;
    invoke-static {p1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$HeaderHolder;->access$200(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$HeaderHolder;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->context:Landroid/content/Context;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v4, v2, v3

    .line 123
    const v3, 0x7f07009c

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 122
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 125
    return-void
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .registers 12
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .line 164
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v3

    .line 165
    .local v3, "viewType":I
    packed-switch v3, :pswitch_data_4e

    goto :goto_3a

    .line 167
    :pswitch_8
    move-object v4, p1

    check-cast v4, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyState;

    .line 168
    .local v4, "holderIten":Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyState;
    invoke-direct {p0, v4}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->bindEmptyState(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyState;)V

    .line 169
    goto :goto_4d

    .line 171
    .end local v4    # "holderIten":Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyState;
    :pswitch_f
    move-object v5, p1

    check-cast v5, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyStateFuturo;

    .line 172
    .local v5, "holderEmptyStateFuturo":Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyStateFuturo;
    # getter for: Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyStateFuturo;->textView:Landroid/widget/TextView;
    invoke-static {v5}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyStateFuturo;->access$700(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyStateFuturo;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0701eb

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 173
    goto :goto_4d

    .line 175
    .end local v5    # "holderEmptyStateFuturo":Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyStateFuturo;
    :pswitch_1d
    move-object v0, p1

    check-cast v0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p2, v1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->bindDadosExtrato(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;IZ)V

    .line 176
    goto :goto_4d

    .line 178
    :pswitch_25
    move-object v6, p1

    check-cast v6, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderSaldoDoDia;

    .line 179
    .local v6, "holderSaldoDoDia":Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderSaldoDoDia;
    const/4 v0, 0x1

    invoke-direct {p0, v6, p2, v0}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->bindDadosExtrato(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;IZ)V

    .line 181
    goto :goto_4d

    .line 183
    .end local v6    # "holderSaldoDoDia":Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderSaldoDoDia;
    :pswitch_2d
    move-object v7, p1

    check-cast v7, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderMaisLancamentos;

    .line 184
    .local v7, "holderMaisLancamentos":Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderMaisLancamentos;
    # getter for: Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderMaisLancamentos;->maisLancamentos:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderMaisLancamentos;->access$800(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderMaisLancamentos;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->onClickListenerMaisLancamentos:Landroid/view/View$OnClickListener;

    .line 185
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    goto :goto_4d

    .line 188
    .end local v7    # "holderMaisLancamentos":Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderMaisLancamentos;
    :goto_3a
    move-object v8, p1

    check-cast v8, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyState;

    .line 189
    .local v8, "holderEmpty":Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyState;
    # getter for: Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyState;->textoEmptyState:Landroid/widget/TextView;
    invoke-static {v8}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyState;->access$900(Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyState;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->context:Landroid/content/Context;

    .line 190
    const v2, 0x7f0701ea

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 189
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    .end local v8    # "holderEmpty":Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyState;
    :goto_4d
    return-void

    :pswitch_data_4e
    .packed-switch 0x1
        :pswitch_8
        :pswitch_f
        :pswitch_1d
        :pswitch_25
        :pswitch_2d
    .end packed-switch
.end method

.method public bridge synthetic onCreateHeaderViewHolder(Landroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .registers 3

    .line 35
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->onCreateHeaderViewHolder(Landroid/view/ViewGroup;)Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$HeaderHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateHeaderViewHolder(Landroid/view/ViewGroup;)Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$HeaderHolder;
    .registers 6
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .line 111
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0300f0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 112
    .local v3, "view":Landroid/view/View;
    new-instance v0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$HeaderHolder;

    const/4 v1, 0x0

    invoke-direct {v0, v3, v1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$HeaderHolder;-><init>(Landroid/view/View;Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$1;)V

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .registers 7
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .line 130
    const/4 v3, 0x0

    .line 131
    .local v3, "viewHolder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    packed-switch p2, :pswitch_data_6e

    goto/16 :goto_5a

    .line 133
    :pswitch_6
    new-instance v3, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyState;

    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 134
    const v1, 0x7f03008f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyState;-><init>(Landroid/view/View;)V

    .line 135
    goto :goto_6d

    .line 137
    :pswitch_16
    new-instance v3, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyStateFuturo;

    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 138
    const v1, 0x7f030090

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {v3, v0, v1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyStateFuturo;-><init>(Landroid/view/View;Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$1;)V

    .line 139
    goto :goto_6d

    .line 141
    :pswitch_27
    new-instance v3, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderSaldoDoDia;

    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 142
    const v1, 0x7f030103

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {v3, v0, v1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderSaldoDoDia;-><init>(Landroid/view/View;Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$1;)V

    .line 143
    goto :goto_6d

    .line 145
    :pswitch_38
    new-instance v3, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderMaisLancamentos;

    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 146
    const v1, 0x7f030100

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {v3, v0, v1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderMaisLancamentos;-><init>(Landroid/view/View;Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$1;)V

    .line 147
    goto :goto_6d

    .line 149
    :pswitch_49
    new-instance v3, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;

    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 150
    const v1, 0x7f030102

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {v3, v0, v1}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolder;-><init>(Landroid/view/View;Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$1;)V

    .line 151
    goto :goto_6d

    .line 153
    :goto_5a
    iget-boolean v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->deveExibirEmptyState:Z

    if-eqz v0, :cond_6d

    .line 154
    new-instance v3, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyState;

    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 155
    const v1, 0x7f03008e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter$ViewHolderEmptyState;-><init>(Landroid/view/View;)V

    .line 159
    :cond_6d
    :goto_6d
    return-object v3

    :pswitch_data_6e
    .packed-switch 0x1
        :pswitch_6
        :pswitch_16
        :pswitch_49
        :pswitch_27
        :pswitch_38
    .end packed-switch
.end method

.method public setDados(Ljava/util/List;I)V
    .registers 5
    .param p1, "listaLancamentos"    # Ljava/util/List;
    .param p2, "ordem"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/LancamentoVO;>;I)V"
        }
    .end annotation

    .line 274
    if-eqz p1, :cond_14

    .line 275
    iput-object p1, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaLancamentos:Ljava/util/List;

    .line 276
    invoke-direct {p0}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->inicializarMapIcones()V

    .line 277
    invoke-static {p1, p2}, Lcom/itau/empresas/feature/extrato/util/ExtratoUtil;->ordenar(Ljava/util/List;I)V

    .line 278
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listaLancamentos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->ultimaPosicao:I

    .line 281
    :cond_14
    invoke-virtual {p0}, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->notifyDataSetChanged()V

    .line 282
    return-void
.end method

.method public setListenerTextoBusca(Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;)V
    .registers 2
    .param p1, "listenerTextoBusca"    # Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;

    .line 106
    iput-object p1, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->listenerTextoBusca:Lcom/itau/empresas/feature/extrato/ExtratoFilter$ListenerTextoBusca;

    .line 107
    return-void
.end method

.method public setTextoBuscado(Ljava/lang/String;)V
    .registers 3
    .param p1, "textoBuscado"    # Ljava/lang/String;

    .line 374
    iput-object p1, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->textoBuscado:Ljava/lang/String;

    .line 375
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->textoBuscado:Ljava/lang/String;

    if-eqz v0, :cond_13

    .line 376
    iget-object v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->textoBuscado:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    const/4 v0, 0x1

    goto :goto_11

    :cond_10
    const/4 v0, 0x0

    :goto_11
    iput-boolean v0, p0, Lcom/itau/empresas/feature/extrato/adapter/ExtratoAdapter;->deveExibirEmptyState:Z

    .line 379
    :cond_13
    return-void
.end method

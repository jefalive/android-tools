.class Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$1;
.super Ljava/lang/Object;
.source "ReceiptsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;


# direct methods
.method constructor <init>(Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;)V
    .registers 2
    .param p1, "this$0"    # Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;

    .line 71
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$1;->this$0:Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 4
    .param p1, "v"    # Landroid/view/View;

    .line 74
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$1;->this$0:Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;

    # getter for: Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->moreItemsLinear:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->access$000(Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 75
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$1;->this$0:Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;

    # getter for: Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->receiptsAdapter:Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->access$200(Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;)Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$1;->this$0:Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;

    # getter for: Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->receipts:Ljava/util/List;
    invoke-static {v1}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->access$100(Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;->setReceipts(Ljava/util/List;)V

    .line 76
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$1;->this$0:Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;

    # getter for: Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->receiptsAdapter:Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;->access$200(Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment;)Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/textovoz/ui/fragment/ReceiptsFragment$ReceiptsAdapter;->notifyDataSetChanged()V

    .line 77
    return-void
.end method

.class public Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;
.super Ljava/lang/Object;
.source "DetalhesMultilimiteXMLVO.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Detalhe"
.end annotation

.annotation runtime Lorg/simpleframework/xml/Root;
    name = "DETALHE"
.end annotation


# instance fields
.field private literal:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "LITERAL"
        required = false
    .end annotation
.end field

.field private sinal:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "SINAL"
        required = false
    .end annotation
.end field

.field private valor:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "VALOR"
        required = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getLiteral()Ljava/lang/String;
    .registers 2

    .line 91
    iget-object v0, p0, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;->literal:Ljava/lang/String;

    return-object v0
.end method

.method public getSinal()Ljava/lang/String;
    .registers 2

    .line 103
    iget-object v0, p0, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;->sinal:Ljava/lang/String;

    return-object v0
.end method

.method public getValor()Ljava/lang/String;
    .registers 2

    .line 95
    iget-object v0, p0, Lcom/itau/empresas/api/universal/model/DetalhesMultilimiteXMLVO$Detalhe;->valor:Ljava/lang/String;

    return-object v0
.end method

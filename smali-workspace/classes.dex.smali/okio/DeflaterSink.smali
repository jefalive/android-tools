.class public final Lokio/DeflaterSink;
.super Ljava/lang/Object;
.source "DeflaterSink.java"

# interfaces
.implements Lokio/Sink;


# instance fields
.field private closed:Z

.field private final deflater:Ljava/util/zip/Deflater;

.field private final sink:Lokio/BufferedSink;


# direct methods
.method constructor <init>(Lokio/BufferedSink;Ljava/util/zip/Deflater;)V
    .registers 5
    .param p1, "sink"    # Lokio/BufferedSink;
    .param p2, "deflater"    # Ljava/util/zip/Deflater;

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    if-nez p1, :cond_d

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_d
    if-nez p2, :cond_17

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "inflater == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_17
    iput-object p1, p0, Lokio/DeflaterSink;->sink:Lokio/BufferedSink;

    .line 56
    iput-object p2, p0, Lokio/DeflaterSink;->deflater:Ljava/util/zip/Deflater;

    .line 57
    return-void
.end method

.method public constructor <init>(Lokio/Sink;Ljava/util/zip/Deflater;)V
    .registers 4
    .param p1, "sink"    # Lokio/Sink;
    .param p2, "deflater"    # Ljava/util/zip/Deflater;

    .line 44
    invoke-static {p1}, Lokio/Okio;->buffer(Lokio/Sink;)Lokio/BufferedSink;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lokio/DeflaterSink;-><init>(Lokio/BufferedSink;Ljava/util/zip/Deflater;)V

    .line 45
    return-void
.end method

.method private deflate(Z)V
    .registers 10
    .param p1, "syncFlush"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .annotation build Lorg/codehaus/mojo/animal_sniffer/IgnoreJRERequirement;
    .end annotation

    .line 84
    iget-object v0, p0, Lokio/DeflaterSink;->sink:Lokio/BufferedSink;

    invoke-interface {v0}, Lokio/BufferedSink;->buffer()Lokio/Buffer;

    move-result-object v5

    .line 86
    .local v5, "buffer":Lokio/Buffer;
    :goto_6
    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Lokio/Buffer;->writableSegment(I)Lokio/Segment;

    move-result-object v6

    .line 92
    .local v6, "s":Lokio/Segment;
    if-eqz p1, :cond_1d

    iget-object v0, p0, Lokio/DeflaterSink;->deflater:Ljava/util/zip/Deflater;

    iget-object v1, v6, Lokio/Segment;->data:[B

    iget v2, v6, Lokio/Segment;->limit:I

    iget v3, v6, Lokio/Segment;->limit:I

    rsub-int v3, v3, 0x2000

    .line 93
    const/4 v4, 0x2

    invoke-virtual {v0, v1, v2, v3, v4}, Ljava/util/zip/Deflater;->deflate([BIII)I

    move-result v7

    goto :goto_2b

    :cond_1d
    iget-object v0, p0, Lokio/DeflaterSink;->deflater:Ljava/util/zip/Deflater;

    iget-object v1, v6, Lokio/Segment;->data:[B

    iget v2, v6, Lokio/Segment;->limit:I

    iget v3, v6, Lokio/Segment;->limit:I

    rsub-int v3, v3, 0x2000

    .line 94
    invoke-virtual {v0, v1, v2, v3}, Ljava/util/zip/Deflater;->deflate([BII)I

    move-result v7

    .line 96
    .local v7, "deflated":I
    :goto_2b
    if-lez v7, :cond_3e

    .line 97
    iget v0, v6, Lokio/Segment;->limit:I

    add-int/2addr v0, v7

    iput v0, v6, Lokio/Segment;->limit:I

    .line 98
    iget-wide v0, v5, Lokio/Buffer;->size:J

    int-to-long v2, v7

    add-long/2addr v0, v2

    iput-wide v0, v5, Lokio/Buffer;->size:J

    .line 99
    iget-object v0, p0, Lokio/DeflaterSink;->sink:Lokio/BufferedSink;

    invoke-interface {v0}, Lokio/BufferedSink;->emitCompleteSegments()Lokio/BufferedSink;

    goto :goto_56

    .line 100
    :cond_3e
    iget-object v0, p0, Lokio/DeflaterSink;->deflater:Ljava/util/zip/Deflater;

    invoke-virtual {v0}, Ljava/util/zip/Deflater;->needsInput()Z

    move-result v0

    if-eqz v0, :cond_56

    .line 101
    iget v0, v6, Lokio/Segment;->pos:I

    iget v1, v6, Lokio/Segment;->limit:I

    if-ne v0, v1, :cond_55

    .line 103
    invoke-virtual {v6}, Lokio/Segment;->pop()Lokio/Segment;

    move-result-object v0

    iput-object v0, v5, Lokio/Buffer;->head:Lokio/Segment;

    .line 104
    invoke-static {v6}, Lokio/SegmentPool;->recycle(Lokio/Segment;)V

    .line 106
    :cond_55
    return-void

    .line 108
    .end local v6    # "s":Lokio/Segment;
    .end local v7    # "deflated":I
    :cond_56
    :goto_56
    goto/16 :goto_6
.end method


# virtual methods
.method public close()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 122
    iget-boolean v0, p0, Lokio/DeflaterSink;->closed:Z

    if-eqz v0, :cond_5

    return-void

    .line 126
    :cond_5
    const/4 v1, 0x0

    .line 128
    .local v1, "thrown":Ljava/lang/Throwable;
    :try_start_6
    invoke-virtual {p0}, Lokio/DeflaterSink;->finishDeflate()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_9} :catch_a

    .line 131
    goto :goto_c

    .line 129
    :catch_a
    move-exception v2

    .line 130
    .local v2, "e":Ljava/lang/Throwable;
    move-object v1, v2

    .line 134
    .end local v2    # "e":Ljava/lang/Throwable;
    :goto_c
    :try_start_c
    iget-object v0, p0, Lokio/DeflaterSink;->deflater:Ljava/util/zip/Deflater;

    invoke-virtual {v0}, Ljava/util/zip/Deflater;->end()V
    :try_end_11
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_11} :catch_12

    .line 137
    goto :goto_16

    .line 135
    :catch_12
    move-exception v2

    .line 136
    .local v2, "e":Ljava/lang/Throwable;
    if-nez v1, :cond_16

    move-object v1, v2

    .line 140
    .end local v2    # "e":Ljava/lang/Throwable;
    :cond_16
    :goto_16
    :try_start_16
    iget-object v0, p0, Lokio/DeflaterSink;->sink:Lokio/BufferedSink;

    invoke-interface {v0}, Lokio/BufferedSink;->close()V
    :try_end_1b
    .catch Ljava/lang/Throwable; {:try_start_16 .. :try_end_1b} :catch_1c

    .line 143
    goto :goto_20

    .line 141
    :catch_1c
    move-exception v2

    .line 142
    .local v2, "e":Ljava/lang/Throwable;
    if-nez v1, :cond_20

    move-object v1, v2

    .line 144
    .end local v2    # "e":Ljava/lang/Throwable;
    :cond_20
    :goto_20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lokio/DeflaterSink;->closed:Z

    .line 146
    if-eqz v1, :cond_28

    invoke-static {v1}, Lokio/Util;->sneakyRethrow(Ljava/lang/Throwable;)V

    .line 147
    :cond_28
    return-void
.end method

.method finishDeflate()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 117
    iget-object v0, p0, Lokio/DeflaterSink;->deflater:Ljava/util/zip/Deflater;

    invoke-virtual {v0}, Ljava/util/zip/Deflater;->finish()V

    .line 118
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lokio/DeflaterSink;->deflate(Z)V

    .line 119
    return-void
.end method

.method public flush()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 112
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lokio/DeflaterSink;->deflate(Z)V

    .line 113
    iget-object v0, p0, Lokio/DeflaterSink;->sink:Lokio/BufferedSink;

    invoke-interface {v0}, Lokio/BufferedSink;->flush()V

    .line 114
    return-void
.end method

.method public timeout()Lokio/Timeout;
    .registers 2

    .line 150
    iget-object v0, p0, Lokio/DeflaterSink;->sink:Lokio/BufferedSink;

    invoke-interface {v0}, Lokio/BufferedSink;->timeout()Lokio/Timeout;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DeflaterSink("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lokio/DeflaterSink;->sink:Lokio/BufferedSink;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write(Lokio/Buffer;J)V
    .registers 12
    .param p1, "source"    # Lokio/Buffer;
    .param p2, "byteCount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 60
    iget-wide v0, p1, Lokio/Buffer;->size:J

    move-wide v4, p2

    const-wide/16 v2, 0x0

    invoke-static/range {v0 .. v5}, Lokio/Util;->checkOffsetAndCount(JJJ)V

    .line 61
    :goto_8
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-lez v0, :cond_45

    .line 63
    iget-object v6, p1, Lokio/Buffer;->head:Lokio/Segment;

    .line 64
    .local v6, "head":Lokio/Segment;
    iget v0, v6, Lokio/Segment;->limit:I

    iget v1, v6, Lokio/Segment;->pos:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v7, v0

    .line 65
    .local v7, "toDeflate":I
    iget-object v0, p0, Lokio/DeflaterSink;->deflater:Ljava/util/zip/Deflater;

    iget-object v1, v6, Lokio/Segment;->data:[B

    iget v2, v6, Lokio/Segment;->pos:I

    invoke-virtual {v0, v1, v2, v7}, Ljava/util/zip/Deflater;->setInput([BII)V

    .line 68
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lokio/DeflaterSink;->deflate(Z)V

    .line 71
    iget-wide v0, p1, Lokio/Buffer;->size:J

    int-to-long v2, v7

    sub-long/2addr v0, v2

    iput-wide v0, p1, Lokio/Buffer;->size:J

    .line 72
    iget v0, v6, Lokio/Segment;->pos:I

    add-int/2addr v0, v7

    iput v0, v6, Lokio/Segment;->pos:I

    .line 73
    iget v0, v6, Lokio/Segment;->pos:I

    iget v1, v6, Lokio/Segment;->limit:I

    if-ne v0, v1, :cond_42

    .line 74
    invoke-virtual {v6}, Lokio/Segment;->pop()Lokio/Segment;

    move-result-object v0

    iput-object v0, p1, Lokio/Buffer;->head:Lokio/Segment;

    .line 75
    invoke-static {v6}, Lokio/SegmentPool;->recycle(Lokio/Segment;)V

    .line 78
    :cond_42
    int-to-long v0, v7

    sub-long/2addr p2, v0

    .line 79
    .end local v6    # "head":Lokio/Segment;
    .end local v7    # "toDeflate":I
    goto :goto_8

    .line 80
    :cond_45
    return-void
.end method

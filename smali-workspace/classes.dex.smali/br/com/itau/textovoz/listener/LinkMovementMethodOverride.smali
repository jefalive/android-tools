.class public Lbr/com/itau/textovoz/listener/LinkMovementMethodOverride;
.super Ljava/lang/Object;
.source "LinkMovementMethodOverride.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 14
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .line 14
    move-object v1, p1

    check-cast v1, Landroid/widget/TextView;

    .line 15
    .local v1, "widget":Landroid/widget/TextView;
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    .line 16
    .local v2, "text":Ljava/lang/Object;
    instance-of v0, v2, Landroid/text/Spanned;

    if-eqz v0, :cond_59

    .line 17
    move-object v3, v2

    check-cast v3, Landroid/text/Spanned;

    .line 19
    .local v3, "buffer":Landroid/text/Spanned;
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    .line 21
    .local v4, "action":I
    const/4 v0, 0x1

    if-eq v4, v0, :cond_17

    if-nez v4, :cond_59

    .line 23
    :cond_17
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v5, v0

    .line 24
    .local v5, "x":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v6, v0

    .line 26
    .local v6, "y":I
    invoke-virtual {v1}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    move-result v0

    sub-int/2addr v5, v0

    .line 27
    invoke-virtual {v1}, Landroid/widget/TextView;->getTotalPaddingTop()I

    move-result v0

    sub-int/2addr v6, v0

    .line 29
    invoke-virtual {v1}, Landroid/widget/TextView;->getScrollX()I

    move-result v0

    add-int/2addr v5, v0

    .line 30
    invoke-virtual {v1}, Landroid/widget/TextView;->getScrollY()I

    move-result v0

    add-int/2addr v6, v0

    .line 32
    invoke-virtual {v1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v7

    .line 33
    .local v7, "layout":Landroid/text/Layout;
    invoke-virtual {v7, v6}, Landroid/text/Layout;->getLineForVertical(I)I

    move-result v8

    .line 34
    .local v8, "line":I
    int-to-float v0, v5

    invoke-virtual {v7, v8, v0}, Landroid/text/Layout;->getOffsetForHorizontal(IF)I

    move-result v9

    .line 36
    .local v9, "off":I
    const-class v0, Landroid/text/style/ClickableSpan;

    invoke-interface {v3, v9, v9, v0}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, [Landroid/text/style/ClickableSpan;

    .line 39
    .local v10, "link":[Landroid/text/style/ClickableSpan;
    array-length v0, v10

    if-eqz v0, :cond_59

    const/4 v0, 0x1

    if-ne v4, v0, :cond_59

    .line 40
    const/4 v0, 0x0

    aget-object v0, v10, v0

    invoke-virtual {v0, v1}, Landroid/text/style/ClickableSpan;->onClick(Landroid/view/View;)V

    .line 41
    const/4 v0, 0x1

    return v0

    .line 46
    .end local v3    # "buffer":Landroid/text/Spanned;
    .end local v4    # "action":I
    .end local v5    # "x":I
    .end local v6    # "y":I
    .end local v7    # "layout":Landroid/text/Layout;
    .end local v8    # "line":I
    .end local v9    # "off":I
    .end local v10    # "link":[Landroid/text/style/ClickableSpan;
    :cond_59
    const/4 v0, 0x0

    return v0
.end method

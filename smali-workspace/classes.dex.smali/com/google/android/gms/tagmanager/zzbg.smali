.class public final Lcom/google/android/gms/tagmanager/zzbg;
.super Ljava/lang/Object;


# static fields
.field static zzbjy:Lcom/google/android/gms/tagmanager/zzbh;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    new-instance v0, Lcom/google/android/gms/tagmanager/zzy;

    invoke-direct {v0}, Lcom/google/android/gms/tagmanager/zzy;-><init>()V

    sput-object v0, Lcom/google/android/gms/tagmanager/zzbg;->zzbjy:Lcom/google/android/gms/tagmanager/zzbh;

    return-void
.end method

.method public static e(Ljava/lang/String;)V
    .registers 2
    .param p0, "message"    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/tagmanager/zzbg;->zzbjy:Lcom/google/android/gms/tagmanager/zzbh;

    invoke-interface {v0, p0}, Lcom/google/android/gms/tagmanager/zzbh;->e(Ljava/lang/String;)V

    return-void
.end method

.method public static v(Ljava/lang/String;)V
    .registers 2
    .param p0, "message"    # Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/tagmanager/zzbg;->zzbjy:Lcom/google/android/gms/tagmanager/zzbh;

    invoke-interface {v0, p0}, Lcom/google/android/gms/tagmanager/zzbh;->v(Ljava/lang/String;)V

    return-void
.end method

.method public static zzaJ(Ljava/lang/String;)V
    .registers 2

    sget-object v0, Lcom/google/android/gms/tagmanager/zzbg;->zzbjy:Lcom/google/android/gms/tagmanager/zzbh;

    invoke-interface {v0, p0}, Lcom/google/android/gms/tagmanager/zzbh;->zzaJ(Ljava/lang/String;)V

    return-void
.end method

.method public static zzaK(Ljava/lang/String;)V
    .registers 2

    sget-object v0, Lcom/google/android/gms/tagmanager/zzbg;->zzbjy:Lcom/google/android/gms/tagmanager/zzbh;

    invoke-interface {v0, p0}, Lcom/google/android/gms/tagmanager/zzbh;->zzaK(Ljava/lang/String;)V

    return-void
.end method

.method public static zzb(Ljava/lang/String;Ljava/lang/Throwable;)V
    .registers 3

    sget-object v0, Lcom/google/android/gms/tagmanager/zzbg;->zzbjy:Lcom/google/android/gms/tagmanager/zzbh;

    invoke-interface {v0, p0, p1}, Lcom/google/android/gms/tagmanager/zzbh;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

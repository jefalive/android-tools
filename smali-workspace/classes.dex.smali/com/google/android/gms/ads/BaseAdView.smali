.class Lcom/google/android/gms/ads/BaseAdView;
.super Landroid/view/ViewGroup;


# instance fields
.field private final zzoJ:Lcom/google/android/gms/ads/internal/client/zzab;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .registers 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "adViewType"    # I

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/gms/ads/internal/client/zzab;

    invoke-static {p2}, Lcom/google/android/gms/ads/BaseAdView;->zze(I)Z

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/ads/internal/client/zzab;-><init>(Landroid/view/ViewGroup;Z)V

    iput-object v0, p0, Lcom/google/android/gms/ads/BaseAdView;->zzoJ:Lcom/google/android/gms/ads/internal/client/zzab;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "adViewType"    # I

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/gms/ads/internal/client/zzab;

    invoke-static {p3}, Lcom/google/android/gms/ads/BaseAdView;->zze(I)Z

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, p2, v2, v1}, Lcom/google/android/gms/ads/internal/client/zzab;-><init>(Landroid/view/ViewGroup;Landroid/util/AttributeSet;ZZ)V

    iput-object v0, p0, Lcom/google/android/gms/ads/BaseAdView;->zzoJ:Lcom/google/android/gms/ads/internal/client/zzab;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .registers 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "adViewType"    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/google/android/gms/ads/internal/client/zzab;

    invoke-static {p4}, Lcom/google/android/gms/ads/BaseAdView;->zze(I)Z

    move-result v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, p2, v2, v1}, Lcom/google/android/gms/ads/internal/client/zzab;-><init>(Landroid/view/ViewGroup;Landroid/util/AttributeSet;ZZ)V

    iput-object v0, p0, Lcom/google/android/gms/ads/BaseAdView;->zzoJ:Lcom/google/android/gms/ads/internal/client/zzab;

    return-void
.end method

.method private static zze(I)Z
    .registers 2

    const/4 v0, 0x2

    if-ne p0, v0, :cond_5

    const/4 v0, 0x1

    goto :goto_6

    :cond_5
    const/4 v0, 0x0

    :goto_6
    return v0
.end method


# virtual methods
.method public destroy()V
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/ads/BaseAdView;->zzoJ:Lcom/google/android/gms/ads/internal/client/zzab;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/client/zzab;->destroy()V

    return-void
.end method

.method public getAdSize()Lcom/google/android/gms/ads/AdSize;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/ads/BaseAdView;->zzoJ:Lcom/google/android/gms/ads/internal/client/zzab;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/client/zzab;->getAdSize()Lcom/google/android/gms/ads/AdSize;

    move-result-object v0

    return-object v0
.end method

.method public loadAd(Lcom/google/android/gms/ads/AdRequest;)V
    .registers 4
    .param p1, "adRequest"    # Lcom/google/android/gms/ads/AdRequest;

    iget-object v0, p0, Lcom/google/android/gms/ads/BaseAdView;->zzoJ:Lcom/google/android/gms/ads/internal/client/zzab;

    invoke-virtual {p1}, Lcom/google/android/gms/ads/AdRequest;->zzaE()Lcom/google/android/gms/ads/internal/client/zzaa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/client/zzab;->zza(Lcom/google/android/gms/ads/internal/client/zzaa;)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .registers 13
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/BaseAdView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_28

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_28

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    sub-int v0, p4, p2

    sub-int/2addr v0, v3

    div-int/lit8 v5, v0, 0x2

    sub-int v0, p5, p3

    sub-int/2addr v0, v4

    div-int/lit8 v6, v0, 0x2

    add-int v0, v5, v3

    add-int v1, v6, v4

    invoke-virtual {v2, v5, v6, v0, v1}, Landroid/view/View;->layout(IIII)V

    :cond_28
    return-void
.end method

.method protected onMeasure(II)V
    .registers 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/BaseAdView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_1d

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1d

    invoke-virtual {p0, v4, p1, p2}, Lcom/google/android/gms/ads/BaseAdView;->measureChild(Landroid/view/View;II)V

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    goto :goto_2f

    :cond_1d
    invoke-virtual {p0}, Lcom/google/android/gms/ads/BaseAdView;->getAdSize()Lcom/google/android/gms/ads/AdSize;

    move-result-object v5

    if-eqz v5, :cond_2f

    invoke-virtual {p0}, Lcom/google/android/gms/ads/BaseAdView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/ads/AdSize;->getWidthInPixels(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v5, v6}, Lcom/google/android/gms/ads/AdSize;->getHeightInPixels(Landroid/content/Context;)I

    move-result v3

    :cond_2f
    :goto_2f
    invoke-virtual {p0}, Lcom/google/android/gms/ads/BaseAdView;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/gms/ads/BaseAdView;->getSuggestedMinimumHeight()I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v2, p1}, Landroid/view/View;->resolveSize(II)I

    move-result v0

    invoke-static {v3, p2}, Landroid/view/View;->resolveSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/ads/BaseAdView;->setMeasuredDimension(II)V

    return-void
.end method

.method public pause()V
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/ads/BaseAdView;->zzoJ:Lcom/google/android/gms/ads/internal/client/zzab;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/client/zzab;->pause()V

    return-void
.end method

.method public resume()V
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/ads/BaseAdView;->zzoJ:Lcom/google/android/gms/ads/internal/client/zzab;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/client/zzab;->resume()V

    return-void
.end method

.method public setAdListener(Lcom/google/android/gms/ads/AdListener;)V
    .registers 4
    .param p1, "adListener"    # Lcom/google/android/gms/ads/AdListener;

    iget-object v0, p0, Lcom/google/android/gms/ads/BaseAdView;->zzoJ:Lcom/google/android/gms/ads/internal/client/zzab;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/ads/internal/client/zzab;->setAdListener(Lcom/google/android/gms/ads/AdListener;)V

    if-eqz p1, :cond_14

    instance-of v0, p1, Lcom/google/android/gms/ads/internal/client/zza;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/gms/ads/BaseAdView;->zzoJ:Lcom/google/android/gms/ads/internal/client/zzab;

    move-object v1, p1

    check-cast v1, Lcom/google/android/gms/ads/internal/client/zza;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/client/zzab;->zza(Lcom/google/android/gms/ads/internal/client/zza;)V

    goto :goto_1c

    :cond_14
    if-nez p1, :cond_1c

    iget-object v0, p0, Lcom/google/android/gms/ads/BaseAdView;->zzoJ:Lcom/google/android/gms/ads/internal/client/zzab;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/client/zzab;->zza(Lcom/google/android/gms/ads/internal/client/zza;)V

    :cond_1c
    :goto_1c
    return-void
.end method

.method public setAdSize(Lcom/google/android/gms/ads/AdSize;)V
    .registers 5
    .param p1, "adSize"    # Lcom/google/android/gms/ads/AdSize;

    iget-object v0, p0, Lcom/google/android/gms/ads/BaseAdView;->zzoJ:Lcom/google/android/gms/ads/internal/client/zzab;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/ads/AdSize;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/client/zzab;->setAdSizes([Lcom/google/android/gms/ads/AdSize;)V

    return-void
.end method

.method public setAdUnitId(Ljava/lang/String;)V
    .registers 3
    .param p1, "adUnitId"    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/ads/BaseAdView;->zzoJ:Lcom/google/android/gms/ads/internal/client/zzab;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/ads/internal/client/zzab;->setAdUnitId(Ljava/lang/String;)V

    return-void
.end method

.method public setInAppPurchaseListener(Lcom/google/android/gms/ads/purchase/InAppPurchaseListener;)V
    .registers 3
    .param p1, "inAppPurchaseListener"    # Lcom/google/android/gms/ads/purchase/InAppPurchaseListener;

    iget-object v0, p0, Lcom/google/android/gms/ads/BaseAdView;->zzoJ:Lcom/google/android/gms/ads/internal/client/zzab;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/ads/internal/client/zzab;->setInAppPurchaseListener(Lcom/google/android/gms/ads/purchase/InAppPurchaseListener;)V

    return-void
.end method

.method public setPlayStorePurchaseParams(Lcom/google/android/gms/ads/purchase/PlayStorePurchaseListener;Ljava/lang/String;)V
    .registers 4
    .param p1, "playStorePurchaseListener"    # Lcom/google/android/gms/ads/purchase/PlayStorePurchaseListener;
    .param p2, "publicKey"    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/ads/BaseAdView;->zzoJ:Lcom/google/android/gms/ads/internal/client/zzab;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/ads/internal/client/zzab;->setPlayStorePurchaseParams(Lcom/google/android/gms/ads/purchase/PlayStorePurchaseListener;Ljava/lang/String;)V

    return-void
.end method

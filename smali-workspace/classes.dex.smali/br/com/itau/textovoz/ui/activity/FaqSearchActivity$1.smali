.class Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$1;
.super Ljava/lang/Object;
.source "FaqSearchActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;


# direct methods
.method constructor <init>(Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;)V
    .registers 2
    .param p1, "this$0"    # Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;

    .line 98
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 6
    .param p1, "view"    # Landroid/view/View;

    .line 101
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    .line 102
    .local v3, "i":I
    sget v0, Lbr/com/itau/textovoz/R$id;->btn_feed_nao_library:I

    if-ne v3, v0, :cond_16

    .line 103
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;

    sget v2, Lbr/com/itau/textovoz/R$string;->feed_not:I

    invoke-virtual {v1, v2}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->putGoogleAnalytic(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->access$000(Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;Ljava/lang/String;)V

    goto :goto_3b

    .line 104
    :cond_16
    sget v0, Lbr/com/itau/textovoz/R$id;->btn_feed_sim_library:I

    if-ne v3, v0, :cond_28

    .line 105
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;

    sget v2, Lbr/com/itau/textovoz/R$string;->feed_yes:I

    invoke-virtual {v1, v2}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    # invokes: Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->putGoogleAnalytic(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->access$000(Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;Ljava/lang/String;)V

    goto :goto_3b

    .line 106
    :cond_28
    sget v0, Lbr/com/itau/textovoz/R$id;->img_close_yes_library:I

    if-eq v3, v0, :cond_30

    sget v0, Lbr/com/itau/textovoz/R$id;->relative_answer_yes_library:I

    if-ne v3, v0, :cond_3b

    .line 107
    :cond_30
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;

    # getter for: Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->relativeAnswerYes:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;->access$100(Lbr/com/itau/textovoz/ui/activity/FaqSearchActivity;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 109
    :cond_3b
    :goto_3b
    return-void
.end method

.class public Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;
.super Lcom/daimajia/swipe/adapters/BaseSwipeAdapter;
.source "ComprovantesAdapter.java"

# interfaces
.implements Landroid/widget/Filterable;


# instance fields
.field private chooseDetailClickListner:Landroid/view/View$OnClickListener;

.field private compartilharClickListener:Landroid/view/View$OnClickListener;

.field private enviarClickListener:Landroid/view/View$OnClickListener;

.field private listaComprovantes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;>;"
        }
    .end annotation
.end field

.field private listaFiltrada:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;>;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private final qtdDias:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<Ljava/lang/Object;>;"
        }
    .end annotation
.end field

.field private salvarClickListener:Landroid/view/View$OnClickListener;

.field private swipeLayout:Lcom/daimajia/swipe/SwipeLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .registers 4
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "listaComprovantes"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;Ljava/util/List<Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;>;)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Lcom/daimajia/swipe/adapters/BaseSwipeAdapter;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->mContext:Landroid/content/Context;

    .line 44
    iput-object p2, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->listaComprovantes:Ljava/util/List;

    .line 45
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->listaComprovantes:Ljava/util/List;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->listaFiltrada:Ljava/util/List;

    .line 46
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->qtdDias:Landroid/util/SparseArray;

    .line 47
    invoke-virtual {p0}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->ordenar()V

    .line 48
    return-void
.end method

.method private controlaVisibilidadeBotaoEnviar(Landroid/widget/ImageButton;Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;)V
    .registers 4
    .param p1, "botaoEnviar"    # Landroid/widget/ImageButton;
    .param p2, "itemVO"    # Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    .line 224
    invoke-virtual {p2}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getIndicadorEmail()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 225
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_14

    .line 227
    :cond_f
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 229
    :goto_14
    return-void
.end method

.method private static defineTituloDias(J)Ljava/lang/String;
    .registers 5
    .param p0, "daysDiff"    # J

    .line 256
    const-string v2, "\u00faltimos 7 dias"

    .line 258
    .local v2, "titulo":Ljava/lang/String;
    const-wide/16 v0, 0x7

    cmp-long v0, p0, v0

    if-lez v0, :cond_11

    const-wide/16 v0, 0xf

    cmp-long v0, p0, v0

    if-gtz v0, :cond_11

    .line 259
    const-string v2, "\u00faltimos 15 dias"

    goto :goto_3d

    .line 260
    :cond_11
    const-wide/16 v0, 0xf

    cmp-long v0, p0, v0

    if-lez v0, :cond_20

    const-wide/16 v0, 0x1e

    cmp-long v0, p0, v0

    if-gtz v0, :cond_20

    .line 261
    const-string v2, "\u00faltimos 30 dias"

    goto :goto_3d

    .line 262
    :cond_20
    const-wide/16 v0, 0x1e

    cmp-long v0, p0, v0

    if-lez v0, :cond_2f

    const-wide/16 v0, 0x3c

    cmp-long v0, p0, v0

    if-gtz v0, :cond_2f

    .line 263
    const-string v2, "\u00faltimos 60 dias"

    goto :goto_3d

    .line 264
    :cond_2f
    const-wide/16 v0, 0x3c

    cmp-long v0, p0, v0

    if-lez v0, :cond_3d

    const-wide/16 v0, 0x5a

    cmp-long v0, p0, v0

    if-gtz v0, :cond_3d

    .line 265
    const-string v2, "\u00faltimos 90 dias"

    .line 268
    :cond_3d
    :goto_3d
    return-object v2
.end method

.method private exibeLinhasComprovantes(Landroid/widget/LinearLayout;)V
    .registers 3
    .param p1, "llLinhasComprovantes"    # Landroid/widget/LinearLayout;

    .line 220
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 221
    return-void
.end method

.method private limpaQuantidadeDias()V
    .registers 2

    .line 240
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->qtdDias:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 241
    return-void
.end method

.method private ordenaListaComprovantePelaDataComprovante()V
    .registers 3

    .line 245
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->listaComprovantes:Ljava/util/List;

    new-instance v1, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter$1;-><init>(Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 251
    return-void
.end method

.method private setaAcessibilidadeHeader(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .param p1, "data"    # Ljava/lang/String;

    .line 232
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 233
    const v2, 0x7f070346

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 232
    return-object v0
.end method

.method private setaQtdDiasHeader(ILandroid/widget/TextView;Landroid/widget/RelativeLayout;Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;)V
    .registers 8
    .param p1, "posicao"    # I
    .param p2, "textoQuantidadeDiasHeader"    # Landroid/widget/TextView;
    .param p3, "headerLista"    # Landroid/widget/RelativeLayout;
    .param p4, "itemVO"    # Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    .line 198
    if-nez p1, :cond_19

    .line 200
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 201
    invoke-virtual {p4}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getDataComprovante()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 202
    .local v2, "data":Ljava/lang/String;
    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    invoke-direct {p0, v2}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->setaAcessibilidadeHeader(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 205
    .end local v2    # "data":Ljava/lang/String;
    goto :goto_5d

    :cond_19
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->listaComprovantes:Ljava/util/List;

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getDataComprovante()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->listaComprovantes:Ljava/util/List;

    .line 206
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getDataComprovante()Ljava/lang/String;

    move-result-object v1

    .line 205
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 208
    const/16 v0, 0x8

    invoke-virtual {p3, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_5d

    .line 211
    :cond_3f
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 212
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->listaComprovantes:Ljava/util/List;

    .line 213
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getDataComprovante()Ljava/lang/String;

    move-result-object v0

    .line 212
    invoke-static {v0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 214
    .local v2, "data":Ljava/lang/String;
    invoke-virtual {p2, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    invoke-direct {p0, v2}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->setaAcessibilidadeHeader(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 217
    .end local v2    # "data":Ljava/lang/String;
    :goto_5d
    return-void
.end method

.method private setaTextoLiteralIdComprovante(Landroid/widget/TextView;Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;)V
    .registers 4
    .param p1, "textoLiteralIdentificacaoComprovante"    # Landroid/widget/TextView;
    .param p2, "itemVO"    # Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    .line 189
    invoke-virtual {p2}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getDataComprovante()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 190
    .line 191
    invoke-virtual {p2}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getLiteralIdentificacaoComprovante()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    :cond_d
    return-void
.end method

.method private setaTextoValor(Landroid/widget/TextView;Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;)V
    .registers 7
    .param p1, "textoValor"    # Landroid/widget/TextView;
    .param p2, "itemVO"    # Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    .line 176
    invoke-virtual {p2}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getValor()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_28

    invoke-virtual {p2}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getValor()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_28

    .line 178
    .line 179
    invoke-virtual {p2}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getValor()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 180
    .line 181
    .local v2, "valorFormatado":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getValor()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 182
    .local v3, "valorFormatadoAcessibilidade":Ljava/lang/String;
    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 185
    .end local v2    # "valorFormatado":Ljava/lang/String;
    .end local v3    # "valorFormatadoAcessibilidade":Ljava/lang/String;
    :cond_28
    return-void
.end method


# virtual methods
.method public fillValues(ILandroid/view/View;)V
    .registers 16
    .param p1, "i"    # I
    .param p2, "view"    # Landroid/view/View;

    .line 80
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->getSwipeLayoutResourceId(I)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/daimajia/swipe/SwipeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->swipeLayout:Lcom/daimajia/swipe/SwipeLayout;

    .line 82
    const v0, 0x7f0e0116

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/ImageButton;

    .line 83
    .line 84
    .local v5, "botaoEnviar":Landroid/widget/ImageButton;
    const v0, 0x7f0e052e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/LinearLayout;

    .line 85
    .line 86
    .local v6, "llLinhasComprovantes":Landroid/widget/LinearLayout;
    const v0, 0x7f0e0529

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/widget/RelativeLayout;

    .line 87
    .local v7, "headerLista":Landroid/widget/RelativeLayout;
    const v0, 0x7f0e02d5

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/widget/TextView;

    .line 88
    .local v8, "textoValor":Landroid/widget/TextView;
    const v0, 0x7f0e052c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/widget/TextView;

    .line 89
    .local v9, "textoNomeOperacao":Landroid/widget/TextView;
    const v0, 0x7f0e0345

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/widget/TextView;

    .line 91
    .local v10, "textoQuantidadeDiasHeader":Landroid/widget/TextView;
    const v0, 0x7f0e052d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Landroid/widget/TextView;

    .line 94
    .local v11, "textoLiteralIdentificacaoComprovante":Landroid/widget/TextView;
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->listaComprovantes:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    .line 96
    .local v12, "itemVO":Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;
    invoke-direct {p0, p1, v10, v7, v12}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->setaQtdDiasHeader(ILandroid/widget/TextView;Landroid/widget/RelativeLayout;Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;)V

    .line 97
    invoke-virtual {v12}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getNomeOperacao()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    invoke-direct {p0, v11, v12}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->setaTextoLiteralIdComprovante(Landroid/widget/TextView;Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;)V

    .line 99
    invoke-direct {p0, v8, v12}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->setaTextoValor(Landroid/widget/TextView;Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;)V

    .line 100
    invoke-direct {p0, v6}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->exibeLinhasComprovantes(Landroid/widget/LinearLayout;)V

    .line 101
    invoke-direct {p0, v5, v12}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->controlaVisibilidadeBotaoEnviar(Landroid/widget/ImageButton;Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;)V

    .line 103
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->swipeLayout:Lcom/daimajia/swipe/SwipeLayout;

    invoke-virtual {v0}, Lcom/daimajia/swipe/SwipeLayout;->getSurfaceView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$SurfaceViewClickListener;

    iget-object v2, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->chooseDetailClickListner:Landroid/view/View$OnClickListener;

    invoke-direct {v1, v2, v12, v3}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$SurfaceViewClickListener;-><init>(Landroid/content/Context;Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;Landroid/view/View$OnClickListener;)V

    .line 104
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    const v0, 0x7f0e02ae

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoSalvarClickListener;

    iget-object v2, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->salvarClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {v1, v2}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoSalvarClickListener;-><init>(Landroid/view/View$OnClickListener;)V

    .line 109
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    const v0, 0x7f0e0116

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoEnviarClickListener;

    iget-object v2, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->enviarClickListener:Landroid/view/View$OnClickListener;

    iget-object v4, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->swipeLayout:Lcom/daimajia/swipe/SwipeLayout;

    invoke-direct {v1, v12, v2, v3, v4}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoEnviarClickListener;-><init>(Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;Landroid/content/Context;Landroid/view/View$OnClickListener;Lcom/daimajia/swipe/SwipeLayout;)V

    .line 113
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    const v0, 0x7f0e01f0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoCompartilharClickListener;

    iget-object v2, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->compartilharClickListener:Landroid/view/View$OnClickListener;

    iget-object v4, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->swipeLayout:Lcom/daimajia/swipe/SwipeLayout;

    invoke-direct {v1, v12, v2, v3, v4}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterListener$BotaoCompartilharClickListener;-><init>(Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;Landroid/content/Context;Landroid/view/View$OnClickListener;Lcom/daimajia/swipe/SwipeLayout;)V

    .line 118
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    return-void
.end method

.method public generateView(ILandroid/view/ViewGroup;)Landroid/view/View;
    .registers 6
    .param p1, "i"    # I
    .param p2, "viewGroup"    # Landroid/view/ViewGroup;

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 74
    const v1, 0x7f0300f5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 73
    return-object v0
.end method

.method public getCount()I
    .registers 2

    .line 125
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->listaComprovantes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .registers 5

    .line 141
    new-instance v0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterFilter;

    iget-object v1, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->listaFiltrada:Ljava/util/List;

    iget-object v2, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->listaComprovantes:Ljava/util/List;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapterFilter;-><init>(Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;Ljava/util/List;Landroid/content/Context;Ljava/util/List;)V

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 3
    .param p1, "position"    # I

    .line 130
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->listaComprovantes:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .param p1, "position"    # I

    .line 135
    int-to-long v0, p1

    return-wide v0
.end method

.method public getSwipeLayoutResourceId(I)I
    .registers 3
    .param p1, "i"    # I

    .line 68
    const v0, 0x7f0e052a

    return v0
.end method

.method protected ordenar()V
    .registers 12

    .line 146
    invoke-direct {p0}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->limpaQuantidadeDias()V

    .line 148
    invoke-direct {p0}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->ordenaListaComprovantePelaDataComprovante()V

    .line 150
    const/4 v2, 0x0

    .line 152
    .local v2, "i":I
    invoke-static {}, Lorg/threeten/bp/LocalDate;->now()Lorg/threeten/bp/LocalDate;

    move-result-object v3

    .line 154
    .local v3, "dataHoje":Lorg/threeten/bp/LocalDate;
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->listaComprovantes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_11
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_51

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;

    .line 156
    .local v5, "comprovante":Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;
    invoke-virtual {v5}, Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;->getDataComprovante()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->parse(Ljava/lang/String;)Lorg/threeten/bp/LocalDate;

    move-result-object v6

    .line 157
    .local v6, "dataLancamento":Lorg/threeten/bp/LocalDate;
    invoke-static {v6, v3}, Lorg/threeten/bp/Period;->between(Lorg/threeten/bp/LocalDate;Lorg/threeten/bp/LocalDate;)Lorg/threeten/bp/Period;

    move-result-object v7

    .line 158
    .local v7, "difDias":Lorg/threeten/bp/Period;
    invoke-virtual {v7}, Lorg/threeten/bp/Period;->getDays()I

    move-result v0

    int-to-long v8, v0

    .line 160
    .local v8, "daysDiff":J
    const-string v10, ""

    .line 161
    .local v10, "titulo":Ljava/lang/String;
    const-wide/16 v0, 0x0

    cmp-long v0, v8, v0

    if-ltz v0, :cond_41

    const-wide/16 v0, 0x5a

    cmp-long v0, v8, v0

    if-gtz v0, :cond_41

    .line 162
    invoke-static {v8, v9}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->defineTituloDias(J)Ljava/lang/String;

    move-result-object v10

    .line 165
    :cond_41
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->qtdDias:Landroid/util/SparseArray;

    invoke-virtual {v0, v10}, Landroid/util/SparseArray;->indexOfValue(Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_4e

    .line 166
    iget-object v0, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->qtdDias:Landroid/util/SparseArray;

    invoke-virtual {v0, v2, v10}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 169
    :cond_4e
    add-int/lit8 v2, v2, 0x1

    .line 170
    .end local v5    # "comprovante":Lcom/itau/empresas/feature/comprovantes/model/ComprovantesVO;
    .end local v6    # "dataLancamento":Lorg/threeten/bp/LocalDate;
    .end local v7    # "difDias":Lorg/threeten/bp/Period;
    .end local v8    # "daysDiff":J
    .end local v10    # "titulo":Ljava/lang/String;
    goto :goto_11

    .line 172
    :cond_51
    invoke-virtual {p0}, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->notifyDataSetChanged()V

    .line 173
    return-void
.end method

.method public setCompartilharClickListener(Landroid/view/View$OnClickListener;)V
    .registers 2
    .param p1, "compartilharClickListener"    # Landroid/view/View$OnClickListener;

    .line 59
    iput-object p1, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->compartilharClickListener:Landroid/view/View$OnClickListener;

    .line 60
    return-void
.end method

.method public setEnviarClickListener(Landroid/view/View$OnClickListener;)V
    .registers 2
    .param p1, "enviarClickListener"    # Landroid/view/View$OnClickListener;

    .line 51
    iput-object p1, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->enviarClickListener:Landroid/view/View$OnClickListener;

    .line 52
    return-void
.end method

.method public setSalvarClickListener(Landroid/view/View$OnClickListener;)V
    .registers 2
    .param p1, "salvarClickListener"    # Landroid/view/View$OnClickListener;

    .line 55
    iput-object p1, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->salvarClickListener:Landroid/view/View$OnClickListener;

    .line 56
    return-void
.end method

.method public setchooseDetailClickListner(Landroid/view/View$OnClickListener;)V
    .registers 2
    .param p1, "chooseDetailClickListner"    # Landroid/view/View$OnClickListener;

    .line 63
    iput-object p1, p0, Lcom/itau/empresas/feature/comprovantes/ComprovantesAdapter;->chooseDetailClickListner:Landroid/view/View$OnClickListener;

    .line 64
    return-void
.end method

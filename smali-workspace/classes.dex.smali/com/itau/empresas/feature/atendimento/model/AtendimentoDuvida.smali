.class public Lcom/itau/empresas/feature/atendimento/model/AtendimentoDuvida;
.super Ljava/lang/Object;
.source "AtendimentoDuvida.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private assunto:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "assunto"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field private produto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "produto"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAssunto()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoDuvida;->assunto:Ljava/util/List;

    return-object v0
.end method

.method public getProduto()Ljava/lang/String;
    .registers 2

    .line 25
    iget-object v0, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoDuvida;->produto:Ljava/lang/String;

    return-object v0
.end method

.method public setAssunto(Ljava/util/List;)V
    .registers 2
    .param p1, "assunto"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Ljava/lang/String;>;)V"
        }
    .end annotation

    .line 21
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoDuvida;->assunto:Ljava/util/List;

    .line 22
    return-void
.end method

.method public setProduto(Ljava/lang/String;)V
    .registers 2
    .param p1, "produto"    # Ljava/lang/String;

    .line 17
    iput-object p1, p0, Lcom/itau/empresas/feature/atendimento/model/AtendimentoDuvida;->produto:Ljava/lang/String;

    .line 18
    return-void
.end method

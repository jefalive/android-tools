.class public final Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;
.super Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;
.source "LisIntroducaoActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 37
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;-><init>()V

    .line 41
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;

    .line 37
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;

    .line 37
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 54
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 55
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 56
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 57
    .local v1, "resources_":Landroid/content/res/Resources;
    const v0, 0x7f0703fd

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->condicoesGeraisPDF:Ljava/lang/String;

    .line 58
    const v0, 0x7f0703fc

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->condicoesGeraisAdicionalPDF:Ljava/lang/String;

    .line 59
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 60
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/lis/LisController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/LisController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->controller:Lcom/itau/empresas/feature/credito/lis/LisController;

    .line 61
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->injectExtras_()V

    .line 63
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->afterInject()V

    .line 64
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 141
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 142
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_1c

    .line 143
    const-string v0, "ofertaContratacaoLisVo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 144
    const-string v0, "ofertaContratacaoLisVo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    .line 147
    :cond_1c
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 172
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_$5;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_$5;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 180
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 160
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_$4;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 168
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 47
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->init_(Landroid/os/Bundle;)V

    .line 48
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 50
    const v0, 0x7f03004b

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->setContentView(I)V

    .line 51
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 6
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 98
    const v0, 0x7f0e020f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->scroView:Landroid/view/View;

    .line 99
    const v0, 0x7f0e022e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->textPreAprovado:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0e0235

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->cvLisAdicional:Landroid/view/View;

    .line 101
    const v0, 0x7f0e0230

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->llLisChequeEspecial:Landroid/view/View;

    .line 102
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 103
    const v0, 0x7f0e023b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 104
    .local v1, "view_textview_lis_introducao_pdf":Landroid/view/View;
    const v0, 0x7f0e023c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 105
    .local v2, "view_text_lis_condicoes_gerais_pdf_adicional":Landroid/view/View;
    const v0, 0x7f0e023d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 107
    .local v3, "view_botao_conhecer":Landroid/view/View;
    if-eqz v1, :cond_50

    .line 108
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_$1;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    :cond_50
    if-eqz v2, :cond_5a

    .line 118
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_$2;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_$2;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    :cond_5a
    if-eqz v3, :cond_64

    .line 128
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_$3;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_$3;-><init>(Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    :cond_64
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->aoCarregarTela()V

    .line 138
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 68
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->setContentView(I)V

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 70
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 80
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->setContentView(Landroid/view/View;)V

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 82
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 74
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 76
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 151
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity;->setIntent(Landroid/content/Intent;)V

    .line 152
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->injectExtras_()V

    .line 153
    return-void
.end method

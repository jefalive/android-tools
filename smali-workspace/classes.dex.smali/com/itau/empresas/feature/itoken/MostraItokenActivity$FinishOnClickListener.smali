.class Lcom/itau/empresas/feature/itoken/MostraItokenActivity$FinishOnClickListener;
.super Ljava/lang/Object;
.source "MostraItokenActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/itoken/MostraItokenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FinishOnClickListener"
.end annotation


# instance fields
.field private final activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Landroid/app/Activity;)V
    .registers 2
    .param p1, "activity"    # Landroid/app/Activity;

    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    iput-object p1, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity$FinishOnClickListener;->activity:Landroid/app/Activity;

    .line 181
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 3
    .param p1, "v"    # Landroid/view/View;

    .line 184
    iget-object v0, p0, Lcom/itau/empresas/feature/itoken/MostraItokenActivity$FinishOnClickListener;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 185
    return-void
.end method

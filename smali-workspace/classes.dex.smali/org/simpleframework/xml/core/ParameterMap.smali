.class Lorg/simpleframework/xml/core/ParameterMap;
.super Ljava/util/LinkedHashMap;
.source "ParameterMap.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap<Ljava/lang/Object;Lorg/simpleframework/xml/core/Parameter;>;Ljava/lang/Iterable<Lorg/simpleframework/xml/core/Parameter;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 46
    invoke-direct {p0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 47
    return-void
.end method


# virtual methods
.method public get(I)Lorg/simpleframework/xml/core/Parameter;
    .registers 3
    .param p1, "ordinal"    # I

    .line 70
    invoke-virtual {p0}, Lorg/simpleframework/xml/core/ParameterMap;->getAll()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/simpleframework/xml/core/Parameter;

    return-object v0
.end method

.method public getAll()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lorg/simpleframework/xml/core/Parameter;>;"
        }
    .end annotation

    .line 82
    invoke-virtual {p0}, Lorg/simpleframework/xml/core/ParameterMap;->values()Ljava/util/Collection;

    move-result-object v1

    .line 84
    .local v1, "list":Ljava/util/Collection;, "Ljava/util/Collection<Lorg/simpleframework/xml/core/Parameter;>;"
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0

    .line 87
    :cond_10
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/Iterator<Lorg/simpleframework/xml/core/Parameter;>;"
        }
    .end annotation

    .line 57
    invoke-virtual {p0}, Lorg/simpleframework/xml/core/ParameterMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

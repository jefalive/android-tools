.class public Lcom/itau/empresas/api/model/DarfInclusaoResponseVO;
.super Ljava/lang/Object;
.source "DarfInclusaoResponseVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private codigoRetorno:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_retorno"
    .end annotation
.end field

.field private dataEstouro:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_estouro_LDP"
    .end annotation
.end field

.field private dataPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_pagamento"
    .end annotation
.end field

.field private duplicado:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "duplicado"
    .end annotation
.end field

.field private identificadorPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "identificador_pagamento"
    .end annotation
.end field

.field private indicadorEstouroLdp:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_estouro_LDP"
    .end annotation
.end field

.field private modalidadePagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "modalidade_pagamento"
    .end annotation
.end field

.field private numeroLote:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "numero_lote"
    .end annotation
.end field

.field private tipoPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_pagamento"
    .end annotation
.end field

.field private valorTotal:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_total"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

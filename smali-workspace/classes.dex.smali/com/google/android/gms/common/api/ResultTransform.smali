.class public abstract Lcom/google/android/gms/common/api/ResultTransform;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R::Lcom/google/android/gms/common/api/Result;S::Lcom/google/android/gms/common/api/Result;>Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Status;
    .registers 2
    .param p1, "status"    # Lcom/google/android/gms/common/api/Status;

    return-object p1
.end method

.method public abstract onSuccess(Lcom/google/android/gms/common/api/Result;)Lcom/google/android/gms/common/api/PendingResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)Lcom/google/android/gms/common/api/PendingResult<TS;>;"
        }
    .end annotation
.end method

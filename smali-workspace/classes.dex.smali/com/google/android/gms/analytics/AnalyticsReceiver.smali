.class public final Lcom/google/android/gms/analytics/AnalyticsReceiver;
.super Landroid/content/BroadcastReceiver;


# static fields
.field static zzOM:Lcom/google/android/gms/internal/zzrp;

.field static zzON:Ljava/lang/Boolean;

.field static zzqy:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/analytics/AnalyticsReceiver;->zzqy:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static zzY(Landroid/content/Context;)Z
    .registers 4

    invoke-static {p0}, Lcom/google/android/gms/common/internal/zzx;->zzz(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/analytics/AnalyticsReceiver;->zzON:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/google/android/gms/analytics/AnalyticsReceiver;->zzON:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_e
    const-class v0, Lcom/google/android/gms/analytics/AnalyticsReceiver;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/analytics/internal/zzam;->zza(Landroid/content/Context;Ljava/lang/Class;Z)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/AnalyticsReceiver;->zzON:Ljava/lang/Boolean;

    return v2
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    invoke-static {p1}, Lcom/google/android/gms/analytics/internal/zzf;->zzaa(Landroid/content/Context;)Lcom/google/android/gms/analytics/internal/zzf;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/analytics/internal/zzf;->zzjm()Lcom/google/android/gms/analytics/internal/zzaf;

    move-result-object v4

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/google/android/gms/analytics/internal/zzf;->zzjn()Lcom/google/android/gms/analytics/internal/zzr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/internal/zzr;->zzkr()Z

    move-result v0

    if-eqz v0, :cond_1c

    const-string v0, "Device AnalyticsReceiver got"

    invoke-virtual {v4, v0, v5}, Lcom/google/android/gms/analytics/internal/zzaf;->zza(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_21

    :cond_1c
    const-string v0, "Local AnalyticsReceiver got"

    invoke-virtual {v4, v0, v5}, Lcom/google/android/gms/analytics/internal/zzaf;->zza(Ljava/lang/String;Ljava/lang/Object;)V

    :goto_21
    const-string v0, "com.google.android.gms.analytics.ANALYTICS_DISPATCH"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6a

    invoke-static {p1}, Lcom/google/android/gms/analytics/AnalyticsService;->zzZ(Landroid/content/Context;)Z

    move-result v6

    new-instance v7, Landroid/content/Intent;

    const-class v0, Lcom/google/android/gms/analytics/AnalyticsService;

    invoke-direct {v7, p1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "com.google.android.gms.analytics.ANALYTICS_DISPATCH"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    sget-object v8, Lcom/google/android/gms/analytics/AnalyticsReceiver;->zzqy:Ljava/lang/Object;

    monitor-enter v8

    :try_start_3c
    invoke-virtual {p1, v7}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_3f
    .catchall {:try_start_3c .. :try_end_3f} :catchall_67

    if-nez v6, :cond_43

    monitor-exit v8

    return-void

    :cond_43
    :try_start_43
    sget-object v0, Lcom/google/android/gms/analytics/AnalyticsReceiver;->zzOM:Lcom/google/android/gms/internal/zzrp;

    if-nez v0, :cond_57

    new-instance v0, Lcom/google/android/gms/internal/zzrp;

    const-string v1, "Analytics WakeLock"

    const/4 v2, 0x1

    invoke-direct {v0, p1, v2, v1}, Lcom/google/android/gms/internal/zzrp;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/analytics/AnalyticsReceiver;->zzOM:Lcom/google/android/gms/internal/zzrp;

    sget-object v0, Lcom/google/android/gms/analytics/AnalyticsReceiver;->zzOM:Lcom/google/android/gms/internal/zzrp;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzrp;->setReferenceCounted(Z)V

    :cond_57
    sget-object v0, Lcom/google/android/gms/analytics/AnalyticsReceiver;->zzOM:Lcom/google/android/gms/internal/zzrp;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/zzrp;->acquire(J)V
    :try_end_5e
    .catch Ljava/lang/SecurityException; {:try_start_43 .. :try_end_5e} :catch_5f
    .catchall {:try_start_43 .. :try_end_5e} :catchall_67

    goto :goto_65

    :catch_5f
    move-exception v9

    const-string v0, "Analytics service at risk of not starting. For more reliable analytics, add the WAKE_LOCK permission to your manifest. See http://goo.gl/8Rd3yj for instructions."

    :try_start_62
    invoke-virtual {v4, v0}, Lcom/google/android/gms/analytics/internal/zzaf;->zzbg(Ljava/lang/String;)V
    :try_end_65
    .catchall {:try_start_62 .. :try_end_65} :catchall_67

    :goto_65
    monitor-exit v8

    goto :goto_6a

    :catchall_67
    move-exception v10

    monitor-exit v8

    throw v10

    :cond_6a
    :goto_6a
    return-void
.end method

.class final Lcom/google/zxing/client/android/a/a;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/hardware/Camera$AutoFocusCallback;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/util/Collection;


# instance fields
.field private c:Z

.field private d:Z

.field private final e:Z

.field private final f:Landroid/hardware/Camera;

.field private g:Landroid/os/AsyncTask;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    const-class v0, Lcom/google/zxing/client/android/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/zxing/client/android/a/a;->a:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/google/zxing/client/android/a/a;->b:Ljava/util/Collection;

    sget-object v0, Lcom/google/zxing/client/android/a/a;->b:Ljava/util/Collection;

    const-string v1, "auto"

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/zxing/client/android/a/a;->b:Ljava/util/Collection;

    const-string v1, "macro"

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/hardware/Camera;)V
    .registers 8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/zxing/client/android/a/a;->f:Landroid/hardware/Camera;

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-virtual {p2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getFocusMode()Ljava/lang/String;

    move-result-object v4

    const-string v0, "zxing_preferences_auto_focus"

    const/4 v1, 0x1

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_24

    sget-object v0, Lcom/google/zxing/client/android/a/a;->b:Ljava/util/Collection;

    invoke-interface {v0, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_24

    const/4 v0, 0x1

    goto :goto_25

    :cond_24
    const/4 v0, 0x0

    :goto_25
    iput-boolean v0, p0, Lcom/google/zxing/client/android/a/a;->e:Z

    sget-object v0, Lcom/google/zxing/client/android/a/a;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Current focus mode \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'; use auto focus? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/zxing/client/android/a/a;->e:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/google/zxing/client/android/a/a;->a()V

    return-void
.end method

.method private declared-synchronized c()V
    .registers 5

    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/zxing/client/android/a/a;->c:Z

    if-nez v0, :cond_22

    iget-object v0, p0, Lcom/google/zxing/client/android/a/a;->g:Landroid/os/AsyncTask;

    if-nez v0, :cond_22

    new-instance v2, Lcom/google/zxing/client/android/a/c;

    const/4 v0, 0x0

    invoke-direct {v2, p0, v0}, Lcom/google/zxing/client/android/a/c;-><init>(Lcom/google/zxing/client/android/a/a;Lcom/google/zxing/client/android/a/b;)V
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_24

    :try_start_f
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v1}, Lcom/google/zxing/client/android/a/c;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    iput-object v2, p0, Lcom/google/zxing/client/android/a/a;->g:Landroid/os/AsyncTask;
    :try_end_19
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_f .. :try_end_19} :catch_1a
    .catchall {:try_start_f .. :try_end_19} :catchall_24

    goto :goto_22

    :catch_1a
    move-exception v3

    :try_start_1b
    sget-object v0, Lcom/google/zxing/client/android/a/a;->a:Ljava/lang/String;

    const-string v1, "Could not request auto focus"

    invoke-static {v0, v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_22
    .catchall {:try_start_1b .. :try_end_22} :catchall_24

    :cond_22
    :goto_22
    monitor-exit p0

    return-void

    :catchall_24
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private declared-synchronized d()V
    .registers 4

    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/google/zxing/client/android/a/a;->g:Landroid/os/AsyncTask;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/zxing/client/android/a/a;->g:Landroid/os/AsyncTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_15

    iget-object v0, p0, Lcom/google/zxing/client/android/a/a;->g:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    :cond_15
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/zxing/client/android/a/a;->g:Landroid/os/AsyncTask;
    :try_end_18
    .catchall {:try_start_1 .. :try_end_18} :catchall_1a

    :cond_18
    monitor-exit p0

    return-void

    :catchall_1a
    move-exception v2

    monitor-exit p0

    throw v2
.end method


# virtual methods
.method declared-synchronized a()V
    .registers 4

    monitor-enter p0

    :try_start_1
    iget-boolean v0, p0, Lcom/google/zxing/client/android/a/a;->e:Z

    if-eqz v0, :cond_24

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/zxing/client/android/a/a;->g:Landroid/os/AsyncTask;

    iget-boolean v0, p0, Lcom/google/zxing/client/android/a/a;->c:Z

    if-nez v0, :cond_24

    iget-boolean v0, p0, Lcom/google/zxing/client/android/a/a;->d:Z
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_26

    if-nez v0, :cond_24

    :try_start_10
    iget-object v0, p0, Lcom/google/zxing/client/android/a/a;->f:Landroid/hardware/Camera;

    invoke-virtual {v0, p0}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/zxing/client/android/a/a;->d:Z
    :try_end_18
    .catch Ljava/lang/RuntimeException; {:try_start_10 .. :try_end_18} :catch_19
    .catchall {:try_start_10 .. :try_end_18} :catchall_26

    goto :goto_24

    :catch_19
    move-exception v2

    :try_start_1a
    sget-object v0, Lcom/google/zxing/client/android/a/a;->a:Ljava/lang/String;

    const-string v1, "Unexpected exception while focusing"

    invoke-static {v0, v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-direct {p0}, Lcom/google/zxing/client/android/a/a;->c()V
    :try_end_24
    .catchall {:try_start_1a .. :try_end_24} :catchall_26

    :cond_24
    :goto_24
    monitor-exit p0

    return-void

    :catchall_26
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method declared-synchronized b()V
    .registers 4

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lcom/google/zxing/client/android/a/a;->c:Z

    iget-boolean v0, p0, Lcom/google/zxing/client/android/a/a;->e:Z

    if-eqz v0, :cond_19

    invoke-direct {p0}, Lcom/google/zxing/client/android/a/a;->d()V
    :try_end_b
    .catchall {:try_start_2 .. :try_end_b} :catchall_1b

    :try_start_b
    iget-object v0, p0, Lcom/google/zxing/client/android/a/a;->f:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelAutoFocus()V
    :try_end_10
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_10} :catch_11
    .catchall {:try_start_b .. :try_end_10} :catchall_1b

    goto :goto_19

    :catch_11
    move-exception v2

    :try_start_12
    sget-object v0, Lcom/google/zxing/client/android/a/a;->a:Ljava/lang/String;

    const-string v1, "Unexpected exception while cancelling focusing"

    invoke-static {v0, v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_19
    .catchall {:try_start_12 .. :try_end_19} :catchall_1b

    :cond_19
    :goto_19
    monitor-exit p0

    return-void

    :catchall_1b
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized onAutoFocus(ZLandroid/hardware/Camera;)V
    .registers 4

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lcom/google/zxing/client/android/a/a;->d:Z

    invoke-direct {p0}, Lcom/google/zxing/client/android/a/a;->c()V
    :try_end_7
    .catchall {:try_start_2 .. :try_end_7} :catchall_9

    monitor-exit p0

    return-void

    :catchall_9
    move-exception p1

    monitor-exit p0

    throw p1
.end method

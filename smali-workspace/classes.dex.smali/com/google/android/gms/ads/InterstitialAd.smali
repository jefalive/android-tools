.class public final Lcom/google/android/gms/ads/InterstitialAd;
.super Ljava/lang/Object;


# instance fields
.field private final zzoL:Lcom/google/android/gms/ads/internal/client/zzac;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/ads/internal/client/zzac;

    invoke-direct {v0, p1}, Lcom/google/android/gms/ads/internal/client/zzac;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/InterstitialAd;->zzoL:Lcom/google/android/gms/ads/internal/client/zzac;

    return-void
.end method


# virtual methods
.method public loadAd(Lcom/google/android/gms/ads/AdRequest;)V
    .registers 4
    .param p1, "adRequest"    # Lcom/google/android/gms/ads/AdRequest;

    iget-object v0, p0, Lcom/google/android/gms/ads/InterstitialAd;->zzoL:Lcom/google/android/gms/ads/internal/client/zzac;

    invoke-virtual {p1}, Lcom/google/android/gms/ads/AdRequest;->zzaE()Lcom/google/android/gms/ads/internal/client/zzaa;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/client/zzac;->zza(Lcom/google/android/gms/ads/internal/client/zzaa;)V

    return-void
.end method

.method public setAdListener(Lcom/google/android/gms/ads/AdListener;)V
    .registers 4
    .param p1, "adListener"    # Lcom/google/android/gms/ads/AdListener;

    iget-object v0, p0, Lcom/google/android/gms/ads/InterstitialAd;->zzoL:Lcom/google/android/gms/ads/internal/client/zzac;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/ads/internal/client/zzac;->setAdListener(Lcom/google/android/gms/ads/AdListener;)V

    if-eqz p1, :cond_14

    instance-of v0, p1, Lcom/google/android/gms/ads/internal/client/zza;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/gms/ads/InterstitialAd;->zzoL:Lcom/google/android/gms/ads/internal/client/zzac;

    move-object v1, p1

    check-cast v1, Lcom/google/android/gms/ads/internal/client/zza;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/client/zzac;->zza(Lcom/google/android/gms/ads/internal/client/zza;)V

    goto :goto_1c

    :cond_14
    if-nez p1, :cond_1c

    iget-object v0, p0, Lcom/google/android/gms/ads/InterstitialAd;->zzoL:Lcom/google/android/gms/ads/internal/client/zzac;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/client/zzac;->zza(Lcom/google/android/gms/ads/internal/client/zza;)V

    :cond_1c
    :goto_1c
    return-void
.end method

.method public setAdUnitId(Ljava/lang/String;)V
    .registers 3
    .param p1, "adUnitId"    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/ads/InterstitialAd;->zzoL:Lcom/google/android/gms/ads/internal/client/zzac;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/ads/internal/client/zzac;->setAdUnitId(Ljava/lang/String;)V

    return-void
.end method

.method public setRewardedVideoAdListener(Lcom/google/android/gms/ads/reward/RewardedVideoAdListener;)V
    .registers 3
    .param p1, "rewardedVideoAdListener"    # Lcom/google/android/gms/ads/reward/RewardedVideoAdListener;

    iget-object v0, p0, Lcom/google/android/gms/ads/InterstitialAd;->zzoL:Lcom/google/android/gms/ads/internal/client/zzac;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/ads/internal/client/zzac;->setRewardedVideoAdListener(Lcom/google/android/gms/ads/reward/RewardedVideoAdListener;)V

    return-void
.end method

.method public show()V
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/ads/InterstitialAd;->zzoL:Lcom/google/android/gms/ads/internal/client/zzac;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/client/zzac;->show()V

    return-void
.end method

.method public zza(Z)V
    .registers 3

    iget-object v0, p0, Lcom/google/android/gms/ads/InterstitialAd;->zzoL:Lcom/google/android/gms/ads/internal/client/zzac;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/ads/internal/client/zzac;->zza(Z)V

    return-void
.end method

.method public zzm(Ljava/lang/String;)V
    .registers 3

    iget-object v0, p0, Lcom/google/android/gms/ads/InterstitialAd;->zzoL:Lcom/google/android/gms/ads/internal/client/zzac;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/ads/internal/client/zzac;->setUserId(Ljava/lang/String;)V

    return-void
.end method

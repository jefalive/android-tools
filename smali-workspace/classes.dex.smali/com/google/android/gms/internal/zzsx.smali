.class public final Lcom/google/android/gms/internal/zzsx;
.super Ljava/lang/Object;


# static fields
.field public static final zzbuA:[Z

.field public static final zzbuB:[Ljava/lang/String;

.field public static final zzbuC:[[B

.field public static final zzbuD:[B

.field public static final zzbuw:[I

.field public static final zzbux:[J

.field public static final zzbuy:[F

.field public static final zzbuz:[D


# direct methods
.method static constructor <clinit>()V
    .registers 1

    const/4 v0, 0x0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/gms/internal/zzsx;->zzbuw:[I

    const/4 v0, 0x0

    new-array v0, v0, [J

    sput-object v0, Lcom/google/android/gms/internal/zzsx;->zzbux:[J

    const/4 v0, 0x0

    new-array v0, v0, [F

    sput-object v0, Lcom/google/android/gms/internal/zzsx;->zzbuy:[F

    const/4 v0, 0x0

    new-array v0, v0, [D

    sput-object v0, Lcom/google/android/gms/internal/zzsx;->zzbuz:[D

    const/4 v0, 0x0

    new-array v0, v0, [Z

    sput-object v0, Lcom/google/android/gms/internal/zzsx;->zzbuA:[Z

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/internal/zzsx;->zzbuB:[Ljava/lang/String;

    const/4 v0, 0x0

    new-array v0, v0, [[B

    sput-object v0, Lcom/google/android/gms/internal/zzsx;->zzbuC:[[B

    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lcom/google/android/gms/internal/zzsx;->zzbuD:[B

    return-void
.end method

.method static zzF(II)I
    .registers 3

    shl-int/lit8 v0, p0, 0x3

    or-int/2addr v0, p1

    return v0
.end method

.method public static zzb(Lcom/google/android/gms/internal/zzsm;I)Z
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/zzsm;->zzmo(I)Z

    move-result v0

    return v0
.end method

.method public static final zzc(Lcom/google/android/gms/internal/zzsm;I)I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzsm;->getPosition()I

    move-result v2

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/zzsm;->zzmo(I)Z

    :goto_8
    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzsm;->zzIX()I

    move-result v0

    if-ne v0, p1, :cond_14

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/zzsm;->zzmo(I)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_14
    invoke-virtual {p0, v2}, Lcom/google/android/gms/internal/zzsm;->zzms(I)V

    return v1
.end method

.method static zzmI(I)I
    .registers 2

    and-int/lit8 v0, p0, 0x7

    return v0
.end method

.method public static zzmJ(I)I
    .registers 2

    ushr-int/lit8 v0, p0, 0x3

    return v0
.end method

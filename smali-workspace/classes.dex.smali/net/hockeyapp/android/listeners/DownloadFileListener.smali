.class public abstract Lnet/hockeyapp/android/listeners/DownloadFileListener;
.super Lnet/hockeyapp/android/StringListener;
.source "DownloadFileListener.java"


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 40
    invoke-direct {p0}, Lnet/hockeyapp/android/StringListener;-><init>()V

    return-void
.end method


# virtual methods
.method public downloadFailed(Lnet/hockeyapp/android/tasks/DownloadFileTask;Ljava/lang/Boolean;)V
    .registers 3
    .param p1, "task"    # Lnet/hockeyapp/android/tasks/DownloadFileTask;
    .param p2, "userWantsRetry"    # Ljava/lang/Boolean;

    .line 42
    return-void
.end method

.method public downloadSuccessful(Lnet/hockeyapp/android/tasks/DownloadFileTask;)V
    .registers 2
    .param p1, "task"    # Lnet/hockeyapp/android/tasks/DownloadFileTask;

    .line 45
    return-void
.end method

.class public final Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;
.super Ljava/lang/Object;
.source "FormatadorTelefone.java"

# interfaces
.implements Lbr/com/concretesolutions/canarinho/formatador/Formatador;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone$SingletonHolder;
    }
.end annotation


# static fields
.field private static final FORMATADOR_NOVE_DIGITOS:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

.field private static final FORMATADOR_OITO_DIGITOS:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

.field private static final NOVE_DIGITOS_DESFORMATADO:Ljava/util/regex/Pattern;

.field private static final NOVE_DIGITOS_FORMATADO:Ljava/util/regex/Pattern;

.field private static final OITO_DIGITOS_DESFORMATADO:Ljava/util/regex/Pattern;

.field private static final OITO_DIGITOS_FORMATADO:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    .line 8
    const-string v0, "\\((\\d{2})\\)\\s(\\d{5})-(\\d{4})"

    .line 9
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->NOVE_DIGITOS_FORMATADO:Ljava/util/regex/Pattern;

    .line 10
    const-string v0, "(\\d{2})(\\d{5})(\\d{4})"

    .line 11
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->NOVE_DIGITOS_DESFORMATADO:Ljava/util/regex/Pattern;

    .line 12
    new-instance v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->NOVE_DIGITOS_FORMATADO:Ljava/util/regex/Pattern;

    const-string v2, "($1) $2-$3"

    sget-object v3, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->NOVE_DIGITOS_DESFORMATADO:Ljava/util/regex/Pattern;

    const-string v4, "$1$2$3"

    invoke-direct {v0, v1, v2, v3, v4}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;-><init>(Ljava/util/regex/Pattern;Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)V

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->FORMATADOR_NOVE_DIGITOS:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    .line 15
    const-string v0, "\\((\\d{2})\\)\\s(\\d{4})-(\\d{4})"

    .line 16
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->OITO_DIGITOS_FORMATADO:Ljava/util/regex/Pattern;

    .line 17
    const-string v0, "(\\d{2})(\\d{4})(\\d{4})"

    .line 18
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->OITO_DIGITOS_DESFORMATADO:Ljava/util/regex/Pattern;

    .line 19
    new-instance v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    sget-object v1, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->OITO_DIGITOS_FORMATADO:Ljava/util/regex/Pattern;

    const-string v2, "($1) $2-$3"

    sget-object v3, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->OITO_DIGITOS_DESFORMATADO:Ljava/util/regex/Pattern;

    const-string v4, "$1$2$3"

    invoke-direct {v0, v1, v2, v3, v4}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;-><init>(Ljava/util/regex/Pattern;Ljava/lang/String;Ljava/util/regex/Pattern;Ljava/lang/String;)V

    sput-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->FORMATADOR_OITO_DIGITOS:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    return-void
.end method

.method private constructor <init>()V
    .registers 1

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone$1;)V
    .registers 2
    .param p1, "x0"    # Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone$1;

    .line 6
    invoke-direct {p0}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;-><init>()V

    return-void
.end method

.method private ehNoveDigitos(Ljava/lang/String;)Z
    .registers 5
    .param p1, "value"    # Ljava/lang/String;

    .line 66
    if-nez p1, :cond_a

    .line 67
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Valor n\u00e3o pode ser nulo"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_a
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/Formatador$Padroes;->PADRAO_SOMENTE_NUMEROS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 71
    .local v2, "desformatado":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xa

    if-le v0, v1, :cond_20

    const/4 v0, 0x1

    goto :goto_21

    :cond_20
    const/4 v0, 0x0

    :goto_21
    return v0
.end method

.method static getInstance()Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;
    .registers 1

    .line 26
    # getter for: Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone$SingletonHolder;->INSTANCE:Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;
    invoke-static {}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone$SingletonHolder;->access$000()Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public desformata(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .param p1, "value"    # Ljava/lang/String;

    .line 40
    invoke-direct {p0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->ehNoveDigitos(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 41
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->FORMATADOR_NOVE_DIGITOS:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    invoke-virtual {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->desformata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 44
    :cond_d
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->FORMATADOR_OITO_DIGITOS:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    invoke-virtual {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->desformata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public estaFormatado(Ljava/lang/String;)Z
    .registers 3
    .param p1, "value"    # Ljava/lang/String;

    .line 49
    invoke-direct {p0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->ehNoveDigitos(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 50
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->FORMATADOR_NOVE_DIGITOS:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    invoke-virtual {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->estaFormatado(Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 53
    :cond_d
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->FORMATADOR_OITO_DIGITOS:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    invoke-virtual {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->estaFormatado(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public formata(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .param p1, "value"    # Ljava/lang/String;

    .line 31
    invoke-direct {p0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->ehNoveDigitos(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 32
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->FORMATADOR_NOVE_DIGITOS:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    invoke-virtual {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 35
    :cond_d
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->FORMATADOR_OITO_DIGITOS:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    invoke-virtual {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public podeSerFormatado(Ljava/lang/String;)Z
    .registers 3
    .param p1, "value"    # Ljava/lang/String;

    .line 58
    invoke-direct {p0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->ehNoveDigitos(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 59
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->FORMATADOR_NOVE_DIGITOS:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    invoke-virtual {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->podeSerFormatado(Ljava/lang/String;)Z

    move-result v0

    return v0

    .line 62
    :cond_d
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorTelefone;->FORMATADOR_OITO_DIGITOS:Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;

    invoke-virtual {v0, p1}, Lbr/com/concretesolutions/canarinho/formatador/FormatadorBase;->podeSerFormatado(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

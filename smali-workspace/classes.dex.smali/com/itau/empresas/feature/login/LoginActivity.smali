.class public Lcom/itau/empresas/feature/login/LoginActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "LoginActivity.java"

# interfaces
.implements Lcom/itau/empresas/feature/login/TecladoVirtualFragment$TeclaSelecionadaClickListener;
.implements Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;


# instance fields
.field private analyticsHelper:Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

.field protected boletoPdfUri:Landroid/net/Uri;

.field botaoAcessar:Lcom/itau/empresas/ui/view/BotaoComProgresso;

.field private codigoTeclado:Ljava/lang/String;

.field customApplication:Lcom/itau/empresas/CustomApplication;

.field iniciaExibindoTeclado:Z

.field protected instalarIToken:Z

.field lembrarAcesso:Z

.field loginController:Lcom/itau/empresas/feature/login/controller/LoginController;

.field loginFingerPrint:Lcom/itau/empresas/feature/login/LoginFingerPrint;

.field private navegacao:I

.field operador:Lcom/itau/empresas/api/model/OperadorVO;

.field preLoginController:Lcom/itau/empresas/feature/login/controller/PreLoginController;

.field rlDadosContaEmOperacao:Landroid/widget/RelativeLayout;

.field root:Landroid/view/ViewGroup;

.field private tecladoVirtualFragment:Lcom/itau/empresas/feature/login/TecladoVirtualFragment;

.field textoLoginSenha:Landroid/widget/TextView;

.field toolbar:Landroid/support/v7/widget/Toolbar;

.field txtSubtitulo1:Landroid/widget/TextView;

.field txtSubtitulo2:Landroid/widget/TextView;

.field txtTitulo:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 65
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    .line 92
    const/4 v0, 0x1

    iput v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->navegacao:I

    .line 121
    invoke-static {}, Lcom/itau/empresas/feature/login/controller/PreLoginController;->getInstance()Lcom/itau/empresas/feature/login/controller/PreLoginController;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->preLoginController:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/login/LoginActivity;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/LoginActivity;

    .line 65
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->limparCampoSenha()V

    return-void
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/login/LoginActivity;I)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/login/LoginActivity;
    .param p1, "x1"    # I

    .line 65
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/login/LoginActivity;->botaoContinuarDeveEstarAcessivel(I)V

    return-void
.end method

.method private botaoContinuarDeveEstarAcessivel(I)V
    .registers 4
    .param p1, "caracteresSenha"    # I

    .line 190
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->botaoAcessar:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    const/4 v1, 0x6

    if-lt p1, v1, :cond_7

    const/4 v1, 0x1

    goto :goto_8

    :cond_7
    const/4 v1, 0x0

    :goto_8
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->setEnabled(Z)V

    .line 191
    return-void
.end method

.method private configuraCampoDeSenha()V
    .registers 3

    .line 469
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->textoLoginSenha:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->criaTextWatcherComAcessibilidade()Lcom/itau/empresas/ui/util/SimpleTextWatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 470
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->textoLoginSenha:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod;

    invoke-direct {v1}, Lcom/itau/empresas/ui/util/ViewUtils$CustomPasswordTransformationMethod;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 471
    return-void
.end method

.method private configuraLoginFingerPrint()V
    .registers 3

    .line 487
    new-instance v0, Lcom/itau/empresas/feature/login/LoginFingerPrint;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/LoginActivity;->customApplication:Lcom/itau/empresas/CustomApplication;

    invoke-direct {v0, p0, v1}, Lcom/itau/empresas/feature/login/LoginFingerPrint;-><init>(Lcom/itau/empresas/feature/login/LoginActivity;Lcom/itau/empresas/CustomApplication;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->loginFingerPrint:Lcom/itau/empresas/feature/login/LoginFingerPrint;

    .line 488
    return-void
.end method

.method private criaTextWatcherComAcessibilidade()Lcom/itau/empresas/ui/util/SimpleTextWatcher;
    .registers 2

    .line 453
    new-instance v0, Lcom/itau/empresas/feature/login/LoginActivity$4;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/login/LoginActivity$4;-><init>(Lcom/itau/empresas/feature/login/LoginActivity;)V

    return-object v0
.end method

.method private disparaEventoAdobe(Ljava/lang/String;)V
    .registers 5
    .param p1, "mensagemErro"    # Ljava/lang/String;

    .line 562
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->analyticsHelper:Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    .line 563
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->analyticsHelper:Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    .line 565
    const v1, 0x7f0700ec

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/login/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Erro"

    .line 564
    invoke-virtual {v0, v1, v2, p1}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    .line 569
    const v0, 0x7f0700ec

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/login/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 570
    const v1, 0x7f0700cc

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/login/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 571
    const v2, 0x7f0700c4

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/login/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 568
    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/feature/login/LoginActivity;->disparaEventoAnalytics(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    return-void
.end method

.method private disparaEventoAnalytics(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "nome"    # Ljava/lang/String;
    .param p2, "secao"    # Ljava/lang/String;
    .param p3, "estadoLogin"    # Ljava/lang/String;

    .line 575
    invoke-static {}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->getInstance()Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->analyticsHelper:Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    .line 576
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->analyticsHelper:Lcom/itau/empresas/feature/analytics/AnalyticsHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/itau/empresas/feature/analytics/AnalyticsHelper;->eventoAdobeState(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    return-void
.end method

.method private efetuatrocaDeSenha(Z)V
    .registers 6
    .param p1, "isPrimeiroAcesso"    # Z

    .line 417
    if-eqz p1, :cond_5

    .line 418
    const-string v2, "primeiroAcesso"

    .local v2, "extra":Ljava/lang/String;
    goto :goto_7

    .line 420
    .end local v2    # "extra":Ljava/lang/String;
    :cond_5
    const-string v2, "trocaObrigatoria"

    .line 423
    .local v2, "extra":Ljava/lang/String;
    :goto_7
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 424
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->cancelable(Z)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 425
    const v1, 0x7f07066e

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->UM_BOTAO:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 426
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 427
    const v1, 0x7f07066d

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNeutro(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 428
    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v3

    .line 430
    .local v3, "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    new-instance v0, Lcom/itau/empresas/feature/login/LoginActivity$3;

    invoke-direct {v0, p0, v3, v2}, Lcom/itau/empresas/feature/login/LoginActivity$3;-><init>(Lcom/itau/empresas/feature/login/LoginActivity;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNeutroListener(Landroid/view/View$OnClickListener;)V

    .line 443
    invoke-virtual {v3, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 444
    return-void
.end method

.method private executaPreLogin()V
    .registers 4

    .line 503
    new-instance v0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;

    invoke-direct {v0}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;-><init>()V

    iget-object v1, p0, Lcom/itau/empresas/feature/login/LoginActivity;->operador:Lcom/itau/empresas/api/model/OperadorVO;

    .line 504
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/OperadorVO;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->operador(Ljava/lang/String;)Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->build()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;

    move-result-object v2

    .line 505
    .local v2, "build":Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->preLoginController:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    invoke-static {v0, v2, p0}, Lcom/itau/empresas/feature/login/strategy/PreLoginStrategyFactory;->operadorUnico(Lcom/itau/empresas/feature/login/controller/PreLoginController;Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;)Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;

    move-result-object v0

    invoke-interface {v0}, Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;->executar()V

    .line 506
    return-void
.end method

.method private exibeMensagemError(Ljava/lang/String;)V
    .registers 3
    .param p1, "mensagem"    # Ljava/lang/String;

    .line 539
    new-instance v0, Lcom/itau/empresas/feature/login/LoginActivity$5;

    invoke-direct {v0, p0, p1}, Lcom/itau/empresas/feature/login/LoginActivity$5;-><init>(Lcom/itau/empresas/feature/login/LoginActivity;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/login/LoginActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 547
    return-void
.end method

.method private exibeTeclado()V
    .registers 3

    .line 495
    iget-boolean v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->iniciaExibindoTeclado:Z

    if-eqz v0, :cond_8

    .line 496
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->chamaAPITeclado()V

    goto :goto_e

    .line 498
    :cond_8
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->botaoAcessar:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->alternaExibicaoParaProgresso(Z)V

    .line 500
    :goto_e
    return-void
.end method

.method private iniciaAppBar()V
    .registers 5

    .line 302
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->txtTitulo:Landroid/widget/TextView;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/itau/empresas/feature/login/LoginActivity;->operador:Lcom/itau/empresas/api/model/OperadorVO;

    .line 303
    invoke-virtual {v2}, Lcom/itau/empresas/api/model/OperadorVO;->getNomeOperador()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/itau/empresas/ui/util/StringUtils;->converteTodaPrimeiraLetraParaMaiusculo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 302
    const v2, 0x7f070456

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/feature/login/LoginActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 304
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->txtSubtitulo1:Landroid/widget/TextView;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/itau/empresas/feature/login/LoginActivity;->operador:Lcom/itau/empresas/api/model/OperadorVO;

    .line 305
    invoke-virtual {v2}, Lcom/itau/empresas/api/model/OperadorVO;->getCodigoOperador()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 304
    const v2, 0x7f070455

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/feature/login/LoginActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 307
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->rlDadosContaEmOperacao:Landroid/widget/RelativeLayout;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/itau/empresas/feature/login/LoginActivity;->operador:Lcom/itau/empresas/api/model/OperadorVO;

    .line 309
    invoke-virtual {v2}, Lcom/itau/empresas/api/model/OperadorVO;->getNomeOperador()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/itau/empresas/feature/login/LoginActivity;->operador:Lcom/itau/empresas/api/model/OperadorVO;

    .line 310
    invoke-virtual {v2}, Lcom/itau/empresas/api/model/OperadorVO;->getCodigoOperador()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 308
    const v2, 0x7f070453

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/feature/login/LoginActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 307
    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 312
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/login/LoginActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 313
    return-void
.end method

.method private iniciaTecladoVirtual(Lcom/itau/empresas/feature/login/model/TecladoVirtualVO;)V
    .registers 6
    .param p1, "tecladoVirtualVO"    # Lcom/itau/empresas/feature/login/model/TecladoVirtualVO;

    .line 392
    :try_start_0
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->botaoAcessar:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->setEnabled(Z)V

    .line 393
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->botaoAcessar:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->alternaExibicaoParaProgresso(Z)V

    .line 394
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 395
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v0, "TECLADO_ARG"

    .line 396
    invoke-virtual {p1}, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO;->getTeclas()Ljava/util/ArrayList;

    move-result-object v1

    .line 395
    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 398
    new-instance v0, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;

    invoke-direct {v0}, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->tecladoVirtualFragment:Lcom/itau/empresas/feature/login/TecladoVirtualFragment;

    .line 399
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->tecladoVirtualFragment:Lcom/itau/empresas/feature/login/TecladoVirtualFragment;

    invoke-virtual {v0, p0}, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->setTeclaSelecionadaListener(Lcom/itau/empresas/feature/login/TecladoVirtualFragment$TeclaSelecionadaClickListener;)V

    .line 400
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->tecladoVirtualFragment:Lcom/itau/empresas/feature/login/TecladoVirtualFragment;

    invoke-virtual {v0, v2}, Lcom/itau/empresas/feature/login/TecladoVirtualFragment;->setArguments(Landroid/os/Bundle;)V

    .line 402
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    .line 403
    .local v3, "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    const v0, 0x7f040012

    const v1, 0x7f040013

    invoke-virtual {v3, v0, v1}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    .line 404
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->tecladoVirtualFragment:Lcom/itau/empresas/feature/login/TecladoVirtualFragment;

    const v1, 0x7f0e0264

    invoke-virtual {v3, v1, v0}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 406
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commit()I
    :try_end_4c
    .catch Landroid/os/BadParcelableException; {:try_start_0 .. :try_end_4c} :catch_4d

    .line 410
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v3    # "fragmentTransaction":Landroid/support/v4/app/FragmentTransaction;
    goto :goto_64

    .line 407
    :catch_4d
    move-exception v2

    .line 408
    .local v2, "ex":Landroid/os/BadParcelableException;
    const v0, 0x7f0701f3

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/login/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/login/LoginActivity;->mostraEstadoDeErro(Ljava/lang/String;)V

    .line 409
    const-string v0, "teclado"

    const v1, 0x7f0701f3

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/login/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 411
    .end local v2    # "ex":Landroid/os/BadParcelableException;
    :goto_64
    return-void
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/login/LoginActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 535
    new-instance v0, Lcom/itau/empresas/feature/login/LoginActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/login/LoginActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static intentItoken(Landroid/content/Context;)Landroid/content/Intent;
    .registers 3
    .param p0, "context"    # Landroid/content/Context;

    .line 554
    invoke-static {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/login/LoginActivity_$IntentBuilder_;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/LoginActivity_$IntentBuilder_;->instalarIToken(Z)Lcom/itau/empresas/feature/login/LoginActivity_$IntentBuilder_;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/LoginActivity_$IntentBuilder_;->get()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private limparCampoSenha()V
    .registers 3

    .line 558
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->textoLoginSenha:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 559
    return-void
.end method

.method private ocultaErroEProgresso()V
    .registers 1

    .line 491
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->escondeDialogoDeProgresso()V

    .line 492
    return-void
.end method

.method private preloginValido()Z
    .registers 2

    .line 550
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->tecladoVirtualFragment:Lcom/itau/empresas/feature/login/TecladoVirtualFragment;

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    goto :goto_7

    :cond_6
    const/4 v0, 0x0

    :goto_7
    return v0
.end method

.method private redirecionamento()V
    .registers 3

    .line 474
    iget-boolean v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->instalarIToken:Z

    if-eqz v0, :cond_7

    .line 475
    const/4 v0, 0x5

    iput v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->navegacao:I

    .line 476
    :cond_7
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->loginFingerPrint:Lcom/itau/empresas/feature/login/LoginFingerPrint;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/LoginFingerPrint;->ofereceFingerPrint()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 477
    const/4 v0, 0x3

    iput v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->navegacao:I

    goto :goto_1a

    .line 478
    :cond_13
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->boletoPdfUri:Landroid/net/Uri;

    if-eqz v0, :cond_1a

    .line 479
    const/4 v0, 0x4

    iput v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->navegacao:I

    .line 482
    :cond_1a
    :goto_1a
    iget v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->navegacao:I

    iget-object v1, p0, Lcom/itau/empresas/feature/login/LoginActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {p0, v0, v1}, Lcom/itau/empresas/feature/login/LoginNavegacao;->redirecionaPara(Landroid/content/Context;ILcom/itau/empresas/CustomApplication;)V

    .line 483
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->finish()V

    .line 484
    return-void
.end method

.method private salvaLembrarAcesso()V
    .registers 3

    .line 383
    iget-boolean v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->lembrarAcesso:Z

    if-eqz v0, :cond_14

    .line 384
    new-instance v1, Lcom/itau/empresas/feature/login/LembrarAcesso;

    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 385
    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/itau/empresas/feature/login/LembrarAcesso;-><init>(Landroid/content/SharedPreferences;)V

    .line 386
    .local v1, "lembrarAcesso":Lcom/itau/empresas/feature/login/LembrarAcesso;
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->operador:Lcom/itau/empresas/api/model/OperadorVO;

    invoke-virtual {v1, v0}, Lcom/itau/empresas/feature/login/LembrarAcesso;->lembrarAcesso(Lcom/itau/empresas/api/model/OperadorVO;)V

    .line 388
    .end local v1    # "lembrarAcesso":Lcom/itau/empresas/feature/login/LembrarAcesso;
    :cond_14
    return-void
.end method


# virtual methods
.method aoClicarLinkSolucoesNegocio()V
    .registers 5

    .line 340
    new-instance v3, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 341
    .local v3, "i":Landroid/content/Intent;
    const v0, 0x7f07044a

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/login/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 342
    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/login/LoginActivity;->startActivity(Landroid/content/Intent;)V

    .line 343
    .line 344
    const v0, 0x7f0700e8

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/login/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 345
    const v1, 0x7f0700cc

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/login/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 346
    const v2, 0x7f0700c4

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/login/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 343
    invoke-direct {p0, v0, v1, v2}, Lcom/itau/empresas/feature/login/LoginActivity;->disparaEventoAnalytics(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    return-void
.end method

.method aoCriar()V
    .registers 2

    .line 133
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->operador:Lcom/itau/empresas/api/model/OperadorVO;

    if-eqz v0, :cond_14

    .line 134
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->iniciaAppBar()V

    .line 135
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->configuraCampoDeSenha()V

    .line 136
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->escondeDialogoDeProgresso()V

    .line 137
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->configuraLoginFingerPrint()V

    .line 138
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->exibeTeclado()V

    goto :goto_17

    .line 140
    :cond_14
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->finish()V

    .line 142
    :goto_17
    return-void
.end method

.method chamaAPIAutenticacaoFingerPrint()V
    .registers 3

    .line 297
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->ocultaErroEProgresso()V

    .line 298
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->loginController:Lcom/itau/empresas/feature/login/controller/LoginController;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/LoginActivity;->operador:Lcom/itau/empresas/api/model/OperadorVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/OperadorVO;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/controller/LoginController;->autenticarFingerprint(Ljava/lang/String;)V

    .line 299
    return-void
.end method

.method chamaAPITeclado()V
    .registers 3

    .line 292
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->ocultaErroEProgresso()V

    .line 293
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->loginController:Lcom/itau/empresas/feature/login/controller/LoginController;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/LoginActivity;->operador:Lcom/itau/empresas/api/model/OperadorVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/OperadorVO;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/controller/LoginController;->buscaTeclado(Ljava/lang/String;)V

    .line 294
    return-void
.end method

.method clicaEmAcessar()V
    .registers 5

    .line 326
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->botaoAcessar:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->alternaExibicaoParaProgresso(Z)V

    .line 327
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->botaoAcessar:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->setEnabled(Z)V

    .line 329
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->preloginValido()Z

    move-result v0

    if-eqz v0, :cond_2f

    .line 330
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->loginController:Lcom/itau/empresas/feature/login/controller/LoginController;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/LoginActivity;->operador:Lcom/itau/empresas/api/model/OperadorVO;

    .line 331
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/OperadorVO;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/login/LoginActivity;->textoLoginSenha:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/itau/empresas/feature/login/LoginActivity;->codigoTeclado:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/itau/empresas/feature/login/controller/LoginController;->autenticar(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_32

    .line 334
    :cond_2f
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->executaPreLogin()V

    .line 336
    :goto_32
    return-void
.end method

.method public clickTeclado(Ljava/lang/String;)V
    .registers 4
    .param p1, "tecla"    # Ljava/lang/String;

    .line 180
    if-eqz p1, :cond_8

    .line 181
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->textoLoginSenha:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    goto :goto_e

    .line 184
    :cond_8
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->textoLoginSenha:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    :goto_e
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->textoLoginSenha:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/login/LoginActivity;->botaoContinuarDeveEstarAcessivel(I)V

    .line 187
    return-void
.end method

.method public falha(Ljava/lang/String;)V
    .registers 4
    .param p1, "backendException"    # Ljava/lang/String;

    .line 521
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->botaoAcessar:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->setEnabled(Z)V

    .line 522
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->botaoAcessar:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->alternaExibicaoParaProgresso(Z)V

    .line 523
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/login/LoginActivity;->exibeMensagemError(Ljava/lang/String;)V

    .line 524
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/login/LoginActivity;->disparaEventoAdobe(Ljava/lang/String;)V

    .line 525
    return-void
.end method

.method habilitarTeclado()V
    .registers 3

    .line 317
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->preloginValido()Z

    move-result v0

    if-nez v0, :cond_1a

    .line 318
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->botaoAcessar:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->alternaExibicaoParaProgresso(Z)V

    .line 319
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->botaoAcessar:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->setEnabled(Z)V

    .line 320
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->executaPreLogin()V

    .line 322
    :cond_1a
    return-void
.end method

.method public onBackPressed()V
    .registers 3

    .line 528
    invoke-static {p0}, Lcom/itau/empresas/feature/login/PreLoginActivity_;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;

    move-result-object v0

    .line 529
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;->navegacaoVindoDoLogin(Z)Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;

    move-result-object v0

    .line 530
    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/PreLoginActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 531
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->finish()V

    .line 532
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .param p1, "menu"    # Landroid/view/Menu;

    .line 165
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f0004

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 166
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 4
    .param p1, "exception"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 263
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->ocultaErroEProgresso()V

    .line 264
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->botaoAcessar:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->alternaExibicaoParaProgresso(Z)V

    .line 265
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->limparCampoSenha()V

    .line 266
    const-class v0, Lcom/itau/empresas/feature/login/LoginActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/login/LoginActivity;->exibeMensagemError(Ljava/lang/String;)V

    .line 268
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;)V
    .registers 6
    .param p1, "fingerprintException"    # Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;

    .line 272
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->UM_BOTAO:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 273
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 274
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/fingerprintcore/ItauFingerprintException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 275
    const v2, 0x7f07044b

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/login/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 274
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 276
    const v1, 0x7f07048c

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNeutro(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v3

    .line 278
    .local v3, "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    new-instance v0, Lcom/itau/empresas/feature/login/LoginActivity$1;

    invoke-direct {v0, p0, v3}, Lcom/itau/empresas/feature/login/LoginActivity$1;-><init>(Lcom/itau/empresas/feature/login/LoginActivity;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v3, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNeutroListener(Landroid/view/View$OnClickListener;)V

    .line 288
    invoke-virtual {v3, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 289
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoLoginComSenha;)V
    .registers 4
    .param p1, "login"    # Lcom/itau/empresas/Evento$EventoLoginComSenha;

    .line 257
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->ocultaErroEProgresso()V

    .line 258
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->loginFingerPrint:Lcom/itau/empresas/feature/login/LoginFingerPrint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/LoginFingerPrint;->setLogarComTeclado(Z)V

    .line 259
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->chamaAPITeclado()V

    .line 260
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoLoginFinalizado;)V
    .registers 4
    .param p1, "logonFinalizado"    # Lcom/itau/empresas/Evento$EventoLoginFinalizado;

    .line 376
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->ocultaErroEProgresso()V

    .line 377
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->botaoAcessar:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->alternaExibicaoParaProgresso(Z)V

    .line 378
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->redirecionamento()V

    .line 379
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->salvaLembrarAcesso()V

    .line 380
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoPrimeiroAcesso;)V
    .registers 5
    .param p1, "eventoPrimeiroAcesso"    # Lcom/itau/empresas/Evento$EventoPrimeiroAcesso;

    .line 357
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->ocultaErroEProgresso()V

    .line 358
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->botaoAcessar:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->alternaExibicaoParaProgresso(Z)V

    .line 360
    new-instance v0, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    invoke-direct {v0, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;->UM_BOTAO:Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;

    .line 361
    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->estado(Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Estados;)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 362
    const v1, 0x7f0704f9

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->mensagem(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    .line 363
    const v1, 0x7f07048c

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->textoNeutro(I)Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog$Builder;->build()Lcom/itau/empresas/ui/dialog/CustomAlertDialog;

    move-result-object v2

    .line 365
    .local v2, "dialog":Lcom/itau/empresas/ui/dialog/CustomAlertDialog;
    new-instance v0, Lcom/itau/empresas/feature/login/LoginActivity$2;

    invoke-direct {v0, p0, v2}, Lcom/itau/empresas/feature/login/LoginActivity$2;-><init>(Lcom/itau/empresas/feature/login/LoginActivity;Lcom/itau/empresas/ui/dialog/CustomAlertDialog;)V

    invoke-virtual {v2, v0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->setNeutroListener(Landroid/view/View$OnClickListener;)V

    .line 372
    invoke-virtual {v2, p0}, Lcom/itau/empresas/ui/dialog/CustomAlertDialog;->show(Landroid/support/v7/app/AppCompatActivity;)Lcom/itau/empresas/ui/dialog/BaseDialog;

    .line 373
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/Evento$EventoTrocaDeSenha;)V
    .registers 4
    .param p1, "trocaDeSenha"    # Lcom/itau/empresas/Evento$EventoTrocaDeSenha;

    .line 351
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->ocultaErroEProgresso()V

    .line 352
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->botaoAcessar:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->alternaExibicaoParaProgresso(Z)V

    .line 353
    invoke-static {}, Lcom/itau/empresas/Evento$EventoTrocaDeSenha;->getPrimeiroAcesso()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/login/LoginActivity;->efetuatrocaDeSenha(Z)V

    .line 354
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/login/model/TecladoVirtualVO;)V
    .registers 5
    .param p1, "tecladoVirtualVO"    # Lcom/itau/empresas/feature/login/model/TecladoVirtualVO;

    .line 222
    invoke-direct {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->ocultaErroEProgresso()V

    .line 224
    invoke-virtual {p1}, Lcom/itau/empresas/feature/login/model/TecladoVirtualVO;->getIdTeclado()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->codigoTeclado:Ljava/lang/String;

    .line 227
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getChaveAcesso()I

    move-result v2

    .line 228
    .local v2, "chaveAcesso":I
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_58

    .line 229
    invoke-static {v2}, Lcom/itau/empresas/feature/chaveacesso/ChaveAcessoValidador;->chaveAcessoLiberadoParaLogin(I)Z

    move-result v0

    if-eqz v0, :cond_58

    .line 231
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->loginFingerPrint:Lcom/itau/empresas/feature/login/LoginFingerPrint;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/LoginFingerPrint;->verificaFingerprintHabilitado()Z

    move-result v0

    if-eqz v0, :cond_54

    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->operador:Lcom/itau/empresas/api/model/OperadorVO;

    .line 232
    invoke-virtual {v0}, Lcom/itau/empresas/api/model/OperadorVO;->getCodigoOperador()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->hasKeyAccess(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_54

    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->loginFingerPrint:Lcom/itau/empresas/feature/login/LoginFingerPrint;

    .line 233
    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/LoginFingerPrint;->getLogarComTeclado()Z

    move-result v0

    if-nez v0, :cond_54

    .line 235
    sparse-switch v2, :sswitch_data_5c

    goto :goto_50

    .line 237
    :sswitch_3b
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->loginFingerPrint:Lcom/itau/empresas/feature/login/LoginFingerPrint;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/LoginFingerPrint;->autenticarFingerPrint()V

    .line 238
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->botaoAcessar:Lcom/itau/empresas/ui/view/BotaoComProgresso;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/view/BotaoComProgresso;->alternaExibicaoParaProgresso(Z)V

    .line 239
    goto :goto_5b

    .line 241
    :sswitch_47
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->loginFingerPrint:Lcom/itau/empresas/feature/login/LoginFingerPrint;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/LoginFingerPrint;->exibeMensagemFingerPrintEmOutroDipositivo()V

    .line 242
    const/4 v0, 0x2

    iput v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->navegacao:I

    .line 243
    goto :goto_5b

    .line 245
    :goto_50
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/login/LoginActivity;->iniciaTecladoVirtual(Lcom/itau/empresas/feature/login/model/TecladoVirtualVO;)V

    .line 246
    goto :goto_5b

    .line 249
    :cond_54
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/login/LoginActivity;->iniciaTecladoVirtual(Lcom/itau/empresas/feature/login/model/TecladoVirtualVO;)V

    goto :goto_5b

    .line 252
    :cond_58
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/login/LoginActivity;->iniciaTecladoVirtual(Lcom/itau/empresas/feature/login/model/TecladoVirtualVO;)V

    .line 254
    :goto_5b
    return-void

    :sswitch_data_5c
    .sparse-switch
        0x2 -> :sswitch_3b
        0x3 -> :sswitch_47
    .end sparse-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .registers 5
    .param p1, "intent"    # Landroid/content/Intent;

    .line 146
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 147
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 148
    .local v1, "extras":Landroid/os/Bundle;
    if-nez v1, :cond_a

    .line 149
    return-void

    .line 150
    :cond_a
    const-string v0, "instalarIToken"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->instalarIToken:Z

    .line 151
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 152
    .local v2, "intentAntigo":Landroid/content/Intent;
    invoke-virtual {v2, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 153
    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/login/LoginActivity;->setIntent(Landroid/content/Intent;)V

    .line 154
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .line 172
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0e06ce

    if-ne v0, v1, :cond_c

    .line 173
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->onBackPressed()V

    .line 175
    :cond_c
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/activity/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 158
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "teclado"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "logon"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "menu"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const-string v1, "operadorDetalhes"

    const/4 v2, 0x3

    aput-object v1, v0, v2

    const-string v1, "listaContas"

    const/4 v2, 0x4

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public proximoPasso(I)V
    .registers 2
    .param p1, "tipoError"    # I

    .line 517
    return-void
.end method

.method public sucesso(Lcom/itau/empresas/api/model/OperadorVO;)V
    .registers 3
    .param p1, "operadorVO"    # Lcom/itau/empresas/api/model/OperadorVO;

    .line 510
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0, p1}, Lcom/itau/empresas/feature/chaveacesso/ChaveAcessoUtil;->defineChaveAcesso(Lcom/itau/empresas/CustomApplication;Lcom/itau/empresas/api/model/OperadorVO;)V

    .line 511
    invoke-virtual {p0}, Lcom/itau/empresas/feature/login/LoginActivity;->chamaAPITeclado()V

    .line 512
    return-void
.end method

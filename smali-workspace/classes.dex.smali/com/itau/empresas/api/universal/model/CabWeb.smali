.class public Lcom/itau/empresas/api/universal/model/CabWeb;
.super Ljava/lang/Object;
.source "CabWeb.java"


# instance fields
.field private cnpj:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        name = "CNPJ"
        required = false
    .end annotation
.end field

.field private codOp:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        name = "COD_OP"
        required = false
    .end annotation
.end field

.field private codt:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        name = "CODT"
        required = false
    .end annotation
.end field

.field private inst:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        name = "INST"
        required = false
    .end annotation
.end field

.field private st:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        name = "ST"
        required = false
    .end annotation
.end field

.field private tela:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        name = "TELA"
        required = false
    .end annotation
.end field

.field private tipo:Ljava/lang/String;
    .annotation runtime Lorg/simpleframework/xml/Attribute;
        name = "TIPO"
        required = false
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 3

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "CabWeb{codt=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/universal/model/CabWeb;->codt:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", st=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/universal/model/CabWeb;->st:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tela=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/universal/model/CabWeb;->tela:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", tipo=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/api/universal/model/CabWeb;->tipo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

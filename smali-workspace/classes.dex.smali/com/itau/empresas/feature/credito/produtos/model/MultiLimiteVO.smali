.class public Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;
.super Ljava/lang/Object;
.source "MultiLimiteVO.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO$DetalheProdutoMultiLimiteVO;
    }
.end annotation


# instance fields
.field private detalheProdutoMultiLimite:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tabela_taxas_limites_produtos"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO$DetalheProdutoMultiLimiteVO;>;"
        }
    .end annotation
.end field

.field private limiteDisponivel:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "limite_disponivel"
    .end annotation
.end field

.field private limiteTotal:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "limite_total"
    .end annotation
.end field

.field private limiteTotalConcedido:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "limite_total_concedido"
    .end annotation
.end field

.field private limiteTotalUtilizado:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "limite_total_utilizado"
    .end annotation
.end field


# virtual methods
.method public getDetalheProdutoMultiLimite()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO$DetalheProdutoMultiLimiteVO;>;"
        }
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;->detalheProdutoMultiLimite:Ljava/util/List;

    return-object v0
.end method

.method public getLimiteDisponivel()Ljava/lang/Float;
    .registers 2

    .line 42
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;->limiteDisponivel:Ljava/lang/Float;

    return-object v0
.end method

.method public getLimiteTotal()Ljava/lang/Float;
    .registers 2

    .line 38
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;->limiteTotal:Ljava/lang/Float;

    return-object v0
.end method

.method public getLimiteTotalConcedido()Ljava/lang/Float;
    .registers 2

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;->limiteTotalConcedido:Ljava/lang/Float;

    return-object v0
.end method

.method public getLimiteTotalUtilizado()Ljava/lang/Float;
    .registers 2

    .line 46
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;->limiteTotalUtilizado:Ljava/lang/Float;

    return-object v0
.end method

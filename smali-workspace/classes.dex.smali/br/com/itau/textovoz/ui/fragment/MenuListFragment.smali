.class public Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;
.super Landroid/support/v4/app/Fragment;
.source "MenuListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenusAdapter;,
        Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuHolder;,
        Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuListListener;
    }
.end annotation


# static fields
.field private static ARG_KEY:Ljava/lang/String;


# instance fields
.field private listener:Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuListListener;

.field private menus:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 33
    const-string v0, "menus"

    sput-object v0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;->ARG_KEY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .registers 1

    .line 32
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;)Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuListListener;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;

    .line 32
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;->listener:Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuListListener;

    return-object v0
.end method

.method public static newInstance(Ljava/util/ArrayList;)Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;
    .registers 4
    .param p0, "menus"    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/ArrayList<Lbr/com/itau/textovoz/model/Menu;>;)Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;"
        }
    .end annotation

    .line 51
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 52
    .local v1, "bundle":Landroid/os/Bundle;
    sget-object v0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;->ARG_KEY:Ljava/lang/String;

    invoke-virtual {v1, v0, p0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 54
    new-instance v2, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;

    invoke-direct {v2}, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;-><init>()V

    .line 55
    .local v2, "f":Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;
    invoke-virtual {v2, v1}, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;->setArguments(Landroid/os/Bundle;)V

    .line 57
    return-object v2
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 62
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 63
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;->ARG_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;->menus:Ljava/util/ArrayList;

    .line 64
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 11
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 69
    sget v0, Lbr/com/itau/textovoz/R$layout;->fragment_menu_list:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 70
    .local v4, "v":Landroid/view/View;
    sget v0, Lbr/com/itau/textovoz/R$id;->text_name_title_list:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;

    .line 71
    .local v5, "headerTitleTextView":Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;
    sget v0, Lbr/com/itau/textovoz/R$string;->view_item_search_suggested:I

    invoke-virtual {p0, v0}, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lbr/com/itau/textovoz/ui/view/custom/CustomTextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    sget v0, Lbr/com/itau/textovoz/R$id;->recycler_view_menus_search:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/support/v7/widget/RecyclerView;

    .line 74
    .local v6, "recyclerView":Landroid/support/v7/widget/RecyclerView;
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 75
    new-instance v0, Landroid/support/v7/widget/DefaultItemAnimator;

    invoke-direct {v0}, Landroid/support/v7/widget/DefaultItemAnimator;-><init>()V

    invoke-virtual {v6, v0}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 77
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Landroid/support/v7/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    .line 78
    new-instance v0, Lbr/com/itau/textovoz/ui/view/DividerItemDecoration;

    .line 79
    invoke-virtual {p0}, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lbr/com/itau/textovoz/R$drawable;->divider_search_list:I

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lbr/com/itau/textovoz/ui/view/DividerItemDecoration;-><init>(Landroid/graphics/drawable/Drawable;ZZ)V

    .line 78
    invoke-virtual {v6, v0}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 80
    new-instance v0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenusAdapter;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;->menus:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1}, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenusAdapter;-><init>(Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;Ljava/util/ArrayList;)V

    invoke-virtual {v6, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 82
    return-object v4
.end method

.method public setMenuListListener(Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuListListener;)V
    .registers 2
    .param p1, "listener"    # Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuListListener;

    .line 43
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/fragment/MenuListFragment;->listener:Lbr/com/itau/textovoz/ui/fragment/MenuListFragment$MenuListListener;

    .line 44
    return-void
.end method

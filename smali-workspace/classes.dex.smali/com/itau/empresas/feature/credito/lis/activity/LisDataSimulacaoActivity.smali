.class public Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;
.super Lcom/itau/empresas/ui/activity/BaseActivity;
.source "LisDataSimulacaoActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field checkBoxLisAdicional:Landroid/widget/CheckBox;

.field controller:Lcom/itau/empresas/feature/credito/lis/LisController;

.field linearLayoutLisAdicional:Landroid/widget/LinearLayout;

.field private lisAdicional:Z

.field lisCardValorView:Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;

.field ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

.field possuiProdutoLisAdicional:Z

.field rlChequeEspecialAdicional:Landroid/view/View;

.field root:Landroid/widget/LinearLayout;

.field seletorDataPagamentoView:Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;

.field textoLisAdicional:Landroid/widget/TextView;

.field textoLisCondicoesGeraisPdf:Landroid/widget/TextView;

.field textoLisExclamacao:Landroid/widget/TextView;

.field toolBar:Landroid/support/v7/widget/Toolbar;

.field private topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

.field valorSelecionado:Ljava/math/BigDecimal;

.field warningData:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 44
    invoke-direct {p0}, Lcom/itau/empresas/ui/activity/BaseActivity;-><init>()V

    return-void
.end method

.method private configuraAcessibilidade()V
    .registers 4

    .line 199
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->warningData:Landroid/view/View;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->textoLisExclamacao:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 200
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->rlChequeEspecialAdicional:Landroid/view/View;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f070422

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 201
    const v2, 0x7f070407

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 202
    const v2, 0x7f070599

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 203
    const v2, 0x7f07059a

    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 200
    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 205
    return-void
.end method

.method private configuraLayout()V
    .registers 8

    .line 170
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->lisCardValorView:Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->valorSelecionado:Ljava/math/BigDecimal;

    .line 171
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/math/BigDecimal;Z)Ljava/lang/String;

    move-result-object v1

    .line 170
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->setTextoValor(Ljava/lang/String;)V

    .line 172
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->lisCardValorView:Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    .line 174
    invoke-virtual {v1}, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;->getTaxaJurosMes()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-static {p0, v1}, Lcom/itau/empresas/ui/util/PorcentagemUtils;->formataPorcentagemTaxaMes(Landroid/content/Context;F)Ljava/lang/String;

    move-result-object v1

    .line 172
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->setTextoObservacao(Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->lisCardValorView:Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    .line 177
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;->getTaxaJurosMes()Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 176
    const v2, 0x7f070417

    invoke-virtual {p0, v2, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 175
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/customview/LisCardValorView;->setContentDescriptionTextoObservacao(Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->textoLisExclamacao:Landroid/widget/TextView;

    const v1, 0x7f0705f3

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    .line 179
    invoke-static {}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->getDataAtual()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/itau/empresas/ui/util/DataEHoraUtils;->formataDataComBarra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    .line 178
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 181
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->textoLisCondicoesGeraisPdf:Landroid/widget/TextView;

    .line 182
    const v1, 0x7f0c000a

    invoke-static {p0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 183
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 184
    const v3, 0x7f07059a

    invoke-virtual {p0, v3}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "condi\u00e7\u00f5es gerais"

    const/4 v6, 0x0

    aput-object v5, v4, v6

    .line 182
    invoke-static {v1, v2, v3, v4}, Lcom/itau/empresas/ui/util/ViewUtils;->destacaStringAlteraCor(IZLjava/lang/String;[Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v1

    .line 181
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->possuiProdutoLisAdicional:Z

    if-nez v0, :cond_89

    .line 188
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->linearLayoutLisAdicional:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 190
    :cond_89
    return-void
.end method

.method private configuraToolBar()V
    .registers 5

    .line 193
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07040a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 194
    .local v2, "titulo":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->toolBar:Landroid/support/v7/widget/Toolbar;

    const v1, 0x7f0e05c2

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/widget/TextView;

    .line 195
    .local v3, "tituloToolbar":Landroid/widget/TextView;
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    return-void
.end method

.method private geraSimulacaoEnvioVO()Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;
    .registers 9

    .line 160
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 162
    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    .line 161
    const/4 v3, 0x7

    invoke-static {v3, v2}, Lcom/itau/empresas/ui/util/ViewUtils;->formataAdicionandoZeros(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 163
    invoke-virtual {v3}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 164
    invoke-virtual {v4}, Lcom/itau/empresas/CustomApplication;->getDadosOperador()Lcom/itau/empresas/api/model/DadosOperadorVO;

    move-result-object v4

    invoke-virtual {v4}, Lcom/itau/empresas/api/model/DadosOperadorVO;->getNumeroDocumento()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->valorSelecionado:Ljava/math/BigDecimal;

    iget-object v6, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->application:Lcom/itau/empresas/CustomApplication;

    .line 165
    invoke-virtual {v6}, Lcom/itau/empresas/CustomApplication;->getCodigoOperador()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->seletorDataPagamentoView:Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;

    .line 166
    invoke-virtual {v7}, Lcom/itau/empresas/ui/view/SeletorDataPagamentoView;->getDia()Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    return-object v0
.end method

.method public static intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 208
    new-instance v0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity_$IntentBuilder_;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private pdfCondicoesGerais()V
    .registers 4

    .line 151
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->possuiProdutoLisAdicional:Z

    if-eqz v0, :cond_7

    sget-object v1, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->condicoesGeraisAdicionalPDF:Ljava/lang/String;

    goto :goto_9

    :cond_7
    sget-object v1, Lcom/itau/empresas/feature/credito/lis/activity/LisIntroducaoActivity_;->condicoesGeraisPDF:Ljava/lang/String;

    .line 154
    .local v1, "url":Ljava/lang/String;
    :goto_9
    new-instance v2, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 155
    .local v2, "i":Landroid/content/Intent;
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 156
    invoke-virtual {p0, v2}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->startActivity(Landroid/content/Intent;)V

    .line 157
    return-void
.end method


# virtual methods
.method afterViews()V
    .registers 1

    .line 110
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->configuraToolBar()V

    .line 111
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->configuraLayout()V

    .line 112
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->configuraAcessibilidade()V

    .line 113
    return-void
.end method

.method alterarValorLisEscolhido()V
    .registers 3

    .line 138
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    .line 139
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$IntentBuilder_;->ofertaContratacaoLisVo(Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;)Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->valorSelecionado:Ljava/math/BigDecimal;

    .line 140
    invoke-virtual {v1}, Ljava/math/BigDecimal;->floatValue()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$IntentBuilder_;->valorDefault(Ljava/lang/Float;)Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$IntentBuilder_;

    move-result-object v0

    iget-boolean v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->possuiProdutoLisAdicional:Z

    .line 141
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$IntentBuilder_;->possuiProdutoLisAdiciona(Z)Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$IntentBuilder_;

    move-result-object v0

    .line 142
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisEscolhaValorActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 143
    return-void
.end method

.method clickPDf()V
    .registers 1

    .line 147
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->pdfCondicoesGerais()V

    .line 148
    return-void
.end method

.method clickcheckboxLisAdicional()V
    .registers 3

    .line 125
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->checkBoxLisAdicional:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_28

    .line 126
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->checkBoxLisAdicional:Landroid/widget/CheckBox;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 127
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->textoLisAdicional:Landroid/widget/TextView;

    const v1, 0x7f0c0039

    invoke-static {p0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 128
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->lisAdicional:Z

    goto :goto_47

    .line 130
    :cond_28
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->checkBoxLisAdicional:Landroid/widget/CheckBox;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 131
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->textoLisAdicional:Landroid/widget/TextView;

    const v1, 0x7f0c00cd

    invoke-static {p0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 132
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->lisAdicional:Z

    .line 134
    :goto_47
    return-void
.end method

.method continuar()V
    .registers 5

    .line 118
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->mostraDialogoDeProgresso()V

    .line 119
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->controller:Lcom/itau/empresas/feature/credito/lis/LisController;

    const-string v1, "contratacao"

    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->geraSimulacaoEnvioVO()Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;

    move-result-object v2

    iget-boolean v3, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->lisAdicional:Z

    .line 120
    invoke-static {v3}, Lcom/itau/empresas/feature/credito/lis/LisConstants;->getTipoProduto(Z)Ljava/lang/String;

    move-result-object v3

    .line 119
    invoke-virtual {v0, v1, v2, v3}, Lcom/itau/empresas/feature/credito/lis/LisController;->efetuaSimulacao(Ljava/lang/String;Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoEnvioVO;Ljava/lang/String;)V

    .line 121
    return-void
.end method

.method public hitAnalytics()Ljava/lang/String;
    .registers 2

    .line 72
    const v0, 0x7f070335

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .registers 2
    .param p1, "v"    # Landroid/view/View;

    .line 105
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->onBackPressed()V

    .line 106
    return-void
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 76
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 77
    return-void

    .line 80
    :cond_7
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->escondeDialogoDeProgresso()V

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v1

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 83
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;)V
    .registers 4
    .param p1, "lisSimulacaoRespostaVO"    # Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;

    .line 86
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->escondeDialogoDeProgresso()V

    .line 87
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    if-eqz v0, :cond_c

    .line 88
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->topSnackbar:Lbr/com/itau/topsnackbar/TopSnackbar;

    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->dismiss()V

    .line 90
    .line 91
    :cond_c
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_;->intent(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$IntentBuilder_;

    move-result-object v0

    iget-boolean v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->lisAdicional:Z

    .line 92
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$IntentBuilder_;->lisAdicional(Z)Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$IntentBuilder_;

    move-result-object v0

    .line 93
    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$IntentBuilder_;->simulacaoRespostaVO(Lcom/itau/empresas/feature/credito/lis/model/LisSimulacaoRespostaVO;)Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$IntentBuilder_;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/lis/activity/LisDataSimulacaoActivity;->ofertaContratacaoLisVo:Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;

    .line 94
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$IntentBuilder_;->ofertaContratacaoLisVo(Lcom/itau/empresas/feature/credito/lis/model/OfertaContratacaoLisVo;)Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$IntentBuilder_;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/lis/activity/LisSimulacaoActivity_$IntentBuilder_;->start()Lorg/androidannotations/api/builder/PostActivityStarter;

    .line 96
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 100
    const-string v0, "creditoRotativoChequeEspecialSimular"

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.class Lcom/itau/empresas/feature/login/controller/PreLoginController$1;
.super Ljava/lang/Object;
.source "PreLoginController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/login/controller/PreLoginController;->buscaOperador(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/login/controller/PreLoginController;

.field final synthetic val$agencia:Ljava/lang/String;

.field final synthetic val$codigoOperador:Ljava/lang/String;

.field final synthetic val$conta:Ljava/lang/String;

.field final synthetic val$dac:Ljava/lang/String;

.field final synthetic val$documento:Ljava/lang/String;

.field final synthetic val$tipoDocumento:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/login/controller/PreLoginController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .param p1, "this$0"    # Lcom/itau/empresas/feature/login/controller/PreLoginController;

    .line 34
    iput-object p1, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController$1;->this$0:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    iput-object p2, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController$1;->val$tipoDocumento:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController$1;->val$agencia:Ljava/lang/String;

    iput-object p4, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController$1;->val$conta:Ljava/lang/String;

    iput-object p5, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController$1;->val$dac:Ljava/lang/String;

    iput-object p6, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController$1;->val$documento:Ljava/lang/String;

    iput-object p7, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController$1;->val$codigoOperador:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 11

    .line 38
    iget-object v7, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController$1;->val$tipoDocumento:Ljava/lang/String;

    .line 39
    .local v7, "tipo":Ljava/lang/String;
    if-eqz v7, :cond_8

    .line 40
    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v7

    .line 42
    :cond_8
    new-instance v0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController$1;->val$agencia:Ljava/lang/String;

    iget-object v2, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController$1;->val$conta:Ljava/lang/String;

    iget-object v3, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController$1;->val$dac:Ljava/lang/String;

    move-object v4, v7

    iget-object v5, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController$1;->val$documento:Ljava/lang/String;

    iget-object v6, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController$1;->val$codigoOperador:Ljava/lang/String;

    .line 43
    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1d

    const/4 v6, 0x0

    goto :goto_1f

    :cond_1d
    iget-object v6, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController$1;->val$codigoOperador:Ljava/lang/String;

    :goto_1f
    invoke-direct/range {v0 .. v6}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v8, v0

    .line 44
    .local v8, "usuario":Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController$1;->this$0:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/controller/PreLoginController;->api()Lcom/itau/empresas/feature/login/PreLoginAPI;

    move-result-object v0

    new-instance v1, Lcom/itau/empresas/feature/login/model/PreLoginVO;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 45
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-direct {v1, v8, v2}, Lcom/itau/empresas/feature/login/model/PreLoginVO;-><init>(Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;Z)V

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/login/PreLoginAPI;->buscaOperadoresAtivos(Lcom/itau/empresas/feature/login/model/PreLoginVO;)Ljava/util/List;

    move-result-object v9

    .line 46
    .local v9, "operadorVOs":Ljava/util/List;, "Ljava/util/List<Lcom/itau/empresas/api/model/OperadorVO;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/PreLoginController$1;->this$0:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    # getter for: Lcom/itau/empresas/feature/login/controller/PreLoginController;->listener:Lcom/itau/empresas/feature/login/strategy/PreLoginControllerListener;
    invoke-static {v0}, Lcom/itau/empresas/feature/login/controller/PreLoginController;->access$000(Lcom/itau/empresas/feature/login/controller/PreLoginController;)Lcom/itau/empresas/feature/login/strategy/PreLoginControllerListener;

    move-result-object v0

    invoke-interface {v0, v9}, Lcom/itau/empresas/feature/login/strategy/PreLoginControllerListener;->listaOperadores(Ljava/util/List;)V

    .line 47
    return-void
.end method

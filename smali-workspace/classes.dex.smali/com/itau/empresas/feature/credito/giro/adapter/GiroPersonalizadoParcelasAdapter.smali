.class public Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;
.super Landroid/widget/BaseAdapter;
.source "GiroPersonalizadoParcelasAdapter.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mListaParcelasEmprestimo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;>;"
        }
    .end annotation
.end field

.field private mQuantidadeMaximaMes:J

.field private mQuantidadeMinimaMes:J


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 25
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method private adicionarItemOutroPlano()V
    .registers 6

    .line 90
    const/4 v2, 0x0

    .line 92
    .local v2, "jaExisteItemOutroPlano":Z
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mListaParcelasEmprestimo:Ljava/util/List;

    if-eqz v0, :cond_47

    .line 94
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mListaParcelasEmprestimo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;

    .line 96
    .local v4, "parcelasEmprestimo":Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->getEnumValoresEmprestimo()Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    move-result-object v0

    if-eqz v0, :cond_2b

    .line 97
    invoke-virtual {v4}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->getEnumValoresEmprestimo()Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;->OUTRO_PLANO:Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    .line 98
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 100
    const/4 v2, 0x1

    .line 102
    .end local v4    # "parcelasEmprestimo":Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;
    :cond_2b
    goto :goto_b

    .line 104
    :cond_2c
    if-nez v2, :cond_47

    .line 106
    new-instance v3, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;

    invoke-direct {v3}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;-><init>()V

    .line 107
    .local v3, "parcelasEmprestimo":Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;
    iget-wide v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mQuantidadeMinimaMes:J

    invoke-virtual {v3, v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->setParcelaMinima(J)V

    .line 108
    iget-wide v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mQuantidadeMaximaMes:J

    invoke-virtual {v3, v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->setParcelaMaxima(J)V

    .line 109
    sget-object v0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;->OUTRO_PLANO:Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    .line 110
    invoke-virtual {v3, v0}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->setEnumValoresEmprestimo(Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;)V

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mListaParcelasEmprestimo:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    .end local v3    # "parcelasEmprestimo":Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;
    :cond_47
    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 2

    .line 160
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mListaParcelasEmprestimo:Ljava/util/List;

    if-eqz v0, :cond_b

    .line 162
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mListaParcelasEmprestimo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0

    .line 165
    :cond_b
    const/4 v0, 0x0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .registers 3
    .param p1, "position"    # I

    .line 170
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mListaParcelasEmprestimo:Ljava/util/List;

    if-eqz v0, :cond_b

    .line 172
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mListaParcelasEmprestimo:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 175
    :cond_b
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .param p1, "position"    # I

    .line 179
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .registers 4
    .param p1, "position"    # I

    .line 265
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mListaParcelasEmprestimo:Ljava/util/List;

    if-eqz v0, :cond_3e

    .line 267
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mListaParcelasEmprestimo:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->getEnumValoresEmprestimo()Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    move-result-object v0

    if-eqz v0, :cond_3c

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mListaParcelasEmprestimo:Ljava/util/List;

    .line 268
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->getEnumValoresEmprestimo()Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;->CARREGADO_DA_API:Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    .line 269
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3a

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mListaParcelasEmprestimo:Ljava/util/List;

    .line 270
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->getEnumValoresEmprestimo()Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;->CARREGADO_MANUALMENTE:Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    .line 271
    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3c

    .line 273
    :cond_3a
    const/4 v0, 0x0

    return v0

    .line 276
    :cond_3c
    const/4 v0, 0x1

    return v0

    .line 280
    :cond_3e
    const/4 v0, 0x0

    return v0
.end method

.method public getItems()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;>;"
        }
    .end annotation

    .line 78
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mListaParcelasEmprestimo:Ljava/util/List;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 16
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .line 185
    move-object v4, p2

    .line 186
    .local v4, "view":Landroid/view/View;
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->getItemViewType(I)I

    move-result v5

    .line 188
    .local v5, "viewType":I
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mListaParcelasEmprestimo:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;

    .line 191
    .local v7, "valoresEmprestimo":Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;
    if-nez v4, :cond_2e

    .line 193
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/view/LayoutInflater;

    .line 195
    .local v6, "inflater":Landroid/view/LayoutInflater;
    if-nez v5, :cond_26

    .line 197
    const v0, 0x7f03010a

    const/4 v1, 0x0

    invoke-virtual {v6, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    goto :goto_2e

    .line 200
    :cond_26
    const v0, 0x7f03010b

    const/4 v1, 0x0

    invoke-virtual {v6, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 204
    .end local v6    # "inflater":Landroid/view/LayoutInflater;
    :cond_2e
    :goto_2e
    if-nez v5, :cond_63

    .line 206
    const v0, 0x7f0e02d5

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/widget/TextView;

    .line 210
    .local v9, "textoValor":Landroid/widget/TextView;
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v9, v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 212
    const-string v0, "%sx %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    .line 213
    invoke-virtual {v7}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->getQuantidade()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 214
    invoke-virtual {v7}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->getValor()F

    move-result v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(FZ)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 212
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 216
    .local v10, "textoValorFormatado":Ljava/lang/String;
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    .end local v9    # "textoValor":Landroid/widget/TextView;
    .end local v10    # "textoValorFormatado":Ljava/lang/String;
    goto :goto_c3

    .line 219
    :cond_63
    const v0, 0x7f0e01b7

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/widget/TextView;

    .line 220
    .line 221
    .local v9, "textoAuxiliar":Landroid/widget/TextView;
    const v0, 0x7f0e01ae

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/widget/RelativeLayout;

    .line 222
    .local v10, "rlContainer":Landroid/widget/RelativeLayout;
    const v0, 0x7f0e0555

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Landroid/widget/TextView;

    .line 224
    .local v11, "textoOutroValor":Landroid/widget/TextView;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0043

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 226
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {v11, v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 228
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mContext:Landroid/content/Context;

    const v1, 0x7f070637

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-wide v2, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mQuantidadeMinimaMes:J

    .line 229
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-wide v2, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mQuantidadeMaximaMes:J

    .line 230
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 228
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 232
    .local v8, "auxiliar":Ljava/lang/String;
    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 234
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter$1;-><init>(Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;)V

    invoke-virtual {v10, v0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 244
    .end local v8    # "auxiliar":Ljava/lang/String;
    .end local v9    # "textoAuxiliar":Landroid/widget/TextView;
    .end local v10    # "rlContainer":Landroid/widget/RelativeLayout;
    .end local v11    # "textoOutroValor":Landroid/widget/TextView;
    :goto_c3
    return-object v4
.end method

.method public getViewTypeCount()I
    .registers 2

    .line 260
    const/4 v0, 0x2

    return v0
.end method

.method public setContext(Landroid/content/Context;)V
    .registers 2
    .param p1, "context"    # Landroid/content/Context;

    .line 45
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mContext:Landroid/content/Context;

    .line 46
    return-void
.end method

.method public setData(Ljava/util/List;JJ)V
    .registers 9
    .param p1, "parcelasEmprestimoLista"    # Ljava/util/List;
    .param p2, "quantidadeMinimaMes"    # J
    .param p4, "quantidadeMaximaMes"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;>;JJ)V"
        }
    .end annotation

    .line 53
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mListaParcelasEmprestimo:Ljava/util/List;

    .line 54
    iput-wide p2, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mQuantidadeMinimaMes:J

    .line 55
    iput-wide p4, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mQuantidadeMaximaMes:J

    .line 57
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mListaParcelasEmprestimo:Ljava/util/List;

    if-nez v0, :cond_11

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mListaParcelasEmprestimo:Ljava/util/List;

    .line 63
    :cond_11
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->mListaParcelasEmprestimo:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_17
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_30

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;

    .line 65
    .local v2, "parcelasEmprestimo":Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;
    invoke-virtual {v2}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->getEnumValoresEmprestimo()Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    move-result-object v0

    if-nez v0, :cond_2f

    .line 67
    sget-object v0, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;->CARREGADO_DA_API:Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;

    .line 68
    invoke-virtual {v2, v0}, Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;->setEnumValoresEmprestimo(Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO$EnumValoresEmprestimo;)V

    .line 71
    .end local v2    # "parcelasEmprestimo":Lcom/itau/empresas/feature/credito/giro/model/ParcelasEmprestimoVO;
    :cond_2f
    goto :goto_17

    .line 73
    :cond_30
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/adapter/GiroPersonalizadoParcelasAdapter;->adicionarItemOutroPlano()V

    .line 74
    return-void
.end method

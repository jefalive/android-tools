.class public Lbr/com/itau/sdk/android/core/DefaultEventedErrorHandler;
.super Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;
.source "SourceFile"


# static fields
.field private static final a:[B

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x16

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/DefaultEventedErrorHandler;->a:[B

    const/16 v0, 0x69

    sput v0, Lbr/com/itau/sdk/android/core/DefaultEventedErrorHandler;->b:I

    return-void

    :array_e
    .array-data 1
        0x59t
        -0x37t
        0x41t
        -0x37t
        -0x24t
        0x9t
        0xct
        0x58t
        -0x45t
        0x16t
        0x4at
        -0x3at
        -0x3t
        0xbt
        0x1t
        0x10t
        0xet
        0xft
        0xbt
        -0x7dt
        0xdt
        0x7dt
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 9
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;-><init>()V

    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    const/4 v5, -0x1

    mul-int/lit8 p1, p1, 0x2

    add-int/lit8 p1, p1, 0x13

    mul-int/lit8 p0, p0, 0x2

    rsub-int/lit8 p0, p0, 0x4

    new-instance v0, Ljava/lang/String;

    sget-object v4, Lbr/com/itau/sdk/android/core/DefaultEventedErrorHandler;->a:[B

    mul-int/lit8 p2, p2, 0x3

    add-int/lit8 p2, p2, 0x45

    new-array v1, p1, [B

    add-int/lit8 p1, p1, -0x1

    if-nez v4, :cond_1f

    move v2, p0

    move v3, p2

    :goto_19
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p0, p0, 0x1

    add-int/lit8 p2, v2, 0x9

    :cond_1f
    add-int/lit8 v5, v5, 0x1

    int-to-byte v2, p2

    aput-byte v2, v1, v5

    if-ne v5, p1, :cond_2b

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_2b
    move v2, p2

    aget-byte v3, v4, p0

    goto :goto_19
.end method


# virtual methods
.method public handleConversionException(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z
    .registers 4

    .line 19
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;

    invoke-direct {v1, p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Conversion;-><init>(Lbr/com/itau/sdk/android/core/exception/BackendException;)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 20
    const/4 v0, 0x0

    return v0
.end method

.method public handleError(Lbr/com/itau/sdk/android/core/exception/BackendException;)Ljava/lang/Throwable;
    .registers 4

    .line 13
    sget-object v0, Lbr/com/itau/sdk/android/core/DefaultEventedErrorHandler;->a:[B

    const/16 v1, 0xe

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0, v0, v0}, Lbr/com/itau/sdk/android/core/DefaultEventedErrorHandler;->a(III)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/Exception;Ljava/lang/String;)V

    .line 14
    invoke-super {p0, p1}, Lbr/com/itau/sdk/android/core/endpoint/DefaultErrorHandler;->handleError(Lbr/com/itau/sdk/android/core/exception/BackendException;)Ljava/lang/Throwable;

    move-result-object v0

    return-object v0
.end method

.method public handleHttpException(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z
    .registers 4

    .line 25
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$HTTP;

    invoke-direct {v1, p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$HTTP;-><init>(Lbr/com/itau/sdk/android/core/exception/BackendException;)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 26
    const/4 v0, 0x0

    return v0
.end method

.method public handleNetworkException(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z
    .registers 4

    .line 31
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Network;

    invoke-direct {v1, p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Network;-><init>(Lbr/com/itau/sdk/android/core/exception/BackendException;)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 32
    const/4 v0, 0x0

    return v0
.end method

.method public handleRouter(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z
    .registers 4

    .line 37
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Router;

    invoke-direct {v1, p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Router;-><init>(Lbr/com/itau/sdk/android/core/exception/BackendException;)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 38
    const/4 v0, 0x0

    return v0
.end method

.method public handleSession(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z
    .registers 4

    .line 55
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Session;

    invoke-direct {v1, p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Session;-><init>(Lbr/com/itau/sdk/android/core/exception/BackendException;)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 56
    const/4 v0, 0x0

    return v0
.end method

.method public handleSimultaneousSession(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z
    .registers 4

    .line 61
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$SimultaneousSession;

    invoke-direct {v1, p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$SimultaneousSession;-><init>(Lbr/com/itau/sdk/android/core/exception/BackendException;)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 62
    const/4 v0, 0x0

    return v0
.end method

.method public handleToken(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z
    .registers 4

    .line 49
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Token;

    invoke-direct {v1, p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Token;-><init>(Lbr/com/itau/sdk/android/core/exception/BackendException;)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 50
    const/4 v0, 0x0

    return v0
.end method

.method public handleUnexpected(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z
    .registers 4

    .line 43
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;

    invoke-direct {v1, p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$Unexpected;-><init>(Lbr/com/itau/sdk/android/core/exception/BackendException;)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 44
    const/4 v0, 0x0

    return v0
.end method

.method public handleVersionControl(Lbr/com/itau/sdk/android/core/exception/BackendException;)Z
    .registers 4

    .line 67
    invoke-static {}, Lde/greenrobot/event/EventBus;->getDefault()Lde/greenrobot/event/EventBus;

    move-result-object v0

    new-instance v1, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$VersionControl;

    invoke-direct {v1, p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$VersionControl;-><init>(Lbr/com/itau/sdk/android/core/exception/BackendException;)V

    invoke-virtual {v0, v1}, Lde/greenrobot/event/EventBus;->post(Ljava/lang/Object;)V

    .line 68
    const/4 v0, 0x0

    return v0
.end method

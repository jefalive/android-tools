.class public abstract Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;
.super Landroid/widget/RelativeLayout;
.source "MarkerView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layoutResource"    # I

    .line 29
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 30
    invoke-direct {p0, p2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;->setupLayoutResource(I)V

    .line 31
    return-void
.end method

.method private setupLayoutResource(I)V
    .registers 7
    .param p1, "layoutResource"    # I

    .line 42
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 45
    .local v4, "inflated":Landroid/view/View;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 47
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 48
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 47
    invoke-virtual {v4, v0, v1}, Landroid/view/View;->measure(II)V

    .line 52
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v4, v2, v3, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 53
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;FF)V
    .registers 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "posx"    # F
    .param p3, "posy"    # F

    .line 68
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;->getXOffset()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr p2, v0

    .line 69
    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;->getYOffset()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr p3, v0

    .line 73
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 74
    invoke-virtual {p0, p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/MarkerView;->draw(Landroid/graphics/Canvas;)V

    .line 75
    neg-float v0, p2

    neg-float v1, p3

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 76
    return-void
.end method

.method public abstract getXOffset()I
.end method

.method public abstract getYOffset()I
.end method

.method public abstract refreshContent(Lbr/com/itau/widgets/graphicalfriedcake/charts/data/Entry;Lbr/com/itau/widgets/graphicalfriedcake/charts/highlight/Highlight;)V
.end method

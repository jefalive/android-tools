.class public final Lbr/com/itau/sdk/android/core/ExtraConfiguration;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final f:[B

.field private static g:I


# instance fields
.field private final a:Lbr/com/itau/sdk/android/core/BackendClient$Builder;

.field private final b:Ljava/lang/String;

.field private c:Lbr/com/itau/sdk/android/core/RouterEnvironment;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x187

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/16 v0, 0xa3

    sput v0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->g:I

    return-void

    :array_e
    .array-data 1
        0x3ct
        0x50t
        0x19t
        -0x7et
        0x15t
        0xct
        -0x5t
        0xat
        0x4t
        -0x4t
        -0x7t
        0x0t
        -0x43t
        0x36t
        -0x2t
        -0x5t
        0x3t
        -0xct
        0x8t
        0x4t
        -0x6t
        -0x36t
        0x34t
        0x13t
        -0x2t
        0xdt
        0x1t
        -0x3t
        0x4t
        -0x38t
        -0xat
        0x1t
        0x3bt
        0x6t
        0x7t
        -0xet
        0xet
        -0x3t
        -0x8t
        0x10t
        -0x45t
        0x3ct
        0xct
        -0x12t
        0x15t
        -0x46t
        0x36t
        0xdt
        -0x1t
        -0x3et
        0x35t
        0x11t
        -0x42t
        0x44t
        -0x2t
        0x7t
        0x0t
        -0xet
        0xet
        -0x44t
        0x35t
        0x10t
        0x1t
        -0x40t
        0x44t
        -0x2t
        0x7t
        0x0t
        -0xet
        0xet
        -0x42t
        0x42t
        0x6t
        -0x12t
        0xbt
        -0x2t
        -0x5t
        0xdt
        0x1t
        -0x3t
        0x4t
        -0x38t
        -0xat
        0x1t
        0x42t
        -0x6t
        0x4t
        0x4t
        0x6t
        -0x4t
        -0x40t
        0x3ct
        0xct
        -0x12t
        0x15t
        -0x46t
        0x36t
        0xdt
        -0x1t
        -0x3et
        0x35t
        0x11t
        -0x42t
        0x44t
        -0x2t
        0x7t
        0x0t
        -0xet
        0xet
        -0x44t
        0x35t
        0x10t
        0x1t
        -0x40t
        0x44t
        -0x2t
        0x7t
        0x0t
        -0xet
        0xet
        -0x42t
        0x42t
        0x6t
        -0x12t
        0xbt
        -0x2t
        -0x5t
        0xdt
        0x1t
        -0x3t
        0x4t
        -0x38t
        -0xat
        0x1t
        0x34t
        0x0t
        0xet
        -0x2t
        0x2t
        -0x2t
        0x6t
        -0x8t
        -0x36t
        0x3ct
        0xct
        -0x12t
        0x15t
        -0x46t
        0x36t
        0xdt
        -0x1t
        -0x3et
        0x35t
        0x11t
        -0x42t
        0x48t
        -0x2t
        -0x4t
        -0x8t
        0x10t
        -0x44t
        0x49t
        -0x15t
        0x10t
        -0x3dt
        -0x2t
        0x1ft
        -0x11t
        0x2t
        0x8t
        0x4t
        -0x2t
        -0x4t
        -0x2t
        0x4t
        0x2t
        -0x44t
        0x4ft
        -0x8t
        0x1t
        0x0t
        0x10t
        -0x52t
        0x42t
        -0x40t
        0x51t
        -0xet
        0x7t
        -0x1t
        0xdt
        0x1t
        -0x3t
        0x4t
        -0x38t
        -0xat
        0x1t
        0x3ft
        0x3t
        -0xct
        0x8t
        0x4t
        -0x6t
        -0x36t
        0x3ct
        0xct
        -0x12t
        0x15t
        -0x46t
        0x36t
        0xdt
        -0x1t
        -0x3et
        0x35t
        0x11t
        -0x42t
        0x44t
        -0x2t
        0x7t
        0x0t
        -0xet
        0xet
        -0x44t
        0x35t
        0x10t
        0x1t
        -0x40t
        0x44t
        -0x2t
        0x7t
        0x0t
        -0xet
        0xet
        -0x42t
        0x3ft
        0x3t
        -0xct
        0x8t
        0x4t
        -0x6t
        0xdt
        0x1t
        -0x3t
        0x4t
        -0x38t
        -0xat
        0x1t
        0x42t
        -0x6t
        0x4t
        0x4t
        0x6t
        -0x4t
        -0x40t
        0x3ct
        0xct
        -0x12t
        0x15t
        -0x46t
        0x36t
        0xdt
        -0x1t
        -0x3et
        0x35t
        0x11t
        -0x42t
        0x44t
        -0x2t
        0x7t
        0x0t
        -0xet
        0xet
        -0x44t
        0x35t
        0x10t
        0x1t
        -0x40t
        0x44t
        -0x2t
        0x7t
        0x0t
        -0xet
        0xet
        -0x42t
        0x3ft
        0x3t
        -0xct
        0x8t
        0x4t
        -0x6t
        0xdt
        0x1t
        -0x3t
        0x4t
        -0x38t
        -0xat
        0x1t
        0x3bt
        0x6t
        0x7t
        -0xet
        0xet
        -0x3t
        -0x8t
        0x10t
        -0x45t
        0x3ct
        0xct
        -0x12t
        0x15t
        -0x46t
        0x36t
        0xdt
        -0x1t
        -0x3et
        0x35t
        0x11t
        -0x42t
        0x44t
        -0x2t
        0x7t
        0x0t
        -0xet
        0xet
        -0x44t
        0x35t
        0x10t
        0x1t
        -0x40t
        0x44t
        -0x2t
        0x7t
        0x0t
        -0xet
        0xet
        -0x42t
        0x3ft
        0x3t
        -0xct
        0x8t
        0x4t
        -0x6t
        0xdt
        0x1t
        -0x3t
        0x4t
        -0x38t
        -0xat
        0x1t
        0x3ft
        0x3t
        -0xct
        0x8t
        0x4t
        -0x6t
        -0x36t
        0x3ct
        0xct
        -0x12t
        0x15t
        -0x46t
        0x36t
        0xdt
        -0x1t
        -0x3et
        0x35t
        0x11t
        -0x42t
        0x44t
        -0x2t
        0x7t
        0x0t
        -0xet
        0xet
        -0x44t
        0x35t
        0x10t
        0x1t
        -0x40t
        0x44t
        -0x2t
        0x7t
        0x0t
        -0xet
        0xet
        -0x42t
        0x42t
        0x6t
        -0x12t
        0xbt
        -0x2t
        -0x5t
    .end array-data
.end method

.method constructor <init>(Lbr/com/itau/sdk/android/core/BackendClient$Builder;)V
    .registers 6

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    sget-object v0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/4 v1, 0x2

    aget-byte v0, v0, v1

    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v1, v0, 0x5

    sget v2, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->g:I

    or-int/lit8 v2, v2, 0xc

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->a(III)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->b:Ljava/lang/String;

    .line 15
    sget-object v0, Lbr/com/itau/sdk/android/core/RouterEnvironment;->MOBILE:Lbr/com/itau/sdk/android/core/RouterEnvironment;

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->c:Lbr/com/itau/sdk/android/core/RouterEnvironment;

    .line 17
    sget-object v0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/16 v1, 0xb

    aget-byte v0, v0, v1

    or-int/lit8 v1, v0, 0x2b

    const/16 v2, 0x145

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->a(III)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->e:Ljava/lang/String;

    .line 20
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->a:Lbr/com/itau/sdk/android/core/BackendClient$Builder;

    .line 21
    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    const/4 v4, 0x0

    rsub-int p2, p2, 0x154

    rsub-int/lit8 p1, p1, 0x35

    new-instance v0, Ljava/lang/String;

    rsub-int/lit8 p0, p0, 0x6d

    sget-object v5, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    new-array v1, p1, [B

    add-int/lit8 p1, p1, -0x1

    if-nez v5, :cond_16

    move v2, p2

    move v3, p1

    :goto_13
    add-int/2addr v2, v3

    add-int/lit8 p0, v2, -0x1

    :cond_16
    int-to-byte v2, p0

    aput-byte v2, v1, v4

    move v2, v4

    add-int/lit8 v4, v4, 0x1

    add-int/lit8 p2, p2, 0x1

    if-ne v2, p1, :cond_25

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_25
    move v2, p0

    aget-byte v3, v5, p2

    goto :goto_13
.end method


# virtual methods
.method public builder()Lbr/com/itau/sdk/android/core/BackendClient$Builder;
    .registers 2

    .line 94
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->a:Lbr/com/itau/sdk/android/core/BackendClient$Builder;

    return-object v0
.end method

.method public getMiddlewareCaller()Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareCaller;
    .registers 2

    .line 132
    new-instance v0, Lbr/com/itau/sdk/android/core/endpoint/f;

    invoke-direct {v0}, Lbr/com/itau/sdk/android/core/endpoint/f;-><init>()V

    return-object v0
.end method

.method public getMiddlewareUrl()Ljava/lang/String;
    .registers 2

    .line 120
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPublicUrl()Ljava/lang/String;
    .registers 5

    .line 109
    sget-object v0, Lbr/com/itau/sdk/android/core/g;->a:[I

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->c:Lbr/com/itau/sdk/android/core/RouterEnvironment;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/RouterEnvironment;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_4c

    goto :goto_34

    .line 111
    :sswitch_e
    sget-object v0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/4 v1, 0x6

    aget-byte v0, v0, v1

    neg-int v0, v0

    sget-object v1, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/16 v2, 0xb

    aget-byte v1, v1, v2

    or-int/lit16 v2, v1, 0x13c

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->a(III)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 113
    :sswitch_21
    sget-object v0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/4 v1, 0x6

    aget-byte v0, v0, v1

    neg-int v0, v0

    sget-object v1, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/16 v2, 0x8a

    aget-byte v1, v1, v2

    const/16 v2, 0x108

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->a(III)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 115
    :goto_34
    sget-object v0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/4 v1, 0x6

    aget-byte v0, v0, v1

    neg-int v0, v0

    sget-object v1, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/16 v2, 0x8a

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/16 v3, 0xb

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->a(III)Ljava/lang/String;

    move-result-object v0

    return-object v0

    nop

    :sswitch_data_4c
    .sparse-switch
        0x1 -> :sswitch_e
        0x2 -> :sswitch_21
    .end sparse-switch
.end method

.method public getRouterUrl()Ljava/lang/String;
    .registers 5

    .line 98
    sget-object v0, Lbr/com/itau/sdk/android/core/g;->a:[I

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->c:Lbr/com/itau/sdk/android/core/RouterEnvironment;

    invoke-virtual {v1}, Lbr/com/itau/sdk/android/core/RouterEnvironment;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_4c

    goto :goto_38

    .line 100
    :sswitch_e
    sget-object v0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/4 v1, 0x6

    aget-byte v0, v0, v1

    neg-int v0, v0

    sget-object v1, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/16 v2, 0xb

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/16 v3, 0x16

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->a(III)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 102
    :sswitch_25
    sget-object v0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/4 v1, 0x6

    aget-byte v0, v0, v1

    neg-int v0, v0

    sget-object v1, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/16 v2, 0x8a

    aget-byte v1, v1, v2

    or-int/lit8 v2, v1, 0x64

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->a(III)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 104
    :goto_38
    sget-object v0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/4 v1, 0x6

    aget-byte v0, v0, v1

    neg-int v0, v0

    sget-object v1, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/16 v2, 0x8a

    aget-byte v1, v1, v2

    const/16 v2, 0x98

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->a(III)Ljava/lang/String;

    move-result-object v0

    return-object v0

    nop

    :sswitch_data_4c
    .sparse-switch
        0x1 -> :sswitch_e
        0x2 -> :sswitch_25
    .end sparse-switch
.end method

.method public getUniversalUrl()Ljava/lang/String;
    .registers 2

    .line 124
    const/4 v0, 0x0

    return-object v0
.end method

.method public getUsabilidadeUrl()Ljava/lang/String;
    .registers 5

    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/4 v2, 0x6

    aget-byte v1, v1, v2

    neg-int v1, v1

    sget-object v2, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/16 v3, 0x19

    aget-byte v2, v2, v3

    const/16 v3, 0xd6

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserAgent()Ljava/lang/String;
    .registers 2

    .line 140
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->d:Ljava/lang/String;

    return-object v0
.end method

.method public isUseCryptoPayload()Ljava/lang/Boolean;
    .registers 2

    .line 128
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public withLightUrl(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/ExtraConfiguration;
    .registers 6
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 72
    if-eqz p1, :cond_1a

    invoke-static {p1}, Landroid/webkit/URLUtil;->isValidUrl(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 73
    new-instance v0, Ljava/lang/RuntimeException;

    sget v1, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->g:I

    and-int/lit8 v1, v1, 0x78

    or-int/lit8 v2, v1, 0x8

    const/16 v3, 0x151

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_1a
    return-object p0
.end method

.method public withMiddlewareCaller(Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareCaller;)Lbr/com/itau/sdk/android/core/ExtraConfiguration;
    .registers 2

    .line 53
    return-object p0
.end method

.method public withMiddlewareUrl(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/ExtraConfiguration;
    .registers 7

    .line 24
    const-string v0, ""

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 25
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/4 v2, 0x2

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v2, v1, 0x5

    sget v3, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->g:I

    or-int/lit8 v3, v3, 0xc

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 26
    :cond_21
    return-object p0
.end method

.method public withPublicUrl(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/ExtraConfiguration;
    .registers 6

    .line 78
    if-eqz p1, :cond_1a

    invoke-static {p1}, Landroid/webkit/URLUtil;->isValidUrl(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 79
    new-instance v0, Ljava/lang/RuntimeException;

    sget v1, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->g:I

    and-int/lit8 v1, v1, 0x78

    or-int/lit8 v2, v1, 0x8

    const/16 v3, 0x151

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_1a
    return-object p0
.end method

.method public withRouterUrl(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/ExtraConfiguration;
    .registers 6

    .line 57
    if-eqz p1, :cond_1a

    invoke-static {p1}, Landroid/webkit/URLUtil;->isValidUrl(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 58
    new-instance v0, Ljava/lang/RuntimeException;

    sget v1, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->g:I

    and-int/lit8 v1, v1, 0x78

    or-int/lit8 v2, v1, 0x8

    const/16 v3, 0x151

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_1a
    return-object p0
.end method

.method public withRouterUrlForRelease(Lbr/com/itau/sdk/android/core/RouterEnvironment;)Lbr/com/itau/sdk/android/core/ExtraConfiguration;
    .registers 2

    .line 84
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->c:Lbr/com/itau/sdk/android/core/RouterEnvironment;

    .line 85
    return-object p0
.end method

.method public withUniversalUrl(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/ExtraConfiguration;
    .registers 7

    .line 30
    const-string v0, ""

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 31
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/4 v2, 0x2

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v2, v1, 0x5

    sget v3, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->g:I

    or-int/lit8 v3, v3, 0xc

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 32
    :cond_21
    return-object p0
.end method

.method public withUsabilidadePage(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/ExtraConfiguration;
    .registers 7

    .line 46
    const-string v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/4 v2, 0x2

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v2, v1, 0x5

    sget v3, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->g:I

    or-int/lit8 v3, v3, 0xc

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_1f
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->e:Ljava/lang/String;

    .line 49
    return-object p0
.end method

.method public withUsabilidadeUrl(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/ExtraConfiguration;
    .registers 7

    .line 40
    const-string v0, ""

    iget-object v1, p0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 41
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->f:[B

    const/4 v2, 0x2

    aget-byte v1, v1, v2

    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v2, v1, 0x5

    sget v3, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->g:I

    or-int/lit8 v3, v3, 0xc

    invoke-static {v1, v2, v3}, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->a(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_21
    return-object p0
.end method

.method public withUseCryptoPayload(Z)Lbr/com/itau/sdk/android/core/ExtraConfiguration;
    .registers 2

    .line 36
    return-object p0
.end method

.method public withUserAgent(Ljava/lang/String;)Lbr/com/itau/sdk/android/core/ExtraConfiguration;
    .registers 2

    .line 89
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/ExtraConfiguration;->d:Ljava/lang/String;

    .line 90
    return-object p0
.end method

.class public Lcom/itau/empresas/ui/dialog/RedesignDialog;
.super Lcom/itau/empresas/ui/dialog/BaseDialog;
.source "RedesignDialog.java"


# instance fields
.field private cancelarclickListner:Landroid/view/View$OnClickListener;

.field private continuarClickListner:Landroid/view/View$OnClickListener;

.field private descricao:Ljava/lang/String;

.field private fecharClickListner:Landroid/view/View$OnClickListener;

.field textoDescricao:Landroid/widget/TextView;

.field textoTitulo:Landroid/widget/TextView;

.field private titulo:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 3

    .line 33
    invoke-direct {p0}, Lcom/itau/empresas/ui/dialog/BaseDialog;-><init>()V

    .line 34
    const v0, 0x7f090146

    const v1, 0x7f090146

    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/ui/dialog/RedesignDialog;->setStyle(II)V

    .line 35
    return-void
.end method


# virtual methods
.method afterViews()V
    .registers 3

    .line 39
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog;->textoTitulo:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog;->titulo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog;->textoDescricao:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog;->descricao:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    return-void
.end method

.method botaoCancelar(Landroid/view/View;)V
    .registers 3
    .param p1, "v"    # Landroid/view/View;

    .line 55
    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/RedesignDialog;->dismiss()V

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog;->cancelarclickListner:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_c

    .line 57
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog;->cancelarclickListner:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 59
    :cond_c
    return-void
.end method

.method botaoContinuar(Landroid/view/View;)V
    .registers 3
    .param p1, "v"    # Landroid/view/View;

    .line 63
    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/RedesignDialog;->dismiss()V

    .line 65
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog;->continuarClickListner:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_c

    .line 66
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog;->continuarClickListner:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 69
    :cond_c
    return-void
.end method

.method botaoFechar(Landroid/view/View;)V
    .registers 3
    .param p1, "v"    # Landroid/view/View;

    .line 45
    invoke-virtual {p0}, Lcom/itau/empresas/ui/dialog/RedesignDialog;->dismiss()V

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog;->fecharClickListner:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_c

    .line 48
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog;->fecharClickListner:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 51
    :cond_c
    return-void
.end method

.method public setContinuarClickListner(Landroid/view/View$OnClickListener;)Lcom/itau/empresas/ui/dialog/RedesignDialog;
    .registers 2
    .param p1, "continuarClickListner"    # Landroid/view/View$OnClickListener;

    .line 82
    iput-object p1, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog;->continuarClickListner:Landroid/view/View$OnClickListener;

    .line 83
    return-object p0
.end method

.method public setDescricao(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/RedesignDialog;
    .registers 2
    .param p1, "descricao"    # Ljava/lang/String;

    .line 91
    iput-object p1, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog;->descricao:Ljava/lang/String;

    .line 93
    return-object p0
.end method

.method public setTitulo(Ljava/lang/String;)Lcom/itau/empresas/ui/dialog/RedesignDialog;
    .registers 2
    .param p1, "titulo"    # Ljava/lang/String;

    .line 101
    iput-object p1, p0, Lcom/itau/empresas/ui/dialog/RedesignDialog;->titulo:Ljava/lang/String;

    .line 103
    return-object p0
.end method

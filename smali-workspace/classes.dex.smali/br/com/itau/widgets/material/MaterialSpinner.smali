.class public Lbr/com/itau/widgets/material/MaterialSpinner;
.super Landroid/widget/Spinner;
.source "MaterialSpinner.java"

# interfaces
.implements Lcom/nineoldandroids/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;
    }
.end annotation


# static fields
.field public static final DEFAULT_ARROW_WIDTH_DP:I = 0xc

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private alignLabels:Z

.field private arrowColor:I

.field private arrowColorSelected:I

.field private arrowPaddingRight:I

.field private arrowSize:F

.field private baseAlpha:I

.field private baseColor:I

.field private currentNbErrorLines:F

.field private disabledColor:I

.field private error:Ljava/lang/CharSequence;

.field private errorAnimationReverse:Z

.field private errorColor:I

.field private errorLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

.field private errorLabelPosX:I

.field private errorLabelSpacing:I

.field private extraPaddingBottom:I

.field private extraPaddingTop:I

.field private floatingLabelAlwaysHighlighted:Z

.field private floatingLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

.field private floatingLabelBottomSpacing:I

.field private floatingLabelColor:I

.field private floatingLabelInsideSpacing:I

.field private floatingLabelPercent:F

.field private floatingLabelText:Ljava/lang/CharSequence;

.field private floatingLabelTextSize:F

.field private floatingLabelTopSpacing:I

.field private floatingLabelVisible:Z

.field private hideUnderline:Z

.field private highlightColor:I

.field private highlightUnderlineColor:I

.field private hint:Ljava/lang/CharSequence;

.field private hintTextColor:I

.field private hintTextSize:F

.field private innerPaddingBottom:I

.field private innerPaddingLeft:I

.field private innerPaddingRight:I

.field private innerPaddingTop:I

.field private isSelected:Z

.field private lastPosition:I

.field private minNbErrorLines:I

.field private multiline:Z

.field private paint:Landroid/graphics/Paint;

.field private paintLine:Landroid/graphics/Paint;

.field private rightLeftSpinnerPadding:I

.field private selectorPath:Landroid/graphics/Path;

.field private selectorPoints:[Landroid/graphics/Point;

.field private staticLayout:Landroid/text/StaticLayout;

.field private textPaint:Landroid/text/TextPaint;

.field private thickness:I

.field private thicknessError:I

.field private typeface:Landroid/graphics/Typeface;

.field private underlineAlwaysHighlighted:Z

.field private underlineBottomSpacing:I

.field private underlineTopSpacing:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 67
    const-class v0, Lbr/com/itau/widgets/material/MaterialSpinner;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbr/com/itau/widgets/material/MaterialSpinner;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 149
    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    .line 150
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 151
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 154
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 155
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/material/MaterialSpinner;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 157
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .line 160
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 161
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/material/MaterialSpinner;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 162
    return-void
.end method

.method static synthetic access$000(Lbr/com/itau/widgets/material/MaterialSpinner;)Ljava/lang/CharSequence;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 63
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->hint:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$100(Lbr/com/itau/widgets/material/MaterialSpinner;)Ljava/lang/CharSequence;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 63
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$1000(Lbr/com/itau/widgets/material/MaterialSpinner;)F
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 63
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->hintTextSize:F

    return v0
.end method

.method static synthetic access$1100(Lbr/com/itau/widgets/material/MaterialSpinner;)I
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 63
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->hintTextColor:I

    return v0
.end method

.method static synthetic access$1200(Lbr/com/itau/widgets/material/MaterialSpinner;)I
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 63
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->disabledColor:I

    return v0
.end method

.method static synthetic access$1300(Lbr/com/itau/widgets/material/MaterialSpinner;)Landroid/graphics/Typeface;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 63
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->typeface:Landroid/graphics/Typeface;

    return-object v0
.end method

.method static synthetic access$200(Lbr/com/itau/widgets/material/MaterialSpinner;)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 63
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelVisible:Z

    return v0
.end method

.method static synthetic access$300(Lbr/com/itau/widgets/material/MaterialSpinner;)V
    .registers 1
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 63
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->showFloatingLabel()V

    return-void
.end method

.method static synthetic access$402(Lbr/com/itau/widgets/material/MaterialSpinner;Z)Z
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialSpinner;
    .param p1, "x1"    # Z

    .line 63
    iput-boolean p1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->isSelected:Z

    return p1
.end method

.method static synthetic access$500(Lbr/com/itau/widgets/material/MaterialSpinner;)V
    .registers 1
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 63
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->hideFloatingLabel()V

    return-void
.end method

.method static synthetic access$600(Lbr/com/itau/widgets/material/MaterialSpinner;)I
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 63
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->arrowColorSelected:I

    return v0
.end method

.method static synthetic access$700(Lbr/com/itau/widgets/material/MaterialSpinner;)I
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 63
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->arrowColor:I

    return v0
.end method

.method static synthetic access$800(Lbr/com/itau/widgets/material/MaterialSpinner;)I
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 63
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->lastPosition:I

    return v0
.end method

.method static synthetic access$802(Lbr/com/itau/widgets/material/MaterialSpinner;I)I
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialSpinner;
    .param p1, "x1"    # I

    .line 63
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->lastPosition:I

    return p1
.end method

.method static synthetic access$900(Lbr/com/itau/widgets/material/MaterialSpinner;)Ljava/lang/CharSequence;
    .registers 2
    .param p0, "x0"    # Lbr/com/itau/widgets/material/MaterialSpinner;

    .line 63
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->error:Ljava/lang/CharSequence;

    return-object v0
.end method

.method private dpToPx(F)I
    .registers 5
    .param p1, "dp"    # F

    .line 365
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 366
    .local v1, "displayMetrics":Landroid/util/DisplayMetrics;
    const/4 v0, 0x1

    invoke-static {v0, p1, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    .line 367
    .local v2, "px":F
    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method private drawSelector(Landroid/graphics/Canvas;II)V
    .registers 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "posX"    # I
    .param p3, "posY"    # I

    .line 528
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->isSelected:Z

    if-eqz v0, :cond_c

    .line 529
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->arrowColorSelected:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_1c

    .line 531
    :cond_c
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->paint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_17

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->arrowColor:I

    goto :goto_19

    :cond_17
    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->disabledColor:I

    :goto_19
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 534
    :goto_1c
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->selectorPoints:[Landroid/graphics/Point;

    const/4 v1, 0x0

    aget-object v4, v0, v1

    .line 535
    .local v4, "point1":Landroid/graphics/Point;
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->selectorPoints:[Landroid/graphics/Point;

    const/4 v1, 0x1

    aget-object v5, v0, v1

    .line 536
    .local v5, "point2":Landroid/graphics/Point;
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->selectorPoints:[Landroid/graphics/Point;

    const/4 v1, 0x2

    aget-object v6, v0, v1

    .line 538
    .local v6, "point3":Landroid/graphics/Point;
    invoke-virtual {v4, p2, p3}, Landroid/graphics/Point;->set(II)V

    .line 539
    int-to-float v0, p2

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->arrowSize:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    invoke-virtual {v5, v0, p3}, Landroid/graphics/Point;->set(II)V

    .line 540
    int-to-float v0, p2

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->arrowSize:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v1, p3

    iget v2, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->arrowSize:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v6, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 543
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->selectorPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 544
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->selectorPath:Landroid/graphics/Path;

    iget v1, v4, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget v2, v4, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 545
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->selectorPath:Landroid/graphics/Path;

    iget v1, v5, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget v2, v5, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 546
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->selectorPath:Landroid/graphics/Path;

    iget v1, v6, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget v2, v6, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 547
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->selectorPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 548
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->selectorPath:Landroid/graphics/Path;

    iget-object v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 549
    return-void
.end method

.method private getCurrentNbErrorLines()F
    .registers 2

    .line 749
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->currentNbErrorLines:F

    return v0
.end method

.method private getErrorLabelPosX()I
    .registers 2

    .line 741
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorLabelPosX:I

    return v0
.end method

.method private getFloatingLabelPercent()F
    .registers 2

    .line 733
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelPercent:F

    return v0
.end method

.method private hideFloatingLabel()V
    .registers 2

    .line 324
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    if-eqz v0, :cond_c

    .line 325
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelVisible:Z

    .line 326
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->reverse()V

    .line 328
    :cond_c
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 174
    invoke-direct {p0, p1, p2}, Lbr/com/itau/widgets/material/MaterialSpinner;->initAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 175
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->initPaintObjects()V

    .line 176
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->initDimensions()V

    .line 177
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->initPadding()V

    .line 178
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->initFloatingLabelAnimator()V

    .line 179
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->initOnItemSelectedListener()V

    .line 180
    invoke-direct {p0, p1}, Lbr/com/itau/widgets/material/MaterialSpinner;->initAdapter(Landroid/content/Context;)V

    .line 183
    sget v0, Lbr/com/itau/widgets/material/R$drawable;->ms_transparent_background:I

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->setBackgroundResource(I)V

    .line 185
    return-void
.end method

.method private initAdapter(Landroid/content/Context;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;

    .line 291
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v0, 0x1090008

    invoke-direct {v1, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 292
    .local v1, "adapter":Landroid/widget/SpinnerAdapter;
    invoke-virtual {p0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 293
    return-void
.end method

.method private initAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 189
    const/4 v0, 0x2

    new-array v0, v0, [I

    sget v1, Lbr/com/itau/widgets/material/R$attr;->colorControlNormal:I

    const/4 v2, 0x0

    aput v1, v0, v2

    sget v1, Lbr/com/itau/widgets/material/R$attr;->colorAccent:I

    const/4 v2, 0x1

    aput v1, v0, v2

    invoke-virtual {p1, v0}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 190
    .local v3, "a":Landroid/content/res/TypedArray;
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    .line 191
    .local v4, "defaultBaseColor":I
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v5

    .line 192
    .local v5, "defaultHighlightColor":I
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/widgets/material/R$color;->normal_red:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 194
    .local v6, "defaultErrorColor":I
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 196
    sget-object v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v7

    .line 197
    .local v7, "array":Landroid/content/res/TypedArray;
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_baseColor:I

    invoke-virtual {v7, v0, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->baseColor:I

    .line 198
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_highlightColor:I

    invoke-virtual {v7, v0, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->highlightColor:I

    .line 199
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_errorColor:I

    invoke-virtual {v7, v0, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorColor:I

    .line 200
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_disabled_color:I

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lbr/com/itau/widgets/material/R$color;->light_gray:I

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v7, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->disabledColor:I

    .line 201
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_error:I

    invoke-virtual {v7, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->error:Ljava/lang/CharSequence;

    .line 202
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_hint:I

    invoke-virtual {v7, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->hint:Ljava/lang/CharSequence;

    .line 203
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_floatingLabelText:I

    invoke-virtual {v7, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelText:Ljava/lang/CharSequence;

    .line 204
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_floatingLabelColor:I

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->baseColor:I

    invoke-virtual {v7, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelColor:I

    .line 205
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_multiline:I

    const/4 v1, 0x1

    invoke-virtual {v7, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->multiline:Z

    .line 206
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_nbErrorLines:I

    const/4 v1, 0x1

    invoke-virtual {v7, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->minNbErrorLines:I

    .line 207
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_alignLabels:I

    const/4 v1, 0x1

    invoke-virtual {v7, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->alignLabels:Z

    .line 208
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_thickness:I

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->dpToPx(F)I

    move-result v1

    invoke-virtual {v7, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->thickness:I

    .line 209
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_thicknessError:I

    const/high16 v1, 0x40000000    # 2.0f

    invoke-direct {p0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->dpToPx(F)I

    move-result v1

    invoke-virtual {v7, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->thicknessError:I

    .line 210
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_arrowColor:I

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->baseColor:I

    invoke-virtual {v7, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->arrowColor:I

    .line 211
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_arrowColorSelected:I

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->baseColor:I

    invoke-virtual {v7, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->arrowColorSelected:I

    .line 212
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_arrowColorSelected:I

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->baseColor:I

    invoke-virtual {v7, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->arrowColorSelected:I

    .line 213
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_arrowSize:I

    const/high16 v1, 0x41400000    # 12.0f

    invoke-direct {p0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->dpToPx(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v7, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->arrowSize:F

    .line 214
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_hideUnderline:I

    const/4 v1, 0x0

    invoke-virtual {v7, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->hideUnderline:Z

    .line 215
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_underlineAlwaysHighlighted:I

    const/4 v1, 0x0

    invoke-virtual {v7, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->underlineAlwaysHighlighted:Z

    .line 216
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_floatingLabelAlwaysHighlighted:I

    const/4 v1, 0x0

    invoke-virtual {v7, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelAlwaysHighlighted:Z

    .line 217
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_floatingLabelTextSize:I

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/widgets/material/R$dimen;->ms_floating_label_text_size:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v7, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelTextSize:F

    .line 218
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_hintTextSize:I

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/widgets/material/R$dimen;->ms_hint_text_size:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v7, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->hintTextSize:F

    .line 219
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_hintTextColor:I

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->baseColor:I

    invoke-virtual {v7, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->hintTextColor:I

    .line 220
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_arrowPaddingRight:I

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lbr/com/itau/widgets/material/R$dimen;->ms_arrow_padding_right:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v7, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->arrowPaddingRight:I

    .line 221
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_highlightUnderlineColor:I

    const/4 v1, -0x1

    invoke-virtual {v7, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->highlightUnderlineColor:I

    .line 222
    sget v0, Lbr/com/itau/widgets/material/R$styleable;->MaterialSpinner_ms_typeface:I

    invoke-virtual {v7, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 223
    .local v8, "typefacePath":Ljava/lang/String;
    if-eqz v8, :cond_15a

    .line 224
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-static {v0, v8}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->typeface:Landroid/graphics/Typeface;

    .line 227
    :cond_15a
    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    .line 229
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelPercent:F

    .line 230
    const/4 v0, 0x0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorLabelPosX:I

    .line 231
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->isSelected:Z

    .line 232
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelVisible:Z

    .line 233
    const/4 v0, -0x1

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->lastPosition:I

    .line 234
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->minNbErrorLines:I

    int-to-float v0, v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->currentNbErrorLines:F

    .line 236
    return-void
.end method

.method private initDimensions()V
    .registers 3

    .line 280
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/widgets/material/R$dimen;->ms_underline_top_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->underlineTopSpacing:I

    .line 281
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/widgets/material/R$dimen;->ms_underline_bottom_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->underlineBottomSpacing:I

    .line 282
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/widgets/material/R$dimen;->ms_floating_label_top_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelTopSpacing:I

    .line 283
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/widgets/material/R$dimen;->ms_floating_label_bottom_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelBottomSpacing:I

    .line 284
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->alignLabels:Z

    if-eqz v0, :cond_3f

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/widgets/material/R$dimen;->ms_right_left_spinner_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_40

    :cond_3f
    const/4 v0, 0x0

    :goto_40
    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->rightLeftSpinnerPadding:I

    .line 285
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/widgets/material/R$dimen;->ms_floating_label_inside_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelInsideSpacing:I

    .line 286
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lbr/com/itau/widgets/material/R$dimen;->ms_error_label_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorLabelSpacing:I

    .line 288
    return-void
.end method

.method private initFloatingLabelAnimator()V
    .registers 5

    .line 306
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    if-nez v0, :cond_17

    .line 307
    const-string v0, "floatingLabelPercent"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_18

    invoke-static {p0, v0, v1}, Lcom/nineoldandroids/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    .line 308
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    invoke-virtual {v0, p0}, Lcom/nineoldandroids/animation/ObjectAnimator;->addUpdateListener(Lcom/nineoldandroids/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 310
    :cond_17
    return-void

    :array_18
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private initOnItemSelectedListener()V
    .registers 2

    .line 296
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 297
    return-void
.end method

.method private initPadding()V
    .registers 3

    .line 262
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getPaddingTop()I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->innerPaddingTop:I

    .line 263
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getPaddingLeft()I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->innerPaddingLeft:I

    .line 264
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getPaddingRight()I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->innerPaddingRight:I

    .line 265
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getPaddingBottom()I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->innerPaddingBottom:I

    .line 267
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelTopSpacing:I

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelInsideSpacing:I

    add-int/2addr v0, v1

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelBottomSpacing:I

    add-int/2addr v0, v1

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->extraPaddingTop:I

    .line 268
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->updateBottomPadding()V

    .line 270
    return-void
.end method

.method private initPaintObjects()V
    .registers 4

    .line 241
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->paint:Landroid/graphics/Paint;

    .line 243
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->textPaint:Landroid/text/TextPaint;

    .line 244
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->textPaint:Landroid/text/TextPaint;

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelTextSize:F

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 245
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->typeface:Landroid/graphics/Typeface;

    if-eqz v0, :cond_22

    .line 246
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->textPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->typeface:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 248
    :cond_22
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->textPaint:Landroid/text/TextPaint;

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->baseColor:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 249
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getAlpha()I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->baseAlpha:I

    .line 251
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->selectorPath:Landroid/graphics/Path;

    .line 252
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->selectorPath:Landroid/graphics/Path;

    sget-object v1, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 254
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/graphics/Point;

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->selectorPoints:[Landroid/graphics/Point;

    .line 255
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_45
    const/4 v0, 0x3

    if-ge v2, v0, :cond_54

    .line 256
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->selectorPoints:[Landroid/graphics/Point;

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    aput-object v1, v0, v2

    .line 255
    add-int/lit8 v2, v2, 0x1

    goto :goto_45

    .line 258
    .end local v2    # "i":I
    :cond_54
    return-void
.end method

.method private needScrollingAnimation()Z
    .registers 7

    .line 384
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->error:Ljava/lang/CharSequence;

    if-eqz v0, :cond_27

    .line 385
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getWidth()I

    move-result v0

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->rightLeftSpinnerPadding:I

    sub-int/2addr v0, v1

    int-to-float v4, v0

    .line 386
    .local v4, "screenWidth":F
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->textPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->error:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->error:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;II)F

    move-result v5

    .line 387
    .local v5, "errorTextWidth":F
    cmpl-float v0, v5, v4

    if-lez v0, :cond_25

    const/4 v0, 0x1

    goto :goto_26

    :cond_25
    const/4 v0, 0x0

    :goto_26
    return v0

    .line 389
    .end local v4    # "screenWidth":F
    .end local v5    # "errorTextWidth":F
    :cond_27
    const/4 v0, 0x0

    return v0
.end method

.method private prepareBottomPadding()I
    .registers 11

    .line 434
    iget v8, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->minNbErrorLines:I

    .line 435
    .local v8, "targetNbLines":I
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->error:Ljava/lang/CharSequence;

    if-eqz v0, :cond_31

    .line 436
    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->error:Ljava/lang/CharSequence;

    iget-object v2, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->staticLayout:Landroid/text/StaticLayout;

    .line 437
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->staticLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v9

    .line 438
    .local v9, "nbErrorLines":I
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->minNbErrorLines:I

    invoke-static {v0, v9}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 440
    .end local v9    # "nbErrorLines":I
    :cond_31
    return v8
.end method

.method private pxToDp(F)F
    .registers 4
    .param p1, "px"    # F

    .line 371
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 372
    .local v1, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, p1

    return v0
.end method

.method private setCurrentNbErrorLines(F)V
    .registers 2
    .param p1, "currentNbErrorLines"    # F

    .line 753
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->currentNbErrorLines:F

    .line 754
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->updateBottomPadding()V

    .line 755
    return-void
.end method

.method private setErrorLabelPosX(I)V
    .registers 2
    .param p1, "errorLabelPosX"    # I

    .line 745
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorLabelPosX:I

    .line 746
    return-void
.end method

.method private setFloatingLabelPercent(F)V
    .registers 2
    .param p1, "floatingLabelPercent"    # F

    .line 737
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelPercent:F

    .line 738
    return-void
.end method

.method private setPadding()V
    .registers 7

    .line 376
    iget v2, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->innerPaddingLeft:I

    .line 377
    .local v2, "left":I
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->innerPaddingTop:I

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->extraPaddingTop:I

    add-int v3, v0, v1

    .line 378
    .local v3, "top":I
    iget v4, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->innerPaddingRight:I

    .line 379
    .local v4, "right":I
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->innerPaddingBottom:I

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->extraPaddingBottom:I

    add-int v5, v0, v1

    .line 380
    .local v5, "bottom":I
    invoke-super {p0, v2, v3, v4, v5}, Landroid/widget/Spinner;->setPadding(IIII)V

    .line 381
    return-void
.end method

.method private showFloatingLabel()V
    .registers 2

    .line 313
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    if-eqz v0, :cond_1a

    .line 314
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelVisible:Z

    .line 315
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 316
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->reverse()V

    goto :goto_1a

    .line 318
    :cond_15
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->start()V

    .line 321
    :cond_1a
    :goto_1a
    return-void
.end method

.method private startErrorMultilineAnimator(F)V
    .registers 5
    .param p1, "destLines"    # F

    .line 348
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    if-nez v0, :cond_13

    .line 349
    const-string v0, "currentNbErrorLines"

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    invoke-static {p0, v0, v1}, Lcom/nineoldandroids/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    goto :goto_1e

    .line 352
    :cond_13
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/nineoldandroids/animation/ObjectAnimator;->setFloatValues([F)V

    .line 354
    :goto_1e
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->start()V

    .line 355
    return-void
.end method

.method private startErrorScrollingAnimator()V
    .registers 6

    .line 332
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->textPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->error:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 333
    .local v4, "textWidth":I
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    if-nez v0, :cond_58

    .line 334
    const-string v0, "errorLabelPosX"

    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v2, v1, v3

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v4

    const/4 v3, 0x1

    aput v2, v1, v3

    invoke-static {p0, v0, v1}, Lcom/nineoldandroids/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Lcom/nineoldandroids/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    .line 335
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Lcom/nineoldandroids/animation/ObjectAnimator;->setStartDelay(J)V

    .line 336
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Lcom/nineoldandroids/animation/ObjectAnimator;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 337
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    iget-object v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->error:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    mul-int/lit16 v1, v1, 0x96

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/nineoldandroids/animation/ObjectAnimator;->setDuration(J)Lcom/nineoldandroids/animation/ObjectAnimator;

    .line 338
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    invoke-virtual {v0, p0}, Lcom/nineoldandroids/animation/ObjectAnimator;->addUpdateListener(Lcom/nineoldandroids/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 339
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/nineoldandroids/animation/ObjectAnimator;->setRepeatCount(I)V

    goto :goto_6e

    .line 341
    :cond_58
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v2, v1, v3

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v4

    const/4 v3, 0x1

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/nineoldandroids/animation/ObjectAnimator;->setIntValues([I)V

    .line 343
    :goto_6e
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->start()V

    .line 344
    return-void
.end method

.method private updateBottomPadding()V
    .registers 4

    .line 273
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v2

    .line 275
    .local v2, "textMetrics":Landroid/graphics/Paint$FontMetrics;
    iget v0, v2, Landroid/graphics/Paint$FontMetrics;->descent:F

    iget v1, v2, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float/2addr v0, v1

    iget-object v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->error:Ljava/lang/CharSequence;

    if-nez v1, :cond_11

    const/4 v1, 0x0

    goto :goto_13

    :cond_11
    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->currentNbErrorLines:F

    :goto_13
    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->underlineTopSpacing:I

    add-int/2addr v0, v1

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->underlineBottomSpacing:I

    add-int/2addr v0, v1

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->extraPaddingBottom:I

    .line 276
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->setPadding()V

    .line 277
    return-void
.end method


# virtual methods
.method public getAccessibilityText()Ljava/lang/String;
    .registers 10

    .line 393
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 395
    .local v2, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getSelectedItemPosition()I

    move-result v3

    .line 396
    .local v3, "selectedItemPosition":I
    if-eqz v3, :cond_d

    const/4 v4, 0x1

    goto :goto_e

    :cond_d
    const/4 v4, 0x0

    .line 397
    .local v4, "itemSelected":Z
    :goto_e
    if-eqz v4, :cond_45

    .line 398
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelText:Ljava/lang/CharSequence;

    if-eqz v0, :cond_17

    iget-object v5, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelText:Ljava/lang/CharSequence;

    goto :goto_19

    :cond_17
    iget-object v5, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->hint:Ljava/lang/CharSequence;

    .line 399
    .local v5, "floatText":Ljava/lang/CharSequence;
    :goto_19
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 400
    invoke-static {}, Landroid/view/accessibility/AccessibilityEvent;->obtain()Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v6

    .line 401
    .local v6, "record":Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getSelectedView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    .line 402
    invoke-virtual {v6}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v7

    .line 403
    .local v7, "recordsText":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    if-eqz v7, :cond_44

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_44

    .line 404
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/lang/CharSequence;

    .line 406
    .local v8, "childText":Ljava/lang/CharSequence;
    const/16 v0, 0x20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 407
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 409
    .end local v5    # "floatText":Ljava/lang/CharSequence;
    .end local v6    # "record":Landroid/view/accessibility/AccessibilityEvent;
    .end local v7    # "recordsText":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    .end local v7
    .end local v8    # "childText":Ljava/lang/CharSequence;
    :cond_44
    goto :goto_4a

    .line 411
    :cond_45
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->hint:Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 414
    :goto_4a
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getError()Ljava/lang/CharSequence;

    move-result-object v5

    .line 415
    .local v5, "error":Ljava/lang/CharSequence;
    if-eqz v5, :cond_5e

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_5e

    .line 416
    const/16 v0, 0x20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 417
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 420
    :cond_5e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBaseColor()I
    .registers 2

    .line 619
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->baseColor:I

    return v0
.end method

.method public getError()Ljava/lang/CharSequence;
    .registers 2

    .line 700
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->error:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getErrorColor()I
    .registers 2

    .line 639
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorColor:I

    return v0
.end method

.method public getFloatingLabelText()Ljava/lang/CharSequence;
    .registers 2

    .line 667
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelText:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getHighlightColor()I
    .registers 2

    .line 630
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->highlightColor:I

    return v0
.end method

.method public getHint()Ljava/lang/CharSequence;
    .registers 2

    .line 653
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->hint:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public hideKeyboardOnGainFocus(Landroid/app/Activity;)V
    .registers 3
    .param p1, "activity"    # Landroid/app/Activity;

    .line 758
    new-instance v0, Lbr/com/itau/widgets/material/MaterialSpinner$2;

    invoke-direct {v0, p0, p1}, Lbr/com/itau/widgets/material/MaterialSpinner$2;-><init>(Lbr/com/itau/widgets/material/MaterialSpinner;Landroid/app/Activity;)V

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 768
    return-void
.end method

.method public onAnimationUpdate(Lcom/nineoldandroids/animation/ValueAnimator;)V
    .registers 2
    .param p1, "animation"    # Lcom/nineoldandroids/animation/ValueAnimator;

    .line 607
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->invalidate()V

    .line 608
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .registers 15
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .line 454
    invoke-super {p0, p1}, Landroid/widget/Spinner;->onDraw(Landroid/graphics/Canvas;)V

    .line 456
    const/4 v6, 0x0

    .line 457
    .local v6, "startX":I
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getWidth()I

    move-result v7

    .line 460
    .local v7, "endX":I
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->underlineTopSpacing:I

    add-int v9, v0, v1

    .line 461
    .local v9, "startYLine":I
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getPaddingTop()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelPercent:F

    iget v2, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelBottomSpacing:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-int v10, v0

    .line 463
    .local v10, "startYFloatingLabel":I
    const/4 v11, -0x1

    .line 464
    .local v11, "lineColor":I
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->error:Ljava/lang/CharSequence;

    if-eqz v0, :cond_82

    .line 465
    iget v8, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->thicknessError:I

    .line 466
    .local v8, "lineHeight":I
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorLabelSpacing:I

    add-int/2addr v0, v9

    iget-boolean v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->hideUnderline:Z

    if-eqz v1, :cond_32

    const/4 v1, 0x0

    goto :goto_33

    :cond_32
    move v1, v8

    :goto_33
    add-int v12, v0, v1

    .line 467
    .local v12, "startYErrorLabel":I
    iget v11, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorColor:I

    .line 468
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 469
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->textPaint:Landroid/text/TextPaint;

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorColor:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 471
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->typeface:Landroid/graphics/Typeface;

    if-eqz v0, :cond_50

    .line 472
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->textPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->typeface:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 475
    :cond_50
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->multiline:Z

    if-eqz v0, :cond_6d

    .line 476
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 477
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->rightLeftSpinnerPadding:I

    add-int/lit8 v0, v0, 0x0

    int-to-float v0, v0

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorLabelSpacing:I

    sub-int v1, v12, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 478
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->staticLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 479
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_81

    .line 483
    :cond_6d
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->error:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->rightLeftSpinnerPadding:I

    add-int/lit8 v1, v1, 0x0

    iget v2, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorLabelPosX:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    int-to-float v2, v12

    iget-object v3, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 490
    .end local v12    # "startYErrorLabel":I
    :goto_81
    goto :goto_b9

    .line 491
    .end local v8    # "lineHeight":I
    :cond_82
    iget v8, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->thickness:I

    .line 493
    .local v8, "lineHeight":I
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->isSelected:Z

    if-nez v0, :cond_8c

    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->underlineAlwaysHighlighted:Z

    if-eqz v0, :cond_9e

    .line 494
    :cond_8c
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->highlightUnderlineColor:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_94

    iget v11, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->highlightUnderlineColor:I

    goto :goto_96

    :cond_94
    iget v11, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->highlightColor:I

    .line 495
    :goto_96
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->highlightColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_b9

    .line 497
    :cond_9e
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_a7

    iget v11, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->baseColor:I

    goto :goto_a9

    :cond_a7
    iget v11, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->disabledColor:I

    .line 498
    :goto_a9
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->paint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_b4

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->baseColor:I

    goto :goto_b6

    :cond_b4
    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->disabledColor:I

    :goto_b6
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 503
    :goto_b9
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->hideUnderline:Z

    if-nez v0, :cond_ce

    .line 504
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, v11}, Landroid/graphics/Paint;->setColor(I)V

    .line 505
    move-object v0, p1

    int-to-float v2, v9

    int-to-float v3, v7

    add-int v1, v9, v8

    int-to-float v4, v1

    iget-object v5, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->paint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 509
    :cond_ce
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->hint:Ljava/lang/CharSequence;

    if-nez v0, :cond_d6

    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelText:Ljava/lang/CharSequence;

    if-eqz v0, :cond_13b

    .line 510
    :cond_d6
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->isSelected:Z

    if-nez v0, :cond_de

    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelAlwaysHighlighted:Z

    if-eqz v0, :cond_e6

    .line 511
    :cond_de
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->textPaint:Landroid/text/TextPaint;

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelColor:I

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    goto :goto_f6

    .line 513
    :cond_e6
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_f1

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelColor:I

    goto :goto_f3

    :cond_f1
    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->disabledColor:I

    :goto_f3
    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 515
    :goto_f6
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_102

    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelVisible:Z

    if-nez v0, :cond_11f

    .line 516
    :cond_102
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->textPaint:Landroid/text/TextPaint;

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelPercent:F

    float-to-double v1, v1

    const-wide v3, 0x3fe999999999999aL    # 0.8

    mul-double/2addr v1, v3

    const-wide v3, 0x3fc999999999999aL    # 0.2

    add-double/2addr v1, v3

    iget v3, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->baseAlpha:I

    int-to-double v3, v3

    mul-double/2addr v1, v3

    iget v3, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelPercent:F

    float-to-double v3, v3

    mul-double/2addr v1, v3

    double-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setAlpha(I)V

    .line 518
    :cond_11f
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelText:Ljava/lang/CharSequence;

    if-eqz v0, :cond_12a

    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelText:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v12

    goto :goto_130

    :cond_12a
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->hint:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v12

    .line 519
    .local v12, "textToDraw":Ljava/lang/String;
    :goto_130
    iget v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->rightLeftSpinnerPadding:I

    add-int/lit8 v0, v0, 0x0

    int-to-float v0, v0

    int-to-float v1, v10

    iget-object v2, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v12, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 522
    .end local v12    # "textToDraw":Ljava/lang/String;
    :cond_13b
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getWidth()I

    move-result v0

    iget v1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->arrowPaddingRight:I

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getPaddingTop()I

    move-result v1

    const/high16 v2, 0x41000000    # 8.0f

    invoke-direct {p0, v2}, Lbr/com/itau/widgets/material/MaterialSpinner;->dpToPx(F)I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {p0, p1, v0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->drawSelector(Landroid/graphics/Canvas;II)V

    .line 525
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .registers 4
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .line 425
    invoke-super {p0, p1}, Landroid/widget/Spinner;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 427
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_10

    .line 428
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getAccessibilityText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 430
    :cond_10
    return-void
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .registers 3
    .param p1, "x0"    # Landroid/widget/Adapter;

    .line 63
    move-object v0, p1

    check-cast v0, Landroid/widget/SpinnerAdapter;

    invoke-virtual {p0, v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/SpinnerAdapter;)V
    .registers 4
    .param p1, "adapter"    # Landroid/widget/SpinnerAdapter;

    .line 729
    new-instance v0, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;

    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, p1, v1}, Lbr/com/itau/widgets/material/MaterialSpinner$HintAdapter;-><init>(Lbr/com/itau/widgets/material/MaterialSpinner;Landroid/widget/SpinnerAdapter;Landroid/content/Context;)V

    invoke-super {p0, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 730
    return-void
.end method

.method public setBaseColor(I)V
    .registers 3
    .param p1, "baseColor"    # I

    .line 623
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->baseColor:I

    .line 624
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    .line 625
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->textPaint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->getAlpha()I

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->baseAlpha:I

    .line 626
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->invalidate()V

    .line 627
    return-void
.end method

.method public setEnabled(Z)V
    .registers 3
    .param p1, "enabled"    # Z

    .line 692
    if-nez p1, :cond_8

    .line 693
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->isSelected:Z

    .line 694
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->invalidate()V

    .line 696
    :cond_8
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 697
    return-void
.end method

.method public setError(I)V
    .registers 4
    .param p1, "resid"    # I

    .line 704
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 705
    .local v1, "error":Ljava/lang/CharSequence;
    invoke-virtual {p0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setError(Ljava/lang/CharSequence;)V

    .line 706
    return-void
.end method

.method public setError(Ljava/lang/CharSequence;)V
    .registers 3
    .param p1, "error"    # Ljava/lang/CharSequence;

    .line 676
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->error:Ljava/lang/CharSequence;

    .line 677
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    if-eqz v0, :cond_b

    .line 678
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorLabelAnimator:Lcom/nineoldandroids/animation/ObjectAnimator;

    invoke-virtual {v0}, Lcom/nineoldandroids/animation/ObjectAnimator;->end()V

    .line 681
    :cond_b
    iget-boolean v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->multiline:Z

    if-eqz v0, :cond_18

    .line 682
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->prepareBottomPadding()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lbr/com/itau/widgets/material/MaterialSpinner;->startErrorMultilineAnimator(F)V

    goto :goto_21

    .line 683
    :cond_18
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->needScrollingAnimation()Z

    move-result v0

    if-eqz v0, :cond_21

    .line 684
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->startErrorScrollingAnimator()V

    .line 686
    :cond_21
    :goto_21
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->updateBottomPadding()V

    .line 687
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->requestLayout()V

    .line 688
    return-void
.end method

.method public setErrorColor(I)V
    .registers 2
    .param p1, "errorColor"    # I

    .line 643
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->errorColor:I

    .line 644
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->invalidate()V

    .line 645
    return-void
.end method

.method public setFloatingLabelText(I)V
    .registers 4
    .param p1, "resid"    # I

    .line 671
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 672
    .local v1, "floatingLabelText":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setFloatingLabelText(Ljava/lang/CharSequence;)V

    .line 673
    return-void
.end method

.method public setFloatingLabelText(Ljava/lang/CharSequence;)V
    .registers 2
    .param p1, "floatingLabelText"    # Ljava/lang/CharSequence;

    .line 662
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->floatingLabelText:Ljava/lang/CharSequence;

    .line 663
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->invalidate()V

    .line 664
    return-void
.end method

.method public setHighlightColor(I)V
    .registers 2
    .param p1, "highlightColor"    # I

    .line 634
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->highlightColor:I

    .line 635
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->invalidate()V

    .line 636
    return-void
.end method

.method public setHint(I)V
    .registers 4
    .param p1, "resid"    # I

    .line 657
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 658
    .local v1, "hint":Ljava/lang/CharSequence;
    invoke-virtual {p0, v1}, Lbr/com/itau/widgets/material/MaterialSpinner;->setHint(Ljava/lang/CharSequence;)V

    .line 659
    return-void
.end method

.method public setHint(Ljava/lang/CharSequence;)V
    .registers 2
    .param p1, "hint"    # Ljava/lang/CharSequence;

    .line 648
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->hint:Ljava/lang/CharSequence;

    .line 649
    invoke-virtual {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->invalidate()V

    .line 650
    return-void
.end method

.method public setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .registers 3
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 561
    new-instance v0, Lbr/com/itau/widgets/material/MaterialSpinner$1;

    invoke-direct {v0, p0, p1}, Lbr/com/itau/widgets/material/MaterialSpinner$1;-><init>(Lbr/com/itau/widgets/material/MaterialSpinner;Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 601
    .local v0, "onItemSelectedListener":Landroid/widget/AdapterView$OnItemSelectedListener;
    invoke-super {p0, v0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 602
    return-void
.end method

.method public setPadding(IIII)V
    .registers 5
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 714
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/Spinner;->setPadding(IIII)V

    .line 715
    return-void
.end method

.method public setPaddingSafe(IIII)V
    .registers 5
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .line 719
    iput p3, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->innerPaddingRight:I

    .line 720
    iput p1, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->innerPaddingLeft:I

    .line 721
    iput p2, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->innerPaddingTop:I

    .line 722
    iput p4, p0, Lbr/com/itau/widgets/material/MaterialSpinner;->innerPaddingBottom:I

    .line 724
    invoke-direct {p0}, Lbr/com/itau/widgets/material/MaterialSpinner;->setPadding()V

    .line 725
    return-void
.end method

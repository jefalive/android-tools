.class final Lde/greenrobot/event/PendingPost;
.super Ljava/lang/Object;
.source "PendingPost.java"


# static fields
.field private static final pendingPostPool:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lde/greenrobot/event/PendingPost;>;"
        }
    .end annotation
.end field


# instance fields
.field event:Ljava/lang/Object;

.field next:Lde/greenrobot/event/PendingPost;

.field subscription:Lde/greenrobot/event/Subscription;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lde/greenrobot/event/PendingPost;->pendingPostPool:Ljava/util/List;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;Lde/greenrobot/event/Subscription;)V
    .registers 3
    .param p1, "event"    # Ljava/lang/Object;
    .param p2, "subscription"    # Lde/greenrobot/event/Subscription;

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lde/greenrobot/event/PendingPost;->event:Ljava/lang/Object;

    .line 30
    iput-object p2, p0, Lde/greenrobot/event/PendingPost;->subscription:Lde/greenrobot/event/Subscription;

    .line 31
    return-void
.end method

.method static obtainPendingPost(Lde/greenrobot/event/Subscription;Ljava/lang/Object;)Lde/greenrobot/event/PendingPost;
    .registers 8
    .param p0, "subscription"    # Lde/greenrobot/event/Subscription;
    .param p1, "event"    # Ljava/lang/Object;

    .line 34
    sget-object v2, Lde/greenrobot/event/PendingPost;->pendingPostPool:Ljava/util/List;

    monitor-enter v2

    .line 35
    :try_start_3
    sget-object v0, Lde/greenrobot/event/PendingPost;->pendingPostPool:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 36
    .local v3, "size":I
    if-lez v3, :cond_1f

    .line 37
    sget-object v0, Lde/greenrobot/event/PendingPost;->pendingPostPool:Ljava/util/List;

    add-int/lit8 v1, v3, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lde/greenrobot/event/PendingPost;

    .line 38
    .local v4, "pendingPost":Lde/greenrobot/event/PendingPost;
    iput-object p1, v4, Lde/greenrobot/event/PendingPost;->event:Ljava/lang/Object;

    .line 39
    iput-object p0, v4, Lde/greenrobot/event/PendingPost;->subscription:Lde/greenrobot/event/Subscription;

    .line 40
    const/4 v0, 0x0

    iput-object v0, v4, Lde/greenrobot/event/PendingPost;->next:Lde/greenrobot/event/PendingPost;
    :try_end_1d
    .catchall {:try_start_3 .. :try_end_1d} :catchall_21

    .line 41
    monitor-exit v2

    return-object v4

    .line 43
    .end local v3    # "size":I
    .end local v4    # "pendingPost":Lde/greenrobot/event/PendingPost;
    :cond_1f
    monitor-exit v2

    goto :goto_24

    :catchall_21
    move-exception v5

    monitor-exit v2

    throw v5

    .line 44
    :goto_24
    new-instance v0, Lde/greenrobot/event/PendingPost;

    invoke-direct {v0, p1, p0}, Lde/greenrobot/event/PendingPost;-><init>(Ljava/lang/Object;Lde/greenrobot/event/Subscription;)V

    return-object v0
.end method

.method static releasePendingPost(Lde/greenrobot/event/PendingPost;)V
    .registers 5
    .param p0, "pendingPost"    # Lde/greenrobot/event/PendingPost;

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lde/greenrobot/event/PendingPost;->event:Ljava/lang/Object;

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lde/greenrobot/event/PendingPost;->subscription:Lde/greenrobot/event/Subscription;

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lde/greenrobot/event/PendingPost;->next:Lde/greenrobot/event/PendingPost;

    .line 51
    sget-object v2, Lde/greenrobot/event/PendingPost;->pendingPostPool:Ljava/util/List;

    monitor-enter v2

    .line 53
    :try_start_c
    sget-object v0, Lde/greenrobot/event/PendingPost;->pendingPostPool:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x2710

    if-ge v0, v1, :cond_1b

    .line 54
    sget-object v0, Lde/greenrobot/event/PendingPost;->pendingPostPool:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1b
    .catchall {:try_start_c .. :try_end_1b} :catchall_1d

    .line 56
    :cond_1b
    monitor-exit v2

    goto :goto_20

    :catchall_1d
    move-exception v3

    monitor-exit v2

    throw v3

    .line 57
    :goto_20
    return-void
.end method

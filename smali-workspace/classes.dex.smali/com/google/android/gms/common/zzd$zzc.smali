.class abstract Lcom/google/android/gms/common/zzd$zzc;
.super Lcom/google/android/gms/common/zzd$zza;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/common/zzd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "zzc"
.end annotation


# static fields
.field private static final zzafJ:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<[B>;"
        }
    .end annotation
.end field


# instance fields
.field private zzafI:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<[B>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 2

    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/google/android/gms/common/zzd$zzc;->zzafJ:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method constructor <init>([B)V
    .registers 3

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/zzd$zza;-><init>([B)V

    sget-object v0, Lcom/google/android/gms/common/zzd$zzc;->zzafJ:Ljava/lang/ref/WeakReference;

    iput-object v0, p0, Lcom/google/android/gms/common/zzd$zzc;->zzafI:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method getBytes()[B
    .registers 5

    move-object v1, p0

    monitor-enter v1

    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/common/zzd$zzc;->zzafI:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, [B

    if-nez v2, :cond_18

    invoke-virtual {p0}, Lcom/google/android/gms/common/zzd$zzc;->zzoL()[B

    move-result-object v2

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/common/zzd$zzc;->zzafI:Ljava/lang/ref/WeakReference;
    :try_end_18
    .catchall {:try_start_2 .. :try_end_18} :catchall_1a

    :cond_18
    monitor-exit v1

    return-object v2

    :catchall_1a
    move-exception v3

    monitor-exit v1

    throw v3
.end method

.method protected abstract zzoL()[B
.end method

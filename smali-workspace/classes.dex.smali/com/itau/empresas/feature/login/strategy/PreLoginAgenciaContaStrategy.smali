.class Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaStrategy;
.super Ljava/lang/Object;
.source "PreLoginAgenciaContaStrategy.java"

# interfaces
.implements Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;
.implements Lcom/itau/empresas/feature/login/strategy/PreLoginControllerListener;


# instance fields
.field private final agencia:Ljava/lang/String;

.field private final conta:Ljava/lang/String;

.field private final controller:Lcom/itau/empresas/feature/login/controller/PreLoginController;

.field private final dac:Ljava/lang/String;

.field private final strategy:Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/login/controller/PreLoginController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;)V
    .registers 7
    .param p1, "controller"    # Lcom/itau/empresas/feature/login/controller/PreLoginController;
    .param p2, "agencia"    # Ljava/lang/String;
    .param p3, "conta"    # Ljava/lang/String;
    .param p4, "dac"    # Ljava/lang/String;
    .param p5, "strategy"    # Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaStrategy;->controller:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    .line 32
    iput-object p5, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaStrategy;->strategy:Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;

    .line 33
    iget-object v0, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaStrategy;->controller:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    invoke-virtual {v0, p0}, Lcom/itau/empresas/feature/login/controller/PreLoginController;->setListener(Lcom/itau/empresas/feature/login/strategy/PreLoginControllerListener;)V

    .line 34
    iput-object p2, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaStrategy;->agencia:Ljava/lang/String;

    .line 35
    iput-object p3, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaStrategy;->conta:Ljava/lang/String;

    .line 36
    iput-object p4, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaStrategy;->dac:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method public backendException(Lbr/com/itau/sdk/android/core/exception/BackendException;)V
    .registers 5
    .param p1, "ex"    # Lbr/com/itau/sdk/android/core/exception/BackendException;

    .line 56
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getError()Lbr/com/itau/sdk/android/core/model/ErrorDTO;

    move-result-object v2

    .line 57
    .local v2, "errorDTO":Lbr/com/itau/sdk/android/core/model/ErrorDTO;
    const-string v0, "agencia_conta_multiplos_operadores"

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getCodigo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 58
    iget-object v0, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaStrategy;->strategy:Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;->proximoPasso(I)V

    goto :goto_33

    .line 59
    :cond_17
    const-string v0, "agencia_conta_multiplos_operadores_documento"

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getCodigo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2a

    .line 60
    iget-object v0, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaStrategy;->strategy:Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;->proximoPasso(I)V

    goto :goto_33

    .line 62
    :cond_2a
    iget-object v0, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaStrategy;->strategy:Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->getMensagem()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;->falha(Ljava/lang/String;)V

    .line 64
    :goto_33
    return-void
.end method

.method public executar()V
    .registers 8

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaStrategy;->controller:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaStrategy;->agencia:Ljava/lang/String;

    iget-object v2, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaStrategy;->conta:Ljava/lang/String;

    iget-object v3, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaStrategy;->dac:Ljava/lang/String;

    const-string v6, ""

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/itau/empresas/feature/login/controller/PreLoginController;->buscaOperador(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    return-void
.end method

.method public listaOperadores(Ljava/util/List;)V
    .registers 5
    .param p1, "operadorVOs"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/api/model/OperadorVO;>;)V"
        }
    .end annotation

    .line 46
    invoke-static {p1}, Lcom/itau/empresas/feature/login/strategy/ValidadorStrategy;->possuiSomenteUmOperador(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 48
    new-instance v0, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;

    invoke-direct {v0}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;-><init>()V

    .line 49
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/api/model/OperadorVO;

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/OperadorVO;->getCodigoOperador()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->operador(Ljava/lang/String;)Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO$PreLoginUsuarioBuilder;->build()Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;

    move-result-object v2

    .line 50
    .local v2, "operador":Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaStrategy;->controller:Lcom/itau/empresas/feature/login/controller/PreLoginController;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/strategy/PreLoginAgenciaContaStrategy;->strategy:Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;

    invoke-static {v0, v2, v1}, Lcom/itau/empresas/feature/login/strategy/PreLoginStrategyFactory;->operadorUnico(Lcom/itau/empresas/feature/login/controller/PreLoginController;Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;Lcom/itau/empresas/feature/login/strategy/ListenerStrategy;)Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;

    move-result-object v0

    invoke-interface {v0}, Lcom/itau/empresas/feature/login/strategy/PreLoginStrategy;->executar()V

    .line 52
    .end local v2    # "operador":Lcom/itau/empresas/feature/login/model/PreLoginUsuarioVO;
    :cond_29
    return-void
.end method

.class public Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;
.super Lcom/itau/empresas/ui/fragment/BaseFragment;
.source "WidgetSaldoMultiLimiteFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;,
        Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$AbriuFechouListener;
    }
.end annotation


# instance fields
.field private abriuFechouListener:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$AbriuFechouListener;

.field private consultaSaldoGiro:Z

.field public elvSaldoMultiLimite:Landroid/widget/ExpandableListView;

.field extratoController:Lcom/itau/empresas/controller/ExtratoController;

.field private listaItensValorSaldo:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
        }
    .end annotation
.end field

.field private listaValorSaldo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field public llMostrarSaldo:Landroid/widget/LinearLayout;

.field llViewSaldoMultiLimite:Landroid/view/View;

.field logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

.field private maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

.field private mostrarSaldo:Z

.field preferenciaWidget:Lcom/itau/empresas/PreferenciaUtils;

.field progressMostrarSaldo:Landroid/widget/ProgressBar;

.field private retornoSaldo:Lcom/itau/empresas/api/model/SaldoVO;

.field root:Landroid/widget/LinearLayout;

.field private saldoExpandableListAdapter:Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;

.field private volatile temDadosInterno:Z

.field private volatile temDadosSaldo:Z

.field textoMostrarSaldo:Landroid/widget/TextView;

.field textoSaldoNaoEncontrado:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 52
    invoke-direct {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;-><init>()V

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->mostrarSaldo:Z

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->consultaSaldoGiro:Z

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    return-object v0
.end method

.method static synthetic access$100(Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;Z)V
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;
    .param p1, "x1"    # Z

    .line 52
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->setCarregando(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;

    .line 52
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->consultaMultiLimite()V

    return-void
.end method

.method private consultaLis()V
    .registers 6

    .line 502
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v4

    .line 503
    .local v4, "contaSelecionada":Lcom/itau/empresas/api/model/ContaOperadorVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->extratoController:Lcom/itau/empresas/controller/ExtratoController;

    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v1

    .line 504
    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    .line 505
    invoke-virtual {v4}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v3

    .line 503
    invoke-virtual {v0, v1, v2, v3}, Lcom/itau/empresas/controller/ExtratoController;->consultaLis(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    return-void
.end method

.method private consultaMultiLimite()V
    .registers 6

    .line 330
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->extratoController:Lcom/itau/empresas/controller/ExtratoController;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->application:Lcom/itau/empresas/CustomApplication;

    .line 331
    invoke-virtual {v1}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v1

    invoke-virtual {v1}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getAgencia()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->application:Lcom/itau/empresas/CustomApplication;

    .line 332
    invoke-virtual {v2}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v2

    invoke-virtual {v2}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getConta()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->application:Lcom/itau/empresas/CustomApplication;

    .line 333
    invoke-virtual {v3}, Lcom/itau/empresas/CustomApplication;->getContaSelecionada()Lcom/itau/empresas/api/model/ContaOperadorVO;

    move-result-object v3

    invoke-virtual {v3}, Lcom/itau/empresas/api/model/ContaOperadorVO;->getDac()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    .line 331
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/itau/empresas/controller/ExtratoController;->consultaMultiLimite(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    return-void
.end method

.method private dadosInternosCarregados()V
    .registers 2

    .line 367
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->temDadosInterno:Z

    .line 368
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->abrirWidget()V

    .line 369
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->setCarregando(Z)V

    .line 370
    return-void
.end method

.method private dadosSaldoCarregados()V
    .registers 3

    .line 361
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->temDadosSaldo:Z

    .line 362
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->setCarregando(Z)V

    .line 363
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;->SALDO_VISIVEL:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 364
    return-void
.end method

.method private inicializaMaquinaDeEstado()Lcom/itau/empresas/ui/util/MaquinaDeEstado;
    .registers 7

    .line 435
    new-instance v0, Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    invoke-direct {v0}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;-><init>()V

    sget-object v1, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;->SALDO_NAO_ENCONTRADO:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->llMostrarSaldo:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->elvSaldoMultiLimite:Landroid/widget/ExpandableListView;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    .line 438
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->textoSaldoNaoEncontrado:Landroid/widget/TextView;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 439
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visible([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    new-instance v3, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$6;

    invoke-direct {v3, p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$6;-><init>(Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;)V

    .line 440
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState(Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 447
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 436
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;->MOSTRA_SALDO:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->elvSaldoMultiLimite:Landroid/widget/ExpandableListView;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->textoSaldoNaoEncontrado:Landroid/widget/TextView;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    .line 450
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->llMostrarSaldo:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 452
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visible([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    new-instance v3, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$5;

    invoke-direct {v3, p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$5;-><init>(Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;)V

    .line 453
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState(Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 460
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 448
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;->SALDO_VISIVEL:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    new-instance v2, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    invoke-direct {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;-><init>()V

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->llMostrarSaldo:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->textoSaldoNaoEncontrado:Landroid/widget/TextView;

    const/4 v5, 0x1

    aput-object v4, v3, v5

    .line 463
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->gone([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    iget-object v4, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->elvSaldoMultiLimite:Landroid/widget/ExpandableListView;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    .line 465
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->visible([Landroid/view/View;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    new-instance v3, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$4;

    invoke-direct {v3, p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$4;-><init>(Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;)V

    .line 466
    invoke-virtual {v2, v3}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->onChangeState(Lcom/itau/empresas/ui/util/maquinaestado/Estado$OnChangeState;)Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;

    move-result-object v2

    .line 474
    invoke-virtual {v2}, Lcom/itau/empresas/ui/util/maquinaestado/Estado$Builder;->build()Lcom/itau/empresas/ui/util/maquinaestado/Estado;

    move-result-object v2

    .line 461
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->novoEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;Lcom/itau/empresas/ui/util/maquinaestado/Estado;)Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    .line 435
    return-object v0
.end method

.method private mostraEstadoErro(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 7
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 479
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->setCarregando(Z)V

    .line 481
    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v0

    invoke-virtual {v0}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    .line 482
    .local v2, "opKey":Ljava/lang/String;
    move-object v3, v2

    const/4 v4, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_50

    goto :goto_33

    :sswitch_16
    const-string v0, "saldo"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    const/4 v4, 0x0

    goto :goto_33

    :sswitch_20
    const-string v0, "multiLimites"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    const/4 v4, 0x1

    goto :goto_33

    :sswitch_2a
    const-string v0, "lis"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_33

    const/4 v4, 0x2

    :cond_33
    :goto_33
    packed-switch v4, :pswitch_data_5e

    goto :goto_4e

    .line 484
    :pswitch_37
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;->SALDO_NAO_ENCONTRADO:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 485
    goto :goto_4e

    .line 488
    :pswitch_3f
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->setCarregando(Z)V

    .line 489
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->consultaLis()V

    .line 490
    goto :goto_4e

    .line 493
    :pswitch_47
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->setCarregando(Z)V

    .line 494
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->montaComboMultilimiteComSaldos()V

    .line 495
    .line 499
    :goto_4e
    return-void

    nop

    :sswitch_data_50
    .sparse-switch
        -0x65c589d0 -> :sswitch_20
        0x1a296 -> :sswitch_2a
        0x68248e9 -> :sswitch_16
    .end sparse-switch

    :pswitch_data_5e
    .packed-switch 0x0
        :pswitch_37
        :pswitch_3f
        :pswitch_47
    .end packed-switch
.end method

.method private mostraMensagemErro(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 5
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 518
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->setCarregando(Z)V

    .line 520
    const-string v0, "ItauEmpresas"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Erro na Chamanda: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-virtual {v2}, Lbr/com/itau/sdk/android/core/exception/BackendException;->getOpKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;->getException()Lbr/com/itau/sdk/android/core/exception/BackendException;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 521
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->root:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0701ef

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lbr/com/itau/topsnackbar/TopSnackbar;->make(Landroid/view/ViewGroup;Ljava/lang/CharSequence;I)Lbr/com/itau/topsnackbar/TopSnackbar;

    move-result-object v0

    .line 522
    invoke-virtual {v0}, Lbr/com/itau/topsnackbar/TopSnackbar;->show()Lbr/com/itau/topsnackbar/TopSnackbar;

    .line 523
    return-void
.end method

.method private setCarregando(Z)V
    .registers 5
    .param p1, "loading"    # Z

    .line 373
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->getEstadoAtual()Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;->MOSTRA_SALDO:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    if-ne v0, v1, :cond_2a

    .line 374
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->progressMostrarSaldo:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_10

    const/4 v1, 0x0

    goto :goto_12

    :cond_10
    const/16 v1, 0x8

    :goto_12
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 376
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->llMostrarSaldo:Landroid/widget/LinearLayout;

    if-nez p1, :cond_1b

    const/4 v1, 0x1

    goto :goto_1c

    :cond_1b
    const/4 v1, 0x0

    :goto_1c
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 377
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->llMostrarSaldo:Landroid/widget/LinearLayout;

    if-nez p1, :cond_25

    const/4 v1, 0x1

    goto :goto_26

    :cond_25
    const/4 v1, 0x0

    :goto_26
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_55

    .line 379
    :cond_2a
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->elvSaldoMultiLimite:Landroid/widget/ExpandableListView;

    .line 380
    const v1, 0x7f0e0566

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/ProgressBar;

    .line 382
    .local v2, "progress":Landroid/widget/ProgressBar;
    if-eqz v2, :cond_55

    .line 383
    if-eqz p1, :cond_3c

    const/4 v0, 0x0

    goto :goto_3e

    :cond_3c
    const/16 v0, 0x8

    :goto_3e
    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 385
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->elvSaldoMultiLimite:Landroid/widget/ExpandableListView;

    if-nez p1, :cond_47

    const/4 v1, 0x1

    goto :goto_48

    :cond_47
    const/4 v1, 0x0

    :goto_48
    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setClickable(Z)V

    .line 386
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->elvSaldoMultiLimite:Landroid/widget/ExpandableListView;

    if-nez p1, :cond_51

    const/4 v1, 0x1

    goto :goto_52

    :cond_51
    const/4 v1, 0x0

    :goto_52
    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setEnabled(Z)V

    .line 389
    .end local v2    # "progress":Landroid/widget/ProgressBar;
    :cond_55
    :goto_55
    return-void
.end method


# virtual methods
.method public abrirWidget()V
    .registers 4

    .line 392
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->getEstadoAtual()Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;->SALDO_VISIVEL:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    if-eq v0, v1, :cond_b

    .line 393
    return-void

    .line 396
    :cond_b
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->elvSaldoMultiLimite:Landroid/widget/ExpandableListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 398
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->abriuFechouListener:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$AbriuFechouListener;

    if-eqz v0, :cond_1a

    .line 399
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->abriuFechouListener:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$AbriuFechouListener;

    invoke-interface {v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$AbriuFechouListener;->abriu()V

    .line 402
    :cond_1a
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->preferenciaWidget:Lcom/itau/empresas/PreferenciaUtils;

    const-string v1, "widget_saldo"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/PreferenciaUtils;->set(Ljava/lang/String;Z)V

    .line 404
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_30

    .line 405
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->elvSaldoMultiLimite:Landroid/widget/ExpandableListView;

    const-string v1, "Expandindo widget de saldo"

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    goto :goto_37

    .line 407
    :cond_30
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->elvSaldoMultiLimite:Landroid/widget/ExpandableListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->sendAccessibilityEvent(I)V

    .line 409
    :goto_37
    return-void
.end method

.method protected carregandoElementosDeTela()V
    .registers 8

    .line 280
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->inicializaMaquinaDeEstado()Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    .line 281
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/CustomApplication;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->application:Lcom/itau/empresas/CustomApplication;

    .line 283
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 284
    .local v3, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 285
    iget v4, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 286
    .local v4, "width":I
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x420c0000    # 35.0f

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ViewUtils;->getDpisFromPixel(Landroid/content/res/Resources;F)I

    move-result v5

    .line 287
    .local v5, "dpi35":I
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ViewUtils;->getDpisFromPixel(Landroid/content/res/Resources;F)I

    move-result v6

    .line 288
    .local v6, "dpi5":I
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_4c

    .line 289
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->elvSaldoMultiLimite:Landroid/widget/ExpandableListView;

    sub-int v1, v4, v5

    sub-int v2, v4, v6

    invoke-virtual {v0, v1, v2}, Landroid/widget/ExpandableListView;->setIndicatorBounds(II)V

    goto :goto_55

    .line 291
    :cond_4c
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->elvSaldoMultiLimite:Landroid/widget/ExpandableListView;

    sub-int v1, v4, v5

    sub-int v2, v4, v6

    invoke-virtual {v0, v1, v2}, Landroid/widget/ExpandableListView;->setIndicatorBoundsRelative(II)V

    .line 294
    :goto_55
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->elvSaldoMultiLimite:Landroid/widget/ExpandableListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setGroupIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 295
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->elvSaldoMultiLimite:Landroid/widget/ExpandableListView;

    new-instance v1, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$3;-><init>(Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setOnGroupClickListener(Landroid/widget/ExpandableListView$OnGroupClickListener;)V

    .line 316
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;->MOSTRA_SALDO:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 318
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->application:Lcom/itau/empresas/CustomApplication;

    sget-object v1, Lcom/itau/empresas/CustomApplication$Exibicao;->EXIBICAO_WIDGET_SALDO:Lcom/itau/empresas/CustomApplication$Exibicao;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/CustomApplication;->verificarPermissao(Lcom/itau/empresas/CustomApplication$Exibicao;)Z

    move-result v0

    if-eqz v0, :cond_7d

    .line 321
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->llViewSaldoMultiLimite:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_84

    .line 325
    :cond_7d
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->llViewSaldoMultiLimite:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 327
    :goto_84
    return-void
.end method

.method public encolherWidget()V
    .registers 4

    .line 244
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->getEstadoAtual()Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    move-result-object v0

    sget-object v1, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;->SALDO_VISIVEL:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    if-eq v0, v1, :cond_b

    .line 245
    return-void

    .line 247
    :cond_b
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->elvSaldoMultiLimite:Landroid/widget/ExpandableListView;

    invoke-virtual {v0}, Landroid/widget/ExpandableListView;->getCount()I

    move-result v0

    if-gez v0, :cond_14

    .line 248
    return-void

    .line 250
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->elvSaldoMultiLimite:Landroid/widget/ExpandableListView;

    const v1, 0x7f0e0544

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/widget/ImageView;

    .line 251
    .local v2, "icFechar":Landroid/widget/ImageView;
    if-eqz v2, :cond_32

    .line 252
    const v0, 0x7f020134

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 253
    const v0, 0x7f0700a6

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 256
    :cond_32
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->elvSaldoMultiLimite:Landroid/widget/ExpandableListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->collapseGroup(I)Z

    .line 258
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->abriuFechouListener:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$AbriuFechouListener;

    if-eqz v0, :cond_41

    .line 259
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->abriuFechouListener:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$AbriuFechouListener;

    invoke-interface {v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$AbriuFechouListener;->encolheu()V

    .line 261
    :cond_41
    return-void
.end method

.method public fecharWidget()V
    .registers 4

    .line 103
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->mostrarSaldo:Z

    .line 104
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->preferenciaWidget:Lcom/itau/empresas/PreferenciaUtils;

    const-string v1, "widget_saldo"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/PreferenciaUtils;->set(Ljava/lang/String;Z)V

    .line 105
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->maquina:Lcom/itau/empresas/ui/util/MaquinaDeEstado;

    sget-object v1, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;->MOSTRA_SALDO:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$Estados;

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/MaquinaDeEstado;->mudaParaEstado(Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;)Lcom/itau/empresas/ui/util/MaquinaDeEstado$Chave;

    .line 106
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->abriuFechouListener:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$AbriuFechouListener;

    if-eqz v0, :cond_1b

    .line 107
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->abriuFechouListener:Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$AbriuFechouListener;

    invoke-interface {v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$AbriuFechouListener;->fechou()V

    .line 109
    :cond_1b
    return-void
.end method

.method public montaComboMultilimiteComSaldos()V
    .registers 9

    .line 412
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->dadosInternosCarregados()V

    .line 414
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 415
    .local v6, "itensTipos":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const v0, 0x7f070667

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 417
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 419
    .local v7, "itensValores":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->retornoSaldo:Lcom/itau/empresas/api/model/SaldoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/SaldoVO;->getDisponivelSaque()Ljava/lang/Double;

    move-result-object v0

    if-eqz v0, :cond_31

    .line 420
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->retornoSaldo:Lcom/itau/empresas/api/model/SaldoVO;

    .line 422
    invoke-virtual {v0}, Lcom/itau/empresas/api/model/SaldoVO;->getDisponivelSaque()Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object v0

    .line 421
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 420
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 426
    :cond_31
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->listaItensValorSaldo:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->listaValorSaldo:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 428
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->saldoExpandableListAdapter:Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->listaValorSaldo:Ljava/util/List;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->listaItensValorSaldo:Ljava/util/HashMap;

    move-object v3, v7

    const-string v4, "saldo"

    .line 429
    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->setData(Ljava/util/List;Ljava/util/HashMap;Ljava/util/List;Ljava/lang/String;Z)V

    .line 431
    return-void
.end method

.method protected mostrarSaldo()V
    .registers 4

    .line 338
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v0}, Lcom/itau/empresas/ui/util/ViewUtils;->temConta(Lcom/itau/empresas/CustomApplication;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 341
    const/4 v0, 0x1

    :try_start_9
    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->setCarregando(Z)V

    .line 342
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->extratoController:Lcom/itau/empresas/controller/ExtratoController;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->application:Lcom/itau/empresas/CustomApplication;

    invoke-static {v1}, Lcom/itau/empresas/ui/util/ViewUtils;->getContaCompleta(Lcom/itau/empresas/CustomApplication;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/controller/ExtratoController;->consultaSaldo(Ljava/lang/String;)V

    .line 343
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->consultaMultiLimite()V
    :try_end_1a
    .catch Ljava/lang/NullPointerException; {:try_start_9 .. :try_end_1a} :catch_1b

    .line 347
    goto :goto_2e

    .line 344
    :catch_1b
    move-exception v2

    .line 345
    .local v2, "npex":Ljava/lang/NullPointerException;
    const-string v0, "WgtSldMultiLimiteFrag"

    invoke-virtual {v2}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 346
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/logout/LogoutHelper;->trataSessaoExpirada(Landroid/app/Activity;)V

    .line 351
    .end local v2    # "npex":Ljava/lang/NullPointerException;
    :goto_2e
    const v0, 0x7f0702d4

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 352
    const v1, 0x7f070317

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 351
    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_49

    .line 354
    :cond_40
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/logout/LogoutHelper;->trataSessaoExpirada(Landroid/app/Activity;)V

    .line 356
    :goto_49
    const v0, 0x7f0702d4

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 357
    const v1, 0x7f070317

    invoke-virtual {p0, v1}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 356
    invoke-virtual {p0, v0, v1}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 86
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/ui/fragment/BaseFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onEventMainThread(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V
    .registers 3
    .param p1, "e"    # Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;

    .line 222
    invoke-static {p1, p0}, Lcom/itau/empresas/api/ApiHelper;->devoTratarEvento(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;Lcom/itau/empresas/api/ApiConsumidor;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 223
    return-void

    .line 226
    :cond_7
    instance-of v0, p1, Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent$HTTP;

    if-eqz v0, :cond_f

    .line 227
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->mostraEstadoErro(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V

    goto :goto_12

    .line 229
    :cond_f
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->mostraMensagemErro(Lbr/com/itau/sdk/android/core/type/BackendExceptionEvent;)V

    .line 232
    :goto_12
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/api/model/LisVO;)V
    .registers 11
    .param p1, "retornoLis"    # Lcom/itau/empresas/api/model/LisVO;

    .line 141
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->dadosInternosCarregados()V

    .line 143
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 144
    .local v6, "itensTipos":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 146
    .local v7, "itensValores":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v8, 0x0

    .line 147
    .local v8, "sujeitoEncargos":Z
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteTotal()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_35

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteTotal()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_35

    .line 148
    const v0, 0x7f070664

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    .line 150
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteTotal()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 149
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    const/4 v8, 0x1

    .line 154
    :cond_35
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteAdicional()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5c

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteAdicional()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5c

    .line 155
    const v0, 0x7f070663

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    .line 157
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteAdicional()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 156
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    const/4 v8, 0x1

    .line 161
    :cond_5c
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->retornoSaldo:Lcom/itau/empresas/api/model/SaldoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/api/model/SaldoVO;->getDisponivelSaque()Ljava/lang/Double;

    move-result-object v0

    if-eqz v0, :cond_81

    .line 162
    const v0, 0x7f070667

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->retornoSaldo:Lcom/itau/empresas/api/model/SaldoVO;

    .line 164
    invoke-virtual {v0}, Lcom/itau/empresas/api/model/SaldoVO;->getDisponivelSaque()Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(DZ)Ljava/lang/String;

    move-result-object v0

    .line 163
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a7

    .line 165
    :cond_81
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteDisponivel()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a7

    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteDisponivel()Ljava/lang/String;

    move-result-object v0

    .line 166
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a7

    .line 167
    const v0, 0x7f070667

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    .line 169
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/LisVO;->getLimiteDisponivel()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 168
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    :cond_a7
    :goto_a7
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->listaItensValorSaldo:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_bb

    .line 173
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->listaItensValorSaldo:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->listaValorSaldo:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    :cond_bb
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->saldoExpandableListAdapter:Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;

    invoke-virtual {v0, p1}, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->setLisVO(Lcom/itau/empresas/api/model/LisVO;)V

    .line 177
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->saldoExpandableListAdapter:Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->listaValorSaldo:Ljava/util/List;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->listaItensValorSaldo:Ljava/util/HashMap;

    move-object v3, v7

    const-string v4, "lis"

    move v5, v8

    invoke-virtual/range {v0 .. v5}, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->setData(Ljava/util/List;Ljava/util/HashMap;Ljava/util/List;Ljava/lang/String;Z)V

    .line 178
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/activity/BaseActivity;

    invoke-virtual {v0}, Lcom/itau/empresas/ui/activity/BaseActivity;->escondeDialogoDeProgresso()V

    .line 179
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/api/model/SaldoVO;)V
    .registers 10
    .param p1, "retornoSaldo"    # Lcom/itau/empresas/api/model/SaldoVO;

    .line 182
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->retornoSaldo:Lcom/itau/empresas/api/model/SaldoVO;

    .line 183
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->listaValorSaldo:Ljava/util/List;

    .line 184
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->listaItensValorSaldo:Ljava/util/HashMap;

    .line 186
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 187
    .local v6, "itensTipos":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 189
    .local v7, "itensValores":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/itau/empresas/api/model/SaldoVO;->getSaldoDisponivelSaque()Ljava/lang/Double;

    move-result-object v0

    if-eqz v0, :cond_35

    .line 190
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->listaValorSaldo:Ljava/util/List;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->retornoSaldo:Lcom/itau/empresas/api/model/SaldoVO;

    .line 191
    invoke-virtual {v1}, Lcom/itau/empresas/api/model/SaldoVO;->getSaldoDisponivelSaque()Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object v1

    .line 190
    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3c

    .line 193
    :cond_35
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->listaValorSaldo:Ljava/util/List;

    const-string v1, "-"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    :goto_3c
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->listaItensValorSaldo:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->listaValorSaldo:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    new-instance v0, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;

    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->listaValorSaldo:Ljava/util/List;

    iget-object v3, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->listaItensValorSaldo:Ljava/util/HashMap;

    move-object v4, v7

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Ljava/util/HashMap;Ljava/util/List;Z)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->saldoExpandableListAdapter:Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;

    .line 199
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->saldoExpandableListAdapter:Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;

    new-instance v1, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$1;-><init>(Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->setFecharClickListener(Landroid/view/View$OnClickListener;)V

    .line 208
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->saldoExpandableListAdapter:Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;

    new-instance v1, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment$2;-><init>(Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;)V

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->setDetalhesLisClickListener(Landroid/view/View$OnClickListener;)V

    .line 215
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->elvSaldoMultiLimite:Landroid/widget/ExpandableListView;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->saldoExpandableListAdapter:Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ExpandableListView;->setAdapter(Landroid/widget/ExpandableListAdapter;)V

    .line 216
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->dadosSaldoCarregados()V

    .line 219
    return-void
.end method

.method public onEventMainThread(Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;)V
    .registers 10
    .param p1, "retornoMultiLimite"    # Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;

    .line 112
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->dadosInternosCarregados()V

    .line 114
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 116
    .local v6, "itensTipos":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 118
    .local v7, "itensValores":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;->getLimiteTotal()Ljava/lang/Float;

    move-result-object v0

    if-eqz v0, :cond_2d

    .line 119
    const v0, 0x7f070665

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    .line 121
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;->getLimiteTotal()Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->toString()Ljava/lang/String;

    move-result-object v0

    .line 120
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 124
    :cond_2d
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;->getLimiteDisponivel()Ljava/lang/Float;

    move-result-object v0

    if-eqz v0, :cond_4d

    .line 125
    const v0, 0x7f070667

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    .line 127
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;->getLimiteDisponivel()Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->toString()Ljava/lang/String;

    move-result-object v0

    .line 126
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    :cond_4d
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;->getLimiteTotalUtilizado()Ljava/lang/Float;

    move-result-object v0

    if-eqz v0, :cond_6d

    .line 131
    const v0, 0x7f070666

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    .line 133
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/produtos/model/MultiLimiteVO;->getLimiteTotalUtilizado()Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->toString()Ljava/lang/String;

    move-result-object v0

    .line 132
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/ValorMonetarioUtils;->formataValorDecimalParaMonetario(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    :cond_6d
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->listaItensValorSaldo:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->listaValorSaldo:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->saldoExpandableListAdapter:Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;

    iget-object v1, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->listaValorSaldo:Ljava/util/List;

    iget-object v2, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->listaItensValorSaldo:Ljava/util/HashMap;

    move-object v3, v7

    const-string v4, "multiLimites"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/itau/empresas/feature/credito/produtos/adapter/SaldoExpandableListAdapter;->setData(Ljava/util/List;Ljava/util/HashMap;Ljava/util/List;Ljava/lang/String;Z)V

    .line 138
    return-void
.end method

.method public onResume()V
    .registers 3

    .line 91
    invoke-super {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->onResume()V

    .line 93
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->preferenciaWidget:Lcom/itau/empresas/PreferenciaUtils;

    const-string v1, "widget_saldo"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/PreferenciaUtils;->get(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 94
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->setCarregando(Z)V

    .line 95
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->mostrarSaldo()V

    goto :goto_18

    .line 97
    :cond_15
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->encolherWidget()V

    .line 100
    :goto_18
    return-void
.end method

.method public onStop()V
    .registers 2

    .line 273
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/itau/empresas/feature/credito/produtos/fragment/WidgetSaldoMultiLimiteFragment;->setCarregando(Z)V

    .line 274
    invoke-super {p0}, Lcom/itau/empresas/ui/fragment/BaseFragment;->onStop()V

    .line 275
    return-void
.end method

.method public opkeysConsumidas()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation

    .line 240
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "saldo"

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "multiLimites"

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "lis"

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.class public final Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;
.super Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;
.source "EmprestimosParceladosSimulacaoActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 38
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;-><init>()V

    .line 42
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;

    .line 38
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;

    .line 38
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 58
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 59
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 60
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 61
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->emprestimosParceladosSimulacaoController:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoController;

    .line 62
    invoke-static {p0}, Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->simulacaoPropostaController:Lcom/itau/empresas/feature/credito/giro/controller/EmprestimosParceladosSimulacaoPropostaController;

    .line 63
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->injectExtras_()V

    .line 64
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->afterInject()V

    .line 65
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 106
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 107
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_4e

    .line 108
    const-string v0, "simulacaoSaidaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 109
    const-string v0, "simulacaoSaidaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->simulacaoSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/ProdutosParceladosSimulacaoSaidaVO;

    .line 111
    :cond_1c
    const-string v0, "ofertaEspecialAceitaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 112
    const-string v0, "ofertaEspecialAceitaVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->ofertaEspecialAceitaVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;

    .line 114
    :cond_2e
    const-string v0, "fluxoPersonalizado"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 115
    const-string v0, "fluxoPersonalizado"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->fluxoPersonalizado:Z

    .line 117
    :cond_3e
    const-string v0, "exibirSimulacao"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4e

    .line 118
    const-string v0, "exibirSimulacao"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->exibirSimulacao:Z

    .line 121
    :cond_4e
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 146
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$2;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 154
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 134
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_$1;-><init>(Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 142
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 51
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->init_(Landroid/os/Bundle;)V

    .line 52
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 54
    const v0, 0x7f030032

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->setContentView(I)V

    .line 55
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 99
    const v0, 0x7f0e016b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->root:Landroid/widget/LinearLayout;

    .line 100
    const v0, 0x7f0e016c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->viewCreditoDetalhado:Lcom/itau/empresas/feature/credito/giro/customview/EmprestimosParceladosDetalhadoView;

    .line 101
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 102
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->aoCarregarElementosDeTela()V

    .line 103
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 69
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->setContentView(I)V

    .line 70
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 71
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 81
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->setContentView(Landroid/view/View;)V

    .line 82
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 83
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 75
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 76
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 77
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 125
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity;->setIntent(Landroid/content/Intent;)V

    .line 126
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/activity/EmprestimosParceladosSimulacaoActivity_;->injectExtras_()V

    .line 127
    return-void
.end method

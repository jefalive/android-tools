.class public final Lorg/joda/money/format/MoneyAmountStyle;
.super Ljava/lang/Object;
.source "MoneyAmountStyle.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final ASCII_DECIMAL_COMMA_GROUP3_DOT:Lorg/joda/money/format/MoneyAmountStyle;

.field public static final ASCII_DECIMAL_COMMA_GROUP3_SPACE:Lorg/joda/money/format/MoneyAmountStyle;

.field public static final ASCII_DECIMAL_COMMA_NO_GROUPING:Lorg/joda/money/format/MoneyAmountStyle;

.field public static final ASCII_DECIMAL_POINT_GROUP3_COMMA:Lorg/joda/money/format/MoneyAmountStyle;

.field public static final ASCII_DECIMAL_POINT_GROUP3_SPACE:Lorg/joda/money/format/MoneyAmountStyle;

.field public static final ASCII_DECIMAL_POINT_NO_GROUPING:Lorg/joda/money/format/MoneyAmountStyle;

.field private static final LOCALIZED_CACHE:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap<Ljava/util/Locale;Lorg/joda/money/format/MoneyAmountStyle;>;"
        }
    .end annotation
.end field

.field public static final LOCALIZED_GROUPING:Lorg/joda/money/format/MoneyAmountStyle;

.field public static final LOCALIZED_NO_GROUPING:Lorg/joda/money/format/MoneyAmountStyle;

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final decimalPointCharacter:I

.field private final forceDecimalPoint:Z

.field private final groupingCharacter:I

.field private final groupingSize:I

.field private final groupingStyle:Lorg/joda/money/format/GroupingStyle;

.field private final negativeCharacter:I

.field private final positiveCharacter:I

.field private final zeroCharacter:I


# direct methods
.method static constructor <clinit>()V
    .registers 9

    .line 58
    new-instance v0, Lorg/joda/money/format/MoneyAmountStyle;

    sget-object v5, Lorg/joda/money/format/GroupingStyle;->FULL:Lorg/joda/money/format/GroupingStyle;

    const/16 v1, 0x30

    const/16 v2, 0x2b

    const/16 v3, 0x2d

    const/16 v4, 0x2e

    const/16 v6, 0x2c

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lorg/joda/money/format/MoneyAmountStyle;-><init>(IIIILorg/joda/money/format/GroupingStyle;IIZ)V

    sput-object v0, Lorg/joda/money/format/MoneyAmountStyle;->ASCII_DECIMAL_POINT_GROUP3_COMMA:Lorg/joda/money/format/MoneyAmountStyle;

    .line 65
    new-instance v0, Lorg/joda/money/format/MoneyAmountStyle;

    sget-object v5, Lorg/joda/money/format/GroupingStyle;->FULL:Lorg/joda/money/format/GroupingStyle;

    const/16 v1, 0x30

    const/16 v2, 0x2b

    const/16 v3, 0x2d

    const/16 v4, 0x2e

    const/16 v6, 0x20

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lorg/joda/money/format/MoneyAmountStyle;-><init>(IIIILorg/joda/money/format/GroupingStyle;IIZ)V

    sput-object v0, Lorg/joda/money/format/MoneyAmountStyle;->ASCII_DECIMAL_POINT_GROUP3_SPACE:Lorg/joda/money/format/MoneyAmountStyle;

    .line 72
    new-instance v0, Lorg/joda/money/format/MoneyAmountStyle;

    sget-object v5, Lorg/joda/money/format/GroupingStyle;->NONE:Lorg/joda/money/format/GroupingStyle;

    const/16 v1, 0x30

    const/16 v2, 0x2b

    const/16 v3, 0x2d

    const/16 v4, 0x2e

    const/16 v6, 0x2c

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lorg/joda/money/format/MoneyAmountStyle;-><init>(IIIILorg/joda/money/format/GroupingStyle;IIZ)V

    sput-object v0, Lorg/joda/money/format/MoneyAmountStyle;->ASCII_DECIMAL_POINT_NO_GROUPING:Lorg/joda/money/format/MoneyAmountStyle;

    .line 79
    new-instance v0, Lorg/joda/money/format/MoneyAmountStyle;

    sget-object v5, Lorg/joda/money/format/GroupingStyle;->FULL:Lorg/joda/money/format/GroupingStyle;

    const/16 v1, 0x30

    const/16 v2, 0x2b

    const/16 v3, 0x2d

    const/16 v4, 0x2c

    const/16 v6, 0x2e

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lorg/joda/money/format/MoneyAmountStyle;-><init>(IIIILorg/joda/money/format/GroupingStyle;IIZ)V

    sput-object v0, Lorg/joda/money/format/MoneyAmountStyle;->ASCII_DECIMAL_COMMA_GROUP3_DOT:Lorg/joda/money/format/MoneyAmountStyle;

    .line 86
    new-instance v0, Lorg/joda/money/format/MoneyAmountStyle;

    sget-object v5, Lorg/joda/money/format/GroupingStyle;->FULL:Lorg/joda/money/format/GroupingStyle;

    const/16 v1, 0x30

    const/16 v2, 0x2b

    const/16 v3, 0x2d

    const/16 v4, 0x2c

    const/16 v6, 0x20

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lorg/joda/money/format/MoneyAmountStyle;-><init>(IIIILorg/joda/money/format/GroupingStyle;IIZ)V

    sput-object v0, Lorg/joda/money/format/MoneyAmountStyle;->ASCII_DECIMAL_COMMA_GROUP3_SPACE:Lorg/joda/money/format/MoneyAmountStyle;

    .line 93
    new-instance v0, Lorg/joda/money/format/MoneyAmountStyle;

    sget-object v5, Lorg/joda/money/format/GroupingStyle;->NONE:Lorg/joda/money/format/GroupingStyle;

    const/16 v1, 0x30

    const/16 v2, 0x2b

    const/16 v3, 0x2d

    const/16 v4, 0x2c

    const/16 v6, 0x2e

    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lorg/joda/money/format/MoneyAmountStyle;-><init>(IIIILorg/joda/money/format/GroupingStyle;IIZ)V

    sput-object v0, Lorg/joda/money/format/MoneyAmountStyle;->ASCII_DECIMAL_COMMA_NO_GROUPING:Lorg/joda/money/format/MoneyAmountStyle;

    .line 99
    new-instance v0, Lorg/joda/money/format/MoneyAmountStyle;

    sget-object v5, Lorg/joda/money/format/GroupingStyle;->FULL:Lorg/joda/money/format/GroupingStyle;

    const/4 v1, -0x1

    const/4 v2, -0x1

    const/4 v3, -0x1

    const/4 v4, -0x1

    const/4 v6, -0x1

    const/4 v7, -0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lorg/joda/money/format/MoneyAmountStyle;-><init>(IIIILorg/joda/money/format/GroupingStyle;IIZ)V

    sput-object v0, Lorg/joda/money/format/MoneyAmountStyle;->LOCALIZED_GROUPING:Lorg/joda/money/format/MoneyAmountStyle;

    .line 105
    new-instance v0, Lorg/joda/money/format/MoneyAmountStyle;

    sget-object v5, Lorg/joda/money/format/GroupingStyle;->NONE:Lorg/joda/money/format/GroupingStyle;

    const/4 v1, -0x1

    const/4 v2, -0x1

    const/4 v3, -0x1

    const/4 v4, -0x1

    const/4 v6, -0x1

    const/4 v7, -0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lorg/joda/money/format/MoneyAmountStyle;-><init>(IIIILorg/joda/money/format/GroupingStyle;IIZ)V

    sput-object v0, Lorg/joda/money/format/MoneyAmountStyle;->LOCALIZED_NO_GROUPING:Lorg/joda/money/format/MoneyAmountStyle;

    .line 110
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lorg/joda/money/format/MoneyAmountStyle;->LOCALIZED_CACHE:Ljava/util/concurrent/ConcurrentMap;

    return-void
.end method

.method private constructor <init>(IIIILorg/joda/money/format/GroupingStyle;IIZ)V
    .registers 9
    .param p1, "zeroCharacter"    # I
    .param p2, "positiveCharacter"    # I
    .param p3, "negativeCharacter"    # I
    .param p4, "decimalPointCharacter"    # I
    .param p5, "groupingStyle"    # Lorg/joda/money/format/GroupingStyle;
    .param p6, "groupingCharacter"    # I
    .param p7, "groupingSize"    # I
    .param p8, "forceDecimalPoint"    # Z

    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    iput p1, p0, Lorg/joda/money/format/MoneyAmountStyle;->zeroCharacter:I

    .line 182
    iput p2, p0, Lorg/joda/money/format/MoneyAmountStyle;->positiveCharacter:I

    .line 183
    iput p3, p0, Lorg/joda/money/format/MoneyAmountStyle;->negativeCharacter:I

    .line 184
    iput p4, p0, Lorg/joda/money/format/MoneyAmountStyle;->decimalPointCharacter:I

    .line 185
    iput-object p5, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingStyle:Lorg/joda/money/format/GroupingStyle;

    .line 186
    iput p6, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingCharacter:I

    .line 187
    iput p7, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingSize:I

    .line 188
    iput-boolean p8, p0, Lorg/joda/money/format/MoneyAmountStyle;->forceDecimalPoint:Z

    .line 189
    return-void
.end method

.method private static getLocalizedStyle(Ljava/util/Locale;)Lorg/joda/money/format/MoneyAmountStyle;
    .registers 14
    .param p0, "locale"    # Ljava/util/Locale;

    .line 251
    sget-object v0, Lorg/joda/money/format/MoneyAmountStyle;->LOCALIZED_CACHE:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lorg/joda/money/format/MoneyAmountStyle;

    .line 252
    .local v9, "protoStyle":Lorg/joda/money/format/MoneyAmountStyle;
    if-nez v9, :cond_62

    .line 255
    :try_start_b
    const-class v0, Ljava/text/DecimalFormatSymbols;

    const-string v1, "getInstance"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/util/Locale;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v11

    .line 256
    .local v11, "method":Ljava/lang/reflect/Method;
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x0

    invoke-virtual {v11, v1, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Ljava/text/DecimalFormatSymbols;
    :try_end_29
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_29} :catch_2a

    .line 259
    .local v10, "symbols":Ljava/text/DecimalFormatSymbols;
    .end local v11    # "method":Ljava/lang/reflect/Method;
    goto :goto_30

    .line 257
    :catch_2a
    move-exception v11

    .line 258
    .local v11, "ex":Ljava/lang/Exception;
    new-instance v10, Ljava/text/DecimalFormatSymbols;

    invoke-direct {v10, p0}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    .line 260
    .end local v11    # "ex":Ljava/lang/Exception;
    :goto_30
    invoke-static {p0}, Ljava/text/NumberFormat;->getCurrencyInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v11

    .line 261
    .local v11, "format":Ljava/text/NumberFormat;
    instance-of v0, v11, Ljava/text/DecimalFormat;

    if-eqz v0, :cond_40

    move-object v0, v11

    check-cast v0, Ljava/text/DecimalFormat;

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getGroupingSize()I

    move-result v12

    goto :goto_41

    :cond_40
    const/4 v12, 0x3

    .line 262
    .local v12, "size":I
    :goto_41
    new-instance v0, Lorg/joda/money/format/MoneyAmountStyle;

    invoke-virtual {v10}, Ljava/text/DecimalFormatSymbols;->getZeroDigit()C

    move-result v1

    invoke-virtual {v10}, Ljava/text/DecimalFormatSymbols;->getMinusSign()C

    move-result v3

    invoke-virtual {v10}, Ljava/text/DecimalFormatSymbols;->getMonetaryDecimalSeparator()C

    move-result v4

    sget-object v5, Lorg/joda/money/format/GroupingStyle;->FULL:Lorg/joda/money/format/GroupingStyle;

    invoke-virtual {v10}, Ljava/text/DecimalFormatSymbols;->getGroupingSeparator()C

    move-result v6

    move v7, v12

    const/16 v2, 0x2b

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lorg/joda/money/format/MoneyAmountStyle;-><init>(IIIILorg/joda/money/format/GroupingStyle;IIZ)V

    move-object v9, v0

    .line 267
    sget-object v0, Lorg/joda/money/format/MoneyAmountStyle;->LOCALIZED_CACHE:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0, v9}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    .end local v10    # "symbols":Ljava/text/DecimalFormatSymbols;
    .end local v11    # "format":Ljava/text/NumberFormat;
    .end local v12    # "size":I
    :cond_62
    return-object v9
.end method

.method public static of(Ljava/util/Locale;)Lorg/joda/money/format/MoneyAmountStyle;
    .registers 2
    .param p0, "locale"    # Ljava/util/Locale;

    .line 160
    invoke-static {p0}, Lorg/joda/money/format/MoneyAmountStyle;->getLocalizedStyle(Ljava/util/Locale;)Lorg/joda/money/format/MoneyAmountStyle;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1, "other"    # Ljava/lang/Object;

    .line 556
    if-ne p1, p0, :cond_4

    .line 557
    const/4 v0, 0x1

    return v0

    .line 559
    :cond_4
    instance-of v0, p1, Lorg/joda/money/format/MoneyAmountStyle;

    if-nez v0, :cond_a

    .line 560
    const/4 v0, 0x0

    return v0

    .line 562
    :cond_a
    move-object v2, p1

    check-cast v2, Lorg/joda/money/format/MoneyAmountStyle;

    .line 563
    .local v2, "otherStyle":Lorg/joda/money/format/MoneyAmountStyle;
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->zeroCharacter:I

    iget v1, v2, Lorg/joda/money/format/MoneyAmountStyle;->zeroCharacter:I

    if-ne v0, v1, :cond_3f

    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->positiveCharacter:I

    iget v1, v2, Lorg/joda/money/format/MoneyAmountStyle;->positiveCharacter:I

    if-ne v0, v1, :cond_3f

    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->negativeCharacter:I

    iget v1, v2, Lorg/joda/money/format/MoneyAmountStyle;->negativeCharacter:I

    if-ne v0, v1, :cond_3f

    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->decimalPointCharacter:I

    iget v1, v2, Lorg/joda/money/format/MoneyAmountStyle;->decimalPointCharacter:I

    if-ne v0, v1, :cond_3f

    iget-object v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingStyle:Lorg/joda/money/format/GroupingStyle;

    iget-object v1, v2, Lorg/joda/money/format/MoneyAmountStyle;->groupingStyle:Lorg/joda/money/format/GroupingStyle;

    if-ne v0, v1, :cond_3f

    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingCharacter:I

    iget v1, v2, Lorg/joda/money/format/MoneyAmountStyle;->groupingCharacter:I

    if-ne v0, v1, :cond_3f

    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingSize:I

    iget v1, v2, Lorg/joda/money/format/MoneyAmountStyle;->groupingSize:I

    if-ne v0, v1, :cond_3f

    iget-boolean v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->forceDecimalPoint:Z

    iget-boolean v1, v2, Lorg/joda/money/format/MoneyAmountStyle;->forceDecimalPoint:Z

    if-ne v0, v1, :cond_3f

    const/4 v0, 0x1

    goto :goto_40

    :cond_3f
    const/4 v0, 0x0

    :goto_40
    return v0
.end method

.method public getDecimalPointCharacter()Ljava/lang/Character;
    .registers 2

    .line 382
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->decimalPointCharacter:I

    if-gez v0, :cond_6

    const/4 v0, 0x0

    goto :goto_d

    :cond_6
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->decimalPointCharacter:I

    int-to-char v0, v0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    :goto_d
    return-object v0
.end method

.method public getGroupingCharacter()Ljava/lang/Character;
    .registers 2

    .line 412
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingCharacter:I

    if-gez v0, :cond_6

    const/4 v0, 0x0

    goto :goto_d

    :cond_6
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingCharacter:I

    int-to-char v0, v0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    :goto_d
    return-object v0
.end method

.method public getGroupingSize()Ljava/lang/Integer;
    .registers 2

    .line 442
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingSize:I

    if-gez v0, :cond_6

    const/4 v0, 0x0

    goto :goto_c

    :cond_6
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingSize:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_c
    return-object v0
.end method

.method public getGroupingStyle()Lorg/joda/money/format/GroupingStyle;
    .registers 2

    .line 499
    iget-object v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingStyle:Lorg/joda/money/format/GroupingStyle;

    return-object v0
.end method

.method public getNegativeSignCharacter()Ljava/lang/Character;
    .registers 2

    .line 352
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->negativeCharacter:I

    if-gez v0, :cond_6

    const/4 v0, 0x0

    goto :goto_d

    :cond_6
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->negativeCharacter:I

    int-to-char v0, v0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    :goto_d
    return-object v0
.end method

.method public getPositiveSignCharacter()Ljava/lang/Character;
    .registers 2

    .line 320
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->positiveCharacter:I

    if-gez v0, :cond_6

    const/4 v0, 0x0

    goto :goto_d

    :cond_6
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->positiveCharacter:I

    int-to-char v0, v0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    :goto_d
    return-object v0
.end method

.method public getZeroCharacter()Ljava/lang/Character;
    .registers 2

    .line 283
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->zeroCharacter:I

    if-gez v0, :cond_6

    const/4 v0, 0x0

    goto :goto_d

    :cond_6
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->zeroCharacter:I

    int-to-char v0, v0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v0

    :goto_d
    return-object v0
.end method

.method public hashCode()I
    .registers 4

    .line 580
    const/16 v2, 0xd

    .line 581
    .local v2, "hash":I
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->zeroCharacter:I

    mul-int/lit8 v0, v0, 0x11

    add-int/lit8 v2, v0, 0xd

    .line 582
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->positiveCharacter:I

    mul-int/lit8 v0, v0, 0x11

    add-int/2addr v2, v0

    .line 583
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->negativeCharacter:I

    mul-int/lit8 v0, v0, 0x11

    add-int/2addr v2, v0

    .line 584
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->decimalPointCharacter:I

    mul-int/lit8 v0, v0, 0x11

    add-int/2addr v2, v0

    .line 585
    iget-object v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingStyle:Lorg/joda/money/format/GroupingStyle;

    invoke-virtual {v0}, Lorg/joda/money/format/GroupingStyle;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x11

    add-int/2addr v2, v0

    .line 586
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingCharacter:I

    mul-int/lit8 v0, v0, 0x11

    add-int/2addr v2, v0

    .line 587
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingSize:I

    mul-int/lit8 v0, v0, 0x11

    add-int/2addr v2, v0

    .line 588
    iget-boolean v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->forceDecimalPoint:Z

    if-eqz v0, :cond_30

    const/4 v0, 0x2

    goto :goto_31

    :cond_30
    const/4 v0, 0x4

    :goto_31
    add-int/2addr v2, v0

    .line 589
    return v2
.end method

.method public isForcedDecimalPoint()Z
    .registers 2

    .line 527
    iget-boolean v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->forceDecimalPoint:Z

    return v0
.end method

.method public isGrouping()Z
    .registers 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 477
    invoke-virtual {p0}, Lorg/joda/money/format/MoneyAmountStyle;->getGroupingStyle()Lorg/joda/money/format/GroupingStyle;

    move-result-object v0

    sget-object v1, Lorg/joda/money/format/GroupingStyle;->FULL:Lorg/joda/money/format/GroupingStyle;

    if-ne v0, v1, :cond_a

    const/4 v0, 0x1

    goto :goto_b

    :cond_a
    const/4 v0, 0x0

    :goto_b
    return v0
.end method

.method public localize(Ljava/util/Locale;)Lorg/joda/money/format/MoneyAmountStyle;
    .registers 5
    .param p1, "locale"    # Ljava/util/Locale;

    .line 207
    const-string v0, "Locale must not be null"

    invoke-static {p1, v0}, Lorg/joda/money/format/MoneyFormatter;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 208
    move-object v1, p0

    .line 209
    .local v1, "result":Lorg/joda/money/format/MoneyAmountStyle;
    const/4 v2, 0x0

    .line 210
    .local v2, "protoStyle":Lorg/joda/money/format/MoneyAmountStyle;
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->zeroCharacter:I

    if-gez v0, :cond_17

    .line 211
    invoke-static {p1}, Lorg/joda/money/format/MoneyAmountStyle;->getLocalizedStyle(Ljava/util/Locale;)Lorg/joda/money/format/MoneyAmountStyle;

    move-result-object v2

    .line 212
    invoke-virtual {v2}, Lorg/joda/money/format/MoneyAmountStyle;->getZeroCharacter()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/joda/money/format/MoneyAmountStyle;->withZeroCharacter(Ljava/lang/Character;)Lorg/joda/money/format/MoneyAmountStyle;

    move-result-object v1

    .line 214
    :cond_17
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->positiveCharacter:I

    if-gez v0, :cond_27

    .line 215
    invoke-static {p1}, Lorg/joda/money/format/MoneyAmountStyle;->getLocalizedStyle(Ljava/util/Locale;)Lorg/joda/money/format/MoneyAmountStyle;

    move-result-object v2

    .line 216
    invoke-virtual {v2}, Lorg/joda/money/format/MoneyAmountStyle;->getPositiveSignCharacter()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/joda/money/format/MoneyAmountStyle;->withPositiveSignCharacter(Ljava/lang/Character;)Lorg/joda/money/format/MoneyAmountStyle;

    move-result-object v1

    .line 218
    :cond_27
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->negativeCharacter:I

    if-gez v0, :cond_37

    .line 219
    invoke-static {p1}, Lorg/joda/money/format/MoneyAmountStyle;->getLocalizedStyle(Ljava/util/Locale;)Lorg/joda/money/format/MoneyAmountStyle;

    move-result-object v2

    .line 220
    invoke-virtual {v2}, Lorg/joda/money/format/MoneyAmountStyle;->getNegativeSignCharacter()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/joda/money/format/MoneyAmountStyle;->withNegativeSignCharacter(Ljava/lang/Character;)Lorg/joda/money/format/MoneyAmountStyle;

    move-result-object v1

    .line 222
    :cond_37
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->decimalPointCharacter:I

    if-gez v0, :cond_4c

    .line 223
    if-nez v2, :cond_42

    invoke-static {p1}, Lorg/joda/money/format/MoneyAmountStyle;->getLocalizedStyle(Ljava/util/Locale;)Lorg/joda/money/format/MoneyAmountStyle;

    move-result-object v0

    goto :goto_43

    :cond_42
    move-object v0, v2

    :goto_43
    move-object v2, v0

    .line 224
    invoke-virtual {v2}, Lorg/joda/money/format/MoneyAmountStyle;->getDecimalPointCharacter()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/joda/money/format/MoneyAmountStyle;->withDecimalPointCharacter(Ljava/lang/Character;)Lorg/joda/money/format/MoneyAmountStyle;

    move-result-object v1

    .line 226
    :cond_4c
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingCharacter:I

    if-gez v0, :cond_61

    .line 227
    if-nez v2, :cond_57

    invoke-static {p1}, Lorg/joda/money/format/MoneyAmountStyle;->getLocalizedStyle(Ljava/util/Locale;)Lorg/joda/money/format/MoneyAmountStyle;

    move-result-object v0

    goto :goto_58

    :cond_57
    move-object v0, v2

    :goto_58
    move-object v2, v0

    .line 228
    invoke-virtual {v2}, Lorg/joda/money/format/MoneyAmountStyle;->getGroupingCharacter()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/joda/money/format/MoneyAmountStyle;->withGroupingCharacter(Ljava/lang/Character;)Lorg/joda/money/format/MoneyAmountStyle;

    move-result-object v1

    .line 230
    :cond_61
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingSize:I

    if-gez v0, :cond_76

    .line 231
    if-nez v2, :cond_6c

    invoke-static {p1}, Lorg/joda/money/format/MoneyAmountStyle;->getLocalizedStyle(Ljava/util/Locale;)Lorg/joda/money/format/MoneyAmountStyle;

    move-result-object v0

    goto :goto_6d

    :cond_6c
    move-object v0, v2

    :goto_6d
    move-object v2, v0

    .line 232
    invoke-virtual {v2}, Lorg/joda/money/format/MoneyAmountStyle;->getGroupingSize()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/joda/money/format/MoneyAmountStyle;->withGroupingSize(Ljava/lang/Integer;)Lorg/joda/money/format/MoneyAmountStyle;

    move-result-object v1

    .line 234
    :cond_76
    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 600
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MoneyAmountStyle[\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/joda/money/format/MoneyAmountStyle;->getZeroCharacter()Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\',\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/joda/money/format/MoneyAmountStyle;->getPositiveSignCharacter()Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\',\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/joda/money/format/MoneyAmountStyle;->getNegativeSignCharacter()Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\',\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/joda/money/format/MoneyAmountStyle;->getDecimalPointCharacter()Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\',\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/joda/money/format/MoneyAmountStyle;->getGroupingStyle()Lorg/joda/money/format/GroupingStyle;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/joda/money/format/MoneyAmountStyle;->getGroupingCharacter()Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\',\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/joda/money/format/MoneyAmountStyle;->getGroupingSize()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\',"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/joda/money/format/MoneyAmountStyle;->isForcedDecimalPoint()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public withDecimalPointCharacter(Ljava/lang/Character;)Lorg/joda/money/format/MoneyAmountStyle;
    .registers 12
    .param p1, "decimalPointCharacter"    # Ljava/lang/Character;

    .line 394
    if-nez p1, :cond_4

    const/4 v9, -0x1

    goto :goto_8

    :cond_4
    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v9

    .line 395
    .local v9, "dpVal":I
    :goto_8
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->decimalPointCharacter:I

    if-ne v9, v0, :cond_d

    .line 396
    return-object p0

    .line 398
    :cond_d
    new-instance v0, Lorg/joda/money/format/MoneyAmountStyle;

    iget v1, p0, Lorg/joda/money/format/MoneyAmountStyle;->zeroCharacter:I

    iget v2, p0, Lorg/joda/money/format/MoneyAmountStyle;->positiveCharacter:I

    iget v3, p0, Lorg/joda/money/format/MoneyAmountStyle;->negativeCharacter:I

    iget-object v5, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingStyle:Lorg/joda/money/format/GroupingStyle;

    iget v6, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingCharacter:I

    iget v7, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingSize:I

    iget-boolean v8, p0, Lorg/joda/money/format/MoneyAmountStyle;->forceDecimalPoint:Z

    move v4, v9

    invoke-direct/range {v0 .. v8}, Lorg/joda/money/format/MoneyAmountStyle;-><init>(IIIILorg/joda/money/format/GroupingStyle;IIZ)V

    return-object v0
.end method

.method public withForcedDecimalPoint(Z)Lorg/joda/money/format/MoneyAmountStyle;
    .registers 11
    .param p1, "forceDecimalPoint"    # Z

    .line 537
    iget-boolean v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->forceDecimalPoint:Z

    if-ne v0, p1, :cond_5

    .line 538
    return-object p0

    .line 540
    :cond_5
    new-instance v0, Lorg/joda/money/format/MoneyAmountStyle;

    iget v1, p0, Lorg/joda/money/format/MoneyAmountStyle;->zeroCharacter:I

    iget v2, p0, Lorg/joda/money/format/MoneyAmountStyle;->positiveCharacter:I

    iget v3, p0, Lorg/joda/money/format/MoneyAmountStyle;->negativeCharacter:I

    iget v4, p0, Lorg/joda/money/format/MoneyAmountStyle;->decimalPointCharacter:I

    iget-object v5, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingStyle:Lorg/joda/money/format/GroupingStyle;

    iget v6, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingCharacter:I

    iget v7, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingSize:I

    move v8, p1

    invoke-direct/range {v0 .. v8}, Lorg/joda/money/format/MoneyAmountStyle;-><init>(IIIILorg/joda/money/format/GroupingStyle;IIZ)V

    return-object v0
.end method

.method public withGrouping(Z)Lorg/joda/money/format/MoneyAmountStyle;
    .registers 3
    .param p1, "grouping"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 489
    if-eqz p1, :cond_5

    sget-object v0, Lorg/joda/money/format/GroupingStyle;->FULL:Lorg/joda/money/format/GroupingStyle;

    goto :goto_7

    :cond_5
    sget-object v0, Lorg/joda/money/format/GroupingStyle;->NONE:Lorg/joda/money/format/GroupingStyle;

    :goto_7
    invoke-virtual {p0, v0}, Lorg/joda/money/format/MoneyAmountStyle;->withGroupingStyle(Lorg/joda/money/format/GroupingStyle;)Lorg/joda/money/format/MoneyAmountStyle;

    move-result-object v0

    return-object v0
.end method

.method public withGroupingCharacter(Ljava/lang/Character;)Lorg/joda/money/format/MoneyAmountStyle;
    .registers 12
    .param p1, "groupingCharacter"    # Ljava/lang/Character;

    .line 424
    if-nez p1, :cond_4

    const/4 v9, -0x1

    goto :goto_8

    :cond_4
    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v9

    .line 425
    .local v9, "groupingVal":I
    :goto_8
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingCharacter:I

    if-ne v9, v0, :cond_d

    .line 426
    return-object p0

    .line 428
    :cond_d
    new-instance v0, Lorg/joda/money/format/MoneyAmountStyle;

    iget v1, p0, Lorg/joda/money/format/MoneyAmountStyle;->zeroCharacter:I

    iget v2, p0, Lorg/joda/money/format/MoneyAmountStyle;->positiveCharacter:I

    iget v3, p0, Lorg/joda/money/format/MoneyAmountStyle;->negativeCharacter:I

    iget v4, p0, Lorg/joda/money/format/MoneyAmountStyle;->decimalPointCharacter:I

    iget-object v5, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingStyle:Lorg/joda/money/format/GroupingStyle;

    iget v7, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingSize:I

    iget-boolean v8, p0, Lorg/joda/money/format/MoneyAmountStyle;->forceDecimalPoint:Z

    move v6, v9

    invoke-direct/range {v0 .. v8}, Lorg/joda/money/format/MoneyAmountStyle;-><init>(IIIILorg/joda/money/format/GroupingStyle;IIZ)V

    return-object v0
.end method

.method public withGroupingSize(Ljava/lang/Integer;)Lorg/joda/money/format/MoneyAmountStyle;
    .registers 12
    .param p1, "groupingSize"    # Ljava/lang/Integer;

    .line 454
    if-nez p1, :cond_4

    const/4 v9, -0x1

    goto :goto_8

    :cond_4
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 455
    .local v9, "sizeVal":I
    :goto_8
    if-eqz p1, :cond_14

    if-gtz v9, :cond_14

    .line 456
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Grouping size must be greater than zero"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 458
    :cond_14
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingSize:I

    if-ne v9, v0, :cond_19

    .line 459
    return-object p0

    .line 461
    :cond_19
    new-instance v0, Lorg/joda/money/format/MoneyAmountStyle;

    iget v1, p0, Lorg/joda/money/format/MoneyAmountStyle;->zeroCharacter:I

    iget v2, p0, Lorg/joda/money/format/MoneyAmountStyle;->positiveCharacter:I

    iget v3, p0, Lorg/joda/money/format/MoneyAmountStyle;->negativeCharacter:I

    iget v4, p0, Lorg/joda/money/format/MoneyAmountStyle;->decimalPointCharacter:I

    iget-object v5, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingStyle:Lorg/joda/money/format/GroupingStyle;

    iget v6, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingCharacter:I

    iget-boolean v8, p0, Lorg/joda/money/format/MoneyAmountStyle;->forceDecimalPoint:Z

    move v7, v9

    invoke-direct/range {v0 .. v8}, Lorg/joda/money/format/MoneyAmountStyle;-><init>(IIIILorg/joda/money/format/GroupingStyle;IIZ)V

    return-object v0
.end method

.method public withGroupingStyle(Lorg/joda/money/format/GroupingStyle;)Lorg/joda/money/format/MoneyAmountStyle;
    .registers 11
    .param p1, "groupingStyle"    # Lorg/joda/money/format/GroupingStyle;

    .line 509
    const-string v0, "groupingStyle"

    invoke-static {p1, v0}, Lorg/joda/money/format/MoneyFormatter;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 510
    iget-object v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingStyle:Lorg/joda/money/format/GroupingStyle;

    if-ne v0, p1, :cond_a

    .line 511
    return-object p0

    .line 513
    :cond_a
    new-instance v0, Lorg/joda/money/format/MoneyAmountStyle;

    iget v1, p0, Lorg/joda/money/format/MoneyAmountStyle;->zeroCharacter:I

    iget v2, p0, Lorg/joda/money/format/MoneyAmountStyle;->positiveCharacter:I

    iget v3, p0, Lorg/joda/money/format/MoneyAmountStyle;->negativeCharacter:I

    iget v4, p0, Lorg/joda/money/format/MoneyAmountStyle;->decimalPointCharacter:I

    iget v6, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingCharacter:I

    iget v7, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingSize:I

    iget-boolean v8, p0, Lorg/joda/money/format/MoneyAmountStyle;->forceDecimalPoint:Z

    move-object v5, p1

    invoke-direct/range {v0 .. v8}, Lorg/joda/money/format/MoneyAmountStyle;-><init>(IIIILorg/joda/money/format/GroupingStyle;IIZ)V

    return-object v0
.end method

.method public withNegativeSignCharacter(Ljava/lang/Character;)Lorg/joda/money/format/MoneyAmountStyle;
    .registers 12
    .param p1, "negativeCharacter"    # Ljava/lang/Character;

    .line 364
    if-nez p1, :cond_4

    const/4 v9, -0x1

    goto :goto_8

    :cond_4
    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v9

    .line 365
    .local v9, "negativeVal":I
    :goto_8
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->negativeCharacter:I

    if-ne v9, v0, :cond_d

    .line 366
    return-object p0

    .line 368
    :cond_d
    new-instance v0, Lorg/joda/money/format/MoneyAmountStyle;

    iget v1, p0, Lorg/joda/money/format/MoneyAmountStyle;->zeroCharacter:I

    iget v2, p0, Lorg/joda/money/format/MoneyAmountStyle;->positiveCharacter:I

    iget v4, p0, Lorg/joda/money/format/MoneyAmountStyle;->decimalPointCharacter:I

    iget-object v5, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingStyle:Lorg/joda/money/format/GroupingStyle;

    iget v6, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingCharacter:I

    iget v7, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingSize:I

    iget-boolean v8, p0, Lorg/joda/money/format/MoneyAmountStyle;->forceDecimalPoint:Z

    move v3, v9

    invoke-direct/range {v0 .. v8}, Lorg/joda/money/format/MoneyAmountStyle;-><init>(IIIILorg/joda/money/format/GroupingStyle;IIZ)V

    return-object v0
.end method

.method public withPositiveSignCharacter(Ljava/lang/Character;)Lorg/joda/money/format/MoneyAmountStyle;
    .registers 12
    .param p1, "positiveCharacter"    # Ljava/lang/Character;

    .line 332
    if-nez p1, :cond_4

    const/4 v9, -0x1

    goto :goto_8

    :cond_4
    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v9

    .line 333
    .local v9, "positiveVal":I
    :goto_8
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->positiveCharacter:I

    if-ne v9, v0, :cond_d

    .line 334
    return-object p0

    .line 336
    :cond_d
    new-instance v0, Lorg/joda/money/format/MoneyAmountStyle;

    iget v1, p0, Lorg/joda/money/format/MoneyAmountStyle;->zeroCharacter:I

    iget v3, p0, Lorg/joda/money/format/MoneyAmountStyle;->negativeCharacter:I

    iget v4, p0, Lorg/joda/money/format/MoneyAmountStyle;->decimalPointCharacter:I

    iget-object v5, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingStyle:Lorg/joda/money/format/GroupingStyle;

    iget v6, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingCharacter:I

    iget v7, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingSize:I

    iget-boolean v8, p0, Lorg/joda/money/format/MoneyAmountStyle;->forceDecimalPoint:Z

    move v2, v9

    invoke-direct/range {v0 .. v8}, Lorg/joda/money/format/MoneyAmountStyle;-><init>(IIIILorg/joda/money/format/GroupingStyle;IIZ)V

    return-object v0
.end method

.method public withZeroCharacter(Ljava/lang/Character;)Lorg/joda/money/format/MoneyAmountStyle;
    .registers 12
    .param p1, "zeroCharacter"    # Ljava/lang/Character;

    .line 300
    if-nez p1, :cond_4

    const/4 v9, -0x1

    goto :goto_8

    :cond_4
    invoke-virtual {p1}, Ljava/lang/Character;->charValue()C

    move-result v9

    .line 301
    .local v9, "zeroVal":I
    :goto_8
    iget v0, p0, Lorg/joda/money/format/MoneyAmountStyle;->zeroCharacter:I

    if-ne v9, v0, :cond_d

    .line 302
    return-object p0

    .line 304
    :cond_d
    new-instance v0, Lorg/joda/money/format/MoneyAmountStyle;

    iget v2, p0, Lorg/joda/money/format/MoneyAmountStyle;->positiveCharacter:I

    iget v3, p0, Lorg/joda/money/format/MoneyAmountStyle;->negativeCharacter:I

    iget v4, p0, Lorg/joda/money/format/MoneyAmountStyle;->decimalPointCharacter:I

    iget-object v5, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingStyle:Lorg/joda/money/format/GroupingStyle;

    iget v6, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingCharacter:I

    iget v7, p0, Lorg/joda/money/format/MoneyAmountStyle;->groupingSize:I

    iget-boolean v8, p0, Lorg/joda/money/format/MoneyAmountStyle;->forceDecimalPoint:Z

    move v1, v9

    invoke-direct/range {v0 .. v8}, Lorg/joda/money/format/MoneyAmountStyle;-><init>(IIIILorg/joda/money/format/GroupingStyle;IIZ)V

    return-object v0
.end method

.class Lcom/itau/empresas/feature/login/LoginActivity$4;
.super Lcom/itau/empresas/ui/util/SimpleTextWatcher;
.source "LoginActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/login/LoginActivity;->criaTextWatcherComAcessibilidade()Lcom/itau/empresas/ui/util/SimpleTextWatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/login/LoginActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/login/LoginActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/login/LoginActivity;

    .line 453
    iput-object p1, p0, Lcom/itau/empresas/feature/login/LoginActivity$4;->this$0:Lcom/itau/empresas/feature/login/LoginActivity;

    invoke-direct {p0}, Lcom/itau/empresas/ui/util/SimpleTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 5
    .param p1, "s"    # Landroid/text/Editable;

    .line 456
    invoke-super {p0, p1}, Lcom/itau/empresas/ui/util/SimpleTextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    .line 457
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1a

    .line 458
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity$4;->this$0:Lcom/itau/empresas/feature/login/LoginActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/login/LoginActivity;->textoLoginSenha:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/LoginActivity$4;->this$0:Lcom/itau/empresas/feature/login/LoginActivity;

    .line 459
    const v2, 0x7f070452

    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/login/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 458
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    goto :goto_28

    .line 461
    :cond_1a
    iget-object v0, p0, Lcom/itau/empresas/feature/login/LoginActivity$4;->this$0:Lcom/itau/empresas/feature/login/LoginActivity;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/LoginActivity$4;->this$0:Lcom/itau/empresas/feature/login/LoginActivity;

    .line 462
    const v2, 0x7f070452

    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/login/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 461
    invoke-static {v0, v1}, Lcom/itau/empresas/ui/util/acessibilidade/AcessibilidadeUtils;->textToSpeech(Landroid/content/Context;Ljava/lang/String;)V

    .line 464
    :goto_28
    return-void
.end method

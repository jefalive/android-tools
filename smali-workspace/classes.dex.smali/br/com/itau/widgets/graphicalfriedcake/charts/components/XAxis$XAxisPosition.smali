.class public final enum Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;
.super Ljava/lang/Enum;
.source "XAxis.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "XAxisPosition"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;>;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

.field public static final enum BOTH_SIDED:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

.field public static final enum BOTTOM:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

.field public static final enum BOTTOM_INSIDE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

.field public static final enum TOP:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

.field public static final enum TOP_INSIDE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

.field public static final enum XAxisPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;


# direct methods
.method static constructor <clinit>()V
    .registers 3

    .line 78
    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    const-string v1, "TOP"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->TOP:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    const-string v1, "BOTTOM"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->BOTTOM:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    const-string v1, "BOTH_SIDED"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->BOTH_SIDED:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    const-string v1, "TOP_INSIDE"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->TOP_INSIDE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    const-string v1, "XAxisPosition"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->XAxisPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    new-instance v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    const-string v1, "BOTTOM_INSIDE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->BOTTOM_INSIDE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    .line 77
    const/4 v0, 0x6

    new-array v0, v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->TOP:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->BOTTOM:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->BOTH_SIDED:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->TOP_INSIDE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->XAxisPosition:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    sget-object v1, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->BOTTOM_INSIDE:Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sput-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->$VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 77
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;
    .registers 2
    .param p0, "name"    # Ljava/lang/String;

    .line 77
    const-class v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    return-object v0
.end method

.method public static values()[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;
    .registers 1

    .line 77
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->$VALUES:[Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    invoke-virtual {v0}, [Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbr/com/itau/widgets/graphicalfriedcake/charts/components/XAxis$XAxisPosition;

    return-object v0
.end method

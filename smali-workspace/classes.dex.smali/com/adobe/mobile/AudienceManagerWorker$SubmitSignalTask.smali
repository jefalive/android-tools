.class public Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask;
.super Ljava/lang/Object;
.source "AudienceManagerWorker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/adobe/mobile/AudienceManagerWorker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SubmitSignalTask"
.end annotation


# instance fields
.field public final callback:Lcom/adobe/mobile/AudienceManager$AudienceManagerCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/adobe/mobile/AudienceManager$AudienceManagerCallback<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
        }
    .end annotation
.end field

.field public final data:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;Lcom/adobe/mobile/AudienceManager$AudienceManagerCallback;)V
    .registers 3
    .param p1, "initData"    # Ljava/util/Map;
    .param p2, "initCallback"    # Lcom/adobe/mobile/AudienceManager$AudienceManagerCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;Lcom/adobe/mobile/AudienceManager$AudienceManagerCallback<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;)V"
        }
    .end annotation

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    iput-object p1, p0, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask;->data:Ljava/util/Map;

    .line 146
    iput-object p2, p0, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask;->callback:Lcom/adobe/mobile/AudienceManager$AudienceManagerCallback;

    .line 147
    return-void
.end method


# virtual methods
.method public run()V
    .registers 11

    .line 153
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 157
    .local v4, "callbackData":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    :try_start_5
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->mobileUsingAudienceManager()Z
    :try_end_c
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_5 .. :try_end_c} :catch_b7
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_c} :catch_d9
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_c} :catch_fb
    .catchall {:try_start_5 .. :try_end_c} :catchall_11d

    move-result v0

    if-nez v0, :cond_21

    .line 200
    iget-object v0, p0, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask;->callback:Lcom/adobe/mobile/AudienceManager$AudienceManagerCallback;

    if-eqz v0, :cond_20

    .line 201
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask$1;

    invoke-direct {v1, p0, v4}, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask$1;-><init>(Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask;Ljava/util/HashMap;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 206
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 158
    :cond_20
    return-void

    .line 162
    :cond_21
    :try_start_21
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getPrivacyStatus()Lcom/adobe/mobile/MobilePrivacyStatus;

    move-result-object v0

    sget-object v1, Lcom/adobe/mobile/MobilePrivacyStatus;->MOBILE_PRIVACY_STATUS_OPT_OUT:Lcom/adobe/mobile/MobilePrivacyStatus;

    if-ne v0, v1, :cond_47

    .line 163
    const-string v0, "Audience Manager - Privacy status is set to opt out, no signals will be submitted."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_35
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_21 .. :try_end_35} :catch_b7
    .catch Lorg/json/JSONException; {:try_start_21 .. :try_end_35} :catch_d9
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_35} :catch_fb
    .catchall {:try_start_21 .. :try_end_35} :catchall_11d

    .line 200
    iget-object v0, p0, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask;->callback:Lcom/adobe/mobile/AudienceManager$AudienceManagerCallback;

    if-eqz v0, :cond_46

    .line 201
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask$1;

    invoke-direct {v1, p0, v4}, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask$1;-><init>(Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask;Ljava/util/HashMap;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 206
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 164
    :cond_46
    return-void

    .line 167
    :cond_47
    :try_start_47
    iget-object v0, p0, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask;->data:Ljava/util/Map;

    # invokes: Lcom/adobe/mobile/AudienceManagerWorker;->BuildSchemaRequest(Ljava/util/Map;)Ljava/lang/String;
    invoke-static {v0}, Lcom/adobe/mobile/AudienceManagerWorker;->access$400(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v5

    .line 169
    .local v5, "requestUrl":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_6e

    .line 170
    const-string v0, "Audience Manager - Unable to create URL object"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5c
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_47 .. :try_end_5c} :catch_b7
    .catch Lorg/json/JSONException; {:try_start_47 .. :try_end_5c} :catch_d9
    .catch Ljava/lang/Exception; {:try_start_47 .. :try_end_5c} :catch_fb
    .catchall {:try_start_47 .. :try_end_5c} :catchall_11d

    .line 200
    iget-object v0, p0, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask;->callback:Lcom/adobe/mobile/AudienceManager$AudienceManagerCallback;

    if-eqz v0, :cond_6d

    .line 201
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask$1;

    invoke-direct {v1, p0, v4}, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask$1;-><init>(Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask;Ljava/util/HashMap;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 206
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 171
    :cond_6d
    return-void

    .line 174
    :cond_6e
    const-string v0, "Audience Manager - request (%s)"

    const/4 v1, 0x1

    :try_start_71
    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v5, v1, v2

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logDebugFormat(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 176
    invoke-static {}, Lcom/adobe/mobile/MobileConfig;->getInstance()Lcom/adobe/mobile/MobileConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/adobe/mobile/MobileConfig;->getAamTimeout()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    const-string v1, "Audience Manager"

    const/4 v2, 0x0

    invoke-static {v5, v2, v0, v1}, Lcom/adobe/mobile/RequestHandler;->retrieveData(Ljava/lang/String;Ljava/util/Map;ILjava/lang/String;)[B

    move-result-object v6

    .line 177
    .local v6, "responseBytes":[B
    const-string v7, ""

    .line 179
    .local v7, "response":Ljava/lang/String;
    if-eqz v6, :cond_98

    array-length v0, v6

    if-lez v0, :cond_98

    .line 180
    new-instance v7, Ljava/lang/String;

    const-string v0, "UTF-8"

    invoke-direct {v7, v6, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 184
    :cond_98
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8, v7}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 187
    .local v8, "jsonResponse":Lorg/json/JSONObject;
    invoke-static {v8}, Lcom/adobe/mobile/AudienceManagerWorker;->processJsonResponse(Lorg/json/JSONObject;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V
    :try_end_a4
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_71 .. :try_end_a4} :catch_b7
    .catch Lorg/json/JSONException; {:try_start_71 .. :try_end_a4} :catch_d9
    .catch Ljava/lang/Exception; {:try_start_71 .. :try_end_a4} :catch_fb
    .catchall {:try_start_71 .. :try_end_a4} :catchall_11d

    .line 200
    .end local v5    # "requestUrl":Ljava/lang/String;
    .end local v6    # "responseBytes":[B
    .end local v7    # "response":Ljava/lang/String;
    .end local v8    # "jsonResponse":Lorg/json/JSONObject;
    iget-object v0, p0, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask;->callback:Lcom/adobe/mobile/AudienceManager$AudienceManagerCallback;

    if-eqz v0, :cond_130

    .line 201
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask$1;

    invoke-direct {v1, p0, v4}, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask$1;-><init>(Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask;Ljava/util/HashMap;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 206
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto/16 :goto_130

    .line 189
    :catch_b7
    move-exception v5

    .line 190
    .local v5, "e":Ljava/io/UnsupportedEncodingException;
    const-string v0, "Audience Manager - Unable to decode server response (%s)"

    const/4 v1, 0x1

    :try_start_bb
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/io/UnsupportedEncodingException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_c7
    .catchall {:try_start_bb .. :try_end_c7} :catchall_11d

    .line 200
    .end local v5    # "e":Ljava/io/UnsupportedEncodingException;
    iget-object v0, p0, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask;->callback:Lcom/adobe/mobile/AudienceManager$AudienceManagerCallback;

    if-eqz v0, :cond_130

    .line 201
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask$1;

    invoke-direct {v1, p0, v4}, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask$1;-><init>(Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask;Ljava/util/HashMap;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 206
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_130

    .line 192
    :catch_d9
    move-exception v5

    .line 193
    .local v5, "e":Lorg/json/JSONException;
    const-string v0, "Audience Manager - Unable to parse JSON data (%s)"

    const/4 v1, 0x1

    :try_start_dd
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Lorg/json/JSONException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_e9
    .catchall {:try_start_dd .. :try_end_e9} :catchall_11d

    .line 200
    .end local v5    # "e":Lorg/json/JSONException;
    iget-object v0, p0, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask;->callback:Lcom/adobe/mobile/AudienceManager$AudienceManagerCallback;

    if-eqz v0, :cond_130

    .line 201
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask$1;

    invoke-direct {v1, p0, v4}, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask$1;-><init>(Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask;Ljava/util/HashMap;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 206
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_130

    .line 195
    :catch_fb
    move-exception v5

    .line 196
    .local v5, "e":Ljava/lang/Exception;
    const-string v0, "Audience Manager - Unexpected error parsing result (%s)"

    const/4 v1, 0x1

    :try_start_ff
    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Exception;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/adobe/mobile/StaticMethods;->logWarningFormat(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_10b
    .catchall {:try_start_ff .. :try_end_10b} :catchall_11d

    .line 200
    .end local v5    # "e":Ljava/lang/Exception;
    iget-object v0, p0, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask;->callback:Lcom/adobe/mobile/AudienceManager$AudienceManagerCallback;

    if-eqz v0, :cond_130

    .line 201
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask$1;

    invoke-direct {v1, p0, v4}, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask$1;-><init>(Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask;Ljava/util/HashMap;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 206
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_130

    .line 200
    :catchall_11d
    move-exception v9

    iget-object v0, p0, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask;->callback:Lcom/adobe/mobile/AudienceManager$AudienceManagerCallback;

    if-eqz v0, :cond_12f

    .line 201
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask$1;

    invoke-direct {v1, p0, v4}, Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask$1;-><init>(Lcom/adobe/mobile/AudienceManagerWorker$SubmitSignalTask;Ljava/util/HashMap;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 206
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_12f
    throw v9

    .line 209
    :cond_130
    :goto_130
    return-void
.end method

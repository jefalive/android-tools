.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;
.super Ljava/lang/Object;
.source "SelectionDetail.java"


# instance fields
.field public dataSet:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;"
        }
    .end annotation
.end field

.field public dataSetIndex:I

.field public val:F


# direct methods
.method public constructor <init>(FILbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;)V
    .registers 4
    .param p1, "val"    # F
    .param p2, "dataSetIndex"    # I
    .param p3, "set"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FILbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet<*>;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;->val:F

    .line 22
    iput p2, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;->dataSetIndex:I

    .line 23
    iput-object p3, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/SelectionDetail;->dataSet:Lbr/com/itau/widgets/graphicalfriedcake/charts/data/DataSet;

    .line 24
    return-void
.end method

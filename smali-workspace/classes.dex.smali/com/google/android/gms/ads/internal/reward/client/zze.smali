.class public Lcom/google/android/gms/ads/internal/reward/client/zze;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/ads/reward/RewardItem;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# instance fields
.field private final zzKz:Lcom/google/android/gms/ads/internal/reward/client/zza;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/internal/reward/client/zza;)V
    .registers 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/ads/internal/reward/client/zze;->zzKz:Lcom/google/android/gms/ads/internal/reward/client/zza;

    return-void
.end method


# virtual methods
.method public getAmount()I
    .registers 3

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/reward/client/zze;->zzKz:Lcom/google/android/gms/ads/internal/reward/client/zza;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    return v0

    :cond_6
    :try_start_6
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/reward/client/zze;->zzKz:Lcom/google/android/gms/ads/internal/reward/client/zza;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/reward/client/zza;->getAmount()I
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_d

    move-result v0

    return v0

    :catch_d
    move-exception v1

    const-string v0, "Could not forward getAmount to RewardItem"

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    return v0
.end method

.method public getType()Ljava/lang/String;
    .registers 3

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/reward/client/zze;->zzKz:Lcom/google/android/gms/ads/internal/reward/client/zza;

    if-nez v0, :cond_6

    const/4 v0, 0x0

    return-object v0

    :cond_6
    :try_start_6
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/reward/client/zze;->zzKz:Lcom/google/android/gms/ads/internal/reward/client/zza;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/reward/client/zza;->getType()Ljava/lang/String;
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_b} :catch_d

    move-result-object v0

    return-object v0

    :catch_d
    move-exception v1

    const-string v0, "Could not forward getType to RewardItem"

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/internal/util/client/zzb;->zzd(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    return-object v0
.end method

.class public Lbr/com/itau/sdk/android/core/model/ErrorDTO;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private campos:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lbr/com/itau/sdk/android/core/model/CampoErroDTO;>;"
        }
    .end annotation
.end field

.field private codigo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation
.end field

.field private detalhes:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lbr/com/itau/sdk/android/core/model/DetalheErroDTO;>;"
        }
    .end annotation
.end field

.field private mensagem:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCampos()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lbr/com/itau/sdk/android/core/model/CampoErroDTO;>;"
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->campos:Ljava/util/List;

    return-object v0
.end method

.method public getCodigo()Ljava/lang/String;
    .registers 2

    .line 20
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->codigo:Ljava/lang/String;

    return-object v0
.end method

.method public getDetalhes()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()Ljava/util/List<Lbr/com/itau/sdk/android/core/model/DetalheErroDTO;>;"
        }
    .end annotation

    .line 44
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->detalhes:Ljava/util/List;

    return-object v0
.end method

.method public getMensagem()Ljava/lang/String;
    .registers 2

    .line 28
    iget-object v0, p0, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->mensagem:Ljava/lang/String;

    return-object v0
.end method

.method public setCampos(Ljava/util/List;)V
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/sdk/android/core/model/CampoErroDTO;>;)V"
        }
    .end annotation

    .line 40
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->campos:Ljava/util/List;

    .line 41
    return-void
.end method

.method public setCodigo(Ljava/lang/String;)V
    .registers 2

    .line 24
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->codigo:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public setDetalhes(Ljava/util/List;)V
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lbr/com/itau/sdk/android/core/model/DetalheErroDTO;>;)V"
        }
    .end annotation

    .line 48
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->detalhes:Ljava/util/List;

    .line 49
    return-void
.end method

.method public setMensagem(Ljava/lang/String;)V
    .registers 2

    .line 32
    iput-object p1, p0, Lbr/com/itau/sdk/android/core/model/ErrorDTO;->mensagem:Ljava/lang/String;

    .line 33
    return-void
.end method

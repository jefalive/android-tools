.class Lbr/com/itau/textovoz/ui/view/FontIconDrawable$MethodGetLayoutDirection;
.super Ljava/lang/Object;
.source "FontIconDrawable.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/ui/view/FontIconDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MethodGetLayoutDirection"
.end annotation


# static fields
.field static sMethod:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    .line 410
    :try_start_0
    const-class v0, Landroid/graphics/drawable/Drawable;

    const-string v1, "getLayoutDirection"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable$MethodGetLayoutDirection;->sMethod:Ljava/lang/reflect/Method;
    :try_end_d
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_d} :catch_e
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_d} :catch_e

    .line 413
    goto :goto_16

    .line 411
    :catch_e
    move-exception v3

    .line 412
    .local v3, "ex":Ljava/lang/Exception;
    # getter for: Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->access$000()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 414
    .end local v3    # "ex":Ljava/lang/Exception;
    :goto_16
    return-void
.end method

.method static invoke(Landroid/graphics/drawable/Drawable;)I
    .registers 4
    .param p0, "drawable"    # Landroid/graphics/drawable/Drawable;

    .line 417
    sget-object v0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable$MethodGetLayoutDirection;->sMethod:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_22

    .line 419
    :try_start_4
    sget-object v0, Lbr/com/itau/textovoz/ui/view/FontIconDrawable$MethodGetLayoutDirection;->sMethod:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 420
    .local v2, "result":Ljava/lang/Object;
    instance-of v0, v2, Ljava/lang/Integer;

    if-eqz v0, :cond_19

    .line 421
    move-object v0, v2

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_17
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_17} :catch_1a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_17} :catch_1a
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_4 .. :try_end_17} :catch_1a

    move-result v0

    return v0

    .line 425
    .end local v2    # "result":Ljava/lang/Object;
    :cond_19
    goto :goto_22

    .line 423
    :catch_1a
    move-exception v2

    .line 424
    .local v2, "ex":Ljava/lang/Exception;
    # getter for: Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lbr/com/itau/textovoz/ui/view/FontIconDrawable;->access$000()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 428
    .end local v2    # "ex":Ljava/lang/Exception;
    :cond_22
    :goto_22
    const/4 v0, 0x0

    return v0
.end method

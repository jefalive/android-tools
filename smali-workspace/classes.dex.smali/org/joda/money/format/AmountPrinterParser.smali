.class final Lorg/joda/money/format/AmountPrinterParser;
.super Ljava/lang/Object;
.source "AmountPrinterParser.java"

# interfaces
.implements Lorg/joda/money/format/MoneyPrinter;
.implements Lorg/joda/money/format/MoneyParser;
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final style:Lorg/joda/money/format/MoneyAmountStyle;


# direct methods
.method constructor <init>(Lorg/joda/money/format/MoneyAmountStyle;)V
    .registers 2
    .param p1, "style"    # Lorg/joda/money/format/MoneyAmountStyle;

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lorg/joda/money/format/AmountPrinterParser;->style:Lorg/joda/money/format/MoneyAmountStyle;

    .line 44
    return-void
.end method


# virtual methods
.method public parse(Lorg/joda/money/format/MoneyParseContext;)V
    .registers 13
    .param p1, "context"    # Lorg/joda/money/format/MoneyParseContext;

    .line 110
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getTextLength()I

    move-result v3

    .line 111
    .local v3, "len":I
    iget-object v0, p0, Lorg/joda/money/format/AmountPrinterParser;->style:Lorg/joda/money/format/MoneyAmountStyle;

    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/joda/money/format/MoneyAmountStyle;->localize(Ljava/util/Locale;)Lorg/joda/money/format/MoneyAmountStyle;

    move-result-object v4

    .line 112
    .local v4, "activeStyle":Lorg/joda/money/format/MoneyAmountStyle;
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getIndex()I

    move-result v0

    sub-int v0, v3, v0

    new-array v5, v0, [C

    .line 113
    .local v5, "buf":[C
    const/4 v6, 0x0

    .line 114
    .local v6, "bufPos":I
    const/4 v7, 0x0

    .line 115
    .local v7, "dpSeen":Z
    const/4 v8, 0x0

    .line 116
    .local v8, "lastWasGroup":Z
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getIndex()I

    move-result v9

    .line 117
    .local v9, "pos":I
    if-ge v9, v3, :cond_91

    .line 118
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    move v1, v9

    add-int/lit8 v9, v9, 0x1

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v10

    .line 119
    .local v10, "ch":C
    invoke-virtual {v4}, Lorg/joda/money/format/MoneyAmountStyle;->getNegativeSignCharacter()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    if-ne v10, v0, :cond_3d

    .line 120
    move v0, v6

    add-int/lit8 v6, v6, 0x1

    const/16 v1, 0x2d

    const/4 v0, 0x0

    aput-char v1, v5, v0

    goto :goto_91

    .line 121
    :cond_3d
    invoke-virtual {v4}, Lorg/joda/money/format/MoneyAmountStyle;->getPositiveSignCharacter()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    if-ne v10, v0, :cond_50

    .line 122
    move v0, v6

    add-int/lit8 v6, v6, 0x1

    const/16 v1, 0x2b

    const/4 v0, 0x0

    aput-char v1, v5, v0

    goto :goto_91

    .line 123
    :cond_50
    invoke-virtual {v4}, Lorg/joda/money/format/MoneyAmountStyle;->getZeroCharacter()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    if-lt v10, v0, :cond_79

    invoke-virtual {v4}, Lorg/joda/money/format/MoneyAmountStyle;->getZeroCharacter()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    add-int/lit8 v0, v0, 0xa

    if-ge v10, v0, :cond_79

    .line 124
    move v0, v6

    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v1, v10, 0x30

    invoke-virtual {v4}, Lorg/joda/money/format/MoneyAmountStyle;->getZeroCharacter()Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Character;->charValue()C

    move-result v2

    sub-int/2addr v1, v2

    int-to-char v1, v1

    const/4 v0, 0x0

    aput-char v1, v5, v0

    goto :goto_91

    .line 125
    :cond_79
    invoke-virtual {v4}, Lorg/joda/money/format/MoneyAmountStyle;->getDecimalPointCharacter()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    if-ne v10, v0, :cond_8d

    .line 126
    move v0, v6

    add-int/lit8 v6, v6, 0x1

    const/16 v1, 0x2e

    const/4 v0, 0x0

    aput-char v1, v5, v0

    .line 127
    const/4 v7, 0x1

    goto :goto_91

    .line 129
    :cond_8d
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->setError()V

    .line 130
    return-void

    .line 133
    .end local v10    # "ch":C
    :cond_91
    :goto_91
    if-ge v9, v3, :cond_eb

    .line 134
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0, v9}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v10

    .line 135
    .local v10, "ch":C
    invoke-virtual {v4}, Lorg/joda/money/format/MoneyAmountStyle;->getZeroCharacter()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    if-lt v10, v0, :cond_c4

    invoke-virtual {v4}, Lorg/joda/money/format/MoneyAmountStyle;->getZeroCharacter()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    add-int/lit8 v0, v0, 0xa

    if-ge v10, v0, :cond_c4

    .line 136
    move v0, v6

    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v1, v10, 0x30

    invoke-virtual {v4}, Lorg/joda/money/format/MoneyAmountStyle;->getZeroCharacter()Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Character;->charValue()C

    move-result v2

    sub-int/2addr v1, v2

    int-to-char v1, v1

    aput-char v1, v5, v0

    .line 137
    const/4 v8, 0x0

    goto :goto_e7

    .line 138
    :cond_c4
    invoke-virtual {v4}, Lorg/joda/money/format/MoneyAmountStyle;->getDecimalPointCharacter()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    if-ne v10, v0, :cond_da

    if-nez v7, :cond_da

    .line 139
    move v0, v6

    add-int/lit8 v6, v6, 0x1

    const/16 v1, 0x2e

    aput-char v1, v5, v0

    .line 140
    const/4 v7, 0x1

    .line 141
    const/4 v8, 0x0

    goto :goto_e7

    .line 142
    :cond_da
    invoke-virtual {v4}, Lorg/joda/money/format/MoneyAmountStyle;->getGroupingCharacter()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    if-ne v10, v0, :cond_eb

    if-nez v8, :cond_eb

    .line 143
    const/4 v8, 0x1

    .line 133
    .end local v10    # "ch":C
    :goto_e7
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_91

    .line 148
    :cond_eb
    if-eqz v8, :cond_ef

    .line 149
    add-int/lit8 v9, v9, -0x1

    .line 152
    :cond_ef
    :try_start_ef
    new-instance v0, Ljava/math/BigDecimal;

    const/4 v1, 0x0

    invoke-direct {v0, v5, v1, v6}, Ljava/math/BigDecimal;-><init>([CII)V

    invoke-virtual {p1, v0}, Lorg/joda/money/format/MoneyParseContext;->setAmount(Ljava/math/BigDecimal;)V

    .line 153
    invoke-virtual {p1, v9}, Lorg/joda/money/format/MoneyParseContext;->setIndex(I)V
    :try_end_fb
    .catch Ljava/lang/NumberFormatException; {:try_start_ef .. :try_end_fb} :catch_fc

    .line 156
    goto :goto_100

    .line 154
    :catch_fc
    move-exception v10

    .line 155
    .local v10, "ex":Ljava/lang/NumberFormatException;
    invoke-virtual {p1}, Lorg/joda/money/format/MoneyParseContext;->setError()V

    .line 157
    .end local v10    # "ex":Ljava/lang/NumberFormatException;
    :goto_100
    return-void
.end method

.method public print(Lorg/joda/money/format/MoneyPrintContext;Ljava/lang/Appendable;Lorg/joda/money/BigMoney;)V
    .registers 16
    .param p1, "context"    # Lorg/joda/money/format/MoneyPrintContext;
    .param p2, "appendable"    # Ljava/lang/Appendable;
    .param p3, "money"    # Lorg/joda/money/BigMoney;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 49
    iget-object v0, p0, Lorg/joda/money/format/AmountPrinterParser;->style:Lorg/joda/money/format/MoneyAmountStyle;

    invoke-virtual {p1}, Lorg/joda/money/format/MoneyPrintContext;->getLocale()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/joda/money/format/MoneyAmountStyle;->localize(Ljava/util/Locale;)Lorg/joda/money/format/MoneyAmountStyle;

    move-result-object v2

    .line 50
    .local v2, "activeStyle":Lorg/joda/money/format/MoneyAmountStyle;
    invoke-static {p3}, Lorg/joda/money/MoneyUtils;->isNegative(Lorg/joda/money/BigMoneyProvider;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 51
    invoke-virtual {p3}, Lorg/joda/money/BigMoney;->negated()Lorg/joda/money/BigMoney;

    move-result-object p3

    .line 52
    invoke-virtual {v2}, Lorg/joda/money/format/MoneyAmountStyle;->getNegativeSignCharacter()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-interface {p2, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 54
    :cond_1f
    invoke-virtual {p3}, Lorg/joda/money/BigMoney;->getAmount()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v3

    .line 55
    .local v3, "str":Ljava/lang/String;
    invoke-virtual {v2}, Lorg/joda/money/format/MoneyAmountStyle;->getZeroCharacter()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v4

    .line 56
    .local v4, "zeroChar":C
    const/16 v0, 0x30

    if-eq v4, v0, :cond_5a

    .line 57
    add-int/lit8 v5, v4, -0x30

    .line 58
    .local v5, "diff":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 59
    .local v6, "zeroConvert":Ljava/lang/StringBuilder;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_3b
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v7, v0, :cond_56

    .line 60
    invoke-virtual {v3, v7}, Ljava/lang/String;->charAt(I)C

    move-result v8

    .line 61
    .local v8, "ch":C
    const/16 v0, 0x30

    if-lt v8, v0, :cond_53

    const/16 v0, 0x39

    if-gt v8, v0, :cond_53

    .line 62
    add-int v0, v8, v5

    int-to-char v0, v0

    invoke-virtual {v6, v7, v0}, Ljava/lang/StringBuilder;->setCharAt(IC)V

    .line 59
    .end local v8    # "ch":C
    :cond_53
    add-int/lit8 v7, v7, 0x1

    goto :goto_3b

    .line 65
    .end local v7    # "i":I
    :cond_56
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 67
    .end local v5    # "diff":I
    .end local v6    # "zeroConvert":Ljava/lang/StringBuilder;
    :cond_5a
    const/16 v0, 0x2e

    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    .line 68
    .local v5, "decPoint":I
    add-int/lit8 v6, v5, 0x1

    .line 69
    .local v6, "afterDecPoint":I
    invoke-virtual {v2}, Lorg/joda/money/format/MoneyAmountStyle;->getGroupingStyle()Lorg/joda/money/format/GroupingStyle;

    move-result-object v0

    sget-object v1, Lorg/joda/money/format/GroupingStyle;->NONE:Lorg/joda/money/format/GroupingStyle;

    if-ne v0, v1, :cond_a0

    .line 70
    if-gez v5, :cond_82

    .line 71
    invoke-interface {p2, v3}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 72
    invoke-virtual {v2}, Lorg/joda/money/format/MoneyAmountStyle;->isForcedDecimalPoint()Z

    move-result v0

    if-eqz v0, :cond_11d

    .line 73
    invoke-virtual {v2}, Lorg/joda/money/format/MoneyAmountStyle;->getDecimalPointCharacter()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-interface {p2, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    goto/16 :goto_11d

    .line 76
    :cond_82
    const/4 v0, 0x0

    invoke-virtual {v3, v0, v5}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    move-result-object v0

    invoke-virtual {v2}, Lorg/joda/money/format/MoneyAmountStyle;->getDecimalPointCharacter()Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Character;->charValue()C

    move-result v1

    invoke-interface {v0, v1}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    move-result-object v0

    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    goto/16 :goto_11d

    .line 80
    :cond_a0
    invoke-virtual {v2}, Lorg/joda/money/format/MoneyAmountStyle;->getGroupingSize()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 81
    .local v7, "groupingSize":I
    invoke-virtual {v2}, Lorg/joda/money/format/MoneyAmountStyle;->getGroupingCharacter()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v8

    .line 82
    .local v8, "groupingChar":C
    if-gez v5, :cond_b7

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v9

    goto :goto_b8

    :cond_b7
    move v9, v5

    .line 83
    .local v9, "pre":I
    :goto_b8
    if-gez v5, :cond_bc

    const/4 v10, 0x0

    goto :goto_c3

    :cond_bc
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    sub-int/2addr v0, v5

    add-int/lit8 v10, v0, -0x1

    .line 84
    .local v10, "post":I
    :goto_c3
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_c4
    if-lez v9, :cond_dc

    .line 85
    invoke-virtual {v3, v11}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-interface {p2, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 86
    if-le v9, v7, :cond_d7

    rem-int v0, v9, v7

    const/4 v1, 0x1

    if-ne v0, v1, :cond_d7

    .line 87
    invoke-interface {p2, v8}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 84
    :cond_d7
    add-int/lit8 v11, v11, 0x1

    add-int/lit8 v9, v9, -0x1

    goto :goto_c4

    .line 90
    .end local v11    # "i":I
    :cond_dc
    if-gez v5, :cond_e4

    invoke-virtual {v2}, Lorg/joda/money/format/MoneyAmountStyle;->isForcedDecimalPoint()Z

    move-result v0

    if-eqz v0, :cond_ef

    .line 91
    :cond_e4
    invoke-virtual {v2}, Lorg/joda/money/format/MoneyAmountStyle;->getDecimalPointCharacter()Ljava/lang/Character;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-interface {p2, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 93
    :cond_ef
    invoke-virtual {v2}, Lorg/joda/money/format/MoneyAmountStyle;->getGroupingStyle()Lorg/joda/money/format/GroupingStyle;

    move-result-object v0

    sget-object v1, Lorg/joda/money/format/GroupingStyle;->BEFORE_DECIMAL_POINT:Lorg/joda/money/format/GroupingStyle;

    if-ne v0, v1, :cond_101

    .line 94
    if-ltz v5, :cond_11d

    .line 95
    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    goto :goto_11d

    .line 98
    :cond_101
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_102
    if-ge v11, v10, :cond_11d

    .line 99
    add-int v0, v11, v6

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-interface {p2, v0}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 100
    rem-int v0, v11, v7

    add-int/lit8 v1, v7, -0x1

    if-ne v0, v1, :cond_11a

    add-int/lit8 v0, v11, 0x1

    if-ge v0, v10, :cond_11a

    .line 101
    invoke-interface {p2, v8}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    .line 98
    :cond_11a
    add-int/lit8 v11, v11, 0x1

    goto :goto_102

    .line 106
    .end local v7    # "groupingSize":I
    .end local v8    # "groupingChar":C
    .end local v9    # "pre":I
    .end local v10    # "post":I
    .end local v11    # "i":I
    :cond_11d
    :goto_11d
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 2

    .line 161
    const-string v0, "${amount}"

    return-object v0
.end method

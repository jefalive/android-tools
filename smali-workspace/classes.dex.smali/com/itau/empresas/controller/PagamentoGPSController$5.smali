.class Lcom/itau/empresas/controller/PagamentoGPSController$5;
.super Ljava/lang/Object;
.source "PagamentoGPSController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/controller/PagamentoGPSController;->simularAutorizacaoGPS(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/controller/PagamentoGPSController;

.field final synthetic val$gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;


# direct methods
.method constructor <init>(Lcom/itau/empresas/controller/PagamentoGPSController;Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;)V
    .registers 3
    .param p1, "this$0"    # Lcom/itau/empresas/controller/PagamentoGPSController;

    .line 86
    iput-object p1, p0, Lcom/itau/empresas/controller/PagamentoGPSController$5;->this$0:Lcom/itau/empresas/controller/PagamentoGPSController;

    iput-object p2, p0, Lcom/itau/empresas/controller/PagamentoGPSController$5;->val$gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 4

    .line 89
    new-instance v2, Lcom/itau/empresas/api/model/DadosGpsRequest;

    invoke-direct {v2}, Lcom/itau/empresas/api/model/DadosGpsRequest;-><init>()V

    .line 90
    .local v2, "dadosGpsRequest":Lcom/itau/empresas/api/model/DadosGpsRequest;
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoGPSController$5;->val$gpsRequestVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;

    invoke-virtual {v2, v0}, Lcom/itau/empresas/api/model/DadosGpsRequest;->setGpsRequestVO(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/GpsRequestVO;)V

    .line 91
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoGPSController$5;->this$0:Lcom/itau/empresas/controller/PagamentoGPSController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/PagamentoGPSController;->api()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    invoke-interface {v0, v2}, Lcom/itau/empresas/api/Api;->simularGPSAutorizacao(Lcom/itau/empresas/api/model/DadosGpsRequest;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    .line 92
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoGPSController$5;->this$0:Lcom/itau/empresas/controller/PagamentoGPSController;

    iget-object v1, p0, Lcom/itau/empresas/controller/PagamentoGPSController$5;->this$0:Lcom/itau/empresas/controller/PagamentoGPSController;

    invoke-virtual {v1}, Lcom/itau/empresas/controller/PagamentoGPSController;->api()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/itau/empresas/api/Api;

    invoke-interface {v1}, Lcom/itau/empresas/api/Api;->incluirAutorizarGps()Lcom/google/gson/JsonObject;

    move-result-object v1

    # invokes: Lcom/itau/empresas/controller/PagamentoGPSController;->parseJson(Lcom/google/gson/JsonObject;)V
    invoke-static {v0, v1}, Lcom/itau/empresas/controller/PagamentoGPSController;->access$000(Lcom/itau/empresas/controller/PagamentoGPSController;Lcom/google/gson/JsonObject;)V

    .line 93
    return-void
.end method

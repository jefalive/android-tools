.class public Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;
.super Ljava/lang/Object;
.source "EmprestimosParceladosOfertaEspecialAceitaVO.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;
    }
.end annotation


# instance fields
.field private agencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia"
    .end annotation
.end field

.field private cnpjCliente:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cnpj_cliente"
    .end annotation
.end field

.field private codigoPlanoFinanciamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_plano_financiamento"
    .end annotation
.end field

.field private conta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta"
    .end annotation
.end field

.field private cpfOperador:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cpf_operador"
    .end annotation
.end field

.field private dadosProdutoVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosProdutoVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dados_produto"
    .end annotation
.end field

.field private dadosSeguroVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dados_seguro"
    .end annotation
.end field

.field private dataPrimeiroPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_primeiro_pagamento"
    .end annotation
.end field

.field private devedorSolidario:Z

.field private digitoConta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "digito_conta"
    .end annotation
.end field

.field private ofertaEspecial:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "oferta_especial"
    .end annotation
.end field

.field private ofertaProdutosParceladosSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

.field private prazoMaximoContratacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "prazo_maximo_contratacao"
    .end annotation
.end field

.field private prazoMinimoContratacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "prazo_minimo_contratacao"
    .end annotation
.end field

.field private quantidadeParcelas:Ljava/lang/Integer;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade_parcelas"
    .end annotation
.end field

.field private taxaJurosMes:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros_mes"
    .end annotation
.end field

.field private valorEmprestimo:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_emprestimo"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public ajustaDados(Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;Z)V
    .registers 7
    .param p1, "saidaVO"    # Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;
    .param p2, "isOfertaEspecial"    # Z

    .line 61
    if-nez p1, :cond_3

    .line 62
    return-void

    .line 65
    :cond_3
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->ofertaProdutosParceladosSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    .line 67
    .line 68
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getOfertaEspecialProdutosParceladosVO()Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;

    move-result-object v3

    .line 70
    .local v3, "ofertaEspecialVO":Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getAgencia()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->agencia:Ljava/lang/String;

    .line 71
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getConta()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->conta:Ljava/lang/String;

    .line 72
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getDigitoConta()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->digitoConta:Ljava/lang/String;

    .line 74
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosProdutoVO;

    invoke-direct {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosProdutoVO;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->dadosProdutoVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosProdutoVO;

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->dadosProdutoVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosProdutoVO;

    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getCodigoProduto()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosProdutoVO;->setCodigoProduto(Ljava/lang/String;)V

    .line 76
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->dadosProdutoVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosProdutoVO;

    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getNomeProduto()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosProdutoVO;->setDescricaoProduto(Ljava/lang/String;)V

    .line 78
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;

    invoke-direct {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->dadosSeguroVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->dadosSeguroVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;

    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->isHasIndicadorSeguro()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;->setIndicadorSeguro(Z)V

    .line 81
    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getCodigoPlanoFinanciamento()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->codigoPlanoFinanciamento:Ljava/lang/String;

    .line 82
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getCpfOperador()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->cpfOperador:Ljava/lang/String;

    .line 83
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getCnpjCliente()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->cnpjCliente:Ljava/lang/String;

    .line 84
    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getValorOferta()Ljava/math/BigDecimal;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->valorEmprestimo:Ljava/math/BigDecimal;

    .line 85
    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getDataPrimeiroVencimento()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->dataPrimeiroPagamento:Ljava/lang/String;

    .line 86
    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getQuantidadeParcelas()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->quantidadeParcelas:Ljava/lang/Integer;

    .line 87
    invoke-virtual {p1}, Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;->getTaxaJurosMes()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->taxaJurosMes:Ljava/lang/String;

    .line 89
    new-instance v0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;-><init>(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->ofertaEspecial:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;

    .line 90
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->ofertaEspecial:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;

    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getValorOferta()Ljava/math/BigDecimal;

    move-result-object v1

    iput-object v1, v0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->valorOferta:Ljava/math/BigDecimal;

    .line 91
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->ofertaEspecial:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;

    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getCodigoBandeiraCartao()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->codigoBandeiraCartao:Ljava/lang/String;

    .line 92
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->ofertaEspecial:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;

    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getDescricaoBandeiraCartao()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->descricaoBandeiraCartao:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->access$002(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;Ljava/lang/String;)Ljava/lang/String;

    .line 93
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->ofertaEspecial:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;

    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getTaxaJuros()D

    move-result-wide v1

    # setter for: Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->taxaJuros:D
    invoke-static {v0, v1, v2}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->access$102(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;D)D

    .line 94
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->ofertaEspecial:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;

    .line 95
    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getQuantidadeDiasPrimeiroPagamento()I

    move-result v1

    .line 94
    # setter for: Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->quantidadeDiasPrimeiroPagamento:I
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->access$202(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;I)I

    .line 96
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->ofertaEspecial:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;

    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getDataContrato()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->dataContrato:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->access$302(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;Ljava/lang/String;)Ljava/lang/String;

    .line 97
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->ofertaEspecial:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;

    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getDataPrimeiroVencimento()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->dataPrimeiroVencimento:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->access$402(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;Ljava/lang/String;)Ljava/lang/String;

    .line 98
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->ofertaEspecial:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;

    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getTipoOferta()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->tipoOferta:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->access$502(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;Ljava/lang/String;)Ljava/lang/String;

    .line 99
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->ofertaEspecial:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;

    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getCodigoProduto()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->codigoProduto:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->access$602(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;Ljava/lang/String;)Ljava/lang/String;

    .line 100
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->ofertaEspecial:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;

    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getNomeProduto()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->nomeProduto:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->access$702(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;Ljava/lang/String;)Ljava/lang/String;

    .line 101
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->ofertaEspecial:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;

    invoke-virtual {v3}, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->getCodigoPlanoFinanciamento()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->codigoPlanoFinanciamento:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->access$802(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;Ljava/lang/String;)Ljava/lang/String;

    .line 103
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->ofertaEspecial:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;

    # getter for: Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->tipoOferta:Ljava/lang/String;
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->access$500(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_f3

    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->ofertaEspecial:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;

    # getter for: Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->tipoOferta:Ljava/lang/String;
    invoke-static {v0}, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;->access$500(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "giro_devedor_solidario"

    .line 104
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f3

    .line 106
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->devedorSolidario:Z

    .line 109
    :cond_f3
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .line 247
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getConta()Ljava/lang/String;
    .registers 2

    .line 168
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->conta:Ljava/lang/String;

    return-object v0
.end method

.method public getDadosSeguroVO()Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;
    .registers 2

    .line 192
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->dadosSeguroVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;

    return-object v0
.end method

.method public getDataPrimeiroPagamento()Ljava/lang/String;
    .registers 2

    .line 120
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->dataPrimeiroPagamento:Ljava/lang/String;

    return-object v0
.end method

.method public getDevedorSolidario()Z
    .registers 2

    .line 243
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->devedorSolidario:Z

    return v0
.end method

.method public getPrazoMaximoContratacao()Ljava/lang/String;
    .registers 2

    .line 233
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->prazoMaximoContratacao:Ljava/lang/String;

    return-object v0
.end method

.method public getPrazoMinimoContratacao()Ljava/lang/String;
    .registers 2

    .line 238
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->prazoMinimoContratacao:Ljava/lang/String;

    return-object v0
.end method

.method public getValorEmprestimo()Ljava/math/BigDecimal;
    .registers 2

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->valorEmprestimo:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public setConta(Ljava/lang/String;)V
    .registers 2
    .param p1, "conta"    # Ljava/lang/String;

    .line 172
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->conta:Ljava/lang/String;

    .line 173
    return-void
.end method

.method public setDadosSeguroVO(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;)V
    .registers 2
    .param p1, "dadosSeguroVO"    # Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;

    .line 196
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->dadosSeguroVO:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosDadosSeguroVO;

    .line 197
    return-void
.end method

.method public setDataPrimeiroPagamento(Ljava/lang/String;)V
    .registers 2
    .param p1, "dataPrimeiroPagamento"    # Ljava/lang/String;

    .line 124
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->dataPrimeiroPagamento:Ljava/lang/String;

    .line 125
    return-void
.end method

.method public setOfertaEspecial(Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;)V
    .registers 2
    .param p1, "ofertaEspecial"    # Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;

    .line 204
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->ofertaEspecial:Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO$OfertaEspecialAceita;

    .line 205
    return-void
.end method

.method public setOfertaProdutosParceladosSaidaVO(Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;)V
    .registers 2
    .param p1, "ofertaProdutosParceladosSaidaVO"    # Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    .line 218
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->ofertaProdutosParceladosSaidaVO:Lcom/itau/empresas/feature/credito/giro/model/OfertaProdutosParceladosSaidaVO;

    .line 219
    return-void
.end method

.method public setPrazoMaximoContratacao(Ljava/lang/String;)V
    .registers 2
    .param p1, "prazoMaximoContratacao"    # Ljava/lang/String;

    .line 223
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->prazoMaximoContratacao:Ljava/lang/String;

    .line 224
    return-void
.end method

.method public setPrazoMinimoContratacao(Ljava/lang/String;)V
    .registers 2
    .param p1, "prazoMinimoContratacao"    # Ljava/lang/String;

    .line 228
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->prazoMinimoContratacao:Ljava/lang/String;

    .line 229
    return-void
.end method

.method public setQuantidadeParcelas(Ljava/lang/Integer;)V
    .registers 2
    .param p1, "quantidadeParcelas"    # Ljava/lang/Integer;

    .line 132
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->quantidadeParcelas:Ljava/lang/Integer;

    .line 133
    return-void
.end method

.method public setValorEmprestimo(Ljava/math/BigDecimal;)V
    .registers 2
    .param p1, "valorEmprestimo"    # Ljava/math/BigDecimal;

    .line 116
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/EmprestimosParceladosOfertaEspecialAceitaVO;->valorEmprestimo:Ljava/math/BigDecimal;

    .line 117
    return-void
.end method

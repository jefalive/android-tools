.class public final Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_;
.super Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;
.source "TomadaDecisaoSeguroView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 36
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;-><init>(Landroid/content/Context;)V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_;->alreadyInflated_:Z

    .line 33
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 37
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_;->init_()V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 41
    invoke-direct {p0, p1, p2}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_;->alreadyInflated_:Z

    .line 33
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 42
    invoke-direct {p0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_;->init_()V

    .line 43
    return-void
.end method

.method private init_()V
    .registers 3

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 69
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 70
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 71
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 3

    .line 59
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_;->alreadyInflated_:Z

    if-nez v0, :cond_16

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_;->alreadyInflated_:Z

    .line 61
    invoke-virtual {p0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030184

    invoke-static {v0, v1, p0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 64
    :cond_16
    invoke-super {p0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView;->onFinishInflate()V

    .line 65
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 81
    const v0, 0x7f0e0693

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_;->botaoComSeguro:Landroid/widget/ToggleButton;

    .line 82
    const v0, 0x7f0e0694

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_;->botaoSemSeguro:Landroid/widget/ToggleButton;

    .line 83
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_;->botaoComSeguro:Landroid/widget/ToggleButton;

    if-eqz v0, :cond_24

    .line 84
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_;->botaoComSeguro:Landroid/widget/ToggleButton;

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_$1;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_$1;-><init>(Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_;)V

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    :cond_24
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_;->botaoSemSeguro:Landroid/widget/ToggleButton;

    if-eqz v0, :cond_32

    .line 94
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_;->botaoSemSeguro:Landroid/widget/ToggleButton;

    new-instance v1, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_$2;-><init>(Lcom/itau/empresas/feature/credito/giro/customview/TomadaDecisaoSeguroView_;)V

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    :cond_32
    return-void
.end method

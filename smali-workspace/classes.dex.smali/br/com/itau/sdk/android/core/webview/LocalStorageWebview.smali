.class public final Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview$SingletonHolder;
    }
.end annotation


# static fields
.field private static final $:[B

.field private static $$:I

.field private static secureStorage:Lbr/com/itau/security/securestorage/SecureStorage;


# direct methods
.method private static $(III)Ljava/lang/String;
    .registers 9

    sget-object v5, Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;->$:[B

    mul-int/lit8 p0, p0, 0x4

    add-int/lit8 p0, p0, 0x6c

    new-instance v0, Ljava/lang/String;

    mul-int/lit8 p2, p2, 0x3

    add-int/lit8 p2, p2, 0x13

    const/4 v4, 0x0

    mul-int/lit8 p1, p1, 0x2

    add-int/lit8 p1, p1, 0x4

    new-array v1, p2, [B

    add-int/lit8 p2, p2, -0x1

    if-nez v5, :cond_1f

    move v2, p1

    move v3, p2

    :goto_19
    neg-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 p0, v2, 0x1

    add-int/lit8 p1, p1, 0x1

    :cond_1f
    int-to-byte v2, p0

    aput-byte v2, v1, v4

    if-ne v4, p2, :cond_29

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_29
    move v2, p0

    add-int/lit8 v4, v4, 0x1

    aget-byte v3, v5, p1

    goto :goto_19
.end method

.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x16

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;->$:[B

    const/16 v0, 0x43

    sput v0, Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;->$$:I

    return-void

    :array_e
    .array-data 1
        0x57t
        0x70t
        -0x4ft
        -0x23t
        -0x2t
        0xdt
        0x3t
        -0xat
        -0x6t
        0x0t
        0x6t
        -0x2t
        0x12t
        -0x5t
        0x3t
        -0x11t
        0x13t
        0x4t
        -0x13t
        0xet
        0x5t
        -0x11t
    .end array-data
.end method

.method private constructor <init>()V
    .registers 1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview$1;)V
    .registers 2

    .line 10
    invoke-direct {p0}, Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;-><init>()V

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;
    .registers 5

    .line 18
    # getter for: Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview$SingletonHolder;->INSTANCE:Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;
    invoke-static {}, Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview$SingletonHolder;->access$000()Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;

    move-result-object v3

    .line 19
    new-instance v0, Lbr/com/itau/security/securestorage/SecureStorage;

    sget-object v1, Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;->$:[B

    const/16 v2, 0x9

    aget-byte v1, v1, v2

    invoke-static {v1, v1, v1}, Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;->$(III)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbr/com/itau/security/securestorage/SecureStorage;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v0, Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;->secureStorage:Lbr/com/itau/security/securestorage/SecureStorage;

    .line 20
    return-object v3
.end method


# virtual methods
.method public clearAll()V
    .registers 2

    .line 36
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;->secureStorage:Lbr/com/itau/security/securestorage/SecureStorage;

    invoke-virtual {v0}, Lbr/com/itau/security/securestorage/SecureStorage;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 37
    return-void
.end method

.method public findItem(Ljava/lang/String;)Ljava/lang/String;
    .registers 4

    .line 28
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;->secureStorage:Lbr/com/itau/security/securestorage/SecureStorage;

    const-string v1, ""

    invoke-virtual {v0, p1, v1}, Lbr/com/itau/security/securestorage/SecureStorage;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public remove(Ljava/lang/String;)V
    .registers 3

    .line 32
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;->secureStorage:Lbr/com/itau/security/securestorage/SecureStorage;

    invoke-virtual {v0}, Lbr/com/itau/security/securestorage/SecureStorage;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 33
    return-void
.end method

.method public save(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4

    .line 24
    sget-object v0, Lbr/com/itau/sdk/android/core/webview/LocalStorageWebview;->secureStorage:Lbr/com/itau/security/securestorage/SecureStorage;

    invoke-virtual {v0}, Lbr/com/itau/security/securestorage/SecureStorage;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 25
    return-void
.end method

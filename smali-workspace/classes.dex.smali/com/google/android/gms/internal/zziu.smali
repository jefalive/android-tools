.class public Lcom/google/android/gms/internal/zziu;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mState:I

.field private final zzDB:F

.field private zzMh:Ljava/lang/String;

.field private zzMi:F

.field private zzMj:F

.field private zzMk:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/internal/zziu;->mState:I

    iput-object p1, p0, Lcom/google/android/gms/internal/zziu;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/android/gms/internal/zziu;->zzDB:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .registers 3

    invoke-direct {p0, p1}, Lcom/google/android/gms/internal/zziu;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/google/android/gms/internal/zziu;->zzMh:Ljava/lang/String;

    return-void
.end method

.method private showDialog()V
    .registers 6

    iget-object v0, p0, Lcom/google/android/gms/internal/zziu;->mContext:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    if-nez v0, :cond_c

    const-string v0, "Can not create dialog without Activity Context"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaJ(Ljava/lang/String;)V

    return-void

    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/internal/zziu;->zzMh:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/internal/zziu;->zzaG(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/google/android/gms/internal/zziu;->mContext:Landroid/content/Context;

    invoke-direct {v3, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const-string v0, "Ad Information"

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const-string v0, "Share"

    new-instance v1, Lcom/google/android/gms/internal/zziu$1;

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/internal/zziu$1;-><init>(Lcom/google/android/gms/internal/zziu;Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const-string v0, "Close"

    new-instance v1, Lcom/google/android/gms/internal/zziu$2;

    invoke-direct {v1, p0}, Lcom/google/android/gms/internal/zziu$2;-><init>(Lcom/google/android/gms/internal/zziu;)V

    invoke-virtual {v3, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method static synthetic zza(Lcom/google/android/gms/internal/zziu;)Landroid/content/Context;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/internal/zziu;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static zzaG(Ljava/lang/String;)Ljava/lang/String;
    .registers 9

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "No debug information"

    return-object v0

    :cond_9
    const-string v0, "\\+"

    const-string v1, "%20"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/android/gms/ads/internal/zzr;->zzbC()Lcom/google/android/gms/internal/zzir;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/internal/zzir;->zze(Landroid/net/Uri;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_33
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5a

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_33

    :cond_5a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6a

    move-object v0, v6

    goto :goto_6c

    :cond_6a
    const-string v0, "No debug information"

    :goto_6c
    return-object v0
.end method


# virtual methods
.method zza(IFF)V
    .registers 7

    if-nez p1, :cond_d

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/internal/zziu;->mState:I

    iput p2, p0, Lcom/google/android/gms/internal/zziu;->zzMi:F

    iput p3, p0, Lcom/google/android/gms/internal/zziu;->zzMj:F

    iput p3, p0, Lcom/google/android/gms/internal/zziu;->zzMk:F

    goto/16 :goto_a3

    :cond_d
    iget v0, p0, Lcom/google/android/gms/internal/zziu;->mState:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_13

    return-void

    :cond_13
    const/4 v0, 0x2

    if-ne p1, v0, :cond_98

    iget v0, p0, Lcom/google/android/gms/internal/zziu;->zzMj:F

    cmpl-float v0, p3, v0

    if-lez v0, :cond_1f

    iput p3, p0, Lcom/google/android/gms/internal/zziu;->zzMj:F

    goto :goto_27

    :cond_1f
    iget v0, p0, Lcom/google/android/gms/internal/zziu;->zzMk:F

    cmpg-float v0, p3, v0

    if-gez v0, :cond_27

    iput p3, p0, Lcom/google/android/gms/internal/zziu;->zzMk:F

    :cond_27
    :goto_27
    iget v0, p0, Lcom/google/android/gms/internal/zziu;->zzMj:F

    iget v1, p0, Lcom/google/android/gms/internal/zziu;->zzMk:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/internal/zziu;->zzDB:F

    const/high16 v2, 0x41f00000    # 30.0f

    mul-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_39

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/zziu;->mState:I

    return-void

    :cond_39
    iget v0, p0, Lcom/google/android/gms/internal/zziu;->mState:I

    if-eqz v0, :cond_42

    iget v0, p0, Lcom/google/android/gms/internal/zziu;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_58

    :cond_42
    iget v0, p0, Lcom/google/android/gms/internal/zziu;->zzMi:F

    sub-float v0, p2, v0

    iget v1, p0, Lcom/google/android/gms/internal/zziu;->zzDB:F

    const/high16 v2, 0x42480000    # 50.0f

    mul-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_77

    iput p2, p0, Lcom/google/android/gms/internal/zziu;->zzMi:F

    iget v0, p0, Lcom/google/android/gms/internal/zziu;->mState:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/zziu;->mState:I

    goto :goto_77

    :cond_58
    iget v0, p0, Lcom/google/android/gms/internal/zziu;->mState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_62

    iget v0, p0, Lcom/google/android/gms/internal/zziu;->mState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_77

    :cond_62
    iget v0, p0, Lcom/google/android/gms/internal/zziu;->zzMi:F

    sub-float v0, p2, v0

    iget v1, p0, Lcom/google/android/gms/internal/zziu;->zzDB:F

    const/high16 v2, -0x3db80000    # -50.0f

    mul-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_77

    iput p2, p0, Lcom/google/android/gms/internal/zziu;->zzMi:F

    iget v0, p0, Lcom/google/android/gms/internal/zziu;->mState:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/zziu;->mState:I

    :cond_77
    :goto_77
    iget v0, p0, Lcom/google/android/gms/internal/zziu;->mState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_81

    iget v0, p0, Lcom/google/android/gms/internal/zziu;->mState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_8a

    :cond_81
    iget v0, p0, Lcom/google/android/gms/internal/zziu;->zzMi:F

    cmpl-float v0, p2, v0

    if-lez v0, :cond_a3

    iput p2, p0, Lcom/google/android/gms/internal/zziu;->zzMi:F

    goto :goto_a3

    :cond_8a
    iget v0, p0, Lcom/google/android/gms/internal/zziu;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_a3

    iget v0, p0, Lcom/google/android/gms/internal/zziu;->zzMi:F

    cmpg-float v0, p2, v0

    if-gez v0, :cond_a3

    iput p2, p0, Lcom/google/android/gms/internal/zziu;->zzMi:F

    goto :goto_a3

    :cond_98
    const/4 v0, 0x1

    if-ne p1, v0, :cond_a3

    iget v0, p0, Lcom/google/android/gms/internal/zziu;->mState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_a3

    invoke-direct {p0}, Lcom/google/android/gms/internal/zziu;->showDialog()V

    :cond_a3
    :goto_a3
    return-void
.end method

.method public zzaF(Ljava/lang/String;)V
    .registers 2

    iput-object p1, p0, Lcom/google/android/gms/internal/zziu;->zzMh:Ljava/lang/String;

    return-void
.end method

.method public zze(Landroid/view/MotionEvent;)V
    .registers 7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v3

    const/4 v4, 0x0

    :goto_5
    if-ge v4, v3, :cond_1b

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p1, v1, v4}, Landroid/view/MotionEvent;->getHistoricalX(II)F

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v4}, Landroid/view/MotionEvent;->getHistoricalY(II)F

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/internal/zziu;->zza(IFF)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_1b
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/internal/zziu;->zza(IFF)V

    return-void
.end method

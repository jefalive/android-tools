.class final Lbr/com/itau/widgets/hintview/Util;
.super Ljava/lang/Object;
.source "Util.java"


# direct methods
.method constructor <init>()V
    .registers 1

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static calculateRectInWindow(Landroid/view/View;)Landroid/graphics/RectF;
    .registers 8
    .param p0, "view"    # Landroid/view/View;

    .line 46
    const/4 v0, 0x2

    new-array v6, v0, [I

    .line 47
    .local v6, "location":[I
    invoke-virtual {p0, v6}, Landroid/view/View;->getLocationInWindow([I)V

    .line 48
    new-instance v0, Landroid/graphics/RectF;

    const/4 v1, 0x0

    aget v1, v6, v1

    int-to-float v1, v1

    const/4 v2, 0x1

    aget v2, v6, v2

    int-to-float v2, v2

    const/4 v3, 0x0

    aget v3, v6, v3

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    const/4 v4, 0x1

    aget v4, v6, v4

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method

.method public static calculateRectOnScreen(Landroid/view/View;)Landroid/graphics/RectF;
    .registers 8
    .param p0, "view"    # Landroid/view/View;

    .line 40
    const/4 v0, 0x2

    new-array v6, v0, [I

    .line 41
    .local v6, "location":[I
    invoke-virtual {p0, v6}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 42
    new-instance v0, Landroid/graphics/RectF;

    const/4 v1, 0x0

    aget v1, v6, v1

    int-to-float v1, v1

    const/4 v2, 0x1

    aget v2, v6, v2

    int-to-float v2, v2

    const/4 v3, 0x0

    aget v3, v6, v3

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    const/4 v4, 0x1

    aget v4, v6, v4

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method

.method public static dpToPx(F)F
    .registers 2
    .param p0, "dp"    # F

    .line 56
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, p0

    return v0
.end method

.method public static gravityToArrowDirection(I)I
    .registers 2
    .param p0, "gravity"    # I

    .line 60
    sparse-switch p0, :sswitch_data_16

    goto :goto_15

    .line 62
    :sswitch_4
    const v0, 0x800005

    return v0

    .line 64
    :sswitch_8
    const/16 v0, 0x50

    return v0

    .line 66
    :sswitch_b
    const v0, 0x800003

    return v0

    .line 68
    :sswitch_f
    const/16 v0, 0x30

    return v0

    .line 70
    :sswitch_12
    const/16 v0, 0x50

    return v0

    .line 72
    :goto_15
    return p0

    :sswitch_data_16
    .sparse-switch
        0x7 -> :sswitch_12
        0x30 -> :sswitch_8
        0x50 -> :sswitch_f
        0x800003 -> :sswitch_4
        0x800005 -> :sswitch_b
    .end sparse-switch
.end method

.method public static pxToDp(F)F
    .registers 2
    .param p0, "px"    # F

    .line 52
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    div-float v0, p0, v0

    return v0
.end method

.method public static removeOnGlobalLayoutListener(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V
    .registers 4
    .param p0, "view"    # Landroid/view/View;
    .param p1, "listener"    # Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 77
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_e

    .line 78
    invoke-virtual {p0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_15

    .line 81
    :cond_e
    invoke-virtual {p0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 83
    :goto_15
    return-void
.end method

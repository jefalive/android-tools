.class Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback$1;
.super Ljava/lang/Object;
.source "FingerprintDialogCallback.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;->fechaDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;


# direct methods
.method constructor <init>(Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;

    .line 49
    iput-object p1, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback$1;->this$0:Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    .line 53
    :try_start_0
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback$1;->this$0:Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;

    # getter for: Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;->dialog:Landroid/support/v7/app/AlertDialog;
    invoke-static {v0}, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;->access$000(Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;)Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback$1;->this$0:Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;

    # getter for: Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;->dialog:Landroid/support/v7/app/AlertDialog;
    invoke-static {v0}, Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;->access$000(Lcom/itau/empresas/ui/dialog/FingerprintDialogCallback;)Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->dismiss()V
    :try_end_15
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_15} :catch_16

    .line 58
    :cond_15
    goto :goto_20

    .line 56
    :catch_16
    move-exception v2

    .line 57
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    const-string v0, "fingerprint"

    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 60
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :goto_20
    return-void
.end method

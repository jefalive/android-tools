.class public final Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;
.super Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;
.source "PagamentoTributoSucessoActivity_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$IntentBuilder_;
    }
.end annotation


# instance fields
.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 41
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;-><init>()V

    .line 45
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method static synthetic access$001(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;

    .line 41
    invoke-super {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->mostraDialogoDeProgresso()V

    return-void
.end method

.method static synthetic access$101(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;)V
    .registers 1
    .param p0, "x0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;

    .line 41
    invoke-super {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->escondeDialogoDeProgresso()V

    return-void
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 59
    new-instance v0, Lcom/itau/empresas/Preferences_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/Preferences_;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->prefs:Lcom/itau/empresas/Preferences_;

    .line 60
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 61
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->application:Lcom/itau/empresas/CustomApplication;

    .line 62
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->injectExtras_()V

    .line 63
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->afterInject()V

    .line 64
    return-void
.end method

.method private injectExtras_()V
    .registers 3

    .line 164
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 165
    .local v1, "extras_":Landroid/os/Bundle;
    if-eqz v1, :cond_2e

    .line 166
    const-string v0, "dadosPagamentoTributo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 167
    const-string v0, "dadosPagamentoTributo"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->dadosPagamentoTributo:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    .line 169
    :cond_1c
    const-string v0, "detalhesAutorizacaoVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2e

    .line 170
    const-string v0, "detalhesAutorizacaoVO"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    .line 173
    :cond_2e
    return-void
.end method


# virtual methods
.method public escondeDialogoDeProgresso()V
    .registers 5

    .line 198
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$6;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$6;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 206
    return-void
.end method

.method public mostraDialogoDeProgresso()V
    .registers 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitTransaction"
        }
    .end annotation

    .line 186
    const-string v0, ""

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$5;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$5;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;)V

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lorg/androidannotations/api/UiThreadExecutor;->runTask(Ljava/lang/String;Ljava/lang/Runnable;J)V

    .line 194
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 51
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 52
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->init_(Landroid/os/Bundle;)V

    .line 53
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 55
    const v0, 0x7f030066

    invoke-virtual {p0, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->setContentView(I)V

    .line 56
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 5
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 98
    const v0, 0x7f0e030d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->root:Landroid/widget/LinearLayout;

    .line 99
    const v0, 0x7f0e0317

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->campoDataPagamento:Landroid/widget/TextView;

    .line 100
    const v0, 0x7f0e0315

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->campoValorTotalPagar:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0e0319

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->campoNomeRecebido:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f0e031b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->textoLimiteHorarioUltrapassado:Landroid/widget/TextView;

    .line 103
    const v0, 0x7f0e031a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->iconeAlerta:Landroid/widget/TextView;

    .line 104
    const v0, 0x7f0e0318

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->labelIdentificacaoComprovante:Landroid/widget/TextView;

    .line 105
    const v0, 0x7f0e0313

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->tituloTipoPagamento:Landroid/widget/TextView;

    .line 106
    const v0, 0x7f0e030f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->labelTituloSucesso:Landroid/widget/TextView;

    .line 107
    const v0, 0x7f0e0310

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->statusPagamento:Landroid/widget/TextView;

    .line 108
    const v0, 0x7f0e0311

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->motivoPendenciaAutorizacao:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f0e031c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->detalhesPagamento:Landroid/widget/TextView;

    .line 110
    const v0, 0x7f0e031e

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->shareComprovante:Landroid/widget/TextView;

    .line 111
    const v0, 0x7f0e031d

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->showComprovante:Landroid/widget/TextView;

    .line 112
    const v0, 0x7f0e012b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->linhaSeparacao:Landroid/view/View;

    .line 113
    const v0, 0x7f0e0328

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->rlNavegacaoHome:Landroid/widget/RelativeLayout;

    .line 114
    const v0, 0x7f0e032b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->rlNovoPagamento:Landroid/widget/RelativeLayout;

    .line 115
    const v0, 0x7f0e0325

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->rlAutorizarPagamento:Landroid/widget/RelativeLayout;

    .line 116
    const v0, 0x7f0e0323

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->rlExtrato:Landroid/widget/RelativeLayout;

    .line 117
    const v0, 0x7f0e00b6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->toolBar:Landroid/support/v7/widget/Toolbar;

    .line 118
    const v0, 0x7f0e031f

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 120
    .local v2, "view_card_view_novo_pagamento":Landroid/view/View;
    if-eqz v2, :cond_eb

    .line 121
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$1;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;)V

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    :cond_eb
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->detalhesPagamento:Landroid/widget/TextView;

    if-eqz v0, :cond_f9

    .line 131
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->detalhesPagamento:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$2;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$2;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    :cond_f9
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->showComprovante:Landroid/widget/TextView;

    if-eqz v0, :cond_107

    .line 141
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->showComprovante:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$3;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$3;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    :cond_107
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->shareComprovante:Landroid/widget/TextView;

    if-eqz v0, :cond_115

    .line 151
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->shareComprovante:Landroid/widget/TextView;

    new-instance v1, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$4;

    invoke-direct {v1, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_$4;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    :cond_115
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->atualizaDados()V

    .line 161
    return-void
.end method

.method public setContentView(I)V
    .registers 3
    .param p1, "layoutResID"    # I

    .line 68
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->setContentView(I)V

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 70
    return-void
.end method

.method public setContentView(Landroid/view/View;)V
    .registers 3
    .param p1, "view"    # Landroid/view/View;

    .line 80
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->setContentView(Landroid/view/View;)V

    .line 81
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 82
    return-void
.end method

.method public setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .line 74
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 75
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 76
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .registers 2
    .param p1, "newIntent"    # Landroid/content/Intent;

    .line 177
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity;->setIntent(Landroid/content/Intent;)V

    .line 178
    invoke-direct {p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/PagamentoTributoSucessoActivity_;->injectExtras_()V

    .line 179
    return-void
.end method

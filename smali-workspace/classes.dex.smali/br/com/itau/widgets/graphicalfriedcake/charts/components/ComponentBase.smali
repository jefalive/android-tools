.class public abstract Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;
.super Ljava/lang/Object;
.source "ComponentBase.java"


# instance fields
.field protected mEnabled:Z

.field protected mTextColor:I

.field protected mTextSize:F

.field protected mTypeface:Landroid/graphics/Typeface;

.field protected mXOffset:F

.field protected mYOffset:F


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;->mEnabled:Z

    .line 22
    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;->mXOffset:F

    .line 26
    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;->mYOffset:F

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;->mTypeface:Landroid/graphics/Typeface;

    .line 34
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;->mTextSize:F

    .line 38
    const/high16 v0, -0x1000000

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;->mTextColor:I

    .line 44
    return-void
.end method


# virtual methods
.method public getTextColor()I
    .registers 2

    .line 157
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;->mTextColor:I

    return v0
.end method

.method public getTextSize()F
    .registers 2

    .line 136
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;->mTextSize:F

    return v0
.end method

.method public getTypeface()Landroid/graphics/Typeface;
    .registers 2

    .line 97
    iget-object v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;->mTypeface:Landroid/graphics/Typeface;

    return-object v0
.end method

.method public getXOffset()F
    .registers 2

    .line 54
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;->mXOffset:F

    return v0
.end method

.method public getYOffset()F
    .registers 2

    .line 75
    iget v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;->mYOffset:F

    return v0
.end method

.method public isEnabled()Z
    .registers 2

    .line 179
    iget-boolean v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;->mEnabled:Z

    return v0
.end method

.method public setEnabled(Z)V
    .registers 2
    .param p1, "enabled"    # Z

    .line 169
    iput-boolean p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;->mEnabled:Z

    .line 170
    return-void
.end method

.method public setTextColor(I)V
    .registers 2
    .param p1, "color"    # I

    .line 147
    iput p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;->mTextColor:I

    .line 148
    return-void
.end method

.method public setTextSize(F)V
    .registers 3
    .param p1, "size"    # F

    .line 120
    const/high16 v0, 0x41c00000    # 24.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_8

    .line 121
    const/high16 p1, 0x41c00000    # 24.0f

    .line 122
    :cond_8
    const/high16 v0, 0x40c00000    # 6.0f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_10

    .line 123
    const/high16 p1, 0x40c00000    # 6.0f

    .line 126
    :cond_10
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;->mTextSize:F

    .line 127
    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
    .registers 2
    .param p1, "tf"    # Landroid/graphics/Typeface;

    .line 107
    iput-object p1, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;->mTypeface:Landroid/graphics/Typeface;

    .line 108
    return-void
.end method

.method public setXOffset(F)V
    .registers 3
    .param p1, "xOffset"    # F

    .line 64
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;->mXOffset:F

    .line 65
    return-void
.end method

.method public setYOffset(F)V
    .registers 3
    .param p1, "yOffset"    # F

    .line 87
    invoke-static {p1}, Lbr/com/itau/widgets/graphicalfriedcake/charts/utils/Utils;->convertDpToPixel(F)F

    move-result v0

    iput v0, p0, Lbr/com/itau/widgets/graphicalfriedcake/charts/components/ComponentBase;->mYOffset:F

    .line 88
    return-void
.end method

.class public Lcom/google/android/gms/common/api/CommonStatusCodes;
.super Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getStatusCodeString(I)Ljava/lang/String;
    .registers 3
    .param p0, "statusCode"    # I

    sparse-switch p0, :sswitch_data_62

    goto/16 :goto_4d

    :sswitch_5
    const-string v0, "SUCCESS_CACHE"

    return-object v0

    :sswitch_8
    const-string v0, "SUCCESS"

    return-object v0

    :sswitch_b
    const-string v0, "SERVICE_MISSING"

    return-object v0

    :sswitch_e
    const-string v0, "SERVICE_VERSION_UPDATE_REQUIRED"

    return-object v0

    :sswitch_11
    const-string v0, "SERVICE_DISABLED"

    return-object v0

    :sswitch_14
    const-string v0, "SIGN_IN_REQUIRED"

    return-object v0

    :sswitch_17
    const-string v0, "INVALID_ACCOUNT"

    return-object v0

    :sswitch_1a
    const-string v0, "RESOLUTION_REQUIRED"

    return-object v0

    :sswitch_1d
    const-string v0, "NETWORK_ERROR"

    return-object v0

    :sswitch_20
    const-string v0, "INTERNAL_ERROR"

    return-object v0

    :sswitch_23
    const-string v0, "SERVICE_INVALID"

    return-object v0

    :sswitch_26
    const-string v0, "DEVELOPER_ERROR"

    return-object v0

    :sswitch_29
    const-string v0, "LICENSE_CHECK_FAILED"

    return-object v0

    :sswitch_2c
    const-string v0, "ERROR"

    return-object v0

    :sswitch_2f
    const-string v0, "INTERRUPTED"

    return-object v0

    :sswitch_32
    const-string v0, "TIMEOUT"

    return-object v0

    :sswitch_35
    const-string v0, "CANCELED"

    return-object v0

    :sswitch_38
    const-string v0, "API_NOT_CONNECTED"

    return-object v0

    :sswitch_3b
    const-string v0, "AUTH_API_INVALID_CREDENTIALS"

    return-object v0

    :sswitch_3e
    const-string v0, "AUTH_API_ACCESS_FORBIDDEN"

    return-object v0

    :sswitch_41
    const-string v0, "AUTH_API_CLIENT_ERROR"

    return-object v0

    :sswitch_44
    const-string v0, "AUTH_API_SERVER_ERROR"

    return-object v0

    :sswitch_47
    const-string v0, "AUTH_TOKEN_ERROR"

    return-object v0

    :sswitch_4a
    const-string v0, "AUTH_URL_RESOLUTION"

    return-object v0

    :goto_4d
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "unknown status code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    nop

    :sswitch_data_62
    .sparse-switch
        -0x1 -> :sswitch_5
        0x0 -> :sswitch_8
        0x1 -> :sswitch_b
        0x2 -> :sswitch_e
        0x3 -> :sswitch_11
        0x4 -> :sswitch_14
        0x5 -> :sswitch_17
        0x6 -> :sswitch_1a
        0x7 -> :sswitch_1d
        0x8 -> :sswitch_20
        0x9 -> :sswitch_23
        0xa -> :sswitch_26
        0xb -> :sswitch_29
        0xd -> :sswitch_2c
        0xe -> :sswitch_2f
        0xf -> :sswitch_32
        0x10 -> :sswitch_35
        0x11 -> :sswitch_38
        0xbb8 -> :sswitch_3b
        0xbb9 -> :sswitch_3e
        0xbba -> :sswitch_41
        0xbbb -> :sswitch_44
        0xbbc -> :sswitch_47
        0xbbd -> :sswitch_4a
    .end sparse-switch
.end method

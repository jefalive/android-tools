.class Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;
.super Ljava/lang/Object;
.source "HomeSearchActivity.java"

# interfaces
.implements Landroid/support/v7/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;


# direct methods
.method constructor <init>(Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;)V
    .registers 2
    .param p1, "this$0"    # Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    .line 324
    iput-object p1, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryTextChange(Ljava/lang/String;)Z
    .registers 12
    .param p1, "query"    # Ljava/lang/String;

    .line 335
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    .line 337
    .local v9, "text":Ljava/lang/String;
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_11

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_41

    .line 338
    :cond_11
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    invoke-virtual {v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->showLoadingDialog()V

    .line 339
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    # getter for: Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchTextTimer:Landroid/os/CountDownTimer;
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->access$000(Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;)Landroid/os/CountDownTimer;

    move-result-object v0

    if-eqz v0, :cond_27

    .line 340
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    # getter for: Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchTextTimer:Landroid/os/CountDownTimer;
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->access$000(Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;)Landroid/os/CountDownTimer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 343
    :cond_27
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    new-instance v1, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1$1;

    move-object v2, p0

    const-wide/16 v3, 0x1f4

    const-wide/16 v5, 0x1f4

    move-object v7, v9

    move-object v8, p1

    invoke-direct/range {v1 .. v8}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1$1;-><init>(Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;JJLjava/lang/String;Ljava/lang/String;)V

    # setter for: Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchTextTimer:Landroid/os/CountDownTimer;
    invoke-static {v0, v1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->access$002(Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;Landroid/os/CountDownTimer;)Landroid/os/CountDownTimer;

    .line 356
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    # getter for: Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->searchTextTimer:Landroid/os/CountDownTimer;
    invoke-static {v0}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->access$000(Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;)Landroid/os/CountDownTimer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 359
    :cond_41
    const/4 v0, 0x1

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .registers 6
    .param p1, "query"    # Ljava/lang/String;

    .line 327
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    invoke-static {v0}, Lbr/com/itau/textovoz/util/ViewUtils;->hideSoftKeyboard(Landroid/app/Activity;)V

    .line 328
    iget-object v0, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    iget-object v1, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    sget v2, Lbr/com/itau/textovoz/R$string;->ga_categoria_busca:I

    invoke-virtual {v1, v2}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity$1;->this$0:Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;

    sget v3, Lbr/com/itau/textovoz/R$string;->ga_acao_enter:I

    invoke-virtual {v2, v3}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lbr/com/itau/textovoz/ui/activity/HomeSearchActivity;->setEventTracker(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    const/4 v0, 0x1

    return v0
.end method

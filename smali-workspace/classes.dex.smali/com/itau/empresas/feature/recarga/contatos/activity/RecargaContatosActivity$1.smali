.class Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$1;
.super Lcom/itau/empresas/ui/util/listener/ViewPagerOnPageSelectedListener;
.source "RecargaContatosActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->carregaElementosDaTela()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    .line 79
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$1;->this$0:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    invoke-direct {p0}, Lcom/itau/empresas/ui/util/listener/ViewPagerOnPageSelectedListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageSelected(I)V
    .registers 6
    .param p1, "position"    # I

    .line 83
    :try_start_0
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$1;->this$0:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->tabContato:Landroid/support/design/widget/TabLayout;

    invoke-virtual {v0, p1}, Landroid/support/design/widget/TabLayout;->getTabAt(I)Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/design/widget/TabLayout$Tab;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 84
    .local v3, "tabText":Ljava/lang/String;
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$1;->this$0:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity$1;->this$0:Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;

    const v2, 0x7f0702c8

    invoke-virtual {v1, v2}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_23

    move-object v2, v3

    goto :goto_25

    :cond_23
    const-string v2, ""

    :goto_25
    invoke-virtual {v0, v1, v2}, Lcom/itau/empresas/feature/recarga/contatos/activity/RecargaContatosActivity;->analyticsHit(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_28
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_28} :catch_29

    .line 90
    .end local v3    # "tabText":Ljava/lang/String;
    goto :goto_34

    .line 87
    :catch_29
    move-exception v3

    .line 88
    .local v3, "e":Ljava/lang/NullPointerException;
    const-string v0, "RecargaContatosActivity"

    const-string v1, "onPageSelected: "

    invoke-static {v0, v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 89
    invoke-static {v3}, Lcom/crashlytics/android/Crashlytics;->logException(Ljava/lang/Throwable;)V

    .line 91
    .end local v3    # "e":Ljava/lang/NullPointerException;
    :goto_34
    return-void
.end method

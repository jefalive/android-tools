.class public Lcom/google/android/gms/internal/zzbf;
.super Ljava/lang/Thread;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/zzbf$zza;
    }
.end annotation


# instance fields
.field private mStarted:Z

.field private zzam:Z

.field private final zzpV:Ljava/lang/Object;

.field private final zzsK:I

.field private final zzsM:I

.field private zzsY:Z

.field private final zzsZ:Lcom/google/android/gms/internal/zzbe;

.field private final zzta:Lcom/google/android/gms/internal/zzbd;

.field private final zztb:Lcom/google/android/gms/internal/zzha;

.field private final zztc:I

.field private final zztd:I

.field private final zzte:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/zzbe;Lcom/google/android/gms/internal/zzbd;Lcom/google/android/gms/internal/zzha;)V
    .registers 5

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzbf;->mStarted:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzbf;->zzsY:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzbf;->zzam:Z

    iput-object p1, p0, Lcom/google/android/gms/internal/zzbf;->zzsZ:Lcom/google/android/gms/internal/zzbe;

    iput-object p2, p0, Lcom/google/android/gms/internal/zzbf;->zzta:Lcom/google/android/gms/internal/zzbd;

    iput-object p3, p0, Lcom/google/android/gms/internal/zzbf;->zztb:Lcom/google/android/gms/internal/zzha;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/zzbf;->zzpV:Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/internal/zzbt;->zzwk:Lcom/google/android/gms/internal/zzbp;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzbp;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/zzbf;->zzsK:I

    sget-object v0, Lcom/google/android/gms/internal/zzbt;->zzwl:Lcom/google/android/gms/internal/zzbp;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzbp;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/zzbf;->zztd:I

    sget-object v0, Lcom/google/android/gms/internal/zzbt;->zzwm:Lcom/google/android/gms/internal/zzbp;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzbp;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/zzbf;->zzsM:I

    sget-object v0, Lcom/google/android/gms/internal/zzbt;->zzwn:Lcom/google/android/gms/internal/zzbp;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzbp;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/zzbf;->zzte:I

    sget-object v0, Lcom/google/android/gms/internal/zzbt;->zzwo:Lcom/google/android/gms/internal/zzbp;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzbp;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/zzbf;->zztc:I

    const-string v0, "ContentFetchTask"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/zzbf;->setName(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzbf;->zzam:Z

    if-nez v0, :cond_53

    :try_start_4
    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzbf;->zzcH()Z

    move-result v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/google/android/gms/internal/zzbf;->zzsZ:Lcom/google/android/gms/internal/zzbe;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzbe;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-nez v2, :cond_18

    const-string v0, "ContentFetchThread: no activity"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaI(Ljava/lang/String;)V
    :try_end_17
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_17} :catch_2d

    goto :goto_0

    :cond_18
    :try_start_18
    invoke-virtual {p0, v2}, Lcom/google/android/gms/internal/zzbf;->zza(Landroid/app/Activity;)V

    goto :goto_24

    :cond_1c
    const-string v0, "ContentFetchTask: sleeping"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaI(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzbf;->zzcJ()V

    :goto_24
    iget v0, p0, Lcom/google/android/gms/internal/zzbf;->zztc:I

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2c
    .catch Ljava/lang/Throwable; {:try_start_18 .. :try_end_2c} :catch_2d

    goto :goto_39

    :catch_2d
    move-exception v2

    const-string v0, "Error in ContentFetchTask"

    invoke-static {v0, v2}, Lcom/google/android/gms/internal/zzin;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzbf;->zztb:Lcom/google/android/gms/internal/zzha;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/internal/zzha;->zza(Ljava/lang/Throwable;Z)V

    :goto_39
    iget-object v2, p0, Lcom/google/android/gms/internal/zzbf;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    :goto_3c
    :try_start_3c
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzbf;->zzsY:Z
    :try_end_3e
    .catchall {:try_start_3c .. :try_end_3e} :catchall_4f

    if-eqz v0, :cond_4d

    const-string v0, "ContentFetchTask: waiting"

    :try_start_42
    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaI(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzbf;->zzpV:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_4a
    .catch Ljava/lang/InterruptedException; {:try_start_42 .. :try_end_4a} :catch_4b
    .catchall {:try_start_42 .. :try_end_4a} :catchall_4f

    goto :goto_3c

    :catch_4b
    move-exception v3

    goto :goto_3c

    :cond_4d
    monitor-exit v2

    goto :goto_52

    :catchall_4f
    move-exception v4

    monitor-exit v2

    throw v4

    :goto_52
    goto :goto_0

    :cond_53
    return-void
.end method

.method public wakeup()V
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzbf;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_4
    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzbf;->zzsY:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/zzbf;->zzpV:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    const-string v0, "ContentFetchThread: wakeup"

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaI(Ljava/lang/String;)V
    :try_end_10
    .catchall {:try_start_4 .. :try_end_10} :catchall_12

    monitor-exit v1

    goto :goto_15

    :catchall_12
    move-exception v2

    monitor-exit v1

    throw v2

    :goto_15
    return-void
.end method

.method zza(Landroid/view/View;Lcom/google/android/gms/internal/zzbc;)Lcom/google/android/gms/internal/zzbf$zza;
    .registers 13

    if-nez p1, :cond_a

    new-instance v0, Lcom/google/android/gms/internal/zzbf$zza;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/internal/zzbf$zza;-><init>(Lcom/google/android/gms/internal/zzbf;II)V

    return-object v0

    :cond_a
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p1, v3}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v4

    instance-of v0, p1, Landroid/widget/TextView;

    if-eqz v0, :cond_3f

    instance-of v0, p1, Landroid/widget/EditText;

    if-nez v0, :cond_3f

    move-object v5, p1

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_37

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, v7, v4}, Lcom/google/android/gms/internal/zzbc;->zzd(Ljava/lang/String;Z)V

    new-instance v0, Lcom/google/android/gms/internal/zzbf$zza;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/internal/zzbf$zza;-><init>(Lcom/google/android/gms/internal/zzbf;II)V

    return-object v0

    :cond_37
    new-instance v0, Lcom/google/android/gms/internal/zzbf$zza;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/internal/zzbf$zza;-><init>(Lcom/google/android/gms/internal/zzbf;II)V

    return-object v0

    :cond_3f
    instance-of v0, p1, Landroid/webkit/WebView;

    if-eqz v0, :cond_63

    instance-of v0, p1, Lcom/google/android/gms/internal/zzjp;

    if-nez v0, :cond_63

    invoke-virtual {p2}, Lcom/google/android/gms/internal/zzbc;->zzcC()V

    move-object v0, p1

    check-cast v0, Landroid/webkit/WebView;

    invoke-virtual {p0, v0, p2, v4}, Lcom/google/android/gms/internal/zzbf;->zza(Landroid/webkit/WebView;Lcom/google/android/gms/internal/zzbc;Z)Z

    move-result v0

    if-eqz v0, :cond_5b

    new-instance v0, Lcom/google/android/gms/internal/zzbf$zza;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/internal/zzbf$zza;-><init>(Lcom/google/android/gms/internal/zzbf;II)V

    return-object v0

    :cond_5b
    new-instance v0, Lcom/google/android/gms/internal/zzbf$zza;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/internal/zzbf$zza;-><init>(Lcom/google/android/gms/internal/zzbf;II)V

    return-object v0

    :cond_63
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_8a

    move-object v5, p1

    check-cast v5, Landroid/view/ViewGroup;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    :goto_6d
    invoke-virtual {v5}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v8, v0, :cond_84

    invoke-virtual {v5, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/internal/zzbf;->zza(Landroid/view/View;Lcom/google/android/gms/internal/zzbc;)Lcom/google/android/gms/internal/zzbf$zza;

    move-result-object v9

    iget v0, v9, Lcom/google/android/gms/internal/zzbf$zza;->zztm:I

    add-int/2addr v6, v0

    iget v0, v9, Lcom/google/android/gms/internal/zzbf$zza;->zztn:I

    add-int/2addr v7, v0

    add-int/lit8 v8, v8, 0x1

    goto :goto_6d

    :cond_84
    new-instance v0, Lcom/google/android/gms/internal/zzbf$zza;

    invoke-direct {v0, p0, v6, v7}, Lcom/google/android/gms/internal/zzbf$zza;-><init>(Lcom/google/android/gms/internal/zzbf;II)V

    return-object v0

    :cond_8a
    new-instance v0, Lcom/google/android/gms/internal/zzbf$zza;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/internal/zzbf$zza;-><init>(Lcom/google/android/gms/internal/zzbf;II)V

    return-object v0
.end method

.method zza(Landroid/app/Activity;)V
    .registers 5

    if-nez p1, :cond_3

    return-void

    :cond_3
    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_23

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_23

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    :cond_23
    if-nez v2, :cond_26

    return-void

    :cond_26
    invoke-virtual {p0, v2}, Lcom/google/android/gms/internal/zzbf;->zze(Landroid/view/View;)Z

    return-void
.end method

.method zza(Lcom/google/android/gms/internal/zzbc;Landroid/webkit/WebView;Ljava/lang/String;Z)V
    .registers 9

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzbc;->zzcB()V

    :try_start_3
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_40

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v0, "text"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Landroid/webkit/WebView;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3d

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Landroid/webkit/WebView;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, p4}, Lcom/google/android/gms/internal/zzbc;->zzc(Ljava/lang/String;Z)V

    goto :goto_40

    :cond_3d
    invoke-virtual {p1, v2, p4}, Lcom/google/android/gms/internal/zzbc;->zzc(Ljava/lang/String;Z)V

    :cond_40
    :goto_40
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzbc;->zzcx()Z

    move-result v0

    if-eqz v0, :cond_4b

    iget-object v0, p0, Lcom/google/android/gms/internal/zzbf;->zzta:Lcom/google/android/gms/internal/zzbd;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/zzbd;->zzb(Lcom/google/android/gms/internal/zzbc;)Z
    :try_end_4b
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_4b} :catch_4c
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_4b} :catch_53

    :cond_4b
    goto :goto_5f

    :catch_4c
    move-exception v2

    const-string v0, "Json string may be malformed."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaI(Ljava/lang/String;)V

    goto :goto_5f

    :catch_53
    move-exception v2

    const-string v0, "Failed to get webview content."

    invoke-static {v0, v2}, Lcom/google/android/gms/internal/zzin;->zza(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzbf;->zztb:Lcom/google/android/gms/internal/zzha;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/internal/zzha;->zza(Ljava/lang/Throwable;Z)V

    :goto_5f
    return-void
.end method

.method zza(Landroid/app/ActivityManager$RunningAppProcessInfo;)Z
    .registers 4

    iget v0, p1, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v1, 0x64

    if-ne v0, v1, :cond_8

    const/4 v0, 0x1

    goto :goto_9

    :cond_8
    const/4 v0, 0x0

    :goto_9
    return v0
.end method

.method zza(Landroid/webkit/WebView;Lcom/google/android/gms/internal/zzbc;Z)Z
    .registers 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    invoke-static {}, Lcom/google/android/gms/internal/zzne;->zzsk()Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x0

    return v0

    :cond_8
    invoke-virtual {p2}, Lcom/google/android/gms/internal/zzbc;->zzcC()V

    new-instance v0, Lcom/google/android/gms/internal/zzbf$2;

    invoke-direct {v0, p0, p2, p1, p3}, Lcom/google/android/gms/internal/zzbf$2;-><init>(Lcom/google/android/gms/internal/zzbf;Lcom/google/android/gms/internal/zzbc;Landroid/webkit/WebView;Z)V

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public zzcG()V
    .registers 4

    iget-object v1, p0, Lcom/google/android/gms/internal/zzbf;->zzpV:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzbf;->mStarted:Z

    if-eqz v0, :cond_e

    const-string v0, "Content hash thread already started, quiting..."

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaI(Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_13

    monitor-exit v1

    return-void

    :cond_e
    const/4 v0, 0x1

    :try_start_f
    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzbf;->mStarted:Z
    :try_end_11
    .catchall {:try_start_f .. :try_end_11} :catchall_13

    monitor-exit v1

    goto :goto_16

    :catchall_13
    move-exception v2

    monitor-exit v1

    throw v2

    :goto_16
    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzbf;->start()V

    return-void
.end method

.method zzcH()Z
    .registers 9

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/zzbf;->zzsZ:Lcom/google/android/gms/internal/zzbe;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzbe;->getContext()Landroid/content/Context;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_5} :catch_5a

    move-result-object v2

    if-nez v2, :cond_a

    const/4 v0, 0x0

    return v0

    :cond_a
    const-string v0, "activity"

    :try_start_c
    invoke-virtual {v2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/app/ActivityManager;

    const-string v0, "keyguard"

    invoke-virtual {v2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/app/KeyguardManager;
    :try_end_1c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_1c} :catch_5a

    if-eqz v3, :cond_20

    if-nez v4, :cond_22

    :cond_20
    const/4 v0, 0x0

    return v0

    :cond_22
    :try_start_22
    invoke-virtual {v3}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;
    :try_end_25
    .catch Ljava/lang/Throwable; {:try_start_22 .. :try_end_25} :catch_5a

    move-result-object v5

    if-nez v5, :cond_2a

    const/4 v0, 0x0

    return v0

    :cond_2a
    :try_start_2a
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2e
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_58

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/app/ActivityManager$RunningAppProcessInfo;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    iget v1, v7, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v0, v1, :cond_57

    invoke-virtual {p0, v7}, Lcom/google/android/gms/internal/zzbf;->zza(Landroid/app/ActivityManager$RunningAppProcessInfo;)Z

    move-result v0

    if-eqz v0, :cond_58

    invoke-virtual {v4}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-nez v0, :cond_58

    invoke-virtual {p0, v2}, Lcom/google/android/gms/internal/zzbf;->zzs(Landroid/content/Context;)Z
    :try_end_52
    .catch Ljava/lang/Throwable; {:try_start_2a .. :try_end_52} :catch_5a

    move-result v0

    if-eqz v0, :cond_58

    const/4 v0, 0x1

    return v0

    :cond_57
    goto :goto_2e

    :cond_58
    const/4 v0, 0x0

    return v0

    :catch_5a
    move-exception v2

    const/4 v0, 0x0

    return v0
.end method

.method public zzcI()Lcom/google/android/gms/internal/zzbc;
    .registers 2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzbf;->zzta:Lcom/google/android/gms/internal/zzbd;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzbd;->zzcF()Lcom/google/android/gms/internal/zzbc;

    move-result-object v0

    return-object v0
.end method

.method public zzcJ()V
    .registers 5

    iget-object v2, p0, Lcom/google/android/gms/internal/zzbf;->zzpV:Ljava/lang/Object;

    monitor-enter v2

    const/4 v0, 0x1

    :try_start_4
    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzbf;->zzsY:Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ContentFetchThread: paused, mPause = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/internal/zzbf;->zzsY:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/zzin;->zzaI(Ljava/lang/String;)V
    :try_end_1e
    .catchall {:try_start_4 .. :try_end_1e} :catchall_20

    monitor-exit v2

    goto :goto_23

    :catchall_20
    move-exception v3

    monitor-exit v2

    throw v3

    :goto_23
    return-void
.end method

.method public zzcK()Z
    .registers 2

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzbf;->zzsY:Z

    return v0
.end method

.method zze(Landroid/view/View;)Z
    .registers 3

    if-nez p1, :cond_4

    const/4 v0, 0x0

    return v0

    :cond_4
    new-instance v0, Lcom/google/android/gms/internal/zzbf$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/internal/zzbf$1;-><init>(Lcom/google/android/gms/internal/zzbf;Landroid/view/View;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    return v0
.end method

.method zzf(Landroid/view/View;)V
    .registers 8

    :try_start_0
    new-instance v4, Lcom/google/android/gms/internal/zzbc;

    iget v0, p0, Lcom/google/android/gms/internal/zzbf;->zzsK:I

    iget v1, p0, Lcom/google/android/gms/internal/zzbf;->zztd:I

    iget v2, p0, Lcom/google/android/gms/internal/zzbf;->zzsM:I

    iget v3, p0, Lcom/google/android/gms/internal/zzbf;->zzte:I

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/google/android/gms/internal/zzbc;-><init>(IIII)V

    invoke-virtual {p0, p1, v4}, Lcom/google/android/gms/internal/zzbf;->zza(Landroid/view/View;Lcom/google/android/gms/internal/zzbc;)Lcom/google/android/gms/internal/zzbf$zza;

    move-result-object v5

    invoke-virtual {v4}, Lcom/google/android/gms/internal/zzbc;->zzcD()V

    iget v0, v5, Lcom/google/android/gms/internal/zzbf$zza;->zztm:I

    if-nez v0, :cond_1d

    iget v0, v5, Lcom/google/android/gms/internal/zzbf$zza;->zztn:I
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_1a} :catch_3b

    if-nez v0, :cond_1d

    return-void

    :cond_1d
    :try_start_1d
    iget v0, v5, Lcom/google/android/gms/internal/zzbf$zza;->zztn:I

    if-nez v0, :cond_28

    invoke-virtual {v4}, Lcom/google/android/gms/internal/zzbc;->zzcE()I
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_1d .. :try_end_24} :catch_3b

    move-result v0

    if-nez v0, :cond_28

    return-void

    :cond_28
    :try_start_28
    iget v0, v5, Lcom/google/android/gms/internal/zzbf$zza;->zztn:I

    if-nez v0, :cond_35

    iget-object v0, p0, Lcom/google/android/gms/internal/zzbf;->zzta:Lcom/google/android/gms/internal/zzbd;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/internal/zzbd;->zza(Lcom/google/android/gms/internal/zzbc;)Z
    :try_end_31
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_31} :catch_3b

    move-result v0

    if-eqz v0, :cond_35

    return-void

    :cond_35
    :try_start_35
    iget-object v0, p0, Lcom/google/android/gms/internal/zzbf;->zzta:Lcom/google/android/gms/internal/zzbd;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/internal/zzbd;->zzc(Lcom/google/android/gms/internal/zzbc;)V
    :try_end_3a
    .catch Ljava/lang/Exception; {:try_start_35 .. :try_end_3a} :catch_3b

    goto :goto_47

    :catch_3b
    move-exception v4

    const-string v0, "Exception in fetchContentOnUIThread"

    invoke-static {v0, v4}, Lcom/google/android/gms/internal/zzin;->zzb(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzbf;->zztb:Lcom/google/android/gms/internal/zzha;

    const/4 v1, 0x1

    invoke-virtual {v0, v4, v1}, Lcom/google/android/gms/internal/zzha;->zza(Ljava/lang/Throwable;Z)V

    :goto_47
    return-void
.end method

.method zzs(Landroid/content/Context;)Z
    .registers 4

    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/os/PowerManager;

    if-nez v1, :cond_d

    const/4 v0, 0x0

    return v0

    :cond_d
    invoke-virtual {v1}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    return v0
.end method

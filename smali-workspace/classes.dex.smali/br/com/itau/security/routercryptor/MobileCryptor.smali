.class public final Lbr/com/itau/security/routercryptor/MobileCryptor;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lrc/b;

.field private static final b:B

.field private static c:[B

.field private static d:Ljava/lang/String;

.field private static final f:[B

.field private static g:I


# instance fields
.field private final e:Lrc/j;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    .line 7
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_32

    sput-object v0, Lbr/com/itau/security/routercryptor/MobileCryptor;->f:[B

    const/16 v0, 0x40

    sput v0, Lbr/com/itau/security/routercryptor/MobileCryptor;->g:I

    new-instance v0, Lrc/h;

    invoke-direct {v0}, Lrc/h;-><init>()V

    sput-object v0, Lbr/com/itau/security/routercryptor/MobileCryptor;->a:Lrc/b;

    .line 3000
    sget-object v5, Lbr/com/itau/security/routercryptor/MobileCryptor;->f:[B

    new-instance v0, Ljava/lang/String;

    const/16 v4, 0x33

    const/4 v1, 0x1

    new-array v1, v1, [B

    if-nez v5, :cond_1f

    const/4 v4, -0x4

    :cond_1f
    int-to-byte v2, v4

    const/4 v3, 0x0

    aput-byte v2, v1, v3

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    .line 9
    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Byte;->parseByte(Ljava/lang/String;)B

    move-result v0

    sput-byte v0, Lbr/com/itau/security/routercryptor/MobileCryptor;->b:B

    return-void

    :array_32
    .array-data 1
        0x2dt
        0x3bt
        0x7bt
        -0x50t
    .end array-data
.end method

.method private constructor <init>(Ljava/lang/String;[B)V
    .registers 4

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Lrc/j;

    invoke-direct {v0, p2, p1}, Lrc/j;-><init>([BLjava/lang/String;)V

    iput-object v0, p0, Lbr/com/itau/security/routercryptor/MobileCryptor;->e:Lrc/j;

    .line 16
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;[BB)V
    .registers 4

    .line 5
    invoke-direct {p0, p1, p2}, Lbr/com/itau/security/routercryptor/MobileCryptor;-><init>(Ljava/lang/String;[B)V

    return-void
.end method

.method public static synthetic a()Ljava/lang/String;
    .registers 1

    .line 5
    sget-object v0, Lbr/com/itau/security/routercryptor/MobileCryptor;->d:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic b()[B
    .registers 1

    .line 5
    sget-object v0, Lbr/com/itau/security/routercryptor/MobileCryptor;->c:[B

    return-object v0
.end method

.method public static decryptDataForBase64String(Ljava/lang/String;[BLjava/lang/String;)[B
    .registers 4

    .line 25
    sput-object p1, Lbr/com/itau/security/routercryptor/MobileCryptor;->c:[B

    .line 26
    sput-object p2, Lbr/com/itau/security/routercryptor/MobileCryptor;->d:Ljava/lang/String;

    .line 27
    .line 2044
    invoke-static {}, Lrc/i;->a()Lbr/com/itau/security/routercryptor/MobileCryptor;

    move-result-object v0

    .line 27
    iget-object v0, v0, Lbr/com/itau/security/routercryptor/MobileCryptor;->e:Lrc/j;

    invoke-static {p0, v0}, Lrc/c;->a(Ljava/lang/String;Lrc/j;)[B

    move-result-object v0

    return-object v0
.end method

.method public static encryptBase64String([B[BLjava/lang/String;)Ljava/lang/String;
    .registers 5

    .line 19
    sput-object p1, Lbr/com/itau/security/routercryptor/MobileCryptor;->c:[B

    .line 20
    sput-object p2, Lbr/com/itau/security/routercryptor/MobileCryptor;->d:Ljava/lang/String;

    .line 21
    .line 1044
    invoke-static {}, Lrc/i;->a()Lbr/com/itau/security/routercryptor/MobileCryptor;

    move-result-object v0

    .line 21
    iget-object v0, v0, Lbr/com/itau/security/routercryptor/MobileCryptor;->e:Lrc/j;

    sget-byte v1, Lbr/com/itau/security/routercryptor/MobileCryptor;->b:B

    invoke-static {p0, v0, v1}, Lrc/c;->a([BLrc/j;B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

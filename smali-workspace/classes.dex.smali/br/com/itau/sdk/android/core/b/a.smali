.class public final Lbr/com/itau/sdk/android/core/b/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lokhttp3/Interceptor;


# static fields
.field private static final a:[B

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    const/16 v0, 0x53

    new-array v0, v0, [B

    fill-array-data v0, :array_e

    sput-object v0, Lbr/com/itau/sdk/android/core/b/a;->a:[B

    const/16 v0, 0xa9

    sput v0, Lbr/com/itau/sdk/android/core/b/a;->b:I

    return-void

    :array_e
    .array-data 1
        0x7bt
        0x17t
        0x61t
        -0x24t
        0x3t
        -0x1bt
        0x35t
        0x16t
        0xft
        0x7t
        -0xdt
        0x11t
        0x4t
        -0x51t
        0x8t
        0x51t
        -0x50t
        0x8t
        0x51t
        -0x4bt
        0x4ct
        -0x46t
        0x51t
        -0x4bt
        0x4ct
        -0x46t
        0x51t
        -0x4bt
        0x4ct
        0x3t
        -0x19t
        0x25t
        0x22t
        0x5t
        0xbt
        -0x3t
        0xct
        -0x7t
        -0xft
        0x16t
        0x11t
        0x0t
        0x2t
        0x2t
        0x8t
        -0xbt
        -0x42t
        0x8t
        0x51t
        -0x50t
        0x8t
        0x51t
        -0x50t
        0x4ct
        0x8t
        -0x4bt
        0x8t
        0xct
        0x6t
        0x38t
        0xat
        0x9t
        -0x4bt
        0x4ct
        -0x46t
        0x51t
        0x3t
        -0x1bt
        0x35t
        0x16t
        0xft
        0x7t
        -0xdt
        0x11t
        0x4t
        -0x51t
        0x25t
        0x10t
        -0x8t
        0x18t
        -0x36t
        0x8t
        0x51t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(III)Ljava/lang/String;
    .registers 9

    sget-object v5, Lbr/com/itau/sdk/android/core/b/a;->a:[B

    rsub-int/lit8 p1, p1, 0x42

    new-instance v0, Ljava/lang/String;

    mul-int/lit8 p0, p0, 0x4

    rsub-int/lit8 p0, p0, 0x26

    const/4 v4, 0x0

    mul-int/lit8 p2, p2, 0x2

    add-int/lit8 p2, p2, 0x3c

    new-array v1, p0, [B

    add-int/lit8 p0, p0, -0x1

    if-nez v5, :cond_1c

    move v2, p0

    move v3, p1

    :goto_17
    add-int/2addr v2, v3

    add-int/lit8 p2, v2, -0x3

    add-int/lit8 p1, p1, 0x1

    :cond_1c
    int-to-byte v2, p2

    aput-byte v2, v1, v4

    if-ne v4, p0, :cond_26

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BI)V

    return-object v0

    :cond_26
    add-int/lit8 v4, v4, 0x1

    move v2, p2

    aget-byte v3, v5, p1

    goto :goto_17
.end method


# virtual methods
.method public intercept(Lokhttp3/Interceptor$Chain;)Lokhttp3/Response;
    .registers 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 18
    invoke-interface {p1}, Lokhttp3/Interceptor$Chain;->request()Lokhttp3/Request;

    move-result-object v6

    .line 19
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v7

    .line 21
    const-string v9, ""

    .line 23
    invoke-virtual {v6}, Lokhttp3/Request;->body()Lokhttp3/RequestBody;

    move-result-object v0

    if-eqz v0, :cond_28

    .line 25
    new-instance v10, Lokio/Buffer;

    invoke-direct {v10}, Lokio/Buffer;-><init>()V

    .line 26
    invoke-virtual {v6}, Lokhttp3/Request;->newBuilder()Lokhttp3/Request$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/Request;->body()Lokhttp3/RequestBody;

    move-result-object v11

    .line 27
    invoke-virtual {v11, v10}, Lokhttp3/RequestBody;->writeTo(Lokio/BufferedSink;)V

    .line 28
    invoke-virtual {v10}, Lokio/Buffer;->readUtf8()Ljava/lang/String;

    move-result-object v9

    .line 31
    :cond_28
    sget-object v0, Lbr/com/itau/sdk/android/core/b/a;->a:[B

    const/4 v1, 0x4

    aget-byte v0, v0, v1

    sget v1, Lbr/com/itau/sdk/android/core/b/a;->b:I

    and-int/lit8 v1, v1, 0x7

    const/16 v2, 0x3e

    invoke-static {v0, v2, v1}, Lbr/com/itau/sdk/android/core/b/a;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v6}, Lokhttp3/Request;->method()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 32
    invoke-virtual {v6}, Lokhttp3/Request;->url()Lokhttp3/HttpUrl;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-interface {p1}, Lokhttp3/Interceptor$Chain;->connection()Lokhttp3/Connection;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    invoke-virtual {v6}, Lokhttp3/Request;->headers()Lokhttp3/Headers;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    .line 31
    invoke-static {p0, v0, v1}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 34
    sget-object v0, Lbr/com/itau/sdk/android/core/b/a;->a:[B

    const/16 v1, 0x21

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/b/a;->a:[B

    const/16 v2, 0x29

    aget-byte v1, v1, v2

    add-int/lit8 v2, v1, 0x1

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/b/a;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v9, v1, v2

    invoke-static {p0, v0, v1}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    invoke-interface {p1, v6}, Lokhttp3/Interceptor$Chain;->proceed(Lokhttp3/Request;)Lokhttp3/Response;

    move-result-object v10

    .line 38
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v11

    .line 39
    sget-object v0, Lbr/com/itau/sdk/android/core/b/a;->a:[B

    const/16 v1, 0x29

    aget-byte v0, v0, v1

    sget-object v1, Lbr/com/itau/sdk/android/core/b/a;->a:[B

    const/16 v2, 0x1f

    aget-byte v1, v1, v2

    sget-object v2, Lbr/com/itau/sdk/android/core/b/a;->a:[B

    const/16 v3, 0x29

    aget-byte v2, v2, v3

    invoke-static {v0, v1, v2}, Lbr/com/itau/sdk/android/core/b/a;->a(III)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v10}, Lokhttp3/Response;->code()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    .line 40
    invoke-virtual {v10}, Lokhttp3/Response;->request()Lokhttp3/Request;

    move-result-object v2

    invoke-virtual {v2}, Lokhttp3/Request;->url()Lokhttp3/HttpUrl;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    sub-long v2, v11, v7

    long-to-double v2, v2

    const-wide v4, 0x412e848000000000L    # 1000000.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    invoke-virtual {v10}, Lokhttp3/Response;->headers()Lokhttp3/Headers;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    .line 39
    invoke-static {p0, v0, v1}, Lbr/com/itau/sdk/android/core/l;->a(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 42
    return-object v10
.end method

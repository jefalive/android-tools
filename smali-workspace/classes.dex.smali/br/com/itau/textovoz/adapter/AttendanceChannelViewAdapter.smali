.class public Lbr/com/itau/textovoz/adapter/AttendanceChannelViewAdapter;
.super Landroid/widget/BaseExpandableListAdapter;
.source "AttendanceChannelViewAdapter.java"


# instance fields
.field private context:Landroid/content/Context;

.field private densityDpi:Ljava/lang/String;

.field private listChannel:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lbr/com/itau/textovoz/model/Channel;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .registers 4
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "channels"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Landroid/content/Context;Ljava/util/List<Lbr/com/itau/textovoz/model/Channel;>;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 29
    iput-object p2, p0, Lbr/com/itau/textovoz/adapter/AttendanceChannelViewAdapter;->listChannel:Ljava/util/List;

    .line 30
    iput-object p1, p0, Lbr/com/itau/textovoz/adapter/AttendanceChannelViewAdapter;->context:Landroid/content/Context;

    .line 31
    iget-object v0, p0, Lbr/com/itau/textovoz/adapter/AttendanceChannelViewAdapter;->context:Landroid/content/Context;

    check-cast v0, Lbr/com/itau/textovoz/ui/activity/BaseActivity;

    invoke-static {v0}, Lbr/com/itau/textovoz/util/DisplayUtils;->getDensity(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbr/com/itau/textovoz/adapter/AttendanceChannelViewAdapter;->densityDpi:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method public getChild(II)Lbr/com/itau/textovoz/model/Channel;
    .registers 4
    .param p1, "groupPosition"    # I
    .param p2, "childPosititon"    # I

    .line 36
    iget-object v0, p0, Lbr/com/itau/textovoz/adapter/AttendanceChannelViewAdapter;->listChannel:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/textovoz/model/Channel;

    return-object v0
.end method

.method public bridge synthetic getChild(II)Ljava/lang/Object;
    .registers 4

    .line 20
    invoke-virtual {p0, p1, p2}, Lbr/com/itau/textovoz/adapter/AttendanceChannelViewAdapter;->getChild(II)Lbr/com/itau/textovoz/model/Channel;

    move-result-object v0

    return-object v0
.end method

.method public getChildId(II)J
    .registers 5
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .line 41
    int-to-long v0, p2

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 11
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I
    .param p3, "isLastChild"    # Z
    .param p4, "convertView"    # Landroid/view/View;
    .param p5, "parent"    # Landroid/view/ViewGroup;

    .line 47
    move-object v2, p4

    .line 49
    .local v2, "view":Landroid/view/View;
    invoke-virtual {p0, p1, p2}, Lbr/com/itau/textovoz/adapter/AttendanceChannelViewAdapter;->getChild(II)Lbr/com/itau/textovoz/model/Channel;

    move-result-object v3

    .line 50
    .local v3, "subItems":Lbr/com/itau/textovoz/model/Channel;
    if-nez v2, :cond_19

    .line 51
    iget-object v0, p0, Lbr/com/itau/textovoz/adapter/AttendanceChannelViewAdapter;->context:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/view/LayoutInflater;

    .line 52
    .local v4, "inflater":Landroid/view/LayoutInflater;
    sget v0, Lbr/com/itau/textovoz/R$layout;->view_attendance_channel:I

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 55
    .end local v4    # "inflater":Landroid/view/LayoutInflater;
    :cond_19
    sget v0, Lbr/com/itau/textovoz/R$id;->text_attendance_channel_description:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/TextView;

    .line 56
    .local v4, "textChannelDescription":Landroid/widget/TextView;
    invoke-virtual {v3}, Lbr/com/itau/textovoz/model/Channel;->getExplanation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    return-object v2
.end method

.method public getChildrenCount(I)I
    .registers 3
    .param p1, "groupPosition"    # I

    .line 63
    const/4 v0, 0x1

    return v0
.end method

.method public getGroup(I)Ljava/lang/Object;
    .registers 3
    .param p1, "groupPosition"    # I

    .line 68
    iget-object v0, p0, Lbr/com/itau/textovoz/adapter/AttendanceChannelViewAdapter;->listChannel:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getGroupCount()I
    .registers 2

    .line 73
    iget-object v0, p0, Lbr/com/itau/textovoz/adapter/AttendanceChannelViewAdapter;->listChannel:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getGroupId(I)J
    .registers 4
    .param p1, "groupPosition"    # I

    .line 78
    int-to-long v0, p1

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 12
    .param p1, "groupPosition"    # I
    .param p2, "isExpanded"    # Z
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .line 84
    move-object v3, p3

    .line 86
    .local v3, "view":Landroid/view/View;
    if-nez v3, :cond_15

    .line 87
    iget-object v0, p0, Lbr/com/itau/textovoz/adapter/AttendanceChannelViewAdapter;->context:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/view/LayoutInflater;

    .line 88
    .local v4, "infalInflater":Landroid/view/LayoutInflater;
    sget v0, Lbr/com/itau/textovoz/R$layout;->view_attendance_channel_group:I

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 91
    .end local v4    # "infalInflater":Landroid/view/LayoutInflater;
    :cond_15
    sget v0, Lbr/com/itau/textovoz/R$id;->text_attendance_channel:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/TextView;

    .line 93
    .local v4, "textChannel":Landroid/widget/TextView;
    iget-object v0, p0, Lbr/com/itau/textovoz/adapter/AttendanceChannelViewAdapter;->listChannel:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbr/com/itau/textovoz/model/Channel;

    invoke-virtual {v0}, Lbr/com/itau/textovoz/model/Channel;->getChannel()Ljava/lang/String;

    move-result-object v0

    const-string v1, "_"

    const-string v2, " "

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    sget v0, Lbr/com/itau/textovoz/R$id;->ic_attendance_channel:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/ImageView;

    .line 98
    .local v5, "icChannel":Landroid/widget/ImageView;
    iget-object v0, p0, Lbr/com/itau/textovoz/adapter/AttendanceChannelViewAdapter;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lbr/com/itau/textovoz/adapter/AttendanceChannelViewAdapter;->listChannel:Ljava/util/List;

    .line 99
    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbr/com/itau/textovoz/model/Channel;

    invoke-virtual {v2}, Lbr/com/itau/textovoz/model/Channel;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "android/"

    .line 100
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbr/com/itau/textovoz/adapter/AttendanceChannelViewAdapter;->densityDpi:Ljava/lang/String;

    .line 101
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lbr/com/itau/textovoz/adapter/AttendanceChannelViewAdapter;->listChannel:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbr/com/itau/textovoz/model/Channel;

    invoke-virtual {v2}, Lbr/com/itau/textovoz/model/Channel;->getIcon()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".png"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 99
    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    sget v1, Lbr/com/itau/textovoz/R$drawable;->itausdkcore_ic_seta_cima:I

    .line 102
    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 104
    sget v0, Lbr/com/itau/textovoz/R$id;->ic_attendance_arrow:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/ImageView;

    .line 105
    .local v6, "icArrow":Landroid/widget/ImageView;
    if-eqz p2, :cond_9a

    sget v0, Lbr/com/itau/textovoz/R$drawable;->ic_seta_cima:I

    goto :goto_9c

    :cond_9a
    sget v0, Lbr/com/itau/textovoz/R$drawable;->ic_seta_baixo:I

    :goto_9c
    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 106
    if-eqz p2, :cond_aa

    iget-object v0, p0, Lbr/com/itau/textovoz/adapter/AttendanceChannelViewAdapter;->context:Landroid/content/Context;

    sget v1, Lbr/com/itau/textovoz/R$string;->close:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_b2

    :cond_aa
    iget-object v0, p0, Lbr/com/itau/textovoz/adapter/AttendanceChannelViewAdapter;->context:Landroid/content/Context;

    sget v1, Lbr/com/itau/textovoz/R$string;->application_action_open:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_b2
    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 108
    return-object v3
.end method

.method public hasStableIds()Z
    .registers 2

    .line 113
    const/4 v0, 0x0

    return v0
.end method

.method public isChildSelectable(II)Z
    .registers 4
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .line 118
    const/4 v0, 0x1

    return v0
.end method

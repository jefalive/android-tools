.class public Lcom/itau/empresas/api/model/PagamentosVO;
.super Ljava/lang/Object;
.source "PagamentosVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private pagamentos:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "pagamentos"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/api/model/PagamentoVO;>;"
        }
    .end annotation
.end field

.field private totalPendenteBoleto:Ljava/lang/Integer;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "total_pendente_boleto"
    .end annotation
.end field

.field private totalPendenteConcessionaria:Ljava/lang/Integer;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "total_pendente_concessionaria"
    .end annotation
.end field

.field private valorPendenteBoleto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_pendente_boleto"
    .end annotation
.end field

.field private valorPendenteConcessionaria:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_pendente_concessionaria"
    .end annotation
.end field

.field private valorPendenteTotal:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_pendente_total"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

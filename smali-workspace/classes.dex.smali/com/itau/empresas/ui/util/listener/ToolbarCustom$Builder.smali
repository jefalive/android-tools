.class public Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;
.super Ljava/lang/Object;
.source "ToolbarCustom.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/ui/util/listener/ToolbarCustom;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final activity:Landroid/support/v7/app/AppCompatActivity;

.field private idDrawable:I

.field private idTextoAcessibilidade:I

.field private idTitulo:I

.field private isFinishActivity:Z

.field private titulo:Ljava/lang/String;

.field private final toolbar:Landroid/support/v7/widget/Toolbar;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/Toolbar;Landroid/support/v7/app/AppCompatActivity;)V
    .registers 4
    .param p1, "toolbar"    # Landroid/support/v7/widget/Toolbar;
    .param p2, "activity"    # Landroid/support/v7/app/AppCompatActivity;

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    const v0, 0x7f02014f

    iput v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->idDrawable:I

    .line 78
    iput-object p2, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->activity:Landroid/support/v7/app/AppCompatActivity;

    .line 79
    iput-object p1, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 80
    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)Landroid/support/v7/app/AppCompatActivity;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->activity:Landroid/support/v7/app/AppCompatActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)Landroid/support/v7/widget/Toolbar;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->toolbar:Landroid/support/v7/widget/Toolbar;

    return-object v0
.end method

.method static synthetic access$200(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)I
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    .line 67
    iget v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->idTextoAcessibilidade:I

    return v0
.end method

.method static synthetic access$300(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)I
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    .line 67
    iget v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->idTitulo:I

    return v0
.end method

.method static synthetic access$400(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)Ljava/lang/String;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    .line 67
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->titulo:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)Z
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    .line 67
    iget-boolean v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->isFinishActivity:Z

    return v0
.end method

.method static synthetic access$700(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;)I
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;

    .line 67
    iget v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->idDrawable:I

    return v0
.end method


# virtual methods
.method public build()Lcom/itau/empresas/ui/util/listener/ToolbarCustom;
    .registers 3

    .line 111
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->toolbar:Landroid/support/v7/widget/Toolbar;

    if-nez v0, :cond_c

    .line 112
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A toolbar est\u00e1 nula"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :cond_c
    iget-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->activity:Landroid/support/v7/app/AppCompatActivity;

    if-nez v0, :cond_18

    .line 114
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "A activity est\u00e1 nula"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_18
    new-instance v0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/itau/empresas/ui/util/listener/ToolbarCustom;-><init>(Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;Lcom/itau/empresas/ui/util/listener/ToolbarCustom$1;)V

    return-object v0
.end method

.method public comClickListenerFinish()Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;
    .registers 2

    .line 95
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->isFinishActivity:Z

    .line 96
    return-object p0
.end method

.method public comContentDescription(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;
    .registers 2
    .param p1, "idTexto"    # I

    .line 100
    iput p1, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->idTextoAcessibilidade:I

    .line 101
    return-object p0
.end method

.method public comTitulo(I)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;
    .registers 3
    .param p1, "idTitulo"    # I

    .line 83
    iput p1, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->idTitulo:I

    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->titulo:Ljava/lang/String;

    .line 85
    return-object p0
.end method

.method public comTitulo(Ljava/lang/String;)Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;
    .registers 3
    .param p1, "titulo"    # Ljava/lang/String;

    .line 89
    iput-object p1, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->titulo:Ljava/lang/String;

    .line 90
    const/4 v0, 0x0

    iput v0, p0, Lcom/itau/empresas/ui/util/listener/ToolbarCustom$Builder;->idTitulo:I

    .line 91
    return-object p0
.end method

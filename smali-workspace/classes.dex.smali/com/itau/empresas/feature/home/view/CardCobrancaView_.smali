.class public final Lcom/itau/empresas/feature/home/view/CardCobrancaView_;
.super Lcom/itau/empresas/feature/home/view/CardCobrancaView;
.source "CardCobrancaView_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# instance fields
.field private alreadyInflated_:Z

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .param p1, "context"    # Landroid/content/Context;

    .line 42
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/view/CardCobrancaView;-><init>(Landroid/content/Context;)V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->alreadyInflated_:Z

    .line 39
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 43
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->init_()V

    .line 44
    return-void
.end method

.method public static build(Landroid/content/Context;)Lcom/itau/empresas/feature/home/view/CardCobrancaView;
    .registers 2
    .param p0, "context"    # Landroid/content/Context;

    .line 47
    new-instance v0, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;-><init>(Landroid/content/Context;)V

    .line 48
    .local v0, "instance":Lcom/itau/empresas/feature/home/view/CardCobrancaView_;
    invoke-virtual {v0}, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->onFinishInflate()V

    .line 49
    return-object v0
.end method

.method private init_()V
    .registers 3

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 69
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 70
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/PreferenciaUtils_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/PreferenciaUtils_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->preferenciaUtils:Lcom/itau/empresas/PreferenciaUtils;

    .line 71
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/home/CobrancaController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/home/CobrancaController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->controller:Lcom/itau/empresas/feature/home/CobrancaController;

    .line 72
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 73
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .registers 2

    .line 60
    iget-boolean v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->alreadyInflated_:Z

    if-nez v0, :cond_c

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->alreadyInflated_:Z

    .line 62
    iget-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 64
    :cond_c
    invoke-super {p0}, Lcom/itau/empresas/feature/home/view/CardCobrancaView;->onFinishInflate()V

    .line 65
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 4
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 77
    const v0, 0x7f0e0645

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->textoExpandir:Landroid/widget/TextView;

    .line 78
    const v0, 0x7f0e0643

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->textoTituloCard:Landroid/widget/TextView;

    .line 79
    const v0, 0x7f0e0646

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/ui/view/TextViewIcon;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->icExpandir:Lcom/itau/empresas/ui/view/TextViewIcon;

    .line 80
    const v0, 0x7f0e0648

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->frameInterno:Landroid/widget/FrameLayout;

    .line 81
    const v0, 0x7f0e041c

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->divisorCard:Landroid/view/View;

    .line 82
    const v0, 0x7f0e064a

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->textoVerMaisEsquerdo:Landroid/widget/TextView;

    .line 83
    const v0, 0x7f0e064b

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->textoVerMaisDireito:Landroid/widget/TextView;

    .line 84
    const v0, 0x7f0e0647

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->progressCard:Landroid/widget/ProgressBar;

    .line 85
    const v0, 0x7f0e05d9

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->textoValorTotalReceber:Landroid/widget/TextView;

    .line 86
    const v0, 0x7f0e05da

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->graficoCobranca:Lbr/com/itau/widgets/graphicalfriedcake/GraphicView;

    .line 87
    const v0, 0x7f0e05d6

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->textoCardCobrancaSemMovimentacao:Landroid/widget/TextView;

    .line 88
    const v0, 0x7f0e05d7

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->llCardCobrancaDetalhe:Landroid/widget/LinearLayout;

    .line 89
    const v0, 0x7f0e0644

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 91
    .local v1, "view_ll_expandir":Landroid/view/View;
    if-eqz v1, :cond_93

    .line 92
    new-instance v0, Lcom/itau/empresas/feature/home/view/CardCobrancaView_$1;

    invoke-direct {v0, p0}, Lcom/itau/empresas/feature/home/view/CardCobrancaView_$1;-><init>(Lcom/itau/empresas/feature/home/view/CardCobrancaView_;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    :cond_93
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/view/CardCobrancaView_;->afterViews()V

    .line 102
    return-void
.end method

.class public final Lbr/com/concretesolutions/canarinho/DigitoPara;
.super Ljava/lang/Object;
.source "DigitoPara.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;
    }
.end annotation


# instance fields
.field private final complementar:Z

.field private final modulo:I

.field private final multiplicadores:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/Integer;>;"
        }
    .end annotation
.end field

.field private final numero:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/Integer;>;"
        }
    .end annotation
.end field

.field private final somarIndividual:Z

.field private final substituicoes:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<Ljava/lang/String;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;)V
    .registers 3
    .param p1, "builder"    # Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara;->numero:Ljava/util/List;

    .line 49
    # getter for: Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->multiplicadores:Ljava/util/List;
    invoke-static {p1}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->access$000(Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara;->multiplicadores:Ljava/util/List;

    .line 50
    # getter for: Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->complementar:Z
    invoke-static {p1}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->access$100(Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara;->complementar:Z

    .line 51
    # getter for: Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->modulo:I
    invoke-static {p1}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->access$200(Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;)I

    move-result v0

    iput v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara;->modulo:I

    .line 52
    # getter for: Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->somarIndividual:Z
    invoke-static {p1}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->access$300(Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara;->somarIndividual:Z

    .line 53
    # getter for: Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->substituicoes:Landroid/util/SparseArray;
    invoke-static {p1}, Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;->access$400(Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;)Landroid/util/SparseArray;

    move-result-object v0

    iput-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara;->substituicoes:Landroid/util/SparseArray;

    .line 54
    return-void
.end method

.method synthetic constructor <init>(Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;Lbr/com/concretesolutions/canarinho/DigitoPara$1;)V
    .registers 3
    .param p1, "x0"    # Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;
    .param p2, "x1"    # Lbr/com/concretesolutions/canarinho/DigitoPara$1;

    .line 38
    invoke-direct {p0, p1}, Lbr/com/concretesolutions/canarinho/DigitoPara;-><init>(Lbr/com/concretesolutions/canarinho/DigitoPara$Builder;)V

    return-void
.end method

.method private proximoMultiplicador(I)I
    .registers 4
    .param p1, "multiplicadorDaVez"    # I

    .line 114
    add-int/lit8 v1, p1, 0x1

    .line 116
    .local v1, "multiplicador":I
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara;->multiplicadores:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v1, v0, :cond_b

    .line 117
    const/4 v1, 0x0

    .line 120
    :cond_b
    return v1
.end method

.method private somaDigitos(I)I
    .registers 4
    .param p1, "total"    # I

    .line 105
    div-int/lit8 v0, p1, 0xa

    rem-int/lit8 v1, p1, 0xa

    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final calcula(Ljava/lang/String;)Ljava/lang/String;
    .registers 10
    .param p1, "trecho"    # Ljava/lang/String;

    .line 65
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara;->numero:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 67
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    .line 69
    .local v2, "digitos":[C
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_a
    array-length v0, v2

    if-ge v3, v0, :cond_1f

    .line 70
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara;->numero:Ljava/util/List;

    aget-char v1, v2, v3

    invoke-static {v1}, Ljava/lang/Character;->getNumericValue(C)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    add-int/lit8 v3, v3, 0x1

    goto :goto_a

    .line 73
    .end local v3    # "i":I
    :cond_1f
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara;->numero:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 75
    const/4 v3, 0x0

    .line 76
    .local v3, "soma":I
    const/4 v4, 0x0

    .line 78
    .local v4, "multiplicadorDaVez":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_27
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara;->numero:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v5, v0, :cond_5b

    .line 79
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara;->multiplicadores:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 80
    .local v6, "multiplicador":I
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara;->numero:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    mul-int v7, v0, v6

    .line 81
    .local v7, "total":I
    iget-boolean v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara;->somarIndividual:Z

    if-eqz v0, :cond_52

    invoke-direct {p0, v7}, Lbr/com/concretesolutions/canarinho/DigitoPara;->somaDigitos(I)I

    move-result v0

    goto :goto_53

    :cond_52
    move v0, v7

    :goto_53
    add-int/2addr v3, v0

    .line 82
    invoke-direct {p0, v4}, Lbr/com/concretesolutions/canarinho/DigitoPara;->proximoMultiplicador(I)I

    move-result v4

    .line 78
    .end local v6    # "multiplicador":I
    .end local v7    # "total":I
    add-int/lit8 v5, v5, 0x1

    goto :goto_27

    .line 85
    .end local v5    # "i":I
    :cond_5b
    iget v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara;->modulo:I

    rem-int v5, v3, v0

    .line 87
    .local v5, "resultado":I
    iget-boolean v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara;->complementar:Z

    if-eqz v0, :cond_67

    .line 88
    iget v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara;->modulo:I

    sub-int v5, v0, v5

    .line 91
    :cond_67
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara;->substituicoes:Landroid/util/SparseArray;

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_78

    .line 92
    iget-object v0, p0, Lbr/com/concretesolutions/canarinho/DigitoPara;->substituicoes:Landroid/util/SparseArray;

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0

    .line 95
    :cond_78
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

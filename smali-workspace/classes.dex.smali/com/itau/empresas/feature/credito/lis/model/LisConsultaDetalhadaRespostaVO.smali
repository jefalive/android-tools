.class public Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;
.super Ljava/lang/Object;
.source "LisConsultaDetalhadaRespostaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public contaVinculada:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta_vinculada"
    .end annotation
.end field

.field public dataVencimentoOperacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_vencimento_operacao"
    .end annotation
.end field

.field public descricaoProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_produto"
    .end annotation
.end field

.field public diaPagamentoEncargo:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dia_pagamento_encargo"
    .end annotation
.end field

.field public periodicidadeCapitalizacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "periodicidade_capitalizacao"
    .end annotation
.end field

.field public taxaCetAno:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_cet_ano"
    .end annotation
.end field

.field public taxaCetMes:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_cet_mes"
    .end annotation
.end field

.field public taxaJurosAno:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros_ano"
    .end annotation
.end field

.field public taxaJurosMes:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros_mes"
    .end annotation
.end field

.field public textoValorLimiteAdicional:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "texto_valor_limite_adicional"
    .end annotation
.end field

.field public valorIof:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_iof"
    .end annotation
.end field

.field public valorLimiteAdicional:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_limite_adicional"
    .end annotation
.end field

.field public valorLimiteContratado:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_limite_contratado"
    .end annotation
.end field

.field public valorLimiteContratadoRecebivel:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_limite_contratado_recebivel"
    .end annotation
.end field

.field public valorLimiteRecebivel:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_limite_recebivel"
    .end annotation
.end field

.field public valorTarifaContratacao:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_tarifa_contratacao"
    .end annotation
.end field

.field public valorTotalOperacao:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_total_operacao"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getContaVinculada()Ljava/lang/String;
    .registers 2

    .line 173
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->contaVinculada:Ljava/lang/String;

    return-object v0
.end method

.method public getDataVencimentoOperacao()Ljava/lang/String;
    .registers 2

    .line 149
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->dataVencimentoOperacao:Ljava/lang/String;

    return-object v0
.end method

.method public getDescricaoProduto()Ljava/lang/String;
    .registers 2

    .line 69
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->descricaoProduto:Ljava/lang/String;

    return-object v0
.end method

.method public getDiaPagamentoEncargo()Ljava/lang/String;
    .registers 2

    .line 85
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->diaPagamentoEncargo:Ljava/lang/String;

    return-object v0
.end method

.method public getPeriodicidadeCapitalizacao()Ljava/lang/String;
    .registers 2

    .line 141
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->periodicidadeCapitalizacao:Ljava/lang/String;

    return-object v0
.end method

.method public getTaxaCetAno()F
    .registers 2

    .line 125
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->taxaCetAno:F

    return v0
.end method

.method public getTaxaCetMes()F
    .registers 2

    .line 133
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->taxaCetMes:F

    return v0
.end method

.method public getTaxaJurosAno()F
    .registers 2

    .line 117
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->taxaJurosAno:F

    return v0
.end method

.method public getTaxaJurosMes()F
    .registers 2

    .line 109
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->taxaJurosMes:F

    return v0
.end method

.method public getTextoValorLimiteAdicional()Ljava/lang/String;
    .registers 2

    .line 165
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->textoValorLimiteAdicional:Ljava/lang/String;

    return-object v0
.end method

.method public getValorIof()F
    .registers 2

    .line 61
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->valorIof:F

    return v0
.end method

.method public getValorLimiteAdicional()Ljava/lang/Float;
    .registers 2

    .line 157
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->valorLimiteAdicional:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getValorLimiteContratado()F
    .registers 2

    .line 77
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->valorLimiteContratado:F

    return v0
.end method

.method public getValorLimiteContratadoRecebivel()F
    .registers 2

    .line 189
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->valorLimiteContratadoRecebivel:F

    return v0
.end method

.method public getValorLimiteRecebivel()F
    .registers 2

    .line 181
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->valorLimiteRecebivel:F

    return v0
.end method

.method public getValorTarifaContratacao()F
    .registers 2

    .line 93
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->valorTarifaContratacao:F

    return v0
.end method

.method public getValorTotalOperacao()F
    .registers 2

    .line 101
    iget v0, p0, Lcom/itau/empresas/feature/credito/lis/model/LisConsultaDetalhadaRespostaVO;->valorTotalOperacao:F

    return v0
.end method

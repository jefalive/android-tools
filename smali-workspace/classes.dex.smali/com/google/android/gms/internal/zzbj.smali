.class public Lcom/google/android/gms/internal/zzbj;
.super Lcom/google/android/gms/internal/zzbg;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzhb;
.end annotation


# instance fields
.field private zztw:Ljava/security/MessageDigest;


# direct methods
.method public constructor <init>()V
    .registers 1

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzbg;-><init>()V

    return-void
.end method


# virtual methods
.method zza([Ljava/lang/String;)[B
    .registers 5

    array-length v0, p1

    new-array v1, v0, [B

    const/4 v2, 0x0

    :goto_4
    array-length v0, p1

    if-ge v2, v0, :cond_16

    aget-object v0, p1, v2

    invoke-static {v0}, Lcom/google/android/gms/internal/zzbi;->zzx(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/zzbj;->zzk(I)B

    move-result v0

    aput-byte v0, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_16
    return-object v1
.end method

.method zzk(I)B
    .registers 6

    and-int/lit16 v0, p1, 0xff

    const v1, 0xff00

    and-int/2addr v1, p1

    shr-int/lit8 v1, v1, 0x8

    xor-int/2addr v0, v1

    const/high16 v1, 0xff0000

    and-int/2addr v1, p1

    shr-int/lit8 v1, v1, 0x10

    xor-int/2addr v0, v1

    const/high16 v1, -0x1000000

    and-int/2addr v1, p1

    shr-int/lit8 v1, v1, 0x18

    xor-int v3, v0, v1

    int-to-byte v0, v3

    return v0
.end method

.method public zzu(Ljava/lang/String;)[B
    .registers 12

    const-string v0, " "

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/gms/internal/zzbj;->zza([Ljava/lang/String;)[B

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzbj;->zzcL()Ljava/security/MessageDigest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzbj;->zztw:Ljava/security/MessageDigest;

    iget-object v5, p0, Lcom/google/android/gms/internal/zzbj;->zzpV:Ljava/lang/Object;

    monitor-enter v5

    :try_start_13
    iget-object v0, p0, Lcom/google/android/gms/internal/zzbj;->zztw:Ljava/security/MessageDigest;

    if-nez v0, :cond_1c

    const/4 v0, 0x0

    new-array v0, v0, [B
    :try_end_1a
    .catchall {:try_start_13 .. :try_end_1a} :catchall_3d

    monitor-exit v5

    return-object v0

    :cond_1c
    :try_start_1c
    iget-object v0, p0, Lcom/google/android/gms/internal/zzbj;->zztw:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->reset()V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzbj;->zztw:Ljava/security/MessageDigest;

    invoke-virtual {v0, v4}, Ljava/security/MessageDigest;->update([B)V

    iget-object v0, p0, Lcom/google/android/gms/internal/zzbj;->zztw:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v6

    const/4 v7, 0x4

    array-length v0, v6

    if-le v0, v7, :cond_32

    move v0, v7

    goto :goto_33

    :cond_32
    array-length v0, v6

    :goto_33
    new-array v8, v0, [B

    array-length v0, v8

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v6, v1, v8, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_3b
    .catchall {:try_start_1c .. :try_end_3b} :catchall_3d

    monitor-exit v5

    return-object v8

    :catchall_3d
    move-exception v9

    monitor-exit v5

    throw v9
.end method

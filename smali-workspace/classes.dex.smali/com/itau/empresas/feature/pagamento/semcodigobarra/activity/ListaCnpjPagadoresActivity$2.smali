.class Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity$2;
.super Ljava/lang/Object;
.source "ListaCnpjPagadoresActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->carregaElementosDaTela()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;

    .line 61
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity$2;->this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 2
    .param p1, "s"    # Landroid/text/Editable;

    .line 73
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .line 65
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 6
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .line 68
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity$2;->this$0:Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;

    iget-object v0, v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/activity/ListaCnpjPagadoresActivity;->cnpjPagamentosAdapter:Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/CnpjPagamentosAdapter;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/CnpjPagamentosAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 69
    return-void
.end method

.class public final Lcom/google/android/gms/internal/zzsz$zzc;
.super Lcom/google/android/gms/internal/zzso;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/zzsz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "zzc"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/zzso<Lcom/google/android/gms/internal/zzsz$zzc;>;"
    }
.end annotation


# instance fields
.field public zzbuO:[B

.field public zzbuP:[[B

.field public zzbuQ:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    invoke-direct {p0}, Lcom/google/android/gms/internal/zzso;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/zzsz$zzc;->zzJE()Lcom/google/android/gms/internal/zzsz$zzc;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 5
    .param p1, "o"    # Ljava/lang/Object;

    if-ne p1, p0, :cond_4

    const/4 v0, 0x1

    return v0

    :cond_4
    instance-of v0, p1, Lcom/google/android/gms/internal/zzsz$zzc;

    if-nez v0, :cond_a

    const/4 v0, 0x0

    return v0

    :cond_a
    move-object v2, p1

    check-cast v2, Lcom/google/android/gms/internal/zzsz$zzc;

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuO:[B

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuO:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_19

    const/4 v0, 0x0

    return v0

    :cond_19
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuP:[[B

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuP:[[B

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/zzss;->zza([[B[[B)Z

    move-result v0

    if-nez v0, :cond_25

    const/4 v0, 0x0

    return v0

    :cond_25
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuQ:Z

    iget-boolean v1, v2, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuQ:Z

    if-eq v0, v1, :cond_2d

    const/4 v0, 0x0

    return v0

    :cond_2d
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    if-eqz v0, :cond_39

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzsq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_49

    :cond_39
    iget-object v0, v2, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    if-eqz v0, :cond_45

    iget-object v0, v2, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/zzsq;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_47

    :cond_45
    const/4 v0, 0x1

    goto :goto_48

    :cond_47
    const/4 v0, 0x0

    :goto_48
    return v0

    :cond_49
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    iget-object v1, v2, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/zzsq;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .registers 4

    const/16 v2, 0x11

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit16 v2, v0, 0x20f

    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuO:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    add-int v2, v0, v1

    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuP:[[B

    invoke-static {v1}, Lcom/google/android/gms/internal/zzss;->zza([[B)I

    move-result v1

    add-int v2, v0, v1

    mul-int/lit8 v0, v2, 0x1f

    iget-boolean v1, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuQ:Z

    if-eqz v1, :cond_2d

    const/16 v1, 0x4cf

    goto :goto_2f

    :cond_2d
    const/16 v1, 0x4d5

    :goto_2f
    add-int v2, v0, v1

    mul-int/lit8 v0, v2, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    if-eqz v1, :cond_3f

    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zzsq;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_41

    :cond_3f
    const/4 v1, 0x0

    goto :goto_47

    :cond_41
    iget-object v1, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/zzsq;->hashCode()I

    move-result v1

    :goto_47
    add-int v2, v0, v1

    return v2
.end method

.method public synthetic mergeFrom(Lcom/google/android/gms/internal/zzsm;)Lcom/google/android/gms/internal/zzsu;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/zzsz$zzc;->zzU(Lcom/google/android/gms/internal/zzsm;)Lcom/google/android/gms/internal/zzsz$zzc;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/android/gms/internal/zzsn;)V
    .registers 6
    .param p1, "output"    # Lcom/google/android/gms/internal/zzsn;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuO:[B

    sget-object v1, Lcom/google/android/gms/internal/zzsx;->zzbuD:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuO:[B

    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/internal/zzsn;->zza(I[B)V

    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuP:[[B

    if-eqz v0, :cond_2c

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuP:[[B

    array-length v0, v0

    if-lez v0, :cond_2c

    const/4 v2, 0x0

    :goto_1a
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuP:[[B

    array-length v0, v0

    if-ge v2, v0, :cond_2c

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuP:[[B

    aget-object v3, v0, v2

    if-eqz v3, :cond_29

    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, Lcom/google/android/gms/internal/zzsn;->zza(I[B)V

    :cond_29
    add-int/lit8 v2, v2, 0x1

    goto :goto_1a

    :cond_2c
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuQ:Z

    if-eqz v0, :cond_36

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuQ:Z

    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/internal/zzsn;->zze(IZ)V

    :cond_36
    invoke-super {p0, p1}, Lcom/google/android/gms/internal/zzso;->writeTo(Lcom/google/android/gms/internal/zzsn;)V

    return-void
.end method

.method public zzJE()Lcom/google/android/gms/internal/zzsz$zzc;
    .registers 2

    sget-object v0, Lcom/google/android/gms/internal/zzsx;->zzbuD:[B

    iput-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuO:[B

    sget-object v0, Lcom/google/android/gms/internal/zzsx;->zzbuC:[[B

    iput-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuP:[[B

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuQ:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuj:Lcom/google/android/gms/internal/zzsq;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuu:I

    return-object p0
.end method

.method public zzU(Lcom/google/android/gms/internal/zzsm;)Lcom/google/android/gms/internal/zzsz$zzc;
    .registers 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzIX()I

    move-result v3

    sparse-switch v3, :sswitch_data_56

    goto :goto_9

    :sswitch_8
    return-object p0

    :goto_9
    invoke-virtual {p0, p1, v3}, Lcom/google/android/gms/internal/zzsz$zzc;->zza(Lcom/google/android/gms/internal/zzsm;I)Z

    move-result v0

    if-nez v0, :cond_53

    return-object p0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->readBytes()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuO:[B

    goto :goto_53

    :sswitch_17
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/zzsx;->zzc(Lcom/google/android/gms/internal/zzsm;I)I

    move-result v4

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuP:[[B

    if-nez v0, :cond_23

    const/4 v5, 0x0

    goto :goto_26

    :cond_23
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuP:[[B

    array-length v5, v0

    :goto_26
    add-int v0, v5, v4

    new-array v6, v0, [[B

    if-eqz v5, :cond_33

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuP:[[B

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v6, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_33
    :goto_33
    array-length v0, v6

    add-int/lit8 v0, v0, -0x1

    if-ge v5, v0, :cond_44

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->readBytes()[B

    move-result-object v0

    aput-object v0, v6, v5

    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzIX()I

    add-int/lit8 v5, v5, 0x1

    goto :goto_33

    :cond_44
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->readBytes()[B

    move-result-object v0

    aput-object v0, v6, v5

    iput-object v6, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuP:[[B

    goto :goto_53

    :sswitch_4d
    invoke-virtual {p1}, Lcom/google/android/gms/internal/zzsm;->zzJc()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuQ:Z

    :cond_53
    :goto_53
    goto/16 :goto_0

    nop

    :sswitch_data_56
    .sparse-switch
        0x0 -> :sswitch_8
        0xa -> :sswitch_10
        0x12 -> :sswitch_17
        0x18 -> :sswitch_4d
    .end sparse-switch
.end method

.method protected zzz()I
    .registers 8

    invoke-super {p0}, Lcom/google/android/gms/internal/zzso;->zzz()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuO:[B

    sget-object v1, Lcom/google/android/gms/internal/zzsx;->zzbuD:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuO:[B

    const/4 v1, 0x1

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzb(I[B)I

    move-result v0

    add-int/2addr v2, v0

    :cond_16
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuP:[[B

    if-eqz v0, :cond_3b

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuP:[[B

    array-length v0, v0

    if-lez v0, :cond_3b

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_22
    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuP:[[B

    array-length v0, v0

    if-ge v5, v0, :cond_37

    iget-object v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuP:[[B

    aget-object v6, v0, v5

    if-eqz v6, :cond_34

    add-int/lit8 v3, v3, 0x1

    invoke-static {v6}, Lcom/google/android/gms/internal/zzsn;->zzG([B)I

    move-result v0

    add-int/2addr v4, v0

    :cond_34
    add-int/lit8 v5, v5, 0x1

    goto :goto_22

    :cond_37
    add-int/2addr v2, v4

    mul-int/lit8 v0, v3, 0x1

    add-int/2addr v2, v0

    :cond_3b
    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuQ:Z

    if-eqz v0, :cond_47

    iget-boolean v0, p0, Lcom/google/android/gms/internal/zzsz$zzc;->zzbuQ:Z

    const/4 v1, 0x3

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/zzsn;->zzf(IZ)I

    move-result v0

    add-int/2addr v2, v0

    :cond_47
    return v2
.end method

.class public Lcom/itau/empresas/feature/home/EmptyStateFragment_$FragmentBuilder_;
.super Lorg/androidannotations/api/builder/FragmentBuilder;
.source "EmptyStateFragment_.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/home/EmptyStateFragment_;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FragmentBuilder_"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/androidannotations/api/builder/FragmentBuilder<Lcom/itau/empresas/feature/home/EmptyStateFragment_$FragmentBuilder_;Lcom/itau/empresas/feature/home/EmptyStateFragment;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 98
    invoke-direct {p0}, Lorg/androidannotations/api/builder/FragmentBuilder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/itau/empresas/feature/home/EmptyStateFragment;
    .registers 3

    .line 104
    new-instance v1, Lcom/itau/empresas/feature/home/EmptyStateFragment_;

    invoke-direct {v1}, Lcom/itau/empresas/feature/home/EmptyStateFragment_;-><init>()V

    .line 105
    .local v1, "fragment_":Lcom/itau/empresas/feature/home/EmptyStateFragment_;
    iget-object v0, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment_$FragmentBuilder_;->args:Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Lcom/itau/empresas/feature/home/EmptyStateFragment_;->setArguments(Landroid/os/Bundle;)V

    .line 106
    return-object v1
.end method

.method public descricao(Ljava/lang/String;)Lcom/itau/empresas/feature/home/EmptyStateFragment_$FragmentBuilder_;
    .registers 4
    .param p1, "descricao"    # Ljava/lang/String;

    .line 127
    iget-object v0, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment_$FragmentBuilder_;->args:Landroid/os/Bundle;

    const-string v1, "descricao"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    return-object p0
.end method

.method public icone(Ljava/lang/Integer;)Lcom/itau/empresas/feature/home/EmptyStateFragment_$FragmentBuilder_;
    .registers 4
    .param p1, "icone"    # Ljava/lang/Integer;

    .line 116
    iget-object v0, p0, Lcom/itau/empresas/feature/home/EmptyStateFragment_$FragmentBuilder_;->args:Landroid/os/Bundle;

    const-string v1, "icone"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 117
    return-object p0
.end method

.class public final Lcom/itau/empresas/feature/home/HomeFragment_;
.super Lcom/itau/empresas/feature/home/HomeFragment;
.source "HomeFragment_.java"

# interfaces
.implements Lorg/androidannotations/api/view/HasViews;
.implements Lorg/androidannotations/api/view/OnViewChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/itau/empresas/feature/home/HomeFragment_$FragmentBuilder_;
    }
.end annotation


# instance fields
.field private contentView_:Landroid/view/View;

.field private final onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 24
    invoke-direct {p0}, Lcom/itau/empresas/feature/home/HomeFragment;-><init>()V

    .line 28
    new-instance v0, Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-direct {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;-><init>()V

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    return-void
.end method

.method public static builder()Lcom/itau/empresas/feature/home/HomeFragment_$FragmentBuilder_;
    .registers 1

    .line 77
    new-instance v0, Lcom/itau/empresas/feature/home/HomeFragment_$FragmentBuilder_;

    invoke-direct {v0}, Lcom/itau/empresas/feature/home/HomeFragment_$FragmentBuilder_;-><init>()V

    return-object v0
.end method

.method private init_(Landroid/os/Bundle;)V
    .registers 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 64
    invoke-static {p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->registerOnViewChangedListener(Lorg/androidannotations/api/view/OnViewChangedListener;)V

    .line 65
    invoke-static {}, Lcom/itau/empresas/CustomApplication_;->getInstance()Lcom/itau/empresas/CustomApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment_;->application:Lcom/itau/empresas/CustomApplication;

    .line 66
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/HomeFragment_;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/pendencias/PendenciasController_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/pendencias/PendenciasController_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment_;->pendenciasController:Lcom/itau/empresas/feature/pendencias/PendenciasController;

    .line 67
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/HomeFragment_;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/feature/logout/LogoutHelper_;->getInstance_(Landroid/content/Context;)Lcom/itau/empresas/feature/logout/LogoutHelper_;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment_;->logoutHelper:Lcom/itau/empresas/feature/logout/LogoutHelper;

    .line 68
    return-void
.end method


# virtual methods
.method public findViewById(I)Landroid/view/View;
    .registers 3
    .param p1, "id"    # I

    .line 41
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_6

    .line 42
    const/4 v0, 0x0

    return-object v0

    .line 44
    :cond_6
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment_;->contentView_:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 33
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-static {v0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    move-result-object v1

    .line 34
    .local v1, "previousNotifier":Lorg/androidannotations/api/view/OnViewChangedNotifier;
    invoke-direct {p0, p1}, Lcom/itau/empresas/feature/home/HomeFragment_;->init_(Landroid/os/Bundle;)V

    .line 35
    invoke-super {p0, p1}, Lcom/itau/empresas/feature/home/HomeFragment;->onCreate(Landroid/os/Bundle;)V

    .line 36
    invoke-static {v1}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->replaceNotifier(Lorg/androidannotations/api/view/OnViewChangedNotifier;)Lorg/androidannotations/api/view/OnViewChangedNotifier;

    .line 37
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .registers 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .line 49
    invoke-super {p0, p1, p2, p3}, Lcom/itau/empresas/feature/home/HomeFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment_;->contentView_:Landroid/view/View;

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment_;->contentView_:Landroid/view/View;

    if-nez v0, :cond_14

    .line 51
    const v0, 0x7f0300a6

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment_;->contentView_:Landroid/view/View;

    .line 53
    :cond_14
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment_;->contentView_:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .registers 2

    .line 58
    invoke-super {p0}, Lcom/itau/empresas/feature/home/HomeFragment;->onDestroyView()V

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment_;->contentView_:Landroid/view/View;

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment_;->rvHome:Landroid/support/v7/widget/RecyclerView;

    .line 61
    return-void
.end method

.method public onViewChanged(Lorg/androidannotations/api/view/HasViews;)V
    .registers 3
    .param p1, "hasViews"    # Lorg/androidannotations/api/view/HasViews;

    .line 82
    const v0, 0x7f0e0421

    invoke-interface {p1, v0}, Lorg/androidannotations/api/view/HasViews;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment_;->rvHome:Landroid/support/v7/widget/RecyclerView;

    .line 83
    invoke-virtual {p0}, Lcom/itau/empresas/feature/home/HomeFragment_;->afterViews()V

    .line 84
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .registers 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .line 72
    invoke-super {p0, p1, p2}, Lcom/itau/empresas/feature/home/HomeFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 73
    iget-object v0, p0, Lcom/itau/empresas/feature/home/HomeFragment_;->onViewChangedNotifier_:Lorg/androidannotations/api/view/OnViewChangedNotifier;

    invoke-virtual {v0, p0}, Lorg/androidannotations/api/view/OnViewChangedNotifier;->notifyViewChanged(Lorg/androidannotations/api/view/HasViews;)V

    .line 74
    return-void
.end method

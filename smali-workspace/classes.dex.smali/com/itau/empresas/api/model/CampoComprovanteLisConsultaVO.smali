.class public final Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;
.super Ljava/lang/Object;
.source "CampoComprovanteLisConsultaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final cetAno:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cet_ano"
    .end annotation
.end field

.field private final cetMes:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cet_mes"
    .end annotation
.end field

.field private final cnpj:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cnpj"
    .end annotation
.end field

.field private final dadosAssinaturaEletronicaCpf:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dados_assinatura_eletronica_cpf"
    .end annotation
.end field

.field private final dadosAssinaturaEletronicaData:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dados_assinatura_eletronica_data"
    .end annotation
.end field

.field private final dadosAssinaturaEletronicaNome:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dados_assinatura_eletronica_nome"
    .end annotation
.end field

.field private final dadosDevedorSolidarioCpf:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dados_devedor_solidario_cpf"
    .end annotation
.end field

.field private final dadosDevedorSolidarioData:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dados_devedor_solidario_data"
    .end annotation
.end field

.field private final dadosDevedorSolidarioNome:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dados_devedor_solidario_nome"
    .end annotation
.end field

.field private final diaPagamentoEncargos:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dia_pagamento_encargos"
    .end annotation
.end field

.field private final periodicidadeCapitalizacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "periodicidade_capitalizacao"
    .end annotation
.end field

.field private final produto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "produto"
    .end annotation
.end field

.field private final taxaJurosAno:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros_ano"
    .end annotation
.end field

.field private final taxaJurosMes:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros_mes"
    .end annotation
.end field

.field private final textoValorLimiteAdicional:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "texto_valor_limite_adicional"
    .end annotation
.end field

.field private final valorIof:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_iof"
    .end annotation
.end field

.field private final valorIofPercentual:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_iof_percentual"
    .end annotation
.end field

.field private final valorLimite:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_limite"
    .end annotation
.end field

.field private final valorLimiteAdicional:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_limite_adicional"
    .end annotation
.end field

.field private final valorLimiteAval:Ljava/lang/Float;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_limite_aval"
    .end annotation
.end field

.field private final valorLimiteAvalPorcentagem:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_limite_aval_porcentagem"
    .end annotation
.end field

.field private final valorLimitePorcentagem:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_limite_porcentagem"
    .end annotation
.end field

.field private final valorTarifaContratacao:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_tarifa_contratacao"
    .end annotation
.end field

.field private final valorTarifaContratacaoPercentual:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_tarifa_contratacao_percentual"
    .end annotation
.end field

.field private final valorTotalOperacaoPercentual:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_total_operacao_percentual"
    .end annotation
.end field


# virtual methods
.method public getCetAno()F
    .registers 2

    .line 192
    iget v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->cetAno:F

    return v0
.end method

.method public getCetMes()F
    .registers 2

    .line 188
    iget v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->cetMes:F

    return v0
.end method

.method public getCnpj()Ljava/lang/String;
    .registers 2

    .line 104
    iget-object v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->cnpj:Ljava/lang/String;

    return-object v0
.end method

.method public getDadosAssinaturaEletronicaCpf()Ljava/lang/String;
    .registers 2

    .line 112
    iget-object v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->dadosAssinaturaEletronicaCpf:Ljava/lang/String;

    return-object v0
.end method

.method public getDadosAssinaturaEletronicaData()Ljava/lang/String;
    .registers 2

    .line 116
    iget-object v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->dadosAssinaturaEletronicaData:Ljava/lang/String;

    return-object v0
.end method

.method public getDadosAssinaturaEletronicaNome()Ljava/lang/String;
    .registers 2

    .line 108
    iget-object v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->dadosAssinaturaEletronicaNome:Ljava/lang/String;

    return-object v0
.end method

.method public getDadosDevedorSolidarioCpf()Ljava/lang/String;
    .registers 2

    .line 124
    iget-object v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->dadosDevedorSolidarioCpf:Ljava/lang/String;

    return-object v0
.end method

.method public getDadosDevedorSolidarioData()Ljava/lang/String;
    .registers 2

    .line 128
    iget-object v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->dadosDevedorSolidarioData:Ljava/lang/String;

    return-object v0
.end method

.method public getDadosDevedorSolidarioNome()Ljava/lang/String;
    .registers 2

    .line 120
    iget-object v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->dadosDevedorSolidarioNome:Ljava/lang/String;

    return-object v0
.end method

.method public getDiaPagamentoEncargos()Ljava/lang/String;
    .registers 2

    .line 164
    iget-object v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->diaPagamentoEncargos:Ljava/lang/String;

    return-object v0
.end method

.method public getPeriodicidadeCapitalizacao()Ljava/lang/String;
    .registers 2

    .line 196
    iget-object v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->periodicidadeCapitalizacao:Ljava/lang/String;

    return-object v0
.end method

.method public getProduto()Ljava/lang/String;
    .registers 2

    .line 152
    iget-object v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->produto:Ljava/lang/String;

    return-object v0
.end method

.method public getTaxaJurosAno()F
    .registers 2

    .line 136
    iget v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->taxaJurosAno:F

    return v0
.end method

.method public getTaxaJurosMes()F
    .registers 2

    .line 184
    iget v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->taxaJurosMes:F

    return v0
.end method

.method public getValorIof()F
    .registers 2

    .line 176
    iget v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->valorIof:F

    return v0
.end method

.method public getValorIofPercentual()F
    .registers 2

    .line 180
    iget v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->valorIofPercentual:F

    return v0
.end method

.method public getValorLimite()F
    .registers 2

    .line 156
    iget v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->valorLimite:F

    return v0
.end method

.method public getValorLimiteAdicional()Ljava/lang/String;
    .registers 2

    .line 140
    iget-object v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->valorLimiteAdicional:Ljava/lang/String;

    return-object v0
.end method

.method public getValorLimiteAval()F
    .registers 2

    .line 144
    iget-object v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->valorLimiteAval:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method public getValorLimiteAvalPorcentagem()F
    .registers 2

    .line 148
    iget v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->valorLimiteAvalPorcentagem:F

    return v0
.end method

.method public getValorLimitePorcentagem()F
    .registers 2

    .line 160
    iget v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->valorLimitePorcentagem:F

    return v0
.end method

.method public getValorTarifaContratacao()F
    .registers 2

    .line 168
    iget v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->valorTarifaContratacao:F

    return v0
.end method

.method public getValorTarifaContratacaoPercentual()F
    .registers 2

    .line 172
    iget v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->valorTarifaContratacaoPercentual:F

    return v0
.end method

.method public getValorTotalOperacaoPercentual()F
    .registers 2

    .line 132
    iget v0, p0, Lcom/itau/empresas/api/model/CampoComprovanteLisConsultaVO;->valorTotalOperacaoPercentual:F

    return v0
.end method

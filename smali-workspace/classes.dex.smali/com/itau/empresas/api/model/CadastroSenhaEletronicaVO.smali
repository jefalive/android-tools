.class public Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;
.super Ljava/lang/Object;
.source "CadastroSenhaEletronicaVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field cnpj:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cnpj"
    .end annotation
.end field

.field novaSenha:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nova_senha"
    .end annotation
.end field

.field novaSenhaRepetida:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nova_senha_repetida"
    .end annotation
.end field

.field senhaExpirada:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "senha_expirada"
    .end annotation
.end field

.field tipoAcesso:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_acesso"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setCnpj(Ljava/lang/String;)V
    .registers 2
    .param p1, "cnpj"    # Ljava/lang/String;

    .line 38
    iput-object p1, p0, Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;->cnpj:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public setNovaSenha(Ljava/lang/String;)V
    .registers 2
    .param p1, "novaSenha"    # Ljava/lang/String;

    .line 26
    iput-object p1, p0, Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;->novaSenha:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public setNovaSenhaRepetida(Ljava/lang/String;)V
    .registers 2
    .param p1, "novaSenhaRepetida"    # Ljava/lang/String;

    .line 30
    iput-object p1, p0, Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;->novaSenhaRepetida:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public setSenhaExpirada(Ljava/lang/String;)V
    .registers 2
    .param p1, "senhaExpirada"    # Ljava/lang/String;

    .line 22
    iput-object p1, p0, Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;->senhaExpirada:Ljava/lang/String;

    .line 23
    return-void
.end method

.method public setTipoAcesso(Ljava/lang/String;)V
    .registers 2
    .param p1, "tipoAcesso"    # Ljava/lang/String;

    .line 42
    iput-object p1, p0, Lcom/itau/empresas/api/model/CadastroSenhaEletronicaVO;->tipoAcesso:Ljava/lang/String;

    .line 43
    return-void
.end method

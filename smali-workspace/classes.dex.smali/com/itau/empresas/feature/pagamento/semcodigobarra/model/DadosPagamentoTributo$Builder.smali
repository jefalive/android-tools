.class public final Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;
.super Ljava/lang/Object;
.source "DadosPagamentoTributo.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

.field private inclusaoTributoResponseVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

.field private mensagem:Ljava/lang/String;

.field private tipoPagamento:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;
    .registers 4

    .line 128
    const/4 v2, 0x0

    .line 129
    .local v2, "dadosPagamentoTributo":Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    if-eqz v0, :cond_3d

    .line 130
    new-instance v2, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    const/4 v1, 0x0

    invoke-direct {v2, v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$1;)V

    .line 131
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->mensagem:Ljava/lang/String;

    # setter for: Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->mensagem:Ljava/lang/String;
    invoke-static {v2, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->access$102(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;Ljava/lang/String;)Ljava/lang/String;

    .line 132
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    # setter for: Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;
    invoke-static {v2, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->access$202(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    .line 133
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->isIndicadorComprovante()Z

    move-result v0

    # setter for: Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->isMostraComprovantes:Z
    invoke-static {v2, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->access$302(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;Z)Z

    .line 134
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->isIndicadorComprovante()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    # setter for: Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->isMostraDetalhesAutorizacao:Z
    invoke-static {v2, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->access$402(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;Z)Z

    .line 135
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;->getDescricaoPagamento()Ljava/lang/String;

    move-result-object v0

    # setter for: Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->tipoPagamento:Ljava/lang/String;
    invoke-static {v2, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->access$502(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_60

    .line 137
    :cond_3d
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->inclusaoTributoResponseVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    if-eqz v0, :cond_60

    .line 138
    new-instance v2, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;

    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->inclusaoTributoResponseVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    const/4 v1, 0x0

    invoke-direct {v2, v0, v1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;-><init>(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$1;)V

    .line 139
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->mensagem:Ljava/lang/String;

    # setter for: Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->mensagem:Ljava/lang/String;
    invoke-static {v2, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->access$102(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;Ljava/lang/String;)Ljava/lang/String;

    .line 140
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->inclusaoTributoResponseVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    # setter for: Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->inclusaoTributoResponseVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;
    invoke-static {v2, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->access$702(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    .line 141
    const/4 v0, 0x0

    # setter for: Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->isMostraDetalhesAutorizacao:Z
    invoke-static {v2, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->access$402(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;Z)Z

    .line 142
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->inclusaoTributoResponseVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    invoke-virtual {v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;->getTipoPagamento()Ljava/lang/String;

    move-result-object v0

    # setter for: Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->tipoPagamento:Ljava/lang/String;
    invoke-static {v2, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->access$502(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;Ljava/lang/String;)Ljava/lang/String;

    .line 145
    :cond_60
    :goto_60
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->tipoPagamento:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6f

    if-eqz v2, :cond_6f

    .line 146
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->tipoPagamento:Ljava/lang/String;

    # setter for: Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->tipoPagamento:Ljava/lang/String;
    invoke-static {v2, v0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;->access$502(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo;Ljava/lang/String;)Ljava/lang/String;

    .line 149
    :cond_6f
    return-object v2
.end method

.method public comDetelhesAutorizacao(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;
    .registers 2
    .param p1, "detalhesAutorizacaoVO"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    .line 107
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->detalhesAutorizacaoVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DetalhesAutorizacaoVO;

    .line 108
    return-object p0
.end method

.method public comInclusaoTributos(Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;
    .registers 2
    .param p1, "inclusaoTributoResponseVO"    # Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    .line 113
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->inclusaoTributoResponseVO:Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    .line 114
    return-object p0
.end method

.method public comMensagem(Ljava/lang/String;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;
    .registers 2
    .param p1, "mensagem"    # Ljava/lang/String;

    .line 118
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->mensagem:Ljava/lang/String;

    .line 119
    return-object p0
.end method

.method public comTipoPagamento(Ljava/lang/String;)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;
    .registers 2
    .param p1, "tipoPagamento"    # Ljava/lang/String;

    .line 123
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/DadosPagamentoTributo$Builder;->tipoPagamento:Ljava/lang/String;

    .line 124
    return-object p0
.end method

.class public Lcom/itau/empresas/feature/pendencias/PendenciasController;
.super Lcom/itau/empresas/controller/BaseController;
.source "PendenciasController.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/itau/empresas/controller/BaseController<Lcom/itau/empresas/api/Api;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 12
    invoke-direct {p0}, Lcom/itau/empresas/controller/BaseController;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/itau/empresas/feature/pendencias/PendenciasController;)Ljava/lang/Object;
    .registers 2
    .param p0, "x0"    # Lcom/itau/empresas/feature/pendencias/PendenciasController;

    .line 12
    invoke-virtual {p0}, Lcom/itau/empresas/feature/pendencias/PendenciasController;->api()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Ljava/lang/Object;)V
    .registers 1
    .param p0, "x0"    # Ljava/lang/Object;

    .line 12
    invoke-static {p0}, Lcom/itau/empresas/feature/pendencias/PendenciasController;->post(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public buscaPendencias(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .param p1, "idConta"    # Ljava/lang/String;
    .param p2, "operador"    # Ljava/lang/String;
    .param p3, "dataInicio"    # Ljava/lang/String;
    .param p4, "dataFinal"    # Ljava/lang/String;

    .line 18
    new-instance v0, Lcom/itau/empresas/feature/pendencias/PendenciasController$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/itau/empresas/feature/pendencias/PendenciasController$1;-><init>(Lcom/itau/empresas/feature/pendencias/PendenciasController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lbr/com/itau/sdk/android/core/BackendClient;->callApi(Lbr/com/itau/sdk/android/core/type/BackendCall;)V

    .line 29
    return-void
.end method

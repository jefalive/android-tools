.class public interface abstract annotation Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareUniversal;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/AnnotationDefault;
    value = .subannotation Lbr/com/itau/sdk/android/core/endpoint/legacy/MiddlewareUniversal;
        connectTimeout = -0x1
        opKey = ""
        readTimeout = -0x1
    .end subannotation
.end annotation

.annotation runtime Ljava/lang/annotation/Documented;
.end annotation

.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->RUNTIME:Ljava/lang/annotation/RetentionPolicy;
.end annotation

.annotation runtime Ljava/lang/annotation/Target;
    value = {
        .enum Ljava/lang/annotation/ElementType;->METHOD:Ljava/lang/annotation/ElementType;
    }
.end annotation


# virtual methods
.method public abstract connectTimeout()I
.end method

.method public abstract opKey()Ljava/lang/String;
.end method

.method public abstract readTimeout()I
.end method

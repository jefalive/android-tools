.class Lcom/facebook/GraphRequest$4;
.super Ljava/lang/Object;
.source "GraphRequest.java"

# interfaces
.implements Lcom/facebook/GraphRequest$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/facebook/GraphRequest;->setCallback(Lcom/facebook/GraphRequest$Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/facebook/GraphRequest;

.field final synthetic val$callback:Lcom/facebook/GraphRequest$Callback;


# direct methods
.method constructor <init>(Lcom/facebook/GraphRequest;Lcom/facebook/GraphRequest$Callback;)V
    .registers 3

    .line 910
    iput-object p1, p0, Lcom/facebook/GraphRequest$4;->this$0:Lcom/facebook/GraphRequest;

    iput-object p2, p0, Lcom/facebook/GraphRequest$4;->val$callback:Lcom/facebook/GraphRequest$Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Lcom/facebook/GraphResponse;)V
    .registers 13
    .param p1, "response"    # Lcom/facebook/GraphResponse;

    .line 913
    invoke-virtual {p1}, Lcom/facebook/GraphResponse;->getJSONObject()Lorg/json/JSONObject;

    move-result-object v2

    .line 914
    .local v2, "responseObject":Lorg/json/JSONObject;
    if-eqz v2, :cond_d

    const-string v0, "__debug__"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    goto :goto_e

    :cond_d
    const/4 v3, 0x0

    .line 916
    .local v3, "debug":Lorg/json/JSONObject;
    :goto_e
    if-eqz v3, :cond_17

    const-string v0, "messages"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    goto :goto_18

    :cond_17
    const/4 v4, 0x0

    .line 918
    .local v4, "debugMessages":Lorg/json/JSONArray;
    :goto_18
    if-eqz v4, :cond_79

    .line 919
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1b
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v5, v0, :cond_79

    .line 920
    invoke-virtual {v4, v5}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 921
    .local v6, "debugMessageObject":Lorg/json/JSONObject;
    if-eqz v6, :cond_2e

    const-string v0, "message"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_2f

    :cond_2e
    const/4 v7, 0x0

    .line 924
    .local v7, "debugMessage":Ljava/lang/String;
    :goto_2f
    if-eqz v6, :cond_38

    const-string v0, "type"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto :goto_39

    :cond_38
    const/4 v8, 0x0

    .line 927
    .local v8, "debugMessageType":Ljava/lang/String;
    :goto_39
    if-eqz v6, :cond_42

    const-string v0, "link"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    goto :goto_43

    :cond_42
    const/4 v9, 0x0

    .line 930
    .local v9, "debugMessageLink":Ljava/lang/String;
    :goto_43
    if-eqz v7, :cond_75

    if-eqz v8, :cond_75

    .line 931
    sget-object v10, Lcom/facebook/LoggingBehavior;->GRAPH_API_DEBUG_INFO:Lcom/facebook/LoggingBehavior;

    .line 932
    .local v10, "behavior":Lcom/facebook/LoggingBehavior;
    const-string v0, "warning"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_53

    .line 933
    sget-object v10, Lcom/facebook/LoggingBehavior;->GRAPH_API_DEBUG_WARNING:Lcom/facebook/LoggingBehavior;

    .line 935
    :cond_53
    invoke-static {v9}, Lcom/facebook/internal/Utility;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_70

    .line 936
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Link: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 938
    :cond_70
    sget-object v0, Lcom/facebook/GraphRequest;->TAG:Ljava/lang/String;

    invoke-static {v10, v0, v7}, Lcom/facebook/internal/Logger;->log(Lcom/facebook/LoggingBehavior;Ljava/lang/String;Ljava/lang/String;)V

    .line 919
    .end local v6    # "debugMessageObject":Lorg/json/JSONObject;
    .end local v7    # "debugMessage":Ljava/lang/String;
    .end local v8    # "debugMessageType":Ljava/lang/String;
    .end local v9    # "debugMessageLink":Ljava/lang/String;
    .end local v10    # "behavior":Lcom/facebook/LoggingBehavior;
    :cond_75
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1b

    .line 942
    .end local v5    # "i":I
    :cond_79
    iget-object v0, p0, Lcom/facebook/GraphRequest$4;->val$callback:Lcom/facebook/GraphRequest$Callback;

    if-eqz v0, :cond_82

    .line 943
    iget-object v0, p0, Lcom/facebook/GraphRequest$4;->val$callback:Lcom/facebook/GraphRequest$Callback;

    invoke-interface {v0, p1}, Lcom/facebook/GraphRequest$Callback;->onCompleted(Lcom/facebook/GraphResponse;)V

    .line 945
    :cond_82
    return-void
.end method

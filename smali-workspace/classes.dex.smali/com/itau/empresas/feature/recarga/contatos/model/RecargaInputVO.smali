.class public Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;
.super Ljava/lang/Object;
.source "RecargaInputVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field cliente:Lcom/itau/empresas/api/model/DadosClienteVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "cliente"
    .end annotation
.end field

.field pessoaJuridica:Lcom/itau/empresas/api/model/DadosPJVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "pessoa_juridica"
    .end annotation
.end field

.field recarga:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "recarga"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getPessoaJuridica()Lcom/itau/empresas/api/model/DadosPJVO;
    .registers 2

    .line 27
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->pessoaJuridica:Lcom/itau/empresas/api/model/DadosPJVO;

    return-object v0
.end method

.method public getRecarga()Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;
    .registers 2

    .line 35
    iget-object v0, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->recarga:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;

    return-object v0
.end method

.method public setCliente(Lcom/itau/empresas/api/model/DadosClienteVO;)V
    .registers 2
    .param p1, "cliente"    # Lcom/itau/empresas/api/model/DadosClienteVO;

    .line 23
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->cliente:Lcom/itau/empresas/api/model/DadosClienteVO;

    .line 24
    return-void
.end method

.method public setPessoaJuridica(Lcom/itau/empresas/api/model/DadosPJVO;)V
    .registers 2
    .param p1, "pessoaJuridica"    # Lcom/itau/empresas/api/model/DadosPJVO;

    .line 31
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->pessoaJuridica:Lcom/itau/empresas/api/model/DadosPJVO;

    .line 32
    return-void
.end method

.method public setRecarga(Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;)V
    .registers 2
    .param p1, "recarga"    # Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;

    .line 39
    iput-object p1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->recarga:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;

    .line 40
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RecargaInputVO{cliente="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->cliente:Lcom/itau/empresas/api/model/DadosClienteVO;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", recarga="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->recarga:Lcom/itau/empresas/feature/recarga/contatos/model/RecargaVO;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", pessoaJuridica="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/itau/empresas/feature/recarga/contatos/model/RecargaInputVO;->pessoaJuridica:Lcom/itau/empresas/api/model/DadosPJVO;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

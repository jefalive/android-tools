.class Lcom/itau/empresas/feature/login/controller/LoginController$2;
.super Ljava/lang/Object;
.source "LoginController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/feature/login/controller/LoginController;->autenticar(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/feature/login/controller/LoginController;

.field final synthetic val$idTeclado:Ljava/lang/String;

.field final synthetic val$operador:Ljava/lang/String;

.field final synthetic val$senha:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/itau/empresas/feature/login/controller/LoginController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .param p1, "this$0"    # Lcom/itau/empresas/feature/login/controller/LoginController;

    .line 46
    iput-object p1, p0, Lcom/itau/empresas/feature/login/controller/LoginController$2;->this$0:Lcom/itau/empresas/feature/login/controller/LoginController;

    iput-object p2, p0, Lcom/itau/empresas/feature/login/controller/LoginController$2;->val$operador:Ljava/lang/String;

    iput-object p3, p0, Lcom/itau/empresas/feature/login/controller/LoginController$2;->val$senha:Ljava/lang/String;

    iput-object p4, p0, Lcom/itau/empresas/feature/login/controller/LoginController$2;->val$idTeclado:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 7

    .line 49
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/LoginController$2;->this$0:Lcom/itau/empresas/feature/login/controller/LoginController;

    iget-object v0, v0, Lcom/itau/empresas/feature/login/controller/LoginController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    const-string v1, "logon"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->inicioMonitorarTempo(Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/LoginController$2;->this$0:Lcom/itau/empresas/feature/login/controller/LoginController;

    # invokes: Lcom/itau/empresas/feature/login/controller/LoginController;->api()Ljava/lang/Object;
    invoke-static {v0}, Lcom/itau/empresas/feature/login/controller/LoginController;->access$300(Lcom/itau/empresas/feature/login/controller/LoginController;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/login/LoginApi;

    iget-object v1, p0, Lcom/itau/empresas/feature/login/controller/LoginController$2;->this$0:Lcom/itau/empresas/feature/login/controller/LoginController;

    iget-object v2, p0, Lcom/itau/empresas/feature/login/controller/LoginController$2;->val$operador:Ljava/lang/String;

    iget-object v3, p0, Lcom/itau/empresas/feature/login/controller/LoginController$2;->val$senha:Ljava/lang/String;

    iget-object v4, p0, Lcom/itau/empresas/feature/login/controller/LoginController$2;->val$idTeclado:Ljava/lang/String;

    .line 51
    # invokes: Lcom/itau/empresas/feature/login/controller/LoginController;->getInputVO(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/api/model/LoginInputVO;
    invoke-static {v1, v2, v3, v4}, Lcom/itau/empresas/feature/login/controller/LoginController;->access$200(Lcom/itau/empresas/feature/login/controller/LoginController;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/itau/empresas/api/model/LoginInputVO;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/itau/empresas/feature/login/LoginApi;->login(Lcom/itau/empresas/api/model/LoginInputVO;)Lcom/itau/empresas/api/model/PerfilOperadorVO;

    move-result-object v5

    .line 52
    .local v5, "perfilOperadorVO":Lcom/itau/empresas/api/model/PerfilOperadorVO;
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/LoginController$2;->this$0:Lcom/itau/empresas/feature/login/controller/LoginController;

    iget-object v0, v0, Lcom/itau/empresas/feature/login/controller/LoginController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0}, Lcom/itau/empresas/CustomApplication;->getEventoTracker()Lcom/itau/empresas/ui/util/analytics/TrackerEvento;

    move-result-object v0

    const-string v1, "logon"

    invoke-virtual {v0, v1}, Lcom/itau/empresas/ui/util/analytics/TrackerEvento;->fimMonitorarTempo(Ljava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/LoginController$2;->this$0:Lcom/itau/empresas/feature/login/controller/LoginController;

    iget-object v0, v0, Lcom/itau/empresas/feature/login/controller/LoginController;->app:Lcom/itau/empresas/CustomApplication;

    invoke-virtual {v0, v5}, Lcom/itau/empresas/CustomApplication;->setPerfilLogado(Lcom/itau/empresas/api/model/PerfilOperadorVO;)V

    .line 56
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/LoginController$2;->this$0:Lcom/itau/empresas/feature/login/controller/LoginController;

    # invokes: Lcom/itau/empresas/feature/login/controller/LoginController;->verificaTrocaSenha(Lcom/itau/empresas/api/model/PerfilOperadorVO;)Z
    invoke-static {v0, v5}, Lcom/itau/empresas/feature/login/controller/LoginController;->access$400(Lcom/itau/empresas/feature/login/controller/LoginController;Lcom/itau/empresas/api/model/PerfilOperadorVO;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 57
    return-void

    .line 59
    :cond_42
    iget-object v0, p0, Lcom/itau/empresas/feature/login/controller/LoginController$2;->this$0:Lcom/itau/empresas/feature/login/controller/LoginController;

    # invokes: Lcom/itau/empresas/feature/login/controller/LoginController;->criaSessao()V
    invoke-static {v0}, Lcom/itau/empresas/feature/login/controller/LoginController;->access$500(Lcom/itau/empresas/feature/login/controller/LoginController;)V

    .line 60
    return-void
.end method

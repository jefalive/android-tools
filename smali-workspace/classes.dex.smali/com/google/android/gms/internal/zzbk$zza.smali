.class public Lcom/google/android/gms/internal/zzbk$zza;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/zzbk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "zza"
.end annotation


# instance fields
.field final value:J

.field final zztx:Ljava/lang/String;

.field final zzty:I


# direct methods
.method constructor <init>(JLjava/lang/String;I)V
    .registers 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/gms/internal/zzbk$zza;->value:J

    iput-object p3, p0, Lcom/google/android/gms/internal/zzbk$zza;->zztx:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/gms/internal/zzbk$zza;->zzty:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 6
    .param p1, "other"    # Ljava/lang/Object;

    if-eqz p1, :cond_6

    instance-of v0, p1, Lcom/google/android/gms/internal/zzbk$zza;

    if-nez v0, :cond_8

    :cond_6
    const/4 v0, 0x0

    return v0

    :cond_8
    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/internal/zzbk$zza;

    iget-wide v0, v0, Lcom/google/android/gms/internal/zzbk$zza;->value:J

    iget-wide v2, p0, Lcom/google/android/gms/internal/zzbk$zza;->value:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1e

    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/internal/zzbk$zza;

    iget v0, v0, Lcom/google/android/gms/internal/zzbk$zza;->zzty:I

    iget v1, p0, Lcom/google/android/gms/internal/zzbk$zza;->zzty:I

    if-ne v0, v1, :cond_1e

    const/4 v0, 0x1

    goto :goto_1f

    :cond_1e
    const/4 v0, 0x0

    :goto_1f
    return v0
.end method

.method public hashCode()I
    .registers 3

    iget-wide v0, p0, Lcom/google/android/gms/internal/zzbk$zza;->value:J

    long-to-int v0, v0

    return v0
.end method

.class public Lcom/itau/empresas/ui/util/analytics/FacebookObserver;
.super Ljava/lang/Object;
.source "FacebookObserver.java"

# interfaces
.implements Ljava/util/Observer;


# instance fields
.field private logger:Lcom/facebook/appevents/AppEventsLogger;


# direct methods
.method public constructor <init>(Lcom/facebook/appevents/AppEventsLogger;)V
    .registers 2
    .param p1, "logger"    # Lcom/facebook/appevents/AppEventsLogger;

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/itau/empresas/ui/util/analytics/FacebookObserver;->logger:Lcom/facebook/appevents/AppEventsLogger;

    .line 19
    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .registers 8
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "o"    # Ljava/lang/Object;

    .line 23
    instance-of v0, p2, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    if-eqz v0, :cond_41

    .line 24
    move-object v3, p2

    check-cast v3, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;

    .line 25
    .local v3, "e":Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 26
    .local v4, "parameters":Landroid/os/Bundle;
    const-string v0, "categoriaEvento"

    invoke-virtual {v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->getCategoria()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    const-string v0, "acaoEvento"

    invoke-virtual {v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->getAcao()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    const-string v0, "marcadorEvento"

    invoke-virtual {v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    invoke-virtual {v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->getValor()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_3a

    .line 30
    const-string v0, "valorEvento"

    invoke-virtual {v3}, Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;->getValor()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v4, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 33
    :cond_3a
    iget-object v0, p0, Lcom/itau/empresas/ui/util/analytics/FacebookObserver;->logger:Lcom/facebook/appevents/AppEventsLogger;

    const-string v1, "evento"

    invoke-virtual {v0, v1, v4}, Lcom/facebook/appevents/AppEventsLogger;->logEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 35
    .end local v3    # "e":Lcom/itau/empresas/ui/util/analytics/EventoAnalytics;
    .end local v4    # "parameters":Landroid/os/Bundle;
    :cond_41
    return-void
.end method

.class Lbr/com/itau/widgets/material/MaterialSpinner$2;
.super Ljava/lang/Object;
.source "MaterialSpinner.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbr/com/itau/widgets/material/MaterialSpinner;->hideKeyboardOnGainFocus(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lbr/com/itau/widgets/material/MaterialSpinner;Landroid/app/Activity;)V
    .registers 3

    .line 758
    iput-object p1, p0, Lbr/com/itau/widgets/material/MaterialSpinner$2;->this$0:Lbr/com/itau/widgets/material/MaterialSpinner;

    iput-object p2, p0, Lbr/com/itau/widgets/material/MaterialSpinner$2;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .registers 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .line 761
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$2;->val$activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_21

    .line 762
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$2;->val$activity:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    .line 763
    .local v2, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    iget-object v0, p0, Lbr/com/itau/widgets/material/MaterialSpinner$2;->val$activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 765
    .end local v2    # "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    :cond_21
    const/4 v0, 0x0

    return v0
.end method

.class public Lcom/itau/empresas/api/model/DataPrimeiroPagamentoVO;
.super Ljava/lang/Object;
.source "DataPrimeiroPagamentoVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private datasPrimeiroPagamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "datas_primeiro_pagamento"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDatasPrimeiroPagamento()Ljava/lang/String;
    .registers 2

    .line 14
    iget-object v0, p0, Lcom/itau/empresas/api/model/DataPrimeiroPagamentoVO;->datasPrimeiroPagamento:Ljava/lang/String;

    return-object v0
.end method

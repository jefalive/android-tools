.class Lcom/google/ads/conversiontracking/GoogleConversionReporter$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/ads/conversiontracking/GoogleConversionReporter;->a(Landroid/content/Context;Lcom/google/ads/conversiontracking/g$c;ZZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/google/ads/conversiontracking/g$c;

.field final synthetic c:Z

.field final synthetic d:Z

.field final synthetic e:Z

.field final synthetic f:Lcom/google/ads/conversiontracking/GoogleConversionReporter;


# direct methods
.method constructor <init>(Lcom/google/ads/conversiontracking/GoogleConversionReporter;Landroid/content/Context;Lcom/google/ads/conversiontracking/g$c;ZZZ)V
    .registers 7

    .line 24
    iput-object p1, p0, Lcom/google/ads/conversiontracking/GoogleConversionReporter$1;->f:Lcom/google/ads/conversiontracking/GoogleConversionReporter;

    iput-object p2, p0, Lcom/google/ads/conversiontracking/GoogleConversionReporter$1;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/ads/conversiontracking/GoogleConversionReporter$1;->b:Lcom/google/ads/conversiontracking/g$c;

    iput-boolean p4, p0, Lcom/google/ads/conversiontracking/GoogleConversionReporter$1;->c:Z

    iput-boolean p5, p0, Lcom/google/ads/conversiontracking/GoogleConversionReporter$1;->d:Z

    iput-boolean p6, p0, Lcom/google/ads/conversiontracking/GoogleConversionReporter$1;->e:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 9

    .line 28
    :try_start_0
    iget-object v0, p0, Lcom/google/ads/conversiontracking/GoogleConversionReporter$1;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/ads/conversiontracking/GoogleConversionReporter$1;->b:Lcom/google/ads/conversiontracking/g$c;

    invoke-static {v0, v1}, Lcom/google/ads/conversiontracking/g;->a(Landroid/content/Context;Lcom/google/ads/conversiontracking/g$c;)Ljava/lang/String;

    move-result-object v6

    .line 32
    if-eqz v6, :cond_1d

    .line 33
    iget-object v0, p0, Lcom/google/ads/conversiontracking/GoogleConversionReporter$1;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/ads/conversiontracking/g;->a(Landroid/content/Context;)Lcom/google/ads/conversiontracking/e;

    move-result-object v7

    .line 34
    move-object v0, v7

    move-object v1, v6

    iget-object v2, p0, Lcom/google/ads/conversiontracking/GoogleConversionReporter$1;->b:Lcom/google/ads/conversiontracking/g$c;

    iget-boolean v3, p0, Lcom/google/ads/conversiontracking/GoogleConversionReporter$1;->c:Z

    iget-boolean v4, p0, Lcom/google/ads/conversiontracking/GoogleConversionReporter$1;->d:Z

    iget-boolean v5, p0, Lcom/google/ads/conversiontracking/GoogleConversionReporter$1;->e:Z

    invoke-virtual/range {v0 .. v5}, Lcom/google/ads/conversiontracking/e;->a(Ljava/lang/String;Lcom/google/ads/conversiontracking/g$c;ZZZ)V
    :try_end_1d
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_1d} :catch_1e

    .line 38
    :cond_1d
    goto :goto_26

    .line 36
    :catch_1e
    move-exception v6

    .line 37
    const-string v0, "GoogleConversionReporter"

    const-string v1, "Error sending ping"

    invoke-static {v0, v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 39
    :goto_26
    return-void
.end method

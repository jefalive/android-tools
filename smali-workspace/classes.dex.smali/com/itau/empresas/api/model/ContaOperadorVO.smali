.class public Lcom/itau/empresas/api/model/ContaOperadorVO;
.super Ljava/lang/Object;
.source "ContaOperadorVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private agencia:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "agencia"
    .end annotation
.end field

.field private apelido:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "apelido"
    .end annotation
.end field

.field private conta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta"
    .end annotation
.end field

.field private contaDefault:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta_default"
    .end annotation
.end field

.field private contaMigrada:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_conta_migrada"
    .end annotation
.end field

.field private contaRelacionamento:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "conta_relacionamento"
    .end annotation
.end field

.field private dac:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "dac"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    .line 25
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/itau/empresas/api/model/ContaOperadorVO;->contaMigrada:Ljava/lang/Boolean;

    .line 24
    return-void
.end method


# virtual methods
.method public getAgencia()Ljava/lang/String;
    .registers 2

    .line 28
    iget-object v0, p0, Lcom/itau/empresas/api/model/ContaOperadorVO;->agencia:Ljava/lang/String;

    return-object v0
.end method

.method public getApelido()Ljava/lang/String;
    .registers 2

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/api/model/ContaOperadorVO;->apelido:Ljava/lang/String;

    return-object v0
.end method

.method public getConta()Ljava/lang/String;
    .registers 2

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/api/model/ContaOperadorVO;->conta:Ljava/lang/String;

    return-object v0
.end method

.method public getContaDefault()Ljava/lang/Boolean;
    .registers 2

    .line 60
    iget-object v0, p0, Lcom/itau/empresas/api/model/ContaOperadorVO;->contaDefault:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/itau/empresas/api/model/ContaOperadorVO;->contaDefault:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_c

    :cond_b
    const/4 v0, 0x0

    :goto_c
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public getDac()Ljava/lang/String;
    .registers 2

    .line 44
    iget-object v0, p0, Lcom/itau/empresas/api/model/ContaOperadorVO;->dac:Ljava/lang/String;

    return-object v0
.end method

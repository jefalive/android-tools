.class public abstract Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;
.super Ljava/lang/Object;
.source "SwipeItemMangerImpl.java"

# interfaces
.implements Lcom/daimajia/swipe/interfaces/SwipeItemMangerInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/daimajia/swipe/implments/SwipeItemMangerImpl$SwipeMemory;,
        Lcom/daimajia/swipe/implments/SwipeItemMangerImpl$OnLayoutListener;,
        Lcom/daimajia/swipe/implments/SwipeItemMangerImpl$ValueBox;
    }
.end annotation


# instance fields
.field public final INVALID_POSITION:I

.field protected mBaseAdapter:Landroid/widget/BaseAdapter;

.field protected mOpenPosition:I

.field protected mOpenPositions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<Ljava/lang/Integer;>;"
        }
    .end annotation
.end field

.field protected mRecyclerAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;

.field protected mShownLayouts:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<Lcom/daimajia/swipe/SwipeLayout;>;"
        }
    .end annotation
.end field

.field private mode:Lcom/daimajia/swipe/util/Attributes$Mode;


# direct methods
.method public constructor <init>(Landroid/widget/BaseAdapter;)V
    .registers 4
    .param p1, "adapter"    # Landroid/widget/BaseAdapter;

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    sget-object v0, Lcom/daimajia/swipe/util/Attributes$Mode;->Single:Lcom/daimajia/swipe/util/Attributes$Mode;

    iput-object v0, p0, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;->mode:Lcom/daimajia/swipe/util/Attributes$Mode;

    .line 25
    const/4 v0, -0x1

    iput v0, p0, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;->INVALID_POSITION:I

    .line 27
    const/4 v0, -0x1

    iput v0, p0, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;->mOpenPosition:I

    .line 29
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;->mOpenPositions:Ljava/util/Set;

    .line 30
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;->mShownLayouts:Ljava/util/Set;

    .line 36
    if-nez p1, :cond_25

    .line 37
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Adapter can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 39
    :cond_25
    instance-of v0, p1, Lcom/daimajia/swipe/interfaces/SwipeItemMangerInterface;

    if-nez v0, :cond_31

    .line 40
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "adapter should implement the SwipeAdapterInterface"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_31
    iput-object p1, p0, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;->mBaseAdapter:Landroid/widget/BaseAdapter;

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;)Lcom/daimajia/swipe/util/Attributes$Mode;
    .registers 2
    .param p0, "x0"    # Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;

    .line 22
    iget-object v0, p0, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;->mode:Lcom/daimajia/swipe/util/Attributes$Mode;

    return-object v0
.end method


# virtual methods
.method public closeAllExcept(Lcom/daimajia/swipe/SwipeLayout;)V
    .registers 5
    .param p1, "layout"    # Lcom/daimajia/swipe/SwipeLayout;

    .line 116
    iget-object v0, p0, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;->mShownLayouts:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/daimajia/swipe/SwipeLayout;

    .line 117
    .local v2, "s":Lcom/daimajia/swipe/SwipeLayout;
    if-eq v2, p1, :cond_18

    .line 118
    invoke-virtual {v2}, Lcom/daimajia/swipe/SwipeLayout;->close()V

    .line 119
    .end local v2    # "s":Lcom/daimajia/swipe/SwipeLayout;
    :cond_18
    goto :goto_6

    .line 120
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_19
    return-void
.end method

.method public getSwipeLayoutId(I)I
    .registers 3
    .param p1, "position"    # I

    .line 75
    iget-object v0, p0, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;->mBaseAdapter:Landroid/widget/BaseAdapter;

    if-eqz v0, :cond_f

    .line 76
    iget-object v0, p0, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;->mBaseAdapter:Landroid/widget/BaseAdapter;

    check-cast v0, Lcom/daimajia/swipe/interfaces/SwipeAdapterInterface;

    check-cast v0, Lcom/daimajia/swipe/interfaces/SwipeAdapterInterface;

    invoke-interface {v0, p1}, Lcom/daimajia/swipe/interfaces/SwipeAdapterInterface;->getSwipeLayoutResourceId(I)I

    move-result v0

    return v0

    .line 77
    :cond_f
    iget-object v0, p0, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;->mRecyclerAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    if-eqz v0, :cond_1e

    .line 78
    iget-object v0, p0, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;->mRecyclerAdapter:Landroid/support/v7/widget/RecyclerView$Adapter;

    check-cast v0, Lcom/daimajia/swipe/interfaces/SwipeAdapterInterface;

    check-cast v0, Lcom/daimajia/swipe/interfaces/SwipeAdapterInterface;

    invoke-interface {v0, p1}, Lcom/daimajia/swipe/interfaces/SwipeAdapterInterface;->getSwipeLayoutResourceId(I)I

    move-result v0

    return v0

    .line 80
    :cond_1e
    const/4 v0, -0x1

    return v0
.end method

.method public isOpen(I)Z
    .registers 4
    .param p1, "position"    # I

    .line 155
    iget-object v0, p0, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;->mode:Lcom/daimajia/swipe/util/Attributes$Mode;

    sget-object v1, Lcom/daimajia/swipe/util/Attributes$Mode;->Multiple:Lcom/daimajia/swipe/util/Attributes$Mode;

    if-ne v0, v1, :cond_11

    .line 156
    iget-object v0, p0, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;->mOpenPositions:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 158
    :cond_11
    iget v0, p0, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;->mOpenPosition:I

    if-ne v0, p1, :cond_17

    const/4 v0, 0x1

    goto :goto_18

    :cond_17
    const/4 v0, 0x0

    :goto_18
    return v0
.end method

.method public setMode(Lcom/daimajia/swipe/util/Attributes$Mode;)V
    .registers 3
    .param p1, "mode"    # Lcom/daimajia/swipe/util/Attributes$Mode;

    .line 60
    iput-object p1, p0, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;->mode:Lcom/daimajia/swipe/util/Attributes$Mode;

    .line 61
    iget-object v0, p0, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;->mOpenPositions:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 62
    iget-object v0, p0, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;->mShownLayouts:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 63
    const/4 v0, -0x1

    iput v0, p0, Lcom/daimajia/swipe/implments/SwipeItemMangerImpl;->mOpenPosition:I

    .line 64
    return-void
.end method

.class public final Lokhttp3/HttpUrl$Builder;
.super Ljava/lang/Object;
.source "HttpUrl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lokhttp3/HttpUrl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lokhttp3/HttpUrl$Builder$ParseResult;
    }
.end annotation


# instance fields
.field encodedFragment:Ljava/lang/String;

.field encodedPassword:Ljava/lang/String;

.field final encodedPathSegments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field encodedQueryNamesAndValues:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Ljava/lang/String;>;"
        }
    .end annotation
.end field

.field encodedUsername:Ljava/lang/String;

.field host:Ljava/lang/String;

.field port:I

.field scheme:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 3

    .line 945
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 937
    const-string v0, ""

    iput-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedUsername:Ljava/lang/String;

    .line 938
    const-string v0, ""

    iput-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedPassword:Ljava/lang/String;

    .line 940
    const/4 v0, -0x1

    iput v0, p0, Lokhttp3/HttpUrl$Builder;->port:I

    .line 941
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    .line 946
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    const-string v1, ""

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 947
    return-void
.end method

.method private static canonicalizeHost(Ljava/lang/String;II)Ljava/lang/String;
    .registers 8
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "pos"    # I
    .param p2, "limit"    # I

    .line 1544
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lokhttp3/HttpUrl;->percentDecode(Ljava/lang/String;IIZ)Ljava/lang/String;

    move-result-object v2

    .line 1547
    .local v2, "percentDecoded":Ljava/lang/String;
    const-string v0, ":"

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4a

    .line 1549
    const-string v0, "["

    invoke-virtual {v2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_29

    .line 1550
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x1

    invoke-static {v2, v1, v0}, Lokhttp3/HttpUrl$Builder;->decodeIpv6(Ljava/lang/String;II)Ljava/net/InetAddress;

    move-result-object v3

    goto :goto_32

    .line 1551
    :cond_29
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v2, v1, v0}, Lokhttp3/HttpUrl$Builder;->decodeIpv6(Ljava/lang/String;II)Ljava/net/InetAddress;

    move-result-object v3

    .line 1552
    .local v3, "inetAddress":Ljava/net/InetAddress;
    :goto_32
    if-nez v3, :cond_36

    const/4 v0, 0x0

    return-object v0

    .line 1553
    :cond_36
    invoke-virtual {v3}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v4

    .line 1554
    .local v4, "address":[B
    array-length v0, v4

    const/16 v1, 0x10

    if-ne v0, v1, :cond_44

    invoke-static {v4}, Lokhttp3/HttpUrl$Builder;->inet6AddressToAscii([B)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1555
    :cond_44
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1558
    .end local v3    # "inetAddress":Ljava/net/InetAddress;
    .end local v4    # "address":[B
    :cond_4a
    invoke-static {v2}, Lokhttp3/internal/Util;->domainToAscii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static decodeIpv4Suffix(Ljava/lang/String;II[BI)Z
    .registers 12
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "pos"    # I
    .param p2, "limit"    # I
    .param p3, "address"    # [B
    .param p4, "addressOffset"    # I

    .line 1635
    move v2, p4

    .line 1637
    .local v2, "b":I
    move v3, p1

    .local v3, "i":I
    :goto_2
    if-ge v3, p2, :cond_4a

    .line 1638
    array-length v0, p3

    if-ne v2, v0, :cond_9

    const/4 v0, 0x0

    return v0

    .line 1641
    :cond_9
    if-eq v2, p4, :cond_17

    .line 1642
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2e

    if-eq v0, v1, :cond_15

    const/4 v0, 0x0

    return v0

    .line 1643
    :cond_15
    add-int/lit8 v3, v3, 0x1

    .line 1647
    :cond_17
    const/4 v4, 0x0

    .line 1648
    .local v4, "value":I
    move v5, v3

    .line 1649
    .local v5, "groupOffset":I
    :goto_19
    if-ge v3, p2, :cond_3c

    .line 1650
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 1651
    .local v6, "c":C
    const/16 v0, 0x30

    if-lt v6, v0, :cond_3c

    const/16 v0, 0x39

    if-le v6, v0, :cond_28

    goto :goto_3c

    .line 1652
    :cond_28
    if-nez v4, :cond_2e

    if-eq v5, v3, :cond_2e

    const/4 v0, 0x0

    return v0

    .line 1653
    :cond_2e
    mul-int/lit8 v0, v4, 0xa

    add-int/2addr v0, v6

    add-int/lit8 v4, v0, -0x30

    .line 1654
    const/16 v0, 0xff

    if-le v4, v0, :cond_39

    const/4 v0, 0x0

    return v0

    .line 1649
    .end local v6    # "c":C
    :cond_39
    add-int/lit8 v3, v3, 0x1

    goto :goto_19

    .line 1656
    :cond_3c
    :goto_3c
    sub-int v6, v3, v5

    .line 1657
    .local v6, "groupLength":I
    if-nez v6, :cond_42

    const/4 v0, 0x0

    return v0

    .line 1660
    :cond_42
    move v0, v2

    add-int/lit8 v2, v2, 0x1

    int-to-byte v1, v4

    aput-byte v1, p3, v0

    .line 1661
    .end local v4    # "value":I
    .end local v5    # "groupOffset":I
    .end local v6    # "groupLength":I
    goto/16 :goto_2

    .line 1663
    .end local v3    # "i":I
    :cond_4a
    add-int/lit8 v0, p4, 0x4

    if-eq v2, v0, :cond_50

    const/4 v0, 0x0

    return v0

    .line 1664
    :cond_50
    const/4 v0, 0x1

    return v0
.end method

.method private static decodeIpv6(Ljava/lang/String;II)Ljava/net/InetAddress;
    .registers 14
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "pos"    # I
    .param p2, "limit"    # I

    .line 1563
    const/16 v0, 0x10

    new-array v3, v0, [B

    .line 1564
    .local v3, "address":[B
    const/4 v4, 0x0

    .line 1565
    .local v4, "b":I
    const/4 v5, -0x1

    .line 1566
    .local v5, "compress":I
    const/4 v6, -0x1

    .line 1568
    .local v6, "groupOffset":I
    move v7, p1

    .local v7, "i":I
    :goto_8
    if-ge v7, p2, :cond_87

    .line 1569
    array-length v0, v3

    if-ne v4, v0, :cond_f

    const/4 v0, 0x0

    return-object v0

    .line 1572
    :cond_f
    add-int/lit8 v0, v7, 0x2

    if-gt v0, p2, :cond_2b

    const-string v0, "::"

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-virtual {p0, v7, v0, v1, v2}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_2b

    .line 1574
    const/4 v0, -0x1

    if-eq v5, v0, :cond_22

    const/4 v0, 0x0

    return-object v0

    .line 1575
    :cond_22
    add-int/lit8 v7, v7, 0x2

    .line 1576
    add-int/lit8 v4, v4, 0x2

    .line 1577
    move v5, v4

    .line 1578
    if-ne v7, p2, :cond_53

    goto/16 :goto_87

    .line 1579
    :cond_2b
    if-eqz v4, :cond_53

    .line 1581
    const-string v0, ":"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v7, v0, v1, v2}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 1582
    add-int/lit8 v7, v7, 0x1

    goto :goto_53

    .line 1583
    :cond_3a
    const-string v0, "."

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v7, v0, v1, v2}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_51

    .line 1585
    add-int/lit8 v0, v4, -0x2

    invoke-static {p0, v6, p2, v3, v0}, Lokhttp3/HttpUrl$Builder;->decodeIpv4Suffix(Ljava/lang/String;II[BI)Z

    move-result v0

    if-nez v0, :cond_4e

    const/4 v0, 0x0

    return-object v0

    .line 1586
    :cond_4e
    add-int/lit8 v4, v4, 0x2

    .line 1587
    goto :goto_87

    .line 1589
    :cond_51
    const/4 v0, 0x0

    return-object v0

    .line 1594
    :cond_53
    :goto_53
    const/4 v8, 0x0

    .line 1595
    .local v8, "value":I
    move v6, v7

    .line 1596
    :goto_55
    if-ge v7, p2, :cond_6a

    .line 1597
    invoke-virtual {p0, v7}, Ljava/lang/String;->charAt(I)C

    move-result v9

    .line 1598
    .local v9, "c":C
    invoke-static {v9}, Lokhttp3/HttpUrl;->decodeHexDigit(C)I

    move-result v10

    .line 1599
    .local v10, "hexDigit":I
    const/4 v0, -0x1

    if-ne v10, v0, :cond_63

    goto :goto_6a

    .line 1600
    :cond_63
    shl-int/lit8 v0, v8, 0x4

    add-int v8, v0, v10

    .line 1596
    .end local v9    # "c":C
    .end local v10    # "hexDigit":I
    add-int/lit8 v7, v7, 0x1

    goto :goto_55

    .line 1602
    :cond_6a
    :goto_6a
    sub-int v9, v7, v6

    .line 1603
    .local v9, "groupLength":I
    if-eqz v9, :cond_71

    const/4 v0, 0x4

    if-le v9, v0, :cond_73

    :cond_71
    const/4 v0, 0x0

    return-object v0

    .line 1606
    :cond_73
    move v0, v4

    add-int/lit8 v4, v4, 0x1

    ushr-int/lit8 v1, v8, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, v3, v0

    .line 1607
    move v0, v4

    add-int/lit8 v4, v4, 0x1

    and-int/lit16 v1, v8, 0xff

    int-to-byte v1, v1

    aput-byte v1, v3, v0

    .line 1608
    .end local v8    # "value":I
    .end local v9    # "groupLength":I
    goto/16 :goto_8

    .line 1619
    .end local v7    # "i":I
    :cond_87
    :goto_87
    array-length v0, v3

    if-eq v4, v0, :cond_9f

    .line 1620
    const/4 v0, -0x1

    if-ne v5, v0, :cond_8f

    const/4 v0, 0x0

    return-object v0

    .line 1621
    :cond_8f
    array-length v0, v3

    sub-int v1, v4, v5

    sub-int/2addr v0, v1

    sub-int v1, v4, v5

    invoke-static {v3, v5, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1622
    array-length v0, v3

    sub-int/2addr v0, v4

    add-int/2addr v0, v5

    const/4 v1, 0x0

    invoke-static {v3, v5, v0, v1}, Ljava/util/Arrays;->fill([BIIB)V

    .line 1626
    :cond_9f
    :try_start_9f
    invoke-static {v3}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;
    :try_end_a2
    .catch Ljava/net/UnknownHostException; {:try_start_9f .. :try_end_a2} :catch_a4

    move-result-object v0

    return-object v0

    .line 1627
    :catch_a4
    move-exception v7

    .line 1628
    .local v7, "e":Ljava/net/UnknownHostException;
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private static inet6AddressToAscii([B)Ljava/lang/String;
    .registers 9
    .param p0, "address"    # [B

    .line 1669
    const/4 v3, -0x1

    .line 1670
    .local v3, "longestRunOffset":I
    const/4 v4, 0x0

    .line 1671
    .local v4, "longestRunLength":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_3
    array-length v0, p0

    if-ge v5, v0, :cond_21

    .line 1672
    move v6, v5

    .line 1673
    .local v6, "currentRunOffset":I
    :goto_7
    const/16 v0, 0x10

    if-ge v5, v0, :cond_18

    aget-byte v0, p0, v5

    if-nez v0, :cond_18

    add-int/lit8 v0, v5, 0x1

    aget-byte v0, p0, v0

    if-nez v0, :cond_18

    .line 1674
    add-int/lit8 v5, v5, 0x2

    goto :goto_7

    .line 1676
    :cond_18
    sub-int v7, v5, v6

    .line 1677
    .local v7, "currentRunLength":I
    if-le v7, v4, :cond_1e

    .line 1678
    move v3, v6

    .line 1679
    move v4, v7

    .line 1671
    .end local v6    # "currentRunOffset":I
    .end local v7    # "currentRunLength":I
    :cond_1e
    add-int/lit8 v5, v5, 0x2

    goto :goto_3

    .line 1684
    .end local v5    # "i":I
    :cond_21
    new-instance v5, Lokio/Buffer;

    invoke-direct {v5}, Lokio/Buffer;-><init>()V

    .line 1685
    .local v5, "result":Lokio/Buffer;
    const/4 v6, 0x0

    .local v6, "i":I
    :cond_27
    :goto_27
    array-length v0, p0

    if-ge v6, v0, :cond_58

    .line 1686
    if-ne v6, v3, :cond_3c

    .line 1687
    const/16 v0, 0x3a

    invoke-virtual {v5, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    .line 1688
    add-int/2addr v6, v4

    .line 1689
    const/16 v0, 0x10

    if-ne v6, v0, :cond_27

    const/16 v0, 0x3a

    invoke-virtual {v5, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    goto :goto_27

    .line 1691
    :cond_3c
    if-lez v6, :cond_43

    const/16 v0, 0x3a

    invoke-virtual {v5, v0}, Lokio/Buffer;->writeByte(I)Lokio/Buffer;

    .line 1692
    :cond_43
    aget-byte v0, p0, v6

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    add-int/lit8 v1, v6, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v1, v1, 0xff

    or-int v7, v0, v1

    .line 1693
    .local v7, "group":I
    int-to-long v0, v7

    invoke-virtual {v5, v0, v1}, Lokio/Buffer;->writeHexadecimalUnsignedLong(J)Lokio/Buffer;

    .line 1694
    add-int/lit8 v6, v6, 0x2

    .line 1695
    .end local v7    # "group":I
    goto :goto_27

    .line 1697
    .end local v6    # "i":I
    :cond_58
    invoke-virtual {v5}, Lokio/Buffer;->readUtf8()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isDot(Ljava/lang/String;)Z
    .registers 3
    .param p1, "input"    # Ljava/lang/String;

    .line 1449
    const-string v0, "."

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    const-string v0, "%2e"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_10
    const/4 v0, 0x1

    goto :goto_13

    :cond_12
    const/4 v0, 0x0

    :goto_13
    return v0
.end method

.method private isDotDot(Ljava/lang/String;)Z
    .registers 3
    .param p1, "input"    # Ljava/lang/String;

    .line 1453
    const-string v0, ".."

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_20

    const-string v0, "%2e."

    .line 1454
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_20

    const-string v0, ".%2e"

    .line 1455
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_20

    const-string v0, "%2e%2e"

    .line 1456
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    :cond_20
    const/4 v0, 0x1

    goto :goto_23

    :cond_22
    const/4 v0, 0x0

    .line 1453
    :goto_23
    return v0
.end method

.method private static parsePort(Ljava/lang/String;II)I
    .registers 13
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "pos"    # I
    .param p2, "limit"    # I

    .line 1703
    move-object v0, p0

    move v1, p1

    move v2, p2

    const-string v3, ""

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    :try_start_9
    invoke-static/range {v0 .. v7}, Lokhttp3/HttpUrl;->canonicalize(Ljava/lang/String;IILjava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v8

    .line 1704
    .local v8, "portString":Ljava/lang/String;
    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_10
    .catch Ljava/lang/NumberFormatException; {:try_start_9 .. :try_end_10} :catch_1b

    move-result v9

    .line 1705
    .local v9, "i":I
    if-lez v9, :cond_19

    const v0, 0xffff

    if-gt v9, v0, :cond_19

    return v9

    .line 1706
    :cond_19
    const/4 v0, -0x1

    return v0

    .line 1707
    .end local v8    # "portString":Ljava/lang/String;
    .end local v9    # "i":I
    :catch_1b
    move-exception v8

    .line 1708
    .local v8, "e":Ljava/lang/NumberFormatException;
    const/4 v0, -0x1

    return v0
.end method

.method private pop()V
    .registers 5

    .line 1470
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    iget-object v1, p0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/lang/String;

    .line 1473
    .local v3, "removed":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2f

    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2f

    .line 1474
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    iget-object v1, p0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_36

    .line 1476
    :cond_2f
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    const-string v1, ""

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1478
    :goto_36
    return-void
.end method

.method private static portColonOffset(Ljava/lang/String;II)I
    .registers 6
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "pos"    # I
    .param p2, "limit"    # I

    .line 1527
    move v2, p1

    .local v2, "i":I
    :goto_1
    if-ge v2, p2, :cond_1c

    .line 1528
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    sparse-switch v0, :sswitch_data_1e

    goto :goto_19

    .line 1530
    :cond_b
    :sswitch_b
    add-int/lit8 v2, v2, 0x1

    if-ge v2, p2, :cond_19

    .line 1531
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x5d

    if-ne v0, v1, :cond_b

    goto :goto_19

    .line 1535
    :sswitch_18
    return v2

    .line 1527
    :cond_19
    :goto_19
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1538
    .end local v2    # "i":I
    :cond_1c
    return p2

    nop

    :sswitch_data_1e
    .sparse-switch
        0x3a -> :sswitch_18
        0x5b -> :sswitch_b
    .end sparse-switch
.end method

.method private push(Ljava/lang/String;IIZZ)V
    .registers 15
    .param p1, "input"    # Ljava/lang/String;
    .param p2, "pos"    # I
    .param p3, "limit"    # I
    .param p4, "addTrailingSlash"    # Z
    .param p5, "alreadyEncoded"    # Z

    .line 1429
    move-object v0, p1

    move v1, p2

    move v2, p3

    const-string v3, " \"<>^`{}|/\\?#"

    move v4, p5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-static/range {v0 .. v7}, Lokhttp3/HttpUrl;->canonicalize(Ljava/lang/String;IILjava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v8

    .line 1431
    .local v8, "segment":Ljava/lang/String;
    invoke-direct {p0, v8}, Lokhttp3/HttpUrl$Builder;->isDot(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1432
    return-void

    .line 1434
    :cond_14
    invoke-direct {p0, v8}, Lokhttp3/HttpUrl$Builder;->isDotDot(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 1435
    invoke-direct {p0}, Lokhttp3/HttpUrl$Builder;->pop()V

    .line 1436
    return-void

    .line 1438
    :cond_1e
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    iget-object v1, p0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_42

    .line 1439
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    iget-object v1, p0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1, v8}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_47

    .line 1441
    :cond_42
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1443
    :goto_47
    if-eqz p4, :cond_50

    .line 1444
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    const-string v1, ""

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1446
    :cond_50
    return-void
.end method

.method private resolvePath(Ljava/lang/String;II)V
    .registers 14
    .param p1, "input"    # Ljava/lang/String;
    .param p2, "pos"    # I
    .param p3, "limit"    # I

    .line 1401
    if-ne p2, p3, :cond_3

    .line 1403
    return-void

    .line 1405
    :cond_3
    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v6

    .line 1406
    .local v6, "c":C
    const/16 v0, 0x2f

    if-eq v6, v0, :cond_f

    const/16 v0, 0x5c

    if-ne v6, v0, :cond_1e

    .line 1408
    :cond_f
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1409
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    const-string v1, ""

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1410
    add-int/lit8 p2, p2, 0x1

    goto :goto_2d

    .line 1413
    :cond_1e
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    iget-object v1, p0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1417
    :goto_2d
    move v7, p2

    .local v7, "i":I
    :goto_2e
    if-ge v7, p3, :cond_4a

    .line 1418
    const-string v0, "/\\"

    invoke-static {p1, v7, p3, v0}, Lokhttp3/internal/Util;->delimiterOffset(Ljava/lang/String;IILjava/lang/String;)I

    move-result v8

    .line 1419
    .local v8, "pathSegmentDelimiterOffset":I
    if-ge v8, p3, :cond_3a

    const/4 v9, 0x1

    goto :goto_3b

    :cond_3a
    const/4 v9, 0x0

    .line 1420
    .local v9, "segmentHasTrailingSlash":Z
    :goto_3b
    move-object v0, p0

    move-object v1, p1

    move v2, v7

    move v3, v8

    move v4, v9

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lokhttp3/HttpUrl$Builder;->push(Ljava/lang/String;IIZZ)V

    .line 1421
    move v7, v8

    .line 1422
    if-eqz v9, :cond_49

    add-int/lit8 v7, v7, 0x1

    .line 1423
    .end local v8    # "pathSegmentDelimiterOffset":I
    .end local v9    # "segmentHasTrailingSlash":Z
    :cond_49
    goto :goto_2e

    .line 1424
    .end local v7    # "i":I
    :cond_4a
    return-void
.end method

.method private static schemeDelimiterOffset(Ljava/lang/String;II)I
    .registers 8
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "pos"    # I
    .param p2, "limit"    # I

    .line 1485
    sub-int v0, p2, p1

    const/4 v1, 0x2

    if-ge v0, v1, :cond_7

    const/4 v0, -0x1

    return v0

    .line 1487
    :cond_7
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 1488
    .local v2, "c0":C
    const/16 v0, 0x61

    if-lt v2, v0, :cond_13

    const/16 v0, 0x7a

    if-le v2, v0, :cond_1d

    :cond_13
    const/16 v0, 0x41

    if-lt v2, v0, :cond_1b

    const/16 v0, 0x5a

    if-le v2, v0, :cond_1d

    :cond_1b
    const/4 v0, -0x1

    return v0

    .line 1490
    :cond_1d
    add-int/lit8 v3, p1, 0x1

    .local v3, "i":I
    :goto_1f
    if-ge v3, p2, :cond_54

    .line 1491
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 1493
    .local v4, "c":C
    const/16 v0, 0x61

    if-lt v4, v0, :cond_2d

    const/16 v0, 0x7a

    if-le v4, v0, :cond_51

    :cond_2d
    const/16 v0, 0x41

    if-lt v4, v0, :cond_35

    const/16 v0, 0x5a

    if-le v4, v0, :cond_51

    :cond_35
    const/16 v0, 0x30

    if-lt v4, v0, :cond_3d

    const/16 v0, 0x39

    if-le v4, v0, :cond_51

    :cond_3d
    const/16 v0, 0x2b

    if-eq v4, v0, :cond_51

    const/16 v0, 0x2d

    if-eq v4, v0, :cond_51

    const/16 v0, 0x2e

    if-ne v4, v0, :cond_4a

    .line 1499
    goto :goto_51

    .line 1500
    :cond_4a
    const/16 v0, 0x3a

    if-ne v4, v0, :cond_4f

    .line 1501
    return v3

    .line 1503
    :cond_4f
    const/4 v0, -0x1

    return v0

    .line 1490
    .end local v4    # "c":C
    :cond_51
    :goto_51
    add-int/lit8 v3, v3, 0x1

    goto :goto_1f

    .line 1507
    .end local v3    # "i":I
    :cond_54
    const/4 v0, -0x1

    return v0
.end method

.method private static slashCount(Ljava/lang/String;II)I
    .registers 6
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "pos"    # I
    .param p2, "limit"    # I

    .line 1512
    const/4 v1, 0x0

    .line 1513
    .local v1, "slashCount":I
    :goto_1
    if-ge p1, p2, :cond_14

    .line 1514
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 1515
    .local v2, "c":C
    const/16 v0, 0x5c

    if-eq v2, v0, :cond_f

    const/16 v0, 0x2f

    if-ne v2, v0, :cond_14

    .line 1516
    :cond_f
    add-int/lit8 v1, v1, 0x1

    .line 1517
    add-int/lit8 p1, p1, 0x1

    .line 1521
    .end local v2    # "c":C
    goto :goto_1

    .line 1522
    :cond_14
    return v1
.end method


# virtual methods
.method public build()Lokhttp3/HttpUrl;
    .registers 3

    .line 1220
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->scheme:Ljava/lang/String;

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "scheme == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1221
    :cond_c
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->host:Ljava/lang/String;

    if-nez v0, :cond_18

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "host == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1222
    :cond_18
    new-instance v0, Lokhttp3/HttpUrl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lokhttp3/HttpUrl;-><init>(Lokhttp3/HttpUrl$Builder;Lokhttp3/HttpUrl$1;)V

    return-object v0
.end method

.method effectivePort()I
    .registers 3

    .line 1007
    iget v0, p0, Lokhttp3/HttpUrl$Builder;->port:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_8

    iget v0, p0, Lokhttp3/HttpUrl$Builder;->port:I

    goto :goto_e

    :cond_8
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->scheme:Ljava/lang/String;

    invoke-static {v0}, Lokhttp3/HttpUrl;->defaultPort(Ljava/lang/String;)I

    move-result v0

    :goto_e
    return v0
.end method

.method public encodedQuery(Ljava/lang/String;)Lokhttp3/HttpUrl$Builder;
    .registers 8
    .param p1, "encodedQuery"    # Ljava/lang/String;

    .line 1106
    if-eqz p1, :cond_12

    move-object v0, p1

    const-string v1, " \"\'<>#"

    .line 1108
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-static/range {v0 .. v5}, Lokhttp3/HttpUrl;->canonicalize(Ljava/lang/String;Ljava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v0

    .line 1107
    invoke-static {v0}, Lokhttp3/HttpUrl;->queryStringToNamesAndValues(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    goto :goto_13

    :cond_12
    const/4 v0, 0x0

    :goto_13
    iput-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedQueryNamesAndValues:Ljava/util/List;

    .line 1110
    return-object p0
.end method

.method public host(Ljava/lang/String;)Lokhttp3/HttpUrl$Builder;
    .registers 6
    .param p1, "host"    # Ljava/lang/String;

    .line 993
    if-nez p1, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "host == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 994
    :cond_a
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {p1, v1, v0}, Lokhttp3/HttpUrl$Builder;->canonicalizeHost(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v3

    .line 995
    .local v3, "encoded":Ljava/lang/String;
    if-nez v3, :cond_2e

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unexpected host: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 996
    :cond_2e
    iput-object v3, p0, Lokhttp3/HttpUrl$Builder;->host:Ljava/lang/String;

    .line 997
    return-object p0
.end method

.method parse(Lokhttp3/HttpUrl;Ljava/lang/String;)Lokhttp3/HttpUrl$Builder$ParseResult;
    .registers 22
    .param p1, "base"    # Lokhttp3/HttpUrl;
    .param p2, "input"    # Ljava/lang/String;

    .line 1278
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v0

    move-object/from16 v1, p2

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Lokhttp3/internal/Util;->skipLeadingAsciiWhitespace(Ljava/lang/String;II)I

    move-result v9

    .line 1279
    .local v9, "pos":I
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v0

    move-object/from16 v1, p2

    invoke-static {v1, v9, v0}, Lokhttp3/internal/Util;->skipTrailingAsciiWhitespace(Ljava/lang/String;II)I

    move-result v10

    .line 1282
    .local v10, "limit":I
    move-object/from16 v0, p2

    invoke-static {v0, v9, v10}, Lokhttp3/HttpUrl$Builder;->schemeDelimiterOffset(Ljava/lang/String;II)I

    move-result v11

    .line 1283
    .local v11, "schemeDelimiterOffset":I
    const/4 v0, -0x1

    if-eq v11, v0, :cond_59

    .line 1284
    move-object/from16 v0, p2

    move v2, v9

    const-string v3, "https:"

    const/4 v1, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x6

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_3a

    .line 1285
    const-string v0, "https"

    move-object/from16 v1, p0

    iput-object v0, v1, Lokhttp3/HttpUrl$Builder;->scheme:Ljava/lang/String;

    .line 1286
    const-string v0, "https:"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v9, v0

    goto :goto_67

    .line 1287
    :cond_3a
    move-object/from16 v0, p2

    move v2, v9

    const-string v3, "http:"

    const/4 v1, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x5

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_56

    .line 1288
    const-string v0, "http"

    move-object/from16 v1, p0

    iput-object v0, v1, Lokhttp3/HttpUrl$Builder;->scheme:Ljava/lang/String;

    .line 1289
    const-string v0, "http:"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v9, v0

    goto :goto_67

    .line 1291
    :cond_56
    sget-object v0, Lokhttp3/HttpUrl$Builder$ParseResult;->UNSUPPORTED_SCHEME:Lokhttp3/HttpUrl$Builder$ParseResult;

    return-object v0

    .line 1293
    :cond_59
    if-eqz p1, :cond_64

    .line 1294
    # getter for: Lokhttp3/HttpUrl;->scheme:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lokhttp3/HttpUrl;->access$100(Lokhttp3/HttpUrl;)Ljava/lang/String;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lokhttp3/HttpUrl$Builder;->scheme:Ljava/lang/String;

    goto :goto_67

    .line 1296
    :cond_64
    sget-object v0, Lokhttp3/HttpUrl$Builder$ParseResult;->MISSING_SCHEME:Lokhttp3/HttpUrl$Builder$ParseResult;

    return-object v0

    .line 1300
    :goto_67
    const/4 v12, 0x0

    .line 1301
    .local v12, "hasUsername":Z
    const/4 v13, 0x0

    .line 1302
    .local v13, "hasPassword":Z
    move-object/from16 v0, p2

    invoke-static {v0, v9, v10}, Lokhttp3/HttpUrl$Builder;->slashCount(Ljava/lang/String;II)I

    move-result v14

    .line 1303
    .local v14, "slashCount":I
    const/4 v0, 0x2

    if-ge v14, v0, :cond_82

    if-eqz p1, :cond_82

    # getter for: Lokhttp3/HttpUrl;->scheme:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lokhttp3/HttpUrl;->access$100(Lokhttp3/HttpUrl;)Ljava/lang/String;

    move-result-object v0

    move-object/from16 v1, p0

    iget-object v1, v1, Lokhttp3/HttpUrl$Builder;->scheme:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_175

    .line 1313
    :cond_82
    add-int/2addr v9, v14

    .line 1316
    :goto_83
    const-string v0, "@/\\?#"

    move-object/from16 v1, p2

    invoke-static {v1, v9, v10, v0}, Lokhttp3/internal/Util;->delimiterOffset(Ljava/lang/String;IILjava/lang/String;)I

    move-result v15

    .line 1317
    .local v15, "componentDelimiterOffset":I
    if-eq v15, v10, :cond_94

    .line 1318
    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Ljava/lang/String;->charAt(I)C

    move-result v16

    goto :goto_96

    :cond_94
    const/16 v16, -0x1

    .line 1320
    .local v16, "c":I
    :goto_96
    sparse-switch v16, :sswitch_data_21c

    goto/16 :goto_173

    .line 1323
    :sswitch_9b
    if-nez v13, :cond_f4

    .line 1324
    move-object/from16 v0, p2

    const/16 v1, 0x3a

    invoke-static {v0, v9, v15, v1}, Lokhttp3/internal/Util;->delimiterOffset(Ljava/lang/String;IIC)I

    move-result v17

    .line 1326
    .local v17, "passwordColonOffset":I
    move-object/from16 v0, p2

    move v1, v9

    move/from16 v2, v17

    const-string v3, " \"\':;<=>@[]^`{}|/\\?#"

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-static/range {v0 .. v7}, Lokhttp3/HttpUrl;->canonicalize(Ljava/lang/String;IILjava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v18

    .line 1328
    .local v18, "canonicalUsername":Ljava/lang/String;
    if-eqz v12, :cond_d4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v1, p0

    iget-object v1, v1, Lokhttp3/HttpUrl$Builder;->encodedUsername:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%40"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_d6

    :cond_d4
    move-object/from16 v0, v18

    :goto_d6
    move-object/from16 v1, p0

    iput-object v0, v1, Lokhttp3/HttpUrl$Builder;->encodedUsername:Ljava/lang/String;

    .line 1331
    move/from16 v0, v17

    if-eq v0, v15, :cond_f2

    .line 1332
    const/4 v13, 0x1

    .line 1333
    move-object/from16 v0, p2

    add-int/lit8 v1, v17, 0x1

    move v2, v15

    const-string v3, " \"\':;<=>@[]^`{}|/\\?#"

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-static/range {v0 .. v7}, Lokhttp3/HttpUrl;->canonicalize(Ljava/lang/String;IILjava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lokhttp3/HttpUrl$Builder;->encodedPassword:Ljava/lang/String;

    .line 1336
    :cond_f2
    const/4 v12, 0x1

    .line 1337
    .end local v17    # "passwordColonOffset":I
    .end local v18    # "canonicalUsername":Ljava/lang/String;
    goto :goto_121

    .line 1338
    :cond_f4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v1, p0

    iget-object v1, v1, Lokhttp3/HttpUrl$Builder;->encodedPassword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%40"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object/from16 v1, p2

    move v2, v9

    move v3, v15

    const-string v4, " \"\':;<=>@[]^`{}|/\\?#"

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-static/range {v1 .. v8}, Lokhttp3/HttpUrl;->canonicalize(Ljava/lang/String;IILjava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lokhttp3/HttpUrl$Builder;->encodedPassword:Ljava/lang/String;

    .line 1341
    :goto_121
    add-int/lit8 v9, v15, 0x1

    .line 1342
    goto :goto_173

    .line 1350
    :sswitch_124
    move-object/from16 v0, p2

    invoke-static {v0, v9, v15}, Lokhttp3/HttpUrl$Builder;->portColonOffset(Ljava/lang/String;II)I

    move-result v17

    .line 1351
    .local v17, "portColonOffset":I
    add-int/lit8 v0, v17, 0x1

    if-ge v0, v15, :cond_150

    .line 1352
    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-static {v0, v9, v1}, Lokhttp3/HttpUrl$Builder;->canonicalizeHost(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lokhttp3/HttpUrl$Builder;->host:Ljava/lang/String;

    .line 1353
    add-int/lit8 v0, v17, 0x1

    move-object/from16 v1, p2

    invoke-static {v1, v0, v15}, Lokhttp3/HttpUrl$Builder;->parsePort(Ljava/lang/String;II)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lokhttp3/HttpUrl$Builder;->port:I

    .line 1354
    move-object/from16 v0, p0

    iget v0, v0, Lokhttp3/HttpUrl$Builder;->port:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_168

    sget-object v0, Lokhttp3/HttpUrl$Builder$ParseResult;->INVALID_PORT:Lokhttp3/HttpUrl$Builder$ParseResult;

    return-object v0

    .line 1356
    :cond_150
    move-object/from16 v0, p2

    move/from16 v1, v17

    invoke-static {v0, v9, v1}, Lokhttp3/HttpUrl$Builder;->canonicalizeHost(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lokhttp3/HttpUrl$Builder;->host:Ljava/lang/String;

    .line 1357
    move-object/from16 v0, p0

    iget-object v0, v0, Lokhttp3/HttpUrl$Builder;->scheme:Ljava/lang/String;

    invoke-static {v0}, Lokhttp3/HttpUrl;->defaultPort(Ljava/lang/String;)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lokhttp3/HttpUrl$Builder;->port:I

    .line 1359
    :cond_168
    move-object/from16 v0, p0

    iget-object v0, v0, Lokhttp3/HttpUrl$Builder;->host:Ljava/lang/String;

    if-nez v0, :cond_171

    sget-object v0, Lokhttp3/HttpUrl$Builder$ParseResult;->INVALID_HOST:Lokhttp3/HttpUrl$Builder$ParseResult;

    return-object v0

    .line 1360
    :cond_171
    move v9, v15

    .line 1361
    goto :goto_1bc

    .line 1363
    .end local v15    # "componentDelimiterOffset":I
    .end local v16    # "c":I
    .end local v17    # "portColonOffset":I
    :goto_173
    goto/16 :goto_83

    .line 1366
    :cond_175
    invoke-virtual/range {p1 .. p1}, Lokhttp3/HttpUrl;->encodedUsername()Ljava/lang/String;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lokhttp3/HttpUrl$Builder;->encodedUsername:Ljava/lang/String;

    .line 1367
    invoke-virtual/range {p1 .. p1}, Lokhttp3/HttpUrl;->encodedPassword()Ljava/lang/String;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lokhttp3/HttpUrl$Builder;->encodedPassword:Ljava/lang/String;

    .line 1368
    # getter for: Lokhttp3/HttpUrl;->host:Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lokhttp3/HttpUrl;->access$200(Lokhttp3/HttpUrl;)Ljava/lang/String;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lokhttp3/HttpUrl$Builder;->host:Ljava/lang/String;

    .line 1369
    # getter for: Lokhttp3/HttpUrl;->port:I
    invoke-static/range {p1 .. p1}, Lokhttp3/HttpUrl;->access$300(Lokhttp3/HttpUrl;)I

    move-result v0

    move-object/from16 v1, p0

    iput v0, v1, Lokhttp3/HttpUrl$Builder;->port:I

    .line 1370
    move-object/from16 v0, p0

    iget-object v0, v0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1371
    move-object/from16 v0, p0

    iget-object v0, v0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    invoke-virtual/range {p1 .. p1}, Lokhttp3/HttpUrl;->encodedPathSegments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1372
    if-eq v9, v10, :cond_1b3

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x23

    if-ne v0, v1, :cond_1bc

    .line 1373
    :cond_1b3
    invoke-virtual/range {p1 .. p1}, Lokhttp3/HttpUrl;->encodedQuery()Ljava/lang/String;

    move-result-object v0

    move-object/from16 v1, p0

    invoke-virtual {v1, v0}, Lokhttp3/HttpUrl$Builder;->encodedQuery(Ljava/lang/String;)Lokhttp3/HttpUrl$Builder;

    .line 1378
    :cond_1bc
    :goto_1bc
    const-string v0, "?#"

    move-object/from16 v1, p2

    invoke-static {v1, v9, v10, v0}, Lokhttp3/internal/Util;->delimiterOffset(Ljava/lang/String;IILjava/lang/String;)I

    move-result v15

    .line 1379
    .local v15, "pathDelimiterOffset":I
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v9, v15}, Lokhttp3/HttpUrl$Builder;->resolvePath(Ljava/lang/String;II)V

    .line 1380
    move v9, v15

    .line 1383
    if-ge v9, v10, :cond_1fa

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x3f

    if-ne v0, v1, :cond_1fa

    .line 1384
    move-object/from16 v0, p2

    const/16 v1, 0x23

    invoke-static {v0, v9, v10, v1}, Lokhttp3/internal/Util;->delimiterOffset(Ljava/lang/String;IIC)I

    move-result v16

    .line 1385
    .local v16, "queryDelimiterOffset":I
    move-object/from16 v0, p2

    add-int/lit8 v1, v9, 0x1

    move/from16 v2, v16

    const-string v3, " \"\'<>#"

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-static/range {v0 .. v7}, Lokhttp3/HttpUrl;->canonicalize(Ljava/lang/String;IILjava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lokhttp3/HttpUrl;->queryStringToNamesAndValues(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lokhttp3/HttpUrl$Builder;->encodedQueryNamesAndValues:Ljava/util/List;

    .line 1387
    move/from16 v9, v16

    .line 1391
    .end local v16    # "queryDelimiterOffset":I
    :cond_1fa
    if-ge v9, v10, :cond_219

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x23

    if-ne v0, v1, :cond_219

    .line 1392
    move-object/from16 v0, p2

    add-int/lit8 v1, v9, 0x1

    move v2, v10

    const-string v3, ""

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lokhttp3/HttpUrl;->canonicalize(Ljava/lang/String;IILjava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v0

    move-object/from16 v1, p0

    iput-object v0, v1, Lokhttp3/HttpUrl$Builder;->encodedFragment:Ljava/lang/String;

    .line 1396
    :cond_219
    sget-object v0, Lokhttp3/HttpUrl$Builder$ParseResult;->SUCCESS:Lokhttp3/HttpUrl$Builder$ParseResult;

    return-object v0

    :sswitch_data_21c
    .sparse-switch
        -0x1 -> :sswitch_124
        0x23 -> :sswitch_124
        0x2f -> :sswitch_124
        0x3f -> :sswitch_124
        0x40 -> :sswitch_9b
        0x5c -> :sswitch_124
    .end sparse-switch
.end method

.method public port(I)Lokhttp3/HttpUrl$Builder;
    .registers 5
    .param p1, "port"    # I

    .line 1001
    if-lez p1, :cond_7

    const v0, 0xffff

    if-le p1, v0, :cond_20

    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unexpected port: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1002
    :cond_20
    iput p1, p0, Lokhttp3/HttpUrl$Builder;->port:I

    .line 1003
    return-object p0
.end method

.method reencodeForUri()Lokhttp3/HttpUrl$Builder;
    .registers 11

    .line 1198
    const/4 v7, 0x0

    .local v7, "i":I
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    .local v8, "size":I
    :goto_7
    if-ge v7, v8, :cond_25

    .line 1199
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Ljava/lang/String;

    .line 1200
    .local v9, "pathSegment":Ljava/lang/String;
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    move-object v1, v9

    const-string v2, "[]"

    .line 1201
    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static/range {v1 .. v6}, Lokhttp3/HttpUrl;->canonicalize(Ljava/lang/String;Ljava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v1

    .line 1200
    invoke-interface {v0, v7, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1198
    .end local v9    # "pathSegment":Ljava/lang/String;
    add-int/lit8 v7, v7, 0x1

    goto :goto_7

    .line 1203
    .end local v7    # "i":I
    .end local v8    # "size":I
    :cond_25
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedQueryNamesAndValues:Ljava/util/List;

    if-eqz v0, :cond_50

    .line 1204
    const/4 v7, 0x0

    .local v7, "i":I
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedQueryNamesAndValues:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    .local v8, "size":I
    :goto_30
    if-ge v7, v8, :cond_50

    .line 1205
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedQueryNamesAndValues:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Ljava/lang/String;

    .line 1206
    .local v9, "component":Ljava/lang/String;
    if-eqz v9, :cond_4d

    .line 1207
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedQueryNamesAndValues:Ljava/util/List;

    move-object v1, v9

    const-string v2, "\\^`{|}"

    .line 1208
    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-static/range {v1 .. v6}, Lokhttp3/HttpUrl;->canonicalize(Ljava/lang/String;Ljava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v1

    .line 1207
    invoke-interface {v0, v7, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1204
    .end local v9    # "component":Ljava/lang/String;
    :cond_4d
    add-int/lit8 v7, v7, 0x1

    goto :goto_30

    .line 1212
    .end local v7    # "i":I
    .end local v8    # "size":I
    :cond_50
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedFragment:Ljava/lang/String;

    if-eqz v0, :cond_62

    .line 1213
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedFragment:Ljava/lang/String;

    const-string v1, " \"#<>\\^`{|}"

    const/4 v2, 0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lokhttp3/HttpUrl;->canonicalize(Ljava/lang/String;Ljava/lang/String;ZZZZ)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedFragment:Ljava/lang/String;

    .line 1216
    :cond_62
    return-object p0
.end method

.method public scheme(Ljava/lang/String;)Lokhttp3/HttpUrl$Builder;
    .registers 5
    .param p1, "scheme"    # Ljava/lang/String;

    .line 950
    if-nez p1, :cond_a

    .line 951
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "scheme == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 952
    :cond_a
    const-string v0, "http"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 953
    const-string v0, "http"

    iput-object v0, p0, Lokhttp3/HttpUrl$Builder;->scheme:Ljava/lang/String;

    goto :goto_3d

    .line 954
    :cond_17
    const-string v0, "https"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 955
    const-string v0, "https"

    iput-object v0, p0, Lokhttp3/HttpUrl$Builder;->scheme:Ljava/lang/String;

    goto :goto_3d

    .line 957
    :cond_24
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unexpected scheme: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 959
    :goto_3d
    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    .line 1226
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1227
    .local v2, "result":Ljava/lang/StringBuilder;
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->scheme:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1228
    const-string v0, "://"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1230
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedUsername:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedPassword:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3b

    .line 1231
    :cond_1f
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedUsername:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1232
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedPassword:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_36

    .line 1233
    const/16 v0, 0x3a

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1234
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedPassword:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1236
    :cond_36
    const/16 v0, 0x40

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1239
    :cond_3b
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->host:Ljava/lang/String;

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_56

    .line 1241
    const/16 v0, 0x5b

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1242
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->host:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1243
    const/16 v0, 0x5d

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_5b

    .line 1245
    :cond_56
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->host:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1248
    :goto_5b
    invoke-virtual {p0}, Lokhttp3/HttpUrl$Builder;->effectivePort()I

    move-result v3

    .line 1249
    .local v3, "effectivePort":I
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->scheme:Ljava/lang/String;

    invoke-static {v0}, Lokhttp3/HttpUrl;->defaultPort(Ljava/lang/String;)I

    move-result v0

    if-eq v3, v0, :cond_6f

    .line 1250
    const/16 v0, 0x3a

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1251
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1254
    :cond_6f
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedPathSegments:Ljava/util/List;

    invoke-static {v2, v0}, Lokhttp3/HttpUrl;->pathSegmentsToString(Ljava/lang/StringBuilder;Ljava/util/List;)V

    .line 1256
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedQueryNamesAndValues:Ljava/util/List;

    if-eqz v0, :cond_82

    .line 1257
    const/16 v0, 0x3f

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1258
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedQueryNamesAndValues:Ljava/util/List;

    invoke-static {v2, v0}, Lokhttp3/HttpUrl;->namesAndValuesToQueryString(Ljava/lang/StringBuilder;Ljava/util/List;)V

    .line 1261
    :cond_82
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedFragment:Ljava/lang/String;

    if-eqz v0, :cond_90

    .line 1262
    const/16 v0, 0x23

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1263
    iget-object v0, p0, Lokhttp3/HttpUrl$Builder;->encodedFragment:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1266
    :cond_90
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

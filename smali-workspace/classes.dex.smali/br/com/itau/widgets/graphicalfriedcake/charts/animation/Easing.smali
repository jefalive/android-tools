.class public Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing;
.super Ljava/lang/Object;
.source "Easing.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;,
        Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    return-void
.end method

.method public static getEasingFunctionFromOption(Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;)Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;
    .registers 3
    .param p0, "easing"    # Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;

    .line 46
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$1;->$SwitchMap$br$com$itau$widgets$graphicalfriedcake$charts$animation$Easing$EasingOption:[I

    invoke-virtual {p0}, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingOption;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_60

    nop

    .line 49
    :pswitch_c
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->Linear:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 51
    :pswitch_f
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseInQuad:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 53
    :pswitch_12
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseOutQuad:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 55
    :pswitch_15
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseInOutQuad:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 57
    :pswitch_18
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseInCubic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 59
    :pswitch_1b
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseOutCubic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 61
    :pswitch_1e
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseInOutCubic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 63
    :pswitch_21
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseInQuart:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 65
    :pswitch_24
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseOutQuart:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 67
    :pswitch_27
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseInOutQuart:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 69
    :pswitch_2a
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseInSine:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 71
    :pswitch_2d
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseOutSine:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 73
    :pswitch_30
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseInOutSine:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 75
    :pswitch_33
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseInExpo:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 77
    :pswitch_36
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseOutExpo:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 79
    :pswitch_39
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseInOutExpo:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 81
    :pswitch_3c
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseInCirc:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 83
    :pswitch_3f
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseOutCirc:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 85
    :pswitch_42
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseInOutCirc:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 87
    :pswitch_45
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseInElastic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 89
    :pswitch_48
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseOutElastic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 91
    :pswitch_4b
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseInOutElastic:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 93
    :pswitch_4e
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseInBack:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 95
    :pswitch_51
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseOutBack:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 97
    :pswitch_54
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseInOutBack:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 99
    :pswitch_57
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseInBounce:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 101
    :pswitch_5a
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseOutBounce:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    .line 103
    :pswitch_5d
    sget-object v0, Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/Easing$EasingFunctions;->EaseInOutBounce:Lbr/com/itau/widgets/graphicalfriedcake/charts/animation/EasingFunction;

    return-object v0

    :pswitch_data_60
    .packed-switch 0x1
        :pswitch_c
        :pswitch_f
        :pswitch_12
        :pswitch_15
        :pswitch_18
        :pswitch_1b
        :pswitch_1e
        :pswitch_21
        :pswitch_24
        :pswitch_27
        :pswitch_2a
        :pswitch_2d
        :pswitch_30
        :pswitch_33
        :pswitch_36
        :pswitch_39
        :pswitch_3c
        :pswitch_3f
        :pswitch_42
        :pswitch_45
        :pswitch_48
        :pswitch_4b
        :pswitch_4e
        :pswitch_51
        :pswitch_54
        :pswitch_57
        :pswitch_5a
        :pswitch_5d
    .end packed-switch
.end method

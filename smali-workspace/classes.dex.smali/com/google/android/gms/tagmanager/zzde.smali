.class Lcom/google/android/gms/tagmanager/zzde;
.super Ljava/lang/Number;

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Number;Ljava/lang/Comparable<Lcom/google/android/gms/tagmanager/zzde;>;"
    }
.end annotation


# instance fields
.field private zzblB:D

.field private zzblC:J

.field private zzblD:Z


# direct methods
.method private constructor <init>(J)V
    .registers 4

    invoke-direct {p0}, Ljava/lang/Number;-><init>()V

    iput-wide p1, p0, Lcom/google/android/gms/tagmanager/zzde;->zzblC:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/tagmanager/zzde;->zzblD:Z

    return-void
.end method

.method public static zzam(J)Lcom/google/android/gms/tagmanager/zzde;
    .registers 3

    new-instance v0, Lcom/google/android/gms/tagmanager/zzde;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/tagmanager/zzde;-><init>(J)V

    return-object v0
.end method


# virtual methods
.method public byteValue()B
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/tagmanager/zzde;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    int-to-byte v0, v0

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .registers 3

    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/tagmanager/zzde;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/tagmanager/zzde;->zza(Lcom/google/android/gms/tagmanager/zzde;)I

    move-result v0

    return v0
.end method

.method public doubleValue()D
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/tagmanager/zzde;->zzHv()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-wide v0, p0, Lcom/google/android/gms/tagmanager/zzde;->zzblC:J

    long-to-double v0, v0

    goto :goto_c

    :cond_a
    iget-wide v0, p0, Lcom/google/android/gms/tagmanager/zzde;->zzblB:D

    :goto_c
    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .registers 3
    .param p1, "other"    # Ljava/lang/Object;

    instance-of v0, p1, Lcom/google/android/gms/tagmanager/zzde;

    if-eqz v0, :cond_f

    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/tagmanager/zzde;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/tagmanager/zzde;->zza(Lcom/google/android/gms/tagmanager/zzde;)I

    move-result v0

    if-nez v0, :cond_f

    const/4 v0, 0x1

    goto :goto_10

    :cond_f
    const/4 v0, 0x0

    :goto_10
    return v0
.end method

.method public floatValue()F
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/tagmanager/zzde;->doubleValue()D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public hashCode()I
    .registers 4

    new-instance v0, Ljava/lang/Long;

    invoke-virtual {p0}, Lcom/google/android/gms/tagmanager/zzde;->longValue()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    return v0
.end method

.method public intValue()I
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/tagmanager/zzde;->zzHx()I

    move-result v0

    return v0
.end method

.method public longValue()J
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/tagmanager/zzde;->zzHw()J

    move-result-wide v0

    return-wide v0
.end method

.method public shortValue()S
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/tagmanager/zzde;->zzHy()S

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/tagmanager/zzde;->zzHv()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-wide v0, p0, Lcom/google/android/gms/tagmanager/zzde;->zzblC:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_13

    :cond_d
    iget-wide v0, p0, Lcom/google/android/gms/tagmanager/zzde;->zzblB:D

    invoke-static {v0, v1}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    :goto_13
    return-object v0
.end method

.method public zzHu()Z
    .registers 2

    invoke-virtual {p0}, Lcom/google/android/gms/tagmanager/zzde;->zzHv()Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    goto :goto_9

    :cond_8
    const/4 v0, 0x0

    :goto_9
    return v0
.end method

.method public zzHv()Z
    .registers 2

    iget-boolean v0, p0, Lcom/google/android/gms/tagmanager/zzde;->zzblD:Z

    return v0
.end method

.method public zzHw()J
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/tagmanager/zzde;->zzHv()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-wide v0, p0, Lcom/google/android/gms/tagmanager/zzde;->zzblC:J

    goto :goto_c

    :cond_9
    iget-wide v0, p0, Lcom/google/android/gms/tagmanager/zzde;->zzblB:D

    double-to-long v0, v0

    :goto_c
    return-wide v0
.end method

.method public zzHx()I
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/tagmanager/zzde;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public zzHy()S
    .registers 3

    invoke-virtual {p0}, Lcom/google/android/gms/tagmanager/zzde;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    int-to-short v0, v0

    return v0
.end method

.method public zza(Lcom/google/android/gms/tagmanager/zzde;)I
    .registers 6

    invoke-virtual {p0}, Lcom/google/android/gms/tagmanager/zzde;->zzHv()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-virtual {p1}, Lcom/google/android/gms/tagmanager/zzde;->zzHv()Z

    move-result v0

    if-eqz v0, :cond_1e

    new-instance v0, Ljava/lang/Long;

    iget-wide v1, p0, Lcom/google/android/gms/tagmanager/zzde;->zzblC:J

    invoke-direct {v0, v1, v2}, Ljava/lang/Long;-><init>(J)V

    iget-wide v1, p1, Lcom/google/android/gms/tagmanager/zzde;->zzblC:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    goto :goto_2a

    :cond_1e
    invoke-virtual {p0}, Lcom/google/android/gms/tagmanager/zzde;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/google/android/gms/tagmanager/zzde;->doubleValue()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    :goto_2a
    return v0
.end method

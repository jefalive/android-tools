.class Lcom/itau/empresas/controller/PagamentoDarfController$8;
.super Ljava/lang/Object;
.source "PagamentoDarfController.java"

# interfaces
.implements Lbr/com/itau/sdk/android/core/type/BackendCall;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/controller/PagamentoDarfController;->incluiComDuplicidade()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/controller/PagamentoDarfController;


# direct methods
.method constructor <init>(Lcom/itau/empresas/controller/PagamentoDarfController;)V
    .registers 2
    .param p1, "this$0"    # Lcom/itau/empresas/controller/PagamentoDarfController;

    .line 123
    iput-object p1, p0, Lcom/itau/empresas/controller/PagamentoDarfController$8;->this$0:Lcom/itau/empresas/controller/PagamentoDarfController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public callBackend()V
    .registers 2

    .line 126
    iget-object v0, p0, Lcom/itau/empresas/controller/PagamentoDarfController$8;->this$0:Lcom/itau/empresas/controller/PagamentoDarfController;

    invoke-virtual {v0}, Lcom/itau/empresas/controller/PagamentoDarfController;->api()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/api/Api;

    invoke-interface {v0}, Lcom/itau/empresas/api/Api;->incluiDarf()Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/InclusaoTributoResponseVO;

    move-result-object v0

    invoke-static {v0}, Lcom/itau/empresas/controller/BaseController;->post(Ljava/lang/Object;)V

    .line 127
    return-void
.end method

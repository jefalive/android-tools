.class public Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/CnpjPagamentosAdapter;
.super Landroid/widget/BaseAdapter;
.source "CnpjPagamentosAdapter.java"

# interfaces
.implements Landroid/widget/Filterable;


# instance fields
.field private cnpjPagamentos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;>;"
        }
    .end annotation
.end field

.field private cnpjVOList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 19
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .registers 2

    .line 36
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/CnpjPagamentosAdapter;->cnpjPagamentos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .registers 4

    .line 57
    new-instance v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/FiltroPagamentos;

    iget-object v1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/CnpjPagamentosAdapter;->cnpjPagamentos:Ljava/util/List;

    iget-object v2, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/CnpjPagamentosAdapter;->cnpjVOList:Ljava/util/List;

    invoke-direct {v0, v1, v2, p0}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/FiltroPagamentos;-><init>(Ljava/util/List;Ljava/util/List;Landroid/widget/BaseAdapter;)V

    return-object v0
.end method

.method public getItem(I)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;
    .registers 3
    .param p1, "i"    # I

    .line 52
    iget-object v0, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/CnpjPagamentosAdapter;->cnpjPagamentos:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .registers 3

    .line 19
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/CnpjPagamentosAdapter;->getItem(I)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .registers 4
    .param p1, "position"    # I

    .line 31
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .registers 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .line 40
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 41
    .local v2, "inflater":Landroid/view/LayoutInflater;
    if-nez p2, :cond_13

    const v0, 0x7f0300f4

    const/4 v1, 0x0

    invoke-virtual {v2, v0, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    goto :goto_14

    :cond_13
    move-object v3, p2

    .line 43
    .local v3, "view":Landroid/view/View;
    :goto_14
    invoke-virtual {p0, p1}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/CnpjPagamentosAdapter;->getItem(I)Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;

    move-result-object v4

    .line 45
    .local v4, "cnpj":Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;
    const v0, 0x7f0e0528

    invoke-static {v3, v0}, Lcom/itau/empresas/ui/util/ViewHolder;->get(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/widget/TextView;

    .line 46
    .local v5, "numeroCnpj":Landroid/widget/TextView;
    sget-object v0, Lbr/com/concretesolutions/canarinho/formatador/FormatadorCPFCNPJ;->CNPJ:Lbr/com/concretesolutions/canarinho/formatador/Formatador;

    invoke-virtual {v4}, Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;->getCnpj()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lbr/com/concretesolutions/canarinho/formatador/Formatador;->formata(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    return-object v3
.end method

.method public setItems(Ljava/util/List;)V
    .registers 2
    .param p1, "cnpjBancos"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Ljava/util/List<Lcom/itau/empresas/feature/pagamento/semcodigobarra/model/CnpjVO;>;)V"
        }
    .end annotation

    .line 25
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/CnpjPagamentosAdapter;->cnpjPagamentos:Ljava/util/List;

    .line 26
    iput-object p1, p0, Lcom/itau/empresas/feature/pagamento/semcodigobarra/adapter/CnpjPagamentosAdapter;->cnpjVOList:Ljava/util/List;

    .line 27
    return-void
.end method

.class Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$2;
.super Ljava/lang/Object;
.source "LinearLayoutWrapper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;->setupAdapter(Landroid/widget/Adapter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;

.field final synthetic val$adapter:Landroid/widget/Adapter;

.field final synthetic val$observer:Landroid/database/DataSetObserver;


# direct methods
.method constructor <init>(Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;Landroid/widget/Adapter;Landroid/database/DataSetObserver;)V
    .registers 4
    .param p1, "this$0"    # Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;

    .line 54
    iput-object p1, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$2;->this$0:Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper;

    iput-object p2, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$2;->val$adapter:Landroid/widget/Adapter;

    iput-object p3, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$2;->val$observer:Landroid/database/DataSetObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    .line 58
    :try_start_0
    iget-object v0, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$2;->val$adapter:Landroid/widget/Adapter;

    iget-object v1, p0, Lcom/itau/empresas/ui/view/wraper/LinearLayoutWrapper$2;->val$observer:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V
    :try_end_7
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_7} :catch_8

    .line 61
    goto :goto_10

    .line 59
    :catch_8
    move-exception v2

    .line 60
    .local v2, "e":Ljava/lang/IllegalStateException;
    const-string v0, "LinearLayoutAdapter"

    const-string v1, "tryBuildViews: Hey guys, looks like we got one exception when build a new view"

    invoke-static {v0, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 62
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    :goto_10
    return-void
.end method

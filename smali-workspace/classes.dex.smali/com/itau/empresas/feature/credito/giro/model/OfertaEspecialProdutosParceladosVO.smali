.class public Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;
.super Ljava/lang/Object;
.source "OfertaEspecialProdutosParceladosVO.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private codigoBandeiraCartao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_bandeira_cartao"
    .end annotation
.end field

.field private codigoPlanoFinanciamento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_plano_financiamento"
    .end annotation
.end field

.field private codigoProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "codigo_produto"
    .end annotation
.end field

.field private dataContrato:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_contrato"
    .end annotation
.end field

.field private dataPrimeiroVencimento:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "data_primeiro_vencimento"
    .end annotation
.end field

.field private descricaoBandeiraCartao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "descricao_bandeira_cartao"
    .end annotation
.end field

.field private hasIndicadorSeguro:Z
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "indicador_seguro"
    .end annotation
.end field

.field private nomeProduto:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "nome_produto"
    .end annotation
.end field

.field private prazoMaximoContratacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "prazo_maximo_contratacao"
    .end annotation
.end field

.field private prazoMinimoContratacao:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "prazo_minimo_contratacao"
    .end annotation
.end field

.field private quantidadeDiasPrimeiroPagamento:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade_dias_primeiro_pagamento"
    .end annotation
.end field

.field private quantidadeParcelas:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "quantidade_parcelas"
    .end annotation
.end field

.field private taxaJuros:D
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "taxa_juros"
    .end annotation
.end field

.field private tipoOferta:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "tipo_oferta"
    .end annotation
.end field

.field private valorOferta:Ljava/math/BigDecimal;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_oferta"
    .end annotation
.end field

.field private valorParcela:D
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "valor_parcela"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCodigoBandeiraCartao()Ljava/lang/String;
    .registers 2

    .line 71
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->codigoBandeiraCartao:Ljava/lang/String;

    return-object v0
.end method

.method public getCodigoPlanoFinanciamento()Ljava/lang/String;
    .registers 2

    .line 151
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->codigoPlanoFinanciamento:Ljava/lang/String;

    return-object v0
.end method

.method public getCodigoProduto()Ljava/lang/String;
    .registers 2

    .line 135
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->codigoProduto:Ljava/lang/String;

    return-object v0
.end method

.method public getDataContrato()Ljava/lang/String;
    .registers 2

    .line 111
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->dataContrato:Ljava/lang/String;

    return-object v0
.end method

.method public getDataPrimeiroVencimento()Ljava/lang/String;
    .registers 2

    .line 119
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->dataPrimeiroVencimento:Ljava/lang/String;

    return-object v0
.end method

.method public getDescricaoBandeiraCartao()Ljava/lang/String;
    .registers 2

    .line 79
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->descricaoBandeiraCartao:Ljava/lang/String;

    return-object v0
.end method

.method public getNomeProduto()Ljava/lang/String;
    .registers 2

    .line 143
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->nomeProduto:Ljava/lang/String;

    return-object v0
.end method

.method public getQuantidadeDiasPrimeiroPagamento()I
    .registers 2

    .line 103
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->quantidadeDiasPrimeiroPagamento:I

    return v0
.end method

.method public getQuantidadeParcelas()I
    .registers 2

    .line 55
    iget v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->quantidadeParcelas:I

    return v0
.end method

.method public getTaxaJuros()D
    .registers 3

    .line 87
    iget-wide v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->taxaJuros:D

    return-wide v0
.end method

.method public getTipoOferta()Ljava/lang/String;
    .registers 2

    .line 127
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->tipoOferta:Ljava/lang/String;

    return-object v0
.end method

.method public getValorOferta()Ljava/math/BigDecimal;
    .registers 2

    .line 47
    iget-object v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->valorOferta:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getValorParcela()D
    .registers 3

    .line 63
    iget-wide v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->valorParcela:D

    return-wide v0
.end method

.method public isHasIndicadorSeguro()Z
    .registers 2

    .line 95
    iget-boolean v0, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->hasIndicadorSeguro:Z

    return v0
.end method

.method public setCodigoPlanoFinanciamento(Ljava/lang/String;)V
    .registers 2
    .param p1, "codigoPlanoFinanciamento"    # Ljava/lang/String;

    .line 155
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->codigoPlanoFinanciamento:Ljava/lang/String;

    .line 156
    return-void
.end method

.method public setCodigoProduto(Ljava/lang/String;)V
    .registers 2
    .param p1, "codigoProduto"    # Ljava/lang/String;

    .line 139
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->codigoProduto:Ljava/lang/String;

    .line 140
    return-void
.end method

.method public setHasIndicadorSeguro(Z)V
    .registers 2
    .param p1, "hasIndicadorSeguro"    # Z

    .line 99
    iput-boolean p1, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->hasIndicadorSeguro:Z

    .line 100
    return-void
.end method

.method public setNomeProduto(Ljava/lang/String;)V
    .registers 2
    .param p1, "nomeProduto"    # Ljava/lang/String;

    .line 147
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->nomeProduto:Ljava/lang/String;

    .line 148
    return-void
.end method

.method public setTipoOferta(Ljava/lang/String;)V
    .registers 2
    .param p1, "tipoOferta"    # Ljava/lang/String;

    .line 131
    iput-object p1, p0, Lcom/itau/empresas/feature/credito/giro/model/OfertaEspecialProdutosParceladosVO;->tipoOferta:Ljava/lang/String;

    .line 132
    return-void
.end method

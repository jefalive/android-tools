.class public Lcom/google/android/gms/common/internal/zza;
.super Lcom/google/android/gms/common/internal/zzp$zza;


# instance fields
.field private mContext:Landroid/content/Context;

.field private zzTI:Landroid/accounts/Account;

.field zzakz:I


# direct methods
.method public static zza(Lcom/google/android/gms/common/internal/zzp;)Landroid/accounts/Account;
    .registers 8

    const/4 v2, 0x0

    if-eqz p0, :cond_21

    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v3

    :try_start_7
    invoke-interface {p0}, Lcom/google/android/gms/common/internal/zzp;->getAccount()Landroid/accounts/Account;
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_a} :catch_10
    .catchall {:try_start_7 .. :try_end_a} :catchall_1c

    move-result-object v0

    move-object v2, v0

    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_21

    :catch_10
    move-exception v5

    const-string v0, "AccountAccessor"

    const-string v1, "Remote account accessor probably died"

    :try_start_15
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_18
    .catchall {:try_start_15 .. :try_end_18} :catchall_1c

    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_21

    :catchall_1c
    move-exception v6

    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v6

    :cond_21
    :goto_21
    return-object v2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .registers 4
    .param p1, "o"    # Ljava/lang/Object;

    if-ne p0, p1, :cond_4

    const/4 v0, 0x1

    return v0

    :cond_4
    instance-of v0, p1, Lcom/google/android/gms/common/internal/zza;

    if-nez v0, :cond_a

    const/4 v0, 0x0

    return v0

    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/common/internal/zza;->zzTI:Landroid/accounts/Account;

    move-object v1, p1

    check-cast v1, Lcom/google/android/gms/common/internal/zza;

    iget-object v1, v1, Lcom/google/android/gms/common/internal/zza;->zzTI:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getAccount()Landroid/accounts/Account;
    .registers 4

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    iget v0, p0, Lcom/google/android/gms/common/internal/zza;->zzakz:I

    if-ne v2, v0, :cond_b

    iget-object v0, p0, Lcom/google/android/gms/common/internal/zza;->zzTI:Landroid/accounts/Account;

    return-object v0

    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/common/internal/zza;->mContext:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/google/android/gms/common/zze;->zzf(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_18

    iput v2, p0, Lcom/google/android/gms/common/internal/zza;->zzakz:I

    iget-object v0, p0, Lcom/google/android/gms/common/internal/zza;->zzTI:Landroid/accounts/Account;

    return-object v0

    :cond_18
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Caller is not GooglePlayServices"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
